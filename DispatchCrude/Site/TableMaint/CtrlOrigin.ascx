﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CtrlOrigin.ascx.cs" Inherits="DispatchCrude.Site.TableMaint.CtrlOrigin" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<style type="text/css">
    .rgEditForm {
            width: auto !important;
    }
    .rgEditForm > div + div,
    .RadGrid .rgEditForm {
            height: auto !important;
    }
    .rgEditForm > div > table{
            height: 100%;
    }
    .rgEditForm > div > table > tbody > tr > td{
            padding: 4px 10px;
    }
    .H2S
    {
        color: red;
    }
    .Deleted
    {
        color: gray;
    }
    div.spacer
    {
        height: 8px;
    }
    .label
    {
        color:black;
    }
</style>
<%--<link href="~/Styles/tablemx.css?version=1.0" rel="stylesheet" type="text/css" />--%>
<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server" >
    <script type="text/javascript">
        function validateItemSpecified(sender, args) {
            debugger;
            //invalid if first character is a "("
            args.IsValid = !args.Value.startsWith("(");
        };
    </script>
</telerik:RadScriptBlock>
<asp:Table id="tblMain" runat="server" border="0" style="border-collapse: collapse">
    <asp:TableRow ID="trData" runat="server" style="vertical-align:top;" >
        <asp:TableCell runat="server" >
            <div class="label">
                  <asp:Label runat="server" Text="Type:<span style='color:red;'>*</span>" />
            </div>
            <div class ="data">
                <asp:HiddenField ID="hfID" runat="server" />
                <telerik:RadComboBox ID="rcbOriginType" runat="server" Width="200px" ClientIDMode="Static" 
                    DataSourceID="dsOriginType" DataTextField="Name" DataValueField="ID" EmptyMessage="Select Origin Type"
                    ValidationGroup="OriginEdit" CausesValidation="false" />
                <asp:RequiredFieldValidator ID="rfvOriginType" runat="server" 
                    ControlToValidate="rcbOriginType" ValidationGroup="OriginEdit"
                    Text="&nbsp;!" ErrorMessage="A Type must be specified" CssClass="NullValidator" />
            </div>
            <div class="spacer" ></div>
            <div class="label">
                  <asp:Label runat="server" Text="Name:<span style='color:red;'>*</span>" />
            </div>
            <div class ="data">
                <telerik:RadTextBox ID="rtxName" runat="server" Width="200px" ClientIDMode="Static" MaxLength="50" 
                    Text='<%# Eval("Name") %>' ValidationGroup="OriginEdit" />
                <asp:RequiredFieldValidator ID="rfvName" runat="server" 
                    ControlToValidate="rtxName" ValidationGroup="OriginEdit"
                    Text="&nbsp;!" ErrorMessage="A Name must be specified" CssClass="NullValidator" />
            </div>
            <div class="spacer" ></div>
            <div class="label">
                  <asp:Label runat="server" Text="Lease Name:<span style='color:red;'>*</span>" />
            </div>
            <div class ="data">
                <telerik:RadTextBox ID="rtxLeaseName" runat="server" Width="200px" ClientIDMode="Static" MaxLength="50" 
                    Text='<%# Eval("LeaseName") %>' ValidationGroup="OriginEdit" />
                <asp:RequiredFieldValidator ID="rfvLeaseName" runat="server" 
                    ControlToValidate="rtxLeaseName" ValidationGroup="OriginEdit"
                    Text="&nbsp;!" ErrorMessage="A Lease Name must be specified" CssClass="NullValidator" />
            </div>
            <div class="spacer" ></div>
            <div class="label">
                  <asp:Label runat="server" Text="Lease #" />
            </div>
            <div class ="data">
                <telerik:RadTextBox ID="rtxLeaseNum" runat="server" Width="200px" ClientIDMode="Static" MaxLength="30" 
                    Text='<%# Eval("LeaseNum") %>' ValidationGroup="OriginEdit" />
            </div>
            <div class="spacer" ></div>
            <div class ="data">
                <asp:CheckBox ID="chkH2S" runat="server" Text="H2S?" Checked='<%# AlonsIT.DBHelper.ToBoolean(Eval("H2S")) %>' />
            </div>
            <div class="spacer" ></div>
            <div class="label">
                  <asp:Label runat="server" Text="Station:" />
            </div>
            <div class ="data">
                <telerik:RadTextBox ID="rtxStation" runat="server" Width="200px" ClientIDMode="Static" MaxLength="25" 
                    Text='<%# Eval("Station") %>' ValidationGroup="OriginEdit"/>
            </div>
            <div class="spacer" ></div>
            <div class="label">
                  <asp:Label runat="server" Text="Ticket Type:<span style='color:red;'>*</span>" />
            </div>
            <div class ="data">
                <telerik:RadComboBox ID="rcbTicketType" runat="server" Width="200px" ClientIDMode="Static" 
                    DataSourceID="dsTicketType" DataTextField="Name" DataValueField="ID" EmptyMessage="Select Ticket Type"
                    ValidationGroup="OriginEdit" CausesValidation="false"
                    AutoPostBack="true" /> 
                <asp:RequiredFieldValidator ID="rfvTicketType" runat="server" 
                    ControlToValidate="rcbTicketType" ValidationGroup="OriginEdit"
                    Text="&nbsp;!" ErrorMessage="A Ticket Type must be specified" CssClass="NullValidator" />
            </div>
            <div class="spacer" ></div>
            <div class="label">
                  <asp:Label runat="server" Text="Products:<span style='color:red;'>*</span>" />
            </div>
            <div class ="data">
                <telerik:RadComboBox ID="rcbProducts" runat="server" Width="200px" ClientIDMode="Static" 
                    DataSourceID="dsProduct" DataTextField="ShortName" DataValueField="ID" CheckBoxes="true" DataCheckedField="Checked"
                    EnableCheckAllItemsCheckBox="true" EmptyMessage="Select Product(s)"
                    ValidationGroup="OriginEdit" CausesValidation="false" />
                <asp:RequiredFieldValidator ID="rfvProducts" runat="server" 
                    ControlToValidate="rcbProducts" ValidationGroup="OriginEdit"
                    Text="&nbsp;!" ErrorMessage="At least 1 product must be specified" CssClass="NullValidator" />
            </div>
       </asp:TableCell>
        <asp:TableCell>
            <div class="label">
                  <asp:Label runat="server" Text="Country:<span style='color:red;'>*</span>" />
            </div>
            <div class ="data">
                <telerik:RadComboBox ID="rcbCountry" runat="server" Width="200px" ClientIDMode="Static" 
                    DataSourceID="dsCountry" DataTextField="Name" DataValueField="ID" EmptyMessage="Select Country"
                    ValidationGroup="OriginEdit" CausesValidation="false"
                    Enabled='<%# (DataItem is Telerik.Web.UI.GridInsertionObject) %>'
                    AutoPostBack="true" OnSelectedIndexChanged="rcbCountry_SelectedIndexChanged" />
                <asp:RequiredFieldValidator ID="rfvCountry" runat="server" 
                    ControlToValidate="rcbCountry" ValidationGroup="OriginEdit"
                    Text="&nbsp;!" ErrorMessage="A Country must be specified" CssClass="NullValidator" />
            </div>
            <div class="spacer"></div>
            <div class="label">
                  <asp:Label runat="server" Text="State/Prov:<span style='color:red;'>*</span>" />
            </div>
            <div class ="data">
                <telerik:RadComboBox ID="rcbState" runat="server" Width="200px" ClientIDMode="Static" 
                    DataSourceID="dsState" DataTextField="Name" DataValueField="ID" EmptyMessage="Select State"
                    ValidationGroup="OriginEdit" CausesValidation="false" />
                <asp:RequiredFieldValidator ID="rfvState" runat="server" 
                    ControlToValidate="rcbState" ValidationGroup="OriginEdit"
                    Text="&nbsp;!" ErrorMessage="A State must be specified" CssClass="NullValidator" />
            </div>
            <div class="spacer"></div>
            <div class="label">
                  <asp:Label runat="server" Text="Region:<span style='color:red;'>*</span>" />
            </div>
            <div class ="data">
                <telerik:RadComboBox ID="rcbRegion" runat="server" Width="200px" ClientIDMode="Static" 
                    DataSourceID="dsRegion" DataTextField="Name" DataValueField="ID" EmptyMessage="Select Region"
                    ValidationGroup="OriginEdit" CausesValidation="false" />
                <asp:RequiredFieldValidator ID="rfvRegion" runat="server" 
                    ControlToValidate="rcbRegion" ValidationGroup="OriginEdit"
                    Text="&nbsp;!" ErrorMessage="A Region must be specified" CssClass="NullValidator" />
            </div>
            <div class="spacer"></div>
            <div class="label">
                  <asp:Label runat="server" Text="County:" />
            </div>
            <div class ="data">
                <telerik:RadComboBox ID="rcboCounty" runat="server" AllowCustomText="true" Filter="StartsWith" ShowDropDownOnTextboxClick="true"
                    DataSourceID="dsCounty" DataTextField="County" DataValueField="County" Text='<%# Eval("County") %>' MaxLength="25" 
                    Width="200px" />
            </div>
            <div class="spacer"></div>
            <div class="label">
                  <asp:Label runat="server" Text="Address:" />
            </div>
            <div class ="data">
                <telerik:RadTextBox ID="rtxAddress" runat="server" Width="200px" ClientIDMode="Static" MaxLength="50" 
                    Text='<%# Eval("Address") %>' ValidationGroup="OriginEdit" />
            </div>
            <div class="spacer"></div>
            <div class="label">
                  <asp:Label runat="server" Text="City:" />
            </div>
            <div class ="data">
                <telerik:RadTextBox ID="rtxCity" runat="server" Width="200px" ClientIDMode="Static" MaxLength="30" 
                    Text='<%# Eval("City") %>' ValidationGroup="OriginEdit" />
            </div>
            <div class="spacer"></div>
            <div class="label">
                  <asp:Label runat="server" Text="Zip:" />
            </div>
            <div class ="data">
                <telerik:RadTextBox ID="rtxZip" runat="server" Width="200px" ClientIDMode="Static" MaxLength="10" 
                    Text='<%# Eval("Zip") %>' ValidationGroup="OriginEdit" />
            </div>
            <div class="spacer"></div>
            <div class="label">
                  <asp:Label runat="server" Text="Time Zone/DST:<span style='color:red;'>*</span>" />
            </div>
            <div class ="data">
                <telerik:RadComboBox ID="rcbTimeZone" runat="server" 
                    DataSourceID="dsTimeZone" DataTextField="Name" DataValueField="ID" Width="150px" 
                    SelectedValue='<%# Eval("TimeZoneID") %>' 
                    ValidationGroup="OriginEdit" CausesValidation="false" />
                <asp:RequiredFieldValidator ID="rfvTimeZone" runat="server" ControlToValidate="rcbTimeZone" 
                    Display="Dynamic" Text="&nbsp;!" ErrorMessage="Time Zone must be specified" CssClass="NullValidator" />
                <asp:CheckBox ID="chkDST" runat="server" Checked='<%# Eval("UseDST") %>' />
            </div>
        </asp:TableCell>
        <asp:TableCell>
            <div class="label">
                  <asp:Label runat="server" Text="Shippers:<span style='color:red;'>*</span>" />
            </div>
            <div class ="data">
                <telerik:RadComboBox ID="rcbShippers" runat="server" Width="200px" ClientIDMode="Static" 
                        DataSourceID="dsShippers" DataTextField="Name" DataValueField="ID" CheckBoxes="true" DataCheckedField="Checked"
                        EnableCheckAllItemsCheckBox="true" EmptyMessage="Select Shipper"
                        ValidationGroup="OriginEdit" CausesValidation="false" />
                <asp:RequiredFieldValidator ID="rfvShippers" runat="server" 
                    ControlToValidate="rcbShippers" ValidationGroup="OriginEdit"
                    Text="&nbsp;!" ErrorMessage="At least 1 Shipper must be specified" CssClass="NullValidator" />
            </div>
            <div class="spacer"></div>
            <div class="label">
                  <asp:Label runat="server" Text="Producer:<span style='color:red;'>*</span>" />
            </div>
            <div class ="data">
                <telerik:RadComboBox ID="rcbProducer" runat="server" Width="200px" ClientIDMode="Static" 
                        DataSourceID="dsProducer" DataTextField="Name" DataValueField="ID" EmptyMessage="Select Producer"
                        SelectedValue='<%# Eval("ProducerID") %>' ValidationGroup="OriginEdit" CausesValidation="false" />
                <asp:RequiredFieldValidator ID="rfvProducer" runat="server" 
                    ControlToValidate="rcbProducer" ValidationGroup="OriginEdit"
                    Text="&nbsp;!" ErrorMessage="A Producer must be specified" CssClass="NullValidator" />
            </div>
            <div class="spacer"></div>
            <div class="label">
                  <asp:Label runat="server" Text="Operator:<span style='color:red;'>*</span>" />
            </div>
            <div class ="data">
                <telerik:RadComboBox ID="rcbOperator" runat="server" Width="200px" ClientIDMode="Static" 
                        DataSourceID="dsOperator" DataTextField="Name" DataValueField="ID" EmptyMessage="Select Operator"
                        ValidationGroup="OriginEdit" CausesValidation="false" 
                        AutoPostBack="true" OnSelectedIndexChanged="rcbOperator_SelectedIndexChanged" />
                <asp:RequiredFieldValidator ID="rfvOperator" runat="server" 
                    ControlToValidate="rcbOperator" ValidationGroup="OriginEdit"
                    Text="&nbsp;!" ErrorMessage="A Operator must be specified" CssClass="NullValidator" />
            </div>
            <div class="spacer"></div>
            <div class="label">
                  <asp:Label runat="server" Text="Pumper:" />
            </div>
            <div class ="data">
                <telerik:RadComboBox ID="rcbPumper" runat="server" Width="200px" ClientIDMode="Static" 
                        DataSourceID="dsPumper" DataTextField="Name" DataValueField="ID" 
                        ValidationGroup="OriginEdit" CausesValidation="false" EmptyMessage="Select Pumper"/>
            </div>
            <div class="spacer"></div>
            <div class="label">
                  <asp:Label runat="server" Text="Legal Description/LSD:" />
            </div>
            <div class ="data">
                <telerik:RadTextBox ID="rtxLegalDescription" runat="server" Width="200px" ClientIDMode="Static" MaxLength="50" 
                    Text='<%# Eval("LegalDescription") %>' ValidationGroup="OriginEdit" />
            </div>
            <div class="spacer"></div>
            <div class="label">
                  <asp:Label runat="server" Text="Field Name:" />
            </div>
            <div class ="data">
                <telerik:RadComboBox ID="rcbFieldName" runat="server" AllowCustomText="true" Filter="StartsWith" ShowDropDownOnTextboxClick="true"
                    DataSourceID="dsFieldName" DataTextField="FieldName" DataValueField="FieldName" Text='<%# Eval("FieldName") %>' MaxLength="25" 
                    Width="200px" />
            </div>
            <div class="spacer"></div>
            <div class="label">
                  <asp:Label runat="server" Text="Well API:" />
            </div>
            <div class ="data">
                <telerik:RadTextBox ID="rtxWellAPI" runat="server" Width="200px" ClientIDMode="Static" MaxLength="20" 
                    Text='<%# Eval("WellAPI") %>' ValidationGroup="OriginEdit" />
            </div>
            <div class="spacer"></div>
            <div class="label">
                  <asp:Label runat="server" Text="LAT/LON | Geo Fence (Meters):" />
            </div>
            <div class ="data">
                <telerik:RadTextBox ID="rtxLAT" runat="server" Width="73px" ClientIDMode="Static" MaxLength="20" 
                    Text='<%# Eval("LAT") %>' ValidationGroup="OriginEdit" />
                /
                <telerik:RadTextBox ID="rtxLON" runat="server" Width="73px" ClientIDMode="Static" MaxLength="20" 
                    Text='<%# Eval("LON") %>' ValidationGroup="OriginEdit" />
                &nbsp;
                <telerik:RadNumericTextBox ID="rntxGeoFenceRadiusMeters" runat="server" ClientIDMode="Static"
                    DataType="System.Int32" NumberFormat-DecimalDigits="0"
                    Width="35px" DbValue='<%# Eval("GeoFenceRadiusMeters") %>'  />
                <br />
                <asp:RegularExpressionValidator ID="revLAT" runat="server" ControlToValidate="rtxLAT" 
                    ValidationExpression="^[0-9\-\.]+$"
                    ValidationGroup="OriginEdit" Text="Invalid Lattitude was provided." CssClass="NullValidator" Display="Dynamic" />
                <br />
                <asp:RegularExpressionValidator ID="revLON" runat="server" ControlToValidate="rtxLON" 
                    ValidationExpression="^[0-9\-\.]+$"
                    ValidationGroup="OriginEdit" Text="Invalid Longitude was provided." CssClass="NullValidator" Display="Dynamic" />
            </div>
        </asp:TableCell>
        <asp:TableCell>
            <div class="label">
                  <asp:Label runat="server" Text="UOM:<span style='color:red;'>*</span>" />
            </div>
            <div class ="data">
                <telerik:RadComboBox ID="rcbUom" runat="server" Width="200px" ClientIDMode="Static" 
                    DataSourceID="dsUom" DataTextField="Name" DataValueField="ID" EmptyMessage="Select..."
                    SelectedValue='<%# Eval("UomID") %>'
                    ValidationGroup="OriginEdit" CausesValidation="false" />
                <asp:RequiredFieldValidator ID="rfvUom" runat="server" 
                    ControlToValidate="rcbUom" ValidationGroup="OriginEdit"
                    Text="&nbsp;!" ErrorMessage="A UOM must be specified" CssClass="NullValidator" />
            </div>
            <div class="spacer"></div>
            <div class="label">
                  <asp:Label runat="server" Text="NDIC File#:" />
            </div>
            <div class ="data">
                <telerik:RadTextBox ID="rtxNDIC" runat="server" Width="200px" ClientIDMode="Static" MaxLength="10" 
                    Text='<%# Eval("NDICFileNum") %>' />
            </div>
            <div class="spacer"></div>
            <div class="label">
                  <asp:Label runat="server" Text="CTB #:" />
            </div>
            <div class ="data">
                <telerik:RadTextBox ID="rtxCTB" runat="server" Width="200px" ClientIDMode="Static" MaxLength="10" 
                    Text='<%# Eval("CTBNum") %>' />
            </div>
            <div class="spacer"></div>
            <div class="label">
                  <asp:Label runat="server" Text="CA:" />
            </div>
            <div class ="data">
                <telerik:RadTextBox ID="rtxCA" runat="server" Width="200px" ClientIDMode="Static" MaxLength="25" 
                    Text='<%# Eval("CA") %>' />
            </div>
            <div class="spacer"></div>
            <div class="label">
                  <asp:Label runat="server" Text="BLM Info:" />
            </div>
            <div class ="data">
                <telerik:RadTextBox ID="rtxNDM" runat="server" Width="200px" ClientIDMode="Static" MaxLength="50" 
                    Text='<%# Eval("NDM") %>' />
            </div>
            <div class="spacer"></div>
            <div class="label">
                  <asp:Label runat="server" Text="Spud Date:" />
            </div>
            <div class ="data">
                <telerik:RadDatePicker ID="rdpSpudDate" runat="server" Width="200px" ClientIDMode="Static" MaxLength="20" 
                    DBSelectedDate='<%# Eval("SpudDate") %>' >
                    <DateInput runat="server" DateFormat="M/d/yyyy" />
                    <DatePopupButton  Enabled="true" />
                </telerik:RadDatePicker>
            </div>
            <div class="spacer"></div>
            <div class="label">
                  <asp:Label runat="server" Text="Total Depth:" />
            </div>
            <div class ="data">
                <telerik:RadNumericTextBox ID="rntxTotalDepth" runat="server" ClientIDMode="Static" 
                    DataType="System.Int32" NumberFormat-DecimalDigits="0"
                    Width="200px" DbValue='<%# Eval("TotalDepth") %>' />
            </div>
            <div class="spacer"></div>
            <div class="label">
                  <asp:Label runat="server" Text="Tax Rate (%):" />
            </div>
            <div class ="data">
                <telerik:RadNumericTextBox ID="rntxTaxRate" runat="server" ClientIDMode="Static"
                    NumberFormat-DecimalDigits="2" Type="Percent"
                    Width="200px" DbValue='<%# Eval("TaxRate") %>' />
            </div>
        </asp:TableCell>
        <asp:TableCell>
            <div class="label">
                <asp:Label ID="lblDrivingDirections" runat="server" Text="Driving Directions" />
            </div>
            <div class ="data">
                <telerik:RadTextBox ID="txtDrivingDirections" runat="server" MaxLength="500" TextMode="MultiLine" Width="200px" Height="100px" 
                    Text='<%# Eval("DrivingDirections") %>' />
            </div>
            <div class="spacer"></div>
            <div class="label">
                  <asp:Label runat="server" Text="Sulfur Content:" />
            </div>
            <div class ="data">
                <telerik:RadNumericTextBox ID="rntxSulfurContent" runat="server" Width="200px" ClientIDMode="Static" MaxLength="10" 
                    NumberFormat-DecimalDigits="4" DbValue='<%# Eval("SulfurContent") %>' />
            </div>
            <div class="spacer"></div>
            <div class="label">
                  <asp:Label runat="server" Text="Private Road Miles (PRM):" />
            </div>
            <div class ="data">
                <telerik:RadTextBox ID="rtxPRM" runat="server" Width="200px" ClientIDMode="Static" MaxLength="10" 
                    Text='<%# Eval("PrivateRoadMiles") %>' ValidationGroup="OriginEdit" />
                <br />
                <asp:RegularExpressionValidator ID="revPRM" runat="server" ControlToValidate="rtxPRM" 
                    ValidationExpression="^\d{0,2}(\.\d{0,1})?$|^$"
                    ValidationGroup="OriginEdit" Text="Invalid PRM was provided. Format = (##.#)" CssClass="NullValidator" Display="Dynamic" />
            </div>
            <div class="spacer"></div>
            <div class="label">
                <asp:Label ID="lblGauger" runat="server" Text="Default Gauger:" />
            </div>
            <div class ="data">
                <telerik:RadComboBox ID="rcbGauger" runat="server" Width="200px" 
                    DataSourceID="dsGauger" DataTextField="FullName" DataValueField="ID" CausesValidation="false"
                    SelectedValue='<%# Eval("GaugerID") %>' EmptyMessage="Select Gauger" />
            </div>
            <div class="spacer"></div>
            <div class="label">
                <asp:Label ID="lblGaugerTicketType" runat="server" Text="Gauger Ticket Type:" />
            </div>
            <div class ="data">
                <telerik:RadComboBox ID="rcbGaugerTicketType" runat="server" Width="200px" 
                    DataSourceID="dsGaugerTicketType" DataTextField="Name" DataValueField="ID" SelectedValue='<%# Eval("GaugerTicketTypeID") %>' 
                    CausesValidation="false" ValidationGroup="OriginEdit" EmptyMessage="Select Ticket Type"/>
                <asp:CustomValidator ID="cvGaugerTicketType" runat="server" ValidationGroup="OriginEdit" 
                    ControlToValidate="rcbGaugerTicketType" ClientValidationFunction="validateItemSpecified" 
                    Text="&nbsp;!" ErrorMessage="Gauger Ticket Type required when Gauger is required" CssClass="NullValidator" />
            </div>
            <div class="spacer"></div>
            <div class ="data">
                <asp:CheckBox ID="chkGaugerRequired" runat="server" Text="Gauger Required?" ValidationGroup="OriginEdit"
                    CausesValidation="false" Checked='<%# Bind("GaugerRequired") %>' 
                    AutoPostBack="true" OnCheckedChanged="chkGaugerRequired_CheckedChanged" />
            </div>
            <div class="spacer"></div>
            <div class="label">
                  <asp:Label runat="server" Text="Terminal:" />
            </div>
            <div class ="data">
                <telerik:RadComboBox ID="rcbTermial" runat="server" Width="200px" ClientIDMode="Static" 
                    DataSourceID="dsTerminal" DataTextField="Name" DataValueField="ID" 
                    ValidationGroup="OriginEdit" CausesValidation="false" />
            </div>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow id="trControls" runat="server">
        <asp:TableCell runat="server" style="text-align:left" ColumnSpan="1" >
            <div style="height:10px;" ></div>
            <asp:Button ID="cmdUpdate" runat="server" Text="Update" CssClass="btn btn-blue shiny" 
                CommandName="Update" CausesValidation="true" ValidationGroup="OriginEdit"
                OnClick="UpdateDataItem" 
                Visible='<%# !(DataItem is Telerik.Web.UI.GridInsertionObject) %>'>
            </asp:Button>
            <asp:Button Text="Save New" runat="server" CssClass="floatLeft btn btn-blue shiny"
                CommandName="PerformInsert" CausesValidation="true" ValidationGroup="OriginEdit"
                OnClick="UpdateDataItem" 
                Visible='<%# (DataItem is Telerik.Web.UI.GridInsertionObject) %>' />
            &nbsp;
            <asp:Button ID="cmdCancel" runat="server" Text="Cancel" CssClass="btn btn-blue shiny" 
                CommandName="Cancel" CausesValidation="False" ValidationGroup="OriginEdit" />
        </asp:TableCell>
        <asp:TableCell ColumnSpan="4" >
            <asp:ValidationSummary ID="validationSummaryMain" runat="server" DisplayMode="BulletList" Enabled="true" EnableClientScript="true"
                ValidationGroup="OriginEdit"
                CssClass="NullValidator" />
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
<blac:DBDataSource ID="dsOriginType" runat="server" SelectCommand="SELECT ID, Name = OriginType FROM dbo.tblOriginType ORDER BY Name" />
<blac:DBDataSource ID="dsCountry" runat="server" SelectCommand="SELECT ID, Name FROM dbo.tblCountry ORDER BY Name" />
<blac:DBDataSource ID="dsProduct" runat="server" SelectCommand="SELECT P.ID, P.Name, P.ShortName, Checked = cast(CASE WHEN OP.ID IS NULL THEN 0 ELSE 1 END as bit) FROM dbo.tblProduct P LEFT JOIN tblOriginProducts OP ON OP.OriginID = @OriginID AND OP.ProductID = P.ID ORDER BY P.Name" >
    <SelectParameters>
        <asp:Parameter Name="OriginID" DefaultValue="0" />
    </SelectParameters>
</blac:DBDataSource>
<blac:DBDataSource ID="dsState" runat="server" SelectCommand="SELECT ID, Name = FullName FROM dbo.tblState WHERE (@CountryID = -1 OR CountryID = @CountryID) ORDER BY Name" >
    <SelectParameters>
        <asp:ControlParameter Name="CountryID" ControlID="rcbCountry" PropertyName="SelectedValue" DbType="String" DefaultValue="-1" />
    </SelectParameters>
</blac:DBDataSource>
<blac:DBDataSource ID="dsCounty" runat="server" SelectCommand="SELECT DISTINCT County FROM viewOrigin WHERE DeleteDateUTC IS NOT NULL AND County IS NOT NULL ORDER BY County" />
<blac:DBDataSource ID="dsFieldName" runat="server" SelectCommand="SELECT DISTINCT FieldName FROM viewOrigin WHERE DeleteDateUTC IS NOT NULL AND FieldName IS NOT NULL ORDER BY FieldName" />
<blac:DBDataSource ID="dsRegion" runat="server" SelectCommand="SELECT ID, Name FROM tblRegion ORDER BY Name" />
<blac:DBDataSource ID="dsTerminal" runat="server" SelectCommand="SELECT ID, Name FROM tblTerminal WHERE DeleteDateUTC IS NULL UNION SELECT NULL, '(All Terminals)' ORDER BY Name" />
<blac:DBDataSource ID="dsUOM" runat="server" SelectCommand="SELECT ID, Name FROM tblUom ORDER BY Name" />
<blac:DBDataSource ID="dsTimeZone" runat="server" SelectCommand="SELECT ID, Name FROM tblTimeZone ORDER BY Name" />
<blac:DBDataSource ID="dsShippers" runat="server" 
    SelectCommand="SELECT C.ID, C.Name, Checked = cast(CASE WHEN OC.ID IS NULL THEN 0 ELSE 1 END as bit) FROM dbo.tblCustomer C LEFT JOIN tblOriginCustomers OC ON OC.OriginID = @OriginID AND OC.CustomerID = C.ID ORDER BY C.Name" >
    <SelectParameters>
        <asp:ControlParameter Name="OriginID" ControlID="hfID" PropertyName="Value" DefaultValue="0" />
    </SelectParameters>
</blac:DBDataSource>
<blac:DBDataSource ID="dsProducer" runat="server" SelectCommand="SELECT ID, Name FROM dbo.tblProducer ORDER BY Name" />
<blac:DBDataSource ID="dsOperator" runat="server" SelectCommand="SELECT ID, Name FROM dbo.tblOperator ORDER BY Name" />
<blac:DBDataSource ID="dsPumper" runat="server" SelectCommand="SELECT ID, Name = FullName FROM dbo.viewPumper WHERE OperatorID = @OperatorID ORDER BY Name" >
    <SelectParameters>
        <asp:ControlParameter Name="OperatorID" ControlID="rcbOperator" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
    </SelectParameters>
</blac:DBDataSource>
<blac:DBDataSource ID="dsTicketType" runat="server" SelectCommand="SELECT ID, Name FROM dbo.viewTicketType WHERE (@CountryID = -1 OR CountryID_CSV_Delim LIKE '%,' + @CountryID + ',%') ORDER BY Name" >
    <SelectParameters>
        <asp:ControlParameter Name="CountryID" ControlID="rcbCountry" PropertyName="SelectedValue" DbType="String" DefaultValue="-1" />
    </SelectParameters>
</blac:DBDataSource>
<blac:DBDataSource ID="dsGauger" runat="server" SelectCommand="SELECT ID, FullName FROM dbo.viewGauger WHERE DeleteDateUTC IS NULL ORDER BY FullName" />
<blac:DBDataSource ID="dsGaugerTicketType" runat="server" SelectCommand="SELECT ID, Name FROM dbo.tblGaugerTicketType ORDER BY Name" />

