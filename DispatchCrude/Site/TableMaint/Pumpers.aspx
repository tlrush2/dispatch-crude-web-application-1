﻿<%@ Page Title="Locations" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Pumpers.aspx.cs" Inherits="DispatchCrude.Site.TableMaint.Public.Pumpers" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script>
        $("#ctl00_ctl00_EntityCaption").html("Pumpers");
    </script>
    <div class="tabbable TabRepaint">
        <ul class="nav nav-tabs" id="tabmenu" runat="server">
            <!--Tabs will print here (see code behind)-->
        </ul>
        <div class="tab-content">
            <div class="tab-pane in active">

                <div id="DataEntryFormGrid" style="height:100%;">
                    <asp:ValidationSummary ID="validationSummaryMain" runat="server" DisplayMode="BulletList" Enabled="true" EnableClientScript="true" CssClass="NullValidator" />
                    <telerik:RadGrid ID="rgMain" runat="server" CellSpacing="0" GridLines="None" Height="800" CssClass="GridRepaint" EnableLinqExpressions="false"
                                     AutoGenerateColumns="False" ShowGroupPanel="False" AllowFilteringByColumn="true" AllowSorting="True" AllowPaging="True" EnableEmbeddedSkins="true" Skin="Vista" AlternatingItemStyle-BackColor="#dcf2fc"
                                     PageSize='<%# Settings.DefaultPageSize %>'
                                     OnItemDataBound="grid_ItemDataBound">
                        <ClientSettings AllowDragToGroup="True">
                            <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                        </ClientSettings>
                        <GroupingSettings CaseSensitive="false" ShowUnGroupButton="true" />
                        <ExportSettings FileName="Pumpers" ExportOnlyData="true" IgnorePaging="true" HideStructureColumns="true" Excel-FileExtension="xls" Excel-Format="Biff" OpenInNewWindow="true" />
                        <MasterTableView DataKeyNames="ID" CommandItemDisplay="Top" CommandItemSettings-AddNewRecordText="Add New Pumper">
                            <CommandItemSettings ShowExportToExcelButton="true" />
                            <RowIndicatorColumn Visible="True">
                                <HeaderStyle Width="20px" />
                            </RowIndicatorColumn>
                            <ExpandCollapseColumn Visible="True">
                                <HeaderStyle Width="20px" />
                            </ExpandCollapseColumn>
                            <Columns>
                                <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Edit" Text="Edit" UniqueName="EditColumn" ImageUrl="~/images/edit.png">
                                    <HeaderStyle Width="40px" />
                                </telerik:GridButtonColumn>
                                <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Delete" Text="Delete" ConfirmText="Are you sure?" UniqueName="DeleteColumn"
                                                          ImageUrl="~/images/delete.png">
                                    <HeaderStyle Width="40px" />
                                </telerik:GridButtonColumn>
                                <telerik:GridTemplateColumn EditFormColumnIndex="0" DataField="Operator" GroupByExpression="Operator GROUP BY Operator"
                                                            HeaderText="Operator" SortExpression="OperatorID" FilterControlWidth="70%" UniqueName="OperatorID">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="ddlOperator" runat="server" DataSourceID="dsOperator" DataTextField="Operator"
                                                          DataValueField="ID" SelectedValue='<%# Bind("OperatorID") %>' CssClass="btn-xs">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rfvOperator" runat="server" Text="*" ErrorMessage="Operator is required" ControlToValidate="ddlOperator"
                                                                    CssClass="NullValidator" />
                                        <asp:CustomValidator ID="cvOperator_Unique" runat="server"
                                                             ControlToValidate="ddlOperator" OnServerValidate="ServerValidate_Unique"
                                                             Text="*" ErrorMessage="First Name | Last Name are already in assigned to this Operator" CssClass="NullValidator" />
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblOperator" runat="server" Text='<%# Eval("Operator") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="200px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn DataField="FirstName" HeaderText="First Name" Groupable="false"
                                                            SortExpression="FirstName" FilterControlWidth="70%" UniqueName="FirstName"
                                                            EditFormColumnIndex="0">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtFirstName" runat="server" Text='<%# Bind("FirstName") %>' />
                                        <asp:RequiredFieldValidator ID="rfvFirstName" runat="server" ControlToValidate="txtFirstName"
                                                                    Text="*" ErrorMessage="First Name is required" CssClass="NullValidator" />
                                        <asp:CustomValidator ID="cvFirstName_Unique" runat="server"
                                                             ControlToValidate="txtFirstName" OnServerValidate="ServerValidate_Unique"
                                                             Text="*" CssClass="NullValidator" />
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblFirstName" runat="server" Text='<%# Eval("FirstName") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="120px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn DataField="LastName" HeaderText="Last Name" SortExpression="LastName" Groupable="false"
                                                            FilterControlWidth="70%" UniqueName="LastName" EditFormColumnIndex="0">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtLastName" runat="server" Text='<%# Bind("LastName") %>' />
                                        <asp:RequiredFieldValidator ID="rfvLastName" runat="server" ControlToValidate="txtLastName"
                                                                    Text="*" ErrorMessage="Last Name is required" CssClass="NullValidator" />
                                        <asp:CustomValidator ID="cvLastName_Unique" runat="server"
                                                             ControlToValidate="txtLastName" OnServerValidate="ServerValidate_Unique"
                                                             Text="*" CssClass="NullValidator" />
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblLastName" runat="server" Text='<%# Eval("LastName") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="120px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn EditFormColumnIndex="1" DataField="ContactEmail" HeaderText="Email" SortExpression="ContactEmail"
                                                         FilterControlWidth="70%" UniqueName="ContactEmail" MaxLength="70">
                                    <HeaderStyle Width="200px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn EditFormColumnIndex="1" DataField="ContactPhone" HeaderText="Phone" SortExpression="ContactPhone"
                                                         FilterControlWidth="70%" UniqueName="ContactPhone" MaxLength="20">
                                    <HeaderStyle Width="100px" />
                                </telerik:GridBoundColumn>

                                <telerik:GridBoundColumn DataField="CreateDate" UniqueName="CreateDate" SortExpression="CreateDate" DataType="System.DateTime" DataFormatString="{0:M/d/yy}"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Create Date" HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="CreatedByUser" UniqueName="CreatedByUser" SortExpression="CreatedByUser"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Created By" HeaderStyle-Width="90px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="LastChangeDate" UniqueName="LastChangeDate" SortExpression="LastChangeDate" DataType="System.DateTime" DataFormatString="{0:M/d/yy HH:mm}"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Last Update Date" HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="LastChangedByUser" UniqueName="LastChangedByUser" SortExpression="LastChangedByUser"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Last Updated By" HeaderStyle-Width="95px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />

                            </Columns>
                            <EditFormSettings ColumnNumber="2">
                                <EditColumn FilterControlAltText="Filter EditCommandColumn column" ButtonType="ImageButton" CancelImageUrl="~/images/cancel.png"
                                            UpdateImageUrl="~/images/apply.png" InsertImageUrl="~/images/apply.png" />
                            </EditFormSettings>
                            <PagerStyle AlwaysVisible="True" />
                        </MasterTableView>
                        <HeaderStyle Wrap="False" />
                        <PagerStyle AlwaysVisible="True" />
                        <FilterMenu EnableImageSprites="False">
                        </FilterMenu>
                    </telerik:RadGrid>
                </div>
                <blc:RadGridDBCtrl ID="dbcMain" runat="server"
                                   ControlID="rgMain"
                                   UpdateTableName="tblPumper"
                                   SelectCommand="Select ID, Active, OperatorID, Operator, FirstName, LastName, ContactEmail, ContactPhone, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser ID FROM dbo.viewPumper ORDER BY Operator, LastName, FirstName" />
                <blac:DBDataSource ID="dsOperator" runat="server" SelectCommand="SELECT ID, Name AS Operator FROM dbo.tblOperator UNION SELECT NULL, '(Select Operator)' ORDER BY Operator" />
            </div>
        </div>
    </div>
</asp:Content>