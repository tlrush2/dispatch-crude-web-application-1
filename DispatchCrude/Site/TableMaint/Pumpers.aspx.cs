﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//add for sql stuff
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web.Security;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using Telerik.Web.UI;
using AlonsIT;
using DispatchCrude.Extensions;

namespace DispatchCrude.Site.TableMaint.Public
{
    public partial class Pumpers : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            //Print tab navigation html to page
            tabmenu.InnerHtml = NavigationHelper.PrintTabArray(NavigationHelper.TabSet_Locations, "Tab_Pumpers").ToString();

        }

        protected void ServerValidate_Unique(object source, ServerValidateEventArgs args)
        {
            args.IsValid = true; // default value
            if ((source as WebControl).NamingContainer is GridEditableItem)
            {
                GridEditableItem gdi = (source as WebControl).NamingContainer as GridEditableItem;
                object operatorID = App_Code.RadGridHelper.GetColumnDropDownSelectedValue(gdi["OperatorID"], "ddlOperator", "")
                    , first = (gdi["FirstName"].FindControl("txtFirstName") as TextBox).Text
                    , last = (gdi["LastName"].FindControl("txtLastName") as TextBox).Text;
                int id = App_Code.RadGridHelper.GetGridItemID(gdi);
                if (operatorID != null && first.ToString().Length > 0 && last.ToString().Length > 0)
                {
                    using (SSDB db = new SSDB())
                    {
                        using (SqlCommand cmd = db.BuildCommand("SELECT count(1) FROM tblPumper WHERE OperatorID=@OperatorID AND FirstName=@First AND LastName=@Last AND ID <> @ID"))
                        {
                            cmd.Parameters.AddWithValue("@OperatorID", operatorID);
                            cmd.Parameters.AddWithValue("@First", first);
                            cmd.Parameters.AddWithValue("@Last", last);
                            cmd.Parameters.AddWithValue("@ID", id);
                            args.IsValid = DBHelper.ToInt32(cmd.ExecuteScalar()) == 0;
                        }
                    }
                }
            }

        }

        protected void grid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem && !e.Item.IsInEditMode)
            {
                //DCWEB-1292 - "Deactivate" functionality is not available on the pumper page.  This code works with viewPumper
                //to hide the delete button when a pumper has been selected for one or more origins.
                //Future Note:  When this page is converted to MVC it would not be a bad idea to add the deactivate functionality.
                DataRowView data = e.Item.DataItem as DataRowView;
                bool locked = DBHelper.ToBoolean(data["Active"]);

                ((e.Item as GridDataItem)["DeleteColumn"].Controls[0] as ImageButton).Enabled = !locked;
                ((e.Item as GridDataItem)["DeleteColumn"].Controls[0] as ImageButton).Visible = !locked;
            }
        }
    }
}