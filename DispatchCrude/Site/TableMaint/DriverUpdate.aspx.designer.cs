﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace DispatchCrude.Site.TableMaint {
    
    
    public partial class DriverUpdate {

        protected global::System.Web.UI.HtmlControls.HtmlGenericControl tabmenu;
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl speedbuttons;

        /// <summary>
        /// rgMain control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Telerik.Web.UI.RadGrid rgMain;
        
        /// <summary>
        /// dsMain control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DispatchCrude.App_Code.Controls.DBDataSource dsMain;
    }
}
