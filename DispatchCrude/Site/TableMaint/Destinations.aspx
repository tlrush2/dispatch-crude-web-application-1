﻿<%@ Page Title="Locations" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Destinations.aspx.cs" Inherits="DispatchCrude.Site.TableMaint.Destinations" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script>
        $("#ctl00_ctl00_EntityCaption").html("Destinations");
    </script>
    <div class="tabbable TabRepaint">
        <ul class="nav nav-tabs" id="tabmenu" runat="server">
            <!--Tabs will print here (see code behind)-->
        </ul>
        <div class="tab-content">
            <div class="tab-pane in active">

                <div id="speedbuttons" class="well speedButtonContainer" runat="server">
                    <!--Buttons will print here (see code behind)-->
                </div>
                <div id="DataEntryFormGrid" style="height:100%;">
                    <telerik:RadWindowManager ID="radWindowManager" runat="server" EnableShadow="true" />
                    <telerik:RadGrid ID="rgMain" runat="server" CellSpacing="0" GridLines="None" Height="800" CssClass="GridRepaint" EnableLinqExpressions="false"
                                     AutoGenerateColumns="False" ShowGroupPanel="False" AllowFilteringByColumn="true" AllowSorting="True" AllowPaging="True" EnableEmbeddedSkins="true" Skin="Vista" AlternatingItemStyle-BackColor="#dcf2fc"
                                     PageSize='<%# Settings.DefaultPageSize %>'
                                     OnItemCommand="grid_ItemCommand" OnNeedDataSource="grid_NeedDataSource" OnItemDataBound="grid_ItemDataBound">
                        <ClientSettings AllowDragToGroup="True">
                            <Scrolling AllowScroll="True" UseStaticHeaders="True" FrozenColumnsCount="2" />
                        </ClientSettings>
                        <GroupingSettings CaseSensitive="false" ShowUnGroupButton="true" />
                        <ExportSettings FileName="Destinations" ExportOnlyData="true" IgnorePaging="true" HideStructureColumns="true" Excel-FileExtension="xls" Excel-Format="Biff" OpenInNewWindow="true" />
                        <MasterTableView DataKeyNames="ID" CommandItemDisplay="Top" CommandItemSettings-AddNewRecordText="Add New Destination" EditMode="PopUp">
                            <CommandItemTemplate>
                                <div style="padding: 5px;">
                                    <asp:LinkButton Style="vertical-align: middle" runat="server"
                                                    CommandName="InitInsert">
                                        <img style="border:0px" alt="" src="../../images/add.png" />
                                        Add New Destination
                                    </asp:LinkButton>

                                    <asp:LinkButton ID="cmdDeactivateStale" Style="vertical-align: middle" runat="server" CommandName="ImportExcel"
                                                    OnClick="cmdDeactivateStale_Click" Enabled="true" CssClass="pull-right NOAJAX">
                                        <img style="border:0px" alt="" src="../../images/wrench_orange.png" />
                                        Deactivate Stale
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="cmdExportExcel" Style="vertical-align: middle" runat="server"
                                                    CommandName="ExportToExcel" CssClass="pull-right NOAJAX">
                                        <img style="border:0px" alt="" src="../../images/exportToExcel.png" />
                                        Excel Export &nbsp;&nbsp;
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnRefresh" Text="Refresh" CommandName="Rebind" CssClass="pull-right" runat="server">Refresh&nbsp;&nbsp;|&nbsp;&nbsp;</asp:LinkButton>
                                    <asp:Button runat="server" ID="buttonRefresh" CommandName="Rebind" CssClass="rgRefresh pull-right" Text="Refresh" Title="Refresh" />
                                </div>
                            </CommandItemTemplate>
                            <EditFormSettings UserControlName="CtrlDestination.ascx" EditFormType="WebUserControl"
                                              CaptionDataField="Name" CaptionFormatString="Edit Destination: {0}">
                                <PopUpSettings Modal="true" />
                                <EditColumn UniqueName="EditColumn" />
                            </EditFormSettings>
                            <RowIndicatorColumn Visible="True">
                                <HeaderStyle Width="20px" />
                            </RowIndicatorColumn>
                            <Columns>
                                <telerik:GridTemplateColumn UniqueName="ActionColumn" HeaderStyle-Width="100px" AllowFiltering="false" AllowSorting="false">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" ID="btnEdit" CssClass="btn btn-xs btn-default shiny" CommandName="Edit" Text="Edit" ImageUrl="~/images/edit.png" />
                                        <asp:ImageButton runat="server" ID="btnDelete" CssClass="btn btn-xs btn-default shiny" CommandName="Delete" Text="Delete" ConfirmText="Are you sure?" ImageUrl="~/images/delete.png" />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>

                                <telerik:GridCheckBoxColumn DataField="Active" UniqueName="Active" HeaderText="Active?" SortExpression="Active"
                                                            ReadOnly="true" HeaderStyle-Width="80px" FilterControlWidth="60%" />

                                <telerik:GridBoundColumn DataField="DestinationType" HeaderText="Destination Type"
                                                         SortExpression="DestinationTypeID" FilterControlWidth="70%" UniqueName="DestinationTypeID">
                                    <HeaderStyle Width="100px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Name" HeaderText="Name" SortExpression="Name" FilterControlWidth="70%"
                                                         UniqueName="Name" MaxLength="50">
                                    <HeaderStyle Width="200px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Station" HeaderText="Station" SortExpression="Station"
                                                         FilterControlWidth="70%" UniqueName="Station" MaxLength="10">
                                    <HeaderStyle Width="100px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Region" EditFormColumnIndex="0"
                                                         HeaderText="Region" SortExpression="Region" FilterControlWidth="70%" UniqueName="Region">
                                    <HeaderStyle Width="150px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Terminal" EditFormColumnIndex="0"
                                                         HeaderText="Terminal" SortExpression="Terminal" FilterControlWidth="70%" UniqueName="Terminal">
                                    <HeaderStyle Width="150px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Address" HeaderText="Address"
                                                         SortExpression="Address" FilterControlWidth="70%" UniqueName="Address" MaxLength="50">
                                    <HeaderStyle Width="250px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="City" HeaderText="City" SortExpression="City" FilterControlWidth="70%" UniqueName="City" MaxLength="30">
                                    <HeaderStyle Width="100px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="StateAbbrev" HeaderText="State" SortExpression="StateAbbrev" FilterControlWidth="70%"
                                                         UniqueName="StateID">
                                    <HeaderStyle Width="100px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Zip" HeaderText="Zip" SortExpression="Zip" FilterControlWidth="70%" UniqueName="Zip" MaxLength="10">
                                    <HeaderStyle Width="100px" />
                                </telerik:GridBoundColumn>

                                <telerik:GridBoundColumn DataField="Products" HeaderText="Products" SortExpression="Products" UniqueName="Products"
                                                         FilterControlWidth="70%" HeaderStyle-Width="300px" ItemStyle-CssClass="showEllipsis">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Uom" HeaderText="UOM" SortExpression="Uom"
                                                         FilterControlWidth="70%" UniqueName="Uom">
                                    <HeaderStyle Width="100px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Customers" HeaderText="Shippers" SortExpression="Customers" UniqueName="Customers"
                                                         FilterControlWidth="70%" HeaderStyle-Width="250px" ItemStyle-CssClass="showEllipsis">
                                </telerik:GridBoundColumn>

                                <telerik:GridBoundColumn DataField="TimeZone"
                                                         HeaderText="Time Zone" SortExpression="TimeZone" FilterControlWidth="70%" UniqueName="TimeZone">
                                    <HeaderStyle Width="125px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridCheckBoxColumn DataField="UseDST" HeaderText="Use DST?" SortExpression="UseDST" UniqueName="UseDST" FilterControlWidth="50%"
                                                            DefaultInsertValue="True" HeaderStyle-Width="70px" />

                                <telerik:GridTemplateColumn UniqueName="DestLatLon" HeaderText="Lat/Lon" HeaderStyle-Width="180px" FilterControlWidth="75%"
                                                            SortExpression="DestLatLon" ItemStyle-Wrap="true">
                                    <ItemTemplate>
                                        <asp:Hyperlink ID="lblDestLL" runat="server" width="100%"
                                                       Text='<%# string.Format("{0}", Converter.IfGPSNullOrEmpty(Eval("LAT"),Eval("LON"), ", ")) %>'
                                                       NavigateUrl='<%# string.Format("http://maps.google.com/maps?z=12&t=k&q=loc:{0}", Converter.IfGPSNullOrEmpty(Eval("LAT"),Eval("LON"), ",")) %>'
                                                       Target="_blank" />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridNumericColumn DataField="GeoFenceRadiusMeters" HeaderText="GeoFence Meters"
                                                           DataType="System.Int16" SortExpression="GeoFenceRadiusMeters" FilterControlWidth="70%" UniqueName="GeoFenceRadiusMeters"
                                                           HeaderStyle-Width="100px" />

                                <telerik:GridBoundColumn DataField="TicketType" HeaderText="Ticket Type"
                                                         FilterControlWidth="70%" UniqueName="TicketTypeID" SortExpression="TicketType" EditFormColumnIndex="2">
                                    <HeaderStyle Width="100px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="PrivateRoadMiles" HeaderText="PRM"
                                                         FilterControlWidth="70%" UniqueName="PrivateRoadMiles" SortExpression="PrivateRoadMiles" EditFormColumnIndex="2">
                                    <HeaderStyle Width="100px" />
                                </telerik:GridBoundColumn>

                                <telerik:GridBoundColumn DataField="CreateDate" UniqueName="CreateDate" SortExpression="CreateDate" DataType="System.DateTime" DataFormatString="{0:M/d/yy}"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Create Date" HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="CreatedByUser" UniqueName="CreatedByUser" SortExpression="CreatedByUser"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Created By" HeaderStyle-Width="90px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="LastChangeDate" UniqueName="LastChangeDate" SortExpression="LastChangeDate" DataType="System.DateTime" DataFormatString="{0:M/d/yy HH:mm}"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Last Update Date" HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="LastChangedByUser" UniqueName="LastChangedByUser" SortExpression="LastChangedByUser"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Last Updated By" HeaderStyle-Width="95px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="DeleteDate" UniqueName="DeleteDate" SortExpression="DeleteDate" DataType="System.DateTime" DataFormatString="{0:M/d/yy HH:mm}"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Delete Date" HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="DeletedByUser" UniqueName="DeletedByUser" SortExpression="DeletedByUser"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Deleted By" HeaderStyle-Width="90px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />

                            </Columns>
                            <PagerStyle AlwaysVisible="True" />
                        </MasterTableView>
                        <HeaderStyle Wrap="False" />
                        <PagerStyle AlwaysVisible="True" />
                        <FilterMenu EnableImageSprites="False">
                        </FilterMenu>
                    </telerik:RadGrid>
                </div>
            </div>
        </div>
    </div>
    <blc:RadGridDBCtrl ID="dbcMain" runat="server" ControlID="rgMain" UpdateTableName="tblDestination" />
</asp:Content>