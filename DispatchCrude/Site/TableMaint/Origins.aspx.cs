﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.IO;
//add for sql stuff
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using Telerik.Web.UI;
using DispatchCrude.App_Code.Controls;
using DispatchCrude.Core;
using DispatchCrude.Extensions;
using AlonsIT;

namespace DispatchCrude.Site.TableMaint
{
    public partial class Origins : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // it is necessary to "re-hook" up this event on every Page_Load or it won't trap the event when needed
            ConfigureAjax(!Core.Settings.SettingsID.DisableAJAX.AsBool());

            //Print tab navigation html to page
            tabmenu.InnerHtml = NavigationHelper.PrintTabArray(NavigationHelper.TabSet_Locations, "Tab_Origins").ToString();

            //Print button navigation html to page
            speedbuttons.InnerHtml = NavigationHelper.PrintButtonArray(NavigationHelper.ButtonSet_TabOrigins, "Button_Origins").ToString();

            // filter to the specified Origin (if any)
            if (Request.QueryString["ID"] != null)
            {
                using (SSDB ssdb = new SSDB())
                {
                    string originName = ssdb.QuerySingleValue("SELECT Name FROM tblOrigin WHERE ID={0}", (object)Request.QueryString["ID"]).ToString();
                    App_Code.RadGridHelper.SetFilter(rgMain.MasterTableView, "Name", originName, GridKnownFunction.EqualTo);
                }
            }
        }

        private void ConfigureAjax(bool enabled)
        {
            if (enabled)
            {
                App_Code.RadAjaxHelper.AddAjaxSetting(this.Page, rgMain, rgMain);
            }
            RadAjaxManager.GetCurrent(Page).EnableAJAX = enabled;
        }

        private void ExportToExcel()
        {
            string filename = string.Format("Origins_{0:yyyy-MM-dd-HH-mm}.xlsx", DateTime.Now);
            // turn paging off so it loads the entire dataset (not just the current "page" of data)
            rgMain.AllowPaging = false;
            rgMain.Rebind();
            MemoryStream ms = new App_Code.RadGridExcelExporter().ExportSheet(rgMain.MasterTableView);
            Response.ExportExcelStream(ms, filename);
        }

        protected void cmdExport_Click(object source, EventArgs e)
        {
            ExportToExcel();
        }

        protected void cmdDeactivateStale_Click(object source, EventArgs e)
        {
            using (SSDB ssdb = new SSDB())
            {
                using (SqlCommand cmd = ssdb.BuildCommand("spDeactivateStaleOrigins"))
                {
                    cmd.Parameters["@UserName"].Value = UserSupport.UserName;                    
                    cmd.ExecuteNonQuery();
                    int originCount = cmd.Parameters["@affectedOrigins"].Value.ToInt32();
                    int routeCount = cmd.Parameters["@affectedRoutes"].Value.ToInt32();

                    // notify the the user of the deactivations
                    string userMsg = string.Format(@"{0} origin{1} deactivated<br/>{2} route{3} deactivated", originCount, originCount == 1 ? "" : "s", routeCount, routeCount == 1 ? "" : "s");
                    radWindowManager.RadAlert(userMsg, 250, 75, "Action Feedback", null);
                }
            }
            rgMain.Rebind();
        }

        protected void grid_ItemCommand(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == "Rebind")  // This is required for the custom refresh button to work as expected
                rgMain.Rebind();            

            if (e.CommandName == RadGrid.ExportToExcelCommandName)
            {
                ExportToExcel();
                e.Canceled = true;
            }
            else if (e.CommandName == RadGrid.InitInsertCommandName) //"Add new" button clicked
            {
                // cancel the default operation
                e.Canceled = true;

                //Prepare an IDictionary with the predefined values
                Hashtable newValues = new Hashtable();
                newValues["TicketTypeID"] = 1;
                newValues["Active"] = true;
                newValues["UseDST"] = true;
                newValues["UomID"] = Settings.SettingsID.Default_UOM.AsInt(1);
                newValues["GeoFenceRadiusMeters"] = Settings.SettingsID.GeoFenceRadiusMeters_Default.AsDecimal(100);
                newValues["GaugerRequired"] = false;
                //Insert the item and rebind
                e.Item.OwnerTableView.InsertItem(newValues);
            }
            // the popup editor has already handled the "update" so just terminate editing and rebind
            else if (e.CommandName == RadGrid.UpdateCommandName || e.CommandName == RadGrid.PerformInsertCommandName || e.CommandName == RadGrid.CancelCommandName)
            {
                e.Item.Edit = false;
                (source as RadGrid).Rebind();
            }// handle the delete logic next
            else if (e.CommandName == RadGrid.DeleteCommandName)
            {
                //Deactivate all active routes that are associated with the deactivated origin
                using (SSDB ssdb = new SSDB())
                {
                    using (SqlCommand cmd = ssdb.BuildCommand(@"UPDATE tblRoute 
                                                              SET DeleteDateUTC = GETUTCDATE()
                                                                , DeletedByUser = @UserName 
                                                              WHERE OriginID = @OriginID
                                                                  AND DeleteDateUTC IS NULL
                                                              "))
                    {
                        GridDataItem clickedRow = e.Item as GridDataItem;
                        cmd.Parameters.AddWithValue("@OriginID", clickedRow.GetDataKeyValue("ID"));
                        cmd.Parameters.AddWithValue("@UserName", UserSupport.UserName);
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            else if (e.CommandName == RadGridDBCtrl.CMD_UNDELETE)
            {
                // Reactivate routes that are associated with the reactivated origin
                // but only if the route's destination is not dectivated
                using (SSDB ssdb = new SSDB())
                {                    
                    using (SqlCommand cmd = ssdb.BuildCommand(@"UPDATE tblRoute 
                                                              SET DeleteDateUTC = NULL
                                                                , DeletedByUser = NULL 
                                                              WHERE OriginID = @OriginID
                                                                  AND DeleteDateUTC IS NOT NULL
                                                                  AND DestinationID IN (
                                                                                        SELECT ID
                                                                                        FROM tblDestination
                                                                                        WHERE DeleteDateUTC IS NULL
                                                                                       )
                                                              "))
                    {
                        GridDataItem clickedRow = e.Item as GridDataItem;
                        cmd.Parameters.AddWithValue("@OriginID", clickedRow.GetDataKeyValue("ID"));
                        cmd.ExecuteNonQuery();
                    }
                }
            }
        }

        protected void grid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                HyperLink hl = (HyperLink)App_Code.RadGridHelper.GetControlByType((e.Item as GridDataItem)["TankCount"], typeof(HyperLink));
                if (hl != null)
                {
                    bool hasTanks = e.Item.DataItem is DataRowView && DBHelper.ToBoolean((e.Item.DataItem as DataRowView)["HasTanks"]);
                    hl.Enabled = hasTanks;
                    if (!hasTanks)
                        hl.Text = "";
                }
            }
        }

    }
}