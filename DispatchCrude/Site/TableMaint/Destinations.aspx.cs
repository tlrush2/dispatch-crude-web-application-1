﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
//add for sql stuff
using System.Data;
using System.Data.SqlClient;
using Telerik.Web.UI;
using System.Collections;
using DispatchCrude.App_Code.Controls;
using DispatchCrude.Core;
using AlonsIT;
using DispatchCrude.Extensions;

namespace DispatchCrude.Site.TableMaint
{
    public partial class Destinations : System.Web.UI.Page
    {

        private void ConfigureAjax(bool enabled)
        {
            if (enabled)
            {
                App_Code.RadAjaxHelper.AddAjaxSetting(this.Page, rgMain, rgMain);
            }
            RadAjaxManager.GetCurrent(Page).EnableAJAX = enabled;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ConfigureAjax(!Settings.SettingsID.DisableAJAX.AsBool());

            //Print tab navigation html to page
            tabmenu.InnerHtml = NavigationHelper.PrintTabArray(NavigationHelper.TabSet_Locations, "Tab_Destinations").ToString();

            //Print button navigation html to page
            speedbuttons.InnerHtml = NavigationHelper.PrintButtonArray(NavigationHelper.ButtonSet_TabDestinations, "Button_Destinations").ToString();

            // filter to the specified Destination (if any)
            if (Request.QueryString["ID"] != null)
            {
                using (SSDB ssdb = new SSDB())
                {
                    string destName = ssdb.QuerySingleValue("SELECT Name FROM tblDestination WHERE ID={0}", (object)Request.QueryString["ID"]).ToString();
                    App_Code.RadGridHelper.SetFilter(rgMain.MasterTableView, "Name", destName, GridKnownFunction.EqualTo);
                }
            }
        }
        protected void cmdExport_Click(object source, EventArgs e)
        {
            rgMain.ExportToExcel();
        }
        protected void cmdDeactivateStale_Click(object source, EventArgs e)
        {
            using (SSDB db = new SSDB())
            {
                using (SqlCommand cmd = db.BuildCommand("spDeactivateStaleDestinations"))
                {
                    cmd.Parameters["@UserName"].Value = UserSupport.UserName;
                    cmd.Parameters["@UserName"].Value = UserSupport.UserName;
                    cmd.ExecuteNonQuery();
                    int destinationCount = cmd.Parameters["@affectedDestinations"].Value.ToInt32();
                    int routeCount = cmd.Parameters["@affectedRoutes"].Value.ToInt32();

                    // notify the the user of the deactivations
                    string userMsg = string.Format(@"{0} destination{1} deactivated<br/>{2} route{3} deactivated", destinationCount, destinationCount == 1 ? "" : "s", routeCount, routeCount == 1 ? "" : "s");
                    radWindowManager.RadAlert(userMsg, 300, 75, "Action Feedback", null);
                }
            }
            rgMain.Rebind();
        }
        protected void grid_ItemCommand(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == "Rebind")  // This is required for the custom refresh button to work as expected
                rgMain.Rebind();

            if (e.CommandName == RadGrid.InitInsertCommandName) //"Add new" button clicked
            {
                // cancel the default operation
                e.Canceled = true;

                Hashtable newValues = new Hashtable();
                newValues["TicketTypeID"] = 1; // default to "BASIC"
                newValues["UomID"] = Settings.SettingsID.Default_UOM.AsInt(1);
                newValues["GeoFenceRadiusMeters"] = Settings.SettingsID.GeoFenceRadiusMeters_Default.AsDecimal(100);
                newValues["UseDST"] = true;
                //Insert the item and rebind
                e.Item.OwnerTableView.InsertItem(newValues);
            }
            // the popup editor has already handled the "update" so just terminate editing and rebind
            else if (e.CommandName == RadGrid.UpdateCommandName || e.CommandName == RadGrid.PerformInsertCommandName || e.CommandName == RadGrid.CancelCommandName)
            {
                e.Item.Edit = false;
                (source as RadGrid).Rebind();
            }
            // handle the delete logic next
            else if (e.CommandName == RadGrid.DeleteCommandName)
            {
                //Deactivate all active routes that are associated with the deactivated destination
                using (SSDB ssdb = new SSDB())
                {
                    using (SqlCommand cmd = ssdb.BuildCommand(@"UPDATE tblRoute 
                                                              SET DeleteDateUTC = GETUTCDATE()
                                                                , DeletedByUser = @UserName 
                                                              WHERE DestinationID = @DestinationID
                                                                  AND DeleteDateUTC IS NULL
                                                              "))
                    {
                        GridDataItem clickedRow = e.Item as GridDataItem;
                        cmd.Parameters.AddWithValue("@DestinationID", clickedRow.GetDataKeyValue("ID"));
                        cmd.Parameters.AddWithValue("@UserName", UserSupport.UserName);
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            else if (e.CommandName == RadGridDBCtrl.CMD_UNDELETE)
            {
                // Reactivate routes that are associated with the reactivated destination
                // but only if the route's origin is not dectivated
                using (SSDB ssdb = new SSDB())
                {
                    using (SqlCommand cmd = ssdb.BuildCommand(@"UPDATE tblRoute 
                                                              SET DeleteDateUTC = NULL
                                                                , DeletedByUser = NULL 
                                                              WHERE DestinationID = @DestinationID
                                                                  AND DeleteDateUTC IS NOT NULL
                                                                  AND OriginID IN (
                                                                                    SELECT ID
                                                                                    FROM tblOrigin
                                                                                    WHERE DeleteDateUTC IS NULL
                                                                                  )
                                                              "))
                    {
                        GridDataItem clickedRow = e.Item as GridDataItem;
                        cmd.Parameters.AddWithValue("@DestinationID", clickedRow.GetDataKeyValue("ID"));
                        cmd.ExecuteNonQuery();
                    }
                }
            }
        }

        protected void grid_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            using (SSDB db = new SSDB())
            {
                DataTable data = Core.DateHelper.AddLocalRowStateDateFields(db.GetPopulatedDataTable("SELECT *, Products = dbo.fnDestinationProductsCSV(ID), ProductID_CSV = dbo.fnDestinationProductIDCSV(ID), Customers = dbo.fnDestinationCustomersCSV(ID), CustomerID_CSV = dbo.fnDestinationCustomerIDCSV(ID) FROM viewDestination ORDER BY Name"), HttpContext.Current);
                rgMain.DataSource = data;
            }
        }

        protected void grid_ItemDataBound(object sender, GridItemEventArgs e)
        {
        }

    }
}