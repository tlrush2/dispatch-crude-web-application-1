﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Collections.Specialized;
using DispatchCrude.Core;
using AlonsIT;
using DispatchCrude.Extensions;

namespace DispatchCrude.Site.TableMaint
{
    public partial class Trailers : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ConfigureAjax(!Core.Settings.SettingsID.DisableAJAX.AsBool());

            //Print tab navigation html to page
            tabmenu.InnerHtml = NavigationHelper.PrintTabArray(NavigationHelper.TabSet_Resources, "Tab_Trailers").ToString();

            //Print button navigation html to page
            speedbuttons.InnerHtml = NavigationHelper.PrintButtonArray(NavigationHelper.ButtonSet_TabTrailers, "Button_Trailers").ToString();

            // filter to the specified Trailer (if any)
            if (Request.QueryString["ID"] != null)
            {
                using (SSDB ssdb = new SSDB())
                {
                    string trailerNum = ssdb.QuerySingleValue("SELECT IDNumber FROM tblTrailer WHERE ID={0}", (object)Request.QueryString["ID"]).ToString();
                    App_Code.RadGridHelper.SetFilter(rgMain.MasterTableView, "IDNumber", trailerNum, GridKnownFunction.EqualTo);
                }
            }
        }

        private void ConfigureAjax(bool enabled)
        {
            App_Code.RadAjaxHelper.AddAjaxSetting(this.Page, rgMain, rgMain);
            App_Code.RadAjaxHelper.AddAjaxSetting(this.Page, rgMain, validationSummaryMain);
            RadAjaxManager.GetCurrent(this.Page).EnableAJAX = enabled;
        }

        protected void grid_ItemCommand(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == "Rebind")  // This is required for the custom refresh button to work as expected
                rgMain.Rebind();

            if (e.CommandName == RadGrid.InitInsertCommandName) //"Add new" button clicked
            {
                // cancel the default operation
                e.Canceled = true;

                ListDictionary newValues = new ListDictionary();
                newValues["CarrierID"] = 0; // default to "(Select a Carrier)"
                newValues["UomID"] = Settings.SettingsID.Default_UOM.AsInt(1);
                //Insert the item and rebind
                e.Item.OwnerTableView.InsertItem(newValues);
            }
        }

        protected void grid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item.IsInEditMode && e.Item is GridEditFormInsertItem)
            {
                using (SSDB db = new SSDB())
                {
                    ((e.Item as GridEditFormInsertItem).FindControl("txtIdNumber") as TextBox).Text = db.QuerySingleValue("SELECT max(isnull(CASE WHEN ISNUMERIC(IdNumber) = 1 THEN IdNumber ELSE 0 END, 1000)) + 1 FROM tblTrailer").ToString();
                }
            }
        }

        protected void uvIDNumber_DataBinding(object sender, EventArgs e)
        {
            // ensure this event is "wired" at databind
            (sender as App_Code.Controls.UniqueValidator).OnGetOtherCellValue += uvIDNumber_GetOtherFieldValue;
        }

        protected object uvIDNumber_GetOtherFieldValue(GridItem item, string uniqueName)
        {
            object ret = null;
            if (uniqueName == "CarrierID" && item is GridEditableItem)
                ret = (App_Code.RadGridHelper.GetColumnDropDown((item as GridEditableItem)[uniqueName], "ddlCarrier") as DropDownList).SelectedValue;
            return ret;
        }

        /*
        protected void uvQRCode_DataBinding(object sender, EventArgs e)
        {
            // ensure this event is "wired" at databind
            (sender as App_Code.Controls.UniqueValidator).OnGetOtherCellValue += uvQRCode_GetOtherFieldValue;
        }

        protected object uvQRCode_GetOtherFieldValue(GridItem item, string uniqueName)
        {
            object ret = null;
            if (uniqueName == "CarrierID" && item is GridEditableItem)
                ret = (App_Code.RadGridHelper.GetColumnDropDown((item as GridEditableItem)[uniqueName], "ddlCarrier") as DropDownList).SelectedValue;
            return ret;
        }
        */
        private List<int> SelectedIDs()
        {
            // Fetch all records, not just current page
            rgMain.MasterTableView.AllowPaging = false;
            rgMain.MasterTableView.Rebind();

            List<int> ret = new List<int>();
            foreach (GridItem item in rgMain.MasterTableView.Items)
            {
                ret.Add(Core.Converter.ToInt32((item as GridDataItem).GetDataKeyValue("ID")));
            }
            return ret;
        }

        protected void cmdExportQR_Click(object source, EventArgs e)
        {
            // Load IDs into session variable and redirect
            Session[DispatchCrude.Controllers.TrailersController.SESSION_NAME_TRAILERIDLIST] = SelectedIDs();
            Response.Redirect("~/Trailers/QRList");
        }
    }
}