﻿<%@ Page Title="Resources" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DriverUpdate.aspx.cs" Inherits="DispatchCrude.Site.TableMaint.DriverUpdate" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<asp:Content ID="mainContent" ContentPlaceHolderID="MainContent" runat="server">
    <script>
        $("#ctl00_ctl00_EntityCaption").html("Driver App History");
    </script>
    <div class="tabbable TabRepaint">
        <ul class="nav nav-tabs" id="tabmenu" runat="server">
            <!--Tabs will print here (see code behind)-->
        </ul>
        <div class="tab-content">
            <div class="tab-pane in active">

                <div id="speedbuttons" class="well speedButtonContainer" runat="server">
                    <!--Buttons will print here (see code behind)-->
                </div>

                <div id="DataEntryFormGrid" style="height:100%;">
                    <telerik:RadGrid ID="rgMain" runat="server" CellSpacing="0" GridLines="None" Height="800" CssClass="GridRepaint" AutoGenerateColumns="False"
                                     ShowGroupPanel="True" AllowFilteringByColumn="true" AllowSorting="True" AllowPaging="True" EnableLinqExpressions="false" EnableEmbeddedSkins="true" Skin="Vista" AlternatingItemStyle-BackColor="#dcf2fc"
                                     PageSize='<%# Settings.DefaultPageSize %>'
                                     DataSourceID="dsMain">
                        <ClientSettings AllowDragToGroup="True" Resizing-AllowResizeToFit="true">
                            <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                        </ClientSettings>
                        <GroupingSettings CaseSensitive="false" ShowUnGroupButton="true" />
                        <ExportSettings FileName="Drivers" ExportOnlyData="true" IgnorePaging="true" HideStructureColumns="true" Excel-FileExtension="xls" Excel-Format="Biff" OpenInNewWindow="true" />
                        <MasterTableView CommandItemDisplay="Top">
                            <CommandItemSettings ShowAddNewRecordButton="false" ShowExportToExcelButton="true" />
                            <RowIndicatorColumn Visible="True">
                                <HeaderStyle Width="20px" />
                            </RowIndicatorColumn>
                            <ExpandCollapseColumn Visible="True">
                                <HeaderStyle Width="20px" />
                            </ExpandCollapseColumn>
                            <SortExpressions>
                                <telerik:GridSortExpression FieldName="UpdateLastUTC" SortOrder="Descending" />
                            </SortExpressions>
                            <GroupByExpressions>
                                <telerik:GridGroupByExpression>
                                    <SelectFields>
                                        <telerik:GridGroupByField FieldAlias="FullName" FieldName="FullName" FormatString="" HeaderText="Driver" />
                                    </SelectFields>
                                    <GroupByFields>
                                        <telerik:GridGroupByField FieldAlias="FullName" FieldName="FullName" SortOrder="Ascending" />
                                    </GroupByFields>
                                </telerik:GridGroupByExpression>
                            </GroupByExpressions>
                            <Columns>
                                <telerik:GridBoundColumn DataField="Name" UniqueName="Name" HeaderText="Carrier" SortExpression="Name"
                                                         ReadOnly="true" HeaderStyle-Width="80px" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="MobileAppVersion" UniqueName="MobileAppVersion" HeaderText="Mobile App Version" SortExpression="MobileAppVersion"
                                                         ReadOnly="true" HeaderStyle-Width="80px" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="TabletID" UniqueName="TabletID" HeaderText="Tablet ID" SortExpression="TabletID"
                                                         ReadOnly="true" HeaderStyle-Width="80px" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="FullName" UniqueName="FullName" HeaderText="Driver" SortExpression="FullName"
                                                         ReadOnly="true" HeaderStyle-Width="125px" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="UpdateLastUTC" UniqueName="UpdateLastUTC" HeaderText="Last Sync Date (UTC)" SortExpression="UpdateLastUTC"
                                                         ReadOnly="true" HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%"
                                                         DataType="System.DateTime" DataFormatString="{0:M/d/yy HH:mm}" ForceExtractValue="Always" />
                            </Columns>
                            <PagerStyle AlwaysVisible="True" />
                        </MasterTableView>
                        <HeaderStyle Wrap="False" />
                        <PagerStyle AlwaysVisible="True" />
                        <FilterMenu EnableImageSprites="False">
                        </FilterMenu>
                    </telerik:RadGrid>
                </div>
                <blac:DBDataSource ID="dsMain"
                                   runat="server"
                                   SelectCommand="SELECT D.FirstName + ' ' + D.LastName AS FullName ,DU.UpdateLastUTC, DU.MobileAppVersion, DU.TabletID, C.Name FROM tblDriver D INNER JOIN tblDriverUpdate DU ON D.ID = DU.DriverID INNER JOIN tblCarrier C ON C.ID = D.CarrierID">
                </blac:DBDataSource>
            </div>
        </div>
    </div>
</asp:Content>