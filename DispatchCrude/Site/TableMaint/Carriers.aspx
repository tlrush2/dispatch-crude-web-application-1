﻿<%@ Page Title="Resources" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Carriers.aspx.cs" Inherits="DispatchCrude.Site.TableMaint.Carriers" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
        <script type="text/javascript">
            function validateItemSpecified(sender, args) {
                debugger;
                //invalid if first character is a "("
                args.IsValid = !args.Value.startsWith("(");
            };
        </script>
    </telerik:RadScriptBlock>

    <script>
        $("#ctl00_ctl00_EntityCaption").html("Carriers");
    </script>
    <div class="tabbable TabRepaint">
        <ul class="nav nav-tabs" id="tabmenu" runat="server">
            <!--Tabs will print here (see code behind)-->
        </ul>
        <div class="tab-content">
            <div class="tab-pane in active">

                <div id="speedbuttons" class="well speedButtonContainer" runat="server">
                    <!--Buttons will print here (see code behind)-->
                </div>

                <div id="DataEntryFormGrid" style="height:100%;">
                    <asp:ValidationSummary ID="validationSummaryMain" runat="server" DisplayMode="BulletList" Enabled="true" EnableClientScript="true" CssClass="NullValidator" />
                    <telerik:RadGrid ID="rgMain" runat="server" CellSpacing="0" GridLines="None" Height="800" CssClass="GridRepaint" EnableLinqExpressions="false"
                                     AutoGenerateColumns="False" ShowGroupPanel="False" AllowFilteringByColumn="true" AllowSorting="True" AllowPaging="True" EnableEmbeddedSkins="true" Skin="Vista" AlternatingItemStyle-BackColor="#dcf2fc"
                                     PageSize='<%# Settings.DefaultPageSize %>'
                                     OnItemCommand="grid_ItemCommand">
                        <ClientSettings AllowDragToGroup="True">
                            <Scrolling AllowScroll="True" UseStaticHeaders="True" FrozenColumnsCount="2" />
                        </ClientSettings>
                        <GroupingSettings CaseSensitive="false" ShowUnGroupButton="true" />
                        <ExportSettings FileName="Carriers" ExportOnlyData="true" IgnorePaging="true" HideStructureColumns="true" Excel-FileExtension="xls" Excel-Format="Biff" OpenInNewWindow="true" />
                        <MasterTableView DataKeyNames="ID" CommandItemDisplay="Top" CommandItemSettings-AddNewRecordText="Add New Carrier">
                            <CommandItemSettings ShowExportToExcelButton="true" />
                            <RowIndicatorColumn Visible="True">
                                <HeaderStyle Width="20px" />
                            </RowIndicatorColumn>
                            <ExpandCollapseColumn Visible="True">
                                <HeaderStyle Width="20px" />
                            </ExpandCollapseColumn>
                            <Columns>
                                <telerik:GridTemplateColumn UniqueName="ActionColumn" HeaderStyle-Width="100px" AllowFiltering="false" AllowSorting="false">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" ID="btnEdit" CssClass="btn btn-xs btn-default shiny" CommandName="Edit" Text="Edit" ImageUrl="~/images/edit.png" />
                                        <asp:ImageButton runat="server" ID="btnDelete" CssClass="btn btn-xs btn-default shiny" CommandName="Delete" Text="Delete" ConfirmText="Are you sure?" ImageUrl="~/images/delete.png" />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>

                                <telerik:GridCheckBoxColumn DataField="Active" UniqueName="Active" HeaderText="Active?" SortExpression="Active"
                                                            ReadOnly="true" HeaderStyle-Width="80px" FilterControlWidth="60%" />

                                <telerik:GridTemplateColumn DataField="IDNumber" HeaderText="Carrier #" SortExpression="IDNumber" FilterControlWidth="70%" UniqueName="IDNumber"
                                                            GroupByExpression="IDNumber GROUP BY IDNumber">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtIDNumber" runat="server" Text='<%# Bind("IDNumber") %>' />
                                        <asp:RequiredFieldValidator ID="rfvIDNumber" runat="server" Text="*" ErrorMessage="Carrier # is required" ControlToValidate="txtIDNumber"
                                                                    CssClass="NullValidator" />
                                        <blac:UniqueValidator ID="uvIDNumber" runat="server" Text="*" ErrorMessage="Carrier # is not unique"
                                                              ControlToValidate="txtIDNumber" TableName="tblCarrier" DataField="IDNumber" CssClass="NullValidator" />
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblIDNumber" runat="server" Text='<%# Eval("IDNumber") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="110px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn DataField="Name" HeaderText="Name" SortExpression="Name" FilterControlWidth="70%" UniqueName="Name"
                                                            GroupByExpression="Name GROUP BY Name">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtName" runat="server" Text='<%# Bind("Name") %>' />
                                        <asp:RequiredFieldValidator ID="rfvName" runat="server" Text="*" ErrorMessage="Name is required" ControlToValidate="txtName"
                                                                    CssClass="NullValidator" />
                                        <blac:UniqueValidator ID="uvName" runat="server" Text="*" ErrorMessage="Name is not unique"
                                                              ControlToValidate="txtName" TableName="tblCarrier" DataField="Name" CssClass="NullValidator" />
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblName" runat="server" Text='<%# Eval("Name") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="300px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridDropDownColumn EditFormColumnIndex="0" DataField="CarrierTypeID" HeaderText="Carrier Type" SortExpression="CarrierType"
                                                            FilterControlWidth="50%" UniqueName="CarrierType"
                                                            DataSourceID="dsCarrierType" ListTextField="Name" ListValueField="ID">
                                    <ColumnValidationSettings EnableRequiredFieldValidation="true">
                                        <RequiredFieldValidator InitialValue="(Select Type)" CssClass="NullValidator" Text="!" ErrorMessage="Carrier Type is required" />
                                    </ColumnValidationSettings>
                                    <HeaderStyle Width="120px" />
                                    <ItemStyle Width="173px" />
                                </telerik:GridDropDownColumn>
                                <telerik:GridBoundColumn EditFormColumnIndex="1" DataField="Address" HeaderText="Address" SortExpression="Address" FilterControlWidth="70%" UniqueName="Address">
                                    <HeaderStyle Width="175px" />
                                    <ItemStyle Wrap="False" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn EditFormColumnIndex="1" DataField="City" HeaderText="City" SortExpression="City" FilterControlWidth="70%" UniqueName="City">
                                    <HeaderStyle Width="125px" />
                                    <ItemStyle Wrap="False" />
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn EditFormColumnIndex="1" DataField="StateAbbrev" HeaderText="State" SortExpression="State"
                                                            FilterControlWidth="70%" UniqueName="StateID">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="ddlState" runat="server" DataSourceID="dsState" DataTextField="FullName" DataValueField="ID"
                                                          SelectedValue='<%# Bind("StateID") %>' CssClass="btn-xs">
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblStateAbbrev" runat="server" Text='<%# Eval("StateAbbrev") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="100px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn EditFormColumnIndex="1" DataField="Zip" HeaderText="Zip" SortExpression="Zip" FilterControlWidth="50%" UniqueName="Zip">
                                    <HeaderStyle Width="75px" />
                                    <ItemStyle Wrap="False" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn EditFormColumnIndex="2" DataField="ContactName" HeaderText="Contact Name" SortExpression="ContactName"
                                                         FilterControlWidth="70%" UniqueName="ContactName">
                                    <HeaderStyle Width="125px" />
                                    <ItemStyle Wrap="False" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn EditFormColumnIndex="2" DataField="ContactEmail"
                                                         HeaderText="Contact Email" SortExpression="ContactEmail" FilterControlWidth="70%" UniqueName="ContactEmail">
                                    <HeaderStyle Width="230px" />
                                    <ItemStyle Wrap="False" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn EditFormColumnIndex="2" DataField="ContactPhone"
                                                         HeaderText="Contact Phone" SortExpression="ContactPhone" FilterControlWidth="70%" UniqueName="ContactPhone">
                                    <HeaderStyle Width="100px" />
                                    <ItemStyle Wrap="False" />
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn DataField="Authority" EditFormColumnIndex="1" GroupByExpression="Authority GROUP BY Authority"
                                                            HeaderText="Authority" SortExpression="Authority" FilterControlWidth="70%" UniqueName="Authority">
                                    <EditItemTemplate>
                                        <telerik:RadComboBox ID="rcbAuthority" runat="server" AllowCustomText="true" Filter="StartsWith" ShowDropDownOnTextboxClick="true"
                                                             DataSourceID="dsAuthority" DataTextField="Authority" DataValueField="Authority" Text='<%# Bind("Authority") %>' MaxLength="30"
                                                             Width="173px" />
                                        <asp:RequiredFieldValidator ID="rfvAuthority" runat="server" ControlToValidate="rcbAuthority" InitialValue=""
                                                                    ErrorMessage="Authority must be specified" Text="*" CssClass="NullValidator" />
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblAuthority" runat="server" Text='<%# Eval("Authority") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="180px" />
                                </telerik:GridTemplateColumn>

                                <%--<telerik:GridTemplateColumn EditFormColumnIndex="1" DataField="Terminal" HeaderText="Terminal" SortExpression="Terminal"
                                                            FilterControlWidth="70%" UniqueName="TerminalID">
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="ddlTerminal" runat="server" DataSourceID="dsTerminal" DataTextField="Name" DataValueField="ID"
                                                          SelectedValue='<%# Bind("TerminalID") %>' CssClass="btn-xs">
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblTerminal" runat="server" Text='<%# Eval("Terminal") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="180px" />
                                </telerik:GridTemplateColumn>--%>
                                
                                <telerik:GridBoundColumn EditFormColumnIndex="0" DataField="MotorCarrierNumber"
                                                         HeaderText="Motor Carrier #" SortExpression="MotorCarrierNumber" FilterControlWidth="70%" UniqueName="MotorCarrierNumber">
                                    <HeaderStyle Width="100px" />
                                    <ItemStyle Wrap="False" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn EditFormColumnIndex="0" DataField="DOTNumber"
                                                         HeaderText="DOT #" SortExpression="DOTNumber" FilterControlWidth="70%" UniqueName="DOTNumber">
                                    <HeaderStyle Width="100px" />
                                    <ItemStyle Wrap="False" />
                                </telerik:GridBoundColumn>

                                <telerik:GridBoundColumn EditFormColumnIndex="3" DataField="FEINNumber"
                                                         HeaderText="FEIN #" SortExpression="FEINNumber" FilterControlWidth="70%" UniqueName="FEINNumber">
                                    <HeaderStyle Width="100px" />
                                    <ItemStyle Wrap="False" />
                                </telerik:GridBoundColumn>

                                <telerik:GridTemplateColumn DataField="Notes" EditFormColumnIndex="4"
                                                            HeaderText="Notes" SortExpression="Notes" FilterControlWidth="70%" UniqueName="Notes" Groupable="false">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtNotes" runat="server" Text='<%# Bind("Notes") %>' TextMode="MultiLine" Height="85px" Width="300px" />
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblNotes" runat="server" Text='<%# Eval("Notes") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="400px" />
                                </telerik:GridTemplateColumn>

                                <telerik:GridBoundColumn DataField="CreateDate" UniqueName="CreateDate" SortExpression="CreateDate" DataType="System.DateTime" DataFormatString="{0:M/d/yy}"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Create Date" HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="CreatedByUser" UniqueName="CreatedByUser" SortExpression="CreatedByUser"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Created By" HeaderStyle-Width="90px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="LastChangeDate" UniqueName="LastChangeDate" SortExpression="LastChangeDate" DataType="System.DateTime" DataFormatString="{0:M/d/yy HH:mm}"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Last Update Date" HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="LastChangedByUser" UniqueName="LastChangedByUser" SortExpression="LastChangedByUser"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Last Updated By" HeaderStyle-Width="95px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="DeleteDate" UniqueName="DeleteDate" SortExpression="DeleteDate" DataType="System.DateTime" DataFormatString="{0:M/d/yy HH:mm}"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Delete Date" HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="DeletedByUser" UniqueName="DeletedByUser" SortExpression="DeletedByUser"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Deleted By" HeaderStyle-Width="90px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />

                                <telerik:GridBoundColumn DataField="ID" DataType="System.Int32"
                                                         HeaderText="ID" ReadOnly="True" SortExpression="ID" UniqueName="ID" Visible="False" ForceExtractValue="Always">
                                    <ItemStyle Wrap="False" />
                                </telerik:GridBoundColumn>
                            </Columns>
                            <EditFormSettings ColumnNumber="5">
                                <EditColumn ButtonType="ImageButton" CancelImageUrl="~/images/cancel.png"
                                            UpdateImageUrl="~/images/apply.png" InsertImageUrl="~/images/apply.png" />
                            </EditFormSettings>
                            <PagerStyle AlwaysVisible="True" />
                        </MasterTableView>
                        <HeaderStyle Wrap="False" />
                        <PagerStyle AlwaysVisible="True" />
                        <FilterMenu EnableImageSprites="False">
                        </FilterMenu>
                    </telerik:RadGrid>
                </div>
                <blc:RadGridDBCtrl ID="dbcMain" runat="server"
                                   ControlID="rgMain"
                                   SelectCommand="SELECT IDNumber, Name, CarrierTypeID, Address, City, StateID, StateAbbrev, Zip, ContactName, ContactEmail, ContactPhone, Authority, TerminalID, Terminal, MotorCarrierNumber, DOTNumber, FEINNumber, Notes, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser, ID FROM viewCarrier ORDER BY Name"
                                   UpdateTableName="tblCarrier" />
                <blac:DBDataSource ID="dsState" runat="server"
                                   SelectCommand="SELECT ID, FullName, Abbreviation FROM dbo.tblState WHERE ID > 0 UNION SELECT NULL, '(Select State)', NULL ORDER BY FullName" />
                <blac:DBDataSource ID="dsCarrierType" runat="server"
                                   SelectCommand="SELECT ID, Name FROM dbo.tblCarrierType UNION SELECT NULL, '(Select Type)' ORDER BY Name" />
                <blac:DBDataSource ID="dsAuthority" runat="server" SelectCommand="SELECT DISTINCT Authority FROM tblCarrier WHERE Authority IS NOT NULL ORDER BY Authority" />
                <blac:DBDataSource ID="dsTerminal" runat="server" SelectCommand="SELECT ID, Name FROM tblTerminal WHERE DeleteDateUTC IS NULL UNION SELECT NULL, '(All Terminals)' ORDER BY Name" />
            </div>
        </div>
    </div>
</asp:Content>