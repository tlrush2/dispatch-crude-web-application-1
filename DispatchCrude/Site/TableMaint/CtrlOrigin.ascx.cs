﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using DispatchCrude.App_Code;
using AlonsIT;
using System.Collections;
using DispatchCrude.Core;

namespace DispatchCrude.Site.TableMaint
{
    public partial class CtrlOrigin : System.Web.UI.UserControl
    {
        private object _dataItem = null;

        protected void Page_Init(object sender, System.EventArgs e)
        {
        }

        private void ConfigureAjax(bool enabled)
        {
            RadAjaxHelper.AddAjaxSetting(this.Page, rcbCountry, rcbTicketType);
            RadAjaxHelper.AddAjaxSetting(this.Page, rcbCountry, rcbState);
            RadAjaxHelper.AddAjaxSetting(this.Page, rcbOperator, rcbPumper);
            RadAjaxHelper.AddAjaxSetting(this.Page, chkGaugerRequired, rcbGaugerTicketType);
            RadAjaxHelper.AddAjaxSetting(this.Page, chkGaugerRequired, cvGaugerTicketType);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            ConfigureAjax(!Settings.SettingsID.DisableAJAX.AsBool());

            if (hfID.Value == "")
            {
                hfID.Value = "0";

                // populate the Origin fields
                if (DataItem != null && DataItem is DataRowView)
                {
                    DataRowView dv = DataItem as DataRowView;

                    dsShippers.SelectParameters["OriginID"].DefaultValue = dsProduct.SelectParameters["OriginID"].DefaultValue = hfID.Value = dv["ID"].ToString();
                    RadComboBoxHelper.Rebind(rcbOriginType, dv["OriginTypeID"]);
                    RadComboBoxHelper.Rebind(rcbCountry, dv["CountryID"]);
                    RadComboBoxHelper.Rebind(rcbTicketType, dv["TicketTypeID"], true);
                    RadComboBoxHelper.Rebind(rcbOperator, dv["OperatorID"], true);
                    RadComboBoxHelper.Rebind(rcbPumper, dv["PumperID"], true);
                    RadComboBoxHelper.Rebind(rcbState, dv["StateID"]);
                    RadComboBoxHelper.Rebind(rcbRegion, dv["RegionID"]);
                    RadComboBoxHelper.Rebind(rcbTermial, dv["TerminalID"]);
                }
                else if (DataItem != null && DataItem is GridInsertionObject)
                {
                    GridInsertionObject dio = DataItem as GridInsertionObject;

                    dsProduct.SelectParameters["OriginID"].DefaultValue = hfID.Value = DBHelper.ToInt32(dio.GetPropertyValue("ID")).ToString();
                    RadComboBoxHelper.Rebind(rcbOriginType, DBHelper.ToInt32(dio.GetPropertyValue("OriginTypeID")));
                    RadComboBoxHelper.Rebind(rcbCountry, DBHelper.ToInt32(dio.GetPropertyValue("CountryID")));
                    RadComboBoxHelper.Rebind(rcbTicketType, DBHelper.ToInt32(dio.GetPropertyValue("TicketTypeID")), true);
                    RadComboBoxHelper.Rebind(rcbOperator, DBHelper.ToInt32(dio.GetPropertyValue("OperatorID")), true);
                    RadComboBoxHelper.Rebind(rcbPumper, DBHelper.ToInt32(dio.GetPropertyValue("PumperID")), true);
                    RadComboBoxHelper.Rebind(rcbState, DBHelper.ToInt32(dio.GetPropertyValue("StateID")));
                    RadComboBoxHelper.Rebind(rcbRegion, DBHelper.ToInt32(dio.GetPropertyValue("RegionID")));
                    RadComboBoxHelper.Rebind(rcbTermial, DBHelper.ToInt32(dio.GetPropertyValue("TerminalID")));
                }
                RadComboBoxHelper.Rebind(rcbProducts);
            }
            cvGaugerTicketType.Enabled = chkGaugerRequired.Checked;
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///          Required method for Designer support - do not modify
        ///          the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion

        public object DataItem
        {
            get
            {
                return this._dataItem;
            }
            set
            {
                this._dataItem = value;
            }
        }

        protected void UpdateDataItem(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                int id = DBHelper.ToInt32(hfID.Value);
                using (SSDB ssdb = new SSDB(true))
                {
                    DataTable dt = ssdb.GetPopulatedDataTable("SELECT * FROM tblOrigin WHERE ID={0}", id);
                    bool isNew = dt.Rows.Count != 1;
                    DataRow row = isNew ? dt.NewRow() : dt.Rows[0];
                    row["OriginTypeID"] = RadComboBoxHelper.SelectedValue(rcbOriginType);
                    row["Name"] = rtxName.Text;
                    row["LeaseName"] = rtxLeaseName.Text;
                    row["LeaseNum"] = rtxLeaseNum.Text;
                    row["H2S"] = chkH2S.Checked;
                    row["Station"] = rtxStation.Text;
                    row["TicketTypeID"] = RadComboBoxHelper.SelectedValue(rcbTicketType);
                    row["StateID"] = RadComboBoxHelper.SelectedValue(rcbState);
                    row["RegionID"] = RadComboBoxHelper.SelectedDbValue(rcbRegion);
                    row["County"] = rcboCounty.Text;
                    row["Address"] = rtxAddress.Text;
                    row["City"] = rtxCity.Text;
                    row["Zip"] = rtxZip.Text;
                    row["TimeZoneID"] = RadComboBoxHelper.SelectedDbValue(rcbTimeZone);
                    row["UseDST"] = chkDST.Checked;
                    row["ProducerID"] = RadComboBoxHelper.SelectedDbValue(rcbProducer);
                    row["OperatorID"] = RadComboBoxHelper.SelectedDbValue(rcbOperator);
                    row["PumperID"] = RadComboBoxHelper.SelectedDbValue(rcbPumper);
                    row["UOMID"] = RadComboBoxHelper.SelectedDbValue(rcbUom);
                    row["FieldName"] = rcbFieldName.Text;
                    row["LegalDescription"] = rtxLegalDescription.Text;
                    row["WellAPI"] = rtxWellAPI.Text;
                    row["LAT"] = rtxLAT.Text;
                    row["LON"] = rtxLON.Text;
                    row["GeoFenceRadiusMeters"] = rntxGeoFenceRadiusMeters.DbValue;
                    row["NDICFileNum"] = rtxNDIC.Text;
                    row["CTBNum"] = rtxCTB.Text;
                    row["CA"] = rtxCA.Text;
                    row["NDM"] = rtxNDM.Text;
                    row["SpudDate"] = rdpSpudDate.DbSelectedDate ?? DBNull.Value;
                    row["TotalDepth"] = rntxTotalDepth.DbValue ?? DBNull.Value;
                    row["TaxRate"] = rntxTaxRate.DbValue ?? DBNull.Value;
                    row["GaugerID"] = RadComboBoxHelper.SelectedDbValue(rcbGauger);
                    row["GaugerTicketTypeID"] = RadComboBoxHelper.SelectedDbValue(rcbGaugerTicketType);
                    row["GaugerRequired"] = chkGaugerRequired.Checked;
                    row["DrivingDirections"] = txtDrivingDirections.Text;
                    row["SulfurContent"] = rntxSulfurContent.DbValue ?? DBNull.Value;
                    row["PrivateRoadMiles"] = (rtxPRM.Text.Trim() == "") ? DBNull.Value : (object)rtxPRM.Text;
                    row["TerminalID"] = RadComboBoxHelper.SelectedDbValue(rcbTermial);

                    id = (int)SSDBSave.SaveDataRow(ssdb, row, "tblOrigin", "ID", SSDBSave.UpdateMode.IgnoreConcurrencyExceptions, UserSupport.UserName);

                    string sql = string.Format("DELETE FROM tblOriginProducts WHERE OriginID = {0}; ", id);
                    foreach (RadComboBoxItem li in rcbProducts.CheckedItems)
                    {
                        sql += string.Format("INSERT INTO tblOriginProducts (OriginID, ProductID) VALUES ({0}, {1}); "
                            , id
                            , li.Value);
                    }
                    sql += string.Format("DELETE FROM tblOriginCustomers WHERE OriginID = {0}; ", id);
                    foreach (RadComboBoxItem li in rcbShippers.CheckedItems)
                    {
                        sql += string.Format("INSERT INTO tblOriginCustomers (OriginID, CustomerID) VALUES ({0}, {1}); "
                            , id
                            , li.Value);
                    }
                    ssdb.ExecuteSql(sql);
                    ssdb.CommitTransaction();
                }
            }
        }

        protected void rcbOperator_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            RadComboBoxHelper.Rebind(rcbPumper, true);
        }

        protected void rcbTicketType_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {

        }

        protected void rcbCountry_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            RadComboBoxHelper.Rebind(rcbTicketType, true);
            if (rcbTicketType.SelectedIndex == -1)
                rcbTicketType.SelectedIndex = 0;
            RadComboBoxHelper.Rebind(rcbState, true);
            if (rcbState.SelectedIndex == -1)
                rcbState.SelectedIndex = 0;
        }

        protected void chkGaugerRequired_CheckedChanged(object sender, EventArgs e)
        {
            cvGaugerTicketType.Enabled = chkGaugerRequired.Checked;
            if (chkGaugerRequired.Checked && rcbGaugerTicketType.SelectedIndex == 0 && rcbGaugerTicketType.Items.Count == 2)
                rcbGaugerTicketType.SelectedIndex = 1;
        }

    }

}