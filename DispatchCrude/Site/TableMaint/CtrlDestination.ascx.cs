﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using DispatchCrude.App_Code;
using AlonsIT;
using DispatchCrude.Core;

namespace DispatchCrude.Site.TableMaint
{
    public partial class CtrlDestination : System.Web.UI.UserControl
    {
        private object _dataItem = null;

        protected void Page_Init(object sender, System.EventArgs e)
        {
        }

        private void ConfigureAjax(bool enabled)
        {
            if (enabled)
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbCountry, rcbState);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            ConfigureAjax(!Settings.SettingsID.DisableAJAX.AsBool());

            if (hfID.Value == "")
            {
                hfID.Value = "0";

                // populate the Destination fields
                if (DataItem != null && DataItem is DataRowView)
                {
                    DataRowView dv = DataItem as DataRowView;

                    dsProduct.SelectParameters["DestID"].DefaultValue = dsShippers.SelectParameters["DestID"].DefaultValue = hfID.Value = dv["ID"].ToString();
                    RadComboBoxHelper.Rebind(rcbCountry, dv["CountryID"]);
                    RadComboBoxHelper.Rebind(rcbState, dv["StateID"]);
                    RadComboBoxHelper.Rebind(rcbTerminal, dv["TerminalID"]);
                }
                else if (DataItem != null && DataItem is GridInsertionObject)
                {
                    GridInsertionObject dio = DataItem as GridInsertionObject;

                    dsProduct.SelectParameters["DestID"].DefaultValue = dsShippers.SelectParameters["DestID"].DefaultValue = hfID.Value = DBHelper.ToInt32(dio.GetPropertyValue("ID")).ToString();
                    RadComboBoxHelper.Rebind(rcbCountry, DBHelper.ToInt32(dio.GetPropertyValue("CountryID")));
                    RadComboBoxHelper.Rebind(rcbState, DBHelper.ToInt32(dio.GetPropertyValue("StateID")));
                    RadComboBoxHelper.Rebind(rcbTerminal, DBHelper.ToInt32(dio.GetPropertyValue("TerminalID")));
                }
                RadComboBoxHelper.Rebind(rcbProducts);
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///          Required method for Designer support - do not modify
        ///          the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion

        public object DataItem
        {
            get
            {
                return this._dataItem;
            }
            set
            {
                this._dataItem = value;
            }
        }

        protected void UpdateDataItem(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                int id = DBHelper.ToInt32(hfID.Value);
                using (SSDB ssdb = new SSDB(true))
                {
                    DataTable dt = ssdb.GetPopulatedDataTable("SELECT * FROM tblDestination WHERE ID={0}", id);
                    bool isNew = dt.Rows.Count != 1;
                    DataRow row = isNew ? dt.NewRow() : dt.Rows[0];
                    row["DestinationTypeID"] = RadComboBoxHelper.SelectedValue(rcbDestinationType);
                    row["Name"] = rtxName.Text;
                    row["Station"] = rtxStation.Text;
                    row["TicketTypeID"] = RadComboBoxHelper.SelectedValue(rcbDestTicketType);
                    row["StateID"] = RadComboBoxHelper.SelectedValue(rcbState);
                    row["RegionID"] = RadComboBoxHelper.SelectedDbValue(rcbRegion);
                    row["Address"] = rtxAddress.Text;
                    row["City"] = rtxCity.Text;
                    row["Zip"] = rtxZip.Text;
                    row["TimeZoneID"] = RadComboBoxHelper.SelectedDbValue(rcbTimeZone);
                    row["UseDST"] = chkDST.Checked;
                    row["UOMID"] = RadComboBoxHelper.SelectedDbValue(rcbUom);
                    row["LAT"] = rtxLAT.Text;
                    row["LON"] = rtxLON.Text;                    
                    row["GeoFenceRadiusMeters"] = rntxGeoFenceRadiusMeters.DbValue;
                    row["DrivingDirections"] = txtDrivingDirections.Text;
                    row["PrivateRoadMiles"] = (rtxPRM.Text.Trim() == "") ? DBNull.Value : (object)rtxPRM.Text;
                    row["TerminalID"] = RadComboBoxHelper.SelectedDbValue(rcbTerminal);

                    id = (int)SSDBSave.SaveDataRow(ssdb, row, "tblDestination", "ID", SSDBSave.UpdateMode.IgnoreConcurrencyExceptions, UserSupport.UserName);

                    string sql = string.Format("DELETE FROM tblDestinationProducts WHERE DestinationID = {0}; ", id);
                    foreach (RadComboBoxItem li in rcbProducts.CheckedItems)
                    {
                        sql += string.Format("INSERT INTO tblDestinationProducts (DestinationID, ProductID) VALUES ({0}, {1}); "
                            , id
                            , li.Value);
                    }

                    sql += string.Format("DELETE FROM tblDestinationCustomers WHERE DestinationID = {0}; ", id);
                    foreach (RadComboBoxItem li in rcbShippers.CheckedItems)
                    {
                        sql += string.Format("INSERT INTO tblDestinationCustomers (DestinationID, CustomerID) VALUES ({0}, {1}); "
                            , id
                            , li.Value);
                    }
                    
                    ssdb.ExecuteSql(sql);
                    ssdb.CommitTransaction();
                }
            }
        }

        protected void rcbCountry_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            RadComboBoxHelper.Rebind(rcbState, true);
            if (rcbState.SelectedIndex == -1)
                rcbState.SelectedIndex = 0;
        }

    }

}