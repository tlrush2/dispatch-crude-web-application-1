﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CtrlDestination.ascx.cs" Inherits="DispatchCrude.Site.TableMaint.CtrlDestination" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<style type="text/css">
    .rgEditForm {
            width: auto !important;
    }
    .rgEditForm > div + div,
    .RadGrid .rgEditForm {
            height: auto !important;
    }
    .rgEditForm > div > table{
            height: 100%;
    }
    .rgEditForm > div > table > tbody > tr > td{
            padding: 4px 10px;
    }
    .H2S
    {
        color: red;
    }
    .Deleted
    {
        color: gray;
    }    
    .label
    {
        color:black;
    }
</style>
<link href="~/Styles/tablemx.css?version=1.0" rel="stylesheet" type="text/css" />
<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server" >
    <script type="text/javascript">
        function validateItemSpecified(sender, args) {
            debugger;
            //invalid if first character is a "("
            args.IsValid = !args.Value.startsWith("(");
        };
    </script>
</telerik:RadScriptBlock>
<asp:Table id="tblMain" runat="server" border="0" style="border-collapse: collapse">
    <asp:TableRow ID="trData" runat="server" style="vertical-align:top;" >
        <asp:TableCell runat="server" >
            <div class="label">
                  <asp:Label runat="server" Text="Type:<span style='color:red;'>*</span>" />
            </div>
            <div class ="data">
                <asp:HiddenField ID="hfID" runat="server" />
                <telerik:RadComboBox ID="rcbDestinationType" runat="server" Width="200px" ClientIDMode="Static" 
                    DataSourceID="dsDestinationType" DataTextField="Name" DataValueField="ID" EmptyMessage="Select Type"
                    SelectedValue='<%# Eval("DestinationTypeID") %>'
                    ValidationGroup="DestinationEdit" CausesValidation="false" />
                <asp:RequiredFieldValidator ID="rfvDestinationType" runat="server" 
                    ControlToValidate="rcbDestinationType" ValidationGroup="DestinationEdit"
                    Text="&nbsp;!" ErrorMessage="A Type must be specified" CssClass="NullValidator" />
            </div>
            <div class="spacer" ></div>
            <div class="label">
                  <asp:Label runat="server" Text="Name:<span style='color:red;'>*</span>" />
            </div>
            <div class ="data">
                <telerik:RadTextBox ID="rtxName" runat="server" Width="200px" ClientIDMode="Static" MaxLength="50" 
                    Text='<%# Eval("Name") %>' ValidationGroup="DestinationEdit" />
                <asp:RequiredFieldValidator ID="rfvName" runat="server" 
                    ControlToValidate="rtxName" ValidationGroup="DestinationEdit"
                    Text="&nbsp;!" ErrorMessage="A Name must be specified" CssClass="NullValidator" />
            </div>
            <div class="spacer" ></div>
            <div class="label">
                  <asp:Label runat="server" Text="Station:" />
            </div>
            <div class ="data">
                <telerik:RadTextBox ID="rtxStation" runat="server" Width="200px" ClientIDMode="Static" MaxLength="25" 
                    Text='<%# Eval("Station") %>' ValidationGroup="DestinationEdit"/>
            </div>
            <div class="spacer" ></div>
            <div class="label">
                  <asp:Label runat="server" Text="Ticket Type:<span style='color:red;'>*</span>" />
            </div>
            <div class ="data">
                <telerik:RadComboBox ID="rcbDestTicketType" runat="server" Width="200px" ClientIDMode="Static" 
                    DataSourceID="dsDestTicketType" DataTextField="Name" DataValueField="ID" EmptyMessage="Select Ticket Type"
                    SelectedValue='<%# Eval("TicketTypeID") %>'
                    ValidationGroup="DestinationEdit" CausesValidation="false"
                    AutoPostBack="true" /> 
                    
                <asp:RequiredFieldValidator ID="rfvDestTicketType" runat="server" 
                    ControlToValidate="rcbDestTicketType" ValidationGroup="DestinationEdit"
                    Text="&nbsp;!" ErrorMessage="A Ticket Type must be specified" CssClass="NullValidator" />
            </div>
            <div class="spacer" ></div>
            <div class="label">
                  <asp:Label runat="server" Text="Shippers:<span style='color:red;'>*</span>" />
            </div>
            <div class ="data">
                <telerik:RadComboBox ID="rcbShippers" runat="server" Width="200px" ClientIDMode="Static" 
                        DataSourceID="dsShippers" DataTextField="Name" DataValueField="ID" CheckBoxes="true" DataCheckedField="Checked"
                        EnableCheckAllItemsCheckBox="true" EmptyMessage="Select Shipper(s)"
                        ValidationGroup="DestinationEdit" CausesValidation="false" />
                <asp:RequiredFieldValidator ID="rfvShippers" runat="server" 
                    ControlToValidate="rcbShippers" ValidationGroup="DestinationEdit"
                    Text="&nbsp;!" ErrorMessage="At least 1 Shipper must be specified" CssClass="NullValidator" />
            </div>
            <div class="spacer" ></div>
            <div class="label">
                  <asp:Label runat="server" Text="Products:<span style='color:red;'>*</span>" />
            </div>
            <div class ="data">
                <telerik:RadComboBox ID="rcbProducts" runat="server" Width="200px" ClientIDMode="Static" 
                        DataSourceID="dsProduct" DataTextField="ShortName" DataValueField="ID" CheckBoxes="true" DataCheckedField="Checked"
                        EnableCheckAllItemsCheckBox="true" EmptyMessage="Select Product(s)"
                        ValidationGroup="DestinationEdit" CausesValidation="false" />
                <asp:RequiredFieldValidator ID="rfvProducts" runat="server" 
                    ControlToValidate="rcbProducts" ValidationGroup="DestinationEdit"
                    Text="&nbsp;!" ErrorMessage="At least 1 product must be specified" CssClass="NullValidator" />
            </div>
            <div class="spacer" ></div>
            <div class="label">
                  <asp:Label runat="server" Text="UOM:<span style='color:red;'>*</span>" />
            </div>
            <div class ="data">
                <telerik:RadComboBox ID="rcbUom" runat="server" Width="200px" ClientIDMode="Static" 
                    DataSourceID="dsUom" DataTextField="Name" DataValueField="ID" EmptyMessage="Select..."
                    SelectedValue='<%# Eval("UomID") %>'
                    ValidationGroup="DestinationEdit" CausesValidation="false" />
                <asp:RequiredFieldValidator ID="rfvUom" runat="server" 
                    ControlToValidate="rcbUom" ValidationGroup="DestinationEdit"
                    Text="&nbsp;!" ErrorMessage="A UOM must be specified" CssClass="NullValidator" />
            </div>
       </asp:TableCell>
        <asp:TableCell>
            <div class="label">
                  <asp:Label runat="server" Text="Country:<span style='color:red;'>*</span>" />
            </div>
            <div class ="data">
                <telerik:RadComboBox ID="rcbCountry" runat="server" Width="200px" ClientIDMode="Static" 
                    DataSourceID="dsCountry" DataTextField="Name" DataValueField="ID" EmptyMessage="Select Country"
                    ValidationGroup="DestinationEdit" CausesValidation="false"
                    Enabled='<%# (DataItem is Telerik.Web.UI.GridInsertionObject) %>'
                    AutoPostBack="true" OnSelectedIndexChanged="rcbCountry_SelectedIndexChanged" />
                <asp:RequiredFieldValidator ID="rfvCountry" runat="server" 
                    ControlToValidate="rcbCountry" ValidationGroup="DestinationEdit"
                    Text="&nbsp;!" ErrorMessage="A Country must be specified" CssClass="NullValidator" />
            </div>
            <div class="spacer"></div>
            <div class="label">
                  <asp:Label runat="server" Text="State/Prov:<span style='color:red;'>*</span>" />
            </div>
            <div class ="data">
                <telerik:RadComboBox ID="rcbState" runat="server" Width="200px" ClientIDMode="Static" 
                    DataSourceID="dsState" DataTextField="Name" DataValueField="ID" EmptyMessage="Select State"
                    ValidationGroup="DestinationEdit" CausesValidation="false" />
                <asp:RequiredFieldValidator ID="rfvState" runat="server" 
                    ControlToValidate="rcbState" ValidationGroup="DestinationEdit"
                    Text="&nbsp;!" ErrorMessage="A State must be specified" CssClass="NullValidator" />
            </div>
            <div class="spacer"></div>
            <div class="label">
                  <asp:Label runat="server" Text="Region:<span style='color:red;'>*</span>" />
            </div>
            <div class ="data">
                <telerik:RadComboBox ID="rcbRegion" runat="server" Width="200px" ClientIDMode="Static" 
                    DataSourceID="dsRegion" DataTextField="Name" DataValueField="ID" EmptyMessage="Select Region"
                    SelectedValue='<%# Eval("RegionID") %>'
                    ValidationGroup="DestinationEdit" CausesValidation="false" />
                <asp:RequiredFieldValidator ID="rfvRegion" runat="server" 
                    ControlToValidate="rcbRegion" ValidationGroup="DestinationEdit"
                    Text="&nbsp;!" ErrorMessage="A Region must be specified" CssClass="NullValidator" />
            </div>
            <div class="spacer"></div>
            <div class="label">
                  <asp:Label runat="server" Text="Address:" />
            </div>
            <div class ="data">
                <telerik:RadTextBox ID="rtxAddress" runat="server" Width="200px" ClientIDMode="Static" MaxLength="50" 
                    Text='<%# Eval("Address") %>' ValidationGroup="DestinationEdit" />
            </div>
            <div class="spacer"></div>
            <div class="label">
                  <asp:Label runat="server" Text="City:" />
            </div>
            <div class ="data">
                <telerik:RadTextBox ID="rtxCity" runat="server" Width="200px" ClientIDMode="Static" MaxLength="30" 
                    Text='<%# Eval("City") %>' ValidationGroup="DestinationEdit" />
            </div>
            <div class="spacer"></div>
            <div class="label">
                  <asp:Label runat="server" Text="Zip:" />
            </div>
            <div class ="data">
                <telerik:RadTextBox ID="rtxZip" runat="server" Width="200px" ClientIDMode="Static" MaxLength="10" 
                    Text='<%# Eval("Zip") %>' ValidationGroup="DestinationEdit" />
            </div>
            <div class="spacer"></div>
            <div class="label">
                  <asp:Label runat="server" Text="Time Zone/DST:<span style='color:red;'>*</span>" />
            </div>
            <div class ="data">
                <telerik:RadComboBox ID="rcbTimeZone" runat="server" 
                    DataSourceID="dsTimeZone" DataTextField="Name" DataValueField="ID" Width="150px" 
                    SelectedValue='<%# Eval("TimeZoneID") %>' 
                    ValidationGroup="DestinationEdit" CausesValidation="false" />
                <asp:RequiredFieldValidator ID="rfvTimeZone" runat="server" ControlToValidate="rcbTimeZone" 
                    Display="Dynamic" Text="&nbsp;!" ErrorMessage="Time Zone must be specified" CssClass="NullValidator" />
                <asp:CheckBox ID="chkDST" runat="server" Checked='<%# Eval("UseDST") %>' />
            </div>
        </asp:TableCell>
        <asp:TableCell>
            <div class="label">
                <asp:Label ID="lblDrivingDirections" runat="server" Text="Driving Directions" />
            </div>
            <div class ="data">
                <telerik:RadTextBox ID="txtDrivingDirections" runat="server" MaxLength="500" TextMode="MultiLine" Width="300px" Height="100px" 
                    Text='<%# Eval("DrivingDirections") %>' />
            </div>
            <div class="label">
                  <asp:Label runat="server" Text="LAT/LON | Geo Fence (Meters):" />
            </div>
            <div class ="data">
                <telerik:RadTextBox ID="rtxLAT" runat="server" Width="73px" ClientIDMode="Static" MaxLength="20" 
                    Text='<%# Eval("LAT") %>' ValidationGroup="DestinationEdit" />
                /
                <telerik:RadTextBox ID="rtxLON" runat="server" Width="73px" ClientIDMode="Static" MaxLength="20" 
                    Text='<%# Eval("LON") %>' ValidationGroup="DestinationEdit" />
                &nbsp;
                <telerik:RadNumericTextBox ID="rntxGeoFenceRadiusMeters" runat="server" ClientIDMode="Static"
                    DataType="System.Int32" NumberFormat-DecimalDigits="0"
                    Width="35px" DbValue='<%# Eval("GeoFenceRadiusMeters") %>'  />
                <br />
                <asp:RegularExpressionValidator ID="revLAT" runat="server" ControlToValidate="rtxLAT" 
                    ValidationExpression="^[0-9\-\.]+$"
                    ValidationGroup="DestinationEdit" Text="Invalid Lattitude was provided." CssClass="NullValidator" Display="Dynamic" />
                <br />
                <asp:RegularExpressionValidator ID="revLON" runat="server" ControlToValidate="rtxLON" 
                    ValidationExpression="^[0-9\-\.]+$"
                    ValidationGroup="DestinationEdit" Text="Invalid Longitude was provided." CssClass="NullValidator" Display="Dynamic" />
            </div>
            <div class="label">
                  <asp:Label runat="server" Text="Private Road Miles (PRM):" />
            </div>
            <div class ="data">
                <telerik:RadTextBox ID="rtxPRM" runat="server" Width="200px" ClientIDMode="Static" MaxLength="10" 
                    Text='<%# Eval("PrivateRoadMiles") %>' ValidationGroup="DestinationEdit" />
                <br />
                <asp:RegularExpressionValidator ID="revPRM" runat="server" ControlToValidate="rtxPRM" 
                    ValidationExpression="^\d{0,2}(\.\d{0,1})?$|^$"
                    ValidationGroup="DestinationEdit" Text="Invalid PRM was provided. Format = (##.#)" CssClass="NullValidator" Display="Dynamic" />
            </div>
            <div class="spacer"></div>
            <div class="label">
                  <asp:Label runat="server" Text="Terminal:" />
            </div>
            <div class ="data">
                <telerik:RadComboBox ID="rcbTerminal" runat="server" Width="200px" ClientIDMode="Static" 
                    DataSourceID="dsTerminal" DataTextField="Name" DataValueField="ID"
                    ValidationGroup="DestinationEdit" CausesValidation="false" />
            </div>
        </asp:TableCell>
        <asp:TableCell>
            <asp:ValidationSummary ID="validationSummaryMain" runat="server" DisplayMode="BulletList" Enabled="true" EnableClientScript="true"
                ValidationGroup="DestinationEdit"
                CssClass="NullValidator" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow id="trControls" runat="server">
        <asp:TableCell runat="server" style="text-align:left" ColumnSpan="1">
            <div style="height:10px;" ></div>
            <asp:Button ID="cmdUpdate" runat="server" Text="Update" CssClass="btn btn-blue shiny" 
                CommandName="Update" CausesValidation="true" ValidationGroup="DestinationEdit"
                OnClick="UpdateDataItem" 
                Visible='<%# !(DataItem is Telerik.Web.UI.GridInsertionObject) %>'>
            </asp:Button>
            <asp:Button Text="Save New" runat="server" CssClass="floatLeft btn btn-blue shiny"
                CommandName="PerformInsert" CausesValidation="true" ValidationGroup="DestinationEdit"
                OnClick="UpdateDataItem" 
                Visible='<%# (DataItem is Telerik.Web.UI.GridInsertionObject) %>' />
            &nbsp;
            <asp:Button ID="cmdCancel" runat="server" Text="Cancel" CssClass="btn btn-blue shiny" CommandName="Cancel" CausesValidation="False" ValidationGroup="DestinationEdit" />
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
<blac:DBDataSource ID="dsDestinationType" runat="server" SelectCommand="SELECT ID, Name = DestinationType FROM dbo.tblDestinationType ORDER BY Name" />
<blac:DBDataSource ID="dsCountry" runat="server" SelectCommand="SELECT ID, Name FROM dbo.tblCountry ORDER BY Name" />
<blac:DBDataSource ID="dsProduct" runat="server" SelectCommand="SELECT P.ID, P.Name, P.ShortName, Checked = cast(CASE WHEN DP.ID IS NULL THEN 0 ELSE 1 END as bit) FROM dbo.tblProduct P LEFT JOIN tblDestinationProducts DP ON DP.DestinationID = @DestID AND DP.ProductID = P.ID ORDER BY P.Name" >
    <SelectParameters>
        <asp:Parameter Name="DestID" DefaultValue="0" />
    </SelectParameters>
</blac:DBDataSource>
<blac:DBDataSource ID="dsShippers" runat="server" SelectCommand="SELECT C.ID, C.Name, Checked = cast(CASE WHEN DC.ID IS NULL THEN 0 ELSE 1 END as bit) FROM dbo.tblCustomer C LEFT JOIN tblDestinationCustomers DC ON DC.DestinationID = @DestID AND DC.CustomerID = C.ID ORDER BY C.Name" >
    <SelectParameters>
        <asp:Parameter Name="DestID" DefaultValue="0" />
    </SelectParameters>
</blac:DBDataSource>
<blac:DBDataSource ID="dsState" runat="server" SelectCommand="SELECT ID, Name = FullName FROM dbo.tblState WHERE (@CountryID = -1 OR CountryID = @CountryID) ORDER BY Name" >
    <SelectParameters>
            <asp:ControlParameter Name="CountryID" ControlID="rcbCountry" PropertyName="SelectedValue" DbType="String" DefaultValue="-1" />
    </SelectParameters>
</blac:DBDataSource>
<blac:DBDataSource ID="dsRegion" runat="server" SelectCommand="SELECT ID, Name FROM tblRegion ORDER BY Name" />
<blac:DBDataSource ID="dsTerminal" runat="server" SelectCommand="SELECT ID, Name FROM tblTerminal WHERE DeleteDateUTC IS NULL UNION SELECT NULL, '(All Terminals)' ORDER BY Name" />
<blac:DBDataSource ID="dsUOM" runat="server" SelectCommand="SELECT ID, Name FROM tblUom ORDER BY Name" />
<blac:DBDataSource ID="dsTimeZone" runat="server" SelectCommand="SELECT ID, Name FROM tblTimeZone ORDER BY Name" />
<blac:DBDataSource ID="dsDestTicketType" runat="server" SelectCommand="SELECT ID, Name FROM dbo.tblDestTicketType ORDER BY Name" />