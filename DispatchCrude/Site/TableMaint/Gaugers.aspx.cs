﻿using System;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using Telerik.Web.UI;
using DispatchCrude.Core;
using AlonsIT;
using DispatchCrude.Extensions;

namespace DispatchCrude.Site.TableMaint
{
    public partial class Gaugers : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Print tab navigation html to page
            tabmenu.InnerHtml = NavigationHelper.PrintTabArray(NavigationHelper.TabSet_Resources, "Tab_Gaugers").ToString();
        }

        protected void grid_ItemCommand(object source, GridCommandEventArgs e)
        {
        }

        protected void grid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item.IsInEditMode)
            {
                // "filter" the dropdowns based on the current CarrierID (and hookup the ddlCarrier_SelectedIndexChanged event)
                GridEditableItem gei = e.Item as GridEditableItem;
                if (e.Item is GridEditFormInsertItem)
                {
                    using (SSDB db = new SSDB())
                    {
                        ((e.Item as GridEditFormInsertItem).FindControl("txtIdNumber") as TextBox).Text = db.QuerySingleValue("SELECT max(isnull(CASE WHEN ISNUMERIC(IdNumber) = 1 THEN IdNumber ELSE 0 END, 1000)) + 1 FROM tblGauger").ToString();
                    }
                }
            }
        }

        protected void ServerValidate_Unique(object source, ServerValidateEventArgs args)
        {
            args.IsValid = true; // default value
            if ((source as WebControl).NamingContainer is GridEditableItem)
            {
                GridEditableItem gdi = (source as WebControl).NamingContainer as GridEditableItem;
                object id = App_Code.RadGridHelper.GetGridItemID(gdi);
                object first = (gdi["FirstName"].FindControl("txtFirstName") as TextBox).Text
                    , last = (gdi["LastName"].FindControl("txtLastName") as TextBox).Text;
                if (first.ToString().Length > 0 && last.ToString().Length > 0)
                {
                    using (SSDB db = new SSDB())
                    {
                        using (SqlCommand cmd = db.BuildCommand("SELECT count(1) FROM tblGauger WHERE ID<>@ID AND FirstName=@First AND LastName=@Last"))
                        {
                            cmd.Parameters.AddWithValue("@ID", id);
                            cmd.Parameters.AddWithValue("@First", first);
                            cmd.Parameters.AddWithValue("@Last", last);
                            args.IsValid = Converter.ToInt32(cmd.ExecuteScalar()) == 0;
                        }
                    }
                }
            }
        }

    }
}