﻿<%@  Title="Print Configuration" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DATicketTemplate.aspx.cs"
    Inherits="DispatchCrude.Site.PrintBM.DATicketTemplate" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script>
        $("#ctl00_ctl00_EntityCaption").html("Driver App Ticket ZPL");
    </script>
    <div class="tabbable TabRepaint">
        <ul class="nav nav-tabs" id="tabmenu" runat="server">
            <!--Tabs will print here (see code behind)-->
        </ul>
        <div class="tab-content">
            <div class="tab-pane in active">

                <div id="speedbuttons" class="well speedButtonContainer" runat="server">
                    <!--Buttons will print here (see code behind)-->
                </div>

                <div id="gridArea" style="height: 100%; min-height: 500px;">
                    <telerik:RadGrid ID="rgMain" runat="server" EnableHeaderContextMenu="False" CellSpacing="0" GridLines="None" EnableLinqExpressions="false"
                                     AllowSorting="True" AllowFilteringByColumn="false" Height="800" CssClass="GridRepaint" ShowGroupPanel="false" EnableEmbeddedSkins="true" Skin="Vista" AlternatingItemStyle-BackColor="#dcf2fc"
                                     DataSourceID="dsMain" OnItemCreated="grid_ItemCreated" OnItemDataBound="grid_ItemDataBound" OnItemCommand="grid_ItemCommand">
                        <ClientSettings AllowDragToGroup="True">
                            <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                        </ClientSettings>
                        <SortingSettings EnableSkinSortStyles="false" />
                        <GroupingSettings CaseSensitive="False" ShowUnGroupButton="true" />
                        <MasterTableView AutoGenerateColumns="False" DataKeyNames="ID" CommandItemDisplay="Top" AllowMultiColumnSorting="true">
                            <CommandItemSettings ShowAddNewRecordButton="true" AddNewRecordText="Add New Ticket ZPL" ShowExportToExcelButton="false" ShowRefreshButton="true" />
                            <RowIndicatorColumn Visible="True">
                                <HeaderStyle Width="20px"></HeaderStyle>
                            </RowIndicatorColumn>
                            <ExpandCollapseColumn Visible="True">
                                <HeaderStyle Width="20px" />
                            </ExpandCollapseColumn>
                            <SortExpressions>
                            </SortExpressions>
                            <Columns>
                                <telerik:GridTemplateColumn UniqueName="ActionColumn" HeaderStyle-Width="100px" AllowFiltering="false" AllowSorting="false">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" ID="btnEdit" CssClass="btn btn-xs btn-default shiny" CommandName="Edit" Text="Edit" ImageUrl="~/images/edit.png" />
                                        <asp:ImageButton runat="server" ID="btnDelete" CssClass="btn btn-xs btn-default shiny" CommandName="Delete" Text="Delete" ConfirmText="Are you sure?" ImageUrl="~/images/delete.png" />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>

                                <telerik:GridDropDownColumn UniqueName="TicketTypeID" DataField="TicketTypeID" HeaderText="Ticket Type" SortExpression="TicketType"
                                                            FilterControlWidth="70%" HeaderStyle-Width="150px" EditFormColumnIndex="0" ItemStyle-Width="250px"
                                                            DataSourceID="dsTicketType" ListValueField="ID" ListTextField="Name" EmptyListItemValue="0" EmptyListItemText="(Select Ticket Type)"
                                                            FilterControlAltText="LightGreen|White" />

                                <telerik:GridTemplateColumn DataField="TemplateText" EditFormColumnIndex="1"
                                                            HeaderText="Template Text" SortExpression="Notes" FilterControlWidth="70%" UniqueName="Notes" Groupable="false">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtTemplateText" runat="server" Text='<%# Bind("TemplateText") %>' TextMode="MultiLine" Height="150px" Width="600px" />
                                    </EditItemTemplate>
                                    <HeaderStyle Width="100px" />
                                </telerik:GridTemplateColumn>

                                <telerik:GridTemplateColumn UniqueName="Validation" HeaderStyle-Width="0px" Display="false" EditFormColumnIndex="2" EditFormHeaderTextFormat="">
                                    <EditItemTemplate>
                                        <asp:CustomValidator ID="cvGridError" runat="server" Display="None" ErrorMessage="Generic Message" />
                                        <asp:ValidationSummary ID="validationSummaryMain" runat="server" DisplayMode="BulletList" Enabled="true" EnableClientScript="true"
                                                               CssClass="NullValidator" />
                                    </EditItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="CreateDate" UniqueName="CreateDate" SortExpression="CreateDate" DataType="System.DateTime" DataFormatString="{0:M/d/yy}"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Create Date" HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="CreatedByUser" UniqueName="CreatedByUser" SortExpression="CreatedByUser"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Created By" HeaderStyle-Width="80px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="LastChangeDate" UniqueName="LastChangeDate" SortExpression="LastChangeDate" DataType="System.DateTime" DataFormatString="{0:M/d/yy HH:mm}"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Last Update Date" HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="LastChangedByUser" UniqueName="LastChangedByUser" SortExpression="LastChangedByUser"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Last Updated By" HeaderStyle-Width="95px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                            </Columns>
                            <EditFormSettings ColumnNumber="3">
                                <EditColumn FilterControlAltText="Filter EditCommandColumn column" ButtonType="ImageButton" CancelImageUrl="~/images/cancel.png"
                                            UpdateImageUrl="~/images/apply.png" InsertImageUrl="~/images/apply.png" />
                            </EditFormSettings>
                            <PagerStyle AlwaysVisible="False" />
                        </MasterTableView>
                        <HeaderStyle Wrap="False" />
                        <PagerStyle AlwaysVisible="True" />
                        <FilterMenu EnableImageSprites="False" />
                    </telerik:RadGrid>
                </div>
                <blac:DBDataSource ID="dsMain" runat="server" SelectIDNullsToZero="true" SelectCommand="SELECT * FROM tblDriverAppPrintTicketTemplate LEFT JOIN tblTicketType TT ON TT.ID = TicketTypeID ORDER BY TT.Name">
                </blac:DBDataSource>
                <blc:RadGridDBCtrl ID="dbcMain" runat="server"
                                   ControlID="rgMain"
                                   UpdateTableName="tblDriverAppPrintTicketTemplate"
                                   FilterActiveEntities="False" />
                <blac:DBDataSource ID="dsTicketType" runat="server" SelectCommand="SELECT ID, Name FROM tblTicketType ORDER BY Name" />
            </div>
        </div>
    </div>
</asp:Content>