﻿using System;
using System.Collections;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
//add for sql stuff
using System.Data.SqlClient;
using System.Data;
using Telerik.Web.UI;
using DispatchCrude.App_Code;
using DispatchCrude.Core;
using DispatchCrude.DataExchange;
using DispatchCrude.Extensions;
using AlonsIT;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Web.SessionState;
using System.Web.Services;

namespace DispatchCrude.Site.Orders
{
    public partial class Orders_Inquiry : System.Web.UI.Page
    {
        static protected string CHANGES_ENABLED = "DeleteEnabled", REPORT_SOURCE = "SourceTable", REPORT_USERNAMES = "UserNames"
            , ADVANCED_CAPTION = "Advanced", HIDE_CAPTION = "Hide Advanced"
            , REPORTCOLUMNID = "ID", BASEFILTERCOUNT = "BaseFilterCount"
            , FILTERVALUE1 = "FilterValue1", FILTERVALUE2 = "FilterValue2", FILTERTYPEID = "FilterTypeID", FILTEROPERATORID_CSV = "FilterOperatorID_CSV"
            , FILTERALLOWCUSTOMTEXT = "FilterAllowCustomText", FILTER_DROPDOWN_SQL = "FilterDropDownSql"
            , BASEFILTERTYPEID = "BaseFilterTypeID", BASEFILTERPROFILEFIELD = "BaseFilterProfileField"
            , BASEFILTERWHERECLAUSE = "BaseFilterWhereClause", BASEFILTERWHERECLAUSE_PROFILETEMPLATE = "BaseFilterWhereClause_ProfileTemplate";
        static protected string SESSION_RC_INPUT_DT = "SESSION_RC_INPUT_DT";

        protected void Page_Init(object sender, EventArgs e)
        {
            cmdUpdateUserNames.Visible = lblUserNames.Visible = rtbUserNames.Visible = User.IsInRole("editReportCenter");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ConfigureAjax(!Core.Settings.SettingsID.DisableAJAX.AsBool());

            lblReportError.Visible = false;

            if (!IsPostBack)
            {
                rdpStart.Calendar.ShowRowHeaders = rdpEnd.Calendar.ShowRowHeaders = false;
                rdpStart.SelectedDate = rdpEnd.SelectedDate = DateTime.Now.Date;

                //Report list dropdown filtering...
                if (UserSupport.IsInGroup("_DCAdministrator") || UserSupport.IsInGroup("_DCSystemManager") || UserSupport.IsInGroup("_DCSupport"))
                {
                    //Let internal group users see everyone's reports                    
                    dsUserReport.SelectParameters["UserName"].DefaultValue = "ALL";
                }
                else
                {
                    //Restrict report list to "your own" reports
                    dsUserReport.SelectParameters["UserName"].DefaultValue = UserSupport.UserName;
                }

                if (UserSupport.IsInRole("importExportReportCenterDefinitions"))
                {
                    this.panelXML.Visible = true;
                }

                //Populate the report field selection list based upon username/permission
                //8/19/16 - This is now not as important after Maverick asked us to remove field specific restrictions (DCWEB-1659)
                dsReportColumnTree.SelectParameters["UserName"].DefaultValue = UserSupport.UserName;

                rcbUserReport.DataBind();
                if (rcbUserReport.Items.Count == 1)
                    LoadReportColumns();
                ConfigureUserEditing();

                //Print tab navigation html to page
                tabmenu.InnerHtml = NavigationHelper.PrintTabArray(NavigationHelper.TabSet_Reports, "Tab_ReportCenter").ToString();

                //Hide certain page objects when user is not allowed to create/edit reports
                cmdShowAdvancedPanel.Visible = 
                    cmdDeleteReport.Visible = 
                    rgMain.Visible = 
                        User.IsInRole("editReportCenter");
            }
        }

        private void ConfigureAjax(bool enableAjax = true)
        {
            if (enableAjax)
            {
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdLoadReport, cmdLoadReport);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdLoadReport, cmdDeleteReport, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdLoadReport, rtbUserNames);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdLoadReport, rgMain);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdExport, lblExportError, false);

                RadAjaxHelper.AddAjaxSetting(this.Page, cmdLoadReport, rdpStart);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdLoadReport, rdpEnd);
                RadAjaxHelper.AddAjaxSetting(this.Page, rgMain, rgMain);
                RadAjaxHelper.AddAjaxSetting(this.Page, rgMain, rdpStart, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, rgMain, rdpEnd, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbUserReport, cmdLoadReport);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbUserReport, cmdDeleteReport);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbUserReport, cmdRenameReport);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbUserReport, lblReportError);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdDeleteReport, rcbUserReport, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdDeleteReport, rgMain);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdDeleteReport, lblReportError);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdShowAdvancedPanel, cmdShowAdvancedPanel, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdShowAdvancedPanel, panelAdvanced);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdCloneReport, cmdCloneReport, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdCloneReport, rcbUserReport);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdCloneReport, rgMain);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdCloneReport, txtNewReportName);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdCloneReport, panelAdvanced, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdCloneReport, cmdShowAdvancedPanel);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdAddNewReport, rcbUserReport, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdAddNewReport, rgMain);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdAddNewReport, txtNewReportName);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdAddNewReport, panelAdvanced, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdAddNewReport, cmdShowAdvancedPanel);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdRenameReport, rcbUserReport);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdRenameReport, txtNewReportName);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdRenameReport, cmdShowAdvancedPanel);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdRenameReport, panelAdvanced, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdImportXml, rcbUserReport, true);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdImportXml, rgMain, true);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdImportXml, panelAdvanced, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdUpdateUserNames, rtbUserNames);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdUpdateUserNames, cmdShowAdvancedPanel);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdUpdateUserNames, panelAdvanced, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdExport, panelExportError, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdExportXml, panelExportError, false);
            }
            RadAjaxManager.GetCurrent(Page).EnableAJAX = enableAjax;
        }

        private bool IsInRole(string roleName)
        {
            return UserSupport.IsInRole(roleName);
        }

        protected void grid_ItemCommand(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.InitInsertCommandName) //"Add new" button clicked
            {
                // cancel the default operation
                e.Canceled = true;

                Hashtable newValues = new Hashtable();
                newValues["FilterOperatorID"] = 1; // default to "NO FILTER"
                newValues["ReportColumnID"] = 88; // (Blank) Report Column
                newValues["GroupingFunctionID"] = 1; // None choice
                using (SSDB ssdb = new SSDB())
                {
                    newValues["UserReportID"] = rcbUserReport.SelectedValue.ToString();
                    newValues["SortNum"] = ssdb.QuerySingleValue(
                        "SELECT isnull(max(SortNum), 0) + 1 FROM tblUserReportColumnDefinition WHERE UserReportID = {0}"
                            , new object[] { rcbUserReport.SelectedValue });
                    newValues["Export"] = true;
                }
                //Insert the item and rebind
                e.Item.OwnerTableView.InsertItem(newValues);
            }
        }

        protected void cmdExport_Click(object source, EventArgs e)
        {
            try
            {
                lblExportError.Text = "";
                Export();
            }
            catch (SqlException exSQL)
            {
                // record this exception to ELMAH (for internal review)
                Elmah.ErrorSignal.FromCurrentContext().Raise(exSQL);
                if (exSQL.Message.ToLower().Contains("timeout"))
                    lblExportError.Text = "SQL Timeout occurred.";
                else
                    lblExportError.Text = exSQL.Message.Truncate(255, true);
            }
            catch (Exception ex)
            {
                // record this exception to ELMAH (for internal review)
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                lblExportError.Text = ex.Message.Truncate(255, true);
                panelExport.Visible = true;
            }
        }

        private void Export()
        {
            string filename = Path.ChangeExtension(rtxFileName.Text, "xlsx");
            MemoryStream ms = (MemoryStream) new ReportCenterExcelExport(UserSupport.UserName
                    , rcbUserReport.SelectedItem.Attributes[REPORT_SOURCE].ToString()
                    , RadComboBoxHelper.SelectedValue(rcbUserReport))
                .Export(rdpStart.SelectedDate, rdpEnd.SelectedDate, false);
            Response.ExportExcelStream(ms, filename);
        }

        protected void cmdLoadReport_Click(object source, EventArgs e)
        {
            LoadReportColumns();
            rtbUserNames.Text = rcbUserReport.SelectedItem.Attributes[REPORT_USERNAMES].ToString();
        }
        private void LoadReportColumns()
        {
            rgMain.Rebind();
            ConfigureUserEditing();
        }
        private void ConfigureUserEditing()
        {
            this.cmdDeleteReport.Enabled = rcbUserReport.SelectedIndex != -1 && DBHelper.ToBoolean(rcbUserReport.SelectedItem.Attributes[CHANGES_ENABLED]);
            if (rgMain != null && rgMain.Columns != null && rgMain.Columns.FindByUniqueNameSafe("ActionColumn") != null)
            {
                rgMain.Columns.FindByUniqueNameSafe("ActionColumn").Visible = this.cmdDeleteReport.Enabled;
                if (!cmdDeleteReport.Enabled)
                {
                    RadGridHelper.DisableAddNewRecordButton(rgMain);
                }
            }
        }

        protected void cmdDeleteReport_Click(object source, EventArgs e)
        {
            using (SSDB ssdb = new SSDB())
            {
                int count = DBHelper.ToInt32(ssdb.QuerySingleValue(
                                    "SELECT count(*) FROM tblUserReportEmailSubscription WHERE UserReportID = {0}"
                                            , new object[] { rcbUserReport.SelectedValue }));

                if (count > 0)
                {
                    lblReportError.Visible = true;
                    lblReportError.Text = "Cannot delete reports with email subscriptions";
                    return;
                }

                ssdb.ExecuteSql(
                    "DELETE FROM tblUserReportColumnDefinition WHERE UserReportID={0}; DELETE FROM tblUserReportDefinition WHERE ID={0}"
                    , RadComboBoxHelper.SelectedValue(rcbUserReport));

                rcbUserReport.DataBind();
                LoadReportColumns();
            }
        }

        private DataTable GetData()
        {
            DataTable ret = null;
            using (SSDB db = new SSDB())
            {
                ret = db.GetPopulatedDataTable(
                    "SELECT * FROM dbo.viewUserReportColumnDefinition WHERE UserReportID={0} ORDER BY SortNum"
                        , RadComboBoxHelper.SelectedValue(rcbUserReport));

                // attempt to update the FilterValues field to show the actual filter values (when possible)
                foreach (DataRow dr in ret.Select("FilterValue1 IS NOT NULL AND FilterOperatorID IN (2,3,13) AND FilterDropDownSql IS NOT NULL"))
                {
                    if (dr[FILTERVALUE1].ToString().Count(x => x == ',') > 2)
                        dr["FilterValues"] = dr["FilterValues"].ToString().Replace("filtered", "multiple");
                    else
                    {
                        string sql = dr[FILTER_DROPDOWN_SQL].ToString();
                        if (sql.Contains("ORDER BY"))
                            sql = sql.Replace("ORDER BY", string.Format("WHERE ID IN ({0}) ORDER BY", dr[FILTERVALUE1]));
                        else
                            sql = string.Format("SELECT * FROM ({0}) X WHERE ID IN ({1})", sql, dr[FILTERVALUE1]);

                        dr["FilterValues"] = string.Empty;
                        int pos = 0;
                        foreach (DataRow drValues in db.GetPopulatedDataTable(sql).Rows)
                        {
                            dr["FilterValues"] += (pos++ > 0 ? ", " : "") + drValues["Name"];
                        }
                    }
                    dr.AcceptChanges();
                }
            }
            return ret;
        }

        protected void grid_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            DataTable dtData = GetData();
            (sender as RadGrid).DataSource = dtData;
            ReportCenterExcelExport.OrderDateParams dates = new ReportCenterExcelExport.OrderDateParams(dtData);
            rdpStart.DbSelectedDate = dates.Start;
            rdpEnd.DbSelectedDate = dates.End;
        }

        protected void grid_ItemCreated(object sender, GridItemEventArgs e)
        {
        }

        protected void grid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {

            }
            if (e.Item is GridEditableItem && (e.Item as GridEditableItem).DataItem is DataRowView)
            {
                RadComboBox rcbFilterOperator = (RadComboBox)RadGridHelper.GetControlByType((e.Item as GridEditableItem)["FilterValues"], typeof(RadComboBox), true, "rcbFilterOperator");
                if (rcbFilterOperator != null)
                {
                    DataRowView dv = (e.Item as GridEditableItem).DataItem as DataRowView;
                    FilterOperatorChanged(rcbFilterOperator, dv[FILTERVALUE1].ToString(), dv[FILTERVALUE2].ToString());
                }
            }
        }

        protected void grid_DataBound(object sender, EventArgs e)
        {
            // TODO
        }

        private DataRow ReportColumnInfo(object rddtReportColumn)
        {
            return ReportColumnInfo(DBHelper.ToInt32((rddtReportColumn as RadDropDownTree).SelectedValue));
        }
        private DataRow ReportColumnInfo(int rcdID)
        {
            using (SSDB ssdb = new SSDB())
            {
                return ssdb.GetPopulatedDataTable("SELECT * FROM viewReportColumnDefinition WHERE ID = {0}", rcdID).Rows[0];
            }
        }

        protected void rddtReportColumn_EntryAdded(object sender, DropDownTreeEntryEventArgs e)
        {
            int rcdID = DBHelper.ToInt32(e.Entry.Value);
            using (SSDB ssdb = new SSDB())
            {
                string filterOperatorCSV = ReportColumnInfo(rcdID)["FilterOperatorID_CSV"].ToString();
                if (string.IsNullOrWhiteSpace(filterOperatorCSV)) filterOperatorCSV = "0";
                RadComboBox rcbFilterOperator = RadGridHelper.GetColumnRadComboBox((sender as WebControl).NamingContainer, "rcbFilterOperator");
                PopulateFilterOperators(rcbFilterOperator, filterOperatorCSV);
                rcbFilterOperator.SelectedIndex = 0;
                FilterOperatorChanged(rcbFilterOperator);
            }
        }

        protected void rddtReportColumn_DataBound(object sender, EventArgs e)
        {
            string filterOperatorCSV = ReportColumnInfo(sender)["FilterOperatorID_CSV"].ToString();
            if (string.IsNullOrWhiteSpace(filterOperatorCSV)) filterOperatorCSV = "0";
            RadComboBox rcbFilterOperator = RadGridHelper.GetColumnRadComboBox((sender as WebControl).NamingContainer, "rcbFilterOperator");
            PopulateFilterOperators(rcbFilterOperator, filterOperatorCSV);
        }

        protected void rddtReportColumn_NodeDataBound(object sender, DropDownTreeNodeDataBoundEventArguments e)
        {
        }

        protected void rcbReportColumn_ItemDataBound(object sender, RadComboBoxItemEventArgs e)
        {
//            if (e.Item.DataItem is DataRowView)
//            {
//                e.Item.Attributes.Add(REPORTCOLUMNID, (e.Item.DataItem as DataRowView)["ID"].ToString());
//                e.Item.Attributes.Add(BASEFILTERCOUNT, (e.Item.DataItem as DataRowView)[BASEFILTERCOUNT].ToString());
//                e.Item.Attributes.Add(FILTERTYPEID, (e.Item.DataItem as DataRowView)[FILTERTYPEID].ToString());
//                e.Item.Attributes.Add(FILTEROPERATORID_CSV, (e.Item.DataItem as DataRowView)[FILTEROPERATORID_CSV].ToString());
//                e.Item.Attributes.Add(FILTER_DROPDOWN_SQL, (e.Item.DataItem as DataRowView)[FILTER_DROPDOWN_SQL].ToString());
//                e.Item.Attributes.Add(FILTERALLOWCUSTOMTEXT, (e.Item.DataItem as DataRowView)[FILTERALLOWCUSTOMTEXT].ToString());
//            }
        }

        protected void rcbReportColumn_DataBound(object sender, EventArgs e)
        {
//            RadComboBox rcbReportColumn = (RadComboBox)sender;
//            RadComboBox rcbFilterOperator = RadGridHelper.GetColumnRadComboBox(rcbReportColumn.NamingContainer, "rcbFilterOperator");
//            PopulateFilterOperators(rcbFilterOperator, rcbReportColumn.SelectedItem.Attributes[FILTEROPERATORID_CSV].ToString());
        }

        protected void rcbReportColumn_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
//            RadComboBox rcbReportColumn = (RadComboBox)sender;
//            RadComboBox rcbFilterOperator = RadGridHelper.GetColumnRadComboBox(rcbReportColumn.NamingContainer, "rcbFilterOperator");
//            PopulateFilterOperators(rcbFilterOperator, rcbReportColumn.SelectedItem.Attributes[FILTEROPERATORID_CSV].ToString());
//            rcbFilterOperator.SelectedIndex = 0;
//            FilterOperatorChanged(rcbFilterOperator);
        }

        private void PopulateFilterOperators(RadComboBox rcbFilterOperator, string id_csv)
        {
            using (SSDB db = new SSDB())
            {
                DataTable data = db.GetPopulatedDataTable(
                    string.Format("SELECT * FROM tblReportFilterOperator WHERE ID IN ({0})", id_csv));

                rcbFilterOperator.Items.Clear();
                foreach (DataRow row in data.Rows)
                {
                    RadComboBoxItem item = new RadComboBoxItem
                    {
                        Value = row["ID"].ToString()
                        ,
                        Text = row["Name"].ToString()
                    };
                    rcbFilterOperator.Items.Add(item);
                }
            }
        }

        protected void rcbFilterOperator_DataBound(object sender, EventArgs e)
        {
        }

        protected void rcbFilterOperator_ItemDataBound(object sender, RadComboBoxItemEventArgs e)
        {
            if (e.Item.DataItem is DataRowView)
            {
            }
        }

        protected void rcbFilterOperator_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            FilterOperatorChanged((RadComboBox)sender);
        }

        private void FilterOperatorChanged(RadComboBox rcbFilterOperator, string filterValue1 = null, string filterValue2 = null)
        {
            bool singleValueVisible = false, twoValuesVisible = false;
            bool allowMultipleSelection = false;
            switch (RadComboBoxHelper.SelectedValue(rcbFilterOperator))
            {
                case 4: // between filter type
                    twoValuesVisible = true;
                    break;
                case 1: // no filter
                case 7: // is empty filter
                case 8: // is not empty filter
                    // both are invisible
                    break;
                case 3: // IN operator
                    singleValueVisible = true;
                    allowMultipleSelection = true;
                    break;
                default:
                    singleValueVisible = true;
                    break;
            }
            Panel panelSingle = (Panel)RadGridHelper.GetControlByType(rcbFilterOperator.NamingContainer, typeof(Panel), true, "panelFEV_SingleValue");
            panelSingle.Visible = singleValueVisible;
            RadGridHelper.GetControlByType(rcbFilterOperator.NamingContainer, typeof(Panel), false, "panelFEV_TwoValues").Visible = twoValuesVisible;
            if (singleValueVisible)
            {
                object rddtReportColumn = RadGridHelper.GetControlByType(rcbFilterOperator.NamingContainer, typeof(RadDropDownTree), true, "rddtReportColumn");
                RadComboBox rcbSingleValue1 = (RadComboBox)RadGridHelper.GetControlByType(panelSingle, typeof(RadComboBox), true, "rcbSingleValue1");
                if (rcbSingleValue1 != null) rcbSingleValue1.AllowCustomText = true;
                PopulateFilterValueDropDown(rcbSingleValue1, ReportColumnInfo(rddtReportColumn), filterValue1, allowMultipleSelection);
                // ensure the two value comboboxs are set to not allow custom text
                RadComboBox rcbTwoValues1 = (RadComboBox)RadGridHelper.GetControlByType(panelSingle, typeof(RadComboBox), true, "rcbTwoValues1");
                if (rcbTwoValues1 != null) rcbTwoValues1.AllowCustomText = false;
                RadComboBox rcbTwoValues2 = (RadComboBox)RadGridHelper.GetControlByType(panelSingle, typeof(RadComboBox), true, "rcbTwoValues2");
                if (rcbTwoValues2 != null) rcbTwoValues2.AllowCustomText = false;
            }
            else if (twoValuesVisible)
            {
                object rddtReportColumn = RadGridHelper.GetControlByType(rcbFilterOperator.NamingContainer, typeof(RadDropDownTree), true, "rddtReportColumn");
                DataRow drReportColumnInfo = ReportColumnInfo(rddtReportColumn);
                RadComboBox rcbTwoValues1 = (RadComboBox)RadGridHelper.GetControlByType(panelSingle, typeof(RadComboBox), true, "rcbTwoValues1");
                if (rcbTwoValues1 != null) rcbTwoValues1.AllowCustomText = true;
                PopulateFilterValueDropDown(rcbTwoValues1, drReportColumnInfo, filterValue1, allowMultipleSelection);
                RadComboBox rcbTwoValues2 = (RadComboBox)RadGridHelper.GetControlByType(panelSingle, typeof(RadComboBox), true, "rcbTwoValues2");
                if (rcbTwoValues2 != null) rcbTwoValues2.AllowCustomText = true;
                PopulateFilterValueDropDown(rcbTwoValues2, drReportColumnInfo, filterValue2, allowMultipleSelection);

                RadComboBox rcbSingleValue1 = (RadComboBox)RadGridHelper.GetControlByType(panelSingle, typeof(RadComboBox), true, "rcbSingleValue1");
                if (rcbSingleValue1 != null) rcbSingleValue1.AllowCustomText = false;
            }
            else
            {
                RadComboBox rcbSingleValue1 = (RadComboBox)RadGridHelper.GetControlByType(panelSingle, typeof(RadComboBox), true, "rcbSingleValue1");
                if (rcbSingleValue1 != null) ClearFilterCombo(rcbSingleValue1);
                RadComboBox rcbTwoValues1 = (RadComboBox)RadGridHelper.GetControlByType(panelSingle, typeof(RadComboBox), true, "rcbTwoValues1");
                ClearFilterCombo(rcbTwoValues1);
                RadComboBox rcbTwoValues2 = (RadComboBox)RadGridHelper.GetControlByType(panelSingle, typeof(RadComboBox), true, "rcbTwoValues2");
                ClearFilterCombo(rcbTwoValues2);
            }
        }

        private void ClearFilterCombo(RadComboBox rcb)
        {
            rcb.Items.Clear();
            rcb.Text = string.Empty;
        }

        private void PopulateFilterValueDropDown(RadComboBox rcbFilterValue, DataRow drReportColumnInfo, string filterValue, bool allowMultipleSelection)
        {
            rcbFilterValue.Items.Clear();

            string sql = drReportColumnInfo[FILTER_DROPDOWN_SQL].ToString();
            bool allowCustomText = DBHelper.ToBoolean(drReportColumnInfo[FILTERALLOWCUSTOMTEXT]);
            if (!string.IsNullOrEmpty(sql))
            {
                if (DBHelper.ToInt32(drReportColumnInfo[BASEFILTERCOUNT]) > 0)
                {
                    sql = ReportCenterExcelExport.ColumnFilterBaseSql.GenerateDropDownSql(UserSupport.UserName, DBHelper.ToInt32(drReportColumnInfo[REPORTCOLUMNID]), sql);                    
                    // TODO: add the base filters to this sql statement
                }
                using (SSDB db = new SSDB())
                {
                    DataTable data = db.GetPopulatedDataTable(sql);
                    rcbFilterValue.CheckBoxes = data.Rows.Count > 1 && allowMultipleSelection;
                    rcbFilterValue.AllowCustomText = allowCustomText;
                    foreach (DataRow row in data.Rows)
                    {
                        RadComboBoxItem item = new RadComboBoxItem
                        {
                            Value = row["ID"].ToString()
                            ,
                            Text = row["Name"].ToString()
                            ,
                            Checked = false // default to not checked
                        };
                        // check/select those items that are currently in the filterValue
                        if (allowMultipleSelection)
                            item.Checked = filterValue != null && ("," + filterValue + ",").Contains("," + item.Value + ",");
                        else
                            item.Selected = filterValue != null && ("," + filterValue + ",").Contains("," + item.Value + ",");
                        rcbFilterValue.Items.Add(item);
                    }
                }
            }
            if (allowCustomText && filterValue != null)
                rcbFilterValue.Text = filterValue;
        }

        protected void cvFilterValue_ServerValidate(object source, ServerValidateEventArgs args)
        { 
            // TODO
        }
        
        protected void dbcMain_GetUnboundChangedValues(GridEditableItem item, DataRow values)
        {
            if (UpdateFilterValue(item["FilterValues"], "rcbTwoValues2", values, FILTERVALUE2))
                UpdateFilterValue(item["FilterValues"], "rcbTwoValues1", values, FILTERVALUE1);
            else
                UpdateFilterValue(item["FilterValues"], "rcbSingleValue1", values, FILTERVALUE1);
            if (values.RowState == DataRowState.Added || string.IsNullOrEmpty(values["UserReportID"].ToString()))
                values["UserReportID"] = RadComboBoxHelper.SelectedValue(rcbUserReport);
            if (string.IsNullOrEmpty(values["FilterOperatorID"].ToString()))
            {
                int id = RadComboBoxHelper.SelectedValue((RadComboBox)item.FindControl("rcbFilterOperator"));
                values["FilterOperatorID"] = id;
            }
        }

        private bool UpdateFilterValue(TableCell cell, string radComboBoxID, DataRow values, string dataField)
        {
            bool ret = false;
            RadComboBox rcbValues = (RadComboBox)RadGridHelper.GetControlByType(cell, typeof(RadComboBox), true, radComboBoxID);
            if (rcbValues != null)
            {
                if (rcbValues.CheckedItems.Count > 0)
                {
                    values[dataField] = rcbValues.CheckedItems.Where(ci => ci.Checked).Select(ci => ci.Value).Aggregate((a, b) => a + "," + b.ToString());
                    ret = true;
                }
                else if (rcbValues.SelectedIndex != -1)
                {
                    values[dataField] = rcbValues.SelectedItem == null || rcbValues.SelectedItem.Value.Equals(rcbValues.SelectedItem.Text) 
                        ? rcbValues.Text : rcbValues.SelectedItem.Value;
                    ret = true;
                }
                else if (rcbValues.AllowCustomText)
                {
                    values[dataField] = rcbValues.Text;
                    ret = true;
                }
                else
                {
                    values[dataField] = DBNull.Value;
                }
            }
            return ret;
        }

        protected void rcbUserReport_ItemDataBound(object sender, RadComboBoxItemEventArgs e)
        {
            e.Item.Attributes.Add(CHANGES_ENABLED
                , (User.IsInRole("editReportCenter") || Converter.ToBoolean((e.Item.DataItem as DataRowView)["UserEditable"])).ToString());
            e.Item.Attributes.Add(REPORT_SOURCE, (e.Item.DataItem as DataRowView)[REPORT_SOURCE].ToString());
            e.Item.Attributes.Add(REPORT_USERNAMES, (e.Item.DataItem as DataRowView)["UserNames"].ToString());
        }

        private int CloneReport(bool includeColumns)
        {
            int ret = 0;
            using (SSDB ssdb = new SSDB())
            {
                using (SqlCommand cmd = ssdb.BuildCommand("spCloneUserReport"))
                {
                    cmd.Parameters["@userReportID"].Value = rcbUserReport.SelectedValue;
                    cmd.Parameters["@reportName"].Value = txtNewReportName.Text;
                    cmd.Parameters["@userNames"].Value = Converter.ToDBNullFromEmpty(rtbUserNames.Text);
                    cmd.Parameters["@includeColumns"].Value = includeColumns;
                    ret = DBHelper.ToInt32(cmd.ExecuteScalar());
                }
            }
            ResetAdvancedPanel(ret);
            return ret;
        }

        private int RenameReport()
        {
            int ret = DBHelper.ToInt32(rcbUserReport.SelectedValue);
            using (SSDB ssdb = new SSDB())
            {
                using (SqlCommand cmd = ssdb.BuildCommand("UPDATE tblUserReportDefinition SET Name = @Name WHERE ID = @UserReportID"))
                {
                    cmd.Parameters.AddWithValue("@UserReportID", ret);
                    cmd.Parameters.AddWithValue("@Name", txtNewReportName.Text);
                    cmd.ExecuteNonQuery();
                }
            }
            ResetAdvancedPanel(ret);
            return ret;
        }

        private void ResetAdvancedPanel(int userReportID)
        {
            rcbUserReport.DataBind();
            RadComboBoxHelper.SetSelectedValue(rcbUserReport, userReportID);
            LoadReportColumns();
            txtNewReportName.Text = "";
            panelAdvanced.Visible = false;
            cmdShowAdvancedPanel.Text = "Advanced";
        }

        protected void cmdCloneReport_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
                CloneReport(true);
        }

        protected void cmdAddNewReport_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
                CloneReport(false);
        }

        protected void cmdRenameReport_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
                RenameReport();
        }

        protected void cmdShowAdvancedPanel_Click(object sender, EventArgs e)
        {
            this.panelAdvanced.Visible = this.cmdShowAdvancedPanel.Text == ADVANCED_CAPTION;
            this.cmdShowAdvancedPanel.Text = this.panelAdvanced.Visible ? HIDE_CAPTION : ADVANCED_CAPTION;
        }

        protected void rcbUserReport_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            lblReportError.Visible = false;
        }

        protected void cmdUpdateUserNames_Click(object sender, EventArgs e)
        {
            int id = DBHelper.ToInt32(rcbUserReport.SelectedValue);
            using (SSDB ssdb = new SSDB())
            {
                using (SqlCommand cmd = ssdb.BuildCommand("UPDATE tblUserReportDefinition SET UserNames = @UserNames WHERE ID = @UserReportID"))
                {
                    cmd.Parameters.AddWithValue("@UserReportID", id);
                    cmd.Parameters.AddWithValue("@UserNames", Converter.ToDBNullFromEmpty(rtbUserNames.Text));
                    cmd.ExecuteNonQuery();
                }
            }
            ResetAdvancedPanel(id);
        }

        protected void cmdExportXml_Click(object sender, EventArgs e)
        {
            string fileName = null;
            MemoryStream ms = Controllers.ReportCenterController.ExportXmlFile(DBHelper.ToInt32(rcbUserReport.SelectedValue), ref fileName);
            Response.ExportFileStream(ms, fileName);
        }

        /* this did not work and KDA could never figure out why - implemented instead in ReportCenterController */
        //[WebMethod(EnableSession = true)]
        //static public string SaveXmlFile(IEnumerable<HttpPostedFileBase> xmlImportFile)
        //{
        //    // The Name of the Upload component is "files"
        //    if (xmlImportFile != null)
        //    {
        //        foreach (var file in xmlImportFile)
        //        {
        //            DataSet ds = new DataSet();
        //            ds.ReadXml(file.InputStream, XmlReadMode.ReadSchema);
        //            // write the resultant datatable to session
        //            HttpContext.Current.Session[SESSION_RC_INPUT_DT] = ds.Tables[0];
        //            break;
        //        }
        //    }

        //    // Return an empty string to signify success
        //    return "";
        //}

        protected void cmdImportXml_Click(object sender, EventArgs e)
        {
            DataSet ds = (DataSet)Session[Controllers.ReportCenterController.SESSION_RC_IMPORT_DS];

            int id = new Controllers.ReportCenterController.Importer().ImportFromDS(ds, txtNewReportName.Text, rtbUserNames.Text);

            // reset and show this newly imported report
            txtNewReportName.Text = "";
            rcbUserReport.DataBind();
            RadComboBoxHelper.SetSelectedValue(rcbUserReport, id);
            LoadReportColumns();
        }

    }

}