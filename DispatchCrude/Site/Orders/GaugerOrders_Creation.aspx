﻿<%@  Title="Dispatch" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="GaugerOrders_Creation.aspx.cs"
    Inherits="DispatchCrude.Site.Orders.GaugerOrders_Creation" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<asp:Content ID="contentHeader" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css" >
        .H2S
        {
            background-color: Red !important;
            color: White !important;
        }
        /** rcbDestination Columns */
        .rcbHeader ul,
        .rcbFooter ul,
        .rcbItem ul,
        .rcbHovered ul,
        .rcbDisabled ul {
                margin: 0;
                padding: 0;
                width: 100%;
                display: inline-block;
                list-style-type: none;
        }
        .col
        {
                margin: 0;
                padding: 0 5px 0 0;
                line-height: 14px;
                float: left;
        }
        .otCol1
        {
            width: 80px;
        }
        .otCol2
        {
            width: 120px;
        }
       

        /* 
            This is not the correct way to fix this but Maverick approved a "quick fix" to the create page dropdown CSS. 
            Long origin names were overlapping the lease names.  This will be taken care of correctly in the new MVC template.
        */
        #ctl00_ctl00_MainContent_MainContent_rcbOrigin_DropDown 
        {              
              width: auto !important;
        }       
        .oCol1
        {
              width: 150px !important;              
              text-align: left !important;
              padding-right: 15px !important;
              float: left !important;
        }
        .oCo2 
        {
              width: auto !important;
              text-align: left !important;
              padding-left: 15px !important;
              float: left !important;
        }
        #ctl00_ctl00_MainContent_MainContent_rcbDriver_DropDown {
              min-width: 375px !important;
        }
        /* END  above quick fix*/
    </style>
</asp:Content>
<asp:Content ID="mainContent" ContentPlaceHolderID="MainContent" runat="server">
    <telerik:RadScriptBlock ID="radScriptBlock" runat="server">
        <script type="text/javascript">
            var noUpdate = false;
            function chkAll_ToggleStateChanged(sender, args) {
                // if we are being called from the chkSel_CheckedChanged below, then just exit (return)
                //debugger;
                if (noUpdate)
                    return;
                var value = args.get_currentToggleState().get_value();
                if (value == "neither") {
                    sender.set_selectedToggleStateIndex(2);
                    value = "true";
                }
                var items = $find("<%= rgMain.ClientID %>").get_masterTableView().get_dataItems();
                for (i = 0; i < items.length; i++) {
                    var item = items[i];
                    var chk = item.findElement("chkSel");
                    if (chk != null) {
                        chk.checked = (value == "true");
                    }
                }
                DisableSelectionButtons(value == "false");
            };
            function chkSel_CheckedChanged() {
                var grid = $find("<%= rgMain.ClientID %>");
                var items = grid.get_masterTableView().get_dataItems();
                var checked = 0, unchecked = 0;
                for (i = 0; i < items.length; i++) {
                    var item = items[i];
                    var chk = item.findElement("chkSel");
                    if (chk.checked)
                        checked++;
                    else
                        unchecked++;
                }
                noUpdate = true;
                var chkAll = $find("chkAll");
                if (checked == 0)
                    chkAll.set_selectedToggleStateIndex(0);
                else if (unchecked == 0)
                    chkAll.set_selectedToggleStateIndex(2);
                else
                    chkAll.set_selectedToggleStateIndex(1);
                noUpdate = false;
                DisableSelectionButtons(checked == 0);
            };
            function DisableSelectionButtons(disabled) {
                debugger;
                $("#" + "<%= cmdDelete.ClientID %>").prop("disabled", disabled);
                var gaugerID = $find('<%=rcbGauger.ClientID %>').get_selectedItem().get_value();
                $("#" + "<%= cmdDispatch.ClientID %>").prop("disabled", disabled || gaugerID <= 0);
            };
            function rcbGauger_onClientSelectedIndexChanged(sender, eventArgs) {
                debugger;
                chkSel_CheckedChanged();
            }
            // main startup script
            $(function () {
                // ensure all buttons are initially disabled/enabled appropriately
                //chkSel_CheckedChanged();
            });
        </script>
    </telerik:RadScriptBlock>
    <script>
        $("#ctl00_ctl00_EntityCaption").html("Gauger Order Creation");
    </script>
    <div class="tabbable TabRepaint">
        <ul class="nav nav-tabs" id="tabmenu" runat="server">
            <!--Tabs will print here (see code behind)-->
        </ul>
        <div class="tab-content">
            <div class="tab-pane in active">

                <div id="speedbuttons" class="well speedButtonContainer" runat="server">
                    <!--Buttons will print here (see code behind)-->        
                </div>

                <div class="leftpanel" style="overflow-y:auto;">
                    <div class="tabbable">
                        <ul class="nav nav-tabs">
                            <li class="active tab-blue">
                                <a data-toggle="tab" href="#Create" aria-expanded="true">Create</a>
                            </li>
                            <li class="tab-yellow">
                                <a data-toggle="tab" href="#OrderActions" aria-expanded="true">Order Actions</a>
                            </li>
                        </ul>
                        <div id="leftTabs" class="tab-content">
                            <div id="Create" class="tab-pane active">
                                <asp:Panel ID="panelGenerate" runat="server" DefaultButton="cmdCreateLoads">
                                    <div class="Entry">
                                        <asp:Label ID="lblRegion" runat="server" Text="Region" AssociatedControlID="rcbRegion" CssClass="Entry" />
                                        <telerik:RadComboBox Width="100%" runat="server" ID="rcbRegion"
                                                             DataSourceID="dsRegion" DataTextField="Name" DataValueField="ID"
                                                             AutoPostBack="true" OnSelectedIndexChanged="regionChanged" CausesValidation="false" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblOrigin" runat="server" Text="Origin" AssociatedControlID="rcbOrigin" CssClass="Entry" />
                                        <telerik:RadComboBox runat="server" ID="rcbOrigin" AllowCustomText="false" Filter="Contains"
                                                             DataSourceID="dsOrigin" DataTextField="LeaseNum_Name" DataValueField="ID" CausesValidation="false"
                                                             AutoPostBack="true" OnSelectedIndexChanged="rcbOrigin_SelectedIndexChanged" OnItemDataBound="rcbOrigin_ItemDataBound"
                                                             Width="100%" DropDownWidth="275">
                                            <HeaderTemplate>
                                                <ul>
                                                    <li class="col oCol1">Lease #</li>
                                                    <li class="col oCol2">Name</li>
                                                </ul>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <ul>
                                                    <li class="col oCol1">
                                                        <%# DataBinder.Eval(Container.DataItem, "LeaseNum") %>
                                                    </li>
                                                    <li class="col oCol2">
                                                        <%# DataBinder.Eval(Container.DataItem, "Name") %>
                                                    </li>
                                                </ul>
                                            </ItemTemplate>
                                        </telerik:RadComboBox>
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblCustomer" runat="server" Text="Shipper" AssociatedControlID="rcbShipper" />
                                        <telerik:RadComboBox Width="100%" runat="server" ID="rcbShipper"
                                                             DataSourceID="dsCustomer" DataTextField="Name" DataValueField="ID" CausesValidation="false"
                                                             AutoPostBack="true" OnSelectedIndexChanged="entryChanged" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblProduct" runat="server" Text="Product" AssociatedControlID="rcbProduct" CssClass="Entry" />
                                        <telerik:RadComboBox Width="100%" runat="server" ID="rcbProduct"
                                                             DataSourceID="dsProduct" DataTextField="Name" DataValueField="ID" CausesValidation="false"
                                                             AutoPostBack="true" OnSelectedIndexChanged="rcbProduct_SelectedIndexChanged" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblTicketType" runat="server" Text="Gauger Ticket Type" AssociatedControlID="rcbTicketType" />
                                        <telerik:RadComboBox Width="100%" runat="server" ID="rcbTicketType"
                                                             DataSourceID="dsTicketType" DataTextField="Name" DataValueField="ID" CausesValidation="false" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label runat="server" Text="Tank ID" AssociatedControlID="rcbOriginTank" CssClass="Entry" />
                                        <telerik:RadComboBox id="rcbOriginTank" runat="server" Filter="Contains"
                                                             DataSourceID="dsOriginTank" DataTextField="TankNum" DataValueField="ID" CausesValidation="false"
                                                             AutoPostBack="true" OnItemDataBound="rcbOriginTank_ItemDataBound" OnSelectedIndexChanged="rcbOriginTank_SelectedIndexChanged"
                                                             Width="100%">
                                            <HeaderTemplate>
                                                <ul>
                                                    <li class="col otCol1">Tank #</li>
                                                    <li class="col otCol2">Lease #/Name</li>
                                                </ul>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <ul>
                                                    <li class="col otCol1">
                                                        <%# DataBinder.Eval(Container.DataItem, "TankNum") %>
                                                    </li>
                                                    <li class="col otCol2">
                                                        <%# DataBinder.Eval(Container.DataItem, "LeaseNum_Name") %>
                                                    </li>
                                                </ul>
                                            </ItemTemplate>
                                        </telerik:RadComboBox>
                                    </div>
                                    <div class="clear"></div>
                                    <div class="Entry">
                                        <asp:Label ID="lblDestination" runat="server" Text="Destination" AssociatedControlID="rcbDestination" />
                                        <telerik:RadComboBox runat="server" ID="rcbDestination"
                                                             DataSourceID="dsDestination" DataTextField="FullName" DataValueField="ID" CausesValidation="false"
                                                             AutoPostBack="true" OnSelectedIndexChanged="entryChanged"
                                                             Width="100%" DropDownWidth="275">
                                            <HeaderTemplate>
                                                <ul>
                                                    <li class="col dCol1">Destination</li>
                                                    <li class="col dCol2">Type</li>
                                                </ul>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <ul>
                                                    <li class="col dCol1">
                                                        <%# DataBinder.Eval(Container.DataItem, "Name") %>
                                                    </li>
                                                </ul>
                                            </ItemTemplate>
                                        </telerik:RadComboBox>
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblGauger" runat="server" Text="Gauger&nbsp;" AssociatedControlID="rcbGauger" />
                                        <asp:Label runat="server" Text="(also used for 'Dispatch' button)" BackColor="LightBlue" />
                                        <telerik:RadComboBox Width="100%" ID="rcbGauger" runat="server"
                                                             DataSourceID="dsGauger" DataTextField="FullName" DataValueField="ID" CausesValidation="false"
                                                             onClientSelectedIndexChanged="rcbGauger_onClientSelectedIndexChanged"
                                                             AutoPostBack="true" OnSelectedIndexChanged="dispatchValueChanged" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblDispatchConfirmNum" runat="server" Text="Shipper PO #" AssociatedControlID="txtDispatchConfirmNum" />
                                        <asp:TextBox Width="98%" ID="txtDispatchConfirmNum" runat="server" CausesValidation="false"
                                                     AutoPostBack="true" OnTextChanged="txtDispatchConfirmNum_TextChanged" />
                                        <asp:RequiredFieldValidator ID="rfvDispatchConfirmNum" runat="server"
                                                                    ControlToValidate="txtDispatchConfirmNum" InitialValue="" Enabled="false" Display="Dynamic"
                                                                    Text="Shipper PO is required" CssClass="NullValidator" />
                                        <asp:CustomValidator ID="cvDispatchConfirmNum" runat="server" ControlToValidate="txtDispatchConfirmNum"
                                                             OnServerValidate="cvDispatchConfirmNum_ServerValidate" Display="Dynamic"
                                                             Text="Shipper PO already in use for this Shipper" CssClass="NullValidator" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblJobNumber" runat="server" Text="Job #" AssociatedControlID="txtJobNumber" />
                                        <asp:TextBox Width="98%" ID="txtJobNumber" runat="server" CausesValidation="false" />
                                    </div>
                                    <asp:Table ID="tableDateQty" runat="server" Width="100%">
                                        <asp:TableRow ID="tableRow1" runat="server">
                                            <asp:TableCell>
                                                <asp:Label ID="lblPriority" runat="server" Text="Priority" /><br />
                                                <blac:PriorityDropDownList CssClass="btn-xs" ID="ddlPriority" runat="server" Width="60px" CausesValidation="false" />
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:Label ID="lblDueDate" runat="server" Text="Due Date" /><br />
                                                <telerik:RadDatePicker ID="rdpDueDate" runat="server" DateInput-DateFormat="M/d/yy" Width="92px" CausesValidation="false" />
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:Label ID="lblLoadQty" runat="server" Text="Qty" /><br />
                                                <telerik:RadNumericTextBox DataType="System.Int32" ID="rntxtQty" runat="server" Value="1" MinValue="1" MaxValue="10000"
                                                                           ShowSpinButtons="True" Width="50px" CausesValidation="false">
                                                    <NumberFormat ZeroPattern="n" DecimalDigits="0" />
                                                    <EnabledStyle HorizontalAlign="Right" />
                                                </telerik:RadNumericTextBox>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow ID="tableRow2" runat="server">
                                            <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                                <div class="spacer10px"></div>
                                                <asp:Button ID="cmdCreateLoads" runat="server" Text="Generate" CssClass="btn btn-blue shiny"
                                                            OnClick="cmdCreateLoads_Click" Enabled="False" CausesValidation="true" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:Panel>
                            </div>
                            <div id="OrderActions" class="tab-pane">
                                <p><i>These actions apply to any currently selected orders in the grid.</i></p>
                                <p><b>Dispatch</b> - Dispatches selected loads to the gauger specified on the Create tab.</p>
                                <div class="spacer10px"></div>
                                <asp:Panel ID="panelSelectedCtrl" runat="server">
                                    <div class="center">
                                        <asp:Button ID="cmdDelete" runat="server" Text="Delete" CssClass="btn btn-blue shiny" OnClick="cmdDelete_Click" />
                                        &nbsp;
                                        <asp:Button ID="cmdDispatch" runat="server" Text="Dispatch" CssClass="btn btn-blue shiny" OnClick="cmdDispatch_Click" />
                                    </div>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="gridArea" style="height: 100%; min-height: 500px;">
                    <telerik:RadWindowManager ID="radWindowManager" runat="server" EnableShadow="true" />
                    <telerik:RadGrid ID="rgMain" runat="server" EnableHeaderContextMenu="False" CellSpacing="0" GridLines="None" ShowStatusBar="false" EnableEmbeddedSkins="true" Skin="Vista" AlternatingItemStyle-BackColor="#dcf2fc"
                                     AllowSorting="True" AllowFilteringByColumn="true" Height="800" CssClass="GridRepaint" ShowGroupPanel="false" EnableLinqExpressions="false"
                                     DataSourceID="dsMain" OnItemCreated="grid_ItemCreated" OnItemDataBound="grid_ItemDataBound" OnItemCommand="grid_ItemCommand" AllowPaging="true"
                                     PageSize='<%# Settings.DefaultPageSize %>'>
                        <ClientSettings AllowDragToGroup="true">
                            <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                        </ClientSettings>
                        <SortingSettings EnableSkinSortStyles="false" />
                        <GroupingSettings CaseSensitive="False" ShowUnGroupButton="true" />
                        <HeaderStyle Wrap="False" />
                        <FilterMenu EnableImageSprites="False" />
                        <MasterTableView AutoGenerateColumns="False" DataKeyNames="OrderID" CommandItemDisplay="Top" EditMode="PopUp">
                            <CommandItemSettings ShowAddNewRecordButton="false" ShowExportToExcelButton="true" />
                            <EditFormSettings UserControlName="CtrlGaugerOrderEdit.ascx" EditFormType="WebUserControl"
                                              CaptionDataField="OrderNum" CaptionFormatString="Order # {0}">
                                <PopUpSettings Modal="true" />
                                <EditColumn UniqueName="EditColumn" />
                            </EditFormSettings>
                            <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                <HeaderStyle Width="20px"></HeaderStyle>
                            </RowIndicatorColumn>
                            <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                <HeaderStyle Width="20px"></HeaderStyle>
                            </ExpandCollapseColumn>
                            <Columns>
                                <telerik:GridTemplateColumn UniqueName="SelColumn" HeaderText=" " AllowFiltering="false" Groupable="false" ReadOnly="true">
                                    <HeaderTemplate>
                                        <telerik:RadButton ID="chkAll" runat="server" ToggleType="CustomToggle" ButtonType="ToggleButton" ClientIDMode="Static"
                                                           OnClientToggleStateChanged="chkAll_ToggleStateChanged"
                                                           AutoPostBack="false">
                                            <ToggleStates>
                                                <telerik:RadButtonToggleState Text="" PrimaryIconCssClass="rbToggleCheckbox" Value="false" Selected="true"></telerik:RadButtonToggleState>
                                                <telerik:RadButtonToggleState Text="" PrimaryIconCssClass="rbToggleCheckboxFilled" Value="neither"></telerik:RadButtonToggleState>
                                                <telerik:RadButtonToggleState Text="" PrimaryIconCssClass="rbToggleCheckboxChecked" Value="true"></telerik:RadButtonToggleState>
                                            </ToggleStates>
                                        </telerik:RadButton>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkSel" runat="server" Checked='<%# Eval("Selected") %>' onclick="chkSel_CheckedChanged()" OnClientLoad="chkSel_CheckedChanged()" />
                                    </ItemTemplate>
                                    <ItemStyle BackColor="LightBlue" />
                                    <HeaderStyle Width="30px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Edit" Text="Edit" UniqueName="EditColumn" ImageUrl="~/images/edit.png">
                                    <HeaderStyle Width="25px" />
                                </telerik:GridButtonColumn>
                                <telerik:GridBoundColumn DataField="Status" UniqueName="Status" HeaderText="Gauger Status" SortExpression="Status"
                                                         FilterControlWidth="70%" HeaderStyle-Width="90px" />
                                <telerik:GridBoundColumn DataField="OrderNum" FilterControlWidth="70%" UniqueName="OrderNum" HeaderText="Order #"
                                                         SortExpression="OrderNum" HeaderStyle-Width="90px" />
                                <telerik:GridBoundColumn DataField="Gauger" HeaderText="Gauger" SortExpression="Gauger" FilterControlWidth="70%"
                                                         UniqueName="Gauger" HeaderStyle-Width="150px" />
                                <telerik:GridTemplateColumn DataField="PriorityNum" HeaderText="Priority" SortExpression="PriorityNum"
                                                            FilterControlWidth="70%" UniqueName="PriorityNum" GroupByExpression="PriorityNum GROUP BY PriorityNum">
                                    <ItemTemplate>
                                        <blac:PriorityLabel ID="lblPriority" runat="server" Text='<%# Eval("PriorityNum") %>' Width="100%" BorderStyle="None" style="text-align:center" />
                                    </ItemTemplate>
                                    <HeaderStyle Width="90px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn DataField="DueDate" HeaderText="Due Date" GroupByExpression="DueDate GROUP BY DueDate"
                                                            FilterControlWidth="70%" UniqueName="DueDate" SortExpression="DueDate">
                                    <ItemTemplate>
                                        <asp:Label ID="gridlblDueDate" runat="server" Text='<%# Eval("DueDate", "{0:M/d/yy}") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="80px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn DataField="Product" HeaderText="Product" SortExpression="Product" FilterControlWidth="70%"
                                                            UniqueName="Product" GroupByExpression="Product GROUP BY Product">
                                    <ItemTemplate>
                                        <asp:Label ID="gridlblProduct" runat="server" Text='<%# Eval("ProductShort") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="150px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="DispatchConfirmNum" UniqueName="DispatchConfirmNum" HeaderText="Shipper PO #" FilterControlWidth="70%"
                                                         HeaderStyle-Width="100px" />
                                <telerik:GridBoundColumn DataField="JobNumber" UniqueName="JobNumber" HeaderText="Job #" FilterControlWidth="70%"
                                                         HeaderStyle-Width="100px" />
                                <telerik:GridBoundColumn DataField="OriginStateAbbrev" UniqueName="OriginStateAbbrev" HeaderText="Origin ST" SortExpression="OriginStateAbbrev"
                                                         ForceExtractValue="Always" FilterControlWidth="50%" HeaderStyle-Width="60px" />
                                <telerik:GridTemplateColumn DataField="Origin" HeaderText="Origin" SortExpression="Origin" FilterControlWidth="70%"
                                                            UniqueName="Origin" GroupByExpression="Origin GROUP BY Origin">
                                    <ItemTemplate>
                                        <asp:Label runat="server"
                                                   Text='<%# String.IsNullOrEmpty(Eval("Origin").ToString()) ? "(Not Assigned)" : Eval("Origin") %>'
                                                   OnPreRender="glblOrigin_PreRender" />
                                        <asp:Label ID="lblH2S" runat="server" Text='' CssClass="label label-danger" />
                                    </ItemTemplate>
                                    <HeaderStyle Width="155px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="LeaseNum" FilterControlWidth="70%" UniqueName="LeaseNum" HeaderText="Lease #"
                                                         MaxLength="20" SortExpression="LeaseNum" HeaderStyle-Width="90px" />
                                <telerik:GridBoundColumn DataField="OriginTankText" FilterControlWidth="70%" UniqueName="OriginTankText" HeaderText="Tank ID"
                                                         MaxLength="20" SortExpression="OriginTankText" HeaderStyle-Width="90px" />
                                <telerik:GridBoundColumn DataField="TicketType" FilterControlWidth="70%" UniqueName="TicketType" HeaderText="Ticket Type"
                                                         SortExpression="TicketType" HeaderStyle-Width="120px" />
                                <telerik:GridTemplateColumn DataField="CreateDate" HeaderText="Create Date" UniqueName="CreateDate" SortExpression="CreateDate"
                                                            FilterControlWidth="70%" GroupByExpression="CreateDate GROUP BY CreateDate" ReadOnly="true">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCreateDate" runat="server" Text='<%# Eval("CreateDate", "{0:M/d/yy HH:mm}") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="150px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="CreatedByUser" UniqueName="CreatedByUser" SortExpression="CreatedByUser"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Created By" HeaderStyle-Width="100px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="OrderID" UniqueName="OrderID" Visible="False" Display="False" />
                            </Columns>
                            <PagerStyle AlwaysVisible="True" />
                        </MasterTableView>
                    </telerik:RadGrid>
                </div>
            </div>
            <blac:DBDataSource ID="dsMain" runat="server" SelectCommand="spGaugerOrderCreationSource">
                <SelectParameters>
                    <asp:ControlParameter Name="RegionID" ControlID="rcbRegion" PropertyName="SelectedValue" Type="Int32" DefaultValue="-1" />
                </SelectParameters>
            </blac:DBDataSource>
            <blac:DBDataSource ID="dsRegion" runat="server" SelectCommand="SELECT ID, Name FROM dbo.tblRegion UNION SELECT NULL, '(All Regions)' ORDER BY Name" />
            <blac:DBDataSource ID="dsOrigin" runat="server"
                               SelectCommand="SELECT ID, LeaseNum = isnull(LeaseNum, 'N/A'), Name, LeaseNum_Name = isnull(LeaseNum + ' - ', '') + Name, H2S
                        FROM dbo.viewOrigin
                        WHERE GaugerTicketTypeID IS NOT NULL AND HasTanks = 1 AND Active=1 AND (@RegionID = 0 OR RegionID = @RegionID) AND DeleteDateUTC IS NULL
                        UNION SELECT 0, '(Select Origin)', '(Select Origin)', '(Select Origin)', 0
                        ORDER BY Name, LeaseNum">
                <SelectParameters>
                    <asp:ControlParameter Name="RegionID" ControlID="rcbRegion" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
                </SelectParameters>
            </blac:DBDataSource>
            <blac:DBDataSource ID="dsOriginTank" runat="server"
                               SelectCommand="SELECT ID, OriginID, TankNum, LeaseNum_Name = isnull(OriginLeaseNum+'-'+Origin, Origin) FROM viewOriginTank WHERE Unstrapped = 0 AND ForGauger=1 AND (@OriginID = 0 OR OriginID=@OriginID) AND DeleteDateUTC IS NULL UNION SELECT NULL, 0, '(Select Tank)', '(Select Tank)' ORDER BY TankNum">
                <SelectParameters>
                    <asp:ControlParameter Name="OriginID" ControlID="rcbOrigin" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
                </SelectParameters>
            </blac:DBDataSource>
            <blac:DBDataSource ID="dsProduct" runat="server" SelectCommand="SELECT ID, Name, ShortName FROM dbo.tblProduct WHERE ID IN (SELECT ProductID FROM tblOriginProducts WHERE OriginID = @OriginID) UNION SELECT -1, '(Select Product)', '(Select Product)' ORDER BY Name">
                <SelectParameters>
                    <asp:ControlParameter Name="OriginID" ControlID="rcbOrigin" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
                </SelectParameters>
            </blac:DBDataSource>
            <blac:DBDataSource ID="dsDestination" runat="server"
                               SelectCommand="SELECT ID, Name, FullName, DestinationType FROM dbo.fnRetrieveEligibleDestinations(@OriginID, @ShipperID, @ProductID, 0) WHERE (@RegionID = 0 OR RegionID = @RegionID) UNION SELECT 0, '(Select Destination)', '(Select Destination)', NULL ORDER BY Name">
                <SelectParameters>
                    <asp:ControlParameter Name="RegionID" ControlID="rcbRegion" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
                    <asp:ControlParameter Name="ShipperID" ControlID="rcbShipper" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
                    <asp:ControlParameter Name="ProductID" ControlID="rcbProduct" PropertyName="SelectedValue" Type="Int32" DefaultValue="1" />
                    <asp:ControlParameter Name="OriginID" ControlID="rcbOrigin" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
                </SelectParameters>
            </blac:DBDataSource>
            <blac:DBDataSource ID="dsGauger" runat="server" SelectCommand="SELECT ID, FullName FROM dbo.viewGauger WHERE DeleteDateUTC IS NULL UNION SELECT 0, '(Select Gauger)' ORDER BY FullName" />
            <blac:DBDataSource ID="dsCustomer" runat="server"
                               SelectCommand="SELECT C.ID, C.Name FROM dbo.tblCustomer C JOIN tblOriginCustomers OC ON OC.CustomerID = C.ID WHERE OC.OriginID = @OriginID UNION SELECT NULL, '(Select Shipper)' ORDER BY Name">
                <SelectParameters>
                    <asp:ControlParameter Name="OriginID" ControlID="rcbOrigin" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
                </SelectParameters>
            </blac:DBDataSource>
            <blac:DBDataSource ID="dsTicketType" runat="server" SelectCommand="SELECT ID, Name FROM dbo.tblGaugerTicketType ORDER BY Name" />
            <blac:DBDataSource ID="dsPriority" runat="server" SelectCommand="SELECT ID, PriorityNum FROM tblPriority ORDER BY PriorityNum" />
        </div>
    </div>
</asp:Content>
