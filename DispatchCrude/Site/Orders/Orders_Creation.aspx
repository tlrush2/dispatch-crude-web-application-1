﻿<%@  Page Title="Dispatch" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Orders_Creation.aspx.cs"
    Inherits="DispatchCrude.Site.Orders.Orders_Creation" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<asp:Content ID="contentHeader" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css" >
        .H2S
        {
            background-color: Red !important;
            color: White !important;
        }
        /** rcbDestination Columns */
        .rcbHeader ul,
        .rcbFooter ul,
        .rcbItem ul,
        .rcbHovered ul,
        .rcbDisabled ul {
                margin: 0;
                padding: 0;
                width: 100%;
                display: inline-block;
                list-style-type: none;
        }
        .col
        {
                margin: 0;
                padding: 0 5px 0 0;
                line-height: 14px;
                float: left;
        }
        .otCol1
        {
            width: 80px;
        }
        .otCol2
        {
            width: 120px;
        }
       

        /* 
            This is not the correct way to fix this but Maverick approved a "quick fix" to the create page dropdown CSS. 
            Long origin names were overlapping the lease names.  This will be taken care of correctly in the new MVC template.
        */
        #ctl00_ctl00_MainContent_MainContent_rcbOrigin_DropDown 
        {              
              width: auto !important;
        }       
        .oCol1
        {
              width: 150px !important;              
              text-align: left !important;
              padding-right: 15px !important;
              float: left !important;
        }
        .oCo2 
        {
              width: auto !important;
              text-align: left !important;
              padding-left: 15px !important;
              float: left !important;
        }
        #ctl00_ctl00_MainContent_MainContent_rcbDriver_DropDown {
              min-width: 375px !important;
        }
        /* END  above quick fix*/
    </style>
</asp:Content>
<asp:Content ID="mainContent" ContentPlaceHolderID="MainContent" runat="server">
    <telerik:RadScriptBlock ID="radScriptBlock" runat="server">
        <script type="text/javascript">
            var noUpdate = false;
            function chkAll_ToggleStateChanged(sender, args) {
                // if we are being called from the chkSel_CheckedChanged below, then just exit (return)
                //debugger;
                if (noUpdate)
                    return;
                var value = args.get_currentToggleState().get_value();
                if (value == "neither") {
                    sender.set_selectedToggleStateIndex(2);
                    value = "true";
                }
                var items = $find("<%= rgMain.ClientID %>").get_masterTableView().get_dataItems();
                for (i = 0; i < items.length; i++) {
                    var item = items[i];
                    var chk = item.findElement("chkSel");
                    if (chk != null) {
                        chk.checked = (value == "true");
                    }
                }
                DisableSelectionButtons(value == "false");
            };
            /* this is fired by either the rcbDriver || rcbCarrier controls */
            function comboSelectedIndexChanged(sender, eventArgs) {
                chkSel_CheckedChanged();
            }
            function chkSel_CheckedChanged() {
                var grid = $find("<%= rgMain.ClientID %>");
                var items = grid.get_masterTableView().get_dataItems();
                var checked = 0, unchecked = 0;
                for (i = 0; i < items.length; i++) {
                    var item = items[i];
                    var chk = item.findElement("chkSel");
                    if (chk.checked)
                        checked++;
                    else
                        unchecked++;
                }
                noUpdate = true;
                try {
                    var chkAll = $find("chkAll");
                    if (checked == 0)
                        chkAll.set_selectedToggleStateIndex(0);
                    else if (unchecked == 0)
                        chkAll.set_selectedToggleStateIndex(2);
                    else
                        chkAll.set_selectedToggleStateIndex(1);
                } catch (ex) {
                    debugger;
                    // eat these errors
                }
                noUpdate = false;
                DisableSelectionButtons(checked == 0);
            };
            function DisableSelectionButtons(disabled) {
                $("#" + "<%= cmdDelete.ClientID %>").prop("disabled", disabled);
                if ($find("<%= rcbCarrier.ClientID %>") != null) {
                    var carrierID = $find("<%= rcbCarrier.ClientID %>").get_value();
                    var driverID = $find("<%= rcbDriver.ClientID %>").get_value();
                    $("#" + "<%= cmdAssignCarrier.ClientID %>").prop("disabled", disabled || carrierID <= 0);
                    $("#" + "<%= cmdDispatch.ClientID %>").prop("disabled", disabled || driverID <= 0);
                }
            };
            function contentResponseEnd(sender, args) {
                chkSel_CheckedChanged();
            };
            function onItemDataBound(sender, eventArgs) {
                var item = eventArgs.get_item();
                var dataItem = eventArgs.get_dataItem();
                if (item.get_text().startsWith("H2S-")) {
                    $(item).addClass("H2S");
                }
            };
            function onClientDropDownOpening(sender, eventArgs) {
                sender.set_text("");
            };
            // main startup script
            $(function () {
                //debugger;
                // ensure all buttons are initially disabled/enabled appropriately
                //chkSel_CheckedChanged();
            });
        </script>
    </telerik:RadScriptBlock>
    <script>
        $("#ctl00_ctl00_EntityCaption").html("Truck Order Creation");
    </script>
    <div class="tabbable TabRepaint">
        <ul class="nav nav-tabs" id="tabmenu" runat="server">
            <!--Tabs will print here (see code behind)-->
        </ul>
        <div class="tab-content">
            <div class="tab-pane in active">

                <div id="speedbuttons" class="well speedButtonContainer" runat="server">
                    <!--Buttons will print here (see code behind)-->        
                </div>

                <div class="leftpanel" style="overflow-y:scroll;">
                    <div class="tabbable">
                        <ul class="nav nav-tabs">
                            <li class="active tab-blue">
                                <a data-toggle="tab" href="#Create" aria-expanded="true">Create</a>
                            </li>
                            <li class="tab-yellow">
                                <a data-toggle="tab" href="#OrderActions" aria-expanded="true">Order Actions</a>
                            </li>
                        </ul>
                        <div id="leftTabs" class="tab-content">
                            <div id="Create" class="tab-pane active">
                                <asp:Panel ID="panelGenerate" runat="server" DefaultButton="cmdCreateLoads">
                                    <div class="Entry">
                                        <asp:Label ID="lblRegion" runat="server" Text="Region" AssociatedControlID="rcbRegion" CssClass="Entry" />
                                        <telerik:RadComboBox Width="100%" runat="server" ID="rcbRegion"
                                                             DataSourceID="dsRegion" DataTextField="Name" DataValueField="ID"
                                                             AutoPostBack="true" OnSelectedIndexChanged="rcbRegion_SelectedIndexChanged" CausesValidation="false" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblTerminal" runat="server" Text="Terminal" AssociatedControlID="rcbTerminal" CssClass="Entry" />
                                        <telerik:RadComboBox Width="100%" runat="server" ID="rcbTerminal"
                                                             DataSourceID="dsTerminal" DataTextField="Name" DataValueField="ID"
                                                             AutoPostBack="true" OnSelectedIndexChanged="rcbTerminal_SelectedIndexChanged" CausesValidation="false" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblOrigin" runat="server" Text="Origin" AssociatedControlID="rcbOrigin" CssClass="Entry" />
                                        <telerik:RadComboBox runat="server" ID="rcbOrigin" AllowCustomText="false" Filter="Contains"
                                                             DataSourceID="dsOrigin" DataTextField="LeaseNum_Name" DataValueField="ID" CausesValidation="false"
                                                             AutoPostBack="true" OnSelectedIndexChanged="rcbOrigin_SelectedIndexChanged" OnItemDataBound="rcbOrigin_ItemDataBound"
                                                             OnClientItemDataBound="onItemDataBound" OnClientDropDownOpening="onClientDropDownOpening"
                                                             Width="100%" DropDownAutoWidth="Enabled" EmptyMessage="(Select Origin)"
                                                             EnableAutomaticLoadOnDemand="true" ShowMoreResultsBox="true" EnableVirtualScrolling="true" ItemsPerRequest="100" EnableItemCaching="false">
                                            <HeaderTemplate>
                                                <ul>
                                                    <li class="col oCol1">Lease #</li>
                                                    <li class="col oCol2">Name</li>
                                                </ul>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <ul>
                                                    <li class="col oCol1">
                                                        <%# DataBinder.Eval(Container.DataItem, "LeaseNum") %>
                                                    </li>
                                                    <li class="col oCol2">
                                                        <%# DataBinder.Eval(Container.DataItem, "Name") %>
                                                    </li>
                                                </ul>
                                            </ItemTemplate>
                                        </telerik:RadComboBox>
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblProducer" runat="server" Text="Producer" />
                                        <asp:TextBox Width="98%" ID="txtProducerName" runat="server" Enabled="false" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblCustomer" runat="server" Text="Shipper" AssociatedControlID="rcbCustomer" />
                                        <telerik:RadComboBox Width="100%" runat="server" ID="rcbCustomer"
                                                             DataSourceID="dsCustomer" DataTextField="Name" DataValueField="ID" CausesValidation="false"
                                                             AutoPostBack="true" OnSelectedIndexChanged="entryChanged" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblProduct" runat="server" Text="Product" AssociatedControlID="rcbProduct" CssClass="Entry" />
                                        <telerik:RadComboBox Width="100%" runat="server" ID="rcbProduct"
                                                             DataSourceID="dsProduct" DataTextField="Name" DataValueField="ID" CausesValidation="false"
                                                             AutoPostBack="true" OnSelectedIndexChanged="rcbProduct_SelectedIndexChanged" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblDestination" runat="server" Text="Destination + (Station)" AssociatedControlID="rcbDestination" />

                                        <telerik:RadComboBox runat="server" ID="rcbDestination"
                                                             DataSourceID="dsDestinationDropdown" DataTextField="Station_Name" DataValueField="ID" CausesValidation="false"
                                                             AutoPostBack="true" OnSelectedIndexChanged="rcbDestination_SelectedIndexChanged"
                                                             Width="100%" DropDownWidth="280px">
                                            <HeaderTemplate>
                                                <ul>
                                                    <li class="col dCol1">Destination</li>
                                                </ul>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <ul>
                                                    <li class="col dCol1">
                                                        <%# DataBinder.Eval(Container.DataItem, "Station_Name") %>
                                                    </li>
                                                </ul>
                                            </ItemTemplate>
                                        </telerik:RadComboBox>

                                    </div>                                    
                                    <div class="Entry">
                                        <asp:Label ID="lblConsignee" runat="server" Text="Consignee" AssociatedControlID="rcbConsignee" CssClass="Entry" />
                                        <telerik:RadComboBox Width="100%" runat="server" ID="rcbConsignee"
                                                             DataSourceID="dsConsignee" DataTextField="Name" DataValueField="ID" CausesValidation="false"
                                                             AutoPostBack="true" OnSelectedIndexChanged="entryChanged" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblRateMiles" runat="server" Text="Rate Miles" />
                                        <asp:TextBox Width="98%" ID="txtRateMiles" runat="server" Enabled="false" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblTicketType" runat="server" Text="Ticket Type" AssociatedControlID="rcbTicketType" />
                                        <telerik:RadComboBox Width="100%" runat="server" ID="rcbTicketType"
                                                             DataSourceID="dsTicketType" DataTextField="TicketType" DataValueField="ID" CausesValidation="false"
                                                             AutoPostBack="true" OnSelectedIndexChanged="rcbTicketType_SelectedIndexChanged" />
                                    </div>
                                    <asp:Panel ID="panelTankInfo" runat="server" Visible="false" CssClass="Entry" Width="100%" Height="45px">
                                        <div class="floatLeft">
                                            <asp:Label runat="server" Text="Tank ID" AssociatedControlID="rcbOriginTank" CssClass="Entry" />
                                            <telerik:RadComboBox id="rcbOriginTank" runat="server" Width="100%"
                                                                 DataSourceID="dsOriginTank" DataTextField="TankNum" DataValueField="ID" CausesValidation="false"
                                                                 AutoPostBack="true" OnSelectedIndexChanged="rcbOriginTank_SelectedIndexChanged"
                                                                 AllowCustomText="false" />
                                        </div>
                                    </asp:Panel>
                                    <asp:Panel ID="panelBOLInfo" runat="server" Visible="false" Width="100%" Height="45px" CssClass="Entry">
                                        <asp:Label ID="lblOriginBOLNum" runat="server" Text="BOL #" AssociatedControlID="txtOriginBOLNum" />
                                        <asp:TextBox Width="100%" runat="server" ID="txtOriginBOLNum" MaxLength="15" CausesValidation="false" />
                                    </asp:Panel>
                                    <div class="clear"></div>
                                    <div class="Entry">
                                        <asp:Label ID="lblCarrier" runat="server" Text="Carrier&nbsp;" AssociatedControlID="rcbCarrier" />
                                        <asp:Label runat="server" Text="(also used for 'Assign' button)" BackColor="LightBlue" />
                                        <telerik:RadComboBox Width="100%" ID="rcbCarrier" runat="server" AllowCustomText="false" Filter="Contains"
                                                             DataSourceID="dsCarrier" DataTextField="Name" DataValueField="ID" CausesValidation="false"
                                                             OnClientSelectedIndexChanged="comboSelectedIndexChanged"
                                                             AutoPostBack="true" OnSelectedIndexChanged="rcbCarrier_SelectedIndexChanged" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblDriver" runat="server" Text="Driver&nbsp;" AssociatedControlID="rcbDriver" />
                                        <asp:Label runat="server" Text="(also used for 'Dispatch' button)" BackColor="LightBlue" />
                                        <telerik:RadComboBox Width="100%" ID="rcbDriver" runat="server" AllowCustomText="false" Filter="Contains"
                                                             DataSourceID="dsDriver" DataTextField="FullName" DataValueField="ID" CausesValidation="false"
                                                             OnClientSelectedIndexChanged="comboSelectedIndexChanged"
                                                             AutoPostBack="true" OnItemDataBound="rcbDriver_ItemDataBound" OnSelectedIndexChanged="rcbDriver_SelectedIndexChanged">
                                            <HeaderTemplate>
                                                <ul>
                                                    <li class="col drCol1" style="width:60% !important;text-align: left !important;float: left !important;">Name</li>
                                                    <!--<li class="col drCol2" style="width:30% !important;text-align: left !important; float: left !important;">Carrier</li>-->
                                                    <li class="col drCol3" style="width:40% !important;text-align: center !important; float: right !important;">HOS Hours</li>
                                                </ul>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <ul>
                                                    <li class="col drCol1" style="width:60% !important;text-align: left !important;float: left !important;">
                                                        <%# DataBinder.Eval(Container.DataItem, "FullName") %>
                                                    </li>
                                                    <!--
                                                    <li class="col drCol2" style="width:30% !important;text-align: left !important; float: left !important;">
                                                        <small><%# DataBinder.Eval(Container.DataItem, "Carrier") %></small>
                                                    </li>-->
                                                    <li class="col drCol3" style="width:40% !important; text-align: right !important; float: right !important;">
                                                        <small title="Driving / On Duty / Weekly On Duty"><%# DataBinder.Eval(Container.DataItem, "HosHrs") %></small>
                                                    </li>
                                                </ul>
                                            </ItemTemplate>
                                        </telerik:RadComboBox>
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblDispatchConfirmNum" runat="server" Text="Shipper PO #" AssociatedControlID="txtDispatchConfirmNum" />
                                        <asp:TextBox Width="98%" ID="txtDispatchConfirmNum" runat="server" CausesValidation="false"
                                                     AutoPostBack="true" OnTextChanged="txtDispatchConfirmNum_TextChanged" />
                                        <asp:RequiredFieldValidator ID="rfvDispatchConfirmNum" runat="server"
                                                                    ControlToValidate="txtDispatchConfirmNum" InitialValue="" Enabled="false" Display="Dynamic"
                                                                    Text="Shipper PO is required" CssClass="NullValidator" />
                                        <asp:CustomValidator ID="cvDispatchConfirmNum" runat="server" ControlToValidate="txtDispatchConfirmNum"
                                                             OnServerValidate="cvDispatchConfirmNum_ServerValidate" Display="Dynamic"
                                                             Text="Shipper PO already in use for this Shipper" CssClass="NullValidator" />
                                    </div>
                                    <div>
                                        <div class="Entry inline-block" style="width:49%;">
                                            <asp:Label ID="lblJobNumber" runat="server" Text="Job #" AssociatedControlID="txtJobNumber" /><br />
                                            <asp:TextBox Width="98%" ID="txtJobNumber" runat="server" CausesValidation="false" />
                                        </div>
                                        <div class="Entry inline-block" style="width:49%;">
                                            <asp:Label ID="lblContractNumber" runat="server" Text="Contract #" AssociatedControlID="txtContractNumber" /><br />
                                            <asp:TextBox Width="98%" ID="txtContractNumber" runat="server" Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblDispatchNotes" runat="server" Text="Dispatch Notes" AssociatedControlID="txtDispatchNotes" />
                                        <asp:TextBox id="txtDispatchNotes" runat="server" Width="98%" Height="75px" MaxLength="500" TextMode="MultiLine"
                                                     Text="">
                                        </asp:TextBox>
                                    </div>
                                    <asp:Table ID="tableDateQty" runat="server" Width="100%">
                                        <asp:TableRow ID="tableRow1" runat="server">
                                            <asp:TableCell>
                                                <asp:Label ID="lblPriority" runat="server" Text="Priority" /><br />
                                                <blac:PriorityDropDownList CssClass="btn-xs" ID="ddlPriority" runat="server" Width="60px" CausesValidation="false" />
                                            </asp:TableCell><asp:TableCell>
                                                <asp:Label ID="lblDueDate" runat="server" Text="Due Date" /><br />
                                                <telerik:RadDatePicker ID="rdpDueDate" runat="server" DateInput-DateFormat="M/d/yy" Width="92px" AutoPostback="true" OnSelectedDateChanged="rdpDueDate_Changed" />
                                            </asp:TableCell><asp:TableCell>
                                                <asp:Label ID="lblLoadQty" runat="server" Text="Qty" /><br />
                                                <telerik:RadNumericTextBox DataType="System.Int32" ID="rntxtQty" runat="server" Value="1" MinValue="1" MaxValue="10000"
                                                                           ShowSpinButtons="True" Width="50px" CausesValidation="false">
                                                    <NumberFormat ZeroPattern="n" DecimalDigits="0" />
                                                    <EnabledStyle HorizontalAlign="Right" />
                                                </telerik:RadNumericTextBox>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow ID="tableRow2" runat="server">
                                            <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                                <div class="spacer10px"></div>
                                                <asp:Button ID="cmdCreateLoads" Text="Generate" runat="server" CssClass="btn btn-blue shiny"
                                                            OnClick="cmdCreateLoads_Click" Enabled="False" CausesValidation="true" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:Panel>
                            </div>
                            <div id="OrderActions" class="tab-pane">
                                <p><i>These actions apply to any currently selected orders in the grid.</i></p><p><b>Assign</b> - Assigns selected loads to the carrier specified on the Create tab.</p><p><b>Dispatch</b> - Dispatches selected loads to the driver specified on the Create tab.</p><div class="spacer10px"></div>
                                <asp:Panel ID="panelSelectedCtrl" runat="server">
                                    <div class="center">
                                        <asp:Button ID="cmdDelete" runat="server" Text="Delete" CssClass="btn btn-blue shiny" OnClick="cmdDelete_Click" />
                                        &nbsp; <asp:Button ID="cmdDispatch" runat="server" Text="Dispatch" CssClass="btn btn-blue shiny" OnClick="cmdDispatch_Click" />
                                        &nbsp; <asp:Button ID="cmdAssignCarrier" runat="server" Text="Assign" CssClass="btn btn-blue shiny" OnClick="cmdAssignCarrier_Click" />
                                    </div>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="gridArea" style="height: 100%; min-height: 500px;">
                    <telerik:RadPersistenceManager ID="rpmMain" runat="server">
                        <PersistenceSettings>
                            <telerik:PersistenceSetting ControlID="rgMain" />
                        </PersistenceSettings>
                    </telerik:RadPersistenceManager>
                    <telerik:RadWindowManager ID="radWindowManager" runat="server" EnableShadow="true" />
                    <telerik:RadGrid ID="rgMain" runat="server" EnableHeaderContextMenu="true" CellSpacing="0" GridLines="None" ShowStatusBar="false" EnableEmbeddedSkins="true" Skin="Vista" AlternatingItemStyle-BackColor="#dcf2fc"
                                     AllowSorting="True" AllowFilteringByColumn="true" Height="800" CssClass="GridRepaint" ShowGroupPanel="false" EnableLinqExpressions="false"
                                     DataSourceID="dsMain" OnItemCreated="grid_ItemCreated" OnItemDataBound="grid_ItemDataBound" OnItemCommand="grid_ItemCommand" AllowAutomaticUpdates="false"
                                     AllowPaging="true" PageSize='<%# Settings.DefaultPageSize %>'>
                        <ClientSettings AllowDragToGroup="false" Resizing-AllowColumnResize="true" Resizing-AllowResizeToFit="true" AllowColumnHide="true" AllowColumnsReorder="true">
                            <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                        </ClientSettings>
                        <SortingSettings EnableSkinSortStyles="false" />
                        <GroupingSettings CaseSensitive="False" ShowUnGroupButton="true" />
                        <HeaderStyle Wrap="False" />
                        <FilterMenu EnableImageSprites="False" />
                        <MasterTableView AutoGenerateColumns="False" DataKeyNames="ID" CommandItemDisplay="Top" EditMode="PopUp" GroupsDefaultExpanded="false">
                            <CommandItemTemplate>
                                <div style="padding: 5px;">
                                    <asp:LinkButton ID="cmdExportExcel" Style="vertical-align: middle" runat="server"
                                                    CommandName="ExportToExcel" CssClass="pull-right NOAJAX">
                                        <img style="border:0px" alt="" src="../../images/exportToExcel.png" />
                                        Excel Export &nbsp;&nbsp;
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnRefresh" Text="Refresh" CommandName="Refresh" CssClass="pull-right" runat="server">Refresh&nbsp;&nbsp;|&nbsp;&nbsp;</asp:LinkButton>
                                    <asp:Button runat="server" ID="buttonRefresh" CommandName="Refresh" CssClass="rgRefresh pull-right" Text="Refresh" Title="Refresh" />

                                    <div style="width:10px" class="pull-right">&nbsp;</div>

                                    <asp:LinkButton ID="cmdResetLayout" Style="vertical-align: middle" runat="server"
                                                    OnClick="cmdResetLayout_Click" Enabled="true" CssClass="pull-right">
                                        <img style="border:0px" alt="" src="../../images/wrench_orange.png" />
                                        Reset Layout
                                    </asp:LinkButton><div style="width:10px" class="pull-right">&nbsp;</div><asp:LinkButton ID="cmdSaveLayout" Style="vertical-align: middle" runat="server"
                                                                                                                            OnClick="cmdSaveLayout_Click" Enabled="true" CssClass="pull-right">
                                        <img style="border:0px" alt="" src="../../images/wrench_orange.png" />
                                        Save Layout
                                    </asp:LinkButton>
                                </div>
                            </CommandItemTemplate><EditFormSettings UserControlName="CtrlOrderEdit.ascx" EditFormType="WebUserControl"
                                                                    CaptionDataField="OrderNum" CaptionFormatString="Order # {0}">
                                <PopUpSettings Modal="true" />
                                <EditColumn UniqueName="EditColumn" />
                            </EditFormSettings>
                            <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                <HeaderStyle Width="20px"></HeaderStyle>
                            </RowIndicatorColumn>
                            <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                <HeaderStyle Width="20px"></HeaderStyle>
                            </ExpandCollapseColumn>
                            <Columns>
                                <telerik:GridTemplateColumn UniqueName="SelColumn" HeaderText=" " AllowFiltering="false" Groupable="false" ReadOnly="true">
                                    <HeaderTemplate>
                                        <telerik:RadButton ID="chkAll" runat="server" ClientIDMode="Static" ToggleType="CustomToggle" ButtonType="ToggleButton"
                                                           OnClientToggleStateChanged="chkAll_ToggleStateChanged"
                                                           AutoPostBack="false">
                                            <ToggleStates>
                                                <telerik:RadButtonToggleState Text="" PrimaryIconCssClass="rbToggleCheckbox" Value="false" Selected="true"></telerik:RadButtonToggleState>
                                                <telerik:RadButtonToggleState Text="" PrimaryIconCssClass="rbToggleCheckboxFilled" Value="neither"></telerik:RadButtonToggleState>
                                                <telerik:RadButtonToggleState Text="" PrimaryIconCssClass="rbToggleCheckboxChecked" Value="true"></telerik:RadButtonToggleState>
                                            </ToggleStates>
                                        </telerik:RadButton>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkSel" runat="server" Checked='<%# Eval("Selected") %>' onclick="chkSel_CheckedChanged()" OnClientLoad="chkSel_CheckedChanged()" />
                                    </ItemTemplate>
                                    <ItemStyle BackColor="LightBlue" />
                                    <HeaderStyle Width="30px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Edit" Text="Edit" UniqueName="EditColumn" ImageUrl="~/images/edit.png">
                                    <HeaderStyle Width="25px" />
                                </telerik:GridButtonColumn>
                                <telerik:GridTemplateColumn EditFormColumnIndex="0" DataField="OrderStatus" UniqueName="OrderStatusID"
                                                            HeaderText="Order Status" SortExpression="OrderStatus" GroupByExpression="OrderStatus GROUP BY OrderStatus"
                                                            FilterControlWidth="70%" EditFormHeaderTextFormat="! / Status:">
                                    <ItemTemplate>
                                        <asp:Label ID="gridlblStatus" runat="server" Text='<%# Eval("OrderStatus") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="90px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="OrderNum" FilterControlWidth="70%" UniqueName="OrderNum" HeaderText="Order #" ReadOnly="true"
                                                         SortExpression="OrderNum" GroupByExpression="OrderNum GROUP BY OrderNum">
                                    <HeaderStyle Width="90px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn DataField="PriorityNum" HeaderText="Priority" SortExpression="PriorityID"
                                                            FilterControlWidth="70%" EditFormColumnIndex="0"
                                                            UniqueName="PriorityID" GroupByExpression="PriorityNum GROUP BY PriorityNum" ReadOnly="true">
                                    <ItemTemplate>
                                        <blac:PriorityLabel ID="lblPriority" runat="server" Text='<%# Eval("PriorityNum") %>' Width="100%" BorderStyle="None" style="text-align:center" />
                                    </ItemTemplate>
                                    <HeaderStyle Width="90px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn DataField="ECOT_Minutes" HeaderText="ECOT" DataType="System.Decimal" FilterControlWidth="60%"
                                                            UniqueName="ECOT_Minutes" SortExpression="ECOT_Minutes" EditFormColumnIndex="2">
                                    <ItemTemplate>
                                        <asp:Label ID="lblECOT_Minutes" runat="server" Text='<%# DateHelper.FormatDurationMinutes(Eval("ECOT_Minutes")) %>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="80px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn DataField="DueDate" HeaderText="Due Date" DataType="System.DateTime" GroupByExpression="DueDate GROUP BY DueDate"
                                                            FilterControlWidth="70%" UniqueName="DueDate" SortExpression="DueDate" EditFormColumnIndex="2">
                                    <ItemTemplate>
                                        <asp:Label ID="gridlblDueDate" runat="server" Text='<%# Eval("DueDate", "{0:M/d/yy}") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="80px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn DataField="ProductShort" HeaderText="Product" SortExpression="ProductShort" DataType="System.String" FilterControlWidth="70%"
                                                            UniqueName="ProductID" EditFormColumnIndex="1" GroupByExpression="ProductShort GROUP BY ProductShort">
                                    <ItemTemplate>
                                        <asp:Label ID="gridlblProduct" runat="server" Text='<%# Eval("ProductShort") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="150px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="DispatchConfirmNum" UniqueName="DispatchConfirmNum" HeaderText="Shipper PO #" FilterControlWidth="70%"
                                                         HeaderStyle-Width="100px" />
                                <telerik:GridBoundColumn DataField="JobNumber" UniqueName="JobNumber" HeaderText="Job #" FilterControlWidth="70%"
                                                         HeaderStyle-Width="100px" />
                                <telerik:GridBoundColumn DataField="ContractNumber" UniqueName="ContractNumber" HeaderText="Contract #" FilterControlWidth="70%"
                                                         HeaderStyle-Width="100px" />
                                <telerik:GridTemplateColumn DataField="Carrier" HeaderText="Carrier" SortExpression="Carrier" DataType="System.String" FilterControlWidth="70%"
                                                            UniqueName="CarrierID" EditFormColumnIndex="1" GroupByExpression="Carrier GROUP BY Carrier">
                                    <ItemTemplate>
                                        <asp:Label ID="gridlblCarrier" runat="server" Text='<%# Eval("Carrier") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="150px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="OriginStateAbbrev" UniqueName="OriginStateAbbrev" HeaderText="Origin ST" SortExpression="OriginStateAbbrev"
                                                         ReadOnly="true" ForceExtractValue="Always" FilterControlWidth="50%" GroupByExpression="OriginStateAbbrev GROUP BY OriginStateAbbrev">
                                    <HeaderStyle Width="70px" />
                                </telerik:GridBoundColumn>                                
                                <telerik:GridTemplateColumn DataField="Origin" HeaderText="Origin" SortExpression="Origin" DataType="System.String" FilterControlWidth="70%"
                                                            UniqueName="OriginID" EditFormColumnIndex="0" GroupByExpression="Origin GROUP BY Origin">
                                    <ItemTemplate>
                                        <asp:Label ID="glblOrigin" runat="server"
                                                   Text='<%# String.IsNullOrEmpty(Eval("Origin").ToString()) ? "(Not Assigned)" : Eval("Origin") %>'
                                                   OnPreRender="glblOrigin_PreRender" />
                                        <asp:Label ID="lblH2S" runat="server" Text='' CssClass="label label-danger" />
                                    </ItemTemplate>
                                    <HeaderStyle Width="250px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="OriginTerminal" UniqueName="OriginTerminal" HeaderText="Origin Terminal" SortExpression="OriginTerminal"
                                                         ReadOnly="true" ForceExtractValue="Always" FilterControlWidth="50%" GroupByExpression="OriginTerminal GROUP BY OriginTerminal">
                                    <HeaderStyle Width="150px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Operator" FilterControlWidth="70%" UniqueName="Operator" HeaderText="Operator"
                                                         SortExpression="Operator">
                                    <HeaderStyle Width="200px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Producer" FilterControlWidth="70%" UniqueName="Producer" HeaderText="Producer"
                                                         SortExpression="Producer">
                                    <HeaderStyle Width="200px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="LeaseNum" FilterControlWidth="70%" UniqueName="LeaseNum" HeaderText="Lease #"
                                                         MaxLength="20" SortExpression="LeaseNum">
                                    <HeaderStyle Width="90px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="OriginTankID_Text" FilterControlWidth="70%" UniqueName="OriginTankID_Text" HeaderText="Tank ID"
                                                         MaxLength="20" SortExpression="OriginTankID_Text">
                                    <HeaderStyle Width="90px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="OriginBOLNum" FilterControlWidth="70%" UniqueName="OriginBOLNum" HeaderText="BOL #"
                                                         MaxLength="15" SortExpression="OriginBOLNum" EditFormColumnIndex="3">
                                    <HeaderStyle Width="90px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn DataField="OriginDrivingDirections" HeaderText="Origin DD" SortExpression="OriginDrivingDirections"
                                                            UniqueName="OriginDrivingDirections" DataType="System.String" FilterControlWidth="70%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblOriginDrivingDirections" runat="server" data-toggle="tooltip"
                                                   class='<%# String.IsNullOrEmpty(Eval("OriginDrivingDirections").ToString()) ? "" : "center glyphicon glyphicon-list-alt" %>'
                                                   title='<%# String.IsNullOrEmpty(Eval("OriginDrivingDirections").ToString()) ? "" : Eval("OriginDrivingDirections") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="80px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn DataField="Destination" HeaderText="Destination" GroupByExpression="Destination GROUP BY Destination"
                                                            SortExpression="Destination" FilterControlWidth="70%" UniqueName="DestinationID" EditFormColumnIndex="0">
                                    <ItemTemplate>
                                        <asp:Label ID="glblDestination" runat="server" Text='<%# String.IsNullOrEmpty(Eval("Destination").ToString()) ? "(Not Assigned)" : Eval("Destination") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="200px" />                                    
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="DestTerminal" UniqueName="DestTerminal" HeaderText="Destination Terminal" SortExpression="DestTerminal"
                                                         ReadOnly="true" ForceExtractValue="Always" FilterControlWidth="50%" GroupByExpression="DestTerminal GROUP BY DestTerminal">
                                    <HeaderStyle Width="150px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn DataField="DestinationStation" HeaderText="Destination Station" GroupByExpression="DestinationStation GROUP BY DestinationStation"
                                                            SortExpression="DestinationStation" FilterControlWidth="70%" UniqueName="DestinationStation" EditFormColumnIndex="0">
                                    <ItemTemplate>
                                        <asp:Label ID="glblDestinationStation" runat="server" Text='<%# String.IsNullOrEmpty(Eval("DestinationStation").ToString()) ? "(Not Assigned)" : Eval("DestinationStation") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="150px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn DataField="DestinationDrivingDirections" HeaderText="Dest DD" SortExpression="DestinationDrivingDirections"
                                                            UniqueName="DestinationDrivingDirections" DataType="System.String" FilterControlWidth="70%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDestinationDrivingDirections" runat="server" data-toggle="tooltip"
                                                   class='<%# String.IsNullOrEmpty(Eval("DestinationDrivingDirections").ToString()) ? "" : "center glyphicon glyphicon-list-alt" %>'
                                                   title='<%# String.IsNullOrEmpty(Eval("DestinationDrivingDirections").ToString()) ? "" : Eval("DestinationDrivingDirections") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="80px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="Customer" FilterControlWidth="70%" UniqueName="CustomerID" HeaderText="Shipper"
                                                         SortExpression="Customer Group BY Customer">
                                    <HeaderStyle Width="250px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn DataField="Consignee" FilterControlWidth="70%" UniqueName="ConsigneeID" HeaderText="Consignee"
                                                            SortExpression="Consignee" EditFormColumnIndex="2" GroupByExpression="Consignee GROUP BY Consignee">
                                    <ItemTemplate>
                                        <asp:Label ID="lblConsignee" runat="server" Text='<%# Eval("Consignee") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="100px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn DataField="TicketType" FilterControlWidth="70%" UniqueName="TicketTypeID" HeaderText="Ticket Type"
                                                            SortExpression="TicketType" EditFormColumnIndex="2" GroupByExpression="TicketType GROUP BY TicketType">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTicketType" runat="server" Text='<%# Eval("TicketType") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="100px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="DispatchNotes" FilterControlWidth="70%" UniqueName="DispatchNotes" HeaderText="Dispatch Notes"
                                                         SortExpression="DispatchNotes">
                                    <HeaderStyle Width="250px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn DataField="CreateDate" HeaderText="Create Date" UniqueName="CreateDate" SortExpression="CreateDate"
                                                            FilterControlWidth="70%" GroupByExpression="CreateDate GROUP BY CreateDate" ReadOnly="true">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCreateDate" runat="server" Text='<%# Eval("CreateDate", "{0:M/d/yy HH:mm}") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle Width="150px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="CreatedByUser" UniqueName="CreatedByUser" SortExpression="CreatedByUser"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Created By" HeaderStyle-Width="100px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                            </Columns>
                            <PagerStyle AlwaysVisible="True" />
                        </MasterTableView>
                    </telerik:RadGrid>
                </div>

                <blc:RadGridDBCtrl ID="dbcMain" runat="server" ControlID="rgMain" FilterActiveEntities="false" />
                <blac:DBDataSource ID="dsMain" runat="server" SelectCommand="spOrderCreationSource">
                    <SelectParameters>
                        <asp:ControlParameter Name="TerminalID" ControlID="rcbTerminal" PropertyName="SelectedValue" Type="Int32" DefaultValue="-1" />
                        <asp:ControlParameter Name="RegionID" ControlID="rcbRegion" PropertyName="SelectedValue" Type="Int32" DefaultValue="-1" />
                    </SelectParameters>
                </blac:DBDataSource>

                <blac:DBDataSource ID="dsState" runat="server" SelectCommand="SELECT ID, FullName FROM dbo.tblState UNION SELECT NULL, '(Select State)' ORDER BY FullName" />

                <blac:DBDataSource ID="dsTerminal" runat="server" SelectCommand="SELECT ID, Name FROM dbo.tblTerminal WHERE DeleteDateUTC IS NULL UNION SELECT -1, '(All Terminals)' ORDER BY Name" />

                <blac:DBDataSource ID="dsRegion" runat="server" SelectCommand="SELECT ID, Name FROM dbo.tblRegion UNION SELECT -1, '(All Regions)' ORDER BY Name" />

                <blac:DBDataSource ID="dsOrigin" runat="server"
                    SelectCommand="SELECT ID, LeaseNum = isnull(LeaseNum, 'N/A'), Name, LeaseNum_Name = isnull(LeaseNum + ' - ', '') + Name, H2S
                        FROM dbo.viewOrigin
                        WHERE Active=1 AND GaugerRequired=0 
                            AND (@RegionID=-1 OR RegionID IS NULL OR RegionID = @RegionID) 
                            AND (@TerminalID=-1 OR TerminalID IS NULL OR TerminalID = @TerminalID) 
                            AND DeleteDateUTC IS NULL
                        ORDER BY Name, LeaseNum">
                    <SelectParameters>
                        <asp:ControlParameter Name="RegionID" ControlID="rcbRegion" PropertyName="SelectedValue" Type="Int32" DefaultValue="-1" />
                        <asp:ControlParameter Name="TerminalID" ControlID="rcbTerminal" PropertyName="SelectedValue" Type="Int32" DefaultValue="-1" />
                    </SelectParameters>
                </blac:DBDataSource>

                <blac:DBDataSource ID="dsDestinationDropdown" runat="server"
                    SelectCommand="SELECT D.ID, Station_Name = (D.Name + ' ' + isnull('(' + D.Station + ')', '')), D.Name
                        FROM viewDestination D
	                        JOIN viewCustomerOriginDestination COD ON COD.DestinationID = D.ID
	                        LEFT JOIN tblDestinationProducts DP ON DP.DestinationID = D.ID
	                        LEFT JOIN tblDestinationCustomers DC ON DC.DestinationID = D.ID
	                        LEFT JOIN tblOrigin O ON O.id = coD.OriginID --Added to gain access to origin region 7/11/16
                        WHERE Active=1
	                        AND O.ID = @OriginID
	                        AND (@ProductID IS NULL OR DP.ProductID = @ProductID)
	                        AND (@ShipperID IS NULL OR DC.CustomerID = @ShipperID)
	                        AND (UPPER((SELECT Value FROM tblSetting WHERE ID = 61)) = 'FALSE' OR D.RegionID = O.RegionID)
                            AND (@TerminalID=-1 OR D.TerminalID IS NULL OR D.TerminalID = @TerminalID)
                            AND (O.TerminalID IS NULL OR D.TerminalID = O.TerminalID OR D.TerminalID IS NULL)
	                        AND D.DeleteDateUTC IS NULL
                        UNION SELECT 0, '(Select Destination)', NULL
                        ORDER BY D.Name">
                    <SelectParameters>
                        <asp:ControlParameter Name="TerminalID" ControlID="rcbTerminal" PropertyName="SelectedValue" Type="Int32" DefaultValue="-1" />
                        <asp:ControlParameter Name="RegionID" ControlID="rcbRegion" PropertyName="SelectedValue" Type="Int32" DefaultValue="-1" />
                        <asp:ControlParameter Name="ProductID" ControlID="rcbProduct" PropertyName="SelectedValue" Type="Int32" DefaultValue="-1" />
                        <asp:ControlParameter Name="ShipperID" ControlID="rcbCustomer" PropertyName="SelectedValue" Type="Int32" DefaultValue="-1" />
                        <asp:ControlParameter Name="OriginID" ControlID="rcbOrigin" PropertyName="SelectedValue" Type="Int32" DefaultValue="-1" />
                    </SelectParameters>
                </blac:DBDataSource>

                <blac:DBDataSource ID="dsOriginTank" runat="server"
                    SelectCommand="SELECT ID, TankNum_Unstrapped AS TankNum, 
                                    CASE WHEN TankNum='*' THEN 1 ELSE 0 END AS SortNum 
                                   FROM viewOriginTank 
                                   WHERE OriginID=@OriginID 
                                   AND DeleteDateUTC IS NULL 
                                   UNION SELECT NULL, '(Select Tank)', 0 ORDER BY SortNum, TankNum">
                    <SelectParameters>
                        <asp:ControlParameter Name="OriginID" ControlID="rcbOrigin" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
                    </SelectParameters>
                </blac:DBDataSource>

                <blac:DBDataSource ID="dsProduct" runat="server" 
                    SelectCommand="SELECT ID, Name, ShortName 
                                   FROM dbo.tblProduct 
                                   WHERE ID IN (SELECT ProductID FROM tblOriginProducts WHERE OriginID = @OriginID) 
                                   UNION SELECT -1, '(Select Product)', '(Select Product)' ORDER BY Name">
                    <SelectParameters>
                        <asp:ControlParameter Name="OriginID" ControlID="rcbOrigin" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
                    </SelectParameters>
                </blac:DBDataSource>

                <blac:DBDataSource ID="dsDestination" runat="server"
                    SelectCommand="SELECT ID, Name, FullName, DestinationType 
                                   FROM dbo.fnRetrieveEligibleDestinations(@OriginID, @ShipperID, @ProductID, 0) 
                                   WHERE (@RegionID = -1 OR RegionID IS NULL OR RegionID = @RegionID) 
                                   UNION SELECT 0, '(Select Destination)', '(Select Destination)', NULL ORDER BY Name">
                    <SelectParameters>
                        <asp:ControlParameter Name="RegionID" ControlID="rcbRegion" PropertyName="SelectedValue" Type="Int32" DefaultValue="-1" />
                        <asp:ControlParameter Name="ShipperID" ControlID="rcbCustomer" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
                        <asp:ControlParameter Name="ProductID" ControlID="rcbProduct" PropertyName="SelectedValue" Type="Int32" DefaultValue="1" />
                        <asp:ControlParameter Name="OriginID" ControlID="rcbOrigin" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
                    </SelectParameters>
                </blac:DBDataSource>

                <blac:DBDataSource ID="dsConsignee" runat="server"
                    SelectCommand="SELECT ID, Name FROM dbo.tblConsignee WHERE DeleteDateUTC IS NULL
                                   AND (@DestinationID = -1 OR ID IN (SELECT ConsigneeID FROM tblDestinationConsignees WHERE DestinationID = @DestinationID))
                                   UNION SELECT NULL, '(Select Consignee)' ORDER BY Name">
                    <SelectParameters>
                        <asp:ControlParameter Name="DestinationID" ControlID="rcbDestination" PropertyName="SelectedValue" Type="Int32" DefaultValue="-1" />
                    </SelectParameters>
                </blac:DBDataSource>

                <blac:DBDataSource ID="dsCustomer" runat="server"
                    SelectCommand="SELECT C.ID, C.Name 
                                   FROM dbo.tblCustomer C 
                                   JOIN tblOriginCustomers OC ON OC.CustomerID = C.ID 
                                   WHERE OC.OriginID = @OriginID UNION SELECT NULL, '(Select Shipper)' ORDER BY Name">
                    <SelectParameters>
                        <asp:ControlParameter Name="OriginID" ControlID="rcbOrigin" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
                    </SelectParameters>
                </blac:DBDataSource>

                <blac:DBDataSource ID="dsCarrier" runat="server"
                    SelectCommand="SELECT ID, Name FROM dbo.viewCarrier WHERE Active = 1 AND
                                    ID IN (SELECT DISTINCT CarrierID FROM tblDriver WHERE DeleteDateUTC IS NULL) 
                                   AND (@RegionID = -1 OR ID IN (SELECT DISTINCT CarrierID FROM tblDriver WHERE RegionID IS NULL OR RegionID = @RegionID AND DeleteDateUTC IS NULL)) 
                                   UNION SELECT NULL, '(Select Carrier)' ORDER BY Name">
                    <SelectParameters>
                        <asp:ControlParameter Name="RegionID" ControlID="rcbRegion" PropertyName="SelectedValue" Type="Int32" DefaultValue="-1" />
                    </SelectParameters>
                </blac:DBDataSource>

                <blac:DBDataSource ID="dsDriver" runat="server" SelectCommand="SELECT ID, FullName, CarrierID, Carrier, DriverScore,
                                    HosHrs = CAST(DrivingHoursLeft AS VARCHAR(10)) + ' / ' + CAST(OnDutyHoursLeft AS VARCHAR(10)) + ' / ' + CAST(WeeklyOnDutyHoursLeft AS VARCHAR(10)) 
                                   FROM dbo.fnRetrieveEligibleDrivers_NoGPS(@CarrierID, @TerminalID, NULL, @RegionID, NULL, @StartDate, ISNULL((SELECT h2s FROM tblOrigin WHERE ID = @OriginID), 0)) 
                                   WHERE (@TerminalID = -1 OR TerminalID IS NULL OR TerminalID = @TerminalID)
                                    AND ((SELECT TerminalID FROM tblOrigin WHERE ID = @OriginID) IS NULL
                                        OR TerminalID = (SELECT TerminalID FROM tblOrigin WHERE ID = @OriginID)
                                        OR TerminalID IS NULL)
                                   AND ((SELECT TerminalID FROM tblDestination WHERE ID = @DestinationID) IS NULL
                                        OR TerminalID = (SELECT TerminalID FROM tblDestination WHERE ID = @DestinationID)
                                        OR TerminalID IS NULL)
                                   UNION SELECT 0, '(Select Driver)', 0, '', 100, '' ORDER BY DriverScore DESC, FullName">
                    <SelectParameters>
                        <asp:ControlParameter Name="CarrierID" ControlID="rcbCarrier" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
                        <asp:ControlParameter Name="TerminalID" ControlID="rcbTerminal" PropertyName="SelectedValue" Type="Int32" DefaultValue="-1" />
                        <asp:ControlParameter Name="RegionID" ControlID="rcbRegion" PropertyName="SelectedValue" Type="Int32" DefaultValue="-1" />
                        <asp:ControlParameter Name="StartDate" ControlID="rdpDueDate" PropertyName="DbSelectedDate" Type="String" DefaultValue="NULL" />
                        <asp:ControlParameter Name="OriginID" ControlID="rcbOrigin" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
                        <asp:ControlParameter Name="DestinationID" ControlID="rcbDestination" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
                    </SelectParameters>
                </blac:DBDataSource>

                <blac:DBDataSource ID="dsTicketType" runat="server" 
                    SelectCommand="SELECT ID, Name AS TicketType 
                                   FROM dbo.tblTicketType 
                                   WHERE (ForTanksOnly=0 OR isnull((SELECT HasTanks FROM viewOrigin WHERE ID=@OriginID), 0) = 1) ORDER BY Name">
                    <SelectParameters>
                        <asp:ControlParameter Name="OriginID" ControlID="rcbOrigin" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
                    </SelectParameters>
                </blac:DBDataSource><blac:DBDataSource ID="dsPriority" runat="server" SelectCommand="SELECT ID, PriorityNum FROM tblPriority ORDER BY PriorityNum" />
            </div>
        </div>
    </div>
</asp:Content>

