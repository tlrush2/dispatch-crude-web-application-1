﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
//add for sql stuff
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Drawing;
using Telerik.Web.UI;
using AlonsIT;
using DispatchCrude.App_Code;
using DispatchCrude.Core;
using DispatchCrude.Extensions;
using System.Linq;

namespace DispatchCrude.Site.Orders
{
    public partial class OrderRules : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            rdpStartDate.DbSelectedDate = DateTime.Now.Date;
            rdpEndDate.DbSelectedDate = DBNull.Value;
            rdpStartDate.Calendar.ShowRowHeaders = rdpEndDate.Calendar.ShowRowHeaders = false;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ConfigureAjax(!Settings.SettingsID.DisableAJAX.AsBool());
            dbcMain.GetGridErrorMessage += GetErrorMessage;
            dbcMain.GetUnboundChangedValues += GetChangedValues;

            //Print tab navigation html to page
            tabmenu.InnerHtml = NavigationHelper.PrintTabArray(NavigationHelper.TabSet_BusinessRules, "Tab_OrderRules").ToString();
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {

            // Hide ID and add new columns on the website
            rgMain.MasterTableView.Columns.FindByUniqueName("ID").Display = false;
            rgMain.MasterTableView.Columns.FindByUniqueName("TypeID").Display = false;

            if (!IsPostBack)
            {
                DataRow dr = null;
                if (Request.QueryString["OrderID"] != null)
                {
                    using (SSDB ssdb = new SSDB())
                    {
                        DataTable dt = ssdb.GetPopulatedDataTable("SELECT * FROM viewOrder WHERE ID = {0}", Request.QueryString["OrderID"] as object);
                        if (dt.Rows.Count == 1)
                            dr = dt.Rows[0];
                    }
                }

                if (dr != null || Request.QueryString["ShipperID"] != null)
                {
                    ddShipper.DataBind();
                    DropDownListHelper.SetSelectedValue(ddShipper, dr != null ? dr["CustomerID"].ToString() : Request.QueryString["ShipperID"]);
                }
                if (dr != null || Request.QueryString["CarrierID"] != null)
                {
                    ddCarrier.DataBind();
                    DropDownListHelper.SetSelectedValue(ddCarrier, dr != null ? dr["CarrierID"].ToString() : Request.QueryString["CarrierID"]);
                }
                if (dr != null || Request.QueryString["ProductGroupID"] != null)
                {
                    ddProductGroup.DataBind();
                    DropDownListHelper.SetSelectedValue(ddProductGroup, dr != null ? dr["ProductGroupID"].ToString() : Request.QueryString["ProductGroupID"]);
                }
                if (dr != null || Request.QueryString["OriginID"] != null)
                {
                    ddOrigin.DataBind();
                    DropDownListHelper.SetSelectedValue(ddOrigin, dr != null ? dr["OriginID"].ToString() : Request.QueryString["OriginID"]);
                }
                if (dr != null || Request.QueryString["DestinationID"] != null)
                {
                    ddDestination.DataBind();
                    DropDownListHelper.SetSelectedValue(ddDestination, dr != null ? dr["DestinationID"].ToString() : Request.QueryString["DestinationID"]);
                }
                if (dr != null || Request.QueryString["OriginStateID"] != null)
                {
                    ddOriginState.DataBind();
                    DropDownListHelper.SetSelectedValue(ddOriginState, dr != null ? dr["OriginStateID"].ToString() : Request.QueryString["OriginStateID"]);
                }
                if (dr != null || Request.QueryString["DestStateID"] != null)
                {
                    ddDestState.DataBind();
                    DropDownListHelper.SetSelectedValue(ddDestState, dr != null ? dr["DestStateID"].ToString() : Request.QueryString["DestStateID"]);
                }
                if (dr != null || Request.QueryString["RegionID"] != null)
                {
                    ddRegion.DataBind();
                    DropDownListHelper.SetSelectedValue(ddRegion, dr != null ? dr["OriginRegionID"].ToString() : Request.QueryString["RegionID"]);
                }
                if (dr != null || Request.QueryString["ProducerID"] != null)
                {
                    ddProducer.DataBind();
                    DropDownListHelper.SetSelectedValue(ddProducer, dr != null ? dr["ProducerID"].ToString() : Request.QueryString["ProducerID"]);
                }
                if (dr != null || Request.QueryString["EffectiveDate"] != null)
                {
                    DateTime effectiveDate = DateTime.Now.Date;
                    if (DateTime.TryParse(dr != null ? dr["OrderDate"].ToString() : Request.QueryString["EffectiveDate"], out effectiveDate))
                    {
                        rdpStartDate.SelectedDate = effectiveDate;
                    }
                }
                rgMain.Rebind();
            }
        }

        private void ConfigureAjax(bool enabled = true)
        {
            RadAjaxManager.GetCurrent(this.Page).EnableAJAX = enabled;
            if (enabled)
            {
                RadAjaxHelper.AddAjaxSetting(this.Page, btnRefresh, rgMain, true);
                RadAjaxHelper.AddAjaxSetting(this.Page, rgMain, rgMain);
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            rgMain.Rebind();
        }

        protected void grid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.EditCommandName)
            {
                if (e.Item is GridEditableItem)
                {
                    //DropDownList ddl = RadGridHelper.GetColumnDropDown((e.Item as GridEditableItem)["CarrierID"]);
                }
            }
            if (e.CommandName == "AddNew" && e.Item is GridDataItem) //Row "Add new" button clicked
            {
                // cancel the default operation
                e.Canceled = true;

                e.Item.OwnerTableView.InsertItem(GetRowValues(e.Item as GridDataItem));
            }
            else if (e.CommandName == RadGrid.InitInsertCommandName)
            {
                // cancel the default operation
                e.Canceled = true;

                e.Item.OwnerTableView.InsertItem(GetNewRowValues(e.Item as GridDataItem));
            }
            else if (e.CommandName == "ExportToExcel")
            {
                e.Canceled = true;
                ExportGridToExcel();
            }
        }

        protected void GetErrorMessage(ref string error)
        {
            if (error.Contains("Overlapping order rules are not allowed"))
                error = "Overlapping/duplicate records are not allowed";
        }

        private Hashtable GetRowValues(GridEditableItem gdi)
        {
            //Prepare an IDictionary with the predefined values
            Hashtable ret = new Hashtable();
            gdi.ExtractValues(ret);
            
            DateTime date = DBHelper.ToDateTime(
                gdi["EffectiveDate"].Controls[1] is Label
                    ? (gdi["EffectiveDate"].Controls[1] as Label).Text
                    : (gdi["EffectiveDate"].Controls[1] as RadDatePicker).DbSelectedDate);
            ret["EffectiveDate"] = date.Date;
            ret["EndDate"] = DateTime.Now.Date.AddYears(1);
            return ret;
        }
        private Hashtable GetNewRowValues(GridEditableItem gdi)
        {
            Hashtable ret = new Hashtable();
            // set ID to DBNULL (since we are creating a new record)
            ret["ID"] = DBNull.Value;
            ret["TypeID"] = 0;
            ret["ShipperID"] = 0;
            ret["CarrierID"] = 0;
            ret["ProductGroupID"] = 0;
            ret["OriginID"] = 0;
            ret["DestinationID"] = 0;
            ret["OriginStateID"] = 0;
            ret["DestStateID"] = 0;
            ret["RegionID"] = 0;
            ret["ProducerID"] = 0;
            // default the new Effective Date to the first day of the current month
            ret["EffectiveDate"] = DateTime.Now.Date.AddDays(1 - DateTime.Now.Day);
            ret["EndDate"] = DateTime.Now.Date.AddYears(1);
            return ret;
        }

        protected void cmdExport_Click(object sender, EventArgs e)
        {
            ExportGridToExcel();
        }

        /*
        protected void cvUpload_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = Path.GetExtension(excelUpload.FileName).ToLower() == ".xlsx";
        }
        */

        /*
        protected void cmdImport_Click(object sender, EventArgs e)
        {
            Page.Validate();
            if (Page.IsValid)
            {
                FinancialImporter fi = new FinancialImporter();
                fi.AddSpec(rgMain, "ID", FinancialImporter.FISpec.FISType.ID);
                fi.AddSpec(rgMain, "xlsTypeID", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgMain, "ShipperID", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgMain, "CarrierID", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgMain, "ProductGroupID", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgMain, "OriginID", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgMain, "DestinationID", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgMain, "OriginStateID", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgMain, "DestinationStateID", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgMain, "RegionID", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgMain, "ProducerID", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgMain, "Value", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgMain, "EffectiveDate", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec(rgMain, "EndDate", FinancialImporter.FISpec.FISType.BOTH);
                fi.AddSpec("CreatedByUser", typeof(string), FinancialImporter.FISpec.FISType.NEW | FinancialImporter.FISpec.FISType.USERNAME);
                fi.AddSpec("CreateDateUTC", typeof(DateTime), FinancialImporter.FISpec.FISType.NEW | FinancialImporter.FISpec.FISType.NOW);
                fi.AddSpec("LastChangedByUser", typeof(string), FinancialImporter.FISpec.FISType.UPDATE | FinancialImporter.FISpec.FISType.USERNAME);
                fi.AddSpec("LastChangeDateUTC", typeof(DateTime), FinancialImporter.FISpec.FISType.UPDATE | FinancialImporter.FISpec.FISType.NOW);
                fi.AddSpec(rgMain, "ImportAction", FinancialImporter.FISpec.FISType.ACTION);
                fi.AddSpec(rgMain, "ImportOutcome", FinancialImporter.FISpec.FISType.OUTCOME);
                fi.GetErrorMessage += GetErrorMessage;
                Response.ExportExcelStream(fi.ProcessSql(excelUpload.FileContent, dbcMain.UpdateTableName), Path.GetFileNameWithoutExtension(excelUpload.FileName) + "_ImportResults.xlsx");
            }
        }
        */

        private void exporter_CellValueChanged(GridDataItem item, string colName, ref object value)
        {
            if (colName == "Value")
                value = (RadGridHelper.GetControlByType(item, "Value", typeof(Label)) as Label).Text;
        }

        private void ExportGridToExcel()
        {
            string filename = string.Format("Order Rules as of {0:yyyyMMdd}.xlsx", rdpStartDate.SelectedDate);
            string[] hiddenToInclude = { "ID" } //, "ImportAction", "ImportOutcome" }
                , visibleToSkip = { "CreateDate", "CreatedByUser", "LastChangeDate", "LastChangedByUser" };
            RadGridExcelExporter exporter = new RadGridExcelExporter(
                    hiddenColNamesToInclude: hiddenToInclude
                    , visibleColNamesToSkip: visibleToSkip
                    , dropDownColumnDataValidationList: true);
            exporter.OnCellValueChanged += exporter_CellValueChanged;
            exporter.AddDropDownColumnDS("TypeID", "dsType");
            rgMain.AllowPaging = false;
            rgMain.Rebind();
            Response.ExportExcelStream(exporter.ExportSheet(rgMain.MasterTableView, "Order Rules"), filename);
        }

        protected void grid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            DataRowView data = e.Item.DataItem as DataRowView;
            if (e.Item is GridEditableItem && e.Item.IsInEditMode)
            {
                if (data != null)
                {
                    RadComboBoxHelper.SetSelectedValue(RadGridHelper.GetColumnRadComboBox(e.Item, "TypeID"), data["TypeID"]);
                    RadComboBox rcbValue = RadGridHelper.GetColumnRadComboBox(e.Item, "Value");
                    PopulateValueDropDown(rcbValue, DBHelper.ToInt32(data["TypeID"]), Converter.ToNullString(data["Value"]));
                    RadGridHelper.GetColumnRadComboBox(e.Item, "TypeID").Enabled = false;
                } 
                else // this is a new record so we need to handle "Type" value changes
                {
                    RadComboBox rcbType = RadGridHelper.GetColumnRadComboBox(e.Item, "TypeID");
                    rcbType.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(rcbType_SelectedIndexChanged);
                    rcbType.AutoPostBack = true;
                    rcbType.Enabled = true;
                    rcbType_SelectedIndexChanged(rcbType, EventArgs.Empty as RadComboBoxSelectedIndexChangedEventArgs);
                }
            }
            else if (e.Item is GridDataItem && !e.Item.IsInEditMode)
            {
                // highlight best match records with a yellow background
                if (DBHelper.ToBoolean(data["BestMatch"])) e.Item.BackColor = Color.Goldenrod;
            }
        }

        protected void rcbType_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            RadComboBox rcbValue = RadGridHelper.GetControlByType(
                ((sender as Control).NamingContainer as GridEditFormInsertItem)["Value"]
                , typeof(RadComboBox)) as RadComboBox;
            //RadComboBox rcbValue = RadGridHelper.GetColumnRadComboBox((sender as RadComboBox).Parent as GridDataItem, "Value");
            PopulateValueDropDown(rcbValue, RadComboBoxHelper.SelectedValue((sender as RadComboBox)));
        }

        private void PopulateValueDropDown(RadComboBox rcbValue, int orderRuleTypeID, string defValue = null)
        {
            rcbValue.Items.Clear();
            int itemCount = 0;

            if (orderRuleTypeID == 7)  //If the selected rule is "Ticket Type Changes Allowed", do some special stuff to get the checkbox dropdown
            {
                using (SSDB db = new SSDB())
                {
                    DataTable ticketTypes = db.GetPopulatedDataTable("SELECT ID, Name FROM tblTicketType"); //get ticket types
                    rcbValue.CheckBoxes = true; //turn on checkboxes

                    foreach (DataRow row in ticketTypes.Rows)
                    {
                        RadComboBoxItem item = new RadComboBoxItem  //create new combobox item
                        {
                            Value = row["ID"].ToString()
                            , Text = row["Name"].ToString()
                            , Checked = false // default to not checked
                        };                       

                        if (defValue != null)
                        {
                            foreach (string type in defValue.Split(','))  //if item is in the currently saved list, check it to retain that value on save
                            {
                                if (type == item.Value)
                                {
                                    item.Checked = true;
                                    break;
                                }
                            }
                        }                        

                        rcbValue.Items.Add(item);  //add the new combobox item to the dropdown list
                    }                    
                }
            }
            else  //otherwise, stay with the original code
            {
                using (SSDB db = new SSDB())
                {
                    using (SqlCommand cmd = db.BuildCommand("spOrderRuleDropDownValues"))
                    {
                        cmd.Parameters["@OrderRuleTypeID"].Value = orderRuleTypeID;
                        DataTable dtValues = SSDB.GetPopulatedDataTable(cmd);
                        itemCount = dtValues.Rows.Count;
                        // if no default value was inputted, then use the one from the db (if any available)
                        if (defValue == null && !DBHelper.IsNull(cmd.Parameters["@DefaultValue"].Value))
                            defValue = cmd.Parameters["@DefaultValue"].Value.ToString();
                        foreach (DataRow drValue in dtValues.Rows)
                        {
                            string value = drValue["Value"].ToString();
                            rcbValue.Items.Add(new RadComboBoxItem(value, value));
                        }
                    }
                }

                if (itemCount > 0 && !DBHelper.IsNull(defValue))
                {
                    RadComboBoxHelper.SetSelectedValue(rcbValue, defValue);
                    rcbValue.AllowCustomText = false;
                }
                else
                    rcbValue.Text = defValue;
            }
        }

        private void GetChangedValues(GridEditableItem item, System.Data.DataRow values)
        {
            RadComboBox rcbRuleType = (RadComboBox)item.FindControl("rcbTypeID");
            RadComboBox rcbTypeID = RadGridHelper.GetColumnRadComboBox(item, "TypeID");

            if (rcbRuleType.SelectedValue == "7")  //If rule type seven, save differently due to the checkbox dropdown.
            {
                UpdateFilterValue(item["Value"], "rcbValue", values, "Value");

                values["TypeID"] = RadComboBoxHelper.SelectedValue(rcbTypeID);
            }
            else
            {
                RadComboBox rcbValue = RadGridHelper.GetColumnRadComboBox(item, "Value");
                values["Value"] = rcbValue.Text;

                values["TypeID"] = RadComboBoxHelper.SelectedValue(rcbTypeID);
            }            
        }
        
        //At this time (9/30/16), This is called only for rule type 7 - "Ticket Type Changes Allowed"
        private bool UpdateFilterValue(TableCell cell, string radComboBoxID, DataRow values, string dataField)
        {
            bool ret = false;
            RadComboBox rcbValues = (RadComboBox)RadGridHelper.GetControlByType(cell, typeof(RadComboBox), true, radComboBoxID);
            if (rcbValues != null)
            {
                if (rcbValues.CheckedItems.Count > 0)
                {
                    values[dataField] = rcbValues.CheckedItems.Where(ci => ci.Checked).Select(ci => ci.Value).Aggregate((a, b) => a + "," + b.ToString());
                    ret = true;
                }
                else if (rcbValues.SelectedIndex != -1)
                {
                    values[dataField] = rcbValues.SelectedItem == null || rcbValues.SelectedItem.Value.Equals(rcbValues.SelectedItem.Text)
                        ? rcbValues.Text : rcbValues.SelectedItem.Value;
                    ret = true;
                }
                else
                {
                    values[dataField] = DBNull.Value;
                }
            }
            return ret;
        }
    }
}