﻿using System;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using Telerik.Web.UI;
using System.Web.UI.WebControls;
using DispatchCrude.App_Code;
using DispatchCrude.Core;
using DispatchCrude.Extensions;
using AlonsIT;

namespace DispatchCrude.Site.Orders
{
    public partial class GaugerOrders_Creation : RadAjaxPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ConfigureAjax(!Core.Settings.SettingsID.DisableAJAX.AsBool());

            //Print tab navigation html to page
            tabmenu.InnerHtml = NavigationHelper.PrintTabArray(NavigationHelper.TabSet_Dispatch, "Tab_OrderCreate").ToString();

            //Print button navigation html to page
            speedbuttons.InnerHtml = NavigationHelper.PrintButtonArray(NavigationHelper.ButtonSet_TabOrderCreate, "Button_GaugerOrders").ToString();

            if (!IsPostBack)
            {
                this.ddlPriority.SelectedValue = "3";  // default value is "Low" priority

                //This is setting the initially selected date (on load) to "today" plus the number of days in "AddDays()"
                this.rdpDueDate.SelectedDate = DateTime.Now.Date.AddDays(1);
                this.rdpDueDate.MinDate = DateTime.Now.Date;
                this.rdpDueDate.Calendar.ShowRowHeaders = false;

                // default the Region to the default user profile (if available and valid)
                if (Context.Profile.GetPropertyValue("RegionID") != null && DBHelper.ToInt32(Context.Profile.GetPropertyValue("RegionID")) != 0)
                {
                    this.rcbRegion.DataBind();
                    RadComboBoxHelper.SetSelectedValue(this.rcbRegion, Context.Profile.GetPropertyValue("RegionID").ToString());
                }
            }
        }

        private void ConfigureAjax(bool enableAjax = true)
        {
            if (enableAjax)
            {
                RadAjaxHelper.AddAjaxSetting(this.Page, ddlPriority, ddlPriority);
                RadAjaxHelper.AddAjaxSetting(this.Page, rgMain, rgMain);
                RadAjaxHelper.AddAjaxSetting(this.Page, rgMain, cmdDispatch);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbRegion, rcbOrigin);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbRegion, rcbShipper);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbRegion, rcbDestination);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbRegion, rgMain);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbRegion, cmdCreateLoads, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbRegion, cmdDispatch, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbOrigin, rcbShipper);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbOrigin, rcbProduct);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbOrigin, rcbOriginTank);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbOrigin, rcbTicketType);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbOrigin, rcbDestination);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbOrigin, rcbGauger);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbOrigin, rfvDispatchConfirmNum);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbOrigin, cvDispatchConfirmNum);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbOrigin, cmdCreateLoads);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbOrigin, cmdDispatch);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbShipper, rcbProduct);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbShipper, rcbDestination);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbOriginTank, rcbOrigin);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbOriginTank, rcbGauger);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbOriginTank, rcbProduct);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbOriginTank, rcbDestination);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbOriginTank, rcbShipper);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbOriginTank, cmdCreateLoads, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbProduct, rcbDestination);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbProduct, rcbGauger);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbProduct, cmdCreateLoads);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbProduct, rgMain);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbProduct, cmdCreateLoads, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbProduct, cmdDispatch, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbProduct, rfvDispatchConfirmNum);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbDestination, rcbGauger);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbDestination, rfvDispatchConfirmNum);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbDestination, cmdCreateLoads);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbTicketType, cmdCreateLoads);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbTicketType, cmdDispatch, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbGauger, cmdCreateLoads);
                RadAjaxHelper.AddAjaxSetting(this.Page, rcbGauger, cmdDispatch);
                RadAjaxHelper.AddAjaxSetting(this.Page, txtDispatchConfirmNum, cmdCreateLoads, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, txtDispatchConfirmNum, rntxtQty, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, txtDispatchConfirmNum, cvDispatchConfirmNum, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, rdpDueDate, rdpDueDate);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdCreateLoads, rgMain);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdCreateLoads, rfvDispatchConfirmNum, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdCreateLoads, cvDispatchConfirmNum, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdCreateLoads, radWindowManager, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdDelete, rgMain);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdDispatch, rgMain);
            }
            else
                RadAjaxManager.GetCurrent(Page).EnableAJAX = false;
        }

        protected void cmdCreateLoads_Click(object source, EventArgs e)
        {
            if (Page.IsValid)
            {
                int count = CreateLoads();

                // refresh the grid
                RebindGrid();

                // provide a dialog to the user indicating success
                AlertUser("created", count);
            }
        }

        private void AlertUser(string action, int count, int width = 250, int height = 75)
        {
            string userMsg = string.Format(@"{0} order{1} {2}."
                , count
                , count == 1 ? "" : "s"
                , action);
            radWindowManager.RadAlert(userMsg, 250, 75, "Action Feedback", null);
        }

        private int CreateLoads()
        {
            int ret = 0;
            bool gaugerAssigned = DBHelper.ToInt32(rcbGauger.SelectedValue) != 0;
            using (SSDB db = new SSDB())
            {
                using (SqlCommand cmd = db.BuildCommand("spCreateGaugerLoads"))
                {
                    cmd.Parameters["@OriginID"].Value = RadComboBoxHelper.SelectedDbValue(rcbOrigin);
                    cmd.Parameters["@ShipperID"].Value = RadComboBoxHelper.SelectedDbValue(rcbShipper);
                    cmd.Parameters["@DestinationID"].Value = RadComboBoxHelper.SelectedDbValue(rcbDestination);
                    cmd.Parameters["@GaugerTicketTypeID"].Value = 1; // TODO
                    cmd.Parameters["@DueDate"].Value = rdpDueDate.SelectedDate.Value.Date;
                    cmd.Parameters["@OriginTankID"].Value = RadComboBoxHelper.SelectedValue(rcbOriginTank);
                    cmd.Parameters["@ProductID"].Value = rcbProduct.SelectedValue;
                    cmd.Parameters["@UserName"].Value = User.Identity.Name;

                    if (txtJobNumber.Text.Length > 0)
                        cmd.Parameters["@JobNumber"].Value = txtJobNumber.Text;

                    if (gaugerAssigned)
                        cmd.Parameters["@GaugerID"].Value = rcbGauger.SelectedValue;
                        cmd.Parameters["@Qty"].Value = rntxtQty.Value;
                        cmd.Parameters["@PriorityID"].Value = ddlPriority.SelectedValue;
                        if (txtDispatchConfirmNum.Text.Length > 0)
                            cmd.Parameters["@DispatchConfirmNum"].Value = txtDispatchConfirmNum.Text;
                    try
                    {
                        cmd.ExecuteNonQuery();
                        ret = DBHelper.ToInt32(cmd.Parameters["@count"].Value);
                    }
                    catch (Exception e)
                    {
                        throw new Exception(
                            string.Format("Exception occurred generating gauger orders with parameters: OriginID={0},DestinationID={1},GaugerTicketTypeID={2},DueDate={3}"
                                , rcbOrigin.SelectedValue
                                , rcbDestination.SelectedValue
                                , rcbTicketType.SelectedValue
                                , rdpDueDate.SelectedDate.Value.ToShortDateString()), e);
                    }
                }
            }
            return ret;
        }

        protected void cmdDelete_Click(object sender, EventArgs e)
        {
            int count = 0;
            using (SSDB db = new SSDB())
            {
                using (SqlCommand cmd = db.BuildCommand(
                    string.Format("UPDATE tblOrder SET DeleteDateUTC = getutcdate(), DeletedByUser = {0} WHERE ID = (@ID)"
                        , DBHelper.QuoteStr(UserSupport.UserName))))
                {
                    count = ExecuteCommandOnSelection(cmd);
                }
            }
            AlertUser("deleted", count);
        }

        protected void cmdDispatch_Click(object sender, EventArgs e)
        {
            int count = 0;
            using (SSDB db = new SSDB())
            {
                using (SqlCommand cmd = db.BuildCommand(
                                string.Format(@"UPDATE tblGaugerOrder SET StatusID = 2, GaugerID = {0} "
                                    + ", LastChangeDateUTC = getutcdate(), LastChangedByUser = {1} "
                                    + "WHERE OrderID = (@ID) AND StatusID = 1"
                                , rcbGauger.SelectedValue
                                , DBHelper.QuoteStr(UserSupport.UserName))))
                {
                    count = ExecuteCommandOnSelection(cmd);
                }
            }
            AlertUser("dispatched", count);
        }

        protected int ExecuteCommandOnSelection(SqlCommand cmd)
        {
            int ret = 0;
            cmd.Parameters.Add("@ID", SqlDbType.Int);

            foreach (GridItem item in rgMain.MasterTableView.Items)
            {
                if (item is GridDataItem && (item.FindControl("chkSel") as CheckBox).Checked)
                {
                    cmd.Parameters["@ID"].Value = (item as GridDataItem).GetDataKeyValue("OrderID");
                    ret += cmd.ExecuteNonQuery();
                }
            }
            RebindGrid();
            return ret;
        }

        protected void rcbOrigin_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (SSDB db = new SSDB())
            {
                App_Code.RadComboBoxHelper.Rebind(rcbShipper, true);
                App_Code.RadComboBoxHelper.Rebind(rcbProduct, true);
                // if only 1 true product is available, just select it
                if (rcbProduct.Items.Count == 2)
                    rcbProduct.SelectedIndex = 1;
                App_Code.RadComboBoxHelper.Rebind(rcbDestination, true);
                if (rcbDestination.Items.Count == 2)
                    rcbDestination.SelectedIndex = 1;
                object ticketTypeID = db.QuerySingleValue("SELECT GaugerTicketTypeID FROM tblOrigin WHERE ID={0}", rcbOrigin.SelectedValue);
                RadComboBoxHelper.SetSelectedValue(rcbTicketType, ticketTypeID ?? 1);
                object gaugerID = db.QuerySingleValue("SELECT GaugerID FROM tblOrigin WHERE ID={0}", rcbOrigin.SelectedValue);
                RadComboBoxHelper.SetSelectedValue(rcbGauger, gaugerID);
                entryChanged(sender, e);
                dispatchValueChanged(sender, e);
                FormatRadComboBoxH2S(sender as RadComboBox);
            }
        }

        private bool dcr_done = false; // prevent redundant executions on the same postback
        private void UpdateDispatchConfirmationRequired()
        {
            if (!dcr_done)
            {
                dcr_done = true;
                bool enabled = RadComboBoxHelper.SelectedValue(rcbGauger) > 0
                        && DBHelper.ToBoolean(
                            OrderRuleHelper.GetRuleValue(DateTime.UtcNow.Date
                                , OrderRuleHelper.Type.RequireShipperPO
                                , RadComboBoxHelper.SelectedValue(rcbShipper)
                                , null
                                , RadComboBoxHelper.SelectedValue(rcbProduct)
                                , RadComboBoxHelper.SelectedValue(rcbOrigin)
                                , RadComboBoxHelper.SelectedValue(rcbDestination)
                                , false));
                this.rfvDispatchConfirmNum.Enabled = enabled;
            }
        }

        protected void rcbOrigin_ItemDataBound(object sender, RadComboBoxItemEventArgs e)
        {
            if (DBHelper.ToBoolean((e.Item.DataItem as DataRowView)["H2S"]))
                e.Item.CssClass += " H2S";
        }

        protected void rcbOriginTank_ItemDataBound(object sender, RadComboBoxItemEventArgs e)
        {
            if (e.Item.DataItem is DataRowView)
            {
                e.Item.Attributes.Add("OriginID", (e.Item.DataItem as DataRowView)["OriginID"].ToString());
            }
        }

        private bool m_ot_sic_inprogress = false;
        protected void rcbOriginTank_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!m_ot_sic_inprogress)
            {
                try
                {
                    m_ot_sic_inprogress = true;
                    entryChanged(sender, e);
                    if ((sender as RadComboBox).SelectedItem != null)
                    {
                        int tankID = RadComboBoxHelper.SelectedValue(sender as RadComboBox);
                        RadComboBoxHelper.SetSelectedValue(rcbOrigin, (sender as RadComboBox).SelectedItem.Attributes["OriginID"]);
                        rcbOrigin_SelectedIndexChanged(rcbOrigin, EventArgs.Empty);
                        RadComboBoxHelper.SetSelectedValue(sender as RadComboBox, tankID);
                    }
                }
                finally
                {
                    m_ot_sic_inprogress = false;
                }
            }
        }

        protected void ddCarrier_SelectedIndexChanged(object sender, EventArgs e)
        {
            App_Code.RadComboBoxHelper.Rebind(rcbGauger);
            entryChanged(sender, e);
            dispatchValueChanged(sender, e);
        }

        protected void regionChanged(object sender, EventArgs e)
        {
            RebindGrid();
            App_Code.RadComboBoxHelper.Rebind(rcbOrigin, true);
            App_Code.RadComboBoxHelper.Rebind(rcbDestination, true);
            entryChanged(sender, e);
        }

        protected void rcbProduct_SelectedIndexChanged(object sender, EventArgs e)
        {
            App_Code.RadComboBoxHelper.Rebind(rcbDestination, true);
            RebindGrid();
            entryChanged(sender, e);
        }

        private void RebindGrid()
        {
            rgMain.Rebind();
            cmdDispatch.Enabled = cmdDelete.Enabled = false;
        }

        protected void entryChanged(object sender, EventArgs e)
        {
            UpdateDispatchConfirmationRequired();
            // ensure the destination and origin + shipper are defined before allowing order creation
            this.cmdCreateLoads.Enabled = rcbOrigin.SelectedIndex > 0 && rcbShipper.SelectedIndex > 0 
                && rcbProduct.SelectedIndex > 0 && rcbDestination.SelectedIndex > 0 
                && rcbOriginTank.SelectedIndex != -1;
        }

        protected void txtDispatchConfirmNum_TextChanged(object sender, EventArgs e)
        {
            if (txtDispatchConfirmNum.Text.Trim().Length > 0)
            {
                rntxtQty.Value = 1;
                rntxtQty.Enabled = false;
            }
            else
                rntxtQty.Enabled = true;
            entryChanged(sender, e);
        }

        protected void carrierValueChanged(object sender, EventArgs e)
        {
            App_Code.RadComboBoxHelper.Rebind(rcbGauger, true);
            dispatchValueChanged(sender, e);
        }

        protected void dispatchValueChanged(object sender, EventArgs e)
        {
            bool itemIsChecked = false;
            foreach (GridItem item in rgMain.MasterTableView.Items)
            {
                if (item is GridDataItem && (item.FindControl("chkSel") as CheckBox).Checked)
                {
                    itemIsChecked = true;
                    break;
                }
            }
            this.cmdDispatch.Enabled = rcbGauger.SelectedIndex > 0 && itemIsChecked;
            entryChanged(sender, e);
        }

        protected void glblOrigin_PreRender(object sender, EventArgs e)
        {
            if ((sender as Label).Text.StartsWith("H2S-"))
                (sender as Label).CssClass += " H2S";
        }
        
        private void FormatRadComboBoxH2S(RadComboBox rcb)
        {
            if (rcb.Text.StartsWith("H2S-") || rcb.SelectedItem.Text.StartsWith("H2S-"))
                rcb.InputCssClass += " H2S";
            else
                rcb.InputCssClass = rcb.InputCssClass.Replace("H2S", "").Trim();
        }

        private void ExportToExcel()
        {
            string filename = string.Format("GaugerOrderCreation_{0:yyyy-MM-dd-HH-mm}.xlsx", DateTime.Now);
            MemoryStream ms = new RadGridExcelExporter().ExportSheet(rgMain.MasterTableView);
            Response.ExportExcelStream(ms, filename);
        }

        protected void grid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.ExportToExcelCommandName)
            {
                ExportToExcel();
                e.Canceled = true;
            }
            else if (e.CommandName == "Refresh")
            {
                rgMain.Rebind();
            }
        }

        protected void grid_ItemCreated(object sender, GridItemEventArgs e)
        {
        } 

        protected void grid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            bool h2s = false;
            if (e.Item.DataItem != null && !(e.Item is GridGroupHeaderItem) && e.Item.DataItem is DataRowView)
                h2s = DBHelper.ToBoolean((e.Item.DataItem as DataRowView)["H2S"]);

            if (e.Item is GridDataItem)
            {
                Color backColor = Color.White;
                switch ((e.Item.DataItem as DataRowView)["Status"].ToString())
                {
                    case "Requested":
                        backColor = Color.LightGreen;
                        break;
                    case "Dispatched":
                        backColor = Color.Tan;
                        break;
                    case "Declined":
                        backColor = Color.LightGray;
                        break;
                }
                if (backColor != Color.Transparent)
                    e.Item.BackColor = backColor;

                // H2S alert
                if (h2s)
                {
                    ((Label)e.Item.FindControl("lblH2S")).Text = " H2S ";
                    ((Label)e.Item.FindControl("lblH2S")).CssClass += " H2S";
                }
            }
        }

        protected void cvDispatchConfirmNum_ServerValidate(object source, ServerValidateEventArgs args)
        {
            using (SSDB ssdb = new SSDB())
            {
                args.IsValid = txtDispatchConfirmNum.Text.Trim().Length == 0 || DBHelper.ToInt32(ssdb.QuerySingleValue(
                    string.Format(
                        "SELECT count(*) FROM tblOrder WHERE DeleteDateUTC IS NULL AND CustomerID = {0} AND DispatchConfirmNum = {1}"
                            , RadComboBoxHelper.SelectedValue(rcbShipper), DBHelper.QuoteStr(txtDispatchConfirmNum.Text)))) == 0;
            }
        }

    }
}