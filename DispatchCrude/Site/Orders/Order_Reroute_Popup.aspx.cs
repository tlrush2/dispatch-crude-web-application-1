﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AlonsIT;

namespace DispatchCrude.Site.Orders
{
    public partial class Order_Reroute_Popup : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Filter destinations to those in user's profile terminal
            int terminalID;
            if (Context.Profile.GetPropertyValue("TerminalID") != null
                && (terminalID = DBHelper.ToInt32(Context.Profile.GetPropertyValue("TerminalID"))) != 0)
            {
                hfRestrictedTerminalID.Value = terminalID.ToString();
            }

            if (!IsPostBack)
            {
                int orderID = DBHelper.ToInt32(Request.QueryString["ID"]);
                if (orderID == 0)
                {
                    // TODO: show an appropriate message and redirect back to the Default page??
                }
                else
                {
                    GetOrderDataBinder(orderID, "1,2,7,8").DataRowToControls(this);
                    this.Page.Title = "Reroute Order #" + this.hiddenOrderNum.Value;
                }
            }
        }

        static private App_Code.SingleRowDataBinder GetOrderDataBinder(int id, string statusIDCSVFilter = "2,3,7,8")
        {
            if (statusIDCSVFilter.Length > 0)
                statusIDCSVFilter = string.Format("StatusID IN ({0}) AND ", statusIDCSVFilter);
            return new App_Code.SingleRowDataBinder(string.Format("SELECT * FROM viewOrder WHERE {0} ID=@ID AND DeleteDateUTC IS NULL", statusIDCSVFilter), "tblOrder", id);
        }

        protected void EntryValueChanged(object sender, EventArgs e)
        {
            cmdReroute.Enabled = App_Code.DropDownListHelper.SelectedValue(ddlNewDest) != 0; // && txtNotes.Text.Length > 0;
        }

        protected void cmdReroute_Click(object sender, EventArgs e)
        {
            // do the reroute
            using (SSDB db = new SSDB())
            {
                using (System.Data.SqlClient.SqlCommand cmd = db.BuildCommand("spRerouteOrder"))
                {
                    cmd.Parameters["@OrderID"].Value = DBHelper.ToInt32(Request.QueryString["ID"]);
                    cmd.Parameters["@NewDestinationID"].Value = DBHelper.ToInt32(ddlNewDest.SelectedValue);
                    cmd.Parameters["@UserName"].Value = User.Identity.Name;
                    cmd.Parameters["@Notes"].Value = txtNotes.Text;
                    cmd.ExecuteNonQuery();
                }
            }
            // disable the "edit" buttons (forcing the user to close the dialog manually)         
            ddlNewDest.Enabled = cmdReroute.Enabled = false;
            //server code to extract and process the reply text here
            ScriptManager.RegisterStartupScript(this, GetType(), "closeWnd", "CloseWnd();", true);
        }
    }
}