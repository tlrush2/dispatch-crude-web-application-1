﻿using System;
using System.Web;
using System.Web.UI.WebControls;
//add for sql stuff
using System.Data;
using System.Drawing;
using System.IO;
using Telerik.Web.UI;
using DispatchCrude.App_Code;
using DispatchCrude.Core;
using DispatchCrude.Extensions;
using DispatchCrude.Models;
using AlonsIT;

namespace DispatchCrude.Site.Orders
{
    public partial class Orders_Dispatch : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            //These two statements are required for personalized grid layout 
            //The Key value must be unique across all site pages
            rpmMain.StorageProviderKey = "Dispatch_rgMain";
            rpmMain.StorageProvider = new DbStorageProvider();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ConfigureAjax(!Core.Settings.SettingsID.DisableAJAX.AsBool());

            // set the hours back parameter for query
            dsMain.SelectParameters["HoursBack"].DefaultValue = Settings.SettingsID.ShowRecentlyDelivered.AsInt().ToString();
            
            //Print tab navigation html to page
            tabmenu.InnerHtml = NavigationHelper.PrintTabArray(NavigationHelper.TabSet_Dispatch, "Tab_Grid").ToString();

            if (!IsPostBack)
            {
                // DCWEB-1027: if this user has a specific Profile Region specified, and the site is restricting users to that region, make the control readonly
                int regionID;
                if (Context.Profile.GetPropertyValue("RegionID") != null
                    && (regionID = DBHelper.ToInt32(Context.Profile.GetPropertyValue("RegionID"))) != 0
                    && Settings.SettingsID.RestrictUserToProfileRegion.AsBool())
                {
                    dsMain.SelectParameters["RegionID"].DefaultValue = regionID.ToString();
                }

                // if the user has a persisted grid layout value, load it now
                if (DbStorageProvider.HasStateInStorage(rpmMain.StorageProviderKey)) rpmMain.LoadState();
            }

            //rgMain.Columns.FindByUniqueNameSafe("Customer").Visible = UserSupport.IsLogistics(//);            
        }

        private void ConfigureAjax(bool enabled = true)
        {
            if (enabled)
            {
                RadAjaxHelper.AddAjaxSetting(Page, rgMain, rgMain);
            }
            RadAjaxManager.GetCurrent(this.Page).EnableAJAX = enabled;
        }

        protected void glblOrigin_PreRender(object sender, EventArgs e)
        {
            if ((sender as Label).Text.StartsWith("H2S-"))
                (sender as Label).CssClass += " H2S";
        }
        private void FormatRadComboBoxH2S(RadComboBox rcb)
        {
            if (rcb.Text.StartsWith("H2S-") || rcb.SelectedItem.Text.StartsWith("H2S-"))
                rcb.InputCssClass += " H2S";
            else
                rcb.InputCssClass = rcb.InputCssClass.Replace("H2S", "").Trim();
        }

        protected void grid_ItemCreated(object sender, GridItemEventArgs e)
        {
        } 

        protected void grid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            OrderStatus.STATUS orderStatus = OrderStatus.STATUS.Audited; // default value
            bool h2s = false;
            DateTime DueDate = DateTime.UtcNow;
            if (e.Item.DataItem != null && !(e.Item is GridGroupHeaderItem) && e.Item.DataItem is DataRowView)
            {
                orderStatus = (OrderStatus.STATUS)DBHelper.ToInt32((e.Item.DataItem as DataRowView)["StatusID"]);
                DueDate = DBHelper.ToDateTime((e.Item.DataItem as DataRowView)["DueDate"]);
                h2s = DBHelper.ToBoolean((e.Item.DataItem as DataRowView)["H2S"]);
            }

            if (e.Item is Telerik.Web.UI.GridDataItem)
            {
                if (User.IsInRole("rerouteOrders"))
                {
                    // only allow rerouting for orders that are PICKED UP
                    bool canReroute = orderStatus.IsIn(OrderStatus.STATUS.PickedUp);
                    (e.Item as GridEditableItem)["Destination"].FindControl("linkDestination").Visible = canReroute;
                    (e.Item as GridEditableItem)["Destination"].FindControl("lblDestination").Visible = !canReroute;
                }

                if (User.IsInRole("transferOrders"))
                {
                    // only allow transfers for orders that are PICKEDUP and not already transferred
                    bool canTransfer = orderStatus.IsIn(OrderStatus.STATUS.PickedUp)
                        && !DBHelper.ToBoolean((e.Item.DataItem as DataRowView)["IsTransfer"]);
                    (e.Item as GridEditableItem)["Driver"].FindControl("linkTransfer").Visible = canTransfer;
                    (e.Item as GridEditableItem)["Driver"].FindControl("lblDriver").Visible = !canTransfer;
                }

                if (orderStatus == OrderStatus.STATUS.Delivered || orderStatus == OrderStatus.STATUS.Audited)
                {
                    // Disable button for editing when delivered orders are shown
                    Button cmdEdit = (Button)e.Item.FindControl("cmdEdit");
                    cmdEdit.Visible = false;
                }
                else if (orderStatus != OrderStatus.STATUS.Assigned)
                {
                    Button cmdEdit = (Button)e.Item.FindControl("cmdEdit");
                    cmdEdit.Text = "Details";
                }

                // H2S alert
                if (h2s)
                {
                    ((Label)e.Item.FindControl("lblH2S")).Text = " H2S ";
                    ((Label)e.Item.FindControl("lblH2S")).CssClass += " H2S";
                }
                // Overdue alert (past due date and not yet picked up)
                if (DueDate < DateTime.Today && orderStatus.IsIn(OrderStatus.STATUS.PickedUp))
                {
                    ((Label)e.Item.FindControl("lblDueDate")).CssClass += " H2S";
                }

                Color backColor = Color.Transparent, foreColor = Color.Black;
                switch (orderStatus)
                {
                    case OrderStatus.STATUS.Declined: 
                        backColor = Color.Salmon;
                        break;
                }
                if (backColor != Color.Transparent)
                    e.Item.BackColor = backColor;
                if (foreColor != Color.Black)
                    e.Item.ForeColor = foreColor;
            }
        }

        protected void grid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "ExportToExcel")
            {
                e.Canceled = true;
                ExportGrid();
            }
            else if (e.CommandName == "Refresh")
            {
                rgMain.Rebind();
            }
        }

        protected void cmdExport_Click(object sender, EventArgs e)
        {
            ExportGrid();
        }

        private void ExportGrid()
        {
            string[] skipColumns = { "ActionColumn" };

            string filename = string.Format("Dispatched_{0:yyyyMMdd}.xlsx", DateTime.Now.Date);
            MemoryStream ms = new RadGridExcelExporter(visibleColNamesToSkip: skipColumns).ExportSheet(rgMain.MasterTableView);
            Response.ExportExcelStream(ms, filename);
        }

        protected void cmdSaveLayout_Click(object source, EventArgs e)
        {
            rpmMain.SaveState();
            rgMain.Rebind();
        }

        protected void cmdResetLayout_Click(object source, EventArgs e)
        {
            DbStorageProvider.ResetStateInStorage(rpmMain.StorageProviderKey);
            Response.Redirect(Request.Url.ToString());
        }

    }
}