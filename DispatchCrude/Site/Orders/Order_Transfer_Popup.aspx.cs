﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DispatchCrude.Core;
using AlonsIT;
using Telerik.Web.UI;
using System.Data;

namespace DispatchCrude.Site.Orders
{
    public partial class Order_Transfer_Popup : System.Web.UI.Page
    {
        bool reqMiles = Settings.SettingsID.RequireTruckMileage.AsBool();

        protected void Page_Load(object sender, EventArgs e)
        {
            // Filter destinations to those in user's profile terminal
            int terminalID;
            if (Context.Profile.GetPropertyValue("TerminalID") != null
                && (terminalID = DBHelper.ToInt32(Context.Profile.GetPropertyValue("TerminalID"))) != 0)
            {
                hfRestrictedTerminalID.Value = terminalID.ToString();
            }

            if (!IsPostBack)
            {
                int orderID = DBHelper.ToInt32(Request.QueryString["ID"]);
                if (orderID == 0)
                {
                    // TODO: show an appropriate message and redirect back to the Default page??
                }
                else
                {
                    GetOrderDataBinder(orderID, "1,2,7,8").DataRowToControls(this);
                    this.Page.Title = "Transfer Order #" + this.hiddenOrderNum.Value;
                }
            }
        }

        static private App_Code.SingleRowDataBinder GetOrderDataBinder(int id, string statusIDCSVFilter = "2,3,7,8")
        {
            if (statusIDCSVFilter.Length > 0)
                statusIDCSVFilter = string.Format("StatusID IN ({0}) AND ", statusIDCSVFilter);
            return new App_Code.SingleRowDataBinder(string.Format("SELECT * FROM viewOrder WHERE {0} ID=@ID AND DeleteDateUTC IS NULL", statusIDCSVFilter), "tblOrder", id);
        }

        protected void EntryValueChanged(object sender, EventArgs e)
        {
            cmdTransfer.Enabled = rcbNewDriver.SelectedIndex != 0;
        }

        protected void rcbNewDriver_ItemDataBound(object sender, RadComboBoxItemEventArgs e)
        {
            if (e.Item.DataItem is DataRowView)
            {
                if ((e.Item.DataItem as DataRowView)["DriverScore"].ToString() == "0")
                {
                    e.Item.Enabled = false;
                    e.Item.Text += " [UNAVAILABLE]";
                }
            }
        }

        protected void cmdTransfer_Click(object sender, EventArgs e)
        {
            rfvNewDriver.Text = "";
            rfvOriginTruckEndMileage.Text = "";
            rfvPctComplete.Text = "";

            if (rcbNewDriver.SelectedItem.Text == txtCurrentDriver.Text)
            {
                rfvNewDriver.Text = "Cannot set new driver to the existing driver";
            } 
            else if (reqMiles && String.IsNullOrEmpty(txtOriginTruckEndMileage.Text))
            {
                rfvOriginTruckEndMileage.Text = "Current truck mileage is required";
            }
            // ensure that truck mileage has increased
            else if ((reqMiles) && (DBHelper.ToInt32(txtOriginTruckEndMileage.Text) <= DBHelper.ToInt32(hfOriginTruckEndMileage.Value)))
            {
                rfvOriginTruckEndMileage.Text = "Invalid mileage (must be greater than current mileage on record)";
            }
            else if ((String.IsNullOrEmpty(txtPctComplete.Text) || DBHelper.ToInt32(txtPctComplete.Text) < 0 || DBHelper.ToInt32(txtPctComplete.Text) > 100))
            {
                // require a percentage to be set
                rfvPctComplete.Text = "Percentage must be set";
            }
            else
            {
                // do the transfer
                using (SSDB db = new SSDB())
                {
                    using (System.Data.SqlClient.SqlCommand cmd = db.BuildCommand("spOrderTransfer"))
                    {
                        cmd.Parameters["@OrderID"].Value = DBHelper.ToInt32(Request.QueryString["ID"]);
                        cmd.Parameters["@DestDriverID"].Value = DBHelper.ToInt32(rcbNewDriver.SelectedValue);
                        if (ddlNewTruck.SelectedIndex > 0)
                            cmd.Parameters["@DestTruckID"].Value = DBHelper.ToInt32(ddlNewTruck.SelectedValue);
                        cmd.Parameters["@OriginTruckEndMileage"].Value = DBHelper.ToInt32(txtOriginTruckEndMileage.Text);
                        cmd.Parameters["@PercentComplete"].Value = DBHelper.ToInt32(txtPctComplete.Text);
                        cmd.Parameters["@Notes"].Value = txtNotes.Text;
                        cmd.Parameters["@UserName"].Value = User.Identity.Name;
                        cmd.ExecuteNonQuery();
                    }
                }
                // disable the "edit" buttons (forcing the user to close the dialog manually)         
                rcbNewDriver.Enabled = cmdTransfer.Enabled = false;
                //server code to extract and process the reply text here
                ScriptManager.RegisterStartupScript(this, GetType(), "closeWnd", "CloseWnd();", true);
            }
        }
    }
}