﻿using System;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//add for sql stuff
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using Telerik.Web.UI;
using AlonsIT;
using DispatchCrude.App_Code;
using DispatchCrude.Core;
using DispatchCrude.Extensions;

namespace DispatchCrude.Site.Orders
{
    public partial class Orders_Approve : System.Web.UI.Page
    {
        static private string APPROVE = "Approve", UNAPPROVE = "Approved";

        protected void Page_Init(object sender, EventArgs e)
        {
            rdpStartDate.DbSelectedDate = DateTime.Now.Date.AddDays(1 - DateTime.Now.Day);
            rdpEndDate.DbSelectedDate = DateTime.Now.Date.AddDays(-DateTime.Now.Day).AddMonths(1);
            rdpStartDate.Calendar.ShowRowHeaders = rdpEndDate.Calendar.ShowRowHeaders = false;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ConfigureAjax(!Settings.SettingsID.DisableAJAX.AsBool());

            //Print tab navigation html to page
            tabmenu.InnerHtml = NavigationHelper.PrintTabArray(NavigationHelper.TabSet_Dispatch, "Tab_Approval").ToString();

            if (!IsPostBack)
            {
                //Default the Terminal to the setting in the current user's profile (if available and valid)
                int terminalID;
                if (Context.Profile.GetPropertyValue("TerminalID") != null && (terminalID = DBHelper.ToInt32(Context.Profile.GetPropertyValue("TerminalID"))) != 0)
                {
                    this.rcbTerminal.DataBind();
                    RadComboBoxHelper.SetSelectedValue(this.rcbTerminal, terminalID);

                    //If the user's terminal is set, filtering is assumed to be "ON" so disable the terminal selection box
                    if (terminalID > 0)
                        rcbTerminal.Enabled = false;
                }
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            // Hide ID column on the website
            rgMain.MasterTableView.Columns.FindByUniqueName("ID").Display = false;

            if (!IsPostBack)
            {
            }
        }

        private void ConfigureAjax(bool enabled = true)
        {
            if (enabled)
            {
                RadAjaxHelper.AddAjaxSetting(this.Page, btnRefresh, rgMain, true);
                RadAjaxHelper.AddAjaxSetting(this.Page, rgMain, rgMain);
            }
            RadAjaxManager.GetCurrent(this.Page).EnableAJAX = enabled;
        }

        protected override void RaisePostBackEvent(IPostBackEventHandler source, string eventArgument)
        {
            base.RaisePostBackEvent(source, eventArgument);

            if (source == rgMain && eventArgument.Contains("|"))
            {
                int id = DBHelper.ToInt32(eventArgument.Split('|')[1]);
                switch (eventArgument.Split('|')[0].ToLower())
                {
                    case "edit":
                        // force the appropriate row into EDIT mode
                        foreach (GridItem item in rgMain.Items)
                        {
                            if (item is GridDataItem && RadGridHelper.GetGridItemID(item) == id)
                            {
                                item.FireCommandEvent("Edit", null);
                                break;
                            }
                        }
                        break;
                    case "approve":
                        using (SSDB db = new SSDB())
                        {
                            using (SqlCommand cmd = db.BuildCommand("spApproveOrder"))
                            {
                                cmd.Parameters["@ID"].Value = id;
                                cmd.Parameters["@UserName"].Value = UserSupport.UserName;
                                cmd.ExecuteNonQuery();
                            }
                            RemoveIdFromCache(id);
                            rgMain.Rebind();
                        }
                        break;
                    case "refresh":
                        using (SSDB db = new SSDB())
                        {
                            // re-apply rates to the specified order
                            using (System.Data.SqlClient.SqlCommand cmd = db.BuildCommand("spApplyRatesBoth"))
                            {
                                cmd.Parameters["@ID"].Value = id;
                                cmd.Parameters["@UserName"].Value = UserSupport.UserName;
                                cmd.ExecuteNonQuery();
                            }
                            // clear the cache so it will requery the data (from the database)
                            using (SqlCommand cmd = db.BuildCommand("spOrderApprovalSource"))
                            {
                                string cacheKey = GetCacheKey(cmd);
                                if (Cache[cacheKey] != null)
                                    Cache.Remove(cacheKey);
                            }
                        }
                        rgMain.Rebind();
                        break;
                }
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {   // setting this to TRUE will ensure that the Refresh button will always clear the cache (and reload from DB)
            _noCache = true;
            rgMain.Rebind();
        }
        private bool _noCache = false;
        protected void grid_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            (sender as RadGrid).DataSource = GetData(noCache: _noCache);
            _noCache = false;
        }
        private string GetCacheKey(SqlCommand cmd)
        {
            string ret = string.Empty;

            cmd.Parameters["@UserName"].Value = UserSupport.UserName;
            cmd.Parameters["@ShipperID"].Value = RadComboBoxHelper.SelectedValue(rcbShipper, -1);
            cmd.Parameters["@CarrierID"].Value = RadComboBoxHelper.SelectedValue(rcbCarrier, -1);
            cmd.Parameters["@ProductGroupID"].Value = RadComboBoxHelper.SelectedValue(rcbProductGroup, -1);
            cmd.Parameters["@OriginStateID"].Value = RadComboBoxHelper.SelectedValue(rcbOriginState, -1);
            cmd.Parameters["@DestStateID"].Value = RadComboBoxHelper.SelectedValue(rcbDestState, -1);
            cmd.Parameters["@TerminalID"].Value = RadComboBoxHelper.SelectedValue(rcbTerminal, -1);
            cmd.Parameters["@RegionID"].Value = RadComboBoxHelper.SelectedValue(rcbRegion, -1);
            //cmd.Parameters["@TerminalID"].Value = (Context.Profile.GetPropertyValue("TerminalID") == null) ? "-1" :
            //                    DBHelper.ToInt32(Context.Profile.GetPropertyValue("TerminalID")).ToString();

            //Context.Profile.GetPropertyValue("TerminalID").ToString();

            cmd.Parameters["@Start"].Value = rdpStartDate.DbSelectedDate;
            cmd.Parameters["@End"].Value = rdpEndDate.DbSelectedDate;
            cmd.Parameters["@IncludeApproved"].Value = chkIncludeApproved.Checked;

            foreach (SqlParameter param in cmd.Parameters)
            {
                if ((param.Direction | ParameterDirection.Input) == ParameterDirection.Input)
                    ret += param.ParameterName + "=" + param.Value.ToString();
            }

            return ret;
        }

        private DataTable GetData(bool cacheOnly = false, bool noCache = false)
        {
            DataTable ret = null;
            bool selectIDNullsToZero = true;
            using (SSDB db = new SSDB())
            {
                using (SqlCommand cmd = db.BuildCommand("spOrderApprovalSource"))
                {
                    string cacheKey = GetCacheKey(cmd);
                    if (noCache && Cache[cacheKey] != null)
                        Cache.Remove(cacheKey);
                    if (!cacheOnly && Cache[cacheKey] == null || !(Cache[cacheKey] is DataTable))
                    {
                        ret = Core.DateHelper.AddLocalRowStateDateFields(SSDB.GetPopulatedDataTable(cmd), HttpContext.Current);
                        ret.Columns.Add("ImportAction");
                        foreach (DataRow row in ret.Rows)
                            row["ImportAction"] = "None";
                        ret.Columns.Add("ImportOutcome");
                        if (selectIDNullsToZero)
                            ret = DispatchCrude_ConnFactory.SetIDNullsToZero(ret);
                        Cache.Insert(cacheKey, ret, null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(30));
                    }
                    else
                        ret = Cache[cacheKey] as DataTable;
                }
            }
            return ret;
        }


        protected void grid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.UpdateCommandName)
            {
                ClearCache();
                (sender as RadGrid).Rebind();
            }
            else if (e.CommandName == APPROVE)
            {
                e.Canceled = true;
                RemoveIdFromCache(RadGridHelper.GetGridItemID(e.Item));
                (sender as RadGrid).Rebind();
            } 
        }

        protected void grid_ItemCreated(object sender, GridItemEventArgs e)
        {
        }

        protected void grid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem && e.Item.DataItem is DataRowView && AlonsIT.DBHelper.ToBoolean((e.Item.DataItem as DataRowView)["Approved"]))
            {
                RadButton btn = App_Code.RadGridHelper.GetControlByType((e.Item as GridDataItem)["ActionColumn"], typeof(RadButton), false, "btnApprove") as RadButton;
                if (btn != null)
                {
                    btn.Text = UNAPPROVE;
                    btn.ForeColor = Color.DarkGray;
                }
            }
            // display tooltips for origin and dest minutes if the wait reason is set
            if (e.Item is GridDataItem)
            {
                GridDataItem griditem = (GridDataItem)e.Item;
                if (!DBHelper.IsNull(DataBinder.Eval(griditem.DataItem, "OriginWaitReason")))
                {
                    griditem["OriginMinutes"].ToolTip = DataBinder.Eval(griditem.DataItem, "OriginWaitReason").ToString();
                    if (!DBHelper.IsNull(DataBinder.Eval(griditem.DataItem, "OriginWaitNotes")))
                        griditem["OriginMinutes"].ToolTip += " - " + DataBinder.Eval(griditem.DataItem, "OriginWaitNotes");
                }

                if (!DBHelper.IsNull(DataBinder.Eval(griditem.DataItem, "DestWaitReason")))
                {
                    griditem["DestMinutes"].ToolTip = DataBinder.Eval(griditem.DataItem, "DestWaitReason").ToString(); 
                    if (!DBHelper.IsNull(DataBinder.Eval(griditem.DataItem, "DestWaitNotes")))
                        griditem["DestMinutes"].ToolTip += " - " + DataBinder.Eval(griditem.DataItem, "DestWaitNotes");
                }
            }
        }

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            switch ((sender as RadButton).CommandName.ToLower())
            {
                case "approve":
                    using (SSDB db = new SSDB())
                    {
                        using (SqlCommand cmd = db.BuildCommand("spApproveOrder"))
                        {
                            cmd.Parameters["@ID"].Value = (sender as RadButton).Value;
                            cmd.Parameters["@UserName"].Value = UserSupport.UserName;
                            cmd.ExecuteNonQuery();
                        }
                        RemoveIdFromCache(DBHelper.ToInt32((sender as RadButton).Value));
                        rgMain.Rebind();
                    }
                    break;
                case "edit":
                    GridEditableItem gdi = (sender as Control).Parent.Parent as GridEditableItem;
                    if (gdi != null)
                    {
                        gdi.FireCommandEvent("Edit", String.Empty);
                    }
                    // navigate to a new page (or show a popup) allowing edit of the overrides on that order
                    break;
                case "refresh":
                    using (SSDB db = new SSDB())
                    {
                        // re-apply rates to the specified order
                        using (System.Data.SqlClient.SqlCommand cmd = db.BuildCommand("spApplyRatesBoth"))
                        {
                            cmd.Parameters["@ID"].Value = (sender as RadButton).Value;
                            cmd.Parameters["@UserName"].Value = UserSupport.UserName;
                            cmd.ExecuteNonQuery();
                        }
                        // clear the cache so it will requery the data (from the database)
                        ClearCache(db);
                    }
                    rgMain.Rebind();
                    break;
            }
        }

        private void ClearCache()
        {
            using (SSDB db = new SSDB())
            {
                ClearCache(db);
            }
        }
        private void ClearCache(SSDB db)
        {
            // clear the cache so it will requery the data (from the database)
            using (SqlCommand cmd = db.BuildCommand("spOrderApprovalSource"))
            {
                string cacheKey = GetCacheKey(cmd);
                if (Cache[cacheKey] != null)
                    Cache.Remove(cacheKey);
            }
        }
        protected void RemoveIdFromCache(int id)
        {
            DataTable cachedData = GetData(true);
            if (cachedData != null)
            {
                foreach (DataRow dr in GetData(true).Select(string.Format("ID={0}", id)))
                    dr.Delete();
                cachedData.AcceptChanges();
            }
        }
        private void UpdateCacheRecord(int id
            , int? overrideActualMiles, int? overrideOriginMinutes, int? overrideDestMinutes
            , bool? overrideH2S, bool? overrideOriginChainup, bool? overrideDestChainup, bool? overrideReroute, decimal? overrideTransferPercentComplete
            , decimal? overrideCarrierMinSettlementUnits, decimal? overrideShipperMinSettlementUnits
            , string approvalNotes)
        {
            DataTable cachedData = GetData(true);
            foreach (DataRow dr in cachedData.Select(string.Format("ID={0}", id)))
            {
                dr["OverrideActualMiles"] = overrideActualMiles.HasValue ? (object)overrideActualMiles.Value : DBNull.Value;
                dr["FinalActualMiles"] = overrideActualMiles.HasValue ? overrideActualMiles.Value : dr["ActualMiles"];
                dr["OverrideOriginMinutes"] = overrideOriginMinutes.HasValue ? (object)overrideOriginMinutes.Value : DBNull.Value;
                dr["FinalOriginMinutes"] = overrideOriginMinutes.HasValue ? overrideOriginMinutes.Value : dr["OriginMinutes"];
                dr["OverrideDestMinutes"] = overrideDestMinutes.HasValue ? (object)overrideDestMinutes.Value : DBNull.Value;
                dr["FinalDestMinutes"] = overrideDestMinutes.HasValue ? overrideDestMinutes.Value : dr["DestMinutes"];
                dr["OverrideOriginChainup"] = overrideOriginChainup.HasValue ? (object)overrideOriginChainup.Value : DBNull.Value;
                dr["FinalOriginChainup"] = overrideOriginChainup.HasValue ? overrideOriginChainup.Value : dr["OriginChainup"];
                dr["OverrideDestChainup"] = overrideDestChainup.HasValue ? (object)overrideDestChainup.Value : DBNull.Value;
                dr["FinalDestChainup"] = overrideDestChainup.HasValue ? overrideDestChainup.Value : dr["DestChainup"];
                dr["OverrideH2S"] = overrideH2S.HasValue ? (object)overrideH2S.Value : DBNull.Value;
                dr["FinalH2S"] = overrideH2S.HasValue ? overrideH2S.Value : dr["H2S"];
                dr["OverrideReroute"] = overrideReroute.HasValue ? (object)overrideReroute.Value : DBNull.Value;
                dr["FinalRerouted"] = overrideReroute.HasValue ? overrideReroute.Value : dr["Rerouted"];
                dr["OverrideTransferPercentComplete"] = overrideTransferPercentComplete.HasValue ? (object)overrideTransferPercentComplete.Value : DBNull.Value;
                dr["FinalTransferPercentComplete"] = overrideTransferPercentComplete.HasValue ? overrideTransferPercentComplete.Value : dr["TransferPercentComplete"];
                dr["CarrierMinSettlementUnits"] = overrideCarrierMinSettlementUnits.HasValue ? (object)overrideCarrierMinSettlementUnits.Value : DBNull.Value;
                dr["FinalCarrierMinSettlementUnits"] = overrideCarrierMinSettlementUnits.HasValue ? overrideCarrierMinSettlementUnits.Value : dr["CarrierMinSettlementUnits"];
                dr["ShipperMinSettlementUnits"] = overrideShipperMinSettlementUnits.HasValue ? (object)overrideShipperMinSettlementUnits.Value : DBNull.Value;
                dr["FinalShipperMinSettlementUnits"] = overrideShipperMinSettlementUnits.HasValue ? overrideShipperMinSettlementUnits.Value : dr["ShipperMinSettlementUnits"];
                dr["Notes"] = approvalNotes;
                dr.AcceptChanges();
            }
        }

        protected void cmApprove_ItemClick(object sender, RadMenuEventArgs e)
        {
            switch (e.Item.Text)
            {
                case "Edit":
                    GridEditableItem gdi = (sender as RadMenu).Parent.Parent as GridEditableItem;
                    if (gdi != null)
                    {
                        gdi.FireCommandEvent("Edit", String.Empty);
                    }
                    // navigate to a new page (or show a popup) allowing edit of the overrides on that order
                    break;
                case "Refresh":
                    using (SSDB db = new SSDB())
                    {
                        // re-apply rates to the specified order
                        using (System.Data.SqlClient.SqlCommand cmd = db.BuildCommand("spApplyRatesBoth"))
                        {
                            cmd.Parameters["@ID"].Value = RadGridHelper.GetGridItemID((sender as RadMenu).Parent.Parent as GridItem);
                            cmd.Parameters["@UserName"].Value = UserSupport.UserName;
                            cmd.ExecuteNonQuery();
                        }
                        // clear the cache so it will requery the data (from the database)
                        ClearCache(db);
                    }
                    rgMain.Rebind();
                    break;
            }
        }

        protected void cmdExport_Click(object sender, EventArgs e)
        {
            ExportGridToExcel();
        }

        protected void cvUpload_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = Path.GetExtension(excelUpload.FileName).ToLower() == ".xlsx";
        }

        protected void cmdImport_Click(object sender, EventArgs e)
        {
            Page.Validate();
            if (Page.IsValid)
            {
                FinancialImporter fi = new FinancialImporter();
                fi.AddSpec(rgMain, "ID", FinancialImporter.FISpec.FISType.ID);
                fi.AddSpec(rgMain, "OverrideActualMiles", FinancialImporter.FISpec.FISType.UPDATE);
                fi.AddSpec(rgMain, "OverrideOriginMinutes", FinancialImporter.FISpec.FISType.UPDATE);
                fi.AddSpec(rgMain, "OverrideDestMinutes", FinancialImporter.FISpec.FISType.UPDATE);
                fi.AddSpec(rgMain, "OverrideH2S", FinancialImporter.FISpec.FISType.UPDATE);
                fi.AddSpec(rgMain, "OverrideOriginChainup", FinancialImporter.FISpec.FISType.UPDATE);
                fi.AddSpec(rgMain, "OverrideDestChainup", FinancialImporter.FISpec.FISType.UPDATE);
                fi.AddSpec(rgMain, "OverrideReroute", FinancialImporter.FISpec.FISType.UPDATE);
                fi.AddSpec(rgMain, "OverrideTransferPercentComplete", FinancialImporter.FISpec.FISType.UPDATE);
                fi.AddSpec(rgMain, "OverrideCarrierMinSettlementUnits", FinancialImporter.FISpec.FISType.UPDATE);
                fi.AddSpec(rgMain, "OverrideShipperMinSettlementUnits", FinancialImporter.FISpec.FISType.UPDATE);
                fi.AddSpec(rgMain, "Approved", FinancialImporter.FISpec.FISType.UPDATE);
                fi.AddSpec(rgMain, "ImportAction", FinancialImporter.FISpec.FISType.ACTION);
                fi.AddSpec(rgMain, "ImportOutcome", FinancialImporter.FISpec.FISType.OUTCOME);
                Response.ExportExcelStream(fi.ProcessSP(excelUpload.FileContent, "spUpdateOrderApproval"), Path.GetFileNameWithoutExtension(excelUpload.FileName) + "_ImportResults.xlsx");
            }
        }

        private void ExportGridToExcel()
        {
            string filename = string.Format("Order Approvals as of {0:yyyyMMdd}.xlsx", rdpStartDate.SelectedDate);
            string[] hiddenToInclude = { "ID", "ImportAction", "ImportOutcome", "OverrideActualMiles", "OverrideOriginMinutes", "OverrideDestMinutes", "OverrideH2S", "OverrideChainup", "OverrideReroute", "ApprovalNotesExport"
                                           , "OverrideTransferPercentComplete", "OverrideCarrierMinSettlementUnits", "OverrideShipperMinSettlementUnits"
                                           , "OriginWaitReasonExport", "OriginWaitNotesExport"
                                           , "RejectReasonExport", "RejectNotesExport"
                                           , "RerouteNotesExport"
                                           , "DestWaitReasonExport", "DestWaitNotesExport"}
                , visibleToSkip = { "ActionColumn", "CreateDate", "CreatedByUser", "OriginWaitNotes", "RejectNotes", "RerouteNotes", "DestWaitNotes", "ApprovalNotes" };
            RadGridExcelExporter exporter = new RadGridExcelExporter(
                    hiddenColNamesToInclude: hiddenToInclude
                    , visibleColNamesToSkip: visibleToSkip
                    , dropDownColumnDataValidationList: true);
            //exporter.OnCellBackColorChanged += CellBackColorChanged;
            rgMain.AllowPaging = false;
            rgMain.Rebind();
            Response.ExportExcelStream(exporter.ExportSheet(rgMain.MasterTableView, "Order Approvals"), filename);
        }

    }
}