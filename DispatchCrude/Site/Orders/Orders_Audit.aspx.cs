﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//add for sql stuff
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web.Security;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using Telerik.Web.UI;
using DispatchCrude.App_Code;
using DispatchCrude.Core;
using DispatchCrude.Models;
using AlonsIT.ExtensionMethods;

namespace DispatchCrude.Site.Orders
{
    public partial class Orders_Audit : System.Web.UI.Page
    {

        private const string TVN_CARRIER = "CarrierTableView", TVN_TICKETS = "TicketTableView", TVN_PRODUCT = "ProductTableView"
            , TVN_REROUTES = "ReroutesTableView", TVN_AUDITNOTES = "AuditNotesTableView";

        protected void Page_Init(object sender, EventArgs e)
        {
            //These two statements are required for personalized grid layout 
            //The Key value must be unique across all site pages
            rpmMain.StorageProviderKey = "Audit_rgMain";
            rpmMain.StorageProvider = new DbStorageProvider();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ConfigureAjax(!Core.Settings.SettingsID.DisableAJAX.AsBool());

            //Print tab navigation html to page
            tabmenu.InnerHtml = Extensions.NavigationHelper.PrintTabArray(Extensions.NavigationHelper.TabSet_Dispatch, "Tab_Audit").ToString();

            //Print button navigation html to page
            speedbuttons.InnerHtml = Extensions.NavigationHelper.PrintButtonArray(Extensions.NavigationHelper.ButtonSet_TabAudit, "Button_Audit").ToString();

            // it is necessary to "re-hook" up this event on every Page_Load or it won't trap the event when needed
            dbcMain.GetUnboundChangedValues += new GetUnboundChangedValues(GetUnboundChangedValues);

            if (!IsPostBack)
            {
                // if the user has a persisted grid layout value, load it now
                if (DbStorageProvider.HasStateInStorage(rpmMain.StorageProviderKey)) rpmMain.LoadState();
            }
        }

        private void ConfigureAjax(bool enabled)
        {
            if (enabled)
            {
                RadAjaxHelper.AddAjaxSetting(this.Page, rgMain, rgMain);
            }
        }

        protected void grid_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item.OwnerTableView.Name == TVN_CARRIER)
            {
                if (e.Item.IsInEditMode)
                {
                    // "filter" the dropdowns based on the current CarrierID (and hookup the ddlCarrier_SelectedIndexChanged event)
                    DropDownList ddl = null;
                    GridEditableItem gei = e.Item as GridEditableItem;
                    if (gei != null && gei["CarrierID"].FindControl("ddlCarrier") is DropDownList)
                    {   // update the dependent SqlDataSource CarrierID parameters, and rebind them
                        UpdateCarrierSelectParameter(gei["CarrierID"].FindControl("ddlCarrier"));
                        ddl = gei["CarrierID"].FindControl("ddlCarrier") as DropDownList;
                        // "hookup" the SelectedIndexChanged event (to trap future CarrierID changes)
                        if (ddl != null)
                        {
                            ddl.SelectedIndexChanged += new EventHandler(ddlCarrier_SelectedIndexChanged);
                            RadAjaxHelper.AddAjaxSetting(Page, ddl, rgMain);
                        }
                        // do manual "Binding" of these values, since automatic "binding" can't be used due to the "Rebind" above
                        if (gei.DataItem is DataRowView)
                        {
                            DataRowView rv = gei.DataItem as DataRowView;
                            RadGridHelper.SetColumnDropDownSelectedValue(gei["DriverID"], (rv["DriverID"] ?? "0").ToString(), "ddlDriver");
                            RadGridHelper.SetColumnDropDownSelectedValue(gei["TruckID"], (rv["TruckID"] ?? "0").ToString(), "ddlTruck");
                            RadGridHelper.SetColumnDropDownSelectedValue(gei["TrailerID"], (rv["TrailerID"] ?? "0").ToString(), "ddlTrailer");
                            RadGridHelper.SetColumnDropDownSelectedValue(gei["Trailer2ID"], (rv["Trailer2ID"] ?? "0").ToString(), "ddlTrailer2");
                        }
                    }
                    // "hook up" the SelectedIndexChanged event handler to ddlDriver
                    ddl = e.Item.FindControl("ddlDriver") as DropDownList;
                    if (ddl != null)
                    {
                        ddl.SelectedIndexChanged += new EventHandler(ddlDriver_SelectedIndexChanged);
                        RadAjaxHelper.AddAjaxSetting(Page, ddl, rgMain);
                    }
                }
            }
            else if (e.Item.OwnerTableView.Name == TVN_TICKETS && e.Item.IsInEditMode)
            {
                DropDownList ddl = RadGridHelper.GetColumnDropDown((e.Item as GridEditableItem)["TicketTypeID"], "ddlTicketType");
                if (ddl != null)
                {
                    ddlTicketType_SelectedIndexChanged(ddl, EventArgs.Empty);
                    ddl.SelectedIndexChanged += ddlTicketType_SelectedIndexChanged;
                }
            }
        }

        protected void ddlTicketType_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddl = sender as DropDownList;
            bool enabled = DropDownListHelper.SelectedValue(ddl).IsIn((int)TicketType.TYPE.GaugeRun, (int)TicketType.TYPE.GaugeNet);
            GridEditableItem item = ddl.NamingContainer as GridEditableItem;
            if (item != null)
            {
                (item.FindControl("txtOpeningFeet") as RadNumericTextBox).Enabled = enabled;
                (item.FindControl("txtOpeningInch") as RadNumericTextBox).Enabled = enabled;
                (item.FindControl("txtOpeningQ") as RadNumericTextBox).Enabled = enabled;
                (item.FindControl("txtClosingFeet") as RadNumericTextBox).Enabled = enabled;
                (item.FindControl("txtClosingInch") as RadNumericTextBox).Enabled = enabled;
                (item.FindControl("txtClosingQ") as RadNumericTextBox).Enabled = enabled;
                //(item.FindControl("rfvGrossUnits") as RequiredFieldValidator).Enabled = enabled;
            }
        }

        protected void ddlDriver_SelectedIndexChanged(object sender, EventArgs e)
        {   
            // when a driver is selected, assign their truck/trailer/trailer2 if specified for this driver (default value)
            DropDownList ddlDriver = sender as DropDownList;

            int driverID = Converter.ToInt32((ddlDriver.SelectedValue ?? "0"));
            
            // query the database for the TruckID|TrailerID|Trailer2ID values for the specified Driver (NULLs if not assigned)
            using (AlonsIT.SSDB db = new AlonsIT.SSDB())
            {
                DataTable data = db.GetPopulatedDataTable(
                    "SELECT TruckID, TrailerID, Trailer2ID FROM tblDriver WHERE ID={0}", driverID);
                GridDataItem item = ddlDriver.NamingContainer as GridDataItem;
                int truckID = data.Rows.Count > 0 ? Converter.ToInt32(data.Rows[0]["TruckID"]) : 0
                    , trailerID = data.Rows.Count > 0 ? Converter.ToInt32(data.Rows[0]["TrailerID"]) : 0
                    , trailer2ID = data.Rows.Count > 0 ? Converter.ToInt32(data.Rows[0]["Trailer2ID"]) : 0;

                // update the Truck|Trailer|Trailer2 controls with the values found above
                RadGridHelper.SetColumnDropDownSelectedValue(item["TruckID"], truckID.ToString(), "ddlTruck");
                RadGridHelper.SetColumnDropDownSelectedValue(item["TrailerID"], trailerID.ToString(), "ddlTrailer");
                RadGridHelper.SetColumnDropDownSelectedValue(item["Trailer2ID"], trailer2ID.ToString(), "ddlTrailer2");
            }
        }

        protected void ddlCarrier_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateCarrierSelectParameter(sender);
        }

        private void UpdateCarrierSelectParameter(object sender)
        {
            DropDownList ddl = sender as DropDownList;
            // update the dependent SqlDataSource select parameters
            dsDriver.SelectParameters["CarrierID"].DefaultValue 
                = dsTrailer.SelectParameters["CarrierID"].DefaultValue 
                = dsTruck.SelectParameters["CarrierID"].DefaultValue = ddl.SelectedValue;
            // rebind the controls using these SqlDataSource objects (to refresh them with "filtered" data for this carrierID)
            GridEditableItem item = ddl.NamingContainer as GridEditableItem;
            if (item != null)
            {   // when the Carrier changes, so does this data, so we need to manually "rebind" now (but this prevents automatic binding, see below)
                RadGridHelper.RebindColumnDropDown(item["DriverID"]);
                RadGridHelper.RebindColumnDropDown(item["TruckID"]);
                RadGridHelper.RebindColumnDropDown(item["TrailerID"]);
                RadGridHelper.RebindColumnDropDown(item["Trailer2ID"]);
            }
        }

        private void GetUnboundChangedValues(GridEditableItem item, System.Data.DataRow values)
        {
            if (item.OwnerTableView.Name == TVN_CARRIER)
            {
                // since we aren't using automatic "binding" of these columns, we need to return the updated values here
                values["DriverID"] = RadGridHelper.GetColumnDropDownSelectedValue(item["DriverID"], "ddlDriver", DBNull.Value);
                values["TruckID"] = RadGridHelper.GetColumnDropDownSelectedValue(item["TruckID"], "ddlTruck", DBNull.Value);
                values["TrailerID"] = RadGridHelper.GetColumnDropDownSelectedValue(item["TrailerID"], "ddlTrailer", DBNull.Value);
                values["Trailer2ID"] = RadGridHelper.GetColumnDropDownSelectedValue(item["Trailer2ID"], "ddlTrailer2", DBNull.Value);
            }
            else if (item.OwnerTableView.Name == TVN_PRODUCT)
            {
                // using the MVC model to get the built-in local <-> UTC conversion logic
                using (DispatchCrudeDB db = new DispatchCrudeDB())
                {
                    int id = AlonsIT.DBHelper.ToInt32(values["ID"]);
                    Order order = db.Orders.First(o => o.ID == id);
                    order.OriginArriveTime = (item["OriginArriveTime"].Controls[0] as RadDateTimePicker).SelectedDate;
                    values["OriginArriveTimeUTC"] = order.OriginArriveTimeUTC;
                    order.OriginDepartTime = (item["OriginDepartTime"].Controls[0] as RadDateTimePicker).SelectedDate;
                    values["OriginDepartTimeUTC"] = order.OriginDepartTimeUTC;
                    order.DestArriveTime = (item["DestArriveTime"].Controls[0] as RadDateTimePicker).SelectedDate;
                    values["DestArriveTimeUTC"] = order.DestArriveTimeUTC;
                    order.DestDepartTime = (item["DestDepartTime"].Controls[0] as RadDateTimePicker).SelectedDate;
                    values["DestDepartTimeUTC"] = order.DestDepartTimeUTC;
                }
            }
        }

        private DataTable _dtDetailData = null;
        protected void grid_DetailTableDataBind(object sender, GridDetailTableDataBindEventArgs e)
        {
            using (AlonsIT.SSDB db = new AlonsIT.SSDB())
            {
                if (e.DetailTableView.Name == TVN_TICKETS)
                {
                    DataTable data = db.GetPopulatedDataTable("SELECT * FROM dbo.viewOrderTicket WHERE OrderID={0}", e.DetailTableView.ParentItem.GetDataKeyValue("ID"));
                    e.DetailTableView.DataSource = Core.DateHelper.AddLocalRowStateDateFields(data);
                }
                else if (e.DetailTableView.Name == TVN_REROUTES)
                {
                    DataTable data = db.GetPopulatedDataTable("SELECT * FROM dbo.viewOrderReroute WHERE OrderID={0}", e.DetailTableView.ParentItem.GetDataKeyValue("ID"));
                    e.DetailTableView.DataSource = Core.DateHelper.AddLocalRowStateDateFields(data);
                }
                else
                {
                    if (_dtDetailData == null)
                    {
                        using (SqlCommand cmd = db.BuildCommand("SELECT * FROM fnOrders_AllTickets_Audit(-1, -1, -1, NULL, @ID)"))
                        {
                            cmd.Parameters.AddWithValue("@ID", e.DetailTableView.ParentItem.GetDataKeyValue("ID"));
                            _dtDetailData = AlonsIT.SSDB.GetPopulatedDataTable(cmd);
                            _dtDetailData = Core.DateHelper.AddLocalRowStateDateFields(_dtDetailData);
                        }
                    }
                    e.DetailTableView.DataSource = _dtDetailData;
                }
            }
        }

        protected void grid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "ExpandAll")
            {
                e.Item.Expanded = true;
            }
            else if (e.CommandName == "Refresh")
            {
                rgMain.Rebind();
            }
            else if (e.Item is GridDataItem && (e.CommandName == "Revert" || e.CommandName == "Done" || e.CommandName == "SaveAll"))
            {
                if (e.CommandName == "Revert")
                {
                    int id = RadGridHelper.GetGridItemID(e.Item as GridDataItem);
                    using (AlonsIT.SSDB db = new AlonsIT.SSDB())
                    {
                        db.ExecuteSql(
                            // for non-rejected loads, simply mark the order as Finalized (not completed) so it will be displayed in the MVC pages
                            "UPDATE tblOrder SET DeliverPrintStatusID={0}, LastChangeDateUTC=getutcdate() WHERE ID={2};"
                            // for Rejected orders, we should also revert the status back to PICKED UP, and Finalized (not completed)
                            + " UPDATE tblOrder SET StatusID={1}, PickupPrintStatusID={0}, AcceptLastChangeDateUTC=getutcdate() WHERE ID={2} AND Rejected = 1;"
                            , (int)PrintStatus.STATUS.Finalized
                            , (int)OrderStatus.STATUS.Accepted
                            , id);
                    }
                    Response.Redirect(string.Format("~/Orders/Details/{0}", id));
                }
                else if (!Page.IsValid)
                    e.Canceled = true;
                else
                {
                    GridDataItem item = e.Item as GridDataItem;
                    foreach (GridTableView gtv in item.ChildItem.NestedTableViews)
                    {
                        foreach (GridItem gi in gtv.Items)
                        {
                            if (gi.IsInEditMode && gi is GridEditableItem)
                                SaveDetailTable(gi);
                        }
                    }
                    item.OwnerTableView.ClearChildEditItems();
                    item.Expanded = false;

                    if (e.CommandName == "Done")
                    {
                        // mark the record as Audited (Completed)
                        using (AlonsIT.SSDB db = new AlonsIT.SSDB())
                        {
                            db.ExecuteSql("UPDATE tblOrder SET StatusID={0}, LastChangeDateUTC=getutcdate(), LastChangedByUser={1} WHERE ID={2}"
                                , (int)OrderStatus.STATUS.Audited
                                , AlonsIT.DBHelper.QuoteStr(UserSupport.UserName)
                                , RadGridHelper.GetGridItemID(item));
                        }
                    }
                    rgMain.Rebind();
                }
            }
            else if (e.CommandName == "EditDetails")
            {
                e.Item.Edit = true;
                e.Item.OwnerTableView.Rebind();
            }
            else if (e.CommandName == "UpdateDetails" && e.Item is GridEditableItem)
            {
                if (Page.IsValid)
                {
                    SaveDetailTable(e.Item);
                    e.Item.Edit = false;
                    e.Item.OwnerTableView.Rebind();
                }
                else
                    e.Canceled = true;
            }
            else if (e.CommandName == "CancelDetails")
            {
                e.Item.Edit = false;
                e.Item.OwnerTableView.Rebind();
            }
            else if (e.CommandName == "InsertDetails")
            {
            }
            else if (e.CommandName == "DeleteDetails")
            {
            }
        }

        private void SaveDetailTable(GridItem item)
        {
            if (item is GridEditableItem)
            {
                switch ((item as GridEditableItem).OwnerTableView.Name)
                {
                    case TVN_CARRIER:
                    case TVN_PRODUCT:
                    case TVN_AUDITNOTES:
                        DBSave_RadGrid rgHelper = new DBSave_RadGrid(dbcMain.UpdateTableName);
                        rgHelper.GetUnboundChangedValues += GetUnboundChangedValues;
                        rgHelper.UpdateGridRow(item as GridEditableItem);
                        break;
                    case TVN_TICKETS:
                        new DBSave_RadGrid("tblOrderTicket").UpdateGridRow(item as GridEditableItem);
                        break;
                }
            }
        }

        protected void GaugeRunValueChanged(object sender, EventArgs e)
        {
            CalculateBarrels(sender, e);
        }

        protected void GrossUnitsChanged(object sender, EventArgs e)
        {
            if (sender is RadNumericTextBox && (sender as RadNumericTextBox).NamingContainer is GridDataItem)
            {
                double barrels = (sender as RadNumericTextBox).Value.HasValue ? (sender as RadNumericTextBox).Value.Value : 0;
                CalculateNetUnits((sender as RadNumericTextBox).NamingContainer as GridDataItem, barrels);
            }
        }


        protected void CalculateBarrels(object sender, EventArgs e)
        {
            GridDataItem item = (sender as WebControl).NamingContainer as GridDataItem;
            int openingFeet = GetItemValueInt(item, "txtOpeningFeet", -1)
                , openingInch = GetItemValueInt(item, "txtOpeningInch", -1)
                , openingQ = GetItemValueInt(item, "txtOpeningQ", -1)
                , closingFeet = GetItemValueInt(item, "txtClosingFeet", -1)
                , closingInch = GetItemValueInt(item, "txtClosingInch", -1)
                , closingQ = GetItemValueInt(item, "txtClosingQ", -1);
            if (openingFeet != -1 && openingInch != -1 && openingQ != -1 && closingFeet != -1 && closingInch != -1 && closingQ != -1)
            {
                using (AlonsIT.SSDB ssdb = new AlonsIT.SSDB())
                {
                    double barrels = Converter.ToDouble(ssdb.QuerySingleValue(
                        "SELECT dbo.fnTankQtyBarrelsDelta({0}, {1}, {2}, {3}, {4}, {5}, {6})"
                        , openingFeet, openingInch, openingQ, closingFeet, closingInch, closingQ, 2));
                    (item.FindControl("txtGrossUnits") as RadNumericTextBox).Value = barrels;
                    CalculateNetUnits(item, barrels);
                }
            }
            else
                (item.FindControl("txtNetUnits") as RadNumericTextBox).Text = (item.FindControl("txtGrossUnits") as RadNumericTextBox).Text = "";
        }

        private void CalculateNetUnits(GridDataItem item, double barrels)
        {
            double obsTemp = GetItemValueDouble(item, "txtObsTemp", -1)
                , obsGravity = GetItemValueDouble(item, "txtObsGravity", -1)
                , bsw = GetItemValueDouble(item, "txtBSW", -1);
            if (obsTemp != -1 && obsGravity != -1 && bsw != -1)
            {
                using (AlonsIT.SSDB ssdb = new AlonsIT.SSDB())
                {
                    using (System.Data.SqlClient.SqlCommand cmd = ssdb.BuildCommand("spCrudeNetCalculator"))
                    {
                        cmd.Parameters["@GrossQty"].Value = barrels;
                        cmd.Parameters["@ObsTemp"].Value = obsTemp;
                        cmd.Parameters["@ObsGravity"].Value = obsGravity;
                        cmd.Parameters["@BSW"].Value = bsw;
                        cmd.ExecuteNonQuery();
                        (item.FindControl("txtNetUnits") as RadNumericTextBox).Text = AlonsIT.DBHelper.IsNull(cmd.Parameters["@NetQty"]) ? "" : cmd.Parameters["@NetQty"].Value.ToString();
                    }
                }
            }
            else
                (item.FindControl("txtNetUnits") as RadNumericTextBox).Text = "";
        }

        private int GetItemValueInt(GridDataItem item, string textBoxName, int defaultValue = 0)
        {
            return Converter.ToInt32(GetItemValue(item, textBoxName), defaultValue);
        }
        private double GetItemValueDouble(GridDataItem item, string textBoxName, double defaultValue = 0)
        {
            return Converter.ToDouble(GetItemValue(item, textBoxName), defaultValue);
        }
        private string GetItemValue(GridDataItem item, string textBoxName)
        {
            if (item.FindControl(textBoxName) is TextBox)
            {
                TextBox txtBox = item.FindControl(textBoxName) as TextBox;
                return txtBox.Text;
            }
            else if (item.FindControl(textBoxName) is Telerik.Web.UI.RadNumericTextBox)
            {
                Telerik.Web.UI.RadNumericTextBox txtBox = item.FindControl(textBoxName) as Telerik.Web.UI.RadNumericTextBox;
                return txtBox.Text;
            }
            return string.Empty;
        }

        protected void chkRejected_CheckChanged(object sender, EventArgs e)
        {   // iterate over every sibling child item and recursively enable/disable their validators
            bool enabled = !(sender as CheckBox).Checked;
            foreach (GridTableView gtv in ((sender as CheckBox).NamingContainer as GridEditableItem).OwnerTableView.ParentItem.ChildItem.NestedTableViews)
            {
                foreach (GridItem item in gtv.GetItems(GridItemType.EditItem))
                {
                    ValidatorHelper.EnableValidators(item, enabled);
                }
            }
        }

        protected void crfvDate_ServerValidate(object source, ServerValidateEventArgs args)
        {
            CustomValidator cv = source as CustomValidator;
            args.IsValid = (cv.NamingContainer.FindControl(cv.ControlToValidate) as RadDatePicker).SelectedDate.HasValue;
        }

        protected void crfvTime_ServerValidate(object source, ServerValidateEventArgs args)
        {
            CustomValidator cv = source as CustomValidator;
            args.IsValid = (cv.NamingContainer.FindControl(cv.ControlToValidate) as RadTimePicker).SelectedDate.HasValue;
        }

        protected void cmdSaveLayout_Click(object source, EventArgs e)
        {
            rpmMain.SaveState();
            rgMain.Rebind();
        }

        protected void cmdResetLayout_Click(object source, EventArgs e)
        {
            DbStorageProvider.ResetStateInStorage(rpmMain.StorageProviderKey);
            Response.Redirect(Request.Url.ToString());
        }

    }
}