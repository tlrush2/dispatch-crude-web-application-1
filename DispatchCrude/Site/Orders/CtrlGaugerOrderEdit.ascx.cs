﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using DispatchCrude.App_Code;
using AlonsIT;
using DispatchCrude.Core;

namespace DispatchCrude.Orders
{
    public partial class CtrlGaugerOrderEdit : System.Web.UI.UserControl
    {
        private object _dataItem = null;

        protected void Page_Load(object sender, System.EventArgs e)
        {
            if (DataItem != null && DataItem is DataRowView)
            {
                DataRowView dv = DataItem as DataRowView;

                hfID.Value = dv["OrderID"].ToString();
                App_Code.DropDownListHelper.Rebind(ddPriority, dv["PriorityID"]);
                App_Code.RadComboBoxHelper.Rebind(rcbGaugerStatus, dv["StatusID"]);
                App_Code.RadComboBoxHelper.Rebind(rcbOrigin, dv["OriginID"]);
                App_Code.RadComboBoxHelper.Rebind(rcbOriginTank, dv["OriginTankID"]);
                App_Code.RadComboBoxHelper.Rebind(rcbProduct, dv["ProductID"], true);
                App_Code.RadComboBoxHelper.Rebind(rcbDestination, dv["DestinationID"], true);
                App_Code.RadComboBoxHelper.Rebind(rcbCustomer, dv["CustomerID"]);
                App_Code.RadComboBoxHelper.Rebind(rcbGauger, dv["GaugerID"], true);
                App_Code.RadComboBoxHelper.Rebind(rcbTicketType, dv["TicketTypeID"], true);
                rdpDueDate.DbSelectedDate = DBHelper.ToDateTime(dv["DueDate"]);
                txtDispatchNotes.Text = dv["DispatchNotes"].ToString();
                txtDispatchConfirmNum.Text = dv["DispatchConfirmNum"].ToString();
                txtJobNumber.Text = dv["JobNumber"].ToString();
                //txtContractNumber.Text = dv["ContractNumber"].ToString();

                ddPriority.Enabled =  
                    rcbOrigin.Enabled = 
                    rcbDestination.Enabled = 
                    rcbTicketType.Enabled =
                    rcbOriginTank.Enabled = txtDispatchConfirmNum.Enabled = 
                        HttpContext.Current.User.IsInRole("editOrders");

                SetDispatchConfirmationRequired();
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///          Required method for Designer support - do not modify
        ///          the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        }
        #endregion

        public object DataItem
        {
            get
            {
                return this._dataItem;
            }
            set
            {
                this._dataItem = value;
            }
        }

        protected void rcbOrigin_ItemDataBound(object o, RadComboBoxItemEventArgs e)
        {
            if (DBHelper.ToBoolean((e.Item.DataItem as DataRowView)["H2S"]))
                e.Item.CssClass += " H2S";
            else if (e.Item.Text.StartsWith("Deleted:"))
                e.Item.CssClass += " Deleted";
        }

        protected void cvStatus_DispatchedValidate(object sender, ServerValidateEventArgs args)
        {
            args.IsValid = true; // default value
            int gaugerID = RadComboBoxHelper.SelectedValue(rcbGauger);
            if (RadComboBoxHelper.SelectedValue(rcbGaugerStatus) != (int)Models.Order.GaugerStatusEnum.Requested && (gaugerID == 0))
            {
                args.IsValid = false;
            }
        }

        protected void UpdateDataItem(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                DispatchCrude.Models.DispatchCrudeDB db = new Models.DispatchCrudeDB();
                int id = DBHelper.ToInt32(hfID.Value);
                //var current = db.OrderCreates.Single(r => r.ID == id);
                var currentOrder = db.Orders.Where(o => o.ID == id).ToList().FirstOrDefault();
                var currentGauger = db.GaugerOrders.SqlQuery("SELECT * FROM tblGaugerOrder WHERE OrderID={0}", id).ToList<Models.GaugerOrder>().FirstOrDefault();

                currentGauger.DispatchNotes = txtDispatchNotes.Text;
                currentGauger.StatusID = DBHelper.ToInt32(rcbGaugerStatus.SelectedValue);
                currentGauger.GaugerID = DBHelper.TZ_(RadComboBoxHelper.SelectedValue(rcbGauger));
                if (HttpContext.Current.User.IsInRole("editOrders"))
                {
                    currentOrder.OriginID = DBHelper.ToInt32(rcbOrigin.SelectedValue);
                    currentOrder.DestinationID = DBHelper.ToInt32(rcbDestination.SelectedValue);
                    currentOrder.CustomerID = DBHelper.ToInt32(rcbCustomer.SelectedValue);
                    currentOrder.ProductID = DBHelper.ToInt32(rcbProduct.SelectedValue);
                    currentOrder.DispatchConfirmNum = txtDispatchConfirmNum.Text;
                    currentOrder.JobNumber = txtJobNumber.Text;
                    //currentOrder.ContractNumber = txtContractNumber.Text;

                    currentGauger.OriginTankID = rcbOriginTank.Enabled ? RadComboBoxHelper.SelectedValue(rcbOriginTank) : (int?)null;
                    currentGauger.TicketTypeID = DBHelper.ToInt32(rcbTicketType.SelectedValue);
                    currentGauger.DueDate = rdpDueDate.SelectedDate.Value;
                    currentGauger.PriorityID = Convert.ToByte(ddPriority.SelectedValue);
                }
                db.SaveChanges(UserSupport.UserName);
            }
        }

        protected void rcbGaugerStatus_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            SetDispatchConfirmationRequired();
            Page.Validate();
        }

        private void SetDispatchConfirmationRequired()
        {
            rfvDispatchConfirmNum.Enabled = RadComboBoxHelper.SelectedValue(rcbGaugerStatus) != (int)DispatchCrude.Models.Order.GaugerStatusEnum.Requested
                && DBHelper.ToBoolean(
                    OrderRuleHelper.GetRuleValue(DateTime.UtcNow.Date
                        , OrderRuleHelper.Type.RequireShipperPO
                        , RadComboBoxHelper.SelectedValue(rcbCustomer)
                        , null
                        , RadComboBoxHelper.SelectedValue(rcbProduct)
                        , RadComboBoxHelper.SelectedValue(rcbOrigin)
                        , RadComboBoxHelper.SelectedValue(rcbDestination)
                        , false));
        }

        protected void rcbOrigin_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            App_Code.RadComboBoxHelper.Rebind(rcbCustomer, true);
            App_Code.RadComboBoxHelper.Rebind(rcbProduct, true);
            App_Code.RadComboBoxHelper.Rebind(rcbDestination, true);
            App_Code.RadComboBoxHelper.Rebind(rcbOriginTank, true);
            object gaugerID = DispatchCrude_ConnFactory.QuerySingleValue("SELECT GaugerID FROM tblOrigin WHERE ID={0}", (sender as RadComboBox).SelectedValue);
            App_Code.RadComboBoxHelper.Rebind(rcbGauger, gaugerID, true);
        }

        private bool m_ot_sic_inprogress = false;
        protected void rcbOriginTank_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (!m_ot_sic_inprogress)
            {
                try
                {
                    m_ot_sic_inprogress = true;
                    if ((sender as RadComboBox).SelectedItem != null)
                    {
                        int tankID = RadComboBoxHelper.SelectedValue(sender as RadComboBox);
                        RadComboBoxHelper.SetSelectedValue(rcbOrigin, (sender as RadComboBox).SelectedItem.Attributes["OriginID"]);
                        rcbOrigin_SelectedIndexChanged(rcbOrigin, e);
                        RadComboBoxHelper.SetSelectedValue(sender as RadComboBox, tankID);
                    }
                }
                finally
                {
                    m_ot_sic_inprogress = false;
                }
            }
        }

        protected void rcbProduct_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            App_Code.RadComboBoxHelper.Rebind(rcbDestination, true);
        }

        protected void rcbGauger_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (DBHelper.ToInt32(rcbGauger.SelectedValue) == 0 
                && RadComboBoxHelper.SelectedValue(rcbGaugerStatus) == (int)Models.Order.GaugerStatusEnum.Requested)
            {
                RadComboBoxHelper.SetSelectedValue(rcbGaugerStatus, (int)Models.Order.GaugerStatusEnum.Dispatched);
            }
            SetDispatchConfirmationRequired();
        }

        protected void rcbTicketType_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
        }

        protected void cvDispatchConfirmNum_ServerValidate(object source, ServerValidateEventArgs args)
        {
            using (SSDB ssdb = new SSDB())
            {
                args.IsValid = txtDispatchConfirmNum.Text.Trim().Length == 0 || DBHelper.ToInt32(ssdb.QuerySingleValue(
                    string.Format(
                        "SELECT count(*) FROM tblOrder WHERE DeleteDateUTC IS NULL AND CustomerID = {0} AND DispatchConfirmNum = {1} AND ID <> {2}"
                            , RadComboBoxHelper.SelectedValue(rcbCustomer)
                            , DBHelper.QuoteStr(txtDispatchConfirmNum.Text)
                            , this.hfID.Value))) == 0;
            }
        }

        protected void rcbOriginTank_ItemDataBound(object sender, RadComboBoxItemEventArgs e)
        {
            if (e.Item.DataItem is DataRowView)
            {
                e.Item.Attributes.Add("OriginID", (e.Item.DataItem as DataRowView)["OriginID"].ToString());
            }
        }

    }
}