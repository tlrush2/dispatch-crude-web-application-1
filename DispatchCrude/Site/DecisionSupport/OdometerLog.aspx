﻿<%@  Title="Resources" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="OdometerLog.aspx.cs"
    Inherits="DispatchCrude.Site.DecisionSupport.OdometerLog" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script>
        $("#ctl00_ctl00_EntityCaption").html("Odometer Log");
    </script>
    <div class="tabbable TabRepaint">
        <ul class="nav nav-tabs" id="tabmenu" runat="server">
            <!--Tabs will print here (see code behind)-->
        </ul>
        <div class="tab-content">
            <div class="tab-pane in active">

                <telerik:GridNumericColumnEditor ID="num10DigitEditor" runat="server">
                    <NumericTextBox runat="server" EnabledStyle-HorizontalAlign="Right" MinValue="0" MaxValue="9999">
                        <NumberFormat AllowRounding="true" DecimalDigits="10" KeepNotRoundedValue="true" KeepTrailingZerosOnFocus="false" />
                    </NumericTextBox>
                </telerik:GridNumericColumnEditor>
                <div class="leftpanel">
                    <asp:Panel ID="panelFilter" runat="server" DefaultButton="btnRefresh" CssClass="well with-header">
                        <div class="header bordered-blue">Filters</div>
                        <div class="Entry">
                            <asp:Label ID="lblType" runat="server" Text="Type" AssociatedControlID="rcbType" />
                            <telerik:RadComboBox Width="100%" runat="server" ID="rcbType" AllowCustomText="false">
                                <Items>
                                    <telerik:RadComboBoxItem Value="Both" Text="Both" Selected="true" />
                                    <telerik:RadComboBoxItem Value="Loaded" Text="Loaded" />
                                    <telerik:RadComboBoxItem Value="Unloaded" Text="Unloaded" />
                                </Items>
                            </telerik:RadComboBox>
                        </div>
                        <div class="Entry">
                            <asp:Label ID="lblCarrier" runat="server" Text="Carrier" AssociatedControlID="ddCarrier" />
                            <asp:DropDownList CssClass="btn-xs" Width="100%" runat="server" ID="ddCarrier" DataTextField="Name" DataValueField="ID" DataSourceID="dsCarrier" />
                        </div>
                        <div class="Entry">
                            <asp:Label ID="lblDriver" runat="server" Text="Driver" AssociatedControlID="ddDriver" />
                            <asp:DropDownList CssClass="btn-xs" Width="100%" runat="server" ID="ddDriver" DataTextField="Name" DataValueField="ID" DataSourceID="dsDriver" />
                        </div>
                        <div class="Entry">
                            <asp:Label ID="lblTruck" runat="server" Text="Truck" AssociatedControlID="ddTruck" />
                            <asp:DropDownList CssClass="btn-xs" Width="100%" runat="server" ID="ddTruck" DataTextField="Name" DataValueField="ID" DataSourceID="dsTruck" />
                        </div>
                        <div>
                            <div class="Entry floatLeft-date-row">
                                <asp:Label ID="lblStartDate" runat="server" Text="Start Date" AssociatedControlID="rdpStartDate" CssClass="Entry" />
                                <telerik:RadDatePicker ID="rdpStartDate" runat="server" Width="100px">
                                    <DateInput runat="server" DateFormat="M/d/yyyy" />
                                    <DatePopupButton Enabled="true" />
                                </telerik:RadDatePicker>
                            </div>
                            <div class="Entry floatRight-date-row">
                                <asp:Label ID="lblEndDate" runat="server" Text="End Date" AssociatedControlID="rdpEndDate" CssClass="Entry" />
                                <telerik:RadDatePicker ID="rdpEndDate" runat="server" Width="100px">
                                    <DateInput runat="server" DateFormat="M/d/yyyy" />
                                    <DatePopupButton Enabled="true" />
                                </telerik:RadDatePicker>
                            </div>
                            <br /><br /><br />
                            <div class="center">
                                <asp:Button ID="btnRefresh" runat="server" Text="Refresh" CssClass="btn btn-blue shiny" OnClick="filterValueChanged" />
                            </div>
                            <div class="clear"></div>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="panelExcel" runat="server" DefaultButton="cmdExport" CssClass="well with-header">
                        <div class="header bordered-green">Export</div>
                        <div class="center">
                            <asp:Button ID="cmdExport" runat="server" Text="Export to Excel" CssClass="btn btn-blue shiny" Enabled="true" OnClick="cmdExport_Click" />
                        </div>
                    </asp:Panel>
                </div>
                <div id="gridArea" style="height: 100%; min-height: 500px;">
                    <telerik:RadGrid ID="rgMain" runat="server" EnableHeaderContextMenu="False" CellSpacing="0" GridLines="None" EnableLinqExpressions="false"
                                     AllowSorting="False" AllowFilteringByColumn="false" Height="800" CssClass="GridRepaint" ShowGroupPanel="false"
                                     AllowPaging="true" EnableEmbeddedSkins="true" Skin="Vista" AlternatingItemStyle-BackColor="#dcf2fc"
                                     PageSize='<%# Settings.DefaultPageSize %>'
                                     DataSourceID="dsMain" OnItemCreated="grid_ItemCreated" OnItemDataBound="grid_ItemDataBound" OnItemCommand="grid_ItemCommand">
                        <ClientSettings AllowDragToGroup="true">
                            <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                        </ClientSettings>
                        <SortingSettings EnableSkinSortStyles="false" />
                        <GroupingSettings CaseSensitive="False" ShowUnGroupButton="true" />
                        <MasterTableView AutoGenerateColumns="False" DataKeyNames="ID" CommandItemDisplay="Top" AllowMultiColumnSorting="true">
                            <CommandItemSettings ShowAddNewRecordButton="false" ShowExportToExcelButton="false" ShowRefreshButton="false" />
                            <RowIndicatorColumn Visible="True">
                                <HeaderStyle Width="20px"></HeaderStyle>
                            </RowIndicatorColumn>
                            <ExpandCollapseColumn Visible="True">
                                <HeaderStyle Width="20px" />
                            </ExpandCollapseColumn>
                            <SortExpressions>
                                <telerik:GridSortExpression FieldName="Truck" SortOrder="Ascending" />
                                <telerik:GridSortExpression FieldName="StartTime" SortOrder="Descending" />
                            </SortExpressions>
                            <Columns>
                                <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Edit" Text="Edit" UniqueName="EditColumn" ImageUrl="~/images/edit.png">
                                    <HeaderStyle Width="25px" />
                                </telerik:GridButtonColumn>


                                <telerik:GridCheckBoxColumn DataField="Locked" HeaderText="Locked?" UniqueName="Locked" HeaderStyle-Width="60px" ReadOnly="true" />

                                <telerik:GridBoundColumn DataField="OrderNum" UniqueName="OrderNum" HeaderText="Order #" ReadOnly="true"
                                                         HeaderStyle-Width="150px" Display="false" ForceExtractValue="Always" />
                                <telerik:GridBoundColumn UniqueName="Type" DataField="Type" HeaderText="Type" SortExpression="Type" ReadOnly="true"
                                                         FilterControlWidth="70%" HeaderStyle-Width="100px" ItemStyle-Width="100px" />
                                <telerik:GridBoundColumn UniqueName="Carrier" DataField="Carrier" HeaderText="Carrier" SortExpression="Carrier" ReadOnly="true"
                                                         FilterControlWidth="70%" HeaderStyle-Width="150px" EditFormColumnIndex="0" ItemStyle-Width="200px" />
                                <telerik:GridBoundColumn UniqueName="Truck" DataField="Truck" HeaderText="Truck" SortExpression="Truck" ReadOnly="true"
                                                         FilterControlWidth="70%" HeaderStyle-Width="150px" ItemStyle-Width="150px" />
                                <telerik:GridBoundColumn UniqueName="Driver" DataField="Driver" HeaderText="Driver" ReadOnly="true"
                                                         FilterControlWidth="70%" HeaderStyle-Width="150px" ItemStyle-Width="150px" SortExpression="Driver" />
                                <telerik:GridBoundColumn UniqueName="StartTime" DataField="StartTime" DataFormatString="{0:MM/dd/yy}" ReadOnly="true"
                                                         FilterControlWidth="70%" HeaderText="Start Time" HeaderStyle-Width="100px" SortExpression="StartTime" DataType="System.DateTime" />
                                <telerik:GridBoundColumn UniqueName="EndTime" DataField="EndTime" DataFormatString="{0:MM/dd/yy}" ReadOnly="true"
                                                         FilterControlWidth="70%" HeaderText="End Time" HeaderStyle-Width="100px" SortExpression="EndTime" DataType="System.DateTime" />
                                <telerik:GridBoundColumn UniqueName="StartOdometer" DataField="StartOdometer"
                                                         FilterControlWidth="70%" HeaderText="Start Odometer" HeaderStyle-Width="100px" SortExpression="StartOdometer" DataType="System.Int32" />
                                <telerik:GridBoundColumn UniqueName="EndOdometer" DataField="EndOdometer"
                                                         FilterControlWidth="70%" HeaderText="End Odometer" HeaderStyle-Width="100px" SortExpression="EndOdometer" DataType="System.Int32" />
                                <telerik:GridBoundColumn UniqueName="Mileage" DataField="Mileage" ReadOnly="true"
                                                         FilterControlWidth="70%" HeaderText="Mileage" HeaderStyle-Width="100px" SortExpression="Mileage" DataType="System.Int32" />
                            </Columns>
                            <EditFormSettings>
                                <EditColumn FilterControlAltText="Filter EditCommandColumn column" ButtonType="ImageButton" CancelImageUrl="~/images/cancel.png"
                                            UpdateImageUrl="~/images/apply.png" InsertImageUrl="~/images/apply.png" />
                            </EditFormSettings>
                            <PagerStyle AlwaysVisible="True" />
                        </MasterTableView>
                        <HeaderStyle Wrap="False" />
                        <PagerStyle AlwaysVisible="True" />
                        <FilterMenu EnableImageSprites="False" />
                    </telerik:RadGrid>
                </div>
                <blac:DBDataSource ID="dsMain" runat="server" SelectIDNullsToZero="false"
                                   SelectCommand="SELECT * FROM dbo.fnMileageLog(isnull(@StartDate, getdate()), coalesce(@EndDate, @StartDate, getdate()), @CarrierID, @DriverID, @TruckID) WHERE @Type LIKE 'Both' OR Type LIKE @Type+'%' ORDER BY Truck, StartTime DESC">
                    <SelectParameters>
                        <asp:ControlParameter Name="Type" ControlID="rcbType" PropertyName="Text" DefaultValue="Both" />
                        <asp:ControlParameter Name="CarrierID" ControlID="ddCarrier" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
                        <asp:ControlParameter Name="TruckID" ControlID="ddTruck" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
                        <asp:ControlParameter Name="DriverID" ControlID="ddDriver" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
                        <asp:ControlParameter Name="StartDate" ControlID="rdpStartDate" PropertyName="DbSelectedDate" Type="String" DefaultValue="NULL" />
                        <asp:ControlParameter Name="EndDate" ControlID="rdpEndDate" PropertyName="DbSelectedDate" Type="String" DefaultValue="NULL" />
                    </SelectParameters>
                </blac:DBDataSource>
                <blc:RadGridDBCtrl ID="dbcMain" runat="server"
                                   ControlID="rgMain"
                                   FilterActiveEntities="False" />
                <blac:DBDataSource ID="dsCarrier" runat="server"
                                   SelectCommand="SELECT ID, Name = CASE WHEN DeleteDateUTC IS NOT NULL THEN 'Deleted:' ELSE '' END + Name, Active = CASE WHEN DeleteDateUTC IS NULL THEN 1 ELSE 0 END FROM dbo.tblCarrier UNION SELECT -1, '(All)', 1 ORDER BY Active DESC, Name">
                </blac:DBDataSource>
                <blac:DBDataSource ID="dsTruck" runat="server" SelectCommand="SELECT ID, Name = IdNumber FROM dbo.tblTruck UNION SELECT -1, '(All)' ORDER BY Name" />
                <blac:DBDataSource ID="dsDriver" runat="server"
                                   SelectCommand="SELECT ID, Name = CASE WHEN DeleteDateUTC IS NOT NULL THEN 'Deleted:' ELSE '' END + FullName, Active = CASE WHEN DeleteDateUTC IS NULL THEN 1 ELSE 0 END FROM dbo.viewDriver UNION SELECT -1, '(All)', 1 ORDER BY Active DESC, Name">
                </blac:DBDataSource>
            </div>
        </div>
    </div>
</asp:Content>