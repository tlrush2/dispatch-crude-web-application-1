﻿<%@ Page Title="Settlement" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DriverGroups.aspx.cs"
    Inherits="DispatchCrude.Site.Financials.DriverGroups" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="pageHeadContent" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="/styles/radgrid_rates.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="pageMainContent" ContentPlaceHolderID="MainContent" runat="server">
    <script>
        $("#ctl00_ctl00_EntityCaption").html("Driver Groups");
    </script>
    <div class="tabbable TabRepaint">
        <ul class="nav nav-tabs" id="tabmenu" runat="server">
            <!--Tabs will print here (see code behind)-->
        </ul>
        <div class="tab-content">
            <div class="tab-pane in active">

                <div id="speedbuttons" class="well speedButtonContainer" runat="server">
                    <!--Buttons will print here (see code behind)-->
                </div>

                <div id="DataEntryFormGrid" style="height:100%;">
                    <telerik:RadGrid ID="rgMain" runat="server" CellSpacing="0" GridLines="None" CssClass="GridRepaint" EnableLinqExpressions="false"
                                     AutoGenerateColumns="False" ShowGroupPanel="False" AllowFilteringByColumn="true" AllowSorting="True" 
                                     EnableEmbeddedSkins="true" Skin="Vista" AlternatingItemStyle-BackColor="#dcf2fc" Height="800"
                                     AllowPaging="True" PageSize='<%# Settings.DefaultPageSize %>'>
                        <ClientSettings AllowDragToGroup="true">
                            <ClientEvents OnPopUpShowing="GridPopupShowing" />
                            <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                        </ClientSettings>
                        <GroupingSettings CaseSensitive="false" ShowUnGroupButton="false" />
                        <ExportSettings FileName="Driver Groups" ExportOnlyData="true" IgnorePaging="true" HideStructureColumns="true" Excel-FileExtension="xls" Excel-Format="Biff" OpenInNewWindow="true" />
                        <MasterTableView DataKeyNames="ID" CommandItemDisplay="Top" CommandItemSettings-AddNewRecordText="Add New Driver Group" EditMode="Popup">
                            <CommandItemSettings ShowExportToExcelButton="true" />
                            <Columns>
                                <telerik:GridTemplateColumn UniqueName="ActionColumn" HeaderStyle-Width="100px" AllowFiltering="false" >
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" ID="btnEdit" CssClass="btn btn-xs btn-default shiny" CommandName="Edit" Text="Edit" ImageUrl="~/images/edit.png" />
                                        <asp:ImageButton runat="server" ID="btnDelete" CssClass="btn btn-xs btn-default shiny" CommandName="Delete" Text="Delete" ImageUrl="~/images/delete.png" 
                                                         Enabled='<%# !AlonsIT.DBHelper.ToBoolean(Eval("Locked")) %>' Visible='<%# !AlonsIT.DBHelper.ToBoolean(Eval("Locked")) %>' />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>

                                <telerik:GridCheckBoxColumn DataField="Active" UniqueName="Active" HeaderText="Active?" SortExpression="Active"
                                                            ReadOnly="true" HeaderStyle-Width="80px" FilterControlWidth="60%" />

                                <telerik:GridTemplateColumn UniqueName="Name" DataField="Name" HeaderText="Name" SortExpression="Name"
                                                            HeaderStyle-Width="200px" FilterControlWidth="70%">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtName" runat="server" MaxLength="20" Text='<%# Bind("Name") %>' Width="200px" />
                                        <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName"
                                                                    Text="&nbsp;!" ErrorMessage="Name is required" CssClass="NullValidator" />
                                        <blac:UniqueValidator ID="uvName" runat="server" ControlToValidate="txtName"
                                                              TableName="tblDriverGroup" DataField="Name"
                                                              Text="&nbsp;!" ErrorMessage="Name is already in use" CssClass="NullValidator" />
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblName" runat="server" Text='<%# Eval("Name") %>' />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>

                                <telerik:GridTemplateColumn UniqueName="Validation" HeaderStyle-Width="0px" Display="false" EditFormColumnIndex="0" EditFormHeaderTextFormat="">
                                    <EditItemTemplate>
                                        <asp:CustomValidator ID="cvGridError" runat="server" Display="None" ErrorMessage="Generic Message" />
                                        <asp:ValidationSummary ID="validationSummaryMain" runat="server" DisplayMode="BulletList" Enabled="true" EnableClientScript="true"
                                                                CssClass="NullValidator gridPopupErrors" />
                                    </EditItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="CreateDate" UniqueName="CreateDate" SortExpression="CreateDate" DataType="System.DateTime" DataFormatString="{0:M/d/yy}"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Create Date" HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="CreatedByUser" UniqueName="CreatedByUser" SortExpression="CreatedByUser"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Created By" HeaderStyle-Width="90px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="LastChangeDate" UniqueName="LastChangeDate" SortExpression="LastChangeDate" DataType="System.DateTime" DataFormatString="{0:M/d/yy HH:mm}"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Last Update Date" HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="LastChangedByUser" UniqueName="LastChangedByUser" SortExpression="LastChangedByUser"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Last Updated By" HeaderStyle-Width="95px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="DeleteDate" UniqueName="DeleteDate" SortExpression="DeleteDate" DataType="System.DateTime" DataFormatString="{0:M/d/yy HH:mm}"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Delete Date" HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="DeletedByUser" UniqueName="DeletedByUser" SortExpression="DeletedByUser"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Deleted By" HeaderStyle-Width="90px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                            </Columns>
                            <EditFormSettings CaptionFormatString="Edit" InsertCaption="Create" PopUpSettings-ShowCaptionInEditForm="false" PopUpSettings-Width="285" >
                                <EditColumn ButtonType="ImageButton" 
                                            CancelImageUrl="~/images/cancel_imageonly.png"
                                            UpdateImageUrl="~/images/apply_imageonly.png" 
                                            InsertImageUrl="~/images/apply_imageonly.png" />
                            </EditFormSettings>
                            <PagerStyle AlwaysVisible="True" />
                        </MasterTableView>
                        <HeaderStyle Wrap="False" />
                        <PagerStyle AlwaysVisible="True" />
                        <FilterMenu EnableImageSprites="False">
                        </FilterMenu>
                    </telerik:RadGrid>
                    <telerik:RadScriptBlock ID="rsbMain" runat="server">
                        <script src="/scripts/radgrid_rates.js" type="text/javascript" ></script>
                    </telerik:RadScriptBlock>
                </div>
                <blc:RadGridDBCtrl ID="dbcMain" runat="server"
                                   ControlID="rgMain"
                                   SelectCommand="SELECT * FROM viewDriverGroup"
                                   UpdateTableName="tblDriverGroup" />
            </div>
        </div>
    </div>
</asp:Content>