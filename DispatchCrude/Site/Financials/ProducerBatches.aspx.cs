﻿using System;
using System.Linq;
using System.Web;
using System.IO;
//add for sql stuff
using System.Data;
using System.Data.SqlClient;
using Telerik.Web.UI;
using AlonsIT;
using DispatchCrude.App_Code;
using DispatchCrude.DataExchange;
using DispatchCrude.Core;
using DispatchCrude.Extensions;

namespace DispatchCrude.Site.Orders
{
    public partial class ProducerBatches : System.Web.UI.Page
    {

        static protected string CHANGES_ENABLED = "DeleteEnabled", REPORT_SOURCE = "SourceTable";

        protected void Page_Load(object sender, EventArgs e)
        {
            ConfigureAjax(!Settings.SettingsID.DisableAJAX.AsBool());

            //Print tab navigation html to page
            tabmenu.InnerHtml = NavigationHelper.PrintTabArray(NavigationHelper.TabSet_Commodities, "Tab_Batches").ToString();

            if (!IsPostBack)
            {
                dsUserReport.SelectParameters["UserName"].DefaultValue = UserSupport.UserName;

                // default values
                DateTime today = DateTime.Now.Date;
                while (today.DayOfWeek != DayOfWeek.Sunday)
                {
                    today = today.AddDays(-1);
                }
                rdpStart.SelectedDate = today;
                rdpEnd.SelectedDate = today.AddDays(7);
                PopulateProducers();

                if (Request.QueryString["BatchID"] != null)
                {
                    string batchID = Request.QueryString["BatchID"];
                    using (SSDB db = new SSDB())
                    {   // set the producer to the assigned Batch.ProducerID
                        App_Code.DropDownListHelper.SetSelectedValue(ddlProducer, db.QuerySingleValue("SELECT ProducerID FROM tblProducerSettlementBatch WHERE ID={0}", batchID).ToString());
                        // set the start/end date filters to the date of the batch
                        rdpStart.SelectedDate = rdpEnd.SelectedDate = DBHelper.ToDateTime(db.QuerySingleValue("SELECT BatchDate FROM tblProducerSettlementBatch WHERE ID={0}", batchID));
                    }
                    PopulateBatches();
                    // set the batch to the requested batchID
                    App_Code.DropDownListHelper.SetSelectedValue(ddlBatch, batchID);
                }
                else if (Request.QueryString["BatchNum"] != null)
                {
                    string batchNum = Request.QueryString["BatchNum"];
                    string batchID = null;
                    using (SSDB db = new SSDB())
                    {   // get batchID
                        batchID = db.QuerySingleValue("SELECT ID FROM tblProducerSettlementBatch WHERE BatchNum={0}", batchNum).ToString();
                        // set the producer to the assigned Batch.ProducerID
                        App_Code.DropDownListHelper.SetSelectedValue(ddlProducer, db.QuerySingleValue("SELECT ProducerID FROM tblProducerSettlementBatch WHERE ID={0}", batchID).ToString());
                        // set the start/end date filters to the date of the batch
                        rdpStart.SelectedDate = rdpEnd.SelectedDate = DBHelper.ToDateTime(db.QuerySingleValue("SELECT BatchDate FROM tblProducerSettlementBatch WHERE ID={0}", batchID));
                    }
                    PopulateBatches();
                    // set the batch to the requested batchID
                    App_Code.DropDownListHelper.SetSelectedValue(ddlBatch, batchID);
                }
                else
                    PopulateBatches();
                ddlBatch_SelectedIndexChanged(ddlBatch, EventArgs.Empty);
            }

            // Show/hide the unsettle batch button based on user's role
            cmdUnsettle.Visible = UserSupport.IsInRole("unsettleProducerBatches");
        }

        private void ConfigureAjax(bool enabled = true)
        {
            if (enabled)
            {
                // necessary to ensure the calendar popup displayes on Chrome
                rdpStart.EnableAjaxSkinRendering = rdpEnd.EnableAjaxSkinRendering = true;

                App_Code.RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, cmdFilter, false);
                App_Code.RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, rgMain);
                App_Code.RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, ddlBatch, false);
                App_Code.RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, txtCreatedByUser, false);
                App_Code.RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, txtCreateDate, false);
                App_Code.RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, txtInvoiceNum, false);
                App_Code.RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, txtNotes, false);
                App_Code.RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, cmdUnsettle, false);

                App_Code.RadAjaxHelper.AddAjaxSetting(this.Page, ddlBatch, txtInvoiceNum, false);
                App_Code.RadAjaxHelper.AddAjaxSetting(this.Page, ddlBatch, txtNotes, false);
                App_Code.RadAjaxHelper.AddAjaxSetting(this.Page, ddlBatch, txtCreatedByUser, false);
                App_Code.RadAjaxHelper.AddAjaxSetting(this.Page, ddlBatch, txtCreateDate, false);
                App_Code.RadAjaxHelper.AddAjaxSetting(this.Page, ddlBatch, rgMain);

                App_Code.RadAjaxHelper.AddAjaxSetting(this.Page, cmdUnsettle, cmdUnsettle, false);
                App_Code.RadAjaxHelper.AddAjaxSetting(this.Page, cmdUnsettle, rgMain);
                App_Code.RadAjaxHelper.AddAjaxSetting(this.Page, cmdUnsettle, ddlBatch, false);
                App_Code.RadAjaxHelper.AddAjaxSetting(this.Page, cmdUnsettle, txtCreatedByUser, false);
                App_Code.RadAjaxHelper.AddAjaxSetting(this.Page, cmdUnsettle, txtCreateDate, false);
                App_Code.RadAjaxHelper.AddAjaxSetting(this.Page, cmdUnsettle, txtInvoiceNum, false);
                App_Code.RadAjaxHelper.AddAjaxSetting(this.Page, cmdUnsettle, txtNotes, false);
                
                App_Code.RadAjaxHelper.AddAjaxSetting(this.Page, rgMain, rgMain);
            }
            RadAjaxManager.GetCurrent(this.Page).EnableAJAX = enabled;
        }

        private void PopulateProducers()
        {
            using (SSDB db = new SSDB())
            {
                ddlProducer.DataSource = db.GetPopulatedDataTable("SELECT ID, CASE WHEN DeleteDateUTC IS NULL THEN '' ELSE 'Deleted: ' END + Name AS FullName FROM dbo.viewProducer ORDER BY DeleteDateUTC, FullName");
            }
            ddlProducer.DataBind();
        }

        private void PopulateBatches()
        {
            using (SSDB db = new SSDB(false))
            {
                using (SqlCommand cmd = db.BuildCommand(
                    "SELECT * FROM dbo.viewProducerSettlementBatch"
                    + " WHERE ProducerID=@ProducerID AND BatchDate BETWEEN @StartDate AND @EndDate ORDER BY BatchNum DESC"))
                {
                    cmd.Parameters.AddWithValue("@ProducerID", ddlProducer.SelectedValue);
                    cmd.Parameters.AddWithValue("@StartDate", rdpStart.SelectedDate);
                    cmd.Parameters.AddWithValue("@EndDate", rdpEnd.SelectedDate);
                    ddlBatch.DataSource = Core.DateHelper.AddLocalRowStateDateFields(SSDB.GetPopulatedDataTable(cmd));
                }
            }
            ddlBatch.DataBind();
            cmdUnsettle.Enabled = ddlBatch.SelectedIndex != -1;
        }

        //Delete the batch record for the currently selected batch
        private void UnsettleBatch(string BatchID)
        {
            if (!String.IsNullOrEmpty(BatchID))
            {
                using (SSDB db = new SSDB())
                {
                    using (SqlCommand cmd = db.BuildCommand("DELETE FROM dbo.tblOrderSettlementProducer WHERE BatchID=@BatchID"))
                    {
                        // Get batch number
                        cmd.Parameters.AddWithValue("@BatchID", BatchID);

                        // Delete settlement records for this batch number
                        cmd.ExecuteNonQuery();
                    }

                    using (SqlCommand cmd = db.BuildCommand("DELETE FROM dbo.tblProducerSettlementBatch WHERE ID=@BatchID"))
                    {
                        // Get batch number
                        cmd.Parameters.AddWithValue("@BatchID", BatchID);

                        // Delete batch number
                        cmd.ExecuteNonQuery();
                    }
                }
            }
        }

        protected void cmdFilter_Click(object source, EventArgs e)
        {
            PopulateBatches();
            ddlBatch_SelectedIndexChanged(this.ddlBatch, EventArgs.Empty);
        }

        //Unsettle the selected batch (this is a role restricted action)
        protected void cmdUnsettle_Click(object source, EventArgs e)
        {
            if (UserSupport.IsInRole("unsettleProducerBatches"))
            {
                // Delete the batch record
                UnsettleBatch(ddlBatch.SelectedValue);
            }

            // Reresh grid and dropdown using the same method as the filter button
            PopulateBatches();
            ddlBatch_SelectedIndexChanged(this.ddlBatch, EventArgs.Empty);
        }

        protected void ddlBatch_SelectedIndexChanged(object source, EventArgs e)
        {
            if (ddlBatch.SelectedIndex == -1)
            {
                txtCreatedByUser.Text = txtCreateDate.Text = txtInvoiceNum.Text = txtNotes.Text = string.Empty;
            }
            else
            {
                using (SSDB db = new SSDB(false))
                {
                    txtCreatedByUser.Text = db.QuerySingleValue("SELECT CreatedByUser FROM tblProducerSettlementBatch WHERE ID={0}", ddlBatch.SelectedValue).ToString();
                    txtCreateDate.Text = Core.DateHelper.ToLocal(
                        DBHelper.ToDateTime(db.QuerySingleValue("SELECT CreateDateUTC FROM tblProducerSettlementBatch WHERE ID={0}", ddlBatch.SelectedValue))
                        , Context).ToString("M/d/yy h:mm tt");
                    txtInvoiceNum.Text = db.QuerySingleValue("SELECT InvoiceNum FROM tblProducerSettlementBatch WHERE ID={0}", ddlBatch.SelectedValue).ToString();
                    txtNotes.Text = db.QuerySingleValue("SELECT Notes FROM tblProducerSettlementBatch WHERE ID={0}", ddlBatch.SelectedValue).ToString();
                }
            }
            PopulateBatchOrders();
        }

        protected void rcbUserReport_ItemDataBound(object sender, RadComboBoxItemEventArgs e)
        {
            e.Item.Attributes.Add(CHANGES_ENABLED
                , (UserSupport.IsInRole("Administrator") || UserSupport.IsInRole("Management") || !DBHelper.IsNull((e.Item.DataItem as DataRowView)["UserNames"])).ToString());
            e.Item.Attributes.Add(REPORT_SOURCE, (e.Item.DataItem as DataRowView)[REPORT_SOURCE].ToString());
        }

        protected void cmdExport_Click(object source, EventArgs e)
        {
            if (ddlBatch.SelectedIndex != -1)
                Export();
        }

        private void Export()
        {
            string filename = Path.GetFileNameWithoutExtension(rtxFileName.Text) + ".xlsx";
            MemoryStream ms = (MemoryStream)new ReportCenterExcelExport(UserSupport.UserName
                    , rcbUserReport.SelectedItem.Attributes[REPORT_SOURCE].ToString()
                    , RadComboBoxHelper.SelectedValue(rcbUserReport))
                .ExportProducerBatch(DropDownListHelper.SelectedValue(ddlBatch));
            Response.ExportExcelStream(ms, filename);
        }

        protected void PopulateBatchOrders()
        {
            if (ddlBatch.SelectedIndex == -1)
                rgMain.DataSource = new DataTable();
            else
            {
                using (SSDB db = new SSDB())
                {
                    using (SqlCommand cmd = db.BuildCommand("spRetrieveOrdersFinancialProducer"))
                    {
                        cmd.Parameters["@BatchID"].Value = App_Code.DropDownListHelper.SelectedValue(ddlBatch);
                        DataTable dt = SSDB.GetPopulatedDataTable(cmd);
                        dt.Columns.Add("spacer");
                        this.rgMain.DataSource = Core.DateHelper.AddLocalRowStateDateFields(dt, Context);
                    }
                }
            }
            rgMain.DataBind();
        }

        protected void grid_ItemCreated(object sender, GridItemEventArgs e)
        {

        }

        protected void grid_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            //Is it a GridDataItem
            if (e.Item is GridDataItem)
            {
                //Get the instance of the right type
                GridDataItem gdi = e.Item as GridDataItem;
                DataRowView rv = e.Item.DataItem as DataRowView;
                //SettlementRateCellHelper.HandleOtherDetails(gdi, rv, "Producer", false);
            }
        }

        protected void grid_DataBound(object sender, EventArgs e)
        {
            cmdFilter.Enabled = true;
        }

        protected void grid_ItemCommand(object sender, GridCommandEventArgs e)
        {
        }

    }
}