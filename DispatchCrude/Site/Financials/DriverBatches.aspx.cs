﻿using System;
using System.IO;
//add for sql stuff
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using Telerik.Web.UI;
using AlonsIT;
using DispatchCrude.App_Code;
using DispatchCrude.DataExchange;
using DispatchCrude.Core;
using DispatchCrude.Extensions;
using DispatchCrude.Models;

namespace DispatchCrude.Site.Orders
{
    public partial class DriverBatches : System.Web.UI.Page
    {

        static protected string CHANGES_ENABLED = "DeleteEnabled", REPORT_SOURCE = "SourceTable";

        protected void Page_Load(object sender, EventArgs e)
        {
            ConfigureAjax(!Settings.SettingsID.DisableAJAX.AsBool());

            //Print tab navigation html to page
            tabmenu.InnerHtml = NavigationHelper.PrintTabArray(NavigationHelper.TabSet_Settlement, "Tab_Batches").ToString();

            //Print button navigation html to page
            speedbuttons.InnerHtml = NavigationHelper.PrintButtonArray(NavigationHelper.ButtonSet_TabBatches, "Button_Driver").ToString();

            // Set the prompt text of the unsettle button
            if (Settings.SettingsID.Settlement_IncludePendingStatus.AsBool())
            {
                cmdUnsettle.OnClientClick = "javascript:if(!confirm('" + SettlementBatch.REVERT_PENDING_WARNING_TEXT + "')){return false;}";
            }
            else
            {
                cmdUnsettle.OnClientClick = "javascript:if(!confirm('" + SettlementBatch.UNSETTLE_WARNING_TEXT + "')){return false;}";
            }

            if (!IsPostBack)
            {
                dsUserReport.SelectParameters["UserName"].DefaultValue = UserSupport.UserName;

                // default values
                //SetDefaultDateFilter();
                PopulateCarriers();
                PopulateDriverGroups();
                PopulateDrivers();

                if (Request.QueryString["BatchID"] != null)
                {
                    string batchID = Request.QueryString["BatchID"];
                    using (SSDB db = new SSDB())
                    {   // set the carrier|drivergroup|driver to the assigned Batch.values
                        DropDownListHelper.SetSelectedValue(ddlCarrier, (db.QuerySingleValue("SELECT CarrierID FROM tblDriverSettlementBatch WHERE ID={0}", batchID) ?? "").ToString());
                        DropDownListHelper.SetSelectedValue(ddlDriverGroup, (db.QuerySingleValue("SELECT DriverGroupID FROM tblDriverSettlementBatch WHERE ID={0}", batchID) ?? "").ToString());
                        DropDownListHelper.SetSelectedValue(ddlDriver, (db.QuerySingleValue("SELECT DriverID FROM tblDriverSettlementBatch WHERE ID={0}", batchID) ?? "").ToString());
                        // set the start/end date filters to the date of the batch
                        rdpStart.SelectedDate = rdpEnd.SelectedDate = DBHelper.ToDateTime((db.QuerySingleValue("SELECT BatchDate FROM tblDriverSettlementBatch WHERE ID={0}", batchID) ?? DateTime.Now.Date));
                    }
                    PopulateBatches();
                    // set the batch to the requested batchID
                    DropDownListHelper.SetSelectedValue(ddlBatch, batchID);
                }
                else if (Request.QueryString["BatchNum"] != null)
                {
                    string batchNum = Request.QueryString["BatchNum"];
                    string batchID = null;
                    using (SSDB db = new SSDB())
                    {   // get batchID
                        batchID = db.QuerySingleValue("SELECT ID FROM tblDriverSettlementBatch WHERE BatchNum={0}", batchNum).ToString();
                        // set the carrier|drivergroup|driver to the assigned Batch.values
                        DropDownListHelper.SetSelectedValue(ddlCarrier, (db.QuerySingleValue("SELECT CarrierID FROM tblDriverSettlementBatch WHERE ID={0}", batchID) ?? "").ToString());
                        DropDownListHelper.SetSelectedValue(ddlDriverGroup, (db.QuerySingleValue("SELECT DriverGroupID FROM tblDriverSettlementBatch WHERE ID={0}", batchID) ?? "").ToString());
                        DropDownListHelper.SetSelectedValue(ddlDriver, (db.QuerySingleValue("SELECT DriverID FROM tblDriverSettlementBatch WHERE ID={0}", batchID) ?? "").ToString());
                        // set the start/end date filters to the date of the batch
                        rdpStart.SelectedDate = rdpEnd.SelectedDate = DBHelper.ToDateTime((db.QuerySingleValue("SELECT BatchDate FROM tblDriverSettlementBatch WHERE ID={0}", batchID) ?? DateTime.Now.Date));
                    }
                    PopulateBatches();
                    // set the batch to the requested batchID
                    DropDownListHelper.SetSelectedValue(ddlBatch, batchID);
                    txtBatchNum.Text = batchNum;
                }

                ddlBatch_SelectedIndexChanged(ddlBatch, EventArgs.Empty);
            }

            // Show/hide the unsettle batch button based on user's role
            cmdUnsettle.Visible = UserSupport.IsInRole("unsettleDriverBatches");
        }

        private void SetDefaultDateFilter()
        {
            DateTime today = DateTime.Now.Date;
            while (today.DayOfWeek != DayOfWeek.Sunday)
            {
                today = today.AddDays(-1);
            }
            rdpStart.SelectedDate = today;
            rdpEnd.SelectedDate = today.AddDays(7);
        }

        private void ConfigureAjax(bool enabled = true)
        {
            if (enabled)
            {
                // necessary to ensure the calendar popup displayes on Chrome
                rdpStart.EnableAjaxSkinRendering = rdpEnd.EnableAjaxSkinRendering = true;

                RadAjaxHelper.AddAjaxSetting(this.Page, txtOrderNum, txtOrderNum);
                RadAjaxHelper.AddAjaxSetting(this.Page, txtOrderNum, ddlCarrier);
                RadAjaxHelper.AddAjaxSetting(this.Page, txtOrderNum, ddlDriverGroup);
                RadAjaxHelper.AddAjaxSetting(this.Page, txtOrderNum, ddlDriver);
                RadAjaxHelper.AddAjaxSetting(this.Page, txtOrderNum, rdpStart);
                RadAjaxHelper.AddAjaxSetting(this.Page, txtOrderNum, rdpEnd);

                RadAjaxHelper.AddAjaxSetting(this.Page, txtOrderNum, rgMain);
                RadAjaxHelper.AddAjaxSetting(this.Page, txtOrderNum, ddlBatch, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, txtOrderNum, txtCreatedByUser, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, txtOrderNum, txtCreateDate, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, txtOrderNum, txtInvoiceNum, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, txtOrderNum, txtNotes, false);

                RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, cmdFilter, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, rgMain);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, ddlBatch, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, txtCreatedByUser, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, txtCreateDate, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, txtInvoiceNum, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, txtNotes, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, cmdUnsettle, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, cmdExportPdfExportZip, false);
                //RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, cmdExport, false); // not sure why but this causes standard export to fail
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, cmdGenerateStatementEmails, false);

                RadAjaxHelper.AddAjaxSetting(this.Page, cmdUnsettle, cmdUnsettle, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdUnsettle, rgMain);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdUnsettle, ddlBatch, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdUnsettle, txtCreatedByUser, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdUnsettle, txtCreateDate, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdUnsettle, txtInvoiceNum, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdUnsettle, txtNotes, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdUnsettle, cmdExportPdfExportZip, false);
                //RadAjaxHelper.AddAjaxSetting(this.Page, cmdUnsettle, cmdExport, false); // not sure why but this causes standard export to fail
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdUnsettle, cmdGenerateStatementEmails, false);

                RadAjaxHelper.AddAjaxSetting(this.Page, ddlBatch, txtInvoiceNum, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, ddlBatch, txtNotes, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, ddlBatch, txtCreatedByUser, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, ddlBatch, txtCreateDate, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, ddlBatch, rgMain);

                RadAjaxHelper.AddAjaxSetting(this.Page, rgMain, rgMain);
            }
            RadAjaxManager.GetCurrent(this.Page).EnableAJAX = enabled;
        }

        private void PopulateCarriers()
        {
            using (SSDB db = new SSDB())
            {
                ddlCarrier.DataSource = db.GetPopulatedDataTable("SELECT ID, FullName = dbo.fnNameWithDeleted(Name, DeleteDateUTC), Active = dbo.fnIsActive(DeleteDateUTC) FROM dbo.tblCarrier UNION SELECT -1, '(All)', 1 ORDER BY Active DESC, FullName");
            }
            ddlCarrier.DataBind();
        }
        private void PopulateDriverGroups()
        {
            using (SSDB db = new SSDB())
            {
                ddlDriverGroup.DataSource = db.GetPopulatedDataTable("SELECT ID, FullName = dbo.fnNameWithDeleted(Name, DeleteDateUTC), Active = dbo.fnIsActive(DeleteDateUTC) FROM tblDriverGroup UNION SELECT -1, '(All)', 1 ORDER BY dbo.fnIsActive(DeleteDateUTC) DESC, FullName");
            }
            ddlDriverGroup.DataBind();
        }
        private void PopulateDrivers()
        {
            using (SSDB db = new SSDB())
            {
                ddlDriver.DataSource = db.GetPopulatedDataTable("SELECT ID, Name = CASE WHEN EXISTS (SELECT * FROM viewDriverBase D2 WHERE D2.FullNameD = D.FullNameD AND D2.ID <> D.ID) THEN dbo.fnNameWithDeleted(Carrier + ': ' + FullName, DeleteDateUTC) ELSE FullNameD END, Active, N = FullName FROM dbo.viewDriverBase D UNION SELECT -1, '(All)', 1, '(All)' ORDER BY Active DESC, N, Name");
            }
            ddlDriver.DataBind();
        }

        private void PopulateBatches()
        {
            string dateField = ddlDateField.SelectedValue;
            using (SSDB db = new SSDB(false))
            {
                using (SqlCommand cmd = db.BuildCommand(
                    "SELECT * FROM dbo.viewDriverSettlementBatch"
                    + " WHERE (@CarrierID = -1 OR CarrierID = @CarrierID) AND (@DriverGroupID = -1 OR DriverGroupID=@DriverGroupID) AND (@DriverID = -1 OR DriverID=@DriverID)"
                    + (rdpStart.SelectedDate != null && rdpEnd.SelectedDate != null ? (" AND " + dateField + " BETWEEN @StartDate AND @EndDate ") : "")
                    + " AND IsFinal = 1"
                    + " AND (@OrderNum = '' OR ID IN (SELECT BatchID FROM dbo.tblOrderSettlementDriver WHERE OrderID = (SELECT ID FROM tblOrder WHERE OrderNum = @OrderNum)))"
                    + " AND (@BatchNum = '' OR BatchNum = @BatchNum)"
                    + " ORDER BY BatchNum DESC"))
                {
                    cmd.Parameters.AddWithValue("@CarrierID", ddlCarrier.SelectedValue);
                    cmd.Parameters.AddWithValue("@DriverGroupID", ddlDriverGroup.SelectedValue);
                    cmd.Parameters.AddWithValue("@DriverID", ddlDriver.SelectedValue);
                    if (rdpStart.SelectedDate != null)
                        cmd.Parameters.AddWithValue("@StartDate", rdpStart.SelectedDate);
                    if (rdpEnd.SelectedDate != null)
                        cmd.Parameters.AddWithValue("@EndDate", rdpEnd.SelectedDate);
                    cmd.Parameters.AddWithValue("@BatchNum", txtBatchNum.Text);
                    cmd.Parameters.AddWithValue("@OrderNum", txtOrderNum.Text);
                    ddlBatch.DataSource = Core.DateHelper.AddLocalRowStateDateFields(SSDB.GetPopulatedDataTable(cmd));
                }
            }
            ddlBatch.DataBind();
        }

        //Delete the batch record for the currently selected batch
        private void UnsettleBatch(string BatchID)
        {
            if (!String.IsNullOrEmpty(BatchID))
            {
                new DriverSettlementBatch().UnsettleBatch(BatchID);
            }
        }

        protected void cmdFilter_Click(object source, EventArgs e)
        {
            PopulateBatches();
            ddlBatch_SelectedIndexChanged(this.ddlBatch, EventArgs.Empty);
        }

        //Unsettle the selected batch (this is a role restricted action)
        protected void cmdUnsettle_Click(object source, EventArgs e)
        {
            if (UserSupport.IsInRole("unsettleDriverBatches"))
            {
                // Delete the batch record
                UnsettleBatch(ddlBatch.SelectedValue);

                // Return to the settlement page and preselect the batch if pending
                string url = "/Site/Financials/DriverInvoicing.aspx";
                if (Settings.SettingsID.Settlement_IncludePendingStatus.AsBool())
                    url = url + "?BatchID={0}";
                Response.Redirect(string.Format(url, ddlBatch.SelectedValue));
            }
        }

        protected void ddlBatch_SelectedIndexChanged(object source, EventArgs e)
        {
            if (ddlBatch.SelectedIndex == -1)
            {
                txtCreatedByUser.Text = txtCreateDate.Text = txtInvoiceNum.Text = txtNotes.Text = string.Empty;
            }
            else
            {
                using (DispatchCrudeDB db = new DispatchCrudeDB())
                {
                    var data = db.DriverSettlementBatches.Find(DropDownListHelper.SelectedValue(ddlBatch));
                    txtCreatedByUser.Text = data.CreatedByUser;
                    txtCreateDate.Text = data.CreateDate.Value.ToString("M/d/yy h:mm tt");
                    txtInvoiceNum.Text = data.InvoiceNum;
                    txtNotes.Text = data.Notes;

                }
            }
            // enabled/disable all buttons dependent on a actively selected Batch
            cmdGenerateStatementEmails.Enabled = cmdExportPdfExportZip.Enabled = cmdUnsettle.Enabled = ddlBatch.SelectedIndex != -1;

            PopulateBatchOrders();
        }

        protected void rcbUserReport_ItemDataBound(object sender, RadComboBoxItemEventArgs e)
        {
            e.Item.Attributes.Add(CHANGES_ENABLED
                , ((UserSupport.IsInGroup("_DCAdministrator") || UserSupport.IsInGroup("_DCSystemManager") || UserSupport.IsInGroup("_DCSupport")) || !DBHelper.IsNull((e.Item.DataItem as DataRowView)["UserNames"])).ToString());
            e.Item.Attributes.Add(REPORT_SOURCE, (e.Item.DataItem as DataRowView)[REPORT_SOURCE].ToString());
        }

        protected void cmdExport_Click(object source, EventArgs e)
        {
            if (ddlBatch.SelectedIndex != -1)
                Export();
        }

        protected void cmdExportPdfExportZip_Click(object source, EventArgs e)
        {
            if (ddlBatch.SelectedIndex != -1 && rddlPdfExport.SelectedIndex != -1)
            {
                Response.Redirect(string.Format(
                    @"/pdfexport/ExportDriverSettlementZipFile?batchid={0}&pdfexportid={1}"
                    , ddlBatch.SelectedValue
                    , rddlPdfExport.SelectedValue));
            }
        }

        protected void cmdGenerateStatementEmails_Click(object source, EventArgs e)
        {
            if (ddlBatch.SelectedIndex != -1 && rddlEmailDefinition.SelectedIndex != -1)
            {
                int batchID = DropDownListHelper.SelectedValue(ddlBatch);
                // call the stored procedure with the appropriate values
                DriverSettlementStatementEmail.GenerateFromBatch(
                    DBHelper.ToInt32(rddlEmailDefinition.SelectedValue)
                    , DropDownListHelper.SelectedValue(ddlBatch)
                    , User.Identity.Name
                    , chkQueueEmailSend.Checked);
                // if not immediately queuing, redirect to the Email page with an appropriate filter
                if (!@chkQueueEmailSend.Checked)
                {
                    Response.Redirect(
                        string.Format(@"/DriverSettlementStatementEmail/Index?BatchID={0}", batchID)
                    );
                }
            }
        }

        private void Export()
        {
            string filename = Path.GetFileNameWithoutExtension(rtxFileName.Text) + ".xlsx";
            MemoryStream ms = (MemoryStream)new ReportCenterExcelExport(UserSupport.UserName
                    , rcbUserReport.SelectedItem.Attributes[REPORT_SOURCE].ToString()
                    , RadComboBoxHelper.SelectedValue(rcbUserReport))
                .ExportDriverBatch(DropDownListHelper.SelectedValue(ddlBatch));
            Response.ExportExcelStream(ms, filename);
        }

        protected void PopulateBatchOrders()
        {
            if (ddlBatch.SelectedIndex == -1)
                rgMain.DataSource = new DataTable();
            else
            {
                using (SSDB db = new SSDB())
                {
                    using (SqlCommand cmd = db.BuildCommand("spRetrieveOrdersFinancialDriver"))
                    {
                        cmd.Parameters["@BatchID"].Value = DropDownListHelper.SelectedValue(ddlBatch);
                        cmd.Parameters["@UserName"].Value = User.Identity.Name;
                        DataTable dt = SSDB.GetPopulatedDataTable(cmd);
                        dt.Columns.Add("spacer");
                        this.rgMain.DataSource = Core.DateHelper.AddLocalRowStateDateFields(dt, Context);
                    }
                }
            }
            rgMain.DataBind();
        }

        protected void grid_ItemCreated(object sender, GridItemEventArgs e)
        {

        }

        protected void grid_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            //Is it a GridDataItem
            if (e.Item is GridDataItem)
            {
                //Get the instance of the right type
                GridDataItem gdi = e.Item as GridDataItem;
                DataRowView rv = e.Item.DataItem as DataRowView;
                SettlementRateCellHelper.HandleOtherDetails(gdi, rv, "Driver", false);
            }
        }

        protected void grid_DataBound(object sender, EventArgs e)
        {
            cmdFilter.Enabled = true;
        }

        protected DataRow GetOrder(int orderNum)
        {
            using (SSDB db = new SSDB())
            {
                DataTable dt = db.GetPopulatedDataTable(
                    @"SELECT OrderID = O.ID, SB.BatchDate, SB.CarrierID, SB.DriverID, SB.DriverGroupID
                        FROM tblOrder O
                        JOIN tblOrderSettlementDriver OS ON OS.OrderID = O.ID
                        JOIN tblDriverSettlementBatch SB ON SB.ID = OS.BatchID
                        WHERE O.OrderNum = {0}", (object)DBHelper.QuoteStr(orderNum));
                return dt.Rows.Count > 0 ? dt.Rows[0] : null;
            }
        }
    }
}