﻿<%@ Page Title="Commodities" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProducerInvoicing.aspx.cs"
    Inherits="DispatchCrude.Site.Orders.ProducerInvoicing" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="contentMain" ContentPlaceHolderID="MainContent" runat="server">
    <telerik:RadScriptBlock ID="radScriptBlock" runat="server">
        <script type="text/javascript">
            var noUpdate = false;
            function chkAll_ToggleStateChanged(sender, args) {
                // if we are being called from the chkSel_CheckedChanged below, then just exit (return)
                //debugger;
                if (noUpdate)
                    return;
                var value = args.get_currentToggleState().get_value();
                if (value == "neither") {
                    sender.set_selectedToggleStateIndex(2);
                    value = "true";
                }
                var grid = $find("<%= rgMain.ClientID %>");
                if (grid == null) return;
                var items = grid.get_masterTableView().get_dataItems();
                for (i = 0; i < items.length; i++) {
                    var item = items[i];
                    var chk = item.findElement("chkSel");
                    if (chk != null) {
                        chk.checked = (value == "true");
                    }
                }
                DisableSelectionButtons(value == "false");
            };
            function chkSel_CheckedChanged() {
                var grid = $find("<%= rgMain.ClientID %>");
                if (grid == null) return;
                var items = grid.get_masterTableView().get_dataItems();
                var checked = 0, unchecked = 0;
                for (i = 0; i < items.length; i++) {
                    var item = items[i];
                    var chk = item.findElement("chkSel");
                    if (chk.checked)
                        checked++;
                    else
                        unchecked++;
                }
                noUpdate = true;
                var chkAll = $find("chkAll");
                if (checked == 0)
                    chkAll.set_selectedToggleStateIndex(0);
                else if (unchecked == 0)
                    chkAll.set_selectedToggleStateIndex(2);
                else
                    chkAll.set_selectedToggleStateIndex(1);
                noUpdate = false;
                DisableSelectionButtons(checked == 0);
            };
            function DisableSelectionButtons(disabled) {
                debugger;
                $("#" + "<%= cmdApplyRates.ClientID %>").prop("disabled", disabled);
                $("#" + "<%= cmdSettleBatch.ClientID %>").prop("disabled", disabled);
            };

            var contextMenu = null;
            function storeContextMenuReference(sender, args) {
                contextMenu = sender;
            }
            function cmdApplyRates_ClientClicking(sender, args) {
                if (args.IsSplitButtonClick()) {
                    var currentLocation = $telerik.getBounds(sender.get_element());
                    contextMenu.showAt(currentLocation.x, currentLocation.y + currentLocation.height);
                    args.set_cancel(true);
                }
            }
            // main startup script
            $(function () {
                // ensure all buttons are initially disabled/enabled appropriately
                chkSel_CheckedChanged();
            });
        </script>
    </telerik:RadScriptBlock>
    <script>
        $("#ctl00_ctl00_EntityCaption").html("Settlement");
    </script>
    <div class="tabbable TabRepaint">
        <ul class="nav nav-tabs" id="tabmenu" runat="server">
            <!--Tabs will print here (see code behind)-->
        </ul>
        <div class="tab-content">
            <div class="tab-pane in active">

                <div class="leftpanel">
                    <div class="tabbable">
                        <ul class="nav nav-tabs">
                            <li class="active tab-blue">
                                <a data-toggle="tab" href="#Filters" aria-expanded="true">Filters</a>
                            </li>
                            <li class="tab-yellow">
                                <a data-toggle="tab" href="#ProcessActions" aria-expanded="true">Process Actions</a>
                            </li>
                            <li class="tab-green">
                                <a data-toggle="tab" href="#Export" aria-expanded="true">Export</a>
                            </li>
                        </ul>
                        <div id="leftTabs" class="tab-content">
                            <div id="Filters" class="tab-pane active">
                                <asp:Panel ID="panelGenerate" runat="server" DefaultButton="cmdFilter">
                                    <div class="Entry">
                                        <asp:Label ID="lblProducer" runat="server" Text="Producer" AssociatedControlID="ddlProducer" CssClass="Entry" />
                                        <asp:DropDownList Width="100%" ID="ddlProducer" runat="server" DataTextField="Name" DataValueField="ID" DataSourceID="dsProducer" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblShipper" runat="server" Text="Shipper" AssociatedControlID="ddlShipper" CssClass="Entry" />
                                        <asp:DropDownList Width="100%" ID="ddlShipper" runat="server" DataTextField="Name" DataValueField="ID" DataSourceID="dsShipper" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblProductGroup" runat="server" Text="Product Group" AssociatedControlID="ddlProductGroup" CssClass="Entry" />
                                        <asp:DropDownList Width="100%" ID="ddlProductGroup" runat="server" DataTextField="Name" DataValueField="ID" DataSourceID="dsProductGroup" />
                                    </div>
                                    <div class="Entry">
                                        <asp:Label ID="lblOriginState" runat="server" Text="Origin State" AssociatedControlID="ddlOriginState" CssClass="Entry" />
                                        <asp:DropDownList Width="100%" ID="ddlOriginState" runat="server" DataTextField="Name" DataValueField="ID" DataSourceID="dsState" />
                                    </div>
                                    <div>
                                        <div class="Entry floatLeft-date-row">
                                            <asp:Label ID="lblStart" runat="server" Text="Start Date" AssociatedControlID="rdpStart" CssClass="Entry" />
                                            <telerik:RadDatePicker ID="rdpStart" runat="server" Width="100px">
                                                <DateInput runat="server" DateFormat="M/d/yyyy" />
                                                <DatePopupButton Enabled="true" />
                                            </telerik:RadDatePicker>
                                        </div>
                                        <div class="Entry floatRight-date-row">
                                            <asp:Label ID="lblEnd" runat="server" Text="End Date" AssociatedControlID="rdpEnd" CssClass="Entry" />
                                            <telerik:RadDatePicker ID="rdpEnd" runat="server" Width="100px">
                                                <DateInput runat="server" DateFormat="M/d/yyyy" />
                                                <DatePopupButton Enabled="true" />
                                            </telerik:RadDatePicker>
                                        </div>
                                    </div>
                                    <br /><br /><br />
                                    <div class="clear"></div>
                                    <div class="spacer5px"></div>
                                    <div style="text-align: center;">
                                        <asp:Button ID="cmdFilter" runat="server" Text="Refresh" CssClass="btn btn-blue shiny"
                                                    UseSubmitBehavior="false" OnClientClicked="DisableFilterButton" OnClick="cmdFilter_Click" />
                                    </div>
                                </asp:Panel>
                            </div>
                            <div id="Export" class="tab-pane">
                                <asp:Panel ID="panelExportActions" runat="server">
                                    <div class="Entry">
                                        <div class="center"><b>SETTLEMENT PREVIEW ONLY</b></div>
                                        <div class="spacer10px"></div>
                                        <asp:Label ID="lblReportCenterSel" runat="server" Text="Report Center Selection" AssociatedControlID="rcbUserReport" CssClass="Entry" />
                                        <telerik:RadComboBox ID="rcbUserReport" runat="server" AllowCustomText="false" ShowDropDownOnTextboxClick="true"
                                                             DataSourceID="dsUserReport" DataTextField="Name" DataValueField="ID" MaxLength="30"
                                                             Width="100%"
                                                             OnItemDataBound="rcbUserReport_ItemDataBound" />
                                        <blac:DBDataSource ID="dsUserReport" runat="server"
                                                           SelectCommand="SELECT * FROM viewUserReportDefinition WHERE (UserNames IS NULL OR @UserName IN (SELECT Value FROM dbo.fnSplitCSV(UserNames))) ORDER BY Name">
                                            <SelectParameters>
                                                <asp:Parameter Name="UserName" DefaultValue="" DbType="String" />
                                            </SelectParameters>
                                        </blac:DBDataSource>
                                    </div>
                                    <div class="spacer5px"></div>
                                    <div class="center">
                                        <asp:Button ID="cmdExport" runat="server" Text="Export" CssClass="NOAJAX btn btn-blue shiny"
                                                    CausesValidation="true" ValidationGroup="vgFileName"
                                                    UseSubmitBehavior="false" OnClick="cmdExport_Click" />
                                    </div>
                                </asp:Panel>
                            </div>
                            <div id="ProcessActions" class="tab-pane">
                                <asp:Panel ID="panelSelectedCtrl" runat="server">
                                    <div>
                                        <div class="center">
                                            <asp:Button ID="cmdApplyRates" runat="server" Text="Apply Rates" CssClass="floatLeft btn btn-blue shiny"
                                                        OnClick="cmdApplyRates_Click" />
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <asp:Button ID="cmdSettleBatch" runat="server" Text="Settle Batch" CssClass="floatRight btn btn-blue shiny"
                                                        UseSubmitBehavior="false" OnClick="cmdSettleBatch_Click" />
                                        </div>
                                    </div>
                                    <div class="spacer5px"></div>
                                    <div class="center">
                                        <telerik:RadTextBox ID="txtInvoiceNum" runat="server" Width="100%" Enabled="false" EmptyMessage="External Invoice #" />
                                        <div class="spacer"></div>
                                        <telerik:RadTextBox ID="txtNotes" runat="server" TextMode="MultiLine" Width="100%" Height="33px" Enabled="false" EmptyMessage="Notes" />
                                    </div>
                                    <div class="clear"></div>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div id="gridArea">
                        <telerik:RadWindowManager ID="radWindowManager" runat="server" EnableShadow="true" />
                        <telerik:RadGrid ID="rgMain" runat="server" EnableHeaderContextMenu="False" Height="800" CssClass="GridRepaint" CellSpacing="0" GridLines="None"
                                         AllowSorting="True" ShowFooter="true" ShowStatusBar="false"
                                         AllowPaging="false" EnableViewState="false" EnableEmbeddedSkins="true" Skin="Vista" AlternatingItemStyle-BackColor="#dcf2fc"
                                         PageSize='<%# Settings.DefaultPageSize %>'
                                         OnNeedDataSource="grid_NeedDataSource" OnDataBound="grid_DataBound" OnItemCommand="grid_ItemCommand" OnItemDataBound="grid_ItemDataBound">
                            <ClientSettings>
                                <Scrolling AllowScroll="True" UseStaticHeaders="True" FrozenColumnsCount="0" />
                            </ClientSettings>
                            <MasterTableView AutoGenerateColumns="False" DataKeyNames="ID" CommandItemDisplay="None" AllowMultiColumnSorting="true" EditMode="PopUp">
                                <ColumnGroups>
                                    <telerik:GridColumnGroup HeaderText="Order Details" Name="OrderDetails" HeaderStyle-HorizontalAlign="Center" />
                                    <telerik:GridColumnGroup HeaderText="Settlement Details" Name="InvoiceDetails" HeaderStyle-HorizontalAlign="Center" />
                                </ColumnGroups>
                                <SortExpressions>
                                    <%--<telerik:GridSortExpression FieldName="HasError" SortOrder="Descending" />--%>
                                </SortExpressions>
                                <CommandItemSettings ExportToPdfText="Export to PDF" />
                                <RowIndicatorColumn Visible="True">
                                    <HeaderStyle Width="20px"></HeaderStyle>
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True">
                                    <HeaderStyle Width="20px"></HeaderStyle>
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridTemplateColumn UniqueName="SelColumn" HeaderText=" " AllowFiltering="false" Groupable="false" ReadOnly="true">
                                        <HeaderTemplate>
                                            <telerik:RadButton ID="chkAll" runat="server" ToggleType="CustomToggle" ButtonType="ToggleButton" ClientIDMode="Static"
                                                               OnClientToggleStateChanged="chkAll_ToggleStateChanged" Checked="true"
                                                               AutoPostBack="false">
                                                <ToggleStates>
                                                    <telerik:RadButtonToggleState Text="" PrimaryIconCssClass="rbToggleCheckbox" Value="false" Selected="true"></telerik:RadButtonToggleState>
                                                    <telerik:RadButtonToggleState Text="" PrimaryIconCssClass="rbToggleCheckboxFilled" Value="neither"></telerik:RadButtonToggleState>
                                                    <telerik:RadButtonToggleState Text="" PrimaryIconCssClass="rbToggleCheckboxChecked" Value="true"></telerik:RadButtonToggleState>
                                                </ToggleStates>
                                            </telerik:RadButton>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSel" runat="server" Checked='<%# Eval("Selected") %>' onclick="chkSel_CheckedChanged()" OnClientLoad="chkSel_CheckedChanged()" />
                                        </ItemTemplate>
                                        <ItemStyle BackColor="LightBlue" />
                                        <HeaderStyle Width="30px" />
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridDateTimeColumn DataField="InvoiceRatesAppliedDate" UniqueName="InvoiceRatesAppliedDate" HeaderText="Rates Applied" SortExpression="InvoiceRatesAppliedDate"
                                                                DataFormatString="{0:M/d/yy HH:mm}" ReadOnly="true" ForceExtractValue="Always">
                                        <HeaderStyle Width="100px" />
                                    </telerik:GridDateTimeColumn>
                                    <telerik:GridCheckBoxColumn DataField="Approved" UniqueName="Approved" HeaderText="A?" SortExpression="Approved"
                                                                ReadOnly="true" ForceExtractValue="Always">
                                        <HeaderStyle Width="40px" />
                                    </telerik:GridCheckBoxColumn>
                                    <telerik:GridCheckBoxColumn DataField="HasError" UniqueName="HasError" HeaderText="E?" SortExpression="HasError" AllowSorting="false"
                                                                ReadOnly="true" ForceExtractValue="Always">
                                        <HeaderStyle Width="40px" />
                                    </telerik:GridCheckBoxColumn>
                                    <telerik:GridBoundColumn DataField="OrderNum" ReadOnly="true" SortExpression="OrderNum"
                                                             HeaderText="Order #" ColumnGroupName="OrderDetails" ForceExtractValue="Always">
                                        <HeaderStyle Width="65px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridDateTimeColumn DataField="OrderDate" UniqueName="OrderDate" HeaderText="Date" SortExpression="OrderDate"
                                                                DataFormatString="{0:M/d/yyyy}" ReadOnly="true" ForceExtractValue="Always" ColumnGroupName="OrderDetails">
                                        <HeaderStyle Width="70px" />
                                    </telerik:GridDateTimeColumn>
                                    <telerik:GridBoundColumn DataField="Producer" UniqueName="Producer" HeaderText="Producer" SortExpression="Producer"
                                                             ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="OrderDetails" Display="true">
                                        <HeaderStyle Width="300px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Customer" UniqueName="Customer" HeaderText="Shipper" SortExpression="Customer"
                                                             ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="OrderDetails" Display="true"
                                                             Aggregate="Count" FooterStyle-HorizontalAlign="Right">
                                        <HeaderStyle Width="300px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Origin" UniqueName="Origin" HeaderText="Origin" SortExpression="Origin"
                                                             ReadOnly="true" ForceExtractValue="Always" ColumnGroupName="OrderDetails">
                                        <HeaderStyle Width="300px" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="LeaseNum" UniqueName="LeaseNum" HeaderText="Lease #" SortExpression="LeaseNum"
                                                             ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="OrderDetails"
                                                             Visible="true" Display="true">
                                        <HeaderStyle Width="65px" />
                                    </telerik:GridBoundColumn>


                                    <telerik:GridBoundColumn DataField="InvoiceSettlementFactor" UniqueName="InvoiceSettlementFactor" HeaderText="Settle Unit" SortExpression="InvoiceSettlementFactor"
                                                             HeaderStyle-Width="110px" ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="InvoiceDetails"
                                                             Visible="true" Display="true" />
                                    <telerik:GridNumericColumn DataField="InvoiceSettlementUnits" UniqueName="InvoiceSettlementUnits" HeaderText="Volume" SortExpression="InvoiceSettlementUnits"
                                                               HeaderStyle-Width="90px" ColumnGroupName="InvoiceDetails" DataFormatString="{0:#,##0.000}" />
                                    <telerik:GridNumericColumn DataField="ProductGroup" UniqueName="ProductGroup" HeaderText="Product Group" SortExpression="ProductGroup"
                                                               HeaderStyle-Width="120px" ColumnGroupName="InvoiceDetails" />
                                    <telerik:GridBoundColumn DataField="InvoiceSettlementUomShort" UniqueName="InvoiceSettlementUomShort" HeaderText="UOM" SortExpression="InvoiceSettlementUomShort"
                                                             HeaderStyle-Width="60px" ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="InvoiceDetails"
                                                             Visible="true" Display="true" />
                                    <telerik:GridHyperLinkColumn DataTextField="InvoiceIndexPrice" DataTextFormatString="{0:$#,##0.0000}"
                                                                 DataNavigateUrlFields="ID,InvoiceCommodityPurchasePricebookID" DataNavigateUrlFormatString="~/CommodityPricing/Pricebook?orderid={0}&pricebook={1}" Target="_blank"
                                                                 HeaderText="Index Price" SortExpression="InvoiceIndexPrice"
                                                                 FilterControlWidth="70%" UniqueName="IndexPrice" ColumnGroupName="InvoiceDetails"
                                                                 Visible="true" Display="true">
                                        <HeaderStyle Width="90px" HorizontalAlign="Right" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </telerik:GridHyperLinkColumn>
                                    <telerik:GridBoundColumn DataField="InvoiceCommodityIndex" UniqueName="CommodityIndex" HeaderText="Index" SortExpression="InvoiceCommodityIndex"
                                                             HeaderStyle-Width="90px" ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="InvoiceDetails"
                                                             Visible="true" Display="true" />
                                    <telerik:GridBoundColumn DataField="InvoiceCommodityMethod" UniqueName="CommodityMethod" HeaderText="Method" SortExpression="InvoiceCommodityMethod"
                                                             HeaderStyle-Width="70px" ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="InvoiceDetails"
                                                             Visible="true" Display="true" />
                                    <telerik:GridDateTimeColumn DataField="InvoiceIndexStartDate" UniqueName="IndexStartDate" HeaderText="Index Start" SortExpression="InvoiceIndexStartDate"
                                                                DataFormatString="{0:M/d/yyyy}" ReadOnly="true" ForceExtractValue="Always" ColumnGroupName="InvoiceDetails">
                                        <HeaderStyle Width="70px" />
                                    </telerik:GridDateTimeColumn>
                                    <telerik:GridDateTimeColumn DataField="InvoiceIndexEndDate" UniqueName="IndexEndDate" HeaderText="Index End" SortExpression="InvoiceIndexEndDate"
                                                                DataFormatString="{0:M/d/yyyy}" ReadOnly="true" ForceExtractValue="Always" ColumnGroupName="InvoiceDetails">
                                        <HeaderStyle Width="70px" />
                                    </telerik:GridDateTimeColumn>
                                    <telerik:GridNumericColumn DataField="InvoiceCommodityPurchasePricebookPriceAmount" UniqueName="InvoiceCommodityPurchasePricebookPriceAmount" HeaderText="Price $$" SortExpression="InvoiceCommodityPurchasePricebookPriceAmount"
                                                               Aggregate="Sum" ItemStyle-HorizontalAlign="Right" ReadOnly="true" ForceExtractValue="Always"
                                                               FooterAggregateFormatString="{0:$#,##0.00}" FooterStyle-HorizontalAlign="Right" FooterStyle-Font-Bold="true"
                                                               DataType="System.Decimal" DecimalDigits="4" DataFormatString="{0:$#,##0.0000}" NumericType="Currency" HeaderStyle-Width="90px" ItemStyle-Font-Bold="true"
                                                               ColumnGroupName="InvoiceDetails" />
                                    <telerik:GridBoundColumn DataField="InvoiceDeduct" UniqueName="InvoiceDeduct" HeaderText="Deduct" SortExpression="InvoiceSettlementDeduct"
                                                             HeaderStyle-Width="80px" ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="InvoiceDetails"
                                                             Visible="true" Display="true" />
                                    <telerik:GridNumericColumn DataField="InvoiceCommodityPurchasePricebookDeductAmount" UniqueName="InvoiceCommodityPurchasePricebookDeductAmount" HeaderText="Deduct $$" SortExpression="InvoiceCommodityPurchasePricebookDeductAmount"
                                                               Aggregate="Sum" ItemStyle-HorizontalAlign="Right" ReadOnly="true" ForceExtractValue="Always"
                                                               FooterAggregateFormatString="{0:$#,##0.00}" FooterStyle-HorizontalAlign="Right" FooterStyle-Font-Bold="true"
                                                               DataType="System.Decimal" DecimalDigits="4" DataFormatString="{0:$#,##0.0000}" NumericType="Currency" HeaderStyle-Width="90px" ItemStyle-Font-Bold="true"
                                                               ColumnGroupName="InvoiceDetails" />
                                    <telerik:GridBoundColumn DataField="InvoicePremium" UniqueName="InvoicePremium" HeaderText="Premium" SortExpression="InvoicePremium"
                                                             HeaderStyle-Width="80px" ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="InvoiceDetails"
                                                             Visible="true" Display="true" />
                                    <telerik:GridBoundColumn DataField="InvoicePremiumDesc" UniqueName="InvoicePremiumDesc" HeaderText="Premium Desc" SortExpression="InvoicePremiumDesc"
                                                             HeaderStyle-Width="200px" ReadOnly="true" ForceExtractValue="InBrowseMode" ColumnGroupName="InvoiceDetails"
                                                             Visible="true" Display="true" />
                                    <telerik:GridNumericColumn DataField="InvoiceCommodityPurchasePricebookPremiumAmount" UniqueName="InvoiceCommodityPurchasePricebookPremiumAmount" HeaderText="Premium $$" SortExpression="InvoiceCommodityPurchasePricebookPremiumAmount"
                                                               Aggregate="Sum" ItemStyle-HorizontalAlign="Right" ReadOnly="true" ForceExtractValue="Always"
                                                               FooterAggregateFormatString="{0:$#,##0.00}" FooterStyle-HorizontalAlign="Right" FooterStyle-Font-Bold="true"
                                                               DataType="System.Decimal" DecimalDigits="4" DataFormatString="{0:$#,##0.0000}" NumericType="Currency" HeaderStyle-Width="90px" ItemStyle-Font-Bold="true"
                                                               ColumnGroupName="InvoiceDetails" />
                                    <telerik:GridNumericColumn DataField="InvoiceCommodityPurchasePricebookTotalAmount" UniqueName="InvoiceCommodityPurchasePricebookTotalAmount" HeaderText="Total $$" SortExpression="InvoiceCommodityPurchasePricebookTotalAmount"
                                                               Aggregate="Sum" ItemStyle-HorizontalAlign="Right" ReadOnly="true" ForceExtractValue="Always"
                                                               FooterAggregateFormatString="{0:$#,##0.00}" FooterStyle-HorizontalAlign="Right" FooterStyle-Font-Bold="true"
                                                               DataType="System.Decimal" DecimalDigits="4" DataFormatString="{0:$#,##0.0000}" NumericType="Currency" HeaderStyle-Width="90px" ItemStyle-Font-Bold="true"
                                                               ColumnGroupName="InvoiceDetails" />
                                    <telerik:GridBoundColumn ReadOnly="true" HeaderText=" " HeaderStyle-Width="30px" UniqueName="Spacer" DataField="Spacer" />
                                    <telerik:GridBoundColumn DataField="ID" UniqueName="ID" Visible="false" Display="false" DataType="System.Int32" ForceExtractValue="Always" ReadOnly="true" />
                                </Columns>
                                <EditFormSettings ColumnNumber="3">
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column" ButtonType="ImageButton" CancelImageUrl="~/images/cancel.png"
                                                UpdateImageUrl="~/images/apply.png" InsertImageUrl="~/images/apply.png" />
                                </EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <HeaderStyle Wrap="False" />
                            <PagerStyle AlwaysVisible="True" />
                            <FilterMenu EnableImageSprites="False" />
                        </telerik:RadGrid>
                        <%-- This hidden field stores the orderNums of the currently retrieved/displayed orders --%>
                        <asp:HiddenField ID="hfOrderNums" runat="server" />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <telerik:RadScriptBlock runat="server">
        <script type="text/javascript">
            var fileDownloadCheckTimer, button, filterButton;
            function DisableFilterButton(sender) {
                DisableSender(filterButton = sender);
            }

            function DisableSender(sender) {
                sender.set_enabled(false);
            }

            function WaitForExport(sender, args) {
                //disable button
                DisableSender(button = sender);

                fileDownloadCheckTimer = window.setInterval(function () {
                    //get cookie and compare
                    var cookieValue = readCookie("fileDownloadToken");
                    if (cookieValue == "done") {
                        finishDownload(false);
                    }
                }, 1000);
            }

            function finishDownload(refresh) {
                window.clearInterval(fileDownloadCheckTimer);
                //clear cookie
                eraseCookie("fileDownloadToken");
                //enable button
                button.set_enabled(true);
                // rebind the data
                if (refresh) {
                    $("#" + "<%= cmdFilter.ClientID %>").click();
                }
            }

            function createCookie(name, value, days) {
                if (days) {
                    var date = new Date();
                    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                    var expires = "; expires=" + date.toGMTString();
                }
                else var expires = "";
                document.cookie = name + "=" + value + expires + "; path=/";
            }

            function readCookie(name) {
                var nameEQ = name + "=";
                var ca = document.cookie.split(';');
                for (var i = 0; i < ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
                }
                return null;
            }

            function eraseCookie(name) {
                createCookie(name, "", -1);
            };
        </script>
    </telerik:RadScriptBlock>
    <blc:RadGridDBCtrl ID="rgdbMain" runat="server" ControlID="rgMain" />
    <blac:DBDataSource ID="dsShipper" runat="server" SelectCommand="SELECT ID, Name FROM dbo.tblCustomer UNION SELECT -1, '(All)' ORDER BY Name" />
    <blac:DBDataSource ID="dsCarrier" runat="server"
                       SelectCommand="SELECT ID, CASE WHEN Active = 1 THEN '' ELSE 'Deleted: ' END + Name AS FullName, Active FROM dbo.viewCarrier UNION SELECT -1, '(All)', 1 ORDER BY Active DESC, FullName">
    </blac:DBDataSource>
    <blac:DBDataSource ID="dsProductGroup" runat="server" SelectCommand="SELECT ID, Name FROM tblProductGroup UNION SELECT -1, '(All)' ORDER BY Name" />
    <blac:DBDataSource ID="dsTruckType" runat="server" SelectCommand="SELECT ID, Name FROM tblTruckType UNION SELECT -1, '(All)' ORDER BY Name" />
    <blac:DBDataSource ID="dsDriverGroup" runat="server" SelectCommand="SELECT ID, Name FROM dbo.tblDriverGroup UNION SELECT -1, '(All)' ORDER BY Name" />
    <blac:DBDataSource ID="dsState" runat="server" SelectCommand="SELECT ID, Name = FullName FROM dbo.tblState UNION SELECT -1, '(All)' ORDER BY Name" />
    <blac:DBDataSource ID="dsProducer" runat="server" SelectCommand="SELECT ID, Name FROM dbo.tblProducer UNION SELECT -1, '(All)' ORDER BY Name" />
</asp:Content>

