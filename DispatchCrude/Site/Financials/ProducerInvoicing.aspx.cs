﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.IO;
using System.Drawing;
//add for sql stuff
using System.Data;
using System.Data.SqlClient;
using Telerik.Web.UI;
using AlonsIT;
using DispatchCrude.App_Code;
using DispatchCrude.DataExchange;
using DispatchCrude.Core;
using DispatchCrude.Extensions;

namespace DispatchCrude.Site.Orders
{
    public partial class ProducerInvoicing : System.Web.UI.Page
    {
        static protected string CHANGES_ENABLED = "DeleteEnabled", REPORT_SOURCE = "SourceTable";

        // list of fields to include (when showing summary only) 
        // only show those with ForceExtractValue="Always"

        protected void Page_Init(object sender, EventArgs e)
        {
            DateTime today = DateTime.Now.Date;
            while (today.DayOfWeek != DayOfWeek.Sunday)
            {
                today = today.AddDays(-1);
            }
            rdpStart.SelectedDate = today;
            rdpEnd.SelectedDate = today.AddDays(7);
            rdpStart.Calendar.ShowRowHeaders = rdpEnd.Calendar.ShowRowHeaders = false;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ConfigureAjax(!Settings.SettingsID.DisableAJAX.AsBool());

            //Print tab navigation html to page
            tabmenu.InnerHtml = NavigationHelper.PrintTabArray(NavigationHelper.TabSet_Commodities, "Tab_Settlement").ToString();

            if (!IsPostBack)
            {
                dsUserReport.SelectParameters["UserName"].DefaultValue = UserSupport.UserName;
            }
        }

        private void ConfigureAjax(bool enabled = true)
        {
            if (enabled)
            {
                // necessary to ensure the calendar popup displayes on Chrome
                rdpStart.EnableAjaxSkinRendering = rdpEnd.EnableAjaxSkinRendering = true;
                RadAjaxHelper.AddAjaxSetting(this.Page, ddlProducer, cmdSettleBatch);
                RadAjaxHelper.AddAjaxSetting(this.Page, ddlProducer, cmdApplyRates);
                RadAjaxHelper.AddAjaxSetting(this.Page, ddlProducer, txtInvoiceNum);
                RadAjaxHelper.AddAjaxSetting(this.Page, ddlProducer, txtNotes);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, cmdFilter, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, hfOrderNums, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, rgMain);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, cmdApplyRates, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, cmdSettleBatch, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, cmdExport, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, txtNotes, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, txtInvoiceNum, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdApplyRates, cmdApplyRates);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdApplyRates, rgMain);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdSettleBatch, cmdSettleBatch);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdSettleBatch, cmdApplyRates, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdSettleBatch, rgMain);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdSettleBatch, txtNotes, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdSettleBatch, txtInvoiceNum, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdSettleBatch, radWindowManager, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, rgMain, rgMain);
                RadAjaxHelper.AddAjaxSetting(this.Page, rgMain, txtNotes, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, rgMain, txtInvoiceNum, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdExport, cmdExport);
            }
            RadAjaxManager.GetCurrent(this.Page).EnableAJAX = enabled;
        }
        
        protected void Entity_SelectedIndexChanged(object source, EventArgs e)
        {
            //rgMain.Rebind();
        }

        protected void cmdFilter_Click(object source, EventArgs e)
        {
            // refresh the grid
            rgMain.Rebind();
            hfOrderNums.Value = string.Join(",", GridOrderNums());
            cmdFilter.Enabled = true;
        }

        private HashSet<int> GridOrderNums()
        {
            HashSet<int> ret = new HashSet<int>();

            // Collect all order numbers
            foreach (GridDataItem item in rgMain.Items)
            {
                ret.Add(Convert.ToInt32(item["OrderNum"].Text));
            }

            return ret;
        }

        protected void cmdApplyRates_Click(object source, EventArgs e)
        {
            if (rgMain.Items.Count > 0)
            {
                ApplyRates();
                // necessary to force a true refresh (not really sure why)
                rgMain.DataSource = null;
                rgMain.Rebind();
                cmdApplyRates.Enabled = true;
            }
        }

        private void AlertUser(string userMsg, int width = 250, int height = 75)
        {
            radWindowManager.RadAlert(userMsg, width, height, "Settlement Feedback", null);
        }

        protected void cmdSettleBatch_Click(object sender, EventArgs e)
        {
            if (rgMain.Items.Count == 0)
            {
                AlertUser("No orders were selected");
                return;
            }
            else
            {
                int count = 0;
                foreach (GridItem item in rgMain.Items)
                {
                    if (item is GridDataItem && (item.FindControl("chkSel") as CheckBox).Checked)
                    {
                        count++;
                        GridDataItem gdi = item as GridDataItem;
                        // if the rates have been applied, then also highlight this cell since the rate must be zero
                        if (gdi["InvoiceRatesAppliedDate"] == null
                            || Converter.IsNullOrEmpty(gdi["InvoiceRatesAppliedDate"].Text)
                            || gdi["InvoiceRatesAppliedDate"].BackColor == Color.Red)
                        {
                            AlertUser("ALL Orders must have valid rates & amounts assigned prior to being Settled", 500);
                            return;
                        }
                    }
                }
                if (count == 0)
                {
                    AlertUser("No orders were selected");
                    return;
                }
            }

            // update the orders as SETTLED (with a Settlement Batch)
            using (SSDB db = new SSDB(true))
            {
                object batchID = null;
                using (SqlCommand cmdCreateBatch = db.BuildCommand("spCreateProducerSettlementBatch"))
                {
                    cmdCreateBatch.Parameters["@ProducerID"].Value = ddlProducer.SelectedValue;
                    cmdCreateBatch.Parameters["@InvoiceNum"].Value = Converter.ToDBNullFromEmpty(txtInvoiceNum.Text);
                    cmdCreateBatch.Parameters["@Notes"].Value = Converter.ToDBNullFromEmpty(txtNotes.Text);
                    cmdCreateBatch.Parameters["@UserName"].Value = User.Identity.Name;
                    cmdCreateBatch.ExecuteNonQuery();
                    batchID = cmdCreateBatch.Parameters["@BatchID"].Value;
                }

                using (SqlCommand cmdSettleOrder = db.BuildCommand("spOrderSettlementProducerAddToBatch"))
                {
                    cmdSettleOrder.Parameters["@BatchID"].Value = batchID;
                    foreach (GridItem item in rgMain.Items)
                    {
                        if (item is GridDataItem && (item.FindControl("chkSel") as CheckBox).Checked)
                        {
                            // update the database to set the data row to SETTLED (with a BatchID)
                            cmdSettleOrder.Parameters["@OrderID"].Value = (item as GridDataItem).GetDataKeyValue("ID");
                            cmdSettleOrder.ExecuteNonQuery();
                        }
                    }
                }
                db.CommitTransaction();

                // navigate to the new batch
                Response.Redirect(string.Format("/Site/Financials/ProducerBatches.aspx?BatchID={0}", batchID));
            }   
        }

        private void ApplyRates(bool resetOverrides = false)
        {
            if (rgMain.Items.Count == 0)
                return;
            // Apply rates
            using (SSDB db = new SSDB(false))
            {
                using (SqlCommand cmd = db.BuildCommand("spApplyRatesProducer"))
                {
                    cmd.Parameters["@UserName"].Value = User.Identity.Name;
                    object id = null;
                    try
                    {
                        foreach (GridItem item in rgMain.Items)
                        {
                            if (item is GridDataItem && (item.FindControl("chkSel") as CheckBox).Checked)
                            {
                                cmd.Parameters["@ID"].Value = id = (item as GridDataItem).GetDataKeyValue("ID");
                                cmd.ExecuteNonQuery();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(string.Format("Exception processing Order with ID = {0}", id), ex);
                    }
                }
            }
            rgMain.Rebind();
        }

        private DataTable GetData()
        {
            using (SSDB db = new SSDB())
            {
                using (SqlCommand cmd = db.BuildCommand("spRetrieveOrdersFinancialProducer"))
                {
                    cmd.Parameters["@StartDate"].Value = rdpStart.SelectedDate;
                    cmd.Parameters["@EndDate"].Value = rdpEnd.SelectedDate.HasValue ? rdpEnd.SelectedDate : DateTime.Now.Date;
                    cmd.Parameters["@ShipperID"].Value = DropDownListHelper.SelectedValue(ddlShipper);
                    cmd.Parameters["@ProductGroupID"].Value = DropDownListHelper.SelectedValue(ddlProductGroup);
                    cmd.Parameters["@OriginStateID"].Value = DropDownListHelper.SelectedValue(ddlOriginState);
                    cmd.Parameters["@ProducerID"].Value = DropDownListHelper.SelectedValue(ddlProducer);
                    DataTable dt = SSDB.GetPopulatedDataTable(cmd);
                    dt.Columns.Add("spacer");
                    dt.Columns.Add("Selected", typeof(Boolean));
                    dt.Columns.Add("HasError", typeof(Boolean));
                    foreach (DataRow row in dt.Rows)
                    {
                        row["Selected"] = true;
                        row["HasError"] = false;
                    }
                    return dt;
                }
            }
        }

        protected void grid_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            (sender as RadGrid).DataSource = GetData();
        }

        protected void grid_DataBound(object sender, EventArgs e)
        {
            cmdFilter.Enabled = true;
            cmdApplyRates.Enabled = cmdExport.Enabled = rgMain.Items.Count > 0;
            // only allow settling a batch for a specific Producer
            cmdSettleBatch.Enabled = txtInvoiceNum.Enabled = txtNotes.Enabled = rgMain.Items.Count > 0 && DBHelper.ToInt32(ddlProducer.SelectedValue) > 0;
        }

        protected void grid_ItemCommand(object sender, GridCommandEventArgs e)
        {
            // the popup editor has already handled the "update" so just terminate editing and rebind
            if (e.CommandName == RadGrid.UpdateCommandName || e.CommandName == RadGrid.PerformInsertCommandName || e.CommandName == RadGrid.CancelCommandName)
            {
                e.Item.Edit = false;
                (sender as RadGrid).DataSource = null;
                (sender as RadGrid).Rebind();
            }
        }

        protected void grid_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            //Is it a GridDataItem
            if (e.Item is GridDataItem)
            {
                //Get the instance of the right type
                GridDataItem gdi = e.Item as GridDataItem;
                DataRowView rv = e.Item.DataItem as DataRowView;

                bool hasInvalidRates = false, hasManualRates = false;
                //Check the formatting condition

                /* ensure the order has already been APPROVED */
                if (!DBHelper.ToBoolean(rv["Approved"]))
                {
                    gdi["Approved"].BackColor = Color.Red;
                    hasInvalidRates = true;
                }

                if (Converter.IsNullOrEmpty(rv["InvoiceCommodityPurchasePricebookID"]))
                {
                    SettlementRateCellHelper.MarkCellAsOverride(gdi, "IndexPrice", Color.Red, SettlementRateCellHelper.MISSING_RATE_TEXT);
                    hasInvalidRates = true;
                }

                // if the rates have been applied, then indicate that certain notable conditions apply (Override or missing rates)
                if (gdi["InvoiceRatesAppliedDate"] != null && !Converter.IsNullOrEmpty(gdi["InvoiceRatesAppliedDate"].Text))
                {
                    // determine if any Manual Rates are present (that aren't already determined)
                    if (!hasManualRates)
                    {
                        foreach (GridColumn col in gdi.OwnerTableView.Columns)
                        {
                            if (gdi[col].BackColor == Color.Olive)
                            {
                                hasManualRates = true;
                                break;
                            }
                        }
                    }
                    if (hasInvalidRates || hasManualRates)
                    {
                        Color backColor = hasInvalidRates ? Color.Red : Color.Olive;
                        SettlementRateCellHelper.MarkCellAsOverride(gdi, "InvoiceRatesAppliedDate", backColor, null);
                    }
                }
                rv["HasError"] = hasInvalidRates;
                (RadGridHelper.GetControlByType(gdi["HasError"], typeof(CheckBox)) as CheckBox).Checked = hasInvalidRates;
            }

        }

        protected void menuApplyRates_ItemClick(object sender, RadMenuEventArgs e)
        {
            if (rgMain.Items.Count > 0)
            {
                ApplyRates(true);
                // necessary to force a true refresh (not really sure why)
                rgMain.DataSource = null;
                rgMain.Rebind();
                cmdApplyRates.Enabled = true;
            }
        }

        protected void rcbUserReport_ItemDataBound(object sender, RadComboBoxItemEventArgs e)
        {
            e.Item.Attributes.Add(CHANGES_ENABLED
                , (UserSupport.IsInRole("Administrator") || UserSupport.IsInRole("Management") || !DBHelper.IsNull((e.Item.DataItem as DataRowView)["UserNames"])).ToString());
            e.Item.Attributes.Add(REPORT_SOURCE, (e.Item.DataItem as DataRowView)[REPORT_SOURCE].ToString());
        }

        protected void cmdExport_Click(object source, EventArgs e)
        {
            if (rgMain.Items.Count > 0)
            {
                Export(new IdContainer(hfOrderNums.Value).IntIDS);
            }
        }

        private void Export(HashSet<int> orderNumbers)
        {
            string filename = "PRODUCER_SETTLEMENT_PREVIEW.xlsx";
            MemoryStream ms = (MemoryStream)new ReportCenterExcelExport(UserSupport.UserName
                    , rcbUserReport.SelectedItem.Attributes[REPORT_SOURCE].ToString()
                    , RadComboBoxHelper.SelectedValue(rcbUserReport))
                .Export(rdpStart.SelectedDate.Value, rdpEnd.SelectedDate.Value, true, orderNumbers);
            Response.ExportExcelStream(ms, filename);
        }        
    }
}