﻿using System;
using System.IO;
//add for sql stuff
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using Telerik.Web.UI;
using AlonsIT;
using DispatchCrude.App_Code;
using DispatchCrude.Core;
using DispatchCrude.Extensions;
using DispatchCrude.Models;

namespace DispatchCrude.Site.Orders
{
    public partial class ShipperBatches : System.Web.UI.Page
    {

        static protected string CHANGES_ENABLED = "DeleteEnabled", REPORT_SOURCE = "SourceTable";

        protected void Page_Load(object sender, EventArgs e)
        {
            ConfigureAjax(!Settings.SettingsID.DisableAJAX.AsBool());

            //Print tab navigation html to page
            tabmenu.InnerHtml = NavigationHelper.PrintTabArray(NavigationHelper.TabSet_Settlement, "Tab_Batches").ToString();

            //Print button navigation html to page
            speedbuttons.InnerHtml = NavigationHelper.PrintButtonArray(NavigationHelper.ButtonSet_TabBatches, "Button_Shipper").ToString();

            // Set the prompt text of the unsettle button
            if (Settings.SettingsID.Settlement_IncludePendingStatus.AsBool())
            {
                cmdUnsettle.OnClientClick = "javascript:if(!confirm('" + SettlementBatch.REVERT_PENDING_WARNING_TEXT + "')){return false;}";
            }
            else
            {
                cmdUnsettle.OnClientClick = "javascript:if(!confirm('" + SettlementBatch.UNSETTLE_WARNING_TEXT + "')){return false;}";
            }

            if (!IsPostBack)
            {
                dsUserReport.SelectParameters["UserName"].DefaultValue = UserSupport.UserName;

                // default values
                //SetDefaultDateFilter();
                PopulateShippers();

                if (Request.QueryString["BatchID"] != null)
                {
                    string batchID = Request.QueryString["BatchID"];
                    using (SSDB db = new SSDB())
                    {   // set the shipper to the assigned Batch.ShipperID
                        DropDownListHelper.SetSelectedValue(ddlShipper, db.QuerySingleValue("SELECT ShipperID FROM tblShipperSettlementBatch WHERE ID={0}", batchID).ToString());
                        // set the start/end date filters to the date of the batch
                        rdpStart.SelectedDate = rdpEnd.SelectedDate = DBHelper.ToDateTime(db.QuerySingleValue("SELECT BatchDate FROM tblShipperSettlementBatch WHERE ID={0}", batchID));
                    }
                    PopulateBatches();
                    // set the batch to the requested batchID
                    DropDownListHelper.SetSelectedValue(ddlBatch, batchID);
                }
                else if (Request.QueryString["BatchNum"] != null)
                {
                    string batchNum = Request.QueryString["BatchNum"];
                    string batchID = null;
                    using (SSDB db = new SSDB())
                    {   // get batchID
                        batchID = db.QuerySingleValue("SELECT ID FROM tblShipperSettlementBatch WHERE BatchNum={0}", batchNum).ToString();
                        // set the shipper to the assigned Batch.ShipperID
                        DropDownListHelper.SetSelectedValue(ddlShipper, db.QuerySingleValue("SELECT ShipperID FROM tblShipperSettlementBatch WHERE ID={0}", batchID).ToString());
                        // set the start/end date filters to the date of the batch
                        rdpStart.SelectedDate = rdpEnd.SelectedDate = DBHelper.ToDateTime(db.QuerySingleValue("SELECT BatchDate FROM tblShipperSettlementBatch WHERE ID={0}", batchID));
                    }
                    PopulateBatches();
                    // set the batch to the requested batchID
                    DropDownListHelper.SetSelectedValue(ddlBatch, batchID);
                    txtBatchNum.Text = batchNum;
                }

                ddlBatch_SelectedIndexChanged(ddlBatch, EventArgs.Empty);
            }

            // Show/hide the unsettle batch button based on user's role
            cmdUnsettle.Visible = UserSupport.IsInRole("unsettleShipperBatches");
        }

        private void SetDefaultDateFilter()
        {
            DateTime today = DateTime.Now.Date;
            while (today.DayOfWeek != DayOfWeek.Sunday)
            {
                today = today.AddDays(-1);
            }
            rdpStart.SelectedDate = today;
            rdpEnd.SelectedDate = today.AddDays(7);
        }

        private void ConfigureAjax(bool enabled = true)
        {
            if (enabled)
            {
                // necessary to ensure the calendar popup displayes on Chrome
                rdpStart.EnableAjaxSkinRendering = rdpEnd.EnableAjaxSkinRendering = true;

                RadAjaxHelper.AddAjaxSetting(this.Page, txtOrderNum, txtOrderNum);
                RadAjaxHelper.AddAjaxSetting(this.Page, txtOrderNum, ddlShipper);
                RadAjaxHelper.AddAjaxSetting(this.Page, txtOrderNum, rdpStart);
                RadAjaxHelper.AddAjaxSetting(this.Page, txtOrderNum, rdpEnd);

                RadAjaxHelper.AddAjaxSetting(this.Page, txtOrderNum, rgMain);
                RadAjaxHelper.AddAjaxSetting(this.Page, txtOrderNum, ddlBatch, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, txtOrderNum, txtCreatedByUser, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, txtOrderNum, txtCreateDate, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, txtOrderNum, txtInvoiceNum, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, txtOrderNum, txtNotes, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, txtOrderNum, cmdUnsettle, false);

                RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, cmdFilter, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, rgMain);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, ddlBatch, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, txtCreatedByUser, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, txtCreateDate, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, txtInvoiceNum, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, txtNotes, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, cmdUnsettle, false);

                RadAjaxHelper.AddAjaxSetting(this.Page, ddlBatch, txtInvoiceNum, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, ddlBatch, txtNotes, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, ddlBatch, txtCreatedByUser, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, ddlBatch, txtCreateDate, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, ddlBatch, rgMain);

                RadAjaxHelper.AddAjaxSetting(this.Page, rgMain, rgMain);

                RadAjaxHelper.AddAjaxSetting(this.Page, cmdUnsettle, cmdUnsettle, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdUnsettle, rgMain);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdUnsettle, ddlBatch, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdUnsettle, txtCreatedByUser, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdUnsettle, txtCreateDate, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdUnsettle, txtInvoiceNum, false);
                RadAjaxHelper.AddAjaxSetting(this.Page, cmdUnsettle, txtNotes, false);
            }
            RadAjaxManager.GetCurrent(this.Page).EnableAJAX = enabled;
        }

        private void PopulateShippers()
        {
            using (SSDB db = new SSDB())
            {
                ddlShipper.DataSource = db.GetPopulatedDataTable("SELECT ID, FullName = Name FROM dbo.viewCustomer UNION SELECT -1, '(All)' ORDER BY FullName");
            }
            ddlShipper.DataBind();
        }

        private void PopulateBatches()
        {
            string dateField = ddlDateField.SelectedValue;
            using (SSDB db = new SSDB(false))
            {
                using (SqlCommand cmd = db.BuildCommand(
                    "SELECT * FROM dbo.viewShipperSettlementBatch"
                    + " WHERE (@ShipperID = -1 OR ShipperID = @ShipperID) "
                    + (rdpStart.SelectedDate != null && rdpEnd.SelectedDate != null ? (" AND " + dateField + " BETWEEN @StartDate AND @EndDate ") : "")
                    + " AND IsFinal = 1"
                    + " AND (@OrderNum = '' OR ID IN (SELECT BatchID FROM dbo.tblOrderSettlementShipper WHERE OrderID = (SELECT ID FROM tblOrder WHERE OrderNum = @OrderNum)))"
                    + " AND (@BatchNum = '' OR BatchNum = @BatchNum)"
                    + " ORDER BY BatchNum DESC"))
                {
                    cmd.Parameters.AddWithValue("@ShipperID", ddlShipper.SelectedValue);
                    if (rdpStart.SelectedDate != null)
                        cmd.Parameters.AddWithValue("@StartDate", rdpStart.SelectedDate);
                    if (rdpEnd.SelectedDate != null)
                        cmd.Parameters.AddWithValue("@EndDate", rdpEnd.SelectedDate);
                    cmd.Parameters.AddWithValue("@BatchNum", txtBatchNum.Text);
                    cmd.Parameters.AddWithValue("@OrderNum", txtOrderNum.Text);
                    ddlBatch.DataSource = Core.DateHelper.AddLocalRowStateDateFields(SSDB.GetPopulatedDataTable(cmd));
                }
            }
            ddlBatch.DataBind();
            cmdUnsettle.Enabled = ddlBatch.SelectedIndex != -1;
        }

        //Delete the batch record for the currently selected batch
        private void UnsettleBatch(string BatchID)
        {
            if (!String.IsNullOrEmpty(BatchID))
            {
                new ShipperSettlementBatch().UnsettleBatch(BatchID);
            }
        }

        protected void cmdFilter_Click(object source, EventArgs e)
        {
            PopulateBatches();
            ddlBatch_SelectedIndexChanged(this.ddlBatch, EventArgs.Empty);
        }

        //Unsettle the selected batch (this is a role restricted action)
        protected void cmdUnsettle_Click(object source, EventArgs e)
        {
            if (UserSupport.IsInRole("unsettleShipperBatches"))
            {
                // Delete the batch record
                UnsettleBatch(ddlBatch.SelectedValue);

                // Return to the settlement page and preselect the batch if pending
                string url = "/Site/Financials/ShipperInvoicing.aspx";
                if (Settings.SettingsID.Settlement_IncludePendingStatus.AsBool())
                    url = url + "?BatchID={0}";
                Response.Redirect(string.Format(url, ddlBatch.SelectedValue));
            }
        }

        protected void ddlBatch_SelectedIndexChanged(object source, EventArgs e)
        {
            if (ddlBatch.SelectedIndex == -1)
            {
                txtCreatedByUser.Text = txtCreateDate.Text = txtInvoiceNum.Text = txtNotes.Text = string.Empty;
            }
            else
            {
                using (SSDB db = new SSDB(false))
                {
                    txtCreatedByUser.Text = db.QuerySingleValue("SELECT CreatedByUser FROM tblShipperSettlementBatch WHERE ID={0}", ddlBatch.SelectedValue).ToString();
                    txtCreateDate.Text = Core.DateHelper.ToLocal(
                        DBHelper.ToDateTime(db.QuerySingleValue("SELECT CreateDateUTC FROM tblShipperSettlementBatch WHERE ID={0}", ddlBatch.SelectedValue))
                        , Context).ToString("M/d/yy h:mm tt");
                    txtInvoiceNum.Text = db.QuerySingleValue("SELECT InvoiceNum FROM tblShipperSettlementBatch WHERE ID={0}", ddlBatch.SelectedValue).ToString();
                    txtNotes.Text = db.QuerySingleValue("SELECT Notes FROM tblShipperSettlementBatch WHERE ID={0}", ddlBatch.SelectedValue).ToString();
                }
            }
            PopulateBatchOrders();
        }

        protected void rcbUserReport_ItemDataBound(object sender, RadComboBoxItemEventArgs e)
        {
            e.Item.Attributes.Add(CHANGES_ENABLED
                , ((UserSupport.IsInGroup("_DCAdministrator") || UserSupport.IsInGroup("_DCSystemManager") || UserSupport.IsInGroup("_DCSupport")) || !DBHelper.IsNull((e.Item.DataItem as DataRowView)["UserNames"])).ToString());
            e.Item.Attributes.Add(REPORT_SOURCE, (e.Item.DataItem as DataRowView)[REPORT_SOURCE].ToString());
        }

        protected void cmdExport_Click(object source, EventArgs e)
        {
            if (ddlBatch.SelectedIndex != -1)
                Export();
        }

        private void Export()
        {
            string filename = Path.GetFileNameWithoutExtension(rtxFileName.Text) + ".xlsx";
            MemoryStream ms = (MemoryStream)new DataExchange.ReportCenterExcelExport(UserSupport.UserName
                    , rcbUserReport.SelectedItem.Attributes[REPORT_SOURCE].ToString()
                    , RadComboBoxHelper.SelectedValue(rcbUserReport))
                .ExportShipperBatch(DropDownListHelper.SelectedValue(ddlBatch));
            Response.ExportExcelStream(ms, filename);
        }

        protected void PopulateBatchOrders()
        {
            using (SSDB db = new SSDB())
            {
                using (SqlCommand cmd = db.BuildCommand("spRetrieveOrdersFinancialShipper"))
                {
                    cmd.Parameters["@BatchID"].Value = DropDownListHelper.SelectedValue(ddlBatch);
                    cmd.Parameters["@UserName"].Value = User.Identity.Name;
                    DataTable dt = SSDB.GetPopulatedDataTable(cmd);
                    dt.Columns.Add("spacer");
                    this.rgMain.DataSource = Core.DateHelper.AddLocalRowStateDateFields(dt, Context);
                }
            }
            this.rgMain.DataBind();
        }

        protected void grid_ItemCreated(object sender, GridItemEventArgs e)
        {

        }

        protected void grid_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            //Is it a GridDataItem
            if (e.Item is GridDataItem)
            {
                //Get the instance of the right type
                GridDataItem gdi = e.Item as GridDataItem;
                DataRowView rv = e.Item.DataItem as DataRowView;
                SettlementRateCellHelper.HandleOtherDetails(gdi, rv, "Shipper", false);
            }
        }

        protected void grid_DataBound(object sender, EventArgs e)
        {
            cmdFilter.Enabled = true;
        }

        protected DataRow GetOrder(int orderNum)
        {
            using (SSDB db = new SSDB())
            {
                DataTable dt = db.GetPopulatedDataTable(
                    @"SELECT OrderID = O.ID, SB.BatchDate, SB.ShipperID
                        FROM tblOrder O
                        JOIN tblOrderSettlementShipper OS ON OS.OrderID = O.ID
                        JOIN tblShipperSettlementBatch SB ON SB.ID = OS.BatchID
                        WHERE O.OrderNum = {0}", (object)DBHelper.QuoteStr(orderNum));
                return dt.Rows.Count > 0 ? dt.Rows[0] : null;
            }
        }
    }
}
