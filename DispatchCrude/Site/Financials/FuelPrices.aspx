﻿<%@ Page Title="Accessorial" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FuelPrices.aspx.cs" Inherits="DispatchCrude.Site.Financials.FuelPrices" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="headContent" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="mainContent" ContentPlaceHolderID="MainContent" runat="server">
    <script>
        $("#ctl00_ctl00_EntityCaption").html("Fuel Prices");
    </script>
    <div class="tabbable TabRepaint">
        <ul class="nav nav-tabs" id="tabmenu" runat="server">
            <!--Tabs will print here (see code behind)-->
        </ul>
        <div class="tab-content">
            <div class="tab-pane in active">

                <div id="DataEntryFormGrid" style="height:100%">
                    <asp:ValidationSummary ID="validationSummaryMain" runat="server" DisplayMode="BulletList" Enabled="true" EnableClientScript="true" CssClass="NullValidator" />
                    <telerik:RadGrid ID="rgMain" runat="server" CellSpacing="0" GridLines="None" CssClass="GridRepaint" EnableLinqExpressions="false"
                                     AutoGenerateColumns="False" ShowGroupPanel="True" AllowFilteringByColumn="true" AllowSorting="True" AllowPaging="True" Height="800"
                                     EnableEmbeddedSkins="true" Skin="Vista" AlternatingItemStyle-BackColor="#dcf2fc" OnItemCommand="grid_ItemCommand"
                                     PageSize='<%# Settings.DefaultPageSize %>'>
                        <ClientSettings AllowDragToGroup="True">
                            <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                        </ClientSettings>
                        <GroupingSettings CaseSensitive="false" ShowUnGroupButton="true" />
                        <ExportSettings FileName="FuelPrices" ExportOnlyData="true" IgnorePaging="true" HideStructureColumns="true" Excel-FileExtension="xls" Excel-Format="Biff" OpenInNewWindow="true" />
                        <MasterTableView DataKeyNames="ID" CommandItemDisplay="Top" CommandItemSettings-AddNewRecordText="Add New PADD Fuel Price" EditMode="InPlace">
                            <CommandItemSettings ShowExportToExcelButton="true" />
                            <RowIndicatorColumn Visible="True">
                                <HeaderStyle Width="20px" />
                            </RowIndicatorColumn>
                            <ExpandCollapseColumn Visible="True">
                                <HeaderStyle Width="20px" />
                            </ExpandCollapseColumn>
                            <Columns>
                                <telerik:GridTemplateColumn UniqueName="ActionColumn" HeaderStyle-Width="50px" AllowFiltering="false">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" CssClass="btn btn-xs btn-default shiny" CommandName="Edit" ImageUrl="~/images/edit.png" CausesValidation="false" />
                                        <asp:ImageButton runat="server" CssClass="btn btn-xs btn-default shiny" id="cmdDelete" CommandName="Delete" ImageUrl="~/images/delete.png"
                                                         OnClientClick="return confirm('Permanently delete?');" CausesValidation="false" />
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:ImageButton runat="server" CssClass="btn btn-xs btn-default shiny" CommandName="Update" ImageUrl="~/images/apply_imageonly.png" CausesValidation="true" />
                                        <asp:ImageButton runat="server" CssClass="btn btn-xs btn-default shiny" CommandName="Cancel" ImageUrl="~/images/cancel_imageonly.png" CausesValidation="false" />
                                    </EditItemTemplate>
                                    <InsertItemTemplate>
                                        <asp:ImageButton runat="server" CssClass="btn btn-xs btn-default shiny" CommandName="PerformInsert" ImageUrl="~/images/apply_imageonly.png" CausesValidation="true" />
                                        <asp:ImageButton runat="server" CssClass="btn btn-xs btn-default shiny" CommandName="Cancel" ImageUrl="~/images/cancel_imageonly.png" CausesValidation="false" />
                                    </InsertItemTemplate>
                                </telerik:GridTemplateColumn>

                                <telerik:GridDateTimeColumn DataField="EffectiveDate" HeaderText="Effective Date" UniqueName="EffectiveDate"
                                                            DataFormatString="{0:M/d/yyyy}" HeaderStyle-Width="70px" FilterControlWidth="70%">
                                </telerik:GridDateTimeColumn>
                                <telerik:GridDropDownColumn DataField="EIAPADDRegionID" HeaderText="PADD Region" UniqueName="EIAPADDRegionID" HeaderStyle-Width="120px"
                                                            DataSourceID="dsPADDRegion" ListTextField="Name" ListValueField="ID"
                                                            EnableEmptyListItem="true" EmptyListItemText="Select..." EmptyListItemValue="0">
                                    <ColumnValidationSettings EnableRequiredFieldValidation="true">
                                        <RequiredFieldValidator InitialValue="Select..." CssClass="NullValidator" ErrorMessage="PADD Region is required" Text="&nbsp;!" />
                                    </ColumnValidationSettings>
                                </telerik:GridDropDownColumn>
                                <telerik:GridNumericColumn DataField="Price" HeaderText="Price" SortExpression="Price" DataType="System.Decimal" NumericType="Number"
                                                           MinValue="0" MaxValue="1000" DecimalDigits="3"
                                                           FilterControlWidth="70%" UniqueName="Price" HeaderStyle-Width="70px">
                                    <ColumnValidationSettings EnableRequiredFieldValidation="true">
                                        <RequiredFieldValidator CssClass="NullValidator" ErrorMessage="Price is required" Text="&nbsp;!" />
                                    </ColumnValidationSettings>
                                </telerik:GridNumericColumn>

                                <telerik:GridBoundColumn DataField="ID" DataType="System.Int32" HeaderText="ID" ReadOnly="True"
                                                         SortExpression="ID" UniqueName="ID" Visible="False" ForceExtractValue="Always" />

                                <telerik:GridBoundColumn DataField="CreateDate" UniqueName="CreateDate" SortExpression="CreateDate" DataType="System.DateTime" DataFormatString="{0:M/d/yy}"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Create Date" HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="CreatedByUser" UniqueName="CreatedByUser" SortExpression="CreatedByUser"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Created By" HeaderStyle-Width="90px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="LastChangeDate" UniqueName="LastChangeDate" SortExpression="LastChangeDate" DataType="System.DateTime" DataFormatString="{0:M/d/yy HH:mm}"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Last Update Date" HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                <telerik:GridBoundColumn DataField="LastChangedByUser" UniqueName="LastChangedByUser" SortExpression="LastChangedByUser"
                                                         ForceExtractValue="Always" ReadOnly="true"
                                                         HeaderText="Last Updated By" HeaderStyle-Width="95px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />

                            </Columns>
                            <GroupByExpressions>
                                <telerik:GridGroupByExpression>
                                    <SelectFields>
                                        <telerik:GridGroupByField FieldAlias="EffectiveDate" FieldName="EffectiveDate"
                                                                  FormatString="" HeaderText="Effective Date" />
                                    </SelectFields>
                                    <GroupByFields>
                                        <telerik:GridGroupByField FieldAlias="EffectiveDate" FieldName="EffectiveDate"
                                                                  FormatString="" HeaderText="" SortOrder="Descending" />
                                    </GroupByFields>
                                </telerik:GridGroupByExpression>
                            </GroupByExpressions>
                            <EditFormSettings ColumnNumber="5">
                                <EditColumn FilterControlAltText="Filter EditCommandColumn column" ButtonType="ImageButton" CancelImageUrl="~/images/cancel.png"
                                            UpdateImageUrl="~/images/apply.png" InsertImageUrl="~/images/apply.png" />
                            </EditFormSettings>
                            <PagerStyle AlwaysVisible="True" />
                        </MasterTableView>
                        <HeaderStyle Wrap="False" />
                        <PagerStyle AlwaysVisible="True" />
                        <FilterMenu EnableImageSprites="False">
                        </FilterMenu>
                    </telerik:RadGrid>
                </div>
                <blc:RadGridDBCtrl ID="dbcMain" runat="server"
                                   ControlID="rgMain"
                                   UpdateTableName="tblFuelPrice"
                                   SelectCommand="SELECT * FROM dbo.tblFuelPrice WHERE EffectiveDate > dateadd(month, -6, getdate()) ORDER BY EffectiveDate DESC, EIAPADDRegionID" />
                <blac:DBDataSource ID="dsPADDRegion" runat="server" SelectCommand="SELECT ID, Name FROM dbo.tblEIAPADDRegion ORDER BY Name" />
            </div>
        </div>
    </div>
</asp:Content>
