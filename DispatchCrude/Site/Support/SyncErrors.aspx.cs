﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.IO;
//add for sql stuff
using System.Data;
using AlonsIT;
using DispatchCrude.Core;
using DispatchCrude.Extensions;

namespace DispatchCrude.Site.Support
{
    public partial class SyncErrors : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            rdpStart.SelectedDate = rdpEnd.SelectedDate = DateTime.Now.Date;
            rdpStart.Calendar.ShowRowHeaders = rdpEnd.Calendar.ShowRowHeaders = false;

            //Print tab navigation html to page
            tabmenu.InnerHtml = NavigationHelper.PrintTabArray(NavigationHelper.TabSet_Logs, "Tab_SyncErrors").ToString();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ConfigureAjax(!Settings.SettingsID.DisableAJAX.AsBool());
        }

        private bool IsInRole(string roleName)
        {
            return Context.User.IsInRole(roleName);
        }

        private void ConfigureAjax(bool enabled = true)
        {
            App_Code.RadAjaxHelper.AddAjaxSetting(this.Page, rgMain, rgMain);
            App_Code.RadAjaxHelper.AddAjaxSetting(this.Page, cmdFilter, rgMain);
        }

        protected void cmdFilter_Click(object source, EventArgs e)
        {
            // refresh the grid
            this.rgMain.Rebind();
        }
        protected void cmdExportToExcel_Click(object source, EventArgs e)
        {
            ExportToExcel();
        }

        private void ExportToExcel()
        {
            string filename = string.Format("SyncErrors_{0:yyyyMMdd}-{1:yyyyMMdd}.xlsx", rdpStart.SelectedDate.Value, rdpEnd.SelectedDate.Value);
            MemoryStream ms = new App_Code.RadGridExcelExporter().ExportSheet(rgMain.MasterTableView);
            Response.ExportExcelStream(ms, filename);
        }

        protected void grid_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            using (SSDB ssdb = new SSDB())
            {
                DataTable dt = ssdb.GetPopulatedDataTable("SELECT * FROM viewSyncError WHERE ({0} = 0 OR DriverID = {0}) AND CreateDateUTC BETWEEN dbo.fnLocal_to_UTC_User({2}, {1}) AND dbo.fnLocal_to_UTC_User(dateadd(day, 1, {3}), {1}) ORDER BY CreateDateUTC DESC"
                    , !Page.IsPostBack ? -1 : App_Code.RadComboBoxHelper.SelectedValue(rcbDriver)
                    , DBHelper.QuoteStr(UserSupport.UserName)
                    , DBHelper.QuoteStr(rdpStart.SelectedDate.Value.ToString("M/d/yyyy"))
                    , DBHelper.QuoteStr(rdpEnd.SelectedDate.Value.ToString("M/d/yyyy")));
                rgMain.DataSource = Core.DateHelper.AddLocalRowStateDateFields(dt);
            }
        }
        
    }
}