﻿<%@  Title="Business Rules" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CarrierRules.aspx.cs"
    Inherits="DispatchCrude.Site.Compliance.CarrierRules" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <telerik:GridNumericColumnEditor ID="num10DigitEditor" runat="server">
        <NumericTextBox runat="server" EnabledStyle-HorizontalAlign="Right" MinValue="0">
            <NumberFormat AllowRounding="true" DecimalDigits="10" KeepNotRoundedValue="true" KeepTrailingZerosOnFocus="false" />
        </NumericTextBox>
    </telerik:GridNumericColumnEditor>
    <div>
        <script>
            $("#ctl00_ctl00_EntityCaption").html("Carrier Rules");
        </script>
        <div class="tabbable TabRepaint">
            <ul class="nav nav-tabs" id="tabmenu" runat="server">
                <!--Tabs will print here (see code behind)-->
            </ul>
            <div class="tab-content">
                <div class="tab-pane in active">

                    <div class="leftpanel">
                        <div class="tabbable">
                            <ul class="nav nav-tabs">
                                <li class="active tab-blue">
                                    <a data-toggle="tab" href="#Filters" aria-expanded="true">Filters</a>
                                </li>
                                <li class="tab-green">
                                    <a data-toggle="tab" href="#Export" aria-expanded="true">Export</a>
                                </li>
                            </ul>
                            <div id="leftTabs" class="tab-content">
                                <div id="Filters" class="tab-pane active">
                                    <asp:Panel ID="panelFilter" runat="server" DefaultButton="btnRefresh">
                                        <div class="Entry">
                                            <asp:Label ID="lblType" runat="server" Text="Type" AssociatedControlID="ddType" />
                                            <asp:DropDownList CssClass="btn-xs" Width="100%" runat="server" ID="ddType" DataTextField="Name" DataValueField="ID" DataSourceID="dsTypeAll" />
                                        </div>
                                        <div class="Entry">
                                            <asp:Label ID="lblDriver" runat="server" Text="Driver" AssociatedControlID="ddDriver" />
                                            <asp:DropDownList CssClass="btn-xs" Width="100%" runat="server" ID="ddDriver" DataTextField="Name" DataValueField="ID" DataSourceID="dsDriver" />
                                        </div>
                                        <div class="Entry">
                                            <asp:Label ID="lblCarrier" runat="server" Text="Carrier" AssociatedControlID="ddCarrier" />
                                            <asp:DropDownList CssClass="btn-xs" Width="100%" runat="server" ID="ddCarrier" DataTextField="Name" DataValueField="ID" DataSourceID="dsCarrier" />
                                        </div>
                                        <div class="Entry">
                                            <asp:Label ID="lblTerminal" runat="server" Text="Terminal" AssociatedControlID="ddTerminal" />
                                            <asp:DropDownList CssClass="btn-xs" Width="100%" runat="server" ID="ddTerminal" DataTextField="Name" DataValueField="ID" DataSourceID="dsTerminal" />
                                        </div>
                                        <div class="Entry">
                                            <asp:Label ID="lblState" runat="server" Text="Driver Operating State" AssociatedControlID="ddState" />
                                            <asp:DropDownList CssClass="btn-xs" Width="100%" runat="server" ID="ddState" DataTextField="Name" DataValueField="ID" DataSourceID="dsState" />
                                        </div>
                                        <div class="Entry">
                                            <asp:Label ID="lblRegion" runat="server" Text="Region" AssociatedControlID="ddRegion" />
                                            <asp:DropDownList CssClass="btn-xs" Width="100%" runat="server" ID="ddRegion" DataTextField="Name" DataValueField="ID" DataSourceID="dsRegion" />
                                        </div>                                        
                                        <div>
                                            <div class="Entry floatLeft">
                                                <asp:Label ID="lblStartDate" runat="server" Text="Start Date" AssociatedControlID="rdpStartDate" CssClass="Entry" />
                                                <telerik:RadDatePicker ID="rdpStartDate" runat="server" Width="100px">
                                                    <DateInput runat="server" DateFormat="M/d/yyyy" />
                                                    <DatePopupButton Enabled="true" />
                                                </telerik:RadDatePicker>
                                            </div>
                                            <div class="Entry floatLeft">
                                                <asp:Label ID="lblEndDate" runat="server" Text="End Date" AssociatedControlID="rdpEndDate" CssClass="Entry" />
                                                <telerik:RadDatePicker ID="rdpEndDate" runat="server" Width="100px">
                                                    <DateInput runat="server" DateFormat="M/d/yyyy" />
                                                    <DatePopupButton Enabled="true" />
                                                </telerik:RadDatePicker>
                                            </div>
                                            <div class="center">
                                                <asp:Button ID="btnRefresh" runat="server" Text="Refresh" CssClass="btn btn-blue shiny" OnClick="btnRefresh_Click" />
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                                <div id="Export" class="tab-pane">
                                    <asp:Panel ID="panelExcel" runat="server" DefaultButton="cmdExport">
                                        <div>
                                            <div>
                                                <asp:Button ID="cmdExport" runat="server" Text="Export to Excel" CssClass="btn btn-blue shiny" Enabled="true" OnClick="cmdExport_Click" />
                                            </div>
                                            <%--
                                            <div class="spacer10px"></div>
                                            <div>
                                                <asp:Button ID="cmdImport" runat="server" ClientIDMode="Static" Text="Import Excel file" CssClass="floatRight btn btn-blue shiny"
                                                            Enabled="false" OnClick="cmdImport_Click" />
                                            </div>
                                        </div>
                                        <div class="spacer10px"></div>
                                        <div class="center">
                                            <asp:FileUpload ID="excelUpload" runat="server" ClientIDMode="Static" CssClass="floatLeft" Width="99%" />
                                            <asp:CustomValidator ID="cvUpload" runat="server" ControlToValidate="excelUpload" CssClass="NullValidator floatLeft" Display="Dynamic"
                                                                 Text="*" ErrorMessage="Only Excel (*.xlsx) files allowed)" OnServerValidate="cvUpload_ServerValidate" />
                                            --%>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="gridArea" style="height: 100%; min-height: 500px;">
                        <telerik:RadGrid ID="rgMain" runat="server" EnableHeaderContextMenu="False" CellSpacing="0" GridLines="None" EnableLinqExpressions="false"
                                         AllowSorting="True" AllowFilteringByColumn="false" Height="800" CssClass="GridRepaint" ShowGroupPanel="false" EnableEmbeddedSkins="true" Skin="Vista" AlternatingItemStyle-BackColor="#dcf2fc"
                                         DataSourceID="dsMain" OnItemCreated="grid_ItemCreated" OnItemDataBound="grid_ItemDataBound" OnItemCommand="grid_ItemCommand" AllowPaging="True"
                                         PageSize='<%# Settings.DefaultPageSize %>'>
                            <ClientSettings AllowDragToGroup="True">
                                <Scrolling AllowScroll="True" UseStaticHeaders="True" FrozenColumnsCount="2" />
                            </ClientSettings>
                            <SortingSettings EnableSkinSortStyles="false" />
                            <GroupingSettings CaseSensitive="False" ShowUnGroupButton="true" />
                            <MasterTableView AutoGenerateColumns="False" DataKeyNames="ID" CommandItemDisplay="Top" AllowMultiColumnSorting="true">
                                <CommandItemSettings ShowAddNewRecordButton="true" AddNewRecordText="Add New Carrier Rule" ShowExportToExcelButton="false" ShowRefreshButton="false" />
                                <RowIndicatorColumn Visible="True">
                                    <HeaderStyle Width="20px"></HeaderStyle>
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True">
                                    <HeaderStyle Width="20px" />
                                </ExpandCollapseColumn>
                                <SortExpressions>
                                    <telerik:GridSortExpression FieldName="EffectiveDate" SortOrder="Ascending" />
                                    <telerik:GridSortExpression FieldName="Carrier" SortOrder="Ascending" />
                                </SortExpressions>
                                <Columns>
                                    <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Edit" Text="Edit" UniqueName="EditColumn" ImageUrl="~/images/edit.png">
                                        <HeaderStyle Width="40px" />
                                    </telerik:GridButtonColumn>
                                    <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Delete" Text="Delete" UniqueName="DeleteColumn" ImageUrl="~/images/delete.png"
                                                              ConfirmText="Delete permanently?">
                                        <HeaderStyle Width="40px" />
                                    </telerik:GridButtonColumn>
                                    <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="AddNew" Text="Add New" UniqueName="AddNewColumn" ImageUrl="~/images/add.png">
                                        <HeaderStyle Width="25px" />
                                    </telerik:GridButtonColumn>

                                    <telerik:GridBoundColumn DataField="ID" UniqueName="ID" HeaderText="ID" ReadOnly="true" Display="false" ForceExtractValue="Always" />

                                    <telerik:GridTemplateColumn UniqueName="TypeID" DataField="Type" HeaderText="Type" SortExpression="Type" GroupByExpression="Type"
                                                                FilterControlWidth="70%" HeaderStyle-Width="300px" EditFormColumnIndex="0" ItemStyle-Width="250px"
                                                                FilterControlAltText="LightGreen|White">
                                        <ItemTemplate>
                                            <asp:Label ID="lblType" runat="server" Text='<%# Eval("Type") %>' />
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <telerik:RadComboBox ID="rcbTypeID" runat="server" Width="250px" CausesValidation="false"
                                                                 DataSourceID="dsType" DataValueField="ID" DataTextField="Name"
                                                                 AutoPostBack="true" OnSelectedIndexChanged="rcbType_SelectedIndexChanged" />
                                        </EditItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridDropDownColumn UniqueName="xlsTypeID" DataField="TypeID" HeaderText="Type" SortExpression="Type"
                                                                FilterControlWidth="70%" HeaderStyle-Width="250px" EditFormColumnIndex="0" ItemStyle-Width="250px"
                                                                DataSourceID="dsType" ListValueField="ID" ListTextField="Name" ReadOnly="true"
                                                                FilterControlAltText="LightGreen|White" />

                                    <telerik:GridBoundColumn UniqueName="RuleType" DataField="RuleType" HeaderText="Rule Type"
                                                             SortExpression="RuleType" FilterControlWidth="70%"
                                                             HeaderStyle-Width="100px" EditFormColumnIndex="1"
                                                             ReadOnly="true" ForceExtractValue="Always" />
                                    <telerik:GridTemplateColumn DataField="Value" UniqueName="Value" HeaderText="Value" SortExpression="Value"
                                                                FilterControlAltText="LightGreen"
                                                                HeaderStyle-Width="100px" EditFormColumnIndex="1">
                                        <ItemTemplate>
                                            <asp:Label ID="lblValue" runat="server" Text='<%# Eval("Value") %>' />
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <telerik:RadComboBox ID="rcbValue" runat="server" Width="200px"
                                                                 AllowCustomText="true" DataValueField="Value" DataTextField="Value" />
                                            <asp:RequiredFieldValidator ID="rfvValue" runat="server" ControlToValidate="rcbValue"
                                                                        Text="!" ErrorMessage="Value is required" CssClass="NullValidator" InitialValue="" />
                                        </EditItemTemplate>
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridDateTimeColumn DataField="EffectiveDate" HeaderText="Effective Date" DataType="System.DateTime"
                                                                UniqueName="EffectiveDate" FilterControlWidth="70%" EditFormColumnIndex="2" ForceExtractValue="Always"
                                                                FilterControlAltText="LightGreen|White" HeaderStyle-Width="100px" DataFormatString="{0:M/d/yyyy}">
                                        <ColumnValidationSettings EnableRequiredFieldValidation="true">
                                            <RequiredFieldValidator ErrorMessage="Effective Date value is required" CssClass="NullValidator" Text="!" />
                                        </ColumnValidationSettings>
                                    </telerik:GridDateTimeColumn>
                                    <telerik:GridDateTimeColumn DataField="EndDate" HeaderText="End Date" DataType="System.DateTime"
                                                                UniqueName="EndDate" FilterControlWidth="70%" EditFormColumnIndex="2" ForceExtractValue="Always"
                                                                FilterControlAltText="LightGreen|White" HeaderStyle-Width="95px" DataFormatString="{0:M/d/yyyy}">
                                        <ColumnValidationSettings EnableRequiredFieldValidation="true">
                                            <RequiredFieldValidator ErrorMessage="End Date value is required" CssClass="NullValidator" Text="!" />
                                        </ColumnValidationSettings>
                                    </telerik:GridDateTimeColumn>
                                                                        
                                    <telerik:GridDropDownColumn UniqueName="DriverID" DataField="DriverID" HeaderText="Driver" SortExpression="Driver"
                                                                FilterControlWidth="70%" HeaderStyle-Width="150px" EditFormColumnIndex="0" ItemStyle-Width="250px"
                                                                DataSourceID="dsDriver" ListValueField="ID" ListTextField="Name"
                                                                FilterControlAltText="LightGreen|White" />
                                    <telerik:GridDropDownColumn UniqueName="CarrierID" DataField="CarrierID" HeaderText="Carrier" SortExpression="Carrier"
                                                                FilterControlWidth="70%" HeaderStyle-Width="250px" EditFormColumnIndex="0" ItemStyle-Width="250px"
                                                                DataSourceID="dsCarrier" ListValueField="ID" ListTextField="Name"
                                                                FilterControlAltText="LightGreen|White" />
                                    <telerik:GridDropDownColumn UniqueName="TerminalID" DataField="TerminalID" HeaderText="Terminal" SortExpression="Terminal"
                                                                FilterControlWidth="70%" HeaderStyle-Width="150px" EditFormColumnIndex="0" ItemStyle-Width="250px"
                                                                DataSourceID="dsTerminal" ListValueField="ID" ListTextField="Name"
                                                                FilterControlAltText="LightGreen|White" />
                                    <telerik:GridDropDownColumn UniqueName="StateID" DataField="StateID" HeaderText="Driver Operating State" SortExpression="State"
                                                                FilterControlWidth="70%" HeaderStyle-Width="150px" EditFormColumnIndex="0" ItemStyle-Width="250px"
                                                                DataSourceID="dsState" ListValueField="ID" ListTextField="Name"
                                                                FilterControlAltText="LightGreen|White" />
                                    <telerik:GridDropDownColumn UniqueName="RegionID" DataField="RegionID" HeaderText="Region" SortExpression="Region"
                                                                FilterControlWidth="70%" HeaderStyle-Width="150px" EditFormColumnIndex="0" ItemStyle-Width="250px"
                                                                DataSourceID="dsRegion" ListValueField="ID" ListTextField="Name"
                                                                FilterControlAltText="LightGreen|White" />                                    

                                    <telerik:GridTemplateColumn UniqueName="Validation" HeaderStyle-Width="0px" Display="false" EditFormColumnIndex="3" EditFormHeaderTextFormat="">
                                        <EditItemTemplate>
                                            <asp:CustomValidator ID="cvGridError" runat="server" Display="None" ErrorMessage="Generic Message" />
                                            <asp:ValidationSummary ID="validationSummaryMain" runat="server" DisplayMode="BulletList" Enabled="true" EnableClientScript="true"
                                                                   CssClass="NullValidator" />
                                        </EditItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="CreateDate" UniqueName="CreateDate" SortExpression="CreateDate" DataType="System.DateTime" DataFormatString="{0:M/d/yy}"
                                                             ForceExtractValue="Always" ReadOnly="true"
                                                             HeaderText="Create Date" HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                    <telerik:GridBoundColumn DataField="CreatedByUser" UniqueName="CreatedByUser" SortExpression="CreatedByUser"
                                                             ForceExtractValue="Always" ReadOnly="true"
                                                             HeaderText="Created By" HeaderStyle-Width="90px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                    <telerik:GridBoundColumn DataField="LastChangeDate" UniqueName="LastChangeDate" SortExpression="LastChangeDate" DataType="System.DateTime" DataFormatString="{0:M/d/yy HH:mm}"
                                                             ForceExtractValue="Always" ReadOnly="true"
                                                             HeaderText="Last Update Date" HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />
                                    <telerik:GridBoundColumn DataField="LastChangedByUser" UniqueName="LastChangedByUser" SortExpression="LastChangedByUser"
                                                             ForceExtractValue="Always" ReadOnly="true"
                                                             HeaderText="Last Updated By" HeaderStyle-Width="95px" ItemStyle-HorizontalAlign="Center" FilterControlWidth="70%" />

                                    <telerik:GridDropDownColumn DataField="ImportAction" UniqueName="ImportAction" HeaderText="Import Action"
                                                                ReadOnly="true" Display="false" ForceExtractValue="Always" ItemStyle-BackColor="Olive"
                                                                FilterControlAltText="LightGreen"
                                                                DataSourceID="dsImportAction" ListTextField="Name" ListValueField="Name" DefaultInsertValue="Add">
                                    </telerik:GridDropDownColumn>
                                    <%--
                                    <telerik:GridBoundColumn DataField="ImportOutcome" UniqueName="ImportOutcome" HeaderText="Import Outcome"
                                                             ReadOnly="true" Visible="false" ForceExtractValue="Always" ItemStyle-BackColor="Olive" />
                                    --%>
                                </Columns>
                                <EditFormSettings ColumnNumber="4">
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column" ButtonType="ImageButton" CancelImageUrl="~/images/cancel.png"
                                                UpdateImageUrl="~/images/apply.png" InsertImageUrl="~/images/apply.png" />
                                </EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <HeaderStyle Wrap="False" />
                            <PagerStyle AlwaysVisible="True" />
                            <FilterMenu EnableImageSprites="False" />
                        </telerik:RadGrid>
                        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
                            <script type="text/javascript">
                                function validateEndDate(source, args) {
                                    debugger;
                                    args.isValid = true;
                                    endDate = moment(args.Value);
                                    if (endDate.isValid()) {
                                        var earliest = moment($(".EffectiveDateRDP").length == 1
                                            ? getDatePickerByClass("EffectiveDateRDP").get_focusedDate()
                                            : $(".EarliestEndDate").val());
                                        if (earliest.isValid() && endDate.diff(earliest) < 0) {
                                            args.isValid = false;
                                            source.errormessage = "End Date must be after the Effective Date (or empty)";
                                        }
                                    }
                                };
                                /*
                                $(function () {
                                    $("#excelUpload").change(function () {
                                        var disabled = $("#excelUpload").val().length == 0;
                                        $("#cmdImport").prop("disabled", disabled);
                                    });
                                    $("#chkReplaceMatching").click(function () {
                                        debugger;
                                        if ($("#chkReplaceMatching").is(':checked')) {
                                            $("#chkReplaceOverlapping").prop("disabled", false);
                                        }
                                        else {
                                            $("#chkReplaceOverlapping").attr("checked", false).prop("disabled", true);
                                        }
                                    });
                                });
                                */
                            </script>
                        </telerik:RadScriptBlock>
                    </div>
                </div>
                <blc:RadGridDBCtrl ID="rgdMain" runat="server"
                                   ControlID="rgMain"
                                   FilterActiveEntities="False" />
                <blac:DBDataSource ID="dsMain" runat="server" SelectIDNullsToZero="true"
                                   SelectCommand="SELECT *, ImportAction='None', ImportOutcome = '' FROM fnCarrierRulesDisplay(isnull(@StartDate, getdate()), coalesce(@EndDate, @StartDate, getdate()), @TypeID, @DriverID, @CarrierID, @TerminalID, @StateID, @RegionID) ORDER BY Carrier, EffectiveDate DESC">
                    <SelectParameters>
                        <asp:ControlParameter Name="TypeID" ControlID="ddType" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
                        <asp:ControlParameter Name="DriverID" ControlID="ddDriver" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
                        <asp:ControlParameter Name="CarrierID" ControlID="ddCarrier" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
                        <asp:ControlParameter Name="TerminalID" ControlID="ddTerminal" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
                        <asp:ControlParameter Name="StateID" ControlID="ddState" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />
                        <asp:ControlParameter Name="RegionID" ControlID="ddRegion" PropertyName="SelectedValue" Type="Int32" DefaultValue="0" />                        
                        <asp:ControlParameter Name="StartDate" ControlID="rdpStartDate" PropertyName="DbSelectedDate" Type="String" DefaultValue="NULL" />
                        <asp:ControlParameter Name="EndDate" ControlID="rdpEndDate" PropertyName="DbSelectedDate" Type="String" DefaultValue="NULL" />
                    </SelectParameters>
                </blac:DBDataSource>
                <blc:RadGridDBCtrl ID="dbcMain" runat="server"
                                   ControlID="rgMain"
                                   UpdateTableName="tblCarrierRule"
                                   FilterActiveEntities="False" />
                <blac:DBDataSource ID="dsTypeAll" runat="server" SelectCommand="SELECT ID, Name FROM tblCarrierRuleType UNION SELECT 0, '(All)' ORDER BY Name" />
                <blac:DBDataSource ID="dsType" runat="server" SelectCommand="SELECT ID, Name FROM tblCarrierRuleType ORDER BY Name" />
                <blac:DBDataSource ID="dsDriver" runat="server" SelectCommand="SELECT ID, Name = FullName FROM viewDriver WHERE Active = 1 UNION SELECT 0, '(All)' ORDER BY Name" />
                <blac:DBDataSource ID="dsCarrier" runat="server" SelectCommand="SELECT ID, Name = dbo.fnNameWithDeleted(Name, DeleteDateUTC), Active = dbo.fnIsActive(DeleteDateUTC) FROM dbo.tblCarrier UNION SELECT 0, '(All)', 1 ORDER BY Active DESC, Name" />                
                <blac:DBDataSource ID="dsTerminal" runat="server" SelectCommand="SELECT ID, Name FROM tblTerminal WHERE DeleteDateUTC IS NULL UNION SELECT 0, '(All)' ORDER BY Name" />
                <blac:DBDataSource ID="dsState" runat="server" SelectCommand="SELECT ID, Name = FullName, Abbreviation FROM tblState UNION SELECT 0, '(All)', NULL ORDER BY FullName" />
                <blac:DBDataSource ID="dsRegion" runat="server" SelectCommand="SELECT ID, Name FROM tblRegion UNION SELECT 0, '(All)' ORDER BY Name" />                
                <blac:DBDataSource ID="dsImportAction" runat="server" SelectCommand="SELECT Name = 'None' UNION SELECT 'Add' UNION SELECT 'Update' UNION SELECT 'Delete'" />
            </div>
        </div>
    </div>
</asp:Content>
