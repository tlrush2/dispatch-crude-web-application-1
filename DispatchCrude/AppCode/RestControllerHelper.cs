﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using Newtonsoft.Json;
using AlonsIT;

namespace DispatchCrude.App_Code
{
    public class RestControllerHelper
    {
        static public string GetJson(string objectName, string fieldsCSV = "ID,Name"
            , string where = null, string orderBy = null
            , bool includeDeleted = false, string deletedSuffix = "Deleted: ", string deletedNameField = "Name"
            , string allID = null, string[] allTexts = null)
        {
            DataTable data = GetData(objectName, fieldsCSV, where, orderBy, includeDeleted, deletedSuffix, deletedNameField, allID, allTexts);
            return JsonConvert.SerializeObject(data);
        }

        static public string GetDDLJson(string objectName, string valueField = "ID", string textField = "Name", string orderBy = null, string allID = null, string[] allTexts = null)
        {
            string fieldsCSV = string.Format("[value]=[{0}],[text]=[{1}]", valueField, textField);
            DataTable data = GetData(objectName, fieldsCSV, null, orderBy, allID: allID, allTexts: allTexts);
            return JsonConvert.SerializeObject(data);
        }

        static public DataTable GetData(string objectName, string fieldsCSV = "ID,Name", string where = null
            , string orderBy = null
            , bool includeDeleted = false, string deletedSuffix = "Deleted: ", string deletedNameField = "Name"
            , string allID = null, string[] allTexts = null)
        {
            objectName = GetSqlObjectName(objectName);

            if ((fieldsCSV ?? "").Length == 0 || fieldsCSV.ToLower() == "all")
                fieldsCSV = @"*";

            where = (where ?? "").Trim();
            if (!includeDeleted && !where.ToLower().Contains("deletedate") && HasDeleteDateUTC(objectName))
                where = "DeleteDateUTC IS NULL" + (where.Length > 0 ? string.Format(" AND ({0})", where) : "");
            if (where.Length > 0 && !where.StartsWith("WHERE", StringComparison.CurrentCultureIgnoreCase))
                where = "WHERE " + where;

            orderBy = (orderBy ?? "").Trim();
            if (orderBy.Length > 0 && !orderBy.StartsWith("ORDER BY", StringComparison.CurrentCultureIgnoreCase))
                orderBy = "ORDER BY " + orderBy;

            string allSql = "";
            if (allID != null && allTexts != null)
            {
                List<string> texts = new List<string>(allTexts.Length);
                foreach (string text in allTexts)
                    texts.Add(DBHelper.QuoteStr(text));

                allSql = string.Format("UNION SELECT {0}, {1}", DBHelper.QuoteStr(allID), string.Join(",", texts));
            }

            string sql = string.Format("SELECT {0} FROM {1} {2} {4} {3}"
                , fieldsCSV
                , objectName
                , where
                , orderBy
                , allSql);

            using (SSDB db = new SSDB())
            {
                return db.GetPopulatedDataTable(sql);
            }
        }

        static private string GetSqlObjectName(string objectName)
        {
            try
            {
                int x = 0;
                if (Int32.TryParse(objectName, out x))
                {
                    using (SSDB db = new SSDB())
                    {
                        return db.QuerySingleValue("SELECT SqlSourceName FROM tblObject WHERE ID = {0}", objectName).ToString();
                    }
                }
            }
            catch (Exception)
            {
                // eat these exceptions, just continue below
            }
            if (!objectName.StartsWith("tbl", StringComparison.CurrentCultureIgnoreCase)
                && !objectName.StartsWith("view", StringComparison.CurrentCultureIgnoreCase)
                && !objectName.StartsWith("fn", StringComparison.CurrentCultureIgnoreCase))
                objectName = "tbl" + objectName;
            return objectName;
        }

        static private bool HasDeleteDateUTC(string objectName)
        {
            try
            {
                using (SSDB db = new SSDB())
                {
                    return !DBHelper.IsNull(
                        db.QuerySingleValue("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.Columns WHERE TABLE_NAME LIKE {0} AND COLUMN_NAME LIKE 'DeleteDateUTC'"
                        , DBHelper.QuoteStr(objectName)));
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}