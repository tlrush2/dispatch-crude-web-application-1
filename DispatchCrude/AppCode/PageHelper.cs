﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace DispatchCrude.App_Code
{
    public class PageHelper
    {
        public static string BOOTSTRAP_SUCCESS = "#53a93f";
        public static string BOOTSTRAP_WARNING = "#f4b400";
        public static string BOOTSTRAP_DANGER = "#d73d32"; // Maverick red?


        public static string SUCCESS = "success";
        public static string WARNING = "warning";
        public static string DANGER = "danger";

        public static string getStatusColor(string status)
        {
            if (status == DANGER) return BOOTSTRAP_DANGER;
            if (status == WARNING) return BOOTSTRAP_WARNING;
            if (status == SUCCESS) return BOOTSTRAP_SUCCESS;
            return "#000000";
        }

        public static string getStatus(double value, double error, double warning)
        {
            if (value >= error)
                return DANGER;
            else if (value >= warning)
                return WARNING;
            else
                return SUCCESS;
        }


        static public Control FindControlRecursive(Control parent, String id)
        {
            Control ctrl = parent.FindControl(id);
            if (ctrl != null)
            {
                return ctrl;
            }
            else if (parent.Controls.Count > 0)
            {
                foreach (Control child in parent.Controls)
                {
                    return FindControlRecursive(child, id);
                }
            }
            return null;
        }
    }
}