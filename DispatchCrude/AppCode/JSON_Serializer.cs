﻿using System;
using Newtonsoft.Json;
using System.IO;
using System.Data;

namespace DispatchCrude.App_Code
{
    public class JSON_Serializer
    {
        public JSON_Serializer(bool formatJsonOutput = true, string dataSetText = "")
        {
            DataSetText = dataSetText;
            FormatJsonOutput = formatJsonOutput;
        }

        public bool FormatJsonOutput { get; set; }
        private string DataSetText { get; set; }

        public string Serialize(object[] values)
        {
            string ret = "";
            foreach (object value in values)
            {
                ret += Serialize(value);
            }
            return ret;
        }

        public string Serialize(object value)
        {
            Type type = value.GetType();

            Newtonsoft.Json.JsonSerializer json = new Newtonsoft.Json.JsonSerializer();

            json.NullValueHandling = NullValueHandling.Ignore;

            json.ObjectCreationHandling = Newtonsoft.Json.ObjectCreationHandling.Replace;
            json.MissingMemberHandling = Newtonsoft.Json.MissingMemberHandling.Ignore;
            json.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;

            if (type == typeof(DataRow))
                json.Converters.Add(new DataRowConverter());
            else if (type == typeof(DataTable))
                json.Converters.Add(new DataTableConverter());
            else if (value as DataSet != null)
                json.Converters.Add(new DataSetConverter(DataSetText));

            StringWriter sw = new StringWriter();
            Newtonsoft.Json.JsonTextWriter writer = new JsonTextWriter(sw);
            if (this.FormatJsonOutput)
                writer.Formatting = Formatting.Indented;
            else
                writer.Formatting = Formatting.None;

            writer.QuoteChar = '"';
            json.Serialize(writer, value);

            string output = sw.ToString();
            writer.Close();
            sw.Close();

            return output;
        }

        public object Deserialize(string jsonText, Type valueType)
        {
            Newtonsoft.Json.JsonSerializer json = new Newtonsoft.Json.JsonSerializer();

            json.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
            json.ObjectCreationHandling = Newtonsoft.Json.ObjectCreationHandling.Replace;
            json.MissingMemberHandling = Newtonsoft.Json.MissingMemberHandling.Ignore;
            json.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;

            StringReader sr = new StringReader(jsonText);
            Newtonsoft.Json.JsonTextReader reader = new JsonTextReader(sr);
            object result = json.Deserialize(reader, valueType);
            reader.Close();

            return result;
        }

        /// <summary>
        /// Converts a <see cref="DataRow"/> object to and from JSON.
        /// </summary>
        public class DataRowConverter : JsonConverter
        {
            /// <summary>
            /// Writes the JSON representation of the object.
            /// </summary>
            /// <param name="writer">The <see cref="JsonWriter"/> to write to.</param>
            /// <param name="value">The value.</param>
            public override void WriteJson(JsonWriter writer, object dataRow, JsonSerializer ser)
            {
                DataRow row = dataRow as DataRow;

                writer.WriteStartObject();
                foreach (DataColumn column in row.Table.Columns)
                {
                    writer.WritePropertyName(column.ColumnName);
                    ser.Serialize(writer, row[column]);
                }
                writer.WriteEndObject();
            }

            /// <summary>
            /// Determines whether this instance can convert the specified value type.
            /// </summary>
            /// <param name="valueType">Type of the value.</param>
            /// <returns>
            ///     <c>true</c> if this instance can convert the specified value type; otherwise, <c>false</c>.
            /// </returns>
            public override bool CanConvert(Type valueType)
            {
                return typeof(DataRow).IsAssignableFrom(valueType);
            }

            /// <summary>
            /// Reads the JSON representation of the object.
            /// </summary>
            /// <param name="reader">The <see cref="JsonReader"/> to read from.</param>
            /// <param name="objectType">Type of the object.</param>
            /// <returns>The object value.</returns>
            public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
            {
                throw new NotImplementedException();
            }
        }


        /// <summary>
        /// Converts a DataTable to JSON. Note no support for deserialization
        /// </summary>
        public class DataTableConverter : JsonConverter
        {
            /// <summary>
            /// Writes the JSON representation of the object.
            /// </summary>
            /// <param name="writer">The <see cref="JsonWriter"/> to write to.</param>
            /// <param name="value">The value.</param>
            public override void WriteJson(JsonWriter writer, object dataTable, JsonSerializer ser)
            {
                DataTable table = dataTable as DataTable;
                DataRowConverter converter = new DataRowConverter();

                bool useTableName = !string.IsNullOrWhiteSpace(table.TableName);
                if (useTableName)
                {
                    writer.WriteStartObject();
                    writer.WritePropertyName(table.TableName);
                }
                writer.WriteStartArray();

                foreach (DataRow row in table.Rows)
                {
                    converter.WriteJson(writer, row, ser);
                }
                writer.WriteEndArray();

                if (useTableName)
                    writer.WriteEndObject();
            }

            /// <summary>
            /// Determines whether this instance can convert the specified value type.
            /// </summary>
            /// <param name="valueType">Type of the value.</param>
            /// <returns>
            ///     <c>true</c> if this instance can convert the specified value type; otherwise, <c>false</c>.
            /// </returns>
            public override bool CanConvert(Type valueType)
            {
                return typeof(DataTable).IsAssignableFrom(valueType);
            }

            /// <summary>
            /// Reads the JSON representation of the object.
            /// </summary>
            /// <param name="reader">The <see cref="JsonReader"/> to read from.</param>
            /// <param name="objectType">Type of the object.</param>
            /// <returns>The object value.</returns>
            public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Converts a <see cref="DataSet"/> object to JSON. No support for reading.
        /// </summary>
        public class DataSetConverter : JsonConverter
        {
            private string DataSetText { get; set; }

            public DataSetConverter(string dataSetText = "")
            {
                DataSetText = dataSetText;
            }

            /// <summary>
            /// Writes the JSON representation of the object.
            /// </summary>
            /// <param name="writer">The <see cref="JsonWriter"/> to write to.</param>
            /// <param name="value">The value.</param>
            public override void WriteJson(JsonWriter writer, object dataset, JsonSerializer ser)
            {
                DataSet dataSet = dataset as DataSet;

                DataTableConverter converter = new DataTableConverter();


                if (DataSetText.Length > 0)
                {
                    writer.WriteStartObject();
                    writer.WritePropertyName(DataSetText);
                    writer.WriteStartArray();
                }

                foreach (DataTable table in dataSet.Tables)
                {
                    converter.WriteJson(writer, table, ser);
                }
                if (DataSetText.Length > 0)
                {
                    writer.WriteEndObject();
                    writer.WriteEndArray();
                }
            }

            /// <summary>
            /// Determines whether this instance can convert the specified value type.
            /// </summary>
            /// <param name="valueType">Type of the value.</param>
            /// <returns>
            ///     <c>true</c> if this instance can convert the specified value type; otherwise, <c>false</c>.
            /// </returns>
            public override bool CanConvert(Type valueType)
            {
                return typeof(DataSet).IsAssignableFrom(valueType);
            }

            /// <summary>
            /// Reads the JSON representation of the object.
            /// </summary>
            /// <param name="reader">The <see cref="JsonReader"/> to read from.</param>
            /// <param name="objectType">Type of the object.</param>
            /// <returns>The object value.</returns>
            public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
            {
                throw new NotImplementedException();
            }
        }
    }
}