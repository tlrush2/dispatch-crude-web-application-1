using System;
using System.Data;
using System.Web.Security;
using DispatchCrude.Extensions;

namespace DispatchCrude.App_Code
{

    /// <summary>
    /// Summary description for GetUsersDS
    /// </summary>
    public class GetUsersDS
    {
        public GetUsersDS()
        {
        }

        /* Here is the list of columns returned of the Membership.GetUsersDS() method
         * UserName, Email, PasswordQuestion, Comment, IsApproved
         * IsLockedOut, LastLockoutDate, CreationDate, LastLoginDate
         * LastActivityDate, LastPasswordChangedDate, IsOnline, ProviderName
         */

        public DataSet GetDS_ByRole(string role)
        {
            DataSet ds = GetDS();
            DataTable dt = ds.Tables["Users"];

            MembershipUserCollection muc = Membership.GetAllUsers();

            string[] usersInRole = null;
            if (role != null && role.Length > 0)
                usersInRole = Roles.GetUsersInRole(role);

            foreach (MembershipUser mu in muc)
            {
                if (role == null || role == "" || mu.UserName.IsIn(usersInRole))
                {
                    dt.Rows.Add(PopulateUserDR(dt.NewRow(), mu));
                }
            }

            return ds;
        }

        public DataSet GetDS_ByActive(bool active)
        {
            MembershipUserCollection muc = Membership.GetAllUsers();

            DataSet ds = GetDS();
            DataTable dt = ds.Tables["Users"];

            foreach (MembershipUser mu in muc)
            {
                if (active == mu.IsApproved)
                {
                    dt.Rows.Add(PopulateUserDR(dt.NewRow(), mu));
                }
            }
            return ds;
        }

        private DataSet GetDS()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable("Users");
            dt = ds.Tables.Add("Users");
            dt.Columns.Add("UserName", Type.GetType("System.String"));
            dt.Columns.Add("Email", Type.GetType("System.String"));
            dt.Columns.Add("PasswordQuestion", Type.GetType("System.String"));
            dt.Columns.Add("Comment", Type.GetType("System.String"));
            dt.Columns.Add("ProviderName", Type.GetType("System.String")); // who the hell cares about provider name? :P
            dt.Columns.Add("CreationDate", Type.GetType("System.DateTime"));
            dt.Columns.Add("LastLoginDate", Type.GetType("System.DateTime"));
            dt.Columns.Add("LastActivityDate", Type.GetType("System.DateTime"));
            dt.Columns.Add("LastPasswordChangedDate", Type.GetType("System.DateTime"));
            dt.Columns.Add("LastLockoutDate", Type.GetType("System.DateTime"));
            dt.Columns.Add("IsApproved", Type.GetType("System.Boolean"));
            dt.Columns.Add("IsLockedOut", Type.GetType("System.Boolean"));
            dt.Columns.Add("IsOnline", Type.GetType("System.Boolean"));
            return ds;
        }

        private DataRow PopulateUserDR(DataRow row, MembershipUser mu)
        {
            row["UserName"] = mu.UserName;
            row["Email"] = mu.Email;
            row["Comment"] = mu.Comment;
            row["CreationDate"] = mu.CreationDate;
            row["LastLoginDate"] = mu.LastLoginDate;
            row["LastActivityDate"] = mu.LastActivityDate;
            row["LastPasswordChangedDate"] = mu.LastPasswordChangedDate;
            row["LastLockoutDate"] = mu.LastLockoutDate;
            row["IsApproved"] = mu.IsApproved;
            row["IsLockedOut"] = mu.IsLockedOut;
            row["IsOnline"] = mu.IsOnline;
            return row;
        }
    }

}