﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data;
using System.Drawing;
using Syncfusion.XlsIO;
using DispatchCrude.Core;
using DispatchCrude.Extensions;
using AlonsIT;

namespace DispatchCrude.App_Code
{
    public delegate void CellValueChanged(GridDataItem gridRow, string colName, ref object value);
    public delegate void CellColorChanged(GridDataItem gridRow, string colName, ref Color color);

    public class RadGridExcelExporter: IDisposable
    {
        private Dictionary<string, KeyValuePair<string, string>> _dicDropDownTemplateColumnDS = new Dictionary<string, KeyValuePair<string, string>>();

        public bool NoHeaderAsTopRow { get; set; }
        public bool IgnoreMergedCells { get; set; }
        public bool OnlySelected { get; set; }
        public bool DropDownColumnDataValidationList { get; set; }
        public void AddDropDownColumnDS(string colUniqueName, string dsID, string fieldName = "Name") 
        {
            _dicDropDownTemplateColumnDS.Add(colUniqueName, new KeyValuePair<string, string>(dsID, fieldName)); 
        }
        public void RemoveDropDownColumnDS(string colUniqueName) { _dicDropDownTemplateColumnDS.Remove(colUniqueName); }
        public string[] HiddenColNamesToInclude { get; set; }
        public string[] VisibleColNamesToSkip { get; set; }
        public string[] ExtraColumnHeaders { get; set; }

        private ExcelEngine _excelEngine = null;
        private IWorkbook _excelWB = null;


        public event CellValueChanged OnCellValueChanged;
        private void RaiseCellValueChanged(GridDataItem gridRow, string colName, ref object value)
        {
            if (OnCellValueChanged != null)
                OnCellValueChanged(gridRow, colName, ref value);
        }

        public event CellColorChanged OnCellBackColorChanged;
        private void RaiseCellBackColorChanged(GridDataItem gridRow, string colName, ref Color backColor)
        {
            if (OnCellBackColorChanged != null)
                OnCellBackColorChanged(gridRow, colName, ref backColor);
        }

        public RadGridExcelExporter(bool noHeaderAsTopRow = false, bool onlySelected = false
            , string[] hiddenColNamesToInclude = null, string[] visibleColNamesToSkip = null, string[] extraColumnHeaders = null
            , bool ignoreMergedCells = false, bool dropDownColumnDataValidationList = false)
        {
            NoHeaderAsTopRow = noHeaderAsTopRow;
            OnlySelected = onlySelected;
            DropDownColumnDataValidationList = dropDownColumnDataValidationList;
            HiddenColNamesToInclude = hiddenColNamesToInclude;
            VisibleColNamesToSkip = visibleColNamesToSkip;
            ExtraColumnHeaders = extraColumnHeaders;
            IgnoreMergedCells = ignoreMergedCells;

            _excelEngine = new ExcelEngine();
            _excelWB = _excelEngine.Excel.Workbooks.Add(ExcelVersion.Excel2010);
            // remove any default worksheets
            for (int i = _excelWB.Worksheets.Count; i > 1; i--)
                _excelWB.Worksheets.Remove(i - 1);
            _excelWB.CalculationOptions.CalculationMode = ExcelCalculationMode.Automatic;
        }

        public static string[] ConvertIncludeToSkipColumns(GridTableView gridTable, string[] includeColumns)
        {
            HashSet<string> skipColumns = new HashSet<string>();
            foreach (GridColumn col in gridTable.Columns)
            {
                if (col.Visible && !includeColumns.Contains(col.UniqueName))
                    skipColumns.Add(col.UniqueName);
            }
            return skipColumns.ToArray();
        }

        public MemoryStream Stream
        {
            get
            {
                MemoryStream ret = new MemoryStream();
                _excelWB.SaveAs(ret, ExcelSaveType.SaveAsXLS);
                return ret;
            }
        }

        private bool _prevCalled = false;
        private string _lookupDataSheetName = null;
        private IWorksheet AddWorksheet(string sheetName = "", bool isLookupData = false)
        {
            IWorksheet ret = null;

            if (isLookupData)
            {
                if (_lookupDataSheetName != null)
                    ret = _excelWB.Worksheets[_lookupDataSheetName];
                else
                {
                    // recursive call to simply add the Lookup Data work (as the last Worksheet in the workbook)
                    ret = AddWorksheet(string.IsNullOrEmpty(sheetName) ? "LookupData" : sheetName);
                    _lookupDataSheetName = ret.Name;
                }
                return ret;
            }

            if (!_prevCalled)
            {
                ret = _excelWB.Worksheets[0];
                _prevCalled = true;
                ret.Name = (sheetName ?? "").Length == 0 ? "Sheet1" : sheetName;
            }
            else
                ret = _excelWB.Worksheets.Create(string.IsNullOrEmpty(sheetName) ? string.Format("Sheet{0}", _excelWB.Worksheets.Count + 1) : sheetName);

            // if this is a data sheet and a LookupData sheet exists, ensure the LookupData sheet remains the last worksheet
            if (_lookupDataSheetName != null)
                ret.Move(_excelWB.Worksheets.Count - 2);
            
            ret.EnableSheetCalculations();
            return ret;
        }

		public MemoryStream ExportSheet(GridTableView gridTable, string sheetName = "")
		{
            IWorksheet sheet = AddWorksheet(sheetName);

            int r = 1, i = 1;
            if (!NoHeaderAsTopRow)
            {
                i = 1;
                foreach (GridColumn col in gridTable.Columns)
                {
                    if (IncludeColumn(col))
                    {
                        WriteHeaderCell(sheet.Range[r, i++], col);
                    }
                }
                if (ExtraColumnHeaders != null)
                {
                    foreach (string colName in ExtraColumnHeaders)
                    {
                        WriteHeaderCell(sheet.Range[r, i++], colName);
                    }
                }
                r++;
            }

            foreach (GridItem row in gridTable.Items)
            {
                if (row is GridDataItem && (!OnlySelected || row.Selected))
                {
                    WriteGridRowToExcel(sheet, row as GridDataItem, r++, 1);
                }
            }
            if (DropDownColumnDataValidationList)
            {
                i = 0;
                foreach (GridColumn col in gridTable.Columns)
                {
                    if (IncludeColumn(col))
                    {
                        i++;
                        string dsID, ltf = null;
                        Controls.DBDataSource dsSql = null;
                        if (
                            (   // the normal behavior - a GridDropDown is defined and has a valid DataSourceID & ListTextField defined
                                (col is GridDropDownColumn 
                                && !string.IsNullOrEmpty(dsID = (col as GridDropDownColumn).DataSourceID) 
                                && !string.IsNullOrEmpty(ltf = (col as GridDropDownColumn).ListTextField))
                                // or a DropDownTemplateColumnDS was provided with a valid DataSourceID & FieldName value was provided
                            ||  (_dicDropDownTemplateColumnDS.ContainsKey(col.UniqueName)
                                && !string.IsNullOrEmpty(dsID = _dicDropDownTemplateColumnDS[col.UniqueName].Key)
                                && !string.IsNullOrEmpty(ltf = _dicDropDownTemplateColumnDS[col.UniqueName].Value))
                            )
                            && (dsSql = col.Owner.OwnerGrid.NamingContainer.FindControl(dsID) as Controls.DBDataSource) != null)
                        {
                            IWorksheet lookup = AddWorksheet(isLookupData: true);
                            string sourceRngName = "Lookup_" + col.HeaderText.Replace(" ", "_");
                            if (!_excelWB.Names.Contains(sourceRngName))
                            {
                                int si = lookup.UsedRange.Columns.Length + 1, sr = 1;
                                lookup.Range[sr++, si].Text = col.HeaderText;
                                DataView dv = dsSql.Select() as DataView;
                                foreach (DataRowView rv in dv)
                                {
                                    lookup.Range[sr++, si].Text = rv[ltf].ToString();
                                }
                                _excelWB.Names.Add(sourceRngName, lookup.Range[2, si, sr, si]);
                            }
                            IDataValidation validation = sheet.Range[2, i, 2000, i].DataValidation;
                            validation.IsEmptyCellAllowed = false;
                            validation.IsListInFormula = true;
                            validation.AllowType = ExcelDataType.User;
                            validation.FirstFormula = sourceRngName;
                        }
                    }
                }
            }

            // auto fit based on the first 50 used data rows
            if (sheet.UsedRange.Columns.Count() > 0)
                sheet.Range[1, 1, 50, sheet.UsedRange.Columns.Count()].AutofitColumns();
            sheet.Range[2, 1].FreezePanes();

            return this.Stream;
        }

        private void WriteHeaderCell(IRange rng, GridColumn col)
        {
            if (!string.IsNullOrEmpty(col.FilterControlAltText) && !col.FilterControlAltText.Contains(' '))
            {
                string colColorName = col.FilterControlAltText.Split(new char[] { '|' })[0];
                rng.EntireColumn.CellStyle.Color = Color.FromName(colColorName);
            }
            WriteHeaderCell(rng, col.HeaderText);
        }

        private void WriteHeaderCell(IRange rng, string headerText)
        {
            AssignRangeValue(rng, headerText);
            rng.CellStyle.Font.Bold = true;
            rng.CellStyle.ColorIndex = ExcelKnownColors.Grey_40_percent;
        }

        private bool IncludeColumn(GridColumn col)
        {
            return !(col is GridButtonColumn)
                && ((col.Display && (VisibleColNamesToSkip == null || !col.UniqueName.IsIn(VisibleColNamesToSkip)))
                    || ((!col.Visible || !col.Display) && HiddenColNamesToInclude != null && col.UniqueName.IsIn(HiddenColNamesToInclude)));

        }

        static public void AssignRangeValue(IRange rng, object value, Type dataType = null)
        {
            if (dataType == null) dataType = typeof(String);

            if (dataType == typeof(byte)
                || dataType == typeof(Int16)
                || dataType == typeof(Int32)
                || dataType == typeof(Int64)
                || dataType == typeof(UInt16)
                || dataType == typeof(UInt32)
                || dataType == typeof(UInt64)
                || dataType == typeof(ushort)
                || dataType == typeof(short)
                || dataType == typeof(decimal)
                || dataType == typeof(double)
                || dataType == typeof(float))
            {
                rng.Number = DBHelper.ToDoubleSafe(value ?? 0);
            }
            else if (dataType == typeof(DateTime) || dataType == typeof(DateTime?))
            {
                DateTime? date = Converter.ToNullableDateTime(value);
                if (date.HasValue && date != new DateTime())
                    rng.DateTime = date.Value;
            }
            else if (dataType == typeof(bool))
            {
                rng.Text = DBHelper.ToBoolean(value) ? "TRUE" : "FALSE";
            }
            else
            {
                rng.Text = (value ?? "").ToString();
            }
        }

        private void WriteGridRowToExcel(IWorksheet sheet, GridDataItem row, int excelRow, int excelCol)
		{
			int i = excelCol;
			foreach (GridColumn col in row.OwnerTableView.Columns)
			{
				IRange rng = sheet.Range[excelRow, i];

                Type dataType = col.DataType;

				if (IncludeColumn(col))
				{
                    object value = null;
                    bool wrapText = false;

                    if (col is GridCheckBoxColumn)
                    {
                        CheckBox chk = App_Code.RadGridHelper.GetControlByType((Control)row[col.UniqueName], typeof(CheckBox)) as CheckBox;
                        if (chk != null)
                            value = chk.Checked;
                    }
                    else if (col is GridHyperLinkColumn)
                    {
                        HyperLink hl = App_Code.RadGridHelper.GetControlByType((Control)row[col.UniqueName], typeof(HyperLink)) as HyperLink;
                        if (hl != null)
                            value = hl.Text;
                    }
                    else if (row[col.UniqueName].Text.Length > 0)
                        value = row[col.UniqueName].Text;
                    else
                        value = GetControlText(row, col.UniqueName);

                    if (value != null && value is String && value.ToString() == "&nbsp;")
                        value = "";
                    else if (col is GridDropDownColumn)
                    {
                        dataType = typeof(string);
                        value = value.ToString(); // for GridDropDownColumns, we just use the string value
                    }
                    else
                    {
                        try
                        {
                            switch (Type.GetTypeCode(col.DataType))
                            {
                                case TypeCode.Int32:
                                case TypeCode.Int16:
                                    value = Converter.ToInt32(value);
                                    break;
                                case TypeCode.Decimal:
                                    value = Converter.ToDecimal(value);
                                    break;
                                case TypeCode.Double:
                                    value = Converter.ToDouble(value);
                                    break;
                                case TypeCode.String:
                                    string val = (value ?? "").ToString();
                                    wrapText = val.Contains("<br/>") || val.Contains("<br />");
                                    if (wrapText)
                                        value = val.Replace("<br/>", "\r\n").Replace("<br />", "\r\n");
                                    break;
                            }
                        }
                        catch (System.FormatException)
                        {
                            // eat these errors (just use the value as it was obtained)
                        }
                    }
                    RaiseCellValueChanged(row, col.UniqueName, ref value);
                    if (value != null && value.ToString().Contains("&"))
                        value = System.Net.WebUtility.HtmlDecode(value.ToString());
                    
                    AssignRangeValue(rng, value, dataType);

                    if (wrapText)
                        rng.CellStyle.WrapText = true;
                    switch (row[col].VerticalAlign)
                    {
                        case VerticalAlign.Top:
                            rng.CellStyle.VerticalAlignment = ExcelVAlign.VAlignTop;
                            break;
                        case VerticalAlign.Middle:
                        case VerticalAlign.NotSet:
                            rng.CellStyle.VerticalAlignment = ExcelVAlign.VAlignCenter;
                            break;
                        case VerticalAlign.Bottom:
                            rng.CellStyle.VerticalAlignment = ExcelVAlign.VAlignBottom;
                            break;
                    }
                    switch (col.ItemStyle.HorizontalAlign)
                    {
                        case HorizontalAlign.NotSet:
                        case HorizontalAlign.Left:
                            break;
                        case HorizontalAlign.Center:
                            rng.CellStyle.HorizontalAlignment = ExcelHAlign.HAlignCenter;
                            break;
                        case HorizontalAlign.Right:
                            rng.CellStyle.HorizontalAlignment = ExcelHAlign.HAlignRight;
                            break;
                    }
                    if (row[col].Font.Bold)
                        rng.CellStyle.Font.Bold = true;
                    if (row[col].Font.Italic)
                        rng.CellStyle.Font.Italic = true;

                    Color backColor = Color.Transparent;
                    RaiseCellBackColorChanged(row, col.UniqueName, ref backColor);

                    if (backColor != Color.Transparent)
                        rng.CellStyle.Color = backColor;
                    else if (!string.IsNullOrEmpty(col.FilterControlAltText))
                    {
                        if (col.FilterControlAltText.Contains('|'))
                        {
                            string cellColorName = col.FilterControlAltText.Split(new char[] { '|' })[1];
                            if (!cellColorName.Equals("Transparent", StringComparison.CurrentCultureIgnoreCase))
                                rng.CellStyle.Color = Color.FromName(cellColorName);
                        }
                    }
                    else if (!col.ItemStyle.BackColor.IsEmpty)
                    {
                        rng.CellStyle.Color = col.ItemStyle.BackColor;
                    }

                    if (!IgnoreMergedCells && (row[col].RowSpan > 1 || row[col].ColumnSpan > 1))
                    {
                        int rs = row[col].RowSpan > 1 ? row[col].RowSpan - 1 : 0, cs = row[col].ColumnSpan > 1 ? row[col].ColumnSpan - 1 : 0;
                        try
                        {
                            sheet.Range[excelRow, i, excelRow + rs, i + cs].Merge();
                        }
                        catch (Exception e)
                        {
                            string msg = e.Message;
                            // eat these errors
                        }
                    }
					i++;
				}
			}
            // give the caller a chance to specify the ExtraColumn data values
            if (ExtraColumnHeaders != null)
            {
                foreach (string colName in ExtraColumnHeaders)
                {
                    object value = string.Empty;
                    RaiseCellValueChanged(row, colName, ref value);
                    AssignRangeValue(sheet.Range[excelRow, i++], value.ToString());
                }
            }
		}

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_excelWB != null) _excelWB.Close();
                if (_excelEngine != null) _excelEngine.Dispose();
            }
        }

        private string GetControlText(GridDataItem gdi, string colUniqueName)
        {
            string ret = null;
            object ctrl = null;
            if ((ctrl = App_Code.RadGridHelper.GetControlByType(gdi[colUniqueName], typeof(TextBox))) != null)
            {
                if ((ctrl as TextBox).Text.Length > 0)
                    ret = (ctrl as TextBox).Text;
            }
            else if ((ctrl = ctrl = App_Code.RadGridHelper.GetControlByType(gdi[colUniqueName], typeof(Label))) != null)
            {
                if ((ctrl as Label).Text.Length > 0)
                    ret = (ctrl as Label).Text;
            }
            else if ((ctrl = ctrl = App_Code.RadGridHelper.GetControlByType(gdi[colUniqueName], typeof(HyperLink))) != null)
            {
                if ((ctrl as HyperLink).Text.Length > 0)
                    ret = (ctrl as HyperLink).Text;
            }
            return ret;
        }

    }
}