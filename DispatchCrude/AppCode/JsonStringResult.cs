﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using DispatchCrude.Core;

namespace DispatchCrude.App_Code
{
    public class JsonStringResult : ContentResult
    {
        public JsonStringResult(string json)
        {
            Content = json;
            ContentType = "application/json";
        }
        public static JsonStringResult Create(string json)
        {
            return new JsonStringResult(json);
        }
        public static JsonStringResult Create(object obj, params string[] skipProperties)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.ContractResolver = new JsonPropListExcludeContractResolver(skipProperties);
            settings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            settings.DateFormatHandling = DateFormatHandling.IsoDateFormat;
            return Create(JsonConvert.SerializeObject(obj, settings));
        }
    }
}