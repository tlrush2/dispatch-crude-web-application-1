﻿using System.Web.UI;
using Telerik.Web.UI;


namespace DispatchCrude.App_Code
{
    static public class RadAjaxHelper
    {
        static public void AddAjaxSetting(Page page, Control ajaxifiedControl, Control updatedControl, bool useLoadingPanel = true)
        {
            RadAjaxLoadingPanel panel = useLoadingPanel && (page.Master is SiteMaster) ? (page.Master as SiteMaster).LoadingPanel : null;
            RadAjaxManager.GetCurrent(page).AjaxSettings.AddAjaxSetting(ajaxifiedControl, updatedControl, panel, UpdatePanelRenderMode.Inline);
        }

        static public void FocusControl(Page page, Control ctrl)
        {
            if (ctrl != null)
                if (RadAjaxManager.GetCurrent(page) != null && RadAjaxManager.GetCurrent(page).IsAjaxRequest)
                    RadAjaxManager.GetCurrent(page).FocusControl(ctrl);
                else
                    ctrl.Focus();
        }

    }
}