﻿using System;
using System.Web;
using System.Data;
using AlonsIT;

namespace DispatchCrude.App_Code.Controls
{
    public class PriorityLabel : System.Web.UI.WebControls.Label
    {
        public class ColorPair
        {
            public ColorPair(string foreColorName = "Black", string backColorName = "Transparent")
            {
                ForeColorName = foreColorName;
                BackColorName = backColorName;
            }
            public string ForeColorName;
            public string BackColorName;
            public System.Drawing.Color ForeColor { get { return System.Drawing.Color.FromName(ForeColorName); } }
            public System.Drawing.Color BackColor { get { return System.Drawing.Color.FromName(BackColorName); } }
        }
        
        static public DataTable GetPriorityDataTable()
        {
            if (HttpContext.Current.Cache["dtPriority"] == null)
            {
                using (SSDB ssdb = new SSDB())
                {
                    DataTable dtCache = ssdb.GetPopulatedDataTable("SELECT * FROM tblPriority");
                    dtCache.PrimaryKey = new DataColumn[] { dtCache.Columns["PriorityNum"] };
                    HttpContext.Current.Cache["dtPriority"] = dtCache;
                }
            }
            return HttpContext.Current.Cache["dtPriority"] as DataTable;
        }
        static public ColorPair GetPriorityColorPair(string priorityNum)
        {
            ColorPair ret = new ColorPair();
            System.Data.DataTable dtPriority = GetPriorityDataTable();

            if (priorityNum.Length > 0)
            {
                DataRow row = dtPriority.Rows.Find(priorityNum);
                if (row != null)
                {
                    ret.ForeColorName = row["ForeColor"].ToString();
                    ret.BackColorName = row["BackColor"].ToString();
                }
            }
            return ret;
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            ColorPair colors = GetPriorityColorPair(this.Text);
            this.BackColor = colors.BackColor;
            this.ForeColor = colors.ForeColor;
        }
    }

}