﻿using System;

namespace DispatchCrude.App_Code.Controls
{
    public class PriorityDropDownList : System.Web.UI.WebControls.DropDownList
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.DataValueField = "ID";
            this.DataTextField = "PriorityNum";
            this.DataSource = PriorityLabel.GetPriorityDataTable();
            this.DataBind();
        }

        protected override void OnDataBound(EventArgs e)
        {
            base.OnDataBound(e);
            foreach (System.Web.UI.WebControls.ListItem li in this.Items)
            {
                if (li.Text.Length > 0)
                {
                    PriorityLabel.ColorPair cp = PriorityLabel.GetPriorityColorPair(li.Text);
                    li.Attributes["style"] = string.Format("background-color:{0};color:{1}", cp.BackColorName, cp.ForeColorName);
                }
            }
        }

        private void SetColors()
        {
            PriorityLabel.ColorPair cp = PriorityLabel.GetPriorityColorPair(this.Text);
            this.BackColor = cp.BackColor;
            this.ForeColor = cp.ForeColor;
        }
    }
}