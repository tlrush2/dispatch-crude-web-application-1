﻿using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DispatchCrude.App_Code
{
    public class ValidatorHelper
    {
        // recursive function to disable/enable all validators in a Control
        static public void EnableValidators(Control parent, bool enabled, params string[] skipValidatorIDs)
        {

            foreach (Control ctrl in parent.Controls)
            {
                if (ctrl is BaseValidator)
                {
                    if (skipValidatorIDs.Length == 0 || !skipValidatorIDs.Contains((ctrl as BaseValidator).ID))
                        (ctrl as BaseValidator).Enabled = enabled;
                }
                else
                    EnableValidators(ctrl, enabled);
            }
        }

    }
}