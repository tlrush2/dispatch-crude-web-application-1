﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DispatchCrude.Models;
using DispatchCrude.Core;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;

namespace DispatchCrude.Controllers
{
    [Authorize(Roles = "viewTruckMaintenance")]
    public class TruckMaintenanceController : Controller
    {
        // This session variable is required for Asyncronous file uploads to work with new records.
        // The file is uploaded here temporarily until the user actually clicks the save button.
        private const string SESSION_DOC_FILENAME = "DOC_FILENAME"
            , SESSION_DOC_FILE = "DOC_FILE";

        private DispatchCrudeDB db = new DispatchCrudeDB();

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        // GET: 
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest request = null, int id = 0)
        {
            return Read(request, null, id);
        }

        private ContentResult Read([DataSourceRequest] DataSourceRequest request, ModelStateDictionary modelState, int id = 0)
        {
            // .ToList() is required due to filtering on "Active" field, which is NOT MAPPED
            var data = db.TruckMaintenance.Where(m => m.ID == id || id == 0)
                        .Include(m => m.Truck)
                        .Include(m => m.Truck.Carrier)
                        .Include(m => m.Truck.Carrier.CarrierType)
                        .Include(m => m.TruckTrailerMaintenanceType)
                        .ToList();

            var result = data.ToDataSourceResult(request, modelState);
            
            // exclude these large fields since they are not needed and can be quite large
            return App_Code.JsonStringResult.Create(result, "DocumentSrc", "Document");
        }

        [HttpPost]
        public ActionResult SaveDocument(IEnumerable<HttpPostedFileBase> document)
        {
            // The Name of the Upload component is "files"
            if (document != null)
            {
                foreach (var file in document)
                {
                    Session[SESSION_DOC_FILENAME] = Path.GetFileName(file.FileName);
                    Session[SESSION_DOC_FILE] = new BinaryReader(file.InputStream).ReadBytes(file.ContentLength);
                    // we only accept a single file so just break
                    break;
                }
            }
            // Return an empty string to signify success
            return Content("");
        }

        public ActionResult ClearDocument()
        {
            Session[SESSION_DOC_FILE] = null;
            Session[SESSION_DOC_FILENAME] = null;

            return Json(new { status="OK"}, JsonRequestBehavior.AllowGet);
        }

        private bool ValidateDocument(ModelStateDictionary modelState = null)
        {
            bool ret = Session[SESSION_DOC_FILENAME] != null && Session[SESSION_DOC_FILE] != null;
            if (!ret && modelState != null)
                modelState.AddModelError("Document", "Document must be provided");
            return ret;
        }


        // POST: Create 
        [HttpPost]
        [Authorize(Roles = "createTruckMaintenance")]
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, TruckMaintenance truckMaintenance)
        {
            // require a valid document for new records
            if (ModelState.IsValid)// && ValidateDocument(ModelState))
            {
                try
                {
                    // ensure all new records are marked "Active" (otherwise they will be created deleted)  
                    //truckMaintenance.Active = true;  //This page does not allow deactivation, it uses real delete.

                    // Save the new file into the database only if the user actually specified a file for this record
                    if (Session[SESSION_DOC_FILE] as byte[] != null)
                    {
                        truckMaintenance.Document = Session[SESSION_DOC_FILE] as byte[];
                        truckMaintenance.DocName = Session[SESSION_DOC_FILENAME].ToString();
                    }                    

                    // Save new record to the database
                    db.TruckMaintenance.Add(truckMaintenance);
                    db.SaveChanges(User.Identity.Name);  // Create dates & usernames will all be taken care of by this line.

                    //Clear document session variable after successful save.  Otherwise the next record added will automatically use this file if the user did not select one.
                    //This is behavior is undesireable because the user could forget to choose the correct file and end up with a file for another record by accident.
                    if (Session[SESSION_DOC_FILE] != null)
                    {
                        Session[SESSION_DOC_FILE] = null;
                        Session[SESSION_DOC_FILENAME] = null;
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(null, ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { truckMaintenance }.ToDataSourceResult(request, ModelState));
                }
            }

            return Read(request, ModelState, truckMaintenance.ID);
        }

        // POST: Update
        [HttpPost]
        [Authorize(Roles = "editTruckMaintenance")]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, TruckMaintenance truckMaintenance)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // Get the existing record
                    // This allows us to keep the existing file upload if one is not specified in an edit
                    TruckMaintenance existing = db.TruckMaintenance.Find(truckMaintenance.ID);

                    // Specify all fields that will/can be updated by user input on the popup
                    db.CopyEntityValues(existing, truckMaintenance, "DocName", "Document");

                    if (ValidateDocument())  // If a file was specified and it is not zero-length
                    {
                        existing.Document = Session[SESSION_DOC_FILE] as byte[];
                        existing.DocName = Session[SESSION_DOC_FILENAME].ToString();
                    }
                    // else - no new image was specified, keep the original image and update the rest of the fields

                    db.SaveChanges(User.Identity.Name);  // Create & Modified dates & usernames will be taken care of by this line.

                    //Clear document session variable after successful save.  Otherwise the next record added will automatically use this file if the user did not select one.
                    //This is behavior is undesireable because the user could forget to choose the correct file and end up with a file for another record by accident.
                    if (Session[SESSION_DOC_FILE] != null)
                    {
                        Session[SESSION_DOC_FILE] = null;
                        Session[SESSION_DOC_FILENAME] = null;
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(null, ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { truckMaintenance }.ToDataSourceResult(request, ModelState));
                }
            }

            return Read(request, ModelState, truckMaintenance.ID);
        }

        // Delete (actually delete)
        [Authorize(Roles = "deactivateTruckMaintenance")]
        public ActionResult Delete(TruckMaintenance truckMaintenance)
        {
            db.TruckMaintenance.Attach(truckMaintenance);
            db.TruckMaintenance.Remove(truckMaintenance);
            db.SaveChanges(User.Identity.Name);

            return null;
        }

        //TODO: is this necessary?
        // Download the document
        public ActionResult Download(int id)
        {
            TruckMaintenance truckMaintenance = db.TruckMaintenance.Find(id);
            return File(truckMaintenance.Document, "application/octet-stream", truckMaintenance.DocName);            
        }        
    }
}
