﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DispatchCrude.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;

namespace DispatchCrude.Controllers
{
    [Authorize(Roles = "viewDriverScheduling")]
    public class DriverSchedulingStatusController : _DBControllerRead
    {
        // GET: DriverScheduling/Index
        public ActionResult Index()
        {
            return View();
        }

        // DriverScheduling/Read
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        override protected ContentResult Read([DataSourceRequest] DataSourceRequest request, ModelStateDictionary modelState, int id)
        {
            var data = db.DriverScheduleStatuses.Where(s => s.ID == id || id == 0 || (id == -1 && !s.DeleteDateUTC.HasValue)).OrderBy(m => m.Name);
            return ToJsonResult(data, request, modelState);
        }

        // POST: Update Shippers
        [HttpPost]
        //[Authorize(Roles = "editDriverScheduleStatus")]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, DriverScheduleStatus status)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    db.AddOrUpdateSave(User.Identity.Name, status);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { status }.ToDataSourceResult(request, ModelState));
                }
            }

            return Read(request, ModelState, status.ID);
        }

        // Delete (Deactivate)
        [HttpPost]
        //[Authorize(Roles = "deactivateDriverScheduleStatus")]
        // NOTE: the _permanentDelete field is passed from the browser if SHIFT was depressed on Delete button click
        public ActionResult Delete([DataSourceRequest] DataSourceRequest request, DriverScheduleStatus model, bool _permanentDelete = false)
        {
            return DeleteInt(request, model, _permanentDelete);
        }
    }
}