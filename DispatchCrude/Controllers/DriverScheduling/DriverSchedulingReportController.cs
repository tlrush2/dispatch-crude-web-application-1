﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DispatchCrude.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using AlonsIT;

namespace DispatchCrude.Controllers
{
    [Authorize(Roles = "viewDriverScheduling")]
    public class DriverSchedulingReportController : _DBController
    {
        // GET: DriverSchedulingReport
        public ActionResult Index()
        {
            int carrierID = DBHelper.ToInt32(Profile.GetPropertyValue("CarrierID"));
            return Index(DateTime.Now.Date, DateTime.Now.Date.AddDays(6), -1, -1, carrierID, -1, -1);
        }

        [HttpPost]
        public ActionResult Index(DateTime startDate, DateTime endDate, int terminalID, int regionID, int carrierID, int driverGroupID, int driverID)
        {
            ViewBag.StartDate = startDate;
            ViewBag.EndDate = endDate;
            ViewBag.TerminalID = terminalID;
            ViewBag.RegionID = regionID;
            ViewBag.CarrierID = carrierID;
            ViewBag.DriverGroupID = driverGroupID;
            ViewBag.DriverID = driverID;
            return View(GetDataTable(startDate, endDate, regionID, carrierID, driverGroupID, driverID));
        }

        [HttpGet]
        public ActionResult Read([DataSourceRequest] DataSourceRequest request = null, DateTime? startDate = null, DateTime? endDate = null, int? terminalID = -1, int regionID = -1, int carrierID = -1, int driverGroupID = -1, int driverID = -1)
        {
            if (!startDate.HasValue) startDate = DateTime.Now.Date;
            if (!endDate.HasValue) endDate = startDate.Value.Date.AddDays(7);
            return Read(request, null, startDate.Value, endDate.Value, terminalID, regionID, carrierID, driverGroupID, driverID);
        }

        [HttpPost]
        public ActionResult Read([DataSourceRequest] DataSourceRequest request, ModelStateDictionary modelState, DateTime startDate, DateTime endDate, int? terminalID, int regionID, int carrierID, int driverGroupID, int driverID)
        {
            DataTable data = GetDataTable(startDate, endDate, terminalID, regionID, carrierID, driverGroupID, driverID);

            if (request != null && request.Aggregates.Any())
            {
                request.Aggregates.Each(agg =>
                {
                    agg.Aggregates.Each(a =>
                    {
                        a.MemberType = data.Columns[agg.Member].DataType;
                    });
                });
            }

            return App_Code.JsonStringResult.Create(data.ToDataSourceResult(request));
        }

        private DataTable GetDataTable(DateTime startDate, DateTime endDate, int? terminalID = -1, int regionID = -1, int carrierID = -1, int driverGroupID = -1, int driverID = -1)
        {
            DataTable rawData = null;
            using (SSDB db = new SSDB())
            {
                using (SqlCommand cmd = db.BuildCommand("spDriverScheduleReportPivotSource"))
                {
                    cmd.Parameters["@Start"].Value = startDate;
                    cmd.Parameters["@End"].Value = endDate;
                    cmd.Parameters["@TerminalID"].Value = terminalID;
                    cmd.Parameters["@RegionID"].Value = regionID;
                    cmd.Parameters["@CarrierID"].Value = carrierID;
                    cmd.Parameters["@DriverGroupID"].Value = driverGroupID;
                    cmd.Parameters["@DriverID"].Value = driverID;
                    rawData = SSDB.GetPopulatedDataTable(cmd);
                }
            }
            rawData.Columns.Add("HeadingSH");
            rawData.Columns.Add("HeadingDH");
            rawData.Columns.Add("HeadingOC");
            foreach (DataRow dr in rawData.Rows)
            {
                dr["HeadingSH"] = dr["Heading"].ToString().Replace("SD", "SH");
                dr["HeadingDH"] = dr["Heading"].ToString().Replace("SD", "DH");
                dr["HeadingOC"] = dr["Heading"].ToString().Replace("SD", "OC");
            }

            // return the pivoted data (Drivers on the y axis, Days on the x axis)
            DataTable ret = PivotHelper.Pivot(rawData.CreateDataReader()
                , new string[] { "DriverID", "Carrier", "Driver", "MobilePhone", "TotalOpenCount" }
                , new string[,] { { "Heading", "PivotStatus" }, { "HeadingSH", "PivotStartHours" }, { "HeadingDH", "PivotDurationHours" }, { "HeadingOC", "PivotOrderCount" } });
            return ret;
        }

        [HttpGet]
        public ContentResult Terminals()
        {
            return DependencyResolver.Current.GetService<RestController>().Get("Terminal", allID: "-1", allTexts: new string[] { "(All Terminals)" });
        }

        [HttpGet]
        public ContentResult Regions()
        {
            return DependencyResolver.Current.GetService<RestController>().Get("Region", allID: "-1", allTexts: new string[] { "(All Regions)" });
        }
        [HttpGet]
        public ContentResult Carriers()
        {
            string where = null, allID = null;
            if (DBHelper.ToInt32(Profile.GetPropertyValue("CarrierID")) != -1)
            {
                where = "ID = " + DBHelper.QuoteStr(Profile.GetPropertyValue("CarrierID").ToString());
            }
            else
            {
                allID = "-1";
            }

            return DependencyResolver.Current.GetService<RestController>().Get("Carrier", where: where, allID: allID, allTexts: allID == null ? null : new string[] { "(All Carriers)" });
        }
        [HttpGet]
        public ContentResult DriverGroups()
        {
            return DependencyResolver.Current.GetService<RestController>().Get("DriverGroup", allID: "-1", allTexts: new string[] { "(All Driver Groups)" });
        }

        [HttpGet]
        public ContentResult Drivers()
        {
            return DependencyResolver.Current.GetService<RestController>().Get("viewDriverBase", fieldsCSV: "ID,Name=FullName", allID: "-1", allTexts: new string[] { "(All Drivers)" });
        }

    }
}