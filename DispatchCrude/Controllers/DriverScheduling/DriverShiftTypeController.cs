﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using DispatchCrude.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;

namespace DispatchCrude.Controllers
{
    [Authorize(Roles = "viewDriverScheduling")]
    public class DriverShiftTypeController : _DBControllerRead
    {
        // GET: DriverScheduling/Index
        public ActionResult Index()
        {
            return View();
        }

        // DriverScheduling/Read
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        override protected ContentResult Read([DataSourceRequest] DataSourceRequest request, ModelStateDictionary modelState, int id)
        {
            // ensure the grandchild "Status" objects are always included
            var data = db.DriverShiftTypes.Where(st => st.ID == id || id == 0 || (id == -1 && !st.DeleteDateUTC.HasValue)).Include(st => st.Days.Select(d => d.Status));
            return ToJsonResult(data, request, modelState);
        }

        // Get/StartHours
        public ContentResult StartHours()
        {

            Dictionary<byte, string> values = new Dictionary<byte, string>(24);
            for (byte i = 1; i <= 24; i++)
            {
                values.Add(i, new DateTime(1, 1, 1, i % 24, 0, 0).ToString("h:mm tt"));
            }
            var ret = new
            {
                Data = values.Select(item => new
                {
                    ID = item.Key,
                    Name = item.Value
                })
            };
            return App_Code.JsonStringResult.Create(ret);
        }

        // POST: DriverShiftType Update 
        [HttpPost]
        //[Authorize(Roles = "editDriverShiftType")]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, DriverShiftType shiftType)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    foreach (DriverShiftTypeDay day in shiftType.Days)
                    {
                        if (day.DriverShiftTypeID != shiftType.ID) day.DriverShiftTypeID = shiftType.ID;
                        if (day.Status.Utilized && (!day.StartHours.HasValue || !day.DurationHours.HasValue))
                            throw new Exception("All 'Utilized' statuses require both Start & Duration values");
                    }
                    using (TransactionScope tx = new TransactionScope())
                    {
                        db.AddOrUpdate(shiftType, "Days");
                        var extraDays = db.Set<DriverShiftTypeDay>().Where(d => d.DriverShiftTypeID == shiftType.ID && d.Position > shiftType.DayCount);
                        db.Set<DriverShiftTypeDay>().RemoveRange(extraDays);
                        db.SaveChanges(User.Identity.Name);
                        tx.Complete();
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { shiftType }.ToDataSourceResult(request, ModelState));
                }
            }

            return Read(request, ModelState, shiftType.ID);
        }

        // Delete (Deactivate)
        [HttpPost]
        //[Authorize(Roles = "deactivateDriverShiftType")]
        public ActionResult Delete([DataSourceRequest] DataSourceRequest request, DriverShiftType shiftType, bool _permanentDelete = false)
        {
            return base.DeleteInt(request, shiftType, _permanentDelete);
        }
    }
}