﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Web.Mvc;
using DispatchCrude.Models;
using DispatchCrude.Core;

namespace DispatchCrude.Controllers
{
    public class SettingsController : Controller
    {
        private DispatchCrudeDB db = new DispatchCrudeDB();
        
        //Settings page
        public ActionResult Index()
        {
            var settings = db.Settings.OrderBy(c => c.Category).ToList();

            return View(settings);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(string[] SelectedBooleans, params string[] ExpandedCategories)
        {            
            int maxID = db.Settings.Max(s => s.ID);
            int intDummy;
            double doubleDummy;

            ViewBag.ExpandedCategories = ExpandedCategories;
                        
            for (int i = 0; i <= maxID; i++)
            {
                string updated = Request.Form[i.ToString()];
                Setting s = db.Settings.Find(i);

                //if a setting with the ID of 'i' exists, proceed to next check.  This is necessary since some setting id's have been deleted and this may happen more in the future.
                if (s != null && s.ReadOnly == false)
                {
                    //special check for the database version number setting. This allows update to happen even though the setting is read only.
                    if (s.ID == 0 && UserSupport.IsInGroup("_DCAdministrator") && !string.IsNullOrEmpty(updated) && s.Value != updated)
                    {
                        s.Value = updated;
                    }
                    //If it's a boolean field, special work is needed to set the value
                    else if (s.SettingTypeID == (byte)SettingType.TYPE.Bool)
                    {
                        updated = "FALSE";

                        foreach (string id in SelectedBooleans)
                        {
                            if (Convert.ToInt32(id) == s.ID)
                            {
                                updated = "TRUE";
                                break;
                            }
                        }

                        if (Convert.ToBoolean(s.Value) != Convert.ToBoolean(updated))
                            s.Value = updated;
                    }
                    else if (s.Value != updated) //if the setting value is not boolean and has changed, validate it
                    {
                        //Validate numeric fields (other types do not need validation at the moment)
                        switch (s.SettingTypeID)
                        {                            
                            case (byte)SettingType.TYPE.Int:
                                if(!s.ReadOnly && !int.TryParse(updated, out intDummy))
                                {
                                    ModelState.AddModelError(i.ToString(), "New value was not a valid integer");
                                }
                                break;
                            case (byte)SettingType.TYPE.Double:
                                if (!s.ReadOnly && !double.TryParse(updated, out doubleDummy))
                                {
                                    ModelState.AddModelError(i.ToString(), "New value was not a valid decimal");
                                }
                                break;
                        }                                               

                        //If numeric validation passes go ahead and save the new value as long as it's not a read only field (we dont want to save null on accident)
                        if (ModelState.IsValid && !s.ReadOnly)
                        {
                            s.Value = updated;
                        }
                    }
                }                
            }

            // if all validated fields are valid, go ahead and save all changes
            if (ModelState.IsValid)
            {
                db.SaveChanges(User.Identity.Name);  // Create & Modified dates & usernames will be taken care of by this line.
                TempData["success"] = "Settings updated successfully.";
            }
            else
            {
                TempData["fail"] = "Settings not updated.  Please check errors below.";
            }            

            var settings = db.Settings.OrderBy(c => c.Category).ToList();            

            return View("Index", settings);
        }
    }
}