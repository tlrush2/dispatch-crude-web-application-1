﻿using System;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using DispatchCrude.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;

namespace DispatchCrude.Controllers
{
    [Authorize(Roles = "viewTruckTrailerMaintenanceTypes")]
    public class TruckTrailerMaintenanceTypesController : Controller
    {
        private DispatchCrudeDB db = new DispatchCrudeDB();

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        // GET: 
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest request = null, int id = 0)
        {
            return Read(request, null, id);
        }

        private ContentResult Read([DataSourceRequest] DataSourceRequest request, ModelStateDictionary modelState, int id = 0)
        {
            // .ToList() is required due to filtering on "Active" field, which is NOT MAPPED
            var data = db.TruckTrailerMaintenanceTypes.Where(m => m.ID == id || id == 0).ToList();

            var result = data.ToDataSourceResult(request, modelState);

            return App_Code.JsonStringResult.Create(result);
        }

        // POST: Create 
        //[HttpPost]
        [Authorize(Roles = "createTruckTrailerMaintenanceTypes")]
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, TruckTrailerMaintenanceType truckTrailerMaintenanceType)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // ensure all new records are marked "Active" (otherwise they will be created deleted)
                    truckTrailerMaintenanceType.Active = true;

                    // Save new record to the database
                    db.TruckTrailerMaintenanceTypes.Add(truckTrailerMaintenanceType);
                    db.SaveChanges(User.Identity.Name);  // Create dates & usernames will all be taken care of by this line.
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(null, ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { truckTrailerMaintenanceType }.ToDataSourceResult(request, ModelState));
                }
            }


            return Read(request, ModelState, truckTrailerMaintenanceType.ID);
        }

        // POST: Update
        [HttpPost]
        [Authorize(Roles = "editTruckTrailerMaintenanceTypes")]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, TruckTrailerMaintenanceType truckTrailerMaintenanceType)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // Get the existing record
                    TruckTrailerMaintenanceType existing = db.TruckTrailerMaintenanceTypes.Find(truckTrailerMaintenanceType.ID);

                    // Specify all fields that will/can be updated by user input on the popup
                    db.CopyEntityValues(existing, truckTrailerMaintenanceType);

                    db.SaveChanges(User.Identity.Name);  // Create & Modified dates & usernames will be taken care of by this line.
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(null, ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { truckTrailerMaintenanceType }.ToDataSourceResult(request, ModelState));
                }
            }

            return Read(request, ModelState, truckTrailerMaintenanceType.ID);
        }


        // Delete (Deactivate)
        [Authorize(Roles = "deactivateTruckTrailerMaintenanceTypes")]
        public ActionResult Delete(TruckTrailerMaintenanceType truckTrailerMaintenanceType)
        {
            db.TruckTrailerMaintenanceTypes.Attach(truckTrailerMaintenanceType);
            db.TruckTrailerMaintenanceTypes.Remove(truckTrailerMaintenanceType);
            db.SaveChanges(User.Identity.Name);

            return null;
        }
    }
}
