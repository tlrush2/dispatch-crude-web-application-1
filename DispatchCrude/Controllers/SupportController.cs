﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Newtonsoft.Json;
using AlonsIT;
using DispatchCrude.Extensions;
using DispatchCrude.Models;

namespace DispatchCrude.Controllers
{
    public class SupportController : _DBController
    {
        //
        // GET: /Support/

        //[HttpPost]
        public ActionResult CustomerRouteRateEarliestEffectiveDate(int customerID = 0, int originID = 0, int destinationID = 0)
        {
            using (SSDB db = new SSDB())
            {
                string sql = "SELECT dateadd(day, 1, max(EffectiveDate)) "
                        + "FROM viewCustomerRouteRates CRR "
                        + "WHERE CustomerID = {0} AND OriginID = {1} AND DestinationID = {2}";
                string earliestEffectiveDate = DBHelper.ToDateTime(db.QuerySingleValue(sql
                        , customerID
                        , originID
                        , destinationID), new DateTime()).ToString("M/d/yyyy");

                return JsonResult(earliestEffectiveDate);
            }
        }

        // support method
        private ActionResult JsonResult(object data)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            settings.Formatting = Formatting.Indented;
            return Content(JsonConvert.SerializeObject(data, settings));
        }

        #region Functions accessible by _ForeignKeyDDL

        // Gets the list of states for the _StateDDL
        public JsonResult GetStates(int? id)
        {        
            var result = ( from s in db.States
                           select new
                           {
                               ID = s.ID,
                               FullName = s.FullName,
                               Name = s.FullName,
                               CountryID = s.CountryID,
                               Selected = (id == s.ID ? true : false)
                           })
                           .OrderBy(s => s.CountryID).ThenBy(s => s.Name)
                           .ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        
        // Gets the list of carriers
        public JsonResult GetCarriers(int? id, bool ActiveOnly = true)
        {        
            var result = ( from c in db.Carriers
                           where ActiveOnly == false || c.DeleteDateUTC == null || c.ID == id
                           select new
                           {
                               ID = c.ID,
                               Name = (c.DeleteDateUTC == null ? "" : "[DEACTIVATED] ") + c.Name,
                               Selected = (id == c.ID ? true : false),
                               SortOrder = (c.DeleteDateUTC == null ? "A" : "Z") + c.Name // put deactivated at end
                           })
                           .OrderBy(x => x.SortOrder)
                           .ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        // Gets the list of consignees
        public JsonResult GetConsignees(int? id, bool ActiveOnly = true)
        {        
            var result = ( from c in db.Consignees
                           where ActiveOnly == false || c.DeleteDateUTC == null || c.ID == id
                           select new
                           {
                               ID = c.ID,
                               Name = (c.DeleteDateUTC == null ? "" : "[DEACTIVATED] ") + c.Name,
                               Selected = (id == c.ID ? true : false),
                               SortOrder = (c.DeleteDateUTC == null ? "A" : "Z") + c.Name // put deactivated at end
                           })
                           .OrderBy(x => x.SortOrder)
                           .ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCarrierTypes(int? id)
        {
            var result = db.CarrierTypes
                .Select(x => new
                {
                    ID = x.ID,
                    Name = x.Name
                })
                .OrderBy(x => x.Name)
                .ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getDestinations()
        {
            return (JsonResult) getDestinationsJSON(null, null, null, null, null);
        }

        public JsonResult GetDrivers(int? id, int? carrierid, bool ActiveOnly = true)
        {        
            var result = db.Drivers.Where(c => (carrierid == null || c.CarrierID == carrierid)
            && (ActiveOnly == false || c.DeleteDateUTC == null))
                .Select(x => new
                {
                    ID = x.ID,
                    Name = (x.DeleteDateUTC == null ? "" : "[DEACTIVATED] ") + x.FirstName + " " + x.LastName,
                    SortOrder = (x.DeleteDateUTC == null ? "A" : "Z") + x.FirstName + x.LastName // put deactivated at end
                })
                .OrderBy(x => x.SortOrder)
                .ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDriverComplianceCategorys(int? id)
        {        
            var result = db.DriverComplianceCategories
                .Select(x => new
                {
                    ID = x.ID,
                    Name = x.Name
                })
                .OrderBy(x => x.Name)
                .ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDriverComplianceTypes(int? id, bool ActiveOnly = true)
        {        
            var result = db.DriverComplianceTypes
                .Where(x => (ActiveOnly == false || x.DeleteDateUTC == null))
                .Select(x => new
                {
                    ID = x.ID,
                    Name = (x.DeleteDateUTC == null ? "" : "[DEACTIVATED] ") + x.Name,
                    SortOrder = (x.DeleteDateUTC == null ? "A" : "Z") + x.Name // put deactivated at end
                })
                .OrderBy(x => x.SortOrder)
                .ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetRecords(int? id, int? objectFieldID)
        {
            ObjectField f = db.ObjectFields.Find(objectFieldID);
            if (f == null)
            {
                return Json(new { }, JsonRequestBehavior.AllowGet); // custom field not found
            }

            // TODO: CAP AT 500 but need a way to filter/page the lookup
            String sql = "select TOP 500 value=CAST(id AS VARCHAR(MAX)), text=CAST(" + f.ObjectDef.KeyField + " AS VARCHAR(MAX)) from " + (f.ObjectDef.SqlTargetName ?? f.ObjectDef.SqlSourceName);
            if (f.ObjectDef.CanDelete) sql += " WHERE DeleteDateUTC IS NULL"; // filter active
            var result = db.Database.SqlQuery<SelectListItem>(sql).AsQueryable()
                .Select(x => new
                {
                    ID = x.Value,
                    Name = x.Text
                })
                .ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetObjectFields(int? id, int? objectID, bool CustomOnly = true)
        {        
            var result = db.ObjectFields.Where(x => x.IsCustom == CustomOnly
                            && (objectID == null || x.ObjectDefID == objectID))
                    .Select(x => new
                    {
                        ID = x.ID,
                        Name = x.ObjectDef.Name + " - " + x.Name, // prepend table in drop down
                        Selected = (id == x.ID ? true : false),
                        SortOrder = x.ObjectDef.Name + x.Name
                    })
                    .OrderBy(x => x.SortOrder)
                    .ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPdfExportTypes(int? id)
        {        
            var result = db.Set<PdfExportType>()
                .Select(x => new
                {
                    ID = x.ID,
                    Name = x.Name,
                    Selected = (id == x.ID ? true : false)
                })
                .OrderBy(x => x.Name)
                .ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        // pdfExportTypeID is passed as a "OtherParams" entry to the "Read() definition" in the popup editor
        public JsonResult GetPdfExports(int? id, int? pdfExportTypeID)
        {
            var result = db.Set<PdfExport>()
                .Where(x => x.PdfExportTypeID == pdfExportTypeID || !pdfExportTypeID.HasValue)
                .Select(x => new
                {
                    ID = x.ID,
                    Name = x.Name,
                    Selected = (id == x.ID)
                })
                .OrderBy(x => x.Name)
                .ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetProducers(int? id, bool ActiveOnly = true)
        {        
            var result = db.Producers
                .Where(t => (ActiveOnly == false || t.DeleteDateUTC == null || t.ID == id))
                .Select(x => new
                {
                    ID = x.ID,
                    Name = (x.DeleteDateUTC == null ? "" : "[DEACTIVATED] ") + x.Name,
                    SortOrder = (x.DeleteDateUTC == null ? "A" : "Z") + x.Name // put deactivated at end
                })
                .OrderBy(x => x.SortOrder)
                .ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetProductGroups(int? id, bool ActiveOnly = true)
        {        
            var result = db.ProductGroups
                .Where(t => (ActiveOnly == false || t.DeleteDateUTC == null || t.ID == id))
                .Select(x => new
                {
                    ID = x.ID,
                    Name = (x.DeleteDateUTC == null ? "" : "[DEACTIVATED] ") + x.Name,
                    SortOrder = (x.DeleteDateUTC == null ? "A" : "Z") + x.Name // put deactivated at end
                })
                .OrderBy(x => x.SortOrder)
                .ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetShippers(int? carrierid, int? id, bool ActiveOnly = true)
        {        
            var result = db.Customers
                .Where(t => (ActiveOnly == false || t.DeleteDateUTC == null || t.ID == id))
                .Select(x => new
                {
                    ID = x.ID,
                    Name = (x.DeleteDateUTC == null ? "" : "[DEACTIVATED] ") + x.Name,
                    SortOrder = (x.DeleteDateUTC == null ? "A" : "Z") + x.Name // put deactivated at end
                })
                .OrderBy(x => x.SortOrder)
                .ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTrucks(int? carrierid, int? id, bool ActiveOnly = true)
        {        
            var result = db.Trucks
                .Where(t => (ActiveOnly == false || t.DeleteDateUTC == null || t.ID == id)
                            && (carrierid == null || t.CarrierID == carrierid))
                .Select(x => new
                {
                    ID = x.ID,
                    Name = (x.DeleteDateUTC == null ? "" : "[DEACTIVATED] ") + x.IDNumber,
                    SortOrder = (x.DeleteDateUTC == null ? "A" : "Z") + x.IDNumber // put deactivated at end
                })
                .OrderBy(x => x.SortOrder)
                .ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTruckTypes(int? id)
        {        
            var result = db.TruckTypes
                .Select(x => new
                {
                    ID = x.ID,
                    Name = x.Name
                })
                .OrderBy(x => x.Name)
                .ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTruckMaintenanceTypes(int? id, bool ActiveOnly = true)
        {
            var result = db.TruckTrailerMaintenanceTypes
                .Where(m => (ActiveOnly == false || m.DeleteDateUTC == null || m.ID == id)
                            && m.ForTruck == true)
                .Select(x => new
                {
                    ID = x.ID,
                    Name = (x.DeleteDateUTC == null ? "" : "[DEACTIVATED] ") + x.Name,
                    SortOrder = (x.DeleteDateUTC == null ? "A" : "Z") + x.Name // put deactivated at end
                })
                .OrderBy(x => x.SortOrder)
                .ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTrailerTypes(int? id)
        {
            var result = db.TrailerTypes
                .Select(x => new
                {
                    ID = x.ID,
                    Name = x.Name
                })
                .OrderBy(x => x.Name)
                .ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTrailerMaintenanceTypes(int? id, bool ActiveOnly = true)
        {
            var result = db.TruckTrailerMaintenanceTypes
                .Where(m => (ActiveOnly == false || m.DeleteDateUTC == null || m.ID == id)
                            && m.ForTrailer == true)
                .Select(x => new
                {
                    ID = x.ID,
                    Name = (x.DeleteDateUTC == null ? "" : "[DEACTIVATED] ") + x.Name,
                    SortOrder = (x.DeleteDateUTC == null ? "A" : "Z") + x.Name // put deactivated at end
                })
                .OrderBy(x => x.SortOrder)
                .ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCarrierComplianceTypes(int? id, int? carrierId, bool ActiveOnly = true)
        {
            //If CarrierID has been specified, filtering out any doc types specified for other carrier types is assumed
            int? carrierTypeId = null;
            if (carrierId != null)
                carrierTypeId = db.Carriers.Find(carrierId).CarrierTypeID;

            var result = db.CarrierComplianceTypes
                            .Where(x => (ActiveOnly == false || x.DeleteDateUTC == null)
                                    && (x.CarrierTypeID == null || x.CarrierTypeID == carrierTypeId || carrierTypeId == null))
                            .Select(x => new
                            {
                                ID = x.ID,
                                Name = (x.DeleteDateUTC == null ? "" : "[DEACTIVATED] ") + x.Name,
                                SortOrder = (x.DeleteDateUTC == null ? "A" : "Z") + x.Name // put deactivated at end
                            })
                            .OrderBy(x => x.SortOrder)
                            .ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTruckComplianceTypes(int? id, int? truckId, bool ActiveOnly = true)
        {
            //If truckID has been specified, filtering out any doc types specified for other truck types is assumed
            int? truckTypeId = null;
            if (truckId != null) 
                truckTypeId = db.Trucks.Find(truckId).TruckTypeID;

            var result = db.TruckComplianceTypes
                            .Where(x => (ActiveOnly == false || x.DeleteDateUTC == null)
                                    && (x.TruckTypeID == null || x.TruckTypeID == truckTypeId || truckTypeId == null))
                            .Select(x => new
                            {
                                ID = x.ID,
                                Name = (x.DeleteDateUTC == null ? "" : "[DEACTIVATED] ") + x.Name,
                                SortOrder = (x.DeleteDateUTC == null ? "A" : "Z") + x.Name // put deactivated at end
                            })
                            .OrderBy(x => x.SortOrder)
                            .ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTrailerComplianceTypes(int? id, int? trailerId, bool ActiveOnly = true)
        {
            //If trailerID has been specified, filtering out any doc types specified for other Trailer types is assumed
            int? trailerTypeId = null;
            if (trailerId != null)
                trailerTypeId = db.Trailers.Find(trailerId).TrailerTypeID;

            var result = db.TrailerComplianceTypes
                            .Where(x => (ActiveOnly == false || x.DeleteDateUTC == null)
                                    && (x.TrailerTypeID == null || x.TrailerTypeID == trailerTypeId || trailerTypeId == null))
                            .Select(x => new
                            {
                                ID = x.ID,
                                Name = (x.DeleteDateUTC == null ? "" : "[DEACTIVATED] ") + x.Name,
                                SortOrder = (x.DeleteDateUTC == null ? "A" : "Z") + x.Name // put deactivated at end
                            })
                            .OrderBy(x => x.SortOrder)
                            .ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTrailers(int? carrierid, int? id, bool ActiveOnly = true)
        {        
            var result = db.Trailers
                .Where(t => (ActiveOnly == false || t.DeleteDateUTC == null || t.ID == id)
                            && (carrierid == null || t.CarrierID == carrierid))
                .Select(x => new
                {
                    ID = x.ID,
                    Name = (x.DeleteDateUTC == null ? "" : "[DEACTIVATED] ") + x.IDNumber,
                    SortOrder = (x.DeleteDateUTC == null ? "A" : "Z") + x.IDNumber // put deactivated at end
                })
                .OrderBy(x => x.SortOrder)
                .ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTerminals(int? id, bool ActiveOnly = true)
        {
            var result = db.Terminals
                .Where(t => (ActiveOnly == false || t.DeleteDateUTC == null || t.ID == id))
                .Select(x => new
                {
                    ID = x.ID,
                    Name = (x.DeleteDateUTC == null ? "" : "[DEACTIVATED] ") + x.Name,
                    SortOrder = (x.DeleteDateUTC == null ? "A" : "Z") + x.Name // put deactivated at end
                })
                .OrderBy(x => x.SortOrder)
                .ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetRegions(int? id, bool ActiveOnly = true)
        {        
            var result = ( from r in db.Regions
                           where ActiveOnly == false || r.DeleteDateUTC == null || r.ID == id
                           select new
                           {
                               ID = r.ID,
                               Name = r.Name,
                               Selected = (id == r.ID ? true : false)
                           }).ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDriverSettlementStatementEmailDefinitions(int? id)
        {
            var result = db.Set<DriverSettlementStatementEmailDefinition>()
                .Select(x => new
                {
                    ID = x.ID,
                    Name = x.Name,
                    Selected = (id == x.ID)
                })
                .OrderBy(x => x.Name)
                .ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDriverShiftTypes(int? id, bool ActiveOnly = true)
        {        
            var result = ( from ds in db.DriverShiftTypes
                           where ActiveOnly == false || ds.DeleteDateUTC == null || ds.ID == id
                           select new
                           {
                               ID = ds.ID,
                               Name = ds.Name,
                               Selected = (id == ds.ID ? true : false)
                           }).ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDriverGroups(int? id)
        {        
            var result = ( from dg in db.DriverGroups
                           select new
                           {
                               ID = dg.ID,
                               Name = dg.Name,
                               Selected = (id == dg.ID ? true : false)
                           }).ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        // Gets the list of Uoms for the _UomDDL editor template
        public JsonResult GetUoms(int? id)
        {
            var result = (from u in db.Uoms
                          orderby u.Name
                          select new
                          {
                              ID = u.ID,
                              Name = u.Name,
                              Selected = (id == u.ID ? true : false)
                          }).ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        // Gets the list of Uom types for the _UomTypeDDL editor template
        public JsonResult GetUomTypes(int? id)
        {
            var result = (from t in db.UomTypes
                          select new
                          {
                              ID = t.ID,
                              TypeName = t.Name,
                              Selected = (id == t.ID ? true : false)
                          }).ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCommodityIndexs(int? id) //Indexs (not Indexes) is required for _ForeignKeyDDL to find this function
        {
            var result = (from i in db.CommodityIndexes
                          select new
                          {
                              ID = i.ID,
                              Name = i.ShortName,
                              Selected = (id == i.ID ? true : false)
                          }).ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDriverEquipmentTypes(int? id, bool ActiveOnly = true)
        {
            var result = db.DriverEquipmentTypes
                .Where(m => (ActiveOnly == false || m.DeleteDateUTC == null || m.ID == id))
                .Select(x => new
                {
                    ID = x.ID,
                    Name = (x.DeleteDateUTC == null ? "" : "[DEACTIVATED] ") + x.Name,
                    SortOrder = (x.DeleteDateUTC == null ? "A" : "Z") + x.Name // put deactivated at end
                })
                .OrderBy(x => x.SortOrder)
                .ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDriverEquipmentManufacturers(int? id, bool ActiveOnly = true)
        {
            var result = db.DriverEquipmentManufacturers
                .Where(m => (ActiveOnly == false || m.DeleteDateUTC == null || m.ID == id))
                .Select(x => new
                {
                    ID = x.ID,
                    Name = (x.DeleteDateUTC == null ? "" : "[DEACTIVATED] ") + x.Name,
                    SortOrder = (x.DeleteDateUTC == null ? "A" : "Z") + x.Name // put deactivated at end
                })
                .OrderBy(x => x.SortOrder)
                .ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #endregion

        /**************************************************************************************************/
        /**  DESCRIPTION: Returns select list for popuplating destinations in an MVC dropdown.  Since    **/
        /**       function is static, it assumes that any profile filters have already been applied.     **/
        /**************************************************************************************************/
        public static SelectList getDestinationsSL(int? OriginID, int? ShipperID, int? RegionID, int? TerminalID, int? ProductID, int? selected = null, bool ActiveOnly = true)
        {
            List<SelectListItem> list = new List<SelectListItem>();

            foreach (Destination destination in getValidDestinations(OriginID, ShipperID, RegionID, TerminalID, ProductID, ActiveOnly))
            {
                list.Add(new SelectListItem()
                {
                    Value = destination.ID.ToString(),
                    Text = destination.Name
                });
            }

            // Add the current destination if it is a deleted record
            if (selected != null)
            {
                using (DispatchCrudeDB db = new DispatchCrudeDB())
                {
                    Destination d = db.Destinations.Find(selected);
                    if (d != null && d.Deleted)
                    {
                        list.Add(new SelectListItem()
                        {
                            Value = d.ID.ToString(),
                            Text = d.Name
                        });
                    }
                }
            }

            return new SelectList(list.OrderBy(d => d.Text), "Value", "Text", selected);
        }

        /**************************************************************************************************/
        /**  PAGE: ~/Destinations/getDestinationsJSON/?OriginID={originid}&ShipperID={shipperid}         **/
        /**                                &RegionID={regionid}&TerminalID={terminalid}                  **/
        /**                                &ProductID={productid}                                        **/
        /**  DESCRIPTION: JSON page that returns destinations for an origin/shipper/region/product       **/
        /**************************************************************************************************/
        public ActionResult getDestinationsJSON(int? OriginID, int? ShipperID, int? RegionID, int? TerminalID, int? ProductID, bool ActiveOnly = true)
        {
            // Filter results based on my shipper/region
            int myShipperID = Core.Converter.ToInt32(Profile.GetPropertyValue("CustomerID"));
            int myRegionID = Core.Converter.ToInt32(Profile.GetPropertyValue("RegionID"));
            int myTerminalID = Core.Converter.ToInt32(Profile.GetPropertyValue("TerminalID"));

            if (ShipperID == null)
            {
                // Show all but use profile to determine what "ALL" means
                ShipperID = myShipperID;
            }
            else if (ShipperID != myShipperID && myShipperID != -1)
            {
                return Json(null, JsonRequestBehavior.AllowGet); // not allowed to see destinations for this shipper
            }

            if (RegionID == null)
            {
                // Show all but use profile to determine what "ALL" means
                RegionID = myRegionID;
            }
            else if (RegionID != myRegionID && myRegionID != 0)
            {
                return Json(null, JsonRequestBehavior.AllowGet); // not allowed to see destinations for this region
            }

            if (TerminalID == null)
            {
                // Show all but use profile to determine what "ALL" means
                TerminalID = myTerminalID;
            }
            else if (TerminalID != myTerminalID && myTerminalID > 0)
            {
                return Json(null, JsonRequestBehavior.AllowGet); // not allowed to see destinations for this region
            }

            var result = getValidDestinations(OriginID, ShipperID, RegionID, TerminalID, ProductID, ActiveOnly)
                .Select(x => new { ID = x.ID, Name = x.Name })
                .OrderBy(x => x.Name)
                .ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        /**************************************************************************************************/
        /**  DESCRIPTION: Retrieve valid destinations based on the search criteria.  Used to populate    **/
        /**       MVC drop downs and for JSON requests.  Since function is static, it assumes that any   **/
        /**       profile filters have already been applied.                                             **/
        /**************************************************************************************************/
        private static List<Destination> getValidDestinations(int? OriginID, int? ShipperID, int? RegionID, int? TerminalID, int? ProductID, bool ActiveOnly)
        {
            using (DispatchCrudeDB db = new DispatchCrudeDB())
            {
                IQueryable<Destination> destinations = db.Destinations;
                if (ActiveOnly)
                {
                    destinations = destinations.Where(d => d.DeleteDateUTC == null);
                }
                if (OriginID != null && OriginID != -1)
                {
                    IQueryable<Customer> originShippers = db.Customers.Where(p => p.Origins.Any(o => o.ID == OriginID));
                    destinations = destinations.Where(d => d.Shippers.Intersect(originShippers).Any());
                }

                if (RegionID != null && RegionID != 0)
                    destinations = destinations.Where(d => d.RegionID == RegionID);

                if (TerminalID != null && TerminalID > 0)
                    destinations = destinations.Where(d => d.TerminalID == TerminalID || d.TerminalID == null);

                if (ProductID != null && ProductID != -1)
                    destinations = destinations.Where(d => d.Products.Any(p => p.ID == ProductID));

                if (ShipperID != null && ShipperID != -1)
                    destinations = destinations.Where(d => d.Shippers.Any(s => s.ID == ShipperID));

                return destinations.OrderBy(x => x.Name).ToList();
            }
        }


        /**************************************************************************************************/
        /**  DESCRIPTION: Retrieve valid origins based on the search criteria.  Used to populate         **/
        /**       MVC drop downs and for JSON requests.  Since function is static, it assumes that any   **/
        /**       profile filters have already been applied.                                             **/
        /**************************************************************************************************/
        private static List<Origin> getValidOrigins(int? DestinationID, int? ShipperID, int? RegionID, int? ProductID, bool ActiveOnly)
        {
            using (DispatchCrudeDB db = new DispatchCrudeDB())
            {
                IQueryable<Origin> origins = db.Origins;
                if (ActiveOnly)
                {
                    origins = origins.Where(d => d.DeleteDateUTC == null);
                }
                if (DestinationID != null && DestinationID != -1)
                {
                    IQueryable<Customer> destinationShippers = db.Customers.Where(p => p.Destinations.Any(o => o.ID == DestinationID));
                    origins = origins.Where(d => d.Shippers.Intersect(destinationShippers).Any());
                }

                if (RegionID != null && RegionID != 0)
                    origins = origins.Where(d => d.RegionID == RegionID);

                if (ProductID != null && ProductID != -1)
                    origins = origins.Where(d => d.Products.Any(p => p.ID == ProductID));

                if (ShipperID != null && ShipperID != -1)
                    origins = origins.Where(d => d.Shippers.Any(s => s.ID == ShipperID));

                return origins.OrderBy(x => x.Name).ToList();
            }
        }


        /**************************************************************************************************/
        /**  DESCRIPTION: Retrieve valid tank Definitions based on the search criteria.                   **/
        /**************************************************************************************************/
        private static List<OriginTankDefinition> getValidOriginTankDefinitions(bool ActiveOnly)
        {
            using (DispatchCrudeDB db = new DispatchCrudeDB())
            {
                IQueryable<OriginTankDefinition> definitions = db.OriginTankDefinitions;

                if (ActiveOnly)
                {
                    definitions = definitions.Where(d => d.DeleteDateUTC == null);
                }

                return definitions.OrderBy(m => m.Name).ToList();
            }
        }


        /**************************************************************************************************/
        /**  DESCRIPTION: Returns select list for popuplating origins in an MVC dropdown.  Since         **/
        /**       function is static, it assumes that any profile filters have already been applied.     **/
        /**************************************************************************************************/
        public static SelectList getOriginsSL(int? DestinationID, int? ShipperID, int? RegionID, int? ProductID, int? selected = null, bool ActiveOnly = true)
        {
            List<SelectListItem> list = new List<SelectListItem>();

            foreach (Origin origin in getValidOrigins(DestinationID, ShipperID, RegionID, ProductID, ActiveOnly))
            {
                list.Add(new SelectListItem()
                {
                    Value = origin.ID.ToString(),
                    Text = origin.Name
                });
            }

            // Add the current destination if it is a deleted record
            if (selected != null)
            {
                using (DispatchCrudeDB db = new DispatchCrudeDB())
                {
                    Origin o = db.Origins.Find(selected);
                    if (o != null && o.Deleted)
                    {
                        list.Add(new SelectListItem()
                        {
                            Value = o.ID.ToString(),
                            Text = o.Name
                        });
                    }
                }
            }

            return new SelectList(list.OrderBy(o => o.Text), "Value", "Text", selected);
        }

        /**************************************************************************************************/
        /**  DESCRIPTION: Returns select list for popuplating the origin tank Definition DDL.              **/
        /**************************************************************************************************/
        public static SelectList getOriginTankDefinitionsSL(int? selected = null, bool ActiveOnly = true)
        {            
            List<SelectListItem> list = new List<SelectListItem>();           

            foreach (OriginTankDefinition definition in getValidOriginTankDefinitions(ActiveOnly))
            {
                list.Add(new SelectListItem()
                {
                    Value = definition.ID.ToString(),
                    Text = definition.Name
                });
            }

            // Add the current definition if it is a deleted record
            if (selected != null)
            {
                using (DispatchCrudeDB db = new DispatchCrudeDB())
                {
                    OriginTankDefinition definition = db.OriginTankDefinitions.Find(selected);
                    if (definition != null && definition.Deleted)
                    {
                        list.Add(new SelectListItem()
                        {
                            Value = definition.ID.ToString(),
                            Text = definition.Name
                        });
                    }
                }
            }

            return new SelectList(list.OrderBy(m => m.Text), "Value", "Text", selected);
        }

    }
}
