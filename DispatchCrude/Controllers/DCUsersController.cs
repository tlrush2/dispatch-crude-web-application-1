﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Mvc;
using System.Data;
using AlonsIT;
using System.Data.Entity;
using System.Net;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using DispatchCrude.Models;
using DispatchCrude.App_Code;
using DispatchCrude.ViewModels;
using System.Web.Profile;

namespace DispatchCrude.Controllers
{
    [Authorize(Roles = "viewUsers")]
    public class DCUsersController : Controller
    {
        private DispatchCrudeDB db = new DispatchCrudeDB();

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        // GET: Users
        public ActionResult Index()
        {
            return View();
        }

        
        public ActionResult Read([DataSourceRequest] DataSourceRequest request = null, Guid? id = null)
        {
            return Read(request, null, id);
        }

        private ContentResult Read([DataSourceRequest] DataSourceRequest request, ModelStateDictionary modelState, Guid? id)
        {
            IEnumerable<DCUserViewModel> data = db.DCUsers.Where(u => u.UserId == id || id == null)
                                                    .Include(u => u.Groups)
                                                    .Include(u => u.Membership)
                                                    .Select(x => new DCUserViewModel()
                                                    {
                                                        Id = x.UserId
                                                        , Active = (x.Membership.DeleteDateUTC == null)
                                                        , GroupCount = x.Groups.Count                                                                                                        
                                                        , UserName = x.UserName
                                                        , Email = x.Membership.Email
                                                        , Comment = x.Membership.Comment
                                                        , LastLoginDateUTC = x.Membership.LastLoginDate
                                                        , CreateDateUTC = (DateTime)x.Membership.CreateDateUTC
                                                        , CreatedByUser = x.Membership.CreatedByUser
                                                        , LastChangeDateUTC = x.Membership.LastChangeDateUTC
                                                        , LastChangedByUser = x.Membership.LastChangedByUser
                                                        , DeleteDateUTC = x.Membership.DeleteDateUTC
                                                        , DeletedByUser = x.Membership.DeletedByUser
                                                    }
                                                    ).ToList();

            //The full name could not be retrieved in the above assignment to "data" therefore a loop here is required in order to display
            //the full name on the grid
            foreach (DCUserViewModel item in data)
            {
                item.FullName = DCUserViewModel.getFullName(item.UserName);
            }

            return JsonStringResult.Create(data.ToDataSourceResult(request, modelState));            
        }

        // POST: Create user
        [HttpPost]
        [Authorize(Roles = "createUsers")]
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, DCUser user, DCMembership membership)
        {
            if (string.IsNullOrEmpty(membership.Password))
                ModelState.AddModelError("password", "The Password is required");

            if (ModelState.IsValid)
            {
                try
                {
                    //Using current membership code (older aspnet membership) to create new user.  This should eventually be changed to 
                    //the new identity stuff.
                    MembershipUser newUser = Membership.CreateUser(user.UserName, membership.Password, membership.Email);

                    //Using MVC to update the membership table was not working.  Used SSDB option to get around that problem.
                    using (SSDB ssdb = new SSDB())
                    {
                        ssdb.ExecuteSql("UPDATE aspnet_Membership SET CreatedByUser = '{0}', Comment = '{1}' WHERE UserId = '{2}'"
                            , User.Identity.Name
                            , ((membership.Comment == null || membership.Comment.Length < 1) ? "" : membership.Comment.Replace("'", "''"))  //this is necessary in order to capture User.Identity.Name
                            , newUser.ProviderUserKey);
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(null, ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { user }.ToDataSourceResult(request, ModelState));
                }
            }

            return Read(request, ModelState, user.UserId);
        }


        // Deactivate
        [Authorize(Roles = "deactivateUsers")]
        public ActionResult Delete([DataSourceRequest] DataSourceRequest request, DCUser user, DCMembership membership)
        {
            //Deactivate
            MembershipUser existingUser = Membership.GetUser(user.UserName);
                        
            //Using MVC to update the membership table was not working.  Used SSDB option to get around that problem.
            using (SSDB ssdb = new SSDB())
            {
                if (membership.DeleteDateUTC == null)
                {
                    //Deactivate
                    ssdb.ExecuteSql("UPDATE aspnet_Membership SET DeleteDateUTC = GETUTCDATE(), DeletedByUser = '{0}', IsApproved = 0 WHERE UserId = '{1}'"
                    , User.Identity.Name
                    , existingUser.ProviderUserKey);

                    //Remove user's groups (and associated permissions)
                    ssdb.ExecuteSql("EXECUTE spDeleteUserGroupsAndPermissions '{0}'", existingUser.ProviderUserKey);
                }
                else
                {
                    //Reactivate
                    ssdb.ExecuteSql("UPDATE aspnet_Membership SET DeleteDateUTC = NULL, DeletedByUser = NULL, IsApproved = 1 WHERE UserId = '{0}'"
                    , existingUser.ProviderUserKey);
                }
                
            }

            //Actual delete
            //Membership.DeleteUser(user.UserName, true);

            return null;
        }

        //GET user profile data        
        public ActionResult UserProfile(Guid id)
        {

            DCUserViewModel user = db.DCUsers.Where(u => u.UserId == id || id == null)
                                                    .Include(u => u.Groups)
                                                    .Include(u => u.Membership)
                                                    .Select(x => new DCUserViewModel()
                                                    {
                                                        Id = x.UserId
                                                        , Active = (x.Membership.DeleteDateUTC == null)
                                                        , GroupCount = x.Groups.Count
                                                        , UserName = x.UserName
                                                        , Email = x.Membership.Email
                                                        , IsApproved = x.Membership.IsApproved
                                                        , IsLockedOut = x.Membership.IsLockedOut
                                                        , Comment = x.Membership.Comment
                                                        //, LastActivityDate = x.LastActivityDate
                                                        //, LastActivityDateDisplay = x.LastActivityDateDisplay
                                                        , CreateDateUTC = (DateTime)x.Membership.CreateDateUTC
                                                        , CreatedByUser = x.Membership.CreatedByUser
                                                        , LastChangeDateUTC = x.Membership.LastChangeDateUTC
                                                        , LastChangedByUser = x.Membership.LastChangedByUser
                                                        , DeleteDateUTC = x.Membership.DeleteDateUTC
                                                        , DeletedByUser = x.Membership.DeletedByUser
                                                        , LastLoginDateUTC = x.Membership.LastLoginDate
                                                        , LastActivityDateUTC = x.LastActivityDate
                                                        , LastLockoutDateUTC = x.Membership.LastLockoutDate
                                                        , LastPasswordChangedDateUTC = x.Membership.LastPasswordChangedDate
                                                    }).ToList().First();

            DCUser ug = db.DCUsers.Include(u => u.Groups).Where(u => u.UserId == id).ToList().First();
            ViewBag.UserGroups = ug.Groups.ToList();
                        
            #region Get user's current profile data
            ProfileBase UserProfile = new DispatchCrudeProfilePresenter().GetProfile(user.UserName);
            
            user.CarrierID = UserProfile.GetPropertyValue("CarrierID") == null ? "" : UserProfile.GetPropertyValue("CarrierID").ToString();
            user.DriverID = UserProfile.GetPropertyValue("DriverID") == null ? "" : UserProfile.GetPropertyValue("DriverID").ToString();
            user.GaugerID = UserProfile.GetPropertyValue("GaugerID") == null ? "" : UserProfile.GetPropertyValue("GaugerID").ToString();
            user.ShipperID = UserProfile.GetPropertyValue("CustomerID") == null ? "" : UserProfile.GetPropertyValue("CustomerID").ToString();
            user.ProducerID = UserProfile.GetPropertyValue("ProducerID") == null ? "" : UserProfile.GetPropertyValue("ProducerID").ToString();
            user.RegionID = UserProfile.GetPropertyValue("RegionID") == null ? "" : UserProfile.GetPropertyValue("RegionID").ToString();
            user.TimeZoneID = UserProfile.GetPropertyValue("TimeZoneID") == null ? "" : UserProfile.GetPropertyValue("TimeZoneID").ToString();
            user.TerminalID = UserProfile.GetPropertyValue("TerminalID") == null ? "" : UserProfile.GetPropertyValue("TerminalID").ToString();
            user.UseDST = UserProfile.GetPropertyValue("UseDST").ToString() == "true" ? true : false;
            user.FullName = (UserProfile.GetPropertyValue("FullName") != null) ? UserProfile.GetPropertyValue("FullName").ToString() : "";
            #endregion

            #region Populate lists for dropdown boxes
            List <SelectListItem> ListOfCarriers = new SelectList(db.Carriers.Where(c => c.DeleteDateUTC == null).OrderBy(c => c.Name), "ID", "Name", user.CarrierID).ToList();
                ListOfCarriers.Insert(0, new SelectListItem() { Text = "(All Carriers)", Value = "-1", Selected = (user.CarrierID == "-1") });
            List<SelectListItem> ListOfDrivers = new SelectList(db.Drivers.Where(d => d.DeleteDateUTC == null).OrderBy(d => d.FirstName).ThenBy(d => d.LastName), "ID", "FullName", user.DriverID).ToList();
            List<SelectListItem> ListOfGaugers = new SelectList(db.Gaugers.Where(g => g.DeleteDateUTC == null).OrderBy(g => g.FirstName).ThenBy(g => g.LastName), "ID", "FullName", user.GaugerID).ToList();
            List<SelectListItem> ListOfShippers = new SelectList(db.Customers.Where(s => s.DeleteDateUTC == null).OrderBy(s=> s.Name), "ID", "Name", user.ShipperID).ToList();
                ListOfShippers.Insert(0, new SelectListItem() { Text = "(All Shippers)", Value = "-1", Selected = (user.ShipperID == "-1") });
            List<SelectListItem> ListOfProducers = new SelectList(db.Producers.Where(p=> p.DeleteDateUTC == null).OrderBy(p => p.Name), "ID", "Name", user.ProducerID).ToList();
            List<SelectListItem> ListOfRegions = new SelectList(db.Regions.OrderBy(r => r.Name), "ID", "Name", user.RegionID).ToList();
            List<SelectListItem> ListOfTimeZones = new SelectList(db.TimeZones.OrderBy(t => t.Name), "ID", "Name", user.TimeZoneID).ToList();
            List<SelectListItem> ListOfTerminals = new SelectList(db.Terminals.OrderBy(t => t.Name), "ID", "Name", user.TerminalID).ToList();                
            #endregion

            #region Set viewbag data
            ViewBag.CarrierID = ListOfCarriers;
            ViewBag.DriverID = ListOfDrivers;
            ViewBag.GaugerID = ListOfGaugers;
            ViewBag.ShipperID = ListOfShippers;
            ViewBag.ProducerID = ListOfProducers;
            ViewBag.RegionID = ListOfRegions;
            ViewBag.TimeZoneID = ListOfTimeZones;
            ViewBag.TerminalID = ListOfTerminals;
            ViewBag.UserID = id;
            ViewBag.UserName = user.UserName;
            ViewBag.AllGroups = db.DCGroups.Include(g => g.Permissions).Where(g => g.DeleteDateUTC == null).OrderBy(g => g.GroupName).ToList();
            #endregion

            return View(user);
        }


        [HttpPost]
        [Authorize(Roles = "editUsers")]
        public ActionResult UserProfile(DCUserViewModel user, params string[] SelectedGroups)
        {
            if (DriverAlreadyAssigned(user.UserName, user.DriverID))
            {
                string assignedUser = WhatUserIsAssigned(user.UserName, user.DriverID);
                string driverName = db.Database.SqlQuery<string>("SELECT NAME = FirstName + ' ' + LastName FROM tblDriver WHERE ID=" + user.DriverID).First();

                TempData["error"] = "The username \"" + assignedUser + "\" is already assigned to driver \""+ driverName +"\"";
                return RedirectToAction("UserProfile", new { id = user.Id });
            }

            #region Save Membership Data
            // Update user information stored in the membership table
            MembershipUser membershipUser = Membership.GetUser(user.UserName);
            membershipUser.Email = user.Email;
            membershipUser.Comment = user.Comment;
            membershipUser.IsApproved = user.IsApproved; //"Active"            
            // Change user password if they specified a new password
            if (!string.IsNullOrEmpty(user.Password1) && !string.IsNullOrEmpty(user.Password2) && user.Password2.Trim() == user.Password1.Trim())
                membershipUser.ChangePassword(membershipUser.ResetPassword(), user.Password1);
            // Unlock user (lockouts are produced by too many invalid password attempts)
            if (membershipUser.IsLockedOut == true && user.IsLockedOut == false)
                membershipUser.UnlockUser();
            // Update the membership user information using the existing built in function
            Membership.UpdateUser(membershipUser);

            //Using MVC to update the membership table last changed information.  Used SSDB option to get around that problem.
            using (SSDB ssdb = new SSDB())
            {
                ssdb.ExecuteSql("UPDATE aspnet_Membership SET LastChangeDateUTC = '{0}', LastChangedByUser = '{1}' WHERE UserId = '{2}'"
                    , DateTime.UtcNow
                    , User.Identity.Name
                    , user.Id);
            }

            // If "Active" has been changed, make sure that the user is actually activated/deactivated
            using (SSDB ssdb = new SSDB())
            {
                MembershipUser existingUser = Membership.GetUser(user.UserName);

                switch (user.IsApproved)
                {
                    case true:
                        //Reactivate
                        ssdb.ExecuteSql("UPDATE aspnet_Membership SET DeleteDateUTC = NULL, DeletedByUser = NULL, IsApproved = 1 WHERE UserId = '{0}'"
                        , existingUser.ProviderUserKey);
                        break;
                    case false:
                        //Deactivate
                        ssdb.ExecuteSql("UPDATE aspnet_Membership SET DeleteDateUTC = GETUTCDATE(), DeletedByUser = '{0}', IsApproved = 0 WHERE UserId = '{1}'"
                        , User.Identity.Name
                        , existingUser.ProviderUserKey);
                        break;
                }
            }
            #endregion

            #region Save User Settings Data
            DispatchCrudeProfilePresenter userProfile = new DispatchCrudeProfilePresenter();
            userProfile.UpdateProfile(user.UserName
                                    , (user.FullName != null) ? user.FullName : ""
                                    , user.CarrierID
                                    , user.DriverID
                                    , user.ShipperID
                                    , user.ProducerID
                                    , user.RegionID
                                    , user.TimeZoneID                                    
                                    , user.UseDST.ToString()
                                    , user.GaugerID
                                    , user.TerminalID
                                    );
            #endregion

            #region Save User Groups Data
            DCUser ug = db.DCUsers.Include(u => u.Groups).Where(u => u.UserId == user.Id).ToList().First();
            IEnumerable<DCGroup> currentGroups = ug.Groups.ToList();

            switch (user.IsApproved)
            {
                case true:  //As long as the users has not been deactivated go ahead and save the current selected groups 
                    if (SelectedGroups != null)
                    {
                        using (SSDB ssdb = new SSDB())
                        {
                            // Add user to any groups they are not already a member of
                            foreach (string groupId in SelectedGroups)
                            {
                                bool inGroup = false;

                                // Check to see if the user is in the group already
                                using (System.Data.SqlClient.SqlCommand cmd = ssdb.BuildCommand("SELECT dbo.fnIsUserInGroup(@UserID, @GroupID)"))
                                {
                                    cmd.Parameters.AddWithValue("@UserID", user.Id);
                                    cmd.Parameters.AddWithValue("@GroupID", groupId);
                                    inGroup = DBHelper.ToBoolean(cmd.ExecuteScalar());
                                }

                                // If the user is not in the group add the group (permissions will be taken care of by the trigger)
                                if (inGroup == false)
                                {
                                    ssdb.ExecuteSql("INSERT INTO aspnet_UsersInGroups (UserId, GroupId) SELECT '{0}', '{1}'", user.Id, groupId);
                                }
                            }

                            // Remove user from any groups that have been deselected
                            foreach (var group in currentGroups)
                            {
                                if (SelectedGroups.Contains(group.GroupId.ToString()) == false)
                                {
                                    //ssdb.ExecuteSql("DELETE FROM aspnet_UsersInGroups WHERE UserId = '{0}' AND GroupId = '{1}'", user.UserId, group.GroupId);
                                    ssdb.ExecuteSql("EXECUTE spDeleteUserGroupsAndPermissions '{0}', '{1}'", user.Id, group.GroupId);
                                }
                            }
                        }
                    }
                    else
                    {
                        using (SSDB ssdb = new SSDB())
                        {
                            // If the user de-selected all groups, remove user from all current groups
                            ssdb.ExecuteSql("EXECUTE spDeleteUserGroupsAndPermissions '{0}'", user.Id);
                        }
                    }
                    break;

                case false:     //Don't save groups if user has been deactivated
                    using (SSDB ssdb = new SSDB())
                    {
                        MembershipUser existingUser = Membership.GetUser(user.UserName);
                        
                        //Remove user's groups (and associated permissions)
                        ssdb.ExecuteSql("EXECUTE spDeleteUserGroupsAndPermissions '{0}'", existingUser.ProviderUserKey);                                               
                    }
                    break;
            }
            #endregion

            // return to user screen
            TempData["success"] = "User profile updated successfully.";
            return RedirectToAction("UserProfile", new { id = user.Id });
        }


        private bool DriverAlreadyAssigned(string Username, string DriverID)
        {
            if (string.IsNullOrEmpty(DriverID) || DriverID == "0")
                return false;

            string sql = string.Format(@"SELECT COUNT(*) FROM aspnet_Users u
                 CROSS APPLY (SELECT * FROM fnUserAllProfileValues(username) where name = 'DriverID') p
                 WHERE Value = {0}
                 AND username <> '{1}'",
                 DriverID,
                 Username);
            var driverEligibility = db.Database.SqlQuery<int>(sql).First();

            return driverEligibility > 0;
        }

        private string WhatUserIsAssigned(string UserName, string DriverID)
        {
            if (string.IsNullOrEmpty(DriverID))
                return null;

            string sql = string.Format(@"SELECT UserName 
                                         FROM aspnet_Users U
                                         CROSS APPLY 
                                             (SELECT * 
                                              FROM fnUserAllProfileValues(username) 
                                              WHERE name = 'DriverID') X
                                         WHERE X.value = {0}
                                         AND U.UserName <> '{1}'",
                                        DriverID,
                                        UserName);
            string assignedUser = db.Database.SqlQuery<string>(sql).First();

            return assignedUser;
        }

    }
}