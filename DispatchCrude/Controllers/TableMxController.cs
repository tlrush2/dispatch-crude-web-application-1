﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DispatchCrude.Controllers
{
    public class TableMxController : Controller
    {
        private Models.DispatchCrudeDB _db = new Models.DispatchCrudeDB();

        public ActionResult OriginEdit(int id)
        {
            return View(_db.Origins.SingleOrDefault(o => o.ID == id));
        }

        [HttpGet]
        public ActionResult TestData()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetTicketTypes()
        {
            return Json(_db.TicketTypes.Select(tt => new { ID = tt.ID, Name = tt.Name }), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetOriginTypes()
        {
            return Json(_db.OriginTypes.Select(tt => new { ID = tt.ID, Name = tt.Name }), JsonRequestBehavior.AllowGet);
        }
    }
}
