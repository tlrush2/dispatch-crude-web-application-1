﻿using System;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using DispatchCrude.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;

namespace DispatchCrude.Controllers
{
    [Authorize(Roles = "viewPdfExportType")]
    public class PdfExportTypeController : _DBControllerRead
    {
        // GET: Product Groups
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        override protected ContentResult Read([DataSourceRequest] DataSourceRequest request, ModelStateDictionary modelState, int id)
        {
            var data = db.Set<PdfExportType>().Where(m => m.ID == id || id == 0 || (id == -1 && !m.DeleteDateUTC.HasValue)).OrderBy(m => m.Name);
            return ToJsonResult(data, request, modelState);
        }

        // POST: Update PdfExportType
        [HttpPost]
        [Authorize(Roles = "editPdfExportType")]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, PdfExportType model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (model.ID == 0 && !User.IsInRole("createPdfExportType")) throw new Exception("User has insufficient rights to create Pdf Export Types");
                    db.AddOrUpdateSave(User.Identity.Name, model);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { model }.ToDataSourceResult(request, ModelState));
                }
            }

            return Read(request, ModelState, model.ID);
        }


        // Delete (Deactivate)
        [HttpPost]
        [Authorize(Roles = "deactivatePdfExportType")]
        // NOTE: the _permanentDelete field is passed from the browser if SHIFT was depressed on Delete button click
        public ActionResult Delete([DataSourceRequest] DataSourceRequest request, PdfExportType model, bool _permanentDelete = false)
        {
            return DeleteInt(request, model, _permanentDelete);
        }
    }
}
