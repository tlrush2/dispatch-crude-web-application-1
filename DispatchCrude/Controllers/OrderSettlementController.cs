﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DispatchCrude.ViewModels;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using DispatchCrude.App_Code;
using DispatchCrude.Core;

namespace DispatchCrude.Controllers
{
    [Authorize(Roles = "viewCarrierSettlementBatches,viewDriverSettlementBatches,viewShipperSettlementBatches")]
    public class OrderSettlementController : _DBController
    {
        public static List<SelectListItem> batchdates = new List<SelectListItem>()
                {
                    new SelectListItem() { Value = "BatchDate", Text = "Batch Date" },
                    new SelectListItem() { Value = "PeriodEndDate", Text = "Period End Date" }
                };


        public ActionResult Index()
        {
            return ShipperIndex(new FilterViewModel<OrderSettlementHistory>());
        }

        public ActionResult CarrierIndex(FilterViewModel<OrderSettlementHistory> fvm)
        {
            fvm.CustomFilter = "Carrier";
            ViewBag.CustomFilter2 = new SelectList(batchdates, "Value", "Text", fvm.CustomFilter2);

            // Filter log based on my carrier
            int myCarrierID = Converter.ToInt32(Profile.GetPropertyValue("CarrierID"));
            ViewBag.CarrierID = new SelectList(db.Carriers.Where(c => (myCarrierID == -1 || c.ID == myCarrierID)).OrderBy(c => c.Name), "ID", "Name", fvm.CarrierID);

            return View("Index", fvm);
        }

        public ActionResult DriverIndex(FilterViewModel<OrderSettlementHistory> fvm)
        {
            // Filter log based on my carrier/driver
            int myCarrierID = Converter.ToInt32(Profile.GetPropertyValue("CarrierID"));
            int myDriverID = Converter.ToInt32(Profile.GetPropertyValue("DriverID"));
            ViewBag.CarrierID = new SelectList(db.Carriers.Where(c => (myCarrierID == -1 || c.ID == myCarrierID)).OrderBy(c => c.Name), "ID", "Name", fvm.CarrierID);
            ViewBag.DriverGroupID = new SelectList(db.DriverGroups.OrderBy(dg => dg.Name), "ID", "Name", fvm.DriverGroupID);
            ViewBag.DriverID = new SelectList(db.Drivers.Where(d => (myCarrierID == -1 || (myDriverID == -1 && d.CarrierID == myCarrierID) || myDriverID == d.ID)).OrderBy(d => d.FirstName).ThenBy(d => d.LastName), "ID", "FullName", fvm.DriverID);

            fvm.CustomFilter = "Driver";
            ViewBag.CustomFilter2 = new SelectList(batchdates, "Value", "Text", fvm.CustomFilter2);

            return View("Index", fvm);
        }

        public ActionResult ShipperIndex(FilterViewModel<OrderSettlementHistory> fvm)
        {
            // Filter dashboard based on my carrier/driver
            int myShipperID = Converter.ToInt32(Profile.GetPropertyValue("CustomerID"));
            ViewBag.ShipperID = new SelectList(db.Customers.Where(x => myShipperID == -1 || x.ID == myShipperID).OrderBy(x => x.Name), "ID", "Name", fvm.ShipperID);

            fvm.CustomFilter = "Shipper";
            ViewBag.CustomFilter2 = new SelectList(batchdates, "Value", "Text", fvm.CustomFilter2);

            return View("Index", fvm);
        }


        public ActionResult ReadCarrier(FilterViewModel<OrderSettlementHistory> fvm, [DataSourceRequest] DataSourceRequest request = null, int id = 0)
        {
            return ReadCarrier(fvm, request, null, id);
        }

        private ContentResult ReadCarrier(FilterViewModel<OrderSettlementHistory> fvm, [DataSourceRequest] DataSourceRequest request, ModelStateDictionary modelState, int id = 0)
        {
            List<CarrierOrderSettlementHistory> data = new List<CarrierOrderSettlementHistory>() { };
            if (fvm.BatchNum != null || fvm.OrderNum != null || fvm.StartDate != null && fvm.EndDate != null)
            {
                string query = "SELECT * FROM fnCarrierSettlementHistory (" +
                        (fvm.BatchNum == null ? "-1" : "ISNULL((SELECT ID FROM tblCarrierSettlementBatch WHERE BatchNum = " + fvm.BatchNum + "), (SELECT TOP 1 ID FROM tblCarrierSettlementBatchDBAudit WHERE BatchNum = " + fvm.BatchNum + "))") + ", " +
                        (fvm.OrderNum == null ? "-1" : "(SELECT ID FROM tblOrder WHERE OrderNum = " + fvm.OrderNum + ")") + ") ";
                if (fvm.StartDate != null && fvm.EndDate != null)
                    query = query + "WHERE CAST(" + fvm.CustomFilter2 + " AS DATE) BETWEEN CAST('" + fvm.StartDate + "' AS DATE) AND CAST('" + fvm.EndDate + "' AS DATE) " +
                        (fvm.CarrierID == null ? "" : ("AND Carrier = (SELECT Name FROM tblCarrier WHERE ID = " + fvm.CarrierID + ")"));

                data = db.Database.SqlQuery<CarrierOrderSettlementHistory>(query).ToList();
            }

            return JsonStringResult.Create(data.ToDataSourceResult(request, modelState));
        }

        public ActionResult ReadDriver(FilterViewModel<OrderSettlementHistory> fvm, [DataSourceRequest] DataSourceRequest request = null, int id = 0)
        {
            return ReadDriver(fvm, request, null, id);
        }

        private ContentResult ReadDriver(FilterViewModel<OrderSettlementHistory> fvm, [DataSourceRequest] DataSourceRequest request, ModelStateDictionary modelState, int id = 0)
        {
            List<DriverOrderSettlementHistory> data = new List<DriverOrderSettlementHistory>() { };
            if (fvm.BatchNum != null || fvm.OrderNum != null || fvm.StartDate != null && fvm.EndDate != null)
            {
                string query = "SELECT * FROM fnDriverSettlementHistory (" + 
                        (fvm.BatchNum == null ? "-1" : "ISNULL((SELECT ID FROM tblDriverSettlementBatch WHERE BatchNum = " + fvm.BatchNum + "), (SELECT TOP 1 ID FROM tblDriverSettlementBatchDBAudit WHERE BatchNum = " + fvm.BatchNum + "))") + ", " + 
                        (fvm.OrderNum == null ? "-1" : "(SELECT ID FROM tblOrder WHERE OrderNum = " + fvm.OrderNum + ")") + ")";
                if (fvm.StartDate != null && fvm.EndDate != null)
                    query = query + "WHERE CAST(" + fvm.CustomFilter2 + " AS DATE) BETWEEN CAST('" + fvm.StartDate + "' AS DATE) AND CAST('" + fvm.EndDate + "' AS DATE) " +
                        (fvm.CarrierID == null ? "" : ("AND Carrier = (SELECT Name FROM tblCarrier WHERE ID = " + fvm.CarrierID + ")")) +
                        (fvm.DriverGroupID == null ? "" : ("AND DriverGroup = (SELECT Name FROM tblDriverGroup WHERE ID = " + fvm.DriverGroupID + ")")) +
                        (fvm.DriverID == null ? "" : ("AND Driver = (SELECT FullName FROM viewDriver WHERE ID = " + fvm.DriverID + ")"));

                data = db.Database.SqlQuery<DriverOrderSettlementHistory>(query).ToList();
            }

            return JsonStringResult.Create(data.ToDataSourceResult(request, modelState));
        }

        public ActionResult ReadShipper(FilterViewModel<OrderSettlementHistory> fvm, [DataSourceRequest] DataSourceRequest request = null, int id = 0)
        {
            return ReadShipper(fvm, request, null, id);
        }

        private ContentResult ReadShipper(FilterViewModel<OrderSettlementHistory> fvm, [DataSourceRequest] DataSourceRequest request, ModelStateDictionary modelState, int id = 0)
        {
            List<ShipperOrderSettlementHistory> data = new List<ShipperOrderSettlementHistory>() { };
            if (fvm.BatchNum != null || fvm.OrderNum != null || fvm.StartDate != null && fvm.EndDate != null)
            {
                string query = "SELECT * FROM fnShipperSettlementHistory (" + 
                        (fvm.BatchNum == null ? "-1" : "ISNULL((SELECT ID FROM tblShipperSettlementBatch WHERE BatchNum = " + fvm.BatchNum + "), (SELECT TOP 1 ID FROM tblShipperSettlementBatchDBAudit WHERE BatchNum = " + fvm.BatchNum + "))") + ", " + 
                        (fvm.OrderNum == null ? "-1" : "(SELECT ID FROM tblOrder WHERE OrderNum = " + fvm.OrderNum + ")") + ")";
                if (fvm.StartDate != null && fvm.EndDate != null)
                    query = query + "WHERE CAST(" + fvm.CustomFilter2 + " AS DATE) BETWEEN CAST('" + fvm.StartDate + "' AS DATE) AND CAST('" + fvm.EndDate + "' AS DATE) " +
                        (fvm.ShipperID == null ? "" : ("AND Shipper = (SELECT Name FROM tblCustomer WHERE ID = " + fvm.ShipperID + ")"));

                data = db.Database.SqlQuery<ShipperOrderSettlementHistory>(query).ToList();
            }

            return JsonStringResult.Create(data.ToDataSourceResult(request, modelState));
        }
    }
}
