﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AlonsIT;

namespace DispatchCrude.Controllers
{
    public class RequireHttpsConditional : RequireHttpsAttribute
    {
        static public bool UseSSL { get { return DBHelper.ToBoolean(AppSettings.GetAppSetting("UseSSL", "false")); } }
        static public int SSL_Port { get { return DBHelper.ToInt32(AppSettings.GetAppSetting("SSL_Port", "443")); } }

        protected override void HandleNonHttpsRequest(AuthorizationContext filterContext)
        {
            if (UseSSL)
            {
                if (!string.Equals(filterContext.HttpContext.Request.HttpMethod, "GET", StringComparison.OrdinalIgnoreCase))
                {
                    throw new InvalidOperationException("The requested resource can only be accessed via SSL.");
                }

                var request = filterContext.HttpContext.Request;
                string url = null;
                int sslPort;

                if ((sslPort = SSL_Port) > 0)
                {
                    url = "https://" + request.Url.Host + request.RawUrl;

                    if (sslPort != 443)
                    {
                        var builder = new UriBuilder(url) {Port = sslPort};
                        url = builder.Uri.ToString();
                    }
                }

                if (sslPort != request.Url.Port)
                {
                    filterContext.Result = new RedirectResult(url);
                }
            }
        }
    }
}