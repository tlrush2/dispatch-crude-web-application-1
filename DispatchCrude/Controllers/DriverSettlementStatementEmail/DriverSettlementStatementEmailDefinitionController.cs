﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DispatchCrude.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System.Collections;

namespace DispatchCrude.Controllers
{
    [Authorize(Roles = "viewDriverSettlementStatementEmailDefinition")]
    public class DriverSettlementStatementEmailDefinitionController : _DBControllerRead
    {
        // This session variable is required for Asyncronous file uploads to work with new records.
        // The file is uploaded here temporarily until the user actually clicks the save button.
        private const string SESSION_WORDFILENAME = "DSS_EMAILBODY_FILENAME"
            , SESSION_WORDFILE = "DSS_EMAILBODY_FILE";

        public ActionResult Index()
        {
            return View();
        }

        /* normally this is the only method that is overridden (defined) in the inheriting controller classes */
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        override protected ContentResult Read([DataSourceRequest] DataSourceRequest request, ModelStateDictionary modelState, int id)
        {
            var data = db.DriverSettlementStatementEmailDefinitions.Where(m => m.ID == id || id == 0)
                            .Include(m => m.PdfExport);
            return ToJsonResult(data, request, modelState, "EmailBodyWordDocument", "WordDocument");
        }

        [HttpPost]
        [Authorize(Roles = "createDriverSettlementStatementEmailDefinition")]
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, DriverSettlementStatementEmailDefinitionBase model)
        {
            return Update(request, model);
        }

        [HttpPost]
        [Authorize(Roles = "editDriverSettlementStatementEmailDefinition")]
        // this method takes the base model to prevent any "child" objects from being validated incorrectly or saved unexpectedly
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, DriverSettlementStatementEmailDefinitionBase model)
        {
            // push the previously provided document info into the model (if present we need to re-validate the model, done below)
            bool resetValidation = false;
            if (resetValidation = Session[SESSION_WORDFILE] != null) model.EmailBodyWordDocument = Session[SESSION_WORDFILE] as byte[];
            if (resetValidation = Session[SESSION_WORDFILENAME] != null) model.EmailBodyWordDocName = Session[SESSION_WORDFILENAME].ToString();
            // reset the ModelState and re-validate (this is required if the document was required)
            if (resetValidation)
            {
                model.Validated = false;
                ModelState.Clear();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    model = IEDTO.CloneToMe(model, typeof(DriverSettlementStatementEmailDefinition));
                    // don't update the Document properties if not provided (will fail validation above if required and not provided)
                    string[] skipParameters = model.EmailBodyWordDocument != null ? null : new string[] { "EmailBodyWordDocument", "EmailBodyWordDocName" };
                    db.AddOrUpdateSave(User.Identity.Name, model, null, skipParameters);

                    //Clear document variable after successful add.  Otherwise the next record added will automatically use this file if the user did not select one
                    Session[SESSION_WORDFILE] = null;
                    Session[SESSION_WORDFILENAME] = null;
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { model }.ToDataSourceResult(request, ModelState));
                }
            }

            return Read(request, ModelState, model.ID);
        }


        // Delete (Deactivate) DriverSettlementStatementEmailDefinition
        [Authorize(Roles = "deactivateDriverSettlementStatementEmailDefinition")]
        public ActionResult Delete([DataSourceRequest] DataSourceRequest request, DriverSettlementStatementEmailDefinitionBase model)
        {
            return DeleteInt(request, model, typeof(DriverSettlementStatementEmailDefinition));
        }

        private bool ValidateUpload(bool isNew, ModelStateDictionary modelState)
        {
            bool ret = Session[SESSION_WORDFILENAME] != null && Session[SESSION_WORDFILE] != null;
            return ret;
        }


        [HttpPost]
        public ActionResult SaveDocument(IEnumerable<HttpPostedFileBase> document)
        {
            // The Name of the Upload component is "files"
            if (document != null)
            {
                foreach (var file in document)
                {
                    Session[SESSION_WORDFILENAME] = Path.GetFileName(file.FileName);
                    Session[SESSION_WORDFILE] = new BinaryReader(file.InputStream).ReadBytes(file.ContentLength);
                    // we only accept a single file so just break
                    break;
                }
            }
            // Return an empty string to signify success
            return Content("");
        }

        public ActionResult ClearDocument()
        {
            Session[SESSION_WORDFILE] = null;
            Session[SESSION_WORDFILENAME] = null;

            return Json(new { status="OK"}, JsonRequestBehavior.AllowGet);
        }

        // Download the Document
        public ActionResult Download(int id)
        {
            DriverSettlementStatementEmailDefinition model = db.DriverSettlementStatementEmailDefinitions.Find(id);
            return File(model.EmailBodyWordDocument, "application/octet-stream", model.EmailBodyWordDocName);
        }

    }
}
