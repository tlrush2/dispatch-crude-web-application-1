﻿using System;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using DispatchCrude.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;

namespace DispatchCrude.Controllers
{
    [Authorize(Roles = "viewProducerMaintenance")]
    public class ProducersController : Controller
    {
        private DispatchCrudeDB db = new DispatchCrudeDB();

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        // GET: Read
        public ActionResult Index(int? id)
        {
            //If a producer has been specified as a starting filter in the url, get it and pass it to the view
            Producer p = db.Producers.Find(id);

            if (p != null)
            {
                ViewBag.ProducerName = p.Name;

                //We don't want the URL filter to stay after we've filtered the grid so this lets us know on the view to get rid of it.
                ViewBag.ClearUrl = true;
            }

            return View();
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest request = null, int id = 0)
        {
            return Read(request, null, id);
        }

        private ContentResult Read([DataSourceRequest] DataSourceRequest request, ModelStateDictionary modelState, int id = 0)
        {
            // .ToList() is required due to filtering on "Active" field, which is NOT MAPPED
            var data = db.Producers.Where(m => m.ID == id || id == 0).ToList();
            var result = data.ToDataSourceResult(request, modelState);            
            return App_Code.JsonStringResult.Create(result);  
        }
        
        // POST: Create
        [HttpPost]
        [Authorize(Roles = "createProducerMaintenance")]
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, Producer producer)
        {            
            if (ModelState.IsValid)
            {
                try
                {
                    // ensure all new records are marked "Active" (otherwise they will be created deleted)
                    producer.Active = true;                    

                    // Save new record to the database
                    db.Producers.Add(producer);
                    db.SaveChanges(User.Identity.Name);  // Create dates & usernames will all be taken care of by this line.
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(null, ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { producer }.ToDataSourceResult(request, ModelState));
                }
            }
            
            return Read(request, ModelState, producer.ID);
        }

        // POST: Update
        [HttpPost]
        [Authorize(Roles = "editProducerMaintenance")]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, Producer producer)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // Get the existing record
                    // This allows us to keep the existing file upload if one is not specified in an edit
                    Producer existing = db.Producers.Find(producer.ID);

                    // Specify all fields that will/can be updated by user input on the popup
                    db.CopyEntityValues(existing, producer);

                    db.SaveChanges(User.Identity.Name);  // Create & Modified dates & usernames will be taken care of by this line.
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(null, ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { producer }.ToDataSourceResult(request, ModelState));
                }
            }

            return Read(request, ModelState, producer.ID);
        }

        // Delete (Deactivate)
        [Authorize(Roles = "deactivateProducerMaintenance")]
        public ActionResult Delete(Producer producer)
        {
            db.Producers.Attach(producer);
            db.Producers.Remove(producer);
            db.SaveChanges(User.Identity.Name);

            return null;
        }
    }
}
