﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.UI;

namespace DispatchCrude.Controllers
{
    public static class ControllerExtensions
    {
        public static ViewResult RazorView(this Controller controller)
        {
            return RazorView(controller, null, null);
        }

        public static ViewResult RazorView(this Controller controller, object model)
        {
            return RazorView(controller, null, model);
        }

        public static ViewResult RazorView(this Controller controller, string viewName)
        {
            return RazorView(controller, viewName, null);
        }

        public static ViewResult RazorView(this Controller controller, string viewName, object model)
        {
            if (model != null)
                controller.ViewData.Model = model;

            controller.ViewBag._ViewName = GetViewName(controller, viewName);

            return new ViewResult
            {
                ViewName = "RazorView",
                ViewData = controller.ViewData,
                TempData = controller.TempData
            };
        }

        static string GetViewName(Controller controller, string viewName)
        {
            return !string.IsNullOrEmpty(viewName)
                ? viewName
                : controller.RouteData.GetRequiredString("action");
        }

        public static string RenderViewToString(Controller controller, string viewName, object model, string layout = null)
        {
            controller.ViewData.Model = model;
            try
            {
                using (StringWriter sw = new StringWriter())
                {
                    if (controller == null) throw new Exception("Controller is null");
                    if (controller.ControllerContext == null) throw new Exception("ControllerContext is null");

                    ViewEngineResult viewResult = ViewEngines.Engines.FindView(controller.ControllerContext, viewName, layout);
                    
                    if (viewResult == null) throw new Exception("viewResult is null with viewName='" + viewName + "'");
                    if (viewResult.View == null) throw new Exception("viewResult.View is null");
                    if (controller.ViewData == null) throw new Exception("Controller.viewData is null");
                    if (controller.TempData == null) throw new Exception("Controller.tempData is null");
                    if (sw == null) throw new Exception("sw is null");
                    
                    ViewContext viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);
                    viewResult.View.Render(viewContext, sw);

                    return sw.GetStringBuilder().ToString();
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public static string RenderPartialViewToString(Controller controller, string viewName, object model)
        {
            controller.ViewData.Model = model;
            try
            {
                using (StringWriter sw = new StringWriter())
                {
                    ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
                    ViewContext viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);
                    viewResult.View.Render(viewContext, sw);

                    return sw.GetStringBuilder().ToString();
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        // This method helps to get the error information from the MVC "ModelState".
        // We can not directly send the ModelState to the client in Json. The "ModelState"
        // object has some circular reference that prevents it to be serialized to Json.
        // http://craftycodeblog.com/2010/05/15/asp-net-mvc-render-partial-view-to-string/
        // Credit: Kevin Craft
        // Pulled from: https://www.codeproject.com/articles/216439/data-validation-using-annotations-for-jquery-ajax
        /// <summary>
        /// NOTE: when calling this from a controller, you actually need to pass the variable "ModelState" to it, not a ModelStateDictionary
        /// <para /> EXAMPLE:  Errors = ControllerExtensions.GetErrorsFromModelState(ModelState)
        /// <para /> (Why? see https://forums.asp.net/t/2003589.aspx?Pass+ModelState+from+Controller+to+Static+Method)
        /// </summary>
        /// <returns></returns>
        public static Dictionary<string, object> GetErrorsFromModelState(ModelStateDictionary modelState)
        {
            var errors = new Dictionary<string, object>();            

            foreach (var key in modelState.Keys)
            {
                // Only send the errors to the client.
                if (modelState[key].Errors.Count > 0)
                {
                    errors[key] = modelState[key].Errors;
                }
            }

            return errors;
        }

        /* http://www.geekytidbits.com/share-razor-partial-view-between-webforms-and-mvc/ */
        public class WebFormDummyController : Controller { }
        public static void RenderPartial(string partialName, object model)
        {
            //get a wrapper for the legacy WebForm context
            var httpCtx = new HttpContextWrapper(System.Web.HttpContext.Current);

            //create a mock route that points to the empty controller
            var rt = new RouteData();
            rt.Values.Add("controller", "WebFormShimController");

            //create a controller context for the route and http context
            var ctx = new ControllerContext(
                new RequestContext(httpCtx, rt), new WebFormDummyController());

            //find the partial view using the viewengine
            var view = ViewEngines.Engines.FindPartialView(ctx, partialName).View;

            //create a view context and assign the model
            var vctx = new ViewContext(ctx, view,
                new ViewDataDictionary { Model = model },
                new TempDataDictionary(), httpCtx.Response.Output);

            //render the partial view
            view.Render(vctx, System.Web.HttpContext.Current.Response.Output);
        }
    }
}