﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Data;
using DispatchCrude.Core;
using DispatchCrude.Models;
using AlonsIT;

namespace DispatchCrude.Controllers
{
    [Authorize(Roles = "viewOrderAudit")]
    public class OrderAuditController : Controller
    {
        //
        // GET: /OrderAudit/

        [Authorize(Roles = "viewOrderAudit")]
        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.Title = "Order Audit";
            return View(AuditData());
        }

        [Authorize(Roles = "viewOrderAudit")]
        [HttpPost]
        public ActionResult Read([DataSourceRequest] DataSourceRequest request)
        {
            DataTable auditData = AuditData();

            if (request.Aggregates.Any())
            {
                request.Aggregates.Each(agg =>
                {
                    agg.Aggregates.Each(a =>
                    {
                        a.MemberType = auditData.Columns[agg.Member].DataType;
                    });
                });
            }

            return Json(auditData.ToDataSourceResult(request));
        }

        [Authorize(Roles = "viewOrderAudit")]
        [HttpPost]
        public ContentResult GoBack(int id)
        {
            using (SSDB db = new SSDB())
            {
                db.ExecuteSql(
                    // for non-rejected loads, simply mark the order as Finalized (not completed) so it will be displayed in the MVC pages
                    "UPDATE tblOrder SET DeliverPrintStatusID={0} WHERE ID={2};"
                    // for Rejected orders, we should also revert the status back to PICKED UP, and Finalized (not completed)
                    + " UPDATE tblOrder SET StatusID={1}, PickupPrintStatusID={0} WHERE ID={2} AND Rejected = 1;"
                    , (int)PrintStatus.STATUS.Finalized
                    , (int)OrderStatus.STATUS.PickedUp
                    , id);
            }
            return Content("success");
        }

        [Authorize(Roles = "viewOrderAudit")]
        [HttpPost]
        public ContentResult Done(int id)
        {
            // mark the record as Audited (Completed)
            using (SSDB db = new SSDB())
            {
                db.ExecuteSql("UPDATE tblOrder SET StatusID={0}, LastChangeDateUTC=getutcdate(), LastChangedByUser={1} WHERE ID={2}"
                    , (int)OrderStatus.STATUS.Audited
                    , DBHelper.QuoteStr(UserSupport.UserName)
                    , id);
            }
            return Content("success");
        }

        private System.Data.DataTable AuditData()
        {
            using (SSDB ssdb = new SSDB())
            {
                return ssdb.GetPopulatedDataTable(
                    "SELECT ID, OrderNum, Rejected, Tickets, DispatchConfirmNum, Errors, Customer, Carrier, OriginDriver"
                    + ", Origin, OriginDistance, OriginGrossUnits, OriginNetUnits, Destination, DestDistance "
                    + "FROM fnOrders_AllTickets_Audit(-1, -1, -1, NULL, NULL) "
                    + "ORDER BY DueDate, Origin, Destination");
            }
        }

    }
}
