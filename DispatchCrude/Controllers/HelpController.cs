﻿using System.Web.Mvc;

namespace DispatchCrude.Controllers
{
    public class HelpController : Controller
    {
        //
        // GET: /Help/
        [Authorize(Roles = "viewHelp")]
        public ActionResult Index()
        {
            return View();
        }

    }
}
