﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DispatchCrude.Models;
using DispatchCrude.Core;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using AlonsIT;

namespace DispatchCrude.Controllers
{
    [Authorize(Roles = "viewProductGroups")]
    public class ProductGroupsController : Controller
    {        
        private DispatchCrudeDB db = new DispatchCrudeDB();

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        // GET: Product Groups
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest request = null, int id = 0)
        {
            return Read(request, null, id);
        }

        private ContentResult Read([DataSourceRequest] DataSourceRequest request, ModelStateDictionary modelState, int id = 0)
        {
            // .ToList() is required due to filtering on "Active" field, which is NOT MAPPED
            var data = db.ProductGroups.Where(p => p.ID == id || id == 0).ToList();

            var result = data.ToDataSourceResult(request, modelState);
            
            return App_Code.JsonStringResult.Create(result);
        }

        // POST: Create Product Groups
        //[HttpPost]
        [Authorize(Roles = "createProductGroups")]
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, ProductGroup productGroup)
        {   
            if (ModelState.IsValid)
            {
                try
                {
                    // ensure all new product groups are marked "Active" (otherwise they will be created deleted)
                    productGroup.Active = true;

                    // Save new record to the database
                    db.ProductGroups.Add(productGroup);
                    db.SaveChanges(User.Identity.Name);  // Create dates & usernames will all be taken care of by this line.
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(null, ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { productGroup }.ToDataSourceResult(request, ModelState));
                }
            }


            return Read(request, ModelState, productGroup.ID);
        }

        // POST: Update Product Groups
        [HttpPost]
        [Authorize(Roles = "editProductGroups")]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, ProductGroup productGroup)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // Get the existing record
                    ProductGroup existing = db.ProductGroups.Find(productGroup.ID);

                    // Specify all fields that will/can be updated by user input on the popup
                    db.CopyEntityValues(existing, productGroup);

                    db.SaveChanges(User.Identity.Name);  // Create & Modified dates & usernames will be taken care of by this line.
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(null, ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { productGroup }.ToDataSourceResult(request, ModelState));
                }
            }

            return Read(request, ModelState, productGroup.ID);
        }

        
        // Delete (Deactivate)  Product Groups
        [Authorize(Roles = "deactivateProductGroups")]
        public ActionResult Delete(ProductGroup productGroup)
        {
            db.ProductGroups.Attach(productGroup);
            db.ProductGroups.Remove(productGroup);
            db.SaveChanges(User.Identity.Name);

            return null;
        }
    }
}
