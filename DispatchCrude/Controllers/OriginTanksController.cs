﻿using DispatchCrude.Models;
using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Collections.Generic;
using AlonsIT;
using System.IO;
using Syncfusion.XlsIO;

namespace DispatchCrude.Controllers
{
    [Authorize(Roles = "viewOriginTanks")]
    public class OriginTanksController : _DBControllerRead
    {
        /***************************************
         *           ORIGIN TANKS
         * ************************************/       

        // GET: Read
        public ActionResult Index(int? OriginID)
        {
            //If an origin has been specified as a starting filter, get it's name and pass itto the view
            Origin o = db.Origins.Find(OriginID);

            if (o != null)
            {
                ViewBag.OriginName = o.Name;
                ViewBag.OriginID = o.ID;
            
                //We don't want the URL filter to stay after we've filtered the grid so this lets us know on the view to get rid of it.
                ViewBag.ClearUrl = true;
            }

            return View();
        }

        override protected ContentResult Read([DataSourceRequest] DataSourceRequest request, ModelStateDictionary modelState, int id)
        {
            var data = db.OriginTanks.Where(m => m.ID == id || id == 0)
                                .Include(m => m.Origin)
                                .Include(m => m.OriginTankDefinition);

            return ToJsonResult(data, request, modelState);
        }

        // POST: Create
        [HttpPost]
        [Authorize(Roles = "createOriginTanks")]
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, OriginTank tank)
        {
            return Update(request, tank);
        }

        

        // POST: Update
        [HttpPost]
        [Authorize(Roles = "editOriginTanks")]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, OriginTank tank)
        {
            //*** NOTE: Special Validation for the new TankDefinitionID field (this fires for new tanks and edited tanks).
            //*** This is done here because validation via the model did not work properly any way we tried it.
            //*** This can be moved back to the model once all our customers' databases have this value filled.
            switch (tank.ID)
            {
                case 0:  //New Tank
                    if (tank.TankDefinitionID == 0)
                        ModelState.AddModelError("TankDefinitionID", "The Tank Definition field is required.");
                    break;
                default:  //Edited Tank
                    if (tank.TankDefinitionID == 0)
                        ModelState.AddModelError("TankDefinitionID", "The Tank Definition field is required.");
                    break;
            }            

            string TankInfoPartialViewHTML = null;
            
            //Determine whether or not the new information has validation errors       
            var valid = TryUpdateModel(tank);
            if (valid)
            {
                tank = (OriginTank)db.AddOrUpdateSave(User.Identity.Name, tank);
                // Get the html content of the Display partial view since we're done editing at this point
                TankInfoPartialViewHTML = ControllerExtensions.RenderPartialViewToString(this, "_TankInfo_Display", tank);
            }

            //Return the errors (if any) and the html of the display partial view (if set).
            // Errors will be returned and then rendered in the Edit partial view.
            // On success, the Display partial view will be returned.
            return Json(new {
                tankID = tank.ID,
                Valid = valid,
                Errors = ControllerExtensions.GetErrorsFromModelState(ModelState),
                result = TankInfoPartialViewHTML
            });
        }


        // Delete (Deactivate)
        [Authorize(Roles = "deactivateOriginTanks")]
        public ActionResult Delete([DataSourceRequest] DataSourceRequest request, OriginTank tank)
        {
            return DeleteInt(request, tank);
        }

        public ActionResult ExportFullStrapTable(int TankID)
        {
            OriginTank tank = db.OriginTanks.Find(TankID);
            Origin origin = db.Origins.Find(tank.OriginID);

            return File(FullStrapTable(tank.ID, tank.TankNum)
                        , "Application/excel"
                        , string.Format("{0}_{1}_Strapping_{2:yyyyMMdd}_Full.xlsx"
                                    , origin.Name
                                    , tank.TankNum
                                    , DateTime.Now.Date));
        }

        //Exports a fully calculated strapping table for the specified tank
        public Stream FullStrapTable(int TankID, string TankNum)
        {
            MemoryStream ret = null;

            using (SSDB ssdb = new SSDB())
            {
                DataTable dt = ssdb.GetPopulatedDataTable("SELECT Origin, TankNum [Tank ID], Feet, Inches, Q, BPQ, IsMinimum [Is Min?], IsMaximum [Is Max?], Barrels, CreateDateUTC"
                                                          + ", CreatedByUser, LastChangeDateUTC, LastChangedByUser FROM dbo.fnOriginTankStrapping({0}) "
                                                          + "ORDER BY Feet, Inches, Q", TankID);

                using (ExcelEngine excelEngine = new ExcelEngine())
                {
                    IApplication excelApp = null;
                    IWorkbook wb = null;
                    IWorksheet ws = null;

                    try
                    {
                        excelApp = excelEngine.Excel;
                        wb = excelApp.Workbooks.Add(ExcelVersion.Excel2010);

                        ws = wb.Worksheets[0];
                        ws.Name = "Tank " + TankNum;                      

                        AddDataTableToWorksheet(ws, dt, 1, 1, true);

                        IStyle headerStyle = wb.Styles.Add("HeaderStyle");
                        headerStyle.Color = System.Drawing.Color.Gray;
                        headerStyle.Font.Color = ExcelKnownColors.White;
                        headerStyle.Font.FontName = "Calibri";
                        headerStyle.Font.Size = 11;
                        ws.Range[1, 1, 1, dt.Columns.Count].CellStyle = headerStyle;

                        ws.Range[2, 1].FreezePanes();
                        ws.AutoFilters.FilterRange = ws.Range[1, 1, 1, dt.Columns.Count];
                        ws.Range[1, 1, Math.Min(dt.Rows.Count, 200), dt.Columns.Count].AutofitColumns();                        
                    }
                    catch
                    { /*Ignore errors*/ }

                    ret = new MemoryStream();
                    wb.SaveAs(ret, ExcelSaveType.SaveAsXLS);
                    wb.Close();
                }
            }
            ret.Position = 0;  //Without this, Excel reports a corrupt file
            return ret;
        }

        private void AddDataTableToWorksheet(IWorksheet sheet, DataTable dt, int startRow = 1, int startCol = 1, bool addHeader = true)
        {
            if (addHeader)
            {
                int col = startCol;
                foreach (DataColumn dc in dt.Columns)
                {
                    sheet.Range[startRow, col++].Value = dc.Caption;
                }
                startRow++;
            }
            foreach (DataRow dr in dt.Rows)
            {
                int col = startCol;
                foreach (DataColumn dc in dt.Columns)
                {
                    sheet.Range[startRow, col++].Value = dr[dc].ToString();
                }
                startRow++;
            }
        }

        /***************************************
         *       ORIGIN TANK STRAPPINGS
         * ************************************/

        [Authorize(Roles = "viewOriginTanks")]
        public ActionResult Straps_Read([DataSourceRequest] DataSourceRequest request, int originTankId = 0)
        {
            List<OriginTankStrapping> result = new List<OriginTankStrapping>();

            result = db.OriginTankStrappings
                        .Where(m => m.OriginTankID == originTankId)
                        .OrderByDescending(m => m.Feet)
                        .ThenByDescending(m => m.Inches)
                        .ThenByDescending(m => m.Q)
                        .ToList();            

            return Json(result.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        
                
        [Authorize(Roles = "editOriginTanks")]
        public ActionResult Straps_UpdateCreateDelete(int tankId,
                                                      int tankDefinitionId,
                                                      List<OriginTankStrapping> updatedStraps,
                                                      List<OriginTankStrapping> newStraps,
                                                      List<OriginTankStrapping> deletedStraps)
        {
            bool valid = true;
            string validationErrors = "<ul class='NullValidator'>";

            //Validate strap records before trying to add/update/delete
            List<string> validationResult = ValidateStraps(updatedStraps, newStraps, deletedStraps, tankId, tankDefinitionId);
            if (validationResult.Count > 0)
            {
                foreach (var error in validationResult)
                {
                    validationErrors += "<li>" + error + "</li>";
                }

                validationErrors += "</ul>";
                valid = false;
            }

            if (valid) //update strap records
            { 
                //update changed strap records
                if (updatedStraps != null && updatedStraps.Count > 0)
                {
                    for (int i = 0; i < updatedStraps.Count; i++)
                    {                    
                        OriginTankStrapping existing = db.OriginTankStrappings.Find(updatedStraps[i].ID);

                        if (existing != null)
                        {
                            //Update this strap record's information
                            db.CopyEntityValues(existing, updatedStraps[i]);                        
                        }
                    }
                }

                //insert new strap records
            
                if (newStraps != null && newStraps.Count > 0)
                {
                    for (int i = 0; i < newStraps.Count; i++)
                    {
                        OriginTankStrapping strap = newStraps[i];
                        strap.OriginTankID = tankId; //Make sure the origin tank id is set to the current tank before trying to insert new strap records
                        db.OriginTankStrappings.Add(strap);
                    }

                }

                //delete removed strap records
                if (deletedStraps != null && deletedStraps.Count > 0)
                {
                    for (int i = 0; i < deletedStraps.Count; i++)
                    {
                        OriginTankStrapping target = db.OriginTankStrappings.Find(deletedStraps[i].ID);                    

                        if (target != null)
                        {
                            db.OriginTankStrappings.Attach(target);
                            db.OriginTankStrappings.Remove(target);
                        }
                    }
                }

                //save all the changes to the strapping table
                db.SaveChanges(User.Identity.Name);
            }

            //Return the errors (if any) and the html of the display partial view (if set).
            // Errors will be returned and then rendered in the Edit partial view.
            // On success, the Display partial view will be returned.
            return Json(new
            {                
                Valid = valid,
                Errors = validationErrors,
                TankID = tankId  //This is sent back specifically for SaveNewTankStrapGrid()
            });
        }

        public ActionResult GetTankDefinition(int definitionID)
        {
            OriginTankDefinition def = db.OriginTankDefinitions.Find(definitionID);

            return Json(new
            {
                CustomBottomRecord = (def.BottomFeet == 0 && def.BottomInches == 0 && def.BottomQ == 0) ? false : true, //If the bottom records are not all set to zero the record contains a custom bottom record
                BF = def.BottomFeet,
                BI = def.BottomInches,
                BQ = def.BottomQ,
                TF = def.TopFeet,
                TI = def.TopInches,
                TQ = def.TopQ,
                BPQ = def.TopBPQ
            }, JsonRequestBehavior.AllowGet);
        }

        public List<string> ValidateStraps(List<OriginTankStrapping> updatedStraps,
                                           List<OriginTankStrapping> newStraps,
                                           List<OriginTankStrapping> deletedStraps,
                                           int tankId, 
                                           int tankDefintionId)
        {
            OriginTankDefinition def = db.OriginTankDefinitions.Find(tankDefintionId);
            List<OriginTankStrapping> validationList = db.OriginTankStrappings.Where(m => m.OriginTankID == tankId).ToList();  // Add existing straps to the validation list            
            OriginTankStrapping highestStrap = new OriginTankStrapping() { Feet = 0, Inches = 0, Q = 0 };
            List<string> errors = new List<string>();
            bool hasZeroStrap = false;
            bool hasMinStrap = false;
            bool hasMaxStrap = false;
            bool hasMinAndMax = false;
            //bool hasZeroStrapWithBPQ = false;
            bool hasNonZeroStrapWithZeroBPQ = false;
            int minCount = 0;
            int maxCount = 0;

            // Remove deleted straps from the list since they do not require validation
            if (deletedStraps != null && deletedStraps.Count > 0)
            {
                foreach (OriginTankStrapping strap in deletedStraps)
                {
                    if (validationList.Exists(x => x.ID == strap.ID))
                        validationList.Remove(validationList.Find(x => x.ID == strap.ID));     
                }
            }

            // Replace any updated straps with the newly submitted information
            if (updatedStraps != null && updatedStraps.Count > 0)
            {
                foreach (OriginTankStrapping strap in updatedStraps)
                {
                    if (validationList.Exists(x => x.ID == strap.ID))
                        validationList[validationList.IndexOf(validationList.Find(x => x.ID == strap.ID))] = strap;
                }
            }

            // Add new straps to the validation list
            if (newStraps != null && newStraps.Count > 0)
            {
                foreach (OriginTankStrapping strap in newStraps)
                {
                    validationList.Add(strap);
                }
            }

            //*** BEGIN VALIDATION CHECKS ***
            foreach (OriginTankStrapping strap in validationList)
            {
                //Find the largest strap in this list
                if ((strap.Feet * 48 + strap.Inches * 4 + strap.Q) > (highestStrap.Feet * 48 + highestStrap.Inches * 4 + highestStrap.Q))
                    highestStrap = strap;                

                //IsMin and IsMax checks
                if (strap.IsMinimum || strap.IsMaximum)
                {
                    if (strap.IsMinimum && strap.IsMaximum)
                        hasMinAndMax = true;

                    if (strap.IsMinimum)
                    {
                        hasMinStrap = true;
                        minCount += 1;
                    }

                    if (strap.IsMaximum)
                    {
                        hasMaxStrap = true;
                        maxCount += 1;
                    }
                }

                //Zero strap check
                if (strap.Feet == 0 && strap.Inches == 0 && strap.Q == 0)
                {
                    hasZeroStrap = true;

                    //if (strap.BPQ > 0)
                    //    hasZeroStrapWithBPQ = true;
                }
                else if (strap.BPQ == 0)
                {
                    //Non-zero straps must have non-zero bpq
                    hasNonZeroStrapWithZeroBPQ = true;
                }
            }

            //Now that we have the largest strap in the list, make sure it does not exceed the hight of the selected tank definition
            //            NOTE: 12/21/16 - This valdiation check was deemed annoying and not very useful and was removed before publish
            //if ((highestStrap.Feet * 48 + highestStrap.Inches * 4 + highestStrap.Q) > (def.TopFeet * 48 + def.TopInches * 4 + def.TopQ))
            //    errors.Add("The highest strap exceeds the selected tank definition's height (" + def.TopFeet + "ft " + def.TopInches + "in " + def.TopQ + "Q)");

            if (!highestStrap.IsMaximum)
                errors.Add("The highest strap is not set as maximum");
            
            if (hasMinAndMax)
                errors.Add("A single strap cannot be both minimum and maximum");

            if (minCount > 1)
                errors.Add("Only one strap can be marked as minimum");

            if (maxCount > 1)
                errors.Add("Only one strap can be marked as maximum");

            if (!hasMinStrap)
                errors.Add("Missing minimum strap");

            if (!hasMaxStrap)
                errors.Add("Missing maximum strap");

            if (!hasZeroStrap)
                errors.Add("Missing zero strap");

            //if (hasZeroStrapWithBPQ)
            //    errors.Add("Zero strap must have BPQ value of zero");

            if (hasNonZeroStrapWithZeroBPQ)
                errors.Add("Non-zero straps must have a BPQ value higher than zero");

            return errors;
        }


        /***************************************
         *           PARTIAL VIEWS
         * ************************************/

        //Partial View (Display tank info)        
        [Authorize(Roles = "viewOriginTanks")]
        public ActionResult TankInfo_Display(int tankID)
        {
            if (tankID != 0)
            {
                OriginTank ot = db.OriginTanks
                                .Where(m => m.ID == tankID)
                                .Include(m => m.Origin)
                                .Include(m => m.OriginTankStrappings)
                                .FirstOrDefault();
                return PartialView("_TankInfo_Display", ot);
            }
            return null;
        }

        //Partial View (Edit tank info)
        [Authorize(Roles = "createOriginTanks, editOriginTanks")]
        public ActionResult TankInfo_Edit(int tankID, int DefaultOriginID)
        {
            // don't let a user without "createOriginTanks" create a new Tank
            if (tankID == 0 && !Core.UserSupport.IsInRole("createOriginTanks"))
                return null;

            // don't let a user without "editOriginTanks" edit an existing Tank
            if (tankID != 0 && !Core.UserSupport.IsInRole("editOriginTanks"))
                return null;

            //find the tank the user clicked on
            OriginTank ot = tankID == 0 ? new OriginTank() 
                : db.OriginTanks
                    .Where(m => m.ID == tankID)
                    .Include(m => m.Origin)
                    .Include(m => m.OriginTankStrappings)
                    .FirstOrDefault();

            //Create available origins list and the available tank definitions list for the type and find dropdowns
            List<SelectListItem> originList = new List<SelectListItem>();
            List<SelectListItem> definitionList = new List<SelectListItem>();

            //Load the lists
            GetDDLList(originList, 1);
            GetDDLList(definitionList, 2);


            //If the user got here from the Origins page (clicking on the "# Tanks" link in the origins page) set the default 
            //origin for a new tank to that origin so they can start adding tanks to "that" origin
            if (tankID == 0 && DefaultOriginID != 0)                
                ot.OriginID = DefaultOriginID;

            //set the origins and tank definitions lists
            ViewBag.AvailableOrigins = new SelectList(originList, "Value", "Text", ot.OriginID);
            ViewBag.AvailableTankDefinitions = new SelectList(definitionList, "Value", "Text", ot.TankDefinitionID);

            return PartialView("_TankInfo_Edit", ot);
        }

        /// <summary>
        /// Return a list based upon the type chosen
        /// </summary>
        /// <param name="List">The list you want to fill</param>
        /// <param name="Type">1 = Origins, 2 = Origin Tank Definitions</param>
        /// <returns></returns>
        public List<SelectListItem> GetDDLList(List<SelectListItem> List, int Type)
        {
            switch(Type)
            {
                case 1:
                    var origins = db.Origins
                            .Where(m => m.DeleteDateUTC == null)
                            .OrderBy(m => m.Name).ToList();

                    foreach (Origin origin in origins)
                    {
                        // add origin to the list
                        List.Add(new SelectListItem()
                        {
                            Value = origin.ID.ToString(),
                            Text = origin.Name
                        });
                    }
                    break;
                case 2:
                    var definitions = db.OriginTankDefinitions
                                .Where(m => m.DeleteDateUTC == null)
                                .OrderBy(m => m.Name).ToList();

                    foreach (OriginTankDefinition definition in definitions)
                    {
                        // add tank definition to the list
                        List.Add(new SelectListItem()
                        {
                            Value = definition.ID.ToString(),
                            Text = definition.Name
                        });
                    }
                    break;
                default:
                    break;
            }

            return List;
        }
    }
}
