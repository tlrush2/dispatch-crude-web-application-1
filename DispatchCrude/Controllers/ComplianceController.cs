﻿using System;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using DispatchCrude.Models;
using DispatchCrude.ViewModels;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Collections.Generic;
using System.Web;
using System.IO;
using System.Data.Entity;
using DispatchCrude.Core;
using DispatchCrude.App_Code;

namespace DispatchCrude.Controllers
{
    [Authorize(Roles = "viewComplianceReport")]
    public class ComplianceController : _DBController
    {
        public static List<SelectListItem> categories = new List<SelectListItem>()
                {
                    new SelectListItem() { Value = "Carrier" },
                    new SelectListItem() { Value = "Driver" },
                    new SelectListItem() { Value = "Truck" },
                    new SelectListItem() { Value = "Trailer" }
                };


        public ActionResult Index(FilterViewModel<ComplianceViewModel> fvm)
        {
            // Filter based on my carrier/driver
            int myCarrierID = Converter.ToInt32(Profile.GetPropertyValue("CarrierID"));
            int myDriverID = Converter.ToInt32(Profile.GetPropertyValue("DriverID"));

            ViewBag.CustomFilter = new SelectList(categories, "Value", "Value", fvm.CustomFilter);
            ViewBag.CarrierID = new SelectList(db.Carriers.Where(c => (myCarrierID == -1 || c.ID == myCarrierID) && c.DeleteDateUTC == null).OrderBy(c => c.Name), "ID", "Name", fvm.CarrierID);
            ViewBag.DriverID = new SelectList(db.Drivers.Where(d => (myCarrierID == -1 && (fvm.CarrierID == null || d.CarrierID == fvm.CarrierID) || (myDriverID == -1 && d.CarrierID == myCarrierID) || myDriverID == d.ID) && d.DeleteDateUTC == null).OrderBy(d => d.FirstName).ThenBy(d => d.LastName), "ID", "FullName", fvm.DriverID);
            ViewBag.TruckID = new SelectList(db.Trucks.Where(t => (myCarrierID == -1 && (fvm.CarrierID == null || t.CarrierID == fvm.CarrierID) || t.CarrierID == myCarrierID) && t.DeleteDateUTC == null).OrderBy(t => t.IDNumber), "ID", "IDNumber", fvm.TrailerID);
            ViewBag.TrailerID = new SelectList(db.Trailers.Where(t => (myCarrierID == -1 && (fvm.CarrierID == null || t.CarrierID == fvm.CarrierID) || t.CarrierID == myCarrierID) && t.DeleteDateUTC == null).OrderBy(t => t.IDNumber), "ID", "IDNumber", fvm.TrailerID);


            return View(fvm);
        }

        public ActionResult Read(FilterViewModel<ComplianceViewModel> fvm, [DataSourceRequest] DataSourceRequest request = null, int id = 0)
        {
            return Read(fvm, request, null, id);
        }

        private ContentResult Read(FilterViewModel<ComplianceViewModel> fvm, [DataSourceRequest] DataSourceRequest request, ModelStateDictionary modelState, int id = 0)
        {
            // Filter based on my carrier/driver
            int myCarrierID = Converter.ToInt32(Profile.GetPropertyValue("CarrierID"));
            int myDriverID = Converter.ToInt32(Profile.GetPropertyValue("DriverID"));

            int? CarrierID = (myCarrierID != -1) ? myCarrierID : fvm.CarrierID; // hardcode carrier if profile not all
            int? DriverID = (myDriverID != 0) ? myDriverID : fvm.DriverID; // hardcode driver if profile not all


            // Get compliance data
            string sql = string.Format(@"
 	            DECLARE @Category VARCHAR(20) = '{0}';
 	            DECLARE @CarrierID INT = {1};
                DECLARE @DriverID INT = {2};
 	            DECLARE @TruckID INT = {3};
                DECLARE @TrailerID INT = {4};
                SELECT * FROM dbo.fnComplianceReport(DEFAULT, DEFAULT)
                    WHERE (@Category = '' OR @Category = Category)
                    AND (@CarrierID = -1 OR @CarrierID = CarrierID)
                    AND (@DriverID = -1 OR Category = 'Driver' AND @DriverID = ItemID)
                    AND (@TruckID = -1 OR Category = 'Truck' AND @TruckID = ItemID)
                    AND (@TrailerID = -1 OR Category = 'Trailer' AND @TrailerID = ItemID)", 
                fvm.CustomFilter,
                CarrierID ?? -1, 
                DriverID ?? -1,
                fvm.TruckID ?? -1,
                fvm.TrailerID ?? -1);

            var results = db.Database.SqlQuery<ComplianceViewModel>(sql).AsQueryable().ToList();

            return JsonStringResult.Create(results.ToDataSourceResult(request, modelState));
        }
    }
}
