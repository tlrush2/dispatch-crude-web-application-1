﻿using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using DispatchCrude.Models;
using DispatchCrude.Extensions;

namespace DispatchCrude.Controllers
{
    [Authorize(Roles = "viewTerminals")]
    public class TerminalsController : _DBControllerRead
    {                      
        [HttpGet]        
        public ActionResult Index(int? id)
        {
            //If a specific record has been passed as a starting filter in the url, get it and pass it to the view for filtering in the grid
            Terminal t = db.Terminals.Find(id);

            if (t != null)
            {
                ViewBag.TerminalName = t.Name;

                //We don't want the URL filter to stay after we've filtered the grid so this lets us know on the view to get rid of it.
                ViewBag.ClearUrl = true;
            }

            return View();
        }

        // Read
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        override protected ContentResult Read([DataSourceRequest] DataSourceRequest request, ModelStateDictionary modelState, int id = 0)
        {
            var data = db.Terminals
                            .Include(m => m.State)
                            .Where(m => m.ID == id || id == 0);
            
            return ToJsonResult(data, request, modelState);
        }

        // Create
        [HttpPost]
        [Authorize(Roles = "createTerminals")]
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, Terminal terminal)
        {
            // Create functionality is now taken care of in the update function as of Kevin's 2017 update to how MVC controllers are written
            return Update(request, terminal);  
        }

        // Update
        [HttpPost]
        [Authorize(Roles = "editTerminals")]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, Terminal terminal)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    //Ensure non-numeric characters from Masked textboxes do not make it to the database
                    terminal.OfficePhone = NumericUtils.RemoveNonNumeric(terminal.OfficePhone);

                    // Create new record or update existing record
                    db.AddOrUpdateSave(User.Identity.Name, terminal);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(null, ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { terminal }.ToDataSourceResult(request, ModelState));
                }
            }

            return Read(request, ModelState, terminal.ID);
        }

        // Delete (Deactivate)
        [HttpPost]
        [Authorize(Roles = "deactivateTerminals")]
        public ActionResult Delete([DataSourceRequest] DataSourceRequest request, Terminal terminal)
        {
            // Deactivate the record
            return DeleteInt(request, terminal);
        }        
    }
}
