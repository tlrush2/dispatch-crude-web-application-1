﻿using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using DispatchCrude.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;

namespace DispatchCrude.Controllers
{
    [Authorize(Roles = "viewDriverEquipment")]
    public class DriverEquipmentController : Controller
    {
        private DispatchCrudeDB db = new DispatchCrudeDB();

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        // GET: Product Groups
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest request = null, int id = 0)
        {
            return Read(request, null, id);
        }

        private ContentResult Read([DataSourceRequest] DataSourceRequest request, ModelStateDictionary modelState, int id = 0)
        {
            // .ToList() is required due to filtering on "Active" field, which is NOT MAPPED
            var data = db.DriverEquipments                            
                            .Where(m => m.ID == id || id == 0)
                            .Include(m => m.DriverEquipmentManufacturer)
                            .Include(m => m.DriverEquipmentType)
                            .ToList();

            var result = data.ToDataSourceResult(request, modelState);

            return App_Code.JsonStringResult.Create(result);
        }

        // POST: Create 
        //[HttpPost]
        [Authorize(Roles = "createDriverEquipment")]
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, DriverEquipment driverEquipment)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // ensure all new records are marked "Active" (otherwise they will be created deleted)
                    driverEquipment.Active = true;

                    // Save new record to the database
                    db.DriverEquipments.Add(driverEquipment);
                    db.SaveChanges(User.Identity.Name);  // Create dates & usernames will all be taken care of by this line.
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(null, ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { driverEquipment }.ToDataSourceResult(request, ModelState));
                }
            }

            return Read(request, ModelState, driverEquipment.ID);
        }

        // POST: Update
        [HttpPost]
        [Authorize(Roles = "editDriverEquipment")]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, DriverEquipment driverEquipment)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // Get the existing record
                    DriverEquipment existing = db.DriverEquipments.Find(driverEquipment.ID);

                    // Specify all fields that will/can be updated by user input on the popup
                    db.CopyEntityValues(existing, driverEquipment);

                    db.SaveChanges(User.Identity.Name);  // Create & Modified dates & usernames will be taken care of by this line.
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(null, ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { driverEquipment }.ToDataSourceResult(request, ModelState));
                }
            }

            return Read(request, ModelState, driverEquipment.ID);
        }


        // Delete (Deactivate)
        [Authorize(Roles = "deactivateDriverEquipment")]
        public ActionResult Delete(DriverEquipment driverEquipment)
        {
            db.DriverEquipments.Attach(driverEquipment);
            db.DriverEquipments.Remove(driverEquipment);
            db.SaveChanges(User.Identity.Name);

            return null;
        }
    }
}
