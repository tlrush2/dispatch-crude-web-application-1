﻿using System;
using System.Web.Mvc;
using System.Web.Security;
using DispatchCrude.Models;
using AlonsIT;
using DispatchCrude.App_Code;
using DispatchCrude.Core;

namespace DispatchCrude.Controllers
{

    //[RequireHttps(Order = 1)]
    [Authorize]
    public class AccountController : Controller
    {
        //
        // GET: /Account/Index
        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /Account/Login
        [RequireHttpsConditional]
        [AllowAnonymous]
        public ActionResult Login()
        {
            // if an ADMIN SWITCH USER caused the Login page to be displayed, just redirect to the home page
            if (!string.IsNullOrEmpty(DBHelper.ToString(Session[DispatchCrudeDB.SESSION_ADMIN_SWITCHED_USER])))
                // TODO: this could also be a "No User Access" page instead
                return RedirectToAction("Index", "Home");
            else
                return View(new LoginModel());
        }

        //
        // POST: /Account/Login

        [AllowAnonymous]
        [HttpPost, ValidateInput(false)]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                if (model.Terms)
                {
                    if (Membership.ValidateUser(model.UserName, model.Password))
                    {
                        // ensure whenever a login takes place we remove the session variable for ADMIN SWITCH USER activity
                        Session.Remove(DispatchCrudeDB.SESSION_ADMIN_SWITCHED_USER);
                        FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe);
                        if (Url.IsLocalUrl(returnUrl))
                        {
                            return Redirect(returnUrl);
                        }
                        else
                        {
                            return RedirectToAction("Index", "Home");
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", "The user name or password provided is incorrect.");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "You must accept the EULA & Terms and Conditions in order to login.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        
        [HttpGet]
        [Authorize]
        public ActionResult SwitchUser()
        {
            if (UserSupport.IsInGroup("_DCAdministrator") 
                || UserSupport.IsInGroup("_DCSystemManager") 
                || UserSupport.IsInGroup("_DCSupport")
                || !string.IsNullOrEmpty(DBHelper.ToString(Session[DispatchCrudeDB.SESSION_ADMIN_SWITCHED_USER])))
                return View();
            return View("Login");
        }
                
        [HttpPost]
        [Authorize]
        public ActionResult SwitchUser(LoginModel model, string returnUrl)
        {
            string newUser = model.UserName; //Using this new variable name to make more sense in the code below

            //Only set the user's original name if this is the first time they switched.  This way
            //we always know who they originated as.
            if (string.IsNullOrEmpty(DBHelper.ToString(Session[DispatchCrudeDB.SESSION_ADMIN_SWITCHED_USER])))
                Session[DispatchCrudeDB.SESSION_ADMIN_SWITCHED_USER] = User.Identity.Name;

            //Get original user (if user is not logged in as their self)
            string originalUser = (string.IsNullOrEmpty(DBHelper.ToString(Session[DispatchCrudeDB.SESSION_ADMIN_SWITCHED_USER])) ?
                                        User.Identity.Name : DBHelper.ToString(Session[DispatchCrudeDB.SESSION_ADMIN_SWITCHED_USER]));            

            if (Membership.FindUsersByName(newUser).Count > 0 
                && CanSwitchUser(originalUser, newUser) == true)
            {
                FormsAuthentication.SetAuthCookie(newUser, false);               

                if (Url.IsLocalUrl(returnUrl))
                    return Redirect(returnUrl);
                else
                    return RedirectToAction("Index", "Home");
            }

            // If we got this far, something failed

            //Check for user exists error
            if (!(Membership.FindUsersByName(newUser).Count > 0))
            {
                ViewBag.errorMessage = "The specified username does not exist";
                ViewBag.errorType = "WARNING";

                if (string.IsNullOrEmpty(ViewBag.ReturnUrl))
                    ViewBag.ReturnUrl = returnUrl;

                return View("SwitchUser");
            }

            //Check for permission error
            if (!CanSwitchUser(originalUser, newUser))
            {
                ViewBag.errorMessage = "You are not authorized to switch to this user";
                ViewBag.errorType = "ERROR";

                if (string.IsNullOrEmpty(ViewBag.ReturnUrl))
                    ViewBag.ReturnUrl = returnUrl;

                return View("SwitchUser");
            }

            ViewBag.ReturnUrl = returnUrl;
            return View("SwitchUser");
        }

        // GET: /Account/LogOff
        public ActionResult LogOff()
        {
            Session.Remove(DispatchCrudeDB.SESSION_ADMIN_SWITCHED_USER);
            FormsAuthentication.SignOut();

            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/Register

        //[AllowAnonymous]
        //public ActionResult Register()
        //{
        //    return View();
        //}

        //
        // POST: /Account/Register

        //[AllowAnonymous]
        //[HttpPost]
        //public ActionResult Register(RegisterModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        // Attempt to register the user
        //        MembershipCreateStatus createStatus;
        //        Membership.CreateUser(model.UserName, model.Password, model.Email, passwordQuestion: null, passwordAnswer: null, isApproved: true, providerUserKey: null, status: out createStatus);

        //        if (createStatus == MembershipCreateStatus.Success)
        //        {
        //            FormsAuthentication.SetAuthCookie(model.UserName, createPersistentCookie: false);
        //            return RedirectToAction("Index", "Home");
        //        }
        //        else
        //        {
        //            ModelState.AddModelError("", ErrorCodeToString(createStatus));
        //        }
        //    }

        //    // If we got this far, something failed, redisplay form
        //    return View(model);
        //}

        //
        // GET: /Account/ChangePassword

        public ActionResult ChangePassword()
        {
            return View();
        }
        
        // POST: /Account/ChangePassword
        [HttpPost, ValidateInput(true)]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {

                // ChangePassword will throw an exception rather
                // than return false in certain failure scenarios.
                bool changePasswordSucceeded;
                try
                {
                    MembershipUser currentUser = Membership.GetUser(User.Identity.Name, userIsOnline: true);
                    changePasswordSucceeded = currentUser.ChangePassword(model.OldPassword, model.NewPassword);
                }
                catch (Exception)
                {
                    changePasswordSucceeded = false;
                }

                if (changePasswordSucceeded)
                {
                    //return RedirectToAction("ChangePasswordSuccess");
                    TempData["success"] = "Password has been changed";
                    return View();
                }
                else
                {
                    ModelState.AddModelError("", "The current password is incorrect, please try again.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }
        
        // GET: /Account/ChangePasswordSuccess
        public ActionResult ChangePasswordSuccess()
        {
            return View();
        }

        //Returns whether or not the current user is allowed to switch to the specified user
        private bool CanSwitchUser(string originalUser, string newUser)
        {
            if (UserSupport.IsInGroup("_DCAdministrator", originalUser)) // the current user is an admin
                return true;

            if (UserSupport.IsInGroup("_DCSystemManager", originalUser) // the current user is a manager
                && !UserSupport.IsInGroup("_DCAdministrator", newUser)) // the new user is not an admin
                return true;

            if (UserSupport.IsInGroup("_DCSupport", originalUser) // the current user is a manager
                && !UserSupport.IsInGroup("_DCAdministrator", newUser) // the new user is not an admin
                && !UserSupport.IsInGroup("_DCSystemManager", newUser)) // the new user is not a manager
                return true;
            
            // Otherwise return false
            return false;
        }

        #region Status Codes
        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #endregion
    }
}
