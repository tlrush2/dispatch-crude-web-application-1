﻿using DispatchCrude.Models;
using DispatchCrude.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using DispatchCrude.Core;
using DispatchCrude.App_Code;
using DispatchCrude.Extensions;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Text.RegularExpressions;

namespace DispatchCrude.Controllers
{
    [Authorize(Roles = "viewDriverMaintenance")]
    public class DriversController : _DBControllerRead
    {
        #region Driver Maintenance Page View/Create/Update/Deactivate
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        override protected ContentResult Read([DataSourceRequest] DataSourceRequest request, ModelStateDictionary modelState, int id = 0)
        {
            // retrieve the profile filter values (TODO: in the database I must not have a regionID value so I defaulted missing to -1, probably should be 0 which will not show any drivers??)
            int carrierID = Profile.GetPropertyValue("CarrierID").ToInt32()
                , regionID = (Profile.GetPropertyValue("RegionID") ?? "-1").ToInt32();
            //, terminalID = (Profile.GetPropertyValue("TerminalID") ?? "-1").ToInt32();  //TODO

            // retrieve Driver model class + ViewInfo class property to include the UserNames values
            var data = db.Drivers
                // return all drivers or just the specified one
                .Where(d => (id == 0 || d.ID == id))
                // include only drivers the current user has Carrier rights to
                .Where(d => (carrierID == -1 || d.CarrierID == carrierID))
                // include only drivers the current user has Region rights to
                .Where(d => (regionID == -1 || d.RegionID == regionID))

                //TODO:
                // include only terminals the current user has Terminal rights to
                //.Where(d => (terminalID == -1 || d.TerminalID == terminalID))
                .Include(d => d.Terminal)
                
                // include the ViewInfo detail for display purposes
                .Include(d => d.ViewInfo);

            // return the data list serialized in a grid consumable json string 
            return ToJsonResult(data, request, modelState);
        }

        // POST: Create
        [HttpPost]
        [Authorize(Roles = "createDriverMaintenance")]
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, Driver driver)
        {
            //New records must have active set to "true" because they default to "false"
            driver.Active = true;

            return Update(request, driver);
        }

        // POST: Update
        [HttpPost]
        [Authorize(Roles = "editDriverMaintenance")]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, Driver driver)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    //Ensure non-numeric characters from Masked textboxes do not make it to the database
                    driver.MobilePhone = NumericUtils.RemoveNonNumeric(driver.MobilePhone);
                    driver.SSN = NumericUtils.RemoveNonNumeric(driver.SSN);

                    db.AddOrUpdateSave(User.Identity.Name, driver);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(null, ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return JsonStringResult.Create(new[] { driver }.ToDataSourceResult(request, ModelState));
                }
            }

            return Read(request, ModelState, driver.ID);
        }

        // Delete (Deactivate)
        [Authorize(Roles = "deactivateDriverMaintenance")]
        [HttpPost]
        public ActionResult Delete([DataSourceRequest] DataSourceRequest request, Driver driver)
        {
            return DeleteInt(request, driver);
        }
        #endregion

        #region Other Driver related functions and some pages
        [HttpGet]
        public ActionResult Details(int id)
        {
            ViewBag.LogDate = DateTime.Today;

            Driver driver = db.Drivers.Where(d => d.ID == id)
                            .Include(d => d.Carrier)
                            .Include(d => d.Terminal)
                            .Include(d => d.Truck)
                            .Include(d => d.Trailer)
                            .Include(d => d.Trailer2)
                            .Include(d => d.Region)
                            .Include(d => d.OperatingState)
                            .FirstOrDefault();
            return View(driver);
        }

        //For the Ajax call on driver maintenance page popup (actually written on index page though).  Returns the next highest driver IDNumber.
        public string NextDriverIdNumber()
        {
            return Driver.getNextIDNumber();
        }


        /**************************************************************************************************/
        /**  DESCRIPTION: Returns select list for popuplating eligible drivers in an MVC dropdown.  This **/
        /**       factors in driver scheduling, compliance, and HOS settings to determine status.  Since **/
        /**       function is static, it assumes that any profile filters have already been applied.     **/
        /**************************************************************************************************/
        public static SelectList getEligibleDriversSL(int? CarrierID, int? TerminalID, int? RegionID, int? StateID, int? DriverGroupID, DateTime? StartDate, bool? h2s, int? selected = null)
        {
            List<SelectListItem> list = new List<SelectListItem>();

            foreach (DriverEligibility driver in getEligibleDrivers(CarrierID, TerminalID ?? 0, RegionID ?? 0, StateID, DriverGroupID, StartDate, h2s))
            {
                list.Add(new SelectListItem()
                {
                    Value = driver.ID.ToString(),
                    Text = driver.DriverName,
                    Disabled = driver.DriverScore == 0
                });
            }

            // Add the current driver if it is a deleted record
            if (selected != null)
            {
                using (DispatchCrudeDB db = new DispatchCrudeDB())
                {
                    Driver d = db.Drivers.Find(selected);
                    if (d != null && d.Deleted)
                    {
                        list.Add(new SelectListItem()
                        {
                            Value = d.ID.ToString(),
                            Text = d.FullName,
                        });
                    }
                }
            }

            return new SelectList(list.OrderBy(d => d.Text), "Value", "Text", selected);
        }

        /**************************************************************************************************/
        /**  DESCRIPTION: Returns "super" select list for popuplating eligible drivers in an MVC dropdown.  This **/
        /**       factors in driver scheduling, compliance, and HOS settings to determine status.  Since **/
        /**       function is static, it assumes that any profile filters have already been applied.     **/
        /**************************************************************************************************/
        public static SelectList getEligibleDriversSSL(int? CarrierID, int? TerminalID, int? RegionID, int? StateID, int? DriverGroupID, DateTime? StartDate, bool? h2s, int? selected = null)
        {
            List<SuperSelectListItem> list = new List<SuperSelectListItem>();

            foreach (DriverEligibility driver in getEligibleDrivers(CarrierID, TerminalID ?? 0, RegionID ?? 0, StateID, DriverGroupID, StartDate, h2s))
            {
                list.Add(new SuperSelectListItem()
                {
                    Value = driver.ID.ToString(),
                    Text = driver.DriverName,
                    Disabled = driver.DriverScore == 0,
                    HTMLContent = @"
                        <div>
                            <span id=""drivername"">" + driver.DriverName + @"</span>
                            <span class=""pull-right label label-info"">" + driver.DriverScore + @"</span>
                        </div>
                        <div style=""font-size: 12px"">
                            Current: " + DateHelper.FormatDuration(driver.CurrentECOT/60.0) + @" hrs
                            Compliance: <i class=""fa fa-" + (driver.ComplianceFactor > 0 ? "check" : "close") + @"""></i>
                            Scheduled: <i class=""fa fa-" + (driver.AvailabilityFactor > 0 ? "check" : "close") + @"""></i>
                            HOS: " + DateHelper.FormatDuration(driver.DrivingHoursLeft) 
                                + " / " + DateHelper.FormatDuration(driver.OnDutyHoursLeft)
                                + " / " + DateHelper.FormatDuration(driver.WeeklyOnDutyHoursLeft) + @"
                        </div>",
                    SortOrder = (driver.DriverScore == 0 ? "ZZZ" : "") + driver.DriverName // put disabled at end
                });
            }

            return new SuperSelectList(list.OrderBy(d => d.SortOrder), selected);
        }

        /**************************************************************************************************/
        /**  PAGE: ~/Drivers/getEligibleDriversJSON/?CarrierID={carrierid}&TerminalID={terminalid}       **/
        /**                                &RegionID={regionid}&StateID={stateid}                        **/
        /**                                &DriverGroupID={drivergroupid}&StartDate={startdate}          **/
        /**                                &h2s={h2s}                                                    **/
        /**  DESCRIPTION: JSON page that returns available drivers for a given date and for a            **/
        /**     carrier/terminal/region/state/driver group/h2s                                           **/
        /**************************************************************************************************/
        public ActionResult getEligibleDriversJSON(int? CarrierID, int? TerminalID, int? RegionID, int? StateID, int? DriverGroupID, DateTime? StartDate, bool? h2s)
        {
            // Filter results based on my shipper/region
            int myCarrierID = Core.Converter.ToInt32(Profile.GetPropertyValue("CarrierID"));
            int myTerminalID = Core.Converter.ToInt32(Profile.GetPropertyValue("TerminalID"));
            int myRegionID = Core.Converter.ToInt32(Profile.GetPropertyValue("RegionID"));

            if (CarrierID == null)
            {
                // Show all but use profile to determine what "ALL" means
                CarrierID = myCarrierID;
            }
            else if (CarrierID != myCarrierID && myCarrierID != -1)
            {
                return Json(null, JsonRequestBehavior.AllowGet); // not allowed to see drivers for this carrier
            }

            if (TerminalID == null)
            {
                // Show all but use profile to determine what "ALL" means
                TerminalID = myTerminalID;
            }
            else if (TerminalID != myTerminalID && myTerminalID != 0)
            {
                return Json(null, JsonRequestBehavior.AllowGet); // not allowed to see drivers for this terminal
            }

            if (RegionID == null)
            {
                // Show all but use profile to determine what "ALL" means
                RegionID = myRegionID;
            }
            else if (RegionID != myRegionID && myRegionID != 0)
            {
                return Json(null, JsonRequestBehavior.AllowGet); // not allowed to see drivers for this region
            }

            var result = getEligibleDrivers(CarrierID, TerminalID, RegionID, StateID, DriverGroupID, StartDate, h2s)
                .Select(x => new { ID = x.ID, Name = x.DriverName })
                .OrderBy(x => x.Name)
                .ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        /**************************************************************************************************/
        /**  DESCRIPTION: Retrieve valid drivers based on the search criteria.  Used to populate    **/
        /**       MVC drop downs and for JSON requests.  Since function is static, it assumes that any   **/
        /**       profile filters have already been applied.                                             **/
        /**************************************************************************************************/
        private static List<DriverEligibility> getEligibleDrivers(int? CarrierID, int? TerminalID, int? StateID, int? RegionID, int? DriverGroupID, DateTime? StartDate, bool? h2s)
        {
            using (DispatchCrudeDB db = new DispatchCrudeDB())
            { 
                string sql = string.Format(@"
                    SELECT *
                    FROM dbo.fnRetrieveEligibleDrivers ({0}, {1}, {2}, {3}, {4}, '{5}', {6}, {7})", 
                    CarrierID ?? -1,
                    TerminalID ?? 0,                    
                    StateID ?? 0,
                    RegionID ?? 0,
                    DriverGroupID ?? 0,
                    StartDate ?? DateTime.Today,
                    (h2s != null && h2s.Value) ? 1 : 0,
                    Settings.AsInt(Settings.SettingsID.DriverLocationsLimitHours));

                return db.Database.SqlQuery<DriverEligibility>(sql).AsQueryable().ToList();
            }
        }
        

        /*************************************************************************************************/
        /**  PAGE: [partial]                                                                            **/
        /**  DESCRIPTION: returns list of driver's upcoming schedule                                    **/
        /*************************************************************************************************/
        public ActionResult Scheduling(int DriverID)
        {
            Driver d = db.Drivers.Find(DriverID);

            // Get HOS policy
            HosPolicy policy = null;
            var PolicyName = CarrierRuleHelper.GetRuleValue(DateTime.Now, CarrierRuleHelper.Type.HOS_POLICY, d.ID, d.CarrierID, d.TerminalID, d.OperatingStateID, d.RegionID);
            if (PolicyName != null)
                policy = db.HosPolicies.Where(p => p.Name == PolicyName.ToString()).FirstOrDefault();
            else
                policy = db.HosPolicies.Find(HosPolicy.DEFAULT_HOS_POLICY);

            ViewBag.Shift = policy.ShiftIntervalDays;
            //get most recent HOS Summary record
            string sql = string.Format(@"
                SELECT *
                    FROM fnDaysInDateRange( GETDATE(), DATEADD(DAY, {1}-1, GETDATE())) d
                    LEFT JOIN viewDriverSchedule ds ON ScheduleDate = d.DayDate AND DriverID = {0}
                    ORDER BY DayDate",
                DriverID,
                policy.ShiftIntervalDays);
            var schedule = db.Database.SqlQuery<DriverScheduleViewModel>(sql).AsQueryable().ToList();

            return PartialView("_Scheduling", schedule);
        }


        /*************************************************************************************************/
        /**  PAGE: [partial]                                                                            **/
        /**  DESCRIPTION: returns list of driver's compliance                                           **/
        /*************************************************************************************************/
        public ActionResult Compliance(int DriverID)
        {
            Driver d = db.Drivers.Find(DriverID);

            string sql = string.Format(@"
                    SELECT *
                    FROM tblDriverCompliance
                    WHERE DriverID = {0}
                    AND DeleteDateUTC IS NULL",
                DriverID);
            List<DriverCompliance> complianceRecords = db.Database.SqlQuery<DriverCompliance>(sql).AsQueryable().ToList();

            ViewBag.ActiveComplianceTypes = db.Database.SqlQuery<DriverComplianceType>("SELECT * FROM tblDriverComplianceType WHERE DeleteDateUTC IS NULL").AsQueryable().ToList();
            return PartialView("_Compliance", complianceRecords);
        }


        /*************************************************************************************************/
        /**  PAGE: [partial]                                                                            **/
        /**  DESCRIPTION: returns list of driver's current HOS hours available                          **/
        /*************************************************************************************************/
        public ActionResult HourSummary(int DriverID)
        {
            //get most recent HOS Summary record
            string sql = string.Format(@"
                SELECT TOP 1 * 
                FROM dbo.fnHosViolationDetail({0}, null, GETUTCDATE()) 
                ORDER BY LogDateUTC DESC",
                DriverID);
            HOSViolationDetailViewModel summary = db.Database.SqlQuery<HOSViolationDetailViewModel>(sql).AsQueryable().FirstOrDefault();

            return PartialView("_HourSummary", summary);
        }


        /*************************************************************************************************/
        /**  PAGE: [partial]                                                                            **/
        /**  DESCRIPTION: returns list driver's HOS violations for the last 3 months rolled up by       **/
        /**         month                                                                               **/
        /*************************************************************************************************/
        public ActionResult Violations(int DriverID)
        {
            ViewBag.DriverID = DriverID;

            //get most recent HOS Summary record
            string sql = string.Format(@"
                DECLARE @EndDate DATETIME = GETUTCDATE()
                DECLARE @StartDate DATETIME = (SELECT dbo.fnLocal_to_UTC_user(dbo.fnFirstDOM(DATEADD(MONTH, -2, GETUTCDATE())), '{1}'))
                SELECT hos.*, Driver = FullName, CarrierID, Carrier
                    FROM viewDriverBase d
                    CROSS APPLY dbo.fnHosDriverViolationSummary(d.ID, @StartDate, @EndDate, 'month') hos
                    WHERE d.ID = {0}
                    ORDER BY [Date]",
                DriverID,
                User.Identity.Name);
            var summary = db.Database.SqlQuery<HOSViolationSummaryViewModel>(sql).AsQueryable();

            return PartialView("_Violations", summary);
        }


        public ActionResult ViolationReport(FilterViewModel<HOSViolationDetailViewModel> fvm)
        {
            // Filter dates
            if (fvm.StartDate == null) // first of month (match driver dashboard)
            {
                DateTime twomonthsago = DateTime.Today.AddMonths(-2);
                fvm.StartDate = DateHelper.StartOfMonth(twomonthsago);
            }
            if (fvm.EndDate == null) // now
            {
                fvm.EndDate = DateTime.Now;
            }

            // Filter dashboard based on my carrier/driver
            int myCarrierID = Converter.ToInt32(Profile.GetPropertyValue("CarrierID"));
            int myDriverID = Converter.ToInt32(Profile.GetPropertyValue("DriverID"));

            //ViewBag.CarrierID = new SelectList(db.Carriers.Where(c => (myCarrierID == -1 || c.ID == myCarrierID) && c.DeleteDateUTC == null).OrderBy(c => c.Name), "ID", "Name", fvm.CarrierID);
            ViewBag.DriverID = new SelectList(db.Drivers.Where(d => (myCarrierID == -1 && (fvm.CarrierID == null || d.CarrierID == fvm.CarrierID) || (myDriverID == -1 && d.CarrierID == myCarrierID) || myDriverID == d.ID) && d.DeleteDateUTC == null).OrderBy(d => d.FirstName).ThenBy(d => d.LastName), "ID", "FullName", fvm.DriverID);


            int? CarrierID = (myCarrierID != -1) ? myCarrierID : fvm.CarrierID; // hardcode carrier if profile not all
            int? DriverID = (myDriverID != 0) ? myDriverID : fvm.DriverID; // hardcode driver if profile not all

            if (DriverID == null)
                return View(fvm); // return empty page

            //get most recent HOS Summary record
            string sql = string.Format(@"
                DECLARE @OnlyViol INT = {3}
                SELECT DriverID, Driver = FullName, 
                    CarrierID, Carrier, hos.*
                FROM viewDriverBase d
                CROSS APPLY fnHosViolationDetail(d.ID, '{1}', '{2}') hos
                WHERE d.ID = {0}
                AND (@onlyviol = 0 OR WeeklyOnDutyViolation = 1 OR WeeklyDrivingViolation = 1 OR OnDutyViolation = 1 OR DrivingViolation = 1 OR BreakViolation = 1)",
                DriverID,
                DateHelper.ToUTC(fvm.StartDate.Value, DispatchCrudeHelper.GetProfileTimeZone(System.Web.HttpContext.Current)),
                DateHelper.ToUTC(fvm.EndDate.Value.AddDays(1), DispatchCrudeHelper.GetProfileTimeZone(System.Web.HttpContext.Current)), // using dates so add 24 hours to include the full day
                (fvm.ShowAll ? 0 : 1));

            fvm.results = db.Database.SqlQuery<HOSViolationDetailViewModel>(sql).AsQueryable().ToList();
            //fvm.results = fvm.results.Where(d => fvm.CarrierID == null || d.CarrierID == fvm.CarrierID);
            return View(fvm);
        }


        public ActionResult LogReport(int ID, DateTime? logDate)
        {
            Driver driver = db.Drivers.Find(ID);
            if (driver == null)
            {
                ViewBag.Error = "That is not a valid driver!";
                return View(); // not a valid driver
            }
            if (hasAccess(driver) == false)
            {
                ViewBag.Error = "You are not allowed to view that driver!";
                return View(); // not allowed to see this driver
            }

            ViewBag.DriverID = ID;
            ViewBag.Driver = driver.FullName;
            ViewBag.LogDate = logDate ?? DateTime.Today;

            return View();
        }

        [Authorize(Roles = "viewDriverMap")]
        public ActionResult Map(int? ID, DateTime? date)
        {
            DateTime StartDate = DateHelper.ToUTC(date ?? DateTime.Now, DispatchCrudeHelper.GetProfileTimeZone(System.Web.HttpContext.Current));
            DateTime EndDate = StartDate.AddDays(1);

            ViewBag.Date = date;
            ViewBag.DriverID = ID;

            int myCarrierID = Converter.ToInt32(Profile.GetPropertyValue("CarrierID"));
            int myDriverID = Converter.ToInt32(Profile.GetPropertyValue("DriverID"));
            ViewBag.ID = new SelectList(db.Drivers.Where(d => d.DeleteDateUTC == null && (myCarrierID == -1 || (myDriverID == 0 && d.CarrierID == myCarrierID) || myDriverID == d.ID)).OrderBy(d => d.FirstName).ThenBy(d => d.LastName), "ID", "FullName", ID);

            if (ID == null)
                return View(); // default page load

            Driver driver = db.Drivers.Find(ID);
            if (driver == null)
            {
                ViewBag.Error = "That is not a valid driver!";
                return View(); // not a valid driver
            }
            if (hasAccess(driver) == false)
            {
                ViewBag.Error = "You are not allowed to view that driver!";
                return View(); // not allowed to see this driver
            }
            var driverLocations = db.DriverLocations.Where(dl => dl.DriverID == ID && 
                                            dl.CreateDateUTC > StartDate && dl.CreateDateUTC < EndDate)
                                    .OrderBy(dl => dl.CreateDateUTC).ToList();

            return View("Map", driverLocations);

        }

        //public ActionResult LogHistory(int DriverID)
        //{
        //    return View();
        //}
        public bool hasAccess(Driver driver)
        {
            // use profile to determine if a user can work with this driver
            int myCarrierID = Converter.ToInt32(Profile.GetPropertyValue("CarrierID"));
            int myDriverID = Converter.ToInt32(Profile.GetPropertyValue("DriverID"));
            return (myCarrierID == -1) || (myCarrierID == driver.CarrierID && myDriverID == 0) || (myDriverID == driver.ID);
        }

        //Return the Carrier ID and Name for a specified driver
        public ActionResult getCarrierJSON(int DriverID)
        {
            Driver driver = db.Drivers.Find(DriverID);

            //If user is not allowed to see this driver (user belongs to another carrier) then do not return any results
            if (hasAccess(driver) == false)
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
            
            var result = new { id = driver.CarrierID, name = driver.Carrier.Name };

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}