﻿using System;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using DispatchCrude.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;

namespace DispatchCrude.Controllers
{
    [Authorize(Roles = "viewDriverComplianceTypes")]
    public class DriverComplianceTypesController : _DBController
    {
        // GET: Product Groups
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest request = null, int id = 0)
        {
            return Read(request, null, id);
        }

        private ContentResult Read([DataSourceRequest] DataSourceRequest request, ModelStateDictionary modelState, int id = 0)
        {
            // .ToList() is required due to filtering on "Active" field, which is NOT MAPPED
            var data = db.DriverComplianceTypes.Where(m => m.ID == id || id == 0).ToList();

            var result = data.ToDataSourceResult(request, modelState);

            return App_Code.JsonStringResult.Create(result);
        }

        // POST: Create Product Groups
        //[HttpPost]
        [Authorize(Roles = "createDriverComplianceTypes")]
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, DriverComplianceType driverComplianceType)
        {
            //Manual validation that would not work in the model
            //If the category is missing...error
            if (driverComplianceType.CategoryID == 0)
                ModelState.AddModelError("Category", "The Category field is required");

            if (ModelState.IsValid)
            {
                try
                {
                    // ensure all new product groups are marked "Active" (otherwise they will be created deleted)
                    driverComplianceType.Active = true;

                    // Save new record to the database
                    db.DriverComplianceTypes.Add(driverComplianceType);
                    db.SaveChanges(User.Identity.Name);  // Create dates & usernames will all be taken care of by this line.
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(null, ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { driverComplianceType }.ToDataSourceResult(request, ModelState));
                }
            }


            return Read(request, ModelState, driverComplianceType.ID);
        }

        // POST: Update Product Groups
        [HttpPost]
        [Authorize(Roles = "editDriverComplianceTypes")]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, DriverComplianceType driverComplianceType)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // Get the existing record
                    DriverComplianceType existing = db.DriverComplianceTypes.Find(driverComplianceType.ID);

                    // Specify all fields that will/can be updated by user input on the popup
                    db.CopyEntityValues(existing, driverComplianceType);

                    db.SaveChanges(User.Identity.Name);  // Create & Modified dates & usernames will be taken care of by this line.
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(null, ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { driverComplianceType }.ToDataSourceResult(request, ModelState));
                }
            }

            return Read(request, ModelState, driverComplianceType.ID);
        }


        // Delete (Deactivate)  Product Groups
        [Authorize(Roles = "deactivateDriverComplianceTypes")]
        public ActionResult Delete(DriverComplianceType driverComplianceType)
        {
            db.DriverComplianceTypes.Attach(driverComplianceType);
            db.DriverComplianceTypes.Remove(driverComplianceType);
            db.SaveChanges(User.Identity.Name);

            return null;
        }
        
        //Returns expiration length (if applicable) and whether or not a document is required (created for/used by driver compliance documents page popup)
        public JsonResult GetDocTypeInformation(int TypeID)
        {
            DriverComplianceType type = db.DriverComplianceTypes.Find(TypeID);

            return Json(new
            {
                ExpirationLength = type.ExpirationLength,
                DocumentRequired = type.RequiresDocument,
                CategoryID = type.CategoryID
            }, JsonRequestBehavior.AllowGet);
        }
    }
}
