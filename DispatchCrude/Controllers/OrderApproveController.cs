﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using AlonsIT;

namespace DispatchCrude.Controllers
{
    public class OrderApproveController : Controller
    {
        //[HttpGet, HttpPost]
        public ActionResult Index()
        {
            ViewBag.Title = "Order Approval";
            return View();
            //return View(Retrieve());
        }

        [HttpPost]
        public ActionResult Read([DataSourceRequest] DataSourceRequest request, string shipperID = "-1", string start = null, string end = null, string includeApproved = "false")
        {
            //if (request.Aggregates.Any())
            //{
            //    request.Aggregates.Each(agg =>
            //    {
            //        agg.Aggregates.Each(a =>
            //        {
            //            a.MemberType = data auditedData.Columns[agg.Member].DataType;
            //        });
            //    });
            //}

            return Json(Retrieve(DBHelper.ToInt32(shipperID), DBHelper.ToDateTime(start), DBHelper.ToDateTime(end), DBHelper.ToBoolean(includeApproved)).ToDataSourceResult(request));
        }

        [HttpPost]
        public IEnumerable<Models.OrderApprovalRO> Retrieve(int? shipperID = null, DateTime? start = null, DateTime? end = null, bool includeApproved = false)
        {
            return Models.OrderApprovalRO.RetrieveWithParameters1(shipperID, start, end, includeApproved);
        }

        [HttpPost]
        public JsonResult RetrieveJson(int? shipperID = null, DateTime? start = null, DateTime? end = null, bool includeApproved = false)
        {
            return Json(Retrieve(shipperID, start, end, includeApproved), JsonRequestBehavior.AllowGet);
        }
    }
}
