﻿using System;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using DispatchCrude.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;

namespace DispatchCrude.Controllers
{
    [Authorize(Roles = "viewDriverEquipmentManufacturers")]
    public class DriverEquipmentManufacturersController : Controller
    {
        private DispatchCrudeDB db = new DispatchCrudeDB();

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        // GET: Product Groups
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest request = null, int id = 0)
        {
            return Read(request, null, id);
        }

        private ContentResult Read([DataSourceRequest] DataSourceRequest request, ModelStateDictionary modelState, int id = 0)
        {
            // .ToList() is required due to filtering on "Active" field, which is NOT MAPPED
            var data = db.DriverEquipmentManufacturers.Where(m => m.ID == id || id == 0).ToList();

            var result = data.ToDataSourceResult(request, modelState);

            return App_Code.JsonStringResult.Create(result);
        }

        // POST: Create Product Groups
        //[HttpPost]
        [Authorize(Roles = "createDriverEquipmentManufacturers")]
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, DriverEquipmentManufacturer driverEquipmentManufacturer)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // ensure all new product groups are marked "Active" (otherwise they will be created deleted)
                    driverEquipmentManufacturer.Active = true;

                    // Save new record to the database
                    db.DriverEquipmentManufacturers.Add(driverEquipmentManufacturer);
                    db.SaveChanges(User.Identity.Name);  // Create dates & usernames will all be taken care of by this line.
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(null, ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { driverEquipmentManufacturer }.ToDataSourceResult(request, ModelState));
                }
            }


            return Read(request, ModelState, driverEquipmentManufacturer.ID);
        }

        // POST: Update Product Groups
        [HttpPost]
        [Authorize(Roles = "editDriverEquipmentManufacturers")]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, DriverEquipmentManufacturer driverEquipmentManufacturer)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // Get the existing record
                    DriverEquipmentManufacturer existing = db.DriverEquipmentManufacturers.Find(driverEquipmentManufacturer.ID);

                    // Specify all fields that will/can be updated by user input on the popup
                    db.CopyEntityValues(existing, driverEquipmentManufacturer);

                    db.SaveChanges(User.Identity.Name);  // Create & Modified dates & usernames will be taken care of by this line.
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(null, ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { driverEquipmentManufacturer }.ToDataSourceResult(request, ModelState));
                }
            }

            return Read(request, ModelState, driverEquipmentManufacturer.ID);
        }


        // Delete (Deactivate)  Product Groups
        [Authorize(Roles = "deactivateDriverEquipmentManufacturers")]
        public ActionResult Delete(DriverEquipmentManufacturer driverEquipmentManufacturer)
        {
            db.DriverEquipmentManufacturers.Attach(driverEquipmentManufacturer);
            db.DriverEquipmentManufacturers.Remove(driverEquipmentManufacturer);
            db.SaveChanges(User.Identity.Name);

            return null;
        }
    }
}
