﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using AlonsIT;
using System.Data.Entity;
using System.Net;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using DispatchCrude.Models;
using DispatchCrude.App_Code;
using DispatchCrude.ViewModels;
using DispatchCrude.Core;

namespace DispatchCrude.Controllers
{
    [Authorize(Roles = "viewGroups")]
    public class DCGroupsController : Controller
    {
        private DispatchCrudeDB db = new DispatchCrudeDB();
        
        // GET: Group
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest request = null, Guid? id = null)
        {
            return Read(request, null, id);
        }

        private ContentResult Read([DataSourceRequest] DataSourceRequest request, ModelStateDictionary modelState, Guid? id)
        {
            IEnumerable<DCGroupViewModel> data = db.DCGroups
                                                    .Include(g => g.Users)
                                                    .Select(x => new DCGroupViewModel()
                                                    {
                                                        Id = x.GroupId
                                                        , Active = (x.DeleteDateUTC == null)
                                                        , UserCount = x.Users.Count
                                                        , PermissionCount = x.Permissions.Count
                                                        , GroupName = x.GroupName
                                                        , Description = x.Description
                                                        , CreateDateUTC = x.CreateDateUTC
                                                        , CreatedByUser = x.CreatedByUser
                                                        , LastChangeDateUTC = x.LastChangeDateUTC
                                                        , LastChangedByUser = x.LastChangedByUser
                                                        , DeleteDateUTC = x.DeleteDateUTC
                                                        , DeletedByUser = x.DeletedByUser
                                                    }
                                                    ).ToList();
            return JsonStringResult.Create(data.ToDataSourceResult(request));
        }

        // POST: Create Groups
        [HttpPost]
        [Authorize(Roles = "createGroups")]
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, DCGroupViewModel groupVM)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    DCGroup newGroup = new DCGroup();

                    newGroup.GroupName = groupVM.GroupName;
                    newGroup.Description = groupVM.Description;
                    newGroup.Active = true;  // ensure all new Shippers are marked "Active" (otherwise they will be created deleted)

                    // Save new record to the database
                    db.DCGroups.Add(newGroup);
                    db.SaveChanges(User.Identity.Name);  // Create dates & usernames will all be taken care of by this line.
                }
                catch (Exception ex)
                {
                    //TODO: This this needs to be revisited and fixed correctly...
                    //This is my "hacky" way to get the error message to show on the popup since it's not working
                    //Right now and I don't have time to work though the whole page/controller/model/viewmodel mess right this minute
                    /**************************/
                    /**/ string key = null;
                    /**/ string message = null;
                    /**/ 
                    /**/ if (ex.Message.Contains("GroupName"))
                    /**/ {
                    /**/     key = "GroupName";
                    /**/     message = "Group name is already in use";
                    /**/ }
                    /***************************/                        

                    ModelState.AddModelError((key == null) ? null : key, (message == null) ? ex.Message : message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { groupVM }.ToDataSourceResult(request, ModelState));
                }
            }

            return Read(request, ModelState, groupVM.Id);
        }

        // POST: Update group
        [HttpPost]
        [Authorize(Roles = "editGroups")]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, DCGroupViewModel groupVM)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // Get the existing record
                    DCGroup existing = db.DCGroups.Find(groupVM.Id);

                    // Specify all fields that will/can be updated by user input on the popup
                    db.CopyEntityValues(existing, groupVM);

                    db.SaveChanges(User.Identity.Name);  // Create & Modified dates & usernames will be taken care of by this line.
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(null, ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { groupVM }.ToDataSourceResult(request, ModelState));
                }
            }

            return Read(request, ModelState, groupVM.Id);
        }
        
        // Delete (Deactivate)        
        [Authorize(Roles = "deactivateGroups")]
        public ActionResult Delete(DCGroupViewModel groupVM)
        {
            DCGroup group = db.DCGroups.Find(groupVM.Id);

            if (group.GroupName == "_DCAdministrator" 
                || group.GroupName == "_DCSystemManager" 
                || group.GroupName == "_DCSupport" 
                || group.GroupName == "_Driver" 
                || group.GroupName == "_Gauger")            
                return null;  // Admin, SysMgr, Support, Driver and Gauger cannot be deactivated
            
            db.DCGroups.Attach(group);
            db.DCGroups.Remove(group);
            db.SaveChanges(User.Identity.Name);

            return null;
        }
        
        //Get Group detail information
        public ActionResult GroupDetails(Guid id)
        {
            DCGroup group = db.DCGroups.Include(g => g.Permissions).Include(g => g.Users).Where(g => g.GroupId == id).First();

            DCGroup gp = db.DCGroups.Include(g => g.Permissions).Where(g => g.GroupId == id).ToList().First();
            ViewBag.GroupPermissions = gp.Permissions.ToList();

            ViewBag.GroupID = id;
            ViewBag.GroupName = group.GroupName;

            //switchUser is elimitated from the list here to keep people from seeing it and trying to use it.
            //This was easier than trying to restrict it's access from the page code.
            ViewBag.AllPermissions = db.DCPermissions
                .Where(p => p.RoleName != "switchUser")
                .Where(p => p.RoleName != "viewElmahErrorLog")
                .OrderBy(p => p.FriendlyName).ToList();                        

            ViewBag.AvailableUsers = new SelectList(GetPossibleGroupUsers(group), "Value", "Text");

            return View(group);
        }


        // Update Group detail information
        [HttpPost]
        public ActionResult GroupDetails(DCGroup group, params string[] SelectedPermissions)
        {
            if (ModelState.IsValid)
            {
                // Save core group information
                DCGroup existing = db.DCGroups.Where(g => g.GroupId == group.GroupId).First();
                db.CopyEntityValues(existing, group);
                db.SaveChanges(User.Identity.Name);  // Create & Modified dates & usernames will be taken care of by this line.


                #region Save group permissions
                DCGroup gp = db.DCGroups.Include(g => g.Permissions).Where(g => g.GroupId == group.GroupId).First();
                IEnumerable<DCPermission> currentPermissions = gp.Permissions.ToList();

                if (SelectedPermissions != null)
                { 
                    using (SSDB ssdb = new SSDB())
                    {
                        // Add permission to group if it is not already an existing permission for the group
                        foreach (string permissionId in SelectedPermissions)
                        {
                            bool existingPermission = false;

                            // Check to see if the group has the permission already
                            using (System.Data.SqlClient.SqlCommand cmd = ssdb.BuildCommand("SELECT dbo.fnIsPermissionInGroup(@GroupID, @PermissionID)"))
                            {                            
                                cmd.Parameters.AddWithValue("@GroupID", group.GroupId);
                                cmd.Parameters.AddWithValue("@PermissionID", permissionId);
                                existingPermission = DBHelper.ToBoolean(cmd.ExecuteScalar());
                            }

                            // If the permission is not in the group add the permission (Adding these permissions to group users will be taken care of by the trigger)
                            if (existingPermission == false)
                            {
                                ssdb.ExecuteSql("INSERT INTO aspnet_RolesInGroups (GroupId, RoleId) SELECT {0}, {1}"
                                    , DBHelper.QuoteStr(group.GroupId)
                                    , DBHelper.QuoteStr(permissionId));
                            }
                        }

                        // Remove any permissions that have been deselected
                        foreach (var permission in currentPermissions)
                        {
                            if (SelectedPermissions.Contains(permission.RoleId.ToString()) == false)
                            {
                                //ssdb.ExecuteSql("DELETE FROM aspnet_RolesInGroups WHERE RoleId = '{0}' AND GroupId = '{1}'", permission.RoleId, group.GroupId);
                                ssdb.ExecuteSql("EXECUTE spDeleteGroupAndUserPermissions {0}, {1}"
                                    , DBHelper.QuoteStr(group.GroupId)
                                    , DBHelper.QuoteStr(permission.RoleId));
                            }
                        }
                    }
                }
                else
                {
                    foreach (var permission in currentPermissions)
                    {
                        using (SSDB ssdb = new SSDB())
                        {
                            // If the user de-selected all permissions, remove all current permissions from the group                            
                            ssdb.ExecuteSql("EXECUTE spDeleteGroupAndUserPermissions {0}, {1}"
                                , DBHelper.QuoteStr(group.GroupId)
                                , DBHelper.QuoteStr(permission.RoleId));
                        }
                    }
                }
                #endregion

                TempData["success"] = "Group details updated successfully.";
                return RedirectToAction("GroupDetails", new { id = group.GroupId });
            }

            //If there was an error, we have to setup the page all over again to get things to display correctly
            DCGroup group2 = db.DCGroups.Include(g => g.Permissions).Include(g => g.Users).Where(g => g.GroupId == group.GroupId).First();
            DCGroup gp2 = db.DCGroups.Include(g => g.Permissions).Where(g => g.GroupId == group.GroupId).ToList().First();
            ViewBag.GroupPermissions = gp2.Permissions.ToList();

            ViewBag.GroupID = group2.GroupId;
            ViewBag.GroupName = group2.GroupName;

            //switchUser is elimitated from the list here to keep people from seeing it and trying to use it.
            //This was easier than trying to restrict it's access from the page code.
            ViewBag.AllPermissions = db.DCPermissions
                .Where(p => p.RoleName != "switchUser")
                .Where(p => p.RoleName != "viewElmahErrorLog")
                .OrderBy(p => p.FriendlyName).ToList();

            ViewBag.AvailableUsers = new SelectList(GetPossibleGroupUsers(group2), "Value", "Text");

            TempData["fail"] = "Group not updated.  Please check errors below.";
            return View(group2);
        }

        //Add users to the group (sent through ajax) from the group details page.
        [Authorize(Roles = "editGroups")]
        public ActionResult addUsersToGroup(string[] Usernames, string groupId)
        {
            string userId;

            using (SSDB ssdb = new SSDB())
            {
                foreach (string user in Usernames)
                {
                    //Get user GUID
                    userId = DBHelper.ToString(ssdb.QuerySingleValue("SELECT UserId FROM aspnet_Users WHERE UserName={0}", DBHelper.QuoteStr(user)));

                    bool inGroup;

                    // Check to see if the user is in the group already
                    using (System.Data.SqlClient.SqlCommand cmd = ssdb.BuildCommand("SELECT dbo.fnIsUserInGroup(@UserID, @GroupID)"))
                    {
                        cmd.Parameters.AddWithValue("@UserID", userId);
                        cmd.Parameters.AddWithValue("@GroupID", groupId);
                        inGroup = DBHelper.ToBoolean(cmd.ExecuteScalar());
                    }

                    // If the user is not in the group add the group (permissions will be taken care of by the trigger)
                    if (inGroup == false)
                    {
                        ssdb.ExecuteSql("INSERT INTO aspnet_UsersInGroups (UserId, GroupId) SELECT {0}, {1}"
                            , DBHelper.QuoteStr(userId)
                            , DBHelper.QuoteStr(groupId));
                    }
                }
            }

            return GroupUsers(groupId);            
        }

        //Collect and return data needed by the user list partial view on the group details page
        [Authorize(Roles = "editGroups")]
        public ActionResult GroupUsers(string groupId, string deletedUserId = "")
        {
            //Get the list of users currently in the group
            Guid guid = Guid.Parse(groupId);
            DCGroup group = db.DCGroups.Include(g => g.Permissions).Include(g => g.Users).Where(g => g.GroupId == guid).First();            

            //Set the viewbag variables required by the partial view
            ViewBag.AvailableUsers = new SelectList(GetPossibleGroupUsers(group), "Value", "Text");
            ViewBag.GroupID = group.GroupId;

            //Show last removed user when applicable
            if(!string.IsNullOrEmpty(deletedUserId))
            {
                var removedUser = db.DCUsers.Find(Guid.Parse(deletedUserId));
                ViewBag.RemovedUser = DCUserViewModel.getFullName(removedUser.UserName) + " (" + removedUser.UserName + ")";
            }            

            return PartialView("_GroupUsers", group);
        }
                
        //Add users to the group (sent through ajax) from the group details page.
        [Authorize(Roles = "editGroups")]
        public ActionResult removeUserFromGroup(string userId, string groupId)
        {
            //Deleted the specified user from the specified group
            using (SSDB ssdb = new SSDB())
            {
                ssdb.ExecuteSql("EXECUTE spDeleteUserGroupsAndPermissions {0}, {1}"
                    , DBHelper.QuoteStr(userId)
                    , DBHelper.QuoteStr(groupId));                
            }

            return GroupUsers(groupId, userId);
        }

        private List<SelectListItem> GetPossibleGroupUsers(DCGroup group)
        {
            //Get list of users that can be added to a group            
            List<SelectListItem> list = new List<SelectListItem>();

            //...Don't include deactivated users
            var users = db.DCUsers.Where(u => u.Membership.DeleteDateUTC == null).ToList();

            using (SSDB ssdb = new SSDB())
            {
                foreach (DCUser user in users)
                {
                    if (DBHelper.ToBoolean(ssdb.QuerySingleValue("SELECT 1 FROM aspnet_UsersInGroups WHERE UserId={0} AND GroupId={1}"
                        , DBHelper.QuoteStr(user.UserId)
                        , DBHelper.QuoteStr(group.GroupId))))
                    {
                        // If the user is already in the group, do not add user to list
                    }
                    else
                    {
                        // If the user is not already in the group, add the user to the list of available users
                        list.Add(new SelectListItem()
                        {
                            Value = user.UserName,
                            Text = DCUserViewModel.getFullName(user.UserName) + " (" + user.UserName + ")"
                        });
                    }
                }
            }

            return list;
        }
    }
}
