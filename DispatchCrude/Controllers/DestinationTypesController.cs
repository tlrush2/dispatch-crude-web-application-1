﻿using System;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using DispatchCrude.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;

namespace DispatchCrude.Controllers
{
    [Authorize(Roles = "viewDestinationTypes")]
    public class DestinationTypesController : Controller
    {
        private DispatchCrudeDB db = new DispatchCrudeDB();

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        // GET: Product Groups
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest request = null, int id = 0)
        {
            return Read(request, null, id);
        }

        private ContentResult Read([DataSourceRequest] DataSourceRequest request, ModelStateDictionary modelState, int id = 0)
        {
            // .ToList() is required due to filtering on "Active" field, which is NOT MAPPED
            var data = db.DestinationTypes.Where(m => m.ID == id || id == 0).ToList();

            var result = data.ToDataSourceResult(request, modelState);

            return App_Code.JsonStringResult.Create(result);
        }

        // POST: Create Product Groups
        //[HttpPost]
        [Authorize(Roles = "createDestinationTypes")]
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, DestinationType destinationType)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // ensure all new product groups are marked "Active" (otherwise they will be created deleted)
                    destinationType.Active = true;

                    // Save new record to the database
                    db.DestinationTypes.Add(destinationType);
                    db.SaveChanges(User.Identity.Name);  // Create dates & usernames will all be taken care of by this line.
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(null, ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { destinationType }.ToDataSourceResult(request, ModelState));
                }
            }


            return Read(request, ModelState, destinationType.ID);
        }

        // POST: Update Product Groups
        [HttpPost]
        [Authorize(Roles = "editDestinationTypes")]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, DestinationType destinationType)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // Get the existing record
                    DestinationType existing = db.DestinationTypes.Find(destinationType.ID);

                    // Specify all fields that will/can be updated by user input on the popup
                    db.CopyEntityValues(existing, destinationType);

                    db.SaveChanges(User.Identity.Name);  // Create & Modified dates & usernames will be taken care of by this line.
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(null, ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { destinationType }.ToDataSourceResult(request, ModelState));
                }
            }

            return Read(request, ModelState, destinationType.ID);
        }


        // Delete (Deactivate)  Product Groups
        [Authorize(Roles = "deactivateDestinationTypes")]
        public ActionResult Delete(DestinationType destinationType)
        {
            db.DestinationTypes.Attach(destinationType);
            db.DestinationTypes.Remove(destinationType);
            db.SaveChanges(User.Identity.Name);

            return null;
        }
    }
}
