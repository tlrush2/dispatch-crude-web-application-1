﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.Profile;
using Newtonsoft.Json;
using DispatchCrude.Core;
using DispatchCrude.Models;
using DispatchCrude.Models.Sync;
using DispatchCrude.Models.Sync.GaugerApp;
using AlonsIT;

namespace DispatchCrude.Controllers
{
    public class GaugerAppController : Controller
    {
        private DispatchCrudeDB db = new DispatchCrudeDB();

        //
        // GET: /Account/Index

        [AllowAnonymous]
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        //
        // POST: /GaugerApp/Login

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(string userName, string password)
        {
            Outcome outcome = new Outcome();
            if (string.IsNullOrEmpty(userName) || !Membership.ValidateUser(userName, password))
                outcome.Message = "User Authentication failed";
            else
            {
                int gaugerID = 0;
                if ((outcome = GetGaugerID(userName, out gaugerID)).Success)
                {   // valid outcome occurred, so return the MasterData instance in the LoginResult
                    return JsonResult(new LoginResult(outcome, MasterData.Init(userName, gaugerID, ref outcome)));
                }
            }
            // an error occurred, so just return the Outcome
            return JsonResult(new LoginResult(outcome));
        }

        private Outcome GetGaugerID(string userName, out int gaugerID)
        {
            Outcome ret = new Outcome();
            gaugerID = 0; // default value
            if (!Roles.IsUserInRole(userName, "Gauger"))
                ret.Message = "User is not assigned the 'Gauger' role";
            else if ((gaugerID = DBHelper.ToInt32(ProfileBase.Create(userName).GetPropertyValue("GaugerID"))) <= 0)
                ret.Message = "User is not assigned to a Gauger account";
            else if (!GetGaugerMobileAppEnabled(gaugerID))
                ret.Message = "Your gauger account is not configured for Mobile App access";
            else
                ret.Success = true;
            return ret;
        }
        private bool GetGaugerMobileAppEnabled(int gaugerID)
        {
            using (SSDB ssdb = new SSDB())
            {
                return DBHelper.ToBoolean(ssdb.QuerySingleValue("SELECT MobileApp FROM dbo.tblGauger WHERE ID = {0}", gaugerID));
            }
        }

        [HttpGet]
        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ChangePassword(string userName, string oldPassword, string newPassword)
        {
            Outcome outcome = new Outcome();
            if (!Membership.ValidateUser(userName, oldPassword))
                outcome.Message = "Old Password was invalid";
            else if (!Membership.GetUser(userName).ChangePassword(oldPassword, newPassword))
                outcome.Message = "Invalid new password was provided";
            else
            {
                int gaugerID = 0;
                if ((outcome = GetGaugerID(userName, out gaugerID)).Success)
                    return JsonResult(new LoginResult(outcome, MasterData.ResetPasswordHash(userName, gaugerID, ref outcome)));
            }
            // an error occurred, so just return the Outcome
            return JsonResult(new LoginResult(outcome));
        }

        [AllowAnonymous]
        public ActionResult Sync()
        {
            return View();
        }

        // support method
        private ActionResult JsonResult(object data)
        {
            return Content(JsonConvert.SerializeObject(data, SyncDataInput.JSonSettings()));
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Sync(string json)
        {
            Outcome outcome = new Outcome();

            SyncDataInput incomingSD = null;
            MasterData outgoingMD = null;
            try
            {
                // deserialize the incoming JSON into a SyncDataInput object
                if (json.Length == 0)
                    outcome.Message = "No JSON was received";
                else
                {
                    try
                    {
                        incomingSD = SyncDataInput.GetFromJSON(json);
                    }
                    catch (Exception)
                    {
                        outcome.Message = "Invalid JSON was received";
                    }

                    if (incomingSD.Master == null)
                        outcome.Message = "Invalid/Missing Master data was received";
                    else if (string.IsNullOrEmpty(incomingSD.Master.UserName))
                        outcome.Message = "UserName was missing or invalid";
                    else
                    {
                        // if successful this will set the outcome.Success to TRUE
                        outgoingMD = MasterData.Sync(incomingSD.Master, ref outcome);
                        if (outcome.Success)
                        {
                            try
                            {
                                // if this is an initial (full) sync, we need to ensure that any "VirtualDelete" records are first cleared (not needed)
                                if (!incomingSD.Master.LastSyncUTC.HasValue)
                                {
                                    using (SSDB db = new SSDB())
                                    {
                                        db.ExecuteSql("DELETE FROM tblGaugerOrderVirtualDelete WHERE GaugerID={0}", incomingSD.Master.GaugerID);
                                    }
                                }
                                else // regular sync logic (incoming data was received)
                                {   // do the PUSH synchronization
                                    // only update the order if the Gauger matches (it returns TRUE if something actually changed)

                                    List<string> processedTicketUIDs = new List<string>();

                                    using (DispatchCrudeDB db = new DispatchCrudeDB())
                                    {
                                        if (incomingSD.OrderEdits != null)
                                        {
                                            // process any changed orders from the Gauger App
                                            foreach (GaugerOrderGA incomingOrder in incomingSD.OrderEdits)
                                            {
                                                GaugerOrder currentOrder = db.GaugerOrders.Where(r => r.OrderID == incomingOrder.OrderID)
                                                    .Include(r => r.GaugerTickets)
                                                    .SingleOrDefault();

                                                if (incomingSD.Master.GaugerID == currentOrder.GaugerID)
                                                {
                                                    bool changed = incomingOrder.UpdateValues(currentOrder);
                                                    if (incomingSD.OrderTickets != null)
                                                    {
                                                        foreach (GaugerTicketGA ticket in incomingSD.OrderTickets.Where(ot => ot.OrderID == incomingOrder.OrderID))
                                                        {
                                                            GaugerTicket currentTicket = currentOrder.GaugerTickets.FirstOrDefault(ot => ot.UID == ticket.UID);
                                                            if (currentTicket == null)
                                                            {
                                                                currentTicket = new GaugerTicket(ticket.UID, ticket.OrderID);
                                                                currentOrder.GaugerTickets.Add(currentTicket);
                                                            }
                                                            processedTicketUIDs.Add(ticket.UID.ToString());
                                                            if (ticket.UpdateValues(currentTicket))
                                                                changed = true;
                                                        }
                                                    }
                                                    if (changed)
                                                    {
                                                        try
                                                        {
                                                            db.SaveChanges(outgoingMD.UserName);
                                                        }
                                                        catch (Exception e)
                                                        {
                                                            outcome.Success = false;
                                                            //outcome.Message += LogSyncError(incomingSD.Master.GaugerID, currentOrder.Order.OrderNum + ": " + e.Message);
                                                            outcome.Message += LogSyncError(incomingSD.Master.GaugerID, "ID=" + currentOrder.OrderID + ": " + e.Message);
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        // process any incoming ticket changes that did not have an associated changed (order)
                                        if (incomingSD.OrderTickets != null)
                                        {
                                            foreach (GaugerTicketGA ticket in incomingSD.OrderTickets)
                                            {
                                                if (!processedTicketUIDs.Contains(ticket.UID.ToString()))
                                                {
                                                    GaugerTicket currentTicket = db.GaugerTickets.FirstOrDefault(ot => ot.UID == ticket.UID);
                                                    if (currentTicket == null)
                                                    {
                                                        currentTicket = new GaugerTicket(ticket.UID, ticket.OrderID);
                                                        db.GaugerTickets.Add(currentTicket);
                                                    }
                                                    if (ticket.UpdateValues(currentTicket))
                                                    {
                                                        try
                                                        {
                                                            db.SaveChanges(outgoingMD.UserName);
                                                        }
                                                        catch (Exception e)
                                                        {
                                                            outcome.Success = false;
                                                            outcome.Message += LogSyncError(incomingSD.Master.GaugerID, currentTicket.CarrierTicketNum + ": " + e.Message);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    // process new ADDs
                                    if (incomingSD.OrderSignatures != null)
                                    {
                                        System.Data.DataTable dtSignatures = SyncHelper.PopulateDataTableFromList(incomingSD.OrderSignatures);
                                        // since UID is the key, but an ID (identity) column exists, remove it so 
                                        //it doesn't try to insert into an identity column (which will fail)
                                        dtSignatures.Columns.Remove("ID");
                                        using (SSDB ssdb = new SSDB(false))
                                        {
                                            foreach (System.Data.DataRow row in dtSignatures.Rows)
                                            {
                                                try
                                                {
                                                    object uid = SSDBSave.SaveDataRow(ssdb, row, "tblOrderSignature", "UID"
                                                        , SSDBSave.UpdateMode.IgnoreConcurrencyExceptions | SSDBSave.UpdateMode.IncludeBlobFields
                                                        , UserSupport.UserName);
                                                }
                                                catch (Exception e)
                                                {
                                                    // record the failure but continue processing
                                                    outcome.Success = false;
                                                    outcome.Message += e.Message;
                                                }
                                            }
                                        }
                                    }
                                    // process new ADDs
                                    if (incomingSD.Exceptions != null)
                                    {
                                        System.Data.DataTable dtExceptions = SyncHelper.PopulateDataTableFromList(incomingSD.Exceptions);
                                        using (SSDB ssdb = new SSDB(false))
                                        {
                                            foreach (System.Data.DataRow row in dtExceptions.Rows)
                                            {
                                                try
                                                {
                                                    object uid = SSDBSave.SaveDataRow(ssdb, row, "tblGaugerAppExceptionLog", "UID"
                                                        , SSDBSave.UpdateMode.IgnoreConcurrencyExceptions
                                                        , UserSupport.UserName);
                                                }
                                                catch (Exception e)
                                                {
                                                    // record the failure but continue processing
                                                    outcome.Success = false;
                                                    outcome.Message += e.Message;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                outcome.Success = false;
                                outcome.Message += LogSyncError(incomingSD.Master.GaugerID, ex.Message);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                outcome.Success = false;
                outcome.Message += LogSyncError(incomingSD.Master.GaugerID, string.Format("GaugerApp.Sync server exception occurred, JSON={0}, Error={1}", json, ex.Message));
            }

            SyncData output = null;
            if (outcome.Success && outgoingMD != null)
            {
                output = new SyncData(incomingSD.Master.LastSyncUTC, outcome, outgoingMD);
            }
            LogSyncOperation(incomingSD.Master.GaugerID, json, output == null ? outcome.Message : output.SerializeAsJson(true));

            // return the result
            return JsonResult(output);
        }

        private void LogSyncOperation(int gaugerID, string incomingJSON, string outputJSON)
        {
            // log the incoming JSON if this associated system setting is enabled
            if (Settings.SettingsID.DA_DEBUG_SYNC.AsBool())
            {
                using (SSDB ssdb = new SSDB())
                {
                    ssdb.ExecuteSql("INSERT INTO tblGaugerSyncLog(GaugerID, IncomingJSON, OutgoingJSON) VALUES ({0}, {1}, {2})"
                        , gaugerID, DBHelper.QuoteStr(incomingJSON), DBHelper.QuoteStr(outputJSON));
                }
            }
        }

        private string LogSyncError(int GaugerID, string error)
        {
            error = error.Replace("The transaction ended in the trigger. ", "");
            error = error.Replace("The batch has been aborted.", "");
            error = error.Replace("FIRED", "");
            error = error.Replace("COMPLETE", "");
            error = error.Replace("trigOrderTicket_IU", "trigOT_IU");
            error = error.Replace("  ", " ").Trim();
            using (SSDB ssdb = new SSDB())
            {
                using (System.Data.SqlClient.SqlCommand cmd = ssdb.BuildCommand("spGaugerAppLogSyncError"))
                {
                    cmd.Parameters["@GaugerID"].Value = GaugerID;
                    cmd.Parameters["@errorMsg"].Value = error;
                    cmd.ExecuteNonQuery();
                }
            }
            return error;
        }
        [Authorize(Roles = "viewGaugerAppSyncLog")]
        public ActionResult GaugerLog(int? gaugerid, string count)
        {
            Gauger gauger = db.Gaugers.Find(gaugerid);
            ViewBag.GaugerID = new SelectList(db.Gaugers.OrderBy(d => d.LastName).ThenBy(d => d.FirstName), "ID", "FullName", gaugerid);
            ViewBag.Count = count;
            return View("Log", gauger);
        }

        [Authorize(Roles = "viewGaugerAppSyncLog")]
        public ActionResult OutgoingJSON(int id)
        {
            return getJson(id, "OutgoingJson");
        }
        [Authorize(Roles = "viewGaugerAppSyncLog")]
        public ActionResult IncomingJSON(int id)
        {
            return getJson(id, "IncomingJson");
        }

        private ActionResult getJson(int id, string field = " OutgoingJson")
        {
            using (SSDB ssdb = new SSDB())
            {
                string json = ssdb.QuerySingleValue("SELECT " + field + " FROM tblGaugerSyncLog WHERE ID = " + id).ToString();
                return new ContentResult { Content = json, ContentType = "application/json" };
            }
        }
    }

}