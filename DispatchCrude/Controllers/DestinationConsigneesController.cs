﻿using System;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using DispatchCrude.Models;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Data.Entity;

namespace DispatchCrude.Controllers
{
    [Authorize(Roles = "viewConsignees")]
    public class DestinationConsigneesController : Controller
    {
        private DispatchCrudeDB db = new DispatchCrudeDB();

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        // GET: Read
        public ActionResult Index(int? consigneeid, int? destinationid)
        {
            //If a destination has been specified as a starting filter in the url, get it and pass it to the view
            Destination d = db.Destinations.Find(destinationid);
            if (d != null)
            {
                ViewBag.DestinationName = d.Name;

                //We don't want the URL filter to stay after we've filtered the grid so this lets us know on the view to get rid of it.
                ViewBag.ClearUrl = true;
            }

            //If a consignee has been specified as a starting filter in the url, get it and pass it to the view
            Consignee c = db.Consignees.Find(consigneeid);
            if (c != null)
            {
                ViewBag.ConsigneeName = c.Name;

                //We don't want the URL filter to stay after we've filtered the grid so this lets us know on the view to get rid of it.
                ViewBag.ClearUrl = true;
            }

            return View();
        }

        public ActionResult Read([DataSourceRequest] DataSourceRequest request = null, int id = 0)
        {
            return Read(request, null, id);
        }

        private ContentResult Read([DataSourceRequest] DataSourceRequest request, ModelStateDictionary modelState, int id = 0)
        {
            // .ToList() is required due to filtering on "Active" field, which is NOT MAPPED
            var data = db.DestinationConsignees.Where(m => m.ID == id || id == 0)
                .Include(dc => dc.Destination)
                .Include(dc => dc.Consignee)
                .ToList();
            var result = data.ToDataSourceResult(request, modelState);            
            return App_Code.JsonStringResult.Create(result);  
        }
        
        // POST: Create
        [HttpPost]
        [Authorize(Roles = "createConsignees")]
        public ActionResult Create([DataSourceRequest] DataSourceRequest request, [Bind(Exclude="Destination,Consignee")] DestinationConsignee dc)
        {            
            if (ModelState.IsValid)
            {
                try
                {
                    // Save new record to the database
                    db.DestinationConsignees.Add(dc);
                    db.SaveChanges(User.Identity.Name);  // Create dates & usernames will all be taken care of by this line.
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(null, ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { dc }.ToDataSourceResult(request, ModelState));
                }
            }
            
            return Read(request, ModelState, dc.ID);
        }

        // POST: Update
        [HttpPost]
        [Authorize(Roles = "editConsignees")]
        public ActionResult Update([DataSourceRequest] DataSourceRequest request, [Bind(Exclude="Destination,Consignee")] DestinationConsignee dc)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // Get the existing record
                    // This allows us to keep the existing file upload if one is not specified in an edit
                    DestinationConsignee existing = db.DestinationConsignees.Find(dc.ID);

                    // Specify all fields that will/can be updated by user input on the popup
                    db.CopyEntityValues(existing, dc);

                    db.SaveChanges(User.Identity.Name);  // Create & Modified dates & usernames will be taken care of by this line.
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(null, ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { dc }.ToDataSourceResult(request, ModelState));
                }
            }

            return Read(request, ModelState, dc.ID);
        }

        // Delete (Deactivate)
        [Authorize(Roles = "deactivateConsignees")]
        public ActionResult Delete(DestinationConsignee dc)
        {
            db.DestinationConsignees.Attach(dc);
            db.DestinationConsignees.Remove(dc);
            db.SaveChanges(User.Identity.Name);

            return null;
        }
    }
}
