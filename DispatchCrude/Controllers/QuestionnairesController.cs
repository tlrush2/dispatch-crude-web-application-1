﻿using AlonsIT;
using DispatchCrude.App_Code;
using DispatchCrude.Core;
using DispatchCrude.Models;
using DispatchCrude.ViewModels;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Syncfusion.XlsIO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using DispatchCrude.DataExchange;

namespace DispatchCrude.Controllers
{
    /**************************************************************************************************/
    /**  CONTORLLER: Questionnaires                                                                  **/
    /**  DESCRIPTION: Anyone with viewQuestionnaires access can view the templates and submitted      **/
    /**      questionnaires.  They can also access can use the wizard to create a template or do     **/
    /**      other administrative tasks with the permission createQuestionnaires.                     **/
    /**************************************************************************************************/
    [Authorize(Roles = "viewQuestionnaires")]
    public class QuestionnairesController : Controller
    {
        private DispatchCrudeDB db = new DispatchCrudeDB();

        /// --------------------------------------------------------------------------------------------
        /// <summary>
        ///     List of all questionnaires filtered by the carrier
        /// </summary>
        /// <remarks>~/Questionnaires/</remarks>
        /// --------------------------------------------------------------------------------------------
        public ActionResult Index()
        {
            // Filter templates that use my carrier
            int myDriverID = Converter.ToInt32(Profile.GetPropertyValue("DriverID"));
            int myCarrierID = Converter.ToInt32(Profile.GetPropertyValue("CarrierID"));
            IQueryable<QuestionnaireTemplate> templates = db.QuestionnaireTemplates.Where(t => myCarrierID == -1 || (t.CarrierID == null) || t.CarrierID == myCarrierID);

            return View(templates);
        }


        /// --------------------------------------------------------------------------------------------
        /// <summary>
        ///     Previews the questionnaire form similar to how it would look on the driver application
        /// </summary>
        /// <param name="id">Database ID of the questionnaire template</param>
        /// <remarks>~/Questionnaires/Preview/{id}</remarks>
        /// --------------------------------------------------------------------------------------------
        public ActionResult Preview(int id)
        {
            QuestionnaireTemplate template = db.QuestionnaireTemplates.Find(id);
            if (template == null)
            {
                TempData["error"] = "Cannot find that template!";
                return RedirectToAction("Index");
            }
            if (HasAccess(template) == false)
            {
                TempData["error"] = "You do not have permission to that template!";
                return RedirectToAction("Index");
            }

            return View("Preview", template);
        }


        /// --------------------------------------------------------------------------------------------
        /// <summary>
        ///     A view of the questions for a given questionnaire
        /// </summary>
        /// <param name="id">Database ID of the questionnaire template</param>
        /// <remarks>[PARTIAL VIEW]</remarks>
        /// --------------------------------------------------------------------------------------------
        public ActionResult ViewQuestions(int id)
        {
            QuestionnaireTemplate template = db.QuestionnaireTemplates.Find(id);
            if (template == null)
            {
                TempData["error"] = "Cannot find that template!";
                return RedirectToAction("Index");
            }
            if (HasAccess(template) == false)
            {
                TempData["error"] = "You do not have permission to that template!";
                return RedirectToAction("Index");
            }

            var questions = db.QuestionnaireQuestions.Where(q => q.QuestionnaireTemplateID == id).OrderBy(q => q.SortNum);

            ViewBag.LoadDefaults = template.LoadDefaults;
            return PartialView(questions);
        }


        /// --------------------------------------------------------------------------------------------
        /// <summary>
        ///     Add a question to a questionnaire template
        /// </summary>
        /// <param name="templateid">Database ID of the questionnaire template</param>
        /// <remarks>~/Questionnaires/AddQuestion/{id}</remarks>
        /// --------------------------------------------------------------------------------------------
        [Authorize(Roles = "createQuestionnaires,editQuestionnaires")]
        public ActionResult AddQuestion(int templateid)
        {
            QuestionnaireTemplate template = db.QuestionnaireTemplates.Find(templateid);
            if (template == null)
            {
                TempData["error"] = "Cannot find that template!";
                return RedirectToAction("Index");
            }
            if (HasAccess(template) == false)
            {
                TempData["error"] = "You do not have permission to that template!";
                return RedirectToAction("Index");
            }
            if (template.Locked)
            {
                TempData["error"] = "Cannot edit a locked template!";
                return RedirectToAction("Index");
            }

            ViewBag.TemplateID = templateid;
            ViewBag.Template = template.Name;
            ViewBag.LoadDefaults = template.LoadDefaults;
            ViewBag.QuestionnaireQuestionTypeID = new SelectList(db.QuestionnaireQuestionTypes, "ID", "QuestionType");
            if (template.QuestionnaireQuestions.Any(q => q.QuestionnaireQuestionTypeID == (int) QuestionnaireQuestionType.TYPE.Blob))
                ViewBag.DefaultSort = template.QuestionnaireQuestions.Count; // attempt to keep signature last
            else
                ViewBag.DefaultSort = template.QuestionnaireQuestions.Count + 1;
            return View(new QuestionnaireQuestion() { QuestionnaireTemplateID = templateid });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "createQuestionnaires,editQuestionnaires")]
        public ActionResult AddQuestion(QuestionnaireQuestion question, string min, string max, string pcmin, string pcmax, bool? pcbool, bool? defbool)
        {
            // Parse AnswerFormat depending on the question type
            if (question.QuestionnaireQuestionTypeID == (int)QuestionnaireQuestionType.TYPE.DropDown)
            {
                if (String.IsNullOrEmpty(question.AnswersFormat))
                {
                    ModelState.AddModelError("AnswersFormat", "Format expected for Drop Down fields!  Please enter a list of options to choose from.");
                }
            }
            else if (  question.QuestionnaireQuestionTypeID == (int)QuestionnaireQuestionType.TYPE.Integer
                     || question.QuestionnaireQuestionTypeID == (int)QuestionnaireQuestionType.TYPE.Decimal
                     || question.QuestionnaireQuestionTypeID == (int)QuestionnaireQuestionType.TYPE.Timestamp)
            {
                if (question.IsValidRange(min, max))
                {
                    if (String.IsNullOrEmpty(min) && String.IsNullOrEmpty(max))
                        question.AnswersFormat = null;
                    else
                        question.AnswersFormat = min + QuestionnaireQuestion.DROPDOWNDELIMITER + max;
                }
                else
                    ModelState.AddModelError("AnswersFormat", "Min and max values are not valid!  Ranges must be in the right format or blank.");
            }
            else
            {
                question.AnswersFormat = null; // clear field
            }

            // Parse PassingCondition depending on the question type
            if (question.QuestionnaireQuestionTypeID == (int)QuestionnaireQuestionType.TYPE.DropDown)
            {
                // check valid answer is one of the drop downs?
            }
            else if (   question.QuestionnaireQuestionTypeID == (int)QuestionnaireQuestionType.TYPE.Integer
                     || question.QuestionnaireQuestionTypeID == (int)QuestionnaireQuestionType.TYPE.Decimal
                     || question.QuestionnaireQuestionTypeID == (int)QuestionnaireQuestionType.TYPE.Timestamp)
            {
                if (question.IsValidRange(pcmin, pcmax))
                {
                    if (String.IsNullOrEmpty(pcmin) && String.IsNullOrEmpty(pcmax))
                        question.PassingCondition = null;
                    else
                        question.PassingCondition = pcmin + QuestionnaireQuestion.DROPDOWNDELIMITER + pcmax;
                }
                else
                    ModelState.AddModelError("PassingCondition", "Min and max values are not valid!  Ranges must be in the right format or blank.");
            }
            else if (question.QuestionnaireQuestionTypeID == (int)QuestionnaireQuestionType.TYPE.Boolean)
            {
                question.PassingCondition = (pcbool == null) ? null : pcbool.ToString();
            }
            else
            {
                question.PassingCondition = null;
            }


            // Parse DefaultAnswer depending on the question type
            if (question.QuestionnaireQuestionTypeID == (int)QuestionnaireQuestionType.TYPE.DropDown)
            {
                // check valid answer is one of the drop downs?
            }
            else if (   question.QuestionnaireQuestionTypeID == (int)QuestionnaireQuestionType.TYPE.Integer
                     || question.QuestionnaireQuestionTypeID == (int)QuestionnaireQuestionType.TYPE.Decimal
                     || question.QuestionnaireQuestionTypeID == (int)QuestionnaireQuestionType.TYPE.Timestamp)
            {
                if (question.IsValidDefault(question.DefaultAnswer) == false)
                    ModelState.AddModelError("DefaultAnswer", "Value is not valid!  Default must be in the right format or blank.");
            }
            else if (question.QuestionnaireQuestionTypeID == (int)QuestionnaireQuestionType.TYPE.Boolean)
            {
                question.DefaultAnswer = (defbool == null) ? null : defbool.ToString();
            }


            if (ModelState.IsValid)
            {
                db.QuestionnaireQuestions.Add(question);
                db.SaveChanges(User.Identity.Name);
                return RedirectToAction("Edit", new { id = question.QuestionnaireTemplateID });
            }

            ViewBag.TemplateID = question.QuestionnaireTemplateID;
            ViewBag.Template = db.QuestionnaireTemplates.Find(question.QuestionnaireTemplateID).Name;
            ViewBag.QuestionnaireQuestionTypeID = new SelectList(db.QuestionnaireQuestionTypes, "ID", "QuestionType", question.QuestionnaireQuestionTypeID);
            ViewBag.DefaultSort = question.SortNum;
            ViewBag.Min = min;
            ViewBag.Max = max;
            ViewBag.pcMin = pcmin;
            ViewBag.pcMax = pcmax;
            ViewBag.pcBool = pcbool;
            ViewBag.defBool = defbool;
            ViewBag.LoadDefaults = db.QuestionnaireTemplates.Find(question.QuestionnaireTemplateID).LoadDefaults;

            return View(question);
        }


        /// --------------------------------------------------------------------------------------------
        /// <summary>
        ///     Edit an existing question for a questionnaire template
        /// </summary>
        /// <param name="id">Database ID of the questionnaire question</param>
        /// <remarks>~/Questionnaires/EditQuestion/{id}</remarks>
        /// --------------------------------------------------------------------------------------------
        [Authorize(Roles = "editQuestionnaires")]
        public ActionResult EditQuestion(int id)
        {
            QuestionnaireQuestion question = db.QuestionnaireQuestions.Find(id);
            if (question == null)
            {
                TempData["error"] = "Cannot find that question!";
                return RedirectToAction("Index");
            }
            if (HasAccess(question.QuestionnaireTemplate) == false)
            {
                TempData["error"] = "You do not have permission to that template!";
                return RedirectToAction("Index");
            }
            if (question.QuestionnaireTemplate.Locked)
            {
                TempData["error"] = "Cannot edit a locked template!";
                return RedirectToAction("Index");
            }

            if (question._AnswersFormat != null && question._AnswersFormat.Contains(QuestionnaireQuestion.DROPDOWNDELIMITER))
            {
                string[] range = question._AnswersFormat.Split(QuestionnaireQuestion.DROPDOWNDELIMITER);
                ViewBag.Min = range[0];
                ViewBag.Max = range[1];
            }
            if (question._PassingCondition != null && question._PassingCondition.Contains(QuestionnaireQuestion.DROPDOWNDELIMITER))
            {
                string[] range = question._PassingCondition.Split(QuestionnaireQuestion.DROPDOWNDELIMITER);
                ViewBag.pcMin = range[0];
                ViewBag.pcMax = range[1];
            }
            ViewBag.pcBool = (question._PassingCondition == null) ? null : (bool?)Converter.ToBoolean(question._PassingCondition, false);
            ViewBag.defBool = (question.DefaultAnswer == null) ? null : (bool?)Converter.ToBoolean(question.DefaultAnswer, false);
            ViewBag.DeleteFlag = question.Deleted;
            ViewBag.Template = question.QuestionnaireTemplate.Name;
            ViewBag.LoadDefaults = question.QuestionnaireTemplate.LoadDefaults;
            ViewBag.QuestionnaireQuestionTypeID = new SelectList(db.QuestionnaireQuestionTypes, "ID", "QuestionType", question.QuestionnaireQuestionTypeID);
            return View(question);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "editQuestionnaires")]
        public ActionResult EditQuestion(QuestionnaireQuestion question, bool DeleteFlag, string min, string max, string pcmin, string pcmax, bool? pcbool, bool? defbool)
        {
            // Parse AnswerFormat depending on the question type
            if (question.QuestionnaireQuestionTypeID == (int)QuestionnaireQuestionType.TYPE.DropDown)
            {
                if (String.IsNullOrEmpty(question.AnswersFormat))
                    ModelState.AddModelError("AnswersFormat", "Format expected for Drop Down fields.  Please enter a list of options to choose from");
            }
            else if (   question.QuestionnaireQuestionTypeID == (int)QuestionnaireQuestionType.TYPE.Integer
                     || question.QuestionnaireQuestionTypeID == (int)QuestionnaireQuestionType.TYPE.Decimal
                     || question.QuestionnaireQuestionTypeID == (int)QuestionnaireQuestionType.TYPE.Timestamp)
            {
                if (question.IsValidRange(min, max))
                {
                    if (String.IsNullOrEmpty(min) && String.IsNullOrEmpty(max))
                        question.AnswersFormat = null;
                    else
                        question.AnswersFormat = min + QuestionnaireQuestion.DROPDOWNDELIMITER + max;
                }
                else
                    ModelState.AddModelError("AnswersFormat", "Min and max values are not valid!  Ranges must be in the right format or blank.");
            }
            else
            {
                question.AnswersFormat = null;
            }

            // Parse PassingCondition depending on the question type
            if (question.QuestionnaireQuestionTypeID == (int)QuestionnaireQuestionType.TYPE.DropDown)
            {
                // check valid answer is one of the drop downs?
            }
            else if (   question.QuestionnaireQuestionTypeID == (int)QuestionnaireQuestionType.TYPE.Integer
                     || question.QuestionnaireQuestionTypeID == (int)QuestionnaireQuestionType.TYPE.Decimal
                     || question.QuestionnaireQuestionTypeID == (int)QuestionnaireQuestionType.TYPE.Timestamp)
            {
                if (question.IsValidRange(pcmin, pcmax))
                {
                    if (String.IsNullOrEmpty(pcmin) && String.IsNullOrEmpty(pcmax))
                        question.PassingCondition = null;
                    else
                        question.PassingCondition = pcmin + QuestionnaireQuestion.DROPDOWNDELIMITER + pcmax;
                }
                else
                    ModelState.AddModelError("PassingCondition", "Min and max values are not valid!  Ranges must be in the right format or blank.");
            }
            else if (question.QuestionnaireQuestionTypeID == (int)QuestionnaireQuestionType.TYPE.Boolean)
            {
                question.PassingCondition = (pcbool == null) ? null : pcbool.ToString();
            }
            else
            {
                question.PassingCondition = null;
            }

            // Parse DefaultAnswer depending on the question type
            if (question.QuestionnaireQuestionTypeID == (int)QuestionnaireQuestionType.TYPE.DropDown)
            {
                // check valid answer is one of the drop downs?
            }
            else if (   question.QuestionnaireQuestionTypeID == (int)QuestionnaireQuestionType.TYPE.Integer
                     || question.QuestionnaireQuestionTypeID == (int)QuestionnaireQuestionType.TYPE.Decimal
                     || question.QuestionnaireQuestionTypeID == (int)QuestionnaireQuestionType.TYPE.Timestamp)
            {
                if (question.IsValidDefault(question.DefaultAnswer) == false)
                    ModelState.AddModelError("DefaultAnswer", "Value is not valid!  Default must be in the right format or blank.");
            }
            else if (question.QuestionnaireQuestionTypeID == (int)QuestionnaireQuestionType.TYPE.Boolean)
            {
                question.DefaultAnswer = (defbool == null) ? null : defbool.ToString();
            }

            if (ModelState.IsValid)
            {
                QuestionnaireQuestion existing = db.QuestionnaireQuestions.Find(question.ID);
                DateTime now = DateTime.UtcNow;
                existing.QuestionText = question.QuestionText;
                existing.QuestionnaireQuestionTypeID = question.QuestionnaireQuestionTypeID;
                existing.AnswersFormat = question.AnswersFormat; // If question type expects a range, set above
                existing.PassingCondition = question.PassingCondition; // If passing condition expects a range, set above
                existing.IsRequired = question.IsRequired;
                existing.DefaultAnswer = question.DefaultAnswer;
                existing.IncludeNotes = question.IncludeNotes;
                existing.SortNum = question.SortNum;
                if (DeleteFlag)
                {
                    existing.DeleteDateUTC = now;
                    existing.DeletedByUser = User.Identity.Name;
                }
                else
                {
                    existing.DeleteDateUTC = null;
                    existing.DeletedByUser = null;
                }

                db.Entry(existing).State = EntityState.Modified;
                db.SaveChanges(User.Identity.Name, now);

                TempData["success"] = "Question updated successfully";
                return RedirectToAction("Edit", new { id = question.QuestionnaireTemplateID });
            }

            ViewBag.DeleteFlag = DeleteFlag;
            ViewBag.TemplateID = question.QuestionnaireTemplateID;
            ViewBag.Template = db.QuestionnaireTemplates.Find(question.QuestionnaireTemplateID).Name;
            ViewBag.QuestionnaireQuestionTypeID = new SelectList(db.QuestionnaireQuestionTypes, "ID", "QuestionType", question.QuestionnaireQuestionTypeID);
            ViewBag.DefaultSort = question.SortNum;
            ViewBag.Min = min;
            ViewBag.Max = max;
            ViewBag.pcMin = pcmin;
            ViewBag.pcMax = pcmax;
            ViewBag.pcBool = pcbool;
            ViewBag.defBool = defbool;
            ViewBag.LoadDefaults = db.QuestionnaireTemplates.Find(question.QuestionnaireTemplateID).LoadDefaults;
            return View(question);
        }


        /// --------------------------------------------------------------------------------------------
        /// <summary>
        ///     Beginning of the create questionnaire wizard.
        /// </summary>
        /// <remarks>~/Questionnaires/Create</remarks>
        /// --------------------------------------------------------------------------------------------
        [Authorize(Roles = "createQuestionnaires")]
        public ActionResult Create()
        {
            ViewBag.CarrierID = new SelectList(db.Carriers.Where(x => x.DeleteDateUTC == null).OrderBy(x => x.Name), "ID", "Name");
            ViewBag.QuestionnaireTemplateTypeID = new SelectList(db.QuestionnaireTemplateTypes.Where(x => x.DeleteDateUTC == null), "ID", "Name");
            return View("Wizard", new QuestionnaireTemplate());
        }


        /// --------------------------------------------------------------------------------------------
        /// <summary>
        ///     Part 1 of the create questionnaire wizard.  Adds template with basic info.
        /// </summary>
        /// <param name="template">The template for the questionnaire being completed</param>
        /// <param name="btnNext">Next button on the wizard, attempt to move to next step</param>
        /// <remarks>~/Questionnaires/Wizard/ (POST only, url will not work)</remarks>
        /// --------------------------------------------------------------------------------------------
        [HttpPost]
        [Authorize(Roles = "createQuestionnaires")]
        public ActionResult Wizard(QuestionnaireTemplate template, string btnNext)
        {
            if (btnNext != null)
            {
                if (db.QuestionnaireTemplates.FirstOrDefault(t => t.Name == template.Name && (template.EffectiveDate <= t.EffectiveDate && template.EndDate >= t.EffectiveDate || template.EffectiveDate <= t.EndDate && template.EndDate >= t.EndDate || template.EffectiveDate >= t.EffectiveDate && template.EndDate <= t.EndDate)) != null)
                {
                    ModelState.AddModelError("Name", "A template with that name already exists for an overlapping date range");
                }
                if (template.EffectiveDate > template.EndDate)
                {
                    ModelState.AddModelError("EndDate", "End date cannot be before effective date");
                }

                if (ModelState.IsValid)
                {
                    db.QuestionnaireTemplates.Add(template);
                    db.SaveChanges(User.Identity.Name);

                    ViewBag.QuestionnaireQuestionTypeID = new SelectList(db.QuestionnaireQuestionTypes, "ID", "QuestionType");
                    ViewBag.TemplateID = template.ID;
                    ViewBag.Template = template.Name;
                    ViewBag.LoadDefaults = template.LoadDefaults;
                    return View("Wizard2", new QuestionnaireQuestion() { QuestionnaireTemplateID = template.ID });
                    //return View("Wizard2");
                }
            }
            ViewBag.CarrierID = new SelectList(db.Carriers.Where(x => x.DeleteDateUTC == null).OrderBy(x => x.Name), "ID", "Name", template.CarrierID);
            ViewBag.QuestionnaireTemplateTypeID = new SelectList(db.QuestionnaireTemplateTypes.Where(x => x.DeleteDateUTC == null), "ID", "Name", template.QuestionnaireTemplateTypeID);
            return View(template);
        }


        /// --------------------------------------------------------------------------------------------
        /// <summary>
        ///     Part 2 of the create questionnaire wizard.  Adds questions to the wizard.
        /// </summary>
        /// <param name="question">Question to add</param>
        /// <param name="min">Minimum value allowed for questions with ranges, tied to AnswerFormat</param>
        /// <param name="max">Maximum value allowed for questions with ranges, tied to AnswerFormat</param>
        /// <param name="pcmin">Passing condition lower limit for questions with ranges, tied to PassingCondition</param>
        /// <param name="pcmax">Passing condition upper limit for questions with ranges, tied to PassingCondition</param>
        /// <param name="pcbool">Passing condition for true/false questions, tied to PassingCondition</param>
        /// <param name="defbool">Default value for true/false questions, tied to DefaultAnswer</param>
        /// <param name="btnPrev">Previous button on the wizard, attempt to move to previous step</param>
        /// <param name="btnNext">Next button on the wizard, attempt to move to next step</param>
        /// <param name="btnAdd">Add question button on the wizard, attempt to add a question to the template</param>
        /// <remarks>~/Questionnaires/Wizard2/ (POST only, url will not work)</remarks>
        /// --------------------------------------------------------------------------------------------
        [HttpPost]
        [Authorize(Roles = "createQuestionnaires")]
        public ActionResult Wizard2(QuestionnaireQuestion question, string min, string max, string pcmin, string pcmax, bool? pcbool, bool? defbool, string btnNext = null, string btnPrev = null, string btnAdd = null)
        {
            if (btnPrev != null)
            {
            }
            else if (btnNext != null)
            {
                return View("Wizard3", db.QuestionnaireTemplates.Find(question.QuestionnaireTemplateID));
            }
            else if (btnAdd != null)
            {
                // Parse AnswersFormat depending on question type
                if (question.QuestionnaireQuestionTypeID == (int)QuestionnaireQuestionType.TYPE.DropDown)
                {
                    if (String.IsNullOrEmpty(question.AnswersFormat))
                        ModelState.AddModelError("AnswersFormat", "Format expected for Drop Down fields.  Please enter a list of options to choose from");
                }
                else if (question.QuestionnaireQuestionTypeID == (int)QuestionnaireQuestionType.TYPE.Integer
                    || question.QuestionnaireQuestionTypeID == (int)QuestionnaireQuestionType.TYPE.Decimal
                    || question.QuestionnaireQuestionTypeID == (int)QuestionnaireQuestionType.TYPE.Timestamp)
                {
                    if (question.IsValidRange(min, max))
                    {
                        if (String.IsNullOrEmpty(min) && String.IsNullOrEmpty(max))
                            question.AnswersFormat = null;
                        else
                            question.AnswersFormat = min + QuestionnaireQuestion.DROPDOWNDELIMITER + max;
                    }
                    else
                        ModelState.AddModelError("AnswersFormat", "Min and max values are not valid!  Ranges must be in the right format or blank.");
                }
                else
                {
                    question.AnswersFormat = null; // clear field
                }

                // Parse PassingCondition depending on the question type
                if (question.QuestionnaireQuestionTypeID == (int)QuestionnaireQuestionType.TYPE.DropDown)
                {
                    // check valid answer is one of the drop downs?
                }
                else if (   question.QuestionnaireQuestionTypeID == (int)QuestionnaireQuestionType.TYPE.Integer
                         || question.QuestionnaireQuestionTypeID == (int)QuestionnaireQuestionType.TYPE.Decimal
                         || question.QuestionnaireQuestionTypeID == (int)QuestionnaireQuestionType.TYPE.Timestamp)
                {
                    if (question.IsValidRange(pcmin, pcmax))
                    {
                        if (String.IsNullOrEmpty(pcmin) && String.IsNullOrEmpty(pcmax))
                            question.PassingCondition = null;
                        else
                            question.PassingCondition = pcmin + QuestionnaireQuestion.DROPDOWNDELIMITER + pcmax;
                    }
                    else
                        ModelState.AddModelError("PassingCondition", "Min and max values are not valid!  Ranges must be in the right format or blank.");
                }
                else if (question.QuestionnaireQuestionTypeID == (int)QuestionnaireQuestionType.TYPE.Boolean)
                {
                    question.PassingCondition = pcbool.ToString();
                }
                else
                {
                    question.PassingCondition = null;
                }

                // Parse DefaultAnswer depending on the question type
                if (question.QuestionnaireQuestionTypeID == (int)QuestionnaireQuestionType.TYPE.DropDown)
                {
                    // check valid answer is one of the drop downs?
                }
                else if (   question.QuestionnaireQuestionTypeID == (int)QuestionnaireQuestionType.TYPE.Integer
                         || question.QuestionnaireQuestionTypeID == (int)QuestionnaireQuestionType.TYPE.Decimal
                         || question.QuestionnaireQuestionTypeID == (int)QuestionnaireQuestionType.TYPE.Timestamp)
                {
                    if (question.IsValidDefault(question.DefaultAnswer) == false)
                        ModelState.AddModelError("DefaultAnswer", "Value is not valid!  Default must be in the right format or blank.");
                }
                else if (question.QuestionnaireQuestionTypeID == (int)QuestionnaireQuestionType.TYPE.Boolean)
                {
                    question.DefaultAnswer = defbool.ToString();
                }


                if (ModelState.IsValid)
                {
                    db.QuestionnaireQuestions.Add(question);
                    db.SaveChanges(User.Identity.Name);
                    question = new QuestionnaireQuestion() { QuestionnaireTemplateID = question.QuestionnaireTemplateID }; // clear form
                    min = null;
                    max = null;
                }
            }
            ViewBag.QuestionnaireQuestionTypeID = new SelectList(db.QuestionnaireQuestionTypes, "ID", "QuestionType", question.QuestionnaireQuestionTypeID);
            QuestionnaireTemplate template = db.QuestionnaireTemplates.Find(question.QuestionnaireTemplateID);
            ViewBag.TemplateID = template.ID;
            ViewBag.Template = template.Name;
            ViewBag.LoadDefaults = template.LoadDefaults;
            ViewBag.QuestionCount = template.QuestionnaireQuestions.Count;
            ViewBag.Min = min;
            ViewBag.Max = max;
            ViewBag.pcBool = pcbool;
            ViewBag.defBool = defbool;
            return View(question);
        }


        /// --------------------------------------------------------------------------------------------
        /// <summary>
        ///     Part 3 of the create questionnaire wizard.  Confirms the form, can add signature.
        /// </summary>
        /// <param name="template">The questionnaire that is being added</param>
        /// <param name="IncludeSignature">Flag whether to add a final Signature question to the new questionnaire</param>
        /// <param name="btnPrev">Previous button on the wizard, attempt to move to previous step</param>
        /// <param name="btnNext">Next button on the wizard, attempt to move to next step</param>
        /// <remarks>~/Questionnaires/Wizard3/ (POST only, url will not work)</remarks>
        /// --------------------------------------------------------------------------------------------
        [HttpPost]
        [Authorize(Roles = "createQuestionnaires")]
        public ActionResult Wizard3(QuestionnaireTemplate template, bool IncludeSignature, string btnNext = null, string btnPrev = null)
        {
            if (btnPrev != null)
            {
                ViewBag.QuestionnaireQuestionTypeID = new SelectList(db.QuestionnaireQuestionTypes, "ID", "QuestionType");
                ViewBag.TemplateID = template.ID;
                ViewBag.Template = template.Name;
                ViewBag.QuestionCount = db.QuestionnaireTemplates.Find(template.ID).QuestionnaireQuestions.Count;
                //return View("Wizard2");
                return View("Wizard2", new QuestionnaireQuestion() { QuestionnaireTemplateID = template.ID });
            }
            else if (btnNext != null)
            {
                if (IncludeSignature)
                {
                    QuestionnaireQuestion question = new QuestionnaireQuestion()
                    {
                        QuestionText = "Signature",
                        QuestionnaireQuestionTypeID = (int)QuestionnaireQuestionType.TYPE.Blob,
                        QuestionnaireTemplateID = template.ID,
                        IsRequired = true,
                        SortNum = db.QuestionnaireTemplates.Find(template.ID).QuestionnaireQuestions.Count + 1
                    };
                    db.QuestionnaireQuestions.Add(question);
                    db.SaveChanges(User.Identity.Name);
                }
                return RedirectToAction("Preview", new { ID = template.ID });
            }
            return View();
        }


        /// --------------------------------------------------------------------------------------------
        /// <summary>
        ///     Edit a questionnaire.  Locked entries can only extend enddate.
        /// </summary>
        /// <param name="id">Database ID of the questionnaire template</param>
        /// <remarks>~/Questionnaires/Edit/{id}</remarks>
        /// --------------------------------------------------------------------------------------------
        [Authorize(Roles = "editQuestionnaires")]
        public ActionResult Edit(int id)
        {
            QuestionnaireTemplate template = db.QuestionnaireTemplates.Find(id);
            if (template == null)
            {
                TempData["error"] = "Cannot find that template!";
                return RedirectToAction("Index");
            }
            if (HasAccess(template) == false)
            {
                TempData["error"] = "You do not have permission to that template!";
                return RedirectToAction("Index");
            }

            ViewBag.Locked = template.Locked;
            ViewBag.QuestionnaireTemplateTypeID = new SelectList(db.QuestionnaireTemplateTypes, "ID", "Name", template.QuestionnaireTemplateTypeID);
            ViewBag.ExceptionFormat = new SelectList(getEmailFormats(), "Value", "Text", template.ExceptionFormat);
            ViewBag.CarrierID = new SelectList(db.Carriers.OrderBy(x => x.Name), "ID", "Name", template.CarrierID);
            return View(template);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "editQuestionnaires")]
        public ActionResult Edit([Bind(Include = "ID,Name,QuestionnaireTemplateTypeID,EffectiveDate,EndDate,CarrierID,LoadDefaults,ExceptionEmail,ExceptionFormat")] QuestionnaireTemplate updated)
        {
            QuestionnaireTemplate existing = db.QuestionnaireTemplates.Find(updated.ID);
            if (db.QuestionnaireTemplates.FirstOrDefault(t => t.ID != updated.ID && t.Name == updated.Name && (updated.EffectiveDate <= t.EffectiveDate && updated.EndDate >= t.EffectiveDate || updated.EffectiveDate <= t.EndDate && updated.EndDate >= t.EndDate || updated.EffectiveDate >= t.EffectiveDate && updated.EndDate <= t.EndDate)) != null)
            {
                ModelState.AddModelError("Name", "A template with that name already exists for an overlapping date range");
            }
            if (updated.EffectiveDate > updated.EndDate)
            {
                ModelState.AddModelError("EndDate", "End date cannot be before effective date");
            }
            if (updated.EndDate != existing.EndDate && updated.EndDate < DateTime.Today)
            {
                ModelState.AddModelError("EndDate", "End date cannot be set in the past");
            }

            if (ModelState.IsValid)
            {
                if (!existing.Locked)
                {
                    existing.Name = updated.Name;
                    existing.QuestionnaireTemplateTypeID = updated.QuestionnaireTemplateTypeID;
                    existing.CarrierID = updated.CarrierID;
                    existing.EffectiveDate = updated.EffectiveDate;
                    existing.LoadDefaults = updated.LoadDefaults;
                }
                existing.EndDate = updated.EndDate;
                existing.ExceptionEmail = updated.ExceptionEmail;
                existing.ExceptionFormat = updated.ExceptionFormat;

                db.Entry(existing).CurrentValues.SetValues(existing);
                //db.Entry(existing).State = EntityState.Modified;
                db.SaveChanges(User.Identity.Name, DateTime.UtcNow);

                TempData["success"] = "Template for " + updated.Name + " was updated successfully";
                return RedirectToAction("Index");
            }

            ViewBag.Locked = existing.Locked;
            ViewBag.QuestionnaireTemplateTypeID = new SelectList(db.QuestionnaireTemplateTypes, "ID", "Name", updated.QuestionnaireTemplateTypeID);
            ViewBag.ExceptionFormat = new SelectList(getEmailFormats(), "Value", "Text", updated.ExceptionFormat);
            ViewBag.CarrierID = new SelectList(db.Carriers.OrderBy(x => x.Name), "ID", "Name", updated.CarrierID);
            return View(updated);
        }


        /// --------------------------------------------------------------------------------------------
        /// <summary>
        ///     Deactivates a questionnaire (unlocked entries only).
        /// </summary>
        /// <param name="id">Database ID of the questionnaire template</param>
        /// <remarks>~/Questionnaires/Deactivate/{id}</remarks>
        /// --------------------------------------------------------------------------------------------
        [Authorize(Roles = "deactivateQuestionnaires")]
        public ActionResult Deactivate(int id)
        {
            QuestionnaireTemplate template = db.QuestionnaireTemplates.Find(id);
            if (template == null)
            {
                TempData["error"] = "Cannot find that template!";
                return RedirectToAction("Index");
            }
            if (HasAccess(template) == false)
            {
                TempData["error"] = "You do not have permission to that template!";
                return RedirectToAction("Index");
            }
            if (template.Locked)
            {
                TempData["error"] = "Cannot deactivate a locked template!";
                return RedirectToAction("Index");
            }

            foreach (var question in db.QuestionnaireQuestions.Where(q => q.QuestionnaireTemplateID == template.ID))
            {
                question.DeleteDateUTC = DateTime.UtcNow;
                question.DeletedByUser = User.Identity.Name;
            }

            template.DeleteDateUTC = DateTime.UtcNow;
            template.DeletedByUser = User.Identity.Name;
            db.SaveChanges(User.Identity.Name);

            TempData["success"] = "Template for " + template.Name + " was deactivated";
            return RedirectToAction("Index");
        }


        /// --------------------------------------------------------------------------------------------
        /// <summary>
        ///     Reactivate a questionnaire (unlocked entries only).
        /// </summary>
        /// <param name="id">Database ID of the questionnaire template</param>
        /// <remarks>~/Questionnaires/Reactivate/{id}</remarks>
        /// --------------------------------------------------------------------------------------------
        [Authorize(Roles = "deactivateQuestionnaires")]
        public ActionResult Reactivate(int id)
        {
            QuestionnaireTemplate template = db.QuestionnaireTemplates.Find(id);
            if (template == null)
            {
                TempData["error"] = "Cannot find that template!";
                return RedirectToAction("Index");
            }
            if (HasAccess(template) == false)
            {
                TempData["error"] = "You do not have permission to that template!";
                return RedirectToAction("Index");
            }
            if (template.Locked)
            {
                TempData["error"] = "Cannot reactivate a locked template!";
                return RedirectToAction("Index");
            }
            if (db.QuestionnaireTemplates.FirstOrDefault(t => t.ID != template.ID && t.Name == template.Name && (template.EffectiveDate <= t.EffectiveDate && template.EndDate >= t.EffectiveDate || template.EffectiveDate <= t.EndDate && template.EndDate >= t.EndDate || template.EffectiveDate >= t.EffectiveDate && template.EndDate <= t.EndDate)) != null)
            {
                TempData["error"] = "A template with that name already exists for an overlapping date range!";
                return RedirectToAction("Index");
            }

            // won't be able to determine which questions were deleted individually before and which were done by a delete questionnaire
            foreach (var question in db.QuestionnaireQuestions.Where(q => q.QuestionnaireTemplateID == template.ID))
            {
                question.DeleteDateUTC = null;
                question.DeletedByUser = null;
            }

            template.DeleteDateUTC = null;
            template.DeletedByUser = null;
            db.SaveChanges(User.Identity.Name);

            TempData["success"] = "Template for " + template.Name + " was reactived";
            return RedirectToAction("Index");
        }


        /// --------------------------------------------------------------------------------------------
        /// <summary>
        ///     A list of submissions filtered the carrier.
        /// </summary>
        /// <param name="fvm">Filter view model makes use of these fields for matching data
        ///     * StartDate - default is today
        ///     * EndDate - default is 6 days ago for a 1 week search
        ///     * QuestionnaireTemplateID - uses id parameter
        ///     * CarrierID
        ///     * DriverID
        ///     * TruckID
        ///     * TrailerID - match trailer 1 or 2
        /// </param>
        /// <param name="id">Database ID of the questionnaire template (optional)</param>
        /// <remarks>~/Questionnaires/Submissions</remarks>
        /// <returns>HOS export zip file</returns>
        /// --------------------------------------------------------------------------------------------
        public ActionResult Submissions(FilterViewModel<QuestionnaireSubmission> fvm, int? id)
        {
            if (id != null)
                fvm.QuestionnaireTemplateID = id;
            if (fvm.StartDate == null)
                fvm.StartDate = DateTime.Today.AddDays(-6);
            if (fvm.EndDate == null)
                fvm.EndDate = DateTime.Today;

            // Filter templates that use my carrier
            int myDriverID = Converter.ToInt32(Profile.GetPropertyValue("DriverID"));
            int myCarrierID = Converter.ToInt32(Profile.GetPropertyValue("CarrierID"));
            IQueryable<QuestionnaireSubmission> submissions = db.QuestionnaireSubmissions.Where(s => myCarrierID == -1 || (s.QuestionnaireTemplate.CarrierID == null) || s.QuestionnaireTemplate.CarrierID == myCarrierID);

            ViewBag.CarrierID = new SelectList(db.Carriers.Where(c => myCarrierID == -1 || c.ID == myCarrierID).OrderBy(x => x.Name), "ID", "Name", fvm.CarrierID);
            ViewBag.QuestionnaireTemplateID = new SelectList(db.QuestionnaireTemplates.Where(s => myCarrierID == -1 || (s.CarrierID == null) || s.CarrierID == myCarrierID).OrderBy(x => x.Name), "ID", "Name", fvm.QuestionnaireTemplateID);
            ViewBag.QuestionnaireTemplateTypeID = new SelectList(db.QuestionnaireTemplateTypes.OrderBy(t => t.Name), "ID", "Name", fvm.QuestionnaireTemplateTypeID);
            ViewBag.DriverID = new SelectList(db.Drivers.Where(s => myCarrierID == -1 || s.CarrierID == myCarrierID).OrderBy(x => x.FirstName).ThenBy(x => x.LastName), "ID", "FullName", fvm.DriverID);
            ViewBag.TruckID = new SelectList(db.Trucks.Where(s => myCarrierID == -1 || s.CarrierID == myCarrierID).OrderBy(x => x.IDNumber), "ID", "IDNumber", fvm.TruckID);
            ViewBag.TrailerID = new SelectList(db.Trailers.Where(s => myCarrierID == -1 || s.CarrierID == myCarrierID).OrderBy(x => x.IDNumber), "ID", "IDNumber", fvm.TrailerID);

            // Apply additional filters
            submissions = submissions.Where(s => s.SubmissionDateUTC >= fvm.StartDate.Value);
            DateTime end = fvm.EndDate.Value.Date.AddDays(1);
            submissions = submissions.Where(s => s.SubmissionDateUTC < end);

            if (fvm.CarrierID != null)
                submissions = submissions.Where(s => s.QuestionnaireTemplate.CarrierID == null || s.QuestionnaireTemplate.CarrierID == fvm.CarrierID);
            if (fvm.QuestionnaireTemplateTypeID != null)
                submissions = submissions.Where(s => s.QuestionnaireTemplate.QuestionnaireTemplateTypeID == fvm.QuestionnaireTemplateTypeID);
            if (fvm.DriverID != null)
                submissions = submissions.Where(s => s.DriverID == fvm.DriverID);
            if (fvm.TruckID != null)
                submissions = submissions.Where(s => s.TruckID == fvm.TruckID);
            if (fvm.TrailerID != null)
                submissions = submissions.Where(s => s.TrailerID == fvm.TrailerID || s.Trailer2ID == fvm.TrailerID);

            if (fvm.QuestionnaireTemplateID != null && fvm.QuestionnaireTemplateID > 0)
            {
                QuestionnaireTemplate template = db.QuestionnaireTemplates.Find(fvm.QuestionnaireTemplateID);
                if (template == null)
                {
                    TempData["error"] = "Cannot find that template!";
                    return View();
                }
                if (HasAccess(template) == false)
                {
                    // check driver? can see others' submissions
                    TempData["error"] = "You do not have access to those submissions!";
                    return View();
                }
                fvm.results = submissions.Where(s => s.QuestionnaireTemplateID == fvm.QuestionnaireTemplateID).ToList();
            }
            else
                fvm.results = submissions.ToList();

            return View(fvm);
        }


        /// --------------------------------------------------------------------------------------------
        /// <summary>
        ///     Dispalys a submitted questionnaire.
        /// </summary>
        /// <param name="id">Database ID of the questionnaire submission</param>
        /// <remarks>~/Questionnaires/Submission/{id}</remarks>
        /// <returns>HOS export zip file</returns>
        /// --------------------------------------------------------------------------------------------
        public ActionResult Submission(int id)
        {
            QuestionnaireSubmission submission = db.QuestionnaireSubmissions.Find(id);
            if (submission == null)
            {
                TempData["error"] = "Cannot find that submission!";
                return RedirectToAction("Submissions");
            }
            if (HasAccess(submission.QuestionnaireTemplate) == false)
            {
                // check driver? can see others' submissions
                TempData["error"] = "You do not have access to that submission!";
                return RedirectToAction("Submissions");
            }
            ViewBag.HideLinks = false;
            return View("Submission", submission);
        }


        /// --------------------------------------------------------------------------------------------
        /// <summary>
        ///     Exports the submitted questionnaire as a PDF file.
        /// </summary>
        /// <param name="id">Database ID of the questionnaire submission</param>
        /// <remarks>~/Questionnaires/ExportSubmission/{id}</remarks>
        /// --------------------------------------------------------------------------------------------
        public ActionResult ExportSubmission(int id)
        {
            QuestionnaireSubmission submission = db.QuestionnaireSubmissions.Find(id);
            if (submission == null)
            {
                TempData["error"] = "Cannot find that submission!";
                return RedirectToAction("Submissions");
            }
            if (HasAccess(submission.QuestionnaireTemplate) == false)
            {
                // check driver? can see others' submissions
                TempData["error"] = "You do not have access to that submission!";
                return RedirectToAction("Submissions");
            }

            ViewBag.HideLinks = true;
            String content = ControllerExtensions.RenderViewToString(this, "Submission", submission, "~/Views/Shared/_Empty.cshtml");

            HiQPdf.HtmlToPdf converter = new HiQPdf.HtmlToPdf();
            converter.BrowserWidth = 900;
            converter.SerialNumber = "ezMSKisf-HTcSGQka-CQJNVUtb-SltJW0NO-SFtISlVK-SVVCQkJC";

            String filename = submission.QuestionnaireTemplate.Name + submission.SubmissionDateUTC + ".pdf";

            GC.Collect();
            return File(converter.ConvertHtmlToMemory(content, null), "Application/pdf", filename);
        }


        /// --------------------------------------------------------------------------------------------
        /// <summary>
        ///     Checks the user profile to see confirm if a user has access to the template.
        /// </summary>
        /// <param name="template">Questionnaire template to check</param>
        /// --------------------------------------------------------------------------------------------
        public bool HasAccess(QuestionnaireTemplate template)
        {
            // use profile to determine if a user can work with this template
            int myCarrierID = Converter.ToInt32(Profile.GetPropertyValue("CarrierID"));
            int myDriverID = Converter.ToInt32(Profile.GetPropertyValue("DriverID"));
            return (myCarrierID == -1) || (template.CarrierID == null) || (myCarrierID == template.CarrierID);
        }




        /// --------------------------------------------------------------------------------------------
        /// <summary>
        ///     Maintenance page for managing template types.
        /// </summary>
        /// <remarks>~/Questionnaires/Types/</remarks>
        /// --------------------------------------------------------------------------------------------
        public ActionResult Types()
        {
            return View("TemplateTypes");
        }


        /// --------------------------------------------------------------------------------------------
        /// <summary>
        ///     Get list of questionnaire template types
        /// </summary>
        /// <param name="id">[AJAX call - grid editing]</param>
        /// <param name="request">[AJAX call - grid editing]</param>
        /// <remarks>Read function for TemplateTypes grid [Uses Telerik MVC grid with AJAX]</remarks>
        /// --------------------------------------------------------------------------------------------
        public ActionResult TemplateType_Read([DataSourceRequest] DataSourceRequest request = null, int id = 0)
        {
            return TemplateType_Read(request, null, id);
        }

        private ContentResult TemplateType_Read([DataSourceRequest] DataSourceRequest request, ModelStateDictionary modelState, int id = 0)
        {
            // .ToList() is required due to filtering on "Active" field, which is NOT MAPPED
            var data = db.QuestionnaireTemplateTypes.Where(m => m.ID == id || id == 0).ToList();
            var result = data.ToDataSourceResult(request, modelState);
            return App_Code.JsonStringResult.Create(result);
        }


        /// --------------------------------------------------------------------------------------------
        /// <summary>
        ///     Adds a non-system template type.
        /// </summary>
        /// <param name="request">[AJAX call - grid editing]</param>
        /// <remarks>Create function for TemplateTypes grid [Uses Telerik MVC grid with AJAX]</remarks>
        /// --------------------------------------------------------------------------------------------
        [HttpPost]
        [Authorize(Roles = "createQuestionnaires")]
        public ActionResult TemplateType_Create([DataSourceRequest] DataSourceRequest request, QuestionnaireTemplateType templateType)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // ensure all new Units of measure are marked "Active" (otherwise they will be created deleted)
                    templateType.Active = true;

                    // Save new record to the database
                    db.QuestionnaireTemplateTypes.Add(templateType);
                    db.SaveChanges(User.Identity.Name);  // Create dates & usernames will all be taken care of by this line.
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(null, ex.Message); // TODO: use a common routine to "cleanup" db generated errors
                    return App_Code.JsonStringResult.Create(new[] { templateType }.ToDataSourceResult(request, ModelState));
                }
            }

            return TemplateType_Read(request, ModelState, templateType.ID);
        }


        /// --------------------------------------------------------------------------------------------
        /// <summary>
        ///     Updates the template type (non-system only)
        /// </summary>
        /// <param name="templateType">Template Type to update</param>
        /// <param name="request">[AJAX call - grid editing]</param>
        /// <remarks>Edit function for TemplateTypes grid [Uses Telerik MVC grid with AJAX]</remarks>
        /// --------------------------------------------------------------------------------------------
        [HttpPost]
        [Authorize(Roles = "editQuestionnaires")]
        public ActionResult TemplateType_Update(DataSourceRequest request, QuestionnaireTemplateType templateType)
        {
            QuestionnaireTemplateType existing = db.QuestionnaireTemplateTypes.Find(templateType.ID);
            if (existing.IsSystem)
            {
                ModelState.AddModelError("Name", "Cannot edit a system type!");
            }

            if (ModelState.IsValid)
            {
                //db.QuestionnaireTemplateTypes.Attach(templateType);
                existing.Name = templateType.Name;

                db.Entry(existing).State = EntityState.Modified;
                db.SaveChanges(User.Identity.Name);
                return RedirectToAction("Types", new RouteValueDictionary { { "grid-filter", Request.QueryString["grid-filter"] },
                                                                            { "grid-sort", Request.QueryString["grid-sort"]},
                                                                            { "grid-page", Request.QueryString["grid-page"]},
                                                                            { "grid-pageSize", Request.QueryString["grid-pageSize"]},
                                                                            { "grid-group", Request.QueryString["grid-group"]}});
            }

            ViewBag.Title = "Template Types";
            var types = db.QuestionnaireTemplateTypes;
            if (Request.QueryString.Keys.Count == 0 || Request.QueryString.Keys.Count == 1 && Request.QueryString["grid-mode"] != null)
                ViewBag.Load = "Initial";

            return TemplateType_Read(request, ModelState, templateType.ID);
        }


        /// --------------------------------------------------------------------------------------------
        /// <summary>
        ///     Deactivates/reactivates the template type (non-system only)
        /// </summary>
        /// <param name="qtt">Questionnaire Template Type</param>
        /// <remarks>Delete function for TemplateTypes grid [Uses Telerik MVC grid with AJAX]</remarks>
        /// --------------------------------------------------------------------------------------------
        [Authorize(Roles = "deactivateQuestionnaires")]
        public ActionResult Delete(QuestionnaireTemplateType qtt)
        {
            db.QuestionnaireTemplateTypes.Attach(qtt);
            db.QuestionnaireTemplateTypes.Remove(qtt);
            db.SaveChanges(User.Identity.Name);

            return null;
        }



        /// --------------------------------------------------------------------------------------------
        /// <summary>
        ///     Exports all submissions for a template for the given date range as an Excel download.  
        ///         The first worksheet is a summary tab, the second displays details and answers.  All 
        ///         questions are columnar format with each submission as a row.
        /// </summary>
        /// <param name="fvm">Filter view model makes use of these fields for matching data
        ///     * StartDate - Default yesterday
        ///     * EndDate - Default yesterday for a single-day range
        ///     * QuestionnaireTemplateID - questionnaire template (must be set so questions and answers match)
        ///     * CarrierID
        ///     * QuestionnaireTemplateTypeID
        ///     * DriverID
        ///     * TruckID
        ///     * TrailerID
        /// </param>
        /// <returns>Questionnaire submission Excel export file</returns>
        /// --------------------------------------------------------------------------------------------
        public ActionResult ExportSubmissions(FilterViewModel<QuestionnaireSubmission> fvm)
        {
            if (fvm.StartDate == null) fvm.StartDate = DateTime.Today.AddDays(-1);
            if (fvm.EndDate == null) fvm.EndDate = DateTime.Today.AddDays(-1);

            QuestionnaireTemplate template = db.QuestionnaireTemplates.Find(fvm.QuestionnaireTemplateID);
            if (template == null)
            {
                TempData["error"] = "Please select a valid questionnaire!";
                return RedirectToAction("Submissions", fvm);
            }
            if (fvm.StartDate == null || fvm.EndDate == null)
            {
                TempData["error"] = "Please select a valid date range!";
                return RedirectToAction("Submissions", fvm);
            }
            if (HasAccess(template) == false)
            {
                // check driver? can see others' submissions
                TempData["error"] = "You do not have access to that submission!";
                return RedirectToAction("Submissions", fvm);
            }
            return File(ExportToExcel(fvm.QuestionnaireTemplateID.Value, fvm.StartDate.Value, fvm.EndDate.Value, 
                                        fvm.CarrierID, fvm.QuestionnaireTemplateTypeID, fvm.DriverID, fvm.TruckID, fvm.TrailerID), "Application/excel", string.Format("{0}_Submissions.xlsx", template.Name));
        }

        /// --------------------------------------------------------------------------------------------
        /// <summary>
        ///     Called by the export page, this function actually creates the excel file.
        /// </summary>
        /// <param name="templateID">Database ID of the questionnaire template</param>
        /// <param name="startdate">Start date to query</param>
        /// <param name="enddate">End date to query</param>
        /// <param name="carrierID">Database ID of the carrier</param>
        /// <param name="templateTypeID">Database ID of the questionnaire template type</param>
        /// <param name="driverID">Database ID of the driver</param>
        /// <param name="truckID">Database ID of the truck</param>
        /// <param name="trailerID">Database ID of the trailer</param>
        /// <param name="submissionID">Database ID of the questionnaire submission</param>
        /// <returns>Data stream for creating an Excel file</returns>
        /// --------------------------------------------------------------------------------------------
        private Stream ExportToExcel(int templateID, DateTime startdate, DateTime enddate, int? carrierID, int? templateTypeID, int? driverID, int? truckID, int? trailerID, int? submissionID = null)
        {
            MemoryStream ret = null;

            using (SSDB ssdb = new SSDB())
            {
                // Get basic questionnaire info
                DataTable dtTemplate = ssdb.GetPopulatedDataTable(
                            "SELECT qt.Name, qt.EffectiveDate, qt.EndDate, c.Name AS Carrier, " +
                                   "0 AS NumSubmissions, " + // filled in later
                                   "'{1}' AS RangeStartDate, " +
                                   "'{2}' AS RangeEndDate " +
                              "FROM tblQuestionnaireTemplate qt " +
                              "LEFT JOIN tblCarrier c ON qt.CarrierID = c.ID " +
                             "WHERE qt.ID={0}", templateID, startdate.Date, enddate.Date, carrierID, templateTypeID, driverID, truckID, trailerID);

                if (dtTemplate.Rows.Count == 1)
                {
                    // Get basic info for submissions
                    DataTable dtSubmissions = ssdb.GetPopulatedDataTable(
                                "SELECT qs.ID, d.LastName AS 'Last Name', d.FirstName AS 'First Name', t.IDNumber AS 'Truck #', " +
                                       "dbo.fnUTC_to_Local(SubmissionDateUTC, 1, 1) AS 'Submission Date (CST)', " +
                                       "dbo.fnUTC_to_Local(SubmissionDateUTC, SubmissionTimeZoneID, 1) AS 'Submission Date (Driver)' " +
                                  "FROM tblQuestionnaireSubmission qs " +
                                  "LEFT JOIN tblQuestionnaireTemplate qt ON qt.ID = qs.QuestionnaireTemplateID " +
                                  "LEFT JOIN tblDriver d ON d.ID = qs.DriverID " +
                                  "LEFT JOIN tblTruck t ON t.ID = qs.TruckID " +
                                  "LEFT JOIN tblTrailer tr ON tr.ID = qs.TrailerID " +
                                  "LEFT JOIN tblTrailer tr2 ON tr2.ID = qs.Trailer2ID " +
                                 "WHERE qs.QuestionnaireTemplateID = {0} " +
                                 ((submissionID != null) ? "AND (qs.ID = {8}) " : "") +
                                 ((carrierID != null) ? "AND (qt.CarrierID IS NULL OR qt.CarrierID = {3}) " : "") +
                                 ((templateTypeID != null) ? "AND qt.QuestionnaireTemplateTypeID = {4} " : "") +
                                 ((driverID != null) ? "AND d.ID = {5} " : "") +
                                 ((truckID != null) ? "AND t.ID = {6} " : "") +
                                 ((trailerID != null) ? "AND (tr.ID = {7} OR tr2.ID = {7}) " : "") +
                                 "AND CAST(qs.SubmissionDateUTC AS DATE) BETWEEN '{1}' AND '{2}'",
                                 templateID, startdate.Date, enddate.Date, carrierID, templateTypeID, driverID, truckID, trailerID, submissionID);

                    // Set number of submissions for the main tab
                    dtTemplate.Rows[0]["NumSubmissions"] = dtSubmissions.Rows.Count;
                    

                    
                    // Get active questions for this questionnaire for Columns for this template
                    DataTable dtQuestions = ssdb.GetPopulatedDataTable(
                                "SELECT ID, QuestionText AS 'Question' " +
	                              "FROM tblQuestionnaireQuestion " +
	                             "WHERE QuestionnaireTemplateID = {0} " +
                                   "AND QuestionnaireQuestionTypeID NOT IN (0, 7) " + // skip labels and blobs
                                   "AND DeleteDateUTC IS NULL", templateID); 

                    // Add each question as a column to the main submission table
                    foreach (DataRow drQuestion in dtQuestions.Rows)
                    {
                        // Use ID because a question text is not unique
                        dtSubmissions.Columns.Add(drQuestion["ID"].ToString());

                    }

                    // Get answers for each question for each submission
                    DataTable dtAnswers = ssdb.GetPopulatedDataTable("SELECT * FROM tblQuestionnaireResponse qr, tblQuestionnaireSubmission qs " +
                                   "WHERE qr.QuestionnaireSubmissionID = qs.ID " +
                                     "AND qs.QuestionnaireTemplateID = {0} " +
                                     "AND CAST(qs.SubmissionDateUTC AS DATE) BETWEEN '{1}' AND '{2}' " +
                                     ((submissionID != null) ? "AND (qs.ID = {3}) " : ""),
                                     templateID, startdate.Date, enddate.Date, submissionID);

                    // Add each answer to the table matching the column (question) and row (submission)
                    foreach (DataRow drAnswer in dtAnswers.Rows)
                    {
                        // only try to insert answer if question is valid (exists for this template)
                        if (dtSubmissions.Columns.Contains(drAnswer["QuestionnaireQuestionID"].ToString()))
                        {
                            DataRow submission = dtSubmissions.Select("ID = " + drAnswer["QuestionnaireSubmissionID"]).FirstOrDefault();
                            submission[drAnswer["QuestionnaireQuestionID"].ToString()] = drAnswer["Answer"].ToString(); 
                        }
                    }

                    // Rename each question column using the QuestionText (instead of ID)
                    foreach (DataRow drQuestion in dtQuestions.Rows)
                    {
                        dtSubmissions.Columns[drQuestion["ID"].ToString()].ColumnName = drQuestion["Question"].ToString();
                    }

                    // Grids populated, begin export
                    using (ExcelEngine excelEngine = new ExcelEngine())
                    {
                        IApplication excelApp = null;
                        IWorkbook wb = null;
                        IWorksheet ws = null;
                        int sheetID = 0;
                        try
                        {
                            excelApp = excelEngine.Excel;
                            wb = excelApp.Workbooks.Add(ExcelVersion.Excel2010);

                            // Create summary tab
                            ws = wb.Worksheets[sheetID++];
                            ws.Name = "Summary";
                            AddDataTableToWorksheet(ws, dtTemplate, 1, 1, true);
                            ws.UsedRange.AutofitColumns();

                            /*
                            // Create questions tab
                            ws = wb.Worksheets[sheetID++];
                            ws.Name = "Questions";
                            AddDataTableToWorksheet(ws, dtQuestions, 1, 1, true);
                            ws.UsedRange.AutofitColumns();
                            */
                            // Create submissions tab
                            ws = wb.Worksheets[sheetID++];
                            ws.Name = "Submissions";
                            AddDataTableToWorksheet(ws, dtSubmissions, 1, 1, true);
                            ws.Columns[4].NumberFormat = "M/d/yyyy h:mm:ss AM/PM"; // Format submission dates
                            ws.Columns[5].NumberFormat = "M/d/yyyy h:mm:ss AM/PM"; // Format driver submission dates
                            ws.UsedRange.AutofitColumns();
                        }
                        catch (Exception)
                        {
                            // TODO: handle these errors
                        }
                        ret = new MemoryStream();
                        wb.SaveAs(ret, ExcelSaveType.SaveAsXLS);
                        wb.Close();
                    }
                }
            }
            ret.Position = 0;
            return ret;
        }

        /// <summary>
        ///     Inserts DataTable into a worksheet looping through columns and rows
        /// </summary>
        /// <param name="sheet">Worksheet to add data</param>
        /// <param name="dt">Data table containing the content of the spreadsheet</param>
        /// <param name="startRow">Start row if offset is used.  Default is 1 (first row)</param>
        /// <param name="startCol">Start column if offset is used.  Default is 1 (first column)</param>
        /// <param name="addHeader">Flag whether to add a header row using the column caption.  Default is true</param>
        /// --------------------------------------------------------------------------------------------
        private void AddDataTableToWorksheet(IWorksheet sheet, DataTable dt, int startRow = 1, int startCol = 1, bool addHeader = true)
        {
            if (addHeader)
            {
                int col = startCol;
                foreach (DataColumn dc in dt.Columns)
                {
                    sheet.Range[startRow, col++].Value = dc.Caption;
                }
                startRow++;
            }
            foreach (DataRow dr in dt.Rows)
            {
                int col = startCol;
                foreach (DataColumn dc in dt.Columns)
                {
                    sheet.Range[startRow, col++].Value = dr[dc].ToString();
                }
                startRow++;
            }
        }


        private List<SelectListItem> getEmailFormats()
        {
            List<SelectListItem> formats = new List<SelectListItem>()
            {
                new SelectListItem() { Value = "HTML", Text = "HTML" },
                new SelectListItem() { Value = "Text", Text = "Text" },
                new SelectListItem() { Value = "Excel", Text = "Excel" }
            };

            // conditional formats based on account.  There should be a corresponding entry in the Send function below
            if (ConfigurationManager.AppSettings["SiteTitle"] == "Twin Eagle Transport")
            {
                formats.Add(new SelectListItem() { Value = "FileMaker", Text = "FileMaker" });
            }

            return formats;
        }


        /// --------------------------------------------------------------------------------------------
        /// <summary>
        ///     Emails a submission using exception settings.  Default format is HTML email
        /// </summary>
        /// <param name="id">Database ID of the questionnaire submission</param>
        /// <remarks>~/Questionnaires/Send/{id}</remarks>
        /// --------------------------------------------------------------------------------------------
        public ActionResult Send(int id)
        {
            QuestionnaireSubmission submission = db.QuestionnaireSubmissions.Find(id);
            if (submission.QuestionnaireTemplate.ExceptionEmail == null)
                return Json(new { status="NOT SENT - No email on file"}, JsonRequestBehavior.AllowGet);

            // *** CUSTOM OVERRIDE SECTION FOR EMAILING QUESTIONNAIRES ***
            // If none of these match, submission will be sent as standard html
            if (submission.QuestionnaireTemplate.ExceptionFormat == "FileMaker")
            {
                return SendFileMaker(submission);
            }
            else if (submission.QuestionnaireTemplate.ExceptionFormat == "Text")
            {
                return SendText(submission);
            }
            else if (submission.QuestionnaireTemplate.ExceptionFormat == "Excel")
            {
                return SendExcel(submission);
            }

            return SendEmail(submission);
        }


        /// --------------------------------------------------------------------------------------------
        /// <summary>
        ///     Emails submission in HTML format.
        /// </summary>
        /// <param name="submission">Database ID of the submission to send</param>
        /// --------------------------------------------------------------------------------------------
        private ActionResult SendEmail(QuestionnaireSubmission submission)
        {
            // Send as basic text
            string title = submission.QuestionnaireTemplate.Name + " - Exception Report";
            string htmlbody = 
                "<div><b>Driver Name:</b> " + submission.Driver.FullName + " (" + submission.Driver.IDNumber + ")</div>" +
                "<div><b>Truck #:</b> " + submission.Truck.IDNumber + "</div>" +
                "<div><b>Trailer #:</b> " + submission.Trailer.IDNumber + "</div>" +
                "<div><b>Date time:</b> " + submission.CreateDate.ToString() + "</div>" +
                "<br>" +
                "<table style=\"border: 1px solid #bbb\">" +
                    "<tr>" +
                        "<th style=\"border: 1px solid #bbb; margin-right: 60px; padding: 5px;\">Question</th>" +
                        "<th style=\"border: 1px solid #bbb; margin-right: 40px; padding: 5px;\">Answer</th>" +
                        "<th style=\"border: 1px solid #bbb; padding: 5px;\">Passing</th> " +
                        "<th style=\"border: 1px solid #bbb; margin-right: 40px; padding: 5px;\">Notes</th>" +
                    "</tr>";
            foreach (QuestionnaireResponse r in submission.QuestionnaireResponses)
            {
                htmlbody += "<tr>"+
                                "<td style=\"border: 1px solid #bbb; padding: 5px;\">" + r.QuestionnaireQuestion.QuestionText + "</td>" +
                                "<td style=\"border: 1px solid #bbb; padding: 5px;\">" + r.Answer + "</td>" +
                                "<td style=\"border: 1px solid #bbb; padding: 5px;" + (r.Passing ? "" : "background-color: #F1D0D0;") + "\">" + r.Passing + "</td>" +
                                "<td style=\"border: 1px solid #bbb; padding: 5px;\">" + r.Notes + "</td>" +
                            "</tr>";
            }
            htmlbody += 
                "</table>" +
                "<br><br>" +
                "<div><a href=\"" +  Request.Url.GetLeftPart(UriPartial.Authority) +"/Questionnaires/Submission/" + submission.ID + "\">View submission</a></div>";

            new CTSEmail(submission.QuestionnaireTemplate.ExceptionEmail, title, htmlbody, true).Send();
            return Json(new { status="OK"}, JsonRequestBehavior.AllowGet);
        }


        /// --------------------------------------------------------------------------------------------
        /// <summary>
        ///     Emails submission in standard text.
        /// </summary>
        /// <param name="submission">Database ID of the submission to send</param>
        /// --------------------------------------------------------------------------------------------
        private ActionResult SendText(QuestionnaireSubmission submission)
        {
            // Send as basic text
            string title = submission.QuestionnaireTemplate.Name + " - Exception Report";
            string body = @"
Driver Name: " + submission.Driver.FullName + " (" + submission.Driver.IDNumber + @")
Truck #: " + submission.Truck.IDNumber + @"
Trailer #: " + submission.Trailer.IDNumber + @"
Date time: " + submission.CreateDate.ToString() + @"

";

            foreach (QuestionnaireResponse r in submission.QuestionnaireResponses)
            {
                body += r.QuestionnaireQuestion.QuestionText + ": " + r.Answer +
                        (r.Passing ? "" : " - ERROR") + 
                        (string.IsNullOrEmpty(r.Notes) ? "" : " *** " + r.Notes + " ***") + @"
";
            }
            body += @"

View submission at " + Request.Url.GetLeftPart(UriPartial.Authority) +"/Questionnaires/Submission/" + submission.ID;

            new CTSEmail(submission.QuestionnaireTemplate.ExceptionEmail, title, body, false).Send();
            return Json(new { status="OK"}, JsonRequestBehavior.AllowGet);
        }
        

        /// --------------------------------------------------------------------------------------------
        /// <summary>
        ///     Emails submission with an excel export attached.
        /// </summary>
        /// <param name="submission">Database ID of the submission to send</param>
        /// --------------------------------------------------------------------------------------------
        private ActionResult SendExcel(QuestionnaireSubmission submission)
        {
            new CTSEmail(submission.QuestionnaireTemplate.ExceptionEmail, 
                submission.QuestionnaireTemplate.Name + " - Exception report", 
                @"Attached is a copy of the failing submission

View submission at " + Request.Url.GetLeftPart(UriPartial.Authority) +"/Questionnaires/Submission/" + submission.ID
                )
                .Attach(
                    ExportToExcel(submission.QuestionnaireTemplateID, submission.SubmissionDateUTC, submission.SubmissionDateUTC
                        , null, null, null, null, null, submission.ID)
                        , submission.QuestionnaireTemplate.Name + "_" + submission.Driver.FullName + "_" + submission.CreateDate.Value.ToString("yyyyMMdd") + ".xlsx"
                    )
                .Send();

            return Json(new { status="OK"}, JsonRequestBehavior.AllowGet);
        }


        /// --------------------------------------------------------------------------------------------
        /// <summary>
        ///     Custom DVIR exception email for FileMaker software program.
        /// </summary>
        /// <param name="submission">Database ID of the submission to send</param>
        /// <remarks>This is custom for TET at this time</remarks>
        /// --------------------------------------------------------------------------------------------
        private ActionResult SendFileMaker(QuestionnaireSubmission submission)
        {
            int TRACTOR_DEFECT1 = 101, TRACTOR_DETAILS1 = 102, TRACTOR_DEFECT2 = 103, TRACTOR_DETAILS2 = 104, 
                TRACTOR_DEFECT3 = 105, TRACTOR_DETAILS3 = 106, TRACTOR_DEFECT4 = 107, TRACTOR_DETAILS4 = 108;
            int TRAILER_DEFECT1 = 109, TRAILER_DETAILS1 = 110, TRAILER_DEFECT2 = 111, TRAILER_DETAILS2 = 112, 
                TRAILER_DEFECT3 = 113, TRAILER_DETAILS3 = 114, TRAILER_DEFECT4 = 115, TRAILER_DETAILS4 = 116;
            int ODOMETER = 117, FUEL = 118, TRAILER_HUB = 119;

            string title = "DVIR - FileMaker";

            string tractor_info = "";
            if ((submission.getAnswer(TRACTOR_DEFECT1)) != null)
            {
                tractor_info += @"
Tractor Defect?: Yes
Tractor Defect Details?: " + submission.getAnswer(TRACTOR_DEFECT1) + @"
Remarks: " + submission.getAnswer(TRACTOR_DETAILS1);

                if ((submission.getAnswer(TRACTOR_DEFECT2)) != null)
                {
                    tractor_info += @"
More Tractor Defects?: Yes
Tractor Defect Details?: " + submission.getAnswer(TRACTOR_DEFECT2) + @"
Remarks: " + submission.getAnswer(TRACTOR_DETAILS2);

                    if ((submission.getAnswer(TRACTOR_DEFECT3)) != null)
                    {
                        tractor_info += @"
More Tractor Defects?: Yes
Tractor Defect Details?: " + submission.getAnswer(TRACTOR_DEFECT3) + @"
Remarks: " + submission.getAnswer(TRACTOR_DETAILS3);

                        if ((submission.getAnswer(TRACTOR_DEFECT4)) != null)
                        {
                            tractor_info += @"
More Tractor Defects?: Yes
Tractor Defect Details?: " + submission.getAnswer(TRACTOR_DEFECT4) + @"
Remarks: " + submission.getAnswer(TRACTOR_DETAILS4);
                        }
                        else
                        {
                            tractor_info += @"
More Tractor Defects?: No";
                        }
                    }
                    else
                    {
                        tractor_info += @"
More Tractor Defects?: No";
                    }
                }
                else
                {
                    tractor_info += @"
More Tractor Defects?: No";
                }
            }
            else
            {
                tractor_info += @"
Tractor Defect?: No";
            }

            string trailer_info = null;
            if ((submission.getAnswer(TRAILER_DEFECT1)) != null)
            {
                trailer_info += @"
Trailer Defect?: Yes
Trailer Defect Details?: " + submission.getAnswer(TRAILER_DEFECT1) + @"
Remarks: " + submission.getAnswer(TRAILER_DETAILS1);

                if ((submission.getAnswer(TRAILER_DEFECT2)) != null)
                {
                    trailer_info += @"
More Trailer Defects?: Yes
Trailer Defect Details?: " + submission.getAnswer(TRAILER_DEFECT2) + @"
Remarks: " + submission.getAnswer(TRAILER_DETAILS2);

                    if ((submission.getAnswer(TRAILER_DEFECT3)) != null)
                    {
                        trailer_info += @"
More Trailer Defects?: Yes
Trailer Defect Details?: " + submission.getAnswer(TRAILER_DEFECT3) + @"
Remarks: " + submission.getAnswer(TRAILER_DETAILS3);

                        if ((submission.getAnswer(TRAILER_DEFECT4)) != null)
                        {
                            trailer_info += @"
More Trailer Defects?: Yes
Trailer Defect Details?: " + submission.getAnswer(TRAILER_DEFECT4) + @"
Remarks: " + submission.getAnswer(TRAILER_DETAILS4);
                        }
                        else
                        {
                            trailer_info += @"
More Trailer Defects?: No";
                        }
                    }
                    else
                    {
                        trailer_info += @"
More Trailer Defects?: No";
                    }
                }
                else
                {
                    trailer_info += @"
More Trailer Defects?: No";
                }
            }
            else
            {
                trailer_info += @"
Trailer Defect?: No";
            }


            DriverLocation lastpoint = db.DriverLocations.Where(dl => dl.DriverID == submission.DriverID && dl.CreateDateUTC < submission.SubmissionDateUTC).FirstOrDefault();
            string location_info = @"
Lat / Long: UNKNOWN
Location: UNKNOWN";
            if (lastpoint != null)
            {
                GoogleLocation gl = LocationHelper.getLocation(lastpoint.Lat, lastpoint.Lon);
                location_info = @"
Lat / Long: " + lastpoint.Lat.ToString("0.0000000000000") + " " + lastpoint.Lon.ToString("0.000000000000") + @"  (Excellent GPS)
Location: " + gl.Street + "; "+gl.CityST + "; (Excellent GPS)";
            }

            string body = submission.Truck.IDNumber + @" sent this message:

To: DVIR

IMessage / Form: DVIR Truck Trailer
Driver Name: " + submission.Driver.FullName + @"
Driver ID: " + submission.Driver.IDNumber + @"
Tractor Odometer: " + submission.getAnswer(ODOMETER) + @"
Trailer #: " + submission.Trailer.IDNumber + @"
Trailer Hub: " + submission.getAnswer(TRAILER_HUB) + @"
Defect Found?: " + ((tractor_info != null || trailer_info != null) ? "Yes" : "No") + 
tractor_info +
trailer_info + @"
Date time: " + submission.CreateDate.ToString() + @"
Fuel: " + submission.getAnswer(FUEL) + @"
Odometer: " + submission.getAnswer(ODOMETER) + 
location_info;

            new CTSEmail(submission.QuestionnaireTemplate.ExceptionEmail, title, body).Send();
            return Json(new { status="OK"}, JsonRequestBehavior.AllowGet);
        }

    }
}
