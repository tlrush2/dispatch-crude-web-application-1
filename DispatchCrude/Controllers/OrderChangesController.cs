﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Syncfusion.XlsIO;
using System.Data;
using AlonsIT;

namespace DispatchCrude.Controllers
{
    [Authorize(Roles = "viewOrders")]
    public class OrderChangesController : Controller
    {
        
        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.Title = "Order Changes";
            return View();
        }

        
        [HttpPost]
        public ActionResult GetChanges(string orderNum)
        {
            return File(ExportChangesToExcel(orderNum), "Application/excel", string.Format("OrderChanges_{0}.xlsx", orderNum));
        }

        private Stream ExportChangesToExcel(string orderNum)
        {
            MemoryStream ret = null;

            using (SSDB ssdb = new SSDB())
            {
                DataTable dtOrder = ssdb.GetPopulatedDataTable("SELECT * FROM viewOrderLocalDates WHERE OrderNum={0}"
                    , (object)DBHelper.QuoteStr(orderNum));
                if (dtOrder.Rows.Count != 1)
                {
                    // TODO: handle this situation by notifying the user that no order was found
                }
                else
                {
                    DataTable dtTickets = ssdb.GetPopulatedDataTable("SELECT * FROM viewOrderTicket WHERE OrderID = {0}", dtOrder.Rows[0]["ID"]);

                    using (ExcelEngine excelEngine = new ExcelEngine())
                    {
                        IApplication excelApp = null;
                        IWorkbook wb = null;
                        IWorksheet ws = null;
                        int sheetID = 0;
                        try
                        {
                            excelApp = excelEngine.Excel;
                            wb = excelApp.Workbooks.Add(ExcelVersion.Excel2010);
                            ws = wb.Worksheets[sheetID++];
                            ws.Name = "Order " + orderNum;

                            AddDataTableToWorksheet(ws, dtOrder, 1, 1, true);

                            using (System.Data.SqlClient.SqlCommand cmd = ssdb.BuildCommand("spGetOrderChanges"))
                            {
                                cmd.Parameters["@id"].Value = dtOrder.Rows[0]["ID"];
                                AddDataTableToWorksheet(ws, SSDB.GetPopulatedDataTable(cmd), 4, 1, true);
                            }

                            foreach (DataRow drTicket in dtTickets.Rows)
                            {
                                if (sheetID < wb.Worksheets.Count)
                                {
                                    ws = wb.Worksheets[sheetID++];
                                }
                                else
                                {
                                    // FIXED C-Z tickets not showing:
                                    // When sheetID = 3 the old way was trying to copy the non-existant sheetID 3
                                    // Old Way:  ws = wb.Worksheets.AddCopy(sheetID++);
                                    ws = wb.Worksheets.AddCopy(sheetID - 1);
                                    sheetID++;
                                }
                                
                                ws.Name = "Ticket " + drTicket["CarrierTicketNum"].ToString();

                                AddTicketToWorkbook(ssdb, ws, drTicket);
                            }
                        }
                        catch (Exception)
                        {
                            // TODO: handle these errors
                        }
                        ret = new MemoryStream();
                        wb.SaveAs(ret, ExcelSaveType.SaveAsXLS);
                        wb.Close();
                    }
                }
            }
            ret.Position = 0;
            return ret;
        }

        private void AddTicketToWorkbook(SSDB ssdb, IWorksheet sheet, DataRow drTicket)
        {
            DataTable dtTicket = drTicket.Table.Clone();
            dtTicket.ImportRow(drTicket);
            AddDataTableToWorksheet(sheet, dtTicket, 1, 1, true);

            using (System.Data.SqlClient.SqlCommand cmd = ssdb.BuildCommand("spGetOrderTicketChanges"))
            {
                cmd.Parameters["@id"].Value = drTicket["ID"];
                AddDataTableToWorksheet(sheet, SSDB.GetPopulatedDataTable(cmd), 4, 1, true);
            }
        }

        private void AddDataTableToWorksheet(IWorksheet sheet, DataTable dt, int startRow = 1, int startCol = 1, bool addHeader = true)
        {
            if (addHeader)
            {
                int col = startCol;
                foreach (DataColumn dc in dt.Columns)
                {
                    sheet.Range[startRow, col++].Value = dc.Caption;
                }
                startRow++;
            }
            foreach (DataRow dr in dt.Rows)
            {
                int col = startCol;
                foreach (DataColumn dc in dt.Columns)
                {
                    sheet.Range[startRow, col++].Value = dr[dc].ToString();
                }
                startRow++;
            }
        }
    }

}
