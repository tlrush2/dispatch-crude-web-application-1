﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Profile;
using AlonsIT;
using DispatchCrude.App_Code;
using DispatchCrude.Core;
using DispatchCrude.Models;
using DispatchCrude.ViewModels;

namespace DispatchCrude.Controllers
{
    public class DispatchMessagingController : Controller
    {
        private DispatchCrudeDB db = new DispatchCrudeDB();

        public ActionResult Index(FilterViewModel<DispatchMessageWithDriver> fvm)
        {
            if (fvm.StartDate == null) { fvm.StartDate = DateTime.Today.AddMonths(-1); }
            if (fvm.EndDate == null) { fvm.EndDate = DateTime.Today; }

            int myCarrierID = Converter.ToInt32(Profile.GetPropertyValue("CarrierID"));
            int myDriverID = Converter.ToInt32(Profile.GetPropertyValue("DriverID"));
            int myTerminalID = Converter.ToInt32(Profile.GetPropertyValue("TerminalID"));
            int myRegionID = Converter.ToInt32(Profile.GetPropertyValue("RegionID"));

            ViewBag.CarrierID = new SelectList(db.Carriers.Where(c => c.DeleteDateUTC == null && (myCarrierID == -1 || c.ID == myCarrierID)).OrderBy(c => c.Name), "ID", "Name", fvm.CarrierID);
            ViewBag.DriverGroupID = new SelectList(db.DriverGroups.Where(d => d.DeleteDateUTC == null).OrderBy(d => d.Name), "ID", "Name", fvm.DriverGroupID);
            ViewBag.DriverID = new SelectList(db.Drivers.Where(d => d.DeleteDateUTC == null && (myCarrierID == -1 || d.CarrierID == myCarrierID || d.ID == myDriverID)).OrderBy(d => d.FirstName).ThenBy(d => d.LastName), "ID", "FullName", fvm.DriverID);
            ViewBag.TerminalID = new SelectList(db.Terminals.Where(t => (myTerminalID <= 0 || t.ID == myTerminalID) && t.DeleteDateUTC == null).OrderBy(t => t.Name), "ID", "Name", fvm.TerminalID);
            ViewBag.RegionID = new SelectList(db.Regions.Where(r => myRegionID == 0 || r.ID == myRegionID).OrderBy(r => r.Name), "ID", "Name", fvm.RegionID);
            ViewBag.StateID = new SelectList(db.States.OrderBy(s => s.ID), "ID", "FullName", fvm.StateID);


            string sql = string.Format(@"
                    DECLARE @CarrierID INT = {0};
                    DECLARE @DrivergroupID INT = {1};
                    DECLARE @DriverID INT = {2};
                    DECLARE @TerminalID INT = {3};
                    DECLARE @RegionID INT = {4};
                    DECLARE @StateID INT = {5};

                SELECT msg.ID, UID, ThreadID, [From] = FromUser, [To] = ToUser, Message, Seen, MessageDateUTC, DriverID = d.ID, Driver = d.FullName, UserName = cs.Value
		            FROM viewDriverBase d 
		            JOIN tblCACHE_DriverUserNames du ON du.DriverID = d.ID
		            OUTER APPLY dbo.fnSplitString(du.UserNames, ',') cs
                    JOIN ( 
                             SELECT dm.* FROM (
                                        SELECT DISTINCT u1 = CASE WHEN FromUser > ToUser THEN ToUser ELSE FromUser END,
                            			    u2 = CASE WHEN FromUser > ToUser THEN FromUser ELSE ToUser END,
			                                ID = MAX(ID)
	                                    FROM tbldispatchmessage
	                                    WHERE CAST(MessageDateUTC AS DATE) BETWEEN '{6}' AND '{7}' " +
	                                    (string.IsNullOrEmpty(fvm.SearchString) ? "" : " AND Message LIKE '%" + fvm.SearchString + "%' ") +
        	                            @"GROUP BY FromUser, ToUser
                            ) m
            	            JOIN tblDispatchMessage dm ON m.ID = dm.ID
                    ) msg ON msg.FromUser = cs.Value OR msg.ToUser = cs.value
		            WHERE (ISNULL(@CarrierID, 0) <= 0 OR d.CarrierID = @CarrierID)
		            AND (ISNULL(@DriverGroupID, 0) <= 0 OR d.DriverGroupID = @DriverGroupID)
		            AND (ISNULL(@DriverID, 0) <= 0 OR d.ID = @DriverID)
		            AND (ISNULL(@TerminalID, 0) <= 0 OR d.TerminalID = @TerminalID OR d.TerminalID IS NULL)
		            AND (ISNULL(@RegionID, 0) <= 0 OR d.RegionID = @RegionID)
		            AND (ISNULL(@StateID, 0) <= 0 OR d.OperatingStateID = @StateID)",
                fvm.CarrierID ?? 0,
                fvm.DriverGroupID ?? 0,
                fvm.DriverID ?? 0,
                fvm.TerminalID ?? 0,
                fvm.RegionID ?? 0,
                fvm.StateID ?? 0,
                fvm.StartDate,
                fvm.EndDate);
            fvm.results = db.Database.SqlQuery<DispatchMessageWithDriver>(sql).AsQueryable().OrderByDescending(m => m.MessageDateUTC).ToList();

            return View(fvm);
        }


        public ActionResult Messages(string username)
        {
            var messages = db.DispatchMessages.Where(m => m.To == User.Identity.Name && m.From == username 
                                                        || m.To == username && m.From == User.Identity.Name)
                                                .OrderBy(m => m.MessageDateUTC);

            ViewBag.Username = username;
            ProfileBase UserProfile = new DispatchCrudeProfilePresenter().GetProfile(username);
            ViewBag.Name = (UserProfile.GetPropertyValue("FullName") != null) ? UserProfile.GetPropertyValue("FullName").ToString() : username;
            ViewBag.Status = "offline";
            ViewBag.LastChat = "long ago";

            DispatchContact contact = DispatchMessage.Contacts().Where(c => c.UserName == username).First();
            if (contact != null && contact.LastChat().HasValue)
            {
                ViewBag.LastChat = contact.LastChat().Value.ToString("M/d @ h:mm tt");
                if (contact.online) ViewBag.Status = "online";
            }

            // mark each message to me as read
            foreach (var message in messages.Where(m => m.To == User.Identity.Name))
            {
                message.Seen = true;
            }
            db.SaveChanges(User.Identity.Name);

            return PartialView("Messages", messages);
        }

        public int AffectedUsers(int? CarrierID, int? DriverGroupID, int? DriverID, int? TerminalID, int? RegionID, int? StateID)
        {
            using (SSDB ssdb = new SSDB())
            {
                return DBHelper.ToInt32(ssdb.QuerySingleValue(@"
                    DECLARE @CarrierID INT = {0};
                    DECLARE @DrivergroupID INT = {1};
                    DECLARE @DriverID INT = {2};
                    DECLARE @TerminalID INT = {3};
                    DECLARE @RegionID INT = {4};
                    DECLARE @StateID INT = {5};

                    SELECT COUNT(*)
		            FROM viewDriverBase d 
		            JOIN tblCACHE_DriverUserNames du ON du.DriverID = d.ID
		            OUTER APPLY dbo.fnSplitString(du.UserNames, ',') cs
		            WHERE (ISNULL(@CarrierID, 0) <= 0 OR d.CarrierID = @CarrierID)
		            AND (ISNULL(@DriverGroupID, 0) <= 0 OR d.DriverGroupID = @DriverGroupID)
		            AND (ISNULL(@DriverID, 0) <= 0 OR d.ID = @DriverID)
		            AND (ISNULL(@TerminalID, 0) <= 0 OR d.TerminalID = @TerminalID OR d.TerminalID IS NULL)
		            AND (ISNULL(@RegionID, 0) <= 0 OR d.RegionID = @RegionID)
		            AND (ISNULL(@StateID, 0) <= 0 OR d.OperatingStateID = @StateID)",
                    CarrierID ?? 0,
                    DriverGroupID ?? 0,
                    DriverID ?? 0,
                    TerminalID ?? 0,
                    RegionID ?? 0,
                    StateID ?? 0));
            }
        }

        [Authorize(Roles ="sendDispatchMessages")]
        public ActionResult SendBlast()
        {
            int myCarrierID = Converter.ToInt32(Profile.GetPropertyValue("CarrierID"));
            int myDriverID = Converter.ToInt32(Profile.GetPropertyValue("DriverID"));
            int myTerminalID = Converter.ToInt32(Profile.GetPropertyValue("TerminalID"));
            int myRegionID = Converter.ToInt32(Profile.GetPropertyValue("RegionID"));

            ViewBag.CarrierID = new SelectList(db.Carriers.Where(c => c.DeleteDateUTC == null && (myCarrierID == -1 || c.ID == myCarrierID)).OrderBy(c => c.Name), "ID", "Name");
            ViewBag.DriverGroupID = new SelectList(db.DriverGroups.Where(d => d.DeleteDateUTC == null).OrderBy(d => d.Name), "ID", "Name");
            ViewBag.DriverID = new SelectList(db.Drivers.Where(d => d.DeleteDateUTC == null && (myCarrierID == -1 || d.CarrierID == myCarrierID || d.ID == myDriverID)).OrderBy(d => d.FirstName).ThenBy(d => d.LastName), "ID", "FullName");
            ViewBag.TerminalID = new SelectList(db.Terminals.Where(t => (myTerminalID <= 0 || t.ID == myTerminalID) && t.DeleteDateUTC == null).OrderBy(t => t.Name), "ID", "Name");
            ViewBag.RegionID = new SelectList(db.Regions.Where(r => myRegionID == 0 || r.ID == myRegionID).OrderBy(r => r.Name), "ID", "Name");
            ViewBag.StateID = new SelectList(db.States.OrderBy(s => s.ID), "ID", "FullName");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles ="sendDispatchMessages")]
        public ActionResult SendBlast(DispatchBlast m)
        {
            int myCarrierID = Converter.ToInt32(Profile.GetPropertyValue("CarrierID"));
            int myDriverID = Converter.ToInt32(Profile.GetPropertyValue("DriverID"));
            int myTerminalID = Converter.ToInt32(Profile.GetPropertyValue("TerminalID"));
            int myRegionID = Converter.ToInt32(Profile.GetPropertyValue("RegionID"));

            ViewBag.CarrierID = new SelectList(db.Carriers.Where(c => c.DeleteDateUTC == null && (myCarrierID == -1 || c.ID == myCarrierID)).OrderBy(c => c.Name), "ID", "Name", m.CarrierID);
            ViewBag.DriverGroupID = new SelectList(db.DriverGroups.Where(d => d.DeleteDateUTC == null).OrderBy(d => d.Name), "ID", "Name", m.DriverGroupID);
            ViewBag.DriverID = new SelectList(db.Drivers.Where(d => d.DeleteDateUTC == null && (myCarrierID == -1 || d.CarrierID == myCarrierID || d.ID == myDriverID)).OrderBy(d => d.FirstName).ThenBy(d => d.LastName), "ID", "FullName", m.DriverID);
            ViewBag.TerminalID = new SelectList(db.Terminals.Where(t => (myTerminalID <= 0 || t.ID == myTerminalID) && t.DeleteDateUTC == null).OrderBy(t => t.Name), "ID", "Name", m.TerminalID);
            ViewBag.RegionID = new SelectList(db.Regions.Where(r => myRegionID == 0 || r.ID == myRegionID).OrderBy(r => r.Name), "ID", "Name", m.RegionID);
            ViewBag.StateID = new SelectList(db.States.OrderBy(s => s.ID), "ID", "FullName", m.StateID);

            if (ModelState.IsValid)
            {
                if (string.IsNullOrWhiteSpace(m.Message))
                {
                    TempData["error"] = "Message cannot be empty!";
                    return View(m);
                }
                // Use stored procedure to send messages
                using (SSDB DCDB = new SSDB())
                {
                    using (System.Data.SqlClient.SqlCommand cmd = DCDB.BuildCommand("spMessageDrivers"))
                    {
                        cmd.Parameters["@Message"].Value = m.Message;
                        cmd.Parameters["@CarrierID"].Value = m.CarrierID;
                        cmd.Parameters["@DriverGroupID"].Value = m.DriverGroupID;
                        cmd.Parameters["@DriverID"].Value = m.DriverID;
                        cmd.Parameters["@TerminalID"].Value = m.TerminalID;
                        cmd.Parameters["@RegionID"].Value = m.RegionID;
                        cmd.Parameters["@StateID"].Value = m.StateID;
                        cmd.Parameters["@Username"].Value = User.Identity.Name;
                        cmd.ExecuteNonQuery();
                    }
                }

                return RedirectToAction("Index");
            }

            return View(m);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddMessage(DispatchMessage m)
        {
            m.From = User.Identity.Name;
            m.UID = Guid.NewGuid();
            m.MessageDateUTC = DateTime.UtcNow;

            db.DispatchMessages.Add(m);
            db.SaveChanges(User.Identity.Name);

            return Messages(m.To);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
   }
}