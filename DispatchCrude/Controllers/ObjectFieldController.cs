﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using DispatchCrude.App_Code;
using DispatchCrude.Models;
using Kendo.Mvc.UI;
using DispatchCrude.Extensions;
using Kendo.Mvc.Extensions;
using AlonsIT;

namespace DispatchCrude.Controllers
{
    [Authorize]
    public class ObjectFieldController : _DBControllerRead
    {
        //
        // GET: /ObjectField/
        [Authorize(Roles = "viewImportCenterCustomDataFields")]
        public ActionResult Index()
        {
            ViewBag.Title = "Custom Data Fields";
            return View();
        }

        // DriverScheduling/Read
        [Authorize(Roles = "viewImportCenterCustomDataFields")]
        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        override protected ContentResult Read([DataSourceRequest] DataSourceRequest request, ModelStateDictionary modelState, int id)
        {
            var data = db.ObjectFields
                    .Where(o => o.IsCustom == true && (id == 0 || o.ID == id))
                    .OrderBy(o => o.Name);
            return ToJsonResult(data, request, modelState, "ParentObject", "ObjectFieldType", "AllowNull", "Fields" );
        }

        // GET: return the Objects currently defined
        [Authorize(Roles = "viewImportCenterCustomDataFields")]
        public ContentResult Objects()
        {
            return Content(RestControllerHelper.GetJson(
                    objectName: "object"
                ), "application/json");
        }

        // GET: return the ObjectFieldTypes currently defined
        [Authorize(Roles = "viewImportCenterCustomDataFields")]
        public ContentResult FieldTypes()
        {
            return Content(RestControllerHelper.GetJson(
                    objectName: "objectfieldtype"
                ), "application/json");
        }

        // GET: return the Objects currently defined 
        [Authorize(Roles = "viewImportCenterCustomDataFields")]
        public ContentResult AllowNulls()
        {
            return Content(RestControllerHelper.GetJson(
                    objectName: "objectfieldallownull"
                ), "application/json");
        }

        [Authorize(Roles = "viewImportCenterCustomDataFields")]
        private string NormalizeObjectName(string objectName)
        {
            return objectName.Replace(" ", ""); ;
        }

        // GET: returns the Custom ObjectFields for the specified object (name)
        // - used by kendoGridExportHelper.js (custom telerik grid excel exporter logic to add custom data to exported .xls file)
        public ContentResult CustomObjectFields(string objectName)
        {
            objectName = NormalizeObjectName(objectName);
            var objectDef = db.Objects.Include(o => o.Fields).Where(o => o.Name.Replace(" ", "").Equals(objectName, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
            if (objectDef != null)
            {
                var data = objectDef.Fields.Where(m => m.IsCustom).Select(f => new { f.ID, f.Name });
                return ToJsonResult(data.ToList());
            }
            return null;
        }

        // GET - retrieves a pivoted table of custom data values 
        // - used by kendoGridExportHelper.js (custom telerik grid excel exporter logic to add custom data to exported .xls file)
        // NOTE: http://stackoverflow.com/questions/16927862/why-do-i-have-to-speicfy-fromuri-to-get-this-to-work
        public ContentResult CustomObjectFieldValues([System.Web.Http.FromUri] string objectName, [System.Web.Http.FromUri] List<int> idList)
        {
            objectName = NormalizeObjectName(objectName);
            using (SSDB db = new SSDB())
            {
                DataTable raw = db.GetPopulatedDataTable(
                    @"SELECT RecordID, ObjectFieldID, Value 
                        FROM tblObjectCustomData 
                        WHERE ObjectFieldID IN (SELECT ID FROM viewObjectField WHERE replace(Object, ' ', '') LIKE {0} AND IsCustom = 1) AND RecordID IN ({1})
                        ORDER BY RecordID" // must be ordered by key column for pivot table
                    , DBHelper.QuoteStr(objectName)
                    , string.Join(",", idList));
                if (raw.Rows.Count > 0)
                {
                    DataTable ret = PivotHelper.Pivot(raw.CreateDataReader(), "RecordID", "ObjectFieldID", "Value");
                    return Content(JsonConvert.SerializeObject(ret));
                }
            }
            return Content(JsonConvert.SerializeObject(Json(new { }, JsonRequestBehavior.AllowGet)));
        }

        //
        // POST: /ReportEmailSubscription/Edit/5
        [Authorize(Roles = "viewImportCenterCustomDataFields")]
        [HttpPost]
        [ValidateHeaderAntiForgeryToken]
        public ContentResult Update([DataSourceRequest] DataSourceRequest request, ObjectField field)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ObjectField existing = null;
                    if (field.ID != 0
                        // an existing record exists
                        && (existing = db.ObjectFields.FirstOrDefault(f => f.ID == field.ID && (f.ObjectDefID != field.ObjectDefID || f.ObjectFieldTypeID != field.ObjectFieldTypeID))) != null
                        // and it is LOCKED
                        && existing.Locked)
                    {
                        throw new Exception("Changes not allowed: Field is Locked");
                    }

                    db.AddOrUpdateSave(User.Identity.Name, field);
                }
                catch (Exception ex)
                {
                    if (ex.Message.Contains("udxObjectField_Name"))
                        ModelState.AddModelError("", "Name is already in use for this Object");
                    else
                        ModelState.AddModelError("", ex.Message); // TODO: use a common routine to "cleanup" db generated errors

                    return JsonStringResult.Create(new[] { field }.ToDataSourceResult(request, ModelState));
                }
            }

            return Read(request, ModelState, field.ID);
        }
        
        //
        // GET: /ReportEmailSubscription/Delete/5
        [Authorize(Roles = "viewImportCenterCustomDataFields")]
        [HttpPost]
        [ValidateHeaderAntiForgeryToken]
        public ActionResult Delete([DataSourceRequest] DataSourceRequest request, ObjectField field)
        {
            return DeleteInt(request, field);
        }

    }
}
