﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Entity;
using Syncfusion.XlsIO;
using System.IO;

namespace DispatchCrude.Models.ImportCenter
{
    public class ImportCenterGroupEngine
    {
        public byte[] Process(int id, string importFileName, Stream inputFileStream, string exportFileName)
        {
            // TODO: prompt the user for the firstRowHeader value (instead of hard-coded TRUE)
            DataTable dtInput = ImportCenterInputHelper.ToDataTable(importFileName, inputFileStream, true);
            using (DispatchCrudeDB db = new DispatchCrudeDB())
            {
                // retrieve the steps for processing (in .Position order)
                var steps = db.ImportCenterGroupSteps.Where(s => s.GroupID == id).Include(s => s.ImportCenterDefinition).OrderBy(s => s.Position);

                // create a new spreadsheet
                using (ExcelEngine excelEngine = new ExcelEngine())
                {
                    IApplication application = excelEngine.Excel;
                    application.DefaultVersion = ExcelVersion.Excel2013;
                    IWorkbook workbook = application.Workbooks.Create(steps.Count());

                    int i = 0;
                    // iterate of the steps (in order), process and add the results to the output excel document
                    foreach (ImportCenterGroupStep step in steps)
                    {
                        ImportCenterEngine logic = new ImportCenterEngine(step.ImportCenterDefinitionID, step.ImportCenterDefinition.RootObjectID, dtInput, importFileName);
                        DataTable dtResults = logic.Process().DataTable;

                        IWorksheet sheet = workbook.Worksheets[i++];
                        System.Text.RegularExpressions.Regex.Replace(step.Name, @"[\[\]/\\\:\?\*]", "_");
                        sheet.Name = step.Name.Replace(" ", "_");
                        sheet.ImportDataTable(dtResults, true, 1, 1);
                    }
                    
                    // save to a stream, then return as a byte array
                    MemoryStream ms = new MemoryStream();
                    workbook.SaveAs(ms, ExcelSaveType.SaveAsXLS);
                    string defaultFileName = string.Format("ImportOutcome - {0:yyyy-MM-dd}.xlsx", DateTime.Now.Date);
                    return ms.ToArray();
                }
            }
        }
    }
}