using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mobile.DispatchCrude.Models
{
    [Table("tblDriver", Schema="dbo")]
    public partial class tblDriver
    {
        public tblDriver()
        {
            this.tblOrders = new List<tblOrder>();
        }

        public int ID { get; set; }
        public int CarrierID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string IdNumber { get; set; }
        public string PIN { get; set; }
        public Nullable<bool> DLonFile { get; set; }
        public string DLNumber { get; set; }
        public Nullable<System.DateTime> DLExpiration { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public Nullable<int> StateID { get; set; }
        public string Zip { get; set; }
        public string MobilePhone { get; set; }
        public string MobileProvider { get; set; }
        public string Email { get; set; }
        public Nullable<int> TruckID { get; set; }
        public Nullable<int> TrailerID { get; set; }
        public Nullable<int> Trailer2ID { get; set; }
        public virtual tblCarrier tblCarrier { get; set; }
        public virtual tblTrailer tblTrailer { get; set; }
        public virtual tblTrailer tblTrailer1 { get; set; }
        public virtual tblTruck tblTruck { get; set; }
        public virtual ICollection<tblOrder> tblOrders { get; set; }
        public virtual tblState tblState { get; set; }
    }
}
