using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mobile.DispatchCrude.Models
{
    [Table("tblState", Schema="dbo")]
    public partial class tblState
    {
        public tblState()
        {
            this.tblCarriers = new List<tblCarrier>();
            this.tblCustomers = new List<tblCustomer>();
            this.tblDestinations = new List<tblDestination>();
            this.tblDrivers = new List<tblDriver>();
            this.tblOperators = new List<tblOperator>();
            this.tblOrigins = new List<tblOrigin>();
        }

        public int ID { get; set; }
        public string Abbreviation { get; set; }
        public string FullName { get; set; }
        public virtual ICollection<tblCarrier> tblCarriers { get; set; }
        public virtual ICollection<tblCustomer> tblCustomers { get; set; }
        public virtual ICollection<tblDestination> tblDestinations { get; set; }
        public virtual ICollection<tblDriver> tblDrivers { get; set; }
        public virtual ICollection<tblOperator> tblOperators { get; set; }
        public virtual ICollection<tblOrigin> tblOrigins { get; set; }
    }
}
