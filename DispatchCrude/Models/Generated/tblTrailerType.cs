using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mobile.DispatchCrude.Models
{
    [Table("tblTrailerType", Schema="dbo")]
    public partial class tblTrailerType
    {
        public tblTrailerType()
        {
            this.tblTrailers = new List<tblTrailer>();
        }

        public int ID { get; set; }
        public string TrailerType { get; set; }
        public bool CertRequired { get; set; }
        public virtual ICollection<tblTrailer> tblTrailers { get; set; }
    }
}
