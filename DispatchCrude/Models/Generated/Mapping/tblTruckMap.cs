using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Mobile.DispatchCrude.Models.Mapping
{
    public class tblTruckMap : EntityTypeConfiguration<tblTruck>
    {
        public tblTruckMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.TruckNumber)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(t => t.DOTnumber)
                .HasMaxLength(20);

            this.Property(t => t.VIN)
                .HasMaxLength(20);

            this.Property(t => t.Make)
                .HasMaxLength(20);

            this.Property(t => t.Model)
                .HasMaxLength(20);


            // Relationships
            this.HasRequired(t => t.tblCarrier)
                .WithMany(t => t.tblTrucks)
                .HasForeignKey(d => d.CarrierID);

        }
    }
}
