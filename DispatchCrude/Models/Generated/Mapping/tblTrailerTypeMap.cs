using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Mobile.DispatchCrude.Models.Mapping
{
    public class tblTrailerTypeMap : EntityTypeConfiguration<tblTrailerType>
    {
        public tblTrailerTypeMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.TrailerType)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(3);

        }
    }
}
