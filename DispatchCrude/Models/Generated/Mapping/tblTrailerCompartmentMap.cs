using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Mobile.DispatchCrude.Models.Mapping
{
    public class tblTrailerCompartmentMap : EntityTypeConfiguration<tblTrailerCompartment>
    {
        public tblTrailerCompartmentMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ID, t.TrailerID, t.CapacityInBarrels });

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.CompartmentNumber)
                .IsFixedLength()
                .HasMaxLength(10);

            this.Property(t => t.TrailerID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CapacityInBarrels)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);


            // Relationships
            this.HasRequired(t => t.tblTrailer)
                .WithMany(t => t.tblTrailerCompartments)
                .HasForeignKey(d => d.TrailerID);

        }
    }
}
