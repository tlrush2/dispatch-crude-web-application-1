using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Mobile.DispatchCrude.Models.Mapping
{
    public class tblRouteMap : EntityTypeConfiguration<tblRoute>
    {
        public tblRouteMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties

            // Relationships
            this.HasRequired(t => t.tblDestination)
                .WithMany(t => t.tblRoutes)
                .HasForeignKey(d => d.DestinationID);
            this.HasRequired(t => t.tblOrigin)
                .WithMany(t => t.tblRoutes)
                .HasForeignKey(d => d.OriginID);

        }
    }
}
