using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Mobile.DispatchCrude.Models.Mapping
{
    public class tblPriorityMap : EntityTypeConfiguration<tblPriority>
    {
        public tblPriorityMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(20);

            this.Property(t => t.ForeColor)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.BackColor)
                .IsRequired()
                .HasMaxLength(50);

        }
    }
}
