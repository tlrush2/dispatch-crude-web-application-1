using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Mobile.DispatchCrude.Models.Mapping
{
    public class tblOperatorMap : EntityTypeConfiguration<tblOperator>
    {
        public tblOperatorMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(40);

            this.Property(t => t.Address)
                .HasMaxLength(40);

            this.Property(t => t.City)
                .HasMaxLength(30);

            this.Property(t => t.Zip)
                .HasMaxLength(10);

            this.Property(t => t.ContactName)
                .HasMaxLength(40);

            this.Property(t => t.ContactEmail)
                .HasMaxLength(75);

            this.Property(t => t.ContactPhone)
                .HasMaxLength(20);

            this.Property(t => t.Notes)
                .HasMaxLength(255);


            // Relationships
            this.HasOptional(t => t.tblState)
                .WithMany(t => t.tblOperators)
                .HasForeignKey(d => d.StateID);

        }
    }
}
