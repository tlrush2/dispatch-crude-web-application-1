using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Mobile.DispatchCrude.Models.Mapping
{
    public class tblCarrierMap : EntityTypeConfiguration<tblCarrier>
    {
        public tblCarrierMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(40);

            this.Property(t => t.Address)
                .HasMaxLength(40);

            this.Property(t => t.City)
                .HasMaxLength(30);

            this.Property(t => t.Zip)
                .HasMaxLength(10);

            this.Property(t => t.ContactName)
                .HasMaxLength(40);

            this.Property(t => t.ContactEmail)
                .HasMaxLength(50);

            this.Property(t => t.ContactPhone)
                .HasMaxLength(15);

            this.Property(t => t.FEINTaxID)
                .HasMaxLength(12);


            // Relationships
            this.HasOptional(t => t.tblState)
                .WithMany(t => t.tblCarriers)
                .HasForeignKey(d => d.StateID);

        }
    }
}
