using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Mobile.DispatchCrude.Models.Mapping
{
    public class tblOrderStatuMap : EntityTypeConfiguration<tblOrderStatu>
    {
        public tblOrderStatuMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.OrderStatus)
                .IsRequired()
                .HasMaxLength(25);

            this.Property(t => t.ActionText)
                .IsRequired()
                .HasMaxLength(25);

        }
    }
}
