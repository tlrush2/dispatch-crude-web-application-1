using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Mobile.DispatchCrude.Models.Mapping
{
    public class tblDriverMap : EntityTypeConfiguration<tblDriver>
    {
        public tblDriverMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.FirstName)
                .IsRequired()
                .HasMaxLength(20);

            this.Property(t => t.LastName)
                .IsRequired()
                .HasMaxLength(20);

            this.Property(t => t.IdNumber)
                .HasMaxLength(20);

            this.Property(t => t.PIN)
                .HasMaxLength(10);

            this.Property(t => t.DLNumber)
                .HasMaxLength(20);

            this.Property(t => t.Address)
                .HasMaxLength(40);

            this.Property(t => t.City)
                .HasMaxLength(30);

            this.Property(t => t.Zip)
                .HasMaxLength(10);

            this.Property(t => t.MobilePhone)
                .HasMaxLength(20);

            this.Property(t => t.MobileProvider)
                .HasMaxLength(30);

            this.Property(t => t.Email)
                .HasMaxLength(75);


            // Relationships
            this.HasRequired(t => t.tblCarrier)
                .WithMany(t => t.tblDrivers)
                .HasForeignKey(d => d.CarrierID);
            this.HasOptional(t => t.tblTrailer)
                .WithMany(t => t.tblDrivers)
                .HasForeignKey(d => d.TrailerID);
            this.HasOptional(t => t.tblTrailer1)
                .WithMany(t => t.tblDrivers1)
                .HasForeignKey(d => d.Trailer2ID);
            this.HasOptional(t => t.tblTruck)
                .WithMany(t => t.tblDrivers)
                .HasForeignKey(d => d.TruckID);
            this.HasOptional(t => t.tblState)
                .WithMany(t => t.tblDrivers)
                .HasForeignKey(d => d.StateID);

        }
    }
}
