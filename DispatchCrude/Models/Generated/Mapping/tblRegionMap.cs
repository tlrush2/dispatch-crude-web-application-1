using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Mobile.DispatchCrude.Models.Mapping
{
    public class tblRegionMap : EntityTypeConfiguration<tblRegion>
    {
        public tblRegionMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(25);

        }
    }
}
