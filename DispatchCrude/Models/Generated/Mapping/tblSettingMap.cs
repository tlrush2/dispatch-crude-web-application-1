using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Mobile.DispatchCrude.Models.Mapping
{
    public class tblSettingMap : EntityTypeConfiguration<tblSetting>
    {
        public tblSettingMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(75);

            this.Property(t => t.Value)
                .HasMaxLength(255);

            this.Property(t => t.Category)
                .HasMaxLength(35);


            // Relationships
            this.HasRequired(t => t.tblSettingType)
                .WithMany(t => t.tblSettings)
                .HasForeignKey(d => d.SettingTypeID);

        }
    }
}
