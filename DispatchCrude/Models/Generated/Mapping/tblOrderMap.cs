using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Mobile.DispatchCrude.Models.Mapping
{
    public class tblOrderMap : EntityTypeConfiguration<tblOrder>
    {
        public tblOrderMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.OriginWaitNotes)
                .HasMaxLength(255);

            this.Property(t => t.OriginBOLNum)
                .HasMaxLength(15);

            this.Property(t => t.DestWaitNotes)
                .HasMaxLength(255);

            this.Property(t => t.DestBOLNum)
                .HasMaxLength(15);

            this.Property(t => t.RejectNotes)
                .HasMaxLength(255);

            this.Property(t => t.OriginTankNum)
                .HasMaxLength(20);

            this.Property(t => t.CarrierTicketNum)
                .HasMaxLength(15);


            // Relationships
            this.HasOptional(t => t.tblCarrier)
                .WithMany(t => t.tblOrders)
                .HasForeignKey(d => d.CarrierID);
            this.HasRequired(t => t.tblCustomer)
                .WithMany(t => t.tblOrders)
                .HasForeignKey(d => d.CustomerID);
            this.HasRequired(t => t.tblDestination)
                .WithMany(t => t.tblOrders)
                .HasForeignKey(d => d.DestinationID);
            this.HasOptional(t => t.tblDriver)
                .WithMany(t => t.tblOrders)
                .HasForeignKey(d => d.DriverID);
            this.HasOptional(t => t.tblOperator)
                .WithMany(t => t.tblOrders)
                .HasForeignKey(d => d.OperatorID);
            this.HasRequired(t => t.tblOrderStatu)
                .WithMany(t => t.tblOrders)
                .HasForeignKey(d => d.StatusID);
            this.HasRequired(t => t.tblOrigin)
                .WithMany(t => t.tblOrders)
                .HasForeignKey(d => d.OriginID);
            this.HasOptional(t => t.tblPriority)
                .WithMany(t => t.tblOrders)
                .HasForeignKey(d => d.PriorityID);
            this.HasOptional(t => t.tblPumper)
                .WithMany(t => t.tblOrders)
                .HasForeignKey(d => d.PumperID);
            this.HasOptional(t => t.tblRoute)
                .WithMany(t => t.tblOrders)
                .HasForeignKey(d => d.RouteID);
            this.HasRequired(t => t.tblTicketType)
                .WithMany(t => t.tblOrders)
                .HasForeignKey(d => d.TicketTypeID);
            this.HasOptional(t => t.tblTrailer)
                .WithMany(t => t.tblOrders)
                .HasForeignKey(d => d.TrailerID);
            this.HasOptional(t => t.tblTrailer1)
                .WithMany(t => t.tblOrders1)
                .HasForeignKey(d => d.Trailer2ID);
            this.HasOptional(t => t.tblTruck)
                .WithMany(t => t.tblOrders)
                .HasForeignKey(d => d.TruckID);

        }
    }
}
