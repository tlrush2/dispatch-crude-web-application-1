using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Mobile.DispatchCrude.Models.Mapping
{
    public class tblPumperMap : EntityTypeConfiguration<tblPumper>
    {
        public tblPumperMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.FirstName)
                .IsRequired()
                .HasMaxLength(25);

            this.Property(t => t.LastName)
                .IsRequired()
                .HasMaxLength(25);

            this.Property(t => t.ContactEmail)
                .HasMaxLength(70);

            this.Property(t => t.ContactPhone)
                .HasMaxLength(20);


            // Relationships
            this.HasRequired(t => t.tblOperator)
                .WithMany(t => t.tblPumpers)
                .HasForeignKey(d => d.OperatorID);

        }
    }
}
