using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Mobile.DispatchCrude.Models.Mapping
{
    public class tblDestinationTypeMap : EntityTypeConfiguration<tblDestinationType>
    {
        public tblDestinationTypeMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.DestinationType)
                .IsRequired()
                .HasMaxLength(20);

        }
    }
}
