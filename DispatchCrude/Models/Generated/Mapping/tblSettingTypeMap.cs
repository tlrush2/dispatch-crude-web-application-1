using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Mobile.DispatchCrude.Models.Mapping
{
    public class tblSettingTypeMap : EntityTypeConfiguration<tblSettingType>
    {
        public tblSettingTypeMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(50);

        }
    }
}
