using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Mobile.DispatchCrude.Models.Mapping
{
    public class tblOrderTicketMap : EntityTypeConfiguration<tblOrderTicket>
    {
        public tblOrderTicketMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.CarrierTicketNum)
                .IsRequired()
                .HasMaxLength(15);

            this.Property(t => t.TankNum)
                .HasMaxLength(20);

            this.Property(t => t.RejectNotes)
                .HasMaxLength(255);


            // Relationships
            this.HasRequired(t => t.tblOrder)
                .WithMany(t => t.tblOrderTickets)
                .HasForeignKey(d => d.OrderID);
            this.HasOptional(t => t.tblTankType)
                .WithMany(t => t.tblOrderTickets)
                .HasForeignKey(d => d.TankTypeID);
            this.HasRequired(t => t.tblTicketType)
                .WithMany(t => t.tblOrderTickets)
                .HasForeignKey(d => d.TicketTypeID);

        }
    }
}
