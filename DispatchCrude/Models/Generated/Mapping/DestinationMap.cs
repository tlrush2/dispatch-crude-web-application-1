using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Mobile.DispatchCrude.Models.Mapping
{
    public class DestinationMap : EntityTypeConfiguration<Destination>
    {
        public DestinationMap()
        {

            // Relationships
            this.HasRequired(t => t.DestinationType)
                .WithMany(t => t.Destinations)
                .HasForeignKey(d => d.DestinationTypeID);
            this.HasOptional(t => t.State)
                .WithMany(t => t.Destinations)
                .HasForeignKey(d => d.StateID);

        }
    }
}
