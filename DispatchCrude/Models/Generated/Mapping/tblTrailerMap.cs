using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Mobile.DispatchCrude.Models.Mapping
{
    public class tblTrailerMap : EntityTypeConfiguration<tblTrailer>
    {
        public tblTrailerMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.TrailerNumber)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(t => t.DOTnumber)
                .HasMaxLength(20);

            this.Property(t => t.VIN)
                .HasMaxLength(20);

            this.Property(t => t.LicenseNumber)
                .HasMaxLength(20);

            this.Property(t => t.Make)
                .HasMaxLength(20);

            this.Property(t => t.Model)
                .HasMaxLength(20);

            this.Property(t => t.Composition)
                .HasMaxLength(50);


            // Relationships
            this.HasRequired(t => t.tblCarrier)
                .WithMany(t => t.tblTrailers)
                .HasForeignKey(d => d.CarrierID);
            this.HasOptional(t => t.tblTrailerType)
                .WithMany(t => t.tblTrailers)
                .HasForeignKey(d => d.TrailerTypeID);

        }
    }
}
