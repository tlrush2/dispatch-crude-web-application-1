using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mobile.DispatchCrude.Models
{
    [Table("tblOriginType", Schema="dbo")]
    public partial class tblOriginType
    {
        public tblOriginType()
        {
            this.tblOrigins = new List<tblOrigin>();
        }

        public int ID { get; set; }
        public string OriginType { get; set; }
        public bool Auditable { get; set; }
        public virtual ICollection<tblOrigin> tblOrigins { get; set; }
    }
}
