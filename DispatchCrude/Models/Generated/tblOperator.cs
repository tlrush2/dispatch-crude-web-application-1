using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mobile.DispatchCrude.Models
{
    [Table("tblOperator", Schema="dbo")]
    public partial class tblOperator
    {
        public tblOperator()
        {
            this.tblOrders = new List<tblOrder>();
            this.tblOrigins = new List<tblOrigin>();
            this.tblPumpers = new List<tblPumper>();
        }

        public int ID { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public Nullable<int> StateID { get; set; }
        public string Zip { get; set; }
        public string ContactName { get; set; }
        public string ContactEmail { get; set; }
        public string ContactPhone { get; set; }
        public string Notes { get; set; }
        public virtual ICollection<tblOrder> tblOrders { get; set; }
        public virtual tblState tblState { get; set; }
        public virtual ICollection<tblOrigin> tblOrigins { get; set; }
        public virtual ICollection<tblPumper> tblPumpers { get; set; }
    }
}
