using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mobile.DispatchCrude.Models
{
    [Table("tblCustomer", Schema="dbo")]
    public partial class tblCustomer
    {
        public tblCustomer()
        {
            this.tblOrders = new List<tblOrder>();
        }

        public int ID { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public Nullable<int> StateID { get; set; }
        public string Zip { get; set; }
        public string ContactName { get; set; }
        public string ContactEmail { get; set; }
        public string ContactPhone { get; set; }
        public decimal Rate { get; set; }
        public virtual ICollection<tblOrder> tblOrders { get; set; }
        public virtual tblState tblState { get; set; }
    }
}
