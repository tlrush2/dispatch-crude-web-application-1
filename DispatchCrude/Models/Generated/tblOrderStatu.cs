using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mobile.DispatchCrude.Models
{
    public partial class tblOrderStatu
    {
        public tblOrderStatu()
        {
            this.tblOrders = new List<tblOrder>();
        }

        public int ID { get; set; }
        public string OrderStatus { get; set; }
        public short StatusNum { get; set; }
        public string ActionText { get; set; }
        public virtual ICollection<tblOrder> tblOrders { get; set; }
    }
}
