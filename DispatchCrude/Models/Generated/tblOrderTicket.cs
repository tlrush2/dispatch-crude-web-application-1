using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mobile.DispatchCrude.Models
{
    [Table("tblOrderTicket", Schema="dbo")]
    public partial class tblOrderTicket
    {
        public int ID { get; set; }
        public int OrderID { get; set; }
        public string CarrierTicketNum { get; set; }
        public int TicketTypeID { get; set; }
        public Nullable<int> TankTypeID { get; set; }
        public string TankNum { get; set; }
        public Nullable<decimal> ProductObsGravity { get; set; }
        public Nullable<decimal> ProductObsTemp { get; set; }
        public Nullable<decimal> ProductBSW { get; set; }
        public Nullable<byte> OpeningGaugeFeet { get; set; }
        public Nullable<byte> OpeningGaugeInch { get; set; }
        public Nullable<byte> OpeningGaugeQ { get; set; }
        public Nullable<byte> ClosingGaugeFeet { get; set; }
        public Nullable<byte> ClosingGaugeInch { get; set; }
        public Nullable<byte> ClosingGaugeQ { get; set; }
        public Nullable<decimal> GrossBarrels { get; set; }
        public Nullable<decimal> NetBarrels { get; set; }
        public bool Rejected { get; set; }
        public string RejectNotes { get; set; }
        public virtual tblOrder tblOrder { get; set; }
        public virtual tblTankType tblTankType { get; set; }
        public virtual tblTicketType tblTicketType { get; set; }
    }
}
