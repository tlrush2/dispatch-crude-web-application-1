using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mobile.DispatchCrude.Models
{
    [Table("tblSetting", Schema="dbo")]
    public partial class tblSetting
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public byte SettingTypeID { get; set; }
        public string Value { get; set; }
        public string Category { get; set; }
        public virtual tblSettingType tblSettingType { get; set; }
    }
}
