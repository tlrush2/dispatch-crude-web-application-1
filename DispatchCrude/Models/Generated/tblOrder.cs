using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mobile.DispatchCrude.Models
{
    [Table("tblOrder", Schema="dbo")]
    public partial class tblOrder
    {
        public tblOrder()
        {
            this.tblOrderAccessorialRates = new List<tblOrderAccessorialRate>();
            this.tblOrderReroutes = new List<tblOrderReroute>();
            this.tblOrderTickets = new List<tblOrderTicket>();
        }

        public int ID { get; set; }
        public Nullable<int> OrderNum { get; set; }
        public int StatusID { get; set; }
        public Nullable<byte> PriorityID { get; set; }
        public System.DateTime DueDate { get; set; }
        public Nullable<int> RouteID { get; set; }
        public int RateMiles { get; set; }
        public int OriginID { get; set; }
        public Nullable<System.DateTime> OriginArriveTime { get; set; }
        public Nullable<System.DateTime> OriginDepartTime { get; set; }
        public Nullable<int> OriginWaitMinutes { get; set; }
        public string OriginWaitNotes { get; set; }
        public string OriginBOLNum { get; set; }
        public Nullable<decimal> OriginGrossBarrels { get; set; }
        public Nullable<decimal> OriginNetBarrels { get; set; }
        public int DestinationID { get; set; }
        public Nullable<System.DateTime> DestArriveTime { get; set; }
        public Nullable<System.DateTime> DestDepartTime { get; set; }
        public Nullable<int> DestWaitMinutes { get; set; }
        public string DestWaitNotes { get; set; }
        public string DestBOLNum { get; set; }
        public Nullable<decimal> DestGrossBarrels { get; set; }
        public Nullable<decimal> DestNetBarrels { get; set; }
        public int CustomerID { get; set; }
        public Nullable<int> CarrierID { get; set; }
        public Nullable<int> DriverID { get; set; }
        public Nullable<int> TruckID { get; set; }
        public Nullable<int> TrailerID { get; set; }
        public Nullable<int> Trailer2ID { get; set; }
        public Nullable<int> OperatorID { get; set; }
        public Nullable<int> PumperID { get; set; }
        public int TicketTypeID { get; set; }
        public bool Rejected { get; set; }
        public string RejectNotes { get; set; }
        public bool ChainUp { get; set; }
        public Nullable<int> OriginTruckMileage { get; set; }
        public bool Invoiced { get; set; }
        public string OriginTankNum { get; set; }
        public Nullable<int> DestTruckMileage { get; set; }
        public string CarrierTicketNum { get; set; }
        public virtual tblCarrier tblCarrier { get; set; }
        public virtual tblCustomer tblCustomer { get; set; }
        public virtual tblDestination tblDestination { get; set; }
        public virtual tblDriver tblDriver { get; set; }
        public virtual tblOperator tblOperator { get; set; }
        public virtual tblOrderStatu tblOrderStatu { get; set; }
        public virtual tblOrigin tblOrigin { get; set; }
        public virtual tblPriority tblPriority { get; set; }
        public virtual tblPumper tblPumper { get; set; }
        public virtual tblRoute tblRoute { get; set; }
        public virtual tblTicketType tblTicketType { get; set; }
        public virtual tblTrailer tblTrailer { get; set; }
        public virtual tblTrailer tblTrailer1 { get; set; }
        public virtual tblTruck tblTruck { get; set; }
        public virtual ICollection<tblOrderAccessorialRate> tblOrderAccessorialRates { get; set; }
        public virtual ICollection<tblOrderReroute> tblOrderReroutes { get; set; }
        public virtual ICollection<tblOrderTicket> tblOrderTickets { get; set; }
    }
}
