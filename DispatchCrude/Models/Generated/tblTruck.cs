using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mobile.DispatchCrude.Models
{
    [Table("tblTruck", Schema="dbo")]
    public partial class tblTruck
    {
        public tblTruck()
        {
            this.tblDrivers = new List<tblDriver>();
            this.tblOrders = new List<tblOrder>();
        }

        public int ID { get; set; }
        public int CarrierID { get; set; }
        public string TruckNumber { get; set; }
        public string DOTnumber { get; set; }
        public string VIN { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public Nullable<int> Year { get; set; }
        public virtual tblCarrier tblCarrier { get; set; }
        public virtual ICollection<tblDriver> tblDrivers { get; set; }
        public virtual ICollection<tblOrder> tblOrders { get; set; }
    }
}
