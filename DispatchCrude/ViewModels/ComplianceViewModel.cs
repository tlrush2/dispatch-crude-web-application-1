﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Script.Serialization;
using DispatchCrude.Models;

namespace DispatchCrude.ViewModels
{
    public class ComplianceViewModel : AuditModelDeleteBase
    {
        public ComplianceViewModel()
        {
        }

        public string Category { get; set; }
        public int? ID { get; set; }
        public int ItemID { get; set; }
        [DisplayName("Name")]
        public string ItemName { get; set; }

        public int CarrierID { get; set; }
        public string Carrier { get; set; }

        [DisplayName("Type")]
        public int ComplianceTypeID { get; set; }
        public string ComplianceType { get; set; }



        public bool MissingDocument { get; set; }
        public bool ExpiredNow { get; set; }
        public bool ExpiredNext30 { get; set; }
        public bool ExpiredNext60 { get; set; }
        public bool ExpiredNext90 { get; set; }
        public bool Missing { get; set; }
    }
}