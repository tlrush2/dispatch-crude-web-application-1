﻿using DispatchCrude.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DispatchCrude.ViewModels
{
    public class DispatchViewModel
    {
        [DisplayName("Order")]
        public int OrderID { get; set; }

        [DisplayName("Driver")]
        public int DriverID { get; set; }

        [DisplayName("Sequence No.")]
        public int? SequenceNum { get; set; }

        [DisplayName("Notes")]
        public string DispatchNotes { get; set; }

    }
}