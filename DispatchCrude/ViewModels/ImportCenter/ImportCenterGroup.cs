﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using DispatchCrude.Models;

namespace DispatchCrude.ViewModels.ImportCenter
{
    public class ImportCenterGroup : AuditModelLastChangeBase
    {
        [Key]
        public int ID { get; set; }

        [Required]
        public string Name { get; set; }

        [StringLength(1000)]
        public string UserNames { get; set; }

        public virtual List<ImportCenterGroupStep> Steps { get; set; }

        [NotMapped]
        public string StepIDCSV { get; set; }
        [NotMapped]
        public string StepImportCSV
        {
            get
            {
                return string.Join(", ", Steps.Select(s => s.ImportCenterDefinition.Name));
            }

            private set
            {

            }
        }
    }
}