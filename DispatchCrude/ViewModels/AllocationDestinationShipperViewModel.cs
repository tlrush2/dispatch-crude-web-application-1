﻿using DispatchCrude.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DispatchCrude.ViewModels
{
    public class AllocationDestinationShipperViewModel
    {
        public int DestinationID { get; set; }
        public string Destination { get; set; }

        public int ShipperID { get; set; }
        public string Shipper { get; set; }

        public int ProductGroupID { get; set; }
        public string ProductGroup { get; set; }

        public int TicketCount { get; set; }
        public decimal TotalGross { get; set; }
        public decimal? DailyUnits { get; set; }
        public DateTime AllocationStartDate { get; set; }
        public DateTime AllocationEndDate { get; set; }
        public decimal? AllocatedUnits { get; set; }
        public int DaysLeft { get; set; }

        public decimal Target
        {
            get { return AllocatedUnits ?? 0; }
        }
        public decimal Production
        {
            get { return Math.Min(TotalGross, (AllocatedUnits ?? 0)); }
        }
        public decimal Remaining
        {
            get { return Math.Max(0, (AllocatedUnits ?? 0) - TotalGross); }
        }
        public decimal Excess
        {
            get { return Math.Max(0, TotalGross - (AllocatedUnits ?? 0)); }
        }
        public decimal DailyUnitsToHitAllocationTarget
        {
            get {
                if (this.DaysLeft == 0) 
                    return 0;
                return this.Remaining / this.DaysLeft;
            }
        }

        public bool AllocationExceeded()
        {
            return TotalGross > AllocatedUnits;
        }

    }
}