﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DispatchCrude.Models;

namespace DispatchCrude.ViewModels
{
    public abstract class OrderSettlementHistory
    {
        public abstract string TABLE { get; }

        public string Description { get; set; }
        //public bool IsCurrent { get; set; }
        public int OrderID { get; set; }
        public int BatchNum { get; set; }
        public int OrderNum { get; set; }
        public string JobNumber { get; set; }
        public string ContractNumber { get; set; }
	    public DateTime? OrderDate { get; set; }
	    public string Shipper { get; set; }
	    public string Carrier { get; set; }
	    public string DriverFirst { get; set; }
	    public string DriverLast { get; set; }
	    public string DriverGroup { get; set; }
	    public string Origin { get; set; }
	    public string Destination { get; set; }
	    public string PreviousDestinations { get; set; }
	    public int? ActualMiles { get; set; }
	    public decimal? SettlementVolume { get; set; }
	    public string SettlementUom { get; set; }
	    public int? SettlementUomID { get; set; }
	    public decimal? OriginChainupAmount { get; set; }
	    public decimal? DestChainupAmount { get; set; }
	    public decimal? H2SAmount { get; set; }
	    public decimal? RerouteAmount { get; set; }
	    public decimal? OrderRejectAmount { get; set; }
	    public decimal? OriginWaitAmount { get; set; }
	    public decimal? DestinationWaitAmount { get; set; }
	    public decimal? TotalWaitAmount { get; set; }
	    public decimal? SplitLoadAmount { get; set; }
        public string MiscDetails { get; set; }
        public decimal? MiscAmount { get; set; }
	    public decimal? FuelSurchargeAmount { get; set; }
	    public decimal? OriginTaxRate { get; set; }
	    public decimal? RouteRate { get; set; }
	    public decimal? RateSheetRate { get; set; }
	    public decimal? LoadAmount { get; set; }
	    public decimal? TotalAmount { get; set; }
	    public DateTime? BatchDate { get; set; }
	    public DateTime? PeriodEndDate { get; set; }
	    public string InvoiceNum { get; set; }
	    public string OrderNotes { get; set; }
	    public string BatchNotes { get; set; }
	    public string SettledByUser { get; set; }
	    public DateTime? SettlementTimestamp { get; set; }

	    public bool IsFinal { get; set; }
    }


    public class CarrierOrderSettlementHistory : OrderSettlementHistory
    {
        public override string TABLE { get { return "Carrier"; } }
    }

    public class DriverOrderSettlementHistory : OrderSettlementHistory
    {
        public override string TABLE { get { return "Driver"; } }
    }

    public class ShipperOrderSettlementHistory : OrderSettlementHistory
    {
        public override string TABLE { get { return "Shipper"; } }
    }

}