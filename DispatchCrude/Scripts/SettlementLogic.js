﻿var noUpdate = false;
function contentResponseEnd(sender, args) {
    debugger;
    if (args.EventTarget.endsWith("$cmdFilter")) {          // grid was reloaded with new data (and a new db-stored session so we know all rows are selected)
        var rowCount = $(".chkSel").length;
        UpdateChkAll("Batch", rowCount > 0);
        UpdateChkAll("RateApply", rowCount > 0);
    } else if (args.EventTarget.endsWith("$cmdApplyRates")  // rates were applied
        || args.EventTarget.endsWith("$menuApplyRates")     // rates were applied
        || (args.EventArgument.startsWith("FireCommand")    // new page is being displayed
            && args.EventTarget.endsWith("$rgMain")
            && (args.EventArgument.split(";") || [0, 0])[1] === "Page")) {

        var url = document.URL + "/GetSessionSel"
        , sessionID = $("#hfSessionID").val()
        , data = { sessionID: sessionID };

        $.ajax({
            type: "POST"
            , url: url
            , data: kendo.stringify(data)
            , contentType: "application/json"
            , success: function (msg) {
                debugger;
                UpdateChkAll("RateApply", msg.d.RateApplySel);
                UpdateChkAll("Batch", msg.d.BatchSel);
            }
        });
    }

    try {
        // cleanup items
        AddToolTips();
        if ($(".ErrorTextBox").val() !== "") showErrors();
    } catch (ex) { /* eat exceptions */ }
}

function grid_OnCommand(sender, args) {
}
function chkAll_ToggleStateChanged(sender, args) {
    if (noUpdate) return;
    debugger;
    var id = sender.get_id()
        , url = document.URL + "/Update" + id.replace("chk", "") + "Sel"
        , sessionID = $("#hfSessionID").val()
        , value = args.get_currentToggleState().get_value()
        , subCss = "." + id.replace("All", "");

    noUpdate = true;
    // don't let the user select a "neither" choice, so roll forward to "true"
    if (value == "neither") sender.set_selectedToggleStateIndex(2);

    var data = { sessionID: sessionID, selected: value != "false" }; // send selected = true for "true" & "neither"

    // inform the server of the selection change (and update the row selection checkboxes upon callback)
    $.ajax({
        type: "POST"
        , url: url
        , data: kendo.stringify(data)
        , contentType: "application/json"
        , success: function (msg) {
            debugger;
            $(subCss).each(function () {
                this.childNodes[0].checked = msg.d;
            });
        }
    });

    noUpdate = false;

    DisableActionControl(type);
}
function chk_CheckedChanged(sender, type) {
    if (noUpdate) return;
    debugger;
    // the order attribute value is assigned in code-behind (ItemDataBound event)
    var orderID = sender.parentElement.attributes["orderid"].value
        , checked = sender.checked;
    SelChanged(type, orderID, checked);
}
function SelChanged(type, orderID, selected) {
    var sessionID = $("#hfSessionID").val()
        , data = { sessionID: sessionID, orderID: orderID, selected: selected }
        , url = document.URL + "/Update" + type + "Sel";
    $.ajax({
        type: "POST"
        , url: url
        , selType: type // done this way to prevent re-entrance issues (pass the value through to the success function)
        , data: kendo.stringify(data)
        , contentType: "application/json"
        , success: function (msg) {
            UpdateChkAll(this.selType, msg.d);
        }
    });
}
function UpdateChkAll(type, value) {
    debugger;
    if (noUpdate) return;
    noUpdate = true;
    var chkAll = $find("chk" + type + "All");
    if (value == null)
        chkAll.set_selectedToggleStateIndex(1); // neither (filled)
    else
        chkAll.set_selectedToggleStateIndex(value ? 2 : 0); // checked, unchecked respectively
    noUpdate = false;
    DisableActionControl(type);
}

function radTextBox_setEnabled(rtbID, enable) {
    var rtb = $find(rtbID);
    if (enable)
        rtb.enable();
    else
        rtb.disable();
}
function DisableActionControl(type) {
    debugger;
    var disabled = $find("chk" + type + "All").get_selectedToggleState().get_value() == "false";
    if (type === "Batch") {
        debugger;
        $(".cmdExport").prop("disabled", disabled);
        // only allow settling a batch only when at least one required DDL control has a specific selection (!= -1)
        disabled = disabled || $('.ddlRequired option:selected[value!=-1]').length == 0;
        radTextBox_setEnabled("txtInvoiceNum", !disabled);
        radTextBox_setEnabled("txtNotes", !disabled);
    }
    $(".cmd" + type).prop("disabled", disabled);
}

var contextMenu = null;
function storeContextMenuReference(sender, args) {
    contextMenu = sender;
}
function cmdApplyRates_ClientClicking(sender, args) {
    if (args.IsSplitButtonClick()) {
        var currentLocation = $telerik.getBounds(sender.get_element());
        contextMenu.showAt(currentLocation.x, currentLocation.y + currentLocation.height);
        args.set_cancel(true);
    }
}
// main startup script
$(function () {
    AddToolTips();
});

function AddToolTips() {
    $(".rgHeader.RAS").attr("title", "Rate Apply Selection");
    $(".rgHeader.BS").attr("title", "Batch Selection");
    $(".rgHeader.Header-Approved > a").attr("title", "Approved?\nClick to sort");
    $(".rgHeader.Header-HasError > a").attr("title", "Has Errors?\nClick to sort");
    $(".rgHeader.Header-ShipperSettled > a").attr("title", "Shipper Settled?\nClick to sort");
    $(".rgHeader.Header-CarrierSettled > a").attr("title", "Carrier Settled?\nClick to sort");
}
function showErrors() {
    $('.aErrors').click();
    return false;
}
