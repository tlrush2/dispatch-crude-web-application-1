﻿// see: http://blog.falafel.com/behold-the-kendo-checkbox/
(function () {
    var CHANGE = 'change';
    kendo.ui.plugin(kendo.ui.Widget.extend({
        init: function (element, options) {
            kendo.ui.Widget.fn.init.call(this, element, options);
            this._create();
        },
        options: {
            name: 'Checkbox'
        },
        events: [
            CHANGE],
        enable: function (val) {
            if (val === false) {
                $(this.element).attr('disabled', 'disabled');
                $(this.wrapper).addClass('k-state-disabled');
            } else {
                $(this.element).removeAttr('disabled');
                $(this.wrapper).removeClass('k-state-disabled');
            }
        },
        value: function (checked) {
            var $element = $(this.element),
                currentValue = $element.prop('checked');

            if (typeof checked === 'boolean') {
                if (checked !== currentValue) {
                    $element.prop('checked', checked);
                    this.trigger(CHANGE, {
                        field: 'value'
                    });
                }
            } else {
                return $element.prop('checked');
            }
        },
        _create: function () {
            var me = this,
                $element = $(me.element),
                text = $element.data('label') || me.options.label || '';

            me.element.prop('type', 'checkbox');
            me.wrapper = $element.wrap('<span class="k-checkbox"></span>')
                .parent();
            $element.after('<label>' + text + '</label>');
            $element.siblings('label').click(function (e) {
                $element.click();
            });
            $element.bind(CHANGE, function (e) {
                //PROPAGATE EVENT TO WIDGET
                me.trigger(CHANGE, {
                    field: 'value'
                });
            });
            if (me.options.enabled !== undefined) {
                me.enable(me.options.enabled);
            }
        }
    }));
})()