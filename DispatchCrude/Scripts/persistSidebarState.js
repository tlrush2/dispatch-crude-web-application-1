/*******************
* Author: Ben Bloodworth
* Date: 7/15/16
* Purpose: This collection of functions works with some of the code in "beyond.js" to persist the user's menu state preference (open/closed).
*          The state is saved each time the user clicks the toggle button (beyond.js line 192) and the code here, called from the layout page,
*          reads the saved state from a site-wide cookie then sets the state if needed.
*********************/

//Check the sidebar state cookie
function checkMenuStateCookie()
{
    //Currently, Maverick wanted the menu defaulted to closed in the layout page. If this is changed to expanded, the code here
    //will also need to be changed to check for cookie="collapsed" and the side bar DOES NOT have the "menu-compact" class
    //in order to retain this functionality
    if ((getMenuStateCookie()) == "expanded" && $(".page-sidebar").hasClass("menu-compact"))
        fakeClick("mouseup", document.getElementById("sidebar-collapse"));
}

//Set the sidebar state cookie value (called from added line of code in "beyond.js", line 192)
function setMenuStateCookie()
{
    var sidebar = $(".page-sidebar");
    var expires = new Date();

    //Set expire date to 30 days so that it will not expire if someone does not login for a while (shippers, etc.)
    expires.setTime(expires.getTime() + (30 * 24 * 60 * 60 * 1000));
    expires = expires.toUTCString();

    if (sidebar.hasClass("menu-compact"))
    {
        document.cookie = "menuState=collapsed; expires=" + expires + "; path=/";
    }
    else
    {
        document.cookie = "menuState=expanded; expires=" + expires + "; path=/";
    }
}

//Get the sidebar state cookie value
function getMenuStateCookie() {
    var cookie = document.cookie.split(";");

    for (var i = 0; i < cookie.length; i++)
    {
        var c = cookie[i];
        while (c.charAt(0) == " ")
        {
            c = c.substring(1);
        }

        if (c.indexOf("menuState=") == 0)
        {
            return c.substring(10, c.length);
        }
    }

    return "";
}

//This function was pulled from: http://stackoverflow.com/questions/1421584/how-can-i-simulate-a-click-to-an-anchor-tag
function fakeClick(event, anchorObj)
{
    if (anchorObj.click)
    {
        anchorObj.click()
    }
    else if (document.createEvent)
    {
        if (event.target !== anchorObj)
        {
            var evt = document.createEvent("MouseEvents");
            evt.initMouseEvent("click", true, true, window,
                0, 0, 0, 0, 0, false, false, false, false, 0, null);
            var allowDefault = anchorObj.dispatchEvent(evt);
            // you can check allowDefault for false to see if
            // any handler called evt.preventDefault().
            // Firefox will *not* redirect to anchorObj.href
            // for you. However every other browser will.
        }
    }
}