﻿var __jsonInt;
function retrieveJSON(url, dataProperty, submitData) {
    $.ajax({
        type: "GET",
        url: url,
        data: submitData,
        async: false,
        success: function (data) {
            __jsonInt = dataProperty ? data[dataProperty] : data;
        },
        error: function (xhr) {
            alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
        }
    });
    return __jsonInt;
};
// find the specified property value in the array based on a key value
// first 2 parameters are required, the 3rd & 4th will be defaulted to 'Name' & 'ID' respectively
function arrayFindValueByKey(dataArray, key, valuePropertyName, keyPropertyName) {
    if (!valuePropertyName) valuePropertyName = "Name"; // default value
    if (!keyPropertyName) keyPropertyName = "ID"; // default value
    var x = arrayFindByKey(dataArray, keyPropertyName, key);
    return x != null ? x[valuePropertyName] : "";
};
// array = [{key:value [, ...]},{key:value [, ...]}]
function arrayFindByKey(dataArray, keyPropertyName, keyValue) {
    if (keyValue != null) {
        for (var i = 0; i < dataArray.length; i++) {
            if (dataArray[i][keyPropertyName] == keyValue) {
                return dataArray[i];
            }
        }
    }
    return null;
};
function arrayFindAllByKey(dataArray, keyPropertyName, keyValue, recurse) {
    var ret = [];
    if (keyValue != null) {
        for (var i = 0; i < dataArray.length; i++) {
            if (dataArray[i][keyPropertyName] == keyValue) {
                ret.push(dataArray[i]);
            } else if (recurse) {
                var result = arrayFindAllByKey(dataArray[i], keyPropertyName, keyValue);
                for (var i = 0; i < result.length; i++)
                    ret.push(result[i]);
            }
        }
    }
    return null;
}
function verificationTokenHeader() {
    return { __RequestVerificationToken: getRequestVerificationToken() };
}
function getRequestVerificationToken() {
    return $('[name=__RequestVerificationToken]').val() || '';
}
function addRequestVerificationToken(data) {
    var securityToken = getRequestVerificationToken();
    if (typeof securityToken != 'undefined') {
        if (data.length > 0)
            data += '&'
        else
            data = '';
        data += "__RequestVerificationToken=" + encodeURIComponent(securityToken);
    }
    return data;
};
/* doesn't seem to fire with kendo dataSource ajax calls (are they set to async: false ??) 
$(function () {
    debugger;
    $('body').bind('ajaxSend', function (elm, xhr, s) {
        debugger;
        if (s.type == 'POST') {
            s.data = addRequestVerificationToken(s.data);
        }
    });
}); 
*/

/* recursive function to "drill" to a nested final data value (specified in data, 'sub.sub2.value' dotted syntax) */
function drillToValue(data, dataPath) {
    if (!data || !dataPath)
        return '';

    var pos = dataPath.indexOf('.');
    if (pos > -1) {
        var segment = dataPath.slice(0, pos);
        dataPath = dataPath.slice(pos + 1);
        return drillToValue(data[segment], dataPath);
    } else
        return data[dataPath] || '';
}

function cloneObject(data, nameValueOverridesArray) {
    var ret = JSON.parse(JSON.stringify(data));
    if (nameValueOverridesArray && nameValueOverridesArray.length && nameValueOverridesArray[0].Name) {
        for (var i = 0; i < nameValueOverridesArray.length; i++) {
            ret[nameValueOverridesArray[i].Name] = nameValueOverridesArray[i].Value;
        }
    }
    return ret;
}