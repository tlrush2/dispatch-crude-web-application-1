﻿//BB - 12/14/16 - Uses new HTML5 option to replace the matched portion of the current URL with the passed replacement string.
//The old way of doing this: window.location.href = window.location.href.replace(/\?originid\=\d+/, "") forces a page refresh but this method will replace
//the URL without requiring a refresh.  This lets us remove the parameters from a URL (for example, for filtering a grid from an incoming link)
//and then once we've filtered the grid we can remove the parameter(s) and the page URL looks normal but we've retained the filtering we wanted.
//Example usage:  RegexReplaceUrl(/\?OriginID\=\d+/, "");
function RegexReplaceUrl(regexPattern, replaceWith) {
    window.history.pushState(null, null, window.location.href.replace(regexPattern, replaceWith));
}

//Returns the passed phone number string in the "(000) 000-0000" format as long as there are 10 digits
function FormatUSAPhoneNumber(number, returnAsLink) {
    if (typeof number != undefined && number.replace(/\D+/g, "").length == 10)
    {
        if (typeof returnAsLink != undefined && returnAsLink == true)
        {
            return "<a href='tel:" + number + "'>" + "(" + number.substr(0, 3) + ") " + number.substr(3, 3) + "-" + number.substr(6, 4) + "</a>";
        }
        else
        {
            return "(" + number.substr(0, 3) + ") " + number.substr(3, 3) + "-" + number.substr(6, 4);
        }        
    }
    else
    {
        return "Invalid Number";
    }        
}

//Returns the passed SSN string in the "000-00-0000" format as long as there are 9 digits
function FormatSSN(number) {
    if (typeof number != undefined && number.replace(/\D+/g, "").length == 9) {
        return number.substr(0, 3) + "-" + number.substr(3, 2) + "-" + number.substr(5, 4);
    }
    else {
        return "Invalid Number";
    }
}

//Returns a whole html link for a GPS point (useful in MVC grid client templates) (Should be replaced by Core.LocationHelper.GetGPSLink() where possible)
function GetGPSLink(lat, lon) {
    return "<a href='http://maps.google.com/maps?z=12&t=k&q=loc:" + lat + "," + lon + "' target='_blank'>" + lat + "," + lon + "</a>";
}

//Used to simplify the client templates on kendo mvc grids when virtuals are used in the columns and an ellipsis is desired for long data
//(Relies upon drillToValue() in jsonSupport.js)
//Usage:   .ClientTemplate("#= ellipseDivDrill(data, 'ComplianceType.Name') #")
function ellipseDivDrill(data, dataPath) {
    return ellipseDiv(drillToValue(data, dataPath));
}

//Used to simplify the client templates on kendo mvc grids when an ellipsis is desired for long data
//Usage:  .ClientTemplate("#= ellipseDiv(data.Notes) #")
function ellipseDiv(value) {
    if (value == null || value == "null" || typeof value == null)
        value = "";

    return "<div class='showEllipsis' data-placement='bottom' title='" + value + "'>" + value + "</div>";
}