﻿function onDSErrors(args) {
    HandleDSErrors(args, $("#grid").data("kendoGrid"));
}

function HandleDSErrors(args, grid) {
    debugger;
    if (args.errors) {
        // prevent more script to execute (at least 1 error has occurred, preventing continued execution)
        args.preventDefault();
        //Look for a validation summary on the page or popup
        //  NOTE: .gridErrors is used extensively throughout many of the MVC rewrites so instead of changing that everwhere,
        //         the two instances of .gridEditorErrors in the driver scheduling pages have been changed to .gridErrors
        var gridValidationList = grid && grid.editable && grid.editable.element.find(".gridErrors").length
                ? grid.editable.element.find(".gridErrors")
                : $(".gridErrorList")  //.gridErrorList is only used on /ObjectField/Index.cshtml.  This page doesn't use a popup like the other grids.

        if (gridValidationList && gridValidationList.length > 0) {
            grid.one("dataBinding", function (e) {
                debugger;
                e.preventDefault();
                displayKendoValidationErrors(gridValidationList, args.errors);
            });
        } else {
            // show a popup of the grid error??
            showDSErrorWindow(args.errors, grid);
        }
    }
}

function showDSErrorWindow(errors, grid) {
    if (grid && grid.element) grid = grid.element;

    $("#windowGridErrorlist").remove();
    debugger;
    var validationMarkup = "<ul class='NullValidator'>" + combinedValidationList(errors) + "</ul>";
    var divError = $("<div id='windowGridErrorList' style='display: none;'>" + validationMarkup + "</div>").appendTo(!grid ? document.body : grid);
    divError.kendoWindow({ modal: true, visible: false, title: "Execution Errors" });
    $("#windowGridErrorList").data("kendoWindow").center().open();
}
function clearKendoValidationErrors(gridValidationList) {
    gridValidationList.empty();
}
function displayKendoValidationErrors(gridValidationList, errors) {
    clearKendoValidationErrors(gridValidationList);
    /* append the error to the grid validation UL element */
    gridValidationList.append(combinedValidationList(errors));
}

function combinedValidationList(errors) {
    var ret = "";
    $.each(errors, function (key, value) {
        ret += singleValidationList(key, value.errors);
    });
    return ret;
}
function singleValidationList(field, messages) {
    var ret = "";
    // if field ends with "ID" then remove ID from the field value
    if (field.slice(-"ID".length) === "ID") field = field.replace(/ID/g, "");
    if (messages.length) {
        for (var i = 0; i < messages.length; i++) {
            debugger;
            ret += "<li>" + /*(field.length ? field + ": " : "") +*/ messages[i] + "</li>";
            //The key was commented out 12/28/16 because this created messages that looked kind of silly...
            //Example: "Category: The Category field is required"  
            //Users will wonder why we stated the field name twice when it was not necessary if we leave it in there.
        }
    }
    return ret;
}