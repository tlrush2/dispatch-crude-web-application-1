﻿using System;
using System.Web.Profile;
using System.Web.UI;
using DispatchCrude.Core;

namespace DispatchCrude
{
    public partial class RootMaster : System.Web.UI.MasterPage
    {
        protected ProfileBase UserProfile;
        protected string Name;

        protected string GetSiteMapMenu()
        {
            return App_Code.SiteMap.Menu(Page.Title);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            UserProfile = new App_Code.DispatchCrudeProfilePresenter().GetProfile(UserSupport.UserName);
            Name = (UserProfile.GetPropertyValue("FullName") != null) ? UserProfile.GetPropertyValue("FullName").ToString() : UserSupport.UserName;

            if (!Converter.IsNullOrEmpty(Page.Title))
                PageTitleText = Page.Title;

            if (Request.Browser.MSDomVersion.Major == 0) // Non IE Browser?
            {
                Response.Cache.SetNoStore(); // No client side cashing for non IE browsers
            }

            //Detect Internet Explorer / Edge browsers and force user to get another browser before using DC
            string browserName = Request.Browser.Browser;
            string userAgent = Request.UserAgent;
            if (browserName == "InternetExplorer" || (userAgent.IndexOf("Edge") > -1))
            {
                Response.Redirect("~/noIESupport.html");
            }
        }

        public string PageTitleText
        {
            get
            {
                return lblRootMasterPageTitle.Text;
            }
            set
            {
                lblRootMasterPageTitle.Text = value;
            }
        }
        public string EntityCaptionText
        {
            get
            {
                //return EntityCaption.Text;
                EntityCaption.Visible = true;
                return "test";
            }
            set
            {
                EntityCaption.Text = value;
                //EntityCaption.Visible = value.Length > 0;
            }
        }
    }
}
