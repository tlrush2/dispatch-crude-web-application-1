﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DispatchCrude.Models;
using DispatchCrude.Extensions;

namespace DispatchCrude
{
    // see http://stackoverflow.com/questions/6431478/how-to-force-mvc-to-validate-ivalidatableobject for details
    public class CustomModelBinder : DefaultModelBinder
    {
        protected override void OnModelUpdated(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            base.OnModelUpdated(controllerContext, bindingContext);

            ForceModelValidation(bindingContext);
        }

        private static void ForceModelValidation(ModelBindingContext bindingContext)
        {
            try
            {
                var model = bindingContext.Model as IValidatableObject;
                if (model == null) return;

                var modelState = bindingContext.ModelState;

                // don't re-validate if already validated
                if (model is IEDTO && (model as IEDTO).Validated) return;

                var errors = model.Validate(new ValidationContext(model, null, null));
                foreach (var error in errors)
                {
                    foreach (var memberName in error.MemberNames)
                    {
                        // Only add errors that haven't already been added.
                        // (This can happen if the model's Validate(...) method is called more than once, which will happen when
                        // there are no property-level validation failures.)
                        var memberNameClone = memberName;
                        var idx = modelState.Keys.IndexOf(k => k == memberNameClone);
                        if (idx < 0) continue;
                        if (modelState.Values.ToArray()[idx].Errors.Any()) continue;

                        modelState.AddModelError(memberName, error.ErrorMessage);
                    }
                }
            }
            catch (MissingMethodException)
            {
                // eat this exception
            }
        }
    }

    public class CustomModelBinderProvider : IModelBinderProvider
    {
        public IModelBinder GetBinder(Type modelType)
        {
            return new CustomModelBinder();
        }
        static public void Register()
        {
            ModelBinderProviders.BinderProviders.Clear();
            ModelBinderProviders.BinderProviders.Add(new CustomModelBinderProvider());        
        }
    }

}