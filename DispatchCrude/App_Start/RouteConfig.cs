﻿using System.Web.Mvc;
using System.Web.Routing;

namespace DispatchCrude
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{*staticfile}", new { staticfile = @".*\.(png|css|js|gif|jpg|map)(/.*)?" });
            routes.IgnoreRoute("{*allaspx}", new { allaspx = @".*\.aspx(/.*)?" });
            routes.IgnoreRoute("{*robotstxt}", new { robotstxt = @"(.*/)?robots.txt(/.*)?" });
            // ignore webform .aspx pages for routing
            routes.IgnoreRoute("favicon.ico");
            routes.IgnoreRoute("Scripts");
            routes.IgnoreRoute("Content");
            routes.IgnoreRoute("Styles");

            routes.MapRoute(
                name: "RestGet",
                url: "Rest/Get/{objectName}/{fieldsCSV}", //string where = null, string orderBy = null, bool includeDeleted = false, string deletedSuffix = "Deleted: ", string deletedNameFIeld = "Name"
                defaults: new { controller = "Rest", action = "Get", fieldsCSV = "ID,Name" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}