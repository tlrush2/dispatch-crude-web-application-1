﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using DispatchCrude.App_Code;

namespace DispatchCrude.Extensions.HtmlHelpers
{
    public static class HtmlHelpers
    {
        public static MvcHtmlString EmbedCss(this HtmlHelper htmlHelper, string path)
        {
            // take a path that starts with "~" and map it to the filesystem.
            var cssFilePath = HttpContext.Current.Server.MapPath(path);
            // load the contents of that file
            try
            {
                var cssText = File.ReadAllText(cssFilePath);
                var styleElement = new TagBuilder("style");
                styleElement.SetInnerText(cssText);
                return MvcHtmlString.Create(styleElement.ToString());
            }
            catch (Exception)
            {
                // return nothing if we can't read the file for any reason
                return null;
            }
        }

        public static MvcHtmlString RequiredLabelFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, object htmlAttributes = null)
        {
            return new MvcHtmlString("<span class=\"RequiredLabel\">*</span>" + html.LabelFor(expression, htmlAttributes).ToHtmlString());
        }


        public static MvcHtmlString SuperDropDownList(this HtmlHelper htmlHelper, string name, SuperSelectList selectList, string optionLabel, object htmlAttributes)
        {
            try
            {

                string html = @"
        <div class=""dropdown"">
            <button class=""btn btn-default dropdown-toggle form-control"" type=""button"" data-toggle=""dropdown"" style=""padding: 0px 3px 0px 16px;"">
                <span id=""" + name + @"_selected"" class=""pull-left"">" + optionLabel + @"</span>
                <span class=""typcn typcn-arrow-sorted-down pull-right""></span>
            </button>
		    <ul class=""dropdown-menu superselect"" id=""" + name + @"_dropdown"">";

                foreach (SuperSelectListItem item in selectList.Items)
                {
                    html += @"
                <li data-option data-value=""" + item.Value + @""" data-name=""" + item.Text + @""" " +
                    (item.Disabled ? @"class=""disabled"" style=""background-color:#f3cdcd""" : "") + @">
                    <a href=""#"">" +
                        item.HTMLContent +
                    @"</a>
                </li>";
                }

                html += @"
            </ul>
	    </div>
	    <select name=""" + name + @""" id=""" + name + @""" style=""display:none"">
		    <option value="""">Choose your option</option>";
            foreach (SuperSelectListItem item in selectList.Items)
            {
		        html += @"<option value=""" + item.Value + @""" " + (selectList.SelectedValue != null && selectList.SelectedValue.ToString() == item.Value ? " SELECTED" : "") + ">" + item.Text + @"</option>";
		    }
	        html += @"</select>
        <script>
            $(""#" + name + @"_selected"").html($(""#" + name + @" option:selected"").text())
            $(""#" + name + @"_dropdown"").on(""click"", ""li:not(.disabled)"", function () {
                $(""#" + name + @"_selected"").html($(this).data(""name""))
                $(""#" + name + @""").val($(this).data(""value""))
            });
        </script>";

                return MvcHtmlString.Create(html);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}