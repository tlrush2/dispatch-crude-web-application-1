﻿using System;
using System.Collections.Generic;
using System.Web;
using DispatchCrude.Core;
using Kendo.Mvc.Extensions;

namespace DispatchCrude.Extensions
{
    public class NavigationObject
    {
        public string ID { get; set; }
        public string Text { get; set; }
        public string Link { get; set; }
        public string[] Permissions { get; set; }
    }

    public class NavigationHelper
    {
        public static HtmlString PrintTabArray(List<NavigationObject> tabArray, string activeTab)
        {
            string htmlResult = "";

            foreach (NavigationObject tab in tabArray)
            {
                bool hasPermission = false;

                foreach (string permission in tab.Permissions)
                {
                    if (UserSupport.IsInRole(permission))
                    {
                        hasPermission = true;
                        break;
                    }
                }

                if (hasPermission)
                {
                    htmlResult += "<li id=\"" + tab.ID + "\"" + (activeTab == tab.ID ? " class=\"active\"" : "") + ">" +
                                        "<a href =\"" + (activeTab == tab.ID ? "#" : tab.Link) + "\">" + tab.Text + "</a></li>";
                }
            }

            return new HtmlString(htmlResult);
        }

        public static HtmlString PrintButtonArray(List<NavigationObject> buttonArray, string activeButton)
        {
            string htmlResult = "";

            foreach (NavigationObject button in buttonArray)
            {
                bool hasPermission = false;

                foreach (string permission in button.Permissions)
                {
                    if (UserSupport.IsInRole(permission))
                    {
                        hasPermission = true;
                        break;
                    }
                }

                if (hasPermission)
                {
                    htmlResult += "<a id=\"" + button.ID + "\" class=\"btn btn-xs btn-blue shiny\" href =\"" + button.Link + "\" " + (activeButton == button.ID ? " disabled=\"disabled\"" : "") + ">" + button.Text + "</a></li>&#09;";
                }
            }

            return new HtmlString(htmlResult);
        }

        //The Tab and button sets below work with the Menu Item redirects in the HomeController to produce the tab and button structure underneath the main menu items
        #region TAB SET DEFINITIONS

        public static List<NavigationObject> TabSet_Dispatch = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Tab_OrderCreate", Text = "Order Create", Link = "/Home/Dispatch", Permissions = new string[] { "viewTruckOrders", "viewGaugerOrders" } },
            new NavigationObject() { ID = "Tab_Grid", Text = "Grid", Link = "/Site/Orders/Orders_Dispatch.aspx", Permissions = new string[] { "viewDispatch" } },
            new NavigationObject() { ID = "Tab_Grid3P", Text = "Grid 3P", Link = "/Orders/Dispatch3P", Permissions = new string[] { "Dispatch3P" } },
            new NavigationObject() { ID = "Tab_Planner", Text = "Planner", Link = "/Orders/DispatchBoard", Permissions = new string[] { "viewDispatchBoard" } },
            new NavigationObject() { ID = "Tab_DriverMap", Text = "Driver Map", Link = "/Orders/DispatchMap", Permissions = new string[] { "viewDispatchMap" } },
            new NavigationObject() { ID = "Tab_EnterEdit", Text = "Enter/Edit", Link = "/Orders/OrderList", Permissions = new string[] { "viewOrders" } },
            new NavigationObject() { ID = "Tab_Audit", Text = "Audit", Link = "/Home/Audit", Permissions = new string[] { "viewOrderAudit", "unauditOrders" } },
            new NavigationObject() { ID = "Tab_Approval", Text = "Approval", Link = "/Site/Orders/Orders_Approve.aspx", Permissions = new string[] { "viewOrderApproval" } },
            new NavigationObject() { ID = "Tab_Search", Text = "Adv. Search", Link = "/Orders/Search", Permissions = new string[] { "searchOrders" } }
        };

        public static List<NavigationObject> TabSet_PrintConfiguration = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Tab_DCDriver", Text = "DC Driver", Link = "/Site/PrintBM/DAHeaderImage.aspx", Permissions = new string[] { "viewDriverPrintConfig" } },
            new NavigationObject() { ID = "Tab_DCGauger", Text = "DC Gauger", Link = "/Site/PrintBM/GAHeaderImage.aspx", Permissions = new string[] { "viewGaugerPrintConfig" } },
            new NavigationObject() { ID = "Tab_BOL", Text = "BOL", Link = "/BOLFields", Permissions = new string[] { "Dispatch3P" } },
            new NavigationObject() { ID = "Tab_TicketTypes", Text = "Ticket Types", Link = "/TicketTypes", Permissions = new string[] { "viewDispatchBoard" } }
        };

        public static List<NavigationObject> TabSet_Logs = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Tab_SyncErrors", Text = "Sync Errors", Link = "/Site/Support/SyncErrors.aspx", Permissions = new string[] { "viewAppSyncErrors" } },
            new NavigationObject() { ID = "Tab_DriverSyncLog", Text = "Driver Sync Log", Link = "/DriverApp/DriverLog", Permissions = new string[] { "viewDriverAppSyncLog" } },
            new NavigationObject() { ID = "Tab_GaugerSyncLog", Text = "Gauger Sync Log", Link = "/GaugerApp/GaugerLog", Permissions = new string[] { "viewGaugerAppSyncLog" } },
            new NavigationObject() { ID = "Tab_AppChangeHistory", Text = "App Change History", Link = "/Site/Support/AppChanges.aspx", Permissions = new string[] { "viewAppChangeHistory" } }
        };

        public static List<NavigationObject> TabSet_Tools = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Tab_RecalculateVolumes", Text = "Recalculate Volumes", Link = "/RecalculateVolumes", Permissions = new string[] { "viewRecalculateVolumes" } },
            new NavigationObject() { ID = "Tab_UndeleteOrders", Text = "Undelete Orders", Link = "/OrderUndelete", Permissions = new string[] { "undeleteOrders" } }
        };

        public static List<NavigationObject> TabSet_BusinessRules = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Tab_CarrierRules", Text = "Carrier Rules", Link = "/Site/Compliance/CarrierRules.aspx", Permissions = new string[] { "manageCarrierRules" } },
            new NavigationObject() { ID = "Tab_OrderRules", Text = "Order Rules", Link = "/Site/Orders/OrderRules.aspx", Permissions = new string[] { "viewOrderRules" } },
            new NavigationObject() { ID = "Tab_HOSPolicies", Text = "HOS Policies", Link = "/HOSPolicies", Permissions = new string[] { "viewHOS" } },
            new NavigationObject() { ID = "Tab_Contracts", Text = "Contracts", Link = "/Contracts", Permissions = new string[] { "viewContracts" } },
            new NavigationObject() { ID = "Tab_Regions", Text = "Regions", Link = "/Regions", Permissions = new string[] { "viewRegionMaintenance" } }
        };

        public static List<NavigationObject> TabSet_Security = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Tab_Users", Text = "Users", Link = "/DCUsers", Permissions = new string[] { "viewUsers" } },
            new NavigationObject() { ID = "Tab_Groups", Text = "Groups", Link = "/DCGroups", Permissions = new string[] { "viewGroups" } }
        };

        public static List<NavigationObject> TabSet_Reports = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Tab_ReportCenter", Text = "Report Center", Link = "/Site/Orders/Orders_Inquiry.aspx", Permissions = new string[] { "viewReportCenter" } },
            new NavigationObject() { ID = "Tab_Subscriptions", Text = "Subscriptions", Link = "/ReportEmailSubscription", Permissions = new string[] { "viewEmailSubscription" } },
            new NavigationObject() { ID = "Tab_WaitAudit", Text = "Wait Audit", Link = "/Orders/DemurrageReport", Permissions = new string[] { "viewDemurrageReport" } },
            new NavigationObject() { ID = "Tab_BOLs", Text = "BOLs", Link = "/Site/Orders/Orders_BOLPrint.aspx", Permissions = new string[] { "viewPrintBOL" } },
            new NavigationObject() { ID = "Tab_OrderSummary", Text = "Order Summary", Link = "/Site/Orders/Orders_SummaryReport.aspx", Permissions = new string[] { "viewOrderSummary" } },  //Is this menu item/page still a page we want to keep?
            new NavigationObject() { ID = "Tab_VolumeAnalysis", Text = "Volume Analysis", Link = "/Site/Orders/Orders_VolAnalysisReport.aspx", Permissions = new string[] { "viewVolumeAnalysis" } },  //Is this menu item/page still a page we want to keep?
            new NavigationObject() { ID = "Tab_OrderMap", Text = "Order Map", Link = "/Orders/Map", Permissions = new string[] { "viewOrderMap" } },
            new NavigationObject() { ID = "Tab_DriverHistory", Text = "Driver History", Link = "/Drivers/Map", Permissions = new string[] { "viewDriverMap" } }  //Was called driver map previously
        };

        public static List<NavigationObject> TabSet_Imports = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Tab_ImportExecute", Text = "Import", Link = "/ImportCenter/Index", Permissions = new string[] { "viewImportCenter" } },
            new NavigationObject() { ID = "Tab_ImportDesigner", Text = "Import Designer", Link = "/ImportCenter/Designer", Permissions = new string[] { "editImportCenter" } },
            new NavigationObject() { ID = "Tab_ImportGroups", Text = "Import Groups", Link = "/ImportCenterGroup", Permissions = new string[] { "viewImportCenterGroups" } }
        };

        public static List<NavigationObject> TabSet_CustomData = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Tab_CustomFields", Text = "Custom Fields", Link = "/ObjectField", Permissions = new string[] { "viewImportCenterCustomDataFields" } },
            new NavigationObject() { ID = "Tab_CustomData", Text = "Custom Data", Link = "/ObjectCustomData", Permissions = new string[] { "viewImportCenterCustomDataFields" } }
        };

        public static List<NavigationObject> TabSet_Allocation = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Tab_DestinationAllocation", Text = "Allocation", Link = "/Dashboard", Permissions = new string[] { "viewAllocationDashboard" } }
        };

        public static List<NavigationObject> TabSet_Locations = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Tab_Origins", Text = "Origins", Link = "/Site/TableMaint/Origins.aspx", Permissions = new string[] { "viewOriginMaintenance" } },
            new NavigationObject() { ID = "Tab_OriginTanks", Text = "Origin Tanks", Link = "/OriginTanks", Permissions = new string[] { "viewOriginTanks" } },
            new NavigationObject() { ID = "Tab_Producers", Text = "Producers", Link = "/Producers", Permissions = new string[] { "viewProducerMaintenance" } },
            new NavigationObject() { ID = "Tab_Operators", Text = "Operators", Link = "/Operators", Permissions = new string[] { "viewOperatorMaintenance" } },
            new NavigationObject() { ID = "Tab_Pumpers", Text = "Pumpers", Link = "/Site/TableMaint/Pumpers.aspx", Permissions = new string[] { "viewPumperMaintenance" } },
            new NavigationObject() { ID = "Tab_Destinations", Text = "Destinations", Link = "/Site/TableMaint/Destinations.aspx", Permissions = new string[] { "viewDestinationMaintenance" } },
            new NavigationObject() { ID = "Tab_Consignees", Text = "Consignees", Link = "/Consignees", Permissions = new string[] { "viewConsignees" } },
            new NavigationObject() { ID = "Tab_Terminals", Text = "Terminals", Link = "/Terminals", Permissions = new string[] { "viewTerminals" } },
            new NavigationObject() { ID = "Tab_Products", Text = "Products", Link = "/Site/TableMaint/Products.aspx", Permissions = new string[] { "viewProducts" } }
        };

        public static List<NavigationObject> TabSet_Resources = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Tab_Drivers", Text = "Drivers", Link = "/Drivers", Permissions = new string[] { "viewDriverMaintenance" } },
            new NavigationObject() { ID = "Tab_Equipment", Text = "Equipment", Link = "/Home/Equipment", Permissions = new string[] { "viewDriverEquipment", "viewDriverEquipmentManufacturers", "viewDriverAppHistory" } },
            new NavigationObject() { ID = "Tab_Trucks", Text = "Trucks", Link = "/Site/TableMaint/Trucks.aspx", Permissions = new string[] { "viewTrucks" } },
            new NavigationObject() { ID = "Tab_Trailers", Text = "Trailers", Link = "/Site/TableMaint/Trailers.aspx", Permissions = new string[] { "viewTrailers" } },
            new NavigationObject() { ID = "Tab_Carriers", Text = "Carriers", Link = "/Site/TableMaint/Carriers.aspx", Permissions = new string[] { "viewCarrierMaintenance" } },
            new NavigationObject() { ID = "Tab_Maintenance", Text = "Maintenance", Link = "/TruckMaintenance", Permissions = new string[] { "viewTruckMaintenance", "viewTrailerMaintenance", "viewTruckTrailerMaintenanceTypes" } },  
            new NavigationObject() { ID = "Tab_OdometerLogs", Text = "Odometer Logs", Link = "/Site/DecisionSupport/OdometerLog.aspx", Permissions = new string[] { "viewOdometerLog" } },
            new NavigationObject() { ID = "Tab_Gaugers", Text = "Gaugers", Link = "/Site/TableMaint/Gaugers.aspx", Permissions = new string[] { "viewGaugerMaintenance" } },
            new NavigationObject() { ID = "Tab_Shippers", Text = "Shippers", Link = "/Shippers", Permissions = new string[] { "viewShippers" } }
        };

        public static List<NavigationObject> TabSet_Compliance = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Tab_Summary", Text = "Summary", Link = "/Compliance", Permissions = new string[] { "viewComplianceReport" } },
            new NavigationObject() { ID = "Tab_CarrierDocs", Text = "Carrier Docs", Link = "/CarrierCompliance", Permissions = new string[] { "viewCarrierCompliance", "viewCarrierComplianceTypes" } },
            new NavigationObject() { ID = "Tab_DriverDocs", Text = "Driver Docs", Link = "/DriverCompliance", Permissions = new string[] { "viewDriverCompliance", "viewDriverComplianceTypes" } },
            new NavigationObject() { ID = "Tab_TruckDocs", Text = "Truck Docs", Link = "/TruckCompliance", Permissions = new string[] { "viewTruckCompliance", "viewTruckComplianceTypes" } },
            new NavigationObject() { ID = "Tab_TrailerDocs", Text = "Trailer Docs", Link = "/TrailerCompliance", Permissions = new string[] { "viewTrailerCompliance", "viewTrailerComplianceTypes" } },            
            new NavigationObject() { ID = "Tab_Questionnaires", Text = "Questionnaires", Link = "/Questionnaires", Permissions = new string[] { "viewQuestionnaires" } },
            new NavigationObject() { ID = "Tab_HOS", Text = "HOS", Link = "/HOS/Daily", Permissions = new string[] { "viewHOS" } }
        };

        public static List<NavigationObject> TabSet_Commodities = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Tab_Pricing", Text = "Pricing", Link = "/CommodityPricing/Indexes", Permissions = new string[] { "viewCommodityPricing" } },
            new NavigationObject() { ID = "Tab_Settlement", Text = "Settlement", Link = "/Site/Financials/ProducerInvoicing.aspx", Permissions = new string[] { "viewProducerSettlement" } },
            new NavigationObject() { ID = "Tab_Batches", Text = "Batches", Link = "/Site/Financials/ProducerBatches.aspx", Permissions = new string[] { "viewProducerSettlementBatches" } }
        };

        public static List<NavigationObject> TabSet_Settlement = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Tab_Settle", Text = "Settle", Link = "/Site/Financials/ShipperInvoicing.aspx", Permissions = new string[] { "viewShipperSettlement", "viewCarrierSettlement", "viewDriverSettlement" } },
            new NavigationObject() { ID = "Tab_Batches", Text = "Batches", Link = "/Site/Financials/ShipperBatches.aspx", Permissions = new string[] { "viewShipperSettlementBatches", "viewCarrierSettlementBatches", "viewDriverSettlementBatches" } },
            new NavigationObject() { ID = "Tab_SettleUnits", Text = "Settle Units", Link = "/Site/Financials/ShipperSettlementFactor.aspx", Permissions = new string[] { "viewShipperSettlementUnits", "viewCarrierSettlementUnits" } },
            new NavigationObject() { ID = "Tab_MinSettleUnits", Text = "Min Settle Units", Link = "/Site/Financials/ShipperMinSettlementUnits.aspx", Permissions = new string[] { "viewShipperMinSettlementUnits", "viewCarrierMinSettlementUnits" } },
            new NavigationObject() { ID = "Tab_DriverGroups", Text = "Driver Groups", Link = "/Site/Financials/DriverGroups.aspx", Permissions = new string[] { "viewCarrierDriverSettlementGroups", "viewCarrierDriverSettlementGroupAssignments" } },
            new NavigationObject() { ID = "Tab_PdfExport", Text = "PDF Exports", Link = "/PdfExport", Permissions = new string[] { "viewCarrierDriverSettlementGroups", "viewCarrierDriverSettlementGroupAssignments" } },
            new NavigationObject() { ID = "Tab_Log", Text = "Log", Link = "/OrderSettlement", Permissions = new string[] { "viewCarrierSettlement", "viewDriverSettlement", "viewShipperSettlement" } },
        };

        public static List<NavigationObject> TabSet_RouteRates = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Tab_RateSheets", Text = "Rate Sheets", Link = "/Site/Financials/ShipperRateSheets.aspx", Permissions = new string[] { "viewShipperRateSheets", "viewCarrierRateSheets", "viewDriverRateSheets" } },
            new NavigationObject() { ID = "Tab_OverrideRates", Text = "Override Rates", Link = "/Site/Financials/ShipperRouteRates.aspx", Permissions = new string[] { "viewShipperRouteRates", "viewCarrierRouteRates", "viewDriverRouteRates" } },
            new NavigationObject() { ID = "Tab_RouteMaintenance", Text = "Route Maintenance", Link = "/Routes", Permissions = new string[] { "viewRouteMaintenance" } }
        };

        public static List<NavigationObject> TabSet_Accessorial = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Tab_AccessorialRates", Text = "Accessorial Rates", Link = "/Site/Financials/ShipperAccessorialRates.aspx", Permissions = new string[] { "viewAssessorialRateTypes", "viewShipperAssessorialRates", "viewCarrierAssessorialRates", "viewDriverAssessorialRates" } },
            new NavigationObject() { ID = "Tab_RejectReasons", Text = "Reject Reasons", Link = "/OrderRejectReasons", Permissions = new string[] { "viewReasonCodes"} },
            new NavigationObject() { ID = "Tab_RejectRates", Text = "Reject Rates", Link = "/Site/Financials/ShipperOrderRejectRates.aspx", Permissions = new string[] { "viewShipperOrderRejectRates", "viewCarrierOrderRejectRates", "viewDriverOrderRejectRates" } },
            new NavigationObject() { ID = "Tab_FSCRates", Text = "FSC Rates", Link = "/Site/Financials/ShipperFuelSurchargeRates.aspx", Permissions = new string[] { "viewShipperFuelSurchargeRates", "viewCarrierFuelSurchargeRates", "viewDriverFuelSurchargeRates" } },
            new NavigationObject() { ID = "Tab_PADDPricing", Text = "PADD Pricing", Link = "/Site/Financials/FuelPrices.aspx", Permissions = new string[] { "viewPADDFuelPrices" } }
        };

        public static List<NavigationObject> TabSet_Demurrage = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Tab_DemurrageReasons", Text = "Demurrage Reasons", Link = "/OriginWaitReasons", Permissions = new string[] { "viewReasonCodes"} },
            new NavigationObject() { ID = "Tab_OriginRates", Text = "Origin Rates", Link = "/Site/Financials/ShipperOriginWaitRates.aspx", Permissions = new string[] { "viewShipperOriginWaitRates", "viewCarrierOriginWaitRates", "viewDriverOriginWaitRates" } },
            new NavigationObject() { ID = "Tab_DestinationRates", Text = "Destination Rates", Link = "/Site/Financials/ShipperDestinationWaitRates.aspx", Permissions = new string[] { "viewShipperDestinationWaitRates", "viewCarrierDestinationWaitRates", "viewDriverDestinationWaitRates" } },
            new NavigationObject() { ID = "Tab_Parameters", Text = "Parameters", Link = "/Site/Financials/ShipperWaitFeeParameters.aspx", Permissions = new string[] { "viewShipperWaitFeeParameters", "viewCarrierWaitFeeParameters" } }
        };        

        #endregion
        


        #region BUTTON SET DEFINITIONS

        public static List<NavigationObject> ButtonSet_TabOrderCreate = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Button_TruckOrders", Text = "Truck Orders", Link = "/Site/Orders/Orders_Creation.aspx", Permissions = new string[] { "viewTruckOrders" } },
            new NavigationObject() { ID = "Button_GaugerOrders", Text = "Gauger Orders", Link = "/Site/Orders/GaugerOrders_Creation.aspx", Permissions = new string[] { "viewGaugerOrders" } }
        };

        public static List<NavigationObject> ButtonSet_TabAudit = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Button_Audit", Text = "Audit Orders", Link = "/Site/Orders/Orders_Audit.aspx", Permissions = new string[] { "viewOrderAudit" } },
            new NavigationObject() { ID = "Button_Unaudit", Text = "Unaudit Orders", Link = "/OrderUnaudit", Permissions = new string[] { "unauditOrders" } }
        };

        public static List<NavigationObject> ButtonSet_TabDCDriver = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Button_HeaderImage", Text = "Header Image", Link = "/Site/PrintBM/DAHeaderImage.aspx", Permissions = new string[] { "viewDriverPrintConfig" } },
            new NavigationObject() { ID = "Button_PicktupTemplate", Text = "Pickup Templates", Link = "/Site/PrintBM/DAPickupTemplate.aspx", Permissions = new string[] { "viewDriverPrintConfig" } },
            new NavigationObject() { ID = "Button_TicketTemplate", Text = "Ticket Templates", Link = "/Site/PrintBM/DATicketTemplate.aspx", Permissions = new string[] { "viewDriverPrintConfig" } },
            new NavigationObject() { ID = "Button_DeliverTemplate", Text = "Deliver Templates", Link = "/Site/PrintBM/DADeliverTemplate.aspx", Permissions = new string[] { "viewDriverPrintConfig" } },
            new NavigationObject() { ID = "Button_FooterTemplate", Text = "Footer Templates ", Link = "/Site/PrintBM/DAFooterTemplate.aspx", Permissions = new string[] { "viewDriverPrintConfig" } }

        };

        public static List<NavigationObject> ButtonSet_TabDCGauger = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Button_HeaderImage", Text = "Header Image", Link = "/Site/PrintBM/GAHeaderImage.aspx", Permissions = new string[] { "viewGaugerPrintConfig" } },
            new NavigationObject() { ID = "Button_PickupTemplate", Text = "Pickup Templates", Link = "/Site/PrintBM/GAPickupTemplate.aspx", Permissions = new string[] { "viewGaugerPrintConfig" } },
            new NavigationObject() { ID = "Button_TicketTemplate", Text = "Ticket Templates", Link = "/Site/PrintBM/GATicketTemplate.aspx", Permissions = new string[] { "viewGaugerPrintConfig" } }

        };

        public static List<NavigationObject> ButtonSet_TabUsers = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Button_Users", Text = "User List", Link = "/DCUsers", Permissions = new string[] { "viewUsers" } }          
        };

        public static List<NavigationObject> ButtonSet_TabGroups = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Button_Groups", Text = "Group List", Link = "/DCGroups", Permissions = new string[] { "viewGroups" } }
        };

        public static List<NavigationObject> ButtonSet_TabOrigins = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Button_Origins", Text = "Origins", Link = "/Site/TableMaint/Origins.aspx", Permissions = new string[] { "viewOriginMaintenance" } },
            new NavigationObject() { ID = "Button_OriginTypes", Text = "Origin Types", Link = "/OriginTypes", Permissions = new string[] { "viewOriginTypes" } }
        };

        public static List<NavigationObject> ButtonSet_TabDestinations = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Button_Destinations", Text = "Destinations", Link = "/Site/TableMaint/Destinations.aspx", Permissions = new string[] { "viewDestinationMaintenance" } },
            new NavigationObject() { ID = "Button_DestinationTypes", Text = "Destination Types", Link = "/DestinationTypes", Permissions = new string[] { "viewDestinationTypes" } }
        };

        public static List<NavigationObject> ButtonSet_TabConsignees = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Button_Consignees", Text = "Consignees", Link = "/Consignees", Permissions = new string[] { "viewConsignees" } },
            new NavigationObject() { ID = "Button_DestinationConsignees", Text = "Relationships", Link = "/DestinationConsignees", Permissions = new string[] { "viewConsignees" } }
        };

        public static List<NavigationObject> ButtonSet_TabProducts = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Button_Products", Text = "Products", Link = "/Site/TableMaint/Products.aspx", Permissions = new string[] { "viewProducts" } },
            new NavigationObject() { ID = "Button_ProductGroups", Text = "Product Groups", Link = "/ProductGroups", Permissions = new string[] { "viewProductGroups" } },
            new NavigationObject() { ID = "Button_UOM", Text = "UOM", Link = "/Uoms", Permissions = new string[] { "viewUnitsOfMeasure" } }
        };

        public static List<NavigationObject> ButtonSet_TabAllocation = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Button_AllocationDashboard", Text = "Allocation Dashboard", Link = "/Dashboard", Permissions = new string[] { "viewAllocationDashboard" } },
            new NavigationObject() { ID = "Button_ManageAllocation", Text = "Manage Allocation", Link = "/Site/Allocation/DestinationShipperAllocations.aspx", Permissions = new string[] { "viewDestinationShipperAllocation" } }
        };

        public static List<NavigationObject> ButtonSet_TabDrivers = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Button_Drivers", Text = "Drivers", Link = "/Drivers", Permissions = new string[] { "viewDriverMaintenance" } },
            new NavigationObject() { ID = "Button_DriverScheduling", Text = "Scheduling", Link = "/DriverScheduling", Permissions = new string[] { "viewDriverScheduling" } },
            new NavigationObject() { ID = "Button_SchedulingReport", Text = "Scheduling Report", Link = "/DriverSchedulingReport", Permissions = new string[] { "viewDriverScheduling" } },
            new NavigationObject() { ID = "Button_SchedulingStatuses", Text = "Scheduling Statuses", Link = "/DriverSchedulingStatus", Permissions = new string[] { "viewDriverScheduling" } },
            new NavigationObject() { ID = "Button_SchedulingShiftTypes", Text = "Shift Types", Link = "/DriverShiftType", Permissions = new string[] { "viewDriverScheduling" } }
        };

        public static List<NavigationObject> ButtonSet_TabEquipment = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Button_Equipment", Text = "Equipment", Link = "/DriverEquipment", Permissions = new string[] { "viewDriverEquipment" } },
            new NavigationObject() { ID = "Button_Manufacturers", Text = "Equipment Manufacturers", Link = "/DriverEquipmentManufacturers", Permissions = new string[] { "viewDriverEquipmentManufacturers" } },
            new NavigationObject() { ID = "Button_AppHistory", Text = "Driver App History", Link = "/Site/TableMaint/DriverUpdate.aspx", Permissions = new string[] { "viewDriverAppHistory" } }
        };

        public static List<NavigationObject> ButtonSet_TabTrucks = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Button_Trucks", Text = "Trucks", Link = "/Site/TableMaint/Trucks.aspx", Permissions = new string[] { "viewTrucks" } },
            new NavigationObject() { ID = "Button_TruckTypes", Text = "Truck Types", Link = "/TruckTypes", Permissions = new string[] { "viewTruckTypes" } }
        };

        public static List<NavigationObject> ButtonSet_TabTrailers = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Button_Trailers", Text = "Trailers", Link = "/Site/TableMaint/Trailers.aspx", Permissions = new string[] { "viewTrailers" } },
            new NavigationObject() { ID = "Button_TrailerTypes", Text = "Trailer Types", Link = "/TrailerTypes", Permissions = new string[] { "viewTrailerTypes" } }
        };

        public static List<NavigationObject> ButtonSet_TabCarriers = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Button_Carriers", Text = "Carriers", Link = "/Site/TableMaint/Carriers.aspx", Permissions = new string[] { "viewCarrierMaintenance" } },
            new NavigationObject() { ID = "Button_CarrierTypes", Text = "Carrier Types", Link = "/Carriers/Types", Permissions = new string[] { "viewCarrierTypes" } }
        };

        public static List<NavigationObject> ButtonSet_TabMaintenance = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Button_TruckTrailerMaintenanceTypes", Text = "Types", Link = "/TruckTrailerMaintenanceTypes", Permissions = new string[] { "viewTruckTrailerMaintenanceTypes" } },
            new NavigationObject() { ID = "Button_TruckMaintenance", Text = "Trucks", Link = "/TruckMaintenance", Permissions = new string[] { "viewTruckMaintenance" } },
            new NavigationObject() { ID = "Button_TrailerMaintenance", Text = "Trailers", Link = "/TrailerMaintenance", Permissions = new string[] { "viewTrailerMaintenance" } }
        };

        public static List<NavigationObject> ButtonSet_CarrierDocs = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Button_CarrierDocs", Text = "Compliance", Link = "/CarrierCompliance", Permissions = new string[] { "viewCarrierCompliance" } },
            new NavigationObject() { ID = "Button_ComplianceTypes", Text = "Doc Types", Link = "/CarrierComplianceTypes", Permissions = new string[] { "viewCarrierComplianceTypes" } }
        };

        public static List<NavigationObject> ButtonSet_DriverDocs = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Button_DriverDocs", Text = "Compliance", Link = "/DriverCompliance", Permissions = new string[] { "viewDriverCompliance" } },
            new NavigationObject() { ID = "Button_ComplianceTypes", Text = "Doc Types", Link = "/DriverComplianceTypes", Permissions = new string[] { "viewDriverComplianceTypes" } },
        };

        public static List<NavigationObject> ButtonSet_TruckDocs = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Button_TruckDocs", Text = "Compliance", Link = "/TruckCompliance", Permissions = new string[] { "viewTruckCompliance" } },
            new NavigationObject() { ID = "Button_ComplianceTypes", Text = "Doc Types", Link = "/TruckComplianceTypes", Permissions = new string[] { "viewTruckComplianceTypes" } },
        };

        public static List<NavigationObject> ButtonSet_TrailerDocs = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Button_TrailerDocs", Text = "Compliance", Link = "/TrailerCompliance", Permissions = new string[] { "viewTrailerCompliance" } },
            new NavigationObject() { ID = "Button_ComplianceTypes", Text = "Doc Types", Link = "/TrailerComplianceTypes", Permissions = new string[] { "viewTrailerComplianceTypes" } },
        };

        public static List<NavigationObject> ButtonSet_Questionnaires = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Button_Templates", Text = "Templates", Link = "/Questionnaires", Permissions = new string[] { "viewQuestionnaires" } },
            new NavigationObject() { ID = "Button_Submissions", Text = "Submissions", Link = "/Questionnaires/Submissions", Permissions = new string[] { "viewQuestionnaires" } },
            new NavigationObject() { ID = "Button_Types", Text = "Types", Link = "/Questionnaires/Types", Permissions = new string[] { "viewQuestionnaires" } }
        };

        public static List<NavigationObject> ButtonSet_HOS = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Button_DailyReport", Text = "Daily Report", Link = "/HOS/Daily", Permissions = new string[] { "viewHOS" } },
            new NavigationObject() { ID = "Button_Violations", Text = "Violations", Link = "/HOS/Violations", Permissions = new string[] { "viewHOS" } },
            new NavigationObject() { ID = "Button_PersonalConveyance", Text = "Personal Conveyance", Link = "/HOS/PersonalConveyance", Permissions = new string[] { "viewHOS" } }
        };

        public static List<NavigationObject> ButtonSet_TabPricing = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Button_Indexes", Text = "Indexes", Link = "/CommodityPricing/Indexes", Permissions = new string[] { "viewCommodityPricing" } },
            new NavigationObject() { ID = "Button_Prices", Text = "Prices", Link = "/CommodityPricing/Prices", Permissions = new string[] { "viewCommodityPricing" } },
            new NavigationObject() { ID = "Button_Methods", Text = "Methods", Link = "/CommodityPricing/Methods", Permissions = new string[] { "viewCommodityPricing" } },
            new NavigationObject() { ID = "Button_NonTrade", Text = "Non-Trade Days", Link = "/CommodityPricing/NonTradeDays", Permissions = new string[] { "viewCommodityPricing" } },
            new NavigationObject() { ID = "Button_Pricebook", Text = "Pricebook", Link = "/CommodityPricing/Pricebook", Permissions = new string[] { "viewCommodityPricing" } }
        };

        public static List<NavigationObject> ButtonSet_TabDriverGroups = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Button_Groups", Text = "Driver Groups", Link = "/Site/Financials/DriverGroups.aspx", Permissions = new string[] { "viewCarrierDriverSettlementGroups" } },
            new NavigationObject() { ID = "Button_GroupAssignement", Text = "Group Assignment", Link = "/Site/Financials/Drivers.aspx", Permissions = new string[] { "viewCarrierDriverSettlementGroupAssignments" } }
        };

        public static List<NavigationObject> ButtonSet_TabPdfExports = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Button_PdfExports", Text = "Pdf Exports", Link = "/PdfExport", Permissions = new string[] { "viewPdfExport" } },
            new NavigationObject() { ID = "Button_DriverSettlementStatementEmails", Text = "Driver Settlement Statement Emails", Link = "/DriverSettlementStatementEmail", Permissions = new string[] { "viewDriverSettlementStatementEmail" } },
            new NavigationObject() { ID = "Button_DriverSettlementStatementEmailDefinitions", Text = "Driver Settlement Statement Email Definitions", Link = "/DriverSettlementStatementEmailDefinition", Permissions = new string[] { "viewDriverSettlementStatementEmailDefinition" } },
        };

        public static List<NavigationObject> ButtonSet_TabSettlement = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Button_Shipper", Text = "Shippers", Link = "/Site/Financials/ShipperInvoicing.aspx", Permissions = new string[] { "viewShipperSettlement" } },
            new NavigationObject() { ID = "Button_Carrier", Text = "Carriers", Link = "/Site/Financials/CarrierInvoicing.aspx", Permissions = new string[] { "viewCarrierSettlement" } },
            new NavigationObject() { ID = "Button_Driver", Text = "Drivers", Link = "/Site/Financials/DriverInvoicing.aspx", Permissions = new string[] { "viewDriverSettlement" } }
        };

        public static List<NavigationObject> ButtonSet_TabBatches = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Button_Shipper", Text = "Shippers", Link = "/Site/Financials/ShipperBatches.aspx", Permissions = new string[] { "viewShipperSettlementBatches" } },
            new NavigationObject() { ID = "Button_Carrier", Text = "Carriers", Link = "/Site/Financials/CarrierBatches.aspx", Permissions = new string[] { "viewCarrierSettlementBatches" } },
            new NavigationObject() { ID = "Button_Driver", Text = "Drivers", Link = "/Site/Financials/DriverBatches.aspx", Permissions = new string[] { "viewDriverSettlementBatches" } }
        };

        public static List<NavigationObject> ButtonSet_TabSettleUnits = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Button_Shipper", Text = "Shippers", Link = "/Site/Financials/ShipperSettlementFactor.aspx", Permissions = new string[] { "viewShipperSettlementUnits" } },
            new NavigationObject() { ID = "Button_Carrier", Text = "Carriers", Link = "/Site/Financials/CarrierSettlementFactor.aspx", Permissions = new string[] { "viewCarrierSettlementUnits" } }
        };

        public static List<NavigationObject> ButtonSet_TabMinSettleUnits = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Button_Shipper", Text = "Shippers", Link = "/Site/Financials/ShipperMinSettlementUnits.aspx", Permissions = new string[] { "viewShipperMinSettlementUnits" } },
            new NavigationObject() { ID = "Button_Carrier", Text = "Carriers", Link = "/Site/Financials/CarrierMinSettlementUnits.aspx", Permissions = new string[] { "viewCarrierMinSettlementUnits" } }
        };

        public static List<NavigationObject> ButtonSet_TabSettlementLog = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Button_Shipper", Text = "Shippers", Link = "/OrderSettlement/ShipperIndex", Permissions = new string[] { "viewShipperSettlementBatches" } },
            new NavigationObject() { ID = "Button_Carrier", Text = "Carriers", Link = "/OrderSettlement/CarrierIndex", Permissions = new string[] { "viewCarrierSettlementBatches" } },
            new NavigationObject() { ID = "Button_Driver", Text = "Drivers", Link = "/OrderSettlement/DriverIndex", Permissions = new string[] { "viewDriverSettlementBatches" } }
        };


        public static List<NavigationObject> ButtonSet_RateSheets = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Button_Shipper", Text = "Shippers", Link = "/Site/Financials/ShipperRateSheets.aspx", Permissions = new string[] { "viewShipperRateSheets" } },
            new NavigationObject() { ID = "Button_Carrier", Text = "Carriers", Link = "/Site/Financials/CarrierRateSheets.aspx", Permissions = new string[] { "viewCarrierRateSheets" } },
            new NavigationObject() { ID = "Button_Driver", Text = "Drivers", Link = "/Site/Financials/DriverRateSheets.aspx", Permissions = new string[] { "viewDriverRateSheets" } }
        };

        public static List<NavigationObject> ButtonSet_OverrideRates = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Button_Shipper", Text = "Shippers", Link = "/Site/Financials/ShipperRouteRates.aspx", Permissions = new string[] { "viewShipperRouteRates" } },
            new NavigationObject() { ID = "Button_Carrier", Text = "Carriers", Link = "/Site/Financials/CarrierRouteRates.aspx", Permissions = new string[] { "viewCarrierRouteRates" } },
            new NavigationObject() { ID = "Button_Driver", Text = "Drivers", Link = "/Site/Financials/DriverRouteRates.aspx", Permissions = new string[] { "viewDriverRouteRates" } }
        };

        public static List<NavigationObject> ButtonSet_TabDemurrageReasons = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Button_Origin", Text = "Origin", Link = "/OriginWaitReasons", Permissions = new string[] { "viewReasonCodes" } },
            new NavigationObject() { ID = "Button_Destination", Text = "Destination", Link = "/DestinationWaitReasons", Permissions = new string[] { "viewReasonCodes" } }
        };

        public static List<NavigationObject> ButtonSet_TabRejectReasons = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Button_Order", Text = "Order", Link = "/OrderRejectReasons", Permissions = new string[] { "viewReasonCodes" } },
            new NavigationObject() { ID = "Button_Ticket", Text = "Ticket", Link = "/OrderTicketRejectReasons", Permissions = new string[] { "viewReasonCodes" } }
        };

        public static List<NavigationObject> ButtonSet_TabAccessorialRates = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Button_Types", Text = "Types", Link = "/Site/Financials/AccessorialRateTypes.aspx", Permissions = new string[] { "viewAssessorialRateTypes" } },
            new NavigationObject() { ID = "Button_Shipper", Text = "Shipper", Link = "/Site/Financials/ShipperAccessorialRates.aspx", Permissions = new string[] { "viewShipperAssessorialRates" } },
            new NavigationObject() { ID = "Button_Carrier", Text = "Carrier", Link = "/Site/Financials/CarrierAccessorialRates.aspx", Permissions = new string[] { "viewCarrierAssessorialRates" } },
            new NavigationObject() { ID = "Button_Driver", Text = "Driver", Link = "/Site/Financials/DriverAccessorialRates.aspx", Permissions = new string[] { "viewDriverAssessorialRates" } }
        };

        public static List<NavigationObject> ButtonSet_TabRejectRates = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Button_Shipper", Text = "Shipper", Link = "/Site/Financials/ShipperOrderRejectRates.aspx", Permissions = new string[] { "viewShipperOrderRejectRates" } },
            new NavigationObject() { ID = "Button_Carrier", Text = "Carrier", Link = "/Site/Financials/CarrierOrderRejectRates.aspx", Permissions = new string[] { "viewCarrierFuelSurchargeRates" } },
            new NavigationObject() { ID = "Button_Driver", Text = "Driver", Link = "/Site/Financials/DriverOrderRejectRates.aspx", Permissions = new string[] { "viewDriverFuelSurchargeRates" } }
        };

        public static List<NavigationObject> ButtonSet_TabFSCRates = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Button_Shipper", Text = "Shipper", Link = "/Site/Financials/ShipperFuelSurchargeRates.aspx", Permissions = new string[] { "viewShipperFuelSurchargeRates" } },
            new NavigationObject() { ID = "Button_Carrier", Text = "Carrier", Link = "/Site/Financials/CarrierFuelSurchargeRates.aspx", Permissions = new string[] { "viewCarrierFuelSurchargeRates" } },
            new NavigationObject() { ID = "Button_Driver", Text = "Driver", Link = "/Site/Financials/DriverFuelSurchargeRates.aspx", Permissions = new string[] { "viewDriverFuelSurchargeRates" } }
        };

        public static List<NavigationObject> ButtonSet_TabOriginRates = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Button_Shipper", Text = "Shipper", Link = "/Site/Financials/ShipperOriginWaitRates.aspx", Permissions = new string[] { "viewShipperOriginWaitRates" } },
            new NavigationObject() { ID = "Button_Carrier", Text = "Carrier", Link = "/Site/Financials/CarrierOriginWaitRates.aspx", Permissions = new string[] { "viewCarrierOriginWaitRates" } },
            new NavigationObject() { ID = "Button_Driver", Text = "Driver", Link = "/Site/Financials/DriverOriginWaitRates.aspx", Permissions = new string[] { "viewDriverOriginWaitRates" } }
        };

        public static List<NavigationObject> ButtonSet_TabDestinationRates = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Button_Shipper", Text = "Shipper", Link = "/Site/Financials/ShipperDestinationWaitRates.aspx", Permissions = new string[] { "viewShipperDestinationWaitRates" } },
            new NavigationObject() { ID = "Button_Carrier", Text = "Carrier", Link = "/Site/Financials/CarrierDestinationWaitRates.aspx", Permissions = new string[] { "viewCarrierDestinationWaitRates" } },
            new NavigationObject() { ID = "Button_Driver", Text = "Driver", Link = "/Site/Financials/DriverDestinationWaitRates.aspx", Permissions = new string[] { "viewDriverDestinationWaitRates" } }
        };

        public static List<NavigationObject> ButtonSet_TabParameters = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Button_Shipper", Text = "Shipper", Link = "/Site/Financials/ShipperWaitFeeParameters.aspx", Permissions = new string[] { "viewShipperWaitFeeParameters" } },
            new NavigationObject() { ID = "Button_Carrier", Text = "Carrier", Link = "/Site/Financials/CarrierWaitFeeParameters.aspx", Permissions = new string[] { "viewCarrierWaitFeeParameters" } }
        };

        public static List<NavigationObject> ButtonSet_TabOriginTanks = new List<NavigationObject>
        {
            new NavigationObject() { ID = "Button_OriginTanks", Text = "Tanks", Link = "/OriginTanks", Permissions = new string[] { "viewOriginTanks" } },
            new NavigationObject() { ID = "Button_OriginTankDefinitions", Text = "Tank Definitions", Link = "/OriginTankDefinitions", Permissions = new string[] { "viewOriginTankDefinitions" } }
        };

        #endregion
    }    
}