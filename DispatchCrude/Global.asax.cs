﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Http;
using System.Web.Routing;
using System.Web.Optimization;
using DispatchCrude.Core;
using AlonsIT;

namespace DispatchCrude
{
    public class Global : HttpApplication
    {

		protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            CustomModelBinderProvider.Register();

            //This removes the response header (fixes a security risk)
            MvcHandler.DisableMvcResponseHeader = true;

            // custom date binder to handle Kendo MVC grids binding to date fields
            // see: http://www.telerik.com/forums/the-value-'-date(-62135578800000)-'-is-not-valid-for
            CustomDateBinder.Register(ModelBinders.Binders);
        }

        void Application_End(object sender, EventArgs e)
        {
            //  Code that runs on application shutdown

        }

        void Application_BeginRequest(object sender, EventArgs e)
        {
            SSDB.ConnectionFactory = new App_Code.DispatchCrude_ConnFactory();

            // redirect all non-SSL requests to an SSL url if the UseSSL web.config option is configured
            //  EXCEPT mobile app syncing (which would likely cause issues, so don't FORCE them to SSL but accept SSL)
            if (!HttpContext.Current.Request.IsSecureConnection && DispatchCrude.Controllers.RequireHttpsConditional.UseSSL 
                && !HttpContext.Current.Request.RawUrl.ToLower().StartsWith("/driverapp")
                && !HttpContext.Current.Request.RawUrl.ToLower().StartsWith("/gaugerapp"))
            {
                int sslPort = DispatchCrude.Controllers.RequireHttpsConditional.SSL_Port;
                Response.Redirect(string.Format("https://{0}{1}{2}"
                    , Request.ServerVariables["HTTP_HOST"] 
                    , (sslPort > 0 ? ":" + sslPort.ToString() : "")
                    , HttpContext.Current.Request.RawUrl));
            }        
        }

        void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs

        }

        void Session_Start(object sender, EventArgs e)
        {
            // Redirect mobile users to the mobile site
            //HttpRequest httpRequest = HttpContext.Current.Request;
            //if (httpRequest.Browser.IsMobileDevice)
            //{
            //    string path = httpRequest.Url.PathAndQuery;
            //    bool isOnMobilePage = path.StartsWith("/Mobile/", StringComparison.OrdinalIgnoreCase);
            //    if (!isOnMobilePage)
            //    {
            //        string redirectTo = "~/Mobile/";

            //        // Could also add special logic to redirect from certain 
            //        // recognised pages to the mobile equivalents of those 
            //        // pages (where they exist). For example,
            //        // if (HttpContext.Current.Handler is UserRegistration)
            //        //     redirectTo = "~/Mobile/Register.aspx";

            //        HttpContext.Current.Response.Redirect(redirectTo);
            //    }
            //}
        }
        void Session_End(object sender, EventArgs e)
        {
            // Code that runs when a session ends. 
            // Note: The Session_End event is raised only when the sessionstate mode
            // is set to InProc in the Web.config file. If session mode is set to StateServer 
            // or SQLServer, the event is not raised.

        }

    }
}
