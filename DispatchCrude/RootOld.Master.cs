﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DispatchCrude.Core;

namespace DispatchCrude
{
    public partial class RootOldMaster : System.Web.UI.MasterPage
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Converter.IsNullOrEmpty(Page.Title))
                PageTitleText = Page.Title;

            if (Request.Browser.MSDomVersion.Major == 0) // Non IE Browser?
            {
                Response.Cache.SetNoStore(); // No client side cashing for non IE browsers
            }
            //if (UserSupport.IsAdmin(//))
            //{
            //    // show the hlSwitchUser control (when admins are logged in )
            //    this.FindControl("HeadLoginView").Controls[0].Controls[5].Visible = true;
            //    //(this.FindControl("hlSwitchUser") as HyperLink).Visible = true;
            //}

            //Detect Internet Explorer / force user to get another browser before using DC
            string browserName = Request.Browser.Browser;
            if (browserName == "InternetExplorer")
            {
                Response.Redirect("~/noIESupport.html");
            }
        }

        protected void NavigationMenu_MenuItemDataBound(object sender, MenuEventArgs e)
        {
            // Check webforms menu for certain MVC pages and hide based on permissions
            System.Web.UI.WebControls.Menu menu = (System.Web.UI.WebControls.Menu) sender;
            SiteMapNode mapNode = (SiteMapNode) e.Item.DataItem;
            if (   (mapNode.Title == "Order Edit" && !UserSupport.IsInRole("Administrator")
                                                  && !UserSupport.IsInRole("viewOrders")
                                                  && !UserSupport.IsInRole("editOrders"))
                || (mapNode.Title == "Un-Audit" && !UserSupport.IsInRole("Administrator"))
                || (mapNode.Title == "Un-Delete"  && !UserSupport.IsInRole("Administrator"))
                || (mapNode.Title == "Sync Errors" && !UserSupport.IsInRole("Administrator")))
            {
                if (e.Item.Parent != null)
                    e.Item.Parent.ChildItems.Remove(e.Item);
            }
        }

        public string PageTitleText
        {
            get
            {
                return lblRootMasterPageTitle.Text;
            }
            set
            {
                lblRootMasterPageTitle.Text = value;
                //lblRootMasterPageTitle.Visible = value.Length > 0;
                PageSep.Visible = PageTitleText.Length > 0 && EntityCaptionText.Length > 0;
                divSecondaryTitle.Visible = lblRootMasterPageTitle.Text.Length > 0 || EntityCaption.Text.Length > 0;
            }
        }
        public string EntityCaptionText
        {
            get
            {
                return EntityCaption.Text;
            }
            set
            {
                EntityCaption.Text = value;
                EntityCaption.Visible = value.Length > 0;
                PageSep.Visible = PageTitleText.Length > 0 && EntityCaptionText.Length > 0;
                divSecondaryTitle.Visible = lblRootMasterPageTitle.Text.Length > 0 || EntityCaption.Text.Length > 0;
            }
        }
    }
}
