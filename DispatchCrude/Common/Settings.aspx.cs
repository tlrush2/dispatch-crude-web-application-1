﻿using System;
using AlonsIT;

namespace DispatchCrude.Common
{

    public partial class Settings : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            App_Code.RadAjaxHelper.AddAjaxSetting(this.Page, rgMain, rgMain);
            Telerik.Web.UI.RadAjaxManager.GetCurrent(this.Page).EnableAJAX = true;
        }

    }
}