﻿<%@ Page Title="System Settings" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Settings.aspx.cs" Inherits="DispatchCrude.Common.Settings" ValidateRequest="false" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="DataEntryFormGrid" >
        <telerik:RadGrid ID="rgMain" runat="server" AutoGenerateColumns="false" ShowGroupPanel="True"
            AllowPaging="True" AllowFilteringByColumn="true" AllowSorting="True" CellSpacing="0" EnableEmbeddedSkins="true" Skin="Vista"  AlternatingItemStyle-BackColor="#dcf2fc"
            PageSize='<%# Settings.DefaultPageSize %>'
            GridLines="None" EnableHeaderContextMenu="False" Height="800" CssClass="GridRepaint">
            <ClientSettings AllowDragToGroup="true" >
                <Scrolling AllowScroll="True" UseStaticHeaders="True" />
            </ClientSettings>
            <GroupingSettings CaseSensitive="false" ShowUnGroupButton="true" />
            <MasterTableView AutoGenerateColumns="False" EditMode="InPlace" GroupsDefaultExpanded="false"
                DataKeyNames="ID" CommandItemDisplay="Top" >
                <CommandItemSettings ExportToPdfText="Export to PDF" ShowAddNewRecordButton="false" />
                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                    <HeaderStyle Width="20px" />
                </RowIndicatorColumn>
                <Columns>
                    <telerik:GridEditCommandColumn ButtonType="ImageButton" 
                        EditImageUrl="~/images/edit.png" EditText="Edit" 
                        CancelImageUrl="~/images/cancel.png" 
                        UpdateImageUrl="~/images/apply.png" >
                        <HeaderStyle Width="50px" />
                    </telerik:GridEditCommandColumn>
                    <telerik:GridBoundColumn DataField="ID" DataType="System.Int32" ReadOnly="True"
                        UniqueName="ID" Visible="False" />
                    <telerik:GridBoundColumn DataField="Category" ReadOnly="true" UniqueName="Category" HeaderText="Category" 
                        HeaderStyle-Width="140px" FilterControlWidth="70%" />
                    <telerik:GridBoundColumn DataField="Name" ReadOnly="true" UniqueName="Name" HeaderText="Setting Name" HeaderStyle-Width="100px" />
                    <telerik:GridBoundColumn DataField="SettingType" UniqueName="SettingType" HeaderText="Setting Type" ReadOnly="true"
                        FilterControlWidth="70%" HeaderStyle-Width="100px" />
                    <telerik:GridBoundColumn DataField="Value" UniqueName="Value" HeaderText="Setting Value" HeaderStyle-Width="100px" />
                </Columns>
                <GroupByExpressions>
                    <telerik:GridGroupByExpression>
                        <SelectFields>
                            <telerik:GridGroupByField FieldAlias="Category" FieldName="Category" FormatString="" HeaderText="Category" />
                        </SelectFields>
                        <GroupByFields>
                            <telerik:GridGroupByField FieldAlias="Category" FieldName="Category" FormatString="" HeaderText="" />
                        </GroupByFields>
                    </telerik:GridGroupByExpression>
                </GroupByExpressions>
                <PagerStyle AlwaysVisible="True" />
            </MasterTableView>
            <HeaderStyle Wrap="False" />
            <PagerStyle AlwaysVisible="True" />
            <FilterMenu EnableImageSprites="False">
            </FilterMenu>
        </telerik:RadGrid>
    </div>
    <blc:RadGridDBCtrl ID="rgdsMain" runat="server"
        ControlID="rgMain"
        UpdateTableName="tblSetting"
        SelectCommand="SELECT * FROM viewSetting WHERE Hidden = 0 ORDER BY Category, Name" />
</asp:Content>