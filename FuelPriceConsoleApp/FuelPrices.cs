﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Data;
using System.IO;
using Syncfusion.XlsIO;
using AlonsIT;

namespace FuelPriceConsoleApp
{
    class FuelPrices
    {
        public string Retrieve(DateTime dataDate)
        {
            string result = null;

            try
            {
                byte[] xlsFileBytes = null;
                string sourceUrl = AppSettings.GetAppSetting("SourceUrl");
                bool isLocal = new Uri(sourceUrl).Host.Length == 0;
                if (!isLocal)
                {
                    System.Net.WebClient webClient = new WebClient();
                    xlsFileBytes = webClient.DownloadData(sourceUrl);

                    // if these settings exist, then save the source data file to disk
                    if (AppSettings.GetAppSetting("FileSaveDirectory").Length > 0 && System.IO.Directory.Exists(AppSettings.GetAppSetting("FileSaveDirectory"))
                        && AppSettings.GetAppSetting("FileSaveName").Length > 0)
                    {
                        string filename = Path.Combine(AppSettings.GetAppSetting("FileSaveDirectory"), AppSettings.GetAppSetting("FileSaveName"));
                        if (DBHelper.ToBoolean(AppSettings.GetAppSetting("FileSaveReplace")) && File.Exists(filename))
                            File.Delete(filename);

                        FileStream fs = new FileStream(filename, FileMode.Create, FileAccess.Write);
                        fs.Write(xlsFileBytes, 0, xlsFileBytes.Length);
                        fs.Close();
                    }
                }

                using (AlonsIT.SSDB ssdb = new AlonsIT.SSDB())
                {
                    System.Data.DataTable dtPADDs = ssdb.GetPopulatedDataTable("SELECT *, ColNum = cast(0 AS int) FROM tblEIAPADDRegion ORDER BY Name");

                    using (ExcelEngine excelEngine = new ExcelEngine())
                    {
                        IApplication excelApp = null;
                        IWorkbook wb = null;
                        IWorksheet ws = null;

                        excelApp = excelEngine.Excel;
                        if (xlsFileBytes == null && sourceUrl.Length > 0 && File.Exists(sourceUrl))
                            wb = excelApp.Workbooks.Open(sourceUrl, ExcelOpenType.BIFF);
                        else
                        {
                            Stream s = new MemoryStream(xlsFileBytes, false);
                            wb = excelApp.Workbooks.Open(s, ExcelOpenType.BIFF);
                        }
                        ws = wb.Worksheets["Data 2"];

                        int row = 2;

                        // get the Columns for each desired data row
                        int remaining = dtPADDs.Rows.Count;
                        if (ws.Range[row, 1].Text.Equals("SourceKey", StringComparison.CurrentCultureIgnoreCase))
                        {
                            int col = 2;
                            string sourceKey = null;
                            while(remaining > 0 && (sourceKey = ws.Range[row, col].Text).Length > 0)
                            {
                                DataRow[] drsSourceKey = dtPADDs.Select("SourceKey='" + sourceKey + "'");
                                if (drsSourceKey.Length > 0)
                                {
                                    drsSourceKey[0]["ColNum"] = col;
                                    remaining--;
                                }
                                col++;
                            }
                        }
                        // successful so far, so now retrieve the actual data
                        if (remaining > 0)
                            result = "Failure: invalid data file provided: missing data columns matching SourceKey values";
                        else
                        {
                            row = 4;
                            DateTime priceDate = new DateTime();
                            bool valid = false;
                            while ((valid = DateTime.TryParse(ws.Range[row, 1].DisplayText, out priceDate)) && priceDate.Date != dataDate.Date)
                                row++;

                            if (!valid)
                                result = string.Format("Failure: Data for date = {0:M/d/yy} not found", dataDate);
                            else
                            {
                                // get the actual data
                                foreach (DataRow drPADDPrice in dtPADDs.Rows)
                                {
                                    decimal price = DBHelper.ToDecimal(ws.Range[row, DBHelper.ToInt32(drPADDPrice["ColNum"])].Value);
                                    ssdb.ExecuteSql("INSERT INTO tblFuelPrice (Price, EIAPADDRegionID, EffectiveDate, CreateDateUTC, CreatedByUser) "
                                        + "VALUES ({0}, {1}, {2}, getutcdate(), 'System')"
                                        , price
                                        , drPADDPrice["ID"]
                                        // actually post the data for Tuesday (when it actually applies)
                                        , DBHelper.QuoteStr(dataDate.AddDays(1).ToShortDateString()));
                                }
                                result = string.Format("Success: Data loaded for {0:M/d/yy}", dataDate);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("duplicate key row"))
                    result = "Success: Data already loaded";
                else
                    result = "Failure: " + ex.Message;
            }

            return result;
        }
    }
}
