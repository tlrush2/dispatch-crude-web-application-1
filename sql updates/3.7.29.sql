-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.7.26.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.7.26.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0
-- backup database [dispatchcrude.dev] to disk = 'C:\Program Files\Microsoft SQL Server\MSSQL10_50.SQLEXPRESSDEV02\MSSQL\Backup\3.7.23-gsm.bak'

SET NOEXEC OFF
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.7.28'
SELECT  @NewVersion = '3.7.29'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'Settlement Module: Add Producer criteria to Shipper Wait & Reject Rates'
	UNION SELECT @NewVersion, 1, 'Settlement Module: Separate Shipper Min Settlement Units & Settlement Units into Best-Match rate pages'
	UNION SELECT @NewVersion, 1, 'Settlement Module: Add Min & Max Units Wait Fee Parameter limits'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

EXEC _spDropIndex 'udxShipperAssessorialRate_Main'
EXEC _spDropIndex 'idxShipperAssessorialRate_Operator'
GO
ALTER TABLE tblShipperAssessorialRate DROP CONSTRAINT FK_ShipperAssessorialRate_Operator
ALTER TABLE tblShipperAssessorialRate DROP COLUMN OperatorID
GO
CREATE UNIQUE CLUSTERED INDEX udxShipperAssessorialRate_Main ON tblShipperAssessorialRate
(
	TypeID ASC,
	ShipperID ASC,
	ProductGroupID ASC,
	OriginID ASC,
	DestinationID ASC,
	OriginStateID ASC,
	DestStateID ASC,
	RegionID ASC,
	ProducerID ASC,
	EffectiveDate ASC
)
GO

/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all ShipperAssessorialRate records
-- Changes:
	- 3.7.29 - 2015/06/18 - KDA - move ProducerID parameter/field to end of criteria to be consistent with other "Best-Match" tables
******************************************************/
ALTER VIEW viewShipperAssessorialRate AS
	SELECT X.*
		, MaxEffectiveDate = (SELECT MIN(OrderDate) FROM viewOrderSettlementShipperAssessorialCharge WHERE AssessorialRateID = X.ID AND BatchID IS NOT NULL)
		, MinEndDate = (SELECT MAX(OrderDate) FROM viewOrderSettlementShipperAssessorialCharge WHERE AssessorialRateID = X.ID AND BatchID IS NOT NULL)
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM viewOrderSettlementShipperAssessorialCharge WHERE AssessorialRateID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblShipperAssessorialRate XN 
			WHERE XN.TypeID = X.TypeID
			  AND dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EffectiveDate > X.EffectiveDate)
		, PriorEndDate = (
			SELECT max(XN.EndDate) 
			FROM tblShipperAssessorialRate XN 
			WHERE XN.TypeID = X.TypeID
			  AND dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EndDate < X.EffectiveDate)
	FROM tblShipperAssessorialRate X

GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper AssessorialRate info for the specified order
-- Changes:
	- 3.7.29 - 2015/06/18 - KDA - move ProducerID parameter/field to end of criteria to be consistent with other "Best-Match" tables
***********************************/
ALTER FUNCTION fnShipperAssessorialRates(@StartDate date, @EndDate date, @TypeID int, @ShipperID int, @ProductGroupID int, @OriginID int, @DestinationID int, @OriginStateID int, @DestStateID int, @RegionID int, @ProducerID int, @BestMatchOnly bit = 0)
RETURNS 
	@ret TABLE (
		ID int
	  , TypeID int
	  , ShipperID int
	  , ProductGroupID int
	  , OriginID int
	  , DestinationID int
	  , OriginStateID int
	  , DestStateID int
	  , RegionID int
	  , ProducerID int
	  , Rate decimal(18, 10)
	  , RateTypeID int
	  , UomID int
	  , EffectiveDate date
	  , EndDate date
	  , MaxEffectiveDate date
	  , MinEndDate date
	  , NextEffectiveDate date
	  , PriorEndDate date
	  , BestMatch bit
	  , Ranking smallmoney
	  , Locked bit
	  , CreateDateUTC datetime
	  , CreatedByUser varchar(100)
	  , LastChangeDateUTC datetime
	  , LastChangedByUser varchar(100)
	)
AS BEGIN
	-- get the raw data (all matched rows)
	DECLARE @src TABLE (ID int, TypeID int, Ranking smallmoney)
	INSERT INTO @src (ID, TypeID, Ranking)
		SELECT ID, TypeID
			, Ranking =	dbo.fnRateRanking(@ShipperID, R.ShipperID, 128, 0)
					  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 64, 0)
					  + dbo.fnRateRanking(@OriginID, R.OriginID, 32, 0)
					  + dbo.fnRateRanking(@DestinationID, R.DestinationID, 16, 0)
					  + dbo.fnRateRanking(@OriginStateID, R.OriginStateID, 8, 0)
					  + dbo.fnRateRanking(@DestStateID, R.DestStateID, 4, 0)
					  + dbo.fnRateRanking(@RegionID, R.RegionID, 2, 0)
					  + dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 1)
		FROM dbo.viewShipperAssessorialRate R
		WHERE coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(R.ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@OriginID, 0), R.OriginID, 0) = coalesce(R.OriginID, nullif(@OriginID, 0), 0)
		  AND coalesce(nullif(@DestinationID, 0), R.DestinationID, 0) = coalesce(R.DestinationID, nullif(@DestinationID, 0), 0)
		  AND coalesce(nullif(@OriginStateID, 0), R.OriginStateID, 0) = coalesce(R.OriginStateID, nullif(@OriginStateID, 0), 0)
		  AND coalesce(nullif(@DestStateID, 0), R.DestStateID, 0) = coalesce(R.DestStateID, nullif(@DestStateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(R.RegionID, nullif(@RegionID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(R.ProducerID, nullif(@ProducerID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
		  AND (nullif(@TypeID, 0) IS NULL OR TypeID = @TypeID)

	-- return the rate data for the best-match rates 
	INSERT INTO @ret (ID, TypeID, ShipperID, ProductGroupID, OriginID, DestinationID, OriginStateID, DestStateID, RegionID, ProducerID, Rate, RateTypeID, UomID, EffectiveDate, EndDate
			, MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate, BestMatch, Ranking, Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser)
		SELECT CAR.ID, TypeID, ShipperID, ProductGroupID, OriginID, DestinationID, OriginStateID, DestStateID, RegionID, ProducerID, Rate, RateTypeID, UomID, EffectiveDate, EndDate
			, MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate, BestMatch, Ranking, Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser
		FROM viewShipperAssessorialRate CAR
		JOIN (
			SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.TypeID IS NULL THEN 0 ELSE 1 END as bit)
			FROM @src S
			LEFT JOIN (
				SELECT TypeID, Ranking = MAX(Ranking)
				FROM @src
				WHERE @StartDate = ISNULL(@EndDate, @StartDate)
				  AND Ranking % 1 = 0.01 * 8  -- ensure some type of match occurred on all criteria choices
				GROUP BY TypeID
			) X ON X.TypeID = S.TypeID AND X.Ranking = S.Ranking
		) X ON X.ID = CAR.ID
		WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
		
	RETURN
END
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper AssessorialRate rows for the specified criteria
-- Changes:
	- 3.7.29 - 2015/06/18 - KDA - move ProducerID parameter/field to end of criteria to be consistent with other "Best-Match" tables
***********************************/
ALTER FUNCTION fnShipperAssessorialRatesDisplay(@StartDate date, @EndDate date, @TypeID int, @ShipperID int, @ProductGroupID int, @OriginID int, @DestinationID int, @OriginStateID int, @DestStateID int, @RegionID int, @ProducerID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ShipperID, R.ProductGroupID, R.OriginID, R.DestinationID, R.OriginStateID, R.DestStateID, R.RegionID, R.ProducerID, R.TypeID, R.Rate, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Type = RT.Name
		, Shipper = C.Name
		, ProductGroup = PG.Name
		, Origin = O.Name
		, OriginFull = O.FullName
		, Destination = D.Name
		, DestinationFull = D.FullName
		, OriginState = OS.FullName
		, OriginStateAbbrev = OS.Abbreviation
		, DestState = DS.FullName
		, DestStateAbbrev = DS.Abbreviation
		, Region = RE.Name
		, Producer = PR.Name
		, RateType = RT.Name
		, Uom = U.Name
		, UomShort = U.Abbrev
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch, Ranking
	FROM dbo.fnShipperAssessorialRates(@StartDate, @EndDate, @TypeID, @ShipperID, @ProductGroupID, @OriginID, @DestinationID, @OriginStateID, @DestStateID, @RegionID, @ProducerID, 0) R
	JOIN tblAssessorialRateType RT ON RT.ID = R.TypeID
	LEFT JOIN tblCustomer C ON C.ID = R.ShipperID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN viewOrigin O ON O.ID = R.OriginID
	LEFT JOIN viewDestination D ON D.ID = R.DestinationID
	LEFT JOIN tblState OS ON OS.ID = R.OriginStateID
	LEFT JOIN tblState DS ON DS.ID = R.DestStateID
	LEFT JOIN tblRegion RE ON RE.ID = R.RegionID
	LEFT JOIN tblProducer PR ON PR.ID = R.ProducerID
	LEFT JOIN tblUom U ON U.ID = R.UomID
	ORDER BY EffectiveDate
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper AssessorialRate info for the specified order
-- Changes:
	- 3.7.29 - 2015/06/18 - KDA - move ProducerID parameter/field to end of criteria to be consistent with other "Best-Match" tables
***********************************/
ALTER FUNCTION fnOrderShipperAssessorialRates(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, TypeID, Rate, RateTypeID, UomID
	FROM dbo.viewOrder O
	CROSS APPLY dbo.fnShipperAssessorialRates(O.OrderDate, null, null, O.CustomerID, O.ProductGroupID, O.OriginID, O.DestinationID, O.OriginStateID, O.DestStateID, O.OriginRegionID, O.ProducerID, 1) R
	WHERE O.ID = @ID
	  AND ((R.TypeID = 1 AND O.ChainUp = 1)
		OR (R.TypeID = 2 AND O.RerouteCount > 0)
		OR (R.TypeID = 3 AND O.TicketCount > 1)
		OR (R.TypeID = 4 AND O.H2S = 1)
		OR R.TypeID > 4)
GO

ALTER TABLE tblShipperWaitFeeParameter ADD ProducerID int NULL
CREATE INDEX idxShipperWaitFeeParameter_Producer ON tblShipperWaitFeeParameter(ProducerID)
EXEC _spDropIndex 'udxShipperWaitFeeParameter_Main'
GO
CREATE UNIQUE CLUSTERED INDEX udxShipperWaitFeeParameter_Main ON tblShipperWaitFeeParameter
(
	ShipperID ASC,
	ProductGroupID ASC,
	OriginStateID ASC,
	DestStateID ASC,
	RegionID ASC,
	ProducerID,
	EffectiveDate ASC
)
GO  
ALTER TABLE tblShipperWaitFeeParameter ADD OriginMinBillableMinutes int NULL
ALTER TABLE tblShipperWaitFeeParameter ADD OriginMaxBillableMinutes int NULL
ALTER TABLE tblShipperWaitFeeParameter ADD DestMinBillableMinutes int NULL
ALTER TABLE tblShipperWaitFeeParameter ADD DestMaxBillableMinutes int NULL
GO
EXEC _spRefreshAllViews
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper WaitFeeParameter info for the specified order
-- Changes:
	- 3.7.29 - 2015/06/18 - KDA - add ProducerID parameter/field as new Best-Match criteria
***********************************/
ALTER FUNCTION fnShipperWaitFeeParameter(@StartDate date, @EndDate date, @ShipperID int, @ProductGroupID int, @OriginStateID int, @DestStateID int, @RegionID int, @ProducerID int, @bestMatchOnly bit = 0)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID
			, Ranking = dbo.fnRateRanking(@ShipperID, R.ShipperID, 32, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 16, 0)
				  + dbo.fnRateRanking(@OriginStateID, R.OriginStateID, 8, 0)
				  + dbo.fnRateRanking(@DestStateID, R.DestStateID, 4, 0)
				  + dbo.fnRateRanking(@RegionID, R.RegionID, 2, 0)
				  + dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 1)
		FROM  dbo.viewShipperWaitFeeParameter R
		WHERE coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@OriginStateID, 0), R.OriginStateID, 0) = coalesce(OriginStateID, nullif(@OriginStateID, 0), 0)
		  AND coalesce(nullif(@DestStateID, 0), R.DestStateID, 0) = coalesce(DestStateID, nullif(@DestStateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(ProducerID, nullif(@ProducerID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	)
	
	SELECT R.ID, ShipperID, ProductGroupID, OriginStateID, DestStateID, RegionID, ProducerID, EffectiveDate, EndDate
		, SubUnitID, RoundingTypeID, OriginThresholdMinutes, DestThresholdMinutes, OriginMinBillableMinutes, OriginMaxBillableMinutes, DestMinBillableMinutes, DestMaxBillableMinutes
		, MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate
		, BestMatch, Ranking
		, Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewShipperWaitFeeParameter R
	JOIN (
		SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 6  -- ensure some type of match occurred on all criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper WaitFeeParameter rows for the specified criteria
-- Changes:
	- 3.7.29 - 2015/06/18 - KDA - add ProducerID parameter/field as new Best-Match criteria
***********************************/
ALTER FUNCTION fnShipperWaitFeeParametersDisplay(@StartDate date, @EndDate date, @ShipperID int, @ProductGroupID int, @OriginStateID int, @DestStateID int, @RegionID int, @ProducerID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ShipperID, R.ProductGroupID, R.OriginStateID, R.DestStateID, R.RegionID, R.ProducerID
		, R.SubUnitID, R.RoundingTypeID, R.OriginThresholdMinutes, R.DestThresholdMinutes
		, R.OriginMinBillableMinutes, R.OriginMaxBillableMinutes, R.DestMinBillableMinutes, R.DestMaxBillableMinutes
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Shipper = C.Name
		, ProductGroup = PG.Name
		, OriginState = OS.FullName
		, OriginStateAbbrev = OS.Abbreviation
		, DestinationState = DS.FullName
		, DestinationStateAbbrev = DS.Abbreviation
		, Region = REG.Name
		, Producer = PR.Name
		, RoundingType = WFRT.Name
		, SubUnit = WFSU.Name
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch, Ranking
	FROM dbo.fnShipperWaitFeeParameter(@StartDate, @EndDate, @ShipperID, @ProductGroupID, @OriginStateID, @DestStateID, @RegionID, @ProducerID, 0) R 
	JOIN tblWaitFeeRoundingType WFRT ON WFRT.ID = R.RoundingTypeID
	JOIN tblWaitFeeSubUnit WFSU ON WFSU.ID = R.SubUnitID
	LEFT JOIN tblCustomer C ON C.ID = R.ShipperID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblState OS ON OS.ID = R.OriginStateID
	LEFT JOIN tblState DS ON DS.ID = R.DestStateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblProducer PR ON PR.ID = R.ProducerID
	ORDER BY EffectiveDate
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper WaitFeeParameter info for the specified order
-- Changes:
	- 3.7.29 - 2015/06/18 - KDA - add ProducerID parameter/field as new Best-Match criteria
								- use viewOrderBase instead of viewOrder for efficiency reasons
***********************************/
ALTER FUNCTION fnOrderShipperWaitFeeParameter(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, SubUnitID, RoundingTypeID, OriginThresholdMinutes, DestThresholdMinutes, OriginMinBillableMinutes, OriginMaxBillableMinutes, DestMinBillableMinutes, DestMaxBillableMinutes
	FROM dbo.viewOrderBase O
	CROSS APPLY dbo.fnShipperWaitFeeParameter(isnull(O.OrderDate, O.DueDate), null, O.CustomerID, O.ProductGroupID, O.OriginStateID, O.DestStateID, O.OriginRegionID, O.ProducerID, 1) R
	WHERE O.ID = @ID 
GO

EXEC _spDropIndex 'udxShipperDestinationWaitRate_Main'
GO
CREATE UNIQUE NONCLUSTERED INDEX udxShipperDestinationWaitRate_Main ON tblShipperDestinationWaitRate
(
	ReasonID ASC,
	ShipperID ASC,
	ProductGroupID ASC,
	DestinationID ASC,
	StateID ASC,
	RegionID ASC,
	ProducerID ASC,
	EffectiveDate ASC
)
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper DestinationWaitRate info for the specified criteria
-- Changes:
	- 3.7.29 - 2015/06/18 - KDA - add ProducerID parameter/field as new Best-Match criteria
***********************************/
ALTER FUNCTION fnShipperDestinationWaitRate
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @ProductGroupID int
, @DestinationID int
, @StateID int
, @RegionID int
, @ProducerID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID, ReasonID
			, Ranking =	dbo.fnRateRanking(@ReasonID, R.ReasonID, 64, 1)
				  + dbo.fnRateRanking(@ShipperID, R.ShipperID, 32, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 16, 0)
				  + dbo.fnRateRanking(@DestinationID, R.DestinationID, 8, 0)
				  + dbo.fnRateRanking(@StateID, R.StateID, 4, 0)
				  + dbo.fnRateRanking(@RegionID, R.RegionID, 2, 0)
				  + dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 1)
		FROM  dbo.viewShipperDestinationWaitRate R
		WHERE coalesce(nullif(@ReasonID, 0), R.ReasonID, 0) = coalesce(ReasonID, nullif(@ReasonID, 0), 0)
		  AND coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@DestinationID, 0), R.DestinationID, 0) = coalesce(DestinationID, nullif(@DestinationID, 0), 0)
		  AND coalesce(nullif(@StateID, 0), R.StateID, 0) = coalesce(StateID, nullif(@StateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(ProducerID, nullif(@ProducerID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	),
	cte2 AS
	(
		SELECT ID, S.ReasonID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT ReasonID = ISNULL(ReasonID, 0), Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 7  -- ensure some type of match occurred on all criteria choices
			GROUP BY ReasonID
		) X ON X.ReasonID = isnull(S.ReasonID, 0) AND X.Ranking = S.Ranking
	), 
	cte3 AS
	(	-- when both ReasonID = NULL AND ReasonID = @ReasonID records are BestMatch = 1, favor the ReasonID = @ReasonID record
		SELECT ID, X.ReasonID, X.Ranking, BestMatch = CASE WHEN BEST.ReasonID IS NULL THEN X.BestMatch ELSE 0 END
		FROM cte2 X
		LEFT JOIN (
			SELECT TOP 1 ReasonID FROM cte2 WHERE ReasonID = @ReasonID AND BestMatch = 1 ORDER BY Ranking DESC
		) BEST ON X.ReasonID IS NULL AND X.BestMatch = 1
	)
	
	SELECT TOP (CASE WHEN @BestMatchOnly = 1 THEN 1 ELSE 10000 END) R.ID, R.ReasonID, ShipperID, ProductGroupID, DestinationID, StateID, RegionID, ProducerID, Rate, EffectiveDate, EndDate
		, MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate
		, BestMatch, Ranking
		, Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewShipperDestinationWaitRate R
	JOIN cte3 X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
	ORDER BY Ranking DESC
)
GO

EXEC _spDropFunction 'fnShipperDestinationWaitRatesDisplay'
GO
/***********************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper DestinationWaitRate rows for the specified criteria
-- Changes:
	- 3.7.29 - 2015/06/18 - KDA - add ProducerID parameter/field as new Best-Match criteria
***********************************/
CREATE FUNCTION fnShipperDestinationWaitRateDisplay
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @ProductGroupID int
, @DestinationID int
, @StateID int
, @RegionID int
, @ProducerID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ReasonID, R.ShipperID, R.ProductGroupID, R.DestinationID, R.StateID, R.RegionID, R.ProducerID, R.Rate, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Shipper = SH.Name
		, ProductGroup = PG.Name
		, Destination = D.Name
		, DestinationFull = D.FullName
		, State = S.FullName
		, StateAbbrev = S.Abbreviation
		, Region = REG.Name
		, Producer = P.Name
		, Reason = REA.Description
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
		, Ranking
	FROM dbo.fnShipperDestinationWaitRate(@StartDate, @EndDate, @ReasonID, @ShipperID, @ProductGroupID, @DestinationID, @StateID, @RegionID, @ProducerID, 0) R
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN viewDestination D ON D.ID = R.DestinationID
	LEFT JOIN tblState S ON S.ID = R.StateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblProducer P ON P.ID = R.ProducerID
	LEFT JOIN tblDestinationWaitReason REA ON REA.ID = R.ReasonID
	ORDER BY EffectiveDate
GO
GRANT SELECT ON fnShipperDestinationWaitRateDisplay TO role_iis_acct
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper DestinationWaitRate info for the specified order
-- Changes
	- 3.7.13 - 5/21/15 - KDA - fix bug which prevented missing rate warning when billable minutes existed but no rate matched
	- 3.7.29 - 2015/06/18 - KDA - add use of @ProducerID parameter
								- use viewOrderBase vs viewOrder for efficiency reasons
***********************************/
ALTER FUNCTION fnOrderShipperDestinationWaitRate(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, Minutes = O.DestMinutes, R.Rate
	FROM viewOrderBase O
	JOIN tblDriver D ON D.ID = O.DriverID
	OUTER APPLY dbo.fnShipperDestinationWaitRate(O.OrderDate, null, O.DestWaitReasonID, O.CustomerID, O.ProductGroupID, DestinationID, O.DestStateID, O.DestRegionID, O.ProducerID, 1) R
	WHERE O.ID = @ID
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper DestinationWait data info for the specified order
-- Changes:
	- 3.7.29 - 2015/06/18 - KDA - use Dest Min|Max Billable Minutes WaitFeeParameter fields
***********************************/
ALTER FUNCTION fnOrderShipperDestinationWaitData(@ID int) RETURNS TABLE AS RETURN
	SELECT RateID, BillableMinutes, BillableHours, Amount = BillableHours * Rate
	FROM (
		SELECT RateID, BillableMinutes
			, BillableHours = dbo.fnComputeBillableWaitHours(BillableMinutes, SubUnitID, RoundingTypeID)
			, Rate
		FROM (
			SELECT RateID = WR.ID
				, WR.Rate
				, BillableMinutes = dbo.fnMaxInt(0, 
					-- Total Destination Minutes between range of DestMinBillableMinutes & DestMaxBillableMinutes (will be ignored if either Min|Max value is NULL)
					CASE WHEN WR.Minutes < WFP.DestMinBillableMinutes THEN 0
						 WHEN WR.Minutes > WFP.DestMaxBillableMinutes THEN WFP.DestMaxBillableMinutes
						 ELSE WR.Minutes END - WFP.DestThresholdMinutes)
				, WFP.SubUnitID, WFP.RoundingTypeID
			FROM dbo.fnOrderShipperDestinationWaitRate(@ID) WR 
			CROSS JOIN dbo.fnOrderShipperWaitFeeParameter(@ID) WFP 
		) X
	) X2
	WHERE BillableHours > 0.0
GO

EXEC _spDropIndex 'udxShipperOrderRejectRate_Main'
GO
CREATE UNIQUE NONCLUSTERED INDEX udxShipperOrderRejectRate_Main ON tblShipperOrderRejectRate
(
	ReasonID ASC,
	ShipperID ASC,
	ProductGroupID ASC,
	OriginID ASC,
	StateID ASC,
	RegionID ASC,
	ProducerID,
	EffectiveDate ASC
)
GO 
CREATE INDEX idxShipperOrderRejectRate_Producer ON tblShipperOrderRejectRate(ProducerID)
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper OrderRejectRate info for the specified criteria
-- Changes:
	- 3.7.29 - 2015/06/18 - KDA - add ProducerID parameter/field as new Best-Match criteria
***********************************/
ALTER FUNCTION fnShipperOrderRejectRate
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @ProductGroupID int
, @OriginID int
, @StateID int
, @RegionID int
, @ProducerID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID, ReasonID
			, Ranking =	dbo.fnRateRanking(@ReasonID, R.ReasonID, 64, 1)
				  + dbo.fnRateRanking(@ShipperID, R.ShipperID, 32, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 16, 0)
				  + dbo.fnRateRanking(@OriginID, R.OriginID, 8, 0)
				  + dbo.fnRateRanking(@StateID, R.StateID, 4, 0)
				  + dbo.fnRateRanking(@RegionID, R.RegionID, 2, 0)
				  + dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 1)
		FROM  dbo.viewShipperOrderRejectRate R
		WHERE coalesce(nullif(@ReasonID, 0), R.ReasonID, 0) = coalesce(ReasonID, nullif(@ReasonID, 0), 0)
		  AND coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@OriginID, 0), R.OriginID, 0) = coalesce(OriginID, nullif(@OriginID, 0), 0)
		  AND coalesce(nullif(@StateID, 0), R.StateID, 0) = coalesce(StateID, nullif(@StateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(ProducerID, nullif(@ProducerID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	),
	cte2 AS
	(
		SELECT ID, S.ReasonID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT ReasonID = ISNULL(ReasonID, 0), Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 7  -- ensure some type of match occurred on all criteria choices
			GROUP BY ReasonID
		) X ON X.ReasonID = isnull(S.ReasonID, 0) AND X.Ranking = S.Ranking
	), 
	cte3 AS
	(	-- when both ReasonID = NULL AND ReasonID = @ReasonID records are BestMatch = 1, favor the ReasonID = @ReasonID record
		SELECT ID, X.ReasonID, X.Ranking, BestMatch = CASE WHEN BEST.ReasonID IS NULL THEN X.BestMatch ELSE 0 END
		FROM cte2 X
		LEFT JOIN (
			SELECT TOP 1 ReasonID FROM cte2 WHERE ReasonID = @ReasonID AND BestMatch = 1 ORDER BY Ranking DESC
		) BEST ON X.ReasonID IS NULL AND X.BestMatch = 1
	)
	
	SELECT R.ID, R.ReasonID, ShipperID, ProductGroupID, OriginID, StateID, RegionID, ProducerID, Rate, RateTypeID, UomID, EffectiveDate, EndDate
		, MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate
		, BestMatch, Ranking
		, Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewShipperOrderRejectRate R
	JOIN cte3 X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)
GO

/***********************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper OrderRejectRate rows for the specified criteria
-- Changes:
	- 3.7.29 - 2015/06/18 - KDA - add ProducerID parameter/field as new Best-Match criteria
***********************************/
ALTER FUNCTION fnShipperOrderRejectRatesDisplay
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @ProductGroupID int
, @OriginID int
, @StateID int
, @RegionID int
, @ProducerID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ReasonID, R.ShipperID, R.ProductGroupID, R.OriginID, R.StateID, R.RegionID, R.ProducerID, R.Rate, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Shipper = C.Name
		, ProductGroup = PG.Name
		, Origin = D.Name
		, OriginFull = D.FullName
		, State = S.FullName
		, StateAbbrev = S.Abbreviation
		, Region = REG.Name
		, Producer = PR.Name
		, Reason = REA.Description
		, RateType = RT.Name
		, Uom = U.Name
		, UomShort = U.Abbrev
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
	FROM dbo.fnShipperOrderRejectRate(@StartDate, @EndDate, @ReasonID, @ShipperID, @ProductGroupID, @OriginID, @StateID, @RegionID, @ProducerID, 0) R
	LEFT JOIN tblCustomer C ON C.ID = R.ShipperID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN viewOrigin D ON D.ID = R.OriginID
	LEFT JOIN tblState S ON S.ID = R.StateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblProducer PR ON PR.ID = R.ProducerID
	LEFT JOIN tblOrderRejectReason REA ON REA.ID = R.ReasonID
	LEFT JOIN tblRateType RT ON RT.ID = R.RateTypeID
	LEFT JOIN tblUom U ON U.ID = R.UomID
	ORDER BY EffectiveDate
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper OrderRejectRate info for the specified order
-- Changes:
	- 3.7.29 - 2015/06/18 - KDA - add usage of ProducerID parameter/field as new Best-Match criteria
								- use viewOrderBase vs viewOrder for efficiency reasons
***********************************/
ALTER FUNCTION fnOrderShipperOrderRejectRate(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, R.Rate, R.RateTypeID, R.UomID
	FROM viewOrderBase O
	CROSS APPLY dbo.fnShipperOrderRejectRate(O.OrderDate, null, O.RejectReasonID, O.CustomerID, O.ProductGroupID, O.OriginID, O.OriginStateID, O.OriginRegionID, O.ProducerID, 1) R
	WHERE O.ID = @ID
GO

EXEC _spDropIndex 'udxShipperOriginWaitRate_Main'
GO
CREATE UNIQUE NONCLUSTERED INDEX udxShipperOriginWaitRate_Main ON tblShipperOriginWaitRate
(
	ReasonID ASC,
	ShipperID ASC,
	ProductGroupID ASC,
	OriginID ASC,
	StateID ASC,
	RegionID ASC,
	ProducerID,
	EffectiveDate ASC
)
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper OriginWaitRate info for the specified criteria
-- Changes:
	- 3.7.29 - 2015/06/18 - KDA - add ProducerID parameter/field as new Best-Match criteria
***********************************/
ALTER FUNCTION fnShipperOriginWaitRate
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @ProductGroupID int
, @OriginID int
, @StateID int
, @RegionID int
, @ProducerID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID, ReasonID
			, Ranking =	dbo.fnRateRanking(@ReasonID, R.ReasonID, 64, 1)
				  + dbo.fnRateRanking(@ShipperID, R.ShipperID, 32, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 16, 0)
				  + dbo.fnRateRanking(@OriginID, R.OriginID, 8, 0)
				  + dbo.fnRateRanking(@StateID, R.StateID, 4, 0)
				  + dbo.fnRateRanking(@RegionID, R.RegionID, 2, 0)
				  + dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 1)
		FROM  dbo.viewShipperOriginWaitRate R
		WHERE coalesce(nullif(@ReasonID, 0), R.ReasonID, 0) = coalesce(ReasonID, nullif(@ReasonID, 0), 0)
		  AND coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@OriginID, 0), R.OriginID, 0) = coalesce(OriginID, nullif(@OriginID, 0), 0)
		  AND coalesce(nullif(@StateID, 0), R.StateID, 0) = coalesce(StateID, nullif(@StateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(ProducerID, nullif(@ProducerID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	),
	cte2 AS
	(
		SELECT ID, S.ReasonID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT ReasonID = ISNULL(ReasonID, 0), Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 7  -- ensure some type of match occurred on all criteria choices
			GROUP BY ReasonID
		) X ON X.ReasonID = isnull(S.ReasonID, 0) AND X.Ranking = S.Ranking
	), 
	cte3 AS
	(	-- when both ReasonID = NULL AND ReasonID = @ReasonID records are BestMatch = 1, favor the ReasonID = @ReasonID record
		SELECT ID, X.ReasonID, X.Ranking, BestMatch = CASE WHEN BEST.ReasonID IS NULL THEN X.BestMatch ELSE 0 END
		FROM cte2 X
		LEFT JOIN (
			SELECT TOP 1 ReasonID FROM cte2 WHERE ReasonID = @ReasonID AND BestMatch = 1 ORDER BY Ranking DESC
		) BEST ON X.ReasonID IS NULL AND X.BestMatch = 1
	)
	
	SELECT TOP (CASE WHEN @BestMatchOnly = 1 THEN 1 ELSE 10000 END) R.ID, R.ReasonID, ShipperID, ProductGroupID, OriginID, StateID, RegionID, ProducerID, Rate, EffectiveDate, EndDate
		, MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate
		, BestMatch, Ranking
		, Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewShipperOriginWaitRate R
	JOIN cte3 X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
	ORDER BY Ranking DESC
)
GO

/***********************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper OriginWaitRate rows for the specified criteria
-- Changes:
	- 3.7.29 - 2015/06/18 - KDA - add ProducerID parameter/field as new Best-Match criteria
***********************************/
ALTER FUNCTION fnShipperOriginWaitRatesDisplay
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @ProductGroupID int
, @OriginID int
, @StateID int
, @RegionID int
, @ProducerID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ReasonID, R.ShipperID, R.ProductGroupID, R.OriginID, R.StateID, R.RegionID, R.ProducerID, R.Rate, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Shipper = C.Name
		, ProductGroup = PG.Name
		, Origin = D.Name
		, OriginFull = D.FullName
		, State = S.FullName
		, StateAbbrev = S.Abbreviation
		, Region = REG.Name
		, Producer = PR.Name
		, Reason = REA.Description
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
	FROM dbo.fnShipperOriginWaitRate(@StartDate, @EndDate, @ReasonID, @ShipperID, @ProductGroupID, @OriginID, @StateID, @RegionID, @ProducerID, 0) R
	LEFT JOIN tblCustomer C ON C.ID = R.ShipperID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN viewOrigin D ON D.ID = R.OriginID
	LEFT JOIN tblState S ON S.ID = R.StateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblProducer PR ON PR.ID = R.ProducerID
	LEFT JOIN tblOriginWaitReason REA ON REA.ID = R.ReasonID
	ORDER BY EffectiveDate
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper OriginWaitRate info for the specified order
-- Changes
	- 3.7.13 - 5/21/15 - KDA - fix bug which prevented missing rate warning when billable minutes existed but no rate matched
	- 3.7.29 - 2015/05/18 - KDA - add usage of @ProducerID parameter
								- use viewOrderBase vs viewOrder for efficiency reasons
***********************************/
ALTER FUNCTION fnOrderShipperOriginWaitRate(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, Minutes = O.OriginMinutes, R.Rate
	FROM viewOrderBase O
	CROSS APPLY dbo.fnShipperOriginWaitRate(O.OrderDate, null, O.OriginWaitReasonID, O.CustomerID, O.ProductGroupID, O.OriginID, O.OriginStateID, O.OriginRegionID, O.ProducerID, 1) R
	WHERE O.ID = @ID
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper OriginWait data info for the specified order
-- Changes:
	- 3.7.29 - 2015/06/18 - KDA - use Origin Min|Max Billable Minutes WaitFeeParameter fields
***********************************/
ALTER FUNCTION fnOrderShipperOriginWaitData(@ID int) RETURNS TABLE AS RETURN
	SELECT RateID, BillableMinutes, BillableHours, Amount = BillableHours * Rate
	FROM (
		SELECT RateID, BillableMinutes
			, BillableHours = dbo.fnComputeBillableWaitHours(BillableMinutes, SubUnitID, RoundingTypeID)
			, Rate
		FROM (
			SELECT RateID = WR.ID
				, WR.Rate
				, BillableMinutes = dbo.fnMaxInt(0, 
					-- Total Origin Minutes between range of OriginMinBillableMinutes & OriginMaxBillableMinutes (will be ignored if either Min|Max value is NULL)
					CASE WHEN WR.Minutes < WFP.OriginMinBillableMinutes THEN 0
						 WHEN WR.Minutes > WFP.OriginMaxBillableMinutes THEN WFP.OriginMaxBillableMinutes
						 ELSE WR.Minutes END - WFP.OriginThresholdMinutes)
				, WFP.SubUnitID, WFP.RoundingTypeID
			FROM dbo.fnOrderShipperOriginWaitRate(@ID) WR 
			CROSS JOIN dbo.fnOrderShipperWaitFeeParameter(@ID) WFP 
		) X
	) X2
	WHERE BillableHours > 0.0
GO

/***********************************
-- Date Created: 5 May 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper RateSheetRangeRate info for the specified criteria
-- Changes:
	- 3.7.29 - 2015/06/18 - KDA - add ProducerID parameter/field as new Best-Match criteria
***********************************/
ALTER FUNCTION fnShipperRateSheet(@StartDate date, @EndDate date, @ShipperID int, @ProductGroupID int, @OriginStateID int, @DestStateID int, @RegionID int, @ProducerID int, @BestMatchOnly bit = 0)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT R.ID
			, Ranking = dbo.fnRateRanking(@ShipperID, R.ShipperID, 32, 0)
				+ dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 16, 0)
				+ dbo.fnRateRanking(@OriginStateID, R.OriginStateID, 8, 0)
				+ dbo.fnRateRanking(@DestStateID, R.DestStateID, 4, 0)
				+ dbo.fnRateRanking(@RegionID, R.RegionID, 2, 0)
				+ dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 1)
		FROM dbo.viewShipperRateSheet R
		WHERE coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@OriginStateID, 0), R.OriginStateID, 0) = coalesce(OriginStateID, nullif(@OriginStateID, 0), 0)
		  AND coalesce(nullif(@DestStateID, 0), R.DestStateID, 0) = coalesce(DestStateID, nullif(@DestStateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(ProducerID, nullif(@ProducerID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	)
	
	SELECT R.ID, RateSheetID = R.ID, ShipperID, ProductGroupID, OriginStateID, DestStateID, RegionID, ProducerID, RateTypeID, UomID, EffectiveDate, EndDate
		, MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate
		, BestMatch, Ranking
		, Locked, R.CreateDateUTC, R.CreatedByUser, R.LastChangeDateUTC, R.LastChangedByUser 
	FROM dbo.viewShipperRateSheet R
	JOIN (
		SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 6  -- ensure some type of match occurred on all criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)
GO

/***********************************
-- Date Created: 5 May 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper RateSheetRangeRate info for the specified criteria
-- Changes:
	- 3.7.29 - 2015/06/18 - KDA - add ProducerID parameter/field as new Best-Match criteria
***********************************/
ALTER FUNCTION fnShipperRateSheetDisplay(@StartDate date, @EndDate date, @ShipperID int, @ProductGroupID int, @OriginStateID int, @DestStateID int, @RegionID int, @ProducerID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ShipperID, R.ProductGroupID, R.ProducerID, R.OriginStateID, R.DestStateID, R.RegionID, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Shipper = S.Name
		, ProductGroup = PG.Name
		, OriginState = OS.FullName
		, OriginStateAbbrev = OS.Abbreviation
		, DestState = OS.FullName
		, DestStateAbbrev = OS.Abbreviation
		, Region = REG.Name
		, Producer = P.Name
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
		, Ranking
	FROM dbo.fnShipperRateSheet(@StartDate, @EndDate, @ShipperID, @ProductGroupID, @OriginStateID, @DestStateID, @RegionID, @ProducerID, 0) R
	LEFT JOIN tblCustomer S ON S.ID = R.ShipperID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblState OS ON OS.ID = R.OriginStateID
	LEFT JOIN tblState DS ON DS.ID = R.DestStateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblProducer P ON P.ID = R.ProducerID
	ORDER BY EffectiveDate
GO

--------------------------------
CREATE TABLE tblShipperMinSettlementUnits
(
  ID int IDENTITY(1,1) NOT NULL CONSTRAINT PK_ShipperMinSettlementUnits PRIMARY KEY NONCLUSTERED 
, ShipperID int NULL CONSTRAINT FK_ShipperMinSettlementUnits_Shipper FOREIGN KEY REFERENCES dbo.tblCustomer (ID)
, ProductGroupID int NULL CONSTRAINT FK_ShipperMinSettlementUnits_ProductGroup FOREIGN KEY REFERENCES dbo.tblProductGroup (ID)
, OriginID int NULL CONSTRAINT FK_ShipperMinSettlementUnits_Origin FOREIGN KEY REFERENCES dbo.tblOrigin (ID)
, OriginStateID int NULL CONSTRAINT FK_ShipperMinSettlementUnits_OriginState FOREIGN KEY REFERENCES dbo.tblState (ID)
, DestinationID int NULL CONSTRAINT FK_ShipperMinSettlementUnits_Destination FOREIGN KEY REFERENCES dbo.tblDestination (ID)
, DestinationStateID int NULL CONSTRAINT FK_ShipperMinSettlementUnits_DestState FOREIGN KEY REFERENCES dbo.tblState (ID)
, ProducerID int NULL CONSTRAINT FK_ShipperMinSettlementUnits_Producer FOREIGN KEY REFERENCES dbo.tblProducer (ID)
, EffectiveDate date NOT NULL
, EndDate date NOT NULL
, MinSettlementUnits int NOT NULL
, UomID int NOT NULL CONSTRAINT FK_ShipperMinSettlementUnits_UomID FOREIGN KEY(UomID) REFERENCES dbo.tblUom (ID)
, CreateDateUTC smalldatetime NOT NULL CONSTRAINT DF_ShipperMinSettlementUnits DEFAULT (getutcdate())
, CreatedByUser varchar(100) NOT NULL
, LastChangeDateUTC smalldatetime NULL
, LastChangedByUser varchar(100) NULL
)
GO
GRANT SELECT, INSERT, UPDATE ON tblShipperMinSettlementUnits TO role_iis_acct
GO

CREATE UNIQUE CLUSTERED INDEX udxShipperMinSettlementUnits_Main ON tblShipperMinSettlementUnits
(
  ShipperID 
, ProductGroupID 
, OriginID 
, OriginStateID 
, DestinationID 
, DestinationStateID 
, ProducerID
, EffectiveDate ASC
)
GO

INSERT INTO tblShipperMinSettlementUnits (ShipperID, MinSettlementUnits, UomID, EffectiveDate, EndDate, CreatedByUser)
	SELECT ID, MinSettlementUnits, MinSettlementUomID, '1/1/2012', '12/31/2099', CreatedByUser FROM tblCustomer WHERE MinSettlementUnits IS NOT NULL
	UNION SELECT NULL, 180, 1, '1/1/2012', '12/31/2099', 'System' 
GO

ALTER TABLE tblOrderSettlementShipper ADD MinSettlementUnitsID int NULL CONSTRAINT FK_OrderSettlementShipper_MinSettlementUnitsID FOREIGN KEY REFERENCES tblShipperMinSettlementUnits(ID)
GO

UPDATE tblOrderSettlementShipper
  SET MinSettlementUnitsID = ISNULL(CMSU.ID, CMSUX.ID)
FROM tblOrderSettlementShipper OSC
JOIN tblOrder O ON O.ID = OSC.OrderID
JOIN tblShipperMinSettlementUnits CMSU ON CMSU.ShipperID = O.CustomerID
LEFT JOIN tblShipperMinSettlementUnits CMSUX ON CMSUX.ShipperID IS NULL
GO

CREATE TABLE tblShipperSettlementFactor
(
  ID int IDENTITY(1,1) NOT NULL CONSTRAINT PK_ShipperSettlementFactor PRIMARY KEY NONCLUSTERED 
, ShipperID int NULL CONSTRAINT FK_ShipperSettlementFactor_Shipper FOREIGN KEY REFERENCES dbo.tblCustomer (ID)
, ProductGroupID int NULL CONSTRAINT FK_ShipperSettlementFactor_ProductGroup FOREIGN KEY REFERENCES dbo.tblProductGroup (ID)
, OriginID int NULL CONSTRAINT FK_ShipperSettlementFactor_Origin FOREIGN KEY REFERENCES dbo.tblOrigin (ID)
, OriginStateID int NULL CONSTRAINT FK_ShipperSettlementFactor_OriginState FOREIGN KEY REFERENCES dbo.tblState (ID)
, DestinationID int NULL CONSTRAINT FK_ShipperSettlementFactor_Destination FOREIGN KEY REFERENCES dbo.tblDestination (ID)
, DestinationStateID int NULL CONSTRAINT FK_ShipperSettlementFactor_DestState FOREIGN KEY REFERENCES dbo.tblState (ID)
, ProducerID int NULL CONSTRAINT FK_ShipperSettlementFactor_Producer FOREIGN KEY REFERENCES dbo.tblProducer (ID)
, EffectiveDate date NOT NULL
, EndDate date NOT NULL
, SettlementFactorID int NOT NULL CONSTRAINT FK_ShipperSettlementFactor_SettlementFactor FOREIGN KEY REFERENCES dbo.tblSettlementFactor(ID)
, CreateDateUTC smalldatetime NOT NULL CONSTRAINT DF_ShipperSettlementFactor DEFAULT (getutcdate())
, CreatedByUser varchar(100) NOT NULL
, LastChangeDateUTC smalldatetime NULL
, LastChangedByUser varchar(100) NULL
)
GO
GRANT SELECT, INSERT, UPDATE ON tblShipperSettlementFactor TO role_iis_acct
GO

CREATE UNIQUE CLUSTERED INDEX udxShipperSettlementFactor_Main ON tblShipperSettlementFactor
(
  ShipperID 
, ProductGroupID 
, OriginID 
, OriginStateID 
, DestinationID 
, DestinationStateID 
, ProducerID
, EffectiveDate ASC
)
GO

INSERT INTO tblShipperSettlementFactor (ShipperID, SettlementFactorID, EffectiveDate, EndDate, CreatedByUser)
	SELECT ID, SettlementFactorID, '1/1/2012', '12/31/2099', CreatedByUser FROM tblCustomer
GO

ALTER TABLE tblOrderSettlementShipper ADD ShipperSettlementFactorID int NULL CONSTRAINT FK_OrderSettlementShipper_ShipperSettlementFactorID FOREIGN KEY REFERENCES tblShipperSettlementFactor(ID)
GO

UPDATE tblOrderSettlementShipper
  SET ShipperSettlementFactorID = ISNULL(CSU.ID, CSUX.ID)
FROM tblOrderSettlementShipper OSC
JOIN tblOrder O ON O.ID = OSC.OrderID
JOIN tblShipperSettlementFactor CSU ON CSU.ShipperID = O.CustomerID
LEFT JOIN tblShipperSettlementFactor CSUX ON CSUX.ShipperID IS NULL
GO

/******************************************************
-- Date Created: 12 June 2015
-- Author: Kevin Alons
-- Purpose: add a computed fields to the ShipperMinSettlementUnits table data
-- Changes:
	- 3.7.29 - 2015/06/18 - KDA - ADDED
******************************************************/
CREATE VIEW viewShipperMinSettlementUnits AS
	SELECT X.*
		, MaxEffectiveDate = (SELECT MIN(OrderDate) FROM tblOrderSettlementShipper WHERE MinSettlementUnitsID = X.ID AND BatchID IS NOT NULL)
		, MinEndDate = (SELECT MAX(OrderDate) FROM tblOrderSettlementShipper WHERE MinSettlementUnitsID = X.ID AND BatchID IS NOT NULL)
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementShipper WHERE MinSettlementUnitsID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblShipperMinSettlementUnits XN 
			WHERE isnull(XN.ShipperID, 0) = isnull(X.ShipperID, 0) 
			  AND isnull(XN.ProductGroupID, 0) = isnull(X.ProductGroupID, 0) 
			  AND isnull(XN.OriginID, 0) = isnull(X.OriginID, 0) 
			  AND isnull(XN.OriginStateID, 0) = isnull(X.OriginStateID, 0) 
			  AND isnull(XN.DestinationID, 0) = isnull(X.DestinationID, 0) 
			  AND isnull(XN.DestinationStateID, 0) = isnull(X.DestinationStateID, 0) 
			  AND isnull(XN.ProducerID, 0) = isnull(X.ProducerID, 0) 
			  AND XN.EffectiveDate > X.EffectiveDate)
		, PriorEndDate = (
			SELECT max(XN.EndDate) 
			FROM tblShipperMinSettlementUnits XN 
			WHERE isnull(XN.ShipperID, 0) = isnull(X.ShipperID, 0) 
			  AND isnull(XN.OriginID, 0) = isnull(X.OriginID, 0) 
			  AND isnull(XN.OriginStateID, 0) = isnull(X.OriginStateID, 0) 
			  AND isnull(XN.DestinationID, 0) = isnull(X.DestinationID, 0) 
			  AND isnull(XN.DestinationStateID, 0) = isnull(X.DestinationStateID, 0) 
			  AND isnull(XN.ProducerID, 0) = isnull(X.ProducerID, 0) 
			  AND XN.EndDate < X.EffectiveDate)
	FROM tblShipperMinSettlementUnits X

GO
GRANT SELECT ON viewShipperMinSettlementUnits TO role_iis_acct
GO

/******************************************************
-- Date Created: 12 June 2015
-- Author: Kevin Alons
-- Purpose: add a computed fields to the ShipperSettlementFactor table data
-- Changes:
	- 3.7.29 - 2015/06/18 - KDA - ADDED
******************************************************/
CREATE VIEW viewShipperSettlementFactor AS
	SELECT X.*
		, MaxEffectiveDate = (SELECT MIN(OrderDate) FROM tblOrderSettlementShipper WHERE ShipperSettlementFactorID = X.ID AND BatchID IS NOT NULL)
		, MinEndDate = (SELECT MAX(OrderDate) FROM tblOrderSettlementShipper WHERE ShipperSettlementFactorID = X.ID AND BatchID IS NOT NULL)
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementShipper WHERE ShipperSettlementFactorID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblShipperSettlementFactor XN 
			WHERE isnull(XN.ShipperID, 0) = isnull(X.ShipperID, 0) 
			  AND isnull(XN.ProductGroupID, 0) = isnull(X.ProductGroupID, 0) 
			  AND isnull(XN.OriginID, 0) = isnull(X.OriginID, 0) 
			  AND isnull(XN.OriginStateID, 0) = isnull(X.OriginStateID, 0) 
			  AND isnull(XN.DestinationID, 0) = isnull(X.DestinationID, 0) 
			  AND isnull(XN.DestinationStateID, 0) = isnull(X.DestinationStateID, 0) 
			  AND isnull(XN.ProducerID, 0) = isnull(X.ProducerID, 0) 
			  AND XN.EffectiveDate > X.EffectiveDate)
		, PriorEndDate = (
			SELECT max(XN.EndDate) 
			FROM tblShipperSettlementFactor XN 
			WHERE isnull(XN.ShipperID, 0) = isnull(X.ShipperID, 0) 
			  AND isnull(XN.OriginID, 0) = isnull(X.OriginID, 0) 
			  AND isnull(XN.OriginStateID, 0) = isnull(X.OriginStateID, 0) 
			  AND isnull(XN.DestinationID, 0) = isnull(X.DestinationID, 0) 
			  AND isnull(XN.DestinationStateID, 0) = isnull(X.DestinationStateID, 0) 
			  AND isnull(XN.ProducerID, 0) = isnull(X.ProducerID, 0) 
			  AND XN.EndDate < X.EffectiveDate)
	FROM tblShipperSettlementFactor X

GO
GRANT SELECT ON viewShipperSettlementFactor TO role_iis_acct
GO

/***********************************
-- Date Created: 12 June 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper MinSettlementUnits info for the specified criteria
-- Changes:
	- 3.7.29 - 2015/06/18 - KDA - ADDED
***********************************/
CREATE FUNCTION fnShipperMinSettlementUnits
(
  @StartDate date
, @EndDate date
, @ShipperID int
, @ProductGroupID int
, @OriginID int
, @OriginStateID int
, @DestinationID int
, @DestinationStateID int
, @ProducerID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID
			, Ranking =	dbo.fnRateRanking(@ShipperID, R.ShipperID, 64, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 32, 0)
				  + dbo.fnRateRanking(@OriginID, R.OriginID, 16, 0)
				  + dbo.fnRateRanking(@OriginStateID, R.OriginStateID, 8, 0)
				  + dbo.fnRateRanking(@DestinationID, R.DestinationID, 4, 0)
				  + dbo.fnRateRanking(@DestinationStateID, R.DestinationStateID, 2, 0)
				  + dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 1)
		FROM  dbo.viewShipperMinSettlementUnits R
		WHERE coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@OriginID, 0), R.OriginID, 0) = coalesce(OriginID, nullif(@OriginID, 0), 0)
		  AND coalesce(nullif(@OriginStateID, 0), R.OriginStateID, 0) = coalesce(OriginStateID, nullif(@OriginStateID, 0), 0)
		  AND coalesce(nullif(@DestinationID, 0), R.DestinationID, 0) = coalesce(DestinationID, nullif(@DestinationID, 0), 0)
		  AND coalesce(nullif(@DestinationStateID, 0), R.DestinationStateID, 0) = coalesce(DestinationStateID, nullif(@DestinationStateID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(ProducerID, nullif(@ProducerID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	)
	
	SELECT TOP (CASE WHEN @BestMatchOnly = 1 THEN 1 ELSE 10000 END) R.ID, ShipperID, ProductGroupID, OriginID, OriginStateID, DestinationID, DestinationStateID, ProducerID
	  , MinSettlementUnits, UomID, EffectiveDate, EndDate
	  , MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate
	  , BestMatch, Ranking
	  , Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewShipperMinSettlementUnits R
	JOIN (
		SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 7  -- ensure some type of match occurred on all criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)
GO
GRANT SELECT ON fnShipperMinSettlementUnits TO role_iis_acct
GO

/***********************************
-- Date Created: 12 June 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper SettlementFactor info for the specified criteria
-- Changes:
	- 3.7.29 - 2015/06/18 - KDA - ADDED
***********************************/
CREATE FUNCTION fnShipperSettlementFactor
(
  @StartDate date
, @EndDate date
, @ShipperID int
, @ProductGroupID int
, @OriginID int
, @OriginStateID int
, @DestinationID int
, @DestinationStateID int
, @ProducerID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID
			, Ranking =	dbo.fnRateRanking(@ShipperID, R.ShipperID, 64, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 32, 0)
				  + dbo.fnRateRanking(@OriginID, R.OriginID, 16, 0)
				  + dbo.fnRateRanking(@OriginStateID, R.OriginStateID, 8, 0)
				  + dbo.fnRateRanking(@DestinationID, R.DestinationID, 4, 0)
				  + dbo.fnRateRanking(@DestinationStateID, R.DestinationStateID, 2, 0)
				  + dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 1)
		FROM  dbo.viewShipperSettlementFactor R
		WHERE coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@OriginID, 0), R.OriginID, 0) = coalesce(OriginID, nullif(@OriginID, 0), 0)
		  AND coalesce(nullif(@OriginStateID, 0), R.OriginStateID, 0) = coalesce(OriginStateID, nullif(@OriginStateID, 0), 0)
		  AND coalesce(nullif(@DestinationID, 0), R.DestinationID, 0) = coalesce(DestinationID, nullif(@DestinationID, 0), 0)
		  AND coalesce(nullif(@DestinationStateID, 0), R.DestinationStateID, 0) = coalesce(DestinationStateID, nullif(@DestinationStateID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(ProducerID, nullif(@ProducerID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	)
	
	SELECT TOP (CASE WHEN @BestMatchOnly = 1 THEN 1 ELSE 10000 END) R.ID, ShipperID, ProductGroupID, OriginID, OriginStateID, DestinationID, DestinationStateID, ProducerID
	  , SettlementFactorID, EffectiveDate, EndDate
	  , MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate
	  , BestMatch, Ranking
	  , Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewShipperSettlementFactor R
	JOIN (
		SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 7  -- ensure some type of match occurred on all criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)
GO
GRANT SELECT ON fnShipperSettlementFactor TO role_iis_acct
GO

/***********************************
-- Date Created: 12 June 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper MinSettlementUnits rows for the specified criteria
-- Changes:
	- 3.7.29 - 2015/06/18 - KDA - ADDED
***********************************/
CREATE FUNCTION fnShipperMinSettlementUnitsDisplay
(
  @StartDate date
, @EndDate date
, @ShipperID int
, @ProductGroupID int
, @OriginID int
, @OriginStateID int
, @DestinationID int
, @DestinationStateID int
, @ProducerID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ShipperID, R.ProductGroupID, R.OriginID, R.OriginStateID, R.DestinationID, R.DestinationStateID, R.ProducerID
		, R.MinSettlementUnits, R.UomID, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Shipper = SH.Name
		, ProductGroup = PG.Name
		, Origin = O.Name
		, OriginFull = O.FullName
		, OriginState = O.State
		, OriginStateAbbrev = O.StateAbbrev
		, Destination = D.Name
		, DestinationFull = D.FullName
		, DestinationState = D.State
		, DestinationStateAbbrev = D.StateAbbrev
		, Producer = P.Name
		, Uom = U.Name
		, UomShort = U.Abbrev
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch, Ranking
	FROM dbo.fnShipperMinSettlementUnits(@StartDate, @EndDate, @ShipperID, @ProductGroupID, @OriginID, @OriginStateID, @DestinationID, @DestinationStateID, @ProducerID, 0) R
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN viewOrigin O ON O.ID = R.OriginID
	LEFT JOIN viewDestination D ON D.ID = R.DestinationID
	LEFT JOIN tblProducer P ON P.ID = R.ProducerID
	LEFT JOIN tblUom U ON U.ID = R.UomID
	ORDER BY EffectiveDate

GO
GRANT SELECT ON fnShipperMinSettlementUnitsDisplay TO role_iis_acct
GO

/***********************************
-- Date Created: 12 June 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper SettlementFactor rows for the specified criteria
-- Changes:
	- 3.7.29 - 2015/06/18 - KDA - ADDED
***********************************/
CREATE FUNCTION fnShipperSettlementFactorDisplay
(
  @StartDate date
, @EndDate date
, @ShipperID int
, @ProductGroupID int
, @OriginID int
, @OriginStateID int
, @DestinationID int
, @DestinationStateID int
, @ProducerID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ShipperID, R.ProductGroupID, R.OriginID, R.OriginStateID, R.DestinationID, R.DestinationStateID, R.ProducerID
		, R.SettlementFactorID, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, SettlementFactor = SF.Name
		, Shipper = SH.Name
		, ProductGroup = PG.Name
		, Origin = O.Name
		, OriginFull = O.FullName
		, OriginState = O.State
		, OriginStateAbbrev = O.StateAbbrev
		, Destination = D.Name
		, DestinationFull = D.FullName
		, DestinationState = D.State
		, DestinationStateAbbrev = D.StateAbbrev
		, Producer = P.Name
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch, Ranking
	FROM dbo.fnShipperSettlementFactor(@StartDate, @EndDate, @ShipperID, @ProductGroupID, @OriginID, @OriginStateID, @DestinationID, @DestinationStateID, @ProducerID, 0) R
	JOIN dbo.tblSettlementFactor SF ON SF.ID = R.SettlementFactorID
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblCustomer C ON C.ID = R.ShipperID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN viewOrigin O ON O.ID = R.OriginID
	LEFT JOIN viewDestination D ON D.ID = R.DestinationID
	LEFT JOIN tblProducer P ON P.ID = R.ProducerID
	ORDER BY EffectiveDate

GO
GRANT SELECT ON fnShipperSettlementFactorDisplay TO role_iis_acct
GO

/***********************************
-- Date Created: 12 June 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper MinSettlementUnits info for the specified order
-- Changes:
	- 3.7.29 - 2015/06/18 - KDA - ADDED
***********************************/
CREATE FUNCTION fnOrderShipperMinSettlementUnits(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, R.MinSettlementUnits, R.UomID
	FROM viewOrderBase O
	CROSS APPLY dbo.fnShipperMinSettlementUnits(O.OrderDate, null, O.CustomerID, O.ProductGroupID, O.OriginID, O.OriginStateID, O.DestinationID, O.DestStateID, O.ProducerID, 1) R
	WHERE O.ID = @ID
GO
GRANT SELECT ON fnOrderShipperMinSettlementUnits TO role_iis_acct
GO

/***********************************
-- Date Created: 12 June 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper SettlementFactor info for the specified order
-- Changes:
	- 3.7.29 - 2015/06/18 - KDA - ADDED
***********************************/
CREATE FUNCTION fnOrderShipperSettlementFactor(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, R.SettlementFactorID
	FROM viewOrderBase O
	CROSS APPLY dbo.fnShipperSettlementFactor(O.OrderDate, null, O.CustomerID, O.ProductGroupID, O.OriginID, O.OriginStateID, O.DestinationID, O.DestStateID, O.ProducerID, 1) R
	WHERE O.ID = @ID
GO
GRANT SELECT ON fnOrderShipperSettlementFactor TO role_iis_acct
GO

/***********************************
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Shipper "Settlement" $$ values to an Delivered/Audited order
-- Changes:
	- 3.7.29 - 2015/06/18 - KDA - Add Shipper SettlementFactorID, MinSettlementUnitsID fields
***********************************/
ALTER VIEW viewOrderSettlementUnitsShipper AS
	SELECT X2.*
		, SettlementUnits = dbo.fnMaxDecimal(X2.ActualUnits, MinSettlementUnits)
	FROM (
		SELECT OrderID
			, ShipperID
			, SettlementUomID
			, ShipperSettlementFactorID
			, SettlementFactorID
			, MinSettlementUnits = dbo.fnMinDecimal(isnull(X.WRMSVUnits, X.MinSettlementUnits), X.MinSettlementUnits)
			, MinSettlementUnitsID
			, ActualUnits
		FROM (
			SELECT OrderID = O.ID
				, ShipperID = O.CustomerID
				, SettlementUomID = O.OriginUomID
				, ShipperSettlementFactorID = MSF.ID
				, MSF.SettlementFactorID
				, WRMSVUnits = dbo.fnConvertUOM(R.WRMSVUnits, R.WRMSVUomID, O.OriginUomID)
				, MinSettlementUnitsID = MSU.ID
				, MinSettlementUnits = isnull(dbo.fnConvertUOM(MSU.MinSettlementUnits, MSU.UomID, O.OriginUomID), 0) 
				, ActualUnits = CASE MSF.ID	WHEN 1 THEN isnull(O.OriginGrossUnits, 0) 
											WHEN 3 THEN isnull(O.OriginGrossStdUnits, O.OriginGrossUnits) 
											WHEN 2 THEN coalesce(O.OriginNetUnits, O.OriginGrossStdUnits, O.OriginGrossUnits, 0) 
											WHEN 4 THEN isnull(O.DestGrossUnits, 0)
											WHEN 5 THEN coalesce(O.DestNetUnits, O.DestGrossUnits, 0) END 
			FROM dbo.tblOrder O
			CROSS APPLY dbo.fnOrderShipperMinSettlementUnits(O.ID) MSU 
			CROSS APPLY dbo.fnOrderShipperSettlementFactor(O.ID) MSF
			JOIN dbo.tblRoute R ON R.ID = O.RouteID
			JOIN tblCustomer C ON C.ID = O.CustomerID
		) X
	) X2
GO

/***********************************
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Shipper "Settlement" $$ values to an Delivered/Audited order
-- Changes:
	- 3.7.29 - 2015/06/18 - KDA - honor new ShipperSettlementFactor | MinSettlementUnits Best-Match parameters
***********************************/
ALTER PROCEDURE spApplyRatesShipper
(
  @ID int
, @UserName varchar(100)
, @OriginWaitAmount money = NULL
, @DestWaitAmount money = NULL
, @RejectionAmount money = NULL
, @FuelSurchargeAmount money = NULL
, @LoadAmount money = NULL
, @AssessorialRateTypeID_CSV varchar(max) = NULL
, @AssessorialAmount_CSV varchar(max) = null
, @ResetOverrides bit = 0
) AS BEGIN

	SET NOCOUNT ON
	
	-- ensure this order hasn't yet been fully settled
	IF EXISTS (SELECT * FROM tblOrderSettlementShipper WHERE OrderID = @ID AND BatchID IS NOT NULL)
	BEGIN
		RAISERROR('Invoice has already been fully settled', 16, 1)
		RETURN
	END

	-- ensure the current Route.ActualMiles is assigned to the order
	UPDATE tblOrder SET ActualMiles = R.ActualMiles
	FROM tblOrder O
	JOIN tblRoute R ON R.ID = O.RouteID
	LEFT JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = O.ID
	WHERE O.ID = @ID
	  AND O.ActualMiles IS NULL OR O.ActualMiles <> R.ActualMiles
	  AND OSS.BatchID IS NULL
	
	-- get the provided manual Assessorial Rates (if any) into a usable able
	DECLARE @ManualAssessorialRates TABLE (ID int primary key, Amount money)
	INSERT INTO @ManualAssessorialRates (ID, Amount) 
		SELECT ID.ID, R.Amount
		FROM (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), ID FROM dbo.fnSplitCSVIDs(@AssessorialRateTypeID_CSV)) ID
		JOIN (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), amount = CAST(value as money) FROM dbo.fnSplitCSV(@AssessorialAmount_CSV)) R ON R.sortid = ID.sortid

	-- add in an existing manual Assessorial Rates from the existing Order Settlement (if any)
	INSERT INTO @ManualAssessorialRates (ID, Amount)
		SELECT AssessorialRateTypeID, Amount 
		FROM dbo.tblOrderSettlementShipperAssessorialCharge 
		WHERE OrderID = @ID AND AssessorialRateID IS NULL 
			AND ID NOT IN (SELECT ID FROM @ManualAssessorialRates)
			AND @ResetOverrides = 0

	IF (@ResetOverrides = 0)
	BEGIN 
		SELECT @OriginWaitAmount = OriginWaitAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @OriginWaitAmount IS NULL AND OriginWaitRateID IS NULL
		SELECT @DestWaitAmount = DestinationWaitAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @DestWaitAmount IS NULL AND DestinationWaitRateID IS NULL
		SELECT @RejectionAmount = OrderRejectAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @RejectionAmount IS NULL AND OrderRejectRateID IS NULL
		SELECT @FuelSurchargeAmount = FuelSurchargeAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @FuelSurchargeAmount IS NULL AND FuelSurchargeRateID IS NULL
		SELECT @LoadAmount = LoadAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @LoadAmount IS NULL AND RouteRateID IS NULL AND RangeRateID IS NULL
	END

	DECLARE @SettlementUnits TABLE
	(
		OrderID int
	  , SettlementUomID int
	  , SettlementFactorID int
	  , ShipperSettlementFactorID int
	  , MinSettlementUnits decimal(18, 10)
	  , MinSettlementUnitsID int
	  , ActualUnits decimal(18, 10)
	  , SettlementUnits decimal(18, 10)
	)
	INSERT INTO @SettlementUnits (OrderID, SettlementUomID, SettlementFactorID, ShipperSettlementFactorID, MinSettlementUnits, MinSettlementUnitsID, ActualUnits, SettlementUnits)
		SELECT OrderID, SettlementUomID, SettlementFactorID, ShipperSettlementFactorID, MinSettlementUnits, MinSettlementUnitsID, ActualUnits, SettlementUnits
		FROM dbo.viewOrderSettlementUnitsShipper
		WHERE OrderID = @ID

	SELECT OrderID = @ID
		, OrderDate
		, ShipperSettlementFactorID, MinSettlementUnitsID
		, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
		, RouteRateID, RangeRateID, LoadAmount
		, WaitFeeParameterID
		, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount 
		, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount
		, OrderRejectRateID, OrderRejectAmount
		, FuelSurchargeRateID, FuelSurchargeRate, FuelSurchargeAmount
		, OriginTaxRate
		, TotalAmount = ISNULL(LoadAmount, 0) 
			+ ISNULL(OriginWaitAmount, 0) 
			+ ISNULL(DestWaitAmount, 0) 
			+ ISNULL(OrderRejectAmount, 0) 
			+ ISNULL(FuelSurchargeAmount, 0)
		, CreatedByUser = @UserName
	INTO #I
	FROM (
		SELECT OrderDate
			, ShipperSettlementFactorID, MinSettlementUnitsID
			, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
			, RouteRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RouteRateID ELSE NULL END
			, RangeRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RangeRateID ELSE NULL END
			, LoadAmount = round(ISNULL(@LoadAmount, RR.Amount), 2)
			, WaitFeeParameterID
			, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount = ROUND(OriginWaitAmount, 2)
			, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount = ROUND(DestWaitAmount, 2)
			, OrderRejectRateID = CASE WHEN @RejectionAmount IS NULL THEN RD.RateID ELSE NULL END
			, OrderRejectAmount = round(isnull(@RejectionAmount, RD.Amount), 2)
			, FuelSurchargeRateID = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.RateID ELSE NULL END
			, FuelSurchargeRate = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.Rate ELSE NULL END
			, FuelSurchargeAmount = round(isnull(@FuelSurchargeAmount, FSR.Amount), 2)
			, OriginTaxRate
		FROM (
			SELECT O.ID
				, O.OriginID
				, O.CustomerID
				, O.RouteID
				, SU.ShipperSettlementFactorID
				, SU.SettlementFactorID
				, SU.SettlementUomID
				, SU.ActualUnits
				, SU.MinSettlementUnitsID
				, SU.MinSettlementUnits
				, SettlementUnits = CASE WHEN O.Rejected = 1 THEN 0 ELSE SU.SettlementUnits END
				, RouteMiles = O.ActualMiles
				, OrderDate = isnull(O.OrderDate, O.DueDate)
				, O.ChainUp
				, O.H2S
				, O.Rejected
				, O.RerouteCount
				, IsSplit = CASE WHEN O.Rejected = 0 AND O.TicketCount > 1 THEN 1 ELSE 0 END
				, WaitFeeParameterID = WFP.ID
				, OriginWaitRateID = CASE WHEN @OriginWaitAmount IS NULL THEN OWD.RateID ELSE NULL END
				, OriginWaitBillableMinutes = OWD.BillableMinutes
				, OriginWaitBillableHours = OWD.BillableHours
				, OriginWaitAmount = isnull(@OriginWaitAmount, OWD.Amount)
				, DestWaitRateID = CASE WHEN @DestWaitAmount IS NULL THEN DWD.RateID ELSE NULL END
				, DestWaitBillableMinutes = DWD.BillableMinutes
				, DestWaitBillableHours = DWD.BillableHours
				, DestWaitAmount = isnull(@DestWaitAmount, DWD.Amount)
				, OriginTaxRate = OO.TaxRate
			FROM dbo.viewOrder O
			JOIN @SettlementUnits SU ON SU.OrderID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			OUTER APPLY dbo.fnOrderShipperOriginWaitData(@ID) OWD 
			OUTER APPLY dbo.fnOrderShipperDestinationWaitData(@ID) DWD 
			OUTER APPLY dbo.fnOrderShipperWaitFeeParameter(@ID) WFP
			WHERE O.ID = @ID
		) X
		OUTER APPLY dbo.fnOrderShipperLoadAmount(@ID, SettlementUnits, SettlementUomID) RR 
		OUTER APPLY dbo.fnOrderShipperOrderRejectData(@ID, SettlementUnits, SettlementUomID) RD
		OUTER APPLY dbo.fnOrderShipperFuelSurchargeData(@ID) FSR
	) X2

	SELECT OrderID = @ID, AssessorialRateTypeID, AssessorialRateID = RateID, Amount = ROUND(Amount, 2), CreatedByUser = @UserName 
	INTO #IA
	FROM @SettlementUnits SU
	CROSS APPLY dbo.fnOrderShipperAssessorialAmounts(@ID, SU.SettlementUnits, SU.SettlementUomID) CAA
	WHERE CAA.AssessorialRateTypeID NOT IN (SELECT ID FROM @ManualAssessorialRates)

	DECLARE @CreatedTran bit; SET @CreatedTran = CASE WHEN @@TRANCOUNT = 0 THEN 1 ELSE 0 END
	BEGIN TRY
		IF (@CreatedTran = 1)
			BEGIN TRAN

		-- remove the existing settlment record (if any)
		DELETE FROM tblOrderSettlementShipperAssessorialCharge WHERE OrderID = @ID
		DELETE FROM tblOrderSettlementShipper WHERE OrderID = @ID

		-- all Units and Rates are first normalized to the Order.OriginUOM then consistent processing
		-- and persisted in the Invoice (Settlement) record in this Order.OriginUOM 
		INSERT INTO tblOrderSettlementShipper (
/*1*/		OrderID
/*2*/		, OrderDate
/*3*/		, ShipperSettlementFactorID
/*4*/		, SettlementFactorID 
/*5*/		, SettlementUomID 
/*6*/		, MinSettlementUnitsID
/*7*/		, MinSettlementUnits 
/*8*/		, SettlementUnits
/*9*/		, RouteRateID
/*10*/		, RangeRateID 
/*11*/		, LoadAmount
/*12*/		, WaitFeeParameterID 
/*13*/		, OriginWaitRateID 
/*14*/		, OriginWaitBillableMinutes 
/*15*/		, OriginWaitBillableHours
/*16*/		, OriginWaitAmount 
/*17*/		, DestinationWaitRateID 
/*18*/		, DestinationWaitBillableMinutes 
/*19*/		, DestinationWaitBillableHours
/*20*/		, DestinationWaitAmount 
/*21*/		, OrderRejectRateID 
/*22*/		, OrderRejectAmount 
/*23*/		, FuelSurchargeRateID 
/*24*/		, FuelSurchargeRate 
/*25*/		, FuelSurchargeAmount 
/*26*/		, OriginTaxRate 
/*27*/		, TotalAmount 
/*28*/		, CreatedByUser)
		SELECT 
/*01*/		OrderID
/*02*/		, OrderDate
/*03*/		, ShipperSettlementFactorID
/*04*/		, SettlementFactorID 
/*05*/		, SettlementUomID 
/*06*/		, MinSettlementUnitsID
/*07*/		, MinSettlementUnits 
/*08*/		, SettlementUnits
/*09*/		, RouteRateID
/*10*/		, RangeRateID 
/*11*/		, LoadAmount
/*12*/		, WaitFeeParameterID 
/*13*/		, OriginWaitRateID 
/*14*/		, OriginWaitBillableMinutes 
/*15*/		, OriginWaitBillableHours
/*16*/		, OriginWaitAmount 
/*17*/		, DestWaitRateID 
/*18*/		, DestWaitBillableMinutes 
/*19*/		, DestWaitBillableHours
/*20*/		, DestWaitAmount 
/*21*/		, OrderRejectRateID 
/*22*/		, OrderRejectAmount 
/*23*/		, FuelSurchargeRateID 
/*24*/		, FuelSurchargeRate 
/*25*/		, FuelSurchargeAmount 
/*26*/		, OriginTaxRate 
/*27*/		, TotalAmount 
/*28*/		, CreatedByUser
		FROM #I

		INSERT INTO tblOrderSettlementShipperAssessorialCharge (OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser)
			SELECT OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser FROM #IA
			UNION
			SELECT @ID, ID, NULL, Amount, @UserName FROM @ManualAssessorialRates
	
		IF (@CreatedTran = 1)
			COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @msg varchar(max), @severity int
		SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY()
		ROLLBACK
		RAISERROR(@msg, @severity, 1)
		RETURN
	END CATCH	
	
END

GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details + FINANCIAL INFO into a single view
-- Changes:
	-- 3.7.29 - 2015/06/18 - KDA - add translated "SettlementFactor" column
/***********************************/
ALTER VIEW viewOrderSettlementShipper AS 
	SELECT OSC.*
		, BatchNum = SB.BatchNum
		, SettlementUom = UOM.Name
		, SettlementUomShort = UOM.Abbrev
		, RouteRate = RR.Rate
		, RouteRateType = RRT.Name
		, RateSheetRate = RSRR.Rate
		, RateSheetRateType = RSRT.Name
		, OrderRejectRate = ORR.Rate
		, OrderRejectRateType = ORRT.Name
		, OriginWaitRate = OWR.Rate
		, DestinationWaitRate = DWR.Rate
		, TotalWaitAmount = cast(isnull(OSC.OriginWaitAmount, 0) as money) + cast(isnull(OSC.DestinationWaitAmount, 0) as money)
		, ChainupRate = CAR.Rate
		, ChainupRateType = CART.Name
		, ChainupAmount = CA.Amount
		, RerouteRate = RAR.Rate
		, RerouteRateType = RART.Name
		, RerouteAmount = RA.Amount
		, SplitLoadRate = SAR.Rate
		, SplitLoadRateType = SART.Name
		, SplitLoadAmount = SA.Amount
		, H2SRate = HAR.Rate
		, H2SRateType = HART.Name
		, H2SAmount = HA.Amount
		, OtherAmount = OA.Amount
		, WaitFeeSubUnit = isnull(WFSU.Name, 'None') 
		, WaitFeeRoundingType = isnull(WFRT.Name, 'None') 
		, SettlementFactor = SF.Name
	FROM tblOrderSettlementShipper OSC 
	LEFT JOIN tblShipperSettlementBatch SB ON SB.ID = OSC.BatchID
	LEFT JOIN tblUom UOM ON UOM.ID = OSC.SettlementUomID
	LEFT JOIN tblShipperOriginWaitRate OWR ON OWR.ID = OSC.OriginWaitRateID
	LEFT JOIN tblShipperDestinationWaitRate DWR ON DWR.ID = OSC.DestinationWaitRateID
	LEFT JOIN tblShipperOrderRejectRate ORR ON ORR.ID = OSC.OrderRejectRateID
	LEFT JOIN tblRateType ORRT ON ORRT.ID = ORR.RateTypeID
	-- route rate/ratesheet JOINs
	LEFT JOIN tblShipperRouteRate RR ON RR.ID = OSC.RouteRateID
	LEFT JOIN tblRateType RRT ON RRT.ID = RR.RateTypeID
	LEFT JOIN viewShipperRateSheetRangeRate RSRR ON RSRR.ID = OSC.RangeRateID
	LEFT JOIN tblRateType RSRT ON RSRT.ID = RSRR.RateTypeID
	-- chainup
	LEFT JOIN tblOrderSettlementShipperAssessorialCharge CA ON CA.OrderID = OSC.OrderID AND CA.AssessorialRateTypeID = 1
	LEFT JOIN tblShipperAssessorialRate CAR ON CAR.ID = CA.AssessorialRateID
	LEFT JOIN tblRateType CART ON CART.ID = CAR.RateTypeID
	-- Reroute
	LEFT JOIN tblOrderSettlementShipperAssessorialCharge RA ON RA.OrderID = OSC.OrderID AND RA.AssessorialRateTypeID = 2
	LEFT JOIN tblShipperAssessorialRate RAR ON RAR.ID = RA.AssessorialRateID
	LEFT JOIN tblRateType RART ON RART.ID = RAR.RateTypeID
	-- Split Load
	LEFT JOIN tblOrderSettlementShipperAssessorialCharge SA ON SA.OrderID = OSC.OrderID AND SA.AssessorialRateTypeID = 3
	LEFT JOIN tblShipperAssessorialRate SAR ON SAR.ID = SA.AssessorialRateID
	LEFT JOIN tblRateType SART ON SART.ID = SAR.RateTypeID
	-- H2S
	LEFT JOIN tblOrderSettlementShipperAssessorialCharge HA ON HA.OrderID = OSC.OrderID AND HA.AssessorialRateTypeID = 4
	LEFT JOIN tblShipperAssessorialRate HAR ON HAR.ID = HA.AssessorialRateID
	LEFT JOIN tblRateType HART ON HART.ID = HAR.RateTypeID
	-- Other Assessorial Charges (combined)
	LEFT JOIN (
		SELECT OrderID, Amount = SUM(Amount) FROM tblOrderSettlementShipperAssessorialCharge WHERE AssessorialRateTypeID NOT IN (1,2,3,4) GROUP BY OrderID
	) OA ON OA.OrderID = OSC.OrderID 
	LEFT JOIN tblSettlementFactor SF ON SF.ID = OSC.SettlementFactorID
	LEFT JOIN tblShipperWaitFeeParameter WFP ON WFP.ID = OSC.WaitFeeParameterID
	LEFT JOIN dbo.tblWaitFeeSubUnit WFSU ON WFSU.ID = WFP.SubUnitID
	LEFT JOIN dbo.tblWaitFeeRoundingType WFRT ON WFRT.ID = WFP.RoundingTypeID

GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details + FINANCIAL INFO into a single view
-- Changes:
	-- 3.7.29 - 2015/06/18 - KDA - add new columns: ShipperSettlementFactorID, SettlementFactorID, MinSettlementUnitsID
/***********************************/
ALTER VIEW viewOrder_Financial_Shipper AS 
	SELECT O.* 
		, Shipper = Customer
		, TicketNums = dbo.fnOrderTicketDetails(O.ID, 'TicketNums', '<br/>') 
		, TankNums = dbo.fnOrderTicketDetails(O.ID, 'TankNums', '<br/>') 
		, PreviousDestinations = dbo.fnRerouteDetails(O.ID, 'PreviousDestinations', '<br/>') 
		, RerouteUsers = dbo.fnRerouteDetails(O.ID, 'RerouteUsers', '<br/>') 
		, RerouteDates = dbo.fnRerouteDetails(O.ID, 'RerouteDates', '<br/>') 
		, RerouteNotes = dbo.fnRerouteDetails(O.ID, 'RerouteNotes', '<br/>') 
		, InvoiceRatesAppliedDate = dbo.fnUTC_To_Local(OS.CreateDateUTC, O.OriginTimeZoneID, O.OriginUseDST)
		, OS.BatchID
		, InvoiceBatchNum = OS.BatchNum 
		, InvoiceOriginWaitBillableMinutes = OS.OriginWaitBillableMinutes
		, InvoiceDestinationWaitBillableMinutes = OS.DestinationWaitBillableMinutes
		, InvoiceTotalWaitBillableMinutes = isnull(OS.OriginWaitBillableMinutes, 0) + ISNULL(OS.DestinationWaitBillableMinutes, 0) 
		, InvoiceWaitFeeParameterID = WaitFeeParameterID
		, InvoiceWaitFeeSubUnit = WaitFeeSubUnit
		, InvoiceWaitFeeRoundingType = WaitFeeRoundingType
		, InvoiceOriginWaitRate = OS.OriginWaitRate 
		, InvoiceOriginWaitAmount = OS.OriginWaitAmount 		
		, InvoiceDestinationWaitRate = OS.DestinationWaitRate 
		, InvoiceDestinationWaitAmount = OS.DestinationWaitAmount 
		, InvoiceTotalWaitAmount = OS.TotalWaitAmount
		, InvoiceOrderRejectRate = OS.OrderRejectRate  -- changed
		, InvoiceOrderRejectRateType = OS.OrderRejectRateType -- changed
		, InvoiceOrderRejectAmount = OS.OrderRejectAmount  -- changed
		, InvoiceChainupRate = OS.ChainupRate
		, InvoiceChainupRateType = OS.ChainupRateType
		, InvoiceChainupAmount = OS.ChainupAmount 
		, InvoiceRerouteRate = OS.RerouteRate
		, InvoiceRerouteRateType = OS.RerouteRateType
		, InvoiceRerouteAmount = OS.RerouteAmount 
		, InvoiceH2SRate = OS.H2SRate
		, InvoiceH2SRateType = OS.H2SRateType
		, InvoiceH2SAmount = OS.H2SAmount
		, InvoiceSplitLoadRate = OS.SplitLoadRate
		, InvoiceSplitLoadRateType = OS.SplitLoadRateType
		, InvoiceSplitLoadAmount = OS.SplitLoadAmount
		, InvoiceOtherAmount = OS.OtherAmount
		, InvoiceTaxRate = OS.OriginTaxRate
		, InvoiceSettlementUom = OS.SettlementUom -- changed
		, InvoiceSettlementUomShort = OS.SettlementUomShort -- changed
		, InvoiceShipperSettlementFactorID = OS.ShipperSettlementFactorID -- 2015/06/18 - KDA - added
		, InvoiceSettlementFactorID = OS.SettlementFactorID -- 2015/06/18 - KDA - added
		, InvoiceSettlementFactor = OS.SettlementFactor -- 2015/06/18 - KDA - added
		, InvoiceMinSettlementUnitsID = OS.MinSettlementUnitsID -- 2015/06/18 - KDA - added
		, InvoiceMinSettlementUnits = OS.MinSettlementUnits
		, InvoiceUnits = OS.SettlementUnits
		, InvoiceRouteRate = OS.RouteRate
		, InvoiceRouteRateType = OS.RouteRateType
		, InvoiceRateSheetRate = OS.RateSheetRate
		, InvoiceRateSheetRateType = OS.RateSheetRateType
		, InvoiceFuelSurchargeRate = OS.FuelSurchargeRate
		, InvoiceFuelSurchargeAmount = OS.FuelSurchargeAmount
		, InvoiceLoadAmount = OS.LoadAmount
		, InvoiceTotalAmount = OS.TotalAmount
	FROM dbo.viewOrderLocalDates O
	LEFT JOIN viewOrderSettlementShipper OS ON OS.OrderID = O.ID
	WHERE O.StatusID NOT IN (-10) -- don't include "Generated" orders (only Assigned+)

GO

EXEC _spRebuildAllObjects
GO

-----------------------------------
-- TODO: add OrderApproval to process
-----------------------------------

COMMIT
SET NOEXEC OFF