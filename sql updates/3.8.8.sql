-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.8.7.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.8.7.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0

DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.8.7'
SELECT  @NewVersion = '3.8.8'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Fix ID issue (swapped) for driver app settings "Log Sync JSON" and "Limit to single active order"'
	UNION SELECT @NewVersion, 0, 'Change category of driver app "Show Bottom Detail Entry Fields (FT, Q)" setting'
	UNION SELECT @NewVersion, 0, 'DCDRV-221: New Order Rule for Ticket Type Changes Allowed'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/* The ID of the two driver app settings "Log Sync JSON" and "Limit to single active order" need to be swapped in the database to match what the driver app is looking for */
UPDATE tblSetting SET ID = 999 WHERE ID = 43
UPDATE tblSetting SET ID = 43 WHERE ID = 44
UPDATE tblSetting SET ID = 44 WHERE ID = 999
GO

/* Correct category of the setting "Show Bottom Detail Entry Fields (FT, Q)" for the driver app */
UPDATE tblSetting SET Category = 'Driver App Settings' WHERE ID = 24
GO

/* 07/23/2015 - GSM - DCDRV-221 - Add Order Rule to allow ticket type changes (between certain types) */
INSERT tblOrderRuleType (ID,Name,RuleTypeID)
     VALUES	(7,'Ticket Type Changes Allowed',5)
GO

COMMIT 
SET NOEXEC OFF