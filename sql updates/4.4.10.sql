SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.4.9'
SELECT  @NewVersion = '4.4.10'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'JT-156 - Add validation/exception reporting to Questionnaires'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


ALTER TABLE tblQuestionnaireTemplate ADD ExceptionEmail VARCHAR(255) NULL
ALTER TABLE tblQuestionnaireTemplate ADD ExceptionFormat VARCHAR(100) NULL


ALTER TABLE tblQuestionnaireQuestion ADD PassingCondition VARCHAR(255) NULL

GO


COMMIT
SET NOEXEC OFF
