-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.11.12.1'
SELECT  @NewVersion = '3.11.12.2'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1260 & DCWEB-1226: Grid filtering fixes for state column on Carrier and Truck maintenanace pages'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/***********************************
-- Date Created: 26 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Truck info with Last Odometer entry 
-- Changes:
		- 3.10.13  - JAE - Add Truck Type
		- 3.11.12.2 - BB - Add Garage State Abbreviation to allow filtering on webpage grid
***********************************/
ALTER VIEW viewTruckWithLastOdometer AS
	SELECT T.ID
		, CarrierID
		, IDNumber
		, DOTNumber
		, VIN
		, Make
		, Model
		, Year
		, T.CreateDateUTC
		, T.CreatedByUser
		, LastChangeDateUTC = dbo.fnMaxDateTime(T.LastChangeDateUTC, TM.OdometerDateUTC)
		, T.LastChangedByUser
		, LicenseNumber
		, AxleCount
		, MeterType
		, HasSleeper
		, PurchasePrice
		, GarageCity
		, GarageStateID
		, GarageStateAbbrev = S.Abbreviation  -- 3.11.12.2
		, RegistrationDocument
		, RegistrationDocName
		, RegistrationExpiration
		, DeleteDateUTC
		, DeletedByUser
		, InsuranceIDCardDocument
		, InsuranceIDCardDocName
		, InsuranceIDCardExpiration
		, InsuranceIDCardIssue
		, CIDNumber
		, SIDNumber
		, GPSUnit
		, OwnerInfo
		, TireSize
		, Active
		, T.FullName
		, CarrierType
		, Carrier
		, LastOdometer = TM.Odometer, LastOdometerDate = TM.OdometerDate
		, TruckTypeID
		, TruckType
	FROM dbo.viewTruck T
	LEFT JOIN viewOrderLastTruckMileage TM ON TM.TruckID = T.ID
	LEFT JOIN tblState S ON S.ID = T.GarageStateID
GO

EXEC sp_RefreshView viewTruckWithLastOdometer
GO

COMMIT 
SET NOEXEC OFF