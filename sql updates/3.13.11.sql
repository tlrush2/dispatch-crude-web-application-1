SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20) = '3.13.10'
	, @NewVersion varchar(20) = '3.13.11'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1453 - Update driver app flag for Shipper PO Order Rule'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

UPDATE tblOrderRuleType SET ForDriverApp = 1 WHERE ID = 2
GO

COMMIT
SET NOEXEC OFF