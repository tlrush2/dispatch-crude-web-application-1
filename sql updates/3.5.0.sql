-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.4.13'
SELECT  @NewVersion = '3.5.0'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Completely rewrite Settlement system to be much modular, granular, optimized and auto-rating (preliminary)'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/***********************************************************
-- PRODUCT GROUP CHANGES
***********************************************************/
CREATE TABLE tblProductGroup
(
  ID int identity(1, 1) NOT NULL CONSTRAINT PK_ProductGroup PRIMARY KEY
, Name varchar(25) NOT NULL
)
GO
GRANT SELECT, DELETE, UPDATE, INSERT ON tblProductGroup TO dispatchcrude_iis_acct
GO
CREATE UNIQUE INDEX udxProductGroup_Name ON tblProductGroup(Name)
GO

INSERT INTO tblProductGroup (Name) SELECT DISTINCT PG = isnull(ProductGroup, 'Other') FROM tblProduct ORDER BY PG
GO

ALTER TABLE tblProduct ADD ProductGroupID int NOT NULL CONSTRAINT DF_Product_ProductGroup DEFAULT (1)
GO
UPDATE tblProduct SET ProductGroupID = (SELECT ID FROM tblProductGroup WHERE Name = isnull(ProductGroup, 'Other'))
GO
ALTER TABLE tblProduct DROP COLUMN ProductGroup
GO

/***********************************/
-- Date Created: 20 Dec 2014
-- Author: Kevin Alons
-- Purpose: return Product records with "friendly" translated values included
/***********************************/
CREATE VIEW viewProduct AS
	SELECT P.*
		, ProductGroup = PG.Name
	FROM tblProduct P
	JOIN tblProductGroup PG ON PG.ID = P.ProductGroupID

GO
GRANT SELECT ON viewProduct TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Order records with "translated friendly" values for FK relationships
/***********************************/
ALTER VIEW [dbo].[viewOrder] AS
SELECT O.*
	, OPS.OrderStatus AS PrintStatus
FROM (
	SELECT O.*
	, OrderDate = cast(dbo.fnUTC_to_Local(O.OriginDepartTimeUTC, vO.TimeZoneID, vO.UseDST) as date) 
	, Origin = vO.Name 
	, OriginFull = vO.FullName 
	, OriginState = vO.State 
	, OriginStateAbbrev = vO.StateAbbrev 
	, OriginStation = vO.Station
	, OriginCounty = vO.County 
	, vO.LeaseName
	, vO.LeaseNum
	, OriginRegion = vO.Region
	, OriginRegionID = vO.RegionID
	, OriginCountryID = vO.CountryID
	, OriginCountry = vO.Country
	, OriginCountryShort = vO.CountryShort
	, OriginLegalDescription = vO.LegalDescription 
	, OriginNDIC = vO.NDICFileNum 
	, OriginNDM = vO.NDM 
	, OriginCA = vO.CA 
	, OriginTimeZoneID = vO.TimeZoneID 
	, OriginUseDST = vO.UseDST 
	, OriginStateID = vO.StateID
	, vO.H2S
	, Destination = vD.Name 
	, DestinationFull = vD.FullName 
	, DestinationState = vD.State 
	, DestinationStateAbbrev = vD.StateAbbrev 
	, DestinationTypeID = vD.ID
	, vD.DestinationType
	, DestStation = vD.Station 
	, DestTimeZoneID = vD.TimeZoneID 
	, DestUseDST = vD.UseDST 
	, DestStateID = vD.StateID
	, DestRegion = vO.Region
	, DestRegionID = vO.RegionID
	, DestCountryID = vD.CountryID
	, DestCountry = vD.Country
	, DestCountryShort = vD.CountryShort
	, Customer = C.Name 
	, Carrier = CA.Name 
	, CarrierType = CT.Name 
	, OS.OrderStatus
	, OS.StatusNum
	, Driver = D.FullName 
	, DriverFirst = D.FirstName 
	, DriverLast = D.LastName 
	, Truck = TRU.FullName 
	, Trailer = TR1.FullName 
	, Trailer2 = TR2.FullName 
	, P.PriorityNum
	, TicketType = TT.Name 
	, DestTicketTypeID = vD.TicketTypeID 
	, DestTicketType = vD.TicketType 
	, Operator = OP.Name 
	, Producer = PR.Name 
	, Pumper = PU.FullName 
	, DriverNumber = D.IDNumber
	, CarrierNumber = CA.IDNumber
	, CarrierTypeID = CA.CarrierTypeID
	, TruckNumber = TRU.IDNumber 
	, TrailerNumber = TR1.IDNumber 
	, Trailer2Number = TR2.IDNumber 
	, Product = PRO.Name
	, ProductShort = PRO.ShortName 
	, PRO.ProductGroupID
	, PRO.ProductGroup
	, OriginUOM = OUom.Name 
	, OriginUomShort = OUom.Abbrev 
	, OriginTankID_Text = CASE WHEN ORT.TankNum = '*' THEN '*' + isnull(O.OriginTankNum, '?') ELSE ORT.TankNum END 
	, DestUOM = DUom.Name 
	, DestUomShort = DUom.Abbrev 
	, Active = cast((CASE WHEN O.DeleteDateUTC IS NULL THEN 1 ELSE 0 END) as bit) 
	, IsDeleted = cast((CASE WHEN O.DeleteDateUTC IS NOT NULL THEN 1 ELSE 0 END) as bit) 
	, PickupPrintStatus = PPS.Name 
	, PickupCompleted = PPS.IsCompleted 
	, DeliverPrintStatus = DPS.Name 
	, DeliverCompleted = DPS.IsCompleted 
	, PrintStatusID = CASE WHEN StatusID = 8 AND PPS.IsCompleted = 0 THEN 7
						   WHEN StatusID = 3 AND DPS.IsCompleted = 0 THEN 8
						ELSE StatusID END 
	, OriginTankText = CASE WHEN O.OriginTankID IS NULL OR ORT.TankNum = '*' THEN O.OriginTankNum ELSE ORT.TankNum END
	, OriginWaitNum = OWR.Num
	, OriginWaitDesc = OWR.Description
	, OriginWaitNumDesc = OWR.NumDesc
	, DestWaitNum = DWR.Num
	, DestWaitDesc = DWR.Description
	, DestWaitNumDesc = DWR.NumDesc
	, RejectNum = ORR.Num
	, RejectDesc = ORR.Description
	, RejectNumDesc = ORR.NumDesc
	, RerouteCount = (SELECT COUNT(1) FROM tblOrderReroute ORE WHERE ORE.OrderID = O.ID)
	, TicketCount = (SELECT COUNT(1) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND OT.DeleteDateUTC IS NULL)
	FROM dbo.tblOrder O
	LEFT JOIN dbo.viewOrigin vO ON vO.ID = O.OriginID
	LEFT JOIN dbo.viewDestination vD ON vD.ID = O.DestinationID
	LEFT JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
	JOIN dbo.tblOrderStatus AS OS ON OS.ID = O.StatusID
	LEFT JOIN dbo.tblCarrier CA ON CA.ID = O.CarrierID
	LEFT JOIN dbo.tblCarrierType CT ON CT.ID = CA.CarrierTypeID
	LEFT JOIN dbo.viewDriverBase D ON D.ID = O.DriverID
	LEFT JOIN dbo.viewTruck TRU ON TRU.ID = O.TruckID
	LEFT JOIN dbo.viewTrailer TR1 ON TR1.ID = O.TrailerID
	LEFT JOIN dbo.viewTrailer TR2 ON TR2.ID = O.Trailer2ID
	LEFT JOIN dbo.tblPriority P ON P.ID = O.PriorityID
	LEFT JOIN dbo.tblTicketType TT ON TT.ID = O.TicketTypeID
	LEFT JOIN dbo.tblOriginTank ORT ON ORT.ID = O.OriginTankID
	LEFT JOIN dbo.tblOperator OP ON OP.ID = O.OperatorID
	LEFT JOIN dbo.viewPumper PU ON PU.ID = O.PumperID
	LEFT JOIN dbo.tblProducer PR ON PR.ID = O.ProducerID
	LEFT JOIN dbo.viewProduct PRO ON PRO.ID = O.ProductID
	LEFT JOIN dbo.tblUom OUom ON OUom.ID = O.OriginUomID
	LEFT JOIN dbo.tblUom DUom ON DUom.ID = O.DestUomID
	LEFT JOIN dbo.tblPrintStatus PPS ON PPS.ID = O.PickupPrintStatusID
	LEFT JOIN dbo.tblPrintStatus DPS ON DPS.ID = O.DeliverPrintStatusID
	LEFT JOIN dbo.viewOriginWaitReason OWR ON OWR.ID = O.OriginWaitReasonID
	LEFT JOIN dbo.viewDestinationWaitReason DWR ON DWR.ID = O.DestWaitReasonID
	LEFT JOIN dbo.viewOrderRejectReason ORR ON ORR.ID = O.RejectReasonID
) O
LEFT JOIN dbo.tblOrderStatus OPS ON OPS.ID = O.PrintStatusID

GO

EXEC _spRebuildAllObjects
GO

CREATE FUNCTION [dbo].[fnMinInt](@first int, @second int) RETURNS int AS BEGIN
	SELECT @first = isnull(@first, 0), @second = isnull(@second, 0)
	RETURN (CASE WHEN @first < @second THEN @first ELSE @second END)
END
GO
GRANT EXECUTE ON fnMinInt TO dispatchcrude_iis_acct
GO

-- GENERIC SETTLEMENT CHANGES

CREATE TABLE tblRateType
(
  ID int NOT NULL CONSTRAINT PK_RateType PRIMARY KEY CLUSTERED
, Name varchar(25) NOT NULL 
, ForCarrier bit NOT NULL CONSTRAINT DF_RateType_ForCarrier DEFAULT (0)
, ForShipper bit NOT NULL CONSTRAINT DF_RateType_ForShipper DEFAULT (0)
, ForOrderReject bit NOT NULL CONSTRAINT DF_RateType_ForOrderReject DEFAULT (0)
)
GO
GRANT SELECT ON tblRateType TO dispatchcrude_iis_acct
GO
CREATE UNIQUE INDEX udxRateType_Name ON tblRateType(Name)
GO

INSERT INTO tblRateType (ID, Name, ForCarrier, ForShipper, ForOrderReject)
	SELECT 1, 'Per Unit', 1, 1, 0 
	UNION SELECT 2, 'Flat', 1, 1, 1
	UNION SELECT 3, '% of Shipper', 1, 0, 1
GO

CREATE TABLE tblAssessorialRateType
(
  ID int identity(10000, 1) NOT NULL CONSTRAINT PK_CarrierAssessorialRateType PRIMARY KEY
, Name varchar(25) NOT NULL
, IsSystem bit NOT NULL CONSTRAINT DF_CarrierAssessorialRateType_IsSystem DEFAULT (0)
, CreateDateUTC smalldatetime NOT NULL CONSTRAINT DF_CarrierAssessorialRateType_CreateDateUTC DEFAULT (getutcdate())
, CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_CarrierAssessorialRateType_CreatedByUser DEFAULT (SUSER_NAME())
, LastChangeDateUTC smalldatetime NULL
, LastChangedByUser varchar(100) NULL
, DeleteDateUTC smalldatetime NULL
, DeletedByUser varchar(100) NULL
)
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblAssessorialRateType TO dispatchcrude_iis_acct
GO
CREATE UNIQUE INDEX udxAssessorialRateType_Name ON tblAssessorialRateType(Name)
GO
set identity_insert tblAssessorialRateType on
insert into tblAssessorialRateType (ID, Name, IsSystem, CreatedByUser)
	select 1, 'Chain Up', 1, 'System'
	union select 2, 'Reroute', 1, 'System'
	union select 3, 'Split Load', 1, 'System'
	union select 4, 'H2S', 1, 'System' 
set identity_insert tblAssessorialRateType off
GO

/***********************************/
-- Date Created: 23 Dec 2014
-- Author: Kevin Alons
-- Purpose: compute and return the normalized "rate" + Amount for the specified order parameters
/***********************************/
CREATE FUNCTION fnRateToAmount(@RateTypeID int, @Units decimal(18, 10), @UomID int, @Rate decimal(18, 10), @RateUomID int, @CustomerAmount money = NULL) RETURNS decimal(18, 10)
AS BEGIN
	DECLARE @ret money
	IF (@RateTypeID = 1) -- Per Unit rate type
		SET @ret = @Units * dbo.fnConvertRateUOM(@Rate, @RateUomID, @UomID)
	ELSE IF (@RateTypeID = 2) -- Flat
		SET @ret = @Rate
	ELSE IF (@RateTypeID = 3) -- % of Customer (Shipper) rate
		SET @ret = @Rate * @CustomerAmount
	
	RETURN (@ret)
END
GO
GRANT EXECUTE ON fnRateToAmount TO dispatchcrude_iis_acct
GO

CREATE FUNCTION fnCompareNullableInts(@int1 int, @int2 int) RETURNS bit AS 
BEGIN
	DECLARE @ret bit
	SET @ret = CASE WHEN ISNULL(@int1, 0) = ISNULL(@int2, 0) THEN 1 ELSE 0 END
	RETURN (@ret)
END
GO
GRANT EXECUTE ON fnCompareNullableInts TO dispatchcrude_iis_acct
GO

---------------------------------------------------------------
-- CARRIER SETTLEMENT CHANGES
---------------------------------------------------------------
GO

CREATE TABLE tblCarrierWaitFeeParameter
(
  ID int identity(1, 1) NOT NULL CONSTRAINT PK_CarrierWaitFeeParameter PRIMARY KEY NONCLUSTERED
, CarrierID int NULL CONSTRAINT FK_CarrierWaitFeeParameter_Carrier FOREIGN KEY REFERENCES tblCarrier(ID)
, ProductGroupID int NULL CONSTRAINT FK_CarrierWaitFeeParameter_ProductGroup FOREIGN KEY REFERENCES tblProductGroup(ID)
, OriginStateID int NULL CONSTRAINT FK_CarrierWaitFeeParameter_OriginState FOREIGN KEY REFERENCES tblState(ID)
, DestStateID int NULL CONSTRAINT FK_CarrierWaitFeeParameter_DestState FOREIGN KEY REFERENCES tblState(ID)
, RegionID int NULL CONSTRAINT FK_CarrierWaitFeeParameter_Region FOREIGN KEY REFERENCES tblRegion(ID)
, EffectiveDate date NOT NULL
, EndDate date NOT NULL 
, RoundingTypeID int NOT NULL CONSTRAINT FK_CarrierWaitFeeParameter_RoundingType FOREIGN KEY REFERENCES tblWaitFeeRoundingType(ID)
, SubUnitID int NOT NULL CONSTRAINT FK_CarrierWaitFeeParameter_SubUnit FOREIGN KEY REFERENCES tblWaitFeeSubUnit(ID)
, ThresholdMinutes int NOT NULL CONSTRAINT DF_CarrierWaitFeeParameter_ThresholdMinutes DEFAULT(60) CONSTRAINT CK_CarrierWaitFeeParameter_ThresholdMinutes_Positive CHECK (ThresholdMinutes > 0)
, CreateDateUTC smalldatetime NOT NULL CONSTRAINT DF_CarrierWaitFeeParameter_CreateDateUTC DEFAULT (getutcdate())
, CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_CarrierWaitFeeParameter_CreatedByUser DEFAULT (SUSER_NAME())
, LastChangeDateUTC smalldatetime NULL
, LastChangedByUser varchar(100) NULL
, CONSTRAINT CK_CarrierWaitFeeParameter_EndDate_Greater CHECK (EndDate >= EffectiveDate)
)
GO
GRANT SELECT, INSERT, UPDATE ON tblCarrierWaitFeeParameter TO dispatchcrude_iis_acct
GO
CREATE UNIQUE CLUSTERED INDEX udxCarrierWaitFeeParameter_Main ON tblCarrierWaitFeeParameter(CarrierID, ProductGroupID, OriginStateID, DestStateID, RegionID, EffectiveDate)
GO
CREATE INDEX idxCarrierWaitFeeParameter_Carrier ON tblCarrierWaitFeeParameter(CarrierID)
GO
CREATE INDEX idxCarrierWaitFeeParameter_Region ON tblCarrierWaitFeeParameter(RegionID)
GO
CREATE INDEX idxCarrierWaitFeeParameter_OriginState ON tblCarrierWaitFeeParameter(OriginStateID)
GO
CREATE INDEX idxCarrierWaitFeeParameter_DestState ON tblCarrierWaitFeeParameter(DestStateID)
GO
CREATE INDEX idxCarrierWaitFeeParameter_ProductGroup ON tblCarrierWaitFeeParameter(ProductGroupID)
GO

/***********************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
/***********************************************/
CREATE TRIGGER trigCarrierWaitFeeParameter_IU ON tblCarrierWaitFeeParameter AFTER INSERT, UPDATE AS
BEGIN
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblCarrierWaitFeeParameter X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(i.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		RAISERROR('Overlapping Parameters are not allowed', 16, 1)
		-- if an explicit TRANSACTION isn't present, then ROLLBACK to ensure these changes cannot be committed
		IF (@@TRANCOUNT = 0) ROLLBACK
	END
END
GO

INSERT INTO tblCarrierWaitFeeParameter (CarrierID, RegionID, EffectiveDate, EndDate, RoundingTypeID, SubUnitID, ThresholdMinutes, CreateDateUTC, CreatedByUser)
	select nullif(carrierid, -1), nullif(regionid, -1), '1/1/2013', '2/28/2015', WaitFeeRoundingTypeID, WaitFeeSubUnitID, 60, CreateDateUTC, CreatedByUser 
	from tblcarrierrates
GO

CREATE TABLE tblCarrierRouteRate
(
  ID int IDENTITY(1,1) NOT NULL CONSTRAINT PK_CarrierRouteRate PRIMARY KEY NONCLUSTERED
, CarrierID int NULL CONSTRAINT FK_CarrierRouteRate_Carrier FOREIGN KEY REFERENCES tblCarrier(ID) ON DELETE CASCADE
, ProductGroupID int NULL CONSTRAINT FK_CarrierRouteRate_ProductGroup FOREIGN KEY REFERENCES tblProductGroup(ID)
, RouteID int NOT NULL CONSTRAINT FK_CarrierRouteRate_Route FOREIGN KEY REFERENCES tblRoute(ID) ON DELETE CASCADE
, Rate decimal(18, 10) NOT NULL
, EffectiveDate date NOT NULL
, EndDate date NOT NULL 
, RateTypeID int NOT NULL CONSTRAINT FK_CarrierRouteRate_RateType FOREIGN KEY REFERENCES tblRateType(ID)
, UomID int NOT NULL CONSTRAINT FK_CarrierRouteRate_Uom FOREIGN KEY REFERENCES tblUom(ID)
, CreateDateUTC smalldatetime NOT NULL CONSTRAINT DF_CarrierRouteRate_CreateDateUTC DEFAULT (getutcdate())
, CreatedByUser varchar(100) NOT NULL
, LastChangeDateUTC smalldatetime NULL
, LastChangedByUser varchar(100) NULL
, CONSTRAINT CK_CarrierRouteRate_EndDate_Greater CHECK (EndDate >= EffectiveDate)
) 
GO
GRANT SELECT, INSERT, UPDATE ON tblCarrierRouteRate TO dispatchcrude_iis_acct
GO
CREATE UNIQUE CLUSTERED INDEX udxCarrierRateSheet_Main ON tblCarrierRouteRate(CarrierID, ProductGroupID, RouteID, EffectiveDate)
GO
CREATE INDEX idxCarrierRouteRate_Carrier ON tblCarrierRouteRate(CarrierID)
GO
CREATE INDEX idxCarrierRouteRate_ProductGroup ON tblCarrierRouteRate(ProductGroupID)
GO
CREATE INDEX idxCarrierRouteRate_Route ON tblCarrierRouteRate(RouteID)
GO

/***********************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
/***********************************************/
CREATE TRIGGER trigCarrierRouteRate_IU ON tblCarrierRouteRate AFTER INSERT, UPDATE AS
BEGIN
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblCarrierRouteRate X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND i.RouteID = X.RouteID
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		RAISERROR('Overlapping Route Rates are not allowed', 16, 1)
		-- if an explicit TRANSACTION isn't present, then ROLLBACK to ensure these changes cannot be committed
		IF (@@TRANCOUNT = 0) ROLLBACK
	END
END
GO

INSERT INTO tblCarrierRouteRate (CarrierID, RouteID, Rate, EffectiveDate, EndDate, CreateDateUTC, CreatedByUser, RateTypeID, UomID)
	select carrierid, routeid, rate, effectivedate
		, dateadd(day, -1, isnull((SELECT MIN(EffectiveDate) FROM tblcarrierrouterates X where X.ID <> i.ID AND X.carrierID = i.carrierid and X.routeid = i.routeid AND x.EffectiveDate > i.EffectiveDate), '2/28/2015'))
		, createdateutc, isnull(createdbyuser, 'system'), 1, uomid 
	from tblcarrierrouterates i
	order by routeid, carrierid, effectivedate
GO

CREATE TABLE tblCarrierRateSheet
(
  ID int identity(1, 1) NOT NULL CONSTRAINT PK_CarrierRateSheet PRIMARY KEY NONCLUSTERED
, CarrierID int NULL CONSTRAINT FK_CarrierRateSheet_Carrier FOREIGN KEY REFERENCES tblCarrier(ID)
, ProductGroupID int NULL CONSTRAINT FK_CarrierRateSheet_ProductGroup FOREIGN KEY REFERENCES tblProductGroup(ID)
, OriginStateID int NULL CONSTRAINT FK_CarrierRateSheet_OriginState FOREIGN KEY REFERENCES tblState(ID)
, DestStateID int NULL CONSTRAINT FK_CarrierRateSheet_DestState FOREIGN KEY REFERENCES tblState(ID)
, RegionID int NULL CONSTRAINT FK_CarrierRateSheet_Region FOREIGN KEY REFERENCES tblRegion(ID)
, EffectiveDate date NOT NULL
, EndDate date NOT NULL
, RateTypeID int NOT NULL CONSTRAINT FK_CarrierRateSheet_RateType FOREIGN KEY REFERENCES tblRateType(ID)
, UomID int NOT NULL CONSTRAINT FK_CarrierRateSheet_UomID FOREIGN KEY REFERENCES tblUom(ID)
, CreateDateUTC smalldatetime NOT NULL CONSTRAINT DF_CarrierRateSheet_CreateDateUTC DEFAULT (getutcdate())
, CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_CarrierRateSheet_CreatedByUser DEFAULT (SUSER_NAME())
, LastChangeDateUTC smalldatetime NULL
, LastChangedByUser varchar(100) NULL
, CONSTRAINT CK_CarrierRateSheet_EndDate_Greater CHECK (EndDate >= EffectiveDate)
)
GO
GRANT SELECT, INSERT, UPDATE ON tblCarrierRateSheet TO dispatchcrude_iis_acct
GO
CREATE UNIQUE CLUSTERED INDEX udxCarrierRateSheet_Main ON tblCarrierRateSheet(CarrierID, ProductGroupID, RegionID, OriginStateID, DestStateID, EffectiveDate)
GO
CREATE INDEX idxCarrierRateSheet_Carrier ON tblCarrierRateSheet(CarrierID)
GO
CREATE INDEX idxCarrierRateSheet_Region ON tblCarrierRateSheet(RegionID)
GO
CREATE INDEX idxCarrierRateSheet_OriginState ON tblCarrierRateSheet(OriginStateID)
GO
CREATE INDEX idxCarrierRateSheet_DestState ON tblCarrierRateSheet(DestStateID)
GO
CREATE INDEX idxCarrierRateSheet_ProductGroup ON tblCarrierRateSheet(ProductGroupID)
GO

/***********************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
/***********************************************/
CREATE TRIGGER trigCarrierRateSheet_IU ON tblCarrierRateSheet AFTER INSERT, UPDATE AS
BEGIN
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblCarrierRateSheet X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(i.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		RAISERROR('Overlapping Rate Sheets are not allowed', 16, 1)
		-- if an explicit TRANSACTION isn't present, then ROLLBACK to ensure these changes cannot be committed
		IF (@@TRANCOUNT = 0) ROLLBACK
	END
END
GO

CREATE TABLE tblCarrierRangeRate
(
  ID int identity(1, 1) NOT NULL CONSTRAINT PK_CarrierRangeRate PRIMARY KEY NONCLUSTERED
, RateSheetID int NOT NULL CONSTRAINT FK_CarrierRangeRate FOREIGN KEY REFERENCES tblCarrierRateSheet(ID) ON DELETE CASCADE
, MinRange int NOT NULL CONSTRAINT CK_CarrierRangeRate_MinRange_Positive CHECK (MinRange >= 0)
, MaxRange int NOT NULL 
, Rate decimal(18,10) NOT NULL
, CreateDateUTC smalldatetime NULL CONSTRAINT DF_CarrierRangeRate_CreateDateUTC DEFAULT (getutcdate())
, CreatedByUser varchar(100) NULL CONSTRAINT DF_CarrierRangeRate_CreatedByUser DEFAULT (SUSER_NAME())
, LastChangeDateUTC smalldatetime NULL
, LastChangedByUser varchar(100) NULL
, CONSTRAINT CK_CarrierRangeRate_MaxRange_Greater CHECK (MaxRange > MinRange)
)
GO
CREATE UNIQUE CLUSTERED INDEX udxCarrierRangeRate_Main ON tblCarrierRangeRate (RateSheetID, MinRange, MaxRange)
GO

/*****************************************************
-- Date Created: 15 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping Range Rates in the same Rate Sheet
*****************************************************/
CREATE TRIGGER trigCarrierRangeRate_IU ON tblCarrierRangeRate FOR INSERT, UPDATE AS
BEGIN
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblCarrierRangeRate X ON X.RateSheetID = i.RateSheetID AND i.ID <> X.ID 
		WHERE i.MinRange BETWEEN X.MinRange AND X.MaxRange 
			OR i.MaxRange BETWEEN X.MinRange AND X.MaxRange
			OR X.MinRange BETWEEN i.MinRange AND i.MaxRange)
	BEGIN
		RAISERROR('Overlapping Range Rates are not allowed', 16, 1)
		-- if an explicit TRANSACTION isn't present, then ROLLBACK to ensure these changes cannot be committed
		IF (@@TRANCOUNT = 0) ROLLBACK
	END
END
GO

CREATE TABLE tblCarrierAssessorialRate
(
  ID int identity(1, 1) NOT NULL CONSTRAINT PK_CarrierAssessorialRate PRIMARY KEY NONCLUSTERED
, TypeID int NOT NULL CONSTRAINT FK_CarrierAccessorialRate_Type FOREIGN KEY REFERENCES tblAssessorialRateType(ID)
, CarrierID int NULL CONSTRAINT FK_CarrierAssessorialRate_Carrier FOREIGN KEY REFERENCES tblCarrier(ID)
, ProductGroupID int NULL CONSTRAINT FK_CarrierAssessorialRate_ProductGroup FOREIGN KEY REFERENCES tblProductGroup(ID)
, OriginID int NULL CONSTRAINT FK_CarrierAssessorialRate_Origin FOREIGN KEY REFERENCES tblOrigin(ID)
, DestinationID int NULL CONSTRAINT FK_CarrierAssessorialRate_Destination FOREIGN KEY REFERENCES tblDestination(ID)
, OriginStateID int NULL CONSTRAINT FK_CarrierAssessorialRate_OriginState FOREIGN KEY REFERENCES tblState(ID)
, DestStateID int NULL CONSTRAINT FK_CarrierAssessorialRate_DestState FOREIGN KEY REFERENCES tblState(ID)
, RegionID int NULL CONSTRAINT FK_CarrierAssessorialRate_Region FOREIGN KEY REFERENCES tblRegion(ID)
, ProducerID int NULL CONSTRAINT FK_CarrierAssessorialRate_Producer FOREIGN KEY REFERENCES tblProducer(ID)
, OperatorID int NULL CONSTRAINT FK_CarrierAssessorialRate_Operator FOREIGN KEY REFERENCES tblOperator(ID)
, EffectiveDate date NOT NULL 
, EndDate date NOT NULL 
, Rate decimal(18,10) NOT NULL
, RateTypeID int NOT NULL CONSTRAINT FK_CarrierAccessorialRate_RateType FOREIGN KEY REFERENCES tblRateType(ID)
, UomID int NOT NULL CONSTRAINT FK_CarrierAccessorialRate_UomID FOREIGN KEY REFERENCES tblUom(ID)
, CreateDateUTC smalldatetime NOT NULL CONSTRAINT DF_CarrierAssessorialRate_CreateDateUTC DEFAULT (getutcdate())
, CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_CarrierAssessorialRate_CreatedByUser DEFAULT (SUSER_NAME())
, LastChangeDateUTC smalldatetime NULL
, LastChangedByUser varchar(100) NULL
, CONSTRAINT CK_CarrierRangeRate_EndDate_Greater CHECK (EndDate >= EffectiveDate)
)
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblCarrierAssessorialRate TO dispatchcrude_iis_acct
GO
CREATE UNIQUE CLUSTERED INDEX udxCarrierAssessorialRate_Main 
	ON tblCarrierAssessorialRate(TypeID, CarrierID, ProductGroupID, OriginID, DestinationID, OriginStateID, DestStateID, RegionID, ProducerID, OperatorID, EffectiveDate)
GO
CREATE INDEX idxCarrierAssessorialRate_Carrier ON tblCarrierAssessorialRate(CarrierID)
GO
CREATE INDEX idxCarrierAssessorialRate_Region ON tblCarrierAssessorialRate(RegionID)
GO
CREATE INDEX idxCarrierAssessorialRate_OriginState ON tblCarrierAssessorialRate(OriginStateID)
GO
CREATE INDEX idxCarrierAssessorialRate_DestState ON tblCarrierAssessorialRate(DestStateID)
GO
CREATE INDEX idxCarrierAssessorialRate_ProductGroup ON tblCarrierAssessorialRate(ProductGroupID)
GO
CREATE INDEX idxCarrierAssessorialRate_Producer ON tblCarrierAssessorialRate(ProducerID)
GO
CREATE INDEX idxCarrierAssessorialRate_Operator ON tblCarrierAssessorialRate(OperatorID)
GO
CREATE INDEX idxCarrierAssessorialRate_Origin ON tblCarrierAssessorialRate(OriginID)
GO
CREATE INDEX idxCarrierAssessorialRate_Destination ON tblCarrierAssessorialRate(DestinationID)
GO

/***********************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
/***********************************************/
CREATE TRIGGER trigCarrierAssessorialRate_IU ON tblCarrierAssessorialRate AFTER INSERT, UPDATE AS
BEGIN
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblCarrierAssessorialRate X 
			ON i.ID <> X.ID
			  AND i.TypeID = X.TypeID
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(i.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(i.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(i.ProducerID, X.ProducerID) = 1
			  AND dbo.fnCompareNullableInts(i.OperatorID, X.OperatorID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		RAISERROR('Overlapping Assessorial Rates are not allowed', 16, 1)
		-- if an explicit TRANSACTION isn't present, then ROLLBACK to ensure these changes cannot be committed
		IF (@@TRANCOUNT = 0) ROLLBACK
	END
END
GO

insert into tblCarrierAssessorialRate (TypeID, CarrierID, RegionID, EffectiveDate, EndDate, Rate, RateTypeID, UomID)
		  select distinct 1, nullif(carrierid, -1), nullif(regionid, -1), '1/1/2013', '2/28/2015', ChainupFee, 2, uomid from tblCarrierRates
	union select distinct 2, nullif(carrierid, -1), nullif(regionid, -1), '1/1/2013', '2/28/2015', RerouteFee, 2, uomid from tblCarrierRates
	union select distinct 3, nullif(carrierid, -1), nullif(regionid, -1), '1/1/2013', '2/28/2015', SplitLoadRate, 2, uomid from tblCarrierRates
	union select distinct 4, nullif(carrierid, -1), nullif(regionid, -1), '1/1/2013', '2/28/2015', H2SRate, 1, uomid from tblCarrierRates
go

-- DestinationWaitRate changes
CREATE TABLE tblCarrierDestinationWaitRate
(
  ID int IDENTITY(1,1) NOT NULL CONSTRAINT PK_CarrierDestinationWaitRate PRIMARY KEY CLUSTERED 
, ReasonID int NULL CONSTRAINT FK_CarrierDestinationWaitRate_Reason FOREIGN KEY REFERENCES tblDestinationWaitReason(ID)
, CarrierID int NULL CONSTRAINT FK_CarrierDestinationWaitRate_Carrier FOREIGN KEY REFERENCES tblCarrier(ID)
, ProductGroupID int NULL CONSTRAINT FK_CarrierDestinationWaitRate_ProductGroup FOREIGN KEY REFERENCES tblProductGroup(ID)
, DestinationID int NULL CONSTRAINT FK_CarrierDestinationWaitRate_Destination FOREIGN KEY REFERENCES tblDestination(ID)
, StateID int NULL CONSTRAINT FK_CarrierDestinationWaitRate_State FOREIGN KEY REFERENCES tblState(ID)
, RegionID int NULL CONSTRAINT FK_CarrierDestinationWaitRate_Region FOREIGN KEY REFERENCES tblRegion(ID)
, EffectiveDate date NOT NULL
, EndDate date NOT NULL
, Rate decimal(18, 10) NOT NULL
, CreateDateUTC datetime NOT NULL CONSTRAINT DF_CarrierDestinationWaitRate_CreateDateUTC DEFAULT (getutcdate())
, CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_CarrierDestinationWaitRate_CreatedByUser DEFAULT (suser_name())
, LastChangeDateUTC datetime NULL
, LastChangedByUser varchar(100) NULL
, CONSTRAINT CK_CarrierDestinationWaitRate_EndDate_Greater CHECK (EndDate >= EffectiveDate)
) 
GO 
GRANT SELECT, INSERT, UPDATE, DELETE ON tblCarrierDestinationWaitRate TO dispatchcrude_iis_acct
GO
CREATE UNIQUE INDEX udxCarrierDestinationWaitRate_Main ON tblCarrierDestinationWaitRate(ReasonID, CarrierID, ProductGroupID, DestinationID, StateID, RegionID, EffectiveDate)
GO
CREATE INDEX idxCarrierDestinationWaitRate_Reason ON tblCarrierDestinationWaitRate(ReasonID)
GO
CREATE INDEX idxCarrierDestinationWaitRate_Carrier ON tblCarrierDestinationWaitRate(CarrierID)
GO
CREATE INDEX idxCarrierDestinationWaitRate_ProductGroup ON tblCarrierDestinationWaitRate(ProductGroupID)
GO
CREATE INDEX idxCarrierDestinationWaitRate_Destination ON tblCarrierDestinationWaitRate(DestinationID)
GO
CREATE INDEX idxCarrierDestinationWaitRate_State ON tblCarrierDestinationWaitRate(StateID)
GO
CREATE INDEX idxCarrierDestinationWaitRate_Region ON tblCarrierDestinationWaitRate(RegionID)
GO

/***********************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
/***********************************************/
CREATE TRIGGER trigCarrierDestinationWaitRate_IU ON tblCarrierDestinationWaitRate AFTER INSERT, UPDATE AS
BEGIN
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblCarrierDestinationWaitRate X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ReasonID, X.ReasonID) = 1
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(i.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		RAISERROR('Overlapping Wait Rates are not allowed', 16, 1)
		-- if an explicit TRANSACTION isn't present, then ROLLBACK to ensure these changes cannot be committed
		IF (@@TRANCOUNT = 0) ROLLBACK
	END
END
GO
 
INSERT INTO tblCarrierDestinationWaitRate (ReasonID, CarrierID, EffectiveDate, EndDate, Rate, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser)
	select nullif(destinationwaitreasonid, -1), nullif(carrierid, -1), effectivedate
		, dateadd(day, -1, isnull((SELECT MIN(effectivedate) FROM tblcarrierdestinationwaitrates X where X.ID <> i.ID AND X.carrierID = i.carrierid and X.destinationwaitreasonid = i.destinationwaitreasonid AND x.EffectiveDate > i.effectivedate), '2/28/2015'))
		, rate, createdateutc, createdbyuser, lastchangedateutc, lastchangedbyuser
	from tblcarrierdestinationwaitrates i
GO
INSERT INTO tblCarrierDestinationWaitRate (ReasonID, CarrierID, RegionID, EffectiveDate, EndDate, Rate, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser)
	select null, nullif(i.carrierid, -1), nullif(i.regionid, -1), '1/1/2015'
		, '2/28/2015'
		, waitfee, i.createdateutc, i.createdbyuser, i.lastchangedateutc, i.lastchangedbyuser
	from tblcarrierrates i
	left join tblCarrierDestinationWaitRate X ON isnull(X.CarrierID, 0) = isnull(i.carrierid, 0) 
		and isnull(X.RegionID, 0) = isnull(i.regionid, 0) 
		and X.ReasonID IS NULL
	where X.ID IS NULL
GO

-- OriginWaitRate changes
CREATE TABLE tblCarrierOriginWaitRate
(
  ID int IDENTITY(1,1) NOT NULL CONSTRAINT PK_CarrierOriginWaitRate PRIMARY KEY CLUSTERED 
, ReasonID int NULL CONSTRAINT FK_CarrierOriginWaitRate_Reason FOREIGN KEY REFERENCES tblOriginWaitReason(ID)
, CarrierID int NULL CONSTRAINT FK_CarrierOriginWaitRate_Carrier FOREIGN KEY REFERENCES tblCarrier(ID)
, ProductGroupID int NULL CONSTRAINT FK_CarrierOriginWaitRate_ProductGroup FOREIGN KEY REFERENCES tblProductGroup(ID)
, OriginID int NULL CONSTRAINT FK_CarrierOriginWaitRate_Origin FOREIGN KEY REFERENCES tblOrigin(ID)
, StateID int NULL CONSTRAINT FK_CarrierOriginWaitRate_State FOREIGN KEY REFERENCES tblState(ID)
, RegionID int NULL CONSTRAINT FK_CarrierOriginWaitRate_Region FOREIGN KEY REFERENCES tblRegion(ID)
, EffectiveDate date NOT NULL
, EndDate date NOT NULL
, Rate decimal(18, 10) NOT NULL
, CreateDateUTC datetime NOT NULL CONSTRAINT DF_CarrierOriginWaitRate_CreateDateUTC DEFAULT (getutcdate())
, CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_CarrierOriginWaitRate_CreatedByUser DEFAULT (suser_name())
, LastChangeDateUTC datetime NULL
, LastChangedByUser varchar(100) NULL
, CONSTRAINT CK_CarrierOriginWaitRate_EndDate_Greater CHECK (EndDate >= EffectiveDate)
) 
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblCarrierOriginWaitRate TO dispatchcrude_iis_acct
GO
CREATE UNIQUE INDEX udxCarrierOriginWaitRate_Main ON tblCarrierOriginWaitRate(ReasonID, CarrierID, ProductGroupID, OriginID, StateID, RegionID, EffectiveDate)
GO
CREATE INDEX idxCarrierOriginWaitRate_Reason ON tblCarrierOriginWaitRate(ReasonID)
GO
CREATE INDEX idxCarrierOriginWaitRate_Carrier ON tblCarrierOriginWaitRate(CarrierID)
GO
CREATE INDEX idxCarrierOriginWaitRate_ProductGroup ON tblCarrierOriginWaitRate(ProductGroupID)
GO
CREATE INDEX idxCarrierOriginWaitRate_Origin ON tblCarrierOriginWaitRate(OriginID)
GO
CREATE INDEX idxCarrierOriginWaitRate_State ON tblCarrierOriginWaitRate(StateID)
GO
CREATE INDEX idxCarrierOriginWaitRate_Region ON tblCarrierOriginWaitRate(RegionID)
GO

/***********************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
/***********************************************/
CREATE TRIGGER trigCarrierOriginWaitRate_IU ON tblCarrierOriginWaitRate AFTER INSERT, UPDATE AS
BEGIN
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblCarrierOriginWaitRate X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ReasonID, X.ReasonID) = 1
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(i.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		RAISERROR('Overlapping Wait Rates are not allowed', 16, 1)
		-- if an explicit TRANSACTION isn't present, then ROLLBACK to ensure these changes cannot be committed
		IF (@@TRANCOUNT = 0) ROLLBACK
	END
END
GO

INSERT INTO tblCarrierOriginWaitRate (ReasonID, CarrierID, EffectiveDate, EndDate, Rate, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser)
	select nullif(originwaitreasonid, -1), nullif(carrierid, -1), effectivedate
		, dateadd(day, -1, isnull((SELECT MIN(effectivedate) FROM tblcarrieroriginwaitrates X where X.ID <> i.id AND X.CarrierID = i.carrierid and X.originwaitreasonid = i.originwaitreasonid AND x.EffectiveDate > i.effectivedate), '2/28/2015'))
		, rate, createdateutc, createdbyuser, lastchangedateutc, lastchangedbyuser
	from tblcarrieroriginwaitrates i
GO
INSERT INTO tblCarrierOriginWaitRate (ReasonID, CarrierID, RegionID, EffectiveDate, EndDate, Rate, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser)
	select null, nullif(i.carrierid, -1), nullif(i.regionid, -1), '1/1/2015'
		, '2/28/2015'
		, waitfee, i.createdateutc, i.createdbyuser, i.lastchangedateutc, i.lastchangedbyuser
	from tblcarrierrates i
	left join tblCarrierOriginWaitRate X ON isnull(X.CarrierID, 0) = isnull(i.carrierid, 0) 
		and isnull(X.RegionID, 0) = isnull(i.regionid, 0) 
		and X.ReasonID IS NULL
	where X.ID IS NULL
GO

CREATE TABLE tblCarrierOrderRejectRate
(
  ID int IDENTITY(1,1) NOT NULL CONSTRAINT PK_CarrierOrderRejectRate PRIMARY KEY CLUSTERED 
, ReasonID int NULL CONSTRAINT FK_CarrierOrderRejectRate_Reason FOREIGN KEY REFERENCES tblOrderRejectReason(ID)
, CarrierID int NULL CONSTRAINT FK_CarrierOrderRejectRate_Carrier FOREIGN KEY REFERENCES tblCarrier(ID)
, ProductGroupID int NULL CONSTRAINT FK_CarrierOrderRejectRate_ProductGroup FOREIGN KEY REFERENCES tblProductGroup(ID)
, OriginID int NULL CONSTRAINT FK_CarrierOrderRejectRate_Origin FOREIGN KEY REFERENCES tblOrigin(ID)
, StateID int NULL CONSTRAINT FK_CarrierOrderRejectRate_State FOREIGN KEY REFERENCES tblState(ID)
, RegionID int NULL CONSTRAINT FK_CarrierOrderRejectRate_Region FOREIGN KEY REFERENCES tblRegion(ID)
, EffectiveDate date NOT NULL
, EndDate date NOT NULL
, Rate decimal(18, 10) NOT NULL
, RateTypeID int NOT NULL CONSTRAINT FK_CarrierOrderRejectRate_RateType FOREIGN KEY REFERENCES tblRateType(ID)
, UomID int NOT NULL CONSTRAINT FK_CarrierOrderRejectRate_Uom FOREIGN KEY REFERENCES tblUom(ID)
, CreateDateUTC datetime NOT NULL CONSTRAINT DF_CarrierOrderRejectRate_CreateDateUTC DEFAULT (getutcdate())
, CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_CarrierOrderRejectRate_CreatedByUser DEFAULT (suser_name())
, LastChangeDateUTC datetime NULL
, LastChangedByUser varchar(100) NULL
) 
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblCarrierOrderRejectRate TO dispatchcrude_iis_acct
GO
CREATE UNIQUE INDEX udxCarrierOrderRejectRate_Main ON tblCarrierOrderRejectRate(ReasonID, CarrierID, ProductGroupID, OriginID, StateID, RegionID, EffectiveDate)
GO
CREATE INDEX idxCarrierOrderRejectRate_Reason ON tblCarrierOrderRejectRate(ReasonID)
GO
CREATE INDEX idxCarrierOrderRejectRate_Carrier ON tblCarrierOrderRejectRate(CarrierID)
GO
CREATE INDEX idxCarrierOrderRejectRate_ProductGroup ON tblCarrierOrderRejectRate(ProductGroupID)
GO
CREATE INDEX idxCarrierOrderRejectRate_Origin ON tblCarrierOrderRejectRate(OriginID)
GO
CREATE INDEX idxCarrierOrderRejectRate_State ON tblCarrierOrderRejectRate(StateID)
GO
CREATE INDEX idxCarrierOrderRejectRate_Region ON tblCarrierOrderRejectRate(RegionID)
GO

/***********************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
/***********************************************/
CREATE TRIGGER trigCarrierOrderRejectRate_IU ON tblCarrierOrderRejectRate AFTER INSERT, UPDATE AS
BEGIN
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblCarrierOrderRejectRate X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ReasonID, X.ReasonID) = 1
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(i.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		RAISERROR('Overlapping Reject Rates are not allowed', 16, 1)
		-- if an explicit TRANSACTION isn't present, then ROLLBACK to ensure these changes cannot be committed
		IF (@@TRANCOUNT = 0) ROLLBACK
	END
END
GO

INSERT INTO tblCarrierOrderRejectRate (ReasonID, CarrierID, EffectiveDate, EndDate, Rate, RateTypeID, UomID, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser)
	select nullif(OrderRejectReasonID, -1), nullif(CarrierID, -1), EffectiveDate
		, dateadd(day, -1, isnull((SELECT MIN(EffectiveDate) FROM tblcarrierorderrejectrates X where X.ID <> i.ID AND X.carrierID = i.carrierid and X.orderrejectreasonid = i.orderrejectreasonid AND x.EffectiveDate > i.EffectiveDate), '2/28/2015'))
		, Rate, 1, 1, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser
	from tblcarrierorderrejectrates i
GO
INSERT INTO tblCarrierOrderRejectRate (ReasonID, CarrierID, RegionID, EffectiveDate, EndDate, Rate, RateTypeID, UomID, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser)
	select null, nullif(i.carrierid, -1), nullif(i.regionid, -1), '1/1/2015'
		, '2/28/2015'
		, rejectionfee, 1, i.uomid
		, i.createdateutc, i.createdbyuser, i.lastchangedateutc, i.lastchangedbyuser
	from tblcarrierrates i
	left join tblCarrierOrderRejectRate X ON isnull(X.CarrierID, 0) = isnull(i.carrierid, 0) 
		and isnull(X.RegionID, 0) = isnull(i.regionid, 0) 
		and X.ReasonID IS NULL
	where X.ID IS NULL
GO

-- OrderSettlementCarrier (was OrderInvoiceCarrier)
CREATE TABLE tblOrderSettlementCarrier
(
  OrderID int NOT NULL CONSTRAINT PK_OrderSettlementCarrier PRIMARY KEY CONSTRAINT FK_OrderSettlementCarrier_Order FOREIGN KEY REFERENCES tblOrder(ID)
, OrderDate date NOT NULL
, BatchID int NULL CONSTRAINT FK_OrderSettlementCarrier_Batch FOREIGN KEY REFERENCES tblCarrierSettlementBatch(ID)

, SettlementFactorID int NOT NULL CONSTRAINT FK_OrderSettlementCarrier_SettlementFactor FOREIGN KEY REFERENCES tblSettlementFactor(ID)
, SettlementUomID int NOT NULL CONSTRAINT FK_OrderSettlementCarrier_SettlementUom FOREIGN KEY REFERENCES tblUom(ID)
, MinSettlementUnits int NOT NULL
, SettlementUnits decimal(18, 10) NOT NULL

, RouteRateID int NULL CONSTRAINT FK_OrderSettlmentCarrier_RouteRate FOREIGN KEY REFERENCES tblCarrierRouteRate(ID) ON DELETE SET NULL
, RangeRateID int NULL CONSTRAINT FK_OrderSettlementCarrier_RangeRate FOREIGN KEY REFERENCES tblCarrierRangeRate(ID) ON DELETE SET NULL
, LoadAmount money NULL

, WaitFeeParameterID int NULL CONSTRAINT FK_OrderSettlementCarrier_WaitFeeParameter FOREIGN KEY REFERENCES tblCarrierWaitFeeParameter(ID) ON DELETE SET NULL
, OriginWaitRateID int NULL CONSTRAINT FK_OrderSettlementCarrier_OriginWaitRate FOREIGN KEY REFERENCES tblCarrierOriginWaitRate(ID) ON DELETE SET NULL
, OriginWaitBillableMinutes int NULL
, OriginWaitBillableHours money NULL
, OriginWaitAmount money NULL
, DestinationWaitRateID int NULL CONSTRAINT FK_OrderSettlementCarrier_DestinationWaitRate FOREIGN KEY REFERENCES tblCarrierDestinationWaitRate(ID) ON DELETE SET NULL
, DestinationWaitBillableMinutes int NULL
, DestinationWaitBillableHours money NULL
, DestinationWaitAmount money NULL
, OrderRejectRateID int NULL CONSTRAINT FK_OrderSettlementCarrier_OrderRejectRate FOREIGN KEY REFERENCES tblCarrierOrderRejectRate(ID) ON DELETE SET NULL
, OrderRejectAmount money NULL

, FuelSurchargeRateID int NULL CONSTRAINT FK_OrderSettlementCarrier_FuelSurchargeRate FOREIGN KEY REFERENCES tblCarrierFuelSurchargeRates(ID) ON DELETE SET NULL
, FuelSurchargeRate money NULL
, FuelSurchargeAmount money NULL

, OriginTaxRate money NULL

, TotalAmount money NOT NULL

, CreateDateUTC datetime NOT NULL CONSTRAINT DF_OrderSettlementCarrier_CreateDateUTC DEFAULT (getutcdate())
, CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_OrderSettlementCarrier_CreatedByUser DEFAULT (suser_name())
, CONSTRAINT CK_OrderSettlementCarrier_RouteOrRangeRateNull CHECK (RouteRateID IS NULL OR RangeRateID IS NULL)
)
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblOrderSettlementCarrier TO dispatchcrude_iis_acct
GO
CREATE NONCLUSTERED INDEX idxOrderSettlementCarrier_Covering1 ON tblOrderSettlementCarrier (WaitFeeParameterID, BatchID)
GO

INSERT INTO tblOrderSettlementCarrier
	SELECT OrderID, OrderDate, BatchID
		, isnull(OIC.SettlementFactorID, C.SettlementFactorID), OIC.UomID, coalesce(OIC.MinSettlementUnits, C.MinSettlementUnits, 0), isnull(Units, 0)
		, NULL, NULL, OIC.LoadFee
		, coalesce(WFP0.ID, WFP1.ID, WFP2.ID)
		, NULL, BillableOriginWaitMinutes, BillableOriginWaitMinutes / 60.0, OriginWaitFee
		, NULL, BillableDestWaitMinutes, BillableDestWaitMinutes / 60.0, DestWaitFee
		, NULL, RejectionFee
		, NULL, FuelSurchargeRate, FuelSurchargeFee
		, TaxRate
		, TotalFee
		, OIC.CreateDateUTC, OIC.CreatedByUser
	FROM viewOrder O
	JOIN tblCarrier C ON C.ID = O.CarrierID
	JOIN tblOrderInvoiceCarrier OIC ON OIC.OrderID = O.ID
	LEFT JOIN tblCarrierWaitFeeParameter WFP0 ON WFP0.CarrierID = O.CarrierID AND WFP0.RegionID = O.OriginRegionID
	LEFT JOIN tblCarrierWaitFeeParameter WFP1 ON WFP1.CarrierID = O.CarrierID AND WFP1.RegionID IS NULL
	LEFT JOIN tblCarrierWaitFeeParameter WFP2 ON WFP2.CarrierID IS NULL AND WFP2.RegionID IS NULL
	LEFT JOIN viewCarrierRouteRates CRR ON CRR.CarrierID = O.CarrierID AND CRR.RouteID = O.RouteID AND O.OrderDate BETWEEN CRR.EffectiveDate AND ISNULL(CRR.EndDate, '1/1/2015')
GO

CREATE TABLE tblOrderSettlementCarrierAssessorialCharge
(
  ID int NOT NULL identity(1, 1) CONSTRAINT PK_OrderSettlementCarrierOtherAssessorialCharge PRIMARY KEY NONCLUSTERED
, OrderID int NOT NULL CONSTRAINT FK_OrderSettlementCarrierOtherAssessorialCharge_Settlement FOREIGN KEY REFERENCES tblOrderSettlementCarrier(OrderID) ON DELETE CASCADE
, AssessorialRateTypeID int NOT NULL CONSTRAINT FK_OrderSettlementCarrierOtherAssessorialCharge_RateType FOREIGN KEY REFERENCES tblAssessorialRateType(ID)
, AssessorialRateID int NULL CONSTRAINT FK_OrderSettlementCarrierOtherAssessorialCharge_AssessorialRate FOREIGN KEY REFERENCES tblCarrierAssessorialRate(ID)
, Amount money NOT NULL
, CreateDateUTC datetime NOT NULL CONSTRAINT DF_OrderSettlmentCarrierOtherAssessorialCharge_CreateDateUTC DEFAULT (getutcdate())
, CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_OrderSettlmentCarrierOtherAssessorialCharge_CreatedByUser DEFAULT (suser_name())
, LastChangeDateUTC datetime NULL
, LastChangedByUser varchar(100) NULL
)
GRANT SELECT, INSERT, UPDATE, DELETE ON tblOrderSettlementCarrierAssessorialCharge TO dispatchcrude_iis_acct
GO
CREATE UNIQUE CLUSTERED INDEX udxOrderSettlementCarrierAssessorialCharge_Main ON tblOrderSettlementCarrierAssessorialCharge(OrderID, AssessorialRateTypeID)
GO

INSERT INTO tblOrderSettlementCarrierAssessorialCharge (OrderID, AssessorialRateTypeID, Amount, CreateDateUTC, CreatedByUser)
	SELECT OrderID
		, 1
		, ChainupFee
		, CreateDateUTC, CreatedByUser
	FROM tblOrderInvoiceCarrier OIC
	WHERE isnull(ChainupFee, 0) <> 0
	UNION SELECT OrderID
		, 2
		, RerouteFee
		, CreateDateUTC, CreatedByUser
	FROM tblOrderInvoiceCarrier OIC
	WHERE isnull(RerouteFee, 0) <> 0
	UNION SELECT OrderID
		, 3
		, SplitLoadFee
		, CreateDateUTC, CreatedByUser
	FROM tblOrderInvoiceCarrier OIC
	WHERE isnull(SplitLoadFee, 0) <> 0
	UNION SELECT OrderID
		, 4
		, H2SFee
		, CreateDateUTC, CreatedByUser
	FROM tblOrderInvoiceCarrier OIC
	WHERE isnull(H2SFee, 0) <> 0
GO

IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'viewCarrierRateSheet'))
	DROP VIEW viewCarrierRateSheet
GO
/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all CarrierRateSheet records
******************************************************/
CREATE VIEW viewCarrierRateSheet AS
	SELECT X.*
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementCarrier SC JOIN tblCarrierRangeRate RR ON RR.ID = SC.RangeRateID WHERE RR.RateSheetID = X.ID AND SC.BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblCarrierRateSheet XN 
			WHERE isnull(XN.CarrierID, 0) = isnull(X.CarrierID, 0) 
			  AND isnull(XN.ProductGroupID, 0) = isnull(X.ProductGroupID, 0) 
			  AND isnull(XN.OriginStateID, 0) = isnull(X.OriginStateID, 0) 
			  AND isnull(XN.DestStateID, 0) = isnull(X.DestStateID, 0) 
			  AND isnull(XN.RegionID, 0) = isnull(X.RegionID, 0) 
			  AND XN.EffectiveDate > X.EffectiveDate
		)
	FROM tblCarrierRateSheet X

GO
GRANT SELECT ON viewCarrierRateSheet TO dispatchcrude_iis_acct
GO

/************************************************
-- Date Created: 18 Jan 2015
-- Author: Kevin Alons
-- Purpose: return combined RateSheet + RangeRate data + friendly translated values
			allow updating of both RateSheet & RangeRate data together
************************************************/
CREATE VIEW viewCarrierRateSheetRangeRate AS
	SELECT RR.ID, RateSheetID = R.ID, R.CarrierID, R.ProductGroupID, R.OriginStateID, R.DestStateID, R.RegionID, RR.Rate, RR.MinRange, RR.MaxRange, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate, R.NextEffectiveDate
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, OriginState = OS.FullName
		, OriginStateAbbrev = OS.Abbreviation
		, DestinationState = DS.FullName
		, DestinationStateAbbrev = DS.Abbreviation
		, Region = REG.Name
		, RateType = RT.Name
		, Uom = U.Name
		, UomShort = U.Abbrev
		, Locked
		, RR.CreateDateUTC, RR.CreatedByUser
		, RR.LastChangeDateUTC, RR.LastChangedByUser
	FROM dbo.viewCarrierRateSheet R
	JOIN dbo.tblCarrierRangeRate RR ON RR.RateSheetID = R.ID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblState OS ON OS.ID = R.OriginStateID
	LEFT JOIN tblState DS ON DS.ID = R.DestStateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblRateType RT ON RT.ID = R.RateTypeID
	LEFT JOIN tblUom U ON U.ID = R.UomID
GO
GRANT SELECT, INSERT, INSERT, DELETE ON viewCarrierRateSheetRangeRate TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details + FINANCIAL INFO into a single view
/***********************************/
CREATE VIEW [dbo].[viewOrderSettlementCarrier] AS 
	SELECT OSC.*
		, BatchNum = SB.BatchNum
		, SettlementUom = UOM.Name
		, SettlementUomShort = UOM.Abbrev
		, RouteRate = RR.Rate
		, RouteRateType = RRT.Name
		, RateSheetRate = RSRR.Rate
		, RateSheetRateType = RSRT.Name
		, OrderRejectRate = ORR.Rate
		, OrderRejectRateType = ORRT.Name
		, OriginWaitRate = OWR.Rate
		, DestinationWaitRate = DWR.Rate
		, TotalWaitAmount = cast(OSC.OriginWaitAmount as money) + cast(OSC.DestinationWaitAmount as money)
		, ChainupRate = CAR.Rate
		, ChainupRateType = CART.Name
		, ChainupAmount = CA.Amount
		, RerouteRate = RAR.Rate
		, RerouteRateType = RART.Name
		, RerouteAmount = RA.Amount
		, SplitLoadRate = SAR.Rate
		, SplitLoadRateType = SART.Name
		, SplitLoadAmount = SA.Amount
		, H2SRate = HAR.Rate
		, H2SRateType = HART.Name
		, H2SAmount = HA.Amount
		, OtherAmount = OA.Amount
		, WaitFeeSubUnit = isnull(WFSU.Name, 'None') 
		, WaitFeeRoundingType = isnull(WFRT.Name, 'None') 
	FROM tblOrderSettlementCarrier OSC 
	LEFT JOIN tblCarrierSettlementBatch SB ON SB.ID = OSC.BatchID
	LEFT JOIN tblUom UOM ON UOM.ID = OSC.SettlementUomID
	LEFT JOIN tblCarrierOriginWaitRate OWR ON OWR.ID = OSC.OriginWaitRateID
	LEFT JOIN tblCarrierDestinationWaitRate DWR ON DWR.ID = OSC.DestinationWaitRateID
	LEFT JOIN tblCarrierOrderRejectRate ORR ON ORR.ID = OSC.OrderRejectRateID
	LEFT JOIN tblRateType ORRT ON ORRT.ID = ORR.RateTypeID
	-- route rate/ratesheet JOINs
	LEFT JOIN tblCarrierRouteRate RR ON RR.ID = OSC.RouteRateID
	LEFT JOIN tblRateType RRT ON RRT.ID = RR.RateTypeID
	LEFT JOIN viewCarrierRateSheetRangeRate RSRR ON RSRR.ID = OSC.RangeRateID
	LEFT JOIN tblRateType RSRT ON RSRT.ID = RSRR.RateTypeID
	-- chainup
	LEFT JOIN tblOrderSettlementCarrierAssessorialCharge CA ON CA.OrderID = OSC.OrderID AND CA.AssessorialRateTypeID = 1
	LEFT JOIN tblCarrierAssessorialRate CAR ON CAR.ID = CA.AssessorialRateID
	LEFT JOIN tblRateType CART ON CART.ID = CAR.RateTypeID
	-- Reroute
	LEFT JOIN tblOrderSettlementCarrierAssessorialCharge RA ON RA.OrderID = OSC.OrderID AND RA.AssessorialRateTypeID = 2
	LEFT JOIN tblCarrierAssessorialRate RAR ON RAR.ID = RA.AssessorialRateID
	LEFT JOIN tblRateType RART ON RART.ID = RAR.RateTypeID
	-- Split Load
	LEFT JOIN tblOrderSettlementCarrierAssessorialCharge SA ON SA.OrderID = OSC.OrderID AND SA.AssessorialRateTypeID = 3
	LEFT JOIN tblCarrierAssessorialRate SAR ON SAR.ID = SA.AssessorialRateID
	LEFT JOIN tblRateType SART ON SART.ID = SAR.RateTypeID
	-- H2S
	LEFT JOIN tblOrderSettlementCarrierAssessorialCharge HA ON HA.OrderID = OSC.OrderID AND HA.AssessorialRateTypeID = 4
	LEFT JOIN tblCarrierAssessorialRate HAR ON HAR.ID = HA.AssessorialRateID
	LEFT JOIN tblRateType HART ON HART.ID = HAR.RateTypeID
	-- Other Assessorial Charges (combined)
	LEFT JOIN (
		SELECT OrderID, Amount = SUM(Amount) FROM tblOrderSettlementCarrierAssessorialCharge WHERE AssessorialRateTypeID NOT IN (1,2,3,4) GROUP BY OrderID
	) OA ON OA.OrderID = OSC.OrderID 
	
	LEFT JOIN tblCarrierWaitFeeParameter WFP ON WFP.ID = OSC.WaitFeeParameterID
	LEFT JOIN dbo.tblWaitFeeSubUnit WFSU ON WFSU.ID = WFP.SubUnitID
	LEFT JOIN dbo.tblWaitFeeRoundingType WFRT ON WFRT.ID = WFP.RoundingTypeID
GO
GRANT SELECT ON viewOrderSettlementCarrier TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return OrderSettlementCarrierAssessorialCharge records with BatchId included (to determine whether actually "Settled" or not)
/***********************************/
CREATE VIEW viewOrderSettlementCarrierAssessorialCharge AS
	SELECT AC.*
		 , SC.BatchID
	FROM tblOrderSettlementCarrierAssessorialCharge AC
	JOIN tblOrderSettlementCarrier SC ON SC.OrderID = AC.OrderID
GO
GRANT SELECT ON viewOrderSettlementCarrierAssessorialCharge TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'viewCarrierAssessorialRate'))
	DROP VIEW viewCarrierAssessorialRate
GO
/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all CarrierAssessorialRate records
******************************************************/
CREATE VIEW viewCarrierAssessorialRate AS
	SELECT X.*
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM viewOrderSettlementCarrierAssessorialCharge WHERE AssessorialRateID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblCarrierAssessorialRate XN 
			WHERE XN.TypeID = X.TypeID
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND dbo.fnCompareNullableInts(XN.OperatorID, X.OperatorID) = 1
			  AND XN.EffectiveDate > X.EffectiveDate)
	FROM tblCarrierAssessorialRate X
GO
GRANT SELECT ON viewCarrierAssessorialRate TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'viewCarrierRouteRate'))
	DROP VIEW viewCarrierRouteRate
GO
/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all CarrierRouteRate records
******************************************************/
CREATE VIEW viewCarrierRouteRate AS
	SELECT X.*
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementCarrier WHERE RouteRateID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, R.OriginID
		, R.DestinationID
		, R.ActualMiles
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblCarrierRouteRate XN 
			WHERE dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND XN.RouteID = X.RouteID
			  AND XN.EffectiveDate > X.EffectiveDate)
	FROM tblCarrierRouteRate X
	JOIN tblRoute R ON R.ID = X.RouteID
GO
GRANT SELECT ON viewCarrierRouteRate TO dispatchcrude_iis_acct
GO

DROP PROCEDURE spGetCarrierRouteRates
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'fnCarrierRouteRate') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION fnCarrierRouteRate
GO 
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RouteRate info for the specified criteria
/***********************************/
CREATE FUNCTION fnCarrierRouteRate(@Date date, @RouteID int, @CarrierID int, @ProductGroupID int, @exactMatch bit = 0) RETURNS TABLE AS RETURN
	SELECT ID, RouteID, CarrierID, ProductGroupID, Rate, RateTypeID, UomID, EffectiveDate, EndDate, NextEffectiveDate
	FROM (
		SELECT TOP 1 ID, RouteID, CarrierID, ProductGroupID, Rate, RateTypeID, UomID, EffectiveDate, EndDate, NextEffectiveDate
			, Ranking =	  CASE WHEN CarrierID IS NULL THEN 0 ELSE 2 END 
						+ CASE WHEN ProductGroupID IS NULL THEN 0 ELSE 1 END
		FROM dbo.viewCarrierRouteRate 
		WHERE coalesce(RouteID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @RouteID END, 0) = isnull(@RouteID, 0)
		  AND coalesce(CarrierID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @CarrierID END, 0) = isnull(@CarrierID, 0)
		  AND coalesce(ProductGroupID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @ProductGroupID END, 0) = isnull(@ProductGroupID, 0)
		  AND @Date BETWEEN EffectiveDate AND EndDate
		ORDER BY Ranking DESC
	) X 

GO
GRANT SELECT ON fnCarrierRouteRate TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fnOrderCarrierRouteRate]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION [dbo].[fnOrderCarrierRouteRate]
GO
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RouteRate info for the specified order
/***********************************/
CREATE FUNCTION fnOrderCarrierRouteRate(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, RateTypeID, Rate, R.UomID
	FROM dbo.viewOrder O
	CROSS APPLY dbo.fnCarrierRouteRate(O.OrderDate, O.RouteID, O.CarrierID, O.ProductGroupID, 0) R
	WHERE O.ID = @ID 

GO
GRANT SELECT ON fnOrderCarrierRouteRate TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'fnCarrierRateSheetRangeRate') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION fnCarrierRateSheetRangeRate
GO
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RateSheetRangeRate info for the specified criteria
/***********************************/
CREATE FUNCTION fnCarrierRateSheetRangeRate(@date date, @RouteMiles int, @CarrierID int, @ProductGroupID int, @OriginStateID int, @DestStateID int, @RegionID int, @exactMatch bit = 0)
RETURNS TABLE AS RETURN
	SELECT ID, RateSheetID, CarrierID, ProductGroupID, OriginStateID, DestStateID, RegionID, Rate, MinRange, MaxRange, RateTypeID, UomID, EffectiveDate, EndDate, NextEffectiveDate
	FROM (
		SELECT TOP 1 RR.ID, RateSheetID = RS.ID, CarrierID, ProductGroupID, OriginStateID, DestStateID, RegionID, Rate, MinRange, MaxRange, RateTypeID, UomID, EffectiveDate, EndDate, NextEffectiveDate
			, Ranking =	  CASE WHEN CarrierID IS NULL THEN 0 ELSE 16 END
						+ CASE WHEN ProductGroupID IS NULL THEN 0 ELSE 8 END
						+ CASE WHEN OriginStateID IS NULL THEN 0 ELSE 4 END
						+ CASE WHEN DestStateID IS NULL THEN 0 ELSE 2 END
						+ CASE WHEN RegionID IS NULL THEN 0 ELSE 1 END 
		FROM dbo.viewCarrierRateSheet RS
		JOIN dbo.tblCarrierRangeRate RR ON RR.RateSheetID = RS.ID AND @RouteMiles BETWEEN RR.MinRange AND RR.MaxRange
		WHERE coalesce(CarrierID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @CarrierID END, 0) = isnull(@CarrierID, 0)
		  AND coalesce(ProductGroupID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @ProductGroupID END, 0) = isnull(@ProductGroupID, 0)
		  AND coalesce(OriginStateID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @OriginStateID END, 0) = isnull(@OriginStateID, 0)
		  AND coalesce(DestStateID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @DestStateID END, 0) = isnull(@DestStateID, 0)
		  AND coalesce(RegionID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @RegionID END, 0) = isnull(@RegionID, 0)
		  AND @date BETWEEN EffectiveDate AND EndDate
		ORDER BY Ranking DESC
	) X

GO
GRANT SELECT ON fnCarrierRateSheetRangeRate TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RateSheetRangeRate info for the specified order
/***********************************/
CREATE FUNCTION fnOrderCarrierRateSheetRangeRate(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, RateTypeID, Rate, UomID
	FROM dbo.viewOrder O
	CROSS APPLY dbo.fnCarrierRateSheetRangeRate(O.OrderDate, O.ActualMiles, O.CarrierID, O.ProductGroupID, O.OriginStateID, O.DestStateID, O.OriginRegionID, 0) R
	WHERE O.ID = @ID

GO
GRANT SELECT ON fnOrderCarrierRateSheetRangeRate TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RateSheetRangeRate info for the specified order
/***********************************/
CREATE FUNCTION fnOrderCarrierLoadAmount(@ID int, @Units decimal(18, 10), @UomID int) RETURNS TABLE AS RETURN
	SELECT TOP 1 RouteRateID, RangeRateID
		, Amount = dbo.fnRateToAmount(RateTypeID, @Units, @UomID, Rate, UomID
			, CASE WHEN RateTypeID = 3 THEN (SELECT TOP 1 LoadAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID) ELSE NULL END)
	FROM (
		SELECT SortID = 1, RouteRateID = ID, RangeRateID = NULL, RateTypeID, UomID, Rate FROM dbo.fnOrderCarrierRouteRate(@ID)
		UNION SELECT 2, NULL, ID, RateTypeID, UomID, Rate FROM dbo.fnOrderCarrierRateSheetRangeRate(@ID)
	) X
	ORDER BY SortID

GO
GRANT SELECT ON fnOrderCarrierLoadAmount TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'viewCarrierWaitFeeParameter'))
	DROP VIEW viewCarrierWaitFeeParameter
GO
/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all CarrierWaitFeeParameter records
******************************************************/
CREATE VIEW viewCarrierWaitFeeParameter AS
	SELECT X.*
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementCarrier WHERE WaitFeeParameterID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblCarrierWaitFeeParameter XN
			WHERE isnull(XN.CarrierID, 0) = isnull(X.CarrierID, 0) 
			  AND isnull(XN.ProductGroupID, 0) = isnull(X.ProductGroupID, 0) 
			  AND isnull(XN.OriginStateID, 0) = isnull(X.OriginStateID, 0) 
			  AND isnull(XN.DestStateID, 0) = isnull(X.DestStateID, 0)
			  AND isnull(XN.RegionID, 0) = isnull(X.RegionID, 0)
			  AND XN.EffectiveDate > X.EffectiveDate)
	FROM tblCarrierWaitFeeParameter X

GO
GRANT SELECT ON viewCarrierWaitFeeParameter TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'fnCarrierWaitFeeParameter') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION fnCarrierWaitFeeParameter
GO
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier WaitFeeParameter info for the specified order
/***********************************/
CREATE FUNCTION fnCarrierWaitFeeParameter(@date date, @CarrierID int, @ProductGroupID int, @OriginStateID int, @DestStateID int, @RegionID int, @exactMatch bit = 0)
RETURNS TABLE AS RETURN
	SELECT ID, CarrierID, ProductGroupID, OriginStateID, DestStateID, RegionID, SubUnitID, RoundingTypeID, ThresholdMinutes, EffectiveDate, EndDate, NextEffectiveDate
	FROM (
		SELECT TOP 1 ID, CarrierID, ProductGroupID, OriginStateID, DestStateID, RegionID, SubUnitID, RoundingTypeID, ThresholdMinutes, EffectiveDate, EndDate, NextEffectiveDate
			, Ranking =	CASE WHEN X.CarrierID IS NULL THEN 0 ELSE 16 END
					  + CASE WHEN X.ProductGroupID IS NULL THEN 0 ELSE 8 END
					  + CASE WHEN X.OriginStateID IS NULL THEN 0 ELSE 4 END
					  + CASE WHEN X.DestStateID IS NULL THEN 0 ELSE 2 END
					  + CASE WHEN X.RegionID IS NULL THEN 0 ELSE 1 END 
		FROM dbo.viewCarrierWaitFeeParameter X 
		WHERE coalesce(CarrierID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @CarrierID END, 0) = isnull(@CarrierID, 0)
		  AND coalesce(ProductGroupID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @ProductGroupID END, 0) = isnull(@ProductGroupID, 0)
		  AND coalesce(OriginStateID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @OriginStateID END, 0) = isnull(@OriginStateID, 0)
		  AND coalesce(DestStateID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @DestStateID END, 0) = isnull(@DestStateID, 0)
		  AND coalesce(RegionID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @RegionID END, 0) = isnull(@RegionID, 0)
		  AND @date BETWEEN EffectiveDate AND EndDate
		ORDER BY Ranking DESC
	) X 
		
GO
GRANT SELECT ON fnCarrierWaitFeeParameter TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier WaitFeeParameter info for the specified order
/***********************************/
CREATE FUNCTION fnOrderCarrierWaitFeeParameter(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, SubUnitID, RoundingTypeID, ThresholdMinutes
	FROM dbo.viewOrder O
	CROSS APPLY dbo.fnCarrierWaitFeeParameter(O.OrderDate, O.CarrierID, O.ProductGroupID, O.OriginStateID, O.DestStateID, O.OriginRegionID, 0) R
	WHERE O.ID = @ID 

GO
GRANT SELECT ON fnOrderCarrierWaitFeeParameter TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'viewCarrierOriginWaitRate'))
	DROP VIEW viewCarrierOriginWaitRate
GO
/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all CarrierOriginWaitRate records
******************************************************/
CREATE VIEW viewCarrierOriginWaitRate AS
	SELECT X.*
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementCarrier WHERE OriginWaitRateID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblCarrierOriginWaitRate XN 
			WHERE isnull(XN.CarrierID, 0) = isnull(X.CarrierID, 0) 
			  AND isnull(XN.ProductGroupID, 0) = isnull(X.ProductGroupID, 0) 
			  AND isnull(XN.OriginID, 0) = isnull(X.OriginID, 0) 
			  AND isnull(XN.StateID, 0) = isnull(X.StateID, 0) 
			  AND isnull(XN.RegionID, 0) = isnull(X.RegionID, 0) 
			  AND XN.EffectiveDate > X.EffectiveDate)
	FROM tblCarrierOriginWaitRate X

GO
GRANT SELECT ON viewCarrierOriginWaitRate TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fnCarrierOriginWaitRate]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION [dbo].[fnCarrierOriginWaitRate]
GO
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OriginWaitRate info for the specified criteria
/***********************************/
CREATE FUNCTION [dbo].[fnCarrierOriginWaitRate](
  @Date date
, @ReasonID int
, @CarrierID int
, @ProductGroupID int
, @OriginID int
, @StateID int
, @RegionID int
, @exactMatch bit = 0)
RETURNS TABLE AS RETURN
		SELECT ID, ReasonID, CarrierID, ProductGroupID, OriginID, StateID, RegionID, Rate, EffectiveDate, EndDate, NextEffectiveDate
		FROM (
			SELECT TOP 1 ID, ReasonID, CarrierID, ProductGroupID, OriginID, StateID, RegionID, Rate, EffectiveDate, EndDate, NextEffectiveDate
				, Ranking =	CASE WHEN ReasonID IS NULL THEN 0 ELSE 16 END
						  + CASE WHEN CarrierID IS NULL THEN 0 ELSE 8 END
						  + CASE WHEN ProductGroupID IS NULL THEN 0 ELSE 4 END
						  + CASE WHEN OriginID IS NULL THEN 0 ELSE 2 END
						  + CASE WHEN StateID IS NULL THEN 0 ELSE 2 END
						  + CASE WHEN RegionID IS NULL THEN 0 ELSE 1 END 
			FROM  dbo.viewCarrierOriginWaitRate 
			WHERE coalesce(ReasonID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @ReasonID END, 0) = isnull(@ReasonID, 0)
			  AND coalesce(CarrierID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @CarrierID END, 0) = isnull(@CarrierID, 0)
			  AND coalesce(ProductGroupID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @ProductGroupID END, 0) = isnull(@ProductGroupID, 0)
			  AND coalesce(OriginID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @OriginID END, 0) = isnull(@OriginID, 0)
			  AND coalesce(StateID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @StateID END, 0) = isnull(@StateID, 0)
			  AND coalesce(RegionID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @RegionID END, 0) = isnull(@RegionID, 0)
			  AND @Date BETWEEN EffectiveDate AND EndDate
			ORDER BY Ranking DESC
		) X
		
GO
GRANT SELECT ON fnCarrierOriginWaitRate TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fnOrderCarrierOriginWaitRate]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION [dbo].[fnOrderCarrierOriginWaitRate]
GO
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OriginWaitRate info for the specified order
/***********************************/
CREATE FUNCTION [dbo].[fnOrderCarrierOriginWaitRate](@ID int) RETURNS TABLE AS RETURN
		SELECT R.ID, Minutes = O.DestMinutes, R.Rate
		FROM viewOrder O
		CROSS APPLY dbo.fnCarrierOriginWaitRate(O.OrderDate, O.DestWaitReasonID, O.CarrierID, O.ProductGroupID, O.OriginID, O.DestStateID, O.DestRegionID, 0) R
		WHERE O.ID = @ID

GO
GRANT SELECT ON fnOrderCarrierOriginWaitRate TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OriginWait data info for the specified order
/***********************************/
CREATE FUNCTION fnOrderCarrierOriginWaitData(@ID int) RETURNS TABLE AS RETURN
	SELECT RateID, BillableMinutes, BillableHours, Amount = BillableHours * Rate, WaitFeeParameterID
	FROM (
		SELECT RateID, BillableMinutes
			, BillableHours = dbo.fnComputeBillableWaitHours(BillableMinutes, SubUnitID, RoundingTypeID)
			, Rate
			, WaitFeeParameterID
		FROM (
			SELECT RateID = OWR.ID
				, Rate
				, BillableMinutes = dbo.fnMaxInt(OWR.Minutes - WFP.ThresholdMinutes, 0)
				, WaitFeeParameterID = WFP.ID, WFP.SubUnitID, WFP.RoundingTypeID
			FROM dbo.fnOrderCarrierOriginWaitRate(@ID) OWR 
			CROSS APPLY dbo.fnOrderCarrierWaitFeeParameter(@ID) WFP
		) X
	) X2
		
GO
GRANT SELECT ON fnOrderCarrierOriginWaitData TO dispatchcrude_iis_acct
GO 

IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'viewCarrierDestinationWaitRate'))
	DROP VIEW viewCarrierDestinationWaitRate
GO
/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all CarrierDestinationWaitRate records
******************************************************/
CREATE VIEW viewCarrierDestinationWaitRate AS
	SELECT X.*
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementCarrier WHERE DestinationWaitRateID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate)
			FROM tblCarrierDestinationWaitRate XN 
			WHERE isnull(XN.CarrierID, 0) = isnull(X.CarrierID, 0) 
			  AND isnull(XN.ProductGroupID, 0) = isnull(X.ProductGroupID, 0) 
			  AND isnull(XN.DestinationID, 0) = isnull(X.DestinationID, 0) 
			  AND isnull(XN.StateID, 0) = isnull(X.StateID, 0) 
			  AND isnull(XN.RegionID, 0) = isnull(X.RegionID, 0) 
			  AND XN.EffectiveDate > X.EffectiveDate)
	FROM tblCarrierDestinationWaitRate X

GO
GRANT SELECT ON viewCarrierDestinationWaitRate TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fnCarrierDestinationWaitRate]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION [dbo].[fnCarrierDestinationWaitRate]
GO
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier DestinationWaitRate info for the specified criteria
/***********************************/
CREATE FUNCTION [dbo].[fnCarrierDestinationWaitRate](
  @Date date
, @ReasonID int
, @CarrierID int
, @ProductGroupID int
, @DestinationID int
, @StateID int
, @RegionID int
, @exactMatch bit = 0)
RETURNS TABLE AS RETURN
		SELECT ID, ReasonID, CarrierID, ProductGroupID, DestinationID, StateID, RegionID, Rate, EffectiveDate, EndDate, NextEffectiveDate
		FROM (
			SELECT TOP 1 ID, ReasonID, CarrierID, ProductGroupID, DestinationID, StateID, RegionID, Rate, EffectiveDate, EndDate, NextEffectiveDate
				, Ranking =	CASE WHEN ReasonID IS NULL THEN 0 ELSE 16 END
						  + CASE WHEN CarrierID IS NULL THEN 0 ELSE 8 END
						  + CASE WHEN ProductGroupID IS NULL THEN 0 ELSE 4 END
						  + CASE WHEN DestinationID IS NULL THEN 0 ELSE 2 END
						  + CASE WHEN StateID IS NULL THEN 0 ELSE 2 END
						  + CASE WHEN RegionID IS NULL THEN 0 ELSE 1 END 
			FROM  dbo.viewCarrierDestinationWaitRate 
			WHERE coalesce(ReasonID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @ReasonID END, 0) = isnull(@ReasonID, 0)
			  AND coalesce(CarrierID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @CarrierID END, 0) = isnull(@CarrierID, 0)
			  AND coalesce(ProductGroupID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @ProductGroupID END, 0) = isnull(@ProductGroupID, 0)
			  AND coalesce(DestinationID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @DestinationID END, 0) = isnull(@DestinationID, 0)
			  AND coalesce(StateID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @StateID END, 0) = isnull(@StateID, 0)
			  AND coalesce(RegionID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @RegionID END, 0) = isnull(@RegionID, 0)
			  AND @Date BETWEEN EffectiveDate AND EndDate
			ORDER BY Ranking DESC
		) X
		
GO
GRANT SELECT ON fnCarrierDestinationWaitRate TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fnOrderCarrierDestinationWaitRate]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION [dbo].[fnOrderCarrierDestinationWaitRate]
GO
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier DestinationWaitRate info for the specified order
/***********************************/
CREATE FUNCTION [dbo].[fnOrderCarrierDestinationWaitRate](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, Minutes = O.DestMinutes, R.Rate
	FROM viewOrder O
	CROSS APPLY dbo.fnCarrierDestinationWaitRate(O.OrderDate, O.DestWaitReasonID, O.CarrierID, O.ProductGroupID, O.OriginID, O.DestStateID, O.DestRegionID, 0) R
	WHERE O.ID = @ID

GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'fnOrderCarrierDestinationWaitData') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION fnOrderCarrierDestinationWaitData
GO
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier DestinationWait data info for the specified order
/***********************************/
CREATE FUNCTION fnOrderCarrierDestinationWaitData(@ID int) RETURNS TABLE AS RETURN
	SELECT RateID, BillableMinutes, BillableHours, Amount = BillableHours * Rate
	FROM (
		SELECT RateID, BillableMinutes
			, BillableHours = dbo.fnComputeBillableWaitHours(BillableMinutes, SubUnitID, RoundingTypeID)
			, Rate
		FROM (
			SELECT RateID = OWR.ID
				, Rate
				, BillableMinutes = dbo.fnMaxInt(OWR.Minutes - WFP.ThresholdMinutes, 0)
				, WFP.SubUnitID, WFP.RoundingTypeID
			FROM dbo.fnOrderCarrierDestinationWaitRate(@ID) OWR 
			CROSS JOIN dbo.fnOrderCarrierWaitFeeParameter(@ID) WFP 
		) X
	) X2
		
GO
GRANT SELECT ON fnOrderCarrierDestinationWaitData TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'viewCarrierOrderRejectRate'))
	DROP VIEW viewCarrierOrderRejectRate
GO
/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all CarrierOrderRejectRate records
******************************************************/
CREATE VIEW viewCarrierOrderRejectRate AS
	SELECT X.*
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementCarrier WHERE OrderRejectRateID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblCarrierOrderRejectRate XN 
			WHERE isnull(XN.CarrierID, 0) = isnull(X.CarrierID, 0) 
			  AND isnull(XN.ProductGroupID, 0) = isnull(X.ProductGroupID, 0) 
			  AND isnull(XN.OriginID, 0) = isnull(X.OriginID, 0) 
			  AND isnull(XN.StateID, 0) = isnull(X.StateID, 0) 
			  AND isnull(XN.RegionID, 0) = isnull(X.RegionID, 0) 
			  AND XN.EffectiveDate > X.EffectiveDate)
	FROM tblCarrierOrderRejectRate X

GO
GRANT SELECT ON viewCarrierOrderRejectRate TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fnCarrierOrderRejectRate]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION [dbo].[fnCarrierOrderRejectRate]
GO
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OrderRejectRate info for the specified criteria
/***********************************/
CREATE FUNCTION fnCarrierOrderRejectRate(@date date, @ReasonID int, @CarrierID int, @ProductGroupID int, @StateID int, @RegionID int, @exactMatch bit = 0)
RETURNS TABLE AS RETURN
	SELECT ID, ReasonID, CarrierID, ProductGroupID, StateID, RegionID, Rate, RateTypeID, UomID, EffectiveDate, EndDate, NextEffectiveDate
	FROM (
		SELECT TOP 1 ID, ReasonID, CarrierID, ProductGroupID, StateID, RegionID, Rate, RateTypeID, UomID, EffectiveDate, EndDate, NextEffectiveDate
			, Ranking =	CASE WHEN ReasonID IS NULL THEN 0 ELSE 16 END
					  + CASE WHEN CarrierID IS NULL THEN 0 ELSE 8 END
					  + CASE WHEN ProductGroupID IS NULL THEN 0 ELSE 4 END
					  + CASE WHEN StateID IS NULL THEN 0 ELSE 2 END
					  + CASE WHEN RegionID IS NULL THEN 0 ELSE 1 END 
		FROM dbo.viewCarrierOrderRejectRate
		WHERE coalesce(ReasonID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @ReasonID END, 0) = isnull(@ReasonID, 0)
		  AND coalesce(CarrierID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @CarrierID END, 0) = isnull(CarrierID, 0)
		  AND coalesce(ProductGroupID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @ProductGroupID END, 0) = isnull(@ProductGroupID, 0)
		  AND coalesce(StateID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @StateID END, 0) = isnull(@StateID, 0)
		  AND coalesce(RegionID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @RegionID END, 0) = isnull(@RegionID, 0)
		  AND @date BETWEEN EffectiveDate AND EndDate
		ORDER BY Ranking DESC
	) X
		
GO
GRANT SELECT ON fnCarrierOrderRejectRate TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'fnOrderCarrierOrderRejectRate') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION fnOrderCarrierOrderRejectRate
GO
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OrderRejectRate info for the specified order
/***********************************/
CREATE FUNCTION fnOrderCarrierOrderRejectRate(@ID int) RETURNS TABLE AS RETURN
	SELECT TOP 1 R.ID, Rate, RateTypeID, UomID
	FROM dbo.viewOrder O
	CROSS APPLY fnCarrierOrderRejectRate(O.OrderDate, O.RejectReasonID, O.CarrierID, O.ProductGroupID, O.OriginStateID, O.OriginRegionID, 0) R
	WHERE O.ID = @ID

GO
GRANT SELECT ON fnOrderCarrierOrderRejectRate TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'fnOrderCarrierOrderRejectData') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION fnOrderCarrierOrderRejectData
GO
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OrderReject data info for the specified order
/***********************************/
CREATE FUNCTION fnOrderCarrierOrderRejectData(@ID int, @Units decimal(18, 10), @UomID int) RETURNS TABLE AS RETURN
	SELECT RateID = OWR.ID
		, Amount = dbo.fnRateToAmount(RateTypeID, @Units, @UomID, Rate, UomID
			, CASE WHEN RateTypeID = 3 THEN (SELECT TOP 1 OrderRejectAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID) ELSE NULL END)
	FROM dbo.fnOrderCarrierOrderRejectRate(@ID) OWR 
		
GO
GRANT SELECT ON fnOrderCarrierOrderRejectData TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fnCarrierAssessorialRates]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION [dbo].fnCarrierAssessorialRates
GO
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier AssessorialRate info for the specified order
/***********************************/
CREATE FUNCTION fnCarrierAssessorialRates(@date date, @CarrierID int, @ProductGroupID int, @OriginID int, @DestinationID int, @OriginStateID int, @DestStateID int, @RegionID int, @ProducerID int, @OperatorID int, @TypeID int, @exactMatch bit = 0)
RETURNS 
	@ret TABLE (
		ID int
	  , CarrierID int
	  , ProductGroupID int
	  , OriginID int
	  , DestinationID int
	  , OriginStateID int
	  , DestStateID int
	  , RegionID int
	  , ProducerID int
	  , OperatorID int
	  , TypeID int
	  , Rate decimal(18, 10)
	  , RateTypeID int
	  , UomID int
	  , EffectiveDate date
	  , EndDate date
	  , NextEffectiveDate date
	)
AS BEGIN
	-- get the raw data (all matched rows)
	DECLARE @src TABLE (ID int, TypeID int, Ranking int)
	INSERT INTO @src (ID, TypeID, Ranking)
		SELECT ID, TypeID
			, Ranking =	CASE WHEN CarrierID IS NULL THEN 0 ELSE 256 END
					  + CASE WHEN ProductGroupID IS NULL THEN 0 ELSE 128 END
					  + CASE WHEN OriginID IS NULL THEN 0 ELSE 64 END
					  + CASE WHEN DestinationID IS NULL THEN 0 ELSE 32 END
					  + CASE WHEN OriginStateID IS NULL THEN 0 ELSE 16 END
					  + CASE WHEN DestStateID IS NULL THEN 0 ELSE 8 END
					  + CASE WHEN RegionID IS NULL THEN 0 ELSE 4 END 
					  + CASE WHEN ProducerID IS NULL THEN 0 ELSE 2 END 
					  + CASE WHEN OperatorID IS NULL THEN 0 ELSE 1 END 
		FROM dbo.viewCarrierAssessorialRate
		WHERE coalesce(CarrierID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @CarrierID END, 0) = isnull(@CarrierID, 0)
		  AND coalesce(ProductGroupID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @ProductGroupID END, 0) = isnull(@ProductGroupID, 0)
		  AND coalesce(OriginID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @OriginID END, 0) = isnull(@OriginID, 0)
		  AND coalesce(DestinationID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @DestinationID END, 0) = isnull(@DestinationID, 0)
		  AND coalesce(OriginStateID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @OriginStateID END, 0) = isnull(@OriginStateID, 0)
		  AND coalesce(DestStateID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @DestStateID END, 0) = isnull(@DestStateID, 0)
		  AND coalesce(RegionID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @RegionID END, 0) = isnull(@RegionID, 0)
		  AND coalesce(ProducerID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @ProducerID END, 0) = isnull(@ProducerID, 0)
		  AND coalesce(OperatorID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @OperatorID END, 0) = isnull(@OperatorID, 0)
		  AND @date BETWEEN EffectiveDate AND EndDate
		  AND (@TypeID IS NULL OR TypeID = @TypeID)
		  
	-- return the rate data for the best-match rates 
	INSERT INTO @ret (ID, CarrierID, ProductGroupID, OriginID, DestinationID, OriginStateID, DestStateID, RegionID, ProducerID, OperatorID, TypeID, Rate, RateTypeID, UomID, EffectiveDate, EndDate, NextEffectiveDate)
		SELECT CAR.ID, CarrierID, ProductGroupID, OriginID, DestinationID, OriginStateID, DestStateID, RegionID, ProducerID, OperatorID, TypeID, Rate, RateTypeID, UomID, EffectiveDate, EndDate, NextEffectiveDate
		FROM viewCarrierAssessorialRate CAR
		JOIN (
			SELECT ID
			FROM @src S
			JOIN (
				SELECT TypeID, Ranking = MAX(Ranking)
				FROM @src
				GROUP BY TypeID
			) X ON X.TypeID = S.TypeID AND X.Ranking = S.Ranking
		) X ON X.ID = CAR.ID
	RETURN
END
GO
GRANT SELECT ON fnCarrierAssessorialRates TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'fnOrderCarrierAssessorialRates') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION fnOrderCarrierAssessorialRates
GO
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier AssessorialRate info for the specified order
/***********************************/
CREATE FUNCTION fnOrderCarrierAssessorialRates(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, TypeID, Rate, RateTypeID, UomID
	FROM dbo.viewOrder O
	CROSS APPLY dbo.fnCarrierAssessorialRates(O.OrderDate, O.CarrierID, O.ProductGroupID, O.OriginID, O.DestinationID, O.OriginStateID, O.DestStateID, O.OriginRegionID, O.ProducerID, O.OperatorID, NULL, 0) R
	WHERE O.ID = @ID
	  AND ((R.TypeID = 1 AND O.ChainUp = 1)
		OR (R.TypeID = 2 AND O.RerouteCount > 0)
		OR (R.TypeID = 3 AND O.TicketCount > 1)
		OR (R.TypeID = 4 AND O.H2S = 1)
		OR R.TypeID > 4)

GO
GRANT SELECT ON fnOrderCarrierAssessorialRates TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'fnOrderCarrierAssessorialAmounts') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION fnOrderCarrierAssessorialAmounts
GO
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier AssessorialRate Amounts info for the specified order
/***********************************/
CREATE FUNCTION fnOrderCarrierAssessorialAmounts(@ID int, @Units decimal(18, 10), @UomID int) RETURNS TABLE AS RETURN
	SELECT AssessorialRateTypeID = TypeID, RateID, RateTypeID, Amount
	FROM (
		SELECT TypeID, RateID = ID, RateTypeID
			, Amount = dbo.fnRateToAmount(RateTypeID, @Units, @UomID, Rate, UomID
			, CASE WHEN RateTypeID = 3 THEN (SELECT TOP 1 Amount FROM tblOrderSettlementCarrierAssessorialCharge WHERE OrderID = @ID AND AssessorialRateTypeID = TypeID) 
				   ELSE NULL END)
		FROM dbo.fnOrderCarrierAssessorialRates(@ID)
	) X
	WHERE Amount IS NOT NULL

GO
GRANT SELECT ON fnOrderCarrierAssessorialAmounts TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'fnCarrierFuelSurchargeRate') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION fnCarrierFuelSurchargeRate
GO
/***********************************/
-- Date Created: 23 Dec 20143
-- Author: Kevin Alons
-- Purpose: compute the appropriate CarrierFuelSurcharge for the specified parameters
/***********************************/
CREATE FUNCTION [dbo].[fnCarrierFuelSurchargeRate](@OriginID int, @CarrierID int, @EffectiveDate smalldatetime) 
RETURNS 
	@ret TABLE
	(
	    RateID int
	  , Rate money
	)
BEGIN
	DECLARE @rateID int, @rate money, @fuelPriceFloor money, @intervalAmount money, @incrementAmount money, @pricediff money	
	
	SELECT TOP 1 @rateID = ID, @fuelPriceFloor = FuelPriceFloor, @intervalAmount = IntervalAmount, @incrementAmount = IncrementAmount
		, @pricediff = dbo.fnMaxDecimal(0, dbo.fnFuelPriceForOrigin(@OriginID, @effectiveDate) - fuelPriceFloor)
	FROM tblCarrierFuelSurchargeRates 
	WHERE CarrierID = @CarrierID AND EffectiveDate <= @EffectiveDate
	ORDER BY EffectiveDate DESC
	
	SELECT @rate = @incrementAmount * (round(@pricediff / @intervalAmount, 0) + CASE WHEN @pricediff % @intervalAmount > 0 THEN 1 ELSe 0 END)
	
	INSERT INTO @ret (RateID, Rate) VALUES (@rateID, @rate)
		
	RETURN 
END

GO
GRANT SELECT ON dbo.fnCarrierFuelSurchargeRate TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Carrier "Settlement" $$ values to an Delivered/Audited order
/***********************************/
CREATE VIEW viewOrderSettlementUnitsCarrier AS
	SELECT X.*
		, SettlementUnits = dbo.fnMaxDecimal(X.ActualUnits, X.MinSettlementUnits)
	FROM (
		SELECT OrderID = O.ID
			, O.CarrierID
			, SettlementUomID = O.OriginUomID
			, C.SettlementFactorID
			, MinSettlementUnits = isnull(dbo.fnConvertUOM(C.MinSettlementUnits, C.MinSettlementUomID, O.OriginUomID), 0) 
			, ActualUnits = CASE C.SettlementFactorID	WHEN 1 THEN O.OriginGrossUnits 
														WHEN 3 THEN isnull(O.OriginGrossStdUnits, O.OriginGrossUnits) 
														ELSE coalesce(O.OriginNetUnits, O.OriginGrossStdUnits, O.OriginGrossUnits, 0) END 
		FROM dbo.tblOrder O
		JOIN tblCarrier C ON C.ID = O.CarrierID
	) X
GO
GRANT SELECT ON viewOrderSettlementUnitsCarrier TO dispatchcrude_iis_acct
GO

-- ensure a stub spProcessShipperInvoice SP exists because it is referenced by spProcessCarrierInvoice
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'spProcessShipperInvoice') AND type in (N'P', N'PC'))
	DROP PROCEDURE spProcessShipperInvoice
GO
CREATE PROCEDURE spProcessShipperInvoice(@ID int, @UserName varchar(100)) AS BEGIN SET NOCOUNT ON; /* code will be here*/ END 
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'spProcessCarrierInvoice') AND type in (N'P', N'PC'))
	DROP PROCEDURE spProcessCarrierInvoice
GO
/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Carrier "Settlement" $$ values to an Delivered/Audited order
/***********************************/
CREATE PROCEDURE spProcessCarrierInvoice
(
  @ID int
, @UserName varchar(100)
, @OriginWaitAmount money = NULL
, @DestWaitAmount money = NULL
, @RejectionAmount money = NULL
, @FuelSurchargeAmount money = NULL
, @LoadAmount money = NULL
, @AssessorialRateTypeID_CSV varchar(max) = NULL
, @AssessorialAmount_CSV varchar(max) = null
, @ResetOverrides bit = 0
) AS BEGIN

	SET NOCOUNT ON
	
	-- ensure this order hasn't yet been fully settled
	IF EXISTS (SELECT * FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND BatchID IS NOT NULL)
	BEGIN
		RAISERROR('Invoice has already been fully settled', 16, 1)
		RETURN
	END

	-- ensure that Shipper Rates have been initially applied prior to applying Carrier rates (since they could be dependent on Shipper rates)
	IF NOT EXISTS (SELECT * FROM tblOrderSettlementShipper WHERE OrderID = @ID)
		EXEC spProcessShipperInvoice @ID, @UserName

	/*
drop table #i
drop table #ia

declare @id int, @userName varchar(100)
, @OriginWaitAmount money 
, @DestWaitAmount money 
, @RejectionAmount money 
, @FuelSurchargeAmount money 
, @LoadAmount money 
, @AssessorialRateTypeID_CSV varchar(max) 
, @AssessorialAmount_CSV varchar(max) 
, @ResetOverrides bit 
select @ID=66969,@UserName='kalons'
--select @OriginWaitAmount=1.0000,@DestWaitAmount=2.0000,@RejectionAmount=3.0000,@FuelSurchargeAmount=4.0000,@LoadAmount=5.0000,@AssessorialRateTypeID_CSV='1',@AssessorialAmount_CSV='99'
select @ResetOverrides=0
--	*/
	-- get the provided manual Assessorial Rates (if any) into a usable able
	DECLARE @ManualAssessorialRates TABLE (ID int primary key, Amount money)
	INSERT INTO @ManualAssessorialRates (ID, Amount) 
		SELECT ID.ID, R.Amount
		FROM (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), ID FROM dbo.fnSplitCSVIDs(@AssessorialRateTypeID_CSV)) ID
		JOIN (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), amount = CAST(value as money) FROM dbo.fnSplitCSV(@AssessorialAmount_CSV)) R ON R.sortid = ID.sortid

	-- add in an existing manual Assessorial Rates from the existing Order Settlement (if any)
	INSERT INTO @ManualAssessorialRates (ID, Amount)
		SELECT AssessorialRateTypeID, Amount 
		FROM dbo.tblOrderSettlementCarrierAssessorialCharge 
		WHERE OrderID = @ID AND AssessorialRateID IS NULL 
			AND ID NOT IN (SELECT ID FROM @ManualAssessorialRates)
			AND @ResetOverrides = 0
--select * from @ManualAssessorialRates

	IF (@ResetOverrides = 0)
	BEGIN 
		SELECT @OriginWaitAmount = OriginWaitAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @OriginWaitAmount IS NULL AND OriginWaitRateID IS NULL
		SELECT @DestWaitAmount = DestinationWaitAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @DestWaitAmount IS NULL AND DestinationWaitRateID IS NULL
		SELECT @RejectionAmount = OrderRejectAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @RejectionAmount IS NULL AND OrderRejectRateID IS NULL
		SELECT @FuelSurchargeAmount = FuelSurchargeAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @FuelSurchargeAmount IS NULL AND FuelSurchargeRateID IS NULL
		SELECT @LoadAmount = LoadAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @LoadAmount IS NULL AND RouteRateID IS NULL AND RangeRateID IS NULL
	END
--select OverrideOriginWaitAmount = @OriginWaitAmount, OverrideDestWaitAmount = @DestWaitAmount, OverrideRejectionAmount = @RejectionAmount, OverrideFuelSurchargeAmount = @FuelSurchargeAmount, OverrideLoadAmount = @LoadAmount

	DECLARE @SettlementUnits TABLE
	(
		OrderID int
	  , SettlementUomID int
	  , SettlementFactorID int
	  , MinSettlementUnits decimal(18, 10)
	  , ActualUnits decimal(18, 10)
	  , SettlementUnits decimal(18, 10)
	)
	INSERT INTO @SettlementUnits (OrderID, SettlementUomID, SettlementFactorID, MinSettlementUnits, ActualUnits, SettlementUnits)
		SELECT OrderID, SettlementUomID, SettlementFactorID, MinSettlementUnits, ActualUnits, SettlementUnits
		FROM dbo.viewOrderSettlementUnitsCarrier 
		WHERE OrderID = @ID

	SELECT OrderID = @ID
		, OrderDate
		, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
		, RouteRateID, RangeRateID, LoadAmount
		, WaitFeeParameterID
		, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount 
		, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount
		, OrderRejectRateID, OrderRejectAmount
		, FuelSurchargeRateID, FuelSurchargeRate, FuelSurchargeAmount
		, OriginTaxRate
		, TotalAmount = ISNULL(LoadAmount, 0) 
			+ ISNULL(OriginWaitAmount, 0) 
			+ ISNULL(DestWaitAmount, 0) 
			+ ISNULL(OrderRejectAmount, 0) 
			+ ISNULL(FuelSurchargeAmount, 0)
		, CreatedByUser = @UserName
	INTO #I
	FROM (
		SELECT OrderDate
			, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
			, RouteRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RouteRateID ELSE NULL END
			, RangeRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RangeRateID ELSE NULL END
			, LoadAmount = ISNULL(@LoadAmount, RR.Amount)
			, WaitFeeParameterID
			, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount
			, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount
			, OrderRejectRateID = CASE WHEN @RejectionAmount IS NULL THEN RD.RateID ELSE NULL END
			, OrderRejectAmount = isnull(@RejectionAmount, RD.Amount)
			, FuelSurchargeRateID = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.RateID ELSE NULL END
			, FuelSurchargeRate = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.Rate ELSE NULL END
			, FuelSurchargeAmount = isnull(@FuelSurchargeAmount, FSR.Rate * X.RouteMiles)
			, OriginTaxRate
		FROM (
			SELECT O.ID
				, O.OriginID
				, O.CarrierID
				, O.RouteID
				, SU.SettlementFactorID
				, SU.SettlementUomID
				, SU.ActualUnits
				, SU.MinSettlementUnits
				, SU.SettlementUnits
				, RouteMiles = O.ActualMiles
				, O.OrderDate
				, O.ChainUp
				, O.H2S
				, O.Rejected
				, O.RerouteCount
				, IsSplit = CASE WHEN O.Rejected = 0 AND O.TicketCount > 1 THEN 1 ELSE 0 END
				, OWD.WaitFeeParameterID
				, OriginWaitRateID = CASE WHEN @OriginWaitAmount IS NULL THEN OWD.RateID ELSE NULL END
				, OriginWaitBillableMinutes = OWD.BillableMinutes
				, OriginWaitBillableHours = OWD.BillableHours
				, OriginWaitAmount = isnull(@OriginWaitAmount, OWD.Amount)
				, DestWaitRateID = CASE WHEN @DestWaitAmount IS NULL THEN DWD.RateID ELSE NULL END
				, DestWaitBillableMinutes = DWD.BillableMinutes
				, DestWaitBillableHours = DWD.BillableHours
				, DestWaitAmount = isnull(@DestWaitAmount, DWD.Amount)
				, OriginTaxRate = OO.TaxRate
			FROM dbo.viewOrder O
			JOIN @SettlementUnits SU ON SU.OrderID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			OUTER APPLY dbo.fnOrderCarrierOriginWaitData(@ID) OWD 
			OUTER APPLY dbo.fnOrderCarrierDestinationWaitData(@ID) DWD 
			WHERE O.ID = @ID
		) X
		OUTER APPLY dbo.fnOrderCarrierLoadAmount(@ID, SettlementUnits, SettlementUomID) RR 
		OUTER APPLY dbo.fnOrderCarrierOrderRejectData(@ID, SettlementUnits, SettlementUomID) RD
		OUTER APPLY dbo.fnCarrierFuelSurchargeRate(X.OriginID, X.CarrierID, X.OrderDate) FSR
	) X2
--select * from #I

	SELECT OrderID = @ID, AssessorialRateTypeID, AssessorialRateID = RateID, Amount, CreatedByUser = @UserName 
	INTO #IA
	FROM @SettlementUnits SU
	CROSS APPLY dbo.fnOrderCarrierAssessorialAmounts(@ID, SU.SettlementUnits, SU.SettlementUomID) CAA
	WHERE CAA.AssessorialRateTypeID NOT IN (SELECT ID FROM @ManualAssessorialRates)
/*
select OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser from #IA
union
select @ID, ID, NULL, Amount, @UserName from @ManualAssessorialRates
--*/
	DECLARE @CreatedTran bit; SET @CreatedTran = CASE WHEN @@TRANCOUNT = 0 THEN 1 ELSE 0 END
	BEGIN TRY
		IF (@CreatedTran = 1)
			BEGIN TRAN

		-- remove the existing settlment record (if any)
		DELETE FROM tblOrderSettlementCarrierAssessorialCharge WHERE OrderID = @ID
		DELETE FROM tblOrderSettlementCarrier WHERE OrderID = @ID
	
		-- all Units and Rates are first normalized to the Order.OriginUOM then consistent processing
		-- and persisted in the Invoice (Settlement) record in this Order.OriginUOM 
		INSERT INTO tblOrderSettlementCarrier (
			OrderID
			, OrderDate
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestinationWaitRateID 
			, DestinationWaitBillableMinutes 
			, DestinationWaitBillableHours
			, DestinationWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser)
		SELECT OrderID
			, OrderDate
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestWaitRateID 
			, DestWaitBillableMinutes 
			, DestWaitBillableHours
			, DestWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser
		FROM #I

		INSERT INTO tblOrderSettlementCarrierAssessorialCharge (OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser)
			SELECT OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser FROM #IA
			UNION
			SELECT @ID, ID, NULL, Amount, @UserName FROM @ManualAssessorialRates
	
		IF (@CreatedTran = 1)
			COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @msg varchar(max), @severity int
		SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY()
		ROLLBACK
		RAISERROR(@msg, @severity, 1)
		RETURN
	END CATCH	
	
END

GO

/*************************************
-- Date Created: 27 Dec 2014
-- Author: Kevin Alons
-- Purpose: ensure the TotalAmount always reflects the sum of the individual amounts (including associated Assessorial charges)
*************************************/
CREATE TRIGGER trigOrderSettlementCarrier_IU ON tblOrderSettlementCarrier FOR INSERT, UPDATE  AS
BEGIN
	-- Note: the TRIGGER_NESTLEVEL( OBJECT_ID('trigOrder_IU') ) = 0 statement prevents recursion
	IF (TRIGGER_NESTLEVEL( OBJECT_ID('trigOrderSettlementCarrier_IU')) = 1
		AND (
			UPDATE(LoadAmount)
			OR UPDATE(OrderRejectAmount)
			OR UPDATE(OriginWaitAmount)
			OR UPDATE(DestinationWaitAmount)
			OR UPDATE(FuelSurchargeAmount)
			OR UPDATE (TotalAmount)
			)
		)
	BEGIN
		UPDATE tblOrderSettlementCarrier
			SET TotalAmount = isnull(SC.LoadAmount, 0) 
				+ isnull(SC.OrderRejectAmount, 0) 
				+ isnull(SC.OriginWaitAmount, 0) 
				+ isnull(SC.DestinationWaitAmount, 0)
				+ isnull(SC.FuelSurchargeAmount, 0)
				+ isnull((SELECT SUM(Amount) FROM tblOrderSettlementCarrierAssessorialCharge WHERE OrderID = SC.OrderID), 0)
		FROM tblOrderSettlementCarrier SC
		JOIN inserted i ON i.OrderID = SC.OrderID 
	END
END
GO

/*************************************/
-- Date Created: 27 Dec 2014
-- Author: Kevin Alons
-- Purpose: ensure the TotalAmount always reflects the sum of the individual amounts (including associated Assessorial charges)
/*************************************/
CREATE TRIGGER trigOrderSettlementCarrierAssessorialCharge_IU ON tblOrderSettlementCarrierAssessorialCharge FOR INSERT, UPDATE  AS
BEGIN
	IF (UPDATE(Amount))
	BEGIN
		UPDATE tblOrderSettlementCarrier
			SET TotalAmount = 0
		FROM tblOrderSettlementCarrier SC
		WHERE OrderID IN (SELECT DISTINCT OrderID FROM inserted)
	END
END
GO

---------------------------------------------------------------
-- END CARRIER SETTLEMENT CHANGES
---------------------------------------------------------------

---------------------------------------------------------------
-- SHIPPER SETTLEMENT CHANGES
---------------------------------------------------------------
GO

CREATE TABLE tblShipperWaitFeeParameter
(
  ID int identity(1, 1) NOT NULL CONSTRAINT PK_ShipperWaitFeeParameter PRIMARY KEY NONCLUSTERED
, ShipperID int NULL CONSTRAINT FK_ShipperWaitFeeParameter_Shipper FOREIGN KEY REFERENCES tblCustomer(ID)
, ProductGroupID int NULL CONSTRAINT FK_ShipperWaitFeeParameter_ProductGroup FOREIGN KEY REFERENCES tblProductGroup(ID)
, OriginStateID int NULL CONSTRAINT FK_ShipperWaitFeeParameter_OriginState FOREIGN KEY REFERENCES tblState(ID)
, DestStateID int NULL CONSTRAINT FK_ShipperWaitFeeParameter_DestState FOREIGN KEY REFERENCES tblState(ID)
, RegionID int NULL CONSTRAINT FK_ShipperWaitFeeParameter_Region FOREIGN KEY REFERENCES tblRegion(ID)
, EffectiveDate date NOT NULL
, EndDate date NOT NULL 
, RoundingTypeID int NOT NULL CONSTRAINT FK_ShipperWaitFeeParameter_RoundingType FOREIGN KEY REFERENCES tblWaitFeeRoundingType(ID)
, SubUnitID int NOT NULL CONSTRAINT FK_ShipperWaitFeeParameter_SubUnit FOREIGN KEY REFERENCES tblWaitFeeSubUnit(ID)
, ThresholdMinutes int NOT NULL CONSTRAINT DF_ShipperWaitFeeParameter_ThresholdMinutes DEFAULT(60) CONSTRAINT CK_ShipperWaitFeeParameter_ThresholdMinutes_Positive CHECK (ThresholdMinutes > 0)
, CreateDateUTC smalldatetime NOT NULL CONSTRAINT DF_ShipperWaitFeeParameter_CreateDateUTC DEFAULT (getutcdate())
, CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_ShipperWaitFeeParameter_CreatedByUser DEFAULT (SUSER_NAME())
, LastChangeDateUTC smalldatetime NULL
, LastChangedByUser varchar(100) NULL
, CONSTRAINT CK_ShipperWaitFeeParameter_EndDate_Greater CHECK (EndDate >= EffectiveDate)
)
GO
GRANT SELECT, INSERT, UPDATE ON tblShipperWaitFeeParameter TO dispatchcrude_iis_acct
GO
CREATE UNIQUE CLUSTERED INDEX udxShipperWaitFeeParameter_Main ON tblShipperWaitFeeParameter(ShipperID, ProductGroupID, OriginStateID, DestStateID, RegionID, EffectiveDate)
GO
CREATE INDEX idxShipperWaitFeeParameter_Shipper ON tblShipperWaitFeeParameter(ShipperID)
GO
CREATE INDEX idxShipperWaitFeeParameter_Region ON tblShipperWaitFeeParameter(RegionID)
GO
CREATE INDEX idxShipperWaitFeeParameter_OriginState ON tblShipperWaitFeeParameter(OriginStateID)
GO
CREATE INDEX idxShipperWaitFeeParameter_DestState ON tblShipperWaitFeeParameter(DestStateID)
GO
CREATE INDEX idxShipperWaitFeeParameter_ProductGroup ON tblShipperWaitFeeParameter(ProductGroupID)
GO

/***********************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
/***********************************************/
CREATE TRIGGER trigShipperWaitFeeParameter_IU ON tblShipperWaitFeeParameter AFTER INSERT, UPDATE AS
BEGIN
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblShipperWaitFeeParameter X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(i.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		RAISERROR('Overlapping Parameters are not allowed', 16, 1)
		-- if an explicit TRANSACTION isn't present, then ROLLBACK to ensure these changes cannot be committed
		IF (@@TRANCOUNT = 0) ROLLBACK
	END
END
GO

INSERT INTO tblShipperWaitFeeParameter (ShipperID, RegionID, EffectiveDate, EndDate, RoundingTypeID, SubUnitID, ThresholdMinutes, CreateDateUTC, CreatedByUser)
	SELECT nullif(customerid, -1), nullif(regionid, -1), '1/1/2013', '2/28/2015', isnull(WaitFeeRoundingTypeID, 1), isnull(WaitFeeSubUnitID, 0), 60, CreateDateUTC, isnull(CreatedByUser, 'system')
	FROM tblcustomerrates
GO

CREATE TABLE tblShipperRouteRate
(
  ID int IDENTITY(1,1) NOT NULL CONSTRAINT PK_ShipperRouteRate PRIMARY KEY NONCLUSTERED
, ShipperID int NULL CONSTRAINT FK_ShipperRouteRate_Shipper FOREIGN KEY REFERENCES tblCustomer(ID) ON DELETE CASCADE
, ProductGroupID int NULL CONSTRAINT FK_ShipperRouteRate_ProductGroup FOREIGN KEY REFERENCES tblProductGroup(ID)
, RouteID int NOT NULL CONSTRAINT FK_ShipperRouteRate_Route FOREIGN KEY REFERENCES tblRoute(ID) ON DELETE CASCADE
, Rate decimal(18, 10) NOT NULL
, EffectiveDate date NOT NULL
, EndDate date NOT NULL 
, RateTypeID int NOT NULL CONSTRAINT FK_ShipperRouteRate_RateType FOREIGN KEY REFERENCES tblRateType(ID)
, UomID int NOT NULL CONSTRAINT FK_ShipperRouteRate_Uom FOREIGN KEY REFERENCES tblUom(ID)
, CreateDateUTC smalldatetime NOT NULL CONSTRAINT DF_ShipperRouteRate_CreateDateUTC DEFAULT (getutcdate())
, CreatedByUser varchar(100) NOT NULL
, LastChangeDateUTC smalldatetime NULL
, LastChangedByUser varchar(100) NULL
, CONSTRAINT CK_ShipperRouteRate_EndDate_Greater CHECK (EndDate >= EffectiveDate)
) 
GO
GRANT SELECT, INSERT, UPDATE ON tblShipperRouteRate TO dispatchcrude_iis_acct
GO
CREATE UNIQUE CLUSTERED INDEX udxShipperRateSheet_Main ON tblShipperRouteRate(ShipperID, ProductGroupID, RouteID, EffectiveDate)
GO
CREATE INDEX idxShipperRouteRate_Shipper ON tblShipperRouteRate(ShipperID)
GO
CREATE INDEX idxShipperRouteRate_ProductGroup ON tblShipperRouteRate(ProductGroupID)
GO
CREATE INDEX idxShipperRouteRate_Route ON tblShipperRouteRate(RouteID)
GO

/***********************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
/***********************************************/
CREATE TRIGGER trigShipperRouteRate_IU ON tblShipperRouteRate AFTER INSERT, UPDATE AS
BEGIN
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblShipperRouteRate X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND i.RouteID = X.RouteID
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		RAISERROR('Overlapping Route Rates are not allowed', 16, 1)
		-- if an explicit TRANSACTION isn't present, then ROLLBACK to ensure these changes cannot be committed
		IF (@@TRANCOUNT = 0) ROLLBACK
	END
END
GO

INSERT INTO tblShipperRouteRate (ShipperID, RouteID, Rate, EffectiveDate, EndDate, CreateDateUTC, CreatedByUser, RateTypeID, UomID)
	select customerid, routeid, rate, effectivedate
		, dateadd(day, -1, isnull((SELECT MIN(effectivedate) FROM tblcustomerrouterates X where X.ID <> i.ID AND X.customerid = i.customerid and X.routeid = i.routeid AND x.EffectiveDate > i.EffectiveDate), '2/28/2015'))
		, createdateutc, isnull(createdbyuser, 'system'), 1, uomid 
	from tblcustomerrouterates i
	order by routeid, customerid, effectivedate
GO

CREATE TABLE tblShipperRateSheet
(
  ID int identity(1, 1) NOT NULL CONSTRAINT PK_ShipperRateSheet PRIMARY KEY NONCLUSTERED
, ShipperID int NULL CONSTRAINT FK_ShipperRateSheet_Shipper FOREIGN KEY REFERENCES tblCustomer(ID)
, ProductGroupID int NULL CONSTRAINT FK_ShipperRateSheet_ProductGroup FOREIGN KEY REFERENCES tblProductGroup(ID)
, OriginStateID int NULL CONSTRAINT FK_ShipperRateSheet_OriginState FOREIGN KEY REFERENCES tblState(ID)
, DestStateID int NULL CONSTRAINT FK_ShipperRateSheet_DestState FOREIGN KEY REFERENCES tblState(ID)
, RegionID int NULL CONSTRAINT FK_ShipperRateSheet_Region FOREIGN KEY REFERENCES tblRegion(ID)
, EffectiveDate date NOT NULL
, EndDate date NOT NULL
, RateTypeID int NOT NULL CONSTRAINT FK_ShipperRateSheet_RateType FOREIGN KEY REFERENCES tblRateType(ID)
, UomID int NOT NULL CONSTRAINT FK_ShipperRateSheet_UomID FOREIGN KEY REFERENCES tblUom(ID)
, CreateDateUTC smalldatetime NOT NULL CONSTRAINT DF_ShipperRateSheet_CreateDateUTC DEFAULT (getutcdate())
, CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_ShipperRateSheet_CreatedByUser DEFAULT (SUSER_NAME())
, LastChangeDateUTC smalldatetime NULL
, LastChangedByUser varchar(100) NULL
, CONSTRAINT CK_ShipperRateSheet_EndDate_Greater CHECK (EndDate >= EffectiveDate)
)
GO
GRANT SELECT, INSERT, UPDATE ON tblShipperRateSheet TO dispatchcrude_iis_acct
GO
CREATE UNIQUE CLUSTERED INDEX udxShipperRateSheet_Main ON tblShipperRateSheet(ShipperID, ProductGroupID, RegionID, OriginStateID, DestStateID, EffectiveDate)
GO
CREATE INDEX idxShipperRateSheet_Shipper ON tblShipperRateSheet(ShipperID)
GO
CREATE INDEX idxShipperRateSheet_Region ON tblShipperRateSheet(RegionID)
GO
CREATE INDEX idxShipperRateSheet_OriginState ON tblShipperRateSheet(OriginStateID)
GO
CREATE INDEX idxShipperRateSheet_DestState ON tblShipperRateSheet(DestStateID)
GO
CREATE INDEX idxShipperRateSheet_ProductGroup ON tblShipperRateSheet(ProductGroupID)
GO

/***********************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
/***********************************************/
CREATE TRIGGER trigShipperRateSheet_IU ON tblShipperRateSheet AFTER INSERT, UPDATE AS
BEGIN
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblShipperRateSheet X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(i.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		RAISERROR('Overlapping Rate Sheets are not allowed', 16, 1)
		-- if an explicit TRANSACTION isn't present, then ROLLBACK to ensure these changes cannot be committed
		IF (@@TRANCOUNT = 0) ROLLBACK
	END
END
GO

CREATE TABLE tblShipperRangeRate
(
  ID int identity(1, 1) NOT NULL CONSTRAINT PK_ShipperRangeRate PRIMARY KEY NONCLUSTERED
, RateSheetID int NOT NULL CONSTRAINT FK_ShipperRangeRate FOREIGN KEY REFERENCES tblShipperRateSheet(ID) ON DELETE CASCADE
, MinRange int NOT NULL CONSTRAINT CK_ShipperRangeRate_MinRange_Positive CHECK (MinRange >= 0)
, MaxRange int NOT NULL 
, Rate decimal(18,10) NOT NULL
, CreateDateUTC smalldatetime NULL CONSTRAINT DF_ShipperRangeRate_CreateDateUTC DEFAULT (getutcdate())
, CreatedByUser varchar(100) NULL CONSTRAINT DF_ShipperRangeRate_CreatedByUser DEFAULT (SUSER_NAME())
, LastChangeDateUTC smalldatetime NULL
, LastChangedByUser varchar(100) NULL
, CONSTRAINT CK_ShipperRangeRate_MaxRange_Greater CHECK (MaxRange > MinRange)
)
GO
CREATE UNIQUE CLUSTERED INDEX udxShipperRangeRate_Main ON tblShipperRangeRate (RateSheetID, MinRange, MaxRange)
GO

/*****************************************************
-- Date Created: 15 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping Range Rates in the same Rate Sheet
*****************************************************/
CREATE TRIGGER trigShipperRangeRate_IU ON tblShipperRangeRate FOR INSERT, UPDATE AS
BEGIN
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblShipperRangeRate X ON X.RateSheetID = i.RateSheetID AND i.ID <> X.ID 
		WHERE i.MinRange BETWEEN X.MinRange AND X.MaxRange 
			OR i.MaxRange BETWEEN X.MinRange AND X.MaxRange
			OR X.MinRange BETWEEN i.MinRange AND i.MaxRange)
	BEGIN
		RAISERROR('Overlapping Range Rates are not allowed', 16, 1)
		-- if an explicit TRANSACTION isn't present, then ROLLBACK to ensure these changes cannot be committed
		IF (@@TRANCOUNT = 0) ROLLBACK
	END
END
GO

CREATE TABLE tblShipperAssessorialRate
(
  ID int identity(1, 1) NOT NULL CONSTRAINT PK_ShipperAssessorialRate PRIMARY KEY NONCLUSTERED
, TypeID int NOT NULL CONSTRAINT FK_ShipperAccessorialRate_Type FOREIGN KEY REFERENCES tblAssessorialRateType(ID)
, ShipperID int NULL CONSTRAINT FK_ShipperAssessorialRate_Shipper FOREIGN KEY REFERENCES tblCustomer(ID)
, ProductGroupID int NULL CONSTRAINT FK_ShipperAssessorialRate_ProductGroup FOREIGN KEY REFERENCES tblProductGroup(ID)
, OriginID int NULL CONSTRAINT FK_ShipperAssessorialRate_Origin FOREIGN KEY REFERENCES tblOrigin(ID)
, DestinationID int NULL CONSTRAINT FK_ShipperAssessorialRate_Destination FOREIGN KEY REFERENCES tblDestination(ID)
, OriginStateID int NULL CONSTRAINT FK_ShipperAssessorialRate_OriginState FOREIGN KEY REFERENCES tblState(ID)
, DestStateID int NULL CONSTRAINT FK_ShipperAssessorialRate_DestState FOREIGN KEY REFERENCES tblState(ID)
, RegionID int NULL CONSTRAINT FK_ShipperAssessorialRate_Region FOREIGN KEY REFERENCES tblRegion(ID)
, ProducerID int NULL CONSTRAINT FK_ShipperAssessorialRate_Producer FOREIGN KEY REFERENCES tblProducer(ID)
, OperatorID int NULL CONSTRAINT FK_ShipperAssessorialRate_Operator FOREIGN KEY REFERENCES tblOperator(ID)
, EffectiveDate date NOT NULL 
, EndDate date NOT NULL 
, Rate decimal(18,10) NOT NULL
, RateTypeID int NOT NULL CONSTRAINT FK_ShipperAccessorialRate_RateType FOREIGN KEY REFERENCES tblRateType(ID)
, UomID int NOT NULL CONSTRAINT FK_ShipperAccessorialRate_UomID FOREIGN KEY REFERENCES tblUom(ID)
, CreateDateUTC smalldatetime NOT NULL CONSTRAINT DF_ShipperAssessorialRate_CreateDateUTC DEFAULT (getutcdate())
, CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_ShipperAssessorialRate_CreatedByUser DEFAULT (SUSER_NAME())
, LastChangeDateUTC smalldatetime NULL
, LastChangedByUser varchar(100) NULL
, CONSTRAINT CK_ShipperRangeRate_EndDate_Greater CHECK (EndDate >= EffectiveDate)
)
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblShipperAssessorialRate TO dispatchcrude_iis_acct
GO
CREATE UNIQUE CLUSTERED INDEX udxShipperAssessorialRate_Main 
	ON tblShipperAssessorialRate(TypeID, ShipperID, ProductGroupID, OriginID, DestinationID, OriginStateID, DestStateID, RegionID, ProducerID, OperatorID, EffectiveDate)
GO
CREATE INDEX idxShipperAssessorialRate_Shipper ON tblShipperAssessorialRate(ShipperID)
GO
CREATE INDEX idxShipperAssessorialRate_Region ON tblShipperAssessorialRate(RegionID)
GO
CREATE INDEX idxShipperAssessorialRate_OriginState ON tblShipperAssessorialRate(OriginStateID)
GO
CREATE INDEX idxShipperAssessorialRate_DestState ON tblShipperAssessorialRate(DestStateID)
GO
CREATE INDEX idxShipperAssessorialRate_ProductGroup ON tblShipperAssessorialRate(ProductGroupID)
GO
CREATE INDEX idxShipperAssessorialRate_Producer ON tblShipperAssessorialRate(ProducerID)
GO
CREATE INDEX idxShipperAssessorialRate_Operator ON tblShipperAssessorialRate(OperatorID)
GO
CREATE INDEX idxShipperAssessorialRate_Origin ON tblShipperAssessorialRate(OriginID)
GO
CREATE INDEX idxShipperAssessorialRate_Destination ON tblShipperAssessorialRate(DestinationID)
GO

/***********************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
/***********************************************/
CREATE TRIGGER trigShipperAssessorialRate_IU ON tblShipperAssessorialRate AFTER INSERT, UPDATE AS
BEGIN
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblShipperAssessorialRate X 
			ON i.ID <> X.ID
			  AND i.TypeID = X.TypeID
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(i.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(i.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(i.ProducerID, X.ProducerID) = 1
			  AND dbo.fnCompareNullableInts(i.OperatorID, X.OperatorID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		RAISERROR('Overlapping Assessorial Rates are not allowed', 16, 1)
		-- if an explicit TRANSACTION isn't present, then ROLLBACK to ensure these changes cannot be committed
		IF (@@TRANCOUNT = 0) ROLLBACK
	END
END
GO

INSERT INTO tblShipperAssessorialRate(TypeID, ShipperID, RegionID, EffectiveDate, EndDate, Rate, RateTypeID, UomID)
		  select distinct 1, nullif(customerid, -1), nullif(regionid, -1), '1/1/2013', '2/28/2015', ChainupFee, 2, uomid from tblcustomerrates where chainupfee is not null
	union select distinct 2, nullif(customerid, -1), nullif(regionid, -1), '1/1/2013', '2/28/2015', RerouteFee, 2, uomid from tblcustomerrates where RerouteFee is not null
	union select distinct 3, nullif(customerid, -1), nullif(regionid, -1), '1/1/2013', '2/28/2015', SplitLoadRate, 2, uomid from tblcustomerrates where splitloadrate is not null
	union select distinct 4, nullif(customerid, -1), nullif(regionid, -1), '1/1/2013', '2/28/2015', H2SRate, 1, uomid from tblcustomerrates where h2srate is not null
GO

-- DestinationWaitRate changes
CREATE TABLE tblShipperDestinationWaitRate
(
  ID int IDENTITY(1,1) NOT NULL CONSTRAINT PK_ShipperDestinationWaitRate PRIMARY KEY CLUSTERED 
, ReasonID int NULL CONSTRAINT FK_ShipperDestinationWaitRate_Reason FOREIGN KEY REFERENCES tblDestinationWaitReason(ID)
, ShipperID int NULL CONSTRAINT FK_ShipperDestinationWaitRate_Shipper FOREIGN KEY REFERENCES tblCustomer(ID)
, ProductGroupID int NULL CONSTRAINT FK_ShipperDestinationWaitRate_ProductGroup FOREIGN KEY REFERENCES tblProductGroup(ID)
, DestinationID int NULL CONSTRAINT FK_ShipperDestinationWaitRate_Destination FOREIGN KEY REFERENCES tblDestination(ID)
, StateID int NULL CONSTRAINT FK_ShipperDestinationWaitRate_State FOREIGN KEY REFERENCES tblState(ID)
, RegionID int NULL CONSTRAINT FK_ShipperDestinationWaitRate_Region FOREIGN KEY REFERENCES tblRegion(ID)
, EffectiveDate date NOT NULL
, EndDate date NOT NULL
, Rate decimal(18, 10) NOT NULL
, CreateDateUTC datetime NOT NULL CONSTRAINT DF_ShipperDestinationWaitRate_CreateDateUTC DEFAULT (getutcdate())
, CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_ShipperDestinationWaitRate_CreatedByUser DEFAULT (suser_name())
, LastChangeDateUTC datetime NULL
, LastChangedByUser varchar(100) NULL
, CONSTRAINT CK_ShipperDestinationWaitRate_EndDate_Greater CHECK (EndDate >= EffectiveDate)
) 
GO 
GRANT SELECT, INSERT, UPDATE, DELETE ON tblShipperDestinationWaitRate TO dispatchcrude_iis_acct
GO
CREATE UNIQUE INDEX udxShipperDestinationWaitRate_Main ON tblShipperDestinationWaitRate(ReasonID, ShipperID, ProductGroupID, DestinationID, StateID, RegionID, EffectiveDate)
GO
CREATE INDEX idxShipperDestinationWaitRate_Reason ON tblShipperDestinationWaitRate(ReasonID)
GO
CREATE INDEX idxShipperDestinationWaitRate_Shipper ON tblShipperDestinationWaitRate(ShipperID)
GO
CREATE INDEX idxShipperDestinationWaitRate_ProductGroup ON tblShipperDestinationWaitRate(ProductGroupID)
GO
CREATE INDEX idxShipperDestinationWaitRate_Destination ON tblShipperDestinationWaitRate(DestinationID)
GO
CREATE INDEX idxShipperDestinationWaitRate_State ON tblShipperDestinationWaitRate(StateID)
GO
CREATE INDEX idxShipperDestinationWaitRate_Region ON tblShipperDestinationWaitRate(RegionID)
GO

/***********************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
/***********************************************/
CREATE TRIGGER trigShipperDestinationWaitRate_IU ON tblShipperDestinationWaitRate AFTER INSERT, UPDATE AS
BEGIN
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblShipperDestinationWaitRate X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ReasonID, X.ReasonID) = 1
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(i.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		RAISERROR('Overlapping Wait Rates are not allowed', 16, 1)
		-- if an explicit TRANSACTION isn't present, then ROLLBACK to ensure these changes cannot be committed
		IF (@@TRANCOUNT = 0) ROLLBACK
	END
END
GO
 
INSERT INTO tblShipperDestinationWaitRate (ReasonID, ShipperID, EffectiveDate, EndDate, Rate, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser)
	select nullif(DestinationWaitReasonID, -1), nullif(customerid, -1), EffectiveDate
		, dateadd(day, -1, isnull((SELECT MIN(EffectiveDate) FROM tblcustomerdestinationwaitrates X where X.ID <> i.ID AND X.customerID = i.customerid and X.destinationwaitreasonid = i.destinationwaitreasonid AND x.EffectiveDate > i.EffectiveDate), '2/28/2015'))
		, Rate, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser
	from tblcustomerdestinationwaitrates i
GO

INSERT INTO tblShipperDestinationWaitRate (ReasonID, ShipperID, RegionID, EffectiveDate, EndDate, Rate, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser)
	select null, nullif(customerid, -1), nullif(i.regionid, -1), '1/1/2015'
		, '2/28/2015'
		, waitfee, i.createdateutc, isnull(i.createdbyuser, 'System'), i.lastchangedateutc, i.lastchangedbyuser
	from tblcustomerrates i
	left join tblShipperDestinationWaitRate X ON isnull(X.ShipperID, 0) = isnull(i.customerid, 0) 
		and isnull(X.RegionID, 0) = isnull(i.regionid, 0) 
		and X.ReasonID IS NULL
	where X.ID IS NULL
GO

-- OriginWaitRate changes
CREATE TABLE tblShipperOriginWaitRate
(
  ID int IDENTITY(1,1) NOT NULL CONSTRAINT PK_ShipperOriginWaitRate PRIMARY KEY CLUSTERED 
, ReasonID int NULL CONSTRAINT FK_ShipperOriginWaitRate_Reason FOREIGN KEY REFERENCES tblOriginWaitReason(ID)
, ShipperID int NULL CONSTRAINT FK_ShipperOriginWaitRate_Shipper FOREIGN KEY REFERENCES tblCustomer(ID)
, ProductGroupID int NULL CONSTRAINT FK_ShipperOriginWaitRate_ProductGroup FOREIGN KEY REFERENCES tblProductGroup(ID)
, OriginID int NULL CONSTRAINT FK_ShipperOriginWaitRate_Origin FOREIGN KEY REFERENCES tblOrigin(ID)
, StateID int NULL CONSTRAINT FK_ShipperOriginWaitRate_State FOREIGN KEY REFERENCES tblState(ID)
, RegionID int NULL CONSTRAINT FK_ShipperOriginWaitRate_Region FOREIGN KEY REFERENCES tblRegion(ID)
, EffectiveDate date NOT NULL
, EndDate date NOT NULL
, Rate decimal(18, 10) NOT NULL
, CreateDateUTC datetime NOT NULL CONSTRAINT DF_ShipperOriginWaitRate_CreateDateUTC DEFAULT (getutcdate())
, CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_ShipperOriginWaitRate_CreatedByUser DEFAULT (suser_name())
, LastChangeDateUTC datetime NULL
, LastChangedByUser varchar(100) NULL
, CONSTRAINT CK_ShipperOriginWaitRate_EndDate_Greater CHECK (EndDate >= EffectiveDate)
) 
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblShipperOriginWaitRate TO dispatchcrude_iis_acct
GO
CREATE UNIQUE INDEX udxShipperOriginWaitRate_Main ON tblShipperOriginWaitRate(ReasonID, ShipperID, ProductGroupID, OriginID, StateID, RegionID, EffectiveDate)
GO
CREATE INDEX idxShipperOriginWaitRate_Reason ON tblShipperOriginWaitRate(ReasonID)
GO
CREATE INDEX idxShipperOriginWaitRate_Shipper ON tblShipperOriginWaitRate(ShipperID)
GO
CREATE INDEX idxShipperOriginWaitRate_ProductGroup ON tblShipperOriginWaitRate(ProductGroupID)
GO
CREATE INDEX idxShipperOriginWaitRate_Origin ON tblShipperOriginWaitRate(OriginID)
GO
CREATE INDEX idxShipperOriginWaitRate_State ON tblShipperOriginWaitRate(StateID)
GO
CREATE INDEX idxShipperOriginWaitRate_Region ON tblShipperOriginWaitRate(RegionID)
GO

/***********************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
/***********************************************/
CREATE TRIGGER trigShipperOriginWaitRate_IU ON tblShipperOriginWaitRate AFTER INSERT, UPDATE AS
BEGIN
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblShipperOriginWaitRate X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ReasonID, X.ReasonID) = 1
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(i.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		RAISERROR('Overlapping Wait Rates are not allowed', 16, 1)
		-- if an explicit TRANSACTION isn't present, then ROLLBACK to ensure these changes cannot be committed
		IF (@@TRANCOUNT = 0) ROLLBACK
	END
END
GO

INSERT INTO tblShipperOriginWaitRate (ReasonID, ShipperID, EffectiveDate, EndDate, Rate, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser)
	select nullif(OriginWaitReasonID, -1), nullif(customerid, -1), EffectiveDate
		, dateadd(day, -1, isnull((SELECT MIN(EffectiveDate) FROM tblcustomeroriginwaitrates X where X.ID <> i.ID AND X.customerID = i.customerid and X.originwaitreasonid = i.originwaitreasonid AND x.EffectiveDate > i.EffectiveDate), '2/28/2015'))
		, Rate, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser
	from tblcustomeroriginwaitrates i
GO
INSERT INTO tblShipperOriginWaitRate (ReasonID, ShipperID, RegionID, EffectiveDate, EndDate, Rate, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser)
	select null, nullif(customerid, -1), nullif(i.regionid, -1), '1/1/2015'
		, '2/28/2015'
		, waitfee, i.createdateutc, isnull(i.createdbyuser, 'System'), i.lastchangedateutc, i.lastchangedbyuser
	from tblcustomerrates i
	left join tblShipperOriginWaitRate X ON isnull(X.ShipperID, 0) = isnull(i.customerid, 0) 
		and isnull(X.RegionID, 0) = isnull(i.regionid, 0) 
		and X.ReasonID IS NULL
	where X.ID IS NULL
GO

CREATE TABLE tblShipperOrderRejectRate
(
  ID int IDENTITY(1,1) NOT NULL CONSTRAINT PK_ShipperOrderRejectRate PRIMARY KEY CLUSTERED 
, ReasonID int NULL CONSTRAINT FK_ShipperOrderRejectRate_Reason FOREIGN KEY REFERENCES tblOrderRejectReason(ID)
, ShipperID int NULL CONSTRAINT FK_ShipperOrderRejectRate_Shipper FOREIGN KEY REFERENCES tblCustomer(ID)
, ProductGroupID int NULL CONSTRAINT FK_ShipperOrderRejectRate_ProductGroup FOREIGN KEY REFERENCES tblProductGroup(ID)
, OriginID int NULL CONSTRAINT FK_ShipperOrderRejectRate_Origin FOREIGN KEY REFERENCES tblOrigin(ID)
, StateID int NULL CONSTRAINT FK_ShipperOrderRejectRate_State FOREIGN KEY REFERENCES tblState(ID)
, RegionID int NULL CONSTRAINT FK_ShipperOrderRejectRate_Region FOREIGN KEY REFERENCES tblRegion(ID)
, EffectiveDate date NOT NULL
, EndDate date NOT NULL
, Rate decimal(18, 10) NOT NULL
, RateTypeID int NOT NULL CONSTRAINT FK_ShipperOrderRejectRate_RateType FOREIGN KEY REFERENCES tblRateType(ID)
, UomID int NOT NULL CONSTRAINT FK_ShipperOrderRejectRate_Uom FOREIGN KEY REFERENCES tblUom(ID)
, CreateDateUTC datetime NOT NULL CONSTRAINT DF_ShipperOrderRejectRate_CreateDateUTC DEFAULT (getutcdate())
, CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_ShipperOrderRejectRate_CreatedByUser DEFAULT (suser_name())
, LastChangeDateUTC datetime NULL
, LastChangedByUser varchar(100) NULL
) 
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblShipperOrderRejectRate TO dispatchcrude_iis_acct
GO
CREATE UNIQUE INDEX udxShipperOrderRejectRate_Main ON tblShipperOrderRejectRate(ReasonID, ShipperID, ProductGroupID, OriginID, StateID, RegionID, EffectiveDate)
GO
CREATE INDEX idxShipperOrderRejectRate_Reason ON tblShipperOrderRejectRate(ReasonID)
GO
CREATE INDEX idxShipperOrderRejectRate_Shipper ON tblShipperOrderRejectRate(ShipperID)
GO
CREATE INDEX idxShipperOrderRejectRate_ProductGroup ON tblShipperOrderRejectRate(ProductGroupID)
GO
CREATE INDEX idxShipperOrderRejectRate_Origin ON tblShipperOrderRejectRate(OriginID)
GO
CREATE INDEX idxShipperOrderRejectRate_State ON tblShipperOrderRejectRate(StateID)
GO
CREATE INDEX idxShipperOrderRejectRate_Region ON tblShipperOrderRejectRate(RegionID)
GO

/***********************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
/***********************************************/
CREATE TRIGGER trigShipperOrderRejectRate_IU ON tblShipperOrderRejectRate AFTER INSERT, UPDATE AS
BEGIN
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblShipperOrderRejectRate X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ReasonID, X.ReasonID) = 1
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(i.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		RAISERROR('Overlapping Reject Rates are not allowed', 16, 1)
		-- if an explicit TRANSACTION isn't present, then ROLLBACK to ensure these changes cannot be committed
		IF (@@TRANCOUNT = 0) ROLLBACK
	END
END
GO

INSERT INTO tblShipperOrderRejectRate (ReasonID, ShipperID, EffectiveDate, EndDate, Rate, RateTypeID, UomID, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser)
	select nullif(OrderRejectReasonID, -1), nullif(customerid, -1), EffectiveDate
		, dateadd(day, -1, isnull((SELECT MIN(EffectiveDate) FROM tblcustomerorderrejectrates X where X.ID <> i.ID AND X.customerID = i.customerid and X.orderrejectreasonid = i.orderrejectreasonid AND x.EffectiveDate > i.EffectiveDate), '2/28/2015'))
		, Rate, 1, 1, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser
	from tblcustomerorderrejectrates i
GO
INSERT INTO tblShipperOrderRejectRate (ReasonID, ShipperID, RegionID, EffectiveDate, EndDate, Rate, RateTypeID, UomID, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser)
	select null, nullif(customerid, -1), nullif(i.regionid, -1), '1/1/2015'
		, '2/28/2015'
		, rejectionfee, 1, i.uomid
		, i.createdateutc, isnull(i.createdbyuser, 'System'), i.lastchangedateutc, i.lastchangedbyuser
	from tblcustomerrates i
	left join tblShipperOrderRejectRate X ON isnull(X.ShipperID, 0) = isnull(i.customerid, 0) 
		and isnull(X.RegionID, 0) = isnull(i.regionid, 0) 
		and X.ReasonID IS NULL
	where X.ID IS NULL
GO

-- OrderSettlementShipper (was OrderInvoiceShipper)
CREATE TABLE tblOrderSettlementShipper
(
  OrderID int NOT NULL CONSTRAINT PK_OrderSettlementShipper PRIMARY KEY CONSTRAINT FK_OrderSettlementShipper_Order FOREIGN KEY REFERENCES tblOrder(ID)
, OrderDate date NOT NULL
, BatchID int NULL CONSTRAINT FK_OrderSettlementShipper_Batch FOREIGN KEY REFERENCES tblCustomerSettlementBatch(ID)

, SettlementFactorID int NOT NULL CONSTRAINT FK_OrderSettlementShipper_SettlementFactor FOREIGN KEY REFERENCES tblSettlementFactor(ID)
, SettlementUomID int NOT NULL CONSTRAINT FK_OrderSettlementShipper_SettlementUom FOREIGN KEY REFERENCES tblUom(ID)
, MinSettlementUnits int NOT NULL
, SettlementUnits decimal(18, 10) NOT NULL

, RouteRateID int NULL CONSTRAINT FK_OrderSettlmentShipper_RouteRate FOREIGN KEY REFERENCES tblShipperRouteRate(ID) ON DELETE SET NULL
, RangeRateID int NULL CONSTRAINT FK_OrderSettlementShipper_RangeRate FOREIGN KEY REFERENCES tblShipperRangeRate(ID) ON DELETE SET NULL
, LoadAmount money NULL

, WaitFeeParameterID int NULL CONSTRAINT FK_OrderSettlementShipper_WaitFeeParameter FOREIGN KEY REFERENCES tblShipperWaitFeeParameter(ID) ON DELETE SET NULL
, OriginWaitRateID int NULL CONSTRAINT FK_OrderSettlementShipper_OriginWaitRate FOREIGN KEY REFERENCES tblShipperOriginWaitRate(ID) ON DELETE SET NULL
, OriginWaitBillableMinutes int NULL
, OriginWaitBillableHours money NULL
, OriginWaitAmount money NULL
, DestinationWaitRateID int NULL CONSTRAINT FK_OrderSettlementShipper_DestinationWaitRate FOREIGN KEY REFERENCES tblShipperDestinationWaitRate(ID) ON DELETE SET NULL
, DestinationWaitBillableMinutes int NULL
, DestinationWaitBillableHours money NULL
, DestinationWaitAmount money NULL
, OrderRejectRateID int NULL CONSTRAINT FK_OrderSettlementShipper_OrderRejectRate FOREIGN KEY REFERENCES tblShipperOrderRejectRate(ID) ON DELETE SET NULL
, OrderRejectAmount money NULL

, FuelSurchargeRateID int NULL CONSTRAINT FK_OrderSettlementShipper_FuelSurchargeRate FOREIGN KEY REFERENCES tblShipperFuelSurchargeRates(ID) ON DELETE SET NULL
, FuelSurchargeRate money NULL
, FuelSurchargeAmount money NULL

, OriginTaxRate money NULL

, TotalAmount money NOT NULL

, CreateDateUTC datetime NOT NULL CONSTRAINT DF_OrderSettlementShipper_CreateDateUTC DEFAULT (getutcdate())
, CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_OrderSettlementShipper_CreatedByUser DEFAULT (suser_name())
, CONSTRAINT CK_OrderSettlementShipper_RouteOrRangeRateNull CHECK (RouteRateID IS NULL OR RangeRateID IS NULL)
)
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblOrderSettlementShipper TO dispatchcrude_iis_acct
GO
CREATE NONCLUSTERED INDEX idxOrderSettlementShipper_Covering1 ON tblOrderSettlementShipper (WaitFeeParameterID, BatchID)
GO

INSERT INTO tblOrderSettlementShipper
	SELECT OrderID, OrderDate, BatchID
		, isnull(OIC.SettlementFactorID, C.SettlementFactorID), OIC.UomID, coalesce(OIC.MinSettlementUnits, C.MinSettlementUnits, 0), isnull(Units, 0)
		, NULL, NULL, OIC.LoadFee
		, coalesce(WFP0.ID, WFP1.ID, WFP2.ID)
		, NULL, BillableOriginWaitMinutes, BillableOriginWaitMinutes / 60.0, OriginWaitFee
		, NULL, BillableDestWaitMinutes, BillableDestWaitMinutes / 60.0, DestWaitFee
		, NULL, RejectionFee
		, NULL, FuelSurchargeRate, FuelSurchargeFee
		, TaxRate
		, TotalFee
		, OIC.CreateDateUTC, OIC.CreatedByUser
	FROM viewOrder O
	JOIN tblCustomer C ON C.ID = O.CustomerID
	JOIN tblOrderInvoicecustomer OIC ON OIC.OrderID = O.ID
	LEFT JOIN tblShipperWaitFeeParameter WFP0 ON WFP0.ShipperID = O.CustomerID AND WFP0.RegionID = O.OriginRegionID
	LEFT JOIN tblShipperWaitFeeParameter WFP1 ON WFP1.ShipperID = O.CustomerID AND WFP1.RegionID IS NULL
	LEFT JOIN tblShipperWaitFeeParameter WFP2 ON WFP2.ShipperID IS NULL AND WFP2.RegionID IS NULL
	LEFT JOIN viewcustomerrouterates CRR ON CRR.customerid = O.CustomerID AND CRR.RouteID = O.RouteID AND O.OrderDate BETWEEN CRR.EffectiveDate AND ISNULL(CRR.EndDate, '1/1/2015')
GO

CREATE TABLE tblOrderSettlementShipperAssessorialCharge
(
  ID int NOT NULL identity(1, 1) CONSTRAINT PK_OrderSettlementShipperOtherAssessorialCharge PRIMARY KEY NONCLUSTERED
, OrderID int NOT NULL CONSTRAINT FK_OrderSettlementShipperOtherAssessorialCharge_Settlement FOREIGN KEY REFERENCES tblOrderSettlementShipper(OrderID) ON DELETE CASCADE
, AssessorialRateTypeID int NOT NULL CONSTRAINT FK_OrderSettlementShipperOtherAssessorialCharge_RateType FOREIGN KEY REFERENCES tblAssessorialRateType(ID)
, AssessorialRateID int NULL CONSTRAINT FK_OrderSettlementShipperOtherAssessorialCharge_AssessorialRate FOREIGN KEY REFERENCES tblShipperAssessorialRate(ID)
, Amount money NOT NULL
, CreateDateUTC datetime NOT NULL CONSTRAINT DF_OrderSettlmentShipperOtherAssessorialCharge_CreateDateUTC DEFAULT (getutcdate())
, CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_OrderSettlmentShipperOtherAssessorialCharge_CreatedByUser DEFAULT (suser_name())
, LastChangeDateUTC datetime NULL
, LastChangedByUser varchar(100) NULL
)
GRANT SELECT, INSERT, UPDATE, DELETE ON tblOrderSettlementShipperAssessorialCharge TO dispatchcrude_iis_acct
GO
CREATE UNIQUE CLUSTERED INDEX udxOrderSettlementShipperAssessorialCharge_Main ON tblOrderSettlementShipperAssessorialCharge(OrderID, AssessorialRateTypeID)
GO

INSERT INTO tblOrderSettlementShipperAssessorialCharge (OrderID, AssessorialRateTypeID, Amount, CreateDateUTC, CreatedByUser)
	SELECT OrderID
		, 1
		, ChainupFee
		, CreateDateUTC, CreatedByUser
	FROM tblOrderInvoiceCustomer OIC
	WHERE isnull(ChainupFee, 0) <> 0
	UNION SELECT OrderID
		, 2
		, RerouteFee
		, CreateDateUTC, CreatedByUser
	FROM tblOrderInvoiceCustomer OIC
	WHERE isnull(RerouteFee, 0) <> 0
	UNION SELECT OrderID
		, 3
		, SplitLoadFee
		, CreateDateUTC, CreatedByUser
	FROM tblOrderInvoiceCustomer OIC
	WHERE isnull(SplitLoadFee, 0) <> 0
	UNION SELECT OrderID
		, 4
		, H2SFee
		, CreateDateUTC, CreatedByUser
	FROM tblOrderInvoiceCustomer OIC
	WHERE isnull(H2SFee, 0) <> 0
GO

IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'viewShipperRateSheet'))
	DROP VIEW viewShipperRateSheet
GO
/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all ShipperRateSheet records
******************************************************/
CREATE VIEW viewShipperRateSheet AS
	SELECT X.*
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementShipper SC JOIN tblShipperRangeRate RR ON RR.ID = SC.RangeRateID WHERE RR.RateSheetID = X.ID AND SC.BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblShipperRateSheet XN 
			WHERE isnull(XN.ShipperID, 0) = isnull(X.ShipperID, 0) 
			  AND isnull(XN.ProductGroupID, 0) = isnull(X.ProductGroupID, 0) 
			  AND isnull(XN.OriginStateID, 0) = isnull(X.OriginStateID, 0) 
			  AND isnull(XN.DestStateID, 0) = isnull(X.DestStateID, 0) 
			  AND isnull(XN.RegionID, 0) = isnull(X.RegionID, 0) 
			  AND XN.EffectiveDate > X.EffectiveDate
		)
	FROM tblShipperRateSheet X

GO
GRANT SELECT ON viewShipperRateSheet TO dispatchcrude_iis_acct
GO

/************************************************
-- Date Created: 18 Jan 2015
-- Author: Kevin Alons
-- Purpose: return combined RateSheet + RangeRate data + friendly translated values
			allow updating of both RateSheet & RangeRate data together
************************************************/
CREATE VIEW viewShipperRateSheetRangeRate AS
	SELECT RR.ID, RateSheetID = R.ID, R.ShipperID, R.ProductGroupID, R.OriginStateID, R.DestStateID, R.RegionID, RR.Rate, RR.MinRange, RR.MaxRange, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate, R.NextEffectiveDate
		, Shipper = C.Name
		, ProductGroup = PG.Name
		, OriginState = OS.FullName
		, OriginStateAbbrev = OS.Abbreviation
		, DestinationState = DS.FullName
		, DestinationStateAbbrev = DS.Abbreviation
		, Region = REG.Name
		, RateType = RT.Name
		, Uom = U.Name
		, UomShort = U.Abbrev
		, Locked
		, RR.CreateDateUTC, RR.CreatedByUser
		, RR.LastChangeDateUTC, RR.LastChangedByUser
	FROM dbo.viewShipperRateSheet R
	JOIN dbo.tblShipperRangeRate RR ON RR.RateSheetID = R.ID
	LEFT JOIN tblCustomer C ON C.ID = R.ShipperID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblState OS ON OS.ID = R.OriginStateID
	LEFT JOIN tblState DS ON DS.ID = R.DestStateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblRateType RT ON RT.ID = R.RateTypeID
	LEFT JOIN tblUom U ON U.ID = R.UomID
GO
GRANT SELECT, INSERT, INSERT, DELETE ON viewShipperRateSheetRangeRate TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details + FINANCIAL INFO into a single view
/***********************************/
CREATE VIEW [dbo].[viewOrderSettlementShipper] AS 
	SELECT OSC.*
		, BatchNum = SB.BatchNum
		, SettlementUom = UOM.Name
		, SettlementUomShort = UOM.Abbrev
		, RouteRate = RR.Rate
		, RouteRateType = RRT.Name
		, RateSheetRate = RSRR.Rate
		, RateSheetRateType = RSRT.Name
		, OrderRejectRate = ORR.Rate
		, OrderRejectRateType = ORRT.Name
		, OriginWaitRate = OWR.Rate
		, DestinationWaitRate = DWR.Rate
		, TotalWaitAmount = cast(OSC.OriginWaitAmount as money) + cast(OSC.DestinationWaitAmount as money)
		, ChainupRate = CAR.Rate
		, ChainupRateType = CART.Name
		, ChainupAmount = CA.Amount
		, RerouteRate = RAR.Rate
		, RerouteRateType = RART.Name
		, RerouteAmount = RA.Amount
		, SplitLoadRate = SAR.Rate
		, SplitLoadRateType = SART.Name
		, SplitLoadAmount = SA.Amount
		, H2SRate = HAR.Rate
		, H2SRateType = HART.Name
		, H2SAmount = HA.Amount
		, OtherAmount = OA.Amount
		, WaitFeeSubUnit = isnull(WFSU.Name, 'None') 
		, WaitFeeRoundingType = isnull(WFRT.Name, 'None') 
	FROM tblOrderSettlementShipper OSC 
	LEFT JOIN tblCustomerSettlementBatch SB ON SB.ID = OSC.BatchID
	LEFT JOIN tblUom UOM ON UOM.ID = OSC.SettlementUomID
	LEFT JOIN tblShipperOriginWaitRate OWR ON OWR.ID = OSC.OriginWaitRateID
	LEFT JOIN tblShipperDestinationWaitRate DWR ON DWR.ID = OSC.DestinationWaitRateID
	LEFT JOIN tblShipperOrderRejectRate ORR ON ORR.ID = OSC.OrderRejectRateID
	LEFT JOIN tblRateType ORRT ON ORRT.ID = ORR.RateTypeID
	-- route rate/ratesheet JOINs
	LEFT JOIN tblShipperRouteRate RR ON RR.ID = OSC.RouteRateID
	LEFT JOIN tblRateType RRT ON RRT.ID = RR.RateTypeID
	LEFT JOIN viewShipperRateSheetRangeRate RSRR ON RSRR.ID = OSC.RangeRateID
	LEFT JOIN tblRateType RSRT ON RSRT.ID = RSRR.RateTypeID
	-- chainup
	LEFT JOIN tblOrderSettlementShipperAssessorialCharge CA ON CA.OrderID = OSC.OrderID AND CA.AssessorialRateTypeID = 1
	LEFT JOIN tblShipperAssessorialRate CAR ON CAR.ID = CA.AssessorialRateID
	LEFT JOIN tblRateType CART ON CART.ID = CAR.RateTypeID
	-- Reroute
	LEFT JOIN tblOrderSettlementShipperAssessorialCharge RA ON RA.OrderID = OSC.OrderID AND RA.AssessorialRateTypeID = 2
	LEFT JOIN tblShipperAssessorialRate RAR ON RAR.ID = RA.AssessorialRateID
	LEFT JOIN tblRateType RART ON RART.ID = RAR.RateTypeID
	-- Split Load
	LEFT JOIN tblOrderSettlementShipperAssessorialCharge SA ON SA.OrderID = OSC.OrderID AND SA.AssessorialRateTypeID = 3
	LEFT JOIN tblShipperAssessorialRate SAR ON SAR.ID = SA.AssessorialRateID
	LEFT JOIN tblRateType SART ON SART.ID = SAR.RateTypeID
	-- H2S
	LEFT JOIN tblOrderSettlementShipperAssessorialCharge HA ON HA.OrderID = OSC.OrderID AND HA.AssessorialRateTypeID = 4
	LEFT JOIN tblShipperAssessorialRate HAR ON HAR.ID = HA.AssessorialRateID
	LEFT JOIN tblRateType HART ON HART.ID = HAR.RateTypeID
	-- Other Assessorial Charges (combined)
	LEFT JOIN (
		SELECT OrderID, Amount = SUM(Amount) FROM tblOrderSettlementShipperAssessorialCharge WHERE AssessorialRateTypeID NOT IN (1,2,3,4) GROUP BY OrderID
	) OA ON OA.OrderID = OSC.OrderID 
	
	LEFT JOIN tblShipperWaitFeeParameter WFP ON WFP.ID = OSC.WaitFeeParameterID
	LEFT JOIN dbo.tblWaitFeeSubUnit WFSU ON WFSU.ID = WFP.SubUnitID
	LEFT JOIN dbo.tblWaitFeeRoundingType WFRT ON WFRT.ID = WFP.RoundingTypeID
GO
GRANT SELECT ON viewOrderSettlementShipper TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return OrderSettlementShipperAssessorialCharge records with BatchId included (to determine whether actually "Settled" or not)
/***********************************/
CREATE VIEW viewOrderSettlementShipperAssessorialCharge AS
	SELECT AC.*
		 , SC.BatchID
	FROM tblOrderSettlementShipperAssessorialCharge AC
	JOIN tblOrderSettlementShipper SC ON SC.OrderID = AC.OrderID
GO
GRANT SELECT ON viewOrderSettlementShipperAssessorialCharge TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'viewShipperAssessorialRate'))
	DROP VIEW viewShipperAssessorialRate
GO
/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all ShipperAssessorialRate records
******************************************************/
CREATE VIEW viewShipperAssessorialRate AS
	SELECT X.*
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM viewOrderSettlementShipperAssessorialCharge WHERE AssessorialRateID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblShipperAssessorialRate XN 
			WHERE XN.TypeID = X.TypeID
			  AND dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND dbo.fnCompareNullableInts(XN.OperatorID, X.OperatorID) = 1
			  AND XN.EffectiveDate > X.EffectiveDate)
	FROM tblShipperAssessorialRate X
GO
GRANT SELECT ON viewShipperAssessorialRate TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'viewShipperRouteRate'))
	DROP VIEW viewShipperRouteRate
GO
/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all ShipperRouteRate records
******************************************************/
CREATE VIEW viewShipperRouteRate AS
	SELECT X.*
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementShipper WHERE RouteRateID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, R.OriginID
		, R.DestinationID
		, R.ActualMiles
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblShipperRouteRate XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND XN.RouteID = X.RouteID
			  AND XN.EffectiveDate > X.EffectiveDate)
	FROM tblShipperRouteRate X
	JOIN tblRoute R ON R.ID = X.RouteID
GO
GRANT SELECT ON viewShipperRouteRate TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'fnShipperRouteRate') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION fnShipperRouteRate
GO 
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper RouteRate info for the specified criteria
/***********************************/
CREATE FUNCTION fnShipperRouteRate(@Date date, @RouteID int, @ShipperID int, @ProductGroupID int, @exactMatch bit = 0) RETURNS TABLE AS RETURN
	SELECT ID, RouteID, ShipperID, ProductGroupID, Rate, RateTypeID, UomID, EffectiveDate, EndDate, NextEffectiveDate
	FROM (
		SELECT TOP 1 ID, RouteID, ShipperID, ProductGroupID, Rate, RateTypeID, UomID, EffectiveDate, EndDate, NextEffectiveDate
			, Ranking =	  CASE WHEN ShipperID IS NULL THEN 0 ELSE 2 END 
						+ CASE WHEN ProductGroupID IS NULL THEN 0 ELSE 1 END
		FROM dbo.viewShipperRouteRate 
		WHERE coalesce(RouteID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @RouteID END, 0) = isnull(@RouteID, 0)
		  AND coalesce(ShipperID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @ShipperID END, 0) = isnull(@ShipperID, 0)
		  AND coalesce(ProductGroupID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @ProductGroupID END, 0) = isnull(@ProductGroupID, 0)
		  AND @Date BETWEEN EffectiveDate AND EndDate
		ORDER BY Ranking DESC
	) X 

GO
GRANT SELECT ON fnShipperRouteRate TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fnOrderShipperRouteRate]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION [dbo].[fnOrderShipperRouteRate]
GO
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper RouteRate info for the specified order
/***********************************/
CREATE FUNCTION fnOrderShipperRouteRate(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, RateTypeID, Rate, R.UomID
	FROM dbo.viewOrder O
	CROSS APPLY dbo.fnShipperRouteRate(O.OrderDate, O.RouteID, O.CustomerID, O.ProductGroupID, 0) R
	WHERE O.ID = @ID 

GO
GRANT SELECT ON fnOrderShipperRouteRate TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'fnShipperRateSheetRangeRate') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION fnShipperRateSheetRangeRate
GO
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper RateSheetRangeRate info for the specified criteria
/***********************************/
CREATE FUNCTION fnShipperRateSheetRangeRate(@date date, @RouteMiles int, @ShipperID int, @ProductGroupID int, @OriginStateID int, @DestStateID int, @RegionID int, @exactMatch bit = 0)
RETURNS TABLE AS RETURN
	SELECT ID, RateSheetID, ShipperID, ProductGroupID, OriginStateID, DestStateID, RegionID, Rate, MinRange, MaxRange, RateTypeID, UomID, EffectiveDate, EndDate, NextEffectiveDate
	FROM (
		SELECT TOP 1 RR.ID, RateSheetID = RS.ID, ShipperID, ProductGroupID, OriginStateID, DestStateID, RegionID, Rate, MinRange, MaxRange, RateTypeID, UomID, EffectiveDate, EndDate, NextEffectiveDate
			, Ranking =	  CASE WHEN ShipperID IS NULL THEN 0 ELSE 16 END
						+ CASE WHEN ProductGroupID IS NULL THEN 0 ELSE 8 END
						+ CASE WHEN OriginStateID IS NULL THEN 0 ELSE 4 END
						+ CASE WHEN DestStateID IS NULL THEN 0 ELSE 2 END
						+ CASE WHEN RegionID IS NULL THEN 0 ELSE 1 END 
		FROM dbo.viewShipperRateSheet RS
		JOIN dbo.tblShipperRangeRate RR ON RR.RateSheetID = RS.ID AND @RouteMiles BETWEEN RR.MinRange AND RR.MaxRange
		WHERE coalesce(ShipperID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @ShipperID END, 0) = isnull(@ShipperID, 0)
		  AND coalesce(ProductGroupID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @ProductGroupID END, 0) = isnull(@ProductGroupID, 0)
		  AND coalesce(OriginStateID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @OriginStateID END, 0) = isnull(@OriginStateID, 0)
		  AND coalesce(DestStateID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @DestStateID END, 0) = isnull(@DestStateID, 0)
		  AND coalesce(RegionID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @RegionID END, 0) = isnull(@RegionID, 0)
		  AND @date BETWEEN EffectiveDate AND EndDate
		ORDER BY Ranking DESC
	) X

GO
GRANT SELECT ON fnShipperRateSheetRangeRate TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper RateSheetRangeRate info for the specified order
/***********************************/
CREATE FUNCTION fnOrderShipperRateSheetRangeRate(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, RateTypeID, Rate, UomID
	FROM dbo.viewOrder O
	CROSS APPLY dbo.fnShipperRateSheetRangeRate(O.OrderDate, O.ActualMiles, O.CustomerID, O.ProductGroupID, O.OriginStateID, O.DestStateID, O.OriginRegionID, 0) R
	WHERE O.ID = @ID

GO
GRANT SELECT ON fnOrderShipperRateSheetRangeRate TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper RateSheetRangeRate info for the specified order
/***********************************/
CREATE FUNCTION fnOrderShipperLoadAmount(@ID int, @Units decimal(18, 10), @UomID int) RETURNS TABLE AS RETURN
	SELECT TOP 1 RouteRateID, RangeRateID
		, Amount = dbo.fnRateToAmount(RateTypeID, @Units, @UomID, Rate, UomID
			, CASE WHEN RateTypeID = 3 THEN (SELECT TOP 1 LoadAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID) ELSE NULL END)
	FROM (
		SELECT SortID = 1, RouteRateID = ID, RangeRateID = NULL, RateTypeID, UomID, Rate FROM dbo.fnOrderShipperRouteRate(@ID)
		UNION SELECT 2, NULL, ID, RateTypeID, UomID, Rate FROM dbo.fnOrderShipperRateSheetRangeRate(@ID)
	) X
	ORDER BY SortID

GO
GRANT SELECT ON fnOrderShipperLoadAmount TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'viewShipperWaitFeeParameter'))
	DROP VIEW viewShipperWaitFeeParameter
GO
/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all ShipperWaitFeeParameter records
******************************************************/
CREATE VIEW viewShipperWaitFeeParameter AS
	SELECT X.*
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementShipper WHERE WaitFeeParameterID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblShipperWaitFeeParameter XN
			WHERE isnull(XN.ShipperID, 0) = isnull(X.ShipperID, 0) 
			  AND isnull(XN.ProductGroupID, 0) = isnull(X.ProductGroupID, 0) 
			  AND isnull(XN.OriginStateID, 0) = isnull(X.OriginStateID, 0) 
			  AND isnull(XN.DestStateID, 0) = isnull(X.DestStateID, 0)
			  AND isnull(XN.RegionID, 0) = isnull(X.RegionID, 0)
			  AND XN.EffectiveDate > X.EffectiveDate)
	FROM tblShipperWaitFeeParameter X

GO
GRANT SELECT ON viewShipperWaitFeeParameter TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'fnShipperWaitFeeParameter') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION fnShipperWaitFeeParameter
GO
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper WaitFeeParameter info for the specified order
/***********************************/
CREATE FUNCTION fnShipperWaitFeeParameter(@date date, @ShipperID int, @ProductGroupID int, @OriginStateID int, @DestStateID int, @RegionID int, @exactMatch bit = 0)
RETURNS TABLE AS RETURN
	SELECT ID, ShipperID, ProductGroupID, OriginStateID, DestStateID, RegionID, SubUnitID, RoundingTypeID, ThresholdMinutes, EffectiveDate, EndDate, NextEffectiveDate
	FROM (
		SELECT TOP 1 ID, ShipperID, ProductGroupID, OriginStateID, DestStateID, RegionID, SubUnitID, RoundingTypeID, ThresholdMinutes, EffectiveDate, EndDate, NextEffectiveDate
			, Ranking =	CASE WHEN X.ShipperID IS NULL THEN 0 ELSE 16 END
					  + CASE WHEN X.ProductGroupID IS NULL THEN 0 ELSE 8 END
					  + CASE WHEN X.OriginStateID IS NULL THEN 0 ELSE 4 END
					  + CASE WHEN X.DestStateID IS NULL THEN 0 ELSE 2 END
					  + CASE WHEN X.RegionID IS NULL THEN 0 ELSE 1 END 
		FROM dbo.viewShipperWaitFeeParameter X 
		WHERE coalesce(ShipperID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @ShipperID END, 0) = isnull(@ShipperID, 0)
		  AND coalesce(ProductGroupID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @ProductGroupID END, 0) = isnull(@ProductGroupID, 0)
		  AND coalesce(OriginStateID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @OriginStateID END, 0) = isnull(@OriginStateID, 0)
		  AND coalesce(DestStateID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @DestStateID END, 0) = isnull(@DestStateID, 0)
		  AND coalesce(RegionID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @RegionID END, 0) = isnull(@RegionID, 0)
		  AND @date BETWEEN EffectiveDate AND EndDate
		ORDER BY Ranking DESC
	) X 
		
GO
GRANT SELECT ON fnShipperWaitFeeParameter TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper WaitFeeParameter info for the specified order
/***********************************/
CREATE FUNCTION fnOrderShipperWaitFeeParameter(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, SubUnitID, RoundingTypeID, ThresholdMinutes
	FROM dbo.viewOrder O
	CROSS APPLY dbo.fnShipperWaitFeeParameter(O.OrderDate, O.CustomerID, O.ProductGroupID, O.OriginStateID, O.DestStateID, O.OriginRegionID, 0) R
	WHERE O.ID = @ID 

GO
GRANT SELECT ON fnOrderShipperWaitFeeParameter TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'viewShipperOriginWaitRate'))
	DROP VIEW viewShipperOriginWaitRate
GO
/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all ShipperOriginWaitRate records
******************************************************/
CREATE VIEW viewShipperOriginWaitRate AS
	SELECT X.*
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementShipper WHERE OriginWaitRateID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblShipperOriginWaitRate XN 
			WHERE isnull(XN.ShipperID, 0) = isnull(X.ShipperID, 0) 
			  AND isnull(XN.ProductGroupID, 0) = isnull(X.ProductGroupID, 0) 
			  AND isnull(XN.OriginID, 0) = isnull(X.OriginID, 0) 
			  AND isnull(XN.StateID, 0) = isnull(X.StateID, 0) 
			  AND isnull(XN.RegionID, 0) = isnull(X.RegionID, 0) 
			  AND XN.EffectiveDate > X.EffectiveDate)
	FROM tblShipperOriginWaitRate X

GO
GRANT SELECT ON viewShipperOriginWaitRate TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fnShipperOriginWaitRate]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION [dbo].[fnShipperOriginWaitRate]
GO
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper OriginWaitRate info for the specified criteria
/***********************************/
CREATE FUNCTION [dbo].[fnShipperOriginWaitRate](
  @Date date
, @ReasonID int
, @ShipperID int
, @ProductGroupID int
, @OriginID int
, @StateID int
, @RegionID int
, @exactMatch bit = 0)
RETURNS TABLE AS RETURN
		SELECT ID, ReasonID, ShipperID, ProductGroupID, OriginID, StateID, RegionID, Rate, EffectiveDate, EndDate, NextEffectiveDate
		FROM (
			SELECT TOP 1 ID, ReasonID, ShipperID, ProductGroupID, OriginID, StateID, RegionID, Rate, EffectiveDate, EndDate, NextEffectiveDate
				, Ranking =	CASE WHEN ReasonID IS NULL THEN 0 ELSE 16 END
						  + CASE WHEN ShipperID IS NULL THEN 0 ELSE 8 END
						  + CASE WHEN ProductGroupID IS NULL THEN 0 ELSE 4 END
						  + CASE WHEN OriginID IS NULL THEN 0 ELSE 2 END
						  + CASE WHEN StateID IS NULL THEN 0 ELSE 2 END
						  + CASE WHEN RegionID IS NULL THEN 0 ELSE 1 END 
			FROM  dbo.viewShipperOriginWaitRate 
			WHERE coalesce(ReasonID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @ReasonID END, 0) = isnull(@ReasonID, 0)
			  AND coalesce(ShipperID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @ShipperID END, 0) = isnull(@ShipperID, 0)
			  AND coalesce(ProductGroupID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @ProductGroupID END, 0) = isnull(@ProductGroupID, 0)
			  AND coalesce(OriginID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @OriginID END, 0) = isnull(@OriginID, 0)
			  AND coalesce(StateID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @StateID END, 0) = isnull(@StateID, 0)
			  AND coalesce(RegionID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @RegionID END, 0) = isnull(@RegionID, 0)
			  AND @Date BETWEEN EffectiveDate AND EndDate
			ORDER BY Ranking DESC
		) X
		
GO
GRANT SELECT ON fnShipperOriginWaitRate TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fnOrderShipperOriginWaitRate]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION [dbo].[fnOrderShipperOriginWaitRate]
GO
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper OriginWaitRate info for the specified order
/***********************************/
CREATE FUNCTION [dbo].[fnOrderShipperOriginWaitRate](@ID int) RETURNS TABLE AS RETURN
		SELECT R.ID, Minutes = O.DestMinutes, R.Rate
		FROM viewOrder O
		CROSS APPLY dbo.fnShipperOriginWaitRate(O.OrderDate, O.DestWaitReasonID, O.CustomerID, O.ProductGroupID, O.OriginID, O.DestStateID, O.DestRegionID, 0) R
		WHERE O.ID = @ID

GO
GRANT SELECT ON fnOrderShipperOriginWaitRate TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper OriginWait data info for the specified order
/***********************************/
CREATE FUNCTION fnOrderShipperOriginWaitData(@ID int) RETURNS TABLE AS RETURN
	SELECT RateID, BillableMinutes, BillableHours, Amount = BillableHours * Rate, WaitFeeParameterID
	FROM (
		SELECT RateID, BillableMinutes
			, BillableHours = dbo.fnComputeBillableWaitHours(BillableMinutes, SubUnitID, RoundingTypeID)
			, Rate
			, WaitFeeParameterID
		FROM (
			SELECT RateID = OWR.ID
				, Rate
				, BillableMinutes = dbo.fnMaxInt(OWR.Minutes - WFP.ThresholdMinutes, 0)
				, WaitFeeParameterID = WFP.ID, WFP.SubUnitID, WFP.RoundingTypeID
			FROM dbo.fnOrderShipperOriginWaitRate(@ID) OWR 
			CROSS APPLY dbo.fnOrderShipperWaitFeeParameter(@ID) WFP
		) X
	) X2
		
GO
GRANT SELECT ON fnOrderShipperOriginWaitData TO dispatchcrude_iis_acct
GO 

IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'viewShipperDestinationWaitRate'))
	DROP VIEW viewShipperDestinationWaitRate
GO
/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all ShipperDestinationWaitRate records
******************************************************/
CREATE VIEW viewShipperDestinationWaitRate AS
	SELECT X.*
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementShipper WHERE DestinationWaitRateID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate)
			FROM tblShipperDestinationWaitRate XN 
			WHERE isnull(XN.ShipperID, 0) = isnull(X.ShipperID, 0) 
			  AND isnull(XN.ProductGroupID, 0) = isnull(X.ProductGroupID, 0) 
			  AND isnull(XN.DestinationID, 0) = isnull(X.DestinationID, 0) 
			  AND isnull(XN.StateID, 0) = isnull(X.StateID, 0) 
			  AND isnull(XN.RegionID, 0) = isnull(X.RegionID, 0) 
			  AND XN.EffectiveDate > X.EffectiveDate)
	FROM tblShipperDestinationWaitRate X

GO
GRANT SELECT ON viewShipperDestinationWaitRate TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fnShipperDestinationWaitRate]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION [dbo].[fnShipperDestinationWaitRate]
GO
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper DestinationWaitRate info for the specified criteria
/***********************************/
CREATE FUNCTION [dbo].[fnShipperDestinationWaitRate](
  @Date date
, @ReasonID int
, @ShipperID int
, @ProductGroupID int
, @DestinationID int
, @StateID int
, @RegionID int
, @exactMatch bit = 0)
RETURNS TABLE AS RETURN
		SELECT ID, ReasonID, ShipperID, ProductGroupID, DestinationID, StateID, RegionID, Rate, EffectiveDate, EndDate, NextEffectiveDate
		FROM (
			SELECT TOP 1 ID, ReasonID, ShipperID, ProductGroupID, DestinationID, StateID, RegionID, Rate, EffectiveDate, EndDate, NextEffectiveDate
				, Ranking =	CASE WHEN ReasonID IS NULL THEN 0 ELSE 16 END
						  + CASE WHEN ShipperID IS NULL THEN 0 ELSE 8 END
						  + CASE WHEN ProductGroupID IS NULL THEN 0 ELSE 4 END
						  + CASE WHEN DestinationID IS NULL THEN 0 ELSE 2 END
						  + CASE WHEN StateID IS NULL THEN 0 ELSE 2 END
						  + CASE WHEN RegionID IS NULL THEN 0 ELSE 1 END 
			FROM  dbo.viewShipperDestinationWaitRate 
			WHERE coalesce(ReasonID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @ReasonID END, 0) = isnull(@ReasonID, 0)
			  AND coalesce(ShipperID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @ShipperID END, 0) = isnull(@ShipperID, 0)
			  AND coalesce(ProductGroupID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @ProductGroupID END, 0) = isnull(@ProductGroupID, 0)
			  AND coalesce(DestinationID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @DestinationID END, 0) = isnull(@DestinationID, 0)
			  AND coalesce(StateID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @StateID END, 0) = isnull(@StateID, 0)
			  AND coalesce(RegionID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @RegionID END, 0) = isnull(@RegionID, 0)
			  AND @Date BETWEEN EffectiveDate AND EndDate
			ORDER BY Ranking DESC
		) X
		
GO
GRANT SELECT ON fnShipperDestinationWaitRate TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fnOrderShipperDestinationWaitRate]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION [dbo].[fnOrderShipperDestinationWaitRate]
GO
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper DestinationWaitRate info for the specified order
/***********************************/
CREATE FUNCTION [dbo].[fnOrderShipperDestinationWaitRate](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, Minutes = O.DestMinutes, R.Rate
	FROM viewOrder O
	CROSS APPLY dbo.fnShipperDestinationWaitRate(O.OrderDate, O.DestWaitReasonID, O.CustomerID, O.ProductGroupID, O.OriginID, O.DestStateID, O.DestRegionID, 0) R
	WHERE O.ID = @ID

GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'fnOrderShipperDestinationWaitData') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION fnOrderShipperDestinationWaitData
GO
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper DestinationWait data info for the specified order
/***********************************/
CREATE FUNCTION fnOrderShipperDestinationWaitData(@ID int) RETURNS TABLE AS RETURN
	SELECT RateID, BillableMinutes, BillableHours, Amount = BillableHours * Rate
	FROM (
		SELECT RateID, BillableMinutes
			, BillableHours = dbo.fnComputeBillableWaitHours(BillableMinutes, SubUnitID, RoundingTypeID)
			, Rate
		FROM (
			SELECT RateID = OWR.ID
				, Rate
				, BillableMinutes = dbo.fnMaxInt(OWR.Minutes - WFP.ThresholdMinutes, 0)
				, WFP.SubUnitID, WFP.RoundingTypeID
			FROM dbo.fnOrderShipperDestinationWaitRate(@ID) OWR 
			CROSS JOIN dbo.fnOrderShipperWaitFeeParameter(@ID) WFP 
		) X
	) X2
		
GO
GRANT SELECT ON fnOrderShipperDestinationWaitData TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'viewShipperOrderRejectRate'))
	DROP VIEW viewShipperOrderRejectRate
GO
/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all ShipperOrderRejectRate records
******************************************************/
CREATE VIEW viewShipperOrderRejectRate AS
	SELECT X.*
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementShipper WHERE OrderRejectRateID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblShipperOrderRejectRate XN 
			WHERE isnull(XN.ShipperID, 0) = isnull(X.ShipperID, 0) 
			  AND isnull(XN.ProductGroupID, 0) = isnull(X.ProductGroupID, 0) 
			  AND isnull(XN.OriginID, 0) = isnull(X.OriginID, 0) 
			  AND isnull(XN.StateID, 0) = isnull(X.StateID, 0) 
			  AND isnull(XN.RegionID, 0) = isnull(X.RegionID, 0) 
			  AND XN.EffectiveDate > X.EffectiveDate)
	FROM tblShipperOrderRejectRate X

GO
GRANT SELECT ON viewShipperOrderRejectRate TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fnShipperOrderRejectRate]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION [dbo].[fnShipperOrderRejectRate]
GO
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper OrderRejectRate info for the specified criteria
/***********************************/
CREATE FUNCTION fnShipperOrderRejectRate(@date date, @ReasonID int, @ShipperID int, @ProductGroupID int, @StateID int, @RegionID int, @exactMatch bit = 0)
RETURNS TABLE AS RETURN
	SELECT ID, ReasonID, ShipperID, ProductGroupID, StateID, RegionID, Rate, RateTypeID, UomID, EffectiveDate, EndDate, NextEffectiveDate
	FROM (
		SELECT TOP 1 ID, ReasonID, ShipperID, ProductGroupID, StateID, RegionID, Rate, RateTypeID, UomID, EffectiveDate, EndDate, NextEffectiveDate
			, Ranking =	CASE WHEN ReasonID IS NULL THEN 0 ELSE 16 END
					  + CASE WHEN ShipperID IS NULL THEN 0 ELSE 8 END
					  + CASE WHEN ProductGroupID IS NULL THEN 0 ELSE 4 END
					  + CASE WHEN StateID IS NULL THEN 0 ELSE 2 END
					  + CASE WHEN RegionID IS NULL THEN 0 ELSE 1 END 
		FROM dbo.viewShipperOrderRejectRate
		WHERE coalesce(ReasonID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @ReasonID END, 0) = isnull(@ReasonID, 0)
		  AND coalesce(ShipperID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @ShipperID END, 0) = isnull(ShipperID, 0)
		  AND coalesce(ProductGroupID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @ProductGroupID END, 0) = isnull(@ProductGroupID, 0)
		  AND coalesce(StateID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @StateID END, 0) = isnull(@StateID, 0)
		  AND coalesce(RegionID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @RegionID END, 0) = isnull(@RegionID, 0)
		  AND @date BETWEEN EffectiveDate AND EndDate
		ORDER BY Ranking DESC
	) X
		
GO
GRANT SELECT ON fnShipperOrderRejectRate TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'fnOrderShipperOrderRejectRate') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION fnOrderShipperOrderRejectRate
GO
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper OrderRejectRate info for the specified order
/***********************************/
CREATE FUNCTION fnOrderShipperOrderRejectRate(@ID int) RETURNS TABLE AS RETURN
	SELECT TOP 1 R.ID, Rate, RateTypeID, UomID
	FROM dbo.viewOrder O
	CROSS APPLY fnShipperOrderRejectRate(O.OrderDate, O.RejectReasonID, O.CustomerID, O.ProductGroupID, O.OriginStateID, O.OriginRegionID, 0) R
	WHERE O.ID = @ID

GO
GRANT SELECT ON fnOrderShipperOrderRejectRate TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'fnOrderShipperOrderRejectData') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION fnOrderShipperOrderRejectData
GO
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper OrderReject data info for the specified order
/***********************************/
CREATE FUNCTION fnOrderShipperOrderRejectData(@ID int, @Units decimal(18, 10), @UomID int) RETURNS TABLE AS RETURN
	SELECT RateID = OWR.ID
		, Amount = dbo.fnRateToAmount(RateTypeID, @Units, @UomID, Rate, UomID
			, CASE WHEN RateTypeID = 3 THEN (SELECT TOP 1 OrderRejectAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID) ELSE NULL END)
	FROM dbo.fnOrderShipperOrderRejectRate(@ID) OWR 
		
GO
GRANT SELECT ON fnOrderShipperOrderRejectData TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fnShipperAssessorialRates]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION [dbo].fnShipperAssessorialRates
GO
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper AssessorialRate info for the specified order
/***********************************/
CREATE FUNCTION fnShipperAssessorialRates(@date date, @ShipperID int, @ProductGroupID int, @OriginID int, @DestinationID int, @OriginStateID int, @DestStateID int, @RegionID int, @ProducerID int, @OperatorID int, @TypeID int, @exactMatch bit = 0)
RETURNS 
	@ret TABLE (
		ID int
	  , ShipperID int
	  , ProductGroupID int
	  , OriginID int
	  , DestinationID int
	  , OriginStateID int
	  , DestStateID int
	  , RegionID int
	  , ProducerID int
	  , OperatorID int
	  , TypeID int
	  , Rate decimal(18, 10)
	  , RateTypeID int
	  , UomID int
	  , EffectiveDate date
	  , EndDate date
	  , NextEffectiveDate date
	)
AS BEGIN
	-- get the raw data (all matched rows)
	DECLARE @src TABLE (ID int, TypeID int, Ranking int)
	INSERT INTO @src (ID, TypeID, Ranking)
		SELECT ID, TypeID
			, Ranking =	CASE WHEN ShipperID IS NULL THEN 0 ELSE 256 END
					  + CASE WHEN ProductGroupID IS NULL THEN 0 ELSE 128 END
					  + CASE WHEN OriginID IS NULL THEN 0 ELSE 64 END
					  + CASE WHEN DestinationID IS NULL THEN 0 ELSE 32 END
					  + CASE WHEN OriginStateID IS NULL THEN 0 ELSE 16 END
					  + CASE WHEN DestStateID IS NULL THEN 0 ELSE 8 END
					  + CASE WHEN RegionID IS NULL THEN 0 ELSE 4 END 
					  + CASE WHEN ProducerID IS NULL THEN 0 ELSE 2 END 
					  + CASE WHEN OperatorID IS NULL THEN 0 ELSE 1 END 
		FROM dbo.viewShipperAssessorialRate
		WHERE coalesce(ShipperID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @ShipperID END, 0) = isnull(@ShipperID, 0)
		  AND coalesce(ProductGroupID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @ProductGroupID END, 0) = isnull(@ProductGroupID, 0)
		  AND coalesce(OriginID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @OriginID END, 0) = isnull(@OriginID, 0)
		  AND coalesce(DestinationID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @DestinationID END, 0) = isnull(@DestinationID, 0)
		  AND coalesce(OriginStateID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @OriginStateID END, 0) = isnull(@OriginStateID, 0)
		  AND coalesce(DestStateID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @DestStateID END, 0) = isnull(@DestStateID, 0)
		  AND coalesce(RegionID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @RegionID END, 0) = isnull(@RegionID, 0)
		  AND coalesce(ProducerID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @ProducerID END, 0) = isnull(@ProducerID, 0)
		  AND coalesce(OperatorID, CASE WHEN @exactMatch = 1 THEN 0 ELSE @OperatorID END, 0) = isnull(@OperatorID, 0)
		  AND @date BETWEEN EffectiveDate AND EndDate
		  AND (@TypeID IS NULL OR TypeID = @TypeID)
		  
	-- return the rate data for the best-match rates 
	INSERT INTO @ret (ID, ShipperID, ProductGroupID, OriginID, DestinationID, OriginStateID, DestStateID, RegionID, ProducerID, OperatorID, TypeID, Rate, RateTypeID, UomID, EffectiveDate, EndDate, NextEffectiveDate)
		SELECT CAR.ID, ShipperID, ProductGroupID, OriginID, DestinationID, OriginStateID, DestStateID, RegionID, ProducerID, OperatorID, TypeID, Rate, RateTypeID, UomID, EffectiveDate, EndDate, NextEffectiveDate
		FROM viewShipperAssessorialRate CAR
		JOIN (
			SELECT ID
			FROM @src S
			JOIN (
				SELECT TypeID, Ranking = MAX(Ranking)
				FROM @src
				GROUP BY TypeID
			) X ON X.TypeID = S.TypeID AND X.Ranking = S.Ranking
		) X ON X.ID = CAR.ID
	RETURN
END
GO
GRANT SELECT ON fnShipperAssessorialRates TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'fnOrderShipperAssessorialRates') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION fnOrderShipperAssessorialRates
GO
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper AssessorialRate info for the specified order
/***********************************/
CREATE FUNCTION fnOrderShipperAssessorialRates(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, TypeID, Rate, RateTypeID, UomID
	FROM dbo.viewOrder O
	CROSS APPLY dbo.fnShipperAssessorialRates(O.OrderDate, O.CustomerID, O.ProductGroupID, O.OriginID, O.DestinationID, O.OriginStateID, O.DestStateID, O.OriginRegionID, O.ProducerID, O.OperatorID, NULL, 0) R
	WHERE O.ID = @ID
	  AND ((R.TypeID = 1 AND O.ChainUp = 1)
		OR (R.TypeID = 2 AND O.RerouteCount > 0)
		OR (R.TypeID = 3 AND O.TicketCount > 1)
		OR (R.TypeID = 4 AND O.H2S = 1)
		OR R.TypeID > 4)

GO
GRANT SELECT ON fnOrderShipperAssessorialRates TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'fnOrderShipperAssessorialAmounts') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION fnOrderShipperAssessorialAmounts
GO
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper AssessorialRate Amounts info for the specified order
/***********************************/
CREATE FUNCTION fnOrderShipperAssessorialAmounts(@ID int, @Units decimal(18, 10), @UomID int) RETURNS TABLE AS RETURN
	SELECT AssessorialRateTypeID = TypeID, RateID, RateTypeID, Amount
	FROM (
		SELECT TypeID, RateID = ID, RateTypeID
			, Amount = dbo.fnRateToAmount(RateTypeID, @Units, @UomID, Rate, UomID
			, CASE WHEN RateTypeID = 3 THEN (SELECT TOP 1 Amount FROM tblOrderSettlementShipperAssessorialCharge WHERE OrderID = @ID AND AssessorialRateTypeID = TypeID) 
				   ELSE NULL END)
		FROM dbo.fnOrderShipperAssessorialRates(@ID)
	) X
	WHERE Amount IS NOT NULL

GO
GRANT SELECT ON fnOrderShipperAssessorialAmounts TO dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'fnShipperFuelSurchargeRate') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION fnShipperFuelSurchargeRate
GO
/***********************************/
-- Date Created: 23 Dec 20143
-- Author: Kevin Alons
-- Purpose: compute the appropriate ShipperFuelSurcharge for the specified parameters
/***********************************/
CREATE FUNCTION [dbo].[fnShipperFuelSurchargeRate](@OriginID int, @ShipperID int, @EffectiveDate smalldatetime) 
RETURNS 
	@ret TABLE
	(
	    RateID int
	  , Rate money
	)
BEGIN
	DECLARE @rateID int, @rate money, @fuelPriceFloor money, @intervalAmount money, @incrementAmount money, @pricediff money	
	
	SELECT TOP 1 @rateID = ID, @fuelPriceFloor = FuelPriceFloor, @intervalAmount = IntervalAmount, @incrementAmount = IncrementAmount
		, @pricediff = dbo.fnMaxDecimal(0, dbo.fnFuelPriceForOrigin(@OriginID, @effectiveDate) - fuelPriceFloor)
	FROM tblShipperFuelSurchargeRates 
	WHERE ShipperID = @ShipperID AND EffectiveDate <= @EffectiveDate
	ORDER BY EffectiveDate DESC
	
	SELECT @rate = @incrementAmount * (round(@pricediff / @intervalAmount, 0) + CASE WHEN @pricediff % @intervalAmount > 0 THEN 1 ELSe 0 END)
	
	INSERT INTO @ret (RateID, Rate) VALUES (@rateID, @rate)
		
	RETURN 
END

GO
GRANT SELECT ON dbo.fnShipperFuelSurchargeRate TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Shipper "Settlement" $$ values to an Delivered/Audited order
/***********************************/
CREATE VIEW viewOrderSettlementUnitsShipper AS
	SELECT X.*
		, SettlementUnits = dbo.fnMaxDecimal(X.ActualUnits, X.MinSettlementUnits)
	FROM (
		SELECT OrderID = O.ID
			, O.CustomerID
			, SettlementUomID = O.OriginUomID
			, C.SettlementFactorID
			, MinSettlementUnits = isnull(dbo.fnConvertUOM(C.MinSettlementUnits, C.MinSettlementUomID, O.OriginUomID), 0) 
			, ActualUnits = CASE C.SettlementFactorID	WHEN 1 THEN O.OriginGrossUnits 
														WHEN 3 THEN isnull(O.OriginGrossStdUnits, O.OriginGrossUnits) 
														ELSE coalesce(O.OriginNetUnits, O.OriginGrossStdUnits, O.OriginGrossUnits, 0) END 
		FROM dbo.tblOrder O
		JOIN tblCustomer C ON C.ID = O.CustomerID
	) X
GO
GRANT SELECT ON viewOrderSettlementUnitsShipper TO dispatchcrude_iis_acct
GO

DROP PROCEDURE spProcessCustomerInvoice
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'spProcessShipperInvoice') AND type in (N'P', N'PC'))
	DROP PROCEDURE spProcessShipperInvoice
GO
/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Shipper "Settlement" $$ values to an Delivered/Audited order
/***********************************/
CREATE PROCEDURE spProcessShipperInvoice
(
  @ID int
, @UserName varchar(100)
, @OriginWaitAmount money = NULL
, @DestWaitAmount money = NULL
, @RejectionAmount money = NULL
, @FuelSurchargeAmount money = NULL
, @LoadAmount money = NULL
, @AssessorialRateTypeID_CSV varchar(max) = NULL
, @AssessorialAmount_CSV varchar(max) = null
, @ResetOverrides bit = 0
) AS BEGIN

	SET NOCOUNT ON
	
	-- ensure this order hasn't yet been fully settled
	IF EXISTS (SELECT * FROM tblOrderSettlementShipper WHERE OrderID = @ID AND BatchID IS NOT NULL)
	BEGIN
		RAISERROR('Invoice has already been fully settled', 16, 1)
		RETURN
	END

	-- ensure that Shipper Rates have been applied prior to applying Shipper rates (since they could be dependent on Shipper rates)
	IF NOT EXISTS (SELECT * FROM tblOrderSettlementShipper WHERE OrderID = @ID)
		EXEC spProcessShipperInvoice @ID, @UserName

	/*
drop table #i
drop table #ia

declare @id int, @userName varchar(100)
, @OriginWaitAmount money 
, @DestWaitAmount money 
, @RejectionAmount money 
, @FuelSurchargeAmount money 
, @LoadAmount money 
, @AssessorialRateTypeID_CSV varchar(max) 
, @AssessorialAmount_CSV varchar(max) 
, @ResetOverrides bit 
select @ID=66969,@UserName='kalons'
--select @OriginWaitAmount=1.0000,@DestWaitAmount=2.0000,@RejectionAmount=3.0000,@FuelSurchargeAmount=4.0000,@LoadAmount=5.0000,@AssessorialRateTypeID_CSV='1',@AssessorialAmount_CSV='99'
select @ResetOverrides=0
--	*/
	-- get the provided manual Assessorial Rates (if any) into a usable able
	DECLARE @ManualAssessorialRates TABLE (ID int primary key, Amount money)
	INSERT INTO @ManualAssessorialRates (ID, Amount) 
		SELECT ID.ID, R.Amount
		FROM (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), ID FROM dbo.fnSplitCSVIDs(@AssessorialRateTypeID_CSV)) ID
		JOIN (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), amount = CAST(value as money) FROM dbo.fnSplitCSV(@AssessorialAmount_CSV)) R ON R.sortid = ID.sortid

	-- add in an existing manual Assessorial Rates from the existing Order Settlement (if any)
	INSERT INTO @ManualAssessorialRates (ID, Amount)
		SELECT AssessorialRateTypeID, Amount 
		FROM dbo.tblOrderSettlementShipperAssessorialCharge 
		WHERE OrderID = @ID AND AssessorialRateID IS NULL 
			AND ID NOT IN (SELECT ID FROM @ManualAssessorialRates)
			AND @ResetOverrides = 0
--select * from @ManualAssessorialRates

	IF (@ResetOverrides = 0)
	BEGIN 
		SELECT @OriginWaitAmount = OriginWaitAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @OriginWaitAmount IS NULL AND OriginWaitRateID IS NULL
		SELECT @DestWaitAmount = DestinationWaitAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @DestWaitAmount IS NULL AND DestinationWaitRateID IS NULL
		SELECT @RejectionAmount = OrderRejectAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @RejectionAmount IS NULL AND OrderRejectRateID IS NULL
		SELECT @FuelSurchargeAmount = FuelSurchargeAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @FuelSurchargeAmount IS NULL AND FuelSurchargeRateID IS NULL
		SELECT @LoadAmount = LoadAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @LoadAmount IS NULL AND RouteRateID IS NULL AND RangeRateID IS NULL
	END
--select OverrideOriginWaitAmount = @OriginWaitAmount, OverrideDestWaitAmount = @DestWaitAmount, OverrideRejectionAmount = @RejectionAmount, OverrideFuelSurchargeAmount = @FuelSurchargeAmount, OverrideLoadAmount = @LoadAmount

	DECLARE @SettlementUnits TABLE
	(
		OrderID int
	  , SettlementUomID int
	  , SettlementFactorID int
	  , MinSettlementUnits decimal(18, 10)
	  , ActualUnits decimal(18, 10)
	  , SettlementUnits decimal(18, 10)
	)
	INSERT INTO @SettlementUnits (OrderID, SettlementUomID, SettlementFactorID, MinSettlementUnits, ActualUnits, SettlementUnits)
		SELECT OrderID, SettlementUomID, SettlementFactorID, MinSettlementUnits, ActualUnits, SettlementUnits
		FROM dbo.viewOrderSettlementUnitsShipper 
		WHERE OrderID = @ID

	SELECT OrderID = @ID
		, OrderDate
		, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
		, RouteRateID, RangeRateID, LoadAmount
		, WaitFeeParameterID
		, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount 
		, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount
		, OrderRejectRateID, OrderRejectAmount
		, FuelSurchargeRateID, FuelSurchargeRate, FuelSurchargeAmount
		, OriginTaxRate
		, TotalAmount = ISNULL(LoadAmount, 0) 
			+ ISNULL(OriginWaitAmount, 0) 
			+ ISNULL(DestWaitAmount, 0) 
			+ ISNULL(OrderRejectAmount, 0) 
			+ ISNULL(FuelSurchargeAmount, 0)
		, CreatedByUser = @UserName
	INTO #I
	FROM (
		SELECT OrderDate
			, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
			, RouteRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RouteRateID ELSE NULL END
			, RangeRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RangeRateID ELSE NULL END
			, LoadAmount = ISNULL(@LoadAmount, RR.Amount)
			, WaitFeeParameterID
			, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount
			, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount
			, OrderRejectRateID = CASE WHEN @RejectionAmount IS NULL THEN RD.RateID ELSE NULL END
			, OrderRejectAmount = isnull(@RejectionAmount, RD.Amount)
			, FuelSurchargeRateID = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.RateID ELSE NULL END
			, FuelSurchargeRate = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.Rate ELSE NULL END
			, FuelSurchargeAmount = isnull(@FuelSurchargeAmount, FSR.Rate * X.RouteMiles)
			, OriginTaxRate
		FROM (
			SELECT O.ID
				, O.OriginID
				, O.CustomerID
				, O.RouteID
				, SU.SettlementFactorID
				, SU.SettlementUomID
				, SU.ActualUnits
				, SU.MinSettlementUnits
				, SU.SettlementUnits
				, RouteMiles = O.ActualMiles
				, O.OrderDate
				, O.ChainUp
				, O.H2S
				, O.Rejected
				, O.RerouteCount
				, IsSplit = CASE WHEN O.Rejected = 0 AND O.TicketCount > 1 THEN 1 ELSE 0 END
				, OWD.WaitFeeParameterID
				, OriginWaitRateID = CASE WHEN @OriginWaitAmount IS NULL THEN OWD.RateID ELSE NULL END
				, OriginWaitBillableMinutes = OWD.BillableMinutes
				, OriginWaitBillableHours = OWD.BillableHours
				, OriginWaitAmount = isnull(@OriginWaitAmount, OWD.Amount)
				, DestWaitRateID = CASE WHEN @DestWaitAmount IS NULL THEN DWD.RateID ELSE NULL END
				, DestWaitBillableMinutes = DWD.BillableMinutes
				, DestWaitBillableHours = DWD.BillableHours
				, DestWaitAmount = isnull(@DestWaitAmount, DWD.Amount)
				, OriginTaxRate = OO.TaxRate
			FROM dbo.viewOrder O
			JOIN @SettlementUnits SU ON SU.OrderID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			OUTER APPLY dbo.fnOrderShipperOriginWaitData(@ID) OWD 
			OUTER APPLY dbo.fnOrderShipperDestinationWaitData(@ID) DWD 
			WHERE O.ID = @ID
		) X
		OUTER APPLY dbo.fnOrderShipperLoadAmount(@ID, SettlementUnits, SettlementUomID) RR 
		OUTER APPLY dbo.fnOrderShipperOrderRejectData(@ID, SettlementUnits, SettlementUomID) RD
		OUTER APPLY dbo.fnShipperFuelSurchargeRate(X.OriginID, X.CustomerID, X.OrderDate) FSR
	) X2
--select * from #I

	SELECT OrderID = @ID, AssessorialRateTypeID, AssessorialRateID = RateID, Amount, CreatedByUser = @UserName 
	INTO #IA
	FROM @SettlementUnits SU
	CROSS APPLY dbo.fnOrderShipperAssessorialAmounts(@ID, SU.SettlementUnits, SU.SettlementUomID) CAA
	WHERE CAA.AssessorialRateTypeID NOT IN (SELECT ID FROM @ManualAssessorialRates)
/*
select OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser from #IA
union
select @ID, ID, NULL, Amount, @UserName from @ManualAssessorialRates
--*/
	DECLARE @CreatedTran bit; SET @CreatedTran = CASE WHEN @@TRANCOUNT = 0 THEN 1 ELSE 0 END
	BEGIN TRY
		IF (@CreatedTran = 1)
			BEGIN TRAN

		-- remove the existing settlment record (if any)
		DELETE FROM tblOrderSettlementShipperAssessorialCharge WHERE OrderID = @ID
		DELETE FROM tblOrderSettlementShipper WHERE OrderID = @ID
	
		-- all Units and Rates are first normalized to the Order.OriginUOM then consistent processing
		-- and persisted in the Invoice (Settlement) record in this Order.OriginUOM 
		INSERT INTO tblOrderSettlementShipper (
			OrderID
			, OrderDate
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestinationWaitRateID 
			, DestinationWaitBillableMinutes 
			, DestinationWaitBillableHours
			, DestinationWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser)
		SELECT OrderID
			, OrderDate
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestWaitRateID 
			, DestWaitBillableMinutes 
			, DestWaitBillableHours
			, DestWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser
		FROM #I

		INSERT INTO tblOrderSettlementShipperAssessorialCharge (OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser)
			SELECT OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser FROM #IA
			UNION
			SELECT @ID, ID, NULL, Amount, @UserName FROM @ManualAssessorialRates
	
		IF (@CreatedTran = 1)
			COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @msg varchar(max), @severity int
		SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY()
		ROLLBACK
		RAISERROR(@msg, @severity, 1)
		RETURN
	END CATCH	
	
END

GO

/*************************************/
-- Date Created: 27 Dec 2014
-- Author: Kevin Alons
-- Purpose: ensure the TotalAmount always reflects the sum of the individual amounts (including associated Assessorial charges)
/*************************************/
CREATE TRIGGER trigOrderSettlementShipper_IU ON tblOrderSettlementShipper FOR INSERT, UPDATE  AS
BEGIN
	-- Note: the TRIGGER_NESTLEVEL( OBJECT_ID('trigOrder_IU') ) = 0 statement prevents recursion
	IF (TRIGGER_NESTLEVEL( OBJECT_ID('trigOrderSettlementShipper_IU')) = 1
		AND (
			UPDATE(LoadAmount)
			OR UPDATE(OrderRejectAmount)
			OR UPDATE(OriginWaitAmount)
			OR UPDATE(DestinationWaitAmount)
			OR UPDATE(FuelSurchargeAmount)
			)
		)
	BEGIN
		UPDATE tblOrderSettlementShipper
			SET TotalAmount = isnull(SC.LoadAmount, 0) 
				+ isnull(SC.OrderRejectAmount, 0) 
				+ isnull(SC.OriginWaitAmount, 0) 
				+ isnull(SC.DestinationWaitAmount, 0)
				+ isnull(SC.FuelSurchargeAmount, 0)
				+ isnull((SELECT SUM(Amount) FROM tblOrderSettlementShipperAssessorialCharge WHERE OrderID = SC.OrderID), 0)
		FROM tblOrderSettlementShipper SC
		JOIN inserted i ON i.OrderID = SC.OrderID 
	END
END
GO

/*************************************/
-- Date Created: 27 Dec 2014
-- Author: Kevin Alons
-- Purpose: ensure the TotalAmount always reflects the sum of the individual amounts (including associated Assessorial charges)
/*************************************/
CREATE TRIGGER trigOrderSettlementShipperAssessorialCharge_IU ON tblOrderSettlementShipperAssessorialCharge FOR INSERT, UPDATE  AS
BEGIN
	IF (UPDATE(Amount))
	BEGIN
		UPDATE tblOrderSettlementShipper
			SET TotalAmount = 0
		FROM tblOrderSettlementShipper SC
		WHERE OrderID IN (SELECT DISTINCT OrderID FROM inserted)
	END
END
GO

---------------------------------------------------------------
-- END SHIPPER SETTLEMENT CHANGES
---------------------------------------------------------------

/****************************************/
-- Date Created: 31 Dec 2014
-- Author: Kevin Alons
-- Purpose: apply both Shipper & Carrier rates (in the appropriate order)
/****************************************/
CREATE PROCEDURE spProcessOrderSettlement(@ID int, @UserName varchar(255), @ResetOverrides bit = 0) AS
BEGIN
	BEGIN TRY
		EXEC spProcessShipperInvoice @ID = @ID, @UserName = @UserName, @ResetOverrides = @ResetOverrides
		EXEC spProcessCarrierInvoice @ID = @ID, @UserName = @UserName, @ResetOverrides = @ResetOverrides
	END TRY
	BEGIN CATCH
		DECLARE @msg varchar(max), @severity int
		SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY()
		RAISERROR(@msg, @severity, 1)
	END CATCH
END
GO
GRANT EXECUTE ON spProcessOrderSettlement TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 28 Feb 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
/***********************************/
ALTER VIEW [dbo].[viewOrderExportFull] AS
SELECT *
  , TotalMinutes = OriginMinutes + DestMinutes 
  , TotalWaitMinutes = isnull(OriginWaitMinutes, 0) + isnull(DestWaitMinutes, 0)
FROM (
	SELECT O.*
	  , OriginWaitMinutes = dbo.fnMaxInt(0, isnull(OriginMinutes, 0) - cast(S.Value as int)) 
	  , DestWaitMinutes = dbo.fnMaxInt(0, isnull(DestMinutes, 0) - cast(S.Value as int)) 
	FROM dbo.viewOrderLocalDates O
	JOIN dbo.tblSetting S ON S.ID = 7 -- the Unbillable Wait Time threshold minutes 
WHERE O.DeleteDateUTC IS NULL
) v

GO

DROP TABLE tblCarrierRouteRates
DROP TABLE tblCarrierDestinationWaitRates
DROP TABLE tblCarrierOriginWaitRates
DROP TABLE tblCarrierOrderRejectRates
DROP TABLE tblCustomerRouteRates
DROP TABLE tblCustomerRates
DROP TABLE tblcustomerdestinationwaitrates
DROP TABLE tblcustomeroriginwaitrates
DROP TABLE tblcustomerorderrejectrates
DROP VIEW viewCarrierDestinationWaitRatesBase
DROP VIEW viewCarrierDestinationWaitRates
DROP VIEW viewCarrierOriginWaitRatesBase
DROP VIEW viewCarrierOriginWaitRates
DROP VIEW viewCarrierOrderRejectRatesBase
DROP VIEW viewCarrierOrderRejectRates
DROP VIEW viewCarrierRouteRatesBase
DROP VIEW viewCarrierRouteRates
DROP VIEW viewCustomerRouteRatesBase
DROP VIEW viewCustomerRouteRates
DROP VIEW viewCustomerDestinationWaitRatesBase
DROP VIEW viewCustomerDestinationWaitRates
DROP VIEW viewCustomerOriginWaitRatesBase
DROP VIEW viewCustomerOriginWaitRates
DROP VIEW viewCustomerOrderRejectRatesBase
DROP VIEW viewCustomerOrderRejectRates
GO

DROP TABLE tblOrderInvoiceCustomer
DROP TABLE tblOrderInvoiceCarrier
GO

/***********************************/
-- Date Created: 20 Sep 2014
-- Author: Kevin Alons
-- Purpose: return Carrier metrics for all Order Statuses
/***********************************/
ALTER VIEW [dbo].[viewCarrier_RouteMetrics] AS
	SELECT O.CarrierID
		, O.RouteID
		, O.ProductGroupID
		, O.OriginID, O.DestinationID
		, OrderCount = COUNT(1)
		, MinOrderDate = cast(MIN(O.OriginDepartTimeUTC) as date)
		, O.StatusID
		, IsSettled = CASE WHEN ISC.BatchID IS NULL THEN 0 ELSE 1 END
	FROM viewOrder O
	LEFT JOIN tblOrderSettlementCarrier ISC ON ISC.OrderID = O.ID
	WHERE DeleteDateUTC IS NULL 
		AND RouteID IS NOT NULL 
		AND CarrierID IS NOT NULL
	GROUP BY CarrierID, RouteID, ProductGroupID
		, O.OriginID, O.DestinationID
		, StatusID, CASE WHEN ISC.BatchID IS NULL THEN 0 ELSE 1 END

GO

DROP VIEW viewCustomer_RouteMetrics
GO

/***********************************/
-- Date Created: 20 Sep 2014
-- Author: Kevin Alons
-- Purpose: return Shipper metrics for all Order Statuses
/***********************************/
CREATE VIEW [dbo].[viewShipper_RouteMetrics] AS
	SELECT O.CustomerID
		, O.RouteID
		, O.ProductGroupID
		, O.OriginID, O.DestinationID
		, OrderCount = COUNT(1)
		, MinOrderDate = cast(MIN(O.OriginDepartTimeUTC) as date)
		, O.StatusID
		, IsSettled = CASE WHEN ISC.BatchID IS NULL THEN 0 ELSE 1 END
	FROM viewOrder O
	LEFT JOIN tblOrderSettlementShipper ISC ON ISC.OrderID = O.ID
	WHERE DeleteDateUTC IS NULL 
		AND RouteID IS NOT NULL 
		AND CustomerID IS NOT NULL
	GROUP BY CustomerID, RouteID, ProductGroupID
		, O.OriginID, O.DestinationID
		, StatusID, CASE WHEN ISC.BatchID IS NULL THEN 0 ELSE 1 END

GO
GRANT SELECT ON viewShipper_RouteMetrics TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 15 Aug 2013
-- Author: Kevin Alons
-- Purpose: return the CarrierSettlementBatch table contents with the CreateDate formatted DateOnly (for the BatchDate)
/***********************************/
ALTER VIEW [dbo].[viewCarrierSettlementBatch] AS
SELECT x.*
	, P.Name AS Product, P.ShortName AS ShortProduct
	, '#' + ltrim(BatchNum) + ' [' + dbo.fnDateMdYY(BatchDate) 
		+ '] ' + ISNULL(P.Name, '') + ' / ' + ltrim(OrderCount) AS FullName
FROM (
	SELECT SB.*
		, CASE WHEN C.Active = 1 THEN '' ELSE 'Deleted: ' END + C.Name AS Carrier
		, (SELECT COUNT(*) FROM tblOrderSettlementCarrier WHERE BatchID = SB.ID) AS OrderCount
	FROM dbo.tblCarrierSettlementBatch SB
	JOIN dbo.viewCarrier C ON C.ID = SB.CarrierID
) x
LEFT JOIN dbo.tblProduct P ON P.ID = x.ProductID

GO

DROP VIEW viewCustomerSettlementBatch
GO

/***********************************/
-- Date Created: 15 Aug 2013
-- Author: Kevin Alons
-- Purpose: return the ShipperSettlementBatch table contents with the CreateDate formatted DateOnly (for the BatchDate)
/***********************************/
CREATE VIEW [dbo].[viewShipperSettlementBatch] AS
SELECT x.*
	, P.Name AS Product, P.ShortName AS ShortProduct
	, '#' + ltrim(BatchNum) + ' [' + dbo.fnDateMdYY(BatchDate) 
		+ '] ' + ISNULL(P.Name, '') + ' / ' + ltrim(OrderCount) AS FullName
FROM (
	SELECT SB.*
		, Shipper = C.Name 
		, (SELECT COUNT(*) FROM tblOrderSettlementShipper WHERE BatchID = SB.ID) AS OrderCount
	FROM dbo.tblCustomerSettlementBatch SB
	JOIN dbo.viewCustomer C ON C.ID = SB.CustomerID
) x
LEFT JOIN dbo.tblProduct P ON P.ID = x.ProductID

GO
GRANT SELECT ON viewShipperSettlementBatch TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Report Center Order data
/***********************************/
ALTER VIEW [dbo].[viewReportCenter_Orders] AS
	SELECT O.*
		, ShipperBatchNum = SS.BatchNum
		, ShipperSettlementUomID = SS.SettlementUomID
		, ShipperSettlementUom = SS.SettlementUom
		, ShipperMinSettlementUnits = SS.MinSettlementUnits
		, ShipperUnits = SS.SettlementUnits
		, ShipperRateSheetRate = SS.RateSheetRate
		, ShipperRateSheetRateType = SS.RateSheetRateType
		, ShipperRouteRate = SS.RouteRate
		, ShipperRouteRateType = SS.RouteRateType
		, ShipperLoadRate = isnull(SS.RouteRate, SS.RateSheetRate)
		, ShipperLoadRateType = isnull(SS.RouteRateType, SS.RateSheetRateType)
		, ShipperLoadAmount = SS.LoadAmount
		, ShipperRejectRate = SS.OrderRejectRate
		, ShipperRejectAmount = SS.OrderRejectAmount
		, ShipperOriginWaitRate = SS.OriginWaitRate
		, ShipperOriginWaitAmount = SS.OriginWaitAmount
		, ShipperDestWaitRate = SS.DestinationWaitRate
		, ShipperDestWaitAmount = SS.DestinationWaitAmount
		, ShipperTotalWaitAmount = SS.TotalWaitAmount

		, ShipperFuelSurchargeRate = SS.FuelSurchargeRate
		, ShipperFuelSurchargeAmount = SS.FuelSurchargeAmount

		, ShipperChainupRate = SS.ChainupRate
		, ShipperChainupAmount = SS.ChainupAmount
		, ShipperRerouteRate = SS.RerouteRate
		, ShipperRerouteAmount = SS.RerouteAmount
		, ShipperSplitLoadRate = SS.SplitLoadRate
		, ShipperSplitLoadAmount = SS.SplitLoadAmount
		, ShipperH2SRate = SS.H2SRate
		, ShipperH2SAmount = SS.H2SAmount

		, ShipperTaxRate = SS.OriginTaxRate
		, ShipperTotalAmount = SS.TotalAmount

		, CarrierBatchNum = SS.BatchNum
		, CarrierSettlementUomID = SS.SettlementUomID
		, CarrierSettlementUom = SS.SettlementUom
		, CarrierMinSettlementUnits = SS.MinSettlementUnits
		, CarrierUnits = SS.SettlementUnits
		, CarrierRateSheetRate = SS.RateSheetRate
		, CarrierRateSheetRateType = SS.RateSheetRateType
		, CarrierRouteRate = SS.RouteRate
		, CarrierRouteRateType = SS.RouteRateType
		, CarrierLoadRate = isnull(SS.RouteRate, SS.RateSheetRate)
		, CarrierLoadRateType = isnull(SS.RouteRateType, SS.RateSheetRateType)
		, CarrierLoadAmount = SS.LoadAmount
		, CarrierRejectRate = SS.OrderRejectRate
		, CarrierRejectAmount = SS.OrderRejectAmount
		, CarrierOriginWaitRate = SS.OriginWaitRate
		, CarrierOriginWaitAmount = SS.OriginWaitAmount
		, CarrierDestWaitRate = SS.DestinationWaitRate
		, CarrierDestWaitAmount = SS.DestinationWaitAmount
		, CarrierTotalWaitAmount = SS.TotalWaitAmount

		, CarrierFuelSurchargeRate = SS.FuelSurchargeRate
		, CarrierFuelSurchargeAmount = SS.FuelSurchargeAmount

		, CarrierChainupRate = SS.ChainupRate
		, CarrierChainupAmount = SS.ChainupAmount
		, CarrierRerouteRate = SS.RerouteRate
		, CarrierRerouteAmount = SS.RerouteAmount
		, CarrierSplitLoadRate = SS.SplitLoadRate
		, CarrierSplitLoadAmount = SS.SplitLoadAmount
		, CarrierH2SRate = SS.H2SRate
		, CarrierH2SAmount = SS.H2SAmount

		, CarrierTaxRate = SS.OriginTaxRate
		, CarrierTotalAmount = SS.TotalAmount

		, OriginGpsLatLon = ltrim(DLO.Lat) + ',' + ltrim(DLO.Lon)
		, OriginLatLon = ltrim(OO.LAT) + ',' + ltrim(OO.LON)
		, OriginAccuracyMeters = DLO.SourceAccuracyMeters
		, OriginDistance = isnull(cast(DLO.DistanceToPoint as int), 99999)
		, OriginGpsArrived = CASE WHEN DLO.DistanceToPoint IS NOT NULL AND DLO.DistanceToPoint <= OO.GeoFenceRadiusMeters THEN 1 ELSE 0 END
		, DestGpsLatLon = ltrim(DLD.Lat) + ',' + ltrim(DLD.Lon)
		, DestLatLon = ltrim(D.LAT) + ',' + ltrim(D.LON)
		, DestAccuracyMeters = DLD.SourceAccuracyMeters
		, DestDistance = isnull(cast(DLD.DistanceToPoint as int), 99999)
		, DestGpsArrived = CASE WHEN DLD.DistanceToPoint IS NOT NULL AND DLD.DistanceToPoint <= OO.GeoFenceRadiusMeters THEN 1 ELSE 0 END
		, ShipperDestCode = CDC.Code
	FROM viewOrder_OrderTicket_Full O
	JOIN tblOrigin OO ON OO.ID = O.OriginID
	JOIN tblDestination D ON D.ID = O.DestinationID
	LEFT JOIN tblCustomerDestinationCode CDC ON CDC.CustomerID = O.CustomerID AND CDC.DestinationID = O.DestinationID
	LEFT JOIN viewDriverLocation_OriginFirstArrive DLO ON DLO.OrderID = O.ID AND DLO.OriginID = O.OriginID
	LEFT JOIN viewDriverLocation_DestinationFirstArrive DLD ON DLD.OrderID = O.ID AND DLD.DestinationID = O.DestinationID
	LEFT JOIN viewOrderSettlementCarrier SC ON SC.OrderID = O.ID
	LEFT JOIN viewOrderSettlementShipper SS ON SS.OrderID = O.ID
	LEFT JOIN tblOrderSettlementShipper IOSS ON IOSS.OrderID = O.ID
	LEFT JOIN tblCustomerSettlementBatch SSB ON SSB.ID = IOSS.BatchID
GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details + FINANCIAL INFO into a single view
/***********************************/
ALTER VIEW viewOrder_Financial_Carrier AS 
	SELECT O.* 
		, TicketNums = dbo.fnOrderTicketDetails(O.ID, 'TicketNums', '<br/>') 
		, TankNums = dbo.fnOrderTicketDetails(O.ID, 'TankNums', '<br/>') 
		, PreviousDestinations = dbo.fnRerouteDetails(O.ID, 'PreviousDestinations', '<br/>') 
		, RerouteUsers = dbo.fnRerouteDetails(O.ID, 'RerouteUsers', '<br/>') 
		, RerouteDates = dbo.fnRerouteDetails(O.ID, 'RerouteDates', '<br/>') 
		, RerouteNotes = dbo.fnRerouteDetails(O.ID, 'RerouteNotes', '<br/>') 
		, InvoiceRatesAppliedDate = dbo.fnUTC_To_Local(OIC.CreateDateUTC, O.OriginTimeZoneID, O.OriginUseDST)
		, OIC.BatchID
		, InvoiceBatchNum = OIC.BatchNum 
		, InvoiceOriginBillableWaitMinutes = OIC.OriginWaitBillableMinutes
		, InvoiceDestinationBillableWaitMinutes = OIC.DestinationWaitBillableMinutes
		, InvoiceTotalBillableWaitMinutes = isnull(OIC.OriginWaitBillableMinutes, 0) + ISNULL(OIC.DestinationWaitBillableMinutes, 0) 
		, InvoiceWaitFeeSubUnit = WaitFeeSubUnit
		, InvoiceWaitFeeRoundingType = WaitFeeRoundingType
		, InvoiceOriginWaitRate = OIC.OriginWaitRate 
		, InvoiceOriginWaitAmount = OIC.OriginWaitAmount 		
		, InvoiceDestinationWaitRate = OIC.DestinationWaitRate 
		, InvoiceDestinationWaitAmount = OIC.DestinationWaitAmount 
		, InvoiceTotalWaitAmount = OIC.TotalWaitAmount
		, InvoiceRejectionRate = OIC.OrderRejectRate
		, InvoiceRejectionRateType = OIC.OrderRejectRateType
		, InvoiceRejectionAmount = OIC.OrderRejectAmount 
		, InvoiceChainupRate = OIC.ChainupRate
		, InvoiceChainupRateType = OIC.ChainupRateType
		, InvoiceChainupAmount = OIC.ChainupAmount 
		, InvoiceRerouteRate = OIC.RerouteRate
		, InvoiceRerouteRateType = OIC.RerouteRateType
		, InvoiceRerouteAmount = OIC.RerouteAmount 
		, InvoiceH2SRate = OIC.H2SRate
		, InvoiceH2SRateType = OIC.H2SRateType
		, InvoiceH2SAmount = OIC.H2SAmount
		, InvoiceSplitLoadRate = OIC.SplitLoadRate
		, InvoiceSplitLoadRateType = OIC.SplitLoadRateType
		, InvoiceSplitLoadAmount = OIC.SplitLoadAmount
		, InvoiceTaxRate = OIC.OriginTaxRate
		, InvoiceUom = OIC.SettlementUom
		, InvoiceUomShort = OIC.SettlementUomShort
		, InvoiceMinSettlementUnits = OIC.MinSettlementUnits
		, InvoiceUnits = OIC.SettlementUnits
		, InvoiceRouteRate = OIC.RouteRate
		, InvoiceRouteRateType = OIC.RouteRateType
		, InvoiceRateSheetRate = OIC.RateSheetRate
		, InvoiceRateSheetRateType = OIC.RateSheetRateType
		, InvoiceFuelSurchargeRate = OIC.FuelSurchargeRate
		, InvoiceFuelSurchargeAmount = OIC.FuelSurchargeAmount
		, InvoiceLoadAmount = OIC.LoadAmount
		, InvoiceTotalAmount = OIC.TotalAmount
	FROM dbo.viewOrderExportFull O
	LEFT JOIN viewOrderSettlementCarrier OIC ON OIC.OrderID = O.ID
	WHERE O.StatusID NOT IN (-10) -- don't include "Generated" orders (only Assigned+)

GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details + FINANCIAL INFO into a single view
/***********************************/
ALTER VIEW [dbo].[viewOrder_Financial_Customer] AS 
	SELECT O.* 
		, TicketNums = dbo.fnOrderTicketDetails(O.ID, 'TicketNums', '<br/>') 
		, TankNums = dbo.fnOrderTicketDetails(O.ID, 'TankNums', '<br/>') 
		, PreviousDestinations = dbo.fnRerouteDetails(O.ID, 'PreviousDestinations', '<br/>') 
		, RerouteUsers = dbo.fnRerouteDetails(O.ID, 'RerouteUsers', '<br/>') 
		, RerouteDates = dbo.fnRerouteDetails(O.ID, 'RerouteDates', '<br/>') 
		, RerouteNotes = dbo.fnRerouteDetails(O.ID, 'RerouteNotes', '<br/>') 
		, InvoiceRatesAppliedDate = dbo.fnUTC_To_Local(OIC.CreateDateUTC, O.OriginTimeZoneID, O.OriginUseDST)
		, OIC.BatchID
		, InvoiceBatchNum = OIC.BatchNum 
		, InvoiceOriginBillableWaitMinutes = OIC.OriginWaitBillableMinutes
		, InvoiceDestinationBillableWaitMinutes = OIC.DestinationWaitBillableMinutes
		, InvoiceTotalBillableWaitMinutes = isnull(OIC.OriginWaitBillableMinutes, 0) + ISNULL(OIC.DestinationWaitBillableMinutes, 0) 
		, InvoiceWaitFeeSubUnit = WaitFeeSubUnit
		, InvoiceWaitFeeRoundingType = WaitFeeRoundingType
		, InvoiceOriginWaitRate = OIC.OriginWaitRate 
		, InvoiceOriginWaitAmount = OIC.OriginWaitAmount 		
		, InvoiceDestinationWaitRate = OIC.DestinationWaitRate 
		, InvoiceDestinationWaitAmount = OIC.DestinationWaitAmount 
		, InvoiceTotalWaitAmount = OIC.TotalWaitAmount
		, InvoiceRejectionRate = OIC.OrderRejectRate
		, InvoiceRejectionRateType = OIC.OrderRejectRateType
		, InvoiceRejectionAmount = OIC.OrderRejectAmount 
		, InvoiceChainupRate = OIC.ChainupRate
		, InvoiceChainupRateType = OIC.ChainupRateType
		, InvoiceChainupAmount = OIC.ChainupAmount 
		, InvoiceRerouteRate = OIC.RerouteRate
		, InvoiceRerouteRateType = OIC.RerouteRateType
		, InvoiceRerouteAmount = OIC.RerouteAmount 
		, InvoiceH2SRate = OIC.H2SRate
		, InvoiceH2SRateType = OIC.H2SRateType
		, InvoiceH2SAmount = OIC.H2SAmount
		, InvoiceSplitLoadRate = OIC.SplitLoadRate
		, InvoiceSplitLoadRateType = OIC.SplitLoadRateType
		, InvoiceSplitLoadAmount = OIC.SplitLoadAmount
		, InvoiceTaxRate = OIC.OriginTaxRate
		, InvoiceUom = OIC.SettlementUom
		, InvoiceUomShort = OIC.SettlementUomShort
		, InvoiceMinSettlementUnits = OIC.MinSettlementUnits
		, InvoiceUnits = OIC.SettlementUnits
		, InvoiceRouteRate = OIC.RouteRate
		, InvoiceRouteRateType = OIC.RouteRateType
		, InvoiceFuelSurchargeRate = OIC.FuelSurchargeRate
		, InvoiceFuelSurchargeAmount = OIC.FuelSurchargeAmount
		, InvoiceLoadAmount = OIC.LoadAmount
		, InvoiceTotalAmount = OIC.TotalAmount
	FROM dbo.viewOrderExportFull O
	LEFT JOIN viewOrderSettlementShipper OIC ON OIC.OrderID = O.ID
	WHERE O.StatusID NOT IN (-10) -- don't include "Generated" orders (only Assigned+)

GO

/**********************************************************
-- Author: Kevin Alons
-- Date Created: 8 Oct 2014
-- Purpose: return the Audit table records (optionally filtered to just the specified driver and/or ordernum)
***********************************************************/
ALTER FUNCTION [dbo].[fnOrders_AllTickets_Audit](@carrierID int, @driverID int, @orderNum int, @id int) RETURNS TABLE AS RETURN
SELECT *
	, OriginDistanceText = CASE WHEN OriginGpsArrived = 1 THEN 'Verified' ELSE isnull(ltrim(OriginDistance), 'N/A') END
	, DestDistanceText = CASE WHEN DestGpsArrived = 1 THEN 'Verified' ELSE isnull(ltrim(DestDistance), 'N/A') END
	, HasError = cast(CASE WHEN Errors IS NULL THEN 0 ELSE 1 END as bit)
FROM (
	SELECT O.* 
		, OriginGpsLatLon = ltrim(DLO.Lat) + ',' + ltrim(DLO.Lon)
		, OriginLatLon = ltrim(OO.LAT) + ',' + ltrim(OO.LON)
		, OriginAccuracyMeters = DLO.SourceAccuracyMeters
		, OriginDistance = DLO.DistanceToPoint
		, OriginGpsArrived = CASE WHEN DLO.DistanceToPoint IS NOT NULL AND DLO.DistanceToPoint <= OO.GeoFenceRadiusMeters THEN 1 ELSE 0 END
		, DestGpsLatLon = ltrim(DLD.Lat) + ',' + ltrim(DLD.Lon)
		, DestLatLon = ltrim(D.LAT) + ',' + ltrim(D.LON)
		, DestAccuracyMeters = DLD.SourceAccuracyMeters
		, DestDistance = DLD.DistanceToPoint
		, DestGpsArrived = CASE WHEN DLD.DistanceToPoint IS NOT NULL AND DLD.DistanceToPoint <= OO.GeoFenceRadiusMeters THEN 1 ELSE 0 END
		, OriginGrossBBLS = dbo.fnConvertUOM(O.OriginGrossUnits, O.OriginUomID, 1)
		, Errors = 
			nullif(
				SUBSTRING(
					CASE WHEN O.OriginArriveTimeUTC IS NULL THEN ',Missing Pickup Arrival' ELSE '' END
				  + CASE WHEN O.OriginDepartTimeUTC IS NULL THEN ',Missing Pickup Departure' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND O.Tickets IS NULL THEN ',No Active Tickets' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND O.DestArriveTimeUTC IS NULL THEN ',Missing Delivery Arrival' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND O.DestDepartTimeUTC IS NULL THEN ',Missing Delivery Departure' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND O.OriginGrossUnits = 0 AND O.OriginNetUnits = 0 THEN 'No Origin Units are entered' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND dbo.fnConvertUOM(O.OriginGrossUnits, O.OriginUomID, 1) NOT BETWEEN 0 AND 320 THEN ',Origin GOV Units out of limits' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND dbo.fnConvertUOM(O.OriginGrossStdUnits, O.OriginUomID, 1) NOT BETWEEN 0 AND 320 THEN ',Origin GSV Units out of limits' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND dbo.fnConvertUOM(O.OriginNetUnits, O.OriginUomID, 1) NOT BETWEEN 0 AND 320 THEN ',Origin NSV Units out of limits' ELSE '' END
				  + CASE WHEN C.RequireDispatchConfirmation = 1 AND nullif(rtrim(O.DispatchConfirmNum), '') IS NULL THEN ',Missing Dispatch Confirm #' ELSE '' END
				  + CASE WHEN O.TruckID IS NULL THEN ',Truck Missing' ELSE '' END
				  + CASE WHEN O.TrailerID IS NULL THEN ',Trailer 1 Missing' ELSE '' END
				, 2, 8000) 
			, '')
		, IsEditable = cast(CASE WHEN O.StatusID = 3 THEN 1 ELSE 0 END as bit)
	FROM viewOrder_AllTickets O
	JOIN tblOrigin OO ON OO.ID = O.OriginID
	JOIN tblCustomer C ON C.ID = OO.CustomerID
	JOIN tblDestination D ON D.ID = O.DestinationID
	LEFT JOIN tblOrderSettlementShipper OIC ON OIC.OrderID = O.ID
	LEFT JOIN viewDriverLocation_OriginFirstArrive DLO ON DLO.OrderID = O.ID AND DLO.OriginID = O.OriginID
	LEFT JOIN viewDriverLocation_DestinationFirstArrive DLD ON DLD.OrderID = O.ID AND DLD.DestinationID = O.DestinationID
	WHERE OIC.BatchID IS NULL /* don't even show SETTLED orders on the AUDIT page ever */
	  AND (isnull(@carrierID, 0) = -1 OR O.CarrierID = @carrierID)
	  AND (isnull(@driverID, 0) = -1 OR O.DriverID = @driverID)
	  AND ((nullif(@orderNum, 0) IS NULL AND O.DeleteDateUTC IS NULL AND (O.StatusID = 3 AND DeliverPrintStatusID IN (SELECT ID FROM tblPrintStatus WHERE IsCompleted = 1))) OR O.OrderNum = @orderNum)
	  AND (nullif(@id, 0) IS NULL OR O.ID LIKE @id)
) X

GO

UPDATE tblReportColumnDefinition SET DataField = REPLACE(DataField, 'Fee', 'Amount') WHERE DataField LIKE '%Fee'
GO

EXEC _spRebuildAllObjects
GO

/* =============================================
-- Author:		Kevin Alons
-- Create date: 19 Dec 2012
-- Description:	trigger to 
				1) validate any changes, and fail the update if invalid changes are submitted
				2) add a unique, incrementing OrderNum to each new Order (manual Identity column)
				3) recompute wait times (origin|destination based on times provided)
				4) generate route table entry for any newly used origin-destination combination
				5) keep Order consistent with Origin.Producer|Operator|UOM & Dest.UOM then the Origin|Dest change
				6) roll the PrintStatus back to NOTFINALIZED (0) when status is rolled back
				7) when DriverID changes, mark those orders AS DELETED and clone the order with the new DriverID and associated records
				8) update any ticket quantities for open orders when the UOM is changed for the Origin
				9) update the Driver.Truck\Trailer\Trailer2 defaults & related DISPATCHED orders when driver updates them on an order
				10) update the Pickup/DeliverLastChangeDateUTC when Origin/Destination is changed (respectively)
				11) (re) apply Settlement Amounts to orders not yet fully settled when status is changed to DELIVERED
				12) if DBAudit is turned on, save an audit record for this Order change
-- =============================================*/
ALTER TRIGGER [dbo].[trigOrder_IU] ON [dbo].[tblOrder] AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @changesFound bit
	
		-- Note: the TRIGGER_NESTLEVEL( OBJECT_ID('trigOrder_IU') ) = 1 statement prevents recursion
	IF (TRIGGER_NESTLEVEL( OBJECT_ID('trigOrder_IU')) = 1 AND TRIGGER_NESTLEVEL( OBJECT_ID('trigOrderTicket_IU')) = 0) BEGIN

		/**********  START OF VALIDATION SECTION ************************/

		IF (UPDATE(OrderNum) 
			OR UPDATE(PriorityID) 
			OR UPDATE(DueDate) 
			OR UPDATE(RouteID) 
			OR UPDATE(OriginID) 
			OR UPDATE(OriginArriveTimeUTC) 
			OR UPDATE(OriginDepartTimeUTC) 
			OR UPDATE(OriginMinutes) 
			OR UPDATE(OriginWaitNotes) 
			OR UPDATE(OriginBOLNum) 
			OR UPDATE(OriginGrossUnits) 
			OR UPDATE(OriginNetUnits) 
			OR UPDATE(DestinationID) 
			OR UPDATE(DestArriveTimeUTC) 
			OR UPDATE(DestDepartTimeUTC) 
			OR UPDATE(DestMinutes) 
			OR UPDATE(DestWaitNotes) 
			OR UPDATE(DestBOLNum) 
			OR UPDATE(DestGrossUnits) 
			OR UPDATE(DestNetUnits) 
			OR UPDATE(CustomerID) 
			OR UPDATE(CarrierID) 
			OR UPDATE(DriverID) 
			OR UPDATE(TruckID)
			OR UPDATE(TrailerID) 
			OR UPDATE(Trailer2ID) 
			OR UPDATE(OperatorID) 
			OR UPDATE(PumperID) 
			OR UPDATE(TicketTypeID) 
			OR UPDATE(Rejected) 
			OR UPDATE(RejectNotes) 
			OR UPDATE(ChainUp) 
			OR UPDATE(OriginTruckMileage) 
			OR UPDATE(OriginTankNum) 
			OR UPDATE(DestTruckMileage) 
			OR UPDATE(CarrierTicketNum) 
			OR UPDATE(AuditNotes) 
			OR UPDATE(CreateDateUTC) 
			-- it is permissible to change the ActualMiles value on Audited orders up to the point the Order has been Shipper-Settled
			OR (UPDATE(ActualMiles) AND EXISTS (SELECT * FROM deleted i JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = i.ID WHERE OSS.BatchID IS NOT NULL))
			OR UPDATE(ProducerID) 
			OR UPDATE(CreatedByUser) 
			OR UPDATE(LastChangeDateUTC) 
			OR UPDATE(LastChangedByUser) 
			OR UPDATE(DeleteDateUTC) 
			OR UPDATE(DeletedByUser) 
			OR UPDATE(DestProductBSW)
			OR UPDATE(DestProductGravity) 
			OR UPDATE(DestProductTemp) 
			OR UPDATE(ProductID) 
			OR UPDATE(AcceptLastChangeDateUTC) 
			OR UPDATE(PickupLastChangeDateUTC) 
			OR UPDATE(DeliverLastChangeDateUTC) 
			OR UPDATE(OriginUomID) 
			OR UPDATE(DestUomID) 
			OR UPDATE(PickupPrintStatusID) 
			OR UPDATE(DeliverPrintStatusID)
			OR UPDATE(PickupPrintDateUTC) 
			OR UPDATE(DeliverPrintDateUTC) 
			OR UPDATE(OriginTankID) 
			OR UPDATE(OriginGrossStdUnits) 
			OR UPDATE(DispatchConfirmNum) 
			OR UPDATE(DispatchNotes)
			OR UPDATE(DriverNotes)
			OR UPDATE(OriginWaitReasonID)
			OR UPDATE(DestWaitReasonID)
			OR UPDATE(RejectReasonID) 
			OR UPDATE(DestOpenMeterUnits) 
			OR UPDATE(DestCloseMeterUnits))
			--OR UPDATE(ReassignKey)) we don't do any work if this value changes so just ignore it
		BEGIN
			-- only allow the StatusID value to be changed on an audited order
			IF EXISTS (SELECT * FROM deleted WHERE StatusID = 4)
			BEGIN
				-- if the "Allow Audited Order Update (temp only)" setting is TRUE then only warn when this occurs
				IF (dbo.fnToBool(dbo.fnSettingValue(35)) = 1)
					PRINT 'AUDITED orders is being modified - please investigate why!'
				ELSE BEGIN
					-- otherwise (normal behavior) - prevent AUDITED orders from being modified (except to UN-AUDIT them)
					RAISERROR('AUDITED orders cannot be modified!', 16, 1)
					IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
					RETURN
				END
			END
		END
		ELSE -- NO CHANGES DETECTED SO JUST QUIT
			RETURN

		PRINT 'trigOrder_IU FIRED'
					
		-- ensure the Origin and Destinations are both specified unless the Status is:
		--   (Generated, Assigned, Dispatched or Declined)
		IF  EXISTS(SELECT * FROM inserted O WHERE (O.OriginID IS NULL OR O.DestinationID IS NULL) AND O.StatusID NOT IN (-10,1,2,9))
		BEGIN
			RAISERROR('Invalid Order Status when the Origin and/or Or Destination is not specified', 16, 1)
			IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
			RETURN
		END
		
		/**********  END OF VALIDATION SECTION ************************/

		WHILE (SELECT count(*) FROM inserted i JOIN tblOrder O ON O.ID = i.ID WHERE O.OrderNum IS NULL) > 0
		BEGIN
			UPDATE tblOrder 
			  SET OrderNum = isnull((SELECT max(OrderNum) FROM tblOrder), 100000) + 1
				, CreateDateUTC = getutcdate()
			WHERE ID = (SELECT min(O.ID) FROM tblOrder O JOIN inserted i ON i.ID = O.ID WHERE O.OrderNum IS NULL)
		END
		
		-- re-compute the OriginMinutes (in case the website failed to compute it properly)
		IF UPDATE(OriginArriveTimeUTC) OR UPDATE(OriginDepartTimeUTC)
		BEGIN
			UPDATE tblOrder SET OriginMinutes = datediff(minute, i.OriginArriveTimeUTC, i.OriginDepartTimeUTC)
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
		END
		-- re-compute the DestWaitMinutes (in case the website failed to compute it properly)
		IF UPDATE(DestArriveTimeUTC) OR UPDATE(DestDepartTimeUTC)
		BEGIN
			UPDATE tblOrder SET DestMinutes = datediff(minute, i.DestArriveTimeUTC, i.DestDepartTimeUTC)
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
		END
		
		-- ensure missing Routes are created and assigned for the specified Origin/Destination combination
		IF UPDATE(OriginID) OR UPDATE(DestinationID)
		BEGIN
			-- create any missing Route records
			INSERT INTO tblRoute (OriginID, DestinationID, CreateDateUTC, CreatedByUser)
				SELECT i.OriginID, i.DestinationID, GETUTCDATE(), ISNULL(i.LastChangedByUser, i.CreatedByUser)
				FROM inserted i
				LEFT JOIN tblRoute R ON R.OriginID = i.OriginID AND R.DestinationID = i.DestinationID
				WHERE i.OriginID IS NOT NULL AND i.DestinationID IS NOT NULL AND R.ID IS NULL
			-- ensure the Order records refer to the correct Route (ID)
			UPDATE tblOrder SET RouteID = R.ID
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
			LEFT JOIN tblRoute R ON R.OriginID = O.OriginID AND R.DestinationID = O.DestinationID

			-- update the ActualMiles from the related Route
			UPDATE tblOrder SET ActualMiles = R.ActualMiles
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
			JOIN tblRoute R ON R.ID = O.RouteID
		END
		
		IF (UPDATE(OriginID))
		BEGIN
			-- update Order.ProducerID to match what is assigned to the new Origin
			UPDATE tblOrder 
				SET ProducerID = OO.ProducerID
					-- update this stage specific date field so the mobile app knows the pickup has been modified
					, PickupLastChangeDateUTC = GETUTCDATE() -- mobile app never changes this so just use getutcdate()
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			WHERE d.OriginID <> O.OriginID

			-- update Order.OriginUomID to match what is assigned to the new Origin (quantities are updated below)
			UPDATE tblOrder SET OriginUomID = OO.UomID
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			WHERE d.OriginID <> O.OriginID AND d.OriginUomID <> OO.UomID
		END
		
		-- keep the DestUomID in sync with the Destination (units are updated below)
		IF (UPDATE(DestinationID))
		BEGIN
			-- update Order.DestUomID to match what is assigned to the new Destination
			UPDATE tblOrder 
			  SET DestUomID = DD.UomID
				-- updating this will ensure the mobile app refreshes itself with this revision (it doesn't use LastChangeDateUTC but rather the stage specific date value)
				, DeliverLastChangeDateUTC = GETUTCDATE() -- this will never be called by the mobile app so just use getutcdate()
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID
			JOIN tblDestination DD ON DD.ID = O.DestinationID
			WHERE d.DestinationID <> O.DestinationID AND d.DestUomID <> DD.UomID
		END
		
		-- ensure that any set PrintStatusID values are reset to 0 when backing up the general StatusID value
		IF (UPDATE(StatusID))
		BEGIN
			UPDATE tblOrder 
			  SET DeliverPrintStatusID = 0 
			  FROM tblOrder O
			  JOIN deleted d ON d.ID = O.ID
			WHERE O.StatusID <> d.StatusID AND O.StatusID IN (-10, 1, 2, 7, 8) -- Generated, Assigned, Dispatched, Accepted, Picked-Up

			UPDATE tblOrder 
			  SET PickupPrintStatusID = 0 
			  FROM tblOrder O
			  JOIN deleted d ON d.ID = O.ID
			WHERE O.StatusID <> d.StatusID AND O.StatusID IN (-10, 1, 2, 7) -- Generated, Assigned, Dispatched, Accepted
		END

		-- just updating the tickets here, trigOrderTicket_IU will in turn update this table
		-- ensure any existing OrderTickets are recomputed with the new OriginUOM
		UPDATE tblOrderTicket
		  SET GrossUnits = dbo.fnConvertUOM(GrossUnits, d.OriginUomID, O.OriginUomID)
			, NetUnits = dbo.fnConvertUOM(NetUnits, d.OriginUomID, O.OriginUomID)
		FROM tblOrderTicket OT
		JOIN tblOrder O ON O.ID = OT.OrderID
		JOIN deleted d ON d.ID = O.ID
		WHERE O.OriginUomID <> d.OriginUomID
		
		-- recalculate the Destination Unit values if only if the DestUomID changed but the values didn't
		UPDATE tblOrder
		  SET DestGrossUnits = dbo.fnConvertUOM(O.DestGrossUnits, d.DestUomID, O.DestUomID)
			, DestNetUnits = dbo.fnConvertUOM(O.DestNetUnits, d.DestUomID, O.DestUomID)
		FROM tblOrder O
		JOIN deleted d ON d.ID = O.ID
		WHERE d.DestUomID <> O.DestUomID 
		  AND d.DestGrossUnits = O.DestGrossUnits
		  AND d.DestNetUnits = O.DestNetUnits

		/*************************************************************************************************************/
		/* handle driver changes - "orphan" the order/ticket (with a new ORDERNUM) - and clone it for the new driver */
		IF (UPDATE(DriverID))
		BEGIN
			-- create cloned order/ticket records (these records will be cloned then the original reset to orphaned status)
			SELECT O.*, NewOrderNum = CAST(NULL as int)
			INTO #clone
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID AND d.DriverID IS NOT NULL AND d.DriverID <> isnull(O.DriverID, 0)

			/* generate the next OrderNum values for these new records */
			DECLARE @newOrderNum int; SET @newOrderNum = isnull((SELECT max(OrderNum) FROM tblOrder), 100000) + 1;
			DECLARE @newReassignKey int; SET @newReassignKey = isnull((SELECT max(ReassignKey) FROM tblOrder), 0) + 1;
			WHILE (SELECT count(*) FROM #clone WHERE NewOrderNum IS NULL) > 0
			BEGIN
				UPDATE #clone
				  SET NewOrderNum = @newOrderNum, ReassignKey = isnull(ReassignKey, @newReassignKey)
					, CreateDateUTC = getutcdate()
				WHERE ID = (SELECT min(ID) FROM #clone WHERE NewOrderNum IS NULL)
				-- if the @newReassignKey wasn't used above, then that # will be skipped, but this should not be an issue
				SELECT @newOrderNum = @newOrderNum + 1, @newReassignKey = @newReassignKey + 1
			END
			
			-- reset the existing records as orphaned
			UPDATE tblOrder
				SET DeleteDateUTC = isnull(O.DeleteDateUTC, GETUTCDATE()), DeletedByUser = isnull(O.DeletedByUser, O.LastChangedByUser)
					, CarrierID = d.CarrierID, DriverID = d.DriverID, TruckID = d.TruckID, TrailerID = d.TrailerID, Trailer2ID = d.Trailer2ID
					, DispatchConfirmNum = NULL
					, ReassignKey = C.ReassignKey
			FROM tblOrder O
			JOIN #clone C ON C.ID = O.ID
			JOIN deleted d on D.ID = O.id

			-- create the cloned records (which is essentially the original with a new ID and the Carrrier & default Truck|Trailers of the new driver)
			INSERT INTO tblOrder (OrderNum, [StatusID], [PriorityID], [DueDate], [RouteID], [OriginID], [OriginArriveTimeUTC], [OriginDepartTimeUTC], [OriginMinutes], [OriginWaitNotes], [OriginBOLNum], [OriginGrossUnits], [OriginNetUnits], [DestinationID], [DestArriveTimeUTC], [DestDepartTimeUTC], [DestMinutes], [DestWaitNotes], [DestBOLNum], [DestGrossUnits], [DestNetUnits], [CustomerID], [CarrierID], [DriverID], [TruckID], [TrailerID], [Trailer2ID], [OperatorID], [PumperID], [TicketTypeID], [Rejected], [RejectNotes], [ChainUp], [OriginTruckMileage], [OriginTankNum], [DestTruckMileage], [CarrierTicketNum], [AuditNotes], [CreateDateUTC], [ActualMiles], [ProducerID], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser], [DestProductBSW], [DestProductGravity], [DestProductTemp], [ProductID], [AcceptLastChangeDateUTC], [PickupLastChangeDateUTC], [DeliverLastChangeDateUTC], [OriginUomID], [DestUomID], [PickupPrintStatusID], [DeliverPrintStatusID], [PickupPrintDateUTC], [DeliverPrintDateUTC], [OriginTankID], [OriginGrossStdUnits], [DispatchNotes], [DriverNotes], [DispatchConfirmNum], [OriginWaitReasonID], [DestWaitReasonID], [RejectReasonID], [DestOpenMeterUnits], [DestCloseMeterUnits], [ReassignKey])
				SELECT NewOrderNum, [StatusID], [PriorityID], [DueDate], [RouteID], [OriginID], [OriginArriveTimeUTC], [OriginDepartTimeUTC], [OriginMinutes], [OriginWaitNotes], [OriginBOLNum], [OriginGrossUnits], [OriginNetUnits], [DestinationID], [DestArriveTimeUTC], [DestDepartTimeUTC], [DestMinutes], [DestWaitNotes], [DestBOLNum], [DestGrossUnits], [DestNetUnits], [CustomerID], D.[CarrierID], [DriverID], D.TruckID, D.TrailerID, D.Trailer2ID, [OperatorID], [PumperID], [TicketTypeID], [Rejected], [RejectNotes], [ChainUp], [OriginTruckMileage], [OriginTankNum], [DestTruckMileage], [CarrierTicketNum], [AuditNotes], O.CreateDateUTC, [ActualMiles], [ProducerID], O.CreatedByUser, O.LastChangeDateUTC, O.LastChangedByUser, O.DeleteDateUTC, O.DeletedByUser, [DestProductBSW], [DestProductGravity], [DestProductTemp], [ProductID], [AcceptLastChangeDateUTC], [PickupLastChangeDateUTC], [DeliverLastChangeDateUTC], [OriginUomID], [DestUomID], [PickupPrintStatusID], [DeliverPrintStatusID], [PickupPrintDateUTC], [DeliverPrintDateUTC], [OriginTankID], [OriginGrossStdUnits], [DispatchNotes], [DriverNotes], DispatchConfirmNum, [OriginWaitReasonID], [DestWaitReasonID], [RejectReasonID], [DestOpenMeterUnits], [DestCloseMeterUnits], ReassignKey
				FROM #clone O
				LEFT JOIN tblDriver D ON D.ID = O.DriverID
		
			-- cache the affected tickets
			SELECT OT.*, C.NewOrderNum, OrderDeletedByUser = C.LastChangedByUser
			INTO #cloneTicket
			FROM tblOrderTicket OT
			JOIN #clone C ON C.ID = OT.OrderID
			WHERE OT.DeleteDateUTC IS NULL

			-- create the cloned Order Ticket records (copy of existing ticket(s) pointed at new "original" Order)
			INSERT INTO tblOrderTicket (OrderID, CarrierTicketNum, TicketTypeID, TankNum, ProductObsGravity, ProductObsTemp, ProductBSW, OpeningGaugeFeet, OpeningGaugeInch, OpeningGaugeQ, ClosingGaugeFeet, ClosingGaugeInch, ClosingGaugeQ
					, GrossUnits, NetUnits, Rejected, RejectNotes, SealOff, SealOn, BOLNum, ProductHighTemp, ProductLowTemp, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser
					, UID, FromMobileApp, OriginTankID, BottomFeet, BottomInches, BottomQ, GrossStdUnits, RejectReasonID, MeterFactor, OpenMeterUnits, CloseMeterUnits)
				SELECT O.ID, CT.CarrierTicketNum, CT.TicketTypeID, CT.TankNum, CT.ProductObsGravity, CT.ProductObsTemp, CT.ProductBSW, CT.OpeningGaugeFeet, CT.OpeningGaugeInch, CT.OpeningGaugeQ, CT.ClosingGaugeFeet, CT.ClosingGaugeInch, CT.ClosingGaugeQ
					, CT.GrossUnits, CT.NetUnits, CT.Rejected, CT.RejectNotes, CT.SealOff, CT.SealOn, CT.BOLNum, CT.ProductHighTemp, CT.ProductLowTemp, CT.CreateDateUTC, CT.CreatedByUser, CT.LastChangeDateUTC, CT.LastChangedByUser, CT.DeleteDateUTC, CT.DeletedByUser
					, newid(), CT.FromMobileApp, CT.OriginTankID, CT.BottomFeet, CT.BottomInches, CT.BottomQ, CT.GrossStdUnits, CT.RejectReasonID, CT.MeterFactor, CT.OpenMeterUnits, CT.CloseMeterUnits
				FROM #cloneTicket CT
				JOIN tblOrder O ON O.OrderNum = CT.NewOrderNum
		END
		/*************************************************************************************************************/
		
		/* HANDLE TRUCK | TRAILER | TRAILER2 default changes when the driver updates his Truck | Trailer | Trailer2 on ACCEPTANCE */
		-- TRUCK
		IF (UPDATE(TruckID) AND EXISTS (
			SELECT i.* 
			FROM inserted i 
			JOIN deleted d ON d.ID = i.ID
			  AND i.DriverID = d.DriverID 
			  AND i.TruckID <> d.TruckID
			WHERE i.StatusID IN (7, 8, 3) -- ACCEPTED, PICKEDUP, DELIVERED
			  AND d.StatusID IN (2))) -- DISPATCHED
		BEGIN
			UPDATE tblDriver
			  SET TruckID = i.TruckID
				, LastChangeDateUTC = i.LastChangeDateUTC
				, LastChangedByUser = i.LastChangedByUser
			FROM tblDriver DR
			JOIN inserted i ON i.DriverID = DR.ID
			JOIN deleted d ON d.DriverID = DR.ID
			WHERE i.TruckID <> d.TruckID
			
			UPDATE tblOrder
			  SET TruckID = i.TruckID
				, LastChangeDateUTC = i.LastChangeDateUTC
				, LastChangedByUser = i.LastChangedByUser
			FROM tblOrder O
			JOIN inserted i ON i.DriverID = O.DriverID 
			  AND O.ID <> i.ID
			  AND O.StatusID IN (2) -- DISPATCHED
			  AND i.TruckID <> O.TruckID
			  AND O.DeleteDateUTC IS NULL
		END
		-- TRAILER
		IF (UPDATE(TrailerID) AND EXISTS (
			SELECT i.* 
			FROM inserted i 
			JOIN deleted d ON d.ID = i.ID
			  AND i.DriverID = d.DriverID 
			  AND i.TrailerID <> d.TrailerID
			WHERE i.StatusID IN (7, 8, 3) -- ACCEPTED, PICKEDUP, DELIVERED
			  AND d.StatusID IN (2))) -- DISPATCHED
		BEGIN
			UPDATE tblDriver
			  SET TrailerID = i.TrailerID
				, LastChangeDateUTC = i.LastChangeDateUTC
				, LastChangedByUser = i.LastChangedByUser
			FROM tblDriver DR
			JOIN inserted i ON i.DriverID = DR.ID
			JOIN deleted d ON d.DriverID = DR.ID
			WHERE i.TrailerID <> d.TrailerID
			
			UPDATE tblOrder
			  SET TrailerID = i.TrailerID
				, LastChangeDateUTC = i.LastChangeDateUTC
				, LastChangedByUser = i.LastChangedByUser
			FROM tblOrder O
			JOIN inserted i ON i.DriverID = O.DriverID 
			  AND O.ID <> i.ID
			  AND O.StatusID IN (2) -- DISPATCHED
			  AND i.TrailerID <> O.TrailerID
			  AND O.DeleteDateUTC IS NULL
		END
		-- TRAILER 2
		IF (UPDATE(Trailer2ID) AND EXISTS (
			SELECT i.* 
			FROM inserted i 
			JOIN deleted d ON d.ID = i.ID
			  AND i.DriverID = d.DriverID 
			  AND isnull(i.Trailer2ID, 0) <> isnull(d.Trailer2ID, 0)
			WHERE i.StatusID IN (7, 8, 3) -- ACCEPTED, PICKEDUP, DELIVERED
			  AND d.StatusID IN (2))) -- DISPATCHED
		BEGIN
			UPDATE tblDriver
			  SET TrailerID = i.TrailerID
				, LastChangeDateUTC = i.LastChangeDateUTC
				, LastChangedByUser = i.LastChangedByUser
			FROM tblDriver DR
			JOIN inserted i ON i.DriverID = DR.ID
			JOIN deleted d ON d.DriverID = DR.ID
			WHERE isnull(i.Trailer2ID, 0) <> isnull(d.Trailer2ID, 0)
			
			UPDATE tblOrder
			  SET Trailer2ID = i.Trailer2ID
				, LastChangeDateUTC = i.LastChangeDateUTC
				, LastChangedByUser = i.LastChangedByUser
			FROM tblOrder O
			JOIN inserted i ON i.DriverID = O.DriverID 
			  AND O.ID <> i.ID
			  AND O.StatusID IN (2) -- DISPATCHED
			  AND isnull(i.Trailer2ID, 0) <> isnull(O.Trailer2ID, 0)
			  AND O.DeleteDateUTC IS NULL
		END
		/*************************************************************************************************************/
	
		-- Apply Settlement Rates/Amounts to Order when STATUS is changed to DELIVERED (until it is FINAL settled)
		IF UPDATE(StatusID) OR UPDATE(DeliverPrintStatusID)
		BEGIN
			DECLARE @deliveredIDs TABLE (ID int)
			INSERT INTO @deliveredIDs (ID)
				SELECT i.ID 
				FROM inserted i
				JOIN tblPrintStatus iPS ON iPS.ID = i.DeliverPrintStatusID
				JOIN deleted d ON d.ID = i.ID
				JOIN tblPrintStatus dPS ON dPS.ID = d.DeliverPrintStatusID
				WHERE i.StatusID = 3 AND iPS.IsCompleted = 1 AND i.StatusID + iPS.IsCompleted <> d.StatusID + dPS.IsCompleted
			
			DECLARE @id int
			WHILE EXISTS (SELECT 1 FROM @deliveredIDs)
			BEGIN
				SELECT TOP 1 @id = ID FROM @deliveredIDs
				EXEC spProcessOrderSettlement @id, 'System' 
				DELETE FROM @deliveredIDs WHERE ID = @id
			END
		END
		
		--NOTE: we do not update the DestOpenMeterUnits/DestCloseMeterUnits since they don't auto update
		--  (and they simply represent a reading that is assumed to in the DestUomID UOM at entry)
		--  (if they are updated in the future [manually] the will calculate again using the DestUomID at that time)

		IF EXISTS (SELECT * FROM inserted EXCEPT SELECT * FROM deleted)
		BEGIN
			-- optionally add tblOrderDBAudit records
			BEGIN TRY
				IF EXISTS(SELECT * FROM tblSetting WHERE ID = 32 AND (Value LIKE 'true' OR Value LIKE 'yes'))
					INSERT INTO tblOrderDbAudit (DBAuditDate, [ID], [OrderNum], [StatusID], [PriorityID], [DueDate], [RouteID], [OriginID], [OriginArriveTimeUTC], [OriginDepartTimeUTC], [OriginMinutes], [OriginWaitNotes], [OriginBOLNum], [OriginGrossUnits], [OriginNetUnits], [DestinationID], [DestArriveTimeUTC], [DestDepartTimeUTC], [DestMinutes], [DestWaitNotes], [DestBOLNum], [DestGrossUnits], [DestNetUnits], [CustomerID], [CarrierID], [DriverID], [TruckID], [TrailerID], [Trailer2ID], [OperatorID], [PumperID], [TicketTypeID], [Rejected], [RejectNotes], [ChainUp], [OriginTruckMileage], [OriginTankNum], [DestTruckMileage], [CarrierTicketNum], [AuditNotes], [CreateDateUTC], [ActualMiles], [ProducerID], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser], [DestProductBSW], [DestProductGravity], [DestProductTemp], [ProductID], [AcceptLastChangeDateUTC], [PickupLastChangeDateUTC], [DeliverLastChangeDateUTC], [OriginUomID], [DestUomID], [PickupPrintStatusID], [DeliverPrintStatusID], [PickupPrintDateUTC], [DeliverPrintDateUTC], [OriginTankID], [OriginGrossStdUnits], [DispatchNotes], [DriverNotes], [DispatchConfirmNum], [OriginWaitReasonID], [DestWaitReasonID], [RejectReasonID], [DestOpenMeterUnits], [DestCloseMeterUnits], ReassignKey)
						SELECT GETUTCDATE(), [ID], [OrderNum], [StatusID], [PriorityID], [DueDate], [RouteID], [OriginID], [OriginArriveTimeUTC], [OriginDepartTimeUTC], [OriginMinutes], [OriginWaitNotes], [OriginBOLNum], [OriginGrossUnits], [OriginNetUnits], [DestinationID], [DestArriveTimeUTC], [DestDepartTimeUTC], [DestMinutes], [DestWaitNotes], [DestBOLNum], [DestGrossUnits], [DestNetUnits], [CustomerID], [CarrierID], [DriverID], [TruckID], [TrailerID], [Trailer2ID], [OperatorID], [PumperID], [TicketTypeID], [Rejected], [RejectNotes], [ChainUp], [OriginTruckMileage], [OriginTankNum], [DestTruckMileage], [CarrierTicketNum], [AuditNotes], [CreateDateUTC], [ActualMiles], [ProducerID], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser], [DestProductBSW], [DestProductGravity], [DestProductTemp], [ProductID], [AcceptLastChangeDateUTC], [PickupLastChangeDateUTC], [DeliverLastChangeDateUTC], [OriginUomID], [DestUomID], [PickupPrintStatusID], [DeliverPrintStatusID], [PickupPrintDateUTC], [DeliverPrintDateUTC], [OriginTankID], [OriginGrossStdUnits], [DispatchNotes], [DriverNotes], [DispatchConfirmNum], [OriginWaitReasonID], [DestWaitReasonID], [RejectReasonID], [DestOpenMeterUnits], [DestCloseMeterUnits], ReassignKey
						FROM deleted d
			END TRY
			BEGIN CATCH
				PRINT 'trigOrder_IU.DBAUDIT FAILURE: ' + ERROR_MESSAGE()
			END CATCH
		END
		
	PRINT 'trigOrder_IU COMPLETE'

	END
	
END
GO
EXEC sp_settriggerorder @triggername=N'[dbo].[trigOrder_IU]', @order=N'First', @stmttype=N'INSERT'
GO
EXEC sp_settriggerorder @triggername=N'[dbo].[trigOrder_IU]', @order=N'First', @stmttype=N'UPDATE'
GO

COMMIT
SET NOEXEC OFF
RETURN

/*
-- TODO: basic testing through here
-- TODO: test thoroughly
-- TODO: remove extraneous objects & fields
-- TODO: clone to Customer (Shipper) logic (remove Shipper rate retrieval parts)
begin tran x
exec spProcessShipperInvoice 67048, 'kalons'
exec spProcessOrderSettlement 66852, 'kalons'
select totalamount, routeamount from tblOrderSettlementShipper where OrderID = 66852
select totalamount, routeamount from tblOrderSettlementCarrier where OrderID = 66852
select * from tblOrderSettlementShipper where OrderID = 66852
select * from tblOrderSettlementCarrier where OrderID = 66852
select routeid, actualmiles from tblOrder where ID = 67035
select * from tblcarrierrouterate where RouteID = 3412
select * from tblShipperRateSheet
select * from tblOrderSettlementCarrier where OrderID = 62364
select * from tblOrderSettlementCarrierAssessorialCharge where orderid = 62364
select top 10 * from viewOrder 
where ActualMiles > 0 and OrderStatus = 'audited' and Rejected = 0 and OriginMinutes > 60 
  and ID not in (select orderid from tblOrderSettlementShipper)
order by id desc
rollback
select * from viewOrder where H2S = 1 and Rejected = 1
select * from tblOrderInvoiceCustomer where OrderID = 19003
-- TODO: tested through this point

select MAX(id) from viewOrder where PrintStatusID in (3,4) and RouteID in (select RouteID from viewCarrierRouteRate where CarrierID = 1095) and CarrierID = 1095
select TOP 1 * FROM viewOrder where PrintStatusID in (3,4) and OriginID = 6573 and OriginMinutes > 60 and CarrierID = 1095 and OriginWaitReasonID is not null
select id, orderdate, orderstatus, carrierid, routeid, originid, destinationid, originstateid, deststateid, originregionid, productgroupid, actualmiles, OriginWaitReasonID, OriginMinutes, ChainUp, RerouteCount, TicketCount, h2s 
from viewOrder 
where H2S = 1 and TicketCount > 1 --ID IN( 66842, 59871)
order by ID desc
select regionid from tblOrigin where ID = 6573
select * from viewCarrierRouteRate where CarrierID = 1095 and RouteID = 2188
select * from dbo.fnOrderCarrierRouteRate(66842)
select * from dbo.fnOrderCarrierRateSheetRangeRate(67035)
select * from dbo.fnOrderCarrierLoadAmount(67035, 100, 1)
select * from dbo.fnOrderShipperOriginWaitRate(67048)
select * from dbo.fnOrderShipperOriginWaitData(67048)
select * from tblShipperOriginWaitRate
select id, OriginWaitReasonID, OrderNum, orderdate, originminutes, CustomerID from viewOrder where OriginMinutes > 100 order by OrderNum desc
select * from dbo.fnOrderCarrierAssessorialRate(62364)
select * from tblCarrierOriginWaitRate
select * from viewCarrierRatesheet
select * from viewCarrierRangeRate
select * from viewCarrierOriginWaitRate
select * from viewReportCenter_Orders where ID = 66852
insert into tblCarrierRateSheet (CarrierID, EffectiveDate, ratetypeid, UomID) values (null, '1/1/2014', 1, 1)
insert into tblCarrierRateSheet (CarrierID, RegionID, EffectiveDate, ratetypeid, UomID) values (null, 2, '1/1/2014', 1, 1)
insert into tblCarrierRangeRate (RateSheetID, MaxRange, Rate) values (1, 300, 10)
insert into tblCarrierRangeRate (RateSheetID, MaxRange, Rate) values (2, 300, 15)

insert into tblCarrierOriginWaitRate (CarrierID, originid, EffectiveDate, Rate) values (null, null, '1/1/2014', 10.01)

select * from viewCarrierAssessorialRate
GO

sp_who2
kill 51
kill 54
*/