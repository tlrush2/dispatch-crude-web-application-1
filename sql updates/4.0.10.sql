SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.0.9'
SELECT  @NewVersion = '4.0.10'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1644 Average route times for calculating Estimated Cost of Time (ECOT)'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


INSERT INTO tblSetting
	(ID, Name, SettingTypeID, Value, Category, CreateDateUTC, CreatedByUser, ReadOnly)
VALUES
	(64, 'ECOT - Number of days back for averaging route times (0 is OFF)', 3, '0', 'System Wide', GETUTCDATE(), 'System', 0)
GO


ALTER TABLE tblRoute ADD ECOT INT NULL -- In minutes
ALTER TABLE tblRoute ADD ECOTLocked BIT NOT NULL DEFAULT 0
GO 

sp_refreshview viewroute
GO

/*************************************************/
-- Creation Info: x.x.x - 2016/08/30
-- Author: Joe Engler
-- Purpose: Calculate estimated cost of time (ECOT) or trip time for a given route.  Will only override if ECOTLocked is false
-- Changes: 
/*************************************************/
CREATE PROCEDURE spAverageRouteTimes AS
BEGIN
	PRINT 'spAverageRouteTimes START: '
	PRINT GETDATE()

	DECLARE @MIN_TRIPS INT = 10
	DECLARE @__SYSTEM_SETTING_AUTO_AVERAGE_ECOT__ INT = 64

	DECLARE @DAYSBACK INT = dbo.fnToInt(dbo.fnSettingValue(@__SYSTEM_SETTING_AUTO_AVERAGE_ECOT__))

	IF (@DAYSBACK IS NULL OR @DAYSBACK = 0)
		PRINT 'System setting turned OFF'
	ELSE
	BEGIN
		PRINT 'Averaging route times using last '+ CAST(@DAYSBACK AS VARCHAR) + ' days'
		;
		WITH xECOT AS
		(
			-- Select routes with enough orders to average (delivered within last 6 months)
			SELECT ID
			FROM tblRoute r
			WHERE ECOTLocked = 0
			  AND (SELECT COUNT(*) FROM tblOrder o 
						WHERE o.RouteID = r.ID 
						  AND o.StatusID IN (3,4)
						  AND o.Rejected = 0 AND o.DeleteDateUTC IS NULL
						  AND o.OrderDate > DATEADD(DAY, -@DAYSBACK, GETUTCDATE()) ) > @MIN_TRIPS
		)
		UPDATE tblRoute
		   SET ECOT = (SELECT AVG(ECOT) FROM (
							SELECT NTILE(5) OVER(ORDER BY ECOT) AS segment, * FROM (
									SELECT ECOT=DATEDIFF(MINUTE, o.OriginArriveTimeUTC, o.DestDepartTimeUTC) FROM tblOrder o
  									 WHERE o.RouteID = xECOT.ID
									   AND o.StatusID IN (3,4)
									   AND o.Rejected = 0 AND o.DeleteDateUTC IS NULL
									   AND o.OrderDate > DATEADD(DAY, -@DAYSBACK, GETUTCDATE()) ) t
							) t2
							WHERE segment NOT IN (1, 5) -- omit top and bottom 20%
					 ),
			   LastChangeDateUTC = GETUTCDATE(),
			   LastChangedByUser = 'Auto ECOT'
		  FROM xECOT
		 WHERE tblRoute.ID = xECOT.ID
		   AND tblRoute.ECOTLocked = 0

	END
	
	PRINT 'spAverageRouteTimes END: '
	PRINT GETDATE()
END

GO



COMMIT
SET NOEXEC OFF