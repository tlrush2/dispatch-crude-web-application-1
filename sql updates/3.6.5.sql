-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.6.4'
SELECT  @NewVersion = '3.6.5'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'Settings: more granular settings section'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

SET IDENTITY_INSERT tblReportColumnDefinition ON
INSERT INTO tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport)
	SELECT 90003, 1, 'replace(dbo.fnOrderCarrierAssessorialDetailsTSV(ID, 0), char(13), char(13) + char(10))', 'SETTLEMENT | CARRIER | Carrier Misc Details', NULL, NULL, 1, NULL, 1, 'Administrator,Management,Carrier', 1
	UNION
	SELECT 90004, 1, 'replace(dbo.fnOrderCarrierAssessorialAmountsTSV(ID, 0), char(13), char(13) + char(10))', 'SETTLEMENT | CARRIER | Carrier Misc $$', NULL, NULL, 1, NULL, 1, 'Administrator,Management,Carrier', 1
	UNION
	SELECT 90005, 1, 'replace(dbo.fnOrderShipperAssessorialDetailsTSV(ID, 0), char(13), char(13) + char(10))', 'SETTLEMENT | SHIPPER | Shipper Misc Details', NULL, NULL, 1, NULL, 1, 'Administrator,Management,Customer', 1
	UNION
	SELECT 90006, 1, 'replace(dbo.fnOrderShipperAssessorialAmountsTSV(ID, 0), char(13), char(13) + char(10))', 'SETTLEMENT | SHIPPER | Shipper Misc $$', NULL, NULL, 1, NULL, 1, 'Administrator,Management,Customer', 1
	UNION
	SELECT 90007, 1, '(SELECT sum(Amount) FROM tblOrderSettlementCarrierAssessorialCharge X WHERE X.OrderID=RS.ID AND X.AssessorialRateTypeID > 4)', 'SETTLEMENT | CARRIER | Carrier Misc Total $$', NULL, NULL, 1, NULL, 1, 'Administrator,Management,Carrier', 1
	UNION
	SELECT 90008, 1, '(SELECT sum(Amount) FROM tblOrderSettlementShipperAssessorialCharge X WHERE X.OrderID=RS.ID AND X.AssessorialRateTypeID > 4)', 'SETTLEMENT | SHIPPER | Shipper Misc Total $$', NULL, NULL, 1, NULL, 1, 'Administrator,Management,Customer', 1
SET IDENTITY_INSERT tblReportColumnDefinition OFF
GO

COMMIT
SET NOEXEC OFF