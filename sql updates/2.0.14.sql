/* add CorrectedAPIGravities computed field
-- fix the missing Reroute field data in spOrdersExportCustomer/Carrier procs
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.0.13', @NewVersion = '2.0.14'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

/***********************************/
-- Date Created: 28 Feb 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc for 3rd parties (Customer/Carrier)
/***********************************/
ALTER PROCEDURE [dbo].[spOrdersFullExport]
(
  @StartDate datetime
, @EndDate datetime
, @CarrierID int = 0 -- -1 = all carriers
, @CustomerID int = 0 -- -1 = all customers
, @ProducerID int = 0
, @AuditedOnly bit = 0
) AS BEGIN
	SELECT @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	
	SELECT * 
		, cast(CASE WHEN TicketCount = 0 THEN TicketType ELSE NULL END as varchar(max)) AS TicketTypes
		, cast(CASE WHEN TicketCount = 0 THEN CarrierTicketNum ELSE NULL END as varchar(max)) AS TicketNums
		, cast(NULL as varchar(max)) AS BOLNums
		, cast(NULL AS varchar(max)) AS TankNums
		, CAST(NULL AS varchar(max)) AS OpenReadings
		, CAST(NULL AS varchar(max)) AS CloseReadings
		, cast(NULL as varchar(max)) AS ProductObsTemps
		, cast(NULL as varchar(max)) AS ProductObsGravities
		, CAST(NULL AS varchar(max)) AS CorrectedAPIGravities
		, cast(NULL as varchar(max)) AS ProductBSWs
		, cast(NULL as varchar(max)) AS TicketsRejected
		, cast(NULL as varchar(max)) AS TicketsRejectNotes
		, cast(NULL as varchar(max)) AS TicketsGrossBBLS
		, cast(NULL as varchar(max)) AS TicketsNetBBLS
		, cast(NULL as varchar(max)) AS SealOffs
		, cast(NULL as varchar(max)) AS SealOns
		, cast(NULL as varchar(max)) AS PreviousDestinations
		, cast(NULL as varchar(max)) AS RerouteUsers
		, cast(NULL as varchar(max)) AS RerouteDates
		, cast(NULL as varchar(max)) AS RerouteNotes
	INTO #Orders
	FROM dbo.viewOrderExportFull
	WHERE (@CarrierID=-1 OR @CustomerID=-1 OR CarrierID=@CarrierID OR CustomerID=@CustomerID OR ProducerID=@ProducerID) 
	  AND OrderDate BETWEEN @StartDate AND @EndDate
	  AND DeleteDateUTC IS NULL
	  AND (@AuditedOnly = 0 OR StatusID = 4)
	ORDER BY OrderDate
	
	DECLARE @OrderID int, @TicketID int, @RerouteID int
	SELECT @OrderID = min(ID) FROM #Orders WHERE ID > 0
	PRINT isnull(@OrderID, -1)
	
	WHILE @OrderID IS NOT NULL
	BEGIN
		-- add the Tickets information to the order (done this way since there could be multiple)
		SELECT @TicketID = min(ID) FROM dbo.tblOrderTicket WHERE OrderID = @OrderID AND DeleteDateUTC IS NULL AND ID > 0
		WHILE @TicketID IS NOT NULL
		BEGIN
			UPDATE #Orders 
				SET TicketTypes = isnull(O.TicketTypes + '<br/>', '') + OT.TicketType
					, TicketNums = isnull(O.TicketNums + '<br/>', '') + OT.CarrierTicketNum
					, BOLNums = isnull(O.BOLNums + '<br/>', '') + OT.BOLNum
					, TankNums = isnull(O.TankNums + '<br/>', '') + OT.TankNum
					, OpenReadings = ISNULL(ltrim(OT.OpeningGaugeFeet) + 'ft ' + ltrim(OT.OpeningGaugeInch) + 'in ' + ltrim(OT.OpeningGaugeQ) + 'q' + '<br/>', '')
					, CloseReadings = ISNULL(ltrim(OT.ClosingGaugeFeet) + 'ft ' + ltrim(OT.ClosingGaugeInch) + 'in ' + ltrim(OT.ClosingGaugeQ) + 'q' + '<br/>', '')
					, ProductObsTemps = isnull(O.ProductObsTemps + '<br/>', '') + isnull(ltrim(OT.ProductObsTemp), '')
					, ProductObsGravities = isnull(O.ProductObsGravities + '<br/>', '') + isnull(ltrim(OT.ProductObsGravity), '')
					-- using cast(xx as decimal(9,3)) to properly round to 3 decimal digits (with no trailing 0's)
					, CorrectedAPIGravities = ISNULL(CorrectedAPIGravities + '<br/>', '') + isnull(ltrim(round(cast(dbo.fnCorrectedAPIGravity(OT.ProductObsGravity, OT.ProductObsTemp) as decimal(9,3)), 10, 3)), '')
					, ProductBSWs = isnull(O.ProductBSWs + '<br/>', '') + isnull(ltrim(OT.ProductBSW), '')
					, SealOffs = isnull(O.SealOffs + '<br/>', '') + isnull(ltrim(OT.SealOff), '')
					, SealOns = isnull(O.SealOns + '<br/>', '') + isnull(ltrim(OT.SealOn), '')
					, TicketsRejected = isnull(O.TicketsRejected + '<br/>', '') + (CASE WHEN OT.Rejected = 0 THEN 'Valid' ELSE 'Rejected' END)
					, TicketsRejectNotes = isnull(O.TicketsRejectNotes + '<br/>', '') + isnull(OT.RejectNotes, '')
					, TicketsGrossBBLS = isnull(O.TicketsGrossBBLS + '<br/>', '') + isnull(ltrim(GrossBarrels), '')
					, TicketsNetBBLS = isnull(O.TicketsNetBBLS + '<br/>', '') + isnull(ltrim(NetBarrels), '')
			FROM #Orders O, viewOrderTicket OT
			WHERE O.ID = @OrderID AND OT.ID = @TicketID
			SELECT @TicketID = min(ID) FROM tblOrderTicket WHERE OrderID = @OrderID AND ID > @TicketID AND DeleteDateUTC IS NULL
		END	
		-- add the Reroutes information to the order (done this way since there could be multiple)
		SELECT @RerouteID = min(ID) FROM dbo.tblOrderReroute WHERE OrderID = @OrderID AND ID > 0
		WHILE @RerouteID IS NOT NULL
		BEGIN
			UPDATE #Orders
				SET PreviousDestinations = isnull(O.PreviousDestinations + '<br/>', '') + O_R.PreviousDestinationFull
					, RerouteUsers = isnull(O.RerouteUsers + '<br/>', '') + O_R.UserName
					, RerouteDates = isnull(O.RerouteDates + '<br/>', '') + dbo.fnDateMdYY(O_R.RerouteDate)
					, RerouteNotes = isnull(O.RerouteNotes + '<br/>', '') + isnull(O_R.Notes, '')
			FROM #Orders O, viewOrderReroute O_R
			WHERE O.ID = @OrderID AND O_R.ID = @RerouteID
			SELECT @RerouteID = min(ID) FROM tblOrderReroute WHERE OrderID = @OrderID AND ID > @RerouteID
		END
		SELECT @OrderID = min(ID) FROM tblOrder WHERE ID > @OrderID
	END
	SELECT * FROM #Orders
END

GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
/***********************************/
ALTER PROCEDURE [dbo].[spOrdersExportCarrier]
(
  @StartDate datetime = NULL
, @EndDate datetime = NULL
, @CarrierID int = -1 -- all carriers
, @BatchID int = NULL  -- either show unbatched (NULL), or the specified batch orders
) AS BEGIN
	SELECT @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	
	SELECT OE.* 
		, dbo.fnLocal_to_UTC(ISNULL(OIC.LastChangeDateUTC, OIC.CreateDateUTC), OE.OriginTimeZoneID, OE.OriginUseDST) AS RatesAppliedDate
		, SB.BatchNum
		, OIC.ChainupFee
		, OIC.RerouteFee
		, OIC.BillableWaitMinutes
		, OIC.WaitFeeSubUnitID
		, OIC.WaitFeeRoundingTypeID
		, OIC.WaitRate
		, OIC.WaitFee
		, OIC.RejectionFee
		, OIC.H2SRate
		, OIC.H2SFee
		, OIC.TaxRate
		, OIC.RouteRate
		, OIC.LoadFee
		, OIC.TotalFee
		, OIC.FuelSurcharge
		, C.MinSettlementBarrels
		, cast(CASE WHEN OE.TicketCount = 0 THEN OE.TicketType ELSE NULL END as varchar(max)) AS TicketTypes
		, cast(CASE WHEN OE.TicketCount = 0 THEN OE.CarrierTicketNum ELSE NULL END as varchar(max)) AS TicketNums
		, cast(NULL AS varchar(max)) AS TankNums
		, cast(NULL as varchar(max)) AS ProductObsTemps
		, cast(NULL as varchar(max)) AS ProductObsGravities
		, cast(NULL as varchar(max)) AS ProductBSWs
		, cast(NULL as varchar(max)) AS TicketsRejected
		, cast(NULL as varchar(max)) AS TicketsRejectNotes
		, cast(NULL as varchar(max)) AS TicketsGrossBBLS
		, cast(NULL as varchar(max)) AS TicketsNetBBLS
		, cast(NULL as varchar(max)) AS PreviousDestinations
		, cast(NULL as varchar(max)) AS RerouteUsers
		, cast(NULL as varchar(max)) AS RerouteDates
		, cast(NULL as varchar(max)) AS RerouteNotes
	INTO #Orders
	FROM dbo.viewOrderExportFull OE
	JOIN dbo.tblCarrier C ON C.ID = OE.CarrierID
	LEFT JOIN dbo.tblOrderInvoiceCarrier OIC ON OIC.OrderID = OE.ID
	LEFT JOIN dbo.tblCarrierSettlementBatch SB ON SB.ID = OIC.BatchID
	WHERE OE.StatusID IN (4)  
	  AND (@CarrierID=-1 OR OE.CarrierID=@CarrierID) 
	  AND (@StartDate IS NULL OR OE.OrderDate >= @StartDate) 
	  AND (@EndDate IS NULL OR OE.OrderDate <= @EndDate)
	  AND ((@BatchID IS NULL AND (OIC.ID IS NULL OR OIC.BatchID IS NULL)) OR OIC.BatchID = @BatchID)
	  AND OE.DeleteDateUTC IS NULL
	ORDER BY OE.OriginDepartTimeUTC
	
	DECLARE @OrderID int, @TicketID int, @RerouteID int
	SELECT @OrderID = min(ID) FROM #Orders WHERE ID > 0
	PRINT isnull(@OrderID, -1)
	
	WHILE @OrderID IS NOT NULL
	BEGIN
		-- add the Tickets information to the order (done this way since there could be multiple)
		SELECT @TicketID = min(ID) FROM dbo.tblOrderTicket WHERE OrderID = @OrderID AND DeleteDateUTC IS NULL AND ID > 0
		WHILE @TicketID IS NOT NULL
		BEGIN
			UPDATE #Orders 
				SET TicketTypes = isnull(O.TicketTypes + '<br/>', '') + OT.TicketType
					, TicketNums = isnull(O.TicketNums + '<br/>', '') + OT.CarrierTicketNum
					, TankNums = isnull(O.TankNums + '<br/>', '') + OT.TankNum
					, ProductObsTemps = isnull(O.ProductObsTemps + '<br/>', '') + isnull(ltrim(OT.ProductObsTemp), '')
					, ProductObsGravities = isnull(O.ProductObsGravities + '<br/>', '') + isnull(ltrim(OT.ProductObsGravity), '')
					, ProductBSWs = isnull(O.ProductBSWs + '<br/>', '') + isnull(ltrim(OT.ProductBSW), '')
					, TicketsRejected = isnull(O.TicketsRejected + '<br/>', '') + (CASE WHEN OT.Rejected = 0 THEN 'Valid' ELSE 'Rejected' END)
					, TicketsRejectNotes = isnull(O.TicketsRejectNotes + '<br/>', '') + isnull(OT.RejectNotes, '')
					, TicketsGrossBBLS = isnull(O.TicketsGrossBBLS + '<br/>', '') + isnull(ltrim(GrossBarrels), '')
					, TicketsNetBBLS = isnull(O.TicketsNetBBLS + '<br/>', '') + isnull(ltrim(NetBarrels), '')
			FROM #Orders O, viewOrderTicket OT
			WHERE O.ID = @OrderID AND OT.ID = @TicketID
			SELECT @TicketID = min(ID) FROM tblOrderTicket WHERE OrderID = @OrderID AND ID > @TicketID AND DeleteDateUTC IS NULL
		END	
		-- add the Reroutes information to the order (done this way since there could be multiple)
		SELECT @RerouteID = min(ID) FROM dbo.tblOrderReroute WHERE OrderID = @OrderID AND ID > 0
		WHILE @RerouteID IS NOT NULL
		BEGIN
			UPDATE #Orders
				SET PreviousDestinations = isnull(O.PreviousDestinations + '<br/>', '') + O_R.PreviousDestinationFull
					, RerouteUsers = isnull(O.RerouteUsers + '<br/>', '') + O_R.UserName
					, RerouteDates = isnull(O.RerouteDates + '<br/>', '') + dbo.fnDateMdYY(O_R.RerouteDate)
					, RerouteNotes = isnull(O.RerouteNotes + '<br/>', '') + isnull(O_R.Notes, '')
			FROM #Orders O, viewOrderReroute O_R
			WHERE O.ID = @OrderID AND O_R.ID = @RerouteID
			SELECT @RerouteID = min(ID) FROM tblOrderReroute WHERE OrderID = @OrderID AND ID > @RerouteID
		END
		SELECT @OrderID = min(ID) FROM tblOrder WHERE ID > @OrderID
	END
	SELECT * FROM #Orders
END

GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
/***********************************/
ALTER PROCEDURE [dbo].[spOrdersExportCustomer]
(
  @StartDate datetime = NULL
, @EndDate datetime = NULL
, @CustomerID int = -1 -- all carriers
, @BatchID int = NULL  -- either show unbatched (NULL), or the specified batch orders
) AS BEGIN
	SELECT @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	
	SELECT OE.* 
		, dbo.fnLocal_to_UTC(ISNULL(OIC.LastChangeDateUTC, OIC.CreateDateUTC), OE.OriginTimeZoneID, OE.OriginUseDST) AS RatesAppliedDate
		, SB.BatchNum
		, OIC.ChainupFee
		, OIC.RerouteFee
		, OIC.BillableWaitMinutes
		, OIC.WaitFeeSubUnitID
		, OIC.WaitFeeRoundingTypeID
		, OIC.WaitRate
		, OIC.WaitFee
		, OIC.RejectionFee
		, OIC.H2SRate
		, OIC.H2SFee
		, OIC.TaxRate
		, OIC.RouteRate
		, OIC.LoadFee
		, OIC.TotalFee
		, OIC.FuelSurcharge
		, C.MinSettlementBarrels
		, cast(CASE WHEN OE.TicketCount = 0 THEN OE.TicketType ELSE NULL END as varchar(max)) AS TicketTypes
		, cast(CASE WHEN OE.TicketCount = 0 THEN OE.CarrierTicketNum ELSE NULL END as varchar(max)) AS TicketNums
		, cast(NULL AS varchar(max)) AS TankNums
		, cast(NULL as varchar(max)) AS ProductObsTemps
		, cast(NULL as varchar(max)) AS ProductObsGravities
		, cast(NULL as varchar(max)) AS ProductBSWs
		, cast(NULL as varchar(max)) AS TicketsRejected
		, cast(NULL as varchar(max)) AS TicketsRejectNotes
		, cast(NULL as varchar(max)) AS TicketsGrossBBLS
		, cast(NULL as varchar(max)) AS TicketsNetBBLS
		, cast(NULL as varchar(max)) AS PreviousDestinations
		, cast(NULL as varchar(max)) AS RerouteUsers
		, cast(NULL as varchar(max)) AS RerouteDates
		, cast(NULL as varchar(max)) AS RerouteNotes
	INTO #Orders
	FROM dbo.viewOrderExportFull OE
	JOIN dbo.tblCustomer C ON C.ID = OE.CustomerID
	LEFT JOIN dbo.tblOrderInvoiceCustomer OIC ON OIC.OrderID = OE.ID
	LEFT JOIN dbo.tblCustomerSettlementBatch SB ON SB.ID = OIC.BatchID
	WHERE OE.StatusID IN (4)  
	  AND (@CustomerID=-1 OR OE.CustomerID=@CustomerID) 
	  AND (@StartDate IS NULL OR OE.OrderDate >= @StartDate) 
	  AND (@EndDate IS NULL OR OE.OrderDate <= @EndDate)
	  AND ((@BatchID IS NULL AND (OIC.ID IS NULL OR OIC.BatchID IS NULL)) OR OIC.BatchID = @BatchID)
	  AND OE.DeleteDateUTC IS NULL
	ORDER BY OE.OrderDate
	
	DECLARE @OrderID int, @TicketID int, @RerouteID int
	SELECT @OrderID = min(ID) FROM #Orders WHERE ID > 0
	PRINT isnull(@OrderID, -1)
	
	WHILE @OrderID IS NOT NULL
	BEGIN
		-- add the Tickets information to the order (done this way since there could be multiple)
		SELECT @TicketID = min(ID) FROM dbo.tblOrderTicket WHERE OrderID = @OrderID AND DeleteDateUTC IS NULL AND ID > 0
		WHILE @TicketID IS NOT NULL
		BEGIN
			UPDATE #Orders 
				SET TicketTypes = isnull(O.TicketTypes + '<br/>', '') + OT.TicketType
					, TicketNums = isnull(O.TicketNums + '<br/>', '') + OT.CarrierTicketNum
					, TankNums = isnull(O.TankNums + '<br/>', '') + OT.TankNum
					, ProductObsTemps = isnull(O.ProductObsTemps + '<br/>', '') + isnull(ltrim(OT.ProductObsTemp), '')
					, ProductObsGravities = isnull(O.ProductObsGravities + '<br/>', '') + isnull(ltrim(OT.ProductObsGravity), '')
					, ProductBSWs = isnull(O.ProductBSWs + '<br/>', '') + isnull(ltrim(OT.ProductBSW), '')
					, TicketsRejected = isnull(O.TicketsRejected + '<br/>', '') + (CASE WHEN OT.Rejected = 0 THEN 'Valid' ELSE 'Rejected' END)
					, TicketsRejectNotes = isnull(O.TicketsRejectNotes + '<br/>', '') + isnull(OT.RejectNotes, '')
					, TicketsGrossBBLS = isnull(O.TicketsGrossBBLS + '<br/>', '') + isnull(ltrim(GrossBarrels), '')
					, TicketsNetBBLS = isnull(O.TicketsNetBBLS + '<br/>', '') + isnull(ltrim(NetBarrels), '')
			FROM #Orders O, viewOrderTicket OT
			WHERE O.ID = @OrderID AND OT.ID = @TicketID
			SELECT @TicketID = min(ID) FROM tblOrderTicket WHERE OrderID = @OrderID AND ID > @TicketID AND DeleteDateUTC IS NULL
		END	
		-- add the Reroutes information to the order (done this way since there could be multiple)
		SELECT @RerouteID = min(ID) FROM dbo.tblOrderReroute WHERE OrderID = @OrderID AND ID > 0
		WHILE @RerouteID IS NOT NULL
		BEGIN
			UPDATE #Orders
				SET PreviousDestinations = isnull(O.PreviousDestinations + '<br/>', '') + O_R.PreviousDestinationFull
					, RerouteUsers = isnull(O.RerouteUsers + '<br/>', '') + O_R.UserName
					, RerouteDates = isnull(O.RerouteDates + '<br/>', '') + dbo.fnDateMdYY(O_R.RerouteDate)
					, RerouteNotes = isnull(O.RerouteNotes + '<br/>', '') + isnull(O_R.Notes, '')
			FROM #Orders O, viewOrderReroute O_R
			WHERE O.ID = @OrderID AND O_R.ID = @RerouteID
			SELECT @RerouteID = min(ID) FROM tblOrderReroute WHERE OrderID = @OrderID AND ID > @RerouteID
		END
		SELECT @OrderID = min(ID) FROM tblOrder WHERE ID > @OrderID
	END
	SELECT * FROM #Orders
END

GO

COMMIT
SET NOEXEC OFF