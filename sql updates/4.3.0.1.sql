SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.3.0'
SELECT  @NewVersion = '4.3.0.1'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1920: Add route (rate) miles to order create page'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/* Create new system setting to show Route (rate) miles on truck orders page */
INSERT INTO tblSetting (ID, Name, SettingTypeID, Value, Category)
	SELECT 66, 'Display Rate Miles on Truck Orders page', 2, 'FALSE', 'Order Create'
GO


COMMIT
SET NOEXEC OFF