/* add Settlement Batching to Settlement processes (both carrier & shipper)
-- also many performance optimizations
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '1.6.8', @NewVersion = '1.6.9'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

CREATE TABLE tblCarrierSettlementBatch
(
  ID int identity(1, 1) not null
, BatchNum int not null
, CarrierID int not null
, Notes varchar(255) null
, CreateDate smalldatetime not null
, CreatedByUser varchar(100) null
, LastChangeDate smalldatetime null
, LastChangedByUser varchar(100) null
, CONSTRAINT PK_CarrierSettlementBatch PRIMARY KEY (ID)
)
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblCarrierSettlementBatch TO dispatchcrude_iis_acct
GO

ALTER TABLE dbo.tblCarrierSettlementBatch ADD CONSTRAINT
	FK_CarrierSettlementBatch_Carrier FOREIGN KEY
	(
		CarrierID
	) REFERENCES dbo.tblCarrier
	(
		ID
	) ON UPDATE CASCADE
	 ON DELETE CASCADE
	
GO

CREATE UNIQUE INDEX uidxCarrierSettlementBatch ON tblCarrierSettlementBatch(BatchNum)
GO

ALTER TABLE tblOrderInvoiceCarrier ADD BatchID int null
GO
ALTER TABLE dbo.tblOrderInvoiceCarrier ADD CONSTRAINT
	FK_OrderInvoiceCarrier_SettlementBatch FOREIGN KEY
	(
	BatchID
	) REFERENCES dbo.tblCarrierSettlementBatch
	(
	ID
	) ON UPDATE CASCADE
	 ON DELETE SET NULL
	
GO

CREATE TABLE tblCustomerSettlementBatch
(
  ID int identity(1, 1) not null
, BatchNum int not null
, CustomerID int not null
, Notes varchar(255) null
, CreateDate smalldatetime not null
, CreatedByUser varchar(100) null
, LastChangeDate smalldatetime null
, LastChangedByUser varchar(100) null
, CONSTRAINT PK_CustomerSettlementBatch PRIMARY KEY (ID)
)
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblCustomerSettlementBatch TO dispatchcrude_iis_acct
GO

ALTER TABLE dbo.tblCustomerSettlementBatch ADD CONSTRAINT
	FK_CustomerSettlementBatch_Customer FOREIGN KEY
	(
		CustomerID
	) REFERENCES dbo.tblCustomer
	(
		ID
	) ON UPDATE CASCADE
	 ON DELETE CASCADE
	
GO

CREATE UNIQUE INDEX uidxCustomerSettlementBatch ON tblCustomerSettlementBatch(BatchNum)
GO

ALTER TABLE tblOrderInvoiceCustomer ADD BatchID int null
GO
ALTER TABLE dbo.tblOrderInvoiceCustomer ADD CONSTRAINT
	FK_OrderInvoiceCustomer_SettlementBatch FOREIGN KEY
	(
	BatchID
	) REFERENCES dbo.tblCustomerSettlementBatch
	(
	ID
	) ON UPDATE CASCADE
	 ON DELETE SET NULL
	
GO

/***********************************/
-- Date Created: 16 Aug 2013
-- Author: Kevin Alons
-- Purpose: compute the next CarrierSettlementBatch.BatchNum
/***********************************/
CREATE FUNCTION fnNextCarrierSettlementBatchNum() RETURNS int AS 
BEGIN 
	DECLARE @ret int
	SELECT @ret = isnull(MAX(BatchNum), 0) + 1 FROM tblCarrierSettlementBatch
	RETURN @ret
END
GO

GRANT EXECUTE ON fnNextCarrierSettlementBatchNum TO dispatchcrude_iis_acct
GO

INSERT INTO tblCarrierSettlementBatch (CarrierID, BatchNum, CreateDate, CreatedByUser) 
	SELECT CarrierID, ROW_NUMBER() OVER(ORDER BY CarrierID) AS BatchNum, GETDATE(), 'System'
	FROM tblOrder O
	JOIN tblOrderInvoiceCarrier OIC ON OIC.OrderID = O.ID
	WHERE StatusID = 4 AND OIC.Invoiced = 1
	GROUP BY CarrierID
GO
UPDATE tblOrderInvoiceCarrier 
	SET BatchID = SB.ID
FROM tblOrderInvoiceCarrier OIC
JOIN tblOrder O ON O.ID = OIC.OrderID
JOIN tblCarrierSettlementBatch SB ON SB.CarrierID = O.CarrierID
WHERE OIC.Invoiced = 1

GO
ALTER TABLE tblOrderInvoiceCarrier DROP CONSTRAINT DF_tblOrderInvoiceCarrier_Invoiced
GO
ALTER TABLE tblOrderInvoiceCarrier DROP COLUMN Invoiced
GO

/***********************************/
-- Date Created: 16 Aug 2013
-- Author: Kevin Alons
-- Purpose: compute the next CustomerSettlementBatch.BatchNum
/***********************************/
CREATE FUNCTION fnNextCustomerSettlementBatchNum() RETURNS int AS 
BEGIN 
	DECLARE @ret int
	SELECT @ret = isnull(MAX(BatchNum), 0) + 1 FROM tblCustomerSettlementBatch
	RETURN @ret
END
GO

GRANT EXECUTE ON fnNextCustomerSettlementBatchNum TO dispatchcrude_iis_acct
GO

INSERT INTO tblCustomerSettlementBatch (CustomerID, BatchNum, CreateDate, CreatedByUser) 
	SELECT CustomerID, ROW_NUMBER() OVER(ORDER BY CustomerID) AS BatchNum, GETDATE(), 'System'
	FROM tblOrder O
	JOIN tblOrderInvoiceCustomer OIC ON OIC.OrderID = O.ID
	WHERE StatusID = 4 AND OIC.Invoiced = 1
	GROUP BY CustomerID
GO
UPDATE tblOrderInvoiceCustomer 
	SET BatchID = SB.ID
FROM tblOrderInvoiceCustomer OIC
JOIN tblOrder O ON O.ID = OIC.OrderID
JOIN tblCustomerSettlementBatch SB ON SB.CustomerID = O.CustomerID
WHERE OIC.Invoiced = 1

GO

ALTER TABLE tblOrderInvoiceCustomer DROP CONSTRAINT DF_tblOrderInvoiceCustomer_Invoiced
GO
ALTER TABLE tblOrderInvoiceCustomer DROP COLUMN Invoiced
GO

/***********************************/
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Order records with "translated friendly" values for FK relationships
/***********************************/
ALTER VIEW [dbo].[viewOrder] AS
SELECT O.*
, vO.Name AS Origin
, vO.FullName AS OriginFull
, vO.State AS OriginState
, vO.StateAbbrev AS OriginStateAbbrev
, vO.LeaseName
, vO.LeaseNum
, vO.H2S
, vD.Name AS Destination
, vD.FullName AS DestinationFull
, vD.State AS DestinationState
, vD.StateAbbrev AS DestinationStateAbbrev
, vD.DestinationType
, vD.Station
, C.Name AS Customer
, CA.Name AS Carrier
, CT.Name AS CarrierType
, OT.OrderStatus
, OT.StatusNum
, D.FullName AS Driver
, D.FirstName AS DriverFirst
, D.LastName AS DriverLast
, TRU.FullName AS Truck
, TR1.FullName AS Trailer
, TR2.FullName AS Trailer2
, P.PriorityNum
, TT.Name AS TicketType
, vD.TicketTypeID AS DestTicketTypeID
, vD.TicketType AS DestTicketType
, OP.Name AS Operator
, PR.Name AS Producer
, PU.FullName AS Pumper
, D.IDNumber AS DriverNumber
, CA.IDNumber AS CarrierNumber
, TRU.IDNumber AS TruckNumber
, TR1.IDNumber AS TrailerNumber
, TR2.IDNumber AS Trailer2Number
, cast((CASE WHEN O.DeleteDate IS NOT NULL THEN 1 ELSE 0 END) as bit) AS IsDeleted
FROM dbo.tblOrder O
LEFT JOIN dbo.viewOrigin vO ON vO.ID = O.OriginID
LEFT JOIN dbo.viewDestination vD ON vD.ID = O.DestinationID
LEFT JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
JOIN dbo.tblOrderStatus AS OT ON OT.ID = O.StatusID
LEFT JOIN dbo.tblCarrier CA ON CA.ID = O.CarrierID
LEFT JOIN dbo.tblCarrierType CT ON CT.ID = CA.CarrierTypeID
LEFT JOIN dbo.viewDriver D ON D.ID = O.DriverID
LEFT JOIN dbo.viewTruck TRU ON TRU.ID = O.TruckID
LEFT JOIN dbo.viewTrailer TR1 ON TR1.ID = O.TrailerID
LEFT JOIN dbo.viewTrailer TR2 ON TR2.ID = O.Trailer2ID
LEFT JOIN dbo.tblPriority P ON P.ID = O.PriorityID
LEFT JOIN dbo.tblTicketType TT ON TT.ID = O.TicketTypeID
LEFT JOIN dbo.tblOperator OP ON OP.ID = O.OperatorID
LEFT JOIN dbo.viewPumper PU ON PU.ID = O.PumperID
LEFT JOIN dbo.tblProducer PR ON PR.ID = O.ProducerID

GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 20 Mar 2013
-- Description:	return all CarrierTicketNums for an Order (separated by ",")
-- =============================================
ALTER VIEW [dbo].[viewOrder_AllTickets] AS 
WITH ActiveTickets AS
(
	SELECT ID, OrderID, CarrierTicketNum FROM tblOrderTicket WHERE DeleteDate IS NULL
)
, Tickets AS 
( 
	--initialization 
	SELECT OT.OrderID, OT.ID, cast(CarrierTicketNum as varchar(255)) AS Tickets
	FROM ActiveTickets OT 
	JOIN (SELECT OrderID, MIN(ID) AS ID FROM tblOrderTicket GROUP BY OrderID) OTI 
		ON OTI.OrderID = OT.OrderID AND OTI.ID = OT.ID
	
	UNION ALL 
	--recursive execution 
	SELECT T.OrderID, OT.ID, cast(T.Tickets + ',' + OT.CarrierTicketNum as varchar(255)) AS Tickets
	FROM ActiveTickets OT JOIN Tickets T ON OT.ID > T.ID AND OT.OrderID = T.OrderID
)

SELECT O.*, T.Tickets
FROM viewOrder O
LEFT JOIN (
	SELECT OrderID, max(Tickets) AS Tickets
	FROM Tickets T
	GROUP BY OrderID
) T ON T.OrderID = O.ID

GO

/***********************************/
-- Date Created: 28 Feb 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
/***********************************/
ALTER VIEW [dbo].[viewOrderExportFull] AS
SELECT *
  , OriginMinutes + DestMinutes AS TotalMinutes
  , isnull(OriginWaitMinutes, 0) + isnull(DestWaitMinutes, 0) AS TotalWaitMinutes
FROM (
	SELECT O.*
	  , dbo.fnMaxInt(0, isnull(OriginMinutes, 0) - cast(S.Value as int)) AS OriginWaitMinutes
	  , dbo.fnMaxInt(0, isnull(DestMinutes, 0) - cast(S.Value as int)) AS DestWaitMinutes
	  , (SELECT count(*) FROM tblOrderTicket WHERE OrderID = O.ID) AS TicketCount
	  , (SELECT count(*) FROM tblOrderReroute WHERE OrderID = O.ID) AS RerouteCount
	FROM dbo.viewOrder O
	JOIN dbo.tblSetting S ON S.ID = 7 -- the Unbillable Wait Time threshold minutes 
WHERE O.StatusID NOT IN (-10) -- don't include "Generated" orders (only Assigned+)
  AND O.DeleteDate IS NULL
) v

GO

/***********************************/
-- Date Created: 26 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Drivers table records with "friendly" translated values included
/***********************************/
ALTER VIEW [dbo].[viewDriver] AS
SELECT *
	, CASE WHEN DeleteDate IS NULL THEN '' ELSE 'Deleted: ' END + FullName AS FullNameD
FROM (
	SELECT D.*
	, cast(CASE WHEN isnull(C.DeleteDate, D.DeleteDate) IS NULL THEN 1 ELSE 0 END as bit) AS Active
	, D.FirstName + ' ' + D.LastName As FullName
	, D.LastName + ', ' + D.FirstName AS FullNameLF
	, isnull(CT.Name, 'Unknown') AS CarrierType
	, C.Name AS Carrier
	, S.Abbreviation AS StateAbbrev 
	, T.FullName AS Truck
	, T1.FullName AS Trailer
	, T2.FullName AS Trailer2
	FROM dbo.tblDriver D 
	JOIN dbo.tblCarrier C ON C.ID = D.CarrierID 
	LEFT JOIN dbo.tblCarrierType CT ON CT.ID = C.CarrierTypeID
	LEFT JOIN tblState S ON S.ID = D.StateID
	LEFT JOIN viewTruck T ON T.ID = D.TruckID
	LEFT JOIN viewTrailer T1 ON T1.ID = D.TrailerID
	LEFT JOIN viewTrailer T2 ON T2.ID = D.Trailer2ID
) X

GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 20 Mar 2013
-- Description:	return the data for the TAX_ND_10A report (for the specified date range)
-- =============================================
ALTER PROCEDURE [dbo].[spReport_Tax_ND10A]
(
  @CustomerID int
, @StartDate datetime
, @EndDate datetime
, @IncludeUninvoiced bit = 0
)
AS BEGIN
SELECT FieldName
	, Operator
	, LeaseNum AS LeaseNumber
	, Name AS [Well Name and Number]
	, NDICFileNum AS [NDIC CTB No.]
	, cast(round(SUM(OD.OriginNetBarrels), 0) as int) AS [Lease Total (Bbls)]
FROM viewOrigin O
JOIN tblOrder OD ON OD.OriginID = O.ID
JOIN tblOrderInvoiceCustomer IC ON IC.OrderID = OD.ID AND (@IncludeUninvoiced = 1 OR IC.BatchID IS NOT NULL)
WHERE O.OriginTypeID IN (2) -- WELLs ONLY
	AND OD.DeleteDate IS NULL
	AND (@CustomerID = -1 OR OD.CustomerID = @CustomerID)
	AND OriginDepartTime >= @StartDate AND OriginDepartTime < DATEADD(day, 1, @EndDate)
GROUP BY FieldName, Operator, LeaseNum, Name, NDICFileNum
ORDER BY FieldName, Operator, LeaseNum, Name, NDICFileNum
END

GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 20 Mar 2013
-- Description:	return the data for the TAX_ND_10B report (for the specified date range)
-- =============================================
ALTER PROCEDURE [dbo].[spReport_Tax_ND10B]
(
  @CustomerID int
, @StartDate datetime
, @EndDate datetime
, @IncludeUninvoiced bit = 0
)
AS BEGIN
SELECT Name AS [Point Received]
	, Customer AS [Purchaser]
	, Destination
	, cast(round(SUM(OD.OriginNetBarrels), 0) as int) AS [Lease Total (Bbls)]
FROM viewOrder OD
JOIN tblOrigin O ON O.ID = OD.OriginID
JOIN tblOrderInvoiceCustomer IC ON IC.OrderID = OD.ID AND (@IncludeUninvoiced = 1 OR IC.BatchID IS NOT NULL)
WHERE O.OriginTypeID IN (2) -- WELLs ONLY
	AND OD.DeleteDate IS NULL
	AND (@CustomerID = -1 OR OD.CustomerID = @CustomerID)
	AND OriginDepartTime >= @StartDate AND OriginDepartTime < DATEADD(day, 1, @EndDate)
GROUP BY Name, Customer, Destination
ORDER BY Name, Customer, Destination
END

GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
/***********************************/
ALTER PROCEDURE [dbo].[spOrdersExportCarrier]
(
  @StartDate datetime
, @EndDate datetime
, @CarrierID int = -1 -- all carriers
, @BatchID int = NULL  -- either show unbatched (NULL), or the specified batch orders
) AS BEGIN
	SELECT OE.* 
		, SB.BatchNum
		, OIC.ChainupFee
		, OIC.RerouteFee
		, OIC.BillableWaitMinutes
		, OIC.WaitFeeSubUnitID
		, OIC.WaitFeeRoundingTypeID
		, OIC.WaitRate
		, OIC.WaitFee
		, OIC.RejectionFee
		, OIC.H2SRate
		, OIC.H2SFee
		, OIC.TaxRate
		, OIC.RouteRate
		, OIC.LoadFee
		, OIC.TotalFee
		, OIC.FuelSurcharge
		, C.MinSettlementBarrels
		, cast(CASE WHEN OE.TicketCount = 0 THEN OE.TicketType ELSE NULL END as varchar(max)) AS TicketTypes
		, cast(CASE WHEN OE.TicketCount = 0 THEN OE.CarrierTicketNum ELSE NULL END as varchar(max)) AS TicketNums
		, cast(NULL AS varchar(max)) AS TankNums
		, cast(NULL as varchar(max)) AS ProductObsTemps
		, cast(NULL as varchar(max)) AS ProductObsGravities
		, cast(NULL as varchar(max)) AS ProductBSWs
		, cast(NULL as varchar(max)) AS TicketsRejected
		, cast(NULL as varchar(max)) AS TicketsRejectNotes
		, cast(NULL as varchar(max)) AS TicketsGrossBBLS
		, cast(NULL as varchar(max)) AS TicketsNetBBLS
		, cast(NULL as varchar(max)) AS PreviousDestinations
		, cast(NULL as varchar(max)) AS RerouteUsers
		, cast(NULL as varchar(max)) AS RerouteDates
		, cast(NULL as varchar(max)) AS RerouteNotes
	INTO #Orders
	FROM dbo.viewOrderExportFull OE
	JOIN dbo.tblCarrier C ON C.ID = OE.CarrierID
	LEFT JOIN dbo.tblOrderInvoiceCarrier OIC ON OIC.OrderID = OE.ID
	LEFT JOIN dbo.tblCarrierSettlementBatch SB ON SB.ID = OIC.BatchID
	WHERE OE.StatusID IN (4)  
	  AND (@CarrierID=-1 OR OE.CarrierID=@CarrierID) 
	  AND OriginDepartTime >= @StartDate AND OE.OriginDepartTime < dateadd(day, 1, @EndDate) 
	  AND ((@BatchID IS NULL AND (OIC.ID IS NULL OR OIC.BatchID IS NULL)) OR OIC.BatchID = @BatchID)
	  AND OE.DeleteDate IS NULL
	ORDER BY OE.OriginDepartTime
	
	DECLARE @OrderID int, @TicketID int, @RerouteID int
	SELECT @OrderID = min(ID) FROM #Orders WHERE ID > 0
	PRINT isnull(@OrderID, -1)
	
	WHILE @OrderID IS NOT NULL
	BEGIN
		-- add the Tickets information to the order (done this way since there could be multiple)
		SELECT @TicketID = min(ID) FROM dbo.tblOrderTicket WHERE OrderID = @OrderID AND DeleteDate IS NULL AND ID > 0
		WHILE @TicketID IS NOT NULL
		BEGIN
			UPDATE #Orders 
				SET TicketTypes = isnull(O.TicketTypes + '<br/>', '') + OT.TicketType
					, TicketNums = isnull(O.TicketNums + '<br/>', '') + OT.CarrierTicketNum
					, TankNums = isnull(O.TankNums + '<br/>', '') + OT.TankNum
					, ProductObsTemps = isnull(O.ProductObsTemps + '<br/>', '') + isnull(ltrim(OT.ProductObsTemp), '')
					, ProductObsGravities = isnull(O.ProductObsGravities + '<br/>', '') + isnull(ltrim(OT.ProductObsGravity), '')
					, ProductBSWs = isnull(O.ProductBSWs + '<br/>', '') + isnull(ltrim(OT.ProductBSW), '')
					, TicketsRejected = isnull(O.TicketsRejected + '<br/>', '') + (CASE WHEN OT.Rejected = 0 THEN 'Valid' ELSE 'Rejected' END)
					, TicketsRejectNotes = isnull(O.TicketsRejectNotes + '<br/>', '') + isnull(OT.RejectNotes, '')
					, TicketsGrossBBLS = isnull(O.TicketsGrossBBLS + '<br/>', '') + isnull(ltrim(GrossBarrels), '')
					, TicketsNetBBLS = isnull(O.TicketsNetBBLS + '<br/>', '') + isnull(ltrim(NetBarrels), '')
			FROM #Orders O, viewOrderTicket OT
			WHERE O.ID = @OrderID AND OT.ID = @TicketID
			SELECT @TicketID = min(ID) FROM tblOrderTicket WHERE OrderID = @OrderID AND ID > @TicketID AND DeleteDate IS NULL
		END	
		-- add the Reroutes information to the order (done this way since there could be multiple)
		SELECT @RerouteID = min(ID) FROM dbo.tblOrderReroute WHERE OrderID = @OrderID AND ID > 0
		WHILE @RerouteID IS NOT NULL
		BEGIN
			UPDATE #Orders
				SET PreviousDestinations = O.PreviousDestinations + '<br/>' + O_R.PreviousDestinationFull
					, RerouteUsers = O.RerouteUsers + '<br/>' + O_R.UserName
					, RerouteDates = O.RerouteDates + '<br/>' + dbo.fnDateMdYY(O_R.RerouteDate)
					, RerouteNotes = isnull(O.RerouteNotes + '<br/>', '') + isnull(O_R.Notes, '')
			FROM #Orders O, viewOrderReroute O_R
			WHERE O.ID = @OrderID AND O_R.ID = @RerouteID
			SELECT @RerouteID = min(ID) FROM tblOrderReroute WHERE OrderID = @OrderID AND ID > @RerouteID
		END
		SELECT @OrderID = min(ID) FROM tblOrder WHERE ID > @OrderID
	END
	SELECT * FROM #Orders
END

GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
/***********************************/
ALTER PROCEDURE [dbo].[spOrdersExportCustomer]
(
  @StartDate datetime
, @EndDate datetime
, @CustomerID int = -1 -- all carriers
, @BatchID int = NULL  -- either show unbatched (NULL), or the specified batch orders
) AS BEGIN
	SELECT OE.* 
		, SB.BatchNum
		, OIC.ChainupFee
		, OIC.RerouteFee
		, OIC.BillableWaitMinutes
		, OIC.WaitFeeSubUnitID
		, OIC.WaitFeeRoundingTypeID
		, OIC.WaitRate
		, OIC.WaitFee
		, OIC.RejectionFee
		, OIC.H2SRate
		, OIC.H2SFee
		, OIC.TaxRate
		, OIC.RouteRate
		, OIC.LoadFee
		, OIC.TotalFee
		, OIC.FuelSurcharge
		, C.MinSettlementBarrels
		, cast(CASE WHEN OE.TicketCount = 0 THEN OE.TicketType ELSE NULL END as varchar(max)) AS TicketTypes
		, cast(CASE WHEN OE.TicketCount = 0 THEN OE.CarrierTicketNum ELSE NULL END as varchar(max)) AS TicketNums
		, cast(NULL AS varchar(max)) AS TankNums
		, cast(NULL as varchar(max)) AS ProductObsTemps
		, cast(NULL as varchar(max)) AS ProductObsGravities
		, cast(NULL as varchar(max)) AS ProductBSWs
		, cast(NULL as varchar(max)) AS TicketsRejected
		, cast(NULL as varchar(max)) AS TicketsRejectNotes
		, cast(NULL as varchar(max)) AS TicketsGrossBBLS
		, cast(NULL as varchar(max)) AS TicketsNetBBLS
		, cast(NULL as varchar(max)) AS PreviousDestinations
		, cast(NULL as varchar(max)) AS RerouteUsers
		, cast(NULL as varchar(max)) AS RerouteDates
		, cast(NULL as varchar(max)) AS RerouteNotes
	INTO #Orders
	FROM dbo.viewOrderExportFull OE
	JOIN dbo.tblCustomer C ON C.ID = OE.CustomerID
	LEFT JOIN dbo.tblOrderInvoiceCustomer OIC ON OIC.OrderID = OE.ID
	LEFT JOIN dbo.tblCustomerSettlementBatch SB ON SB.ID = OIC.BatchID
	WHERE OE.StatusID IN (4)  
	  AND (@CustomerID=-1 OR OE.CustomerID=@CustomerID) 
	  AND OriginDepartTime >= @StartDate AND OE.OriginDepartTime < dateadd(day, 1, @EndDate) 
	  AND ((@BatchID IS NULL AND (OIC.ID IS NULL OR OIC.BatchID IS NULL)) OR OIC.BatchID = @BatchID)
	  AND OE.DeleteDate IS NULL
	ORDER BY OE.OriginDepartTime
	
	DECLARE @OrderID int, @TicketID int, @RerouteID int
	SELECT @OrderID = min(ID) FROM #Orders WHERE ID > 0
	PRINT isnull(@OrderID, -1)
	
	WHILE @OrderID IS NOT NULL
	BEGIN
		-- add the Tickets information to the order (done this way since there could be multiple)
		SELECT @TicketID = min(ID) FROM dbo.tblOrderTicket WHERE OrderID = @OrderID AND DeleteDate IS NULL AND ID > 0
		WHILE @TicketID IS NOT NULL
		BEGIN
			UPDATE #Orders 
				SET TicketTypes = isnull(O.TicketTypes + '<br/>', '') + OT.TicketType
					, TicketNums = isnull(O.TicketNums + '<br/>', '') + OT.CarrierTicketNum
					, TankNums = isnull(O.TankNums + '<br/>', '') + OT.TankNum
					, ProductObsTemps = isnull(O.ProductObsTemps + '<br/>', '') + isnull(ltrim(OT.ProductObsTemp), '')
					, ProductObsGravities = isnull(O.ProductObsGravities + '<br/>', '') + isnull(ltrim(OT.ProductObsGravity), '')
					, ProductBSWs = isnull(O.ProductBSWs + '<br/>', '') + isnull(ltrim(OT.ProductBSW), '')
					, TicketsRejected = isnull(O.TicketsRejected + '<br/>', '') + (CASE WHEN OT.Rejected = 0 THEN 'Valid' ELSE 'Rejected' END)
					, TicketsRejectNotes = isnull(O.TicketsRejectNotes + '<br/>', '') + isnull(OT.RejectNotes, '')
					, TicketsGrossBBLS = isnull(O.TicketsGrossBBLS + '<br/>', '') + isnull(ltrim(GrossBarrels), '')
					, TicketsNetBBLS = isnull(O.TicketsNetBBLS + '<br/>', '') + isnull(ltrim(NetBarrels), '')
			FROM #Orders O, viewOrderTicket OT
			WHERE O.ID = @OrderID AND OT.ID = @TicketID
			SELECT @TicketID = min(ID) FROM tblOrderTicket WHERE OrderID = @OrderID AND ID > @TicketID AND DeleteDate IS NULL
		END	
		-- add the Reroutes information to the order (done this way since there could be multiple)
		SELECT @RerouteID = min(ID) FROM dbo.tblOrderReroute WHERE OrderID = @OrderID AND ID > 0
		WHILE @RerouteID IS NOT NULL
		BEGIN
			UPDATE #Orders
				SET PreviousDestinations = O.PreviousDestinations + '<br/>' + O_R.PreviousDestinationFull
					, RerouteUsers = O.RerouteUsers + '<br/>' + O_R.UserName
					, RerouteDates = O.RerouteDates + '<br/>' + dbo.fnDateMdYY(O_R.RerouteDate)
					, RerouteNotes = isnull(O.RerouteNotes + '<br/>', '') + isnull(O_R.Notes, '')
			FROM #Orders O, viewOrderReroute O_R
			WHERE O.ID = @OrderID AND O_R.ID = @RerouteID
			SELECT @RerouteID = min(ID) FROM tblOrderReroute WHERE OrderID = @OrderID AND ID > @RerouteID
		END
		SELECT @OrderID = min(ID) FROM tblOrder WHERE ID > @OrderID
	END
	SELECT * FROM #Orders
END

GO

/***********************************/
-- Date Created: 15 Aug 2013
-- Author: Kevin Alons
-- Purpose: return the CarrierSettlementBatch table contents with the CreateDate formatted DateOnly (for the BatchDate)
/***********************************/
CREATE VIEW viewCarrierSettlementBatch 
AS
SELECT *
	, dbo.fnDateOnly(CreateDate) AS BatchDate
FROM dbo.tblCarrierSettlementBatch
GO
GRANT SELECT ON viewCarrierSettlementBatch TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 15 Aug 2013
-- Author: Kevin Alons
-- Purpose: return the CustomerSettlementBatch table contents with the CreateDate formatted DateOnly (for the BatchDate)
/***********************************/
CREATE VIEW viewCustomerSettlementBatch 
AS
SELECT *
	, dbo.fnDateOnly(CreateDate) AS BatchDate
FROM dbo.tblCustomerSettlementBatch
GO
GRANT SELECT ON viewCustomerSettlementBatch TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 15 Aug 2013
-- Author: Kevin Alons
-- Purpose: return the Route rate for the specified Carrier/Route/Date combination
/***********************************/
CREATE FUNCTION [dbo].[fnCarrierRouteRate](@CarrierID int, @RouteID int, @OrderDate smalldatetime) RETURNS money
BEGIN
	DECLARE @ret money
	
	SELECT @ret = Rate
	FROM tblCarrierRouteRates
	WHERE CarrierID = @CarrierID
	  AND RouteID = @RouteID
	  AND EffectiveDate = (
		SELECT MAX(EffectiveDate)
		FROM tblCarrierRouteRates
		WHERE CarrierID = @CarrierID
		  AND RouteID = @RouteID
		  AND EffectiveDate <= @OrderDate)
	
	RETURN @ret
END

GO

/***********************************/
-- Date Created: 15 Aug 2013
-- Author: Kevin Alons
-- Purpose: return the Route rate for the specified Customer/Route/Date combination
/***********************************/
CREATE FUNCTION [dbo].[fnCustomerRouteRate](@CustomerID int, @RouteID int, @OrderDate smalldatetime) RETURNS money
BEGIN
	DECLARE @ret money
	
	SELECT @ret = Rate
	FROM tblCustomerRouteRates
	WHERE CustomerID = @CustomerID
	  AND RouteID = @RouteID
	  AND EffectiveDate = (
		SELECT MAX(EffectiveDate)
		FROM tblCustomerRouteRates
		WHERE CustomerID = @CustomerID
		  AND RouteID = @RouteID
		  AND EffectiveDate <= @OrderDate)
	
	RETURN @ret
END

GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Carrier "Settlement" $$ values to an Audited order
/***********************************/
ALTER PROCEDURE [dbo].[spProcessCarrierInvoice]
(
  @ID int
, @UserName varchar(100)
, @ChainupFee smallmoney = NULL
, @WaitFee smallmoney = NULL
, @RerouteFee smallmoney = NULL
, @RejectionFee smallmoney = NULL
, @H2SFee smallmoney = NULL
, @LoadFee smallmoney = NULL
) AS BEGIN
	EXEC dbo.spSyncProducer @ID
	EXEC dbo.spSyncActualMiles @ID
	
	DELETE FROM tblOrderInvoiceCarrier WHERE OrderID = @ID
	
	INSERT INTO tblOrderInvoiceCarrier (OrderID, ChainupFee, RerouteFee, WaitFeeSubUnitID, WaitFeeRoundingTypeID, BillableWaitMinutes, WaitRate, WaitFee
		, RejectionFee, RouteRate, LoadFee, H2SRate, H2SFee, FuelSurcharge, TaxRate
		, TotalFee, CreateDate, CreatedByUser)
	SELECT D.ID, ChainupFee, RerouteFee, WaitFeeSubUnitID, WaitFeeRoundingTypeID, BillableWaitMinutes, WaitRate, WaitFee
		, RejectionFee, RouteRate, LoadFee, H2SRate, H2SFee, FuelSurcharge, TaxRate
		, ChainupFee + RerouteFee + WaitFee + H2SFee + LoadFee AS TotalFee, GETDATE(), @UserName
	FROM (
		SELECT ID
			, coalesce(@ChainupFee, Chainup * ChainupFee, 0) AS ChainupFee
			, coalesce(@RerouteFee, RerouteFee * RerouteCount, 0) AS RerouteFee
			, WaitFeeSubUnitID
			, WaitFeeRoundingTypeID
			, cast(BillableWaitHours * 60 as int) AS BillableWaitMinutes
			, WaitFee AS WaitRate
			, coalesce(@WaitFee, BillableWaitHours * WaitFee, 0) AS WaitFee
			, coalesce(@RejectionFee, Rejected * RejectionFee, 0) AS RejectionFee
			, isnull(H2SRate, 0) AS H2SRate
			, coalesce(@H2SFee, H2S * dbo.fnMaxDecimal(MinSettlementBarrels, ActualBarrels) * H2SRate, 0) AS H2SFee
			, isnull(TaxRate, 0) AS TaxRate
			, isnull(Rate, 0) AS RouteRate
			, coalesce(@LoadFee, dbo.fnMaxDecimal(MinSettlementBarrels, ActualBarrels) * Rate, 0) AS LoadFee
			, isnull(FuelSurcharge, 0) AS FuelSurcharge
		FROM (
			SELECT S.ID
				, S.ChainUp
				, CR.ChainupFee
				, CR.RerouteFee
				, S.RerouteCount
				, S.TotalWaitMinutes
				, CR.WaitFeeSubUnitID
				, CR.WaitFeeRoundingTypeID
				, dbo.fnComputeBillableWaitHours(S.TotalWaitMinutes, CR.WaitFeeSubUnitID, CR.WaitFeeRoundingTypeID) AS BillableWaitHours
				, CR.WaitFee
				, S.Rejected
				, CR.RejectionFee
				, S.H2S
				, isnull(S.H2S * CR.H2SRate, 0) AS H2SRate
				, S.TaxRate
				, dbo.fnCarrierRouteRate(S.CarrierID, S.RouteID, S.OrderDate) AS Rate
				, S.MinSettlementBarrels
				, S.ActualBarrels
				, CR.FuelSurcharge
			FROM (
				SELECT O.ID
					, O.CarrierID
					, O.RouteID
					, O.ChainUp
					, CASE WHEN C.SettlementFactorID = 1 THEN O.OriginGrossBarrels ELSE O.OriginNetBarrels END AS ActualBarrels
					, O.RerouteCount
					, O.TotalWaitMinutes
					, O.Rejected
					, OO.H2S
					, dbo.fnDateOnly(O.OriginArriveTime) AS OrderDate
					, coalesce(CRXX.ID, CRXA.ID, CRAX.ID, CRAA.ID) AS CRID
					, isnull(C.MinSettlementBarrels, 0) AS MinSettlementBarrels
					, OO.TaxRate
				FROM dbo.viewOrderExportFull O
				JOIN tblOrigin OO ON OO.ID = O.OriginID
				JOIN tblCarrier C ON C.ID = O.CarrierID
				LEFT JOIN tblCarrierRates CRXX ON CRXX.CarrierID = O.CarrierID AND CRXX.RegionID = OO.RegionID
				LEFT JOIN tblCarrierRates CRXA ON CRXA.CarrierID = O.CarrierID AND CRXA.RegionID = -1
				LEFT JOIN tblCarrierRates CRAX ON CRAX.CarrierID = -1 AND CRAX.RegionID = OO.RegionID
				LEFT JOIN tblCarrierRates CRAA ON CRAA.CarrierID = -1 AND CRAA.RegionID = -1
				WHERE O.ID = @ID
			) S
			LEFT JOIN tblCarrierRates CR ON CR.ID = S.CRID
		) SS
	) D
END

GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Customer "Settlement" $$ values to an Audited order
/***********************************/
ALTER PROCEDURE [dbo].[spProcessCustomerInvoice]
(
  @ID int
, @UserName varchar(100)
, @ChainupFee smallmoney = NULL
, @WaitFee smallmoney = NULL
, @RerouteFee smallmoney = NULL
, @RejectionFee smallmoney = NULL
, @H2SFee smallmoney = NULL
, @LoadFee smallmoney = NULL
) AS BEGIN
	EXEC dbo.spSyncProducer @ID
	EXEC dbo.spSyncActualMiles @ID
	
	DELETE FROM tblOrderInvoiceCustomer WHERE OrderID = @ID
	
	INSERT INTO tblOrderInvoiceCustomer (OrderID, ChainupFee, RerouteFee, WaitFeeSubUnitID, WaitFeeRoundingTypeID, BillableWaitMinutes, WaitRate, WaitFee
		, RejectionFee, RouteRate, LoadFee, H2SRate, H2SFee, FuelSurcharge, TaxRate
		, TotalFee, CreateDate, CreatedByUser)
	SELECT D.ID, ChainupFee, RerouteFee, WaitFeeSubUnitID, WaitFeeRoundingTypeID, BillableWaitMinutes, WaitRate, WaitFee
		, RejectionFee, RouteRate, LoadFee, H2SRate, H2SFee, FuelSurcharge, TaxRate
		, ChainupFee + RerouteFee + WaitFee + H2SFee + LoadFee AS TotalFee, GETDATE(), @UserName
	FROM (
		SELECT ID
			, coalesce(@ChainupFee, Chainup * ChainupFee, 0) AS ChainupFee
			, coalesce(@RerouteFee, RerouteFee * RerouteCount, 0) AS RerouteFee
			, WaitFeeSubUnitID
			, WaitFeeRoundingTypeID
			, cast(BillableWaitHours * 60 as int) AS BillableWaitMinutes
			, WaitFee AS WaitRate
			, coalesce(@WaitFee, BillableWaitHours * WaitFee, 0) AS WaitFee
			, coalesce(@RejectionFee, Rejected * RejectionFee, 0) AS RejectionFee
			, isnull(H2SRate, 0) AS H2SRate
			, coalesce(@H2SFee, H2S * dbo.fnMaxDecimal(MinSettlementBarrels, ActualBarrels) * H2SRate, 0) AS H2SFee
			, isnull(TaxRate, 0) AS TaxRate
			, isnull(Rate, 0) AS RouteRate
			, coalesce(@LoadFee, dbo.fnMaxDecimal(MinSettlementBarrels, ActualBarrels) * Rate, 0) AS LoadFee
			, isnull(FuelSurcharge, 0) AS FuelSurcharge
		FROM (
			SELECT S.ID
				, S.ChainUp
				, CR.ChainupFee
				, CR.RerouteFee
				, S.RerouteCount
				, S.TotalWaitMinutes
				, CR.WaitFeeSubUnitID
				, CR.WaitFeeRoundingTypeID
				, dbo.fnComputeBillableWaitHours(S.TotalWaitMinutes, CR.WaitFeeSubUnitID, CR.WaitFeeRoundingTypeID) AS BillableWaitHours
				, CR.WaitFee
				, S.Rejected
				, CR.RejectionFee
				, S.H2S
				, isnull(S.H2S * CR.H2SRate, 0) AS H2SRate
				, S.TaxRate
				, dbo.fnCustomerRouteRate(S.CustomerID, S.RouteID, S.OrderDate) AS Rate
				, S.MinSettlementBarrels
				, S.ActualBarrels
				, CR.FuelSurcharge
			FROM (
				SELECT O.ID
					, O.CustomerID
					, O.RouteID
					, O.ChainUp
					, CASE WHEN C.SettlementFactorID = 1 THEN O.OriginGrossBarrels ELSE O.OriginNetBarrels END AS ActualBarrels
					, O.RerouteCount
					, O.TotalWaitMinutes
					, O.Rejected
					, OO.H2S
					, dbo.fnDateOnly(O.OriginArriveTime) AS OrderDate
					, coalesce(CRXX.ID, CRXA.ID, CRAX.ID, CRAA.ID) AS CRID
					, isnull(C.MinSettlementBarrels, 0) AS MinSettlementBarrels
					, OO.TaxRate
				FROM dbo.viewOrderExportFull O
				JOIN tblOrigin OO ON OO.ID = O.OriginID
				JOIN tblCustomer C ON C.ID = O.CustomerID
				LEFT JOIN tblCustomerRates CRXX ON CRXX.CustomerID = O.CustomerID AND CRXX.RegionID = OO.RegionID
				LEFT JOIN tblCustomerRates CRXA ON CRXA.CustomerID = O.CustomerID AND CRXA.RegionID = -1
				LEFT JOIN tblCustomerRates CRAX ON CRAX.CustomerID = -1 AND CRAX.RegionID = OO.RegionID
				LEFT JOIN tblCustomerRates CRAA ON CRAA.CustomerID = -1 AND CRAA.RegionID = -1
				WHERE O.ID = @ID
			) S
			LEFT JOIN tblCustomerRates CR ON CR.ID = S.CRID
		) SS
	) D
END

GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Customer "Settlement" $$ values to an Audited order
/***********************************/
CREATE PROCEDURE spCreateCarrierSettlementBatch
(
  @CarrierID int
, @Notes varchar(255)
, @UserName varchar(255)
, @BatchID int = NULL output
, @BatchNum int = NULL output
) AS BEGIN
	INSERT INTO dbo.tblCarrierSettlementBatch(CarrierID, BatchNum, Notes, CreateDate, CreatedByUser)
		SELECT @CarrierID, isnull(max(BatchNum), 0) + 1, @Notes, getdate(), @UserName 
		FROM dbo.tblCarrierSettlementBatch
		
		SELECT @BatchID = scope_identity()
		SELECT @BatchNum = BatchNum FROM dbo.tblCarrierSettlementBatch WHERE ID = @BatchID
END

GO

GRANT EXECUTE ON spCreateCarrierSettlementBatch TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Customer "Settlement" $$ values to an Audited order
/***********************************/
CREATE PROCEDURE spCreateCustomerSettlementBatch
(
  @CustomerID int
, @Notes varchar(255)
, @UserName varchar(255)
, @BatchID int = NULL output
, @BatchNum int = NULL output
) AS BEGIN
	INSERT INTO dbo.tblCustomerSettlementBatch(CustomerID, BatchNum, Notes, CreateDate, CreatedByUser)
		SELECT @CustomerID, isnull(max(BatchNum), 0) + 1, @Notes, getdate(), @UserName 
		FROM dbo.tblCustomerSettlementBatch
		
		SELECT @BatchID = scope_identity()
		SELECT @BatchNum = BatchNum FROM dbo.tblCustomerSettlementBatch WHERE ID = @BatchID
END

GO

GRANT EXECUTE ON spCreateCustomerSettlementBatch TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Customer "Settlement" $$ values to an Audited order
/***********************************/
CREATE PROCEDURE spOrderInvoiceCarrierAddToBatch
(
  @OrderID int
, @BatchID int
) AS BEGIN
	-- this will only update an order that has had rates applied (or the OIC record will not be present)
	UPDATE tblOrderInvoiceCarrier SET BatchID = @BatchID WHERE OrderID = @OrderID
END
GO

GRANT EXECUTE ON spOrderInvoiceCarrierAddToBatch TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Customer "Settlement" $$ values to an Audited order
/***********************************/
CREATE PROCEDURE spOrderInvoiceCustomerAddToBatch
(
  @OrderID int
, @BatchID int
) AS BEGIN
	UPDATE tblOrderInvoiceCustomer SET BatchID = @BatchID WHERE OrderID = @OrderID
END
GO

GRANT EXECUTE ON spOrderInvoiceCustomerAddToBatch TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 2 Feb 2013
-- Author: Kevin Alons
-- Purpose: return OrderReroute records with "computed" fields used in exporting to spreadsheet/etc
/***********************************/
ALTER VIEW [dbo].[viewOrderReroute] AS
SELECT O_R.*, D.FullName AS PreviousDestinationFull, D.Name AS PreviousDestination
FROM dbo.tblOrderReroute O_R
LEFT JOIN dbo.viewDestination D ON D.ID = O_R.PreviousDestinationID

GO

EXEC _spRecompileAllStoredProcedures
GO

COMMIT
SET NOEXEC OFF
GO
--rollback