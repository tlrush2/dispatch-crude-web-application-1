-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.8.18.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.8.18.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0

DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.8.18'
SELECT  @NewVersion = '3.8.19'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-845: Added Order Create Date to report center.'
	UNION
	SELECT @NewVersion, 0, 'Modified C.O.D.E. export again per customer.'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/***********************************/
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return the Local Arrive|Depart Times + their respective TimeZone abbreviations
--		8/26/15 - BB - Added Order Create Date
/***********************************/
ALTER VIEW [dbo].[viewOrderLocalDates] AS
SELECT O.*
	, dbo.fnUTC_to_Local(O.CreateDateUTC, O.OriginTimeZoneID, O.OriginUseDST) AS OrderCreateDate
	, dbo.fnUTC_to_Local(O.OriginArriveTimeUTC, O.OriginTimeZoneID, O.OriginUseDST) AS OriginArriveTime
	, dbo.fnUTC_to_Local(O.OriginDepartTimeUTC, O.OriginTimeZoneID, O.OriginUseDST) AS OriginDepartTime
	, dbo.fnUTC_to_Local(O.DestArriveTimeUTC, O.DestTimeZoneID, O.DestUseDST) AS DestArriveTime
	, dbo.fnUTC_to_Local(O.DestDepartTimeUTC, O.DestTimeZoneID, O.DestUseDST) AS DestDepartTime
	, dbo.fnTimeZoneAbbrev(O.OriginArriveTimeUTC, O.OriginTimeZoneID, O.OriginUseDST) AS OriginTimeZone
	, dbo.fnTimeZoneAbbrev(O.DestArriveTimeUTC, O.DestTimeZoneID, O.DestUseDST) AS DestTimeZone
	, DATEDIFF(minute, O.OriginDepartTimeUTC, O.DestArriveTimeUTC) AS TransitMinutes
	FROM viewOrder O
GO

exec sp_refreshview viewOrder_OrderTicket_Full
GO

exec sp_refreshview viewReportCenter_Orders
GO

/*
	BB 8/26/15 - DCWEB-845: Added order create date to report center.
*/
SET IDENTITY_INSERT tblReportColumnDefinition ON
  INSERT tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport)
  SELECT 275,1,'OrderCreateDate','TRIP | TIMESTAMPS | Order Create Date',NULL,NULL,0,NULL,1,'*',0    
  EXCEPT SELECT ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport FROM tblReportColumnDefinition
SET IDENTITY_INSERT tblReportColumnDefinition OFF
GO

/*****************************************************************************************/
-- Author: Kevin Alons
-- Date Created: 27 Feb 2014
-- Purpose: with the provided parameters, export the data and mark done (if doing finalExport)
--		8/12/15 - Ben B. - Expanded procedure to include C.O.D.E. export options
--		8/21/15 - Ben B. - Added summary row to CODE export
--		8/26/15 - Ben B. - Added summary row to CODE summary row row count per customer request
/*****************************************************************************************/
ALTER PROCEDURE [dbo].[spOrderExportFinalCustomer]
( 
  @customerID int 
, @exportedByUser varchar(100) = NULL -- will default to SUSER_NAME() -- default value for now
, @exportFormat varchar(100) -- Options are: 'SUNOCO_SUNDEX' or 'CODE_STANDARD'
, @finalExport bit = 1 -- defualt to TRUE
) 
AS BEGIN
	SET NOCOUNT ON
	-- default to the current user if not supplied
	IF @exportedByUser IS NULL SET @exportedByUser = SUSER_NAME()
	
	-- Create variable for error catching with CODE export
	DECLARE @CODEErrorFlag BIT = 0 -- Default to false
	
	-- retrieve the order records to export
	DECLARE @orderIDs IDTABLE
	INSERT INTO @orderIDs (ID)
	SELECT ID
	FROM dbo.viewOrderCustomerFinalExportPending 
	WHERE CustomerID = @customerID 

	BEGIN TRAN exportCustomer
	
	-- export the orders in the specified format
	IF (@exportFormat = 'SUNOCO_SUNDEX') BEGIN
		SELECT E.* 
		INTO #output
		FROM dbo.viewSunocoSundex E
		JOIN @orderIDs IDs ON IDs.ID = E._ID
		WHERE E._CustomerID = @customerID
		ORDER BY _OrderNum
	END
	ELSE
	IF (@exportFormat = 'CODE_STANDARD') BEGIN
		DECLARE @CodeDXCode varchar(2) = (SELECT CodeDXCode FROM tblCustomer WHERE ID = @customerID)
		IF LEN(@CodeDXCode) = 2 BEGIN
			SELECT E.ExportText
			INTO #output2
			FROM dbo.viewOrderExport_CODE E
				JOIN @orderIDs IDs ON IDs.ID = E.OrderID
			WHERE E.CompanyID = @CodeDXCode
		END
		ELSE SET @CODEErrorFlag = 1	
	END

	-- mark the orders as exported FINAL (for a Customer)
	IF (@finalExport = 1) BEGIN
		EXEC spMarkOrderExportFinalCustomer @orderIDs, @exportedByUser
	END
	
	COMMIT TRAN exportCustomer

	-- return the data to 
	IF (@exportFormat = 'SUNOCO_SUNDEX') BEGIN
		SELECT * FROM #output
	END
	
	IF (@exportFormat = 'CODE_STANDARD') BEGIN
		IF @CODEErrorFlag = 0 BEGIN
			SELECT * FROM #output2
			--Include Summary Row
			UNION
			SELECT
				'4' -- Summary Record Type RecordID
				+ X.Company_ID 
				+ X.Record_Count 
				+ X.Pos_Neg_Code 
				+ X.Total_Net
				+ X.Shipper_Net
				+ dbo.fnFixedLenStr('',89)
				+ dbo.fnFixedLenStr('',3)
			FROM
				(SELECT
					ECB.Company_ID
					,CAST(dbo.fnFixedLenNum((COUNT(*) + 1), 10, 0, 1) AS VARCHAR)	AS Record_Count
					,ECB.Pos_Neg_Code
					,CAST(dbo.fnFixedLenNum(SUM(CAST(ECB.Total_Net_Volume AS DECIMAL)), 13, 0, 1) AS VARCHAR) Total_Net
					,CAST(dbo.fnFixedLenNum(SUM(CAST(ECB.Shippers_Net_Volume AS DECIMAL)), 13, 0, 1) AS VARCHAR) Shipper_Net
				FROM dbo.viewOrderExport_CODE_Base ECB			
					JOIN @orderIDs IDs ON IDs.ID = ECB.OrderID			
				WHERE ECB.Company_ID = @CodeDXCode
				GROUP BY ECB.Company_ID,ECB.Pos_Neg_Code) X
		END
		ELSE SELECT 'Invalid Shipper CODE ID specified.  Fix in Shipper maintenance table.'
	END		
END
GO

EXEC _spRebuildAllObjects
GO

COMMIT
SET NOEXEC OFF