-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.0.2'
SELECT  @NewVersion = '4.0.3'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1631: Add H2S Expiration field to Driver Compliance'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/*  DCWEB-1631: add H2S Expiration field in Compliance > Drivers > Licenses */
ALTER TABLE tblDriver
	ADD H2SExpiration DATE NULL
GO



/* Ensure all views, stored procedures, and functions are refreshed to add the new field */
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRebuildAllObjects
GO
EXEC _spRecompileAllStoredProcedures
GO



COMMIT
SET NOEXEC OFF