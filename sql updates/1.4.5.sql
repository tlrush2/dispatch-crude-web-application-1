DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '1.4.4', @NewVersion = '1.4.5'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

/**********************************/
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Route records with translated Origin/Destination values
/***********************************/
ALTER VIEW [dbo].[viewRoute] AS
SELECT R.*
, O.Name AS Origin, O.FullName AS OriginFull
, D.Name AS Destination, D.FullName AS DestinationFull
, cast(CASE WHEN O.Active=1 AND D.DeleteDate IS NULL THEN 1 ELSE 0 END as bit) AS Active
FROM dbo.tblRoute R
JOIN dbo.viewOrigin O ON O.ID = R.OriginID
JOIN dbo.viewDestination D ON D.ID = R.DestinationID

GO

/**********************************/
-- Date Created: 7 July 2013
-- Author: Kevin Alons
-- Purpose: return Route records with translated Origin/Destination values (with "missing" routes)
/***********************************/
CREATE VIEW [dbo].[viewRouteWithMissing] AS
SELECT R.ID, O.ID AS OriginID, D.ID AS DestinationID, R.ActualMiles
	, R.CreateDate, R.CreatedByUser, R.LastChangeDate, R.LastChangedByUser
	, O.Name AS Origin, O.FullName AS OriginFull
	, D.Name AS Destination, D.FullName AS DestinationFull
	, cast(CASE WHEN R.ID IS NOT NULL AND O.Active=1 AND D.DeleteDate IS NULL THEN 1 ELSE 0 END as bit) AS Active
	, CASE WHEN R.ID IS NULL THEN 1 ELSE 0 END AS Missing
FROM (viewOrigin O CROSS JOIN viewDestination D)
LEFT JOIN dbo.tblRoute R ON R.OriginID = O.ID AND R.DestinationID = D.ID

GO

GRANT SELECT ON viewRouteWithMissing TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: return Carrier route rates with computed Earliest & End Date values)
/***********************************/
ALTER VIEW [dbo].[viewCarrierRouteRatesBase] AS
SELECT CRR.*
	, R.OriginID, R.DestinationID
	, (SELECT MIN(dateadd(day, -1, EffectiveDate)) FROM tblCarrierRouteRates CRRN WHERE CRRN.CarrierID = CRR.CarrierID AND CRRN.RouteID = CRR.RouteID AND CRRN.EffectiveDate > CRR.EffectiveDate) AS EndDate
	, (SELECT MAX(dateadd(day, 1, EffectiveDate)) FROM tblCarrierRouteRates CRRP WHERE CRRP.CarrierID = CRR.CarrierID AND CRRP.RouteID = CRR.RouteID AND CRRP.EffectiveDate < CRR.EffectiveDate) AS EarliestEffectiveDate
	, R.ActualMiles
	, R.Active
FROM tblCarrierRouteRates CRR
JOIN viewRoute R ON R.ID = CRR.RouteID

GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: return Customer route rates with computed Earliest & End Date values)
/***********************************/
ALTER VIEW [dbo].[viewCustomerRouteRatesBase] AS
SELECT CRR.*
	, R.OriginID, R.DestinationID
	, (SELECT MIN(dateadd(day, -1, EffectiveDate)) FROM tblCustomerRouteRates CRRN WHERE CRRN.CustomerID = CRR.CustomerID AND CRRN.RouteID = CRR.RouteID AND CRRN.EffectiveDate > CRR.EffectiveDate) AS EndDate
	, (SELECT MAX(dateadd(day, 1, EffectiveDate)) FROM tblCustomerRouteRates CRRP WHERE CRRP.CustomerID = CRR.CustomerID AND CRRP.RouteID = CRR.RouteID AND CRRP.EffectiveDate < CRR.EffectiveDate) AS EarliestEffectiveDate
	, R.ActualMiles
	, R.Active
FROM tblCustomerRouteRates CRR
JOIN viewRoute R ON R.ID = CRR.RouteID

GO

/***********************************/
-- Date Created: 9 Jun 2013
-- Author: Kevin Alons
-- Purpose: return Carrier route rates with translated "friendly" values + "missing route rates", computed Earliest & End Date values)
/***********************************/
ALTER VIEW [dbo].[viewCarrierRouteRates] AS
SELECT RB.ID, R.ID AS RouteID, isnull(RB.Rate, 0) AS Rate, isnull(RB.EffectiveDate, dbo.fnDateOnly(GETDATE())) AS EffectiveDate
	, cast(CASE WHEN C.DeleteDate IS NOT NULL OR RB.Active = 0 THEN 0 ELSE 1 END as bit) AS Active
	, isnull(ORD.OpenOrderCount, 0) as OpenOrderCount
	, isnull(R.ActualMiles, 0) AS ActualMiles
	, RB.CreateDate, RB.CreatedByUser, RB.LastChangeDate, RB.LastChangedByUser
	, RB.EndDate
	, RB.EarliestEffectiveDate
	, C.ID AS CarrierID, C.Name AS Carrier
	, R.OriginID, R.Origin, R.OriginFull
	, R.DestinationID , R.Destination, R.DestinationFull
	, CASE WHEN R.ID IS NULL THEN 'Route Missing' WHEN RB.ID IS NULL THEN 'Missing' WHEN C.DeleteDate IS NOT NULL THEN 'Carrier Deleted' WHEN RB.Active = 0 THEN 'Route Inactive' ELSE 'Active' END AS Status
	, cast(CASE WHEN R.ID IS NULL OR isnull(ORD.OpenOrderCount, 0) = 0 THEN 0 ELSE 1 END as bit) AS RouteInUse
FROM (viewCarrier C CROSS JOIN viewRouteWithMissing R)
LEFT JOIN viewCarrierRouteRatesBase RB ON RB.CarrierID = C.ID AND RB.OriginID = R.OriginID AND RB.DestinationID = R.DestinationID
LEFT JOIN (SELECT CarrierID, RouteID, count(1) AS OpenOrderCount FROM tblOrder WHERE StatusID IN (4) GROUP BY CarrierID, RouteID) ORD ON ORD.CarrierID = C.ID AND ORD.RouteID = R.ID

GO

/***********************************/
-- Date Created: 9 Jun 2013
-- Author: Kevin Alons
-- Purpose: return Carrier route rates with translated "friendly" values + "missing route rates", computed Earliest & End Date values)
/***********************************/
ALTER VIEW [dbo].[viewCustomerRouteRates] AS
SELECT RB.ID, R.ID AS RouteID, isnull(RB.Rate, 0) AS Rate, isnull(RB.EffectiveDate, dbo.fnDateOnly(GETDATE())) AS EffectiveDate
	, RB.Active
	, isnull(ORD.OpenOrderCount, 0) as OpenOrderCount
	, isnull(R.ActualMiles, 0) AS ActualMiles
	, RB.CreateDate, RB.CreatedByUser, RB.LastChangeDate, RB.LastChangedByUser
	, RB.EndDate
	, RB.EarliestEffectiveDate
	, C.ID AS CustomerID, C.Name AS Customer
	, R.OriginID, R.Origin, R.OriginFull
	, R.DestinationID , R.Destination, R.DestinationFull
	, CASE WHEN R.ID IS NULL THEN 'Route Missing' WHEN RB.ID IS NULL THEN 'Missing' WHEN RB.Active = 0 THEN 'Route Inactive' ELSE 'Active' END AS Status
	, cast(CASE WHEN R.ID IS NULL OR isnull(ORD.OpenOrderCount, 0) = 0 THEN 0 ELSE 1 END as bit) AS RouteInUse
FROM (viewCustomer C CROSS JOIN viewRouteWithMissing R)
LEFT JOIN viewCustomerRouteRatesBase RB ON RB.CustomerID = C.ID AND RB.OriginID = R.OriginID AND RB.DestinationID = R.DestinationID
LEFT JOIN (SELECT CustomerID, RouteID, count(1) AS OpenOrderCount FROM tblOrder WHERE StatusID IN (4) GROUP BY CustomerID, RouteID) ORD ON ORD.CustomerID = C.ID AND ORD.RouteID = R.ID

GO

ALTER TABLE tblRoute DROP COLUMN CarrierRateMiles
GO
ALTER TABLE tblRoute DROP COLUMN CustomerRateMiles
GO
DROP INDEX idxOrder_DeleteDate_StatusID_Covering ON tblOrder
GO
ALTER TABLE tblOrder DROP COLUMN CarrierRateMiles
GO
ALTER TABLE tblOrder DROP COLUMN CustomerRateMiles
GO
/****** Object:  Index [idxOrder_DeleteDate_StatusID_Covering]    Script Date: 07/09/2013 22:32:39 ******/
CREATE NONCLUSTERED INDEX [idxOrder_DeleteDate_StatusID_Covering] ON [dbo].[tblOrder] 
(
	[DeleteDate] ASC,
	[StatusID] ASC
)
INCLUDE ( [ID],
[OrderNum],
[PriorityID],
[DueDate],
[RouteID],
[OriginID],
[OriginArriveTime],
[OriginDepartTime],
[OriginMinutes],
[OriginWaitNotes],
[OriginBOLNum],
[OriginGrossBarrels],
[OriginNetBarrels],
[DestinationID],
[DestArriveTime],
[DestDepartTime],
[DestMinutes],
[DestWaitNotes],
[DestBOLNum],
[DestGrossBarrels],
[DestNetBarrels],
[CustomerID],
[CarrierID],
[DriverID],
[TruckID],
[TrailerID],
[Trailer2ID],
[OperatorID],
[PumperID],
[TicketTypeID],
[Rejected],
[RejectNotes],
[ChainUp],
[OriginTruckMileage],
[OriginTankNum],
[DestTruckMileage],
[CarrierTicketNum],
[AuditNotes],
[CreateDate],
[ActualMiles],
[ProducerID],
[CreatedByUser],
[LastChangeDate],
[LastChangedByUser],
[DeletedByUser],
[DestProductBSW],
[DestProductGravity],
[DestProductTemp]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

DROP PROCEDURE spSyncOrderCarrierMiles
GO
DROP PROCEDURE spSyncOrderCustomerMiles
GO

exec _spRefreshAllViews
GO
exec _spRecompileAllStoredProcedures
GO

COMMIT TRANSACTION
SET NOEXEC OFF