-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.3.0'
SELECT  @NewVersion = '3.3.1'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'Allow Sorting of output data in Report Center'
	UNION
	SELECT @NewVersion, 0, 'ALTER TABLE tblUserReportColumnDefinition ADD DataSort smallint'
	UNION
	SELECT @NewVersion, 0, 'ALTER spGetOrderChanges to return translated OrderStatus (instead of StatusID int value)'
GO

ALTER TABLE tblUserReportColumnDefinition ADD DataSort smallint null
GO

/******************************************
-- Date Created: 14 NOv 2014
-- Author: Kevin Alons
-- Purpose: return ReportColumnDefinition records with joined "friendly" data
******************************************/
ALTER VIEW [dbo].[viewReportColumnDefinition] AS
	SELECT RCD.ID
		, RCD.ReportID
		, RCD.DataField
		, Caption = coalesce(RCD.Caption, RCD.DataField)
		, RCD.DataFormat
		, RCD.FilterDataField
		, RCD.FilterTypeID
		, RCD.FilterDropDownSql
		, RCD.FilterAllowCustomText
		, RCD.AllowedRoles
		, RCD.OrderSingleExport
		, RFT.FilterOperatorID_CSV
		, BaseFilterCount = (SELECT COUNT(*) FROM tblReportColumnDefinitionBaseFilter WHERE ReportColumnID = RCD.ID)
		, FilterOperatorID_CSV_Delim = ',' + RFT.FilterOperatorID_CSV + ','
	FROM tblReportColumnDefinition RCD
	JOIN tblReportFilterType RFT ON RFT.ID = RCD.FilterTypeID;

GO

/***********************************************/
-- Date Created: 27 Jul 2014
-- Author: Kevin Alons
-- Purpose: add related JOINed fields to the tblUserReportColumnDefinition table results
/***********************************************/
ALTER VIEW [dbo].[viewUserReportColumnDefinition] AS
	SELECT URCD.ID
		, URCD.UserReportID
		, URCD.ReportColumnID
		, URCD.Caption
		, DataFormat = isnull(URCD.DataFormat, RCD.DataFormat)
		, URCD.SortNum
		, URCD.FilterOperatorID
		, URCD.FilterValue1
		, URCD.FilterValue2
		, URCD.Export
		, URCD.DataSort
		, DataSortABS = abs(URCD.DataSort)
		, URCD.CreateDateUTC, URCD.CreatedByUser
		, URCD.LastChangeDateUTC, URCD.LastChangedByUser
		, RCD.DataField
		, BaseCaption = dbo.fnCaptionRoot(RCD.Caption)
		, OutputCaption = coalesce(URCD.Caption, dbo.fnCaptionRoot(RCD.Caption), RCD.DataField)
		, RCD.FilterTypeID
		, FilterDataField = isnull(RCD.FilterDataField, RCD.DataField)
		, RCD.FilterDropDownSql
		, RCD.FilterAllowCustomText
		, RCD.AllowedRoles
		, RCD.OrderSingleExport
		, FilterOperator = RFO.Name
		, FilterValues = CASE 
				WHEN RFO.ID IN (1,4,5) OR RCD.FilterAllowCustomText = 1 THEN FilterValue1 + ISNULL(' - ' + FilterValue2, '')
				ELSE 
					CASE WHEN FilterValue1 IS NOT NULL THEN '&lt;filtered&gt;' 
						 ELSE NULL 
					END 
			END
	FROM tblUserReportColumnDefinition URCD
	JOIN viewReportColumnDefinition RCD ON RCD.ID = URCD.ReportColumnID
	LEFT JOIN tblReportFilterOperator RFO ON RFO.ID = URCD.FilterOperatorID;

GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 11 Sep 2014
-- Description:	compute and return the order changes for a single order (specified by an Order.ID value)
-- =============================================
ALTER PROCEDURE [dbo].[spGetOrderChanges](@id int) 
AS BEGIN
	DECLARE @ret TABLE 
	(
		FieldName varchar(255)
	  , NewValue varchar(max)
	  , OldValue varchar(max)
	  , ChangeDateUTC varchar(100)
	  , ChangedByUser varchar(100)
	  , IsCurrent bit
	)

	DECLARE @columns TABLE (id int, name varchar(100))
	INSERT INTO @columns 		
		SELECT ROW_NUMBER() OVER (ORDER BY COLUMN_NAME), COLUMN_NAME 
		FROM INFORMATION_SCHEMA.COLUMNS 
		WHERE TABLE_NAME = 'tblOrder' AND COLUMN_NAME NOT IN ('ID','LastChangeDateUTC')
	
	SELECT rowID = ROW_NUMBER() OVER (ORDER BY sortnum, dbauditdate desc, RouteID desc), * 
	INTO #history
	FROM (
		SELECT dbauditdate = NULL, *, sortnum = 0 from tblOrder where ID = @id
		UNION ALL SELECT *, sortnum = 1 from tblOrderDbAudit where ID = @id
	) X

	DECLARE @rowID int, @colID int, @colName varchar(100), @newValue varchar(max), @oldValue varchar(max), @sql nvarchar(max), @newColID int
	DECLARE @changeDateUTC datetime, @changeUser varchar(100), @isCurrent bit

	SELECT @rowID = MIN(rowID) FROM #history
	WHILE EXISTS(SELECT * FROM #history WHERE rowID = @rowID + 1) BEGIN
		SELECT TOP 1 @colName = name, @colID = id FROM @columns WHERE id = 1
		WHILE (@colName IS NOT NULL) BEGIN

			-- get the "now" value
			SET @sql = N'SELECT TOP 1 @value=' + @colName + N', @changeDateUTC=LastChangeDateUTC, @changeUser=LastChangedByUser FROM #history WHERE rowID = @rowID'
			EXEC sp_executesql @sql
				, N'@rowID int, @changeDateUTC datetime OUTPUT, @changeUser varchar(100) OUTPUT, @value varchar(max) OUTPUT'
				, @rowID = @rowID, @changeDateUTC = @changeDateUTC OUTPUT, @changeUser = @changeUser OUTPUT, @value = @newValue OUTPUT

			-- get the "from" value
			SET @sql = N'SELECT TOP 1 @value=' + @colName + N' FROM #history WHERE rowID = @rowID + 1'
			EXEC sp_executesql @sql, N'@rowID int, @value varchar(max) OUTPUT', @rowID = @rowID, @value = @oldValue OUTPUT
			
			IF (isnull(@newValue, '\r') <> isnull(@oldValue, '\r')) BEGIN
				INSERT INTO @ret 
					SELECT @colName, @newValue, @oldValue, @changeDateUTC, @changeUser, CASE WHEN @rowID = 1 THEN 1 ELSE 0 END
			END
			
			SET @colName = NULL
			SELECT TOP 1 @colName = name, @colID = id FROM @columns WHERE id > @colID ORDER BY ID
			
		END

		SET @rowID = @rowID + 1
	END
	
	UPDATE @ret
	  SET NewValue = OSNV.OrderStatus, OldValue = OSOV.OrderStatus
	FROM @ret R 
	LEFT JOIN tblOrderStatus OSNV ON OSNV.ID = R.NewValue
	LEFT JOIN tblOrderStatus OSOV ON OSOV.ID = R.OldValue
	WHERE R.FieldName LIKE 'StatusID'
	
	SELECT * FROM @ret

END

GO

COMMIT
SET NOEXEC OFF