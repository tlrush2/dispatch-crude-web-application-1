SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.5.19.3'
SELECT  @NewVersion = '4.5.20'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'JT-2848 - Add new fields to shipper page'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


-- Add new columns to Shipper table
ALTER TABLE tblCustomer 
	ADD NetTermDays INT NULL
GO
ALTER TABLE tblCustomer
	ADD SingleInvoicePerOrder BIT NULL
GO


-- Set default FALSE Single Invoice Per Order value for all shippers
UPDATE tblCustomer
	SET SingleInvoicePerOrder = 0
GO


-- cleanup phone number fields (leave only numbers) so that the new display code can properly format them
UPDATE tblCustomer
	SET ContactPhone = REPLACE(REPLACE(REPLACE(REPLACE(ContactPhone,'(',''),')',''),'-',''),' ','')
	, HelpDeskPhone = REPLACE(REPLACE(REPLACE(REPLACE(HelpDeskPhone,'(',''),')',''),'-',''),' ','')
GO


EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO


COMMIT
SET NOEXEC OFF