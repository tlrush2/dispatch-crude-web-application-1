-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.5.11'
SELECT  @NewVersion = '3.6.0'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Add tblAllocationDestinationShipper Table'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

CREATE TABLE tblAllocationDestinationShipper
(
  ID int identity(1, 1) NOT NULL CONSTRAINT PK_AllocationDestinationShipper PRIMARY KEY NONCLUSTERED
, DestinationID int NOT NULL CONSTRAINT FK_AllocationDestinationShipper_Destination FOREIGN KEY REFERENCES tblDestination(ID)
, ShipperID int NOT NULL CONSTRAINT FK_AllocationDestinationShipper_Shipper FOREIGN KEY REFERENCES tblCustomer(ID)
, ProductGroupID int NOT NULL CONSTRAINT FK_AllocationDestinationShipper_ProductGroup FOREIGN KEY REFERENCES tblProductGroup(ID)
, UomID int NOT NULL CONSTRAINT FK_AllocationDestinationShipper_Uom FOREIGN KEY REFERENCES tblUom(ID)
, EffectiveDate date NOT NULL 
, EndDate date NOT NULL
, Units int NOT NULL
, Notes Text NULL
, CreateDateUTC datetime NOT NULL CONSTRAINT DF_AllocationDestinationShipper_CreateDateUTC DEFAULT (getutcdate())
, CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_AllocationDestinationShipper_CreatedByUser DEFAULT (suser_name())
, LastChangeDateUTC datetime NULL
, LastChangedByUser varchar(100) NULL
, DeleteDateDateUTC datetime NULL
, DeletedByUser varchar(100) NULL
, CONSTRAINT CK_AllocationDestinationShipper_EndAfter CHECK (EffectiveDate <= EndDate)
)
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblAllocationDestinationShipper TO dispatchcrude_iis_acct
GO
CREATE CLUSTERED INDEX idxcAllocationDestinationShipper ON tblAllocationDestinationShipper(DestinationID, ShipperID, ProductGroupID, EffectiveDate)
GO

/*
/***********************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
/***********************************************/
CREATE TRIGGER trigAllocationDestinationShipper_IU ON tblAllocationDestinationShipper AFTER INSERT, UPDATE AS
BEGIN
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblAllocationDestinationShipper X 
			ON i.ID <> X.ID
			  AND i.DestinationID = X.DestinationID
			  AND i.ShipperID = X.ShipperID
			  AND i.ProductGroupID = X.ProductGroupID
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		RAISERROR('Overlapping Allocations are not allowed', 16, 1)
		-- if an explicit TRANSACTION isn't present, then ROLLBACK to ensure these changes cannot be committed
		IF (@@TRANCOUNT = 0) ROLLBACK
	END
END
*/
GO

/****************************************************
-- Date Created: 25 Jan 2015
-- Author: Kevin Alons
-- Purpose: calculate DailyUnits value
****************************************************/
CREATE VIEW [dbo].[viewAllocationDestinationShipper] AS
	SELECT ADS.*
		, DailyUnits = Units / cast(DATEDIFF(day, EffectiveDate, EndDate) as decimal(18, 10))
		, Destination = D.FullName
		, Shipper = C.Name
		, ProductGroup = PG.Name
		, Uom = U.Name
	FROM tblAllocationDestinationShipper ADS
	JOIN viewDestination D ON D.ID = ADS.DestinationID
	JOIN tblCustomer C ON C.ID = ADS.ShipperID
	JOIN tblProductGroup PG ON PG.ID = ADS.ProductGroupID
	JOIN tblUom U ON U.ID = ADS.UomID

GO

/********************************************
-- Date Created: 25 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve all matching AllocationDestinationShipper records for the specified criteria
********************************************/
CREATE FUNCTION fnRetrieveAllocationDestinationShipper
(
  @DestinationID int = -1
, @ShipperID int = -1
, @ProductGroupID int = -1
, @EffectiveDate date
, @EndDate date
) RETURNS TABLE AS RETURN
	SELECT *
	FROM viewAllocationDestinationShipper
	WHERE @DestinationID IN (-1, DestinationID) 
	  AND @ShipperID IN (-1, ShipperID) 
	  AND @ProductGroupID IN (-1, ProductGroupID) 
	  AND (@EffectiveDate BETWEEN EffectiveDate AND EndDate
		OR @EndDate BETWEEN EffectiveDate AND EndDate
		OR EffectiveDate BETWEEN @EffectiveDate AND @EndDate)

GO
GRANT SELECT ON fnRetrieveAllocationDestinationShipper TO dispatchcrude_iis_acct
GO

/********************************************
-- Date Created: 25 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve the Allocation + Production Units for the specified selection criteria + Date Range
--			done as a function (vs a PROCEDURE) to allow the results to be easily Grouped as needed
********************************************/
CREATE FUNCTION fnRetrieveAllocationDestinationShipper_ForRange
( 
  @DestinationID int = -1
, @ShipperID int = -1
, @ProductGroupID int = -1
, @EffectiveDate date
, @EndDate date
, @UomID int
, @OnlyForAllocated bit = 0
, @IncludeScheduled bit = 1
) RETURNS @ret TABLE 
(
  DestinationID int
, Destination varchar(100)
, ShipperID int
, Shipper varchar(100)
, ProductGroupID int
, ProductGroup varchar(100)
, UomID int
, Uom varchar(100)
, AllocatedUnits money
, ProductionUnits money
) AS BEGIN
	DECLARE @ADS TABLE (DID int, SID int, PGID int)
	INSERT INTO @ADS
		SELECT DISTINCT DestinationID, ShipperID, ProductGroupID 
		FROM tblAllocationDestinationShipper ADS
		WHERE (@EffectiveDate BETWEEN EffectiveDate AND EndDate
		   OR @EndDate BETWEEN EffectiveDate AND EndDate
		   OR EffectiveDate BETWEEN @EffectiveDate AND @EndDate)
		  AND (@DestinationID = -1 OR DestinationID = @DestinationID)
		  AND (@ShipperID = -1 OR ShipperID = @ShipperID)
		  AND (@ProductGroupID = -1 OR ProductGroupID = @ProductGroupID)

	-- insert the ProductionUnits first (since these will definitely exist, the allocations might not)
	INSERT INTO @ret (DestinationID, ShipperID, ProductGroupID, UomID, AllocatedUnits, ProductionUnits)
		SELECT O.DestinationID, O.CustomerID, ProductGroupID, @UomID, 0, SUM(dbo.fnConvertUOM(isnull(OriginNetUnits, 0), OriginUomID, @UomID))
		FROM viewOrderBase O
		JOIN tblDestinationCustomers DC ON DC.DestinationID = O.DestinationID AND DC.CustomerID = O.CustomerID
		JOIN tblProduct P ON P.ID = O.ProductID
		LEFT JOIN @ADS ADS ON ADS.DID = O.DestinationID AND ADS.SID = O.CustomerID AND ADS.PGID = P.ProductGroupID
		WHERE isnull(OrderDate, DueDate) BETWEEN @EffectiveDate AND @EndDate
		  AND (@IncludeScheduled = 1 OR OrderDate IS NOT NULL)
		  AND Rejected = 0 AND O.DeleteDateUTC IS NULL
		  AND @DestinationID IN (-1, O.DestinationID)
		  AND @ShipperID IN (-1, O.CustomerID)
		  AND @ProductGroupID IN (-1, ProductGroupID)
		  AND (@OnlyForAllocated = 0 OR ADS.DID IS NOT NULL)
		GROUP BY O.DestinationID, O.CustomerID, ProductGroupID

	-- ensure a record exists for all Allocation records
	INSERT INTO @ret (DestinationID, ShipperID, ProductGroupID, UomID, AllocatedUnits, ProductionUnits)
		SELECT ADS.DID, ADS.SID, ADS.PGID, @UomID, 0, 0
		FROM @ADS ADS
		LEFT JOIN @ret X ON X.DestinationID = ADS.DID AND X.ShipperID = ADS.SID AND X.ProductGroupID = ADS.PGID
		WHERE X.DestinationID IS NULL
	
	UPDATE @ret
		SET Destination = D.Name
			, Shipper = C.Name
			, ProductGroup = PG.Name
			, Uom = U.Name
	FROM @ret X
	JOIN tblDestination D ON D.ID = X.DestinationID
	JOIN tblCustomer C ON C.ID = X.ShipperID
	JOIN tblProductGroup PG ON PG.ID = X.ProductGroupID
	JOIN tblUom U ON U.ID = X.UomID
	
	-- compute the actual AllocationUnits based on DailyAllocation values
	DECLARE @WorkDate date
	SET @WorkDate = @EffectiveDate
	WHILE @WorkDate <= @EndDate
	BEGIN
		UPDATE @ret SET AllocatedUnits = AllocatedUnits + ADS.Units
		FROM @ret R
		JOIN (
			SELECT DestinationID, ShipperID, ProductGroupID
				, Units = sum(dbo.fnConvertUOM(DailyUnits, UomID, @UomID))
			FROM viewAllocationDestinationShipper
			WHERE @WorkDate BETWEEN EffectiveDate AND EndDate
			GROUP BY DestinationID, ShipperID, ProductGroupID
		) ADS ON ADS.DestinationID = R.DestinationID
				AND ADS.ShipperID = R.ShipperID
				AND ADS.ProductGroupID = R.ProductGroupID
		SET @WorkDate = DATEADD(day, 1, @WorkDate)
	END
	
	RETURN
END
GO
GRANT SELECT ON fnRetrieveAllocationDestinationShipper_ForRange TO dispatchcrude_iis_acct
GO

COMMIT
SET NOEXEC OFF

/*
insert into tblAllocationDestinationShipper (DestinationID, ShipperID, ProductGroupID, UomID, EffectiveDate, EndDate, Units)
	select 11, 12, 1, 1, '1/1/2015', '1/31/2015', 50000
	union select 11, 12, 1, 1, '2/1/2015', '2/28/2015', 50000
	union select 11, 12, 1, 1, '1/1/2014', '1/31/2014', 50000
	union select 11, 12, 1, 1, '2/1/2014', '2/28/2014', 50000
	union select 11, 12, 1, 1, '3/1/2014', '3/31/2014', 50000
	union select 11, 12, 1, 1, '4/1/2014', '4/30/2014', 50000
	union select 11, 12, 1, 1, '5/1/2014', '5/31/2014', 50000
	union select 11, 12, 1, 1, '6/1/2014', '6/30/2014', 50000
	union select 11, 12, 1, 1, '7/1/2014', '7/31/2014', 50000
	union select 11, 12, 1, 1, '8/1/2014', '8/31/2014', 50000
	union select 11, 12, 1, 1, '9/1/2014', '9/30/2014', 50000
	union select 11, 12, 1, 1, '10/1/2014', '10/31/2014', 50000
	union select 11, 12, 1, 1, '11/1/2014', '11/30/2014', 50000
	union select 11, 12, 1, 1, '12/1/2014', '12/31/2014', 50000

select * from viewAllocationDestinationShipper
select * from dbo.fnRetrieveAllocationDestinationShipper_ForRange(-1, 12, 1, '1/1/2014', '4/30/2014', 1, 0, 1)

*/