SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.1.6'
SELECT  @NewVersion = '4.1.7'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1552 Enfoce driver availability'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

-- INSERT new carrier rules
INSERT INTO tblCarrierRuleType
(ID, Name, RuleTypeID, ForDriverApp, Description)
SELECT 13, 'Enforce Driver Scheduling', 2, 0, 'Flag to check driver schedule when dispatching orders'
EXCEPT SELECT ID, Name, RuleTypeID, ForDriverApp, Description FROM tblCarrierRuleType
GO

INSERT INTO tblCarrierRuleType
(ID, Name, RuleTypeID, ForDriverApp, Description)
SELECT 14, 'Enforce Driver Compliance', 2, 0, 'Flag to check driver compliance when dispatching orders'
EXCEPT SELECT ID, Name, RuleTypeID, ForDriverApp, Description FROM tblCarrierRuleType
GO


EXEC _spDropFunction 'fnRetrieveEligibleDrivers_NoGPS' -- this helper function just seems easier to use than the manual syntax...
GO
/****************************************************/
-- Created: 2016.09.14 - x.x.x - Joe Engler
-- Purpose: Retrieve the drivers (without last known GPS coordinates) for the specified filter criteria
-- Changes:
/****************************************************/
CREATE FUNCTION fnRetrieveEligibleDrivers_NoGPS(@CarrierID INT, @RegionID INT, @StateID INT, @DriverGroupID INT, @StartDate DATETIME) 
RETURNS @ret TABLE 
(
	ID INT, 
	LastName VARCHAR(20), FirstName VARCHAR(20), FullName VARCHAR(41), FullNameLF VARCHAR(42),
	CarrierID INT, Carrier VARCHAR(40),
	RegionID INT,
	TruckID INT, Truck VARCHAR(10),
	TrailerID INT, Trailer VARCHAR(10),
	AvailabilityFactor DECIMAL(4,2),
	ComplianceFactor DECIMAL(4,2),
	TruckAvailabilityFactor DECIMAL(4,2),
	CurrentWorkload INT,
	CurrentECOT INT,
	HrsOnShift FLOAT,
	HOSHrsOnShift FLOAT,
	HOSHrsLeftToday FLOAT,
	HOSHrsLeftWeek FLOAT,
	DriverScore INT 
)
AS BEGIN
	DECLARE @__ENFORCE_DRIVER_AVAILABILITY__ INT = 13
	DECLARE @__ENFORCE_DRIVER_COMPLIANCE__ INT = 14

	IF @StartDate IS NULL 
	   SELECT @StartDate = dbo.fnDateOnly(GETDATE())

	INSERT INTO @ret
	SELECT X.*,
		DriverScore = 100 * AvailabilityFactor * ComplianceFactor
	FROM (
		SELECT d.ID, 
			d.FirstName, d.LastName, FullName, d.FullNameLF,
			d.CarrierID, d.Carrier,
			d.RegionID,
			d.TruckID, d.Truck,
			d.TrailerID, d.Trailer,

			--Availability
			AvailabilityFactor = CASE WHEN cr_a.Value IS NULL OR dbo.fnToBool(cr_a.Value) = 0 THEN 1 -- availability not enfoced
										WHEN IsAvailable IS NULL THEN 0 -- Not explicitly available
										ELSE IsAvailable END,

			-- Compliance
			ComplianceFactor = CASE WHEN cr_c.Value IS NULL OR dbo.fnToBool(cr_c.Value) = 0 THEN 1 -- compliance not enforced
									WHEN d.CDLExpiration < @StartDate -- CDL expired
											OR d.H2SExpiration < @StartDate -- H2s expired
											OR d.DLExpiration < @StartDate -- DL expired
											--OR d.MedicalCardDate
											THEN 0
									ELSE 1 END, -- all ok

			--Truck/Trailer Availability
			TruckAvailabilityFactor = 1,
			
			--current workload, time on shift
			o.CurrentWorkload,
			CurrentECOT = ISNULL(o.CurrentECOT, 0),
				

			-- HOS
			HrsOnShift = CAST(DATEDIFF(MINUTE, AvailDateTime, GETDATE())/60.0*IsAvailable AS DECIMAL(5,1)), -- from availabilty
			HOSHrsOnShift = hos.TotalHours, -- from HOS
			HOSHrsLeftToday = 0,
			HOSHrsLeftWeek = 0

		FROM viewDriverBase d -- KDA NOTE: slightly more efficient
		LEFT JOIN tblDriverAvailability da ON da.DriverID = d.ID AND CAST(AvailDateTime AS DATE) = @StartDate
		OUTER APPLY dbo.fnCarrierRules(GETDATE(), null, @__ENFORCE_DRIVER_AVAILABILITY__, d.CarrierID, d.ID, d.StateID, d.RegionID, 1) cr_a -- Enforce Driver Availability Carrier Rule
		OUTER APPLY dbo.fnCarrierRules(GETDATE(), null, @__ENFORCE_DRIVER_COMPLIANCE__, d.CarrierID, d.ID, d.StateID, d.RegionID, 1) cr_c
		OUTER APPLY dbo.fnHosSummary(d.ID, @StartDate, DATEADD(DAY, 1, @StartDate)) hos
		OUTER APPLY (
			SELECT CurrentWorkload = COUNT(*), 
					CurrentECOT = SUM(CASE WHEN o.StatusID = 8 THEN ISNULL(r.ECOT, 60+ISNULL(r.ActualMiles,60)+60)/2
										   ELSE COALESCE(r.ECOT, 60+ISNULL(r.ActualMiles,60)+60) END)
			FROM tblOrder o LEFT JOIN tblRoute r ON o.RouteID = r.ID
			WHERE DriverID = d.ID
			  AND StatusID IN (2,7,8) -- Dispatched, Accepted, Picked Up
			  AND DeleteDateUTC IS NULL) o

		WHERE d.DeleteDateUTC IS NULL
			AND d.TruckID IS NOT NULL AND d.TrailerID IS NOT NULL
			AND (ISNULL(@CarrierID, 0) <= 0 OR @CarrierID = d.CarrierID)
			AND (ISNULL(@DriverGroupID, 0) <= 0 OR @DriverGroupID = d.ID)
			AND (ISNULL(@RegionID, -1) <= 0 OR @RegionID = d.RegionID)
			AND (ISNULL(@StateID, 0) <= 0 OR @StateID = d.StateID)
	) AS X
	ORDER BY DriverScore DESC, LastName, FirstName

	RETURN
END

GO


EXEC _spDropFunction 'fnRetrieveDriverLastLocation'
GO
/****************************************************/
-- Created: 2016.09.14 - x.x.x - Joe Engler
-- Purpose: Retrieve the last known location for the specified driver (or all drivers if @DriverID = -1)
-- Changes:
/****************************************************/
CREATE FUNCTION fnRetrieveDriverLastLocation(@DriverID INT) 
RETURNS TABLE AS
RETURN
    SELECT *
        FROM tblDriverLocation
        WHERE ID IN (
            SELECT ID = max(ID)
            FROM tblDriverLocation 
            WHERE DriverID = isnull(nullif(@DriverID, -1), DriverID) -- see note
			GROUP BY DriverID
		)
GO


EXEC _spDropFunction 'fnRetrieveEligibleDrivers'
GO
/****************************************************/
-- Created: 2016.09.14 - x.x.x - Joe Engler
-- Purpose: Retrieve the drivers (with last known GPS coordinates) for the specified filter criteria.  
--				HoursBack is number of hours before a location is considered "stale" and excluded from this query
-- Changes:
/****************************************************/
CREATE FUNCTION fnRetrieveEligibleDrivers(@CarrierID INT, @RegionID INT, @StateID INT, @DriverGroupID INT, @StartDate DATETIME, @HoursBack INT = 0) 
RETURNS TABLE AS
RETURN
	SELECT d.*, q.Lat, q.Lon, DriverLocationDateUTC = q.CreateDateUTC, DriverLocationID = q.ID
	FROM dbo.fnRetrieveEligibleDrivers_NoGPS(@CarrierID, @RegionID, @StateID, @DriverGroupID, @StartDate) d
	LEFT JOIN fnRetrieveDriverLastLocation(NULL) q ON q.DriverID = d.ID 
				AND (@HoursBack = 0 OR CreateDateUTC > DATEADD(HOUR, -@HoursBack, GETUTCDATE())) -- exclude "stale" locations 
GO


COMMIT
SET NOEXEC OFF