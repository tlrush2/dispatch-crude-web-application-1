SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20) = '3.13.8.1'
	, @NewVersion varchar(20) = '3.13.9'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1613 - Convert SMALLDATETIME fields to DATETIME'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


-- Drop DriverLocation constraints/indexes with dates (recreated below)
EXEC _spDropIndex 'idxDriverLocation_Covering1'
GO
ALTER TABLE tblDriverLocation DROP CONSTRAINT DF_DriverLocation_CreateDateUTC
GO

-- Update DriverLocation table to use datetime instead of smalldatetime
ALTER TABLE tblDriverLocation
ALTER COLUMN SourceDateUTC DATETIME NULL
GO

ALTER TABLE tblDriverLocation
ALTER COLUMN CreateDateUTC DATETIME NOT NULL 
GO


-- Recreate DriverLocation constraints/indexes
ALTER TABLE tblDriverLocation ADD CONSTRAINT DF_DriverLocation_CreateDateUTC DEFAULT GETUTCDATE() FOR CreateDateUTC
GO

CREATE NONCLUSTERED INDEX idxDriverLocation_Covering1 ON tblDriverLocation
(
	OrderID ASC,
	OriginID ASC,
	DestinationID ASC,
	SourceDateUTC ASC
)
INCLUDE (ID,
	UID,
	DriverID,
	LocationTypeID,
	Lat,
	Lon,
	DistanceToPoint,
	SourceAccuracyMeters,
	CreateDateUTC,
	CreatedByUser) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

-- Update Audit tables to use datetime/include seconds (rounding error when smalldatetime)
ALTER TABLE tblOrderDBAudit
ALTER COLUMN CreateDateUTC DATETIME NULL
GO

ALTER TABLE tblOrderDBAudit
ALTER COLUMN LastChangeDateUTC DATETIME NULL
GO

ALTER TABLE tblOrderDBAudit
ALTER COLUMN DeleteDateUTC DATETIME NULL
GO

ALTER TABLE tblOrderDBAudit
ALTER COLUMN AcceptLastChangeDateUTC DATETIME NULL
GO

ALTER TABLE tblOrderDBAudit
ALTER COLUMN PickupLastChangeDateUTC DATETIME NULL
GO

ALTER TABLE tblOrderDBAudit
ALTER COLUMN DeliverLastChangeDateUTC DATETIME NULL
GO



ALTER TABLE tblOrderTicketDBAudit
ALTER COLUMN CreateDateUTC DATETIME NULL
GO

ALTER TABLE tblOrderTicketDBAudit
ALTER COLUMN LastChangeDateUTC DATETIME NULL
GO

ALTER TABLE tblOrderTicketDBAudit
ALTER COLUMN DeleteDateUTC DATETIME NULL
GO

INSERT INTO tblLocationType
VALUES
(8, 'Photo', GETUTCDATE(), 'System'),
(9, 'Accept', GETUTCDATE(), 'System'),
(10, 'Transfer', GETUTCDATE(), 'System'),
(11, 'HOS - Off Duty', GETUTCDATE(), 'System'), 
(12, 'HOS - Sleeper', GETUTCDATE(), 'System'), 
(13, 'HOS - On Duty-Driving', GETUTCDATE(), 'System'), 
(14, 'HOS - On Duty-Not Driving', GETUTCDATE(), 'System')

GO


COMMIT
SET NOEXEC OFF