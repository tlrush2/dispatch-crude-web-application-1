/* Destination.Name from varchar(30) -> varchar(50)
   add tblConsignee table, and Destination.ConsigneeID column & foreign key
   add DestinationCustomers table & related foreign keys
   changes to tblCustomer will cause related Orders to be Synced to mobile app
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.2.1'
SELECT  @NewVersion = '2.2.2'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

ALTER TABLE dbo.tblDestination
	DROP CONSTRAINT FK_Destination_TicketTypeID
GO
ALTER TABLE dbo.tblDestTicketType SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblDestination
	DROP CONSTRAINT FK_Destination_TimeZone
GO
ALTER TABLE dbo.tblTimeZone SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblDestination
	DROP CONSTRAINT FK_tblDestination_tblStates
GO
ALTER TABLE dbo.tblState SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblDestination
	DROP CONSTRAINT FK_tblDestination_tblDestinationTypes
GO
ALTER TABLE dbo.tblDestinationType SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblDestination
	DROP CONSTRAINT DF_Destination_CreateDateUTC
GO
ALTER TABLE dbo.tblDestination
	DROP CONSTRAINT DF__tblDestination_TicketType
GO
ALTER TABLE dbo.tblDestination
	DROP CONSTRAINT DF_Destination_TimeZone
GO
ALTER TABLE dbo.tblDestination
	DROP CONSTRAINT DF_Destination_UseDST
GO
CREATE TABLE dbo.Tmp_tblDestination
	(
	ID int NOT NULL IDENTITY (1, 1),
	DestinationTypeID int NOT NULL,
	Name varchar(50) NOT NULL,
	Address varchar(50) NULL,
	City varchar(30) NULL,
	StateID int NULL,
	Zip varchar(10) NULL,
	LAT varchar(20) NULL,
	LON varchar(20) NULL,
	CreateDateUTC smalldatetime NULL,
	CreatedByUser varchar(100) NULL,
	LastChangeDateUTC smalldatetime NULL,
	LastChangedByUser varchar(100) NULL,
	DeleteDateUTC smalldatetime NULL,
	DeletedByUser varchar(100) NULL,
	RegionID int NULL,
	TicketTypeID int NOT NULL,
	Station varchar(10) NULL,
	TimeZoneID tinyint NOT NULL,
	UseDST bit NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_tblDestination SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_tblDestination ADD CONSTRAINT
	DF_Destination_CreateDateUTC DEFAULT (getutcdate()) FOR CreateDateUTC
GO
ALTER TABLE dbo.Tmp_tblDestination ADD CONSTRAINT
	DF__tblDestination_TicketType DEFAULT ((1)) FOR TicketTypeID
GO
ALTER TABLE dbo.Tmp_tblDestination ADD CONSTRAINT
	DF_Destination_TimeZone DEFAULT ((1)) FOR TimeZoneID
GO
ALTER TABLE dbo.Tmp_tblDestination ADD CONSTRAINT
	DF_Destination_UseDST DEFAULT ((1)) FOR UseDST
GO
SET IDENTITY_INSERT dbo.Tmp_tblDestination ON
GO
IF EXISTS(SELECT * FROM dbo.tblDestination)
	 EXEC('INSERT INTO dbo.Tmp_tblDestination (ID, DestinationTypeID, Name, Address, City, StateID, Zip, LAT, LON, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser, RegionID, TicketTypeID, Station, TimeZoneID, UseDST)
		SELECT ID, DestinationTypeID, Name, Address, City, StateID, Zip, LAT, LON, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser, RegionID, TicketTypeID, Station, TimeZoneID, UseDST FROM dbo.tblDestination WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_tblDestination OFF
GO
ALTER TABLE dbo.tblDestinationProducts
	DROP CONSTRAINT FK_DestinationProducts_Destination
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_Destination
GO
ALTER TABLE dbo.tblRoute
	DROP CONSTRAINT FK_tblRoute_tblDestination
GO
DROP TABLE dbo.tblDestination
GO
EXECUTE sp_rename N'dbo.Tmp_tblDestination', N'tblDestination', 'OBJECT' 
GO
ALTER TABLE dbo.tblDestination ADD CONSTRAINT
	tblDestination_PrimaryKey PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.tblDestination ADD CONSTRAINT
	uqDestination_Name UNIQUE NONCLUSTERED 
	(
	Name
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.tblDestination ADD CONSTRAINT
	FK_tblDestination_tblDestinationTypes FOREIGN KEY
	(
	DestinationTypeID
	) REFERENCES dbo.tblDestinationType
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblDestination ADD CONSTRAINT
	FK_tblDestination_tblStates FOREIGN KEY
	(
	StateID
	) REFERENCES dbo.tblState
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblDestination ADD CONSTRAINT
	FK_Destination_TimeZone FOREIGN KEY
	(
	TimeZoneID
	) REFERENCES dbo.tblTimeZone
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblDestination ADD CONSTRAINT
	FK_Destination_TicketTypeID FOREIGN KEY
	(
	TicketTypeID
	) REFERENCES dbo.tblDestTicketType
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblRoute ADD CONSTRAINT
	FK_tblRoute_tblDestination FOREIGN KEY
	(
	DestinationID
	) REFERENCES dbo.tblDestination
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblRoute SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_Destination FOREIGN KEY
	(
	DestinationID
	) REFERENCES dbo.tblDestination
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrder SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblDestinationProducts ADD CONSTRAINT
	FK_DestinationProducts_Destination FOREIGN KEY
	(
	DestinationID
	) REFERENCES dbo.tblDestination
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	
GO
ALTER TABLE dbo.tblDestinationProducts SET (LOCK_ESCALATION = TABLE)
GO

/*******************************************/
-- Date Created: 25 Apr 2013
-- Author: Kevin Alons
-- Purpose: return readonly Order data for Driver App sync
/*******************************************/
ALTER FUNCTION [dbo].[fnOrderReadOnly_DriverApp]( @DriverID int, @LastChangeDateUTC datetime ) RETURNS TABLE AS
RETURN 
	SELECT O.ID
		, O.OrderNum
		, O.StatusID
		, O.TicketTypeID
		, OO.TankTypeID
		, cast(P.PriorityNum as int) AS PriorityNum
		, O.Product
		, O.DueDate
		, O.Origin
		, O.OriginFull
		, OO.OriginType
		, OO.Station AS OriginStation
		, OO.County AS OriginCounty
		, O.OriginState
		, OO.WellAPI AS OriginAPI
		, O.Destination
		, O.DestinationFull
		, D.DestinationType AS DestType 
		, D.Station AS DestinationStation
		--, cast(CASE WHEN D.TicketTypeID = 2 THEN 1 ELSE 0 END as bit) AS DestBOLAvailable
		, OO.BarrelsPerInch AS OriginBarrelsPerInch
		, O.CreateDateUTC
		, O.CreatedByUser
		, O.LastChangeDateUTC
		, O.LastChangedByUser
		, isnull(ODAVD.VirtualDeleteDateUTC, O.DeleteDateUTC) AS DeleteDateUTC
		, isnull(ODAVD.VirtualDeletedByUser, O.DeletedByUser) AS DeletedByUser
		, cast(CASE WHEN isnull(ODAVD.VirtualDeleteDateUTC, O.DeleteDateUTC) IS NULL THEN 0 ELSE 1 END as bit) AS Deleted
		, O.OriginID
		, O.DestinationID
		, cast(O.PriorityID AS int) AS PriorityID
		, O.Operator
		, O.OperatorID
		, O.Pumper
		, O.PumperID
		, O.Producer
		, O.ProducerID
		, O.Customer
		, O.CustomerID
		, O.Carrier
		, O.CarrierID
		, O.ProductID
		, O.TicketType
		, C.ZPLHeaderBlob AS PrintHeaderBlob
		, isnull(C.EmergencyInfo, 'For an emergency (spill, leak, fire, or accident) contact CHEMTREC @ 800.424.9300 (toll free)') AS EmergencyInfo
		, O.DestTicketTypeID
		, O.DestTicketType
	FROM dbo.viewOrder O
	JOIN dbo.tblPriority P ON P.ID = O.PriorityID
	JOIN dbo.viewOrigin OO ON OO.ID = O.OriginID
	JOIN dbo.viewDestination D ON D.ID = O.DestinationID
	JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
	LEFT JOIN tblOrderDriverAppVirtualDelete ODAVD ON ODAVD.OrderID = O.ID AND ODAVD.DriverID = @DriverID
	WHERE (O.StatusID IN (2, 7, 8) -- Dispatched, Accepted, Picked Up
		-- or status = Delivered and the driver still has access to the record
		OR (StatusID = 3 AND O.DestDepartTimeUTC < DATEADD(hour, (SELECT cast(value as int) FROM tblSetting WHERE ID = 10), GETUTCDATE()))
		-- or this potentially was a DriverApp record, that no longer is, so should be included (as deleted)
		OR (ODAVD.ID IS NOT NULL AND ODAVD.VirtualDeleteDateUTC >= @LastChangeDateUTC))
	  AND (O.DriverID = @DriverID OR ODAVD.DriverID = @DriverID)
	  AND (
		@LastChangeDateUTC IS NULL 
		OR O.CreateDateUTC > @LastChangeDateUTC
		OR O.LastChangeDateUTC >= @LastChangeDateUTC
		OR O.DeleteDateUTC >= @LastChangeDateUTC
		OR C.LastChangeDateUTC >= @LastChangeDateUTC
		OR ODAVD.VirtualDeleteDateUTC >= @LastChangeDateUTC)

GO
CREATE TABLE tblConsignee (
  ID int identity(1, 1) not null constraint PK_Consignee PRIMARY KEY
, Name varchar(50) not null
, [CreateDateUTC] [smalldatetime] NULL CONSTRAINT DF_Consignee_CreateDateUTC DEFAULT getutcdate()
, [CreatedByUser] [varchar](100) NULL
, [LastChangeDateUTC] [smalldatetime] NULL
, [LastChangedByUser] [varchar](100) NULL
, [DeleteDateUTC] [smalldatetime] NULL
, [DeletedByUser] [varchar](100) NULL
)
GO
CREATE UNIQUE INDEX udxConsignee_Name ON tblConsignee(Name)
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblConsignee TO dispatchcrude_iis_acct
GO
ALTER TABLE tblDestination ADD ConsigneeID int null constraint FK_Destination_Consignee FOREIGN KEY references tblConsignee(ID) ON DELETE SET NULL
GO
CREATE TABLE tblDestinationCustomers
(
  ID int identity(1, 1) not null constraint PK_DestinationCustomers PRIMARY KEY
, CustomerID int not null constraint FK_DestinationCustomers_Customer FOREIGN KEY REFERENCES tblCustomer(ID) ON DELETE CASCADE
, DestinationID int not null constraint FK_DestinationCustomers_Destination FOREIGN KEY REFERENCES tblDestination(ID) ON DELETE CASCADE
)
GO
CREATE UNIQUE INDEX udxDestinationCustomers_Main ON tblDestinationCustomers(CustomerID, DestinationID)
GO
GRANT SELECT, INSERT, DELETE ON tblDestinationCustomers TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Destination records with translated value and FullName field (which includes Destination Type)
/***********************************/
ALTER VIEW [dbo].[viewDestination] AS
SELECT D.*
	, cast(CASE WHEN D.DeleteDateUTC IS NULL THEN 1 ELSE 0 END as bit) AS Active
	, DT.DestinationType
	, DT.DestinationType + ' - ' + D.Name AS FullName
	, C.Name AS Consignee
	, S.FullName AS State, S.Abbreviation AS StateAbbrev
	, R.Name AS Region
	, DTT.Name AS TicketType
	, TZ.Name AS TimeZone
FROM dbo.tblDestination D
JOIN dbo.tblDestinationType DT ON DT.ID = D.DestinationTypeID
JOIN dbo.tblDestTicketType DTT ON DTT.ID = D.TicketTypeID
LEFT JOIN dbo.tblConsignee C ON C.ID = D.ConsigneeID
LEFT JOIN tblState S ON S.ID = D.StateID
LEFT JOIN dbo.tblRegion R ON R.ID = D.RegionID
LEFT JOIN dbo.tblTimeZone TZ ON TZ.ID = D.TimeZoneID

GO

/**********************************************************/
-- Create Date: 2 Nov 2013
-- Author: Kevin Alons
-- Purpose: return all Origin|Destinations for each Customer
/**********************************************************/
CREATE VIEW viewCustomerOriginDestination AS
SELECT O.ID AS OriginID, O.CustomerID, DC.DestinationID
FROM tblOrigin O
JOIN tblDestinationCustomers DC ON DC.CustomerID = O.CustomerID
GO
GRANT SELECT ON viewCustomerOriginDestination TO dispatchcrude_iis_acct
GO

COMMIT
SET NOEXEC OFF