DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '1.3.10', @NewVersion = '1.4.0'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

CREATE TABLE [dbo].[tblDestTicketType](
	[ID] [int] NOT NULL,
	[Name] [varchar](25) NOT NULL
 CONSTRAINT [PK_DestTicketType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

INSERT INTO tblDestTicketType VALUES (1, 'Basic')
INSERT INTO tblDestTicketType VALUES (2, 'BOL Available')
INSERT INTO tblDestTicketType VALUES (3, 'Visual Meter')
GO

ALTER TABLE tblDestination ADD TicketTypeID int not null DEFAULT (1)
GO
ALTER TABLE tblDestination ADD CONSTRAINT
	FK_Destination_TicketTypeID FOREIGN KEY
	(
		TicketTypeID
	) REFERENCES dbo.tblDestTicketType
	(
		ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO

UPDATE tblDestination SET TicketTypeID = 2 WHERE BOLAvailable = 1
GO

ALTER TABLE tblDestination DROP CONSTRAINT DF_tblDestination_BOLAvailable
GO

ALTER TABLE tblDestination DROP COLUMN BOLAvailable
GO

/***********************************/
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Destination records with translated value and FullName field (which includes Destination Type)
/***********************************/
ALTER VIEW [dbo].[viewDestination] AS
SELECT D.*
	, DT.DestinationType
	, DT.DestinationType + ' - ' + D.Name AS FullName
	, S.FullName AS State, S.Abbreviation AS StateAbbrev
	, R.Name AS Region
	, DTT.Name AS TicketType
FROM dbo.tblDestination D
JOIN dbo.tblDestinationType DT ON DT.ID = D.DestinationTypeID
JOIN dbo.tblDestTicketType DTT ON DTT.ID= D.TicketTypeID
LEFT JOIN tblState S ON S.ID = D.StateID
LEFT JOIN dbo.tblRegion R ON R.ID = D.RegionID

GO

COMMIT
SET NOEXEC OFF