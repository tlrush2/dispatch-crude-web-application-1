SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.1.3.5'
SELECT  @NewVersion = '4.1.4'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Internal: add support view: vw_aspnet_UsersInGroups'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/******************************************/
-- Created: 4.1.4 - 2016.09.12 - Kevin Alons
-- Purpose: retrieve aspnet_UsesInGroups fields + User + Group info
-- Changes:
/******************************************/
CREATE VIEW vw_aspnet_UsersInGroups AS
	SELECT UIG.*, U.UserName, G.GroupName
	FROM aspnet_UsersInGroups UIG
	JOIN aspnet_Users U ON U.UserId = UIG.UserId
	JOIN aspnet_Groups G ON G.GroupId = UIG.GroupId
GO

COMMIT
SET NOEXEC OFF