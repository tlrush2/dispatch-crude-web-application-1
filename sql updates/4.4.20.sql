SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.4.19'
SELECT  @NewVersion = '4.4.20'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'JT-697 - Add Terminal maintenance page and add Terminal to other pages (Carrier, driver, etc.)'	
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO



-- Add new terminal permissions
EXEC spAddNewPermission 'viewTerminals', 'Allow user to view Terminals maintenance page', 'Business Rules', 'Terminals - View'
GO
EXEC spAddNewPermission 'createTerminals', 'Allow user to create Terminals', 'Business Rules', 'Terminals - Create'
GO
EXEC spAddNewPermission 'editTerminals', 'Allow user to edit Terminals', 'Business Rules', 'Terminals - Edit'
GO
EXEC spAddNewPermission 'deactivateTerminals', 'Allow user to deactivate Terminals', 'Business Rules', 'Terminals - Deactivate'
GO


-- Add forign key fields for Terminal to other tables
ALTER TABLE tblCarrier ADD TerminalID INT NULL
	CONSTRAINT FK_Carrier_Terminal REFERENCES tblTerminal(ID)
GO

ALTER TABLE tblTruck ADD TerminalID INT NULL
	CONSTRAINT FK_Truck_Terminal REFERENCES tblTerminal(ID)
GO

ALTER TABLE tblTrailer ADD TerminalID INT NULL
	CONSTRAINT FK_Trailer_Terminal REFERENCES tblTerminal(ID)
GO

ALTER TABLE tblOrigin ADD TerminalID INT NULL
	CONSTRAINT FK_Origin_Terminal REFERENCES tblTerminal(ID)
GO

ALTER TABLE tblDestination ADD TerminalID INT NULL
	CONSTRAINT FK_Destination_Terminal REFERENCES tblTerminal(ID)
GO



/*****************************************
UPDATE VIEWS
******************************************/
GO

/*********************************************
-- Date Created: 4 Apr 2013
-- Author: Kevin Alons
-- Purpose: return Carrier records with translated "friendly" values
-- Changes:
	-- 3.7.28 - 6/14/2015 - KDA - remove references to MinSettlement & SettlementFactor (which are now a BestMatch separate table)
	-- 4.4.20 - 1/18/2017 - BSB - Added Terminal
**********************************************/
ALTER VIEW viewCarrier AS
	SELECT C.*
		, cast(CASE WHEN C.DeleteDateUTC IS NULL THEN 1 ELSE 0 END as bit) AS Active
		, CT.Name AS CarrierType
		, S.Abbreviation AS StateAbbrev
		, Terminal = T.Name
	FROM tblCarrier C 
		JOIN tblCarrierType CT ON CT.ID = C.CarrierTypeID
		LEFT JOIN tblState S ON S.ID = C.StateID
		LEFT JOIN tblTerminal T ON T.ID = C.TerminalID
GO


/***********************************
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Destination records with translated value and FullName field (which includes Destination Type)
-- Changes:
	- 3.10.6 - 2016/02/01 - BSB - Added LatLon column for Route page maps
	- 4.4.20 - 2017/01/18 - BSB - Added Terminal
***********************************/
ALTER VIEW viewDestination AS
SELECT D.* 
	, Active = cast(CASE WHEN D.DeleteDateUTC IS NULL THEN 1 ELSE 0 END as bit)
	, DT.DestinationType
	, DT.DestinationType + ' - ' + D.Name AS FullName
	, S.FullName AS State, S.Abbreviation AS StateAbbrev
	, S.CountryID
	, Country = CO.Name, CountryShort = CO.Abbrev
	, R.Name AS Region
	, DTT.Name AS TicketType
	, TZ.Name AS TimeZone
	, UOM.Name AS UOM
	, UOM.Abbrev AS UomShort
	, LatLon = D.LAT + ',' + D.LON
	, Terminal = T.Name
FROM dbo.tblDestination D
	JOIN dbo.tblDestinationType DT ON DT.ID = D.DestinationTypeID
	JOIN dbo.tblDestTicketType DTT ON DTT.ID = D.TicketTypeID
	LEFT JOIN tblState S ON S.ID = D.StateID
	LEFT JOIN tblCountry CO ON CO.ID = S.CountryID
	LEFT JOIN dbo.tblRegion R ON R.ID = D.RegionID
	LEFT JOIN dbo.tblTimeZone TZ ON TZ.ID = D.TimeZoneID
	LEFT JOIN dbo.tblUom UOM ON UOM.ID = D.UomID
	LEFT JOIN tblTerminal T ON T.ID = D.TerminalID
GO


/***********************************
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Origin records with translated value and FullName field (which includes Origin Type)
-- Changes:
	- 3.8.11 - 2015/07/28 - KDA - remove Customer info (no longer 1 to 1 relationship with Origin
	- 3.10.6 - 2016/02/01 - BSB - Added LatLon column for Route page maps
	- 4.4.20 - 2017/01/18 - BSB - Added Terminal
***********************************/
ALTER VIEW viewOrigin AS
SELECT O.*
	, FullName = CASE WHEN O.H2S = 1 THEN 'H2S-' ELSE '' END + OT.OriginType + ' - ' + O.Name
	, OT.OriginType
	, State = S.FullName 
	, StateAbbrev = S.Abbreviation 
	, S.CountryID
	, Country = CO.Name, CountryShort = CO.Abbrev
	, Operator = OP.Name
	, Pumper = P.FullName
	, TicketType = TT.Name
	, TT.ForTanksOnly
	, Region = R.Name
	, Producer = PR.Name 
	, TimeZone = TZ.Name 
	, UOM = UOM.Name 
	, UomShort = UOM.Abbrev 
	, OT.HasTanks
	, Active = cast(CASE WHEN O.DeleteDateUTC IS NULL THEN 1 ELSE 0 END as bit)
	, TankCount = (SELECT COUNT(*) FROM tblOriginTank WHERE OriginID = O.ID AND DeleteDateUTC IS NULL) 
	, LatLon = O.LAT + ',' + O.LON
	, Terminal = T.Name
FROM dbo.tblOrigin O
	JOIN dbo.tblOriginType OT ON OT.ID = O.OriginTypeID
	LEFT JOIN tblState S ON S.ID = O.StateID
	LEFT JOIN tblCountry CO ON CO.ID = S.CountryID
	LEFT JOIN dbo.tblOperator OP ON OP.ID = O.OperatorID
	LEFT JOIN dbo.viewPumper P ON P.ID = O.PumperID
	LEFT JOIN dbo.tblTicketType TT ON TT.ID = O.TicketTypeID
	LEFT JOIN dbo.tblRegion R ON R.ID = O.RegionID
	LEFT JOIN dbo.tblProducer PR ON PR.ID = O.ProducerID
	LEFT JOIN dbo.tblTimeZone TZ ON TZ.ID = O.TimeZoneID
	LEFT JOIN dbo.tblUom UOM ON UOM.ID = O.UomID
	LEFT JOIN tblTerminal T ON T.ID = O.TerminalID
GO


/***********************************/
-- Date Created: 26 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Truck records with FullName & translated "friendly" values
-- Changes: 
--		3.10.13		2016-02-29		JAE		Add Truck Type
--		4.0.12		2016-09-01		JAE		Move Garage State join here (removing odometer view)
--		4.4.20		2017-01-18		BSB		Added Terminal
/***********************************/
ALTER VIEW viewTruck AS
SELECT T.*
	, Active = cast(CASE WHEN isnull(C.DeleteDateUTC, T.DeleteDateUTC) IS NULL THEN 1 ELSE 0 END as bit) -- mark truck as active if the carrier and the truck are not deleted
	, FullName = T.IDNumber
	, TruckType = TT.Name
	, LoadOnTruck = TT.LoadOnTruck 
	, CarrierType = CT.Name
	, Carrier = C.Name
	, GarageStateAbbrev = S.Abbreviation  -- 3.11.12.2 (4.0.12)
	, Terminal = TERM.Name
FROM dbo.tblTruck T
	LEFT JOIN dbo.tblTruckType TT ON TT.ID = T.TruckTypeID
	JOIN dbo.tblCarrier C ON C.ID = T.CarrierID
	LEFT JOIN dbo.tblCarrierType CT ON CT.ID = C.CarrierTypeID
	LEFT JOIN tblState S ON S.ID = T.GarageStateID
	LEFT JOIN tblTerminal TERM ON TERM.ID = T.TerminalID
GO


/***********************************/
-- Date Created: 26 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Trailer records with FullName & translated "friendly" values
-- Changes:
--		4.4.20	2017/01/18		BSB		Added Terminal
/***********************************/
ALTER VIEW viewTrailer AS
SELECT T.*
	, cast(CASE WHEN isnull(C.DeleteDateUTC, T.DeleteDateUTC) IS NULL THEN 1 ELSE 0 END as bit) AS Active
	, ISNULL(Compartment1Units, 0) 
		+ ISNULL(Compartment2Units, 0) 
		+ ISNULL(Compartment3Units, 0) 
		+ ISNULL(Compartment4Units, 0) 
		+ ISNULL(Compartment5Units, 0) AS TotalCapacityUnits
	, CASE WHEN isnull(Compartment1Units, 0) > 0 THEN 1 ELSE 0 END 
		+ CASE WHEN isnull(Compartment2Units, 0) > 0 THEN 1 ELSE 0 END 
		+ CASE WHEN isnull(Compartment3Units, 0) > 0 THEN 1 ELSE 0 END
		+ CASE WHEN isnull(Compartment4Units, 0) > 0 THEN 1 ELSE 0 END 
		+ CASE WHEN isnull(Compartment5Units, 0) > 0 THEN 1 ELSE 0 END AS CompartmentCount
	, T.IDNumber AS FullName
	, TT.Name AS TrailerType
	, isnull(CT.Name, 'Unknown') AS CarrierType
	, C.Name AS Carrier
	, UOM.Name AS UOM
	, UOM.Abbrev AS UomShort
	, Terminal = TERM.Name
FROM dbo.tblTrailer T
	LEFT JOIN dbo.tblTrailerType TT ON TT.ID = T.TrailerTypeID
	JOIN dbo.tblCarrier C ON C.ID = T.CarrierID
	LEFT JOIN dbo.tblCarrierType CT ON CT.ID = C.CarrierTypeID
	LEFT JOIN dbo.tblUom UOM ON UOM.ID = T.UomID
	LEFT JOIN tblTerminal TERM ON TERM.ID = T.TerminalID
GO


/***********************************/
-- Date Created: 26 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Drivers table records with LastDelivereredOrderTime + computed assigned ASP.NET usernames
-- Changes
--	4.4.14 - JAE & BB	- 2016-11-29 - Add previously included fields, Moved UserNames to viewDriverBase
--	4.4.19 - BB			- 2017-01-18 - Added Terminal
--	4.4.20 - BSB			- 2017-01-19 - Moved LastDeliveredOrderTime from here to viewDriverBase so it can be displayed on the driver maintenance page
/***********************************/
ALTER VIEW viewDriver AS
SELECT DB.*	
	, DLDocument = dl.Document
	, DLDocName = dl.DocumentName
	, DLExpiration = dl.DocumentDate
	, CDLDocument = cdl.Document
	, CDLDocName = cdl.DocumentName
	, CDLExpiration = cdl.DocumentDate
	, DrugScreenBackgroundCheckDocument = dsbc.Document
	, DrugScreenBackgroundCheckDocName = dsbc.DocumentName
	, MotorVehicleReportDocument = mvr.Document
	, MotorVehicleReportDocName = mvr.DocumentName
	, MedicalCardDocument = mc.Document
	, MedicalCardDocName = mc.DocumentName
	, MedicalCardDate = mc.DocumentDate
	, LongFormPhysicalDocument = lfp.Document
	, LongFormPhysicalDocName = lfp.DocumentName
	, LongFormPhysicalDate = lfp.DocumentDate
	, EmploymentAppDocument = ea.Document
	, EmploymentAppDocName = ea.DocumentName
	, PolicyManualReceiptDocument = pmr.Document
	, PolicyManualReceiptDocName = pmr.DocumentName
	, TrainingReceiptDocument = tr.Document
	, TrainingReceiptDocName = tr.DocumentName
	, OrientationTestDocument = ot.Document
	, OrientationTestDocName = ot.DocumentName
	, DrugScreenBackgroundReleaseDocument = dsbr.Document
	, DrugScreenBackgroundReleaseDocName = dsbr.DocumentName
	, Terminal = T.Name
FROM viewDriverBase DB
	LEFT JOIN tblTerminal T ON T.ID = DB.TerminalID
	OUTER APPLY (select TOP 1 * FROM tblDriverCompliance WHERE ComplianceTypeID=1 AND DriverID = DB.ID ORDER BY DocumentDate DESC) dl
	OUTER APPLY (select TOP 1 * FROM tblDriverCompliance WHERE ComplianceTypeID=2 AND DriverID = DB.ID ORDER BY DocumentDate DESC) cdl
	OUTER APPLY (select TOP 1 * FROM tblDriverCompliance WHERE ComplianceTypeID=3 AND DriverID = DB.ID ORDER BY DocumentDate DESC) dsbc
	OUTER APPLY (select TOP 1 * FROM tblDriverCompliance WHERE ComplianceTypeID=4 AND DriverID = DB.ID ORDER BY DocumentDate DESC) mvr
	OUTER APPLY (select TOP 1 * FROM tblDriverCompliance WHERE ComplianceTypeID=5 AND DriverID = DB.ID ORDER BY DocumentDate DESC) mc
	OUTER APPLY (select TOP 1 * FROM tblDriverCompliance WHERE ComplianceTypeID=6 AND DriverID = DB.ID ORDER BY DocumentDate DESC) lfp
	OUTER APPLY (select TOP 1 * FROM tblDriverCompliance WHERE ComplianceTypeID=7 AND DriverID = DB.ID ORDER BY DocumentDate DESC) ea
	OUTER APPLY (select TOP 1 * FROM tblDriverCompliance WHERE ComplianceTypeID=8 AND DriverID = DB.ID ORDER BY DocumentDate DESC) pmr
	OUTER APPLY (select TOP 1 * FROM tblDriverCompliance WHERE ComplianceTypeID=9 AND DriverID = DB.ID ORDER BY DocumentDate DESC) tr
	OUTER APPLY (select TOP 1 * FROM tblDriverCompliance WHERE ComplianceTypeID=10 AND DriverID = DB.ID ORDER BY DocumentDate DESC) ot
	OUTER APPLY (select TOP 1 * FROM tblDriverCompliance WHERE ComplianceTypeID=11 AND DriverID = DB.ID ORDER BY DocumentDate DESC) dsbr
GO


/***********************************/
-- Date Created: 26 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Drivers table records with "friendly" translated values included
-- Changes: 
-- 3.10.7		- 2016/01/27 - BB	- Added TabletID and PrinterSerial from tblDriver_Sync
-- 3.11.15.2	- 2016/04/20 - BB	- Added DriverGroupName to fix filtering on web page
-- 4.4.14		- 2016/12/08 - BB	- Added Operating State, Added DriverShiftTypeName, Move UserNames from viewDriver to viewDriverBase
--							 - KDA	- use fnFnameWithDeleted() method so future changes can be done in a single place (leaving the views unchanged)
-- 4.4.20		- 2017/01/19 - BSB	- Moved LastDeliveredOrderTime from viewDriver to here so it can be displayed on the driver maintenance page
/***********************************/
ALTER VIEW viewDriverBase AS
SELECT X.*
	, FullNameD = dbo.fnNameWithDeleted(FullName, DeleteDateUTC) -- 4.4.14 - Updated from "Deleted" 12/15/16
FROM (
	SELECT D.*
		, Active = cast(CASE WHEN isnull(C.DeleteDateUTC, D.DeleteDateUTC) IS NULL THEN 1 ELSE 0 END as bit) 
		, FullName = D.FirstName + ' ' + D.LastName 
		, FullNameLF = D.LastName + ', ' + D.FirstName 
		, CarrierType = isnull(CT.Name, 'Unknown') 
		, Carrier = C.Name 
		, StateAbbrev = S.Abbreviation 
		, OperatingStateAbbrev = S2.Abbreviation
		, Truck = T.FullName
		, Trailer = T1.FullName 
		, Trailer2 = T2.FullName 
		, Region = R.Name 
		, DS.MobileAppVersion
		, DS.TabletID
		, DS.PrinterSerial
		, DriverGroupName = DG.Name
		, DriverShiftTypeName = DST.Name
		, DUN.UserNames
		, LastDeliveredOrderTime = (SELECT MAX(DestDepartTimeUTC) FROM dbo.tblOrder WHERE DriverID = D.ID)
	FROM dbo.tblDriver D 
	LEFT JOIN dbo.fnDriverUserNames() DUN ON DUN.DriverID = D.ID
	JOIN dbo.tblCarrier C ON C.ID = D.CarrierID 
	LEFT JOIN dbo.tblDriver_Sync DS ON DS.DriverID = D.ID
	LEFT JOIN dbo.tblCarrierType CT ON CT.ID = C.CarrierTypeID
	LEFT JOIN tblState S ON S.ID = D.StateID
	LEFT JOIN tblState S2 ON S2.ID = D.OperatingStateID
	LEFT JOIN viewTruck T ON T.ID = D.TruckID
	LEFT JOIN viewTrailer T1 ON T1.ID = D.TrailerID
	LEFT JOIN viewTrailer T2 ON T2.ID = D.Trailer2ID
	LEFT JOIN tblRegion R ON R.ID = D.RegionID
	LEFT JOIN tblDriverGroup DG ON DG.ID = D.DriverGroupID
	LEFT JOIN tblDriverShiftType DST ON DST.ID = D.DriverShiftTypeID
) X
GO







-- Refresh views / functions / sp's
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRecompileAllStoredProcedures
GO
EXEC _spRebuildAllObjects
GO
EXEC _spRebuildAllObjects
GO



COMMIT
SET NOEXEC OFF