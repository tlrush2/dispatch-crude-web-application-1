/*
	-- fix date issue with Financial views (InvoiceRatesAppliedDate column)
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.8.2'
SELECT  @NewVersion = '2.8.3'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details + FINANCIAL INFO into a single view
/***********************************/
ALTER VIEW [dbo].[viewOrder_Financial_Carrier] AS 
	SELECT OE.* 
		, dbo.fnOrderTicketDetails(OE.ID, 'TicketNums', '<br/>') AS TicketNums
		, dbo.fnOrderTicketDetails(OE.ID, 'TankNums', '<br/>') AS TankNums
		, dbo.fnRerouteDetails(OE.ID, 'PreviousDestinations', '<br/>') AS PreviousDestinations
		, dbo.fnRerouteDetails(OE.ID, 'RerouteUsers', '<br/>') AS RerouteUsers
		, dbo.fnRerouteDetails(OE.ID, 'RerouteDates', '<br/>') AS RerouteDates
		, dbo.fnRerouteDetails(OE.ID, 'RerouteNotes', '<br/>') AS RerouteNotes
		, InvoiceRatesAppliedDate = dbo.fnUTC_To_Local(ISNULL(OIC.LastChangeDateUTC, OIC.CreateDateUTC), OE.OriginTimeZoneID, OE.OriginUseDST)
		, OIC.BatchID
		, SB.BatchNum AS InvoiceBatchNum
		, OIC.ChainupFee AS InvoiceChainupFee
		, OIC.RerouteFee AS InvoiceRerouteFee
		, OIC.BillableOriginWaitMinutes AS InvoiceOriginBillableWaitMinutes
		, OIC.BillableDestWaitMinutes AS InvoiceDestBillableWaitMinutes
		, isnull(OIC.BillableOriginWaitMinutes, 0) + ISNULL(OIC.BillableDestWaitMinutes, 0) AS InvoiceTotalBillableWaitMinutes
		, isnull(WFSU.Name, 'None') AS InvoiceWaitFeeSubUnit
		, isnull(WFRT.Name, 'None') AS InvoiceWaitFeeRoundingType
		, OIC.WaitRate AS InvoiceWaitRate
		, OIC.OriginWaitFee AS InvoiceOriginWaitFee
		, OIC.DestWaitFee AS InvoiceDestWaitFee
		, ISNULL(OIC.OriginWaitFee, 0) + ISNULL(OIC.DestWaitFee, 0) AS InvoiceTotalWaitFee
		, OIC.RejectionFee AS InvoiceRejectionFee
		, OIC.H2SRate AS InvoiceH2SRate
		, OIC.H2SFee AS InvoiceH2SFee
		, OIC.TaxRate AS InvoiceTaxRate
		, U.Name AS InvoiceUom
		, U.Abbrev AS InvoiceUomShort
		, OIC.MinSettlementUnits AS InvoiceMinSettlementUnits
		, OIC.Units AS InvoiceUnits
		, OIC.RouteRate AS InvoiceRouteRate
		, OIC.LoadFee AS InvoiceLoadFee
		, OIC.TotalFee AS InvoiceTotalFee
		, OIC.FuelSurcharge AS InvoiceFuelSurcharge
	FROM dbo.viewOrderExportFull OE
	LEFT JOIN dbo.tblOrderInvoiceCarrier OIC ON OIC.OrderID = OE.ID
	LEFT JOIN dbo.tblCarrierSettlementBatch SB ON SB.ID = OIC.BatchID
	LEFT JOIN dbo.tblUom U ON U.ID = OIC.UomID	
	LEFT JOIN dbo.tblWaitFeeSubUnit WFSU ON WFSU.ID = OIC.WaitFeeSubUnitID
	LEFT JOIN dbo.tblWaitFeeRoundingType WFRT ON WFRT.ID = OIC.WaitFeeRoundingTypeID

GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details + FINANCIAL INFO into a single view
/***********************************/
ALTER VIEW [dbo].[viewOrder_Financial_Customer] AS 
	SELECT OE.* 
		, dbo.fnOrderTicketDetails(OE.ID, 'TicketNums', '<br/>') AS TicketNums
		, dbo.fnOrderTicketDetails(OE.ID, 'TankNums', '<br/>') AS TankNums
		, dbo.fnRerouteDetails(OE.ID, 'PreviousDestinations', '<br/>') AS PreviousDestinations
		, dbo.fnRerouteDetails(OE.ID, 'RerouteUsers', '<br/>') AS RerouteUsers
		, dbo.fnRerouteDetails(OE.ID, 'RerouteDates', '<br/>') AS RerouteDates
		, dbo.fnRerouteDetails(OE.ID, 'RerouteNotes', '<br/>') AS RerouteNotes
		, InvoiceRatesAppliedDate = dbo.fnUTC_to_Local(ISNULL(OIC.LastChangeDateUTC, OIC.CreateDateUTC), OE.OriginTimeZoneID, OE.OriginUseDST)
		, OIC.BatchID
		, SB.BatchNum AS InvoiceBatchNum
		, OIC.ChainupFee AS InvoiceChainupFee
		, OIC.RerouteFee AS InvoiceRerouteFee
		, OIC.BillableOriginWaitMinutes AS InvoiceOriginBillableWaitMinutes
		, OIC.BillableDestWaitMinutes AS InvoiceDestBillableWaitMinutes
		, isnull(OIC.BillableOriginWaitMinutes, 0) + ISNULL(OIC.BillableDestWaitMinutes, 0) AS InvoiceTotalBillableWaitMinutes
		, isnull(WFSU.Name, 'None') AS InvoiceWaitFeeSubUnit
		, isnull(WFRT.Name, 'None') AS InvoiceWaitFeeRoundingType
		, OIC.WaitRate AS InvoiceWaitRate
		, OIC.OriginWaitFee AS InvoiceOriginWaitFee
		, OIC.DestWaitFee AS InvoiceDestWaitFee
		, ISNULL(OIC.OriginWaitFee, 0) + ISNULL(OIC.DestWaitFee, 0) AS InvoiceTotalWaitFee
		, OIC.RejectionFee AS InvoiceRejectionFee
		, OIC.H2SRate AS InvoiceH2SRate
		, OIC.H2SFee AS InvoiceH2SFee
		, OIC.TaxRate AS InvoiceTaxRate
		, U.Name AS InvoiceUom
		, U.Abbrev AS InvoiceUomShort
		, OIC.MinSettlementUnits AS InvoiceMinSettlementUnits
		, OIC.Units AS InvoiceUnits
		, OIC.RouteRate AS InvoiceRouteRate
		, OIC.LoadFee AS InvoiceLoadFee
		, OIC.TotalFee AS InvoiceTotalFee
		, OIC.FuelSurcharge AS InvoiceFuelSurcharge
	FROM dbo.viewOrderExportFull OE
	LEFT JOIN dbo.tblOrderInvoiceCustomer OIC ON OIC.OrderID = OE.ID
	LEFT JOIN dbo.tblCustomerSettlementBatch SB ON SB.ID = OIC.BatchID
	LEFT JOIN dbo.tblUom U ON U.ID = OIC.UomID	
	LEFT JOIN dbo.tblWaitFeeSubUnit WFSU ON WFSU.ID = OIC.WaitFeeSubUnitID
	LEFT JOIN dbo.tblWaitFeeRoundingType WFRT ON WFRT.ID = OIC.WaitFeeRoundingTypeID

GO

COMMIT
SET NOEXEC OFF