SET NOEXEC OFF  -- since this is 
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.12.6'
SELECT  @NewVersion = '3.12.6.1'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'DCWEB-1450 Fix log to allow deleting of Email Subscription'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


DROP TABLE tblUserReportEmailSubscriptionLog
GO
CREATE TABLE tblUserReportEmailSubscriptionLog
(
  ID int identity(1, 1) NOT NULL CONSTRAINT PK_UserReportEmailSubscriptionLog PRIMARY KEY
, UserName VARCHAR(100) NOT NULL
, UserReportID int NOT NULL CONSTRAINT FK_UserReportEmailSubscriptionLog_UserReport FOREIGN KEY REFERENCES tblUserReportDefinition(ID)
, EmailAddressCSV VARCHAR(MAX) NOT NULL
, ExecutionDateUTC datetime NOT NULL CONSTRAINT DF_UserReportEmailSubscriptionLog_ExecutionDateUTC DEFAULT (getutcdate())
, ExecutionDurationSeconds decimal(8, 3) NULL
, Successful bit NOT NULL CONSTRAINT DF_UserReportEmailSubscriptionLog_Successful DEFAULT (1)
, Errors varchar(1000) NULL
)
GO
GRANT INSERT ON tblUserReportEmailSubscriptionLog TO role_iis_acct
GO



COMMIT
SET NOEXEC OFF