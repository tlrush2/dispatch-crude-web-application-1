-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.7.5'
SELECT  @NewVersion = '4.7.5.1'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0 AND @NewVersion NOT LIKE '4.7.8%'

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Questionnaire update to increase size of select fields'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

ALTER TABLE tblQuestionnaireTemplate ALTER COLUMN ExceptionEmail VARCHAR(1000)
GO

ALTER TABLE tblQuestionnaireQuestion ALTER COLUMN AnswersFormat VARCHAR(1000)
GO

ALTER TABLE tblQuestionnaireResponse ALTER COLUMN Notes VARCHAR(1000)
GO

COMMIT
SET NOEXEC OFF