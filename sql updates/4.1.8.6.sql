SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.1.8.5'
SELECT  @NewVersion = '4.1.8.6'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Settlement - improve data retrieval performance for Shipper | Carrier | Driver settlement pages'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

exec _spDropFunction 'fnTrimToNull'
go
/***********************************/
-- Created: 4.1.8.6 - 2016.09.21 - Kevin Alons
-- Purpose: convert a blank/empty string to NULL
-- Changes:
/***********************************/
CREATE FUNCTION fnTrimToNull(@expr varchar(max)) RETURNS varchar(max) AS
BEGIN
	SET @expr = ltrim(rtrim(@expr))
	SET @expr = nullif(@expr, '')
	RETURN (@expr)
END
GO
GRANT EXECUTE ON fnTrimToNull TO role_iis_acct
GO

/***********************************/
-- Created: ?.?.? - 2013/03/09 - Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
-- Changes:
-- 3.8.11	- 2015/07/28 - KDA	- remove OriginShipperRegion field
-- 3.9.0	- 2/15/08/14 - KDA	- return Approved column
-- 3.9.19.6	- 2015/09/30 - KDA	- add OverrideXXX fields from OrderApproval table
-- 3.9.34	- 2015/12/16 - BB	- Remove deleted orders from the results (DCWEB-851)
-- 3.10.13.6 - 2015/03/01 - JAE	- add TruckTypeID
-- 3.11.3.1	- 2016/03/22 - KDA	- fix to no-op when @BatchID = 0
-- 3.11.19	- 2016/05/02 - BB	- Add Job number and Contract number
-- 3.11.20.3 - 2016/05/11 - KDA	- optimize @BatchID filtering to query directly from tblOrderSettlementShipper table
--								- use tblOrder and minimal JOINs intead of expensive viewOrder
-- 4.1.0	- 2016/08/08 - KDA	- use new @StartSession & @SessionID parameters to persist the retrieved records as a "Session"
--								- stop defaulting @StartDate to 3 months prior if not supplied (null parameter value provided)
--								- use simplified viewOrder_Financial_Shipper for return results - which is now again optimized for "reasonable" performance
-- 4.1.3.5	- 2016/09/17 - BB	- DCWEB-1770: Add reject filter
-- 4.1.8.6	- 2016.09.21 - KDA	- remove dynamic sql and retrieve data in 2 stages (like before) getting the orderID first, then the final data (expensive query)
--								- use new fnTrimToNull() to ensure optional string parameters are trimmed to NULL
/***********************************/
ALTER PROCEDURE spRetrieveOrdersFinancialShipper
(
  @StartDate date = NULL -- will default to first day of 3rd prior month for efficiency sake
, @EndDate date = NULL
, @ShipperID int = -1 -- all
, @ProductGroupID int = -1 -- all 
, @TruckTypeID int = -1 -- all
, @OriginStateID int = -1 -- all 
, @DestStateID int = -1 -- all
, @ProducerID int = -1 -- all
, @BatchID int = NULL -- either show unbatched (NULL), or the specified batch orders
, @JobNumber varchar(20) = NULL -- 3.11.19
, @ContractNumber varchar(20) = NULL -- 3.11.19
, @Rejected int = -1 -- all (rejected + not rejected) -- 4.1.3.5
, @StartSession bit = 1 -- when set to 1, assign the retrieved records to a new Session (ID), when 0 and a sessionID was assigned, then retrieve it
, @SessionID varchar(100) = NULL OUTPUT -- if @startSession = 1, then this return a new value, otherwise will retrieve the existing records in that session
) AS 
BEGIN
	IF (@StartSession IS NULL) SET @StartSession = CASE WHEN @SessionID IS NULL THEN 0 ELSE 1 END

	-- if an empty/blank string parameter is provided, trim it to NULL
	SELECT @JobNumber = dbo.fnTrimToNull(@JobNumber)
		, @ContractNumber = dbo.fnTrimToNull(@ContractNumber)

	-- validate the incoming parameters
	IF (@BatchID IS NOT NULL
		AND (@ShipperID <> -1 
			OR @ProductGroupID <> -1
			OR @TruckTypeID <> -1
			OR @OriginStateID <> -1
			OR @DestStateID <> -1
			OR @ProducerID <> -1))
	BEGIN
		RAISERROR('@BatchID parameter cannot be specified with other parameters', 16, 1)
		RETURN
	END

	DECLARE @IDs IDTABLE

	-- if a @BatchID value was specified then just use this criteria alone
    IF @BatchID IS NOT NULL
	BEGIN
		INSERT INTO @IDs SELECT OrderID FROM tblOrderSettlementShipper WHERE BatchID = @BatchID
	END
	ELSE IF (@StartSession = 0 AND @SessionID IS NOT NULL)
	BEGIN
		INSERT INTO @IDs SELECT OrderID FROM tblOrderSettlementSelectionShipper WHERE SessionID = @SessionID
	END
	ELSE
	BEGIN
		INSERT INTO @IDs 
			SELECT O.ID
			FROM tblOrder O
			JOIN tblProduct P ON P.ID = O.ProductID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			JOIN tblDestination D ON D.ID = O.DestinationID
			JOIN tblTruck T1 ON T1.ID = O.TruckID
			LEFT JOIN dbo.tblOrderTransfer OTR ON OTR.OrderID = O.ID
			LEFT JOIN tblTruck T2 ON T2.ID = OTR.OriginTruckID
			LEFT JOIN tblOrderSettlementShipper OS ON OS.OrderID = O.ID
			WHERE O.StatusID IN (4)  
			  AND O.DeleteDateUTC IS NULL  -- 3.9.34
			  AND OS.BatchID IS NULL -- only get order if it's not part of a batch
			  AND (@ShipperID=-1 OR O.CustomerID=@ShipperID) 
			  AND (@ProductGroupID=-1 OR P.ProductGroupID=@ProductGroupID) 
			  AND (@TruckTypeID=-1 OR T1.TruckTypeID=@TruckTypeID OR T2.TruckTypeID=@TruckTypeID)
			  AND (@ProducerID=-1 OR O.ProducerID=@ProducerID) 
			  AND (@OriginStateID=-1 OR OO.StateID=@OriginStateID) 
			  AND (@DestStateID=-1 OR D.StateID=@DestStateID) 
			  AND (@StartDate IS NULL OR O.OrderDate >= @StartDate) 
			  AND (@EndDate IS NULL OR O.OrderDate <= @EndDate)
			  AND (@JobNumber IS NULL OR O.JobNumber = @JobNumber)
			  AND (@ContractNumber IS NULL OR O.ContractNumber = @ContractNumber)
			  AND (@Rejected=-1 OR O.Rejected=@Rejected) -- 4.1.3.5
	END
	
	-- create the temp table to store the data
	SELECT *, RateApplySel = cast(1 as bit), BatchSel = cast(1 as bit), SessionID = cast(NULL AS varchar(100))
	INTO #ret 
	FROM viewOrder_Financial_Shipper 
	WHERE 1 = 0
	
	-- get the data into the temp table
	INSERT INTO #ret
		SELECT O.*
			, OS.RateApplySel, OS.BatchSel, OS.SessionID
		FROM viewOrder_Financial_Shipper O
		LEFT JOIN tblOrderSettlementSelectionShipper OS ON OS.OrderID = O.ID
		WHERE O.OrderID IN (SELECT ID FROM @IDs)

	-- do the SessionID logic
	IF (@StartSession = 1)
	BEGIN
		BEGIN TRAN RetrieveOrdersFinancialShipper
	
		DELETE FROM tblOrderSettlementSelectionShipper WHERE OrderID IN (SELECT ID FROM #ret) OR SessionID IN (SELECT SessionID FROM #ret)
		SELECT @sessionID = isnull(max(SessionID), 0) + 1 FROM tblOrderSettlementSelectionShipper
		UPDATE #ret SET SessionID = @sessionID, RateApplySel = 1, BatchSel = CASE WHEN HasError = 1 THEN 0 ELSE 1 END
		INSERT INTO tblOrderSettlementSelectionShipper (OrderID, SessionID, RateApplySel, BatchSel)
			SELECT ID, @sessionID, RateApplySel, BatchSel FROM #ret
	
		COMMIT TRAN RetrieveOrdersFinancialShipper
	END
	
	-- return the data to the caller
	SELECT * FROM #ret
END

GO

/**********************************/
-- Created: 2013/03/09 - ?.?.? - Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
-- Changes:
-- 3.7.31	- 2015/06/19 - KDA	- reorder input parameters (move ProviderID down) and add @DriverGroupID parameter
-- 3.9.0	- 2/15/08/14 - KDA	- return Approved column
-- 3.9.19.6	- 2015/09/30 - KDA	- add OverrideXXX fields from OrderApproval table
-- 3.9.21	- 2015/10/03 - KDA	- use [viewOrder] O.OriginDriverGroupID instead of O.DriverGroupID (which could be DestDriver.DriverGroupID)
-- 3.9.34	- 2015/12/16 - BB	- Remove deleted orders from the results (DCWEB-851)
-- 3.10.13.6 - 2015/03/01 - JAE	- add TruckTypeID
-- 3.11.3.1	- 2016/03/22 - KDA	- fix to no-op when @BatchID = 0
-- 3.11.19	- 2016/05/02 - BB	- Add Job number and Contract number
-- 3.11.20	- 2016/05/04 - JAE	- 3 month limit was always being applied, skip if batch is provided
-- 3.11.20.1 - 2016/05/04 - JAE	- Undoing change as timeouts reoccurring, need to investigate
-- 3.11.20.3 - 2016/05/11 - KDA	- optimize @BatchID filtering to query directly from tblOrderSettlementCarrier table
--								- use tblOrder and minimal JOINs intead of expensive viewOrder
-- 4.1.0	- 2016/08/08 - KDA	- use new @StartSession & @SessionID parameters to persist the retrieved records as a "Session"
--								- stop defaulting @StartDate to 3 months prior if not supplied (null parameter value provided)
--								- use simplified viewOrder_Financial_Carrier for return results - which is now again optimized for "reasonable" performance
-- 4.1.3.5	- 2016/09/17 - BB	- DCWEB-1770: Add reject filter
-- 4.1.8.6	- 2016.09.21 - KDA	- remove dynamic sql and retrieve data in 2 stages (like before) getting the orderID first, then the final data (expensive query)
--								- use new fnTrimToNull() to ensure optional string parameters are trimmed to NULL
/***********************************/
ALTER PROCEDURE spRetrieveOrdersFinancialCarrier
(
  @StartDate date = NULL -- will default to first day of 3rd prior month for efficiency sake
, @EndDate date = NULL
, @ShipperID int = -1 -- all
, @CarrierID int = -1 -- all
, @ProductGroupID int = -1 -- all 
, @TruckTypeID int = -1 -- all
, @DriverGroupID int = -1 -- all
, @OriginStateID int = -1 -- all 
, @DestStateID int = -1 -- all
, @ProducerID int = -1 -- all
, @BatchID int = NULL -- either show unbatched (NULL), or the specified batch orders
, @OnlyShipperSettled bit = 0 
, @JobNumber varchar(20) = NULL -- 3.11.19
, @ContractNumber varchar(20) = NULL -- 3.11.19
, @Rejected int = -1 -- all (rejected + not rejected) -- 4.1.3.5
, @StartSession bit = 1 -- when set to 1, assign the retrieved records to a new Session (ID), when 0 and a sessionID was assigned, then retrieve it
, @SessionID varchar(100) = NULL OUTPUT -- if @startSession = 1, then this return a new value, otherwise will retrieve the existing records in that session
) AS 
BEGIN
	IF (@StartSession IS NULL) SET @StartSession = CASE WHEN @SessionID IS NULL THEN 0 ELSE 1 END

	-- if an empty/blank string parameter is provided, trim it to NULL
	SELECT @JobNumber = dbo.fnTrimToNull(@JobNumber)
		, @ContractNumber = dbo.fnTrimToNull(@ContractNumber)

	-- validate the incoming parameters
	IF (@BatchID IS NOT NULL
		AND (@ShipperID <> -1 
			OR @CarrierID <> -1 
			OR @ProductGroupID <> -1
			OR @TruckTypeID <> -1
			OR @DriverGroupID <> -1
			OR @OriginStateID <> -1
			OR @DestStateID <> -1
			OR @ProducerID <> -1))
	BEGIN
		RAISERROR('@BatchID parameter cannot be specified with other parameters', 16, 1)
		RETURN
	END

	DECLARE @IDs IDTABLE

	-- if a @BatchID value was specified then just use this criteria alone
    IF @BatchID IS NOT NULL
	BEGIN
		INSERT INTO @IDs SELECT OrderID FROM tblOrderSettlementCarrier WHERE BatchID = @BatchID
	END
	ELSE IF (@StartSession = 0 AND @SessionID IS NOT NULL)
	BEGIN
		INSERT INTO @IDs SELECT OrderID FROM tblOrderSettlementSelectionCarrier WHERE SessionID = @SessionID
	END
	ELSE
	BEGIN
		INSERT INTO @IDs 
			SELECT O.ID
			FROM tblOrder O
			JOIN tblProduct P ON P.ID = O.ProductID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			JOIN tblDestination D ON D.ID = O.DestinationID
			JOIN tblTruck T1 ON T1.ID = O.TruckID
			LEFT JOIN dbo.tblOrderTransfer OTR ON OTR.OrderID = O.ID
			LEFT JOIN tblTruck T2 ON T2.ID = OTR.OriginTruckID
			LEFT JOIN dbo.viewDriverBase vODR ON vODR.ID = OTR.OriginDriverID
			LEFT JOIN dbo.viewDriverBase vDDR ON vDDR.ID = O.DriverID
			LEFT JOIN tblOrderSettlementCarrier OS ON OS.OrderID = O.ID
			LEFT JOIN tblOrderSettlementShipper OSP ON OSP.OrderID = O.ID
			WHERE O.StatusID IN (4)  
			  AND O.DeleteDateUTC IS NULL  -- 3.9.34
			  AND OS.BatchID IS NULL -- only get order if it's not part of a batch
			  AND (@ShipperID=-1 OR O.CustomerID=@ShipperID) 
			  AND (@CarrierID=-1 OR O.CarrierID=@CarrierID) 
			  AND (@ProductGroupID=-1 OR P.ProductGroupID=@ProductGroupID) 
			  AND (@TruckTypeID=-1 OR T1.TruckTypeID=@TruckTypeID OR T2.TruckTypeID=@TruckTypeID)
			  AND (@DriverGroupID=-1 OR vODR.DriverGroupID = @DriverGroupID OR vDDR.DriverGroupID = @DriverGroupID) 
			  AND (@ProducerID=-1 OR O.ProducerID=@ProducerID) 
			  AND (@OriginStateID=-1 OR OO.StateID=@OriginStateID) 
			  AND (@DestStateID=-1 OR D.StateID=@DestStateID) 
			  AND (@StartDate IS NULL OR O.OrderDate >= @StartDate) 
			  AND (@EndDate IS NULL OR O.OrderDate <= @EndDate)
			  AND (@OnlyShipperSettled = 0 OR OSP.BatchID IS NOT NULL)
			  AND (@JobNumber IS NULL OR O.JobNumber = @JobNumber)
			  AND (@ContractNumber IS NULL OR O.ContractNumber = @ContractNumber)
			  AND (@Rejected=-1 OR O.Rejected=@Rejected) -- 4.1.3.5
	END

	-- create the temp table to store the data
	SELECT *, RateApplySel = cast(1 as bit), BatchSel = cast(1 as bit), SessionID = cast(NULL AS varchar(100))
	INTO #ret 
	FROM viewOrder_Financial_Carrier
	WHERE 1 = 0
	
	-- get the data into the temp table
	INSERT INTO #ret
		SELECT O.*
			, OS.RateApplySel, OS.BatchSel, OS.SessionID
		FROM viewOrder_Financial_Carrier O
		LEFT JOIN tblOrderSettlementSelectionCarrier OS ON OS.OrderID = O.ID
		WHERE O.OrderID IN (SELECT ID FROM @IDs)
	
	BEGIN TRAN RetrieveOrdersFinancialCarrier

	-- do the SessionID logic
	IF (@StartSession = 1)
	BEGIN
		DELETE FROM tblOrderSettlementSelectionCarrier WHERE OrderID IN (SELECT ID FROM #ret) OR SessionID IN (SELECT SessionID FROM #ret)
		SELECT @sessionID = isnull(max(SessionID), 0) + 1 FROM tblOrderSettlementSelectionCarrier
		UPDATE #ret SET SessionID = @sessionID, RateApplySel = 1, BatchSel = CASE WHEN HasError = 1 THEN 0 ELSE 1 END
		INSERT INTO tblOrderSettlementSelectionCarrier (OrderID, SessionID, RateApplySel, BatchSel)
			SELECT ID, @sessionID, RateApplySel, BatchSel FROM #ret
	END
	
	COMMIT TRAN RetrieveOrdersFinancialCarrier

	-- return the data to the caller
	SELECT * FROM #ret
END

GO

/**********************************/
-- Created: 2013/03/09 - ?.?.? - Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
-- Changes:
-- 4.1.0.2	- 2016.08.28 - KDA	- add some PRINT instrumentation statements for performance debugging purposes
-- 4.1.3.5	- 2016/09/17 - BB	- DCWEB-1770: Add reject filter
-- 4.1.8.6	- 2016.09.21 - KDA	- remove dynamic sql and retrieve data in 2 stages (like before) getting the orderID first, then the final data (expensive query)
--								- use new fnTrimToNull() to ensure optional string parameters are trimmed to NULL
/***********************************/
ALTER PROCEDURE spRetrieveOrdersFinancialDriver
(
  @StartDate date = NULL -- will default to first day of 3rd prior month for efficiency sake
, @EndDate date = NULL
, @ShipperID int = -1 -- all
, @CarrierID int = -1 -- all
, @ProductGroupID int = -1 -- all 
, @TruckTypeID int = -1 -- all
, @DriverGroupID int = -1 -- all
, @DriverID int = -1 -- all
, @OriginStateID int = -1 -- all 
, @DestStateID int = -1 -- all
, @ProducerID int = -1 -- all
, @BatchID int = NULL -- either show unbatched (NULL), or the specified batch orders
, @OnlyCarrierSettled bit = 0 
, @JobNumber varchar(20) = NULL -- 3.11.19
, @ContractNumber varchar(20) = NULL -- 3.11.19
, @Rejected int = -1 -- all (rejected + not rejected) -- 4.1.3.5
, @StartSession bit = 1 -- when set to 1, assign the retrieved records to a new Session (ID), when 0 and a sessionID was assigned, then retrieve it
, @SessionID varchar(100) = NULL OUTPUT -- if @startSession = 1, then this return a new value, otherwise will retrieve the existing records in that session
) AS 
BEGIN
	PRINT 'Start=' + CONVERT(VARCHAR(24), GETDATE(), 121)

	IF (@StartSession IS NULL) SET @StartSession = CASE WHEN @SessionID IS NULL THEN 0 ELSE 1 END

	-- validate the incoming parameters
	IF (@BatchID IS NOT NULL
		AND (@ShipperID <> -1 
			OR @CarrierID <> -1 
			OR @ProductGroupID <> -1
			OR @TruckTypeID <> -1
			OR @DriverGroupID <> -1
			OR @DriverID <> -1
			OR @OriginStateID <> -1
			OR @DestStateID <> -1
			OR @ProducerID <> -1))
	BEGIN
		RAISERROR('@BatchID parameter cannot be specified with other parameters', 16, 1)
		RETURN
	END

	DECLARE @IDs IDTABLE

	-- if a @BatchID value was specified then just use this criteria alone
    IF @BatchID IS NOT NULL
	BEGIN
		INSERT INTO @IDs SELECT OrderID FROM tblOrderSettlementDriver WHERE BatchID = @BatchID
	END
	ELSE IF (@StartSession = 0 AND @SessionID IS NOT NULL)
	BEGIN
		INSERT INTO @IDs SELECT OrderID FROM tblOrderSettlementSelectionDriver WHERE SessionID = @SessionID
	END
	ELSE
	BEGIN
		INSERT INTO @IDs 
			SELECT O.ID
			FROM tblOrder O
			JOIN tblProduct P ON P.ID = O.ProductID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			JOIN tblDestination D ON D.ID = O.DestinationID
			JOIN tblTruck T1 ON T1.ID = O.TruckID
			LEFT JOIN dbo.tblOrderTransfer OTR ON OTR.OrderID = O.ID
			LEFT JOIN tblTruck T2 ON T2.ID = OTR.OriginTruckID
			LEFT JOIN dbo.viewDriverBase vODR ON vODR.ID = OTR.OriginDriverID
			LEFT JOIN dbo.viewDriverBase vDDR ON vDDR.ID = O.DriverID
			LEFT JOIN tblOrderSettlementDriver OS ON OS.OrderID = O.ID
			LEFT JOIN tblOrderSettlementCarrier OSP ON OSP.OrderID = O.ID
			WHERE O.StatusID IN (4)  
			  AND O.DeleteDateUTC IS NULL  -- 3.9.34
			  AND OS.BatchID IS NULL -- only get order if it's not part of a batch
			  AND (@ShipperID=-1 OR O.CustomerID=@ShipperID) 
			  AND (@CarrierID=-1 OR O.CarrierID=@CarrierID) 
			  AND (@ProductGroupID=-1 OR P.ProductGroupID=@ProductGroupID) 
			  AND (@TruckTypeID=-1 OR T1.TruckTypeID=@TruckTypeID OR T2.TruckTypeID=@TruckTypeID)
			  AND (@DriverGroupID=-1 OR vODR.DriverGroupID = @DriverGroupID OR vDDR.DriverGroupID = @DriverGroupID) 
			  AND (@DriverID=-1 OR vODR.ID = @DriverID OR vDDR.ID = @DriverID) 
			  AND (@OriginStateID=-1 OR OO.StateID=@OriginStateID) 
			  AND (@DestStateID=-1 OR D.StateID=@DestStateID) 
			  AND (@ProducerID=-1 OR O.ProducerID=@ProducerID) 
			  AND (@StartDate IS NULL OR O.OrderDate >= @StartDate) 
			  AND (@EndDate IS NULL OR O.OrderDate <= @EndDate)
			  AND (@OnlyCarrierSettled = 0 OR OSP.BatchID IS NOT NULL)
			  AND (@JobNumber IS NULL OR O.JobNumber = @JobNumber)
			  AND (@ContractNumber IS NULL OR O.ContractNumber = @ContractNumber)
			  AND (@Rejected=-1 OR O.Rejected=@Rejected)  -- 4.1.3.5
	END

	-- create the temp table to store the data
	SELECT *, RateApplySel = cast(1 as bit), BatchSel = cast(1 as bit), SessionID = cast(NULL AS varchar(100))
	INTO #ret 
	FROM viewOrder_Financial_Driver
	WHERE 1 = 0
	
	-- get the data into the temp table
	INSERT INTO #ret
		SELECT O.*
			, OS.RateApplySel, OS.BatchSel, OS.SessionID
		FROM viewOrder_Financial_Driver O
		LEFT JOIN tblOrderSettlementSelectionDriver OS ON OS.OrderID = O.ID
		WHERE O.OrderID IN (SELECT ID FROM @IDs)

	-- do the SessionID logic
	IF (@StartSession = 1)
	BEGIN
		PRINT 'SessionCreate:Start=' + CONVERT(VARCHAR(24), GETDATE(), 121)
		BEGIN TRAN RetrieveOrdersFinancialDriver

		DELETE FROM tblOrderSettlementSelectionDriver WHERE OrderID IN (SELECT ID FROM #ret) OR SessionID IN (SELECT SessionID FROM #ret)
		SELECT @sessionID = isnull(max(SessionID), 0) + 1 FROM tblOrderSettlementSelectionDriver
		UPDATE #ret SET SessionID = @sessionID, RateApplySel = 1, BatchSel = CASE WHEN HasError = 1 THEN 0 ELSE 1 END
		INSERT INTO tblOrderSettlementSelectionDriver (OrderID, SessionID, RateApplySel, BatchSel)
			SELECT ID, @sessionID, RateApplySel, BatchSel FROM #ret
	
		COMMIT TRAN RetrieveOrdersFinancialDriver
		PRINT 'SessionCreate:Finish=' + CONVERT(VARCHAR(24), GETDATE(), 121)
	END
	
	-- return the data to the caller
	SELECT * FROM #ret

	PRINT 'Finish=' + CONVERT(VARCHAR(24), GETDATE(), 121)
END

GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Shipper "Settlement" $$ values to an Delivered/Audited order
-- Changes:
-- 3.9.0	- 2015/08/13	- KDA	- add Auto-Approve logic
--										- honor Approve overrides in rating process
-- 3.9.9	- 2015/08/31	- KDA	- add new @AllowReApplyPostBatch parameter to allow re-applying after batch is applied
-- 3.9.19.2	- 2015/09/24	- KDA	- fix erroneous logic that computed Asssessorial Settlement results before the Order Level results were posted
-- 3.9.37	- 2015/12/22	- JAE	- Use tblOrderApproval to get override for Shipper Min Settlement Units
-- 3.9.39	- 2016/01/15	- JAE	- Also need to recalculate settlement units since the calculated/max values was already passed
-- 3.13.1	- 2016/07/04	- KDA	- add START/DONE PRINT statements (for debugging purposes)
-- 4.1.8.6	- 2016.09.21	- KDA	- move populate #IA table before transaction starts
--									- fix erroneous OR ActualMiles criteria (needed to be wrapped in () to avoid unnecessary processing)
--									- @debugStatements parameter + add additional START/DONE PRINT statements (for debugging purposes)
/**********************************/
ALTER PROCEDURE spApplyRatesShipper
(
  @ID int
, @UserName varchar(100)
, @OriginWaitAmount money = NULL
, @DestWaitAmount money = NULL
, @RejectionAmount money = NULL
, @FuelSurchargeAmount money = NULL
, @LoadAmount money = NULL
, @AssessorialRateTypeID_CSV varchar(max) = NULL
, @AssessorialAmount_CSV varchar(max) = null
, @ResetOverrides bit = 0
, @AllowReApplyPostBatch bit = 0
, @debugStatements bit = 0 -- turn on to get debugging statements during execution
) AS BEGIN

	SET NOCOUNT ON
	
	IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ') START ' + (CONVERT(VARCHAR(24), GETDATE(), 121))

	-- ensure this order hasn't yet been fully settled
	IF (@AllowReApplyPostBatch = 0) AND EXISTS (SELECT * FROM tblOrderSettlementShipper WHERE OrderID = @ID AND BatchID IS NOT NULL)
	BEGIN
		RAISERROR('Invoice has already been Shipper Settled', 16, 1)
		RETURN
	END

	IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ').UpdateActualMiles START ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	-- ensure the current Route.ActualMiles is assigned to the order
	UPDATE tblOrder SET ActualMiles = R.ActualMiles
	FROM tblOrder O
	JOIN tblRoute R ON R.ID = O.RouteID
	LEFT JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = O.ID
	WHERE O.ID = @ID
	  AND (O.ActualMiles IS NULL OR O.ActualMiles <> R.ActualMiles)
	  AND (@AllowReApplyPostBatch = 1 OR OSS.BatchID IS NULL)
	IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ').UpdateActualMiles DONE ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	
	/* transform the provided manual Assessorial Rates (if any) into a queryable table variable */
	DECLARE @ManualAssessorialRates TABLE (ID int primary key, Amount money)
	INSERT INTO @ManualAssessorialRates (ID, Amount) 
		SELECT ID.ID, R.Amount
		FROM (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), ID FROM dbo.fnSplitCSVIDs(@AssessorialRateTypeID_CSV)) ID
		JOIN (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), amount = CAST(value as money) FROM dbo.fnSplitCSV(@AssessorialAmount_CSV)) R ON R.sortid = ID.sortid

	/* persist all existing MANUAL Assessorial Rates from the existing Order Settlement (if any present and not resetting them) */
	INSERT INTO @ManualAssessorialRates (ID, Amount)
		SELECT AssessorialRateTypeID, Amount 
		FROM dbo.tblOrderSettlementShipperAssessorialCharge 
		WHERE OrderID = @ID AND AssessorialRateID IS NULL 
			AND ID NOT IN (SELECT ID FROM @ManualAssessorialRates)
			AND @ResetOverrides = 0

	/* persist all existing settlement amount overrides from the existing Order Settlement - if not resetting them */
	IF (@ResetOverrides = 0)
	BEGIN 
		SELECT @OriginWaitAmount = OriginWaitAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @OriginWaitAmount IS NULL AND OriginWaitRateID IS NULL
		SELECT @DestWaitAmount = DestinationWaitAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @DestWaitAmount IS NULL AND DestinationWaitRateID IS NULL
		SELECT @RejectionAmount = OrderRejectAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @RejectionAmount IS NULL AND OrderRejectRateID IS NULL
		SELECT @FuelSurchargeAmount = FuelSurchargeAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @FuelSurchargeAmount IS NULL AND FuelSurchargeRateID IS NULL
		SELECT @LoadAmount = LoadAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @LoadAmount IS NULL AND RouteRateID IS NULL AND RangeRateID IS NULL
	END

	/* compute the basic settlement units from the Order being invoices */
	DECLARE @SettlementUnits TABLE
	(
		OrderID int
	  , SettlementUomID int
	  , SettlementFactorID int
	  , ShipperSettlementFactorID int
	  , MinSettlementUnits decimal(18, 10)
	  , MinSettlementUnitsID int
	  , ActualUnits decimal(18, 10)
	  , SettlementUnits decimal(18, 10)
	)
	IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ').RetrieveSettlementUnits START ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	INSERT INTO @SettlementUnits (OrderID, SettlementUomID, SettlementFactorID, ShipperSettlementFactorID, MinSettlementUnits, MinSettlementUnitsID, ActualUnits, SettlementUnits)
		SELECT OSU.OrderID, OSU.SettlementUomID, OSU.SettlementFactorID, OSU.ShipperSettlementFactorID, ISNULL(OA.OverrideShipperMinSettlementUnits, OSU.MinSettlementUnits), OSU.MinSettlementUnitsID, OSU.ActualUnits, 
			-- use passed settlement (already calculated) or pick the max between the actual and overrride min - JAE 1/15/16 - 3.9.39
			CASE WHEN OA.OverrideShipperMinSettlementUnits IS NULL THEN OSU.SettlementUnits
						ELSE dbo.fnMaxDecimal(OSU.ActualUnits, OA.OverrideShipperMinSettlementUnits) END
		FROM dbo.viewOrderSettlementUnitsShipper OSU
		LEFT JOIN tblOrderApproval OA ON OA.OrderID = OSU.OrderID
		WHERE OSU.OrderID = @ID
	IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ').RetrieveSettlementUnits DONE ' + (CONVERT(VARCHAR(24), GETDATE(), 121))

	/* compute the order level settlement rateIDs & settlement amounts - into temp table #I */
	IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ').RetrieveData START ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	SELECT OrderID = @ID
		, OrderDate
		, ShipperSettlementFactorID, MinSettlementUnitsID
		, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
		, RouteRateID, RangeRateID, LoadAmount
		, WaitFeeParameterID
		, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount 
		, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount
		, OrderRejectRateID, OrderRejectAmount
		, FuelSurchargeRateID, FuelSurchargeRate, FuelSurchargeAmount
		, OriginTaxRate
		, TotalAmount = ISNULL(LoadAmount, 0) 
			+ ISNULL(OriginWaitAmount, 0) 
			+ ISNULL(DestWaitAmount, 0) 
			+ ISNULL(OrderRejectAmount, 0) 
			+ ISNULL(FuelSurchargeAmount, 0)
		, CreatedByUser = @UserName
		, Chainup, H2S, Rerouted /* used for the Auto-Approve logic below */
	INTO #I
	FROM (
		SELECT OrderDate
			, ShipperSettlementFactorID, MinSettlementUnitsID
			, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
			, RouteRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RouteRateID ELSE NULL END
			, RangeRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RangeRateID ELSE NULL END
			, LoadAmount = round(ISNULL(@LoadAmount, RR.Amount), 2)
			, WaitFeeParameterID
			, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount = ROUND(OriginWaitAmount, 2)
			, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount = ROUND(DestWaitAmount, 2)
			, OrderRejectRateID = CASE WHEN @RejectionAmount IS NULL THEN RD.RateID ELSE NULL END
			, OrderRejectAmount = round(isnull(@RejectionAmount, RD.Amount), 2)
			, FuelSurchargeRateID = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.RateID ELSE NULL END
			, FuelSurchargeRate = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.Rate ELSE NULL END
			, FuelSurchargeAmount = round(isnull(@FuelSurchargeAmount, FSR.Amount), 2)
			, OriginTaxRate
			, Chainup, H2S, Rerouted = cast(CASE WHEN RerouteCount = 0 THEN 0 ELSE 1 END as bit) /* used for the Auto-Approve logic below */
		FROM (
			SELECT O.ID
				, O.OriginID
				, O.CustomerID
				, O.RouteID
				, SU.ShipperSettlementFactorID
				, SU.SettlementFactorID
				, SU.SettlementUomID
				, SU.ActualUnits
				, SU.MinSettlementUnitsID
				, SU.MinSettlementUnits
				, SettlementUnits = CASE WHEN O.Rejected = 1 THEN 0 ELSE SU.SettlementUnits END
				, RouteMiles = isnull(OA.OverrideActualMiles, O.ActualMiles)
				, OrderDate = isnull(O.OrderDate, O.DueDate)
				, ChainUp = cast(CASE WHEN OA.OverrideChainup = 1 THEN 0 ELSE O.Chainup END as bit)
				, H2S = cast(CASE WHEN OA.OverrideH2S = 1 THEN 0 ELSE O.H2S END as bit)
				, O.Rejected
				, RerouteCount = CASE WHEN OA.OverrideReroute = 1 THEN 0 ELSE O.RerouteCount END
				, IsSplit = CASE WHEN O.Rejected = 0 AND O.TicketCount > 1 THEN 1 ELSE 0 END
				, WaitFeeParameterID = WFP.ID
				, OriginWaitRateID = CASE WHEN @OriginWaitAmount IS NULL THEN OWD.RateID ELSE NULL END
				, OriginWaitBillableMinutes = OWD.BillableMinutes
				, OriginWaitBillableHours = OWD.BillableHours
				, OriginWaitAmount = isnull(@OriginWaitAmount, OWD.Amount)
				, DestWaitRateID = CASE WHEN @DestWaitAmount IS NULL THEN DWD.RateID ELSE NULL END
				, DestWaitBillableMinutes = DWD.BillableMinutes
				, DestWaitBillableHours = DWD.BillableHours
				, DestWaitAmount = isnull(@DestWaitAmount, DWD.Amount)
				, OriginTaxRate = OO.TaxRate
			FROM dbo.viewOrder O
			JOIN @SettlementUnits SU ON SU.OrderID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
			OUTER APPLY dbo.fnOrderShipperOriginWaitData(@ID) OWD 
			OUTER APPLY dbo.fnOrderShipperDestinationWaitData(@ID) DWD 
			OUTER APPLY dbo.fnOrderShipperWaitFeeParameter(@ID) WFP
			WHERE O.ID = @ID
		) X
		OUTER APPLY dbo.fnOrderShipperLoadAmount(@ID, SettlementUnits, SettlementUomID) RR 
		OUTER APPLY dbo.fnOrderShipperOrderRejectData(@ID, SettlementUnits, SettlementUomID) RD
		OUTER APPLY dbo.fnOrderShipperFuelSurchargeData(@ID) FSR
	) X2
	IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ').RetrieveData DONE ' + (CONVERT(VARCHAR(24), GETDATE(), 121))

	IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ').RetrieveAssessorialData START ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	/* compute the new Assessorial Rates for this order - into temp table #IA */
	SELECT OrderID = @ID, AssessorialRateTypeID, AssessorialRateID = RateID, Amount = ROUND(Amount, 2), CreatedByUser = @UserName 
	INTO #IA
	FROM @SettlementUnits SU
	CROSS APPLY dbo.fnOrderShipperAssessorialAmounts(@ID, SU.SettlementUnits, SU.SettlementUomID) CAA
	WHERE CAA.AssessorialRateTypeID NOT IN (SELECT ID FROM @ManualAssessorialRates)
	IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ').RetrieveAssessorialData DONE ' + (CONVERT(VARCHAR(24), GETDATE(), 121))

	IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ').StoreData START ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	DECLARE @CreatedTran bit; SET @CreatedTran = CASE WHEN @@TRANCOUNT = 0 THEN 1 ELSE 0 END
	BEGIN TRY
		IF (@CreatedTran = 1)
			BEGIN TRAN

		-- remove the existing settlment record (if any)
		DELETE FROM tblOrderSettlementShipperAssessorialCharge WHERE OrderID = @ID
		DELETE FROM tblOrderSettlementShipper WHERE OrderID = @ID

		-- all Units and Rates are first normalized to the Order.OriginUOM then consistent processing
		-- and persisted in the Invoice (Settlement) record in this Order.OriginUOM 
		INSERT INTO tblOrderSettlementShipper (
/*1*/		OrderID
/*2*/		, OrderDate
/*3*/		, ShipperSettlementFactorID
/*4*/		, SettlementFactorID 
/*5*/		, SettlementUomID 
/*6*/		, MinSettlementUnitsID
/*7*/		, MinSettlementUnits 
/*8*/		, SettlementUnits
/*9*/		, RouteRateID
/*10*/		, RangeRateID 
/*11*/		, LoadAmount
/*12*/		, WaitFeeParameterID 
/*13*/		, OriginWaitRateID 
/*14*/		, OriginWaitBillableMinutes 
/*15*/		, OriginWaitBillableHours
/*16*/		, OriginWaitAmount 
/*17*/		, DestinationWaitRateID 
/*18*/		, DestinationWaitBillableMinutes 
/*19*/		, DestinationWaitBillableHours
/*20*/		, DestinationWaitAmount 
/*21*/		, OrderRejectRateID 
/*22*/		, OrderRejectAmount 
/*23*/		, FuelSurchargeRateID 
/*24*/		, FuelSurchargeRate 
/*25*/		, FuelSurchargeAmount 
/*26*/		, OriginTaxRate 
/*27*/		, TotalAmount 
/*28*/		, CreatedByUser)
		SELECT 
/*01*/		OrderID
/*02*/		, OrderDate
/*03*/		, ShipperSettlementFactorID
/*04*/		, SettlementFactorID 
/*05*/		, SettlementUomID 
/*06*/		, MinSettlementUnitsID
/*07*/		, MinSettlementUnits 
/*08*/		, SettlementUnits
/*09*/		, RouteRateID
/*10*/		, RangeRateID 
/*11*/		, LoadAmount
/*12*/		, WaitFeeParameterID 
/*13*/		, OriginWaitRateID 
/*14*/		, OriginWaitBillableMinutes 
/*15*/		, OriginWaitBillableHours
/*16*/		, OriginWaitAmount 
/*17*/		, DestWaitRateID 
/*18*/		, DestWaitBillableMinutes 
/*19*/		, DestWaitBillableHours
/*20*/		, DestWaitAmount 
/*21*/		, OrderRejectRateID 
/*22*/		, OrderRejectAmount 
/*23*/		, FuelSurchargeRateID 
/*24*/		, FuelSurchargeRate 
/*25*/		, FuelSurchargeAmount 
/*26*/		, OriginTaxRate 
/*27*/		, TotalAmount 
/*28*/		, CreatedByUser
		FROM #I

		/* apply the computed settlement assessorial rate/amount records to the DB */
		INSERT INTO tblOrderSettlementShipperAssessorialCharge (OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser)
			SELECT OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser FROM #IA
			UNION
			SELECT @ID, ID, NULL, Amount, @UserName FROM @ManualAssessorialRates

		IF (@CreatedTran = 1)
			COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @msg varchar(max), @severity int
		SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY()
		ROLLBACK
		RAISERROR(@msg, @severity, 1)
		RETURN
	END CATCH	
	IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ').StoreData DONE ' + (CONVERT(VARCHAR(24), GETDATE(), 121))

	IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ') DONE ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	
END

GO

/*************************************************/
-- Created: 2013.06.02 - Kevin Alons
-- Purpose: compute and add the various Carrier "Settlement" $$ values to an Delivered/Audited order
-- Changes:
-- 3.7.26	- 2015/06/13	- KDA	- add support for: Driver|DriverGroup, Producer to some rates, separate Carrier|Shipper MinSettlementUnits|SettlementFactor best-match values, 
-- 3.9.0	- 2015/08/13	- KDA	- honor Approve overrides in rating process
-- 3.9.9	- 2015/08/31	- KDA	- add new @AllowReApplyPostBatch parameter to allow re-applying after batch is applied
-- 3.9.19.2	- 2015/09/24	- KDA	- fix erroneous logic that computed Asssessorial Settlement results before the Order Level results were posted
-- 3.9.37	- 2015/12/22	- JAE	- Use tblOrderApproval to get override for Carrier Min Settlement Units
-- 3.9.39	- 2016/01/15	- JAE	- Also need to recalculate settlement units since the calculated/max values was already passed
-- 3.13.1	- 2016/07/04	- KDA	- add START/DONE PRINT statements (for debugging purposes)
-- 4.1.8.6	- 2016.09.21	- KDA	- move populate #IA table before transaction starts
--									- @debugStatements parameter + add additional START/DONE PRINT statements (for debugging purposes)
/*************************************************/
ALTER PROCEDURE spApplyRatesCarrier
(
  @ID int
, @UserName varchar(100)
, @OriginWaitAmount money = NULL
, @DestWaitAmount money = NULL
, @RejectionAmount money = NULL
, @FuelSurchargeAmount money = NULL
, @LoadAmount money = NULL
, @AssessorialRateTypeID_CSV varchar(max) = NULL
, @AssessorialAmount_CSV varchar(max) = null
, @ResetOverrides bit = 0
, @AllowReApplyPostBatch bit = 0
, @debugStatements bit = 0 -- turn on to get debugging statements during execution
) AS BEGIN

	SET NOCOUNT ON

	IF (@debugStatements = 1) PRINT 'spApplyRatesCarrier (' + ltrim(@ID) + ') START ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	
	IF (@AllowReApplyPostBatch = 0) AND EXISTS (SELECT * FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND BatchID IS NOT NULL)
	BEGIN
		RAISERROR('Invoice has already been Carrier Settled', 16, 1)
		RETURN
	END
	
	/* ensure that Shipper Rates have been applied prior to applying Carrier rates (since they could be dependent on Shipper rates) */
	IF NOT EXISTS (SELECT * FROM tblOrderSettlementShipper WHERE OrderID = @ID)
		EXEC spApplyRatesShipper @ID, @UserName

	/* transform the provided manual Assessorial Rates (if any) into a queryable table variable */
	DECLARE @ManualAssessorialRates TABLE (ID int primary key, Amount money)
	INSERT INTO @ManualAssessorialRates (ID, Amount) 
		SELECT ID.ID, R.Amount
		FROM (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), ID FROM dbo.fnSplitCSVIDs(@AssessorialRateTypeID_CSV)) ID
		JOIN (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), amount = CAST(value as money) FROM dbo.fnSplitCSV(@AssessorialAmount_CSV)) R ON R.sortid = ID.sortid

	/* persist all existing MANUAL Assessorial Rates from the existing Order Settlement (if any present and not resetting them) */
	INSERT INTO @ManualAssessorialRates (ID, Amount)
		SELECT AssessorialRateTypeID, Amount 
		FROM dbo.tblOrderSettlementCarrierAssessorialCharge 
		WHERE OrderID = @ID AND AssessorialRateID IS NULL 
			AND ID NOT IN (SELECT ID FROM @ManualAssessorialRates)
			AND @ResetOverrides = 0

	/* persist all existing settlement amount overrides from the existing Order Settlement - if not resetting them */
	IF (@ResetOverrides = 0)
	BEGIN 
		SELECT @OriginWaitAmount = OriginWaitAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @OriginWaitAmount IS NULL AND OriginWaitRateID IS NULL
		SELECT @DestWaitAmount = DestinationWaitAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @DestWaitAmount IS NULL AND DestinationWaitRateID IS NULL
		SELECT @RejectionAmount = OrderRejectAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @RejectionAmount IS NULL AND OrderRejectRateID IS NULL
		SELECT @FuelSurchargeAmount = FuelSurchargeAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @FuelSurchargeAmount IS NULL AND FuelSurchargeRateID IS NULL
		SELECT @LoadAmount = LoadAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @LoadAmount IS NULL AND RouteRateID IS NULL AND RangeRateID IS NULL
	END

	/* compute the basic settlement units from the Order being invoices */
	DECLARE @SettlementUnits TABLE
	(
		OrderID int
	  , SettlementUomID int
	  , SettlementFactorID int
	  , CarrierSettlementFactorID int
	  , MinSettlementUnits decimal(18, 10)
	  , MinSettlementUnitsID int
	  , ActualUnits decimal(18, 10)
	  , SettlementUnits decimal(18, 10) -- calculate value basically max(actual, minimum)
	)
	INSERT INTO @SettlementUnits (OrderID, SettlementUomID, SettlementFactorID, CarrierSettlementFactorID, MinSettlementUnits, MinSettlementUnitsID, ActualUnits, SettlementUnits)
		SELECT OSU.OrderID, OSU.SettlementUomID, OSU.SettlementFactorID, OSU.CarrierSettlementFactorID, ISNULL(OA.OverrideCarrierMinSettlementUnits, OSU.MinSettlementUnits), OSU.MinSettlementUnitsID, OSU.ActualUnits, 
			-- use passed settlement (already calculated) or pick the max between the actual and overrride min - JAE 1/15/16 - 3.9.39
			CASE WHEN OA.OverrideCarrierMinSettlementUnits IS NULL THEN OSU.SettlementUnits
						ELSE dbo.fnMaxDecimal(OSU.ActualUnits, OA.OverrideCarrierMinSettlementUnits) END
		FROM dbo.viewOrderSettlementUnitsCarrier OSU
		LEFT JOIN tblOrderApproval OA ON OA.OrderID = OSU.OrderID
		WHERE OSU.OrderID = @ID

	/* compute the order level settlement rateIDs & settlement amounts - into temp table #I */
	SELECT OrderID = @ID
		, OrderDate
		, CarrierSettlementFactorID, MinSettlementUnitsID
		, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
		, RouteRateID, RangeRateID, LoadAmount
		, WaitFeeParameterID
		, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount 
		, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount
		, OrderRejectRateID, OrderRejectAmount
		, FuelSurchargeRateID, FuelSurchargeRate, FuelSurchargeAmount
		, OriginTaxRate
		, TotalAmount = ISNULL(LoadAmount, 0) 
			+ ISNULL(OriginWaitAmount, 0) 
			+ ISNULL(DestWaitAmount, 0) 
			+ ISNULL(OrderRejectAmount, 0) 
			+ ISNULL(FuelSurchargeAmount, 0)
		, CreatedByUser = @UserName
		, ChainUp, H2S, Rerouted
	INTO #I
	FROM (
		SELECT OrderDate
			, CarrierSettlementFactorID, MinSettlementUnitsID
			, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
			, RouteRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RouteRateID ELSE NULL END
			, RangeRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RangeRateID ELSE NULL END
			, LoadAmount = round(ISNULL(@LoadAmount, RR.Amount), 2)
			, WaitFeeParameterID
			, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount = ROUND(OriginWaitAmount, 2)
			, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount = ROUND(DestWaitAmount, 2)
			, OrderRejectRateID = CASE WHEN @RejectionAmount IS NULL THEN RD.RateID ELSE NULL END
			, OrderRejectAmount = round(isnull(@RejectionAmount, RD.Amount), 2)
			, FuelSurchargeRateID = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.RateID ELSE NULL END
			, FuelSurchargeRate = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.Rate ELSE NULL END
			, FuelSurchargeAmount = round(isnull(@FuelSurchargeAmount, FSR.Amount), 2)
			, OriginTaxRate
			, Chainup, H2S, Rerouted = cast(CASE WHEN RerouteCount = 0 THEN 0 ELSE 1 END as bit) /* used for the Auto-Approve logic below */
		FROM (
			SELECT O.ID
				, O.OriginID
				, O.CarrierID
				, O.RouteID
				, SU.CarrierSettlementFactorID
				, SU.SettlementFactorID
				, SU.SettlementUomID
				, SU.ActualUnits
				, SU.MinSettlementUnitsID
				, SU.MinSettlementUnits
				, SettlementUnits = CASE WHEN O.Rejected = 1 THEN 0 ELSE SU.SettlementUnits END
				, RouteMiles = isnull(OA.OverrideActualMiles, O.ActualMiles)
				, OrderDate = isnull(O.OrderDate, O.DueDate)
				, ChainUp = cast(CASE WHEN OA.OverrideChainup = 1 THEN 0 ELSE O.Chainup END as bit)
				, H2S = cast(CASE WHEN OA.OverrideH2S = 1 THEN 0 ELSE O.H2S END as bit)
				, O.Rejected
				, RerouteCount = CASE WHEN OA.OverrideReroute = 1 THEN 0 ELSE O.RerouteCount END
				, IsSplit = CASE WHEN O.Rejected = 0 AND O.TicketCount > 1 THEN 1 ELSE 0 END
				, WaitFeeParameterID = WFP.ID
				, OriginWaitRateID = CASE WHEN @OriginWaitAmount IS NULL THEN OWD.RateID ELSE NULL END
				, OriginWaitBillableMinutes = OWD.BillableMinutes
				, OriginWaitBillableHours = OWD.BillableHours
				, OriginWaitAmount = isnull(@OriginWaitAmount, OWD.Amount)
				, DestWaitRateID = CASE WHEN @DestWaitAmount IS NULL THEN DWD.RateID ELSE NULL END
				, DestWaitBillableMinutes = DWD.BillableMinutes
				, DestWaitBillableHours = DWD.BillableHours
				, DestWaitAmount = isnull(@DestWaitAmount, DWD.Amount)
				, OriginTaxRate = OO.TaxRate
			FROM dbo.viewOrder O
			JOIN @SettlementUnits SU ON SU.OrderID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
			OUTER APPLY dbo.fnOrderCarrierOriginWaitData(@ID) OWD 
			OUTER APPLY dbo.fnOrderCarrierDestinationWaitData(@ID) DWD 
			OUTER APPLY dbo.fnOrderCarrierWaitFeeParameter(@ID) WFP
			WHERE O.ID = @ID
		) X
		OUTER APPLY dbo.fnOrderCarrierLoadAmount(@ID, SettlementUnits, SettlementUomID) RR 
		OUTER APPLY dbo.fnOrderCarrierOrderRejectData(@ID, SettlementUnits, SettlementUomID) RD
		OUTER APPLY dbo.fnOrderCarrierFuelSurchargeData(@ID) FSR
	) X2

	/* compute the new Assessorial Rates for this order - into temp table #IA */
	SELECT OrderID = @ID, AssessorialRateTypeID, AssessorialRateID = RateID, Amount = ROUND(Amount, 2), CreatedByUser = @UserName 
	INTO #IA
	FROM @SettlementUnits SU
	CROSS APPLY dbo.fnOrderCarrierAssessorialAmounts(@ID, SU.SettlementUnits, SU.SettlementUomID) CAA
	WHERE CAA.AssessorialRateTypeID NOT IN (SELECT ID FROM @ManualAssessorialRates)

	DECLARE @CreatedTran bit; SET @CreatedTran = CASE WHEN @@TRANCOUNT = 0 THEN 1 ELSE 0 END
	BEGIN TRY
		IF (@CreatedTran = 1)
			BEGIN TRAN

		-- remove the existing settlment record (if any)
		DELETE FROM tblOrderSettlementCarrierAssessorialCharge WHERE OrderID = @ID
		DELETE FROM tblOrderSettlementCarrier WHERE OrderID = @ID
	
		-- all Units and Rates are first normalized to the Order.OriginUOM then consistent processing
		-- and persisted in the Invoice (Settlement) record in this Order.OriginUOM 
		INSERT INTO tblOrderSettlementCarrier 
			(OrderID
			, OrderDate
			, CarrierSettlementFactorID
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnitsID
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestinationWaitRateID 
			, DestinationWaitBillableMinutes 
			, DestinationWaitBillableHours
			, DestinationWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser)
		SELECT OrderID
			, OrderDate
			, CarrierSettlementFactorID
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnitsID
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestWaitRateID 
			, DestWaitBillableMinutes 
			, DestWaitBillableHours
			, DestWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser
		FROM #I

		/* apply the computed settlement assessorial rate/amount records to the DB */
		INSERT INTO tblOrderSettlementCarrierAssessorialCharge (OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser)
			SELECT OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser FROM #IA
			UNION
			SELECT @ID, ID, NULL, Amount, @UserName FROM @ManualAssessorialRates

		IF (@CreatedTran = 1)
			COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @msg varchar(max), @severity int
		SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY()
		ROLLBACK
		RAISERROR(@msg, @severity, 1)
		RETURN
	END CATCH	

	IF (@debugStatements = 1) PRINT 'spApplyRatesCarrier (' + ltrim(@ID) + ') DONE ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	
END

GO

/*************************************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: compute and add the various Driver "Settlement" $$ values to an Delivered/Audited order
-- Changes:
-- 4.1.8.6	- 2016.09.21	- KDA	- move populate #IA table before transaction starts
--									- @debugStatements parameter + add additional START/DONE PRINT statements (for debugging purposes)
/*************************************************/
ALTER PROCEDURE spApplyRatesDriver
(
  @ID int
, @UserName varchar(100)
, @OriginWaitAmount money = NULL
, @DestWaitAmount money = NULL
, @RejectionAmount money = NULL
, @FuelSurchargeAmount money = NULL
, @LoadAmount money = NULL
, @AssessorialRateTypeID_CSV varchar(max) = NULL
, @AssessorialAmount_CSV varchar(max) = null
, @ResetOverrides bit = 0
, @AllowReApplyPostBatch bit = 0
, @debugStatements bit = 0 -- turn on to get debugging statements during execution
) AS BEGIN

	SET NOCOUNT ON

	IF (@debugStatements = 1) PRINT 'spApplyRatesDriver (' + ltrim(@ID) + ') START ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	
	IF (@AllowReApplyPostBatch = 0) AND EXISTS (SELECT * FROM tblOrderSettlementDriver WHERE OrderID = @ID AND BatchID IS NOT NULL)
	BEGIN
		RAISERROR('Invoice has already been Driver Settled', 16, 1)
		RETURN
	END
	
	/* ensure that Carrier Rates have been applied prior to applying Driver rates (since they could be dependent on Carrier rates) */
	IF NOT EXISTS (SELECT * FROM tblOrderSettlementCarrier WHERE OrderID = @ID)
		EXEC spApplyRatesCarrier @ID, @UserName

	/* transform the provided manual Assessorial Rates (if any) into a queryable table variable */
	DECLARE @ManualAssessorialRates TABLE (ID int primary key, Amount money)
	INSERT INTO @ManualAssessorialRates (ID, Amount) 
		SELECT ID.ID, R.Amount
		FROM (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), ID FROM dbo.fnSplitCSVIDs(@AssessorialRateTypeID_CSV)) ID
		JOIN (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), amount = CAST(value as money) FROM dbo.fnSplitCSV(@AssessorialAmount_CSV)) R ON R.sortid = ID.sortid

	/* persist all existing MANUAL Assessorial Rates from the existing Order Settlement (if any present and not resetting them) */
	INSERT INTO @ManualAssessorialRates (ID, Amount)
		SELECT AssessorialRateTypeID, Amount 
		FROM dbo.tblOrderSettlementDriverAssessorialCharge 
		WHERE OrderID = @ID AND AssessorialRateID IS NULL 
			AND ID NOT IN (SELECT ID FROM @ManualAssessorialRates)
			AND @ResetOverrides = 0

	/* persist all existing settlement amount overrides from the existing Order Settlement - if not resetting them */
	IF (@ResetOverrides = 0)
	BEGIN 
		SELECT @OriginWaitAmount = OriginWaitAmount FROM tblOrderSettlementDriver WHERE OrderID = @ID AND @OriginWaitAmount IS NULL AND OriginWaitRateID IS NULL
		SELECT @DestWaitAmount = DestinationWaitAmount FROM tblOrderSettlementDriver WHERE OrderID = @ID AND @DestWaitAmount IS NULL AND DestinationWaitRateID IS NULL
		SELECT @RejectionAmount = OrderRejectAmount FROM tblOrderSettlementDriver WHERE OrderID = @ID AND @RejectionAmount IS NULL AND OrderRejectRateID IS NULL
		SELECT @FuelSurchargeAmount = FuelSurchargeAmount FROM tblOrderSettlementDriver WHERE OrderID = @ID AND @FuelSurchargeAmount IS NULL AND FuelSurchargeRateID IS NULL
		SELECT @LoadAmount = LoadAmount FROM tblOrderSettlementDriver WHERE OrderID = @ID AND @LoadAmount IS NULL AND RouteRateID IS NULL AND RangeRateID IS NULL
	END

	/* compute the basic settlement units from the Order being invoices */
	DECLARE @SettlementUnits TABLE
	(
		OrderID int
	  , SettlementUomID int
	  , SettlementFactorID int
	  , CarrierSettlementFactorID int
	  , MinSettlementUnits decimal(18, 10)
	  , MinSettlementUnitsID int
	  , ActualUnits decimal(18, 10)
	  , SettlementUnits decimal(18, 10) -- calculate value basically max(actual, minimum)
	)
	INSERT INTO @SettlementUnits (OrderID, SettlementUomID, SettlementFactorID, CarrierSettlementFactorID, MinSettlementUnits, MinSettlementUnitsID, ActualUnits, SettlementUnits)
		SELECT OSU.OrderID, OSU.SettlementUomID, OSU.SettlementFactorID, OSU.CarrierSettlementFactorID, ISNULL(OA.OverrideCarrierMinSettlementUnits, OSU.MinSettlementUnits), OSU.MinSettlementUnitsID, OSU.ActualUnits
			-- use passed settlement (already calculated) or pick the max between the actual and overrride min - JAE 1/15/16 - 3.9.39
			, CASE WHEN OA.OverrideCarrierMinSettlementUnits IS NULL THEN OSU.SettlementUnits
					ELSE dbo.fnMaxDecimal(OSU.ActualUnits, OA.OverrideCarrierMinSettlementUnits) END
		FROM dbo.viewOrderSettlementUnitsCarrier OSU
		LEFT JOIN tblOrderApproval OA ON OA.OrderID = OSU.OrderID
		WHERE OSU.OrderID = @ID

	/* compute the order level settlement rateIDs & settlement amounts - into temp table #I */
	SELECT OrderID = @ID
		, OrderDate
		, CarrierSettlementFactorID, MinSettlementUnitsID
		, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
		, RouteRateID, RangeRateID, LoadAmount
		, WaitFeeParameterID
		, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount 
		, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount
		, OrderRejectRateID, OrderRejectAmount
		, FuelSurchargeRateID, FuelSurchargeRate, FuelSurchargeAmount
		, OriginTaxRate
		, TotalAmount = ISNULL(LoadAmount, 0) 
			+ ISNULL(OriginWaitAmount, 0) 
			+ ISNULL(DestWaitAmount, 0) 
			+ ISNULL(OrderRejectAmount, 0) 
			+ ISNULL(FuelSurchargeAmount, 0)
		, CreatedByUser = @UserName
		, ChainUp, H2S, Rerouted
	INTO #I
	FROM (
		SELECT OrderDate
			, CarrierSettlementFactorID, MinSettlementUnitsID
			, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
			, RouteRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RouteRateID ELSE NULL END
			, RangeRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RangeRateID ELSE NULL END
			, LoadAmount = round(ISNULL(@LoadAmount, RR.Amount), 2)
			, WaitFeeParameterID
			, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount = ROUND(OriginWaitAmount, 2)
			, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount = ROUND(DestWaitAmount, 2)
			, OrderRejectRateID = CASE WHEN @RejectionAmount IS NULL THEN RD.RateID ELSE NULL END
			, OrderRejectAmount = round(isnull(@RejectionAmount, RD.Amount), 2)
			, FuelSurchargeRateID = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.RateID ELSE NULL END
			, FuelSurchargeRate = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.Rate ELSE NULL END
			, FuelSurchargeAmount = round(isnull(@FuelSurchargeAmount, FSR.Amount), 2)
			, OriginTaxRate
			, Chainup, H2S, Rerouted = cast(CASE WHEN RerouteCount = 0 THEN 0 ELSE 1 END as bit) /* used for the Auto-Approve logic below */
		FROM (
			SELECT O.ID
				, O.OriginID
				, O.CarrierID
				, O.RouteID
				, SU.CarrierSettlementFactorID
				, SU.SettlementFactorID
				, SU.SettlementUomID
				, SU.ActualUnits
				, SU.MinSettlementUnitsID
				, SU.MinSettlementUnits
				, SettlementUnits = CASE WHEN O.Rejected = 1 THEN 0 ELSE SU.SettlementUnits END
				, RouteMiles = isnull(OA.OverrideActualMiles, O.ActualMiles)
				, OrderDate = isnull(O.OrderDate, O.DueDate)
				, ChainUp = cast(CASE WHEN OA.OverrideChainup = 1 THEN 0 ELSE O.Chainup END as bit)
				, H2S = cast(CASE WHEN OA.OverrideH2S = 1 THEN 0 ELSE O.H2S END as bit)
				, O.Rejected
				, RerouteCount = CASE WHEN OA.OverrideReroute = 1 THEN 0 ELSE O.RerouteCount END
				, IsSplit = CASE WHEN O.Rejected = 0 AND O.TicketCount > 1 THEN 1 ELSE 0 END
				, WaitFeeParameterID = WFP.ID
				, OriginWaitRateID = CASE WHEN @OriginWaitAmount IS NULL THEN OWD.RateID ELSE NULL END
				, OriginWaitBillableMinutes = OWD.BillableMinutes
				, OriginWaitBillableHours = OWD.BillableHours
				, OriginWaitAmount = isnull(@OriginWaitAmount, OWD.Amount)
				, DestWaitRateID = CASE WHEN @DestWaitAmount IS NULL THEN DWD.RateID ELSE NULL END
				, DestWaitBillableMinutes = DWD.BillableMinutes
				, DestWaitBillableHours = DWD.BillableHours
				, DestWaitAmount = isnull(@DestWaitAmount, DWD.Amount)
				, OriginTaxRate = OO.TaxRate
			FROM dbo.viewOrder O
			JOIN @SettlementUnits SU ON SU.OrderID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
			OUTER APPLY dbo.fnOrderDriverOriginWaitData(@ID) OWD 
			OUTER APPLY dbo.fnOrderDriverDestinationWaitData(@ID) DWD 
			OUTER APPLY dbo.fnOrderCarrierWaitFeeParameter(@ID) WFP
			WHERE O.ID = @ID
		) X
		OUTER APPLY dbo.fnOrderDriverLoadAmount(@ID, SettlementUnits, SettlementUomID) RR 
		OUTER APPLY dbo.fnOrderDriverOrderRejectData(@ID, SettlementUnits, SettlementUomID) RD
		OUTER APPLY dbo.fnOrderDriverFuelSurchargeData(@ID) FSR
	) X2

	/* compute the new Assessorial Rates for this order - into temp table #IA */
	SELECT OrderID = @ID, AssessorialRateTypeID, AssessorialRateID = RateID, Amount = ROUND(Amount, 2), CreatedByUser = @UserName 
	INTO #IA
	FROM @SettlementUnits SU
	CROSS APPLY dbo.fnOrderDriverAssessorialAmounts(@ID, SU.SettlementUnits, SU.SettlementUomID) AA
	WHERE AA.AssessorialRateTypeID NOT IN (SELECT ID FROM @ManualAssessorialRates)

	DECLARE @CreatedTran bit; SET @CreatedTran = CASE WHEN @@TRANCOUNT = 0 THEN 1 ELSE 0 END
	BEGIN TRY
		IF (@CreatedTran = 1)
			BEGIN TRAN

		-- remove the existing settlment record (if any)
		DELETE FROM tblOrderSettlementDriverAssessorialCharge WHERE OrderID = @ID
		DELETE FROM tblOrderSettlementDriver WHERE OrderID = @ID
	
		-- all Units and Rates are first normalized to the Order.OriginUOM then consistent processing
		-- and persisted in the Invoice (Settlement) record in this Order.OriginUOM 
		INSERT INTO tblOrderSettlementDriver 
			(OrderID
			, OrderDate
			, CarrierSettlementFactorID
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnitsID
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestinationWaitRateID 
			, DestinationWaitBillableMinutes 
			, DestinationWaitBillableHours
			, DestinationWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser)
		SELECT OrderID
			, OrderDate
			, CarrierSettlementFactorID
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnitsID
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestWaitRateID 
			, DestWaitBillableMinutes 
			, DestWaitBillableHours
			, DestWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser
		FROM #I

		/* apply the computed settlement assessorial rate/amount records to the DB */
		INSERT
		 INTO tblOrderSettlementDriverAssessorialCharge (OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser)
			SELECT OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser FROM #IA
			UNION
			SELECT @ID, ID, NULL, Amount, @UserName FROM @ManualAssessorialRates

		IF (@CreatedTran = 1)
			COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @msg varchar(max), @severity int
		SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY()
		ROLLBACK
		RAISERROR(@msg, @severity, 1)
		RETURN
	END CATCH	

	IF (@debugStatements = 1) PRINT 'spApplyRatesDriver (' + ltrim(@ID) + ') DONE ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	
END

GO

/*************************************************
-- Created: 4.1.0 - 2016/08/21 - Kevin Alons
-- Purpose: apply rates to the specified [Carrier] Financial Session
-- Changes:
-- 4.1.8.6	- 2016.09.24 - KDA	- add @ApplyCount OUTPUT int parameter
*************************************************/
ALTER PROCEDURE spApplyRatesCarrierSession
(
  @SessionID varchar(100)
, @UserName varchar(100)
, @ResetOverrides bit = 0
, @ApplyCount int = 0 OUTPUT
) AS BEGIN

	SET NOCOUNT ON

	DECLARE @IDs IDTABLE
	INSERT INTO @IDs SELECT OrderID FROM tblOrderSettlementSelectionCarrier WHERE SessionID = @SessionID AND RateApplySel = 1
	SELECT @ApplyCount = count(*) FROM @IDs

	DECLARE @id int = 0
	WHILE EXISTS (SELECT * FROM @IDs)
	BEGIN
		SELECT @id = min(ID) FROM @IDs
		EXEC spApplyRatesCarrier @ID = @id, @UserName = @UserName, @ResetOverrides = @ResetOverrides
		DELETE FROM @IDs WHERE ID = @id
	END

END

GO

/*************************************************/
-- Created: 4.1.0 - 2016/08/21 - Kevin Alons
-- Purpose: apply rates to the specified [Driver] Financial Session
-- Changes:
-- 4.1.8.6	- 2016.09.24 - KDA	- add @ApplyCount OUTPUT int parameter
/*************************************************/
ALTER PROCEDURE spApplyRatesDriverSession
(
  @SessionID varchar(100)
, @UserName varchar(100)
, @ResetOverrides bit = 0
, @ApplyCount int = 0 OUTPUT
) AS BEGIN

	SET NOCOUNT ON

	DECLARE @IDs IDTABLE
	INSERT INTO @IDs SELECT OrderID FROM tblOrderSettlementSelectionDriver WHERE SessionID = @SessionID AND RateApplySel = 1
	SELECT @ApplyCount = count(*) FROM @IDs

	DECLARE @id int = 0
	WHILE EXISTS (SELECT * FROM @IDs)
	BEGIN
		SELECT @id = min(ID) FROM @IDs
		EXEC spApplyRatesDriver @ID = @id, @UserName = @UserName, @ResetOverrides = @ResetOverrides
		DELETE FROM @IDs WHERE ID = @id
	END

END

GO

/*************************************************
-- Created: 4.1.0 - 2016/08/21 - Kevin Alons
-- Purpose: apply rates to the specified [Shipper] Financial Session
-- Changes:
-- 4.1.8.6	- 2016.09.24 - KDA	- add @ApplyCount OUTPUT int parameter
*************************************************/
ALTER PROCEDURE spApplyRatesShipperSession
(
  @SessionID varchar(100)
, @UserName varchar(100)
, @ResetOverrides bit = 0
, @ApplyCount int = 0 OUTPUT
) AS BEGIN

	SET NOCOUNT ON

	DECLARE @IDs IDTABLE
	INSERT INTO @IDs SELECT OrderID FROM tblOrderSettlementSelectionShipper WHERE SessionID = @SessionID AND RateApplySel = 1
	SELECT @ApplyCount = count(*) FROM @IDs

	DECLARE @id int = 0
	WHILE EXISTS (SELECT * FROM @IDs)
	BEGIN
		SELECT @id = min(ID) FROM @IDs
		EXEC spApplyRatesShipper @ID = @id, @UserName = @UserName, @ResetOverrides = @ResetOverrides
		DELETE FROM @IDs WHERE ID = @id
	END

END

GO

/***********************************/
-- Created: ?.?.? - 2013/06/02 - Kevin Alons
-- Purpose: compute and add the various Carrier "Settlement" $$ values to an Audited order
-- Changes:
-- 4.1.0	- 2016/08/21 - KDA	- use new @SessionID parameter to create the entire settlement record in a single invocation
-- 4.1.8.6	- 2016.09.24 - KDA	- optimize data retrieval into 2 separate steps (MAJOR performance improvement workaround)
/***********************************/
ALTER PROCEDURE spCreateCarrierSettlementBatch
(
  @SessionID varchar(100)
, @CarrierID int
, @InvoiceNum varchar(50)
, @Notes varchar(255)
, @UserName varchar(255)
, @BatchID int = NULL output
, @BatchNum int = NULL output
, @Outcome varchar(25) = 'Success' output
, @Msg varchar(255) = NULL output
) AS BEGIN
	-- retrieve the basic session order data first
	SELECT OS.OrderID, IsRated = cast(0 as bit), HasError = cast(0 as bit) 
	INTO #data
	FROM tblOrderSettlementSelectionCarrier OS 
	WHERE OS.SessionID = @SessionID AND OS.BatchSel = 1
	-- add the Financial information next (done in 2 steps for MAJOR performance reasons - a workaround but a more intuitive way was not identified)
	UPDATE #data
	  SET IsRated = cast(CASE WHEN OFB.InvoiceRatesAppliedDateUTC IS NULL THEN 0 ELSE 1 END as bit), HasError = OFB.HasError
	FROM #data O
	JOIN (
		SELECT OrderID, InvoiceRatesAppliedDateUTC, HasError 
		FROM viewOrder_Financial_Base_Carrier 
		WHERE OrderID IN (SELECT OrderID FROM #data)
	) OFB ON OFB.OrderID = O.OrderID

	-- validate the data next
	IF NOT EXISTS (SELECT * FROM #data)
	BEGIN
		SELECT @Outcome = 'Error', @Msg = 'At least order must be selected to complete settlement!'
		RETURN
	END
	ELSE IF EXISTS (SELECT * FROM #data WHERE IsRated = 0 OR HasError <> 0)
	BEGIN
		SELECT @Outcome = 'Error', @Msg = 'All selected orders must be fully rated with no errors to complete Settlement!'
		RETURN
	END

	-- if validation passed, complete the settlement process below
	SET XACT_ABORT ON
	BEGIN TRAN CarrierSettlement

	INSERT INTO dbo.tblCarrierSettlementBatch(CarrierID, InvoiceNum, BatchNum, BatchDate, Notes, CreateDateUTC, CreatedByUser)
		SELECT @CarrierID
			, @InvoiceNum
			, isnull(max(BatchNum), 0) + 1
			, dbo.fnDateOnly(getdate()), @Notes, getutcdate(), @UserName 
		FROM dbo.tblCarrierSettlementBatch
	SELECT @BatchID = scope_identity()
	SELECT @BatchNum = BatchNum FROM dbo.tblCarrierSettlementBatch WHERE ID = @BatchID

	UPDATE tblOrderSettlementCarrier 
		SET BatchID = @BatchID
	FROM tblOrderSettlementCarrier OSC
	JOIN #data D ON D.OrderID = OSC.OrderID

	SELECT @Outcome = 'Success', @Msg = 'Settlement process complete: ' + ltrim(count(1)) + ' record(s) were added to batch: ' + ltrim(@BatchNum)
	FROM #data

	DELETE FROM tblOrderSettlementSelectionCarrier WHERE SessionID = @SessionID AND BatchSel = 1

	COMMIT TRAN
END

GO

/***********************************/
-- Created: ?.?.? - 2013/06/02 - Kevin Alons
-- Purpose: compute and add the various Driver "Settlement" $$ values to an Audited order
-- Changes:
-- 4.1.8.6	- 2016.09.24 - KDA	- optimize data retrieval into 2 separate steps (MAJOR performance improvement workaround)
/***********************************/
ALTER PROCEDURE spCreateDriverSettlementBatch
(
  @SessionID varchar(100)
, @CarrierID int
, @DriverGroupID int = NULL
, @DriverID int = NULL
, @InvoiceNum varchar(50) = NULL
, @Notes varchar(255) = NULL
, @UserName varchar(255) 
, @BatchID int = NULL output
, @BatchNum int = NULL output
, @Outcome varchar(25) = 'Success' output
, @Msg varchar(255) = NULL output
) AS BEGIN
	IF (@DriverGroupID = 0) SET @DriverGroupID = NULL
	IF (@DriverID = 0) SET @DriverID = NULL

	PRINT 'spCreateDriverSettlementBatch(' + ltrim(@SessionID) + ').Retrieve START ' + (CONVERT(VARCHAR(24), GETDATE(), 121))

	-- retrieve the basic session order data first
	SELECT OS.OrderID, IsRated = cast(0 as bit), HasError = cast(0 as bit) 
	INTO #data
	FROM tblOrderSettlementSelectionDriver OS 
	WHERE OS.SessionID = @SessionID AND OS.BatchSel = 1
	-- add the Financial information next (done in 2 steps for MAJOR performance reasons - a workaround but a more intuitive way was not identified)
	UPDATE #data
	  SET IsRated = cast(CASE WHEN OFB.InvoiceRatesAppliedDateUTC IS NULL THEN 0 ELSE 1 END as bit), HasError = OFB.HasError
	FROM #data O
	JOIN (
		SELECT OrderID, InvoiceRatesAppliedDateUTC, HasError 
		FROM viewOrder_Financial_Base_Driver 
		WHERE OrderID IN (SELECT OrderID FROM #data)
	) OFB ON OFB.OrderID = O.OrderID

	PRINT 'spCreateDriverSettlementBatch(' + ltrim(@SessionID) + ').Retrieve DONE ' + (CONVERT(VARCHAR(24), GETDATE(), 121))

	-- validate the data next
	IF NOT EXISTS (SELECT * FROM #data)
	BEGIN
		SELECT @Outcome = 'Error', @Msg = 'At least 1 order must be selected to complete settlement!'
		RETURN
	END
	ELSE IF EXISTS (SELECT * FROM #data WHERE IsRated = 0 OR HasError <> 0)
	BEGIN
		SELECT @Outcome = 'Error', @Msg = 'All selected orders must be fully rated with no errors to complete Settlement!'
		RETURN
	END

	-- if validation passed, complete the settlement process below
	SET XACT_ABORT ON
	BEGIN TRAN DriverSettlement

	INSERT INTO dbo.tblDriverSettlementBatch(CarrierID, DriverGroupID, DriverID, InvoiceNum, BatchNum, BatchDate, Notes, CreateDateUTC, CreatedByUser)
		SELECT @CarrierID, @DriverGroupID, @DriverID
			, @InvoiceNum
			, isnull(max(BatchNum), 0) + 1
			, dbo.fnDateOnly(getdate()), @Notes, getutcdate(), @UserName 
		FROM dbo.tblDriverSettlementBatch
	SELECT @BatchID = scope_identity()
	SELECT @BatchNum = BatchNum FROM dbo.tblDriverSettlementBatch WHERE ID = @BatchID

	UPDATE tblOrderSettlementDriver 
		SET BatchID = @BatchID
	FROM tblOrderSettlementDriver OSC
	JOIN #data D ON D.OrderID = OSC.OrderID

	SELECT @Outcome = 'Success', @Msg = 'Settlement process complete: ' + ltrim(count(1)) + ' record(s) were added to batch: ' + ltrim(@BatchNum)
	FROM #data

	DELETE FROM tblOrderSettlementSelectionDriver WHERE SessionID = @SessionID AND BatchSel = 1

	COMMIT TRAN
END

GO

/***********************************/
-- Created: ?.?.? - 2013/06/02 - Kevin Alons
-- Purpose: compute and add the various Shipper "Settlement" $$ values to an Audited order
-- Changes:
-- 4.1.0 - 2016/08/21 - KDA - use new @SessionID parameter to create the entire settlement record in a single invocation
-- 4.1.8.6	- 2016.09.24 - KDA	- optimize data retrieval into 2 separate steps (MAJOR performance improvement workaround)
/***********************************/
ALTER PROCEDURE spCreateShipperSettlementBatch
(
  @SessionID varchar(100)
, @ShipperID int
, @InvoiceNum varchar(50)
, @Notes varchar(255)
, @UserName varchar(255)
, @BatchID int = NULL output
, @BatchNum int = NULL output
, @Outcome varchar(25) = 'Success' output
, @Msg varchar(255) = NULL output
) AS BEGIN
	-- retrieve the basic session order data first
	SELECT OS.OrderID, IsRated = cast(0 as bit), HasError = cast(0 as bit) 
	INTO #data
	FROM tblOrderSettlementSelectionShipper OS 
	WHERE OS.SessionID = @SessionID AND OS.BatchSel = 1
	-- add the Financial information next (done in 2 steps for MAJOR performance reasons - a workaround but a more intuitive way was not identified)
	UPDATE #data
	  SET IsRated = cast(CASE WHEN OFB.InvoiceRatesAppliedDateUTC IS NULL THEN 0 ELSE 1 END as bit), HasError = OFB.HasError
	FROM #data O
	JOIN (
		SELECT OrderID, InvoiceRatesAppliedDateUTC, HasError 
		FROM viewOrder_Financial_Base_Shipper 
		WHERE OrderID IN (SELECT OrderID FROM #data)
	) OFB ON OFB.OrderID = O.OrderID

	-- validate the data next
	IF NOT EXISTS (SELECT * FROM #data)
	BEGIN
		SELECT @Outcome = 'Error', @Msg = 'At least order must be selected to complete settlement!'
		RETURN
	END
	ELSE IF EXISTS (SELECT * FROM #data WHERE IsRated = 0 OR HasError <> 0)
	BEGIN
		SELECT @Outcome = 'Error', @Msg = 'All selected orders must be fully rated with no errors to complete Settlement!'
		RETURN
	END

	-- if validation passed, complete the settlement process below
	SET XACT_ABORT ON
	BEGIN TRAN ShipperSettlement

	INSERT INTO dbo.tblShipperSettlementBatch(ShipperID, InvoiceNum, BatchNum, BatchDate, Notes, CreateDateUTC, CreatedByUser)
		SELECT @ShipperID
			, @InvoiceNum
			, isnull(max(BatchNum), 0) + 1
			, dbo.fnDateOnly(getdate()), @Notes, getutcdate(), @UserName 
		FROM dbo.tblShipperSettlementBatch
	SELECT @BatchID = scope_identity()
	SELECT @BatchNum = BatchNum FROM dbo.tblShipperSettlementBatch WHERE ID = @BatchID

	UPDATE tblOrderSettlementShipper 
		SET BatchID = @BatchID
	FROM tblOrderSettlementShipper OSC
	JOIN #data D ON D.OrderID = OSC.OrderID

	SELECT @Outcome = 'Success', @Msg = 'Settlement process complete: ' + ltrim(count(1)) + ' record(s) were added to batch: ' + ltrim(@BatchNum)
	FROM #data

	DELETE FROM tblOrderSettlementSelectionShipper WHERE SessionID = @SessionID AND BatchSel = 1

	COMMIT TRAN
END

GO

/*****************************************/
-- Created: ? - 2015.01.19 - Kevin Alons
-- Purpose: return the tblOrder table with a Local OrderDate field added
-- Changes:
-- ??		- 2015/05/17 - KDA	- add local DeliverDate field
-- 3.8.11	- 2015/07/28 - KDA	- remove OriginShipperRegion field
-- 3.9.2	- 2015/08/25 - KDA	- remove computed OrderDate (it is now stored in the table directly)
-- 3.10.13	- 2015/02/29 - JAE	- Add TruckType
-- 4.1.8.6	- 2016.09.24 - KDA	- move H2S | TicketCount | RerouteCount fields from viewOrder down to viewOrderBase
/*****************************************/
ALTER VIEW viewOrderBase AS
	SELECT O.*
		, OriginStateID = OO.StateID
		, OriginRegionID = OO.RegionID
		, DestStateID = D.StateID
		, DestRegionID = D.RegionID
		, P.ProductGroupID
		, T.TruckTypeID
		, DR.DriverGroupID
		, DeliverDate = cast(dbo.fnUTC_to_Local(O.DestDepartTimeUTC, D.TimeZoneID, D.UseDST) as date) 
		, OO.H2S
		, OTC.TicketCount
		, ORC.RerouteCount
	FROM tblOrder O
	JOIN tblOrigin OO ON OO.ID = O.OriginID
	JOIN tblDestination D ON D.ID = O.DestinationID
	LEFT JOIN tblProduct P ON P.ID = O.ProductID
	LEFT JOIN tblTruck T ON T.ID = O.TruckID
	LEFT JOIN tblDriver DR ON DR.ID = O.DriverID
	LEFT JOIN (SELECT OrderID, TicketCount = COUNT(1) FROM tblOrderTicket OT WHERE OT.DeleteDateUTC IS NULL GROUP BY OrderID) OTC ON OTC.OrderID = O.ID
	LEFT JOIN (SELECT OrderID, RerouteCount = COUNT(1) FROM tblOrderReroute GROUP BY OrderID) ORC ON ORC.OrderID = O.ID

GO

/***************************************/
-- Date Created: ?.?.? - 2012.11.25 - Kevin Alons
-- Purpose: return Order records with "translated friendly" values for FK relationships
-- Changes:
-- 3.9.20  - 2015/10/22 - KDA	- add Origin|Dest DriverID fields (using new tblOrderTransfer table) 
--			2015/10/28  - JAE	- added all Order Transfer fields for ease of use in reporting
--			2015/11/03  - BB	- added cast to make TransferComplet BIT type to avoid "Operand type clash" error when running this update script
-- 3.9.21  - 2015/11/03 - KDA	- add OriginDriverGroupID field (from OrderTransfer.OriginDriverID JOIN)
-- 3.9.25  - 2015/11/10 - JAE	- added origin driver and truck to view
-- 3.10.10 - 2016/02/15 - BB	- Add driver region ID to facilitate new user/region filtering feature
-- 3.10.11 - 2016/02/24 - JAE	- Add destination region (name) to view
-- 3.10.13 - 2016/02/29 - JAE	- Add TruckType
-- 3.13.8  - 2016/07/26	- BB	- Add Destination Station
-- 4.1.0.2 - 2016.08.28 - KDA	- rewrite RerouteCount, TicketCount to use a normal JOIN (instead of uncorrelated sub-query)
--								- use viewDriverBase instead of viewDriver
--								- eliminate viewGaugerBase (was unused), use tblOrderStatus instead of viewOrderPrintStatus
-- 4.1.3.3	- 2016/09/14 - BB	- Add origin and destination driving directions (initially for dispatch and truck order create pages)
-- 4.1.8.6	- 2016.09.24 - KDA	- move H2S | TicketCount | RerouteCount fields from viewOrder down to viewOrderBase
/***************************************/
ALTER VIEW viewOrder AS
SELECT O.*
	-- GENERATED orders that were GAUGED to COMPLETION or SKIPPED will show "Gauger [gauger status]"
	, PrintStatus = CASE WHEN O.StatusID = -10 AND GOS.IsComplete = 1 THEN 'Gauger ' + GOS.Name ELSE OPS.OrderStatus END
FROM (
	SELECT O.*
	, Origin = vO.Name 
	, OriginFull = vO.FullName 
	, OriginState = vO.State 
	, OriginStateAbbrev = vO.StateAbbrev 
	, OriginStation = vO.Station
	, OriginCounty = vO.County 
	, OriginDrivingDirections = vO.DrivingDirections	-- 4.1.3.3
	, vO.LeaseName
	, vO.LeaseNum
	, OriginRegion = vO.Region
	, OriginCountryID = vO.CountryID
	, OriginCountry = vO.Country
	, OriginCountryShort = vO.CountryShort
	, OriginLegalDescription = vO.LegalDescription 
	, OriginNDIC = vO.NDICFileNum 
	, OriginNDM = vO.NDM 
	, OriginCA = vO.CA 
	, OriginTimeZoneID = vO.TimeZoneID 
	, OriginUseDST = vO.UseDST 
	, Destination = vD.Name 
	, DestinationFull = vD.FullName 
	, DestinationState = vD.State 
	, DestinationStateAbbrev = vD.StateAbbrev 
	, DestinationDrivingDirections = vD.DrivingDirections	-- 4.1.3.3
	, DestinationTypeID = vD.ID
	, DestinationStation = vD.Station -- 3.13.8
	, vD.DestinationType
	, DestStation = vD.Station 
	, DestTimeZoneID = vD.TimeZoneID 
	, DestUseDST = vD.UseDST 
	, DestRegion = vD.Region -- 3.10.13
	, DestCountryID = vD.CountryID
	, DestCountry = vD.Country
	, DestCountryShort = vD.CountryShort
	, Customer = C.Name 
	, Carrier = CA.Name 
	, CarrierType = CT.Name 
	, OS.OrderStatus
	, OS.StatusNum
	, Driver = D.FullName 
	, DriverFirst = D.FirstName 
	, DriverLast = D.LastName 
	, DriverRegionID = D.RegionID  -- 3.10.10
	, Truck = TRU.FullName 
	, Trailer = TR1.FullName 
	, Trailer2 = TR2.FullName 
	, P.PriorityNum
	, TicketType = TT.Name 
	, DestTicketTypeID = vD.TicketTypeID 
	, DestTicketType = vD.TicketType 
	, Operator = OP.Name 
	, Producer = PR.Name 
	, Pumper = PU.FullName 
	, DriverNumber = D.IDNumber
	, CarrierNumber = CA.IDNumber
	, CarrierTypeID = CA.CarrierTypeID
	, TruckNumber = TRU.IDNumber
	, TruckType = TRU.TruckType 
	, TrailerNumber = TR1.IDNumber 
	, Trailer2Number = TR2.IDNumber 
	, Product = PRO.Name
	, ProductShort = PRO.ShortName 
	, PRO.ProductGroup
	, OriginUOM = OUom.Name 
	, OriginUomShort = OUom.Abbrev 
	, OriginTankID_Text = coalesce(ORT.TankNum, O.OriginTankNum, '?')
	, DestUOM = DUom.Name 
	, DestUomShort = DUom.Abbrev 
	, Active = CAST((CASE WHEN O.DeleteDateUTC IS NULL THEN 1 ELSE 0 END) AS BIT) 
	, IsDeleted = CAST((CASE WHEN O.DeleteDateUTC IS NOT NULL THEN 1 ELSE 0 END) AS BIT) 
	, PickupPrintStatus = PPS.Name 
	, PickupCompleted = PPS.IsCompleted 
	, DeliverPrintStatus = DPS.Name 
	, DeliverCompleted = DPS.IsCompleted 
	, PrintStatusID = CASE WHEN O.StatusID = -9 THEN ISNULL(-GAO.StatusID, O.StatusID)
						   WHEN O.StatusID = 8 AND PPS.IsCompleted = 0 THEN 7
						   WHEN O.StatusID = 3 AND DPS.IsCompleted = 0 THEN 8
						ELSE O.StatusID END 
	, OriginTankText = CASE WHEN O.OriginTankID IS NULL OR ORT.TankNum = '*' THEN O.OriginTankNum ELSE ORT.TankNum END
	, OriginWaitNum = OWR.Num
	, OriginWaitDesc = OWR.Description
	, OriginWaitNumDesc = OWR.NumDesc
	, DestWaitNum = DWR.Num
	, DestWaitDesc = DWR.Description
	, DestWaitNumDesc = DWR.NumDesc
	, RejectNum = ORR.Num
	, RejectDesc = ORR.Description
	, RejectNumDesc = ORR.NumDesc
	, TotalMinutes = ISNULL(OriginMinutes, 0) + ISNULL(DestMinutes, 0)
	-- TRANSFER FIELDS
	, OriginDriverID = ISNULL(OTR.OriginDriverID, O.DriverID)
	, OriginDriverGroupID = ISNULL(ODG.ID, DDG.ID) 
	, OriginDriverGroup = ISNULL(ODG.Name, DDG.Name)
	, OriginDriver = ISNULL(vODR.FullName, vDDR.FullName)
	, OriginDriverFirst = ISNULL(vODR.FirstName, vDDR.FirstName)
	, OriginDriverLast = ISNULL(vODR.LastName, vDDR.LastName)
	, OriginTruckID = ISNULL(OTR.OriginTruckID, O.TruckID)
	, OriginTruck = ISNULL(vOTRU.FullName, vDTRU.FullName)
	, DestDriverID = O.DriverID
	, DestDriver = D.FullName
	, DestDriverFirst = D.FirstName 
	, DestDriverLast = D.LastName 
	, DestTruckID = O.TruckID
	, DestTruck = TRU.FullName 
	, OriginTruckEndMileage = OTR.OriginTruckEndMileage
	, DestTruckStartMileage = OTR.DestTruckStartMileage
	, TransferPercent = OTR.PercentComplete
	, TransferNotes = OTR.Notes
	, TransferDateUTC = OTR.CreateDateUTC
	, TransferComplete = CAST(OTR.TransferComplete AS BIT)
	, IsTransfer = CAST((CASE WHEN OTR.OrderID IS NOT NULL THEN 1 ELSE 0 END) AS BIT)	
	-- 
	, GaugerStatusID = GAO.StatusID
	, GaugerID = GAO.GaugerID	
	FROM dbo.viewOrderBase O
	JOIN dbo.tblOrderStatus OS ON OS.ID = O.StatusID
	LEFT JOIN dbo.tblOrderTransfer OTR ON OTR.OrderID = O.ID
	LEFT JOIN dbo.viewOrigin vO ON vO.ID = O.OriginID
	LEFT JOIN dbo.viewDestination vD ON vD.ID = O.DestinationID
	LEFT JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
	LEFT JOIN dbo.tblCarrier CA ON CA.ID = O.CarrierID
	LEFT JOIN dbo.tblCarrierType CT ON CT.ID = CA.CarrierTypeID
	LEFT JOIN dbo.viewDriverBase D ON D.ID = O.DriverID
	LEFT JOIN dbo.viewDriverBase vODR ON vODR.ID = OTR.OriginDriverID
	LEFT JOIN dbo.viewDriver vDDR ON vDDR.ID = O.DriverID
	LEFT JOIN dbo.tblDriverGroup ODG ON ODG.ID = vODR.DriverGroupID
	LEFT JOIN dbo.tblDriverGroup DDG ON DDG.ID = O.DriverGroupID
	LEFT JOIN dbo.viewTruck TRU ON TRU.ID = O.TruckID
	LEFT JOIN dbo.viewTruck vOTRU ON vOTRU.ID = OTR.OriginTruckID
	LEFT JOIN dbo.viewTruck vDTRU ON vDTRU.ID = O.TruckID
	LEFT JOIN dbo.viewTrailer TR1 ON TR1.ID = O.TrailerID
	LEFT JOIN dbo.viewTrailer TR2 ON TR2.ID = O.Trailer2ID
	LEFT JOIN dbo.tblPriority P ON P.ID = O.PriorityID
	LEFT JOIN dbo.tblTicketType TT ON TT.ID = O.TicketTypeID
	LEFT JOIN dbo.tblOriginTank ORT ON ORT.ID = O.OriginTankID
	LEFT JOIN dbo.tblOperator OP ON OP.ID = O.OperatorID
	LEFT JOIN dbo.viewPumper PU ON PU.ID = O.PumperID
	LEFT JOIN dbo.tblProducer PR ON PR.ID = O.ProducerID
	LEFT JOIN dbo.viewProduct PRO ON PRO.ID = O.ProductID
	LEFT JOIN dbo.tblUom OUom ON OUom.ID = O.OriginUomID
	LEFT JOIN dbo.tblUom DUom ON DUom.ID = O.DestUomID
	LEFT JOIN dbo.tblPrintStatus PPS ON PPS.ID = O.PickupPrintStatusID
	LEFT JOIN dbo.tblPrintStatus DPS ON DPS.ID = O.DeliverPrintStatusID
	LEFT JOIN dbo.viewOriginWaitReason OWR ON OWR.ID = O.OriginWaitReasonID
	LEFT JOIN dbo.viewDestinationWaitReason DWR ON DWR.ID = O.DestWaitReasonID
	LEFT JOIN dbo.viewOrderRejectReason ORR ON ORR.ID = O.RejectReasonID	
	LEFT JOIN dbo.tblGaugerOrder GAO ON GAO.OrderID = O.ID
) O
LEFT JOIN dbo.tblOrderStatus OPS ON OPS.ID = O.PrintStatusID
LEFT JOIN dbo.tblGaugerOrderStatus GOS ON GOS.ID = O.GaugerStatusID

GO

/***********************************/
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return the Local Arrive|Depart Times + their respective TimeZone abbreviations
-- Changes:
-- 3.9.2 - 2015/08/26 - KDA		- minor syntax changes
/***********************************/
ALTER VIEW viewOrderLocalDates AS
SELECT O.*
	, OriginArriveTime = dbo.fnUTC_to_Local(O.OriginArriveTimeUTC, O.OriginTimeZoneID, O.OriginUseDST) 
	, OriginDepartTime = dbo.fnUTC_to_Local(O.OriginDepartTimeUTC, O.OriginTimeZoneID, O.OriginUseDST) 
	, DestArriveTime = dbo.fnUTC_to_Local(O.DestArriveTimeUTC, O.DestTimeZoneID, O.DestUseDST) 
	, DestDepartTime = dbo.fnUTC_to_Local(O.DestDepartTimeUTC, O.DestTimeZoneID, O.DestUseDST) 
	, OriginTimeZone = dbo.fnTimeZoneAbbrev(O.OriginArriveTimeUTC, O.OriginTimeZoneID, O.OriginUseDST) 
	, DestTimeZone = dbo.fnTimeZoneAbbrev(O.DestArriveTimeUTC, O.DestTimeZoneID, O.DestUseDST) 
	, TransitMinutes = DATEDIFF(minute, O.OriginDepartTimeUTC, O.DestArriveTimeUTC) 
	FROM viewOrder O

GO

exec sp_refreshview 'viewOrder_OrderTicket_Full'
exec sp_refreshview 'viewReportCenter_Orders_NoGPS'
go

exec _spRefreshAllViews
go
exec _spRefreshAllViews
go

/***********************************/
-- Created: 4.1.0 - 2016/08/15 - Kevin Alons
-- Purpose: return BASE Order details + Shipper FINANCIAL INFO into a single view
-- Changes:
--	4.1.0.2 - 2016.08.28 - KDA	- remove redundant calculation of ORR.RerouteCount
--								- remove redundant tblOrigin JOIN (for H2S which is already available in viewOrder)
--	4.1.8.2 - 2016.09.21 - JAE	- Check Final Actual miles (null actual miles ok as long as override)
/***********************************/
ALTER VIEW viewOrder_Financial_Base_Shipper AS 
	SELECT *
		, HasError = cast(CASE WHEN ErrorFieldCSV IS NOT NULL THEN 1 ELSE 0 END as bit)
	FROM (
		SELECT *
			, OrderID = ID
			, ErrorFieldCSV = nullif(substring(CASE WHEN Approved = 0 THEN ',Approved' ELSE '' END
				+ CASE WHEN InvoiceLoadAmount IS NULL THEN ',InvoiceLoadAmount,InvoiceRouteRate,InvoiceRateSheet' ELSE '' END
				+ CASE WHEN FinalChainup = 1 AND InvoiceChainupAmount IS NULL THEN ',InvoiceChainupAmount' ELSE '' END
				+ CASE WHEN FinalRerouteCount > 0 AND InvoiceRerouteAmount IS NULL THEN ',InvoiceRerouteAmount' ELSE '' END
				+ CASE WHEN FinalH2S = 1 AND InvoiceH2SAmount IS NULL THEN ',InvoiceH2SAmount' ELSE '' END
				+ CASE WHEN TicketCount > 1 AND InvoiceSplitLoadAmount IS NULL THEN ',InvoiceSplitLoadAmount' ELSE '' END
				+ CASE WHEN InvoiceOriginWaitBillableMinutes > 0 AND InvoiceOriginWaitAmount IS NULL THEN ',InvoiceOriginWaitAmount' ELSE '' END
				+ CASE WHEN InvoiceDestinationWaitBillableMinutes > 0 AND InvoiceDestinationWaitAmount IS NULL THEN ',InvoiceDestinationWaitAmount' ELSE '' END
				+ CASE WHEN Rejected = 1 AND InvoiceOrderRejectAmount IS NULL THEN ',InvoiceOrderRejectAmount' ELSE '' END
				+ CASE WHEN InvoiceMinSettlementUnitsID IS NULL THEN ',InvoiceMinSettlementUnits' ELSE '' END
				+ CASE WHEN InvoiceSettlementFactorID IS NULL THEN ',InvoiceSettlementFactor' ELSE '' END
				+ CASE WHEN Rejected = 0 AND InvoiceSettlementFactorID IS NULL THEN ',InvoiceSettlementFactor' ELSE '' END
				+ CASE WHEN Rejected = 0 AND InvoiceSettlementFactorID IS NOT NULL AND dbo.fnOrderSettlementUnits(InvoiceSettlementFactorID, OriginGrossUnits, OriginNetUnits, OriginGrossStdUnits, DestGrossUnits, DestNetUnits) IS NULL THEN ','+OrderUnitsField ELSE '' END
				+ CASE WHEN InvoiceWaitFeeParameterID IS NULL THEN ',InvoiceOriginWaitBillableMinutes,InvoiceDestinationWaitBillableMinutes' ELSE '' END
				+ CASE WHEN isnull(FinalActualMiles, 0) = 0 THEN ',ActualMiles' ELSE '' END, 2, 100000), '')
		FROM (
			SELECT O.* 
				, InvoiceRatesAppliedDateUTC = OS.CreateDateUTC
				, OS.BatchID
				, Approved = cast(ISNULL(OA.Approved, 0) as bit)
				, FinalActualMiles = ISNULL(OA.OverrideActualMiles, O.ActualMiles)
				, FinalOriginMinutes = ISNULL(OA.OverrideOriginMinutes, O.OriginMinutes)
				, FinalDestMinutes = ISNULL(OA.OverrideDestMinutes, O.DestMinutes)
				, FinalH2S = cast(CASE WHEN isnull(OA.OverrideH2S, 0) = 1 THEN 0 ELSE O.H2S END as bit)
				, FinalChainup = cast(CASE WHEN isnull(OA.OverrideChainup, 0) = 1 THEN 0 ELSE O.Chainup END as bit)
				, FinalRerouteCount = CASE WHEN isnull(OA.OverrideReroute, 0) = 1 THEN 0 ELSE isnull(O.RerouteCount, 0) END
				, FC.OrderUnitsField
				, InvoiceBatchNum = OS.BatchNum 
				, InvoiceOriginWaitBillableMinutes = OS.OriginWaitBillableMinutes
				, InvoiceDestinationWaitBillableMinutes = OS.DestinationWaitBillableMinutes
				, InvoiceTotalWaitBillableMinutes = isnull(OS.OriginWaitBillableMinutes, 0) + ISNULL(OS.DestinationWaitBillableMinutes, 0) 
				, InvoiceWaitFeeParameterID = WaitFeeParameterID
				, InvoiceWaitFeeSubUnit = WaitFeeSubUnit
				, InvoiceWaitFeeRoundingType = WaitFeeRoundingType
				, InvoiceOriginWaitRate = OS.OriginWaitRate 
				, InvoiceOriginWaitAmount = OS.OriginWaitAmount 		
				, InvoiceDestinationWaitRate = OS.DestinationWaitRate 
				, InvoiceDestinationWaitAmount = OS.DestinationWaitAmount 
				, InvoiceTotalWaitAmount = OS.TotalWaitAmount
				, InvoiceOrderRejectRate = OS.OrderRejectRate	
				, InvoiceOrderRejectRateType = OS.OrderRejectRateType  
				, InvoiceOrderRejectAmount = OS.OrderRejectAmount  
				, InvoiceChainupRate = OS.ChainupRate
				, InvoiceChainupRateType = OS.ChainupRateType
				, InvoiceChainupAmount = OS.ChainupAmount 
				, InvoiceRerouteRate = OS.RerouteRate
				, InvoiceRerouteRateType = OS.RerouteRateType
				, InvoiceRerouteAmount = OS.RerouteAmount 
				, InvoiceH2SRate = OS.H2SRate
				, InvoiceH2SRateType = OS.H2SRateType
				, InvoiceH2SAmount = OS.H2SAmount
				, InvoiceSplitLoadRate = OS.SplitLoadRate
				, InvoiceSplitLoadRateType = OS.SplitLoadRateType
				, InvoiceSplitLoadAmount = OS.SplitLoadAmount
				, InvoiceOtherAmount = OS.OtherAmount
				, InvoiceTaxRate = OS.OriginTaxRate
				, InvoiceSettlementUom = OS.SettlementUom 
				, InvoiceSettlementUomShort = OS.SettlementUomShort 
				, InvoiceShipperSettlementFactorID = OS.ShipperSettlementFactorID 
				, InvoiceSettlementFactorID = OS.SettlementFactorID 
				, InvoiceSettlementFactor = OS.SettlementFactor 
				, InvoiceMinSettlementUnitsID = OS.MinSettlementUnitsID 
				, InvoiceMinSettlementUnits = OS.MinSettlementUnits
				, InvoiceUnits = OS.SettlementUnits
				, InvoiceRouteRate = OS.RouteRate
				, InvoiceRouteRateType = OS.RouteRateType
				, InvoiceRateSheetRate = OS.RateSheetRate
				, InvoiceRateSheetRateType = OS.RateSheetRateType
				, InvoiceFuelSurchargeRate = OS.FuelSurchargeRate
				, InvoiceFuelSurchargeAmount = OS.FuelSurchargeAmount
				, InvoiceLoadAmount = OS.LoadAmount
				, InvoiceTotalAmount = OS.TotalAmount
			FROM dbo.viewOrder O
			LEFT JOIN viewOrderSettlementShipper OS ON OS.OrderID = O.ID
			LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
			LEFT JOIN tblSettlementFactor FC ON FC.ID = OS.SettlementFactorID
			WHERE O.StatusID NOT IN (-10) -- don't include "Generated" orders (only Assigned+)
		) X
	) X1

GO

/***********************************/
-- Created: 4.1.0 - 2016/08/15 - Kevin Alons
-- Purpose: return BASE Order details + Carrier FINANCIAL INFO into a single view
-- Changes:
-- 4.1.0.2 - 2016.08.28 - KDA	- remove redundant calculation of ORR.RerouteCount
--								- remove redundant tblOrigin JOIN (for H2S which is already available in viewOrder)
-- 4.1.8.2 - 2016.09.21 - KDA	- Check Final Actual miles (null actual miles ok as long as override)
/***********************************/
ALTER VIEW viewOrder_Financial_Base_Carrier AS 
	SELECT *
		, HasError = cast(CASE WHEN ErrorFieldCSV IS NOT NULL THEN 1 ELSE 0 END as bit)
	FROM (
		SELECT *
			, OrderID = ID
			, ErrorFieldCSV = nullif(substring(CASE WHEN Approved = 0 THEN ',Approved' ELSE '' END
				+ CASE WHEN InvoiceLoadAmount IS NULL THEN ',InvoiceLoadAmount,InvoiceRouteRate,InvoiceRateSheet' ELSE '' END
				+ CASE WHEN FinalChainup = 1 AND InvoiceChainupAmount IS NULL THEN ',InvoiceChainupAmount' ELSE '' END
				+ CASE WHEN FinalRerouteCount > 0 AND InvoiceRerouteAmount IS NULL THEN ',InvoiceRerouteAmount' ELSE '' END
				+ CASE WHEN FinalH2S = 1 AND InvoiceH2SAmount IS NULL THEN ',InvoiceH2SAmount' ELSE '' END
				+ CASE WHEN TicketCount > 1 AND InvoiceSplitLoadAmount IS NULL THEN ',InvoiceSplitLoadAmount' ELSE '' END
				+ CASE WHEN InvoiceOriginWaitBillableMinutes > 0 AND InvoiceOriginWaitAmount IS NULL THEN ',InvoiceOriginWaitAmount' ELSE '' END
				+ CASE WHEN InvoiceDestinationWaitBillableMinutes > 0 AND InvoiceDestinationWaitAmount IS NULL THEN ',InvoiceDestinationWaitAmount' ELSE '' END
				+ CASE WHEN Rejected = 1 AND InvoiceOrderRejectAmount IS NULL THEN ',InvoiceOrderRejectAmount' ELSE '' END
				+ CASE WHEN InvoiceMinSettlementUnitsID IS NULL THEN ',InvoiceMinSettlementUnits' ELSE '' END
				+ CASE WHEN InvoiceSettlementFactorID IS NULL THEN ',InvoiceSettlementFactor' ELSE '' END
				+ CASE WHEN Rejected = 0 AND InvoiceSettlementFactorID IS NULL THEN ',InvoiceSettlementFactor' ELSE '' END
				+ CASE WHEN Rejected = 0 AND InvoiceSettlementFactorID IS NOT NULL AND dbo.fnOrderSettlementUnits(InvoiceSettlementFactorID, OriginGrossUnits, OriginNetUnits, OriginGrossStdUnits, DestGrossUnits, DestNetUnits) IS NULL THEN ','+OrderUnitsField ELSE '' END
				+ CASE WHEN InvoiceWaitFeeParameterID IS NULL THEN ',InvoiceOriginWaitBillableMinutes,InvoiceDestinationWaitBillableMinutes' ELSE '' END
				+ CASE WHEN isnull(FinalActualMiles, 0) = 0 THEN ',ActualMiles' ELSE '' END, 2, 100000), '')
		FROM (
			SELECT O.* 
				, InvoiceRatesAppliedDateUTC = OS.CreateDateUTC
				, OS.BatchID
				, Approved = cast(ISNULL(OA.Approved, 0) as bit)
				, FinalActualMiles = ISNULL(OA.OverrideActualMiles, O.ActualMiles)
				, FinalOriginMinutes = ISNULL(OA.OverrideOriginMinutes, O.OriginMinutes)
				, FinalDestMinutes = ISNULL(OA.OverrideDestMinutes, O.DestMinutes)
				, FinalH2S = cast(CASE WHEN isnull(OA.OverrideH2S, 0) = 1 THEN 0 ELSE O.H2S END as bit)
				, FinalChainup = cast(CASE WHEN isnull(OA.OverrideChainup, 0) = 1 THEN 0 ELSE O.Chainup END as bit)
				, FinalRerouteCount = CASE WHEN isnull(OA.OverrideReroute, 0) = 1 THEN 0 ELSE isnull(O.RerouteCount, 0) END
				, FC.OrderUnitsField
				, InvoiceBatchNum = OS.BatchNum 
				, InvoiceOriginWaitBillableMinutes = OS.OriginWaitBillableMinutes
				, InvoiceDestinationWaitBillableMinutes = OS.DestinationWaitBillableMinutes
				, InvoiceTotalWaitBillableMinutes = isnull(OS.OriginWaitBillableMinutes, 0) + ISNULL(OS.DestinationWaitBillableMinutes, 0) 
				, InvoiceWaitFeeParameterID = WaitFeeParameterID
				, InvoiceWaitFeeSubUnit = WaitFeeSubUnit
				, InvoiceWaitFeeRoundingType = WaitFeeRoundingType
				, InvoiceOriginWaitRate = OS.OriginWaitRate 
				, InvoiceOriginWaitAmount = OS.OriginWaitAmount 		
				, InvoiceDestinationWaitRate = OS.DestinationWaitRate 
				, InvoiceDestinationWaitAmount = OS.DestinationWaitAmount 
				, InvoiceTotalWaitAmount = OS.TotalWaitAmount
				, InvoiceOrderRejectRate = OS.OrderRejectRate	
				, InvoiceOrderRejectRateType = OS.OrderRejectRateType  
				, InvoiceOrderRejectAmount = OS.OrderRejectAmount  
				, InvoiceChainupRate = OS.ChainupRate
				, InvoiceChainupRateType = OS.ChainupRateType
				, InvoiceChainupAmount = OS.ChainupAmount 
				, InvoiceRerouteRate = OS.RerouteRate
				, InvoiceRerouteRateType = OS.RerouteRateType
				, InvoiceRerouteAmount = OS.RerouteAmount 
				, InvoiceH2SRate = OS.H2SRate
				, InvoiceH2SRateType = OS.H2SRateType
				, InvoiceH2SAmount = OS.H2SAmount
				, InvoiceSplitLoadRate = OS.SplitLoadRate
				, InvoiceSplitLoadRateType = OS.SplitLoadRateType
				, InvoiceSplitLoadAmount = OS.SplitLoadAmount
				, InvoiceOtherAmount = OS.OtherAmount
				, InvoiceTaxRate = OS.OriginTaxRate
				, InvoiceSettlementUom = OS.SettlementUom 
				, InvoiceSettlementUomShort = OS.SettlementUomShort 
				, InvoiceCarrierSettlementFactorID = OS.CarrierSettlementFactorID 
				, InvoiceSettlementFactorID = OS.SettlementFactorID 
				, InvoiceSettlementFactor = OS.SettlementFactor 
				, InvoiceMinSettlementUnitsID = OS.MinSettlementUnitsID 
				, InvoiceMinSettlementUnits = OS.MinSettlementUnits
				, InvoiceUnits = OS.SettlementUnits
				, InvoiceRouteRate = OS.RouteRate
				, InvoiceRouteRateType = OS.RouteRateType
				, InvoiceRateSheetRate = OS.RateSheetRate
				, InvoiceRateSheetRateType = OS.RateSheetRateType
				, InvoiceFuelSurchargeRate = OS.FuelSurchargeRate
				, InvoiceFuelSurchargeAmount = OS.FuelSurchargeAmount
				, InvoiceLoadAmount = OS.LoadAmount
				, InvoiceTotalAmount = OS.TotalAmount
			FROM dbo.viewOrder O
			LEFT JOIN viewOrderSettlementCarrier OS ON OS.OrderID = O.ID
			LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
			LEFT JOIN tblSettlementFactor FC ON FC.ID = OS.SettlementFactorID
			WHERE O.StatusID NOT IN (-10) -- don't include "Generated" orders (only Assigned+)
		) X
	) X1

GO

/***********************************/
-- Created: 4.1.0 - 2016/08/15 - Kevin Alons
-- Purpose: return BASE Order details + Driver FINANCIAL INFO into a single view
-- Changes:
-- 4.1.0.2 - 2016.08.28 - KDA	- remove redundant calculation of ORR.RerouteCount
--								- remove redundant tblOrigin JOIN (for H2S which is already available in viewOrder)
-- 4.1.8.2 - 2016.09.21 - JAE	- Check Final Actual miles (null actual miles ok as long as override)
/***********************************/
ALTER VIEW viewOrder_Financial_Base_Driver AS 
	SELECT *
		, HasError = cast(CASE WHEN ErrorFieldCSV IS NOT NULL THEN 1 ELSE 0 END as bit)
	FROM (
		SELECT *
			, OrderID = ID
			, ErrorFieldCSV = nullif(substring(CASE WHEN Approved = 0 THEN ',Approved' ELSE '' END
				+ CASE WHEN InvoiceLoadAmount IS NULL THEN ',InvoiceLoadAmount,InvoiceRouteRate,InvoiceRateSheet' ELSE '' END
				+ CASE WHEN FinalChainup = 1 AND InvoiceChainupAmount IS NULL THEN ',InvoiceChainupAmount' ELSE '' END
				+ CASE WHEN FinalRerouteCount > 0 AND InvoiceRerouteAmount IS NULL THEN ',InvoiceRerouteAmount' ELSE '' END
				+ CASE WHEN FinalH2S = 1 AND InvoiceH2SAmount IS NULL THEN ',InvoiceH2SAmount' ELSE '' END
				+ CASE WHEN TicketCount > 1 AND InvoiceSplitLoadAmount IS NULL THEN ',InvoiceSplitLoadAmount' ELSE '' END
				+ CASE WHEN InvoiceOriginWaitBillableMinutes > 0 AND InvoiceOriginWaitAmount IS NULL THEN ',InvoiceOriginWaitAmount' ELSE '' END
				+ CASE WHEN InvoiceDestinationWaitBillableMinutes > 0 AND InvoiceDestinationWaitAmount IS NULL THEN ',InvoiceDestinationWaitAmount' ELSE '' END
				+ CASE WHEN Rejected = 1 AND InvoiceOrderRejectAmount IS NULL THEN ',InvoiceOrderRejectAmount' ELSE '' END
				+ CASE WHEN InvoiceMinSettlementUnitsID IS NULL THEN ',InvoiceMinSettlementUnits' ELSE '' END
				+ CASE WHEN InvoiceSettlementFactorID IS NULL THEN ',InvoiceSettlementFactor' ELSE '' END
				+ CASE WHEN Rejected = 0 AND InvoiceSettlementFactorID IS NULL THEN ',InvoiceSettlementFactor' ELSE '' END
				+ CASE WHEN Rejected = 0 AND InvoiceSettlementFactorID IS NOT NULL AND dbo.fnOrderSettlementUnits(InvoiceSettlementFactorID, OriginGrossUnits, OriginNetUnits, OriginGrossStdUnits, DestGrossUnits, DestNetUnits) IS NULL THEN ','+OrderUnitsField ELSE '' END
				+ CASE WHEN InvoiceWaitFeeParameterID IS NULL THEN ',InvoiceOriginWaitBillableMinutes,InvoiceDestinationWaitBillableMinutes' ELSE '' END
				+ CASE WHEN isnull(FinalActualMiles, 0) = 0 THEN ',ActualMiles' ELSE '' END, 2, 100000), '')
		FROM (
			SELECT O.* 
				, DriverGroup = DG.Name
				, InvoiceRatesAppliedDateUTC = OS.CreateDateUTC
				, OS.BatchID
				, Approved = cast(ISNULL(OA.Approved, 0) as bit)
				, FinalActualMiles = ISNULL(OA.OverrideActualMiles, O.ActualMiles)
				, FinalOriginMinutes = ISNULL(OA.OverrideOriginMinutes, O.OriginMinutes)
				, FinalDestMinutes = ISNULL(OA.OverrideDestMinutes, O.DestMinutes)
				, FinalH2S = cast(CASE WHEN isnull(OA.OverrideH2S, 0) = 1 THEN 0 ELSE O.H2S END as bit)
				, FinalChainup = cast(CASE WHEN isnull(OA.OverrideChainup, 0) = 1 THEN 0 ELSE O.Chainup END as bit)
				, FinalRerouteCount = CASE WHEN isnull(OA.OverrideReroute, 0) = 1 THEN 0 ELSE isnull(O.RerouteCount, 0) END
				, FC.OrderUnitsField
				, InvoiceBatchNum = OS.BatchNum 
				, InvoiceOriginWaitBillableMinutes = OS.OriginWaitBillableMinutes
				, InvoiceDestinationWaitBillableMinutes = OS.DestinationWaitBillableMinutes
				, InvoiceTotalWaitBillableMinutes = isnull(OS.OriginWaitBillableMinutes, 0) + ISNULL(OS.DestinationWaitBillableMinutes, 0) 
				, InvoiceWaitFeeParameterID = WaitFeeParameterID
				, InvoiceWaitFeeSubUnit = WaitFeeSubUnit
				, InvoiceWaitFeeRoundingType = WaitFeeRoundingType
				, InvoiceOriginWaitRate = OS.OriginWaitRate 
				, InvoiceOriginWaitAmount = OS.OriginWaitAmount 		
				, InvoiceDestinationWaitRate = OS.DestinationWaitRate 
				, InvoiceDestinationWaitAmount = OS.DestinationWaitAmount 
				, InvoiceTotalWaitAmount = OS.TotalWaitAmount
				, InvoiceOrderRejectRate = OS.OrderRejectRate	
				, InvoiceOrderRejectRateType = OS.OrderRejectRateType  
				, InvoiceOrderRejectAmount = OS.OrderRejectAmount  
				, InvoiceChainupRate = OS.ChainupRate
				, InvoiceChainupRateType = OS.ChainupRateType
				, InvoiceChainupAmount = OS.ChainupAmount 
				, InvoiceRerouteRate = OS.RerouteRate
				, InvoiceRerouteRateType = OS.RerouteRateType
				, InvoiceRerouteAmount = OS.RerouteAmount 
				, InvoiceH2SRate = OS.H2SRate
				, InvoiceH2SRateType = OS.H2SRateType
				, InvoiceH2SAmount = OS.H2SAmount
				, InvoiceSplitLoadRate = OS.SplitLoadRate
				, InvoiceSplitLoadRateType = OS.SplitLoadRateType
				, InvoiceSplitLoadAmount = OS.SplitLoadAmount
				, InvoiceOtherAmount = OS.OtherAmount
				, InvoiceTaxRate = OS.OriginTaxRate
				, InvoiceSettlementUom = OS.SettlementUom 
				, InvoiceSettlementUomShort = OS.SettlementUomShort 
				, InvoiceCarrierSettlementFactorID = OS.CarrierSettlementFactorID 
				, InvoiceSettlementFactorID = OS.SettlementFactorID 
				, InvoiceSettlementFactor = OS.SettlementFactor 
				, InvoiceMinSettlementUnitsID = OS.MinSettlementUnitsID 
				, InvoiceMinSettlementUnits = OS.MinSettlementUnits
				, InvoiceUnits = OS.SettlementUnits
				, InvoiceRouteRate = OS.RouteRate
				, InvoiceRouteRateType = OS.RouteRateType
				, InvoiceRateSheetRate = OS.RateSheetRate
				, InvoiceRateSheetRateType = OS.RateSheetRateType
				, InvoiceFuelSurchargeRate = OS.FuelSurchargeRate
				, InvoiceFuelSurchargeAmount = OS.FuelSurchargeAmount
				, InvoiceLoadAmount = OS.LoadAmount
				, InvoiceTotalAmount = OS.TotalAmount
			FROM dbo.viewOrder O
			LEFT JOIN tblDriverGroup DG ON DG.ID = O.DriverGroupID
			LEFT JOIN viewOrderSettlementDriver OS ON OS.OrderID = O.ID
			LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
			LEFT JOIN tblSettlementFactor FC ON FC.ID = OS.SettlementFactorID
			WHERE O.StatusID NOT IN (-10) -- don't include "Generated" orders (only Assigned+)
		) X
	) X1

GO

exec _spRefreshAllViews
go

COMMIT
SET NOEXEC OFF