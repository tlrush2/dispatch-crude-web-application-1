
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET XACT_ABORT ON
GO


BEGIN TRANSACTION
GO

UPDATE tblSetting SET Value = '1.1.15' WHERE ID=0

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblOriginType] DROP CONSTRAINT [DF_tblOriginTypes_Auditable]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblOriginType] DROP COLUMN [Auditable]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblCarrier] ADD [ACHInstructions] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblOrder] ADD [DestProductBSW] [decimal](9,3) NULL
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblOrder] ADD [DestProductGravity] [decimal](9,3) NULL
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblOrder] ADD [DestProductTemp] [decimal](9,3) NULL
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblCarrier] ADD [DOTNumber] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblCarrier] ADD [FEINNumber] [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblCarrier] ADD [HazmatCertificateDocName] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblCarrier] ADD [HazmatCertificateDocument] [varbinary](max) NULL
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblCarrier] ADD [HazmatExpiration] [smalldatetime] NULL
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblCarrier] ADD [InsuranceCertificateDocName] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblCarrier] ADD [InsuranceCertificateDocument] [varbinary](max) NULL
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblCarrier] ADD [InsuranceExpiration] [smalldatetime] NULL
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblCarrier] ADD [MotorCarrierNumber] [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblCarrier] ADD [W9DocName] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblCarrier] ADD [W9Document] [varbinary](max) NULL
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






/***********************************/
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Origin records with translated value and FullName field (which includes Origin Type)
/***********************************/
ALTER VIEW [dbo].[viewOrigin] AS
SELECT O.*, OT.OriginType + ' - ' + O.Name AS FullName, OT.OriginType
, S.FullName AS State, S.Abbreviation AS StateAbbrev
, OP.Name AS Operator
, P.FullName AS Pumper
, TT.Name AS TicketType
, R.Name AS Region
, C.Name AS Customer
, PR.Name AS Producer
, TAT.FullName AS TankType
FROM dbo.tblOrigin O
JOIN dbo.tblOriginType OT ON OT.ID = O.OriginTypeID
LEFT JOIN tblState S ON S.ID = O.StateID
LEFT JOIN dbo.tblOperator OP ON OP.ID = O.OperatorID
LEFT JOIN dbo.viewPumper P ON P.ID = O.PumperID
LEFT JOIN dbo.tblTicketType TT ON TT.ID = O.TicketTypeID
LEFT JOIN dbo.tblRegion R ON R.ID = O.RegionID
LEFT JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
LEFT JOIN dbo.tblProducer PR ON PR.ID = O.ProducerID
LEFT JOIN dbo.viewTankType TAT ON TAT.ID = O.TankTypeID





GO

EXEC _spRefreshAllViews
EXEC _spRefreshAllViews

COMMIT TRANSACTION
GO