SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20) = '3.13.5'
	, @NewVersion varchar(20) = '3.13.6'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1563 - Add system setting for populating GPS coordinates for empty locations'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


INSERT INTO tblSetting
	(ID, Name, SettingTypeID, Value, Category, CreateDateUTC, CreatedByUser, ReadOnly)
VALUES
	(62, 'Use heuristic data to average GPS locations', 2, 'True', 'System Wide', GETUTCDATE(), 'System', 0)
GO

/*************************************************/
-- Creation Info: 3.13.5 - 2016/07/04
-- Author: Joe Engler
-- Purpose: Calculate average GPS for locations with empty coordinates.  Update both origins and destinations if enough data points are found
-- Changes: 
--		3.13.6 - JAE - 2016/07/26	Add system setting for updating with averages
/*************************************************/

ALTER PROCEDURE spAverageGPSEmptyCoordinates AS
BEGIN
	PRINT 'spAverageGPSEmptyCoordinates START: '
	PRINT GETDATE()

	DECLARE @MIN_POINTS INT = 10
	DECLARE @__SYSTEM_SETTING_AUTO_AVERAGE_GPS__ INT = 62

	IF (dbo.fnToBool(dbo.fnSettingValue(@__SYSTEM_SETTING_AUTO_AVERAGE_GPS__)) = 0)
		PRINT 'System setting turned OFF'
	ELSE
	BEGIN

		PRINT 'Origins'
		-- Update origins with null coordinates
		;
		WITH NoGPS AS
		(
			-- Select origins with null LAT/LON that have enough datapoints
			SELECT ID
			FROM tblOrigin o
			WHERE LAT IS NULL AND LON IS NULL
			AND (SELECT COUNT(*) FROM tblDriverLocation dl WHERE dl.OriginID = o.ID AND LocationTypeID IN (2,3,6)) > @MIN_POINTS
		)
		UPDATE tblOrigin
		   SET LAT = (SELECT AVG(LAT) FROM (
							SELECT NTILE(5) OVER(ORDER BY LAT) AS segment, * FROM (
									SELECT LAT FROM tblDriverLocation
									 WHERE OriginID = NoGPS.ID AND LocationTypeID IN (2,3,6)) t
							) t2
							WHERE segment NOT IN (1, 5) -- omit top and bottom 20%
					 ),
			   LON = (SELECT AVG(LON) FROM   (
							SELECT NTILE(5) OVER(ORDER BY LON) AS segment, * FROM (
									SELECT LON FROM tblDriverLocation 
									WHERE OriginID = NoGPS.ID AND LocationTypeID IN (2,3,6)) t
							) t2
							WHERE segment NOT IN (1, 5) -- omit top and bottom 20%
					 ),
			   LastChangeDateUTC = GETUTCDATE(),
			   LastChangedByUser = 'Auto GPS'
		  FROM NoGPS
		 WHERE tblOrigin.ID = NoGPS.ID
		   AND tblOrigin.LAT IS NULL AND tblOrigin.LON IS NULL


		PRINT 'Destinations'
		-- Update destinations with null coordinates
		;
		WITH NoGPS AS
		(
			-- Select destinations with null LAT/LON that have enough datapoints
			SELECT ID
			FROM tblDestination d
			WHERE LAT IS NULL AND LON IS NULL
			AND (SELECT COUNT(*) FROM tblDriverLocation dl WHERE dl.DestinationID = d.ID AND LocationTypeID IN (2,3,7)) > @MIN_POINTS
		)
		UPDATE tblDestination
		   SET LAT = (SELECT AVG (LAT) FROM (
							SELECT NTILE(5) OVER(ORDER BY LAT) AS segment, * FROM (
									SELECT LAT FROM tblDriverLocation 
									WHERE DestinationID = NoGPS.ID AND LocationTypeID IN (2,3,7)) t
							) t2
							WHERE segment NOT IN (1, 5) -- omit top and bottom 20%
					 ),
			   LON = (SELECT AVG(LON) FROM (
							SELECT NTILE(5) OVER(ORDER BY LON) AS Segment, * FROM (
									SELECT LON FROM tblDriverLocation 
									WHERE DestinationID = NoGPS.ID AND LocationTypeID IN (2,3,7)) t
							) t2
							WHERE segment NOT IN (1, 5) -- omit top and bottom 20%
					 ),
			   LastChangeDateUTC = GETUTCDATE(),
			   LastChangedByUser = 'Auto GPS'
		  FROM NoGPS
		 WHERE tblDestination.ID = NoGPS.ID
		   AND tblDestination.LAT IS NULL AND tblDestination.LON IS NULL

	END
	
	PRINT 'spAverageGPSEmptyCoordinates END: '
	PRINT GETDATE()
END

GO


COMMIT
SET NOEXEC OFF