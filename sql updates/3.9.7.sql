-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.9.5.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.9.5.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0

SET NOEXEC OFF
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.9.6'
SELECT  @NewVersion = '3.9.7'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DB Performance optimization: add tblDriverLocation.idxDriverLocation_UID'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier DestinationWaitRate info for the specified order
-- Changes
	- 3.7.13 - 5/21/15 - KDA - fix bug which prevented missing rate warning when billable minutes existed but no rate matched
	- 3.7.28 - 2015/05/18 - KDA - add use of @ProducerID, @DriverID, @DriverGroupID parameter
	- 3.7.30 - 2015/05/18 - KDA - remove DriverID
	- 3.9.0  - 2015/08/14 - KDA - honor tblOrderApproval.OverrideOriginMinutes in Minutes computation
	- 3.9.7  - 2015/08/31 - KDA - fix typo where DestMinutes was used instead of DestMinutes
***********************************/
ALTER FUNCTION fnOrderCarrierDestinationWaitRate(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID
		, Minutes = isnull(OA.OverrideDestMinutes, O.DestMinutes)
		, R.Rate
	FROM viewOrderBase O
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = @ID
	OUTER APPLY dbo.fnCarrierDestinationWaitRate(O.OrderDate, null, O.DestWaitReasonID, O.CustomerID, O.CarrierID, O.ProductGroupID, O.DriverGroupID, O.DestinationID, O.DestStateID, O.DestRegionID, O.ProducerID, 1) R
	WHERE O.ID = @ID
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper DestinationWaitRate info for the specified order
-- Changes
	- 3.7.13 - 5/21/15 - KDA - fix bug which prevented missing rate warning when billable minutes existed but no rate matched
	- 3.9.0  - 2015/08/14 - KDA - honor tblOrderApproval.OverrideDestMinutes in Minutes computation
	- 3.9.7  - 2015/08/31 - KDA - fix typo where DestMinutes was used instead of DestMinutes
***********************************/
ALTER FUNCTION fnOrderShipperDestinationWaitRate(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID
		, Minutes = isnull(OA.OverrideDestMinutes, O.DestMinutes)
		, R.Rate
	FROM viewOrderBase O
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = @ID
	OUTER APPLY dbo.fnShipperDestinationWaitRate(O.OrderDate, null, O.DestWaitReasonID, O.CustomerID, O.ProductGroupID, DestinationID, O.DestStateID, O.DestRegionID, O.ProducerID, 1) R
	WHERE O.ID = @ID
GO

/* add new associated Report Center - Order columns - add missing "Dest Min Final" */
SET IDENTITY_INSERT tblReportColumnDefinition ON
INSERT INTO tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport)
	SELECT 90017, 1, 'SELECT isnull(OA.OverrideDestMin, RS.DestMin) FROM tblOrderApproval OA WHERE OA.OrderID = RS.ID', 'TRIP | TIMESTAMPS | Dest Min Final', NULL, NULL, 4, NULL, 1, '*', 0
SET IDENTITY_INSERT tblReportColumnDefinition OFF

COMMIT
SET NOEXEC OFF