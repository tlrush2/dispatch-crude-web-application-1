BEGIN TRANSACTION
GO

UPDATE tblSetting SET Value = '1.2.4' WHERE ID=0
GO

ALTER TABLE tblCustomer ADD EmergencyInfo varchar(255) NULL
GO

EXEC _spRefreshAllViews
GO

EXEC _spRecompileAllStoredProcedures
GO

COMMIT TRANSACTION
GO

