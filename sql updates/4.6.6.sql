SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.6.5'
SELECT  @NewVersion = '4.6.6'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'JT-3591 - Update to when sequence number is pushed to mobile'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/*****************************************
 Creation Info: 3.10.5 - 2015/01/29
 Author: Kevin Alons
 Purpose: detect & record changes to order records to optimize Driver App sync
 --		4.6.6		5/3/2017		JAE			Add recent fields that did not make it into the read only sync trigger (sequence #, consignee, etc)
*****************************************/
ALTER TRIGGER trigOrder_AppChanges_U ON tblOrder AFTER UPDATE AS
BEGIN
	IF (TRIGGER_NESTLEVEL( OBJECT_ID('trigOrder_IU')) = 0 AND TRIGGER_NESTLEVEL( OBJECT_ID('trigOrderTicket_IU')) = 0
		AND EXISTS (SELECT * FROM inserted EXCEPT SELECT * FROM deleted)) 
	BEGIN
		SET NOCOUNT ON
		PRINT 'trigOrder_AppChanges_U START: ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
		-- find all records where the OriginID changed
		DECLARE @OCID IDTABLE
		INSERT INTO @OCID
			SELECT ID FROM (
				SELECT ID, OriginID FROM inserted WHERE StatusID IN (2,7,8,3)
				EXCEPT
				SELECT ID, OriginID FROM deleted
			) OC
		-- find all records where any OrderRule fields changed
		-- NOTE: if any new fields are added OrdersRules (best match criteria), they need to also be added below
		DECLARE @ORCID IDTABLE
		INSERT INTO @ORCID
			SELECT ID FROM (
				SELECT ID
					, CustomerID
					, CarrierID
					, ProductID
					, DueDate
					, OriginID
					, DestinationID
					, OperatorID
					, ProducerID
				FROM inserted
				WHERE StatusID IN (2,7,8,3)
				EXCEPT SELECT ID
					, CustomerID
					, CarrierID
					, ProductID
					, DueDate
					, OriginID
					, DestinationID
					, OperatorID
					, ProducerID
				FROM deleted
			) ORC
		-- find all records where any ReadOnly fields changed
		-- NOTE: if any new fields are added OrdersReadOnly for the DriverApp, they need to also be added below
		DECLARE @ROCID IDTABLE
		INSERT INTO @ROCID
			SELECT ID FROM (
				SELECT ID
					, OrderNum
					, StatusID
					, TicketTypeID
					, PriorityID
					, ProductID
					, DueDate
					, OriginID
					, OriginUomID
					, DestinationID
					, DestUomID
					, OperatorID
					, PumperID
					, ProducerID
					, CustomerID
					, CarrierID
					, OriginTankNum
					, OriginTankID
					, DispatchNotes
					, DispatchConfirmNum
					, SequenceNum
					, JobNumber
					, ContractID 
					, ConsigneeID
				FROM inserted
				WHERE StatusID IN (2,7,8,3)
				EXCEPT SELECT ID
					, OrderNum
					, StatusID
					, TicketTypeID
					, PriorityID
					, ProductID
					, DueDate
					, OriginID
					, OriginUomID
					, DestinationID
					, DestUomID
					, OperatorID
					, PumperID
					, ProducerID
					, CustomerID
					, CarrierID
					, OriginTankNum
					, OriginTankID
					, DispatchNotes
					, DispatchConfirmNum
					, SequenceNum
					, JobNumber
					, ContractID 
					, ConsigneeID
				FROM deleted
			) ROC
		-- record these xxxChangeDateUTC values
		EXEC spOrder_AppChanges @OCID, @ORCID, @ROCID

		PRINT 'trigOrder_AppChanges_U FINISH: ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	END
END

GO


COMMIT
SET NOEXEC OFF