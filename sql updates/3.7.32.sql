-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.7.31.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.7.31.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0
SET NOEXEC OFF
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.7.31'
SELECT  @NewVersion = '3.7.32'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'Settlement Center - Carrier Settlement now has "Driver Group" filter criteria'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/***********************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OrderRejectRate rows for the specified criteria
-- Changes:
	- 3.7.28 - 2015/06/18 - KDA - add DriverID/DriverGroupID/ProducerID parameters
	- 3.7.28 - 2015/06/19 - KDA - remove DriverID
***********************************/
ALTER FUNCTION [fnCarrierOrderRejectRatesDisplay]
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @DriverGroupID int
, @OriginID int
, @StateID int
, @RegionID int
, @ProducerID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ReasonID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.DriverGroupID, R.OriginID, R.StateID, R.RegionID, R.ProducerID
		, R.Rate, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Shipper = SH.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, DriverGroup = DG.Name
		, Origin = D.Name
		, OriginFull = D.FullName
		, State = S.FullName
		, StateAbbrev = S.Abbreviation
		, Region = REG.Name
		, Producer = PR.Name
		, Reason = REA.Description
		, RateType = RT.Name
		, Uom = U.Name
		, UomShort = U.Abbrev
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
	FROM dbo.fnCarrierOrderRejectRate(@StartDate, @EndDate, @ReasonID, @ShipperID, @CarrierID, @ProductGroupID, @DriverGroupID, @OriginID, @StateID, @RegionID, @ProducerID, 0) R
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblDriverGroup DG ON DG.ID = R.DriverGroupID
	LEFT JOIN viewOrigin D ON D.ID = R.OriginID
	LEFT JOIN tblState S ON S.ID = R.StateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblProducer PR ON PR.ID = R.ProducerID
	LEFT JOIN tblOrderRejectReason REA ON REA.ID = R.ReasonID
	LEFT JOIN tblRateType RT ON RT.ID = R.RateTypeID
	LEFT JOIN tblUom U ON U.ID = R.UomID
	ORDER BY EffectiveDate
GO

/***********************************
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
-- Changes:
	- 3.7.31 - 2015/06/19 - KDA - reorder input parameters (move ProviderID down) and add @DriverGroupID parameter
***********************************/
ALTER PROCEDURE [spRetrieveOrdersFinancialCarrier]
(
  @StartDate datetime = NULL
, @EndDate datetime = NULL
, @ShipperID int = -1 -- all
, @CarrierID int = -1 -- all
, @ProductGroupID int = -1 -- all 
, @DriverGroupID int = -1 -- all
, @OriginStateID int = -1 -- all 
, @DestStateID int = -1 -- all
, @ProducerID int = -1 -- all
, @BatchID int = NULL  -- either show unbatched (NULL), or the specified batch orders
, @OnlyShipperSettled bit = 0 
) AS BEGIN

	SELECT @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	
	SELECT DISTINCT OE.* 
		, ShipperSettled = cast(CASE WHEN OSS.BatchID IS NOT NULL THEN 1 ELSE 0 END as bit)
		, InvoiceOtherDetailsTSV = dbo.fnOrderCarrierAssessorialDetailsTSV(OE.ID, 0)
		, InvoiceOtherAmountsTSV = dbo.fnOrderCarrierAssessorialAmountsTSV(OE.ID, 0)
	FROM dbo.viewOrder_Financial_Carrier OE
	LEFT JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = OE.ID AND OSS.BatchID IS NOT NULL
	WHERE ID IN (
		SELECT O.ID
		FROM viewOrderBase O
		JOIN tblProduct P ON P.ID = O.ProductID
		JOIN tblOrigin OO ON OO.ID = O.OriginID
		JOIN tblDestination D ON D.ID = O.DestinationID
		LEFT JOIN tblOrderSettlementCarrier OSC ON OSC.OrderID = O.ID
		WHERE O.StatusID IN (4)  
		  AND (@ShipperID=-1 OR O.CustomerID=@ShipperID) 
		  AND (@CarrierID=-1 OR O.CarrierID=@CarrierID) 
		  AND (@ProductGroupID=-1 OR P.ProductGroupID=@ProductGroupID) 
		  AND (@DriverGroupID=-1 OR O.DriverGroupID=@DriverGroupID) 
		  AND (@OriginStateID=-1 OR OO.StateID=@OriginStateID) 
		  AND (@DestStateID=-1 OR D.StateID=@DestStateID) 
		  AND (@ProducerID=-1 OR O.ProducerID=@ProducerID) 
		  AND (@StartDate IS NULL OR O.OrderDate >= @StartDate) 
		  AND (@EndDate IS NULL OR O.OrderDate <= @EndDate)
		  AND ((@BatchID IS NULL AND OSC.BatchID IS NULL) OR OSC.BatchID = @BatchID)
	)
	  AND (@OnlyShipperSettled = 0 OR OSS.BatchID IS NOT NULL)
	ORDER BY OE.OriginDepartTimeUTC
END

GO

COMMIT
SET NOEXEC OFF