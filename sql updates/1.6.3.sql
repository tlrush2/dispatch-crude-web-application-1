/* increase Origin.Name from varchar(30) -> varchar(50)
   add tblTrailer.CIDNumber, SIDNumber
   add tblTruck.CIDNumber, SIDNumber
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '1.6.2', @NewVersion = '1.6.3'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

/* update the Origin.Name field from varchar(30) -> varchar(50) */
ALTER TABLE dbo.tblOrigin
	DROP CONSTRAINT FK_tblOrigin_tblProducer
GO
ALTER TABLE dbo.tblProducer SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrigin
	DROP CONSTRAINT FK_tblOrigin_tblOriginTypes
GO
ALTER TABLE dbo.tblOriginType SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrigin
	DROP CONSTRAINT FK_tblOrigin_tblTicketType
GO
ALTER TABLE dbo.tblTicketType SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrigin
	DROP CONSTRAINT FK_tblOrigin_tblStates
GO
ALTER TABLE dbo.tblState SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrigin
	DROP CONSTRAINT FK_tblOrigin_tblRegion
GO
ALTER TABLE dbo.tblRegion SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrigin
	DROP CONSTRAINT FK_tblOrigin_tblOperator
GO
ALTER TABLE dbo.tblOperator SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrigin
	DROP CONSTRAINT FK_tblOrigin_tblPumper
GO
ALTER TABLE dbo.tblPumper SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrigin
	DROP CONSTRAINT DF_tblOrigin_Active
GO
ALTER TABLE dbo.tblOrigin
	DROP CONSTRAINT DF_tblOrigin_TankTypeID
GO
ALTER TABLE dbo.tblOrigin
	DROP CONSTRAINT DF_tblOrigin_CreateDate
GO
CREATE TABLE dbo.Tmp_tblOrigin
	(
	ID int NOT NULL IDENTITY (1, 1),
	OriginTypeID int NOT NULL,
	Name varchar(50) NOT NULL,
	Address varchar(50) NULL,
	City varchar(30) NULL,
	StateID int NULL,
	Zip varchar(50) NULL,
	wellAPI varchar(20) NULL,
	LAT varchar(20) NULL,
	LON varchar(20) NULL,
	OperatorID int NULL,
	PumperID int NULL,
	County varchar(25) NULL,
	LeaseName varchar(35) NULL,
	LeaseNum varchar(30) NULL,
	TicketTypeID int NOT NULL,
	RegionID int NULL,
	TotalDepth int NULL,
	SpudDate smalldatetime NULL,
	FieldName varchar(25) NULL,
	NDICFileNum varchar(10) NULL,
	CTBNum nchar(10) NULL,
	Active bit NOT NULL,
	CustomerID int NULL,
	ProducerID int NULL,
	TankTypeID int NULL,
	CreateDate smalldatetime NULL,
	CreatedByUser varchar(100) NULL,
	LastChangeDate smalldatetime NULL,
	LastChangedByUser varchar(100) NULL,
	BarrelsPerInch decimal(9, 6) NULL,
	TaxRate smallmoney NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_tblOrigin SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_tblOrigin ADD CONSTRAINT
	DF_tblOrigin_Active DEFAULT ((1)) FOR Active
GO
ALTER TABLE dbo.Tmp_tblOrigin ADD CONSTRAINT
	DF_tblOrigin_TankTypeID DEFAULT ((1)) FOR TankTypeID
GO
ALTER TABLE dbo.Tmp_tblOrigin ADD CONSTRAINT
	DF_tblOrigin_CreateDate DEFAULT (getdate()) FOR CreateDate
GO
SET IDENTITY_INSERT dbo.Tmp_tblOrigin ON
GO
IF EXISTS(SELECT * FROM dbo.tblOrigin)
	 EXEC('INSERT INTO dbo.Tmp_tblOrigin (ID, OriginTypeID, Name, Address, City, StateID, Zip, wellAPI, LAT, LON, OperatorID, PumperID, County, LeaseName, LeaseNum, TicketTypeID, RegionID, TotalDepth, SpudDate, FieldName, NDICFileNum, CTBNum, Active, CustomerID, ProducerID, TankTypeID, CreateDate, CreatedByUser, LastChangeDate, LastChangedByUser, BarrelsPerInch, TaxRate)
		SELECT ID, OriginTypeID, Name, Address, City, StateID, Zip, wellAPI, LAT, LON, OperatorID, PumperID, County, LeaseName, LeaseNum, TicketTypeID, RegionID, TotalDepth, SpudDate, FieldName, NDICFileNum, CTBNum, Active, CustomerID, ProducerID, TankTypeID, CreateDate, CreatedByUser, LastChangeDate, LastChangedByUser, BarrelsPerInch, TaxRate FROM dbo.tblOrigin WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_tblOrigin OFF
GO
ALTER TABLE dbo.tblOrigin
	DROP CONSTRAINT FK_tblOrigin_tblOrigin
GO
ALTER TABLE dbo.tblRoute
	DROP CONSTRAINT FK_tblRoute_tblOrigin
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_Origin
GO
DROP TABLE dbo.tblOrigin
GO
EXECUTE sp_rename N'dbo.Tmp_tblOrigin', N'tblOrigin', 'OBJECT' 
GO
ALTER TABLE dbo.tblOrigin ADD CONSTRAINT
	tblOrigin_PrimaryKey PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE NONCLUSTERED INDEX idxOrigin_County ON dbo.tblOrigin
	(
	County
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.tblOrigin ADD CONSTRAINT
	uqOrigin_Name UNIQUE NONCLUSTERED 
	(
	Name
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
DECLARE @v sql_variant 
SET @v = N'Ensure that Origin.Name is unique'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'tblOrigin', N'CONSTRAINT', N'uqOrigin_Name'
GO
CREATE NONCLUSTERED INDEX idxOrigin_Region ON dbo.tblOrigin
	(
	RegionID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxOrigin_State ON dbo.tblOrigin
	(
	StateID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxOrigin_Customer ON dbo.tblOrigin
	(
	CustomerID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxOrigin_OriginType ON dbo.tblOrigin
	(
	OriginTypeID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.tblOrigin ADD CONSTRAINT
	FK_tblOrigin_tblOrigin FOREIGN KEY
	(
	ID
	) REFERENCES dbo.tblOrigin
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrigin ADD CONSTRAINT
	FK_tblOrigin_tblPumper FOREIGN KEY
	(
	PumperID
	) REFERENCES dbo.tblPumper
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrigin ADD CONSTRAINT
	FK_tblOrigin_tblOperator FOREIGN KEY
	(
	OperatorID
	) REFERENCES dbo.tblOperator
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrigin ADD CONSTRAINT
	FK_tblOrigin_tblRegion FOREIGN KEY
	(
	RegionID
	) REFERENCES dbo.tblRegion
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrigin ADD CONSTRAINT
	FK_tblOrigin_tblStates FOREIGN KEY
	(
	StateID
	) REFERENCES dbo.tblState
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrigin ADD CONSTRAINT
	FK_tblOrigin_tblTicketType FOREIGN KEY
	(
	TicketTypeID
	) REFERENCES dbo.tblTicketType
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrigin ADD CONSTRAINT
	FK_tblOrigin_tblOriginTypes FOREIGN KEY
	(
	OriginTypeID
	) REFERENCES dbo.tblOriginType
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrigin ADD CONSTRAINT
	FK_tblOrigin_tblProducer FOREIGN KEY
	(
	ProducerID
	) REFERENCES dbo.tblProducer
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_Origin FOREIGN KEY
	(
	OriginID
	) REFERENCES dbo.tblOrigin
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrder SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblRoute ADD CONSTRAINT
	FK_tblRoute_tblOrigin FOREIGN KEY
	(
	OriginID
	) REFERENCES dbo.tblOrigin
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblRoute SET (LOCK_ESCALATION = TABLE)
GO

ALTER TABLE tblTrailer ADD CIDNumber varchar(10) NULL
GO
ALTER TABLE tblTrailer ADD SIDNumber varchar(10) NULL
GO
ALTER TABLE tblTruck ADD CIDNumber varchar(10) NULL
GO
ALTER TABLE tblTruck ADD SIDNumber varchar(10) NULL
GO

EXEC _spRefreshAllViews
GO

COMMIT
SET NOEXEC OFF