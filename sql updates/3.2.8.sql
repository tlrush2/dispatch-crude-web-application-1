-- rollback
-- select value from tblsetting where id = 0
/*
	-- change the tblReportColumnDefinition table to use an IDENTITY id column (seeded to 100000) to allow admin entry (of non-system values)
	-- all entries with an ID above 90000 will be considered to be SQL expression columns
	-- add new SQL EXPRESSION column 'Actual Driver Miles' with ID = 900001
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.2.7'
SELECT  @NewVersion = '3.2.8'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

ALTER TABLE dbo.tblReportColumnDefinition
	DROP CONSTRAINT FK_ReportColumnDefinition_ReportDefinition
GO
ALTER TABLE dbo.tblReportDefinition SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblReportColumnDefinition
	DROP CONSTRAINT FK_ReportColumnDefinition_FilterType
GO
ALTER TABLE dbo.tblReportFilterType SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblReportColumnDefinition
	DROP CONSTRAINT DF_ReportColumnDefinition_FilterAllowCustomText
GO
ALTER TABLE dbo.tblReportColumnDefinition
	DROP CONSTRAINT DF_ReportColumnDefinition_AllowedRoles
GO
ALTER TABLE dbo.tblReportColumnDefinition
	DROP CONSTRAINT FK_ReportColumnDefinition_OrderSingleExport
GO
CREATE TABLE dbo.Tmp_tblReportColumnDefinition
	(
	ID int NOT NULL IDENTITY (100000, 1),
	ReportID int NOT NULL,
	DataField varchar(50) NOT NULL,
	Caption varchar(50) NULL,
	DataFormat varchar(255) NULL,
	FilterDataField varchar(50) NULL,
	FilterTypeID int NOT NULL,
	FilterDropDownSql varchar(255) NULL,
	FilterAllowCustomText bit NOT NULL,
	AllowedRoles varchar(1000) NOT NULL,
	OrderSingleExport bit NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_tblReportColumnDefinition SET (LOCK_ESCALATION = TABLE)
GO
GRANT SELECT ON dbo.Tmp_tblReportColumnDefinition TO dispatchcrude_iis_acct  AS dbo
GO
ALTER TABLE dbo.Tmp_tblReportColumnDefinition ADD CONSTRAINT
	DF_ReportColumnDefinition_FilterAllowCustomText DEFAULT ((0)) FOR FilterAllowCustomText
GO
ALTER TABLE dbo.Tmp_tblReportColumnDefinition ADD CONSTRAINT
	DF_ReportColumnDefinition_AllowedRoles DEFAULT ('*') FOR AllowedRoles
GO
ALTER TABLE dbo.Tmp_tblReportColumnDefinition ADD CONSTRAINT
	FK_ReportColumnDefinition_OrderSingleExport DEFAULT ((0)) FOR OrderSingleExport
GO
SET IDENTITY_INSERT dbo.Tmp_tblReportColumnDefinition ON
GO
IF EXISTS(SELECT * FROM dbo.tblReportColumnDefinition)
	 EXEC('INSERT INTO dbo.Tmp_tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport)
		SELECT ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport FROM dbo.tblReportColumnDefinition WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_tblReportColumnDefinition OFF
GO
ALTER TABLE dbo.tblReportColumnDefinitionBaseFilter
	DROP CONSTRAINT FK_ReportColumnDefinitionBaseFilter_ReportColumn
GO
ALTER TABLE dbo.tblUserReportColumnDefinition
	DROP CONSTRAINT FK_UserReportColumnDefinition_ReportColumn
GO
DROP TABLE dbo.tblReportColumnDefinition
GO
EXECUTE sp_rename N'dbo.Tmp_tblReportColumnDefinition', N'tblReportColumnDefinition', 'OBJECT' 
GO
ALTER TABLE dbo.tblReportColumnDefinition ADD CONSTRAINT
	PK_ReportColumnDefinition PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE UNIQUE NONCLUSTERED INDEX udxReportColumnDefinition_Report_DataField ON dbo.tblReportColumnDefinition
	(
	ReportID,
	DataField
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.tblReportColumnDefinition ADD CONSTRAINT
	FK_ReportColumnDefinition_FilterType FOREIGN KEY
	(
	FilterTypeID
	) REFERENCES dbo.tblReportFilterType
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblReportColumnDefinition ADD CONSTRAINT
	FK_ReportColumnDefinition_ReportDefinition FOREIGN KEY
	(
	ReportID
	) REFERENCES dbo.tblReportDefinition
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblUserReportColumnDefinition ADD CONSTRAINT
	FK_UserReportColumnDefinition_ReportColumn FOREIGN KEY
	(
	ReportColumnID
	) REFERENCES dbo.tblReportColumnDefinition
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblUserReportColumnDefinition SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblReportColumnDefinitionBaseFilter ADD CONSTRAINT
	FK_ReportColumnDefinitionBaseFilter_ReportColumn FOREIGN KEY
	(
	ReportColumnID
	) REFERENCES dbo.tblReportColumnDefinition
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblReportColumnDefinitionBaseFilter SET (LOCK_ESCALATION = TABLE)
GO

SET IDENTITY_INSERT  tblReportColumnDefinition ON
INSERT INTO tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport)
	SELECT 90001, 1, 'DestTruckMileage-OriginTruckMileage',	'Actual Driver Miles', NULL, NULL, 0, NULL, 0, '*',1
SET IDENTITY_INSERT  tblReportColumnDefinition ON

COMMIT
SET NOEXEC OFF