/* remove redundant Arrive/Depart TimeZone fields (just OriginTimeZone/DestTimeZone now)
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.1.15'
SELECT  @NewVersion = '2.1.16'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

/***********************************/
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return the Local Arrive|Depart Times + their respective TimeZone abbreviations
/***********************************/
ALTER VIEW [dbo].[viewOrderLocalDates] AS
SELECT O.*
, dbo.fnUTC_to_Local(O.OriginArriveTimeUTC, O.OriginTimeZoneID, O.OriginUseDST) AS OriginArriveTime
, dbo.fnUTC_to_Local(O.OriginDepartTimeUTC, O.OriginTimeZoneID, O.OriginUseDST) AS OriginDepartTime
, dbo.fnUTC_to_Local(O.DestArriveTimeUTC, O.DestTimeZoneID, O.DestUseDST) AS DestArriveTime
, dbo.fnUTC_to_Local(O.DestDepartTimeUTC, O.DestTimeZoneID, O.DestUseDST) AS DestDepartTime
, dbo.fnTimeZoneAbbrev(O.OriginArriveTimeUTC, O.OriginTimeZoneID, O.OriginUseDST) AS OriginTimeZone
, dbo.fnTimeZoneAbbrev(O.DestArriveTimeUTC, O.DestTimeZoneID, O.DestUseDST) AS DestTimeZone
, DATEDIFF(minute, O.OriginDepartTimeUTC, O.DestArriveTimeUTC) AS TransitMinutes
FROM viewOrder O

GO

EXEC _spRefreshAllViews
GO

COMMIT
SET NOEXEC OFF