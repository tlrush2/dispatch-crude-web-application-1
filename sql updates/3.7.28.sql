-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.7.25.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.7.25.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0
-- backup database [dispatchcrude.dev] to disk = 'C:\Program Files\Microsoft SQL Server\MSSQL10_50.SQLEXPRESSDEV02\MSSQL\Backup\3.7.23-gsm.bak'

SET NOEXEC OFF
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.7.27'
SELECT  @NewVersion = '3.7.28'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'Settlement Module: Add Driver | Driver Group criteria to all Carrier Rates'
	UNION SELECT @NewVersion, 1, 'Settlement Module: Add Producer criteria to Carrier Wait & Reject Rates'
	UNION SELECT @NewVersion, 1, 'Settlement Module: Separate Carrier Min Settlement Units & Settlement Units into Best-Match rate pages'
	UNION SELECT @NewVersion, 1, 'Settlement Module: Add Min & Max Units Wait Fee Parameter limits'
	UNION SELECT @NewVersion, 1, 'Settlement Module: Allow negative value Assessorial Rates to provide DISCOUNT capability'
	UNION SELECT @NewVersion, 1, 'Order Approval Module: Add Approval page with support for overriding | omitting Origin | Destination Wait Fee minutes during new Approval Process (prior to Settlement)'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

CREATE TABLE tblDriverGroup
(
	ID int IDENTITY(1,1) NOT NULL CONSTRAINT PK_DriverGroup PRIMARY KEY
	, Name varchar(50) NOT NULL
	, CreateDateUTC smalldatetime NULL CONSTRAINT DF_DriverGroup_CreateDateUTC DEFAULT (getutcdate())
	, CreatedByUser varchar(100) NULL
	, LastChangeDateUTC smalldatetime NULL
	, LastChangedByUser varchar(100) NULL
	, DeleteDateUTC smalldatetime NULL
	, DeletedByUser varchar(100) NULL,
 ) 
GO
GRANT SELECT, INSERT, UPDATE ON tblDriverGroup TO role_iis_acct
GO

ALTER TABLE tblDriver ADD DriverGroupID int NULL CONSTRAINT FK_Driver_DriverGroup FOREIGN KEY REFERENCES tblDriverGroup(ID)
GO

ALTER TABLE tblCarrierAssessorialRate ADD DriverID int NULL 
ALTER TABLE tblCarrierAssessorialRate ADD DriverGroupID int NULL

CREATE INDEX idxCarrierAssessorialRate_Driver ON tblCarrierAssessorialRate(DriverID)
GO
CREATE INDEX idxCarrierAssessorialRate_DriverGroup ON tblCarrierAssessorialRate(DriverGroupID)
GO

EXEC _spDropIndex 'udxCarrierAssessorialRate_Main'
EXEC _spDropIndex 'idxCarrierAssessorialRate_Operator'
GO
ALTER TABLE tblCarrierAssessorialRate DROP CONSTRAINT FK_CarrierAssessorialRate_Operator
ALTER TABLE tblCarrierAssessorialRate DROP COLUMN OperatorID
GO
CREATE UNIQUE CLUSTERED INDEX udxCarrierAssessorialRate_Main ON tblCarrierAssessorialRate
(
	TypeID ASC,
	ShipperID ASC,
	CarrierID ASC,
	ProductGroupID ASC,
	DriverID,
	DriverGroupID,
	OriginID ASC,
	DestinationID ASC,
	OriginStateID ASC,
	DestStateID ASC,
	RegionID ASC,
	ProducerID ASC,
	EffectiveDate ASC
)
GO

/*****************************************
-- Date Created: 19 Jan 2015
-- Author: Kevin Alons
-- Purpose: return the tblOrder table witha Local OrderDate field added
-- Changes:
	-- 5/17/2015 - KDA - add local DeliverDate field
	-- 3.7.28 - 2015/06/18 - add DriverGroupID field
*****************************************/
ALTER VIEW viewOrderBase AS
	SELECT O.*
		, OriginStateID = OO.StateID
		, OriginRegionID = OO.RegionID
		, DestStateID = D.StateID
		, DestRegionID = D.RegionID
		, P.ProductGroupID
		, DR.DriverGroupID
		, OriginShipperRegion = OO.ShipperRegion
		, OrderDate = cast(dbo.fnUTC_to_Local(O.OriginDepartTimeUTC, OO.TimeZoneID, OO.UseDST) as date) 
		, DeliverDate = cast(dbo.fnUTC_to_Local(O.DestDepartTimeUTC, D.TimeZoneID, D.UseDST) as date) 
	FROM tblOrder O
	JOIN tblOrigin OO ON OO.ID = O.OriginID
	JOIN tblDestination D ON D.ID = O.DestinationID
	LEFT JOIN tblProduct P ON P.ID = O.ProductID
	LEFT JOIN tblDriver DR ON DR.ID = O.DriverID
GO

/***********************************
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Order records with "translated friendly" values for FK relationships
-- Changes:
	-- 3.7.28 - 2015/06/18 - KDA - remove explicit columns not defined in "viewOrderBase"
***********************************/
ALTER VIEW viewOrder AS
SELECT O.*
	-- GENERATED orders that were GAUGED to COMPLETION or SKIPPED will show "Gauger [gauger status]"
	, PrintStatus = CASE WHEN O.StatusID = -10 AND GOS.IsComplete = 1 THEN 'Gauger ' + GOS.Name ELSE OPS.OrderStatus END
FROM (
	SELECT O.*
	, Origin = vO.Name 
	, OriginFull = vO.FullName 
	, OriginState = vO.State 
	, OriginStateAbbrev = vO.StateAbbrev 
	, OriginStation = vO.Station
	, OriginCounty = vO.County 
	, vO.LeaseName
	, vO.LeaseNum
	, OriginRegion = vO.Region
	, OriginCountryID = vO.CountryID
	, OriginCountry = vO.Country
	, OriginCountryShort = vO.CountryShort
	, OriginLegalDescription = vO.LegalDescription 
	, OriginNDIC = vO.NDICFileNum 
	, OriginNDM = vO.NDM 
	, OriginCA = vO.CA 
	, OriginTimeZoneID = vO.TimeZoneID 
	, OriginUseDST = vO.UseDST 
	, vO.H2S
	, Destination = vD.Name 
	, DestinationFull = vD.FullName 
	, DestinationState = vD.State 
	, DestinationStateAbbrev = vD.StateAbbrev 
	, DestinationTypeID = vD.ID
	, vD.DestinationType
	, DestStation = vD.Station 
	, DestTimeZoneID = vD.TimeZoneID 
	, DestUseDST = vD.UseDST 
	, DestCountryID = vD.CountryID
	, DestCountry = vD.Country
	, DestCountryShort = vD.CountryShort
	, Customer = C.Name 
	, Carrier = CA.Name 
	, CarrierType = CT.Name 
	, OS.OrderStatus
	, OS.StatusNum
	, Driver = D.FullName 
	, DriverFirst = D.FirstName 
	, DriverLast = D.LastName 
	, Truck = TRU.FullName 
	, Trailer = TR1.FullName 
	, Trailer2 = TR2.FullName 
	, P.PriorityNum
	, TicketType = TT.Name 
	, DestTicketTypeID = vD.TicketTypeID 
	, DestTicketType = vD.TicketType 
	, Operator = OP.Name 
	, Producer = PR.Name 
	, Pumper = PU.FullName 
	, DriverNumber = D.IDNumber
	, CarrierNumber = CA.IDNumber
	, CarrierTypeID = CA.CarrierTypeID
	, TruckNumber = TRU.IDNumber 
	, TrailerNumber = TR1.IDNumber 
	, Trailer2Number = TR2.IDNumber 
	, Product = PRO.Name
	, ProductShort = PRO.ShortName 
	, PRO.ProductGroup
	, OriginUOM = OUom.Name 
	, OriginUomShort = OUom.Abbrev 
	, OriginTankID_Text = CASE WHEN ORT.TankNum = '*' THEN '*' + isnull(O.OriginTankNum, '?') ELSE ORT.TankNum END 
	, DestUOM = DUom.Name 
	, DestUomShort = DUom.Abbrev 
	, Active = cast((CASE WHEN O.DeleteDateUTC IS NULL THEN 1 ELSE 0 END) as bit) 
	, IsDeleted = cast((CASE WHEN O.DeleteDateUTC IS NOT NULL THEN 1 ELSE 0 END) as bit) 
	, PickupPrintStatus = PPS.Name 
	, PickupCompleted = PPS.IsCompleted 
	, DeliverPrintStatus = DPS.Name 
	, DeliverCompleted = DPS.IsCompleted 
	, PrintStatusID = CASE WHEN O.StatusID = -9 THEN ISNULL(-GAO.StatusID, O.StatusID)
						   WHEN O.StatusID = 8 AND PPS.IsCompleted = 0 THEN 7
						   WHEN O.StatusID = 3 AND DPS.IsCompleted = 0 THEN 8
						ELSE O.StatusID END 
	, OriginTankText = CASE WHEN O.OriginTankID IS NULL OR ORT.TankNum = '*' THEN O.OriginTankNum ELSE ORT.TankNum END
	, OriginWaitNum = OWR.Num
	, OriginWaitDesc = OWR.Description
	, OriginWaitNumDesc = OWR.NumDesc
	, DestWaitNum = DWR.Num
	, DestWaitDesc = DWR.Description
	, DestWaitNumDesc = DWR.NumDesc
	, RejectNum = ORR.Num
	, RejectDesc = ORR.Description
	, RejectNumDesc = ORR.NumDesc
	, RerouteCount = (SELECT COUNT(1) FROM tblOrderReroute ORE WHERE ORE.OrderID = O.ID)
	, TicketCount = (SELECT COUNT(1) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND OT.DeleteDateUTC IS NULL)
	, TotalMinutes = isnull(OriginMinutes, 0) + ISNULL(DestMinutes, 0)
	, GaugerStatusID = GAO.StatusID
	, GaugerID = GAO.GaugerID
	FROM dbo.viewOrderBase O
	JOIN dbo.tblOrderStatus AS OS ON OS.ID = O.StatusID
	LEFT JOIN dbo.viewOrigin vO ON vO.ID = O.OriginID
	LEFT JOIN dbo.viewDestination vD ON vD.ID = O.DestinationID
	LEFT JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
	LEFT JOIN dbo.tblCarrier CA ON CA.ID = O.CarrierID
	LEFT JOIN dbo.tblCarrierType CT ON CT.ID = CA.CarrierTypeID
	LEFT JOIN dbo.viewDriverBase D ON D.ID = O.DriverID
	LEFT JOIN dbo.viewTruck TRU ON TRU.ID = O.TruckID
	LEFT JOIN dbo.viewTrailer TR1 ON TR1.ID = O.TrailerID
	LEFT JOIN dbo.viewTrailer TR2 ON TR2.ID = O.Trailer2ID
	LEFT JOIN dbo.tblPriority P ON P.ID = O.PriorityID
	LEFT JOIN dbo.tblTicketType TT ON TT.ID = O.TicketTypeID
	LEFT JOIN dbo.tblOriginTank ORT ON ORT.ID = O.OriginTankID
	LEFT JOIN dbo.tblOperator OP ON OP.ID = O.OperatorID
	LEFT JOIN dbo.viewPumper PU ON PU.ID = O.PumperID
	LEFT JOIN dbo.tblProducer PR ON PR.ID = O.ProducerID
	LEFT JOIN dbo.viewProduct PRO ON PRO.ID = O.ProductID
	LEFT JOIN dbo.tblUom OUom ON OUom.ID = O.OriginUomID
	LEFT JOIN dbo.tblUom DUom ON DUom.ID = O.DestUomID
	LEFT JOIN dbo.tblPrintStatus PPS ON PPS.ID = O.PickupPrintStatusID
	LEFT JOIN dbo.tblPrintStatus DPS ON DPS.ID = O.DeliverPrintStatusID
	LEFT JOIN dbo.viewOriginWaitReason OWR ON OWR.ID = O.OriginWaitReasonID
	LEFT JOIN dbo.viewDestinationWaitReason DWR ON DWR.ID = O.DestWaitReasonID
	LEFT JOIN dbo.viewOrderRejectReason ORR ON ORR.ID = O.RejectReasonID
	LEFT JOIN dbo.tblGaugerOrder GAO ON GAO.OrderID = O.ID
	LEFT JOIN dbo.viewGaugerBase G ON G.ID = GAO.GaugerID
) O
LEFT JOIN dbo.viewOrderPrintStatus OPS ON OPS.ID = O.PrintStatusID
LEFT JOIN dbo.tblGaugerOrderStatus GOS ON GOS.ID = O.GaugerStatusID

GO

/***********************************/
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return the Local Arrive|Depart Times + their respective TimeZone abbreviations
-- Changes:
/***********************************/
ALTER VIEW viewOrderLocalDates AS
SELECT O.*
	, dbo.fnUTC_to_Local(O.OriginArriveTimeUTC, O.OriginTimeZoneID, O.OriginUseDST) AS OriginArriveTime
	, dbo.fnUTC_to_Local(O.OriginDepartTimeUTC, O.OriginTimeZoneID, O.OriginUseDST) AS OriginDepartTime
	, dbo.fnUTC_to_Local(O.DestArriveTimeUTC, O.DestTimeZoneID, O.DestUseDST) AS DestArriveTime
	, dbo.fnUTC_to_Local(O.DestDepartTimeUTC, O.DestTimeZoneID, O.DestUseDST) AS DestDepartTime
	, dbo.fnTimeZoneAbbrev(O.OriginArriveTimeUTC, O.OriginTimeZoneID, O.OriginUseDST) AS OriginTimeZone
	, dbo.fnTimeZoneAbbrev(O.DestArriveTimeUTC, O.DestTimeZoneID, O.DestUseDST) AS DestTimeZone
	, DATEDIFF(minute, O.OriginDepartTimeUTC, O.DestArriveTimeUTC) AS TransitMinutes
	FROM viewOrder O
GO

/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all CarrierAssessorialRate records
-- Changes:
	-- 3.7.28 - 2015/06/18 - KDA - add new columns: move ProducerID column to end (so consistent with all other Best-Match rates/parameters
								 - add DriverID/DriverGroup columns
******************************************************/
ALTER VIEW viewCarrierAssessorialRate AS
	SELECT X.*
		, MaxEffectiveDate = (SELECT min(OrderDate) FROM viewOrderSettlementCarrierAssessorialCharge WHERE AssessorialRateID = X.ID AND BatchID IS NOT NULL)
		, MinEndDate = (SELECT max(OrderDate) FROM viewOrderSettlementCarrierAssessorialCharge WHERE AssessorialRateID = X.ID AND BatchID IS NOT NULL)
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM viewOrderSettlementCarrierAssessorialCharge WHERE AssessorialRateID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblCarrierAssessorialRate XN 
			WHERE XN.TypeID = X.TypeID
			  AND dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverID, X.DriverID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EffectiveDate > X.EffectiveDate)
		, PriorEndDate = (
			SELECT max(XN.EndDate) 
			FROM tblCarrierAssessorialRate XN 
			WHERE XN.TypeID = X.TypeID
			  AND dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverID, X.DriverID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EndDate < X.EffectiveDate)
	FROM tblCarrierAssessorialRate X

GO

EXEC _spRefreshAllViews
GO

/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all ShipperAssessorialRate records
-- Changes:
	-- 3.7.28 - 2015/06/18 - KDA - add new columns: move Producer parameter to end (so consistent with all other Best-Match rates/parameters
******************************************************/
ALTER VIEW viewShipperAssessorialRate AS
	SELECT X.*
		, MaxEffectiveDate = (SELECT MIN(OrderDate) FROM viewOrderSettlementShipperAssessorialCharge WHERE AssessorialRateID = X.ID AND BatchID IS NOT NULL)
		, MinEndDate = (SELECT MAX(OrderDate) FROM viewOrderSettlementShipperAssessorialCharge WHERE AssessorialRateID = X.ID AND BatchID IS NOT NULL)
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM viewOrderSettlementShipperAssessorialCharge WHERE AssessorialRateID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblShipperAssessorialRate XN 
			WHERE XN.TypeID = X.TypeID
			  AND dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EffectiveDate > X.EffectiveDate)
		, PriorEndDate = (
			SELECT max(XN.EndDate) 
			FROM tblShipperAssessorialRate XN 
			WHERE XN.TypeID = X.TypeID
			  AND dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EndDate < X.EffectiveDate)
	FROM tblShipperAssessorialRate X
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier AssessorialRate info for the specified order
-- Changes:
	-- 3.7.28 - 2015/06/18 - KDA - add new columns: move ProducerID parameter to end (so consistent with all other Best-Match rates/parameters
								 - add DriverID/DriverGroup parameters
***********************************/
ALTER FUNCTION fnCarrierAssessorialRates
(
  @StartDate date
, @EndDate date
, @TypeID int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @DriverID int
, @DriverGroupID int
, @OriginID int
, @DestinationID int
, @OriginStateID int
, @DestStateID int
, @RegionID int
, @ProducerID int
, @BestMatchOnly bit = 0
)
RETURNS 
	@ret TABLE (
		ID int
	  , TypeID int
	  , ShipperID int
	  , CarrierID int
	  , ProductGroupID int
	  , DriverID int
	  , DriverGroupID int
	  , OriginID int
	  , DestinationID int
	  , OriginStateID int
	  , DestStateID int
	  , RegionID int
	  , ProducerID int
	  , Rate decimal(18, 10)
	  , RateTypeID int
	  , UomID int
	  , EffectiveDate date
	  , EndDate date
	  , MaxEffectiveDate date
	  , MinEndDate date
	  , NextEffectiveDate date
	  , PriorEndDate date	  
	  , BestMatch bit
	  , Ranking smallmoney
	  , Locked bit
	  , CreateDateUTC datetime
	  , CreatedByUser varchar(100)
	  , LastChangeDateUTC datetime
	  , LastChangedByUser varchar(100)
	)
AS BEGIN
	-- get the raw data (all matched rows)
	DECLARE @src TABLE (ID int, TypeID int, Ranking smallmoney)
	INSERT INTO @src (ID, TypeID, Ranking)
		SELECT ID, TypeID
			, Ranking =	dbo.fnRateRanking(@ShipperID, R.ShipperID, 1024, 0)
					  + dbo.fnRateRanking(@CarrierID, R.CarrierID, 512, 0)
					  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 256, 0)
					  + dbo.fnRateRanking(@DriverID, R.DriverID, 128, 0)
					  + dbo.fnRateRanking(@DriverGroupID, R.DriverGroupID, 64, 1)
					  + dbo.fnRateRanking(@OriginID, R.OriginID, 32, 0)
					  + dbo.fnRateRanking(@DestinationID, R.DestinationID, 16, 0)
					  + dbo.fnRateRanking(@OriginStateID, R.OriginStateID, 8, 0)
					  + dbo.fnRateRanking(@DestStateID, R.DestStateID, 4, 0)
					  + dbo.fnRateRanking(@RegionID, R.RegionID, 2, 0)
					  + dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 1)
		FROM dbo.viewCarrierAssessorialRate R
		WHERE coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(R.ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@DriverID, 0), R.DriverID, 0) = coalesce(DriverID, nullif(@DriverID, 0), 0)
		  AND coalesce(nullif(@DriverGroupID, 0), R.DriverGroupID, 0) = coalesce(DriverGroupID, nullif(@DriverGroupID, 0), 0)
		  AND coalesce(nullif(@OriginID, 0), R.OriginID, 0) = coalesce(R.OriginID, nullif(@OriginID, 0), 0)
		  AND coalesce(nullif(@DestinationID, 0), R.DestinationID, 0) = coalesce(R.DestinationID, nullif(@DestinationID, 0), 0)
		  AND coalesce(nullif(@OriginStateID, 0), R.OriginStateID, 0) = coalesce(R.OriginStateID, nullif(@OriginStateID, 0), 0)
		  AND coalesce(nullif(@DestStateID, 0), R.DestStateID, 0) = coalesce(R.DestStateID, nullif(@DestStateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(R.RegionID, nullif(@RegionID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(R.ProducerID, nullif(@ProducerID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
		  AND (nullif(@TypeID, 0) IS NULL OR TypeID = @TypeID)

	-- return the rate data for the best-match rates 
	INSERT INTO @ret (ID, TypeID, ShipperID, CarrierID, ProductGroupID, DriverID, DriverGroupID, OriginID, DestinationID, OriginStateID, DestStateID, RegionID, ProducerID, Rate, RateTypeID, UomID, EffectiveDate, EndDate
			, MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate, BestMatch, Ranking, Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser)
		SELECT CAR.ID, TypeID, ShipperID, CarrierID, ProductGroupID, DriverID, DriverGroupID, OriginID, DestinationID, OriginStateID, DestStateID, RegionID, ProducerID, Rate, RateTypeID, UomID, EffectiveDate, EndDate
			, MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate, BestMatch, Ranking, Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser
		FROM viewCarrierAssessorialRate CAR
		JOIN (
			SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.TypeID IS NULL THEN 0 ELSE 1 END as bit)
			FROM @src S
			LEFT JOIN (
				SELECT TypeID, Ranking = MAX(Ranking)
				FROM @src
				WHERE @StartDate = ISNULL(@EndDate, @StartDate)
				  AND Ranking % 1 = 0.01 * 11  -- ensure some type of match occurred on all criteria choices
				GROUP BY TypeID
			) X ON X.TypeID = S.TypeID AND X.Ranking = S.Ranking
		) X ON X.ID = CAR.ID
		WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
		
	RETURN
END
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier AssessorialRate rows for the specified criteria
-- Changes:
	-- 3.7.28 - 2015/06/18 - KDA - add new columns: move ProducerID parameter to end (so consistent with all other Best-Match rates/parameters
								 - add DriverID/DriverGroup parameters
**********************************/
ALTER FUNCTION fnCarrierAssessorialRatesDisplay
(  
  @StartDate date
, @EndDate date
, @TypeID int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @DriverID int
, @DriverGroupID int
, @OriginID int
, @DestinationID int
, @OriginStateID int
, @DestStateID int
, @RegionID int
, @ProducerID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.DriverID, R.DriverGroupID, R.OriginID, R.DestinationID, R.OriginStateID, R.DestStateID, R.RegionID, R.ProducerID, R.TypeID, R.Rate, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Type = RT.Name
		, Shipper = S.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, Driver = DR.FullName
		, DriverGroup = DG.Name
		, Origin = O.Name
		, OriginFull = O.FullName
		, Destination = D.Name
		, DestinationFull = D.FullName
		, OriginState = OS.FullName
		, OriginStateAbbrev = OS.Abbreviation
		, DestState = DS.FullName
		, DestStateAbbrev = DS.Abbreviation
		, Region = RE.Name
		, Producer = PR.Name
		, RateType = RT.Name
		, Uom = U.Name
		, UomShort = U.Abbrev
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch, Ranking
	FROM dbo.fnCarrierAssessorialRates(@StartDate, @EndDate, @TypeID, @ShipperID, @CarrierID, @ProductGroupID, @DriverID, @DriverGroupID, @OriginID, @DestinationID, @OriginStateID, @DestStateID, @RegionID, @ProducerID, 0) R
	JOIN tblAssessorialRateType RT ON RT.ID = R.TypeID
	LEFT JOIN tblCustomer S ON S.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN viewDriver DR ON DR.ID = R.DriverID
	LEFT JOIN tblDriverGroup DG ON DG.ID = R.DriverGroupID
	LEFT JOIN viewOrigin O ON O.ID = R.OriginID
	LEFT JOIN viewDestination D ON D.ID = R.DestinationID
	LEFT JOIN tblState OS ON OS.ID = R.OriginStateID
	LEFT JOIN tblState DS ON DS.ID = R.DestStateID
	LEFT JOIN tblRegion RE ON RE.ID = R.RegionID
	LEFT JOIN tblProducer PR ON PR.ID = R.ProducerID
	LEFT JOIN tblUom U ON U.ID = R.UomID
	ORDER BY EffectiveDate
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier AssessorialRate info for the specified order
-- Changes:
	-- 3.7.28 - 2015/06/18 - KDA - add new columns: add DriverID/DriverGroupID parameters, move ProducerID parameter to end (so consistent with all other Best-Match rates/parameters
***********************************/
ALTER FUNCTION fnOrderCarrierAssessorialRates(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, TypeID, Rate, RateTypeID, UomID
	FROM dbo.viewOrder O
	CROSS APPLY dbo.fnCarrierAssessorialRates(O.OrderDate, null, null, O.CustomerID, O.CarrierID, O.ProductGroupID, O.DriverID, O.DriverGroupID, O.OriginID, O.DestinationID, O.OriginStateID, O.DestStateID, O.OriginRegionID, O.ProducerID, 1) R
	WHERE O.ID = @ID
	  AND ((R.TypeID = 1 AND O.ChainUp = 1)
		OR (R.TypeID = 2 AND O.RerouteCount > 0)
		OR (R.TypeID = 3 AND O.TicketCount > 1)
		OR (R.TypeID = 4 AND O.H2S = 1)
		OR R.TypeID > 4)
GO

ALTER TABLE tblCarrierDestinationWaitRate ADD ProducerID int NULL 
ALTER TABLE tblCarrierDestinationWaitRate ADD DriverID int NULL 
ALTER TABLE tblCarrierDestinationWaitRate ADD DriverGroupID int NULL
GO
EXEC _spDropIndex 'udxCarrierDestinationWaitRate_Main'
GO
CREATE UNIQUE NONCLUSTERED INDEX udxCarrierDestinationWaitRate_Main ON tblCarrierDestinationWaitRate
(
	ReasonID ASC,
	ShipperID ASC,
	CarrierID ASC,
	ProductGroupID ASC,
	DriverID,
	DriverGroupID,
	DestinationID ASC,
	StateID ASC,
	RegionID ASC,
	ProducerID,
	EffectiveDate ASC
)
GO
CREATE INDEX idxCarrierDestinationWaitRate_Driver ON tblCarrierDestinationWaitRate(DriverID)
CREATE INDEX idxCarrierDestinationWaitRate_DriverGroup ON tblCarrierDestinationWaitRate(DriverGroupID)
CREATE INDEX idxCarrierDestinationWaitRate_Producer ON tblCarrierDestinationWaitRate(ProducerID)
GO

/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all CarrierDestinationWaitRate records
-- Changes:
	- 3.7.28 - 13 Jun 2015 - KDA - Add DriverID | DriverGroupID | ProducerID
******************************************************/
ALTER VIEW viewCarrierDestinationWaitRate AS
	SELECT X.*
		, MaxEffectiveDate = (SELECT Min(OrderDate) FROM tblOrderSettlementCarrier WHERE DestinationWaitRateID = X.ID AND BatchID IS NOT NULL)
		, MinEndDate = (SELECT Max(OrderDate) FROM tblOrderSettlementCarrier WHERE DestinationWaitRateID = X.ID AND BatchID IS NOT NULL)
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementCarrier WHERE DestinationWaitRateID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate)
			FROM tblCarrierDestinationWaitRate XN 
			WHERE isnull(XN.ShipperID, 0) = isnull(X.ShipperID, 0) 
			  AND isnull(XN.CarrierID, 0) = isnull(X.CarrierID, 0) 
			  AND isnull(XN.ProductGroupID, 0) = isnull(X.ProductGroupID, 0) 
			  AND isnull(XN.DriverID, 0) = isnull(X.DriverID, 0) 
			  AND isnull(XN.DriverGroupID, 0) = isnull(X.DriverGroupID, 0) 
			  AND isnull(XN.DestinationID, 0) = isnull(X.DestinationID, 0) 
			  AND isnull(XN.StateID, 0) = isnull(X.StateID, 0) 
			  AND isnull(XN.RegionID, 0) = isnull(X.RegionID, 0) 
			  AND isnull(XN.ProducerID, 0) = isnull(X.ProducerID, 0) 
			  AND XN.EffectiveDate > X.EffectiveDate)
		, PriorEndDate = (
			SELECT max(XN.EndDate)
			FROM tblCarrierDestinationWaitRate XN 
			WHERE isnull(XN.ShipperID, 0) = isnull(X.ShipperID, 0) 
			  AND isnull(XN.CarrierID, 0) = isnull(X.CarrierID, 0) 
			  AND isnull(XN.ProductGroupID, 0) = isnull(X.ProductGroupID, 0) 
			  AND isnull(XN.DriverID, 0) = isnull(X.DriverID, 0) 
			  AND isnull(XN.DriverGroupID, 0) = isnull(X.DriverGroupID, 0) 
			  AND isnull(XN.DestinationID, 0) = isnull(X.DestinationID, 0) 
			  AND isnull(XN.StateID, 0) = isnull(X.StateID, 0) 
			  AND isnull(XN.RegionID, 0) = isnull(X.RegionID, 0) 
			  AND isnull(XN.ProducerID, 0) = isnull(X.ProducerID, 0) 
			  AND XN.EndDate < X.EffectiveDate)
	FROM tblCarrierDestinationWaitRate X
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier DestinationWaitRate info for the specified criteria
-- Changes:
	- 3.7.28 - 13 Jun 2015 - KDA - Add DriverID | DriverGroupID | ProducerID
***********************************/
ALTER FUNCTION fnCarrierDestinationWaitRate
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @DriverID int
, @DriverGroupID int
, @DestinationID int
, @StateID int
, @RegionID int
, @ProducerID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID, ReasonID
			, Ranking =	dbo.fnRateRanking(@ReasonID, R.ReasonID, 512, 1)
				  + dbo.fnRateRanking(@ShipperID, R.ShipperID, 256, 0)
				  + dbo.fnRateRanking(@CarrierID, R.CarrierID, 128, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 64, 0)
				  + dbo.fnRateRanking(@DriverID, R.DriverID, 32, 0)
				  + dbo.fnRateRanking(@DriverGroupID, R.DriverGroupID, 16, 1)
				  + dbo.fnRateRanking(@DestinationID, R.DestinationID, 8, 0)
				  + dbo.fnRateRanking(@StateID, R.StateID, 4, 0)
				  + dbo.fnRateRanking(@RegionID, R.RegionID, 2, 0)
				  + dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 0)
		FROM  dbo.viewCarrierDestinationWaitRate R
		WHERE coalesce(nullif(@ReasonID, 0), R.ReasonID, 0) = coalesce(ReasonID, nullif(@ReasonID, 0), 0)
		  AND coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@DriverID, 0), R.DriverID, 0) = coalesce(DriverID, nullif(@DriverID, 0), 0)
		  AND coalesce(nullif(@DriverGroupID, 0), R.DriverGroupID, 0) = coalesce(DriverGroupID, nullif(@DriverGroupID, 0), 0)
		  AND coalesce(nullif(@DestinationID, 0), R.DestinationID, 0) = coalesce(DestinationID, nullif(@DestinationID, 0), 0)
		  AND coalesce(nullif(@StateID, 0), R.StateID, 0) = coalesce(StateID, nullif(@StateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(ProducerID, nullif(@ProducerID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	),
	cte2 AS
	(
		SELECT ID, S.ReasonID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT ReasonID = ISNULL(ReasonID, 0), Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 10  -- ensure some type of match occurred on all criteria choices
			GROUP BY ReasonID
		) X ON X.ReasonID = isnull(S.ReasonID, 0) AND X.Ranking = S.Ranking
	), 
	cte3 AS
	(	-- when both ReasonID = NULL AND ReasonID = @ReasonID records are BestMatch = 1, favor the ReasonID = @ReasonID record
		SELECT ID, X.ReasonID, X.Ranking, BestMatch = CASE WHEN BEST.ReasonID IS NULL THEN X.BestMatch ELSE 0 END
		FROM cte2 X
		LEFT JOIN (
			SELECT TOP 1 ReasonID FROM cte2 WHERE ReasonID = @ReasonID AND BestMatch = 1 ORDER BY Ranking DESC
		) BEST ON X.ReasonID IS NULL AND X.BestMatch = 1
	)
	
	SELECT TOP (CASE WHEN @BestMatchOnly = 1 THEN 1 ELSE 10000 END) R.ID, R.ReasonID, ShipperID, CarrierID, ProductGroupID, DriverID, DriverGroupID, DestinationID, StateID, RegionID, ProducerID
		, Rate, EffectiveDate, EndDate
	  , MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate
	  , BestMatch, Ranking
	  , Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewCarrierDestinationWaitRate R
	JOIN cte3 X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
	ORDER BY Ranking DESC
)
GO

/***********************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier DestinationWaitRate rows for the specified criteria
-- Changes:
	- 3.7.28 - 13 Jun 2015 - KDA - Add DriverID | DriverGroupID | ProducerID
***********************************/
ALTER FUNCTION fnCarrierDestinationWaitRatesDisplay
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @DriverID int
, @DriverGroupID int
, @DestinationID int
, @StateID int
, @RegionID int
, @ProducerID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ReasonID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.DriverID, R.DriverGroupID, R.DestinationID, R.StateID, R.RegionID, R.ProducerID, R.Rate, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Shipper = SH.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, Driver = DR.FullName
		, DriverGroup = DG.Name
		, Destination = D.Name
		, DestinationFull = D.FullName
		, State = S.FullName
		, StateAbbrev = S.Abbreviation
		, Region = REG.Name
		, Producer = P.Name
		, Reason = REA.Description
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
		, Ranking
	FROM dbo.fnCarrierDestinationWaitRate(@StartDate, @EndDate, @ReasonID, @ShipperID, @CarrierID, @ProductGroupID, @DriverID, @DriverGroupID, @DestinationID, @StateID, @RegionID, @ProducerID, 0) R
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN viewDriver DR ON DR.ID = R.DriverID
	LEFT JOIN tblDriverGroup DG ON DG.ID = R.DriverGroupID
	LEFT JOIN viewDestination D ON D.ID = R.DestinationID
	LEFT JOIN tblState S ON S.ID = R.StateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblProducer P ON P.ID = R.ProducerID
	LEFT JOIN tblDestinationWaitReason REA ON REA.ID = R.ReasonID
	ORDER BY EffectiveDate
GO
--- rollback
/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier DestinationWaitRate info for the specified order
-- Changes
	-- 3.7.13 - 5/21/15 - KDA - fix bug which prevented missing rate warning when billable minutes existed but no rate matched
							  - add DriverID/DriverGroupID/Producer parameter usage
***********************************/
ALTER FUNCTION fnOrderCarrierDestinationWaitRate(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, Minutes = O.DestMinutes, R.Rate
	FROM viewOrderBase O
	OUTER APPLY dbo.fnCarrierDestinationWaitRate(O.OrderDate, null, O.DestWaitReasonID, O.CustomerID, O.CarrierID, O.ProductGroupID, O.DriverID, O.DriverGroupID, O.DestinationID, O.DestStateID, O.DestRegionID, O.ProducerID, 1) R
	WHERE O.ID = @ID
GO

ALTER TABLE tblCarrierFuelSurchargeRate ADD DriverID int NULL 
ALTER TABLE tblCarrierFuelSurchargeRate ADD DriverGroupID int NULL
GO
EXEC _spDropIndex 'udxCarrierFuelSurchargeRate_Main'
GO
CREATE UNIQUE CLUSTERED INDEX udxCarrierFuelSurchargeRate_Main ON tblCarrierFuelSurchargeRate
(
	ShipperID ASC,
	CarrierID ASC,
	ProductGroupID ASC,
	DriverID,
	DriverGroupID,
	EffectiveDate ASC
)
GO
CREATE INDEX idxCarrierFuelSurchargeRate_Driver ON tblCarrierFuelSurchargeRate(DriverID)
CREATE INDEX idxCarrierFuelSurchargeRate_DriverGroup ON tblCarrierFuelSurchargeRate(DriverGroupID)
GO

ALTER TABLE tblCarrierOrderRejectRate ADD ProducerID int NULL 
ALTER TABLE tblCarrierOrderRejectRate ADD DriverID int NULL 
ALTER TABLE tblCarrierOrderRejectRate ADD DriverGroupID int NULL
GO
EXEC _spDropIndex 'udxCarrierOrderRejectRate_Main'
GO
CREATE UNIQUE NONCLUSTERED INDEX udxCarrierOrderRejectRate_Main ON tblCarrierOrderRejectRate
(
	ReasonID ASC,
	ShipperID ASC,
	CarrierID ASC,
	ProductGroupID ASC,
	DriverID,
	DriverGroupID,
	OriginID ASC,
	StateID ASC,
	RegionID ASC,
	ProducerID,
	EffectiveDate ASC
)
GO
CREATE INDEX idxCarrierOrderRejectRate_Driver ON tblCarrierOrderRejectRate(DriverID)
CREATE INDEX idxCarrierOrderRejectRate_DriverGroup ON tblCarrierOrderRejectRate(DriverGroupID)
CREATE INDEX idxCarrierOrderRejectRate_Producer ON tblCarrierOrderRejectRate(ProducerID)
GO

ALTER TABLE tblCarrierOriginWaitRate ADD ProducerID int NULL 
ALTER TABLE tblCarrierOriginWaitRate ADD DriverID int NULL 
ALTER TABLE tblCarrierOriginWaitRate ADD DriverGroupID int NULL
GO
EXEC _spDropIndex 'udxCarrierOriginWaitRate_Main'
GO
CREATE UNIQUE NONCLUSTERED INDEX udxCarrierOriginWaitRate_Main ON tblCarrierOriginWaitRate
(
	ReasonID ASC,
	ShipperID ASC,
	CarrierID ASC,
	ProductGroupID ASC,
	DriverID,
	DriverGroupID,
	OriginID ASC,
	StateID ASC,
	RegionID ASC,
	ProducerID,
	EffectiveDate ASC
)
GO
CREATE INDEX idxCarrierOriginWaitRate_Driver ON tblCarrierOriginWaitRate(DriverID)
CREATE INDEX idxCarrierOriginWaitRate_DriverGroup ON tblCarrierOriginWaitRate(DriverGroupID)
CREATE INDEX idxCarrierOriginWaitRate_Producer ON tblCarrierOriginWaitRate(ProducerID)
GO

ALTER TABLE tblCarrierRateSheet ADD DriverID int NULL 
ALTER TABLE tblCarrierRateSheet ADD DriverGroupID int NULL
GO
EXEC _spDropIndex 'udxCarrierRateSheet_Main'
GO
CREATE UNIQUE CLUSTERED INDEX udxCarrierRateSheet_Main ON tblCarrierRateSheet
(
	ShipperID ASC,
	CarrierID ASC,
	ProductGroupID ASC,
	DriverID,
	DriverGroupID,
	RegionID ASC,
	OriginStateID ASC,
	DestStateID ASC,
	ProducerID ASC,
	EffectiveDate ASC
)
GO
CREATE INDEX idxCarrierRateSheet_Driver ON tblCarrierRateSheet(DriverID)
CREATE INDEX idxCarrierRateSheet_DriverGroup ON tblCarrierRateSheet(DriverGroupID)
GO

ALTER TABLE tblCarrierRouteRate ADD DriverID int NULL
ALTER TABLE tblCarrierRouteRate ADD DriverGroupID int NULL
GO
EXEC _spDropIndex 'udxCarrierRouteRate_Main'
GO
CREATE UNIQUE CLUSTERED INDEX udxCarrierRouteRate_Main ON tblCarrierRouteRate
(
	ShipperID ASC,
	CarrierID ASC,
	ProductGroupID ASC,
	DriverID,
	DriverGroupID,
	RouteID ASC,
	EffectiveDate ASC
)
GO
CREATE INDEX idxCarrierRouteRate_Driver ON tblCarrierRouteRate(DriverID)
CREATE INDEX idxCarrierRouteRate_DriverGroup ON tblCarrierRouteRate(DriverGroupID)
GO

ALTER TABLE tblShipperDestinationWaitRate ADD ProducerID int NULL 
ALTER TABLE tblShipperOrderRejectRate ADD ProducerID int NULL 
ALTER TABLE tblShipperOriginWaitRate ADD ProducerID int NULL 
GO
CREATE INDEX idxShipperDestinationWaitRate_Producer ON tblShipperDestinationWaitRate(ProducerID)
CREATE INDEX idxShipperOriginWaitRate_Producer ON tblShipperOriginWaitRate(ProducerID)
CREATE INDEX idxShipperOrderRejectRate_Producer ON tblCarrierOrderRejectRate(ProducerID)
GO


ALTER TABLE tblCarrierWaitFeeParameter ADD ProducerID int NULL
CREATE INDEX idxCarrierWaitFeeParameter_Producer ON tblCarrierWaitFeeParameter(ProducerID)
EXEC _spDropIndex 'udxCarrierWaitFeeParameter_Main'
GO
CREATE UNIQUE CLUSTERED INDEX udxCarrierWaitFeeParameter_Main ON tblCarrierWaitFeeParameter
(
	ShipperID ASC,
	CarrierID ASC,
	ProductGroupID ASC,
	OriginStateID ASC,
	DestStateID ASC,
	RegionID ASC,
	ProducerID,
	EffectiveDate ASC
) 
GO 
ALTER TABLE tblCarrierWaitFeeParameter ADD OriginMinBillableMinutes int NULL
ALTER TABLE tblCarrierWaitFeeParameter ADD OriginMaxBillableMinutes int NULL
ALTER TABLE tblCarrierWaitFeeParameter ADD DestMinBillableMinutes int NULL
ALTER TABLE tblCarrierWaitFeeParameter ADD DestMaxBillableMinutes int NULL
GO

EXEC _spRefreshAllViews
GO

/********************************************
-- Date Created: 25 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve the Allocation + Production Units for the specified selection criteria + Date Range
--			done as a function (vs a PROCEDURE) to allow the results to be easily Grouped as needed
-- Changes:
	-- 3.7.28 - 2015/06/18 - KDA - add DriverID/DriverGroupID/Producer parameters
********************************************/
ALTER FUNCTION fnRetrieveAllocationDestinationShipper_ForRange
( 
  @DestinationID int = -1
, @ShipperID int = -1
, @ProductGroupID int = -1
, @EffectiveDate date
, @EndDate date
, @UomID int
, @OnlyForAllocated bit = 0
, @IncludeScheduled bit = 1
) RETURNS @ret TABLE 
(
  DestinationID int
, Destination varchar(100)
, ShipperID int
, Shipper varchar(100)
, ProductGroupID int
, ProductGroup varchar(100)
, UomID int
, Uom varchar(100)
, AllocatedUnits money
, ProductionUnits money
) AS BEGIN
	DECLARE @ADS TABLE (DID int, SID int, PGID int)
	INSERT INTO @ADS
		SELECT DISTINCT DestinationID, ShipperID, ProductGroupID 
		FROM tblAllocationDestinationShipper ADS
		WHERE (@EffectiveDate BETWEEN EffectiveDate AND EndDate
		   OR @EndDate BETWEEN EffectiveDate AND EndDate
		   OR EffectiveDate BETWEEN @EffectiveDate AND @EndDate)
		  AND (@DestinationID = -1 OR DestinationID = @DestinationID)
		  AND (@ShipperID = -1 OR ShipperID = @ShipperID)
		  AND (@ProductGroupID = -1 OR ProductGroupID = @ProductGroupID)

	-- insert the ProductionUnits first (since these will definitely exist, the allocations might not)
	INSERT INTO @ret (DestinationID, ShipperID, ProductGroupID, UomID, AllocatedUnits, ProductionUnits)
		SELECT O.DestinationID, O.CustomerID, O.ProductGroupID, @UomID, 0, SUM(dbo.fnConvertUOM(isnull(OriginNetUnits, 0), OriginUomID, @UomID))
		FROM viewOrderBase O
		JOIN tblDestinationCustomers DC ON DC.DestinationID = O.DestinationID AND DC.CustomerID = O.CustomerID
		LEFT JOIN @ADS ADS ON ADS.DID = O.DestinationID AND ADS.SID = O.CustomerID AND ADS.PGID = O.ProductGroupID
		WHERE isnull(OrderDate, DueDate) BETWEEN @EffectiveDate AND @EndDate
		  AND (@IncludeScheduled = 1 OR OrderDate IS NOT NULL)
		  AND Rejected = 0 AND O.DeleteDateUTC IS NULL
		  AND @DestinationID IN (-1, O.DestinationID)
		  AND @ShipperID IN (-1, O.CustomerID)
		  AND @ProductGroupID IN (-1, O.ProductGroupID)
		  AND (@OnlyForAllocated = 0 OR ADS.DID IS NOT NULL)
		GROUP BY O.DestinationID, O.CustomerID, O.ProductGroupID

	-- ensure a record exists for all Allocation records
	INSERT INTO @ret (DestinationID, ShipperID, ProductGroupID, UomID, AllocatedUnits, ProductionUnits)
		SELECT ADS.DID, ADS.SID, ADS.PGID, @UomID, 0, 0
		FROM @ADS ADS
		LEFT JOIN @ret X ON X.DestinationID = ADS.DID AND X.ShipperID = ADS.SID AND X.ProductGroupID = ADS.PGID
		WHERE X.DestinationID IS NULL
	
	UPDATE @ret
		SET Destination = D.Name
			, Shipper = C.Name
			, ProductGroup = PG.Name
			, Uom = U.Name
	FROM @ret X
	JOIN tblDestination D ON D.ID = X.DestinationID
	JOIN tblCustomer C ON C.ID = X.ShipperID
	JOIN tblProductGroup PG ON PG.ID = X.ProductGroupID
	JOIN tblUom U ON U.ID = X.UomID
	
	-- compute the actual AllocationUnits based on DailyAllocation values
	DECLARE @WorkDate date
	SET @WorkDate = @EffectiveDate
	WHILE @WorkDate <= @EndDate
	BEGIN
		UPDATE @ret SET AllocatedUnits = AllocatedUnits + ADS.Units
		FROM @ret R
		JOIN (
			SELECT DestinationID, ShipperID, ProductGroupID
				, Units = sum(dbo.fnConvertUOM(DailyUnits, UomID, @UomID))
			FROM viewAllocationDestinationShipper
			WHERE @WorkDate BETWEEN EffectiveDate AND EndDate
			GROUP BY DestinationID, ShipperID, ProductGroupID
		) ADS ON ADS.DestinationID = R.DestinationID
				AND ADS.ShipperID = R.ShipperID
				AND ADS.ProductGroupID = R.ProductGroupID
		SET @WorkDate = DATEADD(day, 1, @WorkDate)
	END
	
	RETURN
END
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier WaitFeeParameter info for the specified order
-- Changes:
	-- 3.7.28 - 2015/06/18 - KDA - add Producer parameter
***********************************/
ALTER FUNCTION fnCarrierWaitFeeParameter
(
  @StartDate date
, @EndDate date
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @OriginStateID int
, @DestStateID int
, @RegionID int
, @ProducerID int
, @bestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID
			, Ranking = dbo.fnRateRanking(@ShipperID, R.ShipperID, 64, 0)
				  + dbo.fnRateRanking(@CarrierID, R.CarrierID, 32, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 16, 0)
				  + dbo.fnRateRanking(@OriginStateID, R.OriginStateID, 8, 0)
				  + dbo.fnRateRanking(@DestStateID, R.DestStateID, 4, 0)
				  + dbo.fnRateRanking(@RegionID, R.RegionID, 2, 0)
				  + dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 1)
		FROM  dbo.viewCarrierWaitFeeParameter R
		WHERE coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@OriginStateID, 0), R.OriginStateID, 0) = coalesce(OriginStateID, nullif(@OriginStateID, 0), 0)
		  AND coalesce(nullif(@DestStateID, 0), R.DestStateID, 0) = coalesce(DestStateID, nullif(@DestStateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(ProducerID, nullif(@ProducerID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	)
	
	SELECT R.ID, ShipperID, CarrierID, ProductGroupID, OriginStateID, DestStateID, RegionID, ProducerID, EffectiveDate, EndDate
		, SubUnitID, RoundingTypeID, OriginThresholdMinutes, DestThresholdMinutes, OriginMinBillableMinutes, OriginMaxBillableMinutes, DestMinBillableMinutes, DestMaxBillableMinutes
		, MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate
		, BestMatch, Ranking
		, Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewCarrierWaitFeeParameter R
	JOIN (
		SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 7  -- ensure some type of match occurred on all criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier WaitFeeParameter rows for the specified criteria
-- Changes:
	-- 3.7.28 - 2015/06/18 - KDA - add Producer parameter
***********************************/
ALTER FUNCTION fnCarrierWaitFeeParametersDisplay
(
  @StartDate date
, @EndDate date
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @OriginStateID int
, @DestStateID int
, @RegionID int
, @ProducerID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.OriginStateID, R.DestStateID, R.RegionID, R.ProducerID
		, R.SubUnitID, R.RoundingTypeID, R.OriginThresholdMinutes, R.DestThresholdMinutes
		, R.OriginMinBillableMinutes, R.OriginMaxBillableMinutes, R.DestMinBillableMinutes, R.DestMaxBillableMinutes
		, R.EffectiveDate, R.EndDate, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Shipper = S.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, OriginState = OS.FullName
		, OriginStateAbbrev = OS.Abbreviation
		, DestinationState = DS.FullName
		, DestinationStateAbbrev = DS.Abbreviation
		, Region = REG.Name
		, Producer = PR.Name
		, RoundingType = WFRT.Name
		, SubUnit = WFSU.Name
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch, Ranking
	FROM dbo.fnCarrierWaitFeeParameter(@StartDate, @EndDate, @ShipperID, @CarrierID, @ProductGroupID, @OriginStateID, @DestStateID, @RegionID, @ProducerID, 0) R 
	JOIN tblWaitFeeRoundingType WFRT ON WFRT.ID = R.RoundingTypeID
	JOIN tblWaitFeeSubUnit WFSU ON WFSU.ID = R.SubUnitID
	LEFT JOIN tblCustomer S ON S.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblState OS ON OS.ID = R.OriginStateID
	LEFT JOIN tblState DS ON DS.ID = R.DestStateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblProducer PR ON PR.ID = R.ProducerID
	ORDER BY EffectiveDate
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier WaitFeeParameter info for the specified order
-- Changes:
	-- 3.7.28 - 2015/06/18 - KDA - add Producer parameter
***********************************/
ALTER FUNCTION fnOrderCarrierWaitFeeParameter(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, SubUnitID, RoundingTypeID, OriginThresholdMinutes, DestThresholdMinutes, OriginMinBillableMinutes, OriginMaxBillableMinutes, DestMinBillableMinutes, DestMaxBillableMinutes
	FROM dbo.viewOrderBase O
	CROSS APPLY dbo.fnCarrierWaitFeeParameter(isnull(O.OrderDate, O.DueDate), null, O.CustomerID, O.CarrierID, O.ProductGroupID, O.OriginStateID, O.DestStateID, O.OriginRegionID, O.ProducerID, 1) R
	WHERE O.ID = @ID 
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier DestinationWaitRate info for the specified order
-- Changes
	-- 3.7.13 - 5/21/15 - KDA - fix bug which prevented missing rate warning when billable minutes existed but no rate matched
	-- 3.7.25 - 2015/05/18 - KDA - add use of @ProducerID parameter
***********************************/
ALTER FUNCTION fnOrderCarrierDestinationWaitRate(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID
		, Minutes = DestMinutes
		, R.Rate
	FROM viewOrderBase O
	OUTER APPLY dbo.fnCarrierDestinationWaitRate(O.OrderDate, null, O.DestWaitReasonID, O.CustomerID, O.CarrierID, O.ProductGroupID, O.DriverID, O.DriverGroupID, O.DestinationID, O.DestStateID, O.DestRegionID, O.ProducerID, 1) R
	WHERE O.ID = @ID
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier DestinationWait data info for the specified order
-- Changes:
	-- 3.7.28 - 2015/06/18 - KDA - add use of new WaitFeeParameter.DestMin|Max Billable Minutes
***********************************/
ALTER FUNCTION fnOrderCarrierDestinationWaitData(@ID int) RETURNS TABLE AS RETURN
	SELECT RateID, BillableMinutes, BillableHours, Amount = BillableHours * Rate
	FROM (
		SELECT RateID, BillableMinutes
			, BillableHours = dbo.fnComputeBillableWaitHours(BillableMinutes, SubUnitID, RoundingTypeID)
			, Rate
		FROM (
			SELECT RateID = WR.ID
				, Rate
				, BillableMinutes = dbo.fnMaxInt(
					-- Total Origin Minutes between range of DestMinBillableMinutes & DestMaxBillableMinutes (will be ignored if either Min|Max value is NULL)
					CASE WHEN WR.Minutes < WFP.DestMinBillableMinutes THEN 0
						 WHEN WR.Minutes > WFP.DestMaxBillableMinutes THEN WFP.DestMaxBillableMinutes
						 ELSE WR.Minutes END - WFP.DestThresholdMinutes, 0)
				, WFP.SubUnitID, WFP.RoundingTypeID
			FROM dbo.fnOrderCarrierDestinationWaitRate(@ID) WR 
			CROSS JOIN dbo.fnOrderCarrierWaitFeeParameter(@ID) WFP 
		) X
	) X2
	WHERE BillableHours > 0.0
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier DestinationWaitRate info for the specified criteria
-- Changes:
	-- 3.7.28 - 2015/06/18 - KDA - add DriverID/DriverGroupID parameters
***********************************/
ALTER FUNCTION fnCarrierFuelSurchargeRate
(
  @StartDate date
, @EndDate date
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @DriverID int
, @DriverGroupID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID
			, Ranking =	dbo.fnRateRanking(@ShipperID, R.ShipperID, 16, 0)
				  + dbo.fnRateRanking(@CarrierID, R.CarrierID, 8, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 4, 0)
				  + dbo.fnRateRanking(@DriverID, R.DriverID, 2, 0)
				  + dbo.fnRateRanking(@DriverGroupID, R.DriverGroupID, 1, 1)
		FROM  dbo.viewCarrierFuelSurchargeRate R
		WHERE coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@DriverID, 0), R.DriverID, 0) = coalesce(DriverID, nullif(@DriverID, 0), 0)
		  AND coalesce(nullif(@DriverGroupID, 0), R.DriverGroupID, 0) = coalesce(DriverGroupID, nullif(@DriverGroupID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	)
	SELECT R.ID, ShipperID, CarrierID, ProductGroupID, DriverID, DriverGroupID, FuelPriceFloor, IntervalAmount, IncrementAmount, EffectiveDate, EndDate
	  , MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate
	  , BestMatch, Ranking
	  , Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewCarrierFuelSurchargeRate R
	JOIN (
		SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 5  -- ensure some type of match occurred on all criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)
GO
/***********************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RouteRate rows for the specified criteria
-- Changes:
	-- 3.7.28 - 2015/06/18 - KDA - add DriverID/DriverGroupID parameters
***********************************/
ALTER FUNCTION fnCarrierFuelSurchargeRateDisplay
(
  @StartDate date
, @EndDate date
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @DriverID int
, @DriverGroupID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.DriverID, R.DriverGroupID, R.FuelPriceFloor, R.IntervalAmount, R.IncrementAmount, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Shipper = S.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, Driver = DR.FullName
		, DriverGroup = DG.Name
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch, Ranking
	FROM dbo.fnCarrierFuelSurchargeRate(@StartDate, @EndDate, @ShipperID, @CarrierID, @ProductGroupID, @DriverID, @DriverGroupID, 0) R
	LEFT JOIN tblCustomer S ON S.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN viewDriver DR ON DR.ID = R.DriverID
	LEFT JOIN tblDriverGroup DG ON DG.ID = R.DriverGroupID
	ORDER BY EffectiveDate
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier FuelSurcharge info for the specified order
-- Changes:
	-- 3.7.28 - 2015/06/18 - KDA - add usage of DriverID/DriverGroupID parameters
***********************************/
ALTER FUNCTION fnOrderCarrierFuelSurchargeRate(@ID int) RETURNS TABLE AS RETURN
	SELECT RateID
		, Rate = IncrementAmount * round(Pricediff / nullif(IntervalAmount, 0.0) + .49, 0)
		, RouteMiles
	FROM (
		SELECT TOP 1 RateID = ID, FuelPriceFloor, IntervalAmount, IncrementAmount
			, PriceDiff = dbo.fnMaxDecimal(0, dbo.fnFuelPriceForOrigin(OriginID, OrderDate) - FuelPriceFloor)
			, RouteMiles
		FROM (
			SELECT R.ID, R.FuelPriceFloor, R.IntervalAmount, R.IncrementAmount, O.OrderDate, O.OriginID, RouteMiles = CASE WHEN O.Rejected = 1 THEN 0 ELSE O.ActualMiles END
			FROM dbo.viewOrderBase O
			CROSS APPLY dbo.fnCarrierFuelSurchargeRate(O.OrderDate, null, O.CustomerID, O.CarrierID, O.ProductGroupID, O.DriverID, O.DriverGroupID, 1) R
			WHERE O.ID = @ID 
		) X
	) X2
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OrderRejectRate info for the specified criteria
-- Changes:
	-- 3.7.28 - 2015/06/18 - KDA - add DriverID/DriverGroupID/Producer parameters
***********************************/
ALTER FUNCTION fnCarrierOrderRejectRate
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @DriverID int
, @DriverGroupID int
, @OriginID int
, @StateID int
, @RegionID int
, @ProducerID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID, ReasonID
			, Ranking =	dbo.fnRateRanking(@ReasonID, R.ReasonID, 512, 1)
				  + dbo.fnRateRanking(@ShipperID, R.ShipperID, 256, 0)
				  + dbo.fnRateRanking(@CarrierID, R.CarrierID, 128, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 64, 0)
				  + dbo.fnRateRanking(@DriverID, R.DriverID, 32, 0)
				  + dbo.fnRateRanking(@DriverGroupID, R.DriverGroupID, 16, 1)
				  + dbo.fnRateRanking(@OriginID, R.OriginID, 8, 0)
				  + dbo.fnRateRanking(@StateID, R.StateID, 4, 0)
				  + dbo.fnRateRanking(@RegionID, R.RegionID, 2, 0)
				  + dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 1)
		FROM  dbo.viewCarrierOrderRejectRate R
		WHERE coalesce(nullif(@ReasonID, 0), R.ReasonID, 0) = coalesce(ReasonID, nullif(@ReasonID, 0), 0)
		  AND coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@DriverID, 0), R.DriverID, 0) = coalesce(DriverID, nullif(@DriverID, 0), 0)
		  AND coalesce(nullif(@DriverGroupID, 0), R.DriverGroupID, 0) = coalesce(DriverGroupID, nullif(@DriverGroupID, 0), 0)
		  AND coalesce(nullif(@OriginID, 0), R.OriginID, 0) = coalesce(OriginID, nullif(@OriginID, 0), 0)
		  AND coalesce(nullif(@StateID, 0), R.StateID, 0) = coalesce(StateID, nullif(@StateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(ProducerID, nullif(@ProducerID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	),
	cte2 AS
	(
		SELECT ID, S.ReasonID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT ReasonID = ISNULL(ReasonID, 0), Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 10  -- ensure some type of match occurred on all criteria choices
			GROUP BY ReasonID
		) X ON X.ReasonID = isnull(S.ReasonID, 0) AND X.Ranking = S.Ranking
	), 
	cte3 AS
	(	-- when both ReasonID = NULL AND ReasonID = @ReasonID records are BestMatch = 1, favor the ReasonID = @ReasonID record
		SELECT ID, X.ReasonID, X.Ranking, BestMatch = CASE WHEN BEST.ReasonID IS NULL THEN X.BestMatch ELSE 0 END
		FROM cte2 X
		LEFT JOIN (
			SELECT TOP 1 ReasonID FROM cte2 WHERE ReasonID = @ReasonID AND BestMatch = 1 ORDER BY Ranking DESC
		) BEST ON X.ReasonID IS NULL AND X.BestMatch = 1
	)
	
	SELECT R.ID, R.ReasonID, ShipperID, CarrierID, ProductGroupID, DriverID, DriverGroupID, OriginID, StateID, RegionID, ProducerID
	  , Rate, RateTypeID, UomID, EffectiveDate, EndDate
	  , MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate
	  , BestMatch, Ranking
	  , Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewCarrierOrderRejectRate R
	JOIN cte3 X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)
GO

/***********************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OrderRejectRate rows for the specified criteria
-- Changes:
	-- 3.7.28 - 2015/06/18 - KDA - add DriverID/DriverGroupID/ProducerID parameters
***********************************/
ALTER FUNCTION fnCarrierOrderRejectRatesDisplay
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @DriverID int
, @DriverGroupID int
, @OriginID int
, @StateID int
, @RegionID int
, @ProducerID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ReasonID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.DriverID, R.DriverGroupID, R.OriginID, R.StateID, R.RegionID, R.ProducerID
		, R.Rate, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Shipper = SH.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, Driver = DR.FullName
		, DriverGroup = DG.Name
		, Origin = D.Name
		, OriginFull = D.FullName
		, State = S.FullName
		, StateAbbrev = S.Abbreviation
		, Region = REG.Name
		, Producer = PR.Name
		, Reason = REA.Description
		, RateType = RT.Name
		, Uom = U.Name
		, UomShort = U.Abbrev
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
	FROM dbo.fnCarrierOrderRejectRate(@StartDate, @EndDate, @ReasonID, @ShipperID, @CarrierID, @ProductGroupID, @DriverID, @DriverGroupID, @OriginID, @StateID, @RegionID, @ProducerID, 0) R
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN viewDriver DR ON DR.ID = R.DriverID
	LEFT JOIN tblDriverGroup DG ON DG.ID = R.DriverGroupID
	LEFT JOIN viewOrigin D ON D.ID = R.OriginID
	LEFT JOIN tblState S ON S.ID = R.StateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblProducer PR ON PR.ID = R.ProducerID
	LEFT JOIN tblOrderRejectReason REA ON REA.ID = R.ReasonID
	LEFT JOIN tblRateType RT ON RT.ID = R.RateTypeID
	LEFT JOIN tblUom U ON U.ID = R.UomID
	ORDER BY EffectiveDate
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OrderRejectRate info for the specified order
-- Changes:
	-- 3.7.28 - 2015/06/18 - KDA - add DriverID/DriverGroupID/ProducerID parameters
***********************************/
ALTER FUNCTION fnOrderCarrierOrderRejectRate(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, R.Rate, R.RateTypeID, R.UomID
	FROM viewOrderBase O
	CROSS APPLY dbo.fnCarrierOrderRejectRate(O.OrderDate, null, O.RejectReasonID, O.CustomerID, O.CarrierID, O.ProductGroupID, O.DriverID, O.DriverGroupID, O.OriginID, O.OriginStateID, O.OriginRegionID, O.ProducerID, 1) R
	WHERE O.ID = @ID
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OriginWaitRate info for the specified criteria
-- Changes:
	-- 3.7.28 - 2015/06/18 - KDA - add DriverID/DriverGroupID/ProducerID parameters
***********************************/
ALTER FUNCTION fnCarrierOriginWaitRate
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @DriverID int
, @DriverGroupID int
, @OriginID int
, @StateID int
, @RegionID int
, @ProducerID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID, ReasonID
			, Ranking =	dbo.fnRateRanking(@ReasonID, R.ReasonID, 512, 1)
				  + dbo.fnRateRanking(@ShipperID, R.ShipperID, 256, 0)
				  + dbo.fnRateRanking(@CarrierID, R.CarrierID, 128, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 64, 0)
				  + dbo.fnRateRanking(@DriverID, R.DriverID, 32, 0)
				  + dbo.fnRateRanking(@DriverGroupID, R.DriverGroupID, 16, 1)
				  + dbo.fnRateRanking(@OriginID, R.OriginID, 8, 0)
				  + dbo.fnRateRanking(@StateID, R.StateID, 4, 0)
				  + dbo.fnRateRanking(@RegionID, R.RegionID, 2, 0)
				  + dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 1)
		FROM  dbo.viewCarrierOriginWaitRate R
		WHERE coalesce(nullif(@ReasonID, 0), R.ReasonID, 0) = coalesce(ReasonID, nullif(@ReasonID, 0), 0)
		  AND coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@DriverID, 0), R.DriverID, 0) = coalesce(DriverID, nullif(@DriverID, 0), 0)
		  AND coalesce(nullif(@DriverGroupID, 0), R.DriverGroupID, 0) = coalesce(DriverGroupID, nullif(@DriverGroupID, 0), 0)
		  AND coalesce(nullif(@OriginID, 0), R.OriginID, 0) = coalesce(OriginID, nullif(@OriginID, 0), 0)
		  AND coalesce(nullif(@StateID, 0), R.StateID, 0) = coalesce(StateID, nullif(@StateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(ProducerID, nullif(@ProducerID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	),
	cte2 AS
	(
		SELECT ID, S.ReasonID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT ReasonID = ISNULL(ReasonID, 0), Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 10  -- ensure some type of match occurred on all criteria choices
			GROUP BY ReasonID
		) X ON X.ReasonID = isnull(S.ReasonID, 0) AND X.Ranking = S.Ranking
	), 
	cte3 AS
	(	-- when both ReasonID = NULL AND ReasonID = @ReasonID records are BestMatch = 1, favor the ReasonID = @ReasonID record
		SELECT ID, X.ReasonID, X.Ranking, BestMatch = CASE WHEN BEST.ReasonID IS NULL THEN X.BestMatch ELSE 0 END
		FROM cte2 X
		LEFT JOIN (
			SELECT TOP 1 ReasonID FROM cte2 WHERE ReasonID = @ReasonID AND BestMatch = 1 ORDER BY Ranking DESC
		) BEST ON X.ReasonID IS NULL AND X.BestMatch = 1
	)
	
	SELECT TOP (CASE WHEN @BestMatchOnly = 1 THEN 1 ELSE 10000 END) R.ID, R.ReasonID, ShipperID, CarrierID, ProductGroupID, DriverID, DriverGroupID, OriginID, StateID, RegionID, ProducerID
	  , Rate, EffectiveDate, EndDate
	  , R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
	  , BestMatch, Ranking
	  , Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewCarrierOriginWaitRate R
	JOIN cte3 X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
	ORDER BY Ranking DESC
)
GO

/***********************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OriginWaitRate rows for the specified criteria
-- Changes:
	-- 3.7.28 - 2015/06/18 - KDA - add DriverID/DriverGroupID/ProducerID parameters
***********************************/
ALTER FUNCTION fnCarrierOriginWaitRatesDisplay
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @OriginID int
, @DriverID int
, @DriverGroupID int
, @StateID int
, @RegionID int
, @ProducerID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ReasonID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.DriverID, R.DriverGroupID, R.OriginID, R.StateID, R.RegionID, R.ProducerID
		, R.Rate, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Shipper = SH.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, Driver = DR.FullName
		, DriverGroup = DG.Name
		, Origin = D.Name
		, OriginFull = D.FullName
		, State = S.FullName
		, StateAbbrev = S.Abbreviation
		, Region = REG.Name
		, Producer = PR.Name
		, Reason = REA.Description
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
		, Ranking
	FROM dbo.fnCarrierOriginWaitRate(@StartDate, @EndDate, @ReasonID, @ShipperID, @CarrierID, @ProductGroupID, @DriverID, @DriverGroupID, @OriginID, @StateID, @RegionID, @ProducerID, 0) R
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN viewDriver DR ON DR.ID = R.DriverID
	LEFT JOIN tblDriverGroup DG ON DG.ID = R.DriverGroupID
	LEFT JOIN viewOrigin D ON D.ID = R.OriginID
	LEFT JOIN tblState S ON S.ID = R.StateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblProducer PR ON PR.ID = R.ProducerID
	LEFT JOIN tblOriginWaitReason REA ON REA.ID = R.ReasonID
	ORDER BY EffectiveDate
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OriginWaitRate info for the specified order
-- Changes
	-- 3.7.13 - 5/21/15 - KDA - fix bug which prevented missing rate warning when billable minutes existed but no rate matched
	-- 3.7.28 - 2015/06/18 - KDA - add usage of DriverID/DriverGroupID/ProducerID parameters
***********************************/
ALTER FUNCTION fnOrderCarrierOriginWaitRate(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, Minutes = O.OriginMinutes, R.Rate
	FROM viewOrderBase O
	CROSS APPLY dbo.fnCarrierOriginWaitRate(O.OrderDate, null, O.OriginWaitReasonID, O.CustomerID, O.CarrierID, O.ProductGroupID, O.DriverID, O.DriverGroupID, O.OriginID, O.OriginStateID, O.OriginRegionID, O.ProducerID, 1) R
	WHERE O.ID = @ID
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OriginWait data info for the specified order
-- Changes:
	-- 3.7.28 - 2015/06/18 - KDA - add DriverID/DriverGroupID/ProducerID parameters
								 - add ussage of new WaitFeeParameter.Min|Max Billable Minutes fields
***********************************/
ALTER FUNCTION fnOrderCarrierOriginWaitData(@ID int) RETURNS TABLE AS RETURN
	SELECT RateID, BillableMinutes, BillableHours, Amount = BillableHours * Rate
	FROM (
		SELECT RateID, BillableMinutes
			, BillableHours = dbo.fnComputeBillableWaitHours(BillableMinutes, SubUnitID, RoundingTypeID)
			, Rate
		FROM (
			SELECT RateID = WR.ID
				, WR.Rate
				, BillableMinutes = dbo.fnMaxInt(0, 
					-- Total Origin Minutes between range of OriginMinBillableMinutes & OriginMaxBillableMinutes (will be ignored if either Min|Max value is NULL)
					CASE WHEN WR.Minutes < WFP.OriginMinBillableMinutes THEN 0
						 WHEN WR.Minutes > WFP.OriginMaxBillableMinutes THEN WFP.OriginMaxBillableMinutes
						 ELSE WR.Minutes END - WFP.OriginThresholdMinutes)
				, WFP.SubUnitID, WFP.RoundingTypeID
			FROM dbo.fnOrderCarrierOriginWaitRate(@ID) WR 
			CROSS JOIN dbo.fnOrderCarrierWaitFeeParameter(@ID) WFP 
		) X
	) X2
	WHERE BillableHours > 0.0
GO

/***********************************
-- Date Created: 5 May 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RateSheet info for the specified criteria
-- Changes:
	-- 3.7.28 - 2015/06/18 - KDA - add DriverID/DriverGroupID/ProducerID parameters
***********************************/
ALTER FUNCTION fnCarrierRateSheet
(
  @StartDate date
, @EndDate date
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @DriverID int
, @DriverGroupID int
, @OriginStateID int
, @DestStateID int
, @RegionID int
, @ProducerID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT R.ID
			, Ranking = dbo.fnRateRanking(@ShipperID, R.ShipperID, 256, 0)
				+ dbo.fnRateRanking(@CarrierID, R.CarrierID, 128, 0)
				+ dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 64, 0)
				+ dbo.fnRateRanking(@DriverID, R.DriverID, 32, 0)
				+ dbo.fnRateRanking(@DriverGroupID, R.DriverGroupID, 16, 1)
				+ dbo.fnRateRanking(@OriginStateID, R.OriginStateID, 8, 0)
				+ dbo.fnRateRanking(@DestStateID, R.DestStateID, 4, 0)
				+ dbo.fnRateRanking(@RegionID, R.RegionID, 2, 0)
				+ dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 1)
		FROM dbo.viewCarrierRateSheet R
		WHERE coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@DriverID, 0), R.DriverID, 0) = coalesce(DriverID, nullif(@DriverID, 0), 0)
		  AND coalesce(nullif(@DriverGroupID, 0), R.DriverGroupID, 0) = coalesce(DriverGroupID, nullif(@DriverGroupID, 0), 0)
		  AND coalesce(nullif(@OriginStateID, 0), R.OriginStateID, 0) = coalesce(OriginStateID, nullif(@OriginStateID, 0), 0)
		  AND coalesce(nullif(@DestStateID, 0), R.DestStateID, 0) = coalesce(DestStateID, nullif(@DestStateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(ProducerID, nullif(@ProducerID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	)
	
	SELECT R.ID, ShipperID, CarrierID, ProductGroupID, DriverID, DriverGroupID, OriginStateID, DestStateID, RegionID, ProducerID, RateTypeID, UomID, EffectiveDate, EndDate
	  , MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate
	  , BestMatch, Ranking
	  , Locked, R.CreateDateUTC, R.CreatedByUser, R.LastChangeDateUTC, R.LastChangedByUser 
	FROM dbo.viewCarrierRateSheet R
	JOIN (
		SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 9  -- ensure some type of match occurred on all criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.ID = R.ID 
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)
GO

/***********************************
-- Date Created: 5 May 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RateSheetRangeRate info for the specified criteria
-- Changes:
	-- 3.7.28 - 2015/06/18 - KDA - add DriverID/DriverGroupID/ProducerID parameters
***********************************/
ALTER FUNCTION fnCarrierRateSheetDisplay
(
  @StartDate date
, @EndDate date
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @DriverID int
, @DriverGroupID int
, @OriginStateID int
, @DestStateID int
, @RegionID int
, @ProducerID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.DriverID, R.DriverGroupID, R.OriginStateID, R.DestStateID, R.RegionID, R.ProducerID, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Shipper = SH.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, Driver = DR.FullName
		, DriverGroup = DG.Name
		, OriginState = OS.FullName
		, OriginStateAbbrev = OS.Abbreviation
		, DestState = OS.FullName
		, DestStateAbbrev = OS.Abbreviation
		, Region = REG.Name
		, Producer = P.Name
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
		, Ranking
	FROM dbo.fnCarrierRateSheet(@StartDate, @EndDate, @ShipperID, @CarrierID, @ProductGroupID, @DriverID, @DriverGroupID, @OriginStateID, @DestStateID, @RegionID, @ProducerID, 0) R
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN viewDriver DR ON DR.ID = R.DriverID
	LEFT JOIN tblDriverGroup DG ON DG.ID = R.DriverGroupID
	LEFT JOIN tblState OS ON OS.ID = R.OriginStateID
	LEFT JOIN tblState DS ON DS.ID = R.DestStateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblProducer P ON P.ID = R.ProducerID
	ORDER BY EffectiveDate
GO

/************************************************
-- Date Created: 18 Jan 2015
-- Author: Kevin Alons
-- Purpose: return combined RateSheet + RangeRate data + friendly translated values
			allow updating of both RateSheet & RangeRate data together
-- Changes:
	-- 3.7.28 - 2015/06/18 - KDA - add DriverID/DriverGroupID/ProducerID parameters
************************************************/
ALTER VIEW viewCarrierRateSheetRangeRate AS
	SELECT RR.ID, RateSheetID = R.ID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.DriverID, R.DriverGroupID, R.OriginStateID, R.DestStateID, R.RegionID, R.ProducerID, RR.Rate, RR.MinRange, RR.MaxRange, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate, R.NextEffectiveDate
		, R.Locked
		, RR.CreateDateUTC, RR.CreatedByUser
		, RR.LastChangeDateUTC, RR.LastChangedByUser
	FROM dbo.viewCarrierRateSheet R
	JOIN dbo.tblCarrierRangeRate RR ON RR.RateSheetID = R.ID

GO

/***************************************************
-- Date Created: 6 May 2015
-- Author: Kevin Alons
-- Purpose: return the RateSheet JOIN RangeRate data with translated "friendly" values
-- Changes:
	-- 3.7.28 - 2015/06/18 - KDA - add DriverID/DriverGroupID/ProducerID parameters
***************************************************/
ALTER VIEW viewCarrierRateSheetRangeRatesDisplay AS
	SELECT R.*
		, Shipper = SH.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, Driver = DR.FullName
		, DriverGroup = DG.Name
		, OriginState = OS.FullName
		, OriginStateAbbrev = OS.Abbreviation
		, DestinationState = DS.FullName
		, DestinationStateAbbrev = DS.Abbreviation
		, Region = REG.Name
		, Producer = P.Name
		, RateType = RT.Name
		, Uom = U.Name
		, UomShort = U.Abbrev
	FROM viewCarrierRateSheetRangeRate R
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN viewDriver DR ON DR.ID = R.DriverID
	LEFT JOIN tblDriverGroup DG ON DG.ID = R.DriverGroupID
	LEFT JOIN tblState OS ON OS.ID = R.OriginStateID
	LEFT JOIN tblState DS ON DS.ID = R.DestStateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblProducer P ON P.ID = R.ProducerID
	LEFT JOIN tblRateType RT ON RT.ID = R.RateTypeID
	LEFT JOIN tblUom U ON U.ID = R.UomID

GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RateSheetRangeRate info for the specified criteria
-- Changes:
	-- 3.7.28 - 2015/06/18 - KDA - add DriverID/DriverGroupID/ProducerID parameters
***********************************/
ALTER FUNCTION fnCarrierRateSheetRangeRate
(
  @StartDate date
, @EndDate date
, @RouteMiles int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @DriverID int
, @DriverGroupID int
, @OriginStateID int
, @DestStateID int
, @RegionID int
, @ProducerID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT RateSheetID = R.ID, RangeRateID = RR.ID
			-- the manually added .01 is normally added via the fnRateRanking routine
			, Ranking = CASE WHEN @RouteMiles BETWEEN RR.MinRange AND RR.MaxRange THEN 512.01 ELSE 0 END
				+ dbo.fnRateRanking(@ShipperID, R.ShipperID, 256, 0)
				+ dbo.fnRateRanking(@CarrierID, R.CarrierID, 128, 0)
				+ dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 64, 0)
				+ dbo.fnRateRanking(@DriverID, R.DriverID, 32, 0)
				+ dbo.fnRateRanking(@DriverGroupID, R.DriverGroupID, 16, 1)
				+ dbo.fnRateRanking(@OriginStateID, R.OriginStateID, 8, 0)
				+ dbo.fnRateRanking(@DestStateID, R.DestStateID, 4, 0)
				+ dbo.fnRateRanking(@RegionID, R.RegionID, 2, 0)
				+ dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 1)
		FROM dbo.viewCarrierRateSheet R
		JOIN dbo.tblCarrierRangeRate RR ON RR.RateSheetID = R.ID 
		  AND isnull(@RouteMiles, RR.MinRange) BETWEEN RR.MinRange AND RR.MaxRange
		WHERE coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@DriverID, 0), R.DriverID, 0) = coalesce(DriverID, nullif(@DriverID, 0), 0)
		  AND coalesce(nullif(@DriverGroupID, 0), R.DriverGroupID, 0) = coalesce(DriverGroupID, nullif(@DriverGroupID, 0), 0)
		  AND coalesce(nullif(@OriginStateID, 0), R.OriginStateID, 0) = coalesce(OriginStateID, nullif(@OriginStateID, 0), 0)
		  AND coalesce(nullif(@DestStateID, 0), R.DestStateID, 0) = coalesce(DestStateID, nullif(@DestStateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(ProducerID, nullif(@ProducerID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	)
	
	SELECT RR.ID, RateSheetID = R.ID, ShipperID, CarrierID, ProductGroupID, DriverID, DriverGroupID, OriginStateID, DestStateID, RegionID, ProducerID
	  , Rate, MinRange, MaxRange, RateTypeID, UomID, EffectiveDate, EndDate
	  , MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate
	  , BestMatch, Ranking
	  , Locked, R.CreateDateUTC, R.CreatedByUser, R.LastChangeDateUTC, R.LastChangedByUser 
	FROM dbo.viewCarrierRateSheet R
	JOIN dbo.tblCarrierRangeRate RR ON RR.RateSheetID = R.ID
	JOIN (
		SELECT RateSheetID, RangeRateID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 10  -- ensure some type of match occurred on all criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.RateSheetID = R.ID AND X.RangeRateID = RR.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RateSheetRangeRate info for the specified criteria
-- Changes:
	-- 3.7.28 - 2015/06/18 - KDA - add DriverID/DriverGroupID/ProducerID parameters
***********************************/
ALTER FUNCTION fnCarrierRateSheetRangeRatesDisplay
(
  @StartDate date
, @EndDate date
, @RouteMiles int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @DriverID int
, @DriverGroupID int
, @OriginStateID int
, @DestStateID int
, @RegionID int
, @ProducerID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.RateSheetID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.DriverID, R.DriverGroupID, R.OriginStateID, R.DestStateID, R.RegionID, R.ProducerID, R.MinRange, R.MaxRange, R.Rate, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Shipper = SH.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, Driver = DR.FullName
		, DriverGroup = DG.Name
		, OriginState = OS.FullName
		, OriginStateAbbrev = OS.Abbreviation
		, DestState = OS.FullName
		, DestStateAbbrev = OS.Abbreviation
		, Region = REG.Name
		, Producer = P.Name
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
		, Ranking
	FROM dbo.fnCarrierRateSheetRangeRate(@StartDate, @EndDate, @RouteMiles, @ShipperID, @CarrierID, @ProductGroupID, @DriverID, @DriverGroupID, @OriginStateID, @DestStateID, @RegionID, @ProducerID, 0) R
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN viewDriver DR ON DR.ID = R.DriverID
	LEFT JOIN tblDriverGroup DG ON DG.ID = R.DriverGroupID
	LEFT JOIN tblState OS ON OS.ID = R.OriginStateID
	LEFT JOIN tblState DS ON DS.ID = R.DestStateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblProducer P ON P.ID = R.ProducerID
	ORDER BY EffectiveDate
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RateSheetRangeRate info for the specified order
-- Changes:
	-- 3.7.28 - 2015/06/18 - KDA - add usage of DriverID/DriverGroupID/ProducerID parameters
***********************************/
ALTER FUNCTION fnOrderCarrierRateSheetRangeRate(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, RateTypeID, Rate, UomID
	FROM dbo.viewOrderBase O
	CROSS APPLY dbo.fnCarrierRateSheetRangeRate(O.OrderDate, null, O.ActualMiles, O.CustomerID, O.CarrierID, O.ProductGroupID, O.DriverID, O.DriverGroupID, O.OriginStateID, O.DestStateID, O.OriginRegionID, O.ProducerID, 1) R
	WHERE O.ID = @ID
GO

/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all CarrierRouteRate records
-- Changes:
	-- 3.7.28 - 2015/06/18 - KDA - ADDED
******************************************************/
ALTER VIEW viewCarrierRouteRate AS
	SELECT X.*
		, MaxEffectiveDate = (SELECT MIN(OrderDate) FROM tblOrderSettlementCarrier WHERE RouteRateID = X.ID AND BatchID IS NOT NULL)
		, MinEndDate = (SELECT MAX(OrderDate) FROM tblOrderSettlementCarrier WHERE RouteRateID = X.ID AND BatchID IS NOT NULL)
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementCarrier WHERE RouteRateID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, R.OriginID
		, R.DestinationID
		, R.ActualMiles
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblCarrierRouteRate XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverID, X.DriverID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverGroupID, X.DriverGroupID) = 1
			  AND XN.RouteID = X.RouteID
			  AND XN.EffectiveDate > X.EffectiveDate)
		, PriorEndDate = (
			SELECT min(XN.EndDate) 
			FROM tblCarrierRouteRate XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverID, X.DriverID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverGroupID, X.DriverGroupID) = 1
			  AND XN.RouteID = X.RouteID
			  AND XN.EffectiveDate < X.EffectiveDate)
	FROM tblCarrierRouteRate X
	JOIN tblRoute R ON R.ID = X.RouteID

GO

CREATE INDEX idxOrderSettlementCarrier_Covering2 ON tblOrderSettlementCarrier(RouteRateID, BatchID)
GO

EXEC _spDropFunction 'fnCarrierRouteRate'
GO
/****************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RouteRate info for the specified criteria
-- Changes:
	-- 3.7.28 - 2015/06/18 - KDA - add DriverID/DriverGroupID parameters
*****************************************************/
CREATE FUNCTION fnCarrierRouteRate
(
  @StartDate date
, @EndDate date
, @OriginID int
, @DestinationID int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @DriverID int
, @DriverGroupID int
, @BestMatchOnly bit = 0
) RETURNS @ret TABLE 
(
  ID int
, RouteID int
, OriginID int
, DestinationID int
, ShipperID int
, CarrierID int
, ProductGroupID int
, DriverID int
, DriverGroupID int
, Rate decimal(18, 10)
, RateTypeID int
, UomID int
, EffectiveDate date
, EndDate date
, MaxEffectiveDate date
, MinEndDate date
, NextEffectiveDate date
, PriorEndDate date
, BestMatch bit
, Ranking decimal(10, 2)
, Locked bit
, CreateDateUTC datetime
, CreatedByUser varchar(100)
, LastChangeDateUTC datetime
, LastChangedByUser varchar(100)
) AS BEGIN

	DECLARE @data TABLE (ID int, RouteID int, Ranking decimal(10, 2))
	INSERT INTO @data
		SELECT R.ID
			, R.RouteID
			, Ranking =	dbo.fnRateRanking(@ShipperID, R.ShipperID, 16, 0)
				  + dbo.fnRateRanking(@CarrierID, R.CarrierID, 8, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 4, 0)
				  + dbo.fnRateRanking(@DriverID, R.DriverID, 2, 0)
				  + dbo.fnRateRanking(@DriverGroupID, R.DriverGroupID, 1, 1)
		FROM dbo.viewCarrierRouteRate R
		JOIN tblRoute RO ON RO.ID = R.RouteID
		WHERE (nullif(@OriginID, 0) IS NULL OR @OriginID = RO.OriginID)
		  AND (nullif(@DestinationID, 0) IS NULL OR @DestinationID = RO.DestinationID)
		  AND coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@DriverID, 0), R.DriverID, 0) = coalesce(DriverID, nullif(@DriverID, 0), 0)
		  AND coalesce(nullif(@DriverGroupID, 0), R.DriverGroupID, 0) = coalesce(DriverGroupID, nullif(@DriverGroupID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)

	INSERT INTO @ret
	SELECT R.ID, RouteID, OriginID, DestinationID, ShipperID, CarrierID, ProductGroupID, DriverID, DriverGroupID, Rate, RateTypeID, UomID, EffectiveDate, EndDate
		, MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate
		, BestMatch, Ranking
		, Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewCarrierRouteRate R
	JOIN (
		SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM @data S
		LEFT JOIN (
			SELECT RouteID, Ranking = MAX(Ranking)
			FROM @data
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 5  -- ensure some type of match occurred on all criteria choices
			GROUP BY RouteID			  
		) X ON X.RouteID = S.RouteID AND X.Ranking = S.Ranking
	) X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
	
	RETURN
END
GO
GRANT SELECT ON fnCarrierRouteRate TO role_iis_acct
GO

/****************************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RouteRate rows for the specified criteria
-- Changes:
	-- 3.7.28 - 2015/06/18 - KDA - add DriverID/DriverGroupID parameters
*****************************************************/
ALTER FUNCTION fnCarrierRouteRatesDisplay
(
  @StartDate date
, @EndDate date
, @OriginID int
, @DestinationID int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @DriverID int
, @DriverGroupID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.RouteID, RO.ActualMiles, R.OriginID, R.DestinationID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.DriverID, R.DriverGroupID, R.Rate, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Origin = O.Name
		, OriginFull = O.FullName
		, Destination = D.Name
		, DestinationFull = D.FullName
		, Shipper = SH.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, Driver = DR.FullName
		, DriverGroup = DG.Name
		, RateType = RT.Name
		, Uom = U.Name
		, UomShort = U.Abbrev
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch, Ranking
	FROM dbo.fnCarrierRouteRate(@StartDate, @EndDate, @OriginID, @DestinationID, @ShipperID, @CarrierID, @ProductGroupID, @DriverID, @DriverGroupID, 0) R
	JOIN tblRoute RO ON RO.ID = R.RouteID
	JOIN viewOrigin O ON O.ID = R.OriginID
	JOIN viewDestination D ON D.ID = R.DestinationID
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN viewDriver DR ON DR.ID = R.DriverID
	LEFT JOIN tblDriverGroup DG ON DG.ID = R.DriverGroupID
	LEFT JOIN tblRateType RT ON RT.ID = R.RateTypeID
	LEFT JOIN tblUom U ON U.ID = R.UomID
	ORDER BY EffectiveDate
GO

/****************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RouteRate info for the specified order
-- Changes:
	-- 3.7.28 - 2015/06/18 - KDA - add use of DriverID/DriverGroupID parameters
*****************************************************/
ALTER FUNCTION fnOrderCarrierRouteRate(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, RateTypeID, Rate, R.UomID
	FROM dbo.viewOrderBase O
	CROSS APPLY dbo.fnCarrierRouteRate(O.OrderDate, null, O.OriginID, O.DestinationID, O.CustomerID, O.CarrierID, O.ProductGroupID, O.DriverID, O.DriverGroupID, 1) R
	WHERE O.ID = @ID 
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RateSheetRangeRate info for the specified order
-- Changes:
***********************************/
ALTER FUNCTION fnOrderCarrierLoadAmount(@ID int, @Units decimal(18, 10), @UomID int) RETURNS TABLE AS RETURN
	SELECT RouteRateID, RangeRateID
		, Amount = dbo.fnRateToAmount(RateTypeID, @Units, @UomID, Rate, UomID, NULL
			, CASE WHEN RateTypeID = 3 THEN (SELECT TOP 1 LoadAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID) ELSE NULL END
			, CASE WHEN RateTypeID = 5 THEN (SELECT TOP 1 ActualMiles FROM tblOrder WHERE ID = @ID) ELSE NULL END)
	FROM (
		SELECT TOP 1 * 
		FROM (
			SELECT SortID = 1, RouteRateID = ID, RangeRateID = NULL, RateTypeID, UomID, Rate FROM dbo.fnOrderCarrierRouteRate(@ID)
			UNION SELECT 2, NULL, ID, RateTypeID, UomID, Rate FROM dbo.fnOrderCarrierRateSheetRangeRate(@ID)
		) X
		ORDER BY SortID
	) X
GO

CREATE TABLE tblOrderApproval
(
  OrderID int NOT NULL CONSTRAINT PK_OrderApproval PRIMARY KEY CONSTRAINT FK_OrderApproval_Order FOREIGN KEY REFERENCES tblOrder(ID) ON DELETE CASCADE 
, OriginWaitBillableMinutes int NULL
, DestWaitBillableMinutes int NULL
, BillableActualMiles int NULL
, Approved bit NOT NULL CONSTRAINT DF_OrderApproval_Approved DEFAULT(1)
, CreateDateUTC smalldatetime NULL CONSTRAINT DF_OrderApproval_CreateDateUTC DEFAULT (getutcdate())
, CreatedByUser varchar(100) NULL
)
GO
GRANT SELECT, INSERT ON tblOrderApproval TO role_iis_acct
GO

INSERT INTO tblOrderApproval (OrderID, Approved, CreatedByUser)
	SELECT OrderID, 1, CreatedByUser
	FROM viewOrderSettlementShipper O
	WHERE BatchID IS NOT NULL
GO

/**********************************************************
-- Author: Kevin Alons
-- Date Created: 8 Oct 2014
-- Purpose: return the Audit table records (optionally filtered to just the specified driver and/or ordernum)
-- Changes:
	-- 3.7.28 - 2015/06/18 - KDA - add filter to ensure APPROVED orders are not displayed
***********************************************************/
ALTER FUNCTION fnOrders_AllTickets_Audit(@carrierID int, @driverID int, @orderNum int, @id int) RETURNS TABLE AS RETURN
SELECT *
	, OriginDistanceText = CASE WHEN OriginGpsArrived = 1 THEN 'Verified' ELSE isnull(ltrim(OriginDistance), 'N/A') END
	, DestDistanceText = CASE WHEN DestGpsArrived = 1 THEN 'Verified' ELSE isnull(ltrim(DestDistance), 'N/A') END
	, HasError = cast(CASE WHEN Errors IS NULL THEN 0 ELSE 1 END as bit)
FROM (
	SELECT O.* 
		, OriginGpsLatLon = ltrim(DLO.Lat) + ',' + ltrim(DLO.Lon)
		, OriginLatLon = ltrim(OO.LAT) + ',' + ltrim(OO.LON)
		, OriginAccuracyMeters = DLO.SourceAccuracyMeters
		, OriginDistance = CASE WHEN DLO.DistanceToPoint < 0 THEN NULL ELSE DLO.DistanceToPoint END
		, OriginGpsArrived = CASE WHEN DLO.DistanceToPoint IS NOT NULL AND DLO.DistanceToPoint BETWEEN 0 AND OO.GeoFenceRadiusMeters THEN 1 ELSE 0 END
		, DestGpsLatLon = ltrim(DLD.Lat) + ',' + ltrim(DLD.Lon)
		, DestLatLon = ltrim(D.LAT) + ',' + ltrim(D.LON)
		, DestAccuracyMeters = DLD.SourceAccuracyMeters
		, DestDistance = CASE WHEN DLD.DistanceToPoint < 0 THEN NULL ELSE DLD.DistanceToPoint END
		, DestGpsArrived = CASE WHEN DLD.DistanceToPoint IS NOT NULL AND DLD.DistanceToPoint BETWEEN 0 AND D.GeoFenceRadiusMeters THEN 1 ELSE 0 END
		, OriginGrossBBLS = dbo.fnConvertUOM(O.OriginGrossUnits, O.OriginUomID, 1)
		, Errors = 
			nullif(
				SUBSTRING(
					CASE WHEN O.OriginArriveTimeUTC IS NULL THEN ',Missing Pickup Arrival' ELSE '' END
				  + CASE WHEN O.OriginDepartTimeUTC IS NULL THEN ',Missing Pickup Departure' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND O.Tickets IS NULL THEN ',No Active Tickets' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND O.DestArriveTimeUTC IS NULL THEN ',Missing Delivery Arrival' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND O.DestDepartTimeUTC IS NULL THEN ',Missing Delivery Departure' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND O.OriginGrossUnits = 0 AND O.OriginNetUnits = 0 THEN 'No Origin Units are entered' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND dbo.fnConvertUOM(O.OriginGrossUnits, O.OriginUomID, 1) NOT BETWEEN 0 AND 320 THEN ',Origin GOV Units out of limits' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND dbo.fnConvertUOM(O.OriginGrossStdUnits, O.OriginUomID, 1) NOT BETWEEN 0 AND 320 THEN ',Origin GSV Units out of limits' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND dbo.fnConvertUOM(O.OriginNetUnits, O.OriginUomID, 1) NOT BETWEEN 0 AND 320 THEN ',Origin NSV Units out of limits' ELSE '' END
				  + CASE WHEN dbo.fnToBool(OOR.Value) = 1 AND (SELECT count(*) FROM tblOrderTicket OT WHERE OrderID = O.ID AND OT.DeleteDateUTC IS NULL AND OT.DispatchConfirmNum IS NULL) > 0 THEN ',Missing Ticket Shipper PO' ELSE '' END
				  + CASE WHEN EXISTS(SELECT CustomerID FROM viewOrder_OrderTicket_Full WHERE ID = O.ID AND DeleteDateUTC IS NULL AND nullif(rtrim(T_DispatchConfirmNum), '') IS NOT NULL GROUP BY CustomerID, T_DispatchConfirmNum HAVING COUNT(*) > 1) THEN ',Duplicate Shipper PO' ELSE '' END
				  + CASE WHEN O.TruckID IS NULL THEN ',Truck Missing' ELSE '' END
				  + CASE WHEN O.TrailerID IS NULL THEN ',Trailer 1 Missing' ELSE '' END
				, 2, 8000) 
			, '')
		, IsEditable = cast(CASE WHEN O.StatusID = 3 THEN 1 ELSE 0 END as bit)
	FROM viewOrder_AllTickets O
	JOIN tblOrigin OO ON OO.ID = O.OriginID
	JOIN tblCustomer C ON C.ID = OO.CustomerID
	JOIN tblDestination D ON D.ID = O.DestinationID
	-- ShipperPO_Required OrderRule
	OUTER APPLY (SELECT Value FROM dbo.fnOrderOrderRules(O.ID) WHERE TypeID = 2) OOR
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
	LEFT JOIN tblOrderSettlementShipper OIC ON OIC.OrderID = O.ID
	LEFT JOIN viewDriverLocation_OriginFirstArrive DLO ON DLO.OrderID = O.ID AND DLO.OriginID = O.OriginID
	LEFT JOIN viewDriverLocation_DestinationFirstArrive DLD ON DLD.OrderID = O.ID AND DLD.DestinationID = O.DestinationID
	WHERE OIC.BatchID IS NULL /* don't even show SETTLED orders on the AUDIT page ever */
	  AND nullif(OA.Approved, 0) IS NULL /* don't show Approved orders on the AUDIT page */
	  AND (isnull(@carrierID, 0) = -1 OR O.CarrierID = @carrierID)
	  AND (isnull(@driverID, 0) = -1 OR O.DriverID = @driverID)
	  AND ((nullif(@orderNum, 0) IS NULL AND O.DeleteDateUTC IS NULL AND (O.StatusID = 3 AND DeliverPrintStatusID IN (SELECT ID FROM tblPrintStatus WHERE IsCompleted = 1))) OR O.OrderNum = @orderNum)
	  AND (nullif(@id, 0) IS NULL OR O.ID LIKE @id)
) X
GO

CREATE TABLE tblCarrierMinSettlementUnits
(
  ID int IDENTITY(1,1) NOT NULL CONSTRAINT PK_CarrierMinSettlementUnits PRIMARY KEY NONCLUSTERED 
, ShipperID int NULL CONSTRAINT FK_CarrierMinSettlementUnits_Shipper FOREIGN KEY REFERENCES dbo.tblCustomer (ID)
, CarrierID int NULL CONSTRAINT FK_CarrierMinSettlementUnits_Carrier FOREIGN KEY REFERENCES dbo.tblCarrier (ID)
, ProductGroupID int NULL CONSTRAINT FK_CarrierMinSettlementUnits_ProductGroup FOREIGN KEY REFERENCES dbo.tblProductGroup (ID)
, OriginID int NULL CONSTRAINT FK_CarrierMinSettlementUnits_Origin FOREIGN KEY REFERENCES dbo.tblOrigin (ID)
, OriginStateID int NULL CONSTRAINT FK_CarrierMinSettlementUnits_OriginState FOREIGN KEY REFERENCES dbo.tblState (ID)
, DestinationID int NULL CONSTRAINT FK_CarrierMinSettlementUnits_Destination FOREIGN KEY REFERENCES dbo.tblDestination (ID)
, DestinationStateID int NULL CONSTRAINT FK_CarrierMinSettlementUnits_DestState FOREIGN KEY REFERENCES dbo.tblState (ID)
, ProducerID int NULL CONSTRAINT FK_CarrierMinSettlementUnits_Producer FOREIGN KEY REFERENCES dbo.tblProducer (ID)
, EffectiveDate date NOT NULL
, EndDate date NOT NULL
, MinSettlementUnits int NOT NULL
, UomID int NOT NULL CONSTRAINT FK_CarrierMinSettlementUnits_UomID FOREIGN KEY(UomID) REFERENCES dbo.tblUom (ID)
, CreateDateUTC smalldatetime NOT NULL CONSTRAINT DF_CarrierMinSettlementUnits DEFAULT (getutcdate())
, CreatedByUser varchar(100) NOT NULL
, LastChangeDateUTC smalldatetime NULL
, LastChangedByUser varchar(100) NULL
)
GO
GRANT SELECT, INSERT, UPDATE ON tblCarrierMinSettlementUnits TO role_iis_acct
GO

CREATE UNIQUE CLUSTERED INDEX udxCarrierMinSettlementUnits_Main ON tblCarrierMinSettlementUnits
(
  ShipperID 
, CarrierID 
, ProductGroupID 
, OriginID 
, OriginStateID 
, DestinationID 
, DestinationStateID 
, ProducerID
, EffectiveDate ASC
)
GO

INSERT INTO tblCarrierMinSettlementUnits (CarrierID, MinSettlementUnits, UomID, EffectiveDate, EndDate, CreatedByUser)
	SELECT ID, MinSettlementUnits, MinSettlementUomID, '1/1/2012', '12/31/2099', CreatedByUser FROM tblCarrier WHERE DeleteDateUTC IS NULL AND MinSettlementUnits IS NOT NULL
	UNION SELECT NULL, 180, 1, '1/1/2012', '12/31/2099', 'System' 
GO

ALTER TABLE tblOrderSettlementCarrier ADD MinSettlementUnitsID int NULL CONSTRAINT FK_OrderSettlementCarrier_MinSettlementUnitsID FOREIGN KEY REFERENCES tblCarrierMinSettlementUnits(ID)
GO

UPDATE tblOrderSettlementCarrier
  SET MinSettlementUnitsID = ISNULL(CMSU.ID, CMSUX.ID)
FROM tblOrderSettlementCarrier OSC
JOIN tblOrder O ON O.ID = OSC.OrderID
JOIN tblCarrierMinSettlementUnits CMSU ON CMSU.CarrierID = O.CarrierID
LEFT JOIN tblCarrierMinSettlementUnits CMSUX ON CMSUX.CarrierID IS NULL
GO

CREATE TABLE tblCarrierSettlementFactor
(
  ID int IDENTITY(1,1) NOT NULL CONSTRAINT PK_CarrierSettlementFactor PRIMARY KEY NONCLUSTERED 
, ShipperID int NULL CONSTRAINT FK_CarrierSettlementFactor_Shipper FOREIGN KEY REFERENCES dbo.tblCustomer (ID)
, CarrierID int NULL CONSTRAINT FK_CarrierSettlementFactor_Carrier FOREIGN KEY REFERENCES dbo.tblCarrier (ID)
, ProductGroupID int NULL CONSTRAINT FK_CarrierSettlementFactor_ProductGroup FOREIGN KEY REFERENCES dbo.tblProductGroup (ID)
, OriginID int NULL CONSTRAINT FK_CarrierSettlementFactor_Origin FOREIGN KEY REFERENCES dbo.tblOrigin (ID)
, OriginStateID int NULL CONSTRAINT FK_CarrierSettlementFactor_OriginState FOREIGN KEY REFERENCES dbo.tblState (ID)
, DestinationID int NULL CONSTRAINT FK_CarrierSettlementFactor_Destination FOREIGN KEY REFERENCES dbo.tblDestination (ID)
, DestinationStateID int NULL CONSTRAINT FK_CarrierSettlementFactor_DestState FOREIGN KEY REFERENCES dbo.tblState (ID)
, ProducerID int NULL CONSTRAINT FK_CarrierSettlementFactor_Producer FOREIGN KEY REFERENCES dbo.tblProducer (ID)
, EffectiveDate date NOT NULL
, EndDate date NOT NULL
, SettlementFactorID int NOT NULL CONSTRAINT FK_CarrierSettlementFactor_SettlementFactor FOREIGN KEY REFERENCES dbo.tblSettlementFactor(ID)
, CreateDateUTC smalldatetime NOT NULL CONSTRAINT DF_CarrierSettlementFactor DEFAULT (getutcdate())
, CreatedByUser varchar(100) NOT NULL
, LastChangeDateUTC smalldatetime NULL
, LastChangedByUser varchar(100) NULL
)
GO
GRANT SELECT, INSERT, UPDATE ON tblCarrierSettlementFactor TO role_iis_acct
GO

CREATE UNIQUE CLUSTERED INDEX udxCarrierSettlementFactor_Main ON tblCarrierSettlementFactor
(
  ShipperID 
, CarrierID 
, ProductGroupID 
, OriginID 
, OriginStateID 
, DestinationID 
, DestinationStateID 
, ProducerID
, EffectiveDate ASC
)
GO

INSERT INTO tblCarrierSettlementFactor (CarrierID, SettlementFactorID, EffectiveDate, EndDate, CreatedByUser)
	SELECT ID, SettlementFactorID, '1/1/2012', '12/31/2099', CreatedByUser FROM tblCarrier WHERE DeleteDateUTC IS NULL
GO

ALTER TABLE tblOrderSettlementCarrier ADD CarrierSettlementFactorID int NULL CONSTRAINT FK_OrderSettlementCarrier_CarrierSettlementFactorID FOREIGN KEY REFERENCES tblCarrierSettlementFactor(ID)
GO

UPDATE tblOrderSettlementCarrier
  SET CarrierSettlementFactorID = ISNULL(CSU.ID, CSUX.ID)
FROM tblOrderSettlementCarrier OSC
JOIN tblOrder O ON O.ID = OSC.OrderID
JOIN tblCarrierSettlementFactor CSU ON CSU.CarrierID = O.CarrierID
LEFT JOIN tblCarrierSettlementFactor CSUX ON CSUX.CarrierID IS NULL
GO

/******************************************************
-- Date Created: 12 June 2015
-- Author: Kevin Alons
-- Purpose: add a computed fields to the CarrierMinSettlementUnits table data
-- Changes: 
	- 3.7.28 - 2015/06/18 - KDA - ADDED
******************************************************/
CREATE VIEW viewCarrierMinSettlementUnits AS
	SELECT X.*
		, MaxEffectiveDate = (SELECT MIN(OrderDate) FROM tblOrderSettlementCarrier WHERE MinSettlementUnitsID = X.ID AND BatchID IS NOT NULL)
		, MinEndDate = (SELECT MAX(OrderDate) FROM tblOrderSettlementCarrier WHERE MinSettlementUnitsID = X.ID AND BatchID IS NOT NULL)
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementCarrier WHERE MinSettlementUnitsID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblCarrierMinSettlementUnits XN 
			WHERE isnull(XN.ShipperID, 0) = isnull(X.ShipperID, 0) 
			  AND isnull(XN.CarrierID, 0) = isnull(X.CarrierID, 0) 
			  AND isnull(XN.ProductGroupID, 0) = isnull(X.ProductGroupID, 0) 
			  AND isnull(XN.OriginID, 0) = isnull(X.OriginID, 0) 
			  AND isnull(XN.OriginStateID, 0) = isnull(X.OriginStateID, 0) 
			  AND isnull(XN.DestinationID, 0) = isnull(X.DestinationID, 0) 
			  AND isnull(XN.DestinationStateID, 0) = isnull(X.DestinationStateID, 0) 
			  AND isnull(XN.ProducerID, 0) = isnull(X.ProducerID, 0) 
			  AND XN.EffectiveDate > X.EffectiveDate)
		, PriorEndDate = (
			SELECT max(XN.EndDate) 
			FROM tblCarrierMinSettlementUnits XN 
			WHERE isnull(XN.ShipperID, 0) = isnull(X.ShipperID, 0) 
			  AND isnull(XN.CarrierID, 0) = isnull(X.CarrierID, 0) 
			  AND isnull(XN.OriginID, 0) = isnull(X.OriginID, 0) 
			  AND isnull(XN.OriginStateID, 0) = isnull(X.OriginStateID, 0) 
			  AND isnull(XN.DestinationID, 0) = isnull(X.DestinationID, 0) 
			  AND isnull(XN.DestinationStateID, 0) = isnull(X.DestinationStateID, 0) 
			  AND isnull(XN.ProducerID, 0) = isnull(X.ProducerID, 0) 
			  AND XN.EndDate < X.EffectiveDate)
	FROM tblCarrierMinSettlementUnits X

GO
GRANT SELECT ON viewCarrierMinSettlementUnits TO role_iis_acct
GO

/******************************************************
-- Date Created: 12 June 2015
-- Author: Kevin Alons
-- Purpose: add a computed fields to the CarrierSettlementFactor table data
-- Changes: 
	- 3.7.28 - 2015/06/18 - KDA - ADDED
******************************************************/
CREATE VIEW viewCarrierSettlementFactor AS
	SELECT X.*
		, MaxEffectiveDate = (SELECT MIN(OrderDate) FROM tblOrderSettlementCarrier WHERE CarrierSettlementFactorID = X.ID AND BatchID IS NOT NULL)
		, MinEndDate = (SELECT MAX(OrderDate) FROM tblOrderSettlementCarrier WHERE CarrierSettlementFactorID = X.ID AND BatchID IS NOT NULL)
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementCarrier WHERE CarrierSettlementFactorID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblCarrierSettlementFactor XN 
			WHERE isnull(XN.ShipperID, 0) = isnull(X.ShipperID, 0) 
			  AND isnull(XN.CarrierID, 0) = isnull(X.CarrierID, 0) 
			  AND isnull(XN.ProductGroupID, 0) = isnull(X.ProductGroupID, 0) 
			  AND isnull(XN.OriginID, 0) = isnull(X.OriginID, 0) 
			  AND isnull(XN.OriginStateID, 0) = isnull(X.OriginStateID, 0) 
			  AND isnull(XN.DestinationID, 0) = isnull(X.DestinationID, 0) 
			  AND isnull(XN.DestinationStateID, 0) = isnull(X.DestinationStateID, 0) 
			  AND isnull(XN.ProducerID, 0) = isnull(X.ProducerID, 0) 
			  AND XN.EffectiveDate > X.EffectiveDate)
		, PriorEndDate = (
			SELECT max(XN.EndDate) 
			FROM tblCarrierSettlementFactor XN 
			WHERE isnull(XN.ShipperID, 0) = isnull(X.ShipperID, 0) 
			  AND isnull(XN.CarrierID, 0) = isnull(X.CarrierID, 0) 
			  AND isnull(XN.OriginID, 0) = isnull(X.OriginID, 0) 
			  AND isnull(XN.OriginStateID, 0) = isnull(X.OriginStateID, 0) 
			  AND isnull(XN.DestinationID, 0) = isnull(X.DestinationID, 0) 
			  AND isnull(XN.DestinationStateID, 0) = isnull(X.DestinationStateID, 0) 
			  AND isnull(XN.ProducerID, 0) = isnull(X.ProducerID, 0) 
			  AND XN.EndDate < X.EffectiveDate)
	FROM tblCarrierSettlementFactor X

GO
GRANT SELECT ON viewCarrierSettlementFactor TO role_iis_acct
GO

/***********************************
-- Date Created: 12 June 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier MinSettlementUnits info for the specified criteria
-- Changes: 
	- 3.7.28 - 2015/06/18 - KDA - ADDED
***********************************/
CREATE FUNCTION fnCarrierMinSettlementUnits
(
  @StartDate date
, @EndDate date
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @OriginID int
, @OriginStateID int
, @DestinationID int
, @DestinationStateID int
, @ProducerID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID
			, Ranking =	dbo.fnRateRanking(@ShipperID, R.ShipperID, 128, 0)
				  + dbo.fnRateRanking(@CarrierID, R.CarrierID, 64, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 32, 0)
				  + dbo.fnRateRanking(@OriginID, R.OriginID, 16, 0)
				  + dbo.fnRateRanking(@OriginStateID, R.OriginStateID, 8, 0)
				  + dbo.fnRateRanking(@DestinationID, R.DestinationID, 4, 0)
				  + dbo.fnRateRanking(@DestinationStateID, R.DestinationStateID, 2, 0)
				  + dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 1)
		FROM  dbo.viewCarrierMinSettlementUnits R
		WHERE coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@OriginID, 0), R.OriginID, 0) = coalesce(OriginID, nullif(@OriginID, 0), 0)
		  AND coalesce(nullif(@OriginStateID, 0), R.OriginStateID, 0) = coalesce(OriginStateID, nullif(@OriginStateID, 0), 0)
		  AND coalesce(nullif(@DestinationID, 0), R.DestinationID, 0) = coalesce(DestinationID, nullif(@DestinationID, 0), 0)
		  AND coalesce(nullif(@DestinationStateID, 0), R.DestinationStateID, 0) = coalesce(DestinationStateID, nullif(@DestinationStateID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(ProducerID, nullif(@ProducerID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	)
	
	SELECT TOP (CASE WHEN @BestMatchOnly = 1 THEN 1 ELSE 10000 END) R.ID, ShipperID, CarrierID, ProductGroupID, OriginID, OriginStateID, DestinationID, DestinationStateID, ProducerID
	  , MinSettlementUnits, UomID, EffectiveDate, EndDate
	  , MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate
	  , BestMatch, Ranking
	  , Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewCarrierMinSettlementUnits R
	JOIN (
		SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 8  -- ensure some type of match occurred on all criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)
GO
GRANT SELECT ON fnCarrierMinSettlementUnits TO role_iis_acct
GO

/***********************************
-- Date Created: 12 June 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier SettlementFactor info for the specified criteria
-- Changes: 
	- 3.7.28 - 2015/06/18 - KDA - ADDED
***********************************/
CREATE FUNCTION fnCarrierSettlementFactor
(
  @StartDate date
, @EndDate date
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @OriginID int
, @OriginStateID int
, @DestinationID int
, @DestinationStateID int
, @ProducerID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID
			, Ranking =	dbo.fnRateRanking(@ShipperID, R.ShipperID, 128, 0)
				  + dbo.fnRateRanking(@CarrierID, R.CarrierID, 64, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 32, 0)
				  + dbo.fnRateRanking(@OriginID, R.OriginID, 16, 0)
				  + dbo.fnRateRanking(@OriginStateID, R.OriginStateID, 8, 0)
				  + dbo.fnRateRanking(@DestinationID, R.DestinationID, 4, 0)
				  + dbo.fnRateRanking(@DestinationStateID, R.DestinationStateID, 2, 0)
				  + dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 1)
		FROM  dbo.viewCarrierSettlementFactor R
		WHERE coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@OriginID, 0), R.OriginID, 0) = coalesce(OriginID, nullif(@OriginID, 0), 0)
		  AND coalesce(nullif(@OriginStateID, 0), R.OriginStateID, 0) = coalesce(OriginStateID, nullif(@OriginStateID, 0), 0)
		  AND coalesce(nullif(@DestinationID, 0), R.DestinationID, 0) = coalesce(DestinationID, nullif(@DestinationID, 0), 0)
		  AND coalesce(nullif(@DestinationStateID, 0), R.DestinationStateID, 0) = coalesce(DestinationStateID, nullif(@DestinationStateID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(ProducerID, nullif(@ProducerID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	)
	
	SELECT TOP (CASE WHEN @BestMatchOnly = 1 THEN 1 ELSE 10000 END) R.ID, ShipperID, CarrierID, ProductGroupID, OriginID, OriginStateID, DestinationID, DestinationStateID, ProducerID
	  , SettlementFactorID, EffectiveDate, EndDate
	  , MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate
	  , BestMatch, Ranking
	  , Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewCarrierSettlementFactor R
	JOIN (
		SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 8  -- ensure some type of match occurred on all criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)
GO
GRANT SELECT ON fnCarrierSettlementFactor TO role_iis_acct
GO

/***********************************
-- Date Created: 12 June 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier MinSettlementUnits rows for the specified criteria
-- Changes: 
	- 3.7.28 - 2015/06/18 - KDA - ADDED
***********************************/
CREATE FUNCTION fnCarrierMinSettlementUnitsDisplay
(
  @StartDate date
, @EndDate date
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @OriginID int
, @OriginStateID int
, @DestinationID int
, @DestinationStateID int
, @ProducerID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.OriginID, R.OriginStateID, R.DestinationID, R.DestinationStateID, R.ProducerID
		, R.MinSettlementUnits, R.UomID, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Shipper = SH.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, Origin = O.Name
		, OriginFull = O.FullName
		, OriginState = O.State
		, OriginStateAbbrev = O.StateAbbrev
		, Destination = D.Name
		, DestinationFull = D.FullName
		, DestinationState = D.State
		, DestinationStateAbbrev = D.StateAbbrev
		, Producer = P.Name
		, Uom = U.Name
		, UomShort = U.Abbrev
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch, Ranking
	FROM dbo.fnCarrierMinSettlementUnits(@StartDate, @EndDate, @ShipperID, @CarrierID, @ProductGroupID, @OriginID, @OriginStateID, @DestinationID, @DestinationStateID, @ProducerID, 0) R
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN viewOrigin O ON O.ID = R.OriginID
	LEFT JOIN viewDestination D ON D.ID = R.DestinationID
	LEFT JOIN tblProducer P ON P.ID = R.ProducerID
	LEFT JOIN tblUom U ON U.ID = R.UomID
	ORDER BY EffectiveDate

GO
GRANT SELECT ON fnCarrierMinSettlementUnitsDisplay TO role_iis_acct
GO

/***********************************
-- Date Created: 12 June 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier SettlementFactor rows for the specified criteria
-- Changes: 
	- 3.7.28 - 2015/06/18 - KDA - ADDED
***********************************/
CREATE FUNCTION fnCarrierSettlementFactorDisplay
(
  @StartDate date
, @EndDate date
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @OriginID int
, @OriginStateID int
, @DestinationID int
, @DestinationStateID int
, @ProducerID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.OriginID, R.OriginStateID, R.DestinationID, R.DestinationStateID, R.ProducerID
		, R.SettlementFactorID, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, SettlementFactor = SF.Name
		, Shipper = SH.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, Origin = O.Name
		, OriginFull = O.FullName
		, OriginState = O.State
		, OriginStateAbbrev = O.StateAbbrev
		, Destination = D.Name
		, DestinationFull = D.FullName
		, DestinationState = D.State
		, DestinationStateAbbrev = D.StateAbbrev
		, Producer = P.Name
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch, Ranking
	FROM dbo.fnCarrierSettlementFactor(@StartDate, @EndDate, @ShipperID, @CarrierID, @ProductGroupID, @OriginID, @OriginStateID, @DestinationID, @DestinationStateID, @ProducerID, 0) R
	JOIN dbo.tblSettlementFactor SF ON SF.ID = R.SettlementFactorID
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN viewOrigin O ON O.ID = R.OriginID
	LEFT JOIN viewDestination D ON D.ID = R.DestinationID
	LEFT JOIN tblProducer P ON P.ID = R.ProducerID
	ORDER BY EffectiveDate

GO
GRANT SELECT ON fnCarrierSettlementFactorDisplay TO role_iis_acct
GO

/***********************************
-- Date Created: 12 June 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier MinSettlementUnits info for the specified order
-- Changes: 
	- 3.7.28 - 2015/06/18 - KDA - ADDED
***********************************/
CREATE FUNCTION fnOrderCarrierMinSettlementUnits(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, R.MinSettlementUnits, R.UomID
	FROM viewOrderBase O
	CROSS APPLY dbo.fnCarrierMinSettlementUnits(O.OrderDate, null, O.CustomerID, O.CarrierID, O.ProductGroupID, O.OriginID, O.OriginStateID, O.DestinationID, O.DestStateID, O.ProducerID, 1) R
	WHERE O.ID = @ID
GO
GRANT SELECT ON fnOrderCarrierMinSettlementUnits TO role_iis_acct
GO

/***********************************
-- Date Created: 12 June 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier SettlementFactor info for the specified order
-- Changes: 
	- 3.7.28 - 2015/06/18 - KDA - ADDED
***********************************/
CREATE FUNCTION fnOrderCarrierSettlementFactor(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, R.SettlementFactorID
	FROM viewOrderBase O
	CROSS APPLY dbo.fnCarrierSettlementFactor(O.OrderDate, null, O.CustomerID, O.CarrierID, O.ProductGroupID, O.OriginID, O.OriginStateID, O.DestinationID, O.DestStateID, O.ProducerID, 1) R
	WHERE O.ID = @ID
GO
GRANT SELECT ON fnOrderCarrierSettlementFactor TO role_iis_acct
GO

UPDATE tblSettlementFactor SET Name = 'Origin ' + Name
INSERT INTO tblSettlementFactor (ID, Name)
	SELECT 4, 'Dest GOV'
	UNION SELECT 5, 'Dest NSV'
GO

/***********************************
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Carrier "Settlement" $$ values to an Delivered/Audited order
-- Changes: 
	- 3.7.28 - 2015/06/18 - KDA - add logic to handle new DESTINATION Settlement Factor types
***********************************/
ALTER VIEW viewOrderSettlementUnitsCarrier AS
	SELECT X2.*
		, SettlementUnits = dbo.fnMaxDecimal(X2.ActualUnits, MinSettlementUnits)
	FROM (
		SELECT OrderID
			, CarrierID
			, SettlementUomID
			, CarrierSettlementFactorID
			, SettlementFactorID
			, MinSettlementUnits = dbo.fnMinDecimal(isnull(X.WRMSVUnits, X.MinSettlementUnits), X.MinSettlementUnits)
			, MinSettlementUnitsID
			, ActualUnits
		FROM (
			SELECT OrderID = O.ID
				, O.CarrierID
				, SettlementUomID = O.OriginUomID
				, CarrierSettlementFactorID = MSF.ID
				, MSF.SettlementFactorID
				, WRMSVUnits = dbo.fnConvertUOM(R.WRMSVUnits, R.WRMSVUomID, O.OriginUomID)
				, MinSettlementUnitsID = MSU.ID
				, MinSettlementUnits = isnull(dbo.fnConvertUOM(MSU.MinSettlementUnits, MSU.UomID, O.OriginUomID), 0) 
				, ActualUnits = CASE MSF.ID	WHEN 1 THEN isnull(O.OriginGrossUnits, 0) 
											WHEN 3 THEN isnull(O.OriginGrossStdUnits, O.OriginGrossUnits) 
											WHEN 2 THEN coalesce(O.OriginNetUnits, O.OriginGrossStdUnits, O.OriginGrossUnits, 0) 
											WHEN 4 THEN isnull(O.DestGrossUnits, 0)
											WHEN 5 THEN coalesce(O.DestNetUnits, O.DestGrossUnits, 0) END 
			FROM dbo.tblOrder O
			CROSS APPLY dbo.fnOrderCarrierMinSettlementUnits(O.ID) MSU 
			CROSS APPLY dbo.fnOrderCarrierSettlementFactor(O.ID) MSF
			JOIN dbo.tblRoute R ON R.ID = O.RouteID
			JOIN tblCarrier C ON C.ID = O.CarrierID
		) X
	) X2
GO

UPDATE tblOrigin SET LAT = REPLACE(LAT, ',', ''), LON = REPLACE(LON, ',', '')
UPDATE tblDestination SET LAT = REPLACE(LAT, ',', ''), LON = REPLACE(LON, ',', '')
GO

/***********************************
-- Date Created: 13 Jun 2015
-- Author: Kevin Alons
-- Purpose: compute the geography POINT for the specified LAT/LON coordinate strings
-- Changes: 
	- 3.7.28 - 2015/06/18 - KDA - ADDED
***********************************/
CREATE FUNCTION fnPointFromLatLon(@Lat varchar(25), @Lon varchar(25)) RETURNS geography AS
BEGIN
	DECLARE @ret geography
	SELECT @Lat = REPLACE(@Lat, ',', ''), @Lon = REPLACE(@Lon, ',', '')
	SELECT @ret = CASE WHEN PT IS NULL THEN NULL ELSE geography::STGeomFromText(PT, 4326) END
	FROM (
		SELECT PT = CASE WHEN @LAT IS NOT NULL AND @LON IS NOT NULL 
							AND isnumeric(@LAT) = 1 AND ISNUMERIC(@LON) = 1 
							AND abs(cast(@LAT as decimal(12, 1))) <= 90 
							AND cast(@LON as decimal(12, 1)) BETWEEN -128 AND -65 THEN 'POINT(' + @LON + ' ' + @LAT + ')' ELSE NULL END
	) X	
	RETURN (@ret)
END
GO
GRANT EXECUTE ON fnPointFromLatLon TO role_iis_acct
GO

/***********************************
-- Date Created: 13 Jun 2015
-- Author: Kevin Alons
-- Purpose: compute the geography DISTANCE BETWEEN two geography::Point parameters (in MILEs or Kilometers)
-- Changes: 
	- 3.7.28 - 2015/06/18 - KDA - ADDED
***********************************/
CREATE FUNCTION fnDistance(@start geography, @dest geography, @unit varchar(5)) RETURNS decimal(18, 2) AS
BEGIN
	DECLARE @ret decimal(18, 2)
	SELECT @ret = @start.STDistance(@dest)/ CASE WHEN @unit LIKE 'mile%' THEN 1609.344 ELSE 1000 END
	RETURN (@ret)
END
GO
GRANT EXECUTE ON fnDistance TO role_iis_acct
GO

/**********************************
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Route records with translated Origin/Destination values
-- Changes: 
	- 3.7.28 - 2015/06/18 - KDA - ADDED
***********************************/
ALTER VIEW viewRoute AS
SELECT R.*
, Origin = O.Name, OriginFull = O.FullName 
, Destination = D.Name, DestinationFull = D.FullName 
, Active = cast(CASE WHEN O.Active = 1 AND D.Active = 1 THEN 1 ELSE 0 END as bit) 
, ComputedMiles = dbo.fnDistance(dbo.fnPointFromLatLon(O.LAT, O.LON) , dbo.fnPointFromLatLon(D.LAT, D.LON), 'miles')
FROM dbo.tblRoute R
JOIN dbo.viewOrigin O ON O.ID = R.OriginID
JOIN dbo.viewDestination D ON D.ID = R.DestinationID
GO

EXEC _spDropProcedure 'spOrderApprovalSource'
GO
/*************************************************************
-- Date Created: 15 Jun 2015
-- Author: Kevin Alons
-- Purpose: return the data for the Order Approval page
-- Changes: 
	- 3.7.28 - 2015/06/18 - KDA - ADDED
*************************************************************/
CREATE PROCEDURE spOrderApprovalSource(@ShipperID int = NULL, @Start date, @End date, @userName varchar(100), @IncludeApproved bit = 0) 
AS BEGIN
	SELECT O.ID, O.OrderNum, O.OrderDate, Shipper = O.Customer, OA.Approved, OSS.WaitFeeParameterID
		, O.Origin, O.OriginMinutes, OriginNumDesc = OWR.NumDesc, OriginNotes = O.OriginWaitNotes
		, OSS.OriginWaitBillableMinutes, OSS.OriginWaitRateID
		, OriginOverrideMinutes = OA.OriginWaitBillableMinutes
		, O.Destination, O.DestMinutes, DestNumDesc = OWR.NumDesc, DestNotes = O.DestWaitNotes
		, OSS.DestinationWaitBillableMinutes, OSS.DestinationWaitRateID
		, DestinationOverrideMinutes = OA.DestWaitBillableMinutes
		, O.ActualMiles, OA.BillableActualMiles, ComputedMiles = round(R.ComputedMiles, 0)
		, OSS.OrderID
	INTO #X
	FROM viewOrder O
	JOIN viewRoute R ON R.ID = O.RouteID
	LEFT JOIN tblOrderSettlementCarrier OSS ON OSS.OrderID = O.ID
	LEFT JOIN viewOriginWaitReason OWR ON OWR.ID = O.OriginWaitReasonID
	LEFT JOIN viewDestinationWaitReason DWR ON DWR.ID = O.DestWaitReasonID
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
	WHERE O.StatusID IN (3)
	  AND (NULLIF(@ShipperID, -1) IS NULL OR O.CustomerID = @ShipperID)
	  AND (O.OrderDate BETWEEN @Start AND @End)
	  AND (@IncludeApproved = 1 OR isnull(Approved, 0) = 0) 
	  AND OSS.BatchID IS NULL

	DECLARE @id int
	WHILE EXISTS (SELECT * FROM #x WHERE OrderID IS NULL)
	BEGIN
		SELECT @id = min(ID) FROM #x WHERE OrderID IS NULL
		EXEC spApplyRatesCarrier @id, @userName
		UPDATE #x SET OrderID = @id WHERE ID = @id
	END
		
	SELECT * FROM #x
END
GO
GRANT EXECUTE ON spOrderApprovalSource TO role_iis_acct
GO

/*************************************************
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Carrier "Settlement" $$ values to an Delivered/Audited order
-- Changes:
	-- 3.7.28 - 13 Jun 2015 - KDA - add support for: Driver|DriverGroup, Producer to some rates, separate Carrier|Shipper MinSettlementUnits|SettlementFactor best-match values, 
*************************************************/
ALTER PROCEDURE spApplyRatesCarrier
(
  @ID int
, @UserName varchar(100)
, @OriginWaitAmount money = NULL
, @DestWaitAmount money = NULL
, @RejectionAmount money = NULL
, @FuelSurchargeAmount money = NULL
, @LoadAmount money = NULL
, @AssessorialRateTypeID_CSV varchar(max) = NULL
, @AssessorialAmount_CSV varchar(max) = null
, @ResetOverrides bit = 0
) AS BEGIN

	SET NOCOUNT ON
	
	IF EXISTS (SELECT * FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND BatchID IS NOT NULL)
	BEGIN
		RAISERROR('Invoice has already been fully settled', 16, 1)
		RETURN
	END

	-- ensure that Shipper Rates have been applied prior to applying Carrier rates (since they could be dependent on Shipper rates)
	IF NOT EXISTS (SELECT * FROM tblOrderSettlementShipper WHERE OrderID = @ID)
		EXEC spApplyRatesShipper @ID, @UserName
/* test code
drop table #i
drop table #ia

declare @id int, @userName varchar(100)
, @OriginWaitAmount money 
, @DestWaitAmount money 
, @RejectionAmount money 
, @FuelSurchargeAmount money 
, @LoadAmount money 
, @AssessorialRateTypeID_CSV varchar(max) 
, @AssessorialAmount_CSV varchar(max) 
, @ResetOverrides bit 
select @ID=62210,@UserName='kalons'
--select @OriginWaitAmount=1.0000,@DestWaitAmount=2.0000,@RejectionAmount=3.0000,@FuelSurchargeAmount=4.0000,@LoadAmount=5.0000,@AssessorialRateTypeID_CSV='1',@AssessorialAmount_CSV='99'
select @ResetOverrides=0
-- */
	-- get the provided manual Assessorial Rates (if any) into a usable able
	DECLARE @ManualAssessorialRates TABLE (ID int primary key, Amount money)
	INSERT INTO @ManualAssessorialRates (ID, Amount) 
		SELECT ID.ID, R.Amount
		FROM (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), ID FROM dbo.fnSplitCSVIDs(@AssessorialRateTypeID_CSV)) ID
		JOIN (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), amount = CAST(value as money) FROM dbo.fnSplitCSV(@AssessorialAmount_CSV)) R ON R.sortid = ID.sortid

	-- add in an existing manual Assessorial Rates from the existing Order Settlement (if any)
	INSERT INTO @ManualAssessorialRates (ID, Amount)
		SELECT AssessorialRateTypeID, Amount 
		FROM dbo.tblOrderSettlementCarrierAssessorialCharge 
		WHERE OrderID = @ID AND AssessorialRateID IS NULL 
			AND ID NOT IN (SELECT ID FROM @ManualAssessorialRates)
			AND @ResetOverrides = 0
--select * from @ManualAssessorialRates

	IF (@ResetOverrides = 0)
	BEGIN 
		SELECT @OriginWaitAmount = OriginWaitAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @OriginWaitAmount IS NULL AND OriginWaitRateID IS NULL
		SELECT @DestWaitAmount = DestinationWaitAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @DestWaitAmount IS NULL AND DestinationWaitRateID IS NULL
		SELECT @RejectionAmount = OrderRejectAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @RejectionAmount IS NULL AND OrderRejectRateID IS NULL
		SELECT @FuelSurchargeAmount = FuelSurchargeAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @FuelSurchargeAmount IS NULL AND FuelSurchargeRateID IS NULL
		SELECT @LoadAmount = LoadAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @LoadAmount IS NULL AND RouteRateID IS NULL AND RangeRateID IS NULL
	END
--select OverrideOriginWaitAmount = @OriginWaitAmount, OverrideDestWaitAmount = @DestWaitAmount, OverrideRejectionAmount = @RejectionAmount, OverrideFuelSurchargeAmount = @FuelSurchargeAmount, OverrideLoadAmount = @LoadAmount

	DECLARE @SettlementUnits TABLE
	(
		OrderID int
	  , SettlementUomID int
	  , SettlementFactorID int
	  , CarrierSettlementFactorID int
	  , MinSettlementUnits decimal(18, 10)
	  , MinSettlementUnitsID int
	  , ActualUnits decimal(18, 10)
	  , SettlementUnits decimal(18, 10)
	)
	INSERT INTO @SettlementUnits (OrderID, SettlementUomID, SettlementFactorID, CarrierSettlementFactorID, MinSettlementUnits, MinSettlementUnitsID, ActualUnits, SettlementUnits)
		SELECT OrderID, SettlementUomID, SettlementFactorID, CarrierSettlementFactorID, MinSettlementUnits, MinSettlementUnitsID, ActualUnits, SettlementUnits
		FROM dbo.viewOrderSettlementUnitsCarrier 
		WHERE OrderID = @ID

	SELECT OrderID = @ID
		, OrderDate
		, CarrierSettlementFactorID, MinSettlementUnitsID
		, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
		, RouteRateID, RangeRateID, LoadAmount
		, WaitFeeParameterID
		, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount 
		, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount
		, OrderRejectRateID, OrderRejectAmount
		, FuelSurchargeRateID, FuelSurchargeRate, FuelSurchargeAmount
		, OriginTaxRate
		, TotalAmount = ISNULL(LoadAmount, 0) 
			+ ISNULL(OriginWaitAmount, 0) 
			+ ISNULL(DestWaitAmount, 0) 
			+ ISNULL(OrderRejectAmount, 0) 
			+ ISNULL(FuelSurchargeAmount, 0)
		, CreatedByUser = @UserName
	INTO #I
	FROM (
		SELECT OrderDate
			, CarrierSettlementFactorID, MinSettlementUnitsID
			, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
			, RouteRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RouteRateID ELSE NULL END
			, RangeRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RangeRateID ELSE NULL END
			, LoadAmount = round(ISNULL(@LoadAmount, RR.Amount), 2)
			, WaitFeeParameterID
			, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount = ROUND(OriginWaitAmount, 2)
			, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount = ROUND(DestWaitAmount, 2)
			, OrderRejectRateID = CASE WHEN @RejectionAmount IS NULL THEN RD.RateID ELSE NULL END
			, OrderRejectAmount = round(isnull(@RejectionAmount, RD.Amount), 2)
			, FuelSurchargeRateID = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.RateID ELSE NULL END
			, FuelSurchargeRate = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.Rate ELSE NULL END
			, FuelSurchargeAmount = round(isnull(@FuelSurchargeAmount, FSR.Amount), 2)
			, OriginTaxRate
		FROM (
			SELECT O.ID
				, O.OriginID
				, O.CarrierID
				, O.RouteID
				, SU.CarrierSettlementFactorID
				, SU.SettlementFactorID
				, SU.SettlementUomID
				, SU.ActualUnits
				, SU.MinSettlementUnitsID
				, SU.MinSettlementUnits
				, SettlementUnits = CASE WHEN O.Rejected = 1 THEN 0 ELSE SU.SettlementUnits END
				, RouteMiles = O.ActualMiles
				, OrderDate = isnull(O.OrderDate, O.DueDate)
				, O.ChainUp
				, O.H2S
				, O.Rejected
				, O.RerouteCount
				, IsSplit = CASE WHEN O.Rejected = 0 AND O.TicketCount > 1 THEN 1 ELSE 0 END
				, WaitFeeParameterID = WFP.ID
				, OriginWaitRateID = CASE WHEN @OriginWaitAmount IS NULL THEN OWD.RateID ELSE NULL END
				, OriginWaitBillableMinutes = OWD.BillableMinutes
				, OriginWaitBillableHours = OWD.BillableHours
				, OriginWaitAmount = isnull(@OriginWaitAmount, OWD.Amount)
				, DestWaitRateID = CASE WHEN @DestWaitAmount IS NULL THEN DWD.RateID ELSE NULL END
				, DestWaitBillableMinutes = DWD.BillableMinutes
				, DestWaitBillableHours = DWD.BillableHours
				, DestWaitAmount = isnull(@DestWaitAmount, DWD.Amount)
				, OriginTaxRate = OO.TaxRate
			FROM dbo.viewOrder O
			JOIN @SettlementUnits SU ON SU.OrderID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			OUTER APPLY dbo.fnOrderCarrierOriginWaitData(@ID) OWD 
			OUTER APPLY dbo.fnOrderCarrierDestinationWaitData(@ID) DWD 
			OUTER APPLY dbo.fnOrderCarrierWaitFeeParameter(@ID) WFP
			WHERE O.ID = @ID
		) X
		OUTER APPLY dbo.fnOrderCarrierLoadAmount(@ID, SettlementUnits, SettlementUomID) RR 
		OUTER APPLY dbo.fnOrderCarrierOrderRejectData(@ID, SettlementUnits, SettlementUomID) RD
		OUTER APPLY dbo.fnOrderCarrierFuelSurchargeData(@ID) FSR
	) X2
--select * from #I
--select * from #i
	SELECT OrderID = @ID, AssessorialRateTypeID, AssessorialRateID = RateID, Amount = ROUND(Amount, 2), CreatedByUser = @UserName 
	INTO #IA
	FROM @SettlementUnits SU
	CROSS APPLY dbo.fnOrderCarrierAssessorialAmounts(@ID, SU.SettlementUnits, SU.SettlementUomID) CAA
	WHERE CAA.AssessorialRateTypeID NOT IN (SELECT ID FROM @ManualAssessorialRates)
/* more test code
select OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser from #IA
union
select @ID, ID, NULL, Amount, @UserName from @ManualAssessorialRates
--*/
	DECLARE @CreatedTran bit; SET @CreatedTran = CASE WHEN @@TRANCOUNT = 0 THEN 1 ELSE 0 END
	BEGIN TRY
		IF (@CreatedTran = 1)
			BEGIN TRAN

		-- remove the existing settlment record (if any)
		DELETE FROM tblOrderSettlementCarrierAssessorialCharge WHERE OrderID = @ID
		DELETE FROM tblOrderSettlementCarrier WHERE OrderID = @ID
	
		-- all Units and Rates are first normalized to the Order.OriginUOM then consistent processing
		-- and persisted in the Invoice (Settlement) record in this Order.OriginUOM 
		INSERT INTO tblOrderSettlementCarrier (
			OrderID
			, OrderDate
			, CarrierSettlementFactorID
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnitsID
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestinationWaitRateID 
			, DestinationWaitBillableMinutes 
			, DestinationWaitBillableHours
			, DestinationWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser)
		SELECT OrderID
			, OrderDate
			, CarrierSettlementFactorID
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnitsID
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestWaitRateID 
			, DestWaitBillableMinutes 
			, DestWaitBillableHours
			, DestWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser
		FROM #I

		INSERT INTO tblOrderSettlementCarrierAssessorialCharge (OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser)
			SELECT OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser FROM #IA
			UNION
			SELECT @ID, ID, NULL, Amount, @UserName FROM @ManualAssessorialRates
	
		IF (@CreatedTran = 1)
			COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @msg varchar(max), @severity int
		SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY()
		ROLLBACK
		RAISERROR(@msg, @severity, 1)
		RETURN
	END CATCH	
	
END
GO

ALTER TABLE dbo.tblCarrier
	DROP CONSTRAINT FK_Carrier_SettlementFactor
GO
ALTER TABLE dbo.tblSettlementFactor SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblCarrier
	DROP CONSTRAINT FK_Carrier_MinSettlementUom
GO
ALTER TABLE dbo.tblUom SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblCarrier
	DROP CONSTRAINT DF_Carrier_MinSettlementUomID
GO
ALTER TABLE dbo.tblCarrier
	DROP COLUMN MinSettlementUnits, SettlementFactorID, MinSettlementUomID
GO

/*********************************************
-- Date Created: 4 Apr 2013
-- Author: Kevin Alons
-- Purpose: return Carrier records with translated "friendly" values
-- Changes:
	-- 3.7.28 - 6/14/2015 - KDA - remove references to MinSettlement & SettlementFactor (which are now a BestMatch separate table)
**********************************************/
ALTER VIEW viewCarrier AS
	SELECT C.*
		, cast(CASE WHEN C.DeleteDateUTC IS NULL THEN 1 ELSE 0 END as bit) AS Active
		, CT.Name AS CarrierType
		, S.Abbreviation AS StateAbbrev
	FROM tblCarrier C 
	JOIN tblCarrierType CT ON CT.ID = C.CarrierTypeID
	LEFT JOIN tblState S ON S.ID = C.StateID
GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details + FINANCIAL INFO into a single view
-- Changes:
	-- 3.7.28 - 2015/06/18 - KDA - add translated "SettlementFactor" column
/***********************************/
ALTER VIEW viewOrderSettlementCarrier AS 
	SELECT OSC.*
		, BatchNum = SB.BatchNum
		, SettlementUom = UOM.Name
		, SettlementUomShort = UOM.Abbrev
		, RouteRate = RR.Rate
		, RouteRateType = RRT.Name
		, RateSheetRate = RSRR.Rate
		, RateSheetRateType = RSRT.Name
		, OrderRejectRate = ORR.Rate
		, OrderRejectRateType = ORRT.Name
		, OriginWaitRate = OWR.Rate
		, DestinationWaitRate = DWR.Rate
		, TotalWaitAmount = cast(isnull(OSC.OriginWaitAmount, 0) as money) + cast(isnull(OSC.DestinationWaitAmount, 0) as money)
		, ChainupRate = CAR.Rate
		, ChainupRateType = CART.Name
		, ChainupAmount = CA.Amount
		, RerouteRate = RAR.Rate
		, RerouteRateType = RART.Name
		, RerouteAmount = RA.Amount
		, SplitLoadRate = SAR.Rate
		, SplitLoadRateType = SART.Name
		, SplitLoadAmount = SA.Amount
		, H2SRate = HAR.Rate
		, H2SRateType = HART.Name
		, H2SAmount = HA.Amount
		, OtherAmount = OA.Amount
		, WaitFeeSubUnit = isnull(WFSU.Name, 'None') 
		, WaitFeeRoundingType = isnull(WFRT.Name, 'None')
		, SettlementFactor = SF.Name
	FROM tblOrderSettlementCarrier OSC 
	LEFT JOIN tblCarrierSettlementBatch SB ON SB.ID = OSC.BatchID
	LEFT JOIN tblUom UOM ON UOM.ID = OSC.SettlementUomID
	LEFT JOIN tblCarrierOriginWaitRate OWR ON OWR.ID = OSC.OriginWaitRateID
	LEFT JOIN tblCarrierDestinationWaitRate DWR ON DWR.ID = OSC.DestinationWaitRateID
	LEFT JOIN tblCarrierOrderRejectRate ORR ON ORR.ID = OSC.OrderRejectRateID
	LEFT JOIN tblRateType ORRT ON ORRT.ID = ORR.RateTypeID
	-- route rate/ratesheet JOINs
	LEFT JOIN tblCarrierRouteRate RR ON RR.ID = OSC.RouteRateID
	LEFT JOIN tblRateType RRT ON RRT.ID = RR.RateTypeID
	LEFT JOIN viewCarrierRateSheetRangeRate RSRR ON RSRR.ID = OSC.RangeRateID
	LEFT JOIN tblRateType RSRT ON RSRT.ID = RSRR.RateTypeID
	-- chainup
	LEFT JOIN tblOrderSettlementCarrierAssessorialCharge CA ON CA.OrderID = OSC.OrderID AND CA.AssessorialRateTypeID = 1
	LEFT JOIN tblCarrierAssessorialRate CAR ON CAR.ID = CA.AssessorialRateID
	LEFT JOIN tblRateType CART ON CART.ID = CAR.RateTypeID
	-- Reroute
	LEFT JOIN tblOrderSettlementCarrierAssessorialCharge RA ON RA.OrderID = OSC.OrderID AND RA.AssessorialRateTypeID = 2
	LEFT JOIN tblCarrierAssessorialRate RAR ON RAR.ID = RA.AssessorialRateID
	LEFT JOIN tblRateType RART ON RART.ID = RAR.RateTypeID
	-- Split Load
	LEFT JOIN tblOrderSettlementCarrierAssessorialCharge SA ON SA.OrderID = OSC.OrderID AND SA.AssessorialRateTypeID = 3
	LEFT JOIN tblCarrierAssessorialRate SAR ON SAR.ID = SA.AssessorialRateID
	LEFT JOIN tblRateType SART ON SART.ID = SAR.RateTypeID
	-- H2S
	LEFT JOIN tblOrderSettlementCarrierAssessorialCharge HA ON HA.OrderID = OSC.OrderID AND HA.AssessorialRateTypeID = 4
	LEFT JOIN tblCarrierAssessorialRate HAR ON HAR.ID = HA.AssessorialRateID
	LEFT JOIN tblRateType HART ON HART.ID = HAR.RateTypeID
	-- Other Assessorial Charges (combined)
	LEFT JOIN (
		SELECT OrderID, Amount = SUM(Amount) FROM tblOrderSettlementCarrierAssessorialCharge WHERE AssessorialRateTypeID NOT IN (1,2,3,4) GROUP BY OrderID
	) OA ON OA.OrderID = OSC.OrderID 
	LEFT JOIN tblSettlementFactor SF ON SF.ID = OSC.SettlementFactorID
	LEFT JOIN tblCarrierWaitFeeParameter WFP ON WFP.ID = OSC.WaitFeeParameterID
	LEFT JOIN dbo.tblWaitFeeSubUnit WFSU ON WFSU.ID = WFP.SubUnitID
	LEFT JOIN dbo.tblWaitFeeRoundingType WFRT ON WFRT.ID = WFP.RoundingTypeID
GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details + FINANCIAL INFO into a single view
-- Changes:
	-- 3.7.28 - 2015/06/18 - KDA - add new columns: CarrierSettlementFactorID, SettlementFactorID, MinSettlementUnitsID
/***********************************/
ALTER VIEW viewOrder_Financial_Carrier AS 
	SELECT O.* 
		, TicketNums = dbo.fnOrderTicketDetails(O.ID, 'TicketNums', '<br/>') 
		, TankNums = dbo.fnOrderTicketDetails(O.ID, 'TankNums', '<br/>') 
		, PreviousDestinations = dbo.fnRerouteDetails(O.ID, 'PreviousDestinations', '<br/>') 
		, RerouteUsers = dbo.fnRerouteDetails(O.ID, 'RerouteUsers', '<br/>') 
		, RerouteDates = dbo.fnRerouteDetails(O.ID, 'RerouteDates', '<br/>') 
		, RerouteNotes = dbo.fnRerouteDetails(O.ID, 'RerouteNotes', '<br/>') 
		, InvoiceRatesAppliedDate = dbo.fnUTC_To_Local(OS.CreateDateUTC, O.OriginTimeZoneID, O.OriginUseDST)
		, OS.BatchID
		, InvoiceBatchNum = OS.BatchNum 
		, InvoiceOriginWaitBillableMinutes = OS.OriginWaitBillableMinutes
		, InvoiceDestinationWaitBillableMinutes = OS.DestinationWaitBillableMinutes
		, InvoiceTotalWaitBillableMinutes = isnull(OS.OriginWaitBillableMinutes, 0) + ISNULL(OS.DestinationWaitBillableMinutes, 0) 
		, InvoiceWaitFeeParameterID = WaitFeeParameterID
		, InvoiceWaitFeeSubUnit = WaitFeeSubUnit
		, InvoiceWaitFeeRoundingType = WaitFeeRoundingType
		, InvoiceOriginWaitRate = OS.OriginWaitRate 
		, InvoiceOriginWaitAmount = OS.OriginWaitAmount 		
		, InvoiceDestinationWaitRate = OS.DestinationWaitRate 
		, InvoiceDestinationWaitAmount = OS.DestinationWaitAmount 
		, InvoiceTotalWaitAmount = OS.TotalWaitAmount
		, InvoiceOrderRejectRate = OS.OrderRejectRate	-- changed
		, InvoiceOrderRejectRateType = OS.OrderRejectRateType  -- changed
		, InvoiceOrderRejectAmount = OS.OrderRejectAmount  -- changed
		, InvoiceChainupRate = OS.ChainupRate
		, InvoiceChainupRateType = OS.ChainupRateType
		, InvoiceChainupAmount = OS.ChainupAmount 
		, InvoiceRerouteRate = OS.RerouteRate
		, InvoiceRerouteRateType = OS.RerouteRateType
		, InvoiceRerouteAmount = OS.RerouteAmount 
		, InvoiceH2SRate = OS.H2SRate
		, InvoiceH2SRateType = OS.H2SRateType
		, InvoiceH2SAmount = OS.H2SAmount
		, InvoiceSplitLoadRate = OS.SplitLoadRate
		, InvoiceSplitLoadRateType = OS.SplitLoadRateType
		, InvoiceSplitLoadAmount = OS.SplitLoadAmount
		, InvoiceOtherAmount = OS.OtherAmount
		, InvoiceTaxRate = OS.OriginTaxRate
		, InvoiceSettlementUom = OS.SettlementUom -- changed
		, InvoiceSettlementUomShort = OS.SettlementUomShort -- changed
		, InvoiceCarrierSettlementFactorID = OS.CarrierSettlementFactorID -- 2015/06/18 - KDA - added
		, InvoiceSettlementFactorID = OS.SettlementFactorID -- 2015/06/18 - KDA - added
		, InvoiceSettlementFactor = OS.SettlementFactor -- 2015/06/18 - KDA - added
		, InvoiceMinSettlementUnitsID = OS.MinSettlementUnitsID -- 2015/06/18 - KDA - added
		, InvoiceMinSettlementUnits = OS.MinSettlementUnits
		, InvoiceUnits = OS.SettlementUnits
		, InvoiceRouteRate = OS.RouteRate
		, InvoiceRouteRateType = OS.RouteRateType
		, InvoiceRateSheetRate = OS.RateSheetRate
		, InvoiceRateSheetRateType = OS.RateSheetRateType
		, InvoiceFuelSurchargeRate = OS.FuelSurchargeRate
		, InvoiceFuelSurchargeAmount = OS.FuelSurchargeAmount
		, InvoiceLoadAmount = OS.LoadAmount
		, InvoiceTotalAmount = OS.TotalAmount
	FROM dbo.viewOrderLocalDates O
	LEFT JOIN viewOrderSettlementCarrier OS ON OS.OrderID = O.ID
	WHERE O.StatusID NOT IN (-10) -- don't include "Generated" orders (only Assigned+)

GO

EXEC _spRebuildAllObjects
GO

COMMIT
SET NOEXEC OFF