-- rollback
-- select value from tblsetting where id = 0
/*
	- optimize fnDriverUserNames() function for performance (XML STUFF was quite slow)
	- create new VIEW viewReportCenter_Orders
	- add new GPS columns to Report Center - Order report data source
	- update audit Errors text to compute Origin GOV/GSV/NSV error message (instead of more vague Units ... message)
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.2.2'
SELECT  @NewVersion = '3.2.3'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

/*************************************************************/
-- Date Created: 9 Sep 2014
-- Author: Kevin Alons
-- Purpose: return the UserNames (CSV style) for each DriverID
/*************************************************************/
ALTER FUNCTION [dbo].fnDriverUserNames() RETURNS 
	@DriverUserNames TABLE (
		DriverID int
	  , UserNames varchar(1000)
	) AS
BEGIN
	DECLARE @PD TABLE (id int, UserName varchar(100), DriverID int)
	INSERT INTO @PD SELECT ROW_NUMBER() OVER (ORDER BY ProfileValue, UserName), * FROM dbo.fnUserProfileValues('DriverID')

	DECLARE @id int, @driverID int, @priorDriverID int, @name varchar(100), @names varchar(1000);
	

	SELECT TOP 1 @id = id, @priorDriverID = DriverID, @driverID = DriverID, @name = UserName FROM @PD ORDER BY id
	WHILE (@id IS NOT NULL)
	BEGIN
		DELETE FROM @PD WHERE id = @id
		IF (@driverID = @priorDriverID)
			SELECT @names = isnull(@names + ',', '') + @name
		ELSE
		BEGIN
			INSERT INTO @DriverUserNames (DriverID, UserNames) VALUES (@priorDriverID, @names)
			SELECT @priorDriverID = @driverID, @names = @name
		END
		SET @id = NULL
		SELECT TOP 1 @id = id, @driverID = DriverID, @name = UserName FROM @PD ORDER BY id
	END

	RETURN
END

GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Report Center Order data
/***********************************/
CREATE VIEW viewReportCenter_Orders AS
	SELECT O.*
		, OriginGpsLatLon = ltrim(DLO.Lat) + ',' + ltrim(DLO.Lon)
		, OriginLatLon = ltrim(OO.LAT) + ',' + ltrim(OO.LON)
		, OriginAccuracyMeters = DLO.SourceAccuracyMeters
		, OriginDistance = isnull(DLO.DistanceToPoint, 99999)
		, OriginGpsArrived = CASE WHEN DLO.DistanceToPoint IS NOT NULL AND DLO.DistanceToPoint <= OO.GeoFenceRadiusMeters THEN 1 ELSE 0 END
		, DestGpsLatLon = ltrim(DLD.Lat) + ',' + ltrim(DLD.Lon)
		, DestLatLon = ltrim(D.LAT) + ',' + ltrim(D.LON)
		, DestAccuracyMeters = isnull(DLD.SourceAccuracyMeters, 99999)
		, DestDistance = DLD.DistanceToPoint
		, DestGpsArrived = CASE WHEN DLD.DistanceToPoint IS NOT NULL AND DLD.DistanceToPoint <= OO.GeoFenceRadiusMeters THEN 1 ELSE 0 END
	FROM viewOrder_OrderTicket_Full O
	JOIN tblOrigin OO ON OO.ID = O.OriginID
	JOIN tblDestination D ON D.ID = O.DestinationID
	LEFT JOIN viewDriverLocation_OriginFirstArrive DLO ON DLO.OrderID = O.ID AND DLO.OriginID = O.OriginID
	LEFT JOIN viewDriverLocation_DestinationFirstArrive DLD ON DLD.OrderID = O.ID AND DLD.DestinationID = O.DestinationID
GO

GRANT SELECT ON viewReportCenter_Orders TO dispatchcrude_iis_acct
GO

update tblReportDefinition set SourceTable = 'viewReportCenter_Orders' where ID = 1

insert into tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport)
	select 138, 1, 'OriginGpsLatLon', 'Origin Driver GPS', NULL, NULL, 2, NULL, 1, '*', 0
	union
	select 139, 1, 'OriginLatLon', 'Origin Coordinates', NULL, NULL, 2, NULL, 1, '*', 0
	union
	select 140, 1, 'OriginDistance', 'Origin DTP', NULL, NULL, 4, NULL, 1, '*', 0
	union
	select 141, 1, 'OriginGpsArrived', 'Origin GPS Verified?', '\Y\e\s;\N\/\A;\No', NULL, 5, 'SELECT ID=1, Name=''Yes'' UNION SELECT 0, ''No''', 1, '*', 0
	union
	select 142, 1, 'DestGpsLatLon', 'Destination Driver GPS', NULL, NULL, 2, NULL, 1, '*', 0
	union
	select 143, 1, 'DestLatLon', 'Destination Coordinates', NULL, NULL, 2, NULL, 1, '*', 0
	union
	select 144, 1, 'DestDistance', 'Destination DTP', NULL, NULL, 4, NULL, 1, '*', 0
	union
	select 145, 1, 'DestGpsArrived', 'Destination GPS Verified?', '\Y\e\s;\N\/\A;\No', NULL, 5, 'SELECT ID=1, Name=''Yes'' UNION SELECT 0, ''No''', 1, '*', 0
go

update tblReportColumnDefinitionBaseFilter set IncludeWhereClause = '(@ProducerID IN (-1, 0) OR ProducerID = @ProducerID)' where ID = 5
go

/**********************************************************
-- Author: Kevin Alons
-- Date Created: 8 Oct 2014
-- Purpose: return the Audit table records (optionally filtered to just the specified driver and/or ordernum)
***********************************************************/
ALTER FUNCTION [dbo].[fnOrders_AllTickets_Audit](@carrierID int, @driverID int, @orderNum int, @id int) RETURNS TABLE AS RETURN
SELECT *
	, OriginDistanceText = CASE WHEN OriginGpsArrived = 1 THEN 'Verified' ELSE isnull(ltrim(OriginDistance), 'N/A') END
	, DestDistanceText = CASE WHEN DestGpsArrived = 1 THEN 'Verified' ELSE isnull(ltrim(DestDistance), 'N/A') END
	, HasError = cast(CASE WHEN Errors IS NULL THEN 0 ELSE 1 END as bit)
FROM (
	SELECT O.* 
		, OriginGpsLatLon = ltrim(DLO.Lat) + ',' + ltrim(DLO.Lon)
		, OriginLatLon = ltrim(OO.LAT) + ',' + ltrim(OO.LON)
		, OriginAccuracyMeters = DLO.SourceAccuracyMeters
		, OriginDistance = DLO.DistanceToPoint
		, OriginGpsArrived = CASE WHEN DLO.DistanceToPoint IS NOT NULL AND DLO.DistanceToPoint <= OO.GeoFenceRadiusMeters THEN 1 ELSE 0 END
		, DestGpsLatLon = ltrim(DLD.Lat) + ',' + ltrim(DLD.Lon)
		, DestLatLon = ltrim(D.LAT) + ',' + ltrim(D.LON)
		, DestAccuracyMeters = DLD.SourceAccuracyMeters
		, DestDistance = DLD.DistanceToPoint
		, DestGpsArrived = CASE WHEN DLD.DistanceToPoint IS NOT NULL AND DLD.DistanceToPoint <= OO.GeoFenceRadiusMeters THEN 1 ELSE 0 END
		, Errors = 
			nullif(
				SUBSTRING(
					CASE WHEN O.OriginArriveTimeUTC IS NULL THEN ',Missing Pickup Arrival' ELSE '' END
				  + CASE WHEN O.OriginDepartTimeUTC IS NULL THEN ',Missing Pickup Departure' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND O.DestArriveTimeUTC IS NULL THEN ',Missing Delivery Arrival' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND O.DestDepartTimeUTC IS NULL THEN ',Missing Delivery Departure' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND dbo.fnConvertUOM(O.OriginGrossUnits, O.OriginUomID, 1) NOT BETWEEN 100 AND 320 THEN ',Origin GOV Units out of limits' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND dbo.fnConvertUOM(O.OriginGrossStdUnits, O.OriginUomID, 1) NOT BETWEEN 100 AND 320 THEN ',Origin GSV Units out of limits' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND dbo.fnConvertUOM(O.OriginNetUnits, O.OriginUomID, 1) NOT BETWEEN 100 AND 320 THEN ',Origin NSV Units out of limits' ELSE '' END
				  + CASE WHEN C.RequireDispatchConfirmation = 1 AND nullif(rtrim(O.DispatchConfirmNum), '') IS NULL THEN ',Missing Dispatch Confirm #' ELSE '' END
				  + CASE WHEN O.TruckID IS NULL THEN ',Truck Missing' ELSE '' END
				  + CASE WHEN O.TrailerID IS NULL THEN ',Trailer 1 Missing' ELSE '' END
				, 2, 8000)
			, '')
		, IsEditable = cast(CASE WHEN O.StatusID = 3 THEN 1 ELSE 0 END as bit)
	FROM viewOrder_AllTickets O
	JOIN tblOrigin OO ON OO.ID = O.OriginID
	JOIN tblCustomer C ON C.ID = OO.CustomerID
	JOIN tblDestination D ON D.ID = O.DestinationID
	LEFT JOIN tblOrderInvoiceCustomer IOC ON IOC.OrderID = O.ID
	LEFT JOIN viewDriverLocation_OriginFirstArrive DLO ON DLO.OrderID = O.ID AND DLO.OriginID = O.OriginID
	LEFT JOIN viewDriverLocation_DestinationFirstArrive DLD ON DLD.OrderID = O.ID AND DLD.DestinationID = O.DestinationID
	WHERE IOC.BatchID IS NULL /* don't even show SETTLED orders on the AUDIT page ever */
	  AND (isnull(@carrierID, 0) = -1 OR O.CarrierID = @carrierID)
	  AND (isnull(@driverID, 0) = -1 OR O.DriverID = @driverID)
	  AND ((nullif(@orderNum, 0) IS NULL AND O.DeleteDateUTC IS NULL AND (O.StatusID = 3 AND DeliverPrintStatusID IN (SELECT ID FROM tblPrintStatus WHERE IsCompleted = 1))) OR O.OrderNum = @orderNum)
	  AND (nullif(@id, 0) IS NULL OR O.ID LIKE @id)
) X

GO

EXEC _spRebuildAllObjects
GO

COMMIT
SET NOEXEC OFF