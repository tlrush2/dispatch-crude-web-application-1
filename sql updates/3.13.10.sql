SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20) = '3.13.9'
	, @NewVersion varchar(20) = '3.13.10'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1562 - Driver Scheduling - add Driver Shift Type batch edit functionality'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

CREATE TABLE tblDriverWeekShiftType
(
  ID int not null identity(1, 1) constraint PK_DriverWeekShiftType primary key
, Name varchar(100) not null constraint UC_DriverWeekShiftType_Name UNIQUE
, SunStartTime time null
, MonStartTime time null
, TueStartTime time null
, WedStartTime time null
, ThuStartTime time null
, FriStartTime time null
, SatStartTime time null
, CreateDateUTC datetime not null constraint DF_DriverWeekShiftType_CreateDateUTC DEFAULT (getutcdate())
, CreatedByUser varchar(100) not null constraint DF_DriverWeekShiftType_CreatedByUser DEFAULT ('System')
, LastChangeDateUTC datetime null
, LastChangedByUser varchar(100) null
)
GO

INSERT INTO tblDriverWeekShiftType (Name, MonStartTime, TueStartTime, WedStartTime, ThuStartTime, FriStartTime)
	SELECT 'Regular M-F 8AM', '08:00 AM', '08:00 AM', '08:00 AM', '08:00 AM', '08:00 AM'
GO

/****************************************/
-- Created: 2016/07/30 - 3.13.10 - Kevin Alons
-- Purpose: retrieve the defined StartDateTime for the specified id & date value
-- Changes:
/****************************************/
CREATE FUNCTION fnDriverWeekShiftTypeDateValue(@id int, @date date) RETURNS datetime AS
BEGIN
	DECLARE @ret datetime
	DECLARE @weekday char(3) = left(datename(weekday, @date), 3)

	SELECT @ret = CASE left(datename(weekday, @date), 3) 
			WHEN 'Sun' THEN SunStartTime
			WHEN 'Mon' THEN MonStartTime
			WHEN 'Tue' THEN TueStartTime
			WHEN 'Wed' THEN WedStartTime
			WHEN 'Thu' THEN ThuStartTime
			WHEN 'Fri' THEN FriStartTime
			WHEN 'Sat' THEN SatStartTime
		END
	FROM tblDriverWeekShiftType
	WHERE ID = @id

	IF (@ret IS NOT NULL) SET @ret = dbo.fnCombineDateAndTime(@date, cast(@ret as time))

	RETURN (@ret)
END
GO
GRANT EXECUTE ON fnDriverWeekShiftTypeDateValue TO role_iis_acct
GO

/***************************************/
-- Created: 2016/07/30 - 3.13.10 - Kevin Alons
-- Purpose: apply the DriverWeekShiftType to the specified drivers for the specified date range
-- Changes:
/***************************************/
CREATE PROCEDURE spApplyShiftToDrivers
(
  @id int
, @start datetime
, @end datetime
, @driverID_CSV varchar(max)
, @UserName varchar(100)
) AS
BEGIN
	IF (datediff(day, @start, @end) < 0)
	BEGIN
		RAISERROR('Start date cannot be after End date', 16, 1)
		RETURN
	END
	ELSE
	BEGIN
		INSERT INTO tblDriverAvailability (DriverID, AvailDateTime, IsAvailable, CreateDateUTC, CreatedByUser)
			SELECT X.DriverID, isnull(X.AvailDateTime, X.AvailDate), CASE WHEN X.AvailDateTime IS NULL THEN 0 ELSE 1 END, getutcdate(), @UserName
			FROM (
				SELECT DriverID = D.ID, AvailDate, AvailDateTime = dbo.fnDriverWeekShiftTypeDateValue(@id, A.AvailDate)
				FROM (
					SELECT AvailDate = dateadd(day, n-1, @start) FROM viewNumbers1000 WHERE n <= datediff(day, @start, @end)+1
				) A 
				CROSS JOIN dbo.fnSplitCSVIDs(@driverID_CSV) D
			) X
			-- don't overwrite any existing (already defined) DriverAvailability records
			LEFT JOIN tblDriverAvailability DA ON DA.DriverID = X.DriverID AND cast(DA.AvailDateTime as date) = cast(X.AvailDateTime as date)
			WHERE DA.ID IS NULL
	END

END
GO
GRANT EXECUTE ON spApplyShiftToDrivers TO role_iis_acct
GO

COMMIT
SET NOEXEC OFF