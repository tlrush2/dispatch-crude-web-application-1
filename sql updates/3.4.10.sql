-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.4.9'
SELECT  @NewVersion = '3.4.10'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'Add "App Changes" page'
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fnVersionToInt]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION [dbo].[fnVersionToInt]
GO

/***************************************************
-- Date Created: 12/13/2014
-- Author: Kevin Alons
-- Purpose: convert a version value to an int (for comparison/sorting purposes)
***************************************************/
CREATE FUNCTION [dbo].fnVersionToInt
(
	@version varchar(max)
)
RETURNS int
AS
BEGIN
	IF (@version LIKE '%;%' OR @version LIKE '%GO%')
	BEGIN
		RETURN 0
	END

	DECLARE @ID varchar(10), @Pos int, @ret int, @offset int; SELECT @ret = 0, @offset = power(1000, LEN(@version) - LEN(REPLACE(@version, '.', '')))

	SET @version = LTRIM(RTRIM(@version))+ '.'
	SET @Pos = CHARINDEX('.', @version, 1)

	IF REPLACE(@version, '.', '') <> ''
	BEGIN
		WHILE @Pos > 0
		BEGIN
			SET @ret = @ret + @offset * cast(LTRIM(RTRIM(LEFT(@version, @Pos - 1))) as int)
			SET @offset = @offset / 1000
			SET @version = RIGHT(@version, LEN(@version) - @Pos)
			SET @Pos = CHARINDEX('.', @version, 1)

		END
	END	
	RETURN @ret
END

GO
GRANT EXECUTE ON dbo.fnVersionToInt TO dispatchcrude_iis_acct
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[viewAppChanges]'))
	DROP VIEW [dbo].[viewAppChanges]
GO

/***************************************************
-- Date Created: 12/13/2014
-- Author: Kevin Alons
-- Purpose: return a sorted list of AppChanges
***************************************************/
CREATE VIEW viewAppChanges AS
SELECT TOP 100 PERCENT *
	, VersionInt = dbo.fnVersionToInt(VersionNum)
FROM tblAppChanges
ORDER BY VersionInt, ForPublic DESC

GO
GRANT SELECT ON viewAppChanges TO dispatchcrude_iis_acct
GO

COMMIT
SET NOEXEC OFF