-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.0.13'
SELECT  @NewVersion = '4.0.14'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'DCWEB-1606: Order Approval Setting False not Auto Approving Orders'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/*************************************************/
-- Created: 3.13.1 - 2016.07.04 - Kevin Alons
-- Purpose: accomplish any required OrderProcessStatusChanges operations
-- Changes:
-- 4.0.14 - 2016.09.05 - KDA	- fix Auto-Approve failing following an Auto-Approve operation 
--								-- (the new record tblOrderProcessStatusChange record was invalid) + it was added before the DELETE statement removed it)
/*************************************************/
ALTER PROCEDURE spOrderProcessStatusChangesAccomplish AS
BEGIN
	SET NOCOUNT ON
	PRINT 'spOrderProcessStatusChangesAccomplish START: ' + (CONVERT(VARCHAR(24), GETDATE(), 121))

	DECLARE @toAudit bit, @id int

	WHILE EXISTS (SELECT * FROM tblOrderProcessStatusChange WHERE NewStatusID = 3 /*DELIVERED*/ AND OldStatusID <> 4 /*AUDITED*/)
	BEGIN
		SELECT TOP 1 @ID = OrderID FROM tblOrderProcessStatusChange WHERE NewStatusID = 3 /*DELIVERED*/ AND OldStatusID <> 4 /*AUDITED*/

		-- attempt to apply rates to all newly DELIVERED orders
		EXEC spApplyRatesBoth @id, 'System' 

		-- attempt to Auto-Audit this order
		EXEC spAutoAuditOrder @ID, 'System', @toAudit OUTPUT

		-- remove this record to COMPLETE the Auto-Audit process
		DELETE FROM tblOrderProcessStatusChange WHERE OrderID = @ID

		/* if the order was updated to AUDITED status, then we need to attempt also to auto-approve [below] */
		IF (@toAudit = 1)
			-- add a status change record 
			INSERT INTO tblOrderProcessStatusChange (OrderID, OldStatusID, NewStatusID, StatusChangeDateUTC) 
				VALUES( @ID, 3 /*DELIVERED*/, 4 /*AUDITED*/, getutcdate() )
	END

	-- attempt to Auto_Approve any orders not yet approved
	WHILE EXISTS (SELECT OrderID FROM tblOrderProcessStatusChange WHERE NewStatusID = 4 /*AUDITED*/)
	BEGIN
		SELECT TOP 1 @id = OrderID FROM tblOrderProcessStatusChange WHERE NewStatusID = 4 /*AUDITED*/

		EXEC spAutoApproveOrder @ID, 'System'

		DELETE FROM tblOrderProcessStatusChange WHERE OrderID = @ID
	END

	-- ensure any orders Downgraded from AUDITED are automatically Un-APPROVED
	WHILE EXISTS (SELECT OrderID FROM tblOrderProcessStatusChange WHERE OldStatusID = 4 /*AUDITED*/)
	BEGIN
		SELECT TOP 1 @id = OrderID FROM tblOrderProcessStatusChange WHERE OldStatusID = 4 /*AUDITED*/

		DELETE FROM tblOrderAppChanges WHERE OrderID = @ID

		DELETE FROM tblOrderProcessStatusChange WHERE OrderID = @ID
	END

	PRINT 'spOrderProcessStatusChangesAccomplish DONE: ' + (CONVERT(VARCHAR(24), GETDATE(), 121))

END
GO

COMMIT
SET NOEXEC OFF