SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.5.1.1'
SELECT  @NewVersion = '4.5.2'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'JT-1067 - Convert truck compliance pages to MVC'	
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

-- -------------------------------------------------------------
-- Create Truck Compliance Permissions
-- -------------------------------------------------------------

-- Update existing Truck compliance page view permission to fit with new permissions being added below 
UPDATE aspnet_Roles
SET FriendlyName = 'Compliance Docs - View'
	, RoleName = 'viewTruckCompliance'
	, LoweredRoleName = 'viewtruckcompliance'
	, Category = 'Assets - Compliance - Trucks'
	, Description = 'Allow user to view the Truck Compliance page'
WHERE RoleName = 'viewComplianceTruck'
GO

-- Add new Truck compliance permissions
EXEC spAddNewPermission 'createTruckCompliance', 'Allow user to create Truck Compliance Documents', 'Assets - Compliance - Trucks', 'Compliance Docs - Create'
GO
EXEC spAddNewPermission 'editTruckCompliance', 'Allow user to edit Truck Compliance Documents', 'Assets - Compliance - Trucks', 'Compliance Docs - Edit'
GO
EXEC spAddNewPermission 'deactivateTruckCompliance', 'Allow user to deactivate Truck Compliance Documents', 'Assets - Compliance - Trucks', 'Compliance Docs - Deactivate'
GO

-- Add new Truck compliance permissions to any groups that already had the "view" permission 
EXEC spAddNewPermissionToGroupsWithExistingRole 'viewTruckCompliance', 'createTruckCompliance'
GO
EXEC spAddNewPermissionToGroupsWithExistingRole 'viewTruckCompliance', 'editTruckCompliance'
GO
EXEC spAddNewPermissionToGroupsWithExistingRole 'viewTruckCompliance', 'deactivateTruckCompliance'
GO

-- Add new Truck compliance type permissions ("Types" permissions do not get added to users by default when they are new permissions.  They will be added on request when needed by a user.)
EXEC spAddNewPermission 'viewTruckComplianceTypes', 'Allow user to view the Truck Compliance Types page', 'Assets - Compliance - Trucks', 'Compliance Types - View'
GO
EXEC spAddNewPermission 'createTruckComplianceTypes', 'Allow user to create Truck Compliance Types', 'Assets - Compliance - Trucks', 'Compliance Types - Create'
GO
EXEC spAddNewPermission 'editTruckComplianceTypes', 'Allow user to edit Truck Compliance Types', 'Assets - Compliance - Trucks', 'Compliance Types - Edit'
GO
EXEC spAddNewPermission 'deactivateTruckComplianceTypes', 'Allow user to deactivate Truck Compliance Types', 'Assets - Compliance - Trucks', 'Compliance Types - Deactivate'
GO

-- Add new Truck compliance type permissions to any groups that already had the "view" permission 
EXEC spAddNewPermissionToGroupsWithExistingRole 'viewComplianceInspectionTypes', 'viewTruckComplianceTypes'
GO
EXEC spAddNewPermissionToGroupsWithExistingRole 'viewComplianceInspectionTypes', 'createTruckComplianceTypes'
GO
EXEC spAddNewPermissionToGroupsWithExistingRole 'viewComplianceInspectionTypes', 'editTruckComplianceTypes'
GO
EXEC spAddNewPermissionToGroupsWithExistingRole 'viewComplianceInspectionTypes', 'deactivateTruckComplianceTypes'
GO


-- -------------------------------------------------------------
-- Update Truck Compliance Type (formerly Truck Inspection Type) - (Change tblTruckInspectionType to tblTruckComplianceType)
-- -------------------------------------------------------------
EXEC sp_rename N'dbo.tblTruckInspectionType', N'tblTruckComplianceType', 'OBJECT'
GO
EXEC sp_rename 'PK_TruckInspectionType', 'PK_TruckComplianceType'
GO
EXEC sp_rename 'DF_TruckInspectionType_CreateDateUTC', 'DF_TruckComplianceType_CreateDateUTC'
GO
EXEC sp_rename 'DF_TruckInspectionType_Required', 'DF_TruckComplianceType_Required'
GO

ALTER TABLE tblTruckComplianceType ADD IsSystem BIT NOT NULL CONSTRAINT DF_TruckComplianceType_IsSystem DEFAULT 0
GO
--ALTER TABLE tblTruckComplianceType ADD IsRequired BIT NOT NULL CONSTRAINT DF_TruckComplianceType_IsRequired DEFAULT 0
EXEC sp_rename 'dbo.tblTruckComplianceType.Required', 'IsRequired', 'COLUMN'
GO
ALTER TABLE tblTruckComplianceType DROP CONSTRAINT DF_TruckComplianceType_Required
GO
ALTER TABLE tblTruckComplianceType ADD CONSTRAINT DF_TruckComplianceType_IsRequired DEFAULT 0 FOR IsRequired
GO
ALTER TABLE tblTruckComplianceType ADD RequiresDocument BIT NOT NULL CONSTRAINT DF_TruckComplianceType_RequiresDocument DEFAULT 0
GO
--ALTER TABLE tblTruckComplianceType ADD ExpirationLength INT NULL
EXEC sp_rename 'dbo.tblTruckComplianceType.ExpirationMonths', 'ExpirationLength', 'COLUMN'
GO
ALTER TABLE tblTruckComplianceType ALTER COLUMN ExpirationLength INT NULL
GO
ALTER TABLE tblTruckComplianceType ADD TruckTypeID INT NULL CONSTRAINT FK_TruckComplianceType_TruckType REFERENCES tblTruckType(ID)
GO



-- -------------------------------------------------------------
-- Update Truck Compliance (formerly Truck Inspection)
-- -------------------------------------------------------------
EXEC sp_rename N'dbo.tblTruckInspection', N'tblTruckCompliance', 'OBJECT'
GO
EXEC sp_rename 'PK_TruckInspection', 'PK_TruckCompliance'
GO
EXEC sp_rename 'FK_TruckInspection_Truck', 'FK_TruckCompliance_Truck'
GO
EXEC sp_rename 'FK_TruckInspection_Type', 'FK_TruckCompliance_Type'
GO
EXEC sp_rename 'DF_TruckInspection_CreateDateUTC', 'DF_TruckCompliance_CreateDateUTC'
GO
EXEC sp_rename 'DF_TruckInspection_InspectionDate', 'DF_TruckCompliance_InspectionDate'
GO
EXEC sp_rename 'dbo.tblTruckCompliance.TruckInspectionTypeID', 'TruckComplianceTypeID', 'COLUMN'
GO


--ALTER TABLE tblTruckCompliance ADD DocumentName VARCHAR(255) NULL
EXEC sp_rename 'dbo.tblTruckCompliance.DocName', 'DocumentName', 'COLUMN'
GO
--ALTER TABLE tblTruckCompliance ADD DocumentDate SMALLDATETIME NULL CONSTRAINT DF_TruckCompliance_DocumentDate DEFAULT GETDATE()
EXEC sp_rename 'dbo.tblTruckCompliance.InspectionDate', 'DocumentDate', 'COLUMN'
GO
ALTER TABLE tblTruckCompliance ALTER COLUMN DocumentDate SMALLDATETIME NULL
GO
EXEC sp_rename 'DF_TruckCompliance_InspectionDate', 'PK_TruckCompliance_DocumentDate'
GO
ALTER TABLE tblTruckCompliance ADD DeleteDateUTC DATETIME NULL
GO
ALTER TABLE tblTruckCompliance ADD DeletedByUser VARCHAR(100) NULL
GO


/*************************************/
-- Date Created: 03 Feb 2017 - 4.5.2
-- Author: Joe Engler
-- Purpose: ensure only one active compliance record exists
/*************************************/
CREATE TRIGGER trigTruckCompliance_IU ON tblTruckCompliance AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	UPDATE tblTruckCompliance
	SET DeleteDateUTC = ISNULL(i.LastChangeDateUTC, i.CreateDateUTC),
	    DeletedByUser = ISNULL(i.LastChangedByUser, i.CreatedByUser)
	FROM tblTruckCompliance TC
		LEFT JOIN inserted i ON i.TruckID = TC.TruckID AND i.TruckComplianceTypeID = TC.TruckComplianceTypeID
	WHERE i.ID IS NOT NULL AND i.ID <> TC.ID
		AND TC.DeleteDateUTC IS NULL
END
GO

-- Insert new carrier rule type
INSERT INTO tblCarrierRuleType 
VALUES (24, 'Enforce Truck Compliance', 2, 0, 'Flag to check truck compliance when dispatching orders')
GO


-- ----------------------------------------------------------------
-- Update compliance type table
-- ----------------------------------------------------------------

-- Update existing records
ALTER TABLE tblTruckCompliance NOCHECK CONSTRAINT FK_TruckCompliance_Type
GO

UPDATE tblTruckCompliance 
SET TruckComplianceTypeID = TruckComplianceTypeID + 100
GO

SET IDENTITY_INSERT tblTruckComplianceType ON
INSERT INTO tblTruckComplianceType (ID, Name, ExpirationLength, IsRequired, IsSystem, RequiresDocument, TruckTypeID,
		CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser)
SELECT ID+100, Name, ExpirationLength, IsRequired, IsSystem, RequiresDocument, TruckTypeID,
		CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser 
FROM tblTruckComplianceType
	
--Delete existing low numbered records
DELETE FROM tblTruckComplianceType WHERE ID < 100
GO

INSERT INTO tblTruckComplianceType (ID, Name, TruckTypeID, IsSystem, IsRequired, RequiresDocument, ExpirationLength, CreatedByUser)
VALUES (1, 'Registration', NULL, 1, 1, 1, 365, 'System'),
	   (2, 'Insurance ID Card', NULL, 1, 1, 1, 365, 'System')
SET IDENTITY_INSERT tblTruckComplianceType OFF	
GO

ALTER TABLE tblTruckCompliance WITH CHECK CHECK CONSTRAINT FK_TruckCompliance_Type
GO

-- Reseed the Identity #
DBCC CHECKIDENT (tblTruckComplianceType, RESEED, 1001)
GO


-- ----------------------------------------------------------------
-- Copy existing data into new compliance table
-- ----------------------------------------------------------------
INSERT INTO tblTruckCompliance (TruckID, TruckComplianceTypeID, Document, DocumentName, ExpirationDate)
SELECT ID, 1, RegistrationDocument, RegistrationDocName, RegistrationExpiration FROM tblTruck
WHERE RegistrationDocName IS NOT NULL
GO

INSERT INTO tblTruckCompliance (TruckID, TruckComplianceTypeID, Document, DocumentName, DocumentDate, ExpirationDate)
SELECT ID, 2, InsuranceIDCardDocument, InsuranceIDCardDocName, InsuranceIDCardIssue, InsuranceIDCardExpiration FROM tblTruck
WHERE InsuranceIDCardDocName IS NOT NULL
GO


-- ----------------------------------------------------------------
-- Purge old columns from Truck table
-- ----------------------------------------------------------------
ALTER TABLE tblTruck DROP COLUMN RegistrationDocument
GO
ALTER TABLE tblTruck DROP COLUMN RegistrationDocName
GO
ALTER TABLE tblTruck DROP COLUMN RegistrationExpiration
GO
ALTER TABLE tblTruck DROP COLUMN InsuranceIDCardDocName
GO
ALTER TABLE tblTruck DROP COLUMN InsuranceIDCardDocument
GO
ALTER TABLE tblTruck DROP COLUMN InsuranceIDCardExpiration
GO
ALTER TABLE tblTruck DROP COLUMN InsuranceIDCardIssue
GO


-- ----------------------------------------------------------------
-- Refresh views after table changes
-- ----------------------------------------------------------------
EXEC sp_refreshview viewTruck
GO
EXEC sp_refreshview viewTruckMaintenance
GO
EXEC sp_refreshview viewDriverBase
GO
EXEC sp_refreshview viewOrder
GO



-- ----------------------------------------------------------------
-- Views
-- ----------------------------------------------------------------
GO

DROP VIEW viewTruckInspection
GO
/***********************************/
-- Date Created: 5 Aug 2013 
-- Author: Kevin Alons
-- Purpose: return Truck Inspection records with translated "friendly" values + missing required records
-- Changes:
--		4.5.2		2017/02/02		JAE			(Changed name - originally viewTruckInspection) Reworked using the new truck compliance fields added some helper logic (missing, expired, etc)
/***********************************/
CREATE VIEW viewTruckCompliance AS
SELECT TC.ID, 
	TC.TruckID, 
	Truck = T.IDNumber,
	T.CarrierID,
	T.Carrier,
	TC.TruckComplianceTypeID, 
	TruckComplianceType = TCT.Name,
	TC.DocumentName,
	TC.DocumentDate,
	TC.ExpirationDate,
	TC.Notes,
	TC.CreateDateUTC,
	TC.CreatedByuser,
	TC.LastChangeDateUTC,
	TC.LastChangedByUser,
	TC.DeleteDateUTC,
	TC.DeletedByUser,
	MissingDocument = CAST(CASE WHEN Document IS NULL AND TCT.RequiresDocument = 1 THEN 1 ELSE 0 END AS BIT),
	ExpiredNow = CAST(CASE WHEN ExpirationDate < CAST(GETDATE() AS DATE) THEN 1 ELSE 0 END AS BIT),
	ExpiredNext30 = CAST(CASE WHEN ExpirationDate < CAST(DATEADD(MONTH, 1, GETDATE()) AS DATE) AND ExpirationDate >= CAST(GETDATE() AS DATE) THEN 1 ELSE 0 END AS BIT),
	ExpiredNext60 = CAST(CASE WHEN ExpirationDate < CAST(DATEADD(MONTH, 2, GETDATE()) AS DATE) AND ExpirationDate >= CAST(DATEADD(MONTH, 1, GETDATE()) AS DATE) THEN 1 ELSE 0 END AS BIT),
	ExpiredNext90 = CAST(CASE WHEN ExpirationDate < CAST(DATEADD(MONTH, 3, GETDATE()) AS DATE) AND ExpirationDate >= CAST(DATEADD(MONTH, 2, GETDATE()) AS DATE) THEN 1 ELSE 0 END AS BIT)
 FROM tblTruckCompliance TC
 JOIN tblTruckComplianceType TCT ON TC.TruckComplianceTypeID = TCT.ID
 JOIN viewTruck T ON TC.TruckID = T.ID AND (TCT.TruckTypeID IS NULL OR TCT.TruckTypeID = T.TruckTypeID)
GO


-- ----------------------------------------------------------------
-- Functions
-- ----------------------------------------------------------------
GO
/****************************************************
 Date Created: 2017/02/02 - 4.5.2
 Author: Joe Engler
 Purpose: return all carrier compliance with non compliant and missing records
 Changes:
****************************************************/
CREATE FUNCTION fnTruckComplianceSummary(@showCompliant INT = 0, @showUnenforced INT = 0)
RETURNS @ret TABLE
(
	ID INT,
	TruckID INT,
	Truck VARCHAR(40),
	CarrierID INT,
	Carrier VARCHAR(40),
	ComplianceTypeID INT,
	ComplianceType VARCHAR(100),
	MissingDocument BIT,
	ExpiredNow BIT,
	ExpiredNext30 BIT,
	ExpiredNext60 BIT,
	ExpiredNext90 BIT,
	Missing BIT
)
AS BEGIN
 	DECLARE @__ENFORCE_TRUCK_COMPLIANCE__ INT = 24

	INSERT INTO @ret
	SELECT  ID, 
		TruckID, 
		Truck, 
		CarrierID, 
		Carrier, 
		TruckComplianceTypeID, 
		TruckComplianceType, 
		MissingDocument, 
		ExpiredNow, 
		ExpiredNext30, 
		ExpiredNext60, 
		ExpiredNext90, 
		Missing = CAST(0 AS BIT)
	  FROM viewTruckCompliance
	 WHERE DeleteDateUTC IS NULL
	   AND (@showCompliant = 1 OR MissingDocument = 1 OR ExpiredNow = 1 OR ExpiredNext30 = 1 OR ExpiredNext60 = 1 OR ExpiredNext90 = 1)

	UNION
 
	SELECT ID = NULL,
		TruckID = Q.ID,
		Truck = Q.IDNumber,
		CarrierID = Q.ID,
		CarrierName = Q.Carrier,
		ComplianceTypeID = DT.ID,
		ComplianceType = DT.Name,
		MissingDocument = CAST(0 AS BIT),
		ExpiredNow = CAST(0 AS BIT),
		ExpiredNext30 = CAST(0 AS BIT),
		ExpiredNext60 = CAST(0 AS BIT),
		ExpiredNext90 = CAST(0 AS BIT),
		Missing = DT.IsRequired
	FROM ( -- Get active trucks with compliance enforced
			SELECT T.* 
				FROM viewTruck T
				OUTER APPLY dbo.fnCarrierRules(GETUTCDATE(), NULL, @__ENFORCE_TRUCK_COMPLIANCE__, NULL, T.CarrierID, NULL, NULL, NULL, 1) CR_C

				WHERE T.DeleteDateUTC IS NULL --active trucks
				AND (@showUnenforced = 1 OR dbo.fnToBool(CR_C.Value) = 1) -- compliance enforced
		) Q,
		tblTruckComplianceType DT
	WHERE (@showCompliant = 1 OR DT.IsRequired = 1)
		AND NOT EXISTS -- no active record in the truck compliance table
		(SELECT  1
			FROM viewTruckCompliance
			WHERE DeleteDateUTC IS NULL AND TruckID = Q.ID and TruckComplianceTypeID = DT.ID
		)

	RETURN
END
GO


/****************************************************
 Date Created: 2016/12/22 - 4.4.14
 Author: Joe Engler
 Purpose: return carrier, driver, truck and trailer compliance
 Changes:
	- 4.5.2		2017-02-02		JAE			Integrate Truck compliance
****************************************************/
ALTER FUNCTION fnComplianceReport(@showCompliant INT = 0, @showUnenforced INT = 0)
RETURNS TABLE AS
RETURN
	SELECT Category = 'Carrier',
			ItemID = CarrierID, 
			ItemName = Carrier, 
			CarrierID, 
			Carrier,
			ComplianceTypeID, 
			ComplianceType,
			MissingDocument, 
			ExpiredNow, 
			ExpiredNext30, 
			ExpiredNext60, 
			ExpiredNext90, 
			Missing
	FROM fnCarrierComplianceSummary(@showCompliant, @showUnenforced)
	UNION
	SELECT Category = 'Driver',
			ItemID = DriverID, 
			ItemName = FullName,
			CarrierID, 
			Carrier,
			ComplianceTypeID, 
			ComplianceType,
			MissingDocument, 
			ExpiredNow, 
			ExpiredNext30, 
			ExpiredNext60, 
			ExpiredNext90, 
			Missing
	FROM fnDriverComplianceSummary(@showCompliant, @showUnenforced)
	UNION
	SELECT Category = 'Truck',
			ItemID = TruckID, 
			ItemName = Truck,
			CarrierID, 
			Carrier,
			ComplianceTypeID, 
			ComplianceType,
			MissingDocument, 
			ExpiredNow, 
			ExpiredNext30, 
			ExpiredNext60, 
			ExpiredNext90, 
			Missing
	FROM fnTruckComplianceSummary(@showCompliant, @showUnenforced)
	--union
	--trailer (to be written)
GO


-- ----------------------------------------------------------------
-- Refresh all
-- ----------------------------------------------------------------
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRebuildAllObjects
GO
EXEC _spRebuildAllObjects
GO


COMMIT
SET NOEXEC OFF