SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.0.8'
SELECT  @NewVersion = '4.0.9'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1703 Convert Unit of Measure page to MVC'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

	
-- Update existing permission to fit with new permissions below
UPDATE aspnet_Roles
SET Description = 'Allow user to view Units of Measure page'
	, Category = 'System Data Types - UOM'
	, FriendlyName = 'View'
WHERE RoleName = 'viewUnitsOfMeasure'
GO


-- Add new Unit of Measure page permissions		
EXEC spAddNewPermission 'createUnitsOfMeasure','Allow user to create Units of Measure','System Data Types - UOM','Create'
GO
EXEC spAddNewPermission 'editUnitsOfMeasure','Allow user to edit Units of Measure','System Data Types - UOM','Edit'
GO
EXEC spAddNewPermission 'deactivateUnitsOfMeasure','Allow user to deactivate Units of Measure','System Data Types - UOM','Deactivate'
GO



COMMIT
SET NOEXEC OFF