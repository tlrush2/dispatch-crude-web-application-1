SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.5.11'
SELECT  @NewVersion = '4.5.12'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Import Center catch-up'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

-- Skipped from 4.5.2

-- Truck.InsuranceIDCardExpiration
DELETE FROM tblImportCenterFieldDefinition WHERE ObjectFieldID = 260
GO
DELETE FROM tblObjectField WHERE ID = 260
GO

-- Truck.InsuranceIDCardIssue
DELETE FROM tblImportCenterFieldDefinition WHERE ObjectFieldID = 261
GO
DELETE FROM tblObjectField WHERE ID = 261
GO

--Truck.RegistrationExpiration
DELETE FROM tblImportCenterFieldDefinition WHERE ObjectFieldID = 268
GO
DELETE FROM tblObjectField WHERE ID = 268
GO


-- Skipped from 4.5.3

-- Trailer.InsuranceIDCardExpiration
DELETE FROM tblImportCenterFieldDefinition WHERE ObjectFieldID = 239
GO
DELETE FROM tblObjectField WHERE ID = 239
GO

-- Trailer.InsuranceIDCardIssue
DELETE FROM tblImportCenterFieldDefinition WHERE ObjectFieldID = 240
GO
DELETE FROM tblObjectField WHERE ID = 240
GO

--Trailer.RegistrationExpiration
DELETE FROM tblImportCenterFieldDefinition WHERE ObjectFieldID = 247
GO
DELETE FROM tblObjectField WHERE ID = 247
GO

COMMIT
SET NOEXEC OFF