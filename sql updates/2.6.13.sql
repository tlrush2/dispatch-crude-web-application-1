/* 
	add tblOrigin.LegalDescription
	update tblOrder.validate trigger logic to ensure changes to the driver also update the truck/trailer/trailer2 to his/her defaults
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.6.12'
SELECT  @NewVersion = '2.6.13'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

/***********************************************************/
--  Date Created: 22 June 2013
--  Author: Kevin Alons
--  Purpose: Round the Billable Wait minutes into a decimal hour value (based on rounding parameters)
/***********************************************************/
ALTER FUNCTION [dbo].[fnComputeBillableWaitHours](@min int, @SubUnitID int, @RoundingTypeID int) RETURNS decimal (18, 6) AS
BEGIN
	DECLARE @ret decimal(18, 6)
	SELECT @ret = @min
	
	IF (@SubUnitID <> 0 AND @RoundingTypeID <> 0)
	BEGIN
		SELECT @ret = @ret / @SubUnitID
		IF (@RoundingTypeID = 1)
			SELECT @ret = floor(@ret)
		ELSE IF (@RoundingTypeID = 2)
			SELECT @ret = round(@ret - 0.01, 0)
		ELSE IF (@RoundingTypeID = 3)
			SELECT @ret = round(@ret, 0)
		ELSE IF (@RoundingTypeID = 4)
			SELECT @ret = ceiling(@ret)
		ELSE
			SELECT @ret = @min
	END
	
	RETURN @ret / (60.0 / CASE WHEN (@SubUnitID = 0 OR @RoundingTypeID = 0) THEN 1 ELSE @SubUnitID END)
END

GO

ALTER TABLE tblOrderInvoiceCarrier ADD BillableOriginWaitMinutes int null
GO
ALTER TABLE tblOrderInvoiceCarrier ADD BillableDestWaitMinutes int null
GO
ALTER TABLE tblOrderInvoiceCarrier ADD OriginWaitFee money null
GO
ALTER TABLE tblOrderInvoiceCarrier ADD DestWaitFee money null
GO
UPDATE tblOrderInvoiceCarrier
  SET BillableOriginWaitMinutes = S3.BillableOriginWaitMinutes
	, BillableDestWaitMinutes = S3.BillableDestWaitMinutes
	, OriginWaitFee = S3.OriginWaitFee
	, DestWaitFee = S3.DestWaitFee
FROM tblOrderInvoiceCarrier OIC
JOIN (
	SELECT ID
		, BillableOriginWaitHours * WaitRate AS OriginWaitFee
		, BillableDestWaitHours * WaitRate AS DestWaitFee
		, ROUND(BillableOriginWaitHours * 60, 0) AS BillableOriginWaitMinutes
		, round(BillableDestWaitHours * 60, 0) AS BillableDestWaitMinutes
	FROM (
		SELECT S.ID
			, dbo.fnComputeBillableWaitHours(S.OriginWaitMinutes, CR.WaitFeeSubUnitID, CR.WaitFeeRoundingTypeID) AS BillableOriginWaitHours
			, dbo.fnComputeBillableWaitHours(S.DestWaitMinutes, CR.WaitFeeSubUnitID, CR.WaitFeeRoundingTypeID) AS BillableDestWaitHours
			, CR.WaitFee AS WaitRate
		FROM (
			SELECT O.ID, OriginWaitMinutes, DestWaitMinutes
				, coalesce(CRXX.ID, CRXA.ID, CRAX.ID, CRAA.ID) AS CRID
			FROM dbo.viewOrderExportFull O
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			JOIN tblCarrier C ON C.ID = O.CarrierID
			LEFT JOIN tblCarrierRates CRXX ON CRXX.CarrierID = O.CarrierID AND CRXX.RegionID = OO.RegionID
			LEFT JOIN tblCarrierRates CRXA ON CRXA.CarrierID = O.CarrierID AND CRXA.RegionID = -1
			LEFT JOIN tblCarrierRates CRAX ON CRAX.CarrierID = -1 AND CRAX.RegionID = OO.RegionID
			LEFT JOIN tblCarrierRates CRAA ON CRAA.CarrierID = -1 AND CRAA.RegionID = -1
		) S
		LEFT JOIN tblCarrierRates CR ON CR.ID = S.CRID
	) S2
) S3 ON S3.ID = OIC.OrderID
GO
ALTER TABLE tblOrderInvoiceCarrier DROP COLUMN BillableWaitMinutes
GO
ALTER TABLE tblOrderInvoiceCarrier DROP COLUMN WaitFee
GO

ALTER TABLE tblOrderInvoiceCustomer ADD BillableOriginWaitMinutes int null
GO
ALTER TABLE tblOrderInvoiceCustomer ADD BillableDestWaitMinutes int null
GO
ALTER TABLE tblOrderInvoiceCustomer ADD OriginWaitFee money null
GO
ALTER TABLE tblOrderInvoiceCustomer ADD DestWaitFee money null
GO
UPDATE tblOrderInvoiceCarrier
  SET BillableOriginWaitMinutes = S3.BillableOriginWaitMinutes
	, BillableDestWaitMinutes = S3.BillableDestWaitMinutes
	, OriginWaitFee = S3.OriginWaitFee
	, DestWaitFee = S3.DestWaitFee
FROM tblOrderInvoiceCarrier OIC
JOIN (
	SELECT ID
		, BillableOriginWaitHours * WaitRate AS OriginWaitFee
		, BillableDestWaitHours * WaitRate AS DestWaitFee
		, ROUND(BillableOriginWaitHours * 60, 0) AS BillableOriginWaitMinutes
		, round(BillableDestWaitHours * 60, 0) AS BillableDestWaitMinutes
	FROM (
		SELECT S.ID
			, dbo.fnComputeBillableWaitHours(S.OriginWaitMinutes, CR.WaitFeeSubUnitID, CR.WaitFeeRoundingTypeID) AS BillableOriginWaitHours
			, dbo.fnComputeBillableWaitHours(S.DestWaitMinutes, CR.WaitFeeSubUnitID, CR.WaitFeeRoundingTypeID) AS BillableDestWaitHours
			, CR.WaitFee AS WaitRate
		FROM (
			SELECT O.ID, OriginWaitMinutes, DestWaitMinutes
				, coalesce(CRXX.ID, CRXA.ID, CRAX.ID, CRAA.ID) AS CRID
			FROM dbo.viewOrderExportFull O
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			JOIN tblCarrier C ON C.ID = O.CarrierID
			LEFT JOIN tblCarrierRates CRXX ON CRXX.CarrierID = O.CarrierID AND CRXX.RegionID = OO.RegionID
			LEFT JOIN tblCarrierRates CRXA ON CRXA.CarrierID = O.CarrierID AND CRXA.RegionID = -1
			LEFT JOIN tblCarrierRates CRAX ON CRAX.CarrierID = -1 AND CRAX.RegionID = OO.RegionID
			LEFT JOIN tblCarrierRates CRAA ON CRAA.CarrierID = -1 AND CRAA.RegionID = -1
		) S
		LEFT JOIN tblCarrierRates CR ON CR.ID = S.CRID
	) S2
) S3 ON S3.ID = OIC.OrderID
GO
ALTER TABLE tblOrderInvoiceCustomer DROP COLUMN BillableWaitMinutes
GO
ALTER TABLE tblOrderInvoiceCustomer DROP COLUMN WaitFee
GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Carrier "Settlement" $$ values to an Audited order
/***********************************/
ALTER PROCEDURE [dbo].[spProcessCarrierInvoice]
(
  @ID int
, @UserName varchar(100)
, @ChainupFee smallmoney = NULL
, @OriginWaitFee smallmoney = NULL
, @DestWaitFee smallmoney = NULL
, @RerouteFee smallmoney = NULL
, @RejectionFee smallmoney = NULL
, @H2SFee smallmoney = NULL
, @LoadFee smallmoney = NULL
) AS BEGIN
	DELETE FROM tblOrderInvoiceCarrier WHERE OrderID = @ID
	DECLARE @ReportUomID int
	SELECT @ReportUomID = Value FROM tblSetting WHERE ID = 16
	
	-- all Units and Rates are first normalized to GALLONS UOM then processed consistently in Gallons
	-- but then converted to the System.ReportUomID setting UOM units for saving in the Invoice|Settlement record (for display purposes)
	INSERT INTO tblOrderInvoiceCarrier (OrderID
		, ChainupFee, RerouteFee
		, WaitFeeSubUnitID, WaitFeeRoundingTypeID, BillableOriginWaitMinutes, BillableDestWaitMinutes, WaitRate, OriginWaitFee, DestWaitFee
		, UomID, MinSettlementUnits, Units
		, RouteRate, H2SRate, TaxRate
		, RejectionFee, LoadFee, H2SFee, FuelSurcharge
		, TotalFee, CreateDateUTC, CreatedByUser)
	SELECT D.ID
		, ChainupFee, RerouteFee
		, WaitFeeSubUnitID, WaitFeeRoundingTypeID, BillableOriginWaitMinutes, BillableDestWaitMinutes, WaitRate, OriginWaitFee, DestWaitFee
		-- normalize all of these values to the current @ReportUomID UNIT OF MEASURE (for reporting purposes)
		, @ReportUomID, dbo.fnConvertUOM(MinSettlementGallons, 2, @ReportUomID), dbo.fnConvertUOM(ActualGallons, 2, @ReportUomID)
		, dbo.fnConvertRateUOM(RouteRate, 2, @ReportUomID), dbo.fnConvertRateUOM(H2SRate, 2, @ReportUomID), TaxRate
		, RejectionFee, LoadFee, H2SFee, FuelSurcharge
		, RejectionFee + ChainupFee + RerouteFee + OriginWaitFee + DestWaitFee + H2SFee + LoadFee AS TotalFee
		, GETUTCDATE(), @UserName
	FROM (
		-- compute the actual fees or use the manual overide fee values (if provided)
		SELECT ID
			, coalesce(@ChainupFee, Chainup * ChainupFee, 0) AS ChainupFee
			, coalesce(@RerouteFee, RerouteFee * RerouteCount, 0) AS RerouteFee
			, WaitFeeSubUnitID
			, WaitFeeRoundingTypeID
			, cast(round(BillableOriginWaitHours * 60, 0) as int) AS BillableOriginWaitMinutes
			, cast(round(BillableDestWaitHours * 60, 0) as int) AS BillableDestWaitMinutes
			, WaitRate
			, coalesce(@OriginWaitFee, BillableOriginWaitHours * WaitRate, 0) AS OriginWaitFee
			, coalesce(@DestWaitFee, BillableDestWaitHours * WaitRate, 0) AS DestWaitFee
			, coalesce(@RejectionFee, Rejected * RejectionRate, 0) AS RejectionFee
			, H2SRate
			, MinSettlementGallons
			, ActualGallons
			, coalesce(@H2SFee, H2S * dbo.fnMaxDecimal(MinSettlementGallons, ActualGallons) * H2SRate, 0) AS H2SFee
			, isnull(TaxRate, 0) AS TaxRate
			, isnull(RouteRate, 0) AS RouteRate
			-- if rejected, use 0 AS the Rate
			, round(coalesce(@LoadFee, (1 - Rejected) * dbo.fnMaxDecimal(MinSettlementGallons, ActualGallons) * RouteRate, 0), 4) AS LoadFee
			, isnull(FuelSurcharge, 0) AS FuelSurcharge
		FROM (
			-- normalize the Accessorial Rates to GALLONS + add normalized Route Rate and other time normalization
			SELECT S.ID
				, S.ChainUp
				, CR.ChainupFee
				, CR.RerouteFee
				, S.RerouteCount
				, CR.WaitFeeSubUnitID
				, CR.WaitFeeRoundingTypeID
				, dbo.fnComputeBillableWaitHours(S.OriginWaitMinutes, CR.WaitFeeSubUnitID, CR.WaitFeeRoundingTypeID) AS BillableOriginWaitHours
				, dbo.fnComputeBillableWaitHours(S.DestWaitMinutes, CR.WaitFeeSubUnitID, CR.WaitFeeRoundingTypeID) AS BillableDestWaitHours
				, CR.WaitFee AS WaitRate
				, S.Rejected
				, CR.RejectionFee AS RejectionRate
				, S.H2S
				-- normalize the Origin.UOM H2SRate for "Gallons" UOM
				, dbo.fnConvertRateUOM(isnull(S.H2S * CR.H2SRate, 0), OriginUomID, 2) AS H2SRate
				, S.TaxRate
				-- normalize the Origin.UOM Route Rate for "Gallons" UOM
				, dbo.fnConvertRateUom(dbo.fnCarrierRouteRate(S.CarrierID, S.RouteID, S.OrderDate), OriginUomID, 2) AS RouteRate
				, S.MinSettlementGallons
				, isnull(S.ActualGallons, 0) AS ActualGallons
				, CR.FuelSurcharge
			FROM (
				-- get the Order raw data (with Units Normalized to GALLONS) and matching Carrier Accessorial Rate ID
				SELECT O.ID
					, O.CarrierID
					, O.RouteID
					, O.ChainUp
					-- normalize the Order.OriginUOM ActualUnits for "Gallons" UOM
					, dbo.fnConvertUOM(CASE WHEN C.SettlementFactorID = 1 THEN O.OriginGrossUnits ELSE O.OriginNetUnits END, O.OriginUomID, 2) AS ActualGallons
					, O.OriginUomID
					, O.RerouteCount
					, O.OriginWaitMinutes
					, O.DestWaitMinutes
					, O.Rejected
					, OO.H2S
					, O.OrderDate
					, coalesce(CRXX.ID, CRXA.ID, CRAX.ID, CRAA.ID) AS CRID
					-- normalize the Origin.UOM MinSettlementUnits for "Gallons" UOM
					, isnull(dbo.fnConvertUOM(C.MinSettlementUnits, C.MinSettlementUomID, 2), 0) AS MinSettlementGallons
					, OO.TaxRate
				FROM dbo.viewOrderExportFull O
				JOIN tblOrigin OO ON OO.ID = O.OriginID
				JOIN tblCarrier C ON C.ID = O.CarrierID
				LEFT JOIN tblCarrierRates CRXX ON CRXX.CarrierID = O.CarrierID AND CRXX.RegionID = OO.RegionID
				LEFT JOIN tblCarrierRates CRXA ON CRXA.CarrierID = O.CarrierID AND CRXA.RegionID = -1
				LEFT JOIN tblCarrierRates CRAX ON CRAX.CarrierID = -1 AND CRAX.RegionID = OO.RegionID
				LEFT JOIN tblCarrierRates CRAA ON CRAA.CarrierID = -1 AND CRAA.RegionID = -1
				WHERE O.ID = @ID
			) S
			LEFT JOIN tblCarrierRates CR ON CR.ID = S.CRID
		) SS
	) D
END

GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Customer "Settlement" $$ values to an Audited order
/***********************************/
ALTER PROCEDURE [dbo].[spProcessCustomerInvoice]
(
  @ID int
, @UserName varchar(100)
, @ChainupFee smallmoney = NULL
, @OriginWaitFee smallmoney = NULL
, @DestWaitFee smallmoney = NULL
, @RerouteFee smallmoney = NULL
, @RejectionFee smallmoney = NULL
, @H2SFee smallmoney = NULL
, @LoadFee smallmoney = NULL
) AS BEGIN
	DELETE FROM tblOrderInvoiceCustomer WHERE OrderID = @ID
	DECLARE @ReportUomID int
	SELECT @ReportUomID = Value FROM tblSetting WHERE ID = 16
	
	-- all Units and Rates are first normalized to GALLONS UOM then processed consistently in Gallons
	-- but then converted to the System.ReportUomID setting UOM units for saving in the Invoice|Settlement record (for display purposes)
	INSERT INTO tblOrderInvoiceCustomer (OrderID
		, ChainupFee, RerouteFee
		, WaitFeeSubUnitID, WaitFeeRoundingTypeID, BillableOriginWaitMinutes, BillableDestWaitMinutes, WaitRate, OriginWaitFee, DestWaitFee
		, UomID, MinSettlementUnits, Units
		, RouteRate, H2SRate, TaxRate
		, RejectionFee, LoadFee, H2SFee, FuelSurcharge
		, TotalFee, CreateDateUTC, CreatedByUser)
	SELECT D.ID
		, ChainupFee, RerouteFee
		, WaitFeeSubUnitID, WaitFeeRoundingTypeID, BillableOriginWaitMinutes, BillableDestWaitMinutes, WaitRate, OriginWaitFee, DestWaitFee
		-- normalize all of these values to the current @ReportUomID UNIT OF MEASURE (for reporting purposes)
		, @ReportUomID, dbo.fnConvertUOM(MinSettlementGallons, 2, @ReportUomID), dbo.fnConvertUOM(ActualGallons, 2, @ReportUomID)
		, dbo.fnConvertRateUOM(RouteRate, 2, @ReportUomID), dbo.fnConvertRateUOM(H2SRate, 2, @ReportUomID), TaxRate
		, RejectionFee, LoadFee, H2SFee, FuelSurcharge
		, RejectionFee + ChainupFee + RerouteFee + OriginWaitFee + DestWaitFee + H2SFee + LoadFee AS TotalFee
		, GETUTCDATE(), @UserName
	FROM (
		-- compute the actual fees or use the manual overide fee values (if provided)
		SELECT ID
			, coalesce(@ChainupFee, Chainup * ChainupFee, 0) AS ChainupFee
			, coalesce(@RerouteFee, RerouteFee * RerouteCount, 0) AS RerouteFee
			, WaitFeeSubUnitID
			, WaitFeeRoundingTypeID
			, cast(round(BillableOriginWaitHours * 60, 0) as int) AS BillableOriginWaitMinutes
			, cast(round(BillableDestWaitHours * 60, 0) as int) AS BillableDestWaitMinutes
			, WaitRate
			, coalesce(@OriginWaitFee, BillableOriginWaitHours * WaitRate, 0) AS OriginWaitFee
			, coalesce(@DestWaitFee, BillableDestWaitHours * WaitRate, 0) AS DestWaitFee
			, coalesce(@RejectionFee, Rejected * RejectionRate, 0) AS RejectionFee
			, H2SRate
			, MinSettlementGallons
			, ActualGallons
			, coalesce(@H2SFee, H2S * dbo.fnMaxDecimal(MinSettlementGallons, ActualGallons) * H2SRate, 0) AS H2SFee
			, isnull(TaxRate, 0) AS TaxRate
			, isnull(RouteRate, 0) AS RouteRate
			-- if rejected, use 0 AS the Rate
			, round(coalesce(@LoadFee, (1 - Rejected) * dbo.fnMaxDecimal(MinSettlementGallons, ActualGallons) * RouteRate, 0), 4) AS LoadFee
			, isnull(FuelSurcharge, 0) AS FuelSurcharge
		FROM (
			-- normalize the Accessorial Rates to GALLONS + add normalized Route Rate and other time normalization
			SELECT S.ID
				, S.ChainUp
				, CR.ChainupFee
				, CR.RerouteFee
				, S.RerouteCount
				, CR.WaitFeeSubUnitID
				, CR.WaitFeeRoundingTypeID
				, dbo.fnComputeBillableWaitHours(S.OriginWaitMinutes, CR.WaitFeeSubUnitID, CR.WaitFeeRoundingTypeID) AS BillableOriginWaitHours
				, dbo.fnComputeBillableWaitHours(S.DestWaitMinutes, CR.WaitFeeSubUnitID, CR.WaitFeeRoundingTypeID) AS BillableDestWaitHours
				, CR.WaitFee AS WaitRate
				, S.Rejected
				, CR.RejectionFee AS RejectionRate
				, S.H2S
				-- normalize the Origin.UOM H2SRate for "Gallons" UOM
				, dbo.fnConvertRateUOM(isnull(S.H2S * CR.H2SRate, 0), OriginUomID, 2) AS H2SRate
				, S.TaxRate
				-- normalize the Origin.UOM Route Rate for "Gallons" UOM
				, dbo.fnConvertRateUom(dbo.fnCustomerRouteRate(S.CustomerID, S.RouteID, S.OrderDate), OriginUomID, 2) AS RouteRate
				, S.MinSettlementGallons
				, isnull(S.ActualGallons, 0) AS ActualGallons
				, CR.FuelSurcharge
			FROM (
				-- get the Order raw data (with Units Normalized to GALLONS) and matching Customer Accessorial Rate ID
				SELECT O.ID
					, O.CustomerID
					, O.RouteID
					, O.ChainUp
					-- normalize the Order.OriginUOM ActualUnits for "Gallons" UOM
					, dbo.fnConvertUOM(CASE WHEN C.SettlementFactorID = 1 THEN O.OriginGrossUnits ELSE O.OriginNetUnits END, O.OriginUomID, 2) AS ActualGallons
					, O.OriginUomID
					, O.RerouteCount
					, O.OriginWaitMinutes
					, O.DestWaitMinutes
					, O.Rejected
					, OO.H2S
					, O.OrderDate
					, coalesce(CRXX.ID, CRXA.ID, CRAX.ID, CRAA.ID) AS CRID
					-- normalize the Origin.UOM MinSettlementUnits for "Gallons" UOM
					, isnull(dbo.fnConvertUOM(C.MinSettlementUnits, C.MinSettlementUomID, 2), 0) AS MinSettlementGallons
					, OO.TaxRate
				FROM dbo.viewOrderExportFull O
				JOIN tblOrigin OO ON OO.ID = O.OriginID
				JOIN tblCustomer C ON C.ID = O.CustomerID
				LEFT JOIN tblCustomerRates CRXX ON CRXX.CustomerID = O.CustomerID AND CRXX.RegionID = OO.RegionID
				LEFT JOIN tblCustomerRates CRXA ON CRXA.CustomerID = O.CustomerID AND CRXA.RegionID = -1
				LEFT JOIN tblCustomerRates CRAX ON CRAX.CustomerID = -1 AND CRAX.RegionID = OO.RegionID
				LEFT JOIN tblCustomerRates CRAA ON CRAA.CustomerID = -1 AND CRAA.RegionID = -1
				WHERE O.ID = @ID
			) S
			LEFT JOIN tblCustomerRates CR ON CR.ID = S.CRID
		) SS
	) D
END

GO

COMMIT
SET NOEXEC OFF