SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.10.5.2'
SELECT  @NewVersion = '3.10.6'

-- only ensure the curr version matches when we are not testing with an "x.x.x" DEV version
IF @NewVersion <> 'x.x.x' AND (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

-- only update the DB VERSION when not testing with an "x.x.x" DEV version
IF @NewVersion <> 'x.x.x' 
BEGIN
	UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

	INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
		SELECT @NewVersion, 0, 'DCWEB-1010:  Add Google maps link to routes page where coordinates are available to map the route.'
		EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
END
GO


/***********************************
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Origin records with translated value and FullName field (which includes Origin Type)
-- Changes:
	- 3.8.11 - 2015/07/28 - KDA - remove Customer info (no longer 1 to 1 relationship with Origin
	- 3.10.6 - 2016/02/01 - BB - Added LatLon column for Route page maps
***********************************/
ALTER VIEW [dbo].[viewOrigin] AS
SELECT O.*
	, FullName = CASE WHEN O.H2S = 1 THEN 'H2S-' ELSE '' END + OT.OriginType + ' - ' + O.Name
	, OT.OriginType
	, State = S.FullName 
	, StateAbbrev = S.Abbreviation 
	, S.CountryID
	, Country = CO.Name, CountryShort = CO.Abbrev
	, Operator = OP.Name
	, Pumper = P.FullName
	, TicketType = TT.Name
	, TT.ForTanksOnly
	, Region = R.Name
	, Producer = PR.Name 
	, TimeZone = TZ.Name 
	, UOM = UOM.Name 
	, UomShort = UOM.Abbrev 
	, OT.HasTanks
	, Active = cast(CASE WHEN O.DeleteDateUTC IS NULL THEN 1 ELSE 0 END as bit)
	, TankCount = (SELECT COUNT(*) FROM tblOriginTank WHERE OriginID = O.ID AND DeleteDateUTC IS NULL) 
	, LatLon = O.LAT + ',' + O.LON
FROM dbo.tblOrigin O
JOIN dbo.tblOriginType OT ON OT.ID = O.OriginTypeID
LEFT JOIN tblState S ON S.ID = O.StateID
LEFT JOIN tblCountry CO ON CO.ID = S.CountryID
LEFT JOIN dbo.tblOperator OP ON OP.ID = O.OperatorID
LEFT JOIN dbo.viewPumper P ON P.ID = O.PumperID
LEFT JOIN dbo.tblTicketType TT ON TT.ID = O.TicketTypeID
LEFT JOIN dbo.tblRegion R ON R.ID = O.RegionID
LEFT JOIN dbo.tblProducer PR ON PR.ID = O.ProducerID
LEFT JOIN dbo.tblTimeZone TZ ON TZ.ID = O.TimeZoneID
LEFT JOIN dbo.tblUom UOM ON UOM.ID = O.UomID
GO

EXEC sp_RefreshView viewOrigin
GO


/***********************************
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Destination records with translated value and FullName field (which includes Destination Type)
-- Changes:
	- 3.10.6 - 2016/02/01 - BB - Added LatLon column for Route page maps
***********************************/
ALTER VIEW [dbo].[viewDestination] AS
SELECT D.* 
	, Active = cast(CASE WHEN D.DeleteDateUTC IS NULL THEN 1 ELSE 0 END as bit)
	, DT.DestinationType
	, DT.DestinationType + ' - ' + D.Name AS FullName
	, S.FullName AS State, S.Abbreviation AS StateAbbrev
	, S.CountryID
	, Country = CO.Name, CountryShort = CO.Abbrev
	, R.Name AS Region
	, DTT.Name AS TicketType
	, TZ.Name AS TimeZone
	, UOM.Name AS UOM
	, UOM.Abbrev AS UomShort
	, LatLon = D.LAT + ',' + D.LON
FROM dbo.tblDestination D
JOIN dbo.tblDestinationType DT ON DT.ID = D.DestinationTypeID
JOIN dbo.tblDestTicketType DTT ON DTT.ID = D.TicketTypeID
LEFT JOIN tblState S ON S.ID = D.StateID
LEFT JOIN tblCountry CO ON CO.ID = S.CountryID
LEFT JOIN dbo.tblRegion R ON R.ID = D.RegionID
LEFT JOIN dbo.tblTimeZone TZ ON TZ.ID = D.TimeZoneID
LEFT JOIN dbo.tblUom UOM ON UOM.ID = D.UomID
GO

EXEC sp_RefreshView viewDestination
GO


/**********************************
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Route records with translated Origin/Destination values
-- Changes: 
	- 3.7.28 - 2015/06/18 - KDA - ADDED
	- 3.10.6 - 2016/02/01 - BB - Added MapLinkGPSData column for Route page maps
***********************************/
ALTER VIEW [dbo].[viewRoute] AS
SELECT R.*
, Origin = O.Name, OriginFull = O.FullName 
, Destination = D.Name, DestinationFull = D.FullName 
, Active = cast(CASE WHEN O.Active = 1 AND D.Active = 1 THEN 1 ELSE 0 END as bit) 
, ComputedMiles = dbo.fnDistance(dbo.fnPointFromLatLon(O.LAT, O.LON) , dbo.fnPointFromLatLon(D.LAT, D.LON), 'miles')
, MapLinkGPSData = (CASE WHEN O.LatLon IS NOT NULL AND D.LatLon IS NOT NULL THEN O.LatLon + '/' + D.LatLon ELSE NULL END)
FROM dbo.tblRoute R
JOIN dbo.viewOrigin O ON O.ID = R.OriginID
JOIN dbo.viewDestination D ON D.ID = R.DestinationID
GO

EXEC sp_RefreshView viewRoute
GO


COMMIT
SET NOEXEC OFF