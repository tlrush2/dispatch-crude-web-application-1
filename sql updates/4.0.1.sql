SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.0.0.1'
SELECT  @NewVersion = '4.0.1'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1399: Add column for capturing time zone offset (Questionnaires and HOS)'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


ALTER TABLE tblQuestionnaireSubmission ADD SubmissionTZOffset INT
GO

ALTER TABLE tblHos ADD LogTZOffset INT
ALTER TABLE tblHosDBAudit ADD LogTZOffset INT
GO


/*****************************************
-- Date Created: 2016/06/16
-- Author: Joe Engler
-- Purpose: Audit history for HOS entries
-- Changes
--		4.0.1		JAE		2016-08-19		Add TZ Offset for attempting to grab driver's local time when entry was submitted
*****************************************/
ALTER TRIGGER [dbo].[trigHos_IU] ON [dbo].[tblHos] AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	-- only do anything if something actually changed
	IF EXISTS (SELECT * FROM inserted EXCEPT SELECT * FROM deleted)
	BEGIN
		/* START DB AUDIT *********************************************************/
		BEGIN TRY
			IF EXISTS(SELECT * FROM tblSetting WHERE ID = 32 AND (Value LIKE 'true' OR Value LIKE 'yes'))
				INSERT INTO tblHosDbAudit (DBAuditDate, ID, UID, DriverID, HosDriverStatusID, HosStatusID, Lat, Lon, NearestCity, PersonalUse, YardMove, LogDateUTC, LogTZOffset
												, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser)
					SELECT GETUTCDATE(), ID, UID, DriverID, HosDriverStatusID, HosStatusID, Lat, Lon, NearestCity, PersonalUse, YardMove, LogDateUTC, LogTZOffset
												, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser
					FROM deleted d
		END TRY
		BEGIN CATCH
			PRINT 'trigHos_IU.DBAUDIT FAILURE: ' + ERROR_MESSAGE()
		END CATCH
		/* END DB AUDIT *********************************************************/
	END
END

GO


COMMIT
SET NOEXEC OFF