-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.7.40.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.7.40.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0

DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.7.40'
SELECT  @NewVersion = '3.7.41'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-801: remove Shipper (customer).SettlementFactorID & MinSettlementUomID fields (not implemented with Best Match rate type records)'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

ALTER TABLE tblCustomer DROP CONSTRAINT FK_Customer_SettlementFactor
GO
ALTER TABLE tblCustomer DROP COLUMN SettlementFactorID
GO
ALTER TABLE tblCustomer DROP CONSTRAINT FK_Customer_MinSettlementUom
GO
ALTER TABLE tblCustomer DROP CONSTRAINT DF_Customer_MinSettlementUomID
GO
ALTER TABLE tblCustomer DROP COLUMN MinSettlementUomID
GO

/***********************************
-- Date Created: 27 Jan 2013
-- Author: Kevin Alons
-- Purpose: return Customer records with "translated friendly" values for FK relationships
-- Changes:
	- 3.7.41 - 2015/07/01 - KDA - remove UOM/SettlementFactor related columns
***********************************/
ALTER VIEW [viewCustomer] AS
SELECT C.*
	, S.Abbreviation AS StateAbbrev
	, S.FullName AS State
FROM dbo.tblCustomer C
LEFT JOIN dbo.tblState S ON S.ID = C.StateID

GO

EXEC _spRebuildAllObjects
GO

COMMIT
SET NOEXEC OFF