/* SQL Update supporting DCDRV-266 (Photos) by Geoff Mochau */

SET NOEXEC OFF
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.9.30'
SELECT  @NewVersion = '3.9.31'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'Add new OrderPhotos & PhotoType tables for Location & Status documentation capture.  Photos availble on Print BOLs page.'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/* Create photo type table.*/
CREATE TABLE tblPhotoType
(
  ID tinyint NOT NULL CONSTRAINT PK_PhotoType PRIMARY KEY
, Name varchar(25) NOT NULL
, CreateDateUTC smalldatetime NOT NULL CONSTRAINT DF_PhotoType_CreateDate DEFAULT (getutcdate())
, CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_PhotoType_CreatedBy DEFAULT ('System')
)
GO
GRANT SELECT ON tblPhotoType TO role_iis_acct
GO
CREATE UNIQUE INDEX udxPhotoType_Name ON tblPhotoType(Name)
GO
	
/* Create default photo types. */
INSERT INTO tblPhotoType  (ID,Name)
	VALUES
			(1, 'Location')
		,	(2, 'Reject')
GO

/* Create order photo table.  Stores photos and the orderID they're associated with */
CREATE TABLE tblOrderPhoto
(
  ID int IDENTITY(1,1) NOT NULL CONSTRAINT PK_OrderPhoto PRIMARY KEY
, UID uniqueidentifier NOT NULL
, OrderID int NOT NULL CONSTRAINT FK_OrderPhoto_Order FOREIGN KEY REFERENCES tblOrder(ID)
, OriginID int NULL CONSTRAINT FK_OrderPhoto_Origin FOREIGN KEY REFERENCES tblOrigin(ID)
, DestinationID int NULL CONSTRAINT FK_OrderPhoto_Destination FOREIGN KEY REFERENCES tblDestination(ID)
, PhotoTypeID tinyint NULL CONSTRAINT FK_OrderPhoto_PhotoType FOREIGN KEY REFERENCES tblPhotoType(ID)
, DriverID int NULL CONSTRAINT FK_OrderPhoto_Driver FOREIGN KEY REFERENCES tblDriver(ID)
, PhotoBlob varbinary(max) NULL
, GpsData varchar (100) NULL
, CreateDateUTC smalldatetime NOT NULL CONSTRAINT DF_OrderPhoto_CreateDate DEFAULT (getutcdate())
, CreatedByUser varchar(100) NOT NULL
)
GO
GRANT SELECT, INSERT, UPDATE ON tblOrderPhoto TO role_iis_acct
GO

/* Create system level photo quality setting */
INSERT INTO tblSetting (ID, Category, Name, SettingTypeID, Value, ReadOnly, CreateDateUTC, CreatedByUser)
	VALUES (57, 'Driver App Settings', 'Photo Quality', 3, 50, 0, GETUTCDATE(), 'System')
GO

/* 12/4/15 BB - Create order rules for driver app photos */
INSERT INTO tblOrderRuleType (ID, Name, RuleTypeID)
	SELECT 9, 'Require Reject Photo for rejected Order?', 2
	UNION
	SELECT 10, 'Require Location Photo at Origin?', 2
	UNION
	SELECT 11, 'Require Location Photo at Destination?', 2	
  EXCEPT SELECT ID, Name, RuleTypeID 
FROM tblOrderRuleType
GO

/***********************************
 Date Created: 28 Feb 2013
 Author: Kevin Alons
 Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc for 3rd parties (Customer/Carrier)
 Changes:
	??		- 8/18/15  - BB		- Made delivered orders possible to print eBOLs page
	3.9.31	- 12/04/15 - JAE&BB - Join tblOrderPhoto for access to most recent photos on eBOL print page
								- convert @StartDate & @EndDate to date data type (eliminates need to use fnDateOnly() )
			- 12/09/15 - BB     - Made reject photo accessible separately
***********************************/
ALTER PROCEDURE [dbo].[spOrdersBasicExport]
(
  @StartDate date
, @EndDate date
, @CarrierID int = 0 -- -1 = all carriers
, @CustomerID int = 0 -- -1 = all customers
, @ProducerID int = 0
, @OrderNum varchar(20) = NULL
, @StatusID_CSV varchar(max) = '3,4'
) AS BEGIN	
	SELECT OE.*
		, OriginPhotoID = (SELECT TOP 1 OP.ID FROM tblOrderPhoto OP WHERE OrderID = OE.ID AND OP.OriginID IS NOT NULL AND OP.PhotoTypeID = 1 ORDER BY OP.CreateDateUTC DESC)
		, RejectPhotoID = (SELECT TOP 1 OP.ID FROM tblOrderPhoto OP WHERE OrderID = OE.ID AND OE.Rejected = 1 AND OP.PhotoTypeID = 2 ORDER BY OP.CreateDateUTC DESC)
		, DestPhotoID = (SELECT TOP 1 OP.ID FROM tblOrderPhoto OP WHERE OrderID = OE.ID AND OP.DestinationID IS NOT NULL AND OP.PhotoTypeID = 1 ORDER BY OP.CreateDateUTC DESC)		
	FROM dbo.viewOrderExportFull_Reroute OE
	WHERE (@CarrierID=-1 OR @CustomerID=-1 OR CarrierID=@CarrierID OR OE.CustomerID=@CustomerID OR OE.ProducerID=@ProducerID) 
	  AND OE.OrderDate BETWEEN @StartDate AND @EndDate
	  AND OE.DeleteDateUTC IS NULL
	  AND (OE.StatusID IN (SELECT ID FROM dbo.fnSplitCSVIDs(@StatusID_CSV)))
	  AND (@OrderNum IS NULL OR OE.OrderNum LIKE @OrderNum)
	ORDER BY OE.OrderDate, OE.OrderNum
END
GO

COMMIT
SET NOEXEC OFF