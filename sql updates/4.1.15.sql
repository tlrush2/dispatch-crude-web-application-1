SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.1.14'
SELECT  @NewVersion = '4.1.15'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1833 - Convert product groups page to MVC.'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO



/* Add date tracking columns to product group table */
ALTER TABLE tblProductGroup
	ADD CreateDateUTC SMALLDATETIME NULL
		, CreatedByUser VARCHAR(100) NULL
		, LastChangeDateUTC SMALLDATETIME NULL
		, LastChangedByUser VARCHAR(100) NULL
		, DeleteDateUTC SMALLDATETIME NULL
		, DeletedByUser VARCHAR(100) NULL
GO


/* Set initial create date since the field didn't exist before */
UPDATE tblProductGroup
	SET CreatedByUser = 'System'
	, CreateDateUTC = '2015-01-01 06:00:00'
GO



-- Update existing product group view permission to fit with new permissions below
UPDATE aspnet_Roles
SET FriendlyName = 'Product Groups - View'
WHERE RoleName = 'viewProductGroups'
GO



-- Add new permissions for product groups
EXEC spAddNewPermission 'createProductGroups', 'Allow user to create Product Groups', 'Products', 'Product Groups - Create'
GO
EXEC spAddNewPermission 'editProductGroups', 'Allow user to edit Product Groups', 'Products', 'Product Groups - Edit'
GO
EXEC spAddNewPermission 'deactivateProductGroups', 'Allow user to deactivate Product Groups', 'Products', 'Product Groups - Deactivate'
GO



EXEC _spRefreshAllViews
GO
EXEC _spRebuildAllObjects
GO
EXEC _spRecompileAllStoredProcedures
GO



COMMIT
SET NOEXEC OFF