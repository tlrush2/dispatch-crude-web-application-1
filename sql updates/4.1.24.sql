SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.1.23'
SELECT  @NewVersion = '4.1.24'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-965 - Convert Operator Maintenance page to MVC.'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO



/* Update existing page view permission to fit with new permissions being added below */
UPDATE aspnet_Roles
SET FriendlyName = 'Operators - View'
WHERE RoleName = 'viewOperatorMaintenance'
GO



/* Add new permissions for the operator maintenance page */
EXEC spAddNewPermission 'createOperatorMaintenance', 'Allow user to create Operators', 'Maintenance', 'Operators - Create'
GO
EXEC spAddNewPermission 'editOperatorMaintenance', 'Allow user to edit Operators', 'Maintenance', 'Operators - Edit'
GO
EXEC spAddNewPermission 'deactivateOperatorMaintenance', 'Allow user to deactivate Operators', 'Maintenance', 'Operators - Deactivate'
GO



COMMIT
SET NOEXEC OFF