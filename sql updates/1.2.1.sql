BEGIN TRANSACTION
GO

UPDATE tblSetting SET Value = '1.2.1' WHERE ID=0
GO

ALTER TABLE tblDriver DROP COLUMN PIN
GO

ALTER TABLE tblDriver DROP CONSTRAINT DF_tblDriver_DLonFile
GO

ALTER TABLE tblDriver DROP COLUMN DLONFILE
GO

ALTER TABLE tblDriver ADD RegionID int null
GO

ALTER TABLE tblDriver ADD OperatingStateID int null
GO

ALTER TABLE tblDriver ADD SSN varchar(13) null
GO

ALTER TABLE tblDriver ADD HireDate smalldatetime null
GO

ALTER TABLE tblDriver ADD DLDocument varbinary(max) null
GO
ALTER TABLE tblDriver ADD DLDocName varchar(100) null
GO

ALTER TABLE tblDriver ADD CDLNumber varchar(20) null
GO

ALTER TABLE tblDriver ADD CDLDocument varbinary(max) null
GO
ALTER TABLE tblDriver ADD CDLDocName varchar(100) null
GO
ALTER TABLE tblDriver ADD CDLExpiration smalldatetime null
GO
ALTER TABLE tblDriver ADD Endorsements varchar(255) null
GO
ALTER TABLE tblDriver ADD Restrictions varchar(255) null
GO

ALTER TABLE tblDriver ADD DrugScreenBackgroundCheckDocument varbinary(max) null
GO
ALTER TABLE tblDriver ADD DrugScreenBackgroundCheckDocName varchar(100) null
GO

ALTER TABLE tblDriver ADD MotorVehicleReportDocument varbinary(max) null
GO
ALTER TABLE tblDriver ADD MotorVehicleReportDocName varchar(100) null
GO

ALTER TABLE tblDriver ADD MedicalCardDocument varbinary(max) null
GO
ALTER TABLE tblDriver ADD MedicalCardDocName varchar(100) null
GO
ALTER TABLE tblDriver ADD MedicalCardDate smalldatetime null
GO

ALTER TABLE tblDriver ADD LongFormPhysicalDocument varbinary(max) null
GO
ALTER TABLE tblDriver ADD LongFormPhysicalDocName varchar(100) null
GO
ALTER TABLE tblDriver ADD LongFormPhysicalDate smalldatetime null
GO

ALTER TABLE tblDriver ADD EmploymentAppDocument varbinary(max) null
GO
ALTER TABLE tblDriver ADD EmploymentAppDocName varchar(100) null
GO

ALTER TABLE tblDriver ADD PolicyManualReceiptDocument varbinary(max) null
GO
ALTER TABLE tblDriver ADD PolicyManualReceiptDocName varchar(100) null
GO

ALTER TABLE tblDriver ADD TrainingReceiptDocument varbinary(max) null
GO
ALTER TABLE tblDriver ADD TrainingReceiptDocName varchar(100) null
GO

ALTER TABLE tblDriver ADD OrientationTestDocument varbinary(max) null
GO
ALTER TABLE tblDriver ADD OrientationTestDocName varchar(100) null
GO

EXEC _spRefreshAllViews
GO

EXEC _spRecompileAllStoredProcedures
GO

COMMIT