/* fix/update some Route Rate maintenance functionality
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.1.6', @NewVersion = '2.1.7'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

/***********************************/
-- Date Created: 9 Jun 2013
-- Author: Kevin Alons
-- Purpose: return Carrier route rates with translated "friendly" values + "missing route rates", computed Earliest & End Date values)
/***********************************/
ALTER VIEW [dbo].[viewCarrierRouteRates] AS
SELECT RB.ID, R.ID AS RouteID, isnull(RB.Rate, 0) AS Rate, isnull(RB.EffectiveDate, dbo.fnDateOnly(ORD.MinOpenOrderDate)) AS EffectiveDate
	, cast(CASE WHEN C.DeleteDateUTC IS NOT NULL OR RB.Active = 0 THEN 0 ELSE 1 END as bit) AS Active
	, isnull(ORD.OpenOrderCount, 0) as OpenOrderCount
	, isnull(R.ActualMiles, 0) AS ActualMiles
	, RB.CreateDateUTC, RB.CreatedByUser, RB.LastChangeDateUTC, RB.LastChangedByUser
	, RB.EndDate
	, RB.EarliestEffectiveDate
	, C.ID AS CarrierID, C.Name AS Carrier
	, R.OriginID, R.Origin, R.OriginFull
	, R.DestinationID , R.Destination, R.DestinationFull
	, CASE WHEN R.ID IS NULL THEN 'Route Missing' WHEN RB.ID IS NULL THEN 'Missing' WHEN C.Active = 0 THEN 'Carrier Deleted' WHEN RB.Active = 0 THEN 'Route Inactive' ELSE 'Active' END AS Status
	, cast(CASE WHEN R.ID IS NULL OR isnull(ORD.OpenOrderCount, 0) = 0 THEN 0 ELSE 1 END as bit) AS RouteInUse
FROM (viewCarrier C CROSS JOIN viewRouteWithMissing R)
LEFT JOIN viewCarrierRouteRatesBase RB ON RB.CarrierID = C.ID AND RB.OriginID = R.OriginID AND RB.DestinationID = R.DestinationID
LEFT JOIN (SELECT CarrierID, RouteID, count(1) AS OpenOrderCount, min(OrderDate) AS MinOpenOrderDate FROM viewOrder WHERE StatusID IN (4) GROUP BY CarrierID, RouteID) ORD ON ORD.CarrierID = C.ID AND ORD.RouteID = R.ID

GO

/***********************************/
-- Date Created: 9 Jun 2013
-- Author: Kevin Alons
-- Purpose: return Customer route rates with translated "friendly" values + "missing route rates", computed Earliest & End Date values)
/***********************************/
ALTER VIEW [dbo].[viewCustomerRouteRates] AS
SELECT RB.ID, R.ID AS RouteID, isnull(RB.Rate, 0) AS Rate, isnull(RB.EffectiveDate, dbo.fnDateOnly(ORD.MinOpenOrderDate)) AS EffectiveDate
	, cast(CASE WHEN RB.Active = 0 THEN 0 ELSE 1 END as bit) AS Active
	, isnull(ORD.OpenOrderCount, 0) as OpenOrderCount
	, isnull(R.ActualMiles, 0) AS ActualMiles
	, RB.CreateDateUTC, RB.CreatedByUser, RB.LastChangeDateUTC, RB.LastChangedByUser
	, RB.EndDate
	, RB.EarliestEffectiveDate
	, C.ID AS CustomerID, C.Name AS Customer
	, R.OriginID, R.Origin, R.OriginFull
	, R.DestinationID , R.Destination, R.DestinationFull
	, CASE WHEN R.ID IS NULL THEN 'Route Missing' WHEN RB.ID IS NULL THEN 'Missing' WHEN RB.Active = 0 THEN 'Route Inactive' ELSE 'Active' END AS Status
	, cast(CASE WHEN R.ID IS NULL OR isnull(ORD.OpenOrderCount, 0) = 0 THEN 0 ELSE 1 END as bit) AS RouteInUse
FROM (viewCustomer C CROSS JOIN viewRouteWithMissing R)
LEFT JOIN viewCustomerRouteRatesBase RB ON RB.CustomerID = C.ID AND RB.OriginID = R.OriginID AND RB.DestinationID = R.DestinationID
LEFT JOIN (SELECT CustomerID, RouteID, count(1) AS OpenOrderCount, min(OrderDate) AS MinOpenOrderDate FROM viewOrder WHERE StatusID IN (4) GROUP BY CustomerID, RouteID) ORD ON ORD.CustomerID = C.ID AND ORD.RouteID = R.ID

GO

/***********************************/
-- Date Created: 1 Jun 2013
-- Author: Kevin Alons
-- Purpose: add a new CarrierRouteRate
/***********************************/
ALTER PROCEDURE [dbo].[spCarrierRouteRate_Add]
(
  @CarrierID int
, @RouteID int
, @Rate smallmoney
, @EffectiveDate smalldatetime
, @ReplaceMatching bit
, @ReplaceAllOverlapping bit
, @UserName varchar(100)
, @Success bit = NULL out
, @Message varchar(255) = NULL out
) AS 
BEGIN
	DECLARE @removed int, @replaced int

	IF (@ReplaceMatching = 0)
	BEGIN
		IF ((SELECT count(1) FROM tblCarrierRouteRates WHERE CarrierID=@CarrierID AND RouteID=@RouteID AND EffectiveDate=@EffectiveDate) > 0)
		BEGIN
			SELECT @Success = 0, @Message = 'Skipped: Existing Route Rate already exists'
			RETURN
		END
	END
	ELSE
	BEGIN
		-- remove any matching/overlapping rate records
		IF (@ReplaceAllOverlapping = 1)
		BEGIN
			SELECT @removed = count(1) FROM tblCarrierRouteRates WHERE CarrierID=@CarrierID AND RouteID=@RouteID AND EffectiveDate>@EffectiveDate
			DELETE FROM tblCarrierRouteRates WHERE ID IN (
				SELECT ID FROM viewCarrierRouteRates WHERE CarrierID=@CarrierID AND RouteID=@RouteID AND EffectiveDate>@EffectiveDate)
		END

		SELECT @replaced = count(1) FROM tblCarrierRouteRates WHERE CarrierID=@CarrierID AND RouteID=@RouteID AND EffectiveDate=@EffectiveDate
		DELETE FROM tblCarrierRouteRates WHERE CarrierID=@CarrierID AND RouteID=@RouteID AND EffectiveDate=@EffectiveDate
	END
	
	-- do the actual insert now
	INSERT INTO tblCarrierRouteRates (CarrierID, RouteID, Rate, EffectiveDate, CreateDateUTC, CreatedByUser) 
		VALUES (@CarrierID, @RouteID, @Rate, @EffectiveDate, GETUTCDATE(), @UserName)
	SELECT @success = 1, @Message = 'Rate added successfully [' + ltrim(isnull(@replaced, 0)) + ' replaced, ' + ltrim(isnull(@removed, 0)) + ' deleted]'
END

GO

/***********************************/
-- Date Created: 1 Jun 2013
-- Author: Kevin Alons
-- Purpose: add a new CustomerRouteRate
/***********************************/
ALTER PROCEDURE [dbo].[spCustomerRouteRate_Add]
(
  @CustomerID int
, @RouteID int
, @Rate smallmoney
, @EffectiveDate smalldatetime
, @ReplaceMatching bit
, @ReplaceAllOverlapping bit
, @UserName varchar(100)
, @Success bit = NULL out
, @Message varchar(255) = NULL out
) AS 
BEGIN
	DECLARE @removed int, @replaced int

	IF (@ReplaceMatching = 0)
	BEGIN
		IF ((SELECT count(1) FROM tblCustomerRouteRates WHERE CustomerID=@CustomerID AND RouteID=@RouteID AND EffectiveDate=@EffectiveDate) > 0)
		BEGIN
			SELECT @Success = 0, @Message = 'Skipped: Existing Route Rate already exists'
			RETURN
		END
	END
	ELSE
	BEGIN
		-- remove any matching/overlapping rate records
		IF (@ReplaceAllOverlapping = 1)
		BEGIN
			SELECT @removed = count(1) FROM tblCustomerRouteRates WHERE CustomerID=@CustomerID AND RouteID=@RouteID AND EffectiveDate>@EffectiveDate
			DELETE FROM tblCustomerRouteRates WHERE ID IN (
				SELECT ID FROM viewCustomerRouteRates WHERE CustomerID=@CustomerID AND RouteID=@RouteID AND EffectiveDate>@EffectiveDate)
		END

		SELECT @replaced = count(1) FROM tblCustomerRouteRates WHERE CustomerID=@CustomerID AND RouteID=@RouteID AND EffectiveDate=@EffectiveDate
		DELETE FROM tblCustomerRouteRates WHERE CustomerID=@CustomerID AND RouteID=@RouteID AND EffectiveDate=@EffectiveDate
	END
	
	-- do the actual insert now
	INSERT INTO tblCustomerRouteRates (CustomerID, RouteID, Rate, EffectiveDate, CreateDateUTC, CreatedByUser) 
		VALUES (@CustomerID, @RouteID, @Rate, @EffectiveDate, GETUTCDATE(), @UserName)
	SELECT @success = 1, @Message = 'Rate added successfully [' + ltrim(isnull(@replaced, 0)) + ' replaced, ' + ltrim(isnull(@removed, 0)) + ' deleted]'
END

GO

COMMIT
SET NOEXEC OFF