-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.9.7.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.9.7.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0

SET NOEXEC OFF
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.9.7'
SELECT  @NewVersion = '3.9.8'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Report Center Order Approval FINALXXX column error fixes'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

UPDATE tblReportColumnDefinition SET DataField = 'SELECT isnull(OA.OverrideOriginMinutes, RS.OriginMinutes) FROM tblOrderApproval OA WHERE OA.OrderID = RS.ID' WHERE ID = 90011
UPDATE tblReportColumnDefinition SET DataField = 'SELECT isnull(OA.OverrideDestMinutes, RS.DestMinutes) FROM tblOrderApproval OA WHERE OA.OrderID = RS.ID' WHERE ID = 90012
UPDATE tblReportColumnDefinition SET DataField = 'SELECT coalesce(OA.OverrideOriginMinutes, RS.OriginMinutes, 0) + coalesce(OA.OverrideDestMinutes, RS.DestMinutes, 0) FROM tblOrderApproval OA WHERE OA.OrderID = RS.ID' WHERE ID = 90013
UPDATE tblReportColumnDefinition SET DataField = 'SELECT isnull(OA.OverrideDestMinutes, RS.DestMinutes) FROM tblOrderApproval OA WHERE OA.OrderID = RS.ID' WHERE ID = 90017

COMMIT
SET NOEXEC OFF