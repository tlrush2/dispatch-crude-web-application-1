/* 
	change tblCarrier.ACHInstructions field -> ACHInstructionsDocument/ACHInstructionsDocName
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.5.3'
SELECT  @NewVersion = '2.5.4'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

ALTER TABLE tblCarrier ADD ACHInstructionsDocument varbinary(max) NULL
GO
ALTER TABLE tblCarrier ADD ACHInstructionsDocName varchar(255) NULL
GO
ALTER TABLE tblCarrier DROP COLUMN ACHInstructions
GO

EXEC _spRefreshAllViews
GO

COMMIT
SET NOEXEC OFF