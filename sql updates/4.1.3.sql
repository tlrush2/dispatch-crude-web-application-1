SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.1.2'
SELECT  @NewVersion = '4.1.3'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1668 Capture driver local time'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/**********************************************************/
-- Creation Info: 3.13.1 - 2016/07/04
-- Author: Kevin Alons
-- Purpose: return the DriverLocation_OriginFirstArrive info for a single OrderID
-- Changes:
--		4.1.3		JAE		2016/09/12		Corrected ID to use OrderID
/**********************************************************/
ALTER FUNCTION [dbo].[fnDriverLocation_OriginFirstArrive](@OrderID int, @OriginID int) RETURNS TABLE AS RETURN
	SELECT * FROM viewDriverLocation_OriginFirstArrive WHERE OrderID = @OrderID AND (nullif(@OriginID, 0) IS NULL OR OriginID = @OriginID)

GO

/**********************************************************/
-- Creation Info: 3.13.1 - 2016/07/04
-- Author: Kevin Alons
-- Purpose: return the DriverLocation_DestinationFirstArrive info for a single OrderID
-- Changes:
--		4.1.3		JAE		2016/09/12		Corrected ID to use OrderID
/**********************************************************/
ALTER FUNCTION [dbo].[fnDriverLocation_DestinationFirstArrive](@OrderID int, @DestinationID int) RETURNS TABLE AS RETURN
	SELECT * FROM viewDriverLocation_DestinationFirstArrive WHERE OrderID = @OrderID AND (nullif(@DestinationID, 0) IS NULL OR DestinationID = @DestinationID)

GO


EXEC _spRebuildAllObjects
GO


COMMIT
SET NOEXEC OFF