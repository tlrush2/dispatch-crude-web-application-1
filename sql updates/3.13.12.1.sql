-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.13.12'
SELECT  @NewVersion = '3.13.12.1'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1638: Create permission/role for import center wizard'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/* Add permission to the import center wizart page */
INSERT INTO aspnet_Roles (ApplicationId, RoleId, RoleName, LoweredRoleName, Description)
SELECT '446467D6-39CD-45E3-B3E0-CAC7945AF3E8', NEWID(), 'viewImportCenter', 'viewimportcenter', 'Allow user to view Import Center page'
EXCEPT SELECT ApplicationId, RoleId, RoleName, LoweredRoleName, Description
FROM aspnet_Roles

COMMIT 
SET NOEXEC OFF