-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.12.3'
SELECT  @NewVersion = '3.12.3.1'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Add new manageCarrierRules role'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


-- Add new role
INSERT INTO ASPNET_ROLES
select applicationid, NEWID(), 'manageCarrierRules', LOWER('manageCarrierRules'), 'Create and edit carrier rules' from aspnet_applications 
where not exists (select 1 from aspnet_roles where rolename = 'manageCarrierRules')
GO

-- Add current admins to role
EXEC spAddUserPermissionsByRole 'Administrator', 'manageCarrierRules'
GO


COMMIT
SET NOEXEC OFF