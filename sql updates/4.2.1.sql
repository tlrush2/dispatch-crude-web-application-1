SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.2.0'
SELECT  @NewVersion = '4.2.1'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1870 - Weight fields in Report Center'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


-- Fix order single export and dest uom if needed to older weight fields
UPDATE tblReportColumnDefinition
SET OrderSingleExport = 1,
	DataField = 'dbo.fnConvertUOM(RS.DestWeightTareUnits,RS.DestUomID,5)'
WHERE ID = 90048

UPDATE tblReportColumnDefinition
SET OrderSingleExport = 1,
	DataField = 'dbo.fnConvertUOM(RS.DestWeightGrossUnits,RS.DestUomID,5)'
WHERE ID = 90049

UPDATE tblReportColumnDefinition
SET OrderSingleExport = 1,
	DataField = 'dbo.fnConvertUOM(RS.DestWeightNetUnits,RS.DestUomID,5)'
WHERE ID = 90050

UPDATE tblReportColumnDefinition
SET OrderSingleExport = 1,
	DataField = 'dbo.fnConvertUOM(RS.DestWeightGrossUnits,RS.DestUomID,6)',
	Caption = 'DESTINATION | WEIGHTS | ST | Destination Gross Weight (ST)'
WHERE ID = 90051

UPDATE tblReportColumnDefinition
SET OrderSingleExport = 1,
	DataField = 'dbo.fnConvertUOM(RS.DestWeightNetUnits,RS.DestUomID,6)',
	Caption = 'DESTINATION | WEIGHTS | ST | Destination Net Weight (ST)'
WHERE ID = 90052

GO


SET IDENTITY_INSERT tblReportColumnDefinition ON

  -- Add new Report Center fields for metric tons
  INSERT tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport)
	SELECT 90056,1,'dbo.fnConvertUOM(RS.T_WeightGrossUnits,RS.OriginUomID,7)','TICKET | WEIGHTS | Tonnes | Ticket Gross Weight (Tonnes)','#,##0.000',NULL,4,NULL,1,'*',0
	UNION
	SELECT 90057,1,'dbo.fnConvertUOM(RS.T_WeightTareUnits,RS.OriginUomID,7)','TICKET | WEIGHTS | Tonnes | Ticket Tare Weight (Tonnes)','#,##0.000',NULL,4,NULL,1,'*',0	
	UNION
	SELECT 90058,1,'dbo.fnConvertUOM(RS.T_WeightNetUnits,RS.OriginUomID,7)','TICKET | WEIGHTS | Tonnes | Ticket Net Weight (Tonnes)','#,##0.000',NULL,4,NULL,1,'*',0	
	UNION
	SELECT 90059,1,'dbo.fnConvertUOM(RS.DestWeightGrossUnits,RS.DestUomID,7)','DESTINATION | WEIGHTS | Tonnes | Destination Gross Weight (Tonnes)','#,##0.000',NULL,4,NULL,1,'*',1
	UNION
	SELECT 90060,1,'dbo.fnConvertUOM(RS.DestWeightTareUnits,RS.DestUomID,7)','DESTINATION | WEIGHTS | Tonnes | Destination Tare Weight (Tonnes)','#,##0.000',NULL,4,NULL,1,'*',1
	UNION
	SELECT 90061,1,'dbo.fnConvertUOM(RS.DestWeightNetUnits,RS.DestUomID,7)','DESTINATION | WEIGHTS | Tonnes | Destination Net Weight (Tonnes)','#,##0.000',NULL,4,NULL,1,'*',1
  EXCEPT SELECT ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport FROM tblReportColumnDefinition

  -- Add Report Center fields for litres
  INSERT tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport)
	SELECT 90062,1,'dbo.fnConvertUOM(RS.T_GrossUnits,RS.OriginUomID,4)','TICKET | VOLUMES | LTR | Ticket GOV','#,##0.0',NULL,4,NULL,1,'*',0
	UNION
	SELECT 90063,1,'dbo.fnConvertUOM(RS.T_GrossStdUnits,RS.OriginUomID,4)','TICKET | VOLUMES | LTR | Ticket GSV','#,##0.0',NULL,4,NULL,1,'*',0	
	UNION
	SELECT 90064,1,'dbo.fnConvertUOM(RS.T_NetUnits,RS.OriginUomID,4)','TICKET | VOLUMES | LTR | Ticket NSV','#,##0.0',NULL,4,NULL,1,'*',0	
	UNION
	SELECT 90065,1,'dbo.fnConvertUOM(RS.OriginGrossUnits,RS.OriginUomID,4)','ORIGIN | VOLUMES | LTR | Origin GOV','#,##0.0',NULL,4,NULL,1,'*',1
	UNION
	SELECT 90066,1,'dbo.fnConvertUOM(RS.OriginGrossStdUnits,RS.OriginUomID,4)','ORIGIN | VOLUMES | LTR | Origin GSV','#,##0.0',NULL,4,NULL,1,'*',1
	UNION
	SELECT 90067,1,'dbo.fnConvertUOM(RS.OriginNetUnits,RS.OriginUomID,4)','ORIGIN | VOLUMES | LTR | Origin NSV','#,##0.0',NULL,4,NULL,1,'*',1
	UNION
	SELECT 90068,1,'dbo.fnConvertUOM(RS.DestGrossUnits,RS.DestUomID,4)','DESTINATION | VOLUMES | LTR | Destination GOV','#,##0.0',NULL,4,NULL,1,'*',1
	UNION
	SELECT 90069,1,'dbo.fnConvertUOM(RS.DestGrossStdUnits,RS.DestUomID,4)','DESTINATION | VOLUMES | LTR | Destination GSV','#,##0.0',NULL,4,NULL,1,'*',1
	UNION
	SELECT 90070,1,'dbo.fnConvertUOM(RS.DestNetUnits,RS.DestUomID,4)','DESTINATION | VOLUMES | LTR | Destination NSV','#,##0.0',NULL,4,NULL,1,'*',1
  EXCEPT SELECT ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport FROM tblReportColumnDefinition

SET IDENTITY_INSERT tblReportColumnDefinition OFF

GO


COMMIT
SET NOEXEC OFF