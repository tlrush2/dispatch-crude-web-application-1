SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.5.9'
SELECT  @NewVersion = '4.5.10'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'JT-2171 - Normalize Drivers License and fields to compliance'
	UNION
	SELECT @NewVersion, 0, 'JT-2171 - Integrate H2S check with dispatch'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


-- Make Commercial Drivers License a system type
UPDATE tblDriverComplianceType SET IsSystem = 1 WHERE ID = 2 /* CDL */
GO

-- Add a certificate category to the driver's compliance
SET IDENTITY_INSERT tblDriverComplianceCategory ON
INSERT INTO tblDriverComplianceCategory (ID, Name)
VALUES (4, 'Certificates')
SET IDENTITY_INSERT tblDriverComplianceCategory OFF
GO

-- Make H2S a certificate (not a license)
UPDATE tblDriverComplianceType SET CategoryID = 4 WHERE ID = 12 /* H2S */
GO


-- Add new license columns to the driver compliance table
ALTER TABLE tblDriverCompliance ADD DocumentStateID INT NULL CONSTRAINT FK_DriverCompliance_DocumentState REFERENCES tblState(ID)
GO

ALTER TABLE tblDriverCompliance ADD DocumentNumber VARCHAR(20) NULL
GO


ALTER TABLE tblDriverCompliance ADD Endorsements VARCHAR(255) NULL
ALTER TABLE tblDriverCompliance ADD Restrictions VARCHAR(255) NULL
GO


-- ----------------------------------------------
-- Remove DL from options
-- ----------------------------------------------
-- Move any drivers license to CDLs (should be none)
UPDATE tblDriverCompliance
SET ComplianceTypeID = 2
WHERE ComplianceTypeID = 1

-- Remove DL
DELETE FROM tblDriverComplianceType WHERE ID = 1

GO

-- ----------------------------------------------
-- Copy over existing CDL numbers
-- ----------------------------------------------

-- Add the CDL #/restrictions/endorsements to the compliance record
UPDATE tblDriverCompliance 
SET DocumentNumber = CDLNumber
	, Endorsements = d.Endorsements
	, Restrictions = d.Restrictions
FROM tblDriverCompliance dc 
LEFT JOIN tblDriver d ON d.ID = dc.DriverID
WHERE ComplianceTypeID = 2 /* CDL */ 
  AND (CDLNumber IS NOT NULL OR d.Endorsements IS NOT NULL OR d.Restrictions IS NOT NULL)

-- Create new CDL compliance records for those that don't have one
INSERT INTO tblDriverCompliance (DriverID, ComplianceTypeID, DocumentNumber, Endorsements, Restrictions)
SELECT ID
	, 2 /* CDL */
	, CDLNumber 
	, Endorsements
	, Restrictions
FROM tblDriver d 
WHERE (CDLNumber IS NOT NULL OR Endorsements IS NOT NULL OR Restrictions IS NOT NULL)
AND NOT EXISTS (SELECT 1 FROM tblDriverCompliance WHERE ComplianceTypeID = 2 /* CDL */ AND DriverID = d.ID)

GO

-- Add the H2S Expiration to the compliance record (if expiration date not already set)
UPDATE tblDriverCompliance 
SET ExpirationDate = d.H2SExpiration
FROM tblDriverCompliance dc 
LEFT JOIN tblDriver d ON d.ID = dc.DriverID
WHERE ComplianceTypeID = 12 /* H2S */ 
  AND H2SExpiration IS NOT NULL 
  AND dc.ExpirationDate IS NULL

-- Create new H2s compliance records for those that don't have one
INSERT INTO tblDriverCompliance (DriverID, ComplianceTypeID, ExpirationDate)
SELECT ID
	, 12 /* H2S */
	, H2SExpiration
FROM tblDriver d 
WHERE H2SExpiration IS NOT NULL
AND NOT EXISTS (SELECT 1 FROM tblDriverCompliance WHERE ComplianceTypeID = 12 /* H2S */ AND DriverID = d.ID)

GO


-- ----------------------------------------------
-- Drop legacy columns
-- ----------------------------------------------
ALTER TABLE tblDriver DROP COLUMN DLNumber
ALTER TABLE tblDriver DROP COLUMN CDLNumber
ALTER TABLE tblDriver DROP COLUMN H2SExpiration

ALTER TABLE tblDriver DROP COLUMN Endorsements
ALTER TABLE tblDriver DROP COLUMN Restrictions

GO

-- Remove from Import Center

-- Driver.DLExpiration
DELETE FROM tblImportCenterFieldDefinition WHERE ObjectFieldID = 47
DELETE FROM tblObjectField WHERE ID = 47 

-- Driver.DLNumber
DELETE FROM tblImportCenterFieldDefinition WHERE ObjectFieldID = 48
DELETE FROM tblObjectField WHERE ID = 48 

-- Driver.Endorsements
DELETE FROM tblImportCenterFieldDefinition WHERE ObjectFieldID = 51
DELETE FROM tblObjectField WHERE ID = 51 

-- Driver.LongFormPhysicalDate
DELETE FROM tblImportCenterFieldDefinition WHERE ObjectFieldID = 56
DELETE FROM tblObjectField WHERE ID = 56 

-- Driver.MedicalCardDate
DELETE FROM tblImportCenterFieldDefinition WHERE ObjectFieldID = 57
DELETE FROM tblObjectField WHERE ID = 57 

-- Driver.Restrictions
DELETE FROM tblImportCenterFieldDefinition WHERE ObjectFieldID = 62
DELETE FROM tblObjectField WHERE ID = 62 

-- Driver.H2SExpiration
DELETE FROM tblImportCenterFieldDefinition WHERE ObjectFieldID = 426
DELETE FROM tblObjectField WHERE ID = 426 

GO


EXEC sp_refreshview viewDriverBase
GO

/***********************************/
-- Date Created: 26 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Drivers table records with LastDelivereredOrderTime + computed assigned ASP.NET usernames
-- Changes
--	4.4.14	- JAE & BB	- 2016-11-29 - Add previously included fields, Moved UserNames to viewDriverBase
--	4.4.19	- BB		- 2017-01-18 - Added Terminal
--	4.4.20	- BSB		- 2017-01-19 - Moved LastDeliveredOrderTime from here to viewDriverBase so it can be displayed on the driver maintenance page
--	4.5.0	- BSB		- 2017-01-26 - Removed Terminal and put it in viewDriverBase due to access need in functions
--	4.5.10	- JAE		- 2017-02-17 - Removed compliance records by default.  Adding the two system types (DL and H2S for coding)
/***********************************/
ALTER VIEW viewDriver AS
SELECT DB.*	
	, DLStateID = dl.DocumentStateID
	, DLState = dls.Abbreviation
	, DLNumber = dl.DocumentNumber
	, DLEndorsements = dl.Endorsements
	, DLRestrictions = dl.Restrictions
	, DLID = dl.ID
	, H2SExpiration = h2s.ExpirationDate
	, H2S = CAST(CASE WHEN h2s.ExpirationDate IS NOT NULL AND h2s.ExpirationDate >= CAST(GETDATE() AS DATE) THEN 1 ELSE 0 END AS BIT)
	, H2SID = h2s.ID

FROM viewDriverBase DB	
	OUTER APPLY (select TOP 1 * FROM tblDriverCompliance WHERE ComplianceTypeID=2 AND DriverID = DB.ID ORDER BY DocumentDate DESC) dl 
	OUTER APPLY (select TOP 1 * FROM tblDriverCompliance WHERE ComplianceTypeID=12 AND DriverID = DB.ID ORDER BY DocumentDate DESC) h2s
	LEFT JOIN tblState dls ON dl.DocumentStateID = dls.ID

GO


INSERT INTO tblCarrierRuleType
	VALUES (26, 'Enforce H2S', 2, 0, 'Flag to check H2S Certification when dispatching orders')

GO


/****************************************************/
-- Created: 2016.09.14 - 4.1.7
-- Author: Joe Engler
-- Purpose: Retrieve the drivers (without last known GPS coordinates) for the specified filter criteria
-- Changes:
--	4.1.8.3		JAE		2016-09-21		Fixed reference to current time to start time (passed in)
--	4.1.10.1	JAE		2016-09-27		Added qualifier to DeleteDateUTC column (since same column was added to route table)
--	4.2.0		JAE		2016-09-30		Add hos fields to view
--	4.2.5		JAE		2016-10-26		Use operating state (instead of state) to carrier rule call
--											Added Weekly OnDuty fields
--	4.3.2		KDA		2016.11.07		replace tblDriverAvailability references with viewDriverSchedule (tblDriverSchedule)
--	4.4.14		JAE		2016-12-22		Update compliance check to use new compliance tables			
--  4.4.22.1	JAE		2017-01-31		Join HOS policy to get default times when no history occurs, update hours left based on date
--	4.5.0		BSB		2017-01-25		Add Terminal
--  4.5.10		JAE		2017-02-20		Added H2S check to compliance
/****************************************************/
ALTER FUNCTION fnRetrieveEligibleDrivers_NoGPS(@CarrierID INT, @TerminalID INT, @StateID INT, @RegionID INT, @DriverGroupID INT, @StartDate DATETIME, @H2S BIT = 0) 
RETURNS @ret TABLE 
(
	ID INT
	, LastName VARCHAR(20)
	, FirstName VARCHAR(20)
	, FullName VARCHAR(41)
	, FullNameLF VARCHAR(42)
	, CarrierID INT
	, Carrier VARCHAR(40)
	, TerminalID INT
	, RegionID INT
	, TruckID INT
	, Truck VARCHAR(10)
	, TrailerID INT
	, Trailer VARCHAR(10)
	, AvailabilityFactor DECIMAL(4,2)
	, ComplianceFactor DECIMAL(4,2)
	, H2SFactor DECIMAL(4,2)
	, TruckAvailabilityFactor DECIMAL(4,2)
	, CurrentWorkload INT
	, CurrentECOT INT
	, HOSFactor DECIMAL(4,2)
	, OnDutyHoursSinceWeeklyReset DECIMAL(6,2)
	, WeeklyOnDutyHoursLeft DECIMAL(6,2)
	, DrivingHoursSinceWeeklyReset DECIMAL(6,2)
	, WeeklyDrivingHoursLeft DECIMAL(6,2)
	, OnDutyHoursSinceDailyReset DECIMAL(6,2)
	, OnDutyHoursLeft DECIMAL(6,2)
	, DrivingHoursSinceDailyReset DECIMAL(6,2)
	, DrivingHoursLeft DECIMAL(6,2)
	, HOSHrsOnShift DECIMAL(6,2)
	, DriverScore INT 
)
AS BEGIN
	DECLARE @__HOS_POLICY__ INT = 1
	DECLARE @__ENFORCE_DRIVER_SCHEDULING__ INT = 13
	DECLARE @__ENFORCE_DRIVER_COMPLIANCE__ INT = 14
	DECLARE @__ENFORCE_HOS__ INT = 15
	DECLARE @__ENFORCE_H2S__ INT = 26
	
	IF @StartDate IS NULL 
	   SELECT @StartDate = dbo.fnDateOnly(GETDATE())

	INSERT INTO @ret
	SELECT X.*
		, DriverScore = 100 * AvailabilityFactor * ComplianceFactor * TruckAvailabilityFactor * HOSFactor * H2SFactor
	FROM (
		SELECT d.ID
			, d.FirstName
			, d.LastName
			, FullName
			, d.FullNameLF
			, d.CarrierID
			, d.Carrier
			, d.TerminalID
			, d.RegionID
			, d.TruckID
			, d.Truck
			, d.TrailerID
			, d.Trailer
			--Availability
			, AvailabilityFactor = CASE WHEN cr_a.Value IS NULL OR dbo.fnToBool(cr_a.Value) = 0 THEN 1 -- scheduling not enforced
										ELSE isnull(DS.OnDuty, 0) END
			-- Compliance
			, ComplianceFactor = CASE WHEN cr_c.Value IS NULL OR dbo.fnToBool(cr_c.Value) = 0 THEN 1 -- compliance not enforced
									WHEN (select count(*) from fnDriverComplianceSummary(0, 0) WHERE DriverID = d.ID) > 0 THEN 0
									ELSE 1 END -- all ok
			--H2S
			, H2SFactor = CASE WHEN cr_h2s.Value IS NULL OR dbo.fnToBool(cr_h2s.Value) = 0 THEN 1 -- H2S not enforced
									WHEN @H2S = 1 AND (H2SExpiration IS NULL OR H2SExpiration < CAST(@StartDate AS DATE)) THEN 0 -- H2S required but not certified/expired
									ELSE 1 END -- all ok
			
			--Truck/Trailer Availability
			, TruckAvailabilityFactor = 1			
			--current workload, time on shift
			, o.CurrentWorkload
			, CurrentECOT = ISNULL(o.CurrentECOT, 0)
			-- HOS
			, HOSFactor = CASE WHEN cr_h.Value IS NULL OR dbo.fnToBool(cr_h.Value) = 0 THEN 1 -- HOS not enforced
							WHEN @StartDate > DATEADD(HOUR, policy.WeeklyResetHR, GETUTCDATE()) THEN 1 -- Equivalent of next "week"
							WHEN @StartDate > GETUTCDATE() AND hos.WeeklyOnDutyHoursLeft <= 0 THEN 0 -- check tomorrow (out of weekly hours)
							WHEN @StartDate > GETUTCDATE() AND hos.WeeklyOnDutyHoursLeft > 0 THEN 1 -- check tomorrow (ignore daily numbers)
							WHEN hos.WeeklyOnDutyHoursLeft <= 0 OR hos.OnDutyHoursLeft <= 0 OR hos.DrivingHoursLeft <= 0 THEN 0 -- check today
							-- compare ECOT to hours and see
							ELSE 1 END
			, hos.OnDutyHoursSinceWeeklyReset
			, WeeklyOnDutyHoursLeft = CASE WHEN @StartDate > DATEADD(HOUR, policy.WeeklyResetHR, GETUTCDATE()) THEN policy.OnDutyWeeklyLimitHR ELSE ISNULL(hos.WeeklyOnDutyHoursLeft, policy.OnDutyWeeklyLimitHR) END
			, hos.DrivingHoursSinceWeeklyReset
			, WeeklyDrivingHoursLeft = CASE WHEN @StartDate > GETUTCDATE() THEN policy.DrivingWeeklyLimitHR ELSE ISNULL(hos.WeeklyDrivingHoursLeft, policy.DrivingWeeklyLimitHR) END
			, hos.OnDutyHoursSinceDailyReset
			, OnDutyHoursLeft = CASE WHEN @StartDate > GETUTCDATE() THEN policy.OnDutyDailyLimitHR ELSE hos.OnDutyHoursLeft END
			, hos.DrivingHoursSinceDailyReset
			, DrivingHoursLeft = CASE WHEN @StartDate > GETUTCDATE() THEN policy.DrivingDailyLimitHR ELSE hos.DrivingHoursLeft END
			, HrsOnShift = CAST(DATEDIFF(MINUTE, ds.StartDateTime, getdate())/60.0*isnull(ds.OnDuty, 0) AS DECIMAL(5,1)) -- from DriverScheduling
			/* return the number of hours the Driver Scheduling module defines the driver is Scheduled on the specified day */
			--,ShiftHours = dbo.fnMinInt(DS.DurationHours, 24 - DS.StartHours % 24)-- from DriverScheduling

		FROM viewDriver d
			LEFT JOIN (SELECT * FROM viewDriverSchedule WHERE OnDuty = 1) ds ON ds.DriverID = d.ID AND ScheduleDate = CAST(@StartDate AS DATE)
			OUTER APPLY dbo.fnCarrierRules(@StartDate, null, @__ENFORCE_DRIVER_SCHEDULING__, d.ID, d.CarrierID, d.TerminalID, d.OperatingStateID, d.RegionID, 1) cr_a -- Enforce Driver Scheduling Carrier Rule
			OUTER APPLY dbo.fnCarrierRules(@StartDate, null, @__ENFORCE_DRIVER_COMPLIANCE__, d.ID, d.CarrierID, d.TerminalID, d.OperatingStateID, d.RegionID, 1) cr_c
			OUTER APPLY dbo.fnCarrierRules(@StartDate, null, @__ENFORCE_H2S__, d.ID, d.CarrierID, d.TerminalID, d.OperatingStateID, d.RegionID, 1) cr_h2s
			OUTER APPLY dbo.fnCarrierRules(@StartDate, null, @__ENFORCE_HOS__, d.ID, d.CarrierID, d.TerminalID, d.OperatingStateID, d.RegionID, 1) cr_h
			OUTER APPLY (SELECT TOP 1 * FROM dbo.fnHosViolationDetail(d.ID, null, @StartDate) ORDER BY LogDateUTC DESC) hos
			OUTER APPLY (SELECT TOP 1 * FROM viewHosPolicy WHERE ID = ISNULL((SELECT TOP 1 p.ID FROM tblHosPolicy p WHERE Name = 
																					(SELECT Value FROM dbo.fnCarrierRules(@StartDate, null, @__HOS_POLICY__ , d.ID, d.CarrierID, d.TerminalID, d.OperatingStateID, d.RegionID, 1)))
																	, 1)) policy
			OUTER APPLY (
						SELECT CurrentWorkload = COUNT(*) 
							, CurrentECOT = SUM(CASE WHEN o.StatusID = 8 THEN ISNULL(r.ECOT, 60+ISNULL(r.ActualMiles,60)+60)/2
													   ELSE COALESCE(r.ECOT, 60+ISNULL(r.ActualMiles,60)+60) END)
						FROM tblOrder o LEFT JOIN tblRoute r ON o.RouteID = r.ID
						WHERE o.DriverID = d.ID
						  AND o.StatusID IN (2,7,8) -- Dispatched, Accepted, Picked Up
						  AND o.DeleteDateUTC IS NULL) o

		WHERE d.DeleteDateUTC IS NULL
			AND d.TruckID IS NOT NULL AND d.TrailerID IS NOT NULL		
			AND (ISNULL(@CarrierID, 0) <= 0 OR @CarrierID = d.CarrierID)
			AND (ISNULL(@TerminalID, 0) <= 0 OR @TerminalID = d.TerminalID OR d.TerminalID IS NULL)
			AND (ISNULL(@RegionID, -1) <= 0 OR @RegionID = d.RegionID)
			AND (ISNULL(@StateID, 0) <= 0 OR @StateID = d.StateID)
			AND (ISNULL(@DriverGroupID, 0) <= 0 OR @DriverGroupID = d.ID)
	) AS X
	ORDER BY DriverScore DESC, LastName, FirstName
	RETURN
END
GO


/****************************************************/
-- Created: 2016.09.14 - 4.1.7 - Joe Engler
-- Purpose: Retrieve the drivers (with last known GPS coordinates) for the specified filter criteria.  
--				HoursBack is number of hours before a location is considered "stale" and excluded from this query
-- Changes:
--	4.5.0	- 2017/01/25	- BSB	- Add terminal
--  4.5.10	- 2017-02-20	- JAE	- Added H2S check to compliance
/****************************************************/
ALTER FUNCTION fnRetrieveEligibleDrivers(@CarrierID INT, @TerminalID INT, @StateID INT, @RegionID INT, @DriverGroupID INT, @StartDate DATETIME, @H2S BIT = 0, @HoursBack INT = 0) 
RETURNS TABLE AS
RETURN
	SELECT d.*, q.Lat, q.Lon, DriverLocationDateUTC = q.CreateDateUTC, DriverLocationID = q.ID
	FROM dbo.fnRetrieveEligibleDrivers_NoGPS(@CarrierID, @TerminalID, @StateID, @RegionID, @DriverGroupID, @StartDate, @H2S) d
	LEFT JOIN fnRetrieveDriverLastLocation(NULL) q ON q.DriverID = d.ID 
		AND (@HoursBack = 0 OR CreateDateUTC > DATEADD(HOUR, -@HoursBack, GETUTCDATE())) -- exclude "stale" locations 
GO


-- ----------------------------------------------------------------
-- Refresh all
-- ----------------------------------------------------------------
EXEC _spRefreshAllViews
GO
EXEC _spRebuildAllObjects
GO
EXEC _spRecompileAllStoredProcedures
GO


COMMIT
SET NOEXEC OFF