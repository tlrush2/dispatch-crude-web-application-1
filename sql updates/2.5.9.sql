/* 
	add viewOrderDisplay for use by OrderDisplay model (for optimized display querying)
	add spMVCOrders stored procedure to return the data for the 
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.5.8'
SELECT  @NewVersion = '2.5.9'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

/***********************************/
-- Date Created: 28 Jan 2014
-- Author: Kevin Alons
-- Purpose: return Order records with necessary JOINed fields for OrderList in MVC pages
/***********************************/
CREATE VIEW viewOrderDisplay AS 
	SELECT O.*
		, isnull(D.MobileApp, 0) AS MobileApp
		, OS.EntrySortNum
		, P.PriorityNum
		, C.Name AS CarrierName
	FROM tblOrder O
	LEFT JOIN tblDriver D ON D.ID = O.DriverID
	JOIN tblOrderStatus OS ON OS.ID = O.StatusID
	JOIN tblPriority P ON P.ID = O.PriorityID
	JOIN tblCarrier C ON C.ID = O.CarrierID

GO
GRANT SELECT ON viewOrderDisplay to dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 28 Jan 2014
-- Author: Kevin Alons
-- Purpose: return Order records with necessary JOINed fields for OrderList in MVC pages
/***********************************/
CREATE PROCEDURE spMVCOrders(@driverID int, @carrierID int) AS BEGIN
	SELECT O.* 
	FROM viewOrderDisplay O
	JOIN viewOrder vO ON vO.ID = O.ID
	JOIN (SELECT ID, Value FROM tblSetting) S ON S.ID = 10
	WHERE O.DeleteDateUTC IS NULL 
	  AND (O.DriverID = @driverID
			OR O.CarrierID = @carrierID
			OR (O.CarrierID IS NOT NULL AND @carrierID = -1))
	  AND (vO.PrintStatusID IN (2,7,8)
			OR (O.StatusID IN (3) AND O.DestDepartTimeUTC > DATEADD(HOUR, -cast(S.Value as int), GETUTCDATE())))

END

GO
GRANT EXECUTE ON spMVCOrders to dispatchcrude_iis_acct
GO

COMMIT
SET NOEXEC OFF