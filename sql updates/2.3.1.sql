/* 
  revamp order exports to simplify and use rowspan cell merging instead of all multiple line cell values
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.3.0'
SELECT  @NewVersion = '2.3.1'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

/***********************************************************/
-- Date Created: 3 Nov 2013
-- Author: Kevin Alons
-- Purpose: add "friendly" names to the DriverAvailability table
/***********************************************************/
ALTER VIEW [dbo].[viewDriverAvailability] AS
	SELECT DA.*
		, dbo.fnDateOnly(AvailDateTime) AS AvailDate
		, D.FullName AS Driver
		, C.Name AS Carrier
	FROM tblDriverAvailability DA
	JOIN viewDriver D ON D.ID = DA.DriverID
	JOIN tblCarrier C ON C.ID = D.CarrierID



GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return concatenated OrderTicket details for a single order
/***********************************/
CREATE FUNCTION [dbo].[fnOrderTicketDetails](@OrderID int, @fieldName varchar(50), @delimiter varchar(10)) RETURNS varchar(max) AS BEGIN
	DECLARE @ret varchar(max)
	
	-- Retrieve the OrderTicket Details that are specified by @OrderID | fieldName
	DECLARE @ID int
	SELECT @ID = min(ID) FROM dbo.tblOrderTicket WHERE OrderID = @OrderID AND ID > 0
	WHILE @ID IS NOT NULL
	BEGIN
		SELECT @ret = isnull(@ret + @delimiter, '') 
			+ CASE WHEN @fieldName LIKE 'TicketNums' THEN OT.CarrierTicketNum
					WHEN @fieldName LIKE 'TankNums' THEN OT.TankNum END
		FROM tblOrderTicket OT
		WHERE OT.ID = @ID
		SELECT @ID = min(ID) FROM tblOrderTicket WHERE OrderID = @OrderID AND ID > @ID
	END

	RETURN (@ret)
END


GO

/***********************************/
-- Date Created: 28 Feb 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
/***********************************/
ALTER VIEW [dbo].[viewOrderExportFull] AS
SELECT *
  , OriginMinutes + DestMinutes AS TotalMinutes
  , isnull(OriginWaitMinutes, 0) + isnull(DestWaitMinutes, 0) AS TotalWaitMinutes
FROM (
	SELECT O.*
	  , dbo.fnMaxInt(0, isnull(OriginMinutes, 0) - cast(S.Value as int)) AS OriginWaitMinutes
	  , dbo.fnMaxInt(0, isnull(DestMinutes, 0) - cast(S.Value as int)) AS DestWaitMinutes
	  , (SELECT count(*) FROM tblOrderTicket WHERE OrderID = O.ID AND DeleteDateUTC IS NULL) AS TicketCount
	  , (SELECT count(*) FROM tblOrderReroute WHERE OrderID = O.ID) AS RerouteCount
	FROM dbo.viewOrderLocalDates O
	JOIN dbo.tblSetting S ON S.ID = 7 -- the Unbillable Wait Time threshold minutes 
WHERE O.StatusID NOT IN (-10) -- don't include "Generated" orders (only Assigned+)
  AND O.DeleteDateUTC IS NULL
) v



GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return the specified Order Reroute details concatenated into a single field for a single provided OrderID
/***********************************/
CREATE FUNCTION fnRerouteDetails(@OrderID int, @fieldName varchar(50), @delimiter varchar(10)) RETURNS varchar(max) AS BEGIN
	DECLARE @ret varchar(max)
	
	-- Retrieve the Reroute Details that are specified by @OrderID | fieldName
	DECLARE @RerouteID int
	SELECT @RerouteID = min(ID) FROM dbo.tblOrderReroute WHERE OrderID = @OrderID AND ID > 0
	WHILE @RerouteID IS NOT NULL
	BEGIN
		SELECT @ret = isnull(@ret + @delimiter, '') 
			+ CASE WHEN @fieldName LIKE 'PreviousDestinations' THEN O_R.PreviousDestinationFull
					WHEN @fieldName LIKE 'RerouteUsers' THEN O_R.UserName
					WHEN @fieldName LIKE 'RerouteDates' THEN dbo.fnDateMdYY(O_R.RerouteDate)
					WHEN @fieldName LIKE 'RerouteNotes' THEN isnull(O_R.Notes, '') END
		FROM viewOrderReroute O_R
		WHERE O_R.ID = @RerouteID
		SELECT @RerouteID = min(ID) FROM tblOrderReroute WHERE OrderID = @OrderID AND ID > @RerouteID
	END

	RETURN (@ret)
END


GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details + FINANCIAL INFO into a single view
/***********************************/
CREATE VIEW [dbo].[viewOrder_Financial_Carrier] AS 
	SELECT OE.* 
		, dbo.fnLocal_to_UTC(ISNULL(OIC.LastChangeDateUTC, OIC.CreateDateUTC), OE.OriginTimeZoneID, OE.OriginUseDST) AS RatesAppliedDate
		, OIC.BatchID
		, SB.BatchNum
		, OIC.ChainupFee
		, OIC.RerouteFee
		, OIC.BillableWaitMinutes
		, OIC.WaitFeeSubUnitID
		, OIC.WaitFeeRoundingTypeID
		, OIC.WaitRate
		, OIC.WaitFee
		, OIC.RejectionFee
		, OIC.H2SRate
		, OIC.H2SFee
		, OIC.TaxRate
		, OIC.RouteRate
		, OIC.LoadFee
		, OIC.TotalFee
		, OIC.FuelSurcharge
		, C.MinSettlementBarrels
		, dbo.fnOrderTicketDetails(OE.ID, 'TicketNums', '<br/') AS TicketNums
		, dbo.fnOrderTicketDetails(OE.ID, 'TankNums', '<br/') AS TankNums
	FROM dbo.viewOrderExportFull OE
	JOIN dbo.tblCarrier C ON C.ID = OE.CarrierID
	LEFT JOIN dbo.tblOrderInvoiceCarrier OIC ON OIC.OrderID = OE.ID
	LEFT JOIN dbo.tblCarrierSettlementBatch SB ON SB.ID = OIC.BatchID


GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details + FINANCIAL INFO into a single view
/***********************************/
CREATE VIEW [dbo].[viewOrder_Financial_Customer] AS 
	SELECT OE.* 
		, dbo.fnLocal_to_UTC(ISNULL(OIC.LastChangeDateUTC, OIC.CreateDateUTC), OE.OriginTimeZoneID, OE.OriginUseDST) AS RatesAppliedDate
		, OIC.BatchID
		, SB.BatchNum
		, OIC.ChainupFee
		, OIC.RerouteFee
		, OIC.BillableWaitMinutes
		, OIC.WaitFeeSubUnitID
		, OIC.WaitFeeRoundingTypeID
		, OIC.WaitRate
		, OIC.WaitFee
		, OIC.RejectionFee
		, OIC.H2SRate
		, OIC.H2SFee
		, OIC.TaxRate
		, OIC.RouteRate
		, OIC.LoadFee
		, OIC.TotalFee
		, OIC.FuelSurcharge
		, C.MinSettlementBarrels
		, dbo.fnOrderTicketDetails(OE.ID, 'TicketNums', '<br/') AS TicketNums
		, dbo.fnOrderTicketDetails(OE.ID, 'TankNums', '<br/') AS TankNums
	FROM dbo.viewOrderExportFull OE
	JOIN dbo.tblCustomer C ON C.ID = OE.CustomerID
	LEFT JOIN dbo.tblOrderInvoiceCustomer OIC ON OIC.OrderID = OE.ID
	LEFT JOIN dbo.tblCustomerSettlementBatch SB ON SB.ID = OIC.BatchID


GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details into a single view
/***********************************/
CREATE VIEW [dbo].[viewOrder_OrderTicket_Full] AS 
	SELECT OE.* 
		, CASE WHEN OE.TicketCount = 0 THEN OE.TicketType ELSE OT.TicketType END AS T_TicketType
		, CASE WHEN OE.TicketCount = 0 THEN OE.CarrierTicketNum ELSE OT.CarrierTicketNum END AS T_CarrierTicketNum
		, CASE WHEN OE.TicketCount = 0 THEN OE.OriginBOLNum ELSE OT.BOLNum END AS T_BOLNum
		, OT.TankNum AS T_TankNum
		, ltrim(OT.OpeningGaugeFeet) + 'ft ' + ltrim(OT.OpeningGaugeInch) + 'in ' + ltrim(OT.OpeningGaugeQ) + 'q' AS T_OpenReading
		, ltrim(OT.ClosingGaugeFeet) + 'ft ' + ltrim(OT.ClosingGaugeInch) + 'in ' + ltrim(OT.ClosingGaugeQ) + 'q' AS T_CloseReading
		-- using cast(xx as decimal(9,4)) to properly round to 3 decimal digits (with no trailing 0's)
		, ltrim(round(cast(dbo.fnCorrectedAPIGravity(OT.ProductObsGravity, OT.ProductObsTemp) as decimal(9,4)), 9, 4)) AS T_CorrectedAPIGravity
		, ltrim(OT.SealOff) AS T_SealOff
		, ltrim(OT.SealOn) AS T_SealOn
		, OT.ProductObsTemp AS T_ProductObsTemp
		, OT.ProductObsGravity AS T_ProductObsGravity
		, OT.ProductBSW AS T_ProductBSW
		, OT.Rejected AS T_Rejected
		, OT.RejectNotes AS T_RejectNotes
		, OT.GrossBarrels AS T_GrossBarrels
		, OT.NetBarrels AS T_NetBarrels
		, dbo.fnRerouteDetails(OE.ID, 'PreviousDestinations', '<br/>') AS PreviousDestinations
		, dbo.fnRerouteDetails(OE.ID, 'RerouteUsers', '<br/>') AS RerouteUsers
		, dbo.fnRerouteDetails(OE.ID, 'RerouteDates', '<br/>') AS RerouteDates
		, dbo.fnRerouteDetails(OE.ID, 'RerouteNotes', '<br/>') AS RerouteNotes
	FROM dbo.viewOrderExportFull OE
	LEFT JOIN viewOrderTicket OT ON OT.OrderID = OE.ID AND OT.DeleteDateUTC IS NULL
	WHERE OE.DeleteDateUTC IS NULL
	

GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
/***********************************/
ALTER PROCEDURE [dbo].[spOrdersExportCarrier]
(
  @StartDate datetime = NULL
, @EndDate datetime = NULL
, @CarrierID int = -1 -- all carriers
, @ProductID int = -1 -- all products
, @BatchID int = NULL  -- either show unbatched (NULL), or the specified batch orders
) AS BEGIN
	SELECT @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	
	SELECT OE.* 
	FROM dbo.viewOrder_Financial_Carrier OE
	WHERE OE.StatusID IN (4)  
	  AND (@CarrierID=-1 OR OE.CarrierID=@CarrierID) 
	  AND (@ProductID=-1 OR OE.ProductID=@ProductID) 
	  AND (@StartDate IS NULL OR OE.OrderDate >= @StartDate) 
	  AND (@EndDate IS NULL OR OE.OrderDate <= @EndDate)
	  AND ((@BatchID IS NULL AND OE.BatchID IS NULL) OR OE.BatchID = @BatchID)
	ORDER BY OE.OriginDepartTimeUTC
	
END


GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
/***********************************/
ALTER PROCEDURE [dbo].[spOrdersExportCustomer]
(
  @StartDate datetime = NULL
, @EndDate datetime = NULL
, @CustomerID int = -1 -- all carriers
, @ProductID int = -1 -- all products
, @BatchID int = NULL  -- either show unbatched (NULL), or the specified batch orders
) AS BEGIN
	SELECT @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	
	SELECT OE.* 
	FROM dbo.viewOrder_Financial_Customer OE
	WHERE  OE.StatusID IN (4)  
	  AND (@CustomerID=-1 OR OE.CustomerID=@CustomerID) 
	  AND (@ProductID=-1 OR OE.ProductID=@ProductID) 
	  AND (@StartDate IS NULL OR OE.OrderDate >= @StartDate) 
	  AND (@EndDate IS NULL OR OE.OrderDate <= @EndDate)
	  AND ((@BatchID IS NULL AND OE.BatchID IS NULL) OR OE.BatchID = @BatchID)
	ORDER BY OE.OriginDepartTimeUTC
END


GO

/***********************************/
-- Date Created: 28 Feb 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc for 3rd parties (Customer/Carrier)
/***********************************/
ALTER PROCEDURE [dbo].[spOrdersFullExport]
(
  @StartDate datetime
, @EndDate datetime
, @CarrierID int = 0 -- -1 = all carriers
, @CustomerID int = 0 -- -1 = all customers
, @ProducerID int = 0
, @AuditedOnly bit = 0
) AS BEGIN
	SELECT @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	
	SELECT * 
	FROM dbo.viewOrder_OrderTicket_Full OE
	WHERE (@CarrierID=-1 OR @CustomerID=-1 OR CarrierID=@CarrierID OR CustomerID=@CustomerID OR ProducerID=@ProducerID) 
	  AND OrderDate BETWEEN @StartDate AND @EndDate
	  AND DeleteDateUTC IS NULL
	  AND (@AuditedOnly = 0 OR StatusID = 4)
	ORDER BY OrderDate
END

GO

IF EXISTS(SELECT 1 FROM sys.database_principals WHERE [name] = N'dispatchcrude_iis_acct')
	GRANT EXECUTE ON OBJECT::[dbo].[fnOrderTicketDetails] TO [dispatchcrude_iis_acct]
GO

IF EXISTS(SELECT 1 FROM sys.database_principals WHERE [name] = N'dispatchcrude_iis_acct')
	GRANT SELECT ON OBJECT::[dbo].[viewOrder_OrderTicket_Full] TO [dispatchcrude_iis_acct]
GO

IF EXISTS(SELECT 1 FROM sys.database_principals WHERE [name] = N'dispatchcrude_iis_acct')
	GRANT SELECT ON OBJECT::[dbo].[viewOrder_Financial_Customer] TO [dispatchcrude_iis_acct]
GO

IF EXISTS(SELECT 1 FROM sys.database_principals WHERE [name] = N'dispatchcrude_iis_acct')
	GRANT EXECUTE ON OBJECT::[dbo].[fnRerouteDetails] TO [dispatchcrude_iis_acct]
GO

IF EXISTS(SELECT 1 FROM sys.database_principals WHERE [name] = N'dispatchcrude_iis_acct')
	GRANT SELECT ON OBJECT::[dbo].[viewOrder_Financial_Carrier] TO [dispatchcrude_iis_acct]
GO

COMMIT TRANSACTION
SET NOEXEC OFF

