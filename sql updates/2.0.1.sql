/* move PrintHeaderBlob/PrintHeaderFileName from tblCarrier to tblCustomer (shipper)
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.0.0', @NewVersion = '2.0.1'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

ALTER TABLE tblCarrier DROP COLUMN PrintHeaderBlob, PrintHeaderFileName
GO

ALTER TABLE tblCustomer ADD PrintHeaderBlob varbinary(max) NULL
GO

ALTER TABLE tblCustomer ADD PrintHeaderFileName varchar(255) NULL
GO

/***********************************/
-- Date Created: 4 Apr 2013
-- Author: Kevin Alons
-- Purpose: return Carrier records with translated "friendly" values
/***********************************/
ALTER VIEW [dbo].[viewCarrier] AS
SELECT C.*
	, cast(CASE WHEN C.DeleteDateUTC IS NULL THEN 1 ELSE 0 END as bit) AS Active
	, CT.Name AS CarrierType
	, S.Abbreviation AS StateAbbrev
	, SF.Name AS SettlementFactor 
FROM tblCarrier C 
JOIN tblCarrierType CT ON CT.ID = C.CarrierTypeID
LEFT JOIN tblState S ON S.ID = C.StateID
LEFT JOIN tblSettlementFactor SF ON SF.ID = C.SettlementFactorID

GO
/***********************************/
-- Date Created: 27 Jan 2013
-- Author: Kevin Alons
-- Purpose: return Customer records with "translated friendly" values for FK relationships
/***********************************/
ALTER VIEW [dbo].[viewCustomer] AS
SELECT C.*, S.Abbreviation AS StateAbbrev, S.FullName AS State, SF.Name AS SettlementFactor
FROM dbo.tblCustomer C
LEFT JOIN dbo.tblState S ON S.ID = C.StateID
LEFT JOIN tblSettlementFactor SF ON SF.ID = C.SettlementFactorID

GO

/*******************************************/
-- Date Created: 25 Apr 2013
-- Author: Kevin Alons
-- Purpose: return Order data for Driver App sync
/*******************************************/
ALTER FUNCTION [dbo].[fnOrder_DriverApp]( @DriverID int, @LastChangeDateUTC datetime ) RETURNS  TABLE AS
RETURN 
	SELECT O.ID
		, O.OrderNum
		, O.StatusID
		, O.TicketTypeID
		, OO.TankTypeID
		, P.PriorityNum
		, O.Product
		, O.DueDate
		, O.TruckID
		, O.TrailerID
		, O.Trailer2ID
		, O.Origin
		, O.OriginFull
		, OO.Station AS OriginStation
		, OO.County AS OrigionCounty
		, O.OriginState
		, OO.WellAPI AS OriginAPI
		, O.OriginBOLNum
		, O.OriginArriveTimeUTC
		, O.OriginDepartTimeUTC
		, O.OriginMinutes
		, O.OriginWaitNotes
		, O.OriginTruckMileage
		, O.OriginGrossBarrels
		, O.OriginNetBarrels
		, O.ChainUp
		, O.Rejected
		, O.RejectNotes
		, O.OriginTankNum
		, O.Destination
		, O.DestinationFull
		, D.Station AS DestinationStation
		, O.DestArriveTimeUTC
		, O.DestDepartTimeUTC
		, O.DestMinutes
		, O.DestWaitNotes
		, O.DestTruckMileage
		, O.DestGrossBarrels
		, O.DestNetBarrels
		, O.DestProductTemp
		, O.DestProductBSW
		, O.DestProductGravity
		, O.DestOpenMeterBarrels
		, O.DestCloseMeterBarrels
		, O.CarrierTicketNum
		, cast(CASE WHEN D.TicketTypeID = 2 THEN 1 ELSE 0 END as bit) AS DestBOLAvailable
		, OO.BarrelsPerInch AS OriginBarrelsPerInch
		, O.CreateDateUTC
		, O.CreatedByUser
		, O.LastChangeDateUTC
		, O.LastChangedByUser
		, O.DeleteDateUTC
		, O.DeletedByUser
		, O.OriginID
		, O.DestinationID
		, O.PriorityID
		, O.Operator
		, O.OperatorID
		, O.Pumper
		, O.PumperID
		, O.Producer
		, O.ProducerID
		, O.Customer
		, O.CustomerID
		, O.Carrier
		, O.CarrierID
		, O.ProductID
		, O.AcceptLastChangeDateUTC
		, O.PickupLastChangeDateUTC
		, O.DeliverLastChangeDateUTC
		, C.PrintHeaderBlob
	FROM dbo.viewOrder O
	JOIN dbo.tblPriority P ON P.ID = O.PriorityID
	JOIN dbo.viewOrigin OO ON OO.ID = O.OriginID
	JOIN dbo.tblDestination D ON D.ID = O.DestinationID
	JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
	WHERE (StatusID IN (2, 7, 8) -- Dispatched, Accepted, Picked Up
		-- or status = Delivered and the driver still has access to the record
		OR (StatusID = 3 AND O.DestDepartTimeUTC < DATEADD(hour, (SELECT cast(value as int) FROM tblSetting WHERE ID = 10), GETUTCDATE())))
	  AND O.DriverID = @DriverID
	  AND (@LastChangeDateUTC IS NULL OR O.LastChangeDateUTC >= @LastChangeDateUTC)

GO

EXEC _spRefreshAllViews
GO

COMMIT
SET NOEXEC OFF