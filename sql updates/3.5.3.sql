-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.5.2'
SELECT  @NewVersion = '3.5.3'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'rename, reconfigure and add new Settlement related Report Center columns'
	UNION SELECT @NewVersion, 1, 'Settlement Module Report Center changes'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details + FINANCIAL INFO into a single view
/***********************************/
ALTER VIEW [dbo].[viewOrder_Financial_Carrier] AS 
	SELECT O.* 
		, TicketNums = dbo.fnOrderTicketDetails(O.ID, 'TicketNums', '<br/>') 
		, TankNums = dbo.fnOrderTicketDetails(O.ID, 'TankNums', '<br/>') 
		, PreviousDestinations = dbo.fnRerouteDetails(O.ID, 'PreviousDestinations', '<br/>') 
		, RerouteUsers = dbo.fnRerouteDetails(O.ID, 'RerouteUsers', '<br/>') 
		, RerouteDates = dbo.fnRerouteDetails(O.ID, 'RerouteDates', '<br/>') 
		, RerouteNotes = dbo.fnRerouteDetails(O.ID, 'RerouteNotes', '<br/>') 
		, InvoiceRatesAppliedDate = dbo.fnUTC_To_Local(OIC.CreateDateUTC, O.OriginTimeZoneID, O.OriginUseDST)
		, OIC.BatchID
		, InvoiceBatchNum = OIC.BatchNum 
		, InvoiceOriginWaitBillableMinutes = OIC.OriginWaitBillableMinutes
		, InvoiceDestinationWaitBillableMinutes = OIC.DestinationWaitBillableMinutes
		, InvoiceTotalWaitBillableMinutes = isnull(OIC.OriginWaitBillableMinutes, 0) + ISNULL(OIC.DestinationWaitBillableMinutes, 0) 
		, InvoiceWaitFeeSubUnit = WaitFeeSubUnit
		, InvoiceWaitFeeRoundingType = WaitFeeRoundingType
		, InvoiceOriginWaitRate = OIC.OriginWaitRate 
		, InvoiceOriginWaitAmount = OIC.OriginWaitAmount 		
		, InvoiceDestinationWaitRate = OIC.DestinationWaitRate 
		, InvoiceDestinationWaitAmount = OIC.DestinationWaitAmount 
		, InvoiceTotalWaitAmount = OIC.TotalWaitAmount
		, InvoiceOrderRejectRate = OIC.OrderRejectRate	-- changed
		, InvoiceOrderRejectRateType = OIC.OrderRejectRateType  -- changed
		, InvoiceOrderRejectAmount = OIC.OrderRejectAmount  -- changed
		, InvoiceChainupRate = OIC.ChainupRate
		, InvoiceChainupRateType = OIC.ChainupRateType
		, InvoiceChainupAmount = OIC.ChainupAmount 
		, InvoiceRerouteRate = OIC.RerouteRate
		, InvoiceRerouteRateType = OIC.RerouteRateType
		, InvoiceRerouteAmount = OIC.RerouteAmount 
		, InvoiceH2SRate = OIC.H2SRate
		, InvoiceH2SRateType = OIC.H2SRateType
		, InvoiceH2SAmount = OIC.H2SAmount
		, InvoiceSplitLoadRate = OIC.SplitLoadRate
		, InvoiceSplitLoadRateType = OIC.SplitLoadRateType
		, InvoiceSplitLoadAmount = OIC.SplitLoadAmount
		, InvoiceTaxRate = OIC.OriginTaxRate
		, InvoiceSettlementUom = OIC.SettlementUom -- changed
		, InvoiceSettlementUomShort = OIC.SettlementUomShort -- changed
		, InvoiceMinSettlementUnits = OIC.MinSettlementUnits
		, InvoiceUnits = OIC.SettlementUnits
		, InvoiceRouteRate = OIC.RouteRate
		, InvoiceRouteRateType = OIC.RouteRateType
		, InvoiceRateSheetRate = OIC.RateSheetRate
		, InvoiceRateSheetRateType = OIC.RateSheetRateType
		, InvoiceFuelSurchargeRate = OIC.FuelSurchargeRate
		, InvoiceFuelSurchargeAmount = OIC.FuelSurchargeAmount
		, InvoiceLoadAmount = OIC.LoadAmount
		, InvoiceTotalAmount = OIC.TotalAmount
	FROM dbo.viewOrderExportFull O
	LEFT JOIN viewOrderSettlementCarrier OIC ON OIC.OrderID = O.ID
	WHERE O.StatusID NOT IN (-10) -- don't include "Generated" orders (only Assigned+)

GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details + FINANCIAL INFO into a single view
/***********************************/
ALTER VIEW [dbo].[viewOrder_Financial_Shipper] AS 
	SELECT O.* 
		, Shipper = Customer
		, TicketNums = dbo.fnOrderTicketDetails(O.ID, 'TicketNums', '<br/>') 
		, TankNums = dbo.fnOrderTicketDetails(O.ID, 'TankNums', '<br/>') 
		, PreviousDestinations = dbo.fnRerouteDetails(O.ID, 'PreviousDestinations', '<br/>') 
		, RerouteUsers = dbo.fnRerouteDetails(O.ID, 'RerouteUsers', '<br/>') 
		, RerouteDates = dbo.fnRerouteDetails(O.ID, 'RerouteDates', '<br/>') 
		, RerouteNotes = dbo.fnRerouteDetails(O.ID, 'RerouteNotes', '<br/>') 
		, InvoiceRatesAppliedDate = dbo.fnUTC_To_Local(OIC.CreateDateUTC, O.OriginTimeZoneID, O.OriginUseDST)
		, OIC.BatchID
		, InvoiceBatchNum = OIC.BatchNum 
		, InvoiceOriginWaitBillableMinutes = OIC.OriginWaitBillableMinutes
		, InvoiceDestinationWaitBillableMinutes = OIC.DestinationWaitBillableMinutes
		, InvoiceTotalWaitBillableMinutes = isnull(OIC.OriginWaitBillableMinutes, 0) + ISNULL(OIC.DestinationWaitBillableMinutes, 0) 
		, InvoiceWaitFeeSubUnit = WaitFeeSubUnit
		, InvoiceWaitFeeRoundingType = WaitFeeRoundingType
		, InvoiceOriginWaitRate = OIC.OriginWaitRate 
		, InvoiceOriginWaitAmount = OIC.OriginWaitAmount 		
		, InvoiceDestinationWaitRate = OIC.DestinationWaitRate 
		, InvoiceDestinationWaitAmount = OIC.DestinationWaitAmount 
		, InvoiceTotalWaitAmount = OIC.TotalWaitAmount
		, InvoiceOrderRejectRate = OIC.OrderRejectRate  -- changed
		, InvoiceOrderRejectRateType = OIC.OrderRejectRateType -- changed
		, InvoiceOrderRejectAmount = OIC.OrderRejectAmount  -- changed
		, InvoiceChainupRate = OIC.ChainupRate
		, InvoiceChainupRateType = OIC.ChainupRateType
		, InvoiceChainupAmount = OIC.ChainupAmount 
		, InvoiceRerouteRate = OIC.RerouteRate
		, InvoiceRerouteRateType = OIC.RerouteRateType
		, InvoiceRerouteAmount = OIC.RerouteAmount 
		, InvoiceH2SRate = OIC.H2SRate
		, InvoiceH2SRateType = OIC.H2SRateType
		, InvoiceH2SAmount = OIC.H2SAmount
		, InvoiceSplitLoadRate = OIC.SplitLoadRate
		, InvoiceSplitLoadRateType = OIC.SplitLoadRateType
		, InvoiceSplitLoadAmount = OIC.SplitLoadAmount
		, InvoiceTaxRate = OIC.OriginTaxRate
		, InvoiceSettlementUom = OIC.SettlementUom -- changed
		, InvoiceSettlementUomShort = OIC.SettlementUomShort -- changed
		, InvoiceMinSettlementUnits = OIC.MinSettlementUnits
		, InvoiceUnits = OIC.SettlementUnits
		, InvoiceRouteRate = OIC.RouteRate
		, InvoiceRouteRateType = OIC.RouteRateType
		, InvoiceRateSheetRate = OIC.RateSheetRate
		, InvoiceRateSheetRateType = OIC.RateSheetRateType
		, InvoiceFuelSurchargeRate = OIC.FuelSurchargeRate
		, InvoiceFuelSurchargeAmount = OIC.FuelSurchargeAmount
		, InvoiceLoadAmount = OIC.LoadAmount
		, InvoiceTotalAmount = OIC.TotalAmount
	FROM dbo.viewOrderExportFull O
	LEFT JOIN viewOrderSettlementShipper OIC ON OIC.OrderID = O.ID
	WHERE O.StatusID NOT IN (-10) -- don't include "Generated" orders (only Assigned+)

GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Report Center Order data
/***********************************/
ALTER VIEW [dbo].[viewReportCenter_Orders] AS
	SELECT O.*
		, ShipperBatchNum = SS.BatchNum
		, ShipperBatchInvoiceNum = SSB.InvoiceNum
		, ShipperSettlementUomID = SS.SettlementUomID
		, ShipperSettlementUom = SS.SettlementUom
		, ShipperMinSettlementUnits = SS.MinSettlementUnits
		, ShipperSettlementUnits = SS.SettlementUnits
		, ShipperRateSheetRate = SS.RateSheetRate
		, ShipperRateSheetRateType = SS.RateSheetRateType
		, ShipperRouteRate = SS.RouteRate
		, ShipperRouteRateType = SS.RouteRateType
		, ShipperLoadRate = isnull(SS.RouteRate, SS.RateSheetRate)
		, ShipperLoadRateType = isnull(SS.RouteRateType, SS.RateSheetRateType)
		, ShipperLoadAmount = SS.LoadAmount
		, ShipperOrderRejectRate = SS.OrderRejectRate -- changed
		, ShipperOrderRejectRateType = SS.OrderRejectRateType -- new
		, ShipperOrderRejectAmount = SS.OrderRejectAmount -- changed
		, ShipperWaitFeeSubUnit = SS.WaitFeeSubUnit  -- new
		, ShipperWaitFeeRoundingType = SS.WaitFeeRoundingType  -- new
		, ShipperOriginWaitBillableMinutes = SS.OriginWaitBillableMinutes  -- new
		, ShipperOriginWaitRate = SS.OriginWaitRate
		, ShipperOriginWaitAmount = SS.OriginWaitAmount
		, ShipperDestinationWaitBillableMinutes = SS.DestinationWaitBillableMinutes   -- new
		, ShipperDestinationWaitRate = SS.DestinationWaitRate  -- changed
		, ShipperDestinationWaitAmount = SS.DestinationWaitAmount  -- changed
		, ShipperTotalWaitAmount = SS.TotalWaitAmount

		, ShipperFuelSurchargeRate = SS.FuelSurchargeRate
		, ShipperFuelSurchargeAmount = SS.FuelSurchargeAmount

		, ShipperChainupRate = SS.ChainupRate
		, ShipperChainupRateType = SS.ChainupRateType  -- new
		, ShipperChainupAmount = SS.ChainupAmount
		, ShipperRerouteRate = SS.RerouteRate
		, ShipperRerouteRateType = SS.RerouteRateType  -- new
		, ShipperRerouteAmount = SS.RerouteAmount
		, ShipperSplitLoadRate = SS.SplitLoadRate
		, ShipperSplitLoadRateType = SS.SplitLoadRateType  -- new
		, ShipperSplitLoadAmount = SS.SplitLoadAmount
		, ShipperH2SRate = SS.H2SRate
		, ShipperH2SRateType = SS.H2SRateType  -- new
		, ShipperH2SAmount = SS.H2SAmount

		, ShipperTaxRate = SS.OriginTaxRate
		, ShipperTotalAmount = SS.TotalAmount

		, CarrierBatchNum = SC.BatchNum
		, CarrierSettlementUomID = SC.SettlementUomID
		, CarrierSettlementUom = SC.SettlementUom
		, CarrierMinSettlementUnits = SC.MinSettlementUnits
		, CarrierSettlementUnits = SC.SettlementUnits
		, CarrierRateSheetRate = SC.RateSheetRate
		, CarrierRateSheetRateType = SC.RateSheetRateType
		, CarrierRouteRate = SC.RouteRate
		, CarrierRouteRateType = SC.RouteRateType
		, CarrierLoadRate = isnull(SC.RouteRate, SC.RateSheetRate)
		, CarrierLoadRateType = isnull(SC.RouteRateType, SC.RateSheetRateType)
		, CarrierLoadAmount = SC.LoadAmount
		, CarrierOrderRejectRate = SC.OrderRejectRate -- changed
		, CarrierOrderRejectRateType = SC.OrderRejectRateType -- new
		, CarrierOrderRejectAmount = SC.OrderRejectAmount -- changed
		, CarrierWaitFeeSubUnit = SC.WaitFeeSubUnit  -- new
		, CarrierWaitFeeRoundingType = SC.WaitFeeRoundingType  -- new
		, CarrierOriginWaitBillableMinutes = SC.OriginWaitBillableMinutes  -- new
		, CarrierOriginWaitRate = SC.OriginWaitRate
		, CarrierOriginWaitAmount = SC.OriginWaitAmount
		, CarrierDestinationWaitBillableMinutes = SC.DestinationWaitBillableMinutes  -- new
		, CarrierDestinationWaitRate = SC.DestinationWaitRate -- changed
		, CarrierDestinationWaitAmount = SC.DestinationWaitAmount  -- changed
		, CarrierTotalWaitAmount = SC.TotalWaitAmount

		, CarrierFuelSurchargeRate = SC.FuelSurchargeRate
		, CarrierFuelSurchargeAmount = SC.FuelSurchargeAmount

		, CarrierChainupRate = SC.ChainupRate
		, CarrierChainupRateType = SC.ChainupRateType  -- new
		, CarrierChainupAmount = SC.ChainupAmount
		, CarrierRerouteRate = SC.RerouteRate
		, CarrierRerouteRateType = SC.RerouteRateType  -- new
		, CarrierRerouteAmount = SC.RerouteAmount
		, CarrierSplitLoadRate = SC.SplitLoadRate
		, CarrierSplitLoadRateType = SC.SplitLoadRateType  -- new
		, CarrierSplitLoadAmount = SC.SplitLoadAmount
		, CarrierH2SRate = SC.H2SRate
		, CarrierH2SRateType = SC.H2SRateType  -- new
		, CarrierH2SAmount = SC.H2SAmount

		, CarrierTaxRate = SC.OriginTaxRate
		, CarrierTotalAmount = SC.TotalAmount

		, OriginGpsLatLon = ltrim(DLO.Lat) + ',' + ltrim(DLO.Lon)
		, OriginLatLon = ltrim(OO.LAT) + ',' + ltrim(OO.LON)
		, OriginAccuracyMeters = DLO.SourceAccuracyMeters
		, OriginDistance = isnull(cast(DLO.DistanceToPoint as int), 99999)
		, OriginGpsArrived = CASE WHEN DLO.DistanceToPoint IS NOT NULL AND DLO.DistanceToPoint <= OO.GeoFenceRadiusMeters THEN 1 ELSE 0 END
		, DestGpsLatLon = ltrim(DLD.Lat) + ',' + ltrim(DLD.Lon)
		, DestLatLon = ltrim(D.LAT) + ',' + ltrim(D.LON)
		, DestAccuracyMeters = DLD.SourceAccuracyMeters
		, DestDistance = isnull(cast(DLD.DistanceToPoint as int), 99999)
		, DestGpsArrived = CASE WHEN DLD.DistanceToPoint IS NOT NULL AND DLD.DistanceToPoint <= OO.GeoFenceRadiusMeters THEN 1 ELSE 0 END
		, ShipperDestCode = CDC.Code
	FROM viewOrder_OrderTicket_Full O
	JOIN tblOrigin OO ON OO.ID = O.OriginID
	JOIN tblDestination D ON D.ID = O.DestinationID
	LEFT JOIN tblCustomerDestinationCode CDC ON CDC.CustomerID = O.CustomerID AND CDC.DestinationID = O.DestinationID
	LEFT JOIN viewDriverLocation_OriginFirstArrive DLO ON DLO.OrderID = O.ID AND DLO.OriginID = O.OriginID
	LEFT JOIN viewDriverLocation_DestinationFirstArrive DLD ON DLD.OrderID = O.ID AND DLD.DestinationID = O.DestinationID
	LEFT JOIN viewOrderSettlementCarrier SC ON SC.OrderID = O.ID
	LEFT JOIN viewOrderSettlementShipper SS ON SS.OrderID = O.ID
	LEFT JOIN tblShipperSettlementBatch SSB ON SSB.ID = SS.BatchID

GO

--	SELECT MAX(id) from tblReportColumnDefinition where ID < 90000
--  select * from tblReportColumnDefinition where DataField like '%DestWaitRate%' or DataField like '%DestWaitAmount%'

UPDATE tblReportColumnDefinition SET DataField = 'ShipperRerouteRate', Caption = 'SETTLEMENT | SHIPPER | Shipper Reroute Rate' WHERE ID = 151
UPDATE tblReportColumnDefinition SET DataField = 'CarrierRerouteRate', Caption = 'SETTLEMENT | CARRIER | Carrier Reroute Rate' WHERE ID = 169
UPDATE tblReportColumnDefinition SET DataField = REPLACE(DataField, 'DestWait', 'DestinationWait') WHERE DataField like '%DestWaitRate%' or DataField like '%DestWaitAmount%'
UPDATE tblReportColumnDefinition SET DataField = 'ShipperSettlementUnits' WHERE ID = 182
UPDATE tblReportColumnDefinition SET DataField = 'CarrierSettlementUnits' WHERE ID = 183
UPDATE tblReportColumnDefinition SET Caption = 'SETTLEMENT | SHIPPER | Shipper Override Route Rate' WHERE ID = 160
UPDATE tblReportColumnDefinition SET Caption = 'SETTLEMENT | CARRIER | Carrier Override Route Rate' WHERE ID = 178
UPDATE tblReportColumnDefinition SET DataField = 'PriorityNum' WHERE ID = 13
UPDATE tblReportColumnDefinition SET DataField = 'ShipperLoadAmount' WHERE ID = 161
UPDATE tblReportColumnDefinition SET DataField = 'CarrierLoadAmount' WHERE ID = 179

UPDATE tblReportColumnDefinition SET Caption = 'ORIGIN | GENERAL | Origin UOM Short' WHERE ID = 77
GO

SET IDENTITY_INSERT tblReportColumnDefinition ON
INSERT INTO tblReportColumnDefinition 
	(  ID, ReportID, DataField								, Caption														, DataFormat, FilterDataField	, FilterTypeID	, FilterDropDownSql	, FilterAllowCustomText	, AllowedRoles, OrderSingleExport)
	SELECT 209, 1, 'ShipperOrderRejectRate'					, 'SETTLEMENT | SHIPPER | Shipper Reject Rate'					, '#0.00'	, NULL				, 4				, NULL				, 1						, 'Administrator,Management', 1
	UNION
	SELECT 210, 1, 'ShipperOrderRejectRateType'				, 'SETTLEMENT | SHIPPER | Shipper Reject Rate Type'				, NULL		, NULL				, 1				, NULL				, 1						, 'Administrator,Management', 1
	UNION
	SELECT 211, 1, 'ShipperOrderRejectAmount'				, 'SETTLEMENT | SHIPPER | Shipper Reject $$'					, '#0.00'	, NULL				, 4				, NULL				, 1						, 'Administrator,Management', 1
	UNION
	SELECT 212, 1, 'CarrierOrderRejectRate'					, 'SETTLEMENT | CARRIER | Carrier Reject Rate'					, '#0.00'	, NULL				, 4				, NULL				, 1						, 'Administrator,Management', 1
	UNION
	SELECT 213, 1, 'CarrierOrderRejectRateType'				, 'SETTLEMENT | CARRIER | Carrier Reject Rate Type'				, NULL		, NULL				, 1				, NULL				, 1						, 'Administrator,Management', 1
	UNION
	SELECT 214, 1, 'CarrierOrderRejectAmount'				, 'SETTLEMENT | CARRIER | Carrier Reject $$'					, '#0.00'	, NULL				, 4				, NULL				, 1						, 'Administrator,Management', 1
	UNION
	SELECT 215, 1, 'ShipperWaitFeeSubUnit'					, 'SETTLEMENT | SHIPPER | Shipper WaitFee Sub Unit'				, NULL		, NULL				, 1				, NULL				, 1						, 'Administrator,Management', 1
	UNION
	SELECT 216, 1, 'ShipperWaitFeeRoundingType'				, 'SETTLEMENT | SHIPPER | Shipper WaitFee Rounding Type'		, NULL		, NULL				, 1				, NULL				, 1						, 'Administrator,Management', 1
	UNION
	SELECT 217, 1, 'CarrierWaitFeeSubUnit'					, 'SETTLEMENT | CARRIER | Carrier WaitFee Sub Unit'				, NULL		, NULL				, 1				, NULL				, 1						, 'Administrator,Management', 1
	UNION
	SELECT 218, 1, 'CarrierWaitFeeRoundingType'				, 'SETTLEMENT | CARRIER | Carrier WaitFee Rounding Type'		, NULL		, NULL				, 1				, NULL				, 1						, 'Administrator,Management', 1

	UNION
	SELECT 219, 1, 'ShipperOriginWaitBillableMinutes'		, 'SETTLEMENT | SHIPPER | Shipper Origin Wait Billable Minutes'	, NULL		, NULL				, 2				, NULL				, 1						, 'Administrator,Management', 1
	UNION
	SELECT 220, 1, 'ShipperDestinationWaitBillableMinutes'	, 'SETTLEMENT | SHIPPER | Shipper Dest Wait Billable Minutes'	, NULL		, NULL				, 2				, NULL				, 1						, 'Administrator,Management', 1
	UNION
	SELECT 221, 1, 'CarrierOriginWaitBillableMinutes'		, 'SETTLEMENT | CARRIER | Carrier Origin Wait Billable Minutes'	, NULL		, NULL				, 2				, NULL				, 1						, 'Administrator,Management', 1
	UNION
	SELECT 222, 1, 'CarrierDestinationWaitBillableMinutes'	, 'SETTLEMENT | CARRIER | Carrier Dest Wait Billable Minutes'	, NULL		, NULL				, 2				, NULL				, 1						, 'Administrator,Management', 1

	UNION
	SELECT 223, 1, 'ShipperChainupRateType'					, 'SETTLEMENT | SHIPPER | Shipper Chainup Rate Type'			, NULL		, NULL				, 1				, NULL				, 1						, 'Administrator,Management', 1
	UNION
	SELECT 224, 1, 'ShipperRerouteRateType'					, 'SETTLEMENT | SHIPPER | Shipper Reroute Rate Type'			, NULL		, NULL				, 1				, NULL				, 1						, 'Administrator,Management', 1
	UNION
	SELECT 225, 1, 'ShipperSplitLoadRateType'				, 'SETTLEMENT | SHIPPER | Shipper Split Load Rate Type'			, NULL		, NULL				, 1				, NULL				, 1						, 'Administrator,Management', 1
	UNION
	SELECT 226, 1, 'ShipperH2SRateType'						, 'SETTLEMENT | SHIPPER | Shipper H2S Rate Type'				, NULL		, NULL				, 1				, NULL				, 1						, 'Administrator,Management', 1
	UNION
	SELECT 227, 1, 'CarrierChainupRateType'					, 'SETTLEMENT | CARRIER | Carrier Chainup Rate Type'			, NULL		, NULL				, 1				, NULL				, 1						, 'Administrator,Management', 1
	UNION
	SELECT 228, 1, 'CarrierRerouteRateType'					, 'SETTLEMENT | CARRIER | Carrier Reroute Rate Type'			, NULL		, NULL				, 1				, NULL				, 1						, 'Administrator,Management', 1
	UNION
	SELECT 229, 1, 'CarrierSplitLoadRateType'				, 'SETTLEMENT | CARRIER | Carrier Split Load Rate Type'			, NULL		, NULL				, 1				, NULL				, 1						, 'Administrator,Management', 1
	UNION
	SELECT 230, 1, 'CarrierH2SRateType'						, 'SETTLEMENT | CARRIER | Carrier H2S Rate Type'				, NULL		, NULL				, 1				, NULL				, 1						, 'Administrator,Management', 1
	UNION
	SELECT 231,	1, 'ShipperChainupRate'						, 'SETTLEMENT | SHIPPER | Shipper Chainup Rate'					, '#0.00'	, NULL				, 4				, NULL				, 1						, 'Administrator,Management', 1
	UNION
	SELECT 232,	1, 'CarrierChainupRate'						, 'SETTLEMENT | CARRIER | Carrier Chainup Rate'					, '#0.00'	, NULL				, 4				, NULL				, 1						, 'Administrator,Management', 1

	UNION
	SELECT 233, 1, 'ShipperRouteRate'						, 'SETTLEMENT | SHIPPER | Shipper Rate Sheet Rate'				, '#0.0000'	, NULL				, 4				, NULL				, 1						, 'Administrator,Management', 1
	UNION
	SELECT 234, 1, 'CarrierRouteRate'						, 'SETTLEMENT | CARRIER | Carrier Rate Sheet Rate'				, '#0.0000'	, NULL				, 4				, NULL				, 1						, 'Administrator,Management', 1
	
	UNION 
	SELECT 235, 1, 'ShipperSplitLoadRate'					, 'SETTLEMENT | SHIPPER | Shipper Split Load Rate'				, '#0.0000'	, NULL				, 4				, NULL				, 1						, 'Administrator,Management', 1
	UNION 
	SELECT 236, 1, 'CarrierSplitLoadRate'					, 'SETTLEMENT | CARRIER | Carrier Split Load Rate'				, '#0.0000'	, NULL				, 4				, NULL				, 1						, 'Administrator,Management', 1
	
SET IDENTITY_INSERT tblReportColumnDefinition OFF
--  select * from tblReportColumnDefinition order by caption
GO

CREATE FUNCTION fnCharIndexRev(@char char(1), @expr varchar(max)) RETURNS int AS
BEGIN
	DECLARE @pos int, @npos int; SET @pos = 0
	SET @npos = CHARINDEX(@char, @expr, @pos+1)
	WHILE @npos > 0
	BEGIN
		SET @pos = @npos
		SET @npos = CHARINDEX(@char, @expr, @pos+1)
	END
	RETURN @pos
END
GO
GRANT EXECUTE ON dbo.fnCharIndexRev TO dispatchcrude_iis_acct
GO

COMMIT
SET NOEXEC OFF