-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.6.19'
SELECT  @NewVersion = '3.6.20'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Report Center: Custom Report Columns now reference the base sql as "RS." (not "viewReportCenter_Orders.")'
	UNION SELECT @NewVersion, 0, 'Settlement: Fix bug with Carrier Rate Sheets using "% of Shipper" rates computing NULL Load Amount'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

UPDATE tblReportColumnDefinition SET DataField = REPLACE(DataField, 'viewReportCenter_Orders.', 'RS.') WHERE DataField LIKE '%viewReportCenter_Orders.%'
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RateSheetRangeRate info for the specified order
/***********************************/
ALTER FUNCTION fnOrderCarrierLoadAmount(@ID int, @Units decimal(18, 10), @UomID int) RETURNS TABLE AS RETURN
	SELECT RouteRateID, RangeRateID
		, Amount = dbo.fnRateToAmount(RateTypeID, @Units, @UomID, Rate, UomID, NULL
			, CASE WHEN RateTypeID = 3 THEN (SELECT TOP 1 LoadAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID) ELSE NULL END)
	FROM (
		SELECT TOP 1 * 
		FROM (
			SELECT SortID = 1, RouteRateID = ID, RangeRateID = NULL, RateTypeID, UomID, Rate FROM dbo.fnOrderCarrierRouteRate(@ID)
			UNION SELECT 2, NULL, ID, RateTypeID, UomID, Rate FROM dbo.fnOrderCarrierRateSheetRangeRate(@ID)
		) X
		ORDER BY SortID
	) X
GO

COMMIT
SET NOEXEC OFF