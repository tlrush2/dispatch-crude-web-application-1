/*
	-- more Report Center functionality
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.0.0'
SELECT  @NewVersion = '3.0.1'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

/*******************************************/
-- Date Created: 23 Jul 2014
-- Author: Kevin Alons
-- Purpose: clone an existing UserReport (defaults to a full clone with columns, but can do an AddNew otherwise)
/*******************************************/
CREATE PROCEDURE spCloneUserReport
(
  @userReportID int
, @reportName varchar(50)
, @userName varchar(100)
, @includeColumns bit = 1
, @newID int = 0 OUTPUT 
) AS
BEGIN
	SET NOCOUNT ON
	INSERT INTO tblUserReportDefinition (Name, ReportID, UserName, CreatedByUser)
		SELECT @reportName, ReportID, @userName, @userName
		FROM tblUserReportDefinition WHERE ID = @userReportID
	SET @newID = SCOPE_IDENTITY()
	IF (@includeColumns = 1)
	BEGIN
		INSERT INTO tblUserReportColumnDefinition (UserReportID, ReportColumnID, Caption
			, SortNum, DataFormat, FilterOperatorID, FilterValue1, FilterValue2, CreatedByUser)
			SELECT @newID, ReportColumnID, Caption
				, SortNum, DataFormat, FilterOperatorID, FilterValue1, FilterValue2, @userName
			FROM tblUserReportColumnDefinition WHERE UserReportID = @userReportID
	END
	
	SELECT NEWID=@newID
	RETURN (isnull(@newID, 0))
END

GO
GRANT EXECUTE on spCloneUserReport TO dispatchcrude_iis_acct
GO

DROP INDEX [udxUserReportDefinition_Name] ON [dbo].[tblUserReportDefinition] WITH ( ONLINE = OFF )
GO
CREATE UNIQUE NONCLUSTERED INDEX [udxUserReportDefinition_UserName_Name] ON [dbo].[tblUserReportDefinition] 
(
	[UserName] ASC,
	[Name] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

COMMIT
SET NOEXEC OFF