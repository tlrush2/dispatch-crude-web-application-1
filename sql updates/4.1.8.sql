SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.1.7'
SELECT  @NewVersion = '4.1.8'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1781 Add HOS permissions'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


EXEC spAddNewPermission 'viewHOS',
'Allow user to view Hours of Service (HOS) records',
'Compliance',
'HOS - View'

GO


EXEC spAddNewPermission 'editHOS',
'Allow user to edit Hours of Service (HOS) records',
'Compliance',
'HOS - Edit'

GO


COMMIT
SET NOEXEC OFF