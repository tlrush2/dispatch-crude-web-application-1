SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.6.8'
SELECT  @NewVersion = '4.6.9'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'Module - Driver Settlement Statement Email'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

create table tblDriverSettlementStatementEmailDefinition
(
  ID int identity(1,1) not null constraint PK_DriverSettlementStatementEmailDefinition primary key clustered 
, Name varchar(50) not null constraint UCK_DriverSettlementStatementEmailDefinition_Name unique 
, PdfExportID int not null constraint FK_DriverSettlementStatementEmailDefinition_PdfExport foreign key references tblPdfExport(ID)
, EmailSubject varchar(255) not null
, EmailBodyWordDocument varbinary(max) not null 
, EmailBodyWordDocName varchar(255) not null
, CCEmailAddress varchar(255) null
, MissingEmailAddress varchar(255) null
, CreateDateUTC datetime not null constraint DF_DriverSettlementStatementEmailDefinition_CreateDateUTC default (getutcdate())
, CreatedByUser varchar(100) not null constraint DF_DriverSettlementStatementEmailDefinition_CreatedByUser default ('System')
, LastChangeDateUTC datetime null 
, LastChangedByUser varchar(100) null
, DeleteDateUTC datetime null 
, DeletedByUser varchar(100) null
)
go
grant select, insert, update on tblDriverSettlementStatementEmailDefinition to dispatchcrude_iis_acct
go

exec spAddNewPermission 'viewDriverSettlementStatementEmailDefinition', 'Users in this role can view Driver Settlement Statement Email Definition', 'Driver Settlement Statement', 'View Driver Settlement Statement Email Definitions'
exec spAddNewPermission 'createDriverSettlementStatementEmailDefinition', 'Users in this role can create Driver Settlement Statement Email Definitions', 'Driver Settlement Statement', 'Create Driver Settlement Statement Email Definitions'
exec spAddNewPermission 'editDriverSettlementStatementEmailDefinition', 'Users in this role can edit Driver Settlement Statement Email Definitions', 'Driver Settlement Statement', 'Edit Driver Settlement Statement Email Definitions'
exec spAddNewPermission 'deactivateDriverSettlementStatementEmailDefinition', 'Users in this role can deactivate Driver Settlement Statement Email Definitions', 'Driver Settlement Statement', 'Deactivate Driver Settlement Statement Email Definitions'
go

create table tblDriverSettlementStatementEmail
(
  ID int identity(1,1) not null constraint PK_DriverSettlementStatementEmail primary key clustered 
, DriverSettlementStatementEmailDefinitionID int not null 
	constraint FK_DriverSettlementStatementEmail_DriverSettlementStatementEmailDefinition foreign key references tblDriverSettlementStatementEmailDefinition(ID)
, PdfExportID int not null constraint FK_DriverSettlementStatementEmail_PdfExport foreign key references tblPdfExport(ID)
, BatchID int not null constraint FK_DriverSettlementStatementEmail_Batch foreign key references tblDriverSettlementBatch(ID)
, DriverID int not null constraint FKD_DriverSettlementStatementEmail_Driver foreign key references tblDriver(ID)
, EmailAddress varchar(1000) null
, CCEmailAddress varchar(255) null
, CreateDateUTC datetime not null constraint DF_DriverSettlementStatementEmail_CreateDateUTC default (getutcdate())
, CreatedByUser varchar(100) not null constraint DF_DriverSettlementStatementEmail_CreatedByUser default ('System')
, QueueEmailSend bit not null constraint DF_DriverSettlementStatementEmail_QueueEmailSend default (0)
, EmailDateUTC datetime null
, EmailedByUser varchar(100) null
) 
go
grant select, insert, update on tblDriverSettlementStatementEmail to dispatchcrude_iis_acct
go

exec spAddNewPermission 'viewDriverSettlementStatementEmail', 'Users in this role can view Driver Settlement Statement Emails', 'Driver Settlement Statement', 'View Driver Settlement Statement Emails'
exec spAddNewPermission 'createDriverSettlementStatementEmail', 'Users in this role can create Driver Settlement Statement Emails', 'Driver Settlement Statement', 'Create Driver Settlement Statement Emails'
exec spAddNewPermission 'editDriverSettlementStatementEmail', 'Users in this role can edit Driver Settlement Statement Emails', 'Driver Settlement Statement', 'Edit Driver Settlement Statement Emails'
--exec spAddNewPermission 'deactivateDriverSettlementStatementEmail', 'Users in this role can deactivate Driver Settlement Statement Emails', 'Driver Settlement Statement', 'Deactivate Driver Settlement Statement Emails'
go

exec _spDropView 'viewOrderTransferAll'
go
/***************************************
 Created: 4.6.8 - 2017.05.07 - Kevin Alons
 Purpose: return all Orders with OrderTransfer info with "translated friendly" values
 Changes:
***************************************/
CREATE VIEW viewOrderTransferAll AS
SELECT OTAOrderID = O.ID /* using OTAOrderID so we can later include all columns with * without an OrderID clash */
	, OriginDriverID = ISNULL(OTR.OriginDriverID, O.DriverID)
	, OriginDriverGroupID = ISNULL(ODG.ID, DDG.ID) 
	, OriginDriverGroup = ISNULL(ODG.Name, DDG.Name)
	, OriginDriver = ISNULL(vODR.FullName, vDDR.FullName)
	, OriginDriverFirst = ISNULL(vODR.FirstName, vDDR.FirstName)
	, OriginDriverLast = ISNULL(vODR.LastName, vDDR.LastName)
	, OriginTruckID = ISNULL(OTR.OriginTruckID, O.TruckID)
	, OriginTruck = ISNULL(vOTRU.FullName, vDTRU.FullName)
	, OriginTruckTerminalID = ISNULL(vOTRU.TerminalID, vDTRU.TerminalID)
	, OriginTruckTerminal = ISNULL(vOTRU.Terminal, vDTRU.Terminal)
	, OriginDriverTerminalID = ISNULL(vODR.TerminalID, D.TerminalID)
	, OriginDriverTerminal = ISNULL(vODR.Terminal, D.Terminal)
	, DestDriverID = CASE WHEN OTR.OrderID IS NULL THEN NULL ELSE O.DriverID END
	, DestDriver = CASE WHEN OTR.OrderID IS NULL THEN NULL ELSE D.FullName END
	, DestDriverFirst = CASE WHEN OTR.OrderID IS NULL THEN NULL ELSE D.FirstName END
	, DestDriverLast = CASE WHEN OTR.OrderID IS NULL THEN NULL ELSE D.LastName END
	, DestTruckID = CASE WHEN OTR.OrderID IS NULL THEN NULL ELSE O.TruckID END
	, DestTruck = CASE WHEN OTR.OrderID IS NULL THEN NULL ELSE TRU.FullName END
	, DestTruckTerminalID = CASE WHEN OTR.OrderID IS NULL THEN NULL ELSE TRU.TerminalID END
	, DestTruckTerminal = CASE WHEN OTR.OrderID IS NULL THEN NULL ELSE TRU.Terminal END
	, DestDriverTerminalID = CASE WHEN OTR.OrderID IS NULL THEN NULL ELSE D.TerminalID END
	, DestDriverTerminal = CASE WHEN OTR.OrderID IS NULL THEN NULL ELSE D.Terminal END
	, OriginTruckEndMileage = OTR.OriginTruckEndMileage
	, DestTruckStartMileage = OTR.DestTruckStartMileage
	, TransferPercent = OTR.PercentComplete
	, TransferNotes = OTR.Notes
	, TransferDateUTC = OTR.CreateDateUTC
	, TransferComplete = CAST(OTR.TransferComplete AS BIT)
FROM tblOrder O
LEFT JOIN tblOrderTransfer OTR ON OTR.OrderID = O.ID
LEFT JOIN viewOrigin vO ON vO.ID = O.OriginID
LEFT JOIN viewDestination vD ON vD.ID = O.DestinationID
LEFT JOIN viewDriverBase D ON D.ID = O.DriverID
LEFT JOIN viewDriverBase vODR ON vODR.ID = OTR.OriginDriverID
LEFT JOIN viewDriver vDDR ON vDDR.ID = O.DriverID
LEFT JOIN tblDriverGroup ODG ON ODG.ID = vODR.DriverGroupID
LEFT JOIN tblDriverGroup DDG ON DDG.ID = D.DriverGroupID
LEFT JOIN viewTruck TRU ON TRU.ID = O.TruckID
LEFT JOIN viewTruck vOTRU ON vOTRU.ID = OTR.OriginTruckID
LEFT JOIN viewTruck vDTRU ON vDTRU.ID = O.TruckID

GO

/***************************************/
-- Date Created: 2012.11.25 - Kevin Alons
-- Purpose: return Order records with "translated friendly" values for FK relationships
-- Changes:
-- 3.9.20  - 2015/10/22 - KDA	- add Origin|Dest DriverID fields (using new tblOrderTransfer table) 
--			2015/10/28  - JAE	- added all Order Transfer fields for ease of use in reporting
--			2015/11/03  - BB	- added cast to make TransferComplet BIT type to avoid "Operand type clash" error when running this update script
-- 3.9.21  - 2015/11/03 - KDA	- add OriginDriverGroupID field (from OrderTransfer.OriginDriverID JOIN)
-- 3.9.25  - 2015/11/10 - JAE	- added origin driver and truck to view
-- 3.10.10 - 2016/02/15 - BB	- Add driver region ID to facilitate new user/region filtering feature
-- 3.10.11 - 2016/02/24 - JAE	- Add destination region (name) to view
-- 3.10.13 - 2016/02/29 - JAE	- Add TruckType
-- 3.13.8  - 2016/07/26	- BB	- Add Destination Station
-- 4.1.0.2 - 2016.08.28 - KDA	- rewrite RerouteCount, TicketCount to use a normal JOIN (instead of uncorrelated sub-query)
--								- use viewDriverBase instead of viewDriver
--								- eliminate viewGaugerBase (was unused), use tblOrderStatus instead of viewOrderPrintStatus
-- 4.1.3.3	- 2016/09/14 - BB	- Add origin and destination driving directions (initially for dispatch and truck order create pages)
-- 4.1.8.6	- 2016.09.24 - KDA	- move H2S | TicketCount | RerouteCount fields from viewOrder down to viewOrderBase
-- 4.5.0	- 2017/01/26 - BSB/JAE	- Add driver terminal
-- 4.5.11	- 2017/02/08 - JAE	- Add Contract info
-- 4.5.13	- 2017/02/23 - JAE	- Add Consignee
-- 4.6.8	- 2017.05.07 - KDA	- use viewOrderTransferAll instead of embedded queries (very slightly slower here, but leverages logic in one place)
/***************************************/
ALTER VIEW viewOrder AS
SELECT O.*
	-- GENERATED orders that were GAUGED to COMPLETION or SKIPPED will show "Gauger [gauger status]"
	, PrintStatus = CASE WHEN O.StatusID = -10 AND GOS.IsComplete = 1 THEN 'Gauger ' + GOS.Name ELSE OPS.OrderStatus END
FROM (
	SELECT O.*
	, Origin = vO.Name 
	, OriginFull = vO.FullName 
	, OriginState = vO.State 
	, OriginStateAbbrev = vO.StateAbbrev 
	, OriginStation = vO.Station
	, OriginCounty = vO.County 
	, OriginDrivingDirections = vO.DrivingDirections	-- 4.1.3.3
	, vO.LeaseName
	, vO.LeaseNum
	, OriginTerminal = vO.Terminal
	, OriginRegion = vO.Region
	, OriginCountryID = vO.CountryID
	, OriginCountry = vO.Country
	, OriginCountryShort = vO.CountryShort
	, OriginLegalDescription = vO.LegalDescription 
	, OriginNDIC = vO.NDICFileNum 
	, OriginNDM = vO.NDM 
	, OriginCA = vO.CA 
	, OriginTimeZoneID = vO.TimeZoneID 
	, OriginUseDST = vO.UseDST 
	, Destination = vD.Name 
	, DestinationFull = vD.FullName 
	, DestinationState = vD.State 
	, DestinationStateAbbrev = vD.StateAbbrev 
	, DestinationDrivingDirections = vD.DrivingDirections	-- 4.1.3.3
	, DestinationTypeID = vD.ID
	, DestinationStation = vD.Station -- 3.13.8
	, vD.DestinationType
	, DestStation = vD.Station 
	, DestTimeZoneID = vD.TimeZoneID 
	, DestUseDST = vD.UseDST 
	, DestTerminal = vD.Terminal
	, DestRegion = vD.Region -- 3.10.13
	, DestCountryID = vD.CountryID
	, DestCountry = vD.Country
	, DestCountryShort = vD.CountryShort
	, Customer = C.Name 
	, Consignee = CO.Name
	, Carrier = CA.Name 
	, CarrierType = CT.Name 
	, OS.OrderStatus
	, OS.StatusNum
	, Driver = D.FullName 
	, DriverFirst = D.FirstName 
	, DriverLast = D.LastName 
	, DriverRegionID = D.RegionID  -- 3.10.10
	, DriverTerminalID = D.TerminalID
	, DriverTerminal = D.Terminal
	, Truck = TRU.FullName 
	, Trailer = TR1.FullName 
	, TrailerTerminalID = TR1.TerminalID
	, TrailerTerminal = TR1.Terminal
	, Trailer2 = TR2.FullName 
	, Trailer2TerminalID = TR2.TerminalID
	, Trailer2Terminal = TR2.Terminal
	, P.PriorityNum
	, TicketType = TT.Name 
	, DestTicketTypeID = vD.TicketTypeID 
	, DestTicketType = vD.TicketType 
	, Operator = OP.Name 
	, Producer = PR.Name 
	, Pumper = PU.FullName 
	, DriverNumber = D.IDNumber
	, CarrierNumber = CA.IDNumber
	, CarrierTypeID = CA.CarrierTypeID
	, TruckNumber = TRU.IDNumber
	, TruckType = TRU.TruckType 
	, TrailerNumber = TR1.IDNumber 
	, Trailer2Number = TR2.IDNumber 
	, Product = PRO.Name
	, ProductShort = PRO.ShortName 
	, PRO.ProductGroup
	, OriginUOM = OUom.Name 
	, OriginUomShort = OUom.Abbrev 
	, OriginTankID_Text = coalesce(ORT.TankNum, O.OriginTankNum, '?')
	, DestUOM = DUom.Name 
	, DestUomShort = DUom.Abbrev 
	, Active = CAST((CASE WHEN O.DeleteDateUTC IS NULL THEN 1 ELSE 0 END) AS BIT) 
	, IsDeleted = CAST((CASE WHEN O.DeleteDateUTC IS NOT NULL THEN 1 ELSE 0 END) AS BIT) 
	, PickupPrintStatus = PPS.Name 
	, PickupCompleted = PPS.IsCompleted 
	, DeliverPrintStatus = DPS.Name 
	, DeliverCompleted = DPS.IsCompleted 
	, PrintStatusID = CASE WHEN O.StatusID = -9 THEN ISNULL(-GAO.StatusID, O.StatusID)
						   WHEN O.StatusID = 8 AND PPS.IsCompleted = 0 THEN 7
						   WHEN O.StatusID = 3 AND DPS.IsCompleted = 0 THEN 8
						ELSE O.StatusID END 
	, OriginTankText = CASE WHEN O.OriginTankID IS NULL OR ORT.TankNum = '*' THEN O.OriginTankNum ELSE ORT.TankNum END
	, OriginWaitNum = OWR.Num
	, OriginWaitDesc = OWR.Description
	, OriginWaitNumDesc = OWR.NumDesc
	, DestWaitNum = DWR.Num
	, DestWaitDesc = DWR.Description
	, DestWaitNumDesc = DWR.NumDesc
	, RejectNum = ORR.Num
	, RejectDesc = ORR.Description
	, RejectNumDesc = ORR.NumDesc
	, TotalMinutes = ISNULL(OriginMinutes, 0) + ISNULL(DestMinutes, 0)
	-- TRANSFER FIELDS
	, OTR.*
	, IsTransfer = CAST((CASE WHEN OTR.OTAOrderID IS NOT NULL THEN 1 ELSE 0 END) AS BIT)	
	-- 
	, GaugerStatusID = GAO.StatusID
	, GaugerID = GAO.GaugerID	
	--
	, ContractNumber = CN.ContractNumber
	, ContractPrintDescription = CN.PrintDescription
	, ContractNotes = CN.Notes
	, ContractUomID = CN.UomID
	, ContractVolume = CN.Units
	FROM viewOrderBase O
	JOIN tblOrderStatus OS ON OS.ID = O.StatusID
	LEFT JOIN viewOrderTransferAll OTR ON OTR.OTAOrderID = O.ID
	LEFT JOIN viewOrigin vO ON vO.ID = O.OriginID
	LEFT JOIN viewDestination vD ON vD.ID = O.DestinationID
	LEFT JOIN tblCustomer C ON C.ID = O.CustomerID
	LEFT JOIN tblCarrier CA ON CA.ID = O.CarrierID
	LEFT JOIN tblCarrierType CT ON CT.ID = CA.CarrierTypeID
	LEFT JOIN tblConsignee CO ON CO.ID = O.ConsigneeID
	LEFT JOIN viewDriverBase D ON D.ID = O.DriverID
	LEFT JOIN viewDriver vDDR ON vDDR.ID = O.DriverID
	LEFT JOIN viewTruck TRU ON TRU.ID = O.TruckID
	LEFT JOIN viewTrailer TR1 ON TR1.ID = O.TrailerID
	LEFT JOIN viewTrailer TR2 ON TR2.ID = O.Trailer2ID
	LEFT JOIN tblPriority P ON P.ID = O.PriorityID
	LEFT JOIN tblTicketType TT ON TT.ID = O.TicketTypeID
	LEFT JOIN tblOriginTank ORT ON ORT.ID = O.OriginTankID
	LEFT JOIN tblOperator OP ON OP.ID = O.OperatorID
	LEFT JOIN viewPumper PU ON PU.ID = O.PumperID
	LEFT JOIN tblProducer PR ON PR.ID = O.ProducerID
	LEFT JOIN viewProduct PRO ON PRO.ID = O.ProductID
	LEFT JOIN tblUom OUom ON OUom.ID = O.OriginUomID
	LEFT JOIN tblUom DUom ON DUom.ID = O.DestUomID
	LEFT JOIN tblPrintStatus PPS ON PPS.ID = O.PickupPrintStatusID
	LEFT JOIN tblPrintStatus DPS ON DPS.ID = O.DeliverPrintStatusID
	LEFT JOIN viewOriginWaitReason OWR ON OWR.ID = O.OriginWaitReasonID
	LEFT JOIN viewDestinationWaitReason DWR ON DWR.ID = O.DestWaitReasonID
	LEFT JOIN viewOrderRejectReason ORR ON ORR.ID = O.RejectReasonID	
	LEFT JOIN tblGaugerOrder GAO ON GAO.OrderID = O.ID
	LEFT JOIN tblContract CN ON CN.ID = O.ContractID
) O
LEFT JOIN tblOrderStatus OPS ON OPS.ID = O.PrintStatusID
LEFT JOIN tblGaugerOrderStatus GOS ON GOS.ID = O.GaugerStatusID

GO

-- run a few refresh views for vieworder changes to propagate
exec sp_refreshview viewOrderLocalDates
exec sp_refreshview viewOrder_AllTickets
exec sp_refreshview viewOrder_OrderTicket_Full
exec sp_refreshview viewReportCenter_Orders_NoGPS
go
exec _spRefreshAllViews
exec _spRefreshAllViews
go

exec _spDropFunction 'fnBatchOriginDriverID'
go
/***********************************/
-- Created: 4.6.8 - 2017.05.07 - Kevin Alons
-- Purpose: return a formatted BatchOriginDriverID value (used in multiple places), using a function consolidates the logic in 1 place
-- Changes:
/***********************************/
CREATE FUNCTION fnBatchOriginDriverID(@batchID int, @originDriverID int) RETURNS varchar(100) AS
BEGIN
	RETURN (rtrim(@batchID) + '.' + rtrim(@originDriverID))
END
GO
GRANT EXECUTE ON fnBatchOriginDriverID TO dispatchcrude_iis_acct
GO

/***********************************/
-- Created: 4.6.2 - 2017.04.20 - Kevin Alons
-- Purpose: return DriverSettlementBatch records split into separate records per included Driver
-- Changes:
--  4.6.8	- 2017.05.04 - KDA	- use fnBatchOriginDriverID()	
--								- add OriginDriverID column
/***********************************/
ALTER VIEW viewDriverSettlementBatch_PerDriver AS
	SELECT BatchID = SB.ID, SB.*, OS.*
	FROM tblDriverSettlementBatch SB
	CROSS APPLY (
		SELECT BatchOriginDriverID, OriginDriverID, BatchOriginDriver
			, TotalAmount = sum(TotalAmount), OrderCount = count(*)
		FROM (
			SELECT OS.BatchID
				, BatchOriginDriverID = dbo.fnBatchOriginDriverID(OS.BatchID, OT.OriginDriverID)
				, OriginDriverID, BatchOriginDriver = OT.OriginDriver
				, OS.TotalAmount
			FROM tblOrder O
			JOIN viewOrderTransferAll OT ON OT.OTAOrderID = O.ID
			JOIN viewOrderSettlementDriver OS ON OS.OrderID = O.ID
			WHERE OS.BatchID IS NOT NULL AND O.DeleteDateUTC IS NULL 
		) X
		WHERE X.BatchID = SB.ID
		GROUP BY BatchOriginDriverID, OriginDriverID, BatchOriginDriver
	) OS

GO

/***********************************/
-- Created: 4.1.0 - 2016/08/15 - Kevin Alons
-- Purpose: return BASE Order details + Driver FINANCIAL INFO into a single view
-- Changes:
--  4.1.0.2 - 2016.08.28 - KDA	- remove redundant calculation of ORR.RerouteCount
--								- remove redundant tblOrigin JOIN (for H2S which is already available in viewOrder)
--  4.1.8.2 - 2016.09.21 - JAE	- Check Final Actual miles (null actual miles ok as long as override)
--  4.1.23	- 2016.10.17 - JAE	- Add weight units for settlement
--  4.4.1	- 2016/11/04 - JAE	- Add Destination Chainup
--  4.6.2	- 2017.04.21 - KDA	- Add InvoicePeriodEnd, InvoiceNotes, BatchNum, BatchDate fields
--								- Add BatchOriginDriverID & BatchOriginDriver fields (for Driver Settlement Statement logic)
--	4.6.8	- 2017.05.09 - KDA	- retrieve BatchOriginDriverID using fnBatchOriginDriverID() function
/***********************************/
ALTER VIEW viewOrder_Financial_Base_Driver AS 
	SELECT *
		, HasError = cast(CASE WHEN ErrorFieldCSV IS NOT NULL THEN 1 ELSE 0 END as bit)
	FROM (
		SELECT *
			, OrderID = ID
			, ErrorFieldCSV = nullif(substring(CASE WHEN Approved = 0 THEN ',Approved' ELSE '' END
				+ CASE WHEN InvoiceLoadAmount IS NULL THEN ',InvoiceLoadAmount,InvoiceRouteRate,InvoiceRateSheet' ELSE '' END
				+ CASE WHEN FinalOriginChainup = 1 AND InvoiceOriginChainupAmount IS NULL THEN ',InvoiceOriginChainupAmount' ELSE '' END
				+ CASE WHEN FinalDestChainup = 1 AND InvoiceDestChainupAmount IS NULL THEN ',InvoiceDestChainupAmount' ELSE '' END
				+ CASE WHEN FinalRerouteCount > 0 AND InvoiceRerouteAmount IS NULL THEN ',InvoiceRerouteAmount' ELSE '' END
				+ CASE WHEN FinalH2S = 1 AND InvoiceH2SAmount IS NULL THEN ',InvoiceH2SAmount' ELSE '' END
				+ CASE WHEN TicketCount > 1 AND InvoiceSplitLoadAmount IS NULL THEN ',InvoiceSplitLoadAmount' ELSE '' END
				+ CASE WHEN InvoiceOriginWaitBillableMinutes > 0 AND InvoiceOriginWaitAmount IS NULL THEN ',InvoiceOriginWaitAmount' ELSE '' END
				+ CASE WHEN InvoiceDestinationWaitBillableMinutes > 0 AND InvoiceDestinationWaitAmount IS NULL THEN ',InvoiceDestinationWaitAmount' ELSE '' END
				+ CASE WHEN Rejected = 1 AND InvoiceOrderRejectAmount IS NULL THEN ',InvoiceOrderRejectAmount' ELSE '' END
				+ CASE WHEN InvoiceMinSettlementUnitsID IS NULL THEN ',InvoiceMinSettlementUnits' ELSE '' END
				+ CASE WHEN InvoiceSettlementFactorID IS NULL THEN ',InvoiceSettlementFactor' ELSE '' END
				+ CASE WHEN Rejected = 0 AND InvoiceSettlementFactorID IS NULL THEN ',InvoiceSettlementFactor' ELSE '' END
				+ CASE WHEN Rejected = 0 AND InvoiceSettlementFactorID IS NOT NULL AND dbo.fnOrderSettlementUnits(InvoiceSettlementFactorID, OriginGrossUnits, OriginNetUnits, OriginGrossStdUnits, DestGrossUnits, DestNetUnits, OriginWeightNetUnits, DestWeightNetUnits) IS NULL THEN ','+OrderUnitsField ELSE '' END
				+ CASE WHEN InvoiceWaitFeeParameterID IS NULL THEN ',InvoiceOriginWaitBillableMinutes,InvoiceDestinationWaitBillableMinutes' ELSE '' END
				+ CASE WHEN isnull(FinalActualMiles, 0) = 0 THEN ',ActualMiles' ELSE '' END, 2, 100000), '')
		FROM (
			SELECT O.* 
				, DriverGroup = DG.Name
				, InvoiceRatesAppliedDateUTC = OS.CreateDateUTC
				, OS.BatchID
				, Approved = cast(ISNULL(OA.Approved, 0) as bit)
				, FinalActualMiles = ISNULL(OA.OverrideActualMiles, O.ActualMiles)
				, FinalOriginMinutes = ISNULL(OA.OverrideOriginMinutes, O.OriginMinutes)
				, FinalDestMinutes = ISNULL(OA.OverrideDestMinutes, O.DestMinutes)
				, FinalH2S = cast(CASE WHEN isnull(OA.OverrideH2S, 0) = 1 THEN 0 ELSE O.H2S END as bit)
				, FinalOriginChainup = cast(CASE WHEN isnull(OA.OverrideOriginChainup, 0) = 1 THEN 0 ELSE O.OriginChainup END as bit)
				, FinalDestChainup = cast(CASE WHEN isnull(OA.OverrideDestChainup, 0) = 1 THEN 0 ELSE O.DestChainup END as bit)
				, FinalRerouteCount = CASE WHEN isnull(OA.OverrideReroute, 0) = 1 THEN 0 ELSE isnull(O.RerouteCount, 0) END
				, FC.OrderUnitsField
				, InvoiceOriginWaitBillableMinutes = OS.OriginWaitBillableMinutes
				, InvoiceDestinationWaitBillableMinutes = OS.DestinationWaitBillableMinutes
				, InvoiceTotalWaitBillableMinutes = isnull(OS.OriginWaitBillableMinutes, 0) + ISNULL(OS.DestinationWaitBillableMinutes, 0) 
				, InvoiceWaitFeeParameterID = WaitFeeParameterID
				, InvoiceWaitFeeSubUnit = WaitFeeSubUnit
				, InvoiceWaitFeeRoundingType = WaitFeeRoundingType
				, InvoiceOriginWaitRate = OS.OriginWaitRate 
				, InvoiceOriginWaitAmount = OS.OriginWaitAmount 		
				, InvoiceDestinationWaitRate = OS.DestinationWaitRate 
				, InvoiceDestinationWaitAmount = OS.DestinationWaitAmount 
				, InvoiceTotalWaitAmount = OS.TotalWaitAmount
				, InvoiceOrderRejectRate = OS.OrderRejectRate	
				, InvoiceOrderRejectRateType = OS.OrderRejectRateType  
				, InvoiceOrderRejectAmount = OS.OrderRejectAmount  
				, InvoiceOriginChainupRate = OS.OriginChainupRate
				, InvoiceOriginChainupRateType = OS.OriginChainupRateType
				, InvoiceOriginChainupAmount = OS.OriginChainupAmount 
				, InvoiceDestChainupRate = OS.DestChainupRate
				, InvoiceDestChainupRateType = OS.DestChainupRateType
				, InvoiceDestChainupAmount = OS.DestChainupAmount 
				, InvoiceRerouteRate = OS.RerouteRate
				, InvoiceRerouteRateType = OS.RerouteRateType
				, InvoiceRerouteAmount = OS.RerouteAmount 
				, InvoiceH2SRate = OS.H2SRate
				, InvoiceH2SRateType = OS.H2SRateType
				, InvoiceH2SAmount = OS.H2SAmount
				, InvoiceSplitLoadRate = OS.SplitLoadRate
				, InvoiceSplitLoadRateType = OS.SplitLoadRateType
				, InvoiceSplitLoadAmount = OS.SplitLoadAmount
				, InvoiceOtherAmount = OS.OtherAmount
				, InvoiceTaxRate = OS.OriginTaxRate
				, InvoiceSettlementUom = OS.SettlementUom 
				, InvoiceSettlementUomShort = OS.SettlementUomShort 
				, InvoiceCarrierSettlementFactorID = OS.CarrierSettlementFactorID 
				, InvoiceSettlementFactorID = OS.SettlementFactorID 
				, InvoiceSettlementFactor = OS.SettlementFactor 
				, InvoiceMinSettlementUnitsID = OS.MinSettlementUnitsID 
				, InvoiceMinSettlementUnits = OS.MinSettlementUnits
				, InvoiceUnits = OS.SettlementUnits
				, InvoiceRouteRate = OS.RouteRate
				, InvoiceRouteRateType = OS.RouteRateType
				, InvoiceRateSheetRate = OS.RateSheetRate
				, InvoiceRateSheetRateType = OS.RateSheetRateType
				, InvoiceFuelSurchargeRate = OS.FuelSurchargeRate
				, InvoiceFuelSurchargeAmount = OS.FuelSurchargeAmount
				, InvoiceLoadAmount = OS.LoadAmount
				, InvoiceTotalAmount = OS.TotalAmount
				, InvoicePeriodEnd = OS.PeriodEndDate
				, InvoiceNotes = OS.Notes
				, OS.BatchNum 
				, OS.BatchDate
				/* used for Driver Settlement Statement logic */
				, BatchOriginDriverID = dbo.fnBatchOriginDriverID(OS.BatchID, O.OriginDriverID)
				, BatchOriginDriver = O.OriginDriver
			FROM dbo.viewOrder O
			LEFT JOIN tblDriverGroup DG ON DG.ID = O.DriverGroupID
			LEFT JOIN viewOrderSettlementDriver OS ON OS.OrderID = O.ID
			LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
			LEFT JOIN tblSettlementFactor FC ON FC.ID = OS.SettlementFactorID
			WHERE O.StatusID NOT IN (-10) -- don't include "Generated" orders (only Assigned+)
		) X
	) X1

GO

exec _spDropProcedure 'spDriverSettlementStatementEmail'
go
/***********************************/
-- Created: 4.6.8 - 2017.05.07 - Kevin Alons
-- Purpose: generate all tblDriverSettlementStatementEmail records for the specified Driver Settlement Batch
-- Changes:
/***********************************/
CREATE PROCEDURE spDriverSettlementStatementEmail
(
  @DefinitionID int
, @BatchID int
, @UserName varchar(100)
, @QueueEmailSend bit = 0
, @PdfExportID int = null
) AS
BEGIN
	INSERT INTO tblDriverSettlementStatementEmail 
	(
		DriverSettlementStatementEmailDefinitionID
		, PdfExportID
		, BatchID
		, DriverID
		, EmailAddress
		, CCEmailAddress
		, QueueEmailSend
		, EmailedByUser
		, CreatedByUser
	)
		SELECT DSSD.ID
			, isnull(@PdfExportID, DSSD.PdfExportID)
			, @BatchID
			, DSBD.OriginDriverID
			, isnull(D.Email, DSSD.MissingEmailAddress)
			, DSSD.CCEmailAddress
			, @QueueEmailSend
			, CASE WHEN @QueueEmailSend = 1 THEN @UserName ELSE NULL END
			, @UserName
		FROM tblDriverSettlementStatementEmailDefinition DSSD
		JOIN viewDriverSettlementBatch_PerDriver DSBD ON DSBD.BatchID = @BatchID
		JOIN tblDriver D ON D.ID = DSBD.OriginDriverID
		/* prevent duplicate records, just skip if already created */
		LEFT JOIN tblDriverSettlementStatementEmail DSSE ON DSSE.BatchID = @BatchID
		WHERE DSSD.ID = @DefinitionID
			AND DSSE.BatchID IS NULL
END
GO
GRANT EXECUTE ON spDriverSettlementStatementEmail TO dispatchcrude_iis_acct
GO


EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRebuildAllObjects
GO
EXEC _spRebuildAllObjects
GO
EXEC _spRecompileAllStoredProcedures
GO

exec _spRefreshAllViews
exec _spRefreshAllViews
exec _spRebuildAllObjects
go

commit
set noexec off