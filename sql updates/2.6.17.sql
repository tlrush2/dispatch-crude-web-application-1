/*  
	-- add basics for automated (and consistent) exporting of orders for Customers
*/ 
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.6.16'
SELECT  @NewVersion = '2.6.17'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

CREATE TABLE tblOrderExportFinalCustomer
(
  ID int identity(1, 1) not null CONSTRAINT PK_OrderExportFinalCustomer PRIMARY KEY
, OrderID int NOT NULL
, OrderLastChangeDateUTC datetime NULL 
, ExportDateUTC datetime NOT NULL CONSTRAINT DF_OrderExportFinalCustomer_ExportDate DEFAULT (getutcdate())
, ExportedByUser varchar(100) NULL
)
GO
CREATE UNIQUE INDEX udxOrderExportFinalCustomer ON tblOrderExportFinalCustomer (OrderID, OrderLastChangeDateUTC)
GO

/*****************************************************************************************/
-- Author: Kevin Alons
-- Date Created: 27 Feb 2014
-- Purpose: generic ID int table type used to pass a variable # of ids to a function/proc
/*****************************************************************************************/
CREATE TYPE IDTABLE AS TABLE 
(
  ID int not null
, PRIMARY KEY (ID)
)
GO

/*****************************************************************************************/
-- Author: Kevin Alons
-- Date Created: 27 Feb 2014
-- Purpose: mark the orders specified in the @orderIDs IDTABLE parameter as exported FINAL
/*****************************************************************************************/
CREATE PROCEDURE spMarkOrderExportFinalCustomer @orderIDs IDTABLE READONLY, @exportedByUser varchar(100)
AS BEGIN
    SET NOCOUNT ON
    
    INSERT INTO dbo.tblOrderExportFinalCustomer (OrderID, OrderLastChangeDateUTC, ExportedByUser)
		SELECT IDs.ID, O.LastChangeDateUTC, @exportedByUser
		FROM @orderIDs IDs
		JOIN tblOrder O ON O.ID = IDs.ID
END

GO
GRANT EXECUTE ON spMarkOrderExportFinalCustomer TO dispatchcrude_iis_acct
GO

/**************************************************************/
-- Author: Kevin Alons
-- Date Created: 27 Feb 2014
-- Purpose: return all orderIDs not yet final exported for a Customer
/**************************************************************/
CREATE VIEW viewOrderCustomerFinalExportPending AS
	SELECT DISTINCT O.ID
		, O.CustomerID
		, CASE WHEN OEFC.ID IS NULL THEN 1 ELSE 0 END AS IsNew
		, CASE WHEN OEFC.ID IS NOT NULL THEN 0 ELSE 1 END AS IsChanged
	FROM tblOrder O
	LEFT JOIN tblOrderExportFinalCustomer OEFC ON OEFC.OrderID = O.ID 
	WHERE (OEFC.ID IS NULL 
			OR isnull(OEFC.OrderLastChangeDateUTC, '1/1/1900') <> isnull(O.LastChangeDateUTC, '1/1/1900'))
	  AND O.StatusID IN (4) -- AUDITED status only
	  AND O.DeleteDateUTC IS NULL

GO
GRANT SELECT ON viewOrderCustomerFinalExportPending to dispatchcrude_iis_acct
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblCustomerDestinationCode]') AND type in (N'U'))
CREATE TABLE [dbo].[tblCustomerDestinationCode](
	[CustomerID] [int] NOT NULL,
	[DestinationID] [int] NOT NULL,
	[Code] [varchar](25) NOT NULL,
 CONSTRAINT [PK_tblCustomerDestinationCode] PRIMARY KEY CLUSTERED 
(
	[CustomerID] ASC,
	[DestinationID] ASC,
	[Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblCustomerDestinationCode TO dispatchcrude_iis_acct
GO

/****** Object:  UserDefinedFunction [dbo].[fnDateMMddYYYY]    Script Date: 02/27/2014 22:26:18 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fnDateMMddYYYY]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fnDateMMddYYYY]
GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 6 Feb 2012
-- Description:	return the provided date value in MM/dd/yyyy format
-- =============================================
CREATE FUNCTION [dbo].[fnDateMMddYYYY]
(
  @value datetime
)
RETURNS varchar(25)
AS
BEGIN
	DECLARE @ret varchar(25)

	SELECT @ret = right('0' +  ltrim(MONTH(@value)), 2) 
		+ '/' + right('0' +  ltrim(DAY(@value)), 2) 
		+ '/' + cast(year(@value) as varchar(4))

	RETURN @ret
END

GO
GRANT EXECUTE ON fnDateMMddYYYY TO dispatchcrude_iis_acct
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fnTimeOnly]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fnTimeOnly]
GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 26 Jan 2014
-- Description:	strip the date portion of a datetime value, return as HH:mm string format
-- =============================================
CREATE FUNCTION [dbo].[fnTimeOnly]
(
  @val datetime
)
RETURNS char(5)
AS
BEGIN
	DECLARE @ret char(5)
	select @ret = left(convert(TIME, @val, 108), 5)

	RETURN @ret
END
GO

IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[viewSunocoSundex]'))
DROP VIEW [dbo].[viewSunocoSundex]
GO

/*****************************************************************************************/
-- Author: Kevin Alons
-- Date Created: 27 Feb 2014
-- Purpose: return export final orders in the SunocoSundex
-- TODO: this has some hardcoded values for the SUNOCO customer (shipper), need to make dynamic
/*****************************************************************************************/
CREATE VIEW [dbo].[viewSunocoSundex] AS
SELECT
	O.ID AS _ID
	, O.CustomerID AS _CustomerID
	, O.OrderNum AS _OrderNum
	, CASE WHEN EP.IsNew = 1 THEN 'A' ELSE 'C' END AS Request_Code
	, '0007020233' AS Company_Code
	, UPPER(left(O.TicketType, 1)) AS Ticket_Type
	, 'BATA' AS Ticket_Source_Code
	, T.CarrierTicketNum AS Ticket_Number
	, dbo.fnDateMMddYYYY(O.OrderDate) AS Ticket_Date
	, '' AS SXL_Property_Code
	, O.OriginStation AS TP_Property_Code
	, O.Origin AS Lease_Company_Name
	, CDC.Code AS Destination
	, T.TankNum AS Tank_Meter_Number
	, dbo.fnDateMMddYYYY(O.OriginArriveTime) AS Open_Date
	, dbo.fnTimeOnly(O.OriginArriveTime) AS Open_Time
	, dbo.fnDateMMddYYYY(O.OriginDepartTime) AS Close_Date
	, dbo.fnTimeOnly(O.OriginDepartTime) AS Close_Time
	, cast(ROUND(O.OriginGrossUnits, 2) as decimal(18, 2)) AS Estimated_Volume
	, cast(ROUND(O.OriginGrossUnits, 2) as decimal(18, 2)) AS Gross_Volume
	, cast(ROUND(O.OriginNetUnits, 2) as decimal(18, 2)) AS Net_Volume
	, T.ProductObsGravity AS Observed_Gravity
	, T.ProductObsTemp AS Observed_Temperature
	, T.ProductBSW AS Observed_BSW
	, 0 AS Corrected_Gravity_API
	, 'Sonoco Logistics' AS Purchaser
	, T.OpeningGaugeFeet AS First_Reading_Gauge_Ft
	, T.OpeningGaugeInch AS First_Reading_Gauge_In
	, T.OpeningGaugeQ AS First_Reading_Gauge_Nu
	, 4 AS First_Reading_Gauge_De
	, T.ProductHighTemp AS First_Temperature
	, T.ClosingGaugeFeet AS First_Bottom_Ft
	, T.ClosingGaugeInch AS First_Bottom_In
	, T.ClosingGaugeQ AS First_Bottom_Nu
	, 4 AS First_Bottom_De
	, 0 AS Second_Reading_Gauge_Ft
	, 0 AS Second_Reading_Gauge_In
	, 0 AS Second_Reading_Gauge_Nu
	, 0 AS Second_Reading_Gauge_De
	, T.ProductLowTemp AS Second_Temperature
	, 0 AS Second_Bottom_Ft
	, 0 AS Second_Bottom_In
	, 0 AS Second_Bottom_Nu
	, 4 AS Second_Bottom_De
	, '' AS Shrinkage_Incrustation_Factor
	, 0 AS First_Reading_Meter
	, 0 AS Second_Reading_Meter
	, 0 AS Meter_Factor
	, '' AS Temp_Comp_Meter
	, '' AS Avg_Line_Temp
	, '' AS Truck_ID
	, '' AS Trailer_ID
	, '' AS Driver_ID
	, '' AS Miles
	, '' AS CountyState
	, '' AS Invoice_Number
	, '' AS Invoice_Date
	, '' AS Remarks
	, '' AS API_Compliant_Chapter
	, 'Y' AS Use_SXL_Calculation
	, T.SealOn AS Seal_On
	, T.SealOff AS Seal_Off
	, CASE WHEN T.Rejected = 1 THEN 'RF' ELSE '' END AS Ticket_Exclusion_Cd
	, '9999999999' AS Confirmation_Number
	, CASE WHEN (SELECT COUNT(*) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND DeleteDateUTC IS NULL) > 1 THEN 'Y' ELSE 'N' END AS Split_Flag
	, (SELECT min(CarrierTicketNum) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND DeleteDateUTC IS NULL AND OT.CarrierTicketNum <> T.CarrierTicketNum) AS Paired_Ticket_Number
	, 'N' AS Bobtail_Flag
	, '' AS Ticket_Extra_Info_Flag
FROM viewOrderLocalDates O
JOIN dbo.viewOrderCustomerFinalExportPending EP ON EP.ID = O.ID
LEFT JOIN viewOrderTicket T ON T.OrderID = O.ID AND T.DeleteDateUTC IS NULL
LEFT JOIN dbo.tblCustomerDestinationCode CDC ON CDC.DestinationID = O.DestinationID

GO
GRANT SELECT ON viewSunocoSundex TO dispatchcrude_iis_acct
GO

/*****************************************************************************************/
-- Author: Kevin Alons
-- Date Created: 27 Feb 2014
-- Purpose: with the provided parameters, export the data and mark done (if doing finalExport)
/*****************************************************************************************/
CREATE PROCEDURE spOrderExportFinalCustomer
( 
  @customerID int
, @exportedByUser varchar(100) = NULL -- will defaul to SUSER_NAME() -- default value for now
, @exportFormat varchar(100) = 'SUNOCO_SUNDEX' -- default value for now
, @finalExport bit = 1 -- defualt to TRUE
) 
AS BEGIN
	SET NOCOUNT ON
	-- default to the current user if not supplied
	IF @exportedByUser IS NULL SET @exportedByUser = SUSER_NAME()
	
	-- retrieve the order records to export
	DECLARE @orderIDs IDTABLE
	INSERT INTO @orderIDs (ID)
	SELECT ID
	FROM dbo.viewOrderCustomerFinalExportPending 
	WHERE CustomerID = @customerID 

	BEGIN TRAN exportCustomer
	
	-- export the orders in the specified format
	IF (@exportFormat = 'SUNOCO_SUNDEX') BEGIN
		SELECT E.* 
		INTO #output
		FROM dbo.viewSunocoSundex E
		JOIN @orderIDs IDs ON IDs.ID = E._ID
		WHERE E._CustomerID = @customerID
		ORDER BY _OrderNum
	END

	-- mark the orders as exported FINAL (for a Customer)
	IF (@finalExport = 1) BEGIN
		EXEC spMarkOrderExportFinalCustomer @orderIDs, @exportedByUser
	END
	
	COMMIT TRAN exportCustomer

	-- return the data to 
	SELECT * FROM #output
		
END

GO
GRANT EXECUTE ON spOrderExportFinalCustomer TO dispatchcrude_iis_acct
GO

COMMIT
SET NOEXEC OFF