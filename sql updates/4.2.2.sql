SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.2.1'
SELECT  @NewVersion = '4.2.2'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1873 - Enforce order sequencing'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


--select * from tblCarrierRuleType
INSERT INTO tblCarrierRuleType
VALUES 
(21, 'Mobile Order Sequencing', 6, 1, 'How to enforce order selection/priority for drivers')

GO

--select * from tblCarrierRuleTypeDropDownValues
INSERT INTO tblCarrierRuleTypeDropDownValues
VALUES 
(21, 'Any', 1, 1, 0),
(21, 'In Order - All Visible', 2, 0, 1),
(21, 'In Order - Single Visible', 3, 0, 2)

GO

INSERT INTO tblCarrierRuleTypeDropDownValues
VALUES 
(21, 'In Order - Priority 3 Driver Choice', 4, 0, 3)

GO

COMMIT
SET NOEXEC OFF