/* 
	add tblDriver_Sync.MobileAppVersion
	add tblPrintStatus table
	add tblOrder.PickupPrintStatusID column
	add tblOrder.DeliverPrintStatusID column
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.4.8'
SELECT  @NewVersion = '2.4.9'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

ALTER TABLE tblDriver_Sync Add MobileAppVersion varchar(25) NULL
GO

/***********************************/
-- Date Created: 26 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Drivers table records with "friendly" translated values included
/***********************************/
ALTER VIEW [dbo].[viewDriver] AS
SELECT *
	, CASE WHEN Active = 1 THEN '' ELSE 'Deleted: ' END + FullName AS FullNameD
FROM (
	SELECT D.*
	, cast(CASE WHEN isnull(C.DeleteDateUTC, D.DeleteDateUTC) IS NULL THEN 1 ELSE 0 END as bit) AS Active
	, D.FirstName + ' ' + D.LastName As FullName
	, D.LastName + ', ' + D.FirstName AS FullNameLF
	, isnull(CT.Name, 'Unknown') AS CarrierType
	, C.Name AS Carrier
	, S.Abbreviation AS StateAbbrev 
	, T.FullName AS Truck
	, T1.FullName AS Trailer
	, T2.FullName AS Trailer2
	, R.Name AS Region
	, DS.MobileAppVersion
	FROM dbo.tblDriver D 
	JOIN dbo.tblCarrier C ON C.ID = D.CarrierID 
	LEFT JOIN dbo.tblDriver_Sync DS ON DS.DriverID = D.ID
	LEFT JOIN dbo.tblCarrierType CT ON CT.ID = C.CarrierTypeID
	LEFT JOIN tblState S ON S.ID = D.StateID
	LEFT JOIN viewTruck T ON T.ID = D.TruckID
	LEFT JOIN viewTrailer T1 ON T1.ID = D.TrailerID
	LEFT JOIN viewTrailer T2 ON T2.ID = D.Trailer2ID
	LEFT JOIN tblRegion R ON R.ID = D.RegionID
) X

GO

/*******************************************/
-- Date Created: 22 Apr 2013
-- Author: Kevin Alons
-- Purpose: validate parameters, if valid insert/update the tblDriver_Sync table for the specified DriverID
/*******************************************/
ALTER PROCEDURE [dbo].[spDriver_Sync]
(
  @UserName varchar(100)
, @DriverID int
, @MobileAppVersion varchar(25) = NULL
, @SyncDateUTC datetime = NULL
, @PasswordHash varchar(25) = NULL
, @Valid bit = NULL output
, @Message varchar(255) = NULL output
) AS
BEGIN
	-- if resetting, delete the entire record (it will be recreated below)
	IF (@SyncDateUTC IS NULL)
	BEGIN
		DELETE FROM tblDriver_Sync WHERE DriverID = @DriverID
		-- result of count(*) query will be 1 or 0 (true or false respectively)
		SELECT @Valid = (SELECT COUNT(*) FROM tblDriver WHERE ID = @DriverID)
		IF (@Valid = 0)
			SELECT @Message = 'DriverID was not valid'
	END
	ELSE
	BEGIN
		-- query the ValidatePasswordHash setting parameter
		DECLARE @ValidatePasswordHash bit
		SELECT @ValidatePasswordHash = CASE WHEN Value IN ('1', 'true', 'yes') THEN 1 ELSE 0 END
		FROM tblSetting
		WHERE ID = 13

		-- result of count(*) query will be 1 or 0 (true or false respectively)
		SELECT @Valid = (SELECT count(*) FROM tblDriver_Sync WHERE DriverID = @DriverID 
			AND (@ValidatePasswordHash = 0 OR PasswordHash = @PasswordHash))
		IF (@Valid = 0)
			SELECT @Message = 'PasswordHash was not valid'
	END
	
	IF (@Valid = 1)
	BEGIN
		-- if a sync record already exists, just update the new LastSync value
		UPDATE tblDriver_Sync SET LastSyncUTC = @SyncDateUTC, MobileAppVersion = @MobileAppVersion WHERE DriverID = @DriverID
		-- otherwise insert a new record with a new passwordhash value
		INSERT INTO tblDriver_Sync (DriverID, MobileAppVersion, LastSyncUTC, PasswordHash)
			SELECT @DriverID, @MobileAppVersion, NULL, dbo.fnGeneratePasswordHash()
			FROM tblDriver D
			LEFT JOIN tblDriver_Sync DS ON DS.DriverID = D.ID
			WHERE D.ID = @DriverID AND DS.DriverID IS NULL

		-- return the current "Master" data
		SELECT * FROM dbo.fnDriverMasterData(@Valid, @DriverID, @UserName)
	END
	ELSE
	BEGIN
		SELECT @Valid AS Valid, @UserName AS UserName, 0 AS DriverID, NULL AS DriverName, 0 AS MobilePrint
			, NULL AS LastSyncUTC
			, (SELECT cast(Value as int) FROM tblSetting WHERE ID = 11) AS SyncMinutes
			, (SELECT Value FROM tblSetting WHERE ID = 0) AS SchemaVersion
			, @MobileAppVersion AS MobileAppVersion
			, (SELECT Value FROM tblSetting WHERE ID = 12) AS LatestAppVersion
			, NULL AS PasswordHash
		FROM tblSetting S WHERE S.ID = 0
	END 
END

GO

CREATE TABLE tblPrintStatus
(
  ID int NOT NULL CONSTRAINT PK_PrintStatus PRIMARY KEY
, Name varchar(25) NOT NULL
, IsCompleted bit NOT NULL CONSTRAINT DF_PrintStatus_IsCompleted DEFAULT (0)
)
GO
CREATE UNIQUE INDEX udxPrintStatus_Name ON tblPrintStatus(Name)
GO
GRANT SELECT ON tblPrintStatus to dispatchcrude_iis_acct
GO

INSERT INTO tblPrintStatus (ID, Name, IsCompleted)
	SELECT 0, 'Not Finalized', 0
	UNION SELECT 1, 'Finalized', 0
	UNION SELECT 2, 'Printed', 1
	UNION SELECT 3, 'Handwritten', 1
GO

ALTER TABLE tblOrder ADD PickupPrintStatusID int NULL 
	CONSTRAINT FK_Order_Pickup_PrintStatus FOREIGN KEY REFERENCES tblPrintStatus(ID)
	CONSTRAINT DF_Order_PickupPrintStatus DEFAULT (0)
GO
ALTER TABLE tblOrder ADD DeliverPrintStatusID int NULL 
	CONSTRAINT FK_Order_Deliver_PrintStatus FOREIGN KEY REFERENCES tblPrintStatus(ID)
	CONSTRAINT DF_Order_DeliverPrintStatus DEFAULT (0)
GO
ALTER TABLE tblOrder ADD PickupPrintDateUTC datetime NULL 
GO
ALTER TABLE tblOrder ADD DeliverPrintDateUTC datetime NULL 
GO

UPDATE tblOrder
  SET PickupPrintStatusID = CASE 
			-- picked up/delivered/audited records that were PRINTED on MobileApp = PRINTED
			WHEN StatusID IN (8, 3, 4) AND isnull(FMA.FMA, 0) = 1 AND isnull(HW.HW, 0) = 0 THEN 2
			-- picked up/delivered/audited records that were not from mobile app or were explicitly HANDWRITTEN = HANDWRITTEN
			WHEN StatusID IN (8, 3, 4) AND isnull(FMA.FMA, 0) = 0 OR isnull(HW.HW, 0) = 1 THEN 3
			ELSE 0 END
  , DeliverPrintStatusID = CASE
			-- delivered/audited records that were PRINTED on MobileApp = PRINTED
			WHEN StatusID IN (3, 4) AND isnull(FMA.FMA, 0) = 1 AND isnull(HW.HW, 0) = 0 THEN 2
			-- delivered/audited records that were not from mobile app or were explicitly HANDWRITTEN = HANDWRITTEN
			WHEN StatusID IN (3, 4) AND isnull(FMA.FMA, 0) = 0 OR isnull(HW.HW, 0) = 1 THEN 3
			ELSE 0 END

FROM tblOrder O
LEFT JOIN (SELECT OT.OrderID, sign(COUNT(*)) AS FMA FROM tblOrderTicket OT WHERE OT.FromMobileApp = 1 GROUP BY OT.OrderID) FMA ON FMA.OrderID = O.ID
LEFT JOIN (SELECT OPL.OrderID, SIGN(count(*)) AS HW FROM tblOrderPrintLog OPL WHERE HandWritten = 1 GROUP BY OPL.OrderID) HW ON HW.OrderID = O.ID
GO

/***********************************/
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Order records with "translated friendly" values for FK relationships
/***********************************/
ALTER VIEW [dbo].[viewOrder] AS
SELECT O.*
	, OPS.OrderStatus AS PrintStatus
FROM (
	SELECT O.*
	, dbo.fnDateOnly(dbo.fnUTC_to_Local(O.OriginDepartTimeUTC, vO.TimeZoneID, vO.UseDST)) AS OrderDate
	, vO.Name AS Origin
	, vO.FullName AS OriginFull
	, vO.State AS OriginState
	, vO.StateAbbrev AS OriginStateAbbrev
	, vO.Station AS OriginStation
	, vO.LeaseName
	, vO.LeaseNum
	, vO.TimeZoneID AS OriginTimeZoneID
	, vO.UseDST AS OriginUseDST
	, vO.H2S
	, vD.Name AS Destination
	, vD.FullName AS DestinationFull
	, vD.State AS DestinationState
	, vD.StateAbbrev AS DestinationStateAbbrev
	, vD.DestinationType
	, vD.Station AS DestStation
	, vD.TimeZoneID AS DestTimeZoneID
	, vD.UseDST AS DestUseDST
	, C.Name AS Customer
	, CA.Name AS Carrier
	, CT.Name AS CarrierType
	, OS.OrderStatus
	, OS.StatusNum
	, D.FullName AS Driver
	, D.FirstName AS DriverFirst
	, D.LastName AS DriverLast
	, TRU.FullName AS Truck
	, TR1.FullName AS Trailer
	, TR2.FullName AS Trailer2
	, P.PriorityNum
	, TT.Name AS TicketType
	, vD.TicketTypeID AS DestTicketTypeID
	, vD.TicketType AS DestTicketType
	, OP.Name AS Operator
	, PR.Name AS Producer
	, PU.FullName AS Pumper
	, D.IDNumber AS DriverNumber
	, CA.IDNumber AS CarrierNumber
	, TRU.IDNumber AS TruckNumber
	, TR1.IDNumber AS TrailerNumber
	, TR2.IDNumber AS Trailer2Number
	, PRO.Name as Product
	, PRO.ShortName AS ProductShort
	, OUom.Name AS OriginUOM
	, OUom.Abbrev AS OriginUomShort
	, DUom.Name AS DestUOM
	, DUom.Abbrev AS DestUomShort
	, cast((CASE WHEN O.DeleteDateUTC IS NULL THEN 1 ELSE 0 END) as bit) AS Active
	, cast((CASE WHEN O.DeleteDateUTC IS NOT NULL THEN 1 ELSE 0 END) as bit) AS IsDeleted
	, PPS.Name AS PickupPrintStatus
	, PPS.IsCompleted AS PickupCompleted
	, DPS.Name AS DeliverPrintStatus
	, DPS.IsCompleted AS DeliverCompleted
	, CASE WHEN StatusID = 8 AND PPS.IsCompleted = 0 THEN 7
		   WHEN StatusID = 3 AND DPS.IsCompleted = 0 THEN 8
		   ELSE StatusID END AS PrintStatusID
	FROM dbo.tblOrder O
	LEFT JOIN dbo.viewOrigin vO ON vO.ID = O.OriginID
	LEFT JOIN dbo.viewDestination vD ON vD.ID = O.DestinationID
	LEFT JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
	JOIN dbo.tblOrderStatus AS OS ON OS.ID = O.StatusID
	LEFT JOIN dbo.tblCarrier CA ON CA.ID = O.CarrierID
	LEFT JOIN dbo.tblCarrierType CT ON CT.ID = CA.CarrierTypeID
	LEFT JOIN dbo.viewDriver D ON D.ID = O.DriverID
	LEFT JOIN dbo.viewTruck TRU ON TRU.ID = O.TruckID
	LEFT JOIN dbo.viewTrailer TR1 ON TR1.ID = O.TrailerID
	LEFT JOIN dbo.viewTrailer TR2 ON TR2.ID = O.Trailer2ID
	LEFT JOIN dbo.tblPriority P ON P.ID = O.PriorityID
	LEFT JOIN dbo.tblTicketType TT ON TT.ID = O.TicketTypeID
	LEFT JOIN dbo.tblOperator OP ON OP.ID = O.OperatorID
	LEFT JOIN dbo.viewPumper PU ON PU.ID = O.PumperID
	LEFT JOIN dbo.tblProducer PR ON PR.ID = O.ProducerID
	LEFT JOIN dbo.tblProduct PRO ON PRO.ID = O.ProductID
	LEFT JOIN dbo.tblUom OUom ON OUom.ID = O.OriginUomID
	LEFT JOIN dbo.tblUom DUom ON DUom.ID = O.DestUomID
	LEFT JOIN dbo.tblPrintStatus PPS ON PPS.ID = O.PickupPrintStatusID
	LEFT JOIN dbo.tblPrintStatus DPS ON DPS.ID = O.DeliverPrintStatusID
) O
LEFT JOIN dbo.tblOrderStatus OPS ON OPS.ID = O.PrintStatusID

GO

/*******************************************/
-- Date Created: 31 Aug 2013
-- Author: Kevin Alons
-- Purpose: return driver editable Order data for Driver App sync
/*******************************************/
ALTER FUNCTION [dbo].[fnOrderEdit_DriverApp]( @DriverID int, @LastChangeDateUTC datetime ) RETURNS TABLE AS
RETURN 
	SELECT O.ID
		, O.StatusID
		, O.TruckID
		, O.TrailerID
		, O.Trailer2ID
		, O.OriginBOLNum
		, O.OriginArriveTimeUTC
		, O.OriginDepartTimeUTC
		, O.OriginMinutes
		, O.OriginWaitNotes
		, O.OriginTruckMileage
		, O.OriginGrossUnits
		, O.OriginNetUnits
		, O.ChainUp
		, O.Rejected
		, O.RejectNotes
		, O.OriginTankNum
		, O.DestArriveTimeUTC
		, O.DestDepartTimeUTC
		, O.DestMinutes
		, O.DestWaitNotes
		, O.DestBOLNum
		, O.DestTruckMileage
		, O.DestGrossUnits
		, O.DestNetUnits
		, O.DestProductTemp
		, O.DestProductBSW
		, O.DestProductGravity
		, O.DestOpenMeterUnits
		, O.DestCloseMeterUnits
		, O.CarrierTicketNum
		, O.AcceptLastChangeDateUTC
		, O.PickupLastChangeDateUTC
		, O.DeliverLastChangeDateUTC
		, O.PickupPrintStatusID
		, O.DeliverPrintStatusID
		, O.PickupPrintDateUTC
		, O.DeliverPrintDateUTC
	FROM dbo.tblOrder O
	LEFT JOIN tblOrderDriverAppVirtualDelete ODAVD ON ODAVD.OrderID = O.ID AND ODAVD.DriverID = @DriverID
	WHERE (O.StatusID IN (2, 7, 8) -- Dispatched, Accepted, Picked Up
		-- or status = Delivered and the driver still has access to the record
		OR (O.StatusID = 3 AND O.DestDepartTimeUTC < DATEADD(hour, (SELECT cast(value as int) FROM tblSetting WHERE ID = 10), GETUTCDATE()))
		-- or this potentially was a DriverApp record, that no longer is, so should be included (as deleted)
		OR (ODAVD.ID IS NOT NULL AND ODAVD.VirtualDeleteDateUTC >= @LastChangeDateUTC))
	  AND (O.DriverID = @DriverID OR ODAVD.DriverID = @DriverID)
	  AND (
		@LastChangeDateUTC IS NULL 
		OR O.CreateDateUTC > @LastChangeDateUTC
		OR O.LastChangeDateUTC >= @LastChangeDateUTC
		OR O.DeleteDateUTC >= @LastChangeDateUTC
		OR ODAVD.VirtualDeleteDateUTC >= @LastChangeDateUTC)

GO

/***********************************************************************/
-- Date Created: 18 Apr 2013
-- Author: Kevin Alons
-- Purpose: return the DriverApp.MasterData for a single driver/login
/***********************************************************************/
ALTER FUNCTION [dbo].[fnDriverMasterData](@Valid bit, @DriverID int, @UserName varchar(100)) RETURNS TABLE AS 
RETURN
	SELECT CASE WHEN DS.DriverID IS NULL THEN 0 ELSE @Valid END AS Valid
		, @UserName AS UserName
		, @DriverID AS DriverID
		, D.FullName AS DriverName
		, D.MobilePrint
		, DS.LastSyncUTC
		, cast(SSF.Value as int) AS SyncMinutes
		, SSV.Value AS SchemaVersion
		, LAV.Value AS LatestAppVersion
		, DS.MobileAppVersion
		, DS.PasswordHash
	FROM viewDriver D
	LEFT JOIN tblDriver_Sync DS ON DS.DriverID = D.ID
	JOIN tblSetting SSV ON SSV.ID = 0  -- Schema Version
	JOIN tblSetting SSF ON SSF.ID = 11 -- sync frequency (in minutes)
	JOIN tblSetting LAV ON LAV.ID = 12 -- LatestAppVersion
	WHERE D.ID = @DriverID

GO

COMMIT
SET NOEXEC OFF

EXEC _spRefreshAllViews
GO