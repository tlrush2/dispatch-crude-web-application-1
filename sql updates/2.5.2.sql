/* 
	performance optimization for Converting Uom (no-op when to/from units are the same
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.5.1'
SELECT  @NewVersion = '2.5.2'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details into a single view
/***********************************/
CREATE VIEW [dbo].[viewOrderExportFull_Reroute] AS 
	SELECT OE.* 
		, dbo.fnRerouteDetails(OE.ID, 'PreviousDestinations', '<br/>') AS PreviousDestinations
		, dbo.fnRerouteDetails(OE.ID, 'RerouteUsers', '<br/>') AS RerouteUsers
		, dbo.fnRerouteDetails(OE.ID, 'RerouteDates', '<br/>') AS RerouteDates
		, dbo.fnRerouteDetails(OE.ID, 'RerouteNotes', '<br/>') AS RerouteNotes
	FROM dbo.viewOrderExportFull OE
	WHERE OE.DeleteDateUTC IS NULL	

GO
GRANT SELECT ON viewOrderExportFull_Reroute TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 28 Feb 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc for 3rd parties (Customer/Carrier)
/***********************************/
CREATE PROCEDURE [dbo].[spOrdersBasicExport]
(
  @StartDate datetime
, @EndDate datetime
, @CarrierID int = 0 -- -1 = all carriers
, @CustomerID int = 0 -- -1 = all customers
, @ProducerID int = 0
, @StatusID_CSV varchar(max) = '4'
) AS BEGIN
	SELECT @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	
	SELECT * 
	FROM dbo.viewOrderExportFull_Reroute
	WHERE (@CarrierID=-1 OR @CustomerID=-1 OR CarrierID=@CarrierID OR CustomerID=@CustomerID OR ProducerID=@ProducerID) 
	  AND OrderDate BETWEEN @StartDate AND @EndDate
	  AND DeleteDateUTC IS NULL
	  AND (StatusID IN (SELECT ID FROM dbo.fnSplitCSVIDs(@StatusID_CSV)))
	ORDER BY OrderDate, OrderNum
END

GO

COMMIT
SET NOEXEC OFF