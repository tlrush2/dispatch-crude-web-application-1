-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.11.12.2'
SELECT  @NewVersion = '3.11.13'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-992: Questionnaire / DVIR / JSA'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO



-- 3.11.13 - BB - 2016/01/19 - Answer Types
/*******************************************
 Date Created: 20 Jan 2016
 Author: Ben Bloodworth
 Purpose: Store the possible types of answers to VIR questions
 Changes: 
	2/5/2016 - JAE - expanded into a questionnaire for use with multiple reports
*******************************************/
CREATE TABLE tblQuestionnaireQuestionType
(  
	  ID INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_QuestionnaireQuestionType PRIMARY KEY CLUSTERED
	, QuestionType VARCHAR(25) NOT NULL CONSTRAINT DF_QuestionnaireQuestionType_QuestionType DEFAULT('String')
) 
GO
GRANT SELECT ON tblQuestionnaireQuestionType TO role_iis_acct
GO

SET IDENTITY_INSERT tblQuestionnaireQuestionType ON
GO
INSERT INTO tblQuestionnaireQuestionType (ID, QuestionType)
  SELECT 0, 'Label' -- Text only, no answer
  UNION 
  SELECT 1, 'Boolean'
  UNION 
  SELECT 2, 'Integer'
  UNION 
  SELECT 3, 'Decimal'
  UNION 
  SELECT 4, 'String'
  UNION 
  SELECT 5, 'Drop Down'
  UNION 
  SELECT 6, 'Timestamp'
  UNION
  SELECT 7, 'Blob'  
GO
SET IDENTITY_INSERT tblQuestionnaireQuestionType OFF
GO


-- 3.11.13 - JAE - 2016/04/05 - Template Type (for future use)
/*******************************************
 Date Created: 05 Apr 2016
 Author: Joe Engler
 Purpose: Store the types of questionnaire templates
 Changes: 
*******************************************/
CREATE TABLE tblQuestionnaireTemplateType(
	ID int IDENTITY(10000,1) NOT NULL CONSTRAINT PK_QuestionnaireTemplateType PRIMARY KEY CLUSTERED, 
	Name VARCHAR(50) NOT NULL,
	IsSystem BIT NOT NULL CONSTRAINT DF_QuestionnaireTemplateType_IsSystem  DEFAULT 0,
	CreateDateUTC SMALLDATETIME NOT NULL CONSTRAINT DF_QuestionnaireTemplateType_CreateDateUTC DEFAULT GETUTCDATE(),
	CreatedByUser VARCHAR(100) NOT NULL CONSTRAINT DF_QuestionnaireTemplateType_CreatedByUser DEFAULT SUSER_NAME(),
	LastChangeDateUTC SMALLDATETIME NULL,
	LastChangedByUser VARCHAR(100) NULL,
	DeleteDateUTC SMALLDATETIME NULL,
	DeletedByUser VARCHAR(100) NULL
)
GO
GRANT SELECT, INSERT, UPDATE ON tblQuestionnaireTemplateType TO role_iis_acct
GO

SET IDENTITY_INSERT tblQuestionnaireTemplateType ON
INSERT INTO tblQuestionnaireTemplateType (ID, Name, IsSystem, CreateDateUTC, CreatedByUser)
VALUES (1, 'DVIR', 1, GETUTCDATE(), 'System')
INSERT INTO tblQuestionnaireTemplateType (ID, Name, IsSystem, CreateDateUTC, CreatedByUser)
VALUES (2, 'JSA', 1, GETUTCDATE(), 'System')
SET IDENTITY_INSERT tblQuestionnaireTemplateType OFF
GO

-- 3.11.13 - BB - 2016/01/19 - VIR Templates
/*******************************************
 Date Created: 20 Jan 2016
 Author: Ben Bloodworth
 Purpose: Store the user created VIR templates
 Changes: 
	2/5/2016 - JAE - expanded into a questionnaire for use with multiple reports
	4/4/2016 - JAE - added template type
*******************************************/
CREATE TABLE tblQuestionnaireTemplate
(  
	  ID INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_QuestionnaireTemplate PRIMARY KEY CLUSTERED
	, Name VARCHAR(50) NOT NULL
	, QuestionnaireTemplateTypeID INT NOT NULL CONSTRAINT FK_QuestionnaireTemplate_QuestionnaireTemplateTypeID REFERENCES tblQuestionnaireTemplateType(ID)
	, EffectiveDate DATETIME NOT NULL
	, EndDate DATETIME NOT NULL
	, CarrierID INT NULL CONSTRAINT FK_QuestionnaireTemplate_Carrier REFERENCES tblCarrier(ID)	
	, CreateDateUTC DATETIME NOT NULL CONSTRAINT DF_QuestionnaireTemplate_CreateDateUTC DEFAULT(GETUTCDATE())
	, CreatedByUser VARCHAR(100) NOT NULL CONSTRAINT DF_QuestionnaireTemplate_CreatedByUser DEFAULT('System')
	, LastChangeDateUTC DATETIME NULL
	, LastChangedByUser VARCHAR(100) NULL
	, DeleteDateUTC DATETIME NULL
	, DeletedByUser VARCHAR(100) NULL
) 
GO
GRANT SELECT, INSERT, UPDATE ON tblQuestionnaireTemplate TO role_iis_acct
GO


-- 3.11.13 - BB - 2016/01/20 - VIR Questions
/*******************************************
 Date Created: 20 Jan 2016
 Author: Ben Bloodworth
 Purpose: Store user created questions for VIRs
 Changes: 
	2/5/2016 - JAE - expanded into a questionnaire for use with multiple reports
*******************************************/
CREATE TABLE tblQuestionnaireQuestion
(  
	  ID INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_QuestionnaireQuestion PRIMARY KEY CLUSTERED
	, QuestionnaireTemplateID INT NOT NULL CONSTRAINT FK_QuestionnaireQuestion_QuestionnaireTemplate REFERENCES tblQuestionnaireTemplate(ID)
	, QuestionnaireQuestionTypeID INT NOT NULL CONSTRAINT FK_QuestionnaireQuestion_QuestionnaireQuestionType REFERENCES tblQuestionnaireQuestionType(ID) -- Type of answer question should be
	, QuestionText VARCHAR(255) NOT NULL
	, AnswersFormat VARCHAR(255) NULL -- text for configuring answer (ie "YES|NO")
	, SortNum INT NULL
	, CreateDateUTC DATETIME NOT NULL CONSTRAINT DF_QuestionnaireQuestion_CreateDateUTC DEFAULT(GETUTCDATE())
	, CreatedByUser VARCHAR(100) NOT NULL CONSTRAINT DF_QuestionnaireQuestion_CreatedByUser DEFAULT('System')
	, LastChangeDateUTC DATETIME NULL
	, LastChangedByUser VARCHAR(100) NULL
	, DeleteDateUTC DATETIME NULL
	, DeletedByUser VARCHAR(100) NULL
) 
GO
GRANT SELECT, INSERT, UPDATE ON tblQuestionnaireQuestion TO role_iis_acct
GO



-- 3.11.13 - BB - 2016/01/20 - VIR Submissions 
/*******************************************
 Date Created: 20 Jan 2016
 Author: Ben Bloodworth
 Purpose: Store sets of answers by VIR template per VIR submission
 Changes: 
	2/5/2016 - JAE - expanded into a questionnaire for use with multiple reports
*******************************************/
CREATE TABLE tblQuestionnaireSubmission
(  
	  ID INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_QuestionnaireSubmission PRIMARY KEY CLUSTERED
	, QuestionnaireTemplateID INT NOT NULL CONSTRAINT FK_QuestionnaireSubmission_QuestionnaireTemplate REFERENCES tblQuestionnaireTemplate(ID)
	, DriverID INT NULL CONSTRAINT FK_QuestionnaireSubmission_Driver REFERENCES tblDriver(ID)
	, TruckID INT NULL CONSTRAINT FK_QuestionnaireSubmission_Truck REFERENCES tblTruck(ID)
	, TrailerID INT NULL CONSTRAINT FK_QuestionnaireSubmission_Trailer REFERENCES tblTrailer(ID)
	, Trailer2ID INT NULL CONSTRAINT FK_QuestionnaireSubmission_Trailer2 REFERENCES tblTrailer(ID)
	, SubmissionDateUTC DATETIME NOT NULL CONSTRAINT DF_QuestionnaireSubmission_SubmissionDateUTC DEFAULT(GETUTCDATE())
	, CreateDateUTC DATETIME NOT NULL CONSTRAINT DF_QuestionnaireSubmission_CreateDateUTC DEFAULT(GETUTCDATE())
	, CreatedByUser VARCHAR(100) NOT NULL CONSTRAINT DF_QuestionnaireSubmission_CreatedByUser DEFAULT('System')
) 
GO
GRANT SELECT ON tblQuestionnaireSubmission TO role_iis_acct
GO



-- 3.11.13 - BB - 2016/01/20 - VIR Responses
/*******************************************
 Date Created: 20 Jan 2016
 Author: Ben Bloodworth
 Purpose: Store individual responses to VIR questions
 Changes: 
	2/5/2016 - JAE - expanded into a questionnaire for use with multiple reports
				   - answer can be null (for a label)
*******************************************/
CREATE TABLE tblQuestionnaireResponse
(  
	  ID INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_QuestionnaireResponse PRIMARY KEY CLUSTERED
	, QuestionnaireSubmissionID INT NOT NULL CONSTRAINT FK_QuestionnaireResponse_QuestionnaireSubmission REFERENCES tblQuestionnaireSubmission(ID)
	, QuestionnaireQuestionID INT NOT NULL CONSTRAINT FK_QuestionnaireResponse_QuestionnaireQuestion REFERENCES tblQuestionnaireQuestion(ID)
	, Answer VARCHAR(255) NULL	
	, CreateDateUTC DATETIME NOT NULL CONSTRAINT DF_QuestionnaireResponse_CreateDateUTC DEFAULT(GETUTCDATE())
	, CreatedByUser VARCHAR(100) NOT NULL CONSTRAINT DF_QuestionnaireResponse_CreatedByUser DEFAULT('System')
) 
GO
GRANT SELECT ON tblQuestionnaireSubmission TO role_iis_acct
GO


-- 3.11.13 - BB - 2016/01/20 - VIR Blobs
/*******************************************
 Date Created: 20 Jan 2016
 Author: Ben Bloodworth
 Purpose: Store signature images for VIRs
 Changes: 
	2/5/2016 - JAE - expanded into a questionnaire for use with multiple reports
*******************************************/
CREATE TABLE tblQuestionnaireResponseBlob
(  
	  ID INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_QuestionnaireBlob PRIMARY KEY CLUSTERED
	, QuestionnaireResponseID INT NOT NULL CONSTRAINT FK_QuestionnaireResponseBlob_QuestionnaireResponse REFERENCES tblQuestionnaireResponse(ID)
	, AnswerBlob VARBINARY(MAX) NOT NULL
	, CreateDateUTC DATETIME NOT NULL CONSTRAINT DF_QuestionnaireResponseBlob_CreateDateUTC DEFAULT(GETUTCDATE())
	, CreatedByUser VARCHAR(100) NOT NULL CONSTRAINT DF_QuestionnaireResponseBlob_CreatedByUser DEFAULT('System')
) 
GO
GRANT SELECT ON tblQuestionnaireSubmission TO role_iis_acct
GO


-- 3.11.13
-- =============================================
-- Author:		Joe Engler
-- Create date: 02 Feb 2016
-- Description:	trigger to advance any duplicated SortNums in a Questionnaire to make reordering easier
--               Copied from trigUserReportColumnDefinition
-- =============================================
CREATE TRIGGER trigQuestionnaireQuestion_IU_SortNum ON tblQuestionnaireQuestion FOR INSERT, UPDATE AS
BEGIN
	IF (trigger_nestlevel() < 2)  -- logic below will recurse unless this is used to prevent it
	BEGIN
		IF (UPDATE(SortNum))
		BEGIN
			SELECT i.QuestionnaireTemplateID, i.SortNum, ID = MAX(i2.ID)
			INTO #new
			FROM (
				SELECT QuestionnaireTemplateID, SortNum = min(SortNum)
				FROM inserted
				GROUP BY QuestionnaireTemplateID
			) i
			JOIN inserted i2 ON i2.QuestionnaireTemplateID = i.QuestionnaireTemplateID AND i2.SortNum = i.SortNum
			GROUP BY i.QuestionnaireTemplateID, i.SortNum
			
			DECLARE @TemplateID int, @SortNum int, @ID int
			SELECT TOP 1 @TemplateID = QuestionnaireTemplateID, @SortNum = SortNum, @ID = ID FROM #new 

			WHILE (@TemplateID IS NOT NULL)
			BEGIN
				UPDATE tblQuestionnaireQuestion
					SET SortNum = N.NewSortNum
				FROM tblQuestionnaireQuestion QQ
				JOIN (
					SELECT ID, QuestionnaireTemplateID, NewSortNum = @SortNum + ROW_NUMBER() OVER (ORDER BY SortNum) 
					FROM tblQuestionnaireQuestion
					WHERE QuestionnaireTemplateID = @TemplateID AND SortNum >= @SortNum AND ID <> @ID
				) N ON N.ID = QQ.ID
				
				DELETE FROM #new WHERE ID = @ID
				SET @TemplateID = NULL
				SELECT TOP 1 @TemplateID = QuestionnaireTemplateID, @SortNum = SortNum, @ID = ID FROM #new
			END	
		END
	END
END
GO


--3.11.13
/*************************************/
-- Date Created: 26 Feb 2016
-- Author: Joe Engler
-- Purpose: Update questionnaire template timestamp when question is edited
/*************************************/
CREATE TRIGGER trigQuestionnaireQuestion_IU ON tblQuestionnaireQuestion AFTER INSERT, UPDATE AS
BEGIN
	UPDATE tblQuestionnaireTemplate 
		SET LastChangeDateUTC = GETUTCDATE(),
	        LastChangedByUser = (SELECT TOP 1 COALESCE(DeletedByUser, LastChangedByUser, CreatedByUser) FROM inserted)
	WHERE ID IN (SELECT QuestionnaireTemplateID FROM inserted)
END
GO


/* 3.11.13 - Adding permission to roles table for the questionnaire pages */
INSERT INTO aspnet_Roles (ApplicationId, RoleId, RoleName, LoweredRoleName, Description)
SELECT '446467D6-39CD-45E3-B3E0-CAC7945AF3E8', NEWID(), 'manageQuestionnaires', 'managequestionnaires', 'Allow user to see and use any Questionnaire pages'
EXCEPT SELECT ApplicationId, RoleId, RoleName, LoweredRoleName, Description
FROM aspnet_Roles
GO



COMMIT 
SET NOEXEC OFF