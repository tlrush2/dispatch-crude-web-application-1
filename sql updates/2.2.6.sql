/* add Region to spDriverAvailability
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.2.5'
SELECT  @NewVersion = '2.2.6'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

/***********************************/
-- Date Created: 26 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Drivers table records with "friendly" translated values included
/***********************************/
ALTER VIEW [dbo].[viewDriver] AS
SELECT *
	, CASE WHEN Active = 1 THEN '' ELSE 'Deleted: ' END + FullName AS FullNameD
FROM (
	SELECT D.*
	, cast(CASE WHEN isnull(C.DeleteDateUTC, D.DeleteDateUTC) IS NULL THEN 1 ELSE 0 END as bit) AS Active
	, D.FirstName + ' ' + D.LastName As FullName
	, D.LastName + ', ' + D.FirstName AS FullNameLF
	, isnull(CT.Name, 'Unknown') AS CarrierType
	, C.Name AS Carrier
	, S.Abbreviation AS StateAbbrev 
	, T.FullName AS Truck
	, T1.FullName AS Trailer
	, T2.FullName AS Trailer2
	, R.Name AS Region
	FROM dbo.tblDriver D 
	JOIN dbo.tblCarrier C ON C.ID = D.CarrierID 
	LEFT JOIN dbo.tblCarrierType CT ON CT.ID = C.CarrierTypeID
	LEFT JOIN tblState S ON S.ID = D.StateID
	LEFT JOIN viewTruck T ON T.ID = D.TruckID
	LEFT JOIN viewTrailer T1 ON T1.ID = D.TrailerID
	LEFT JOIN viewTrailer T2 ON T2.ID = D.Trailer2ID
	LEFT JOIN tblRegion R ON R.ID = D.RegionID
) X

GO

/***********************************************************/
-- Date Created: 3 Nov 2013
-- Author: Kevin Alons
-- Purpose: return all "potential" Driver Availability records for a date range
/***********************************************************/
ALTER PROCEDURE [dbo].[spDriverAvailability]
(
  @StartDate smalldatetime
, @EndDate smalldatetime
, @CarrierID int = -1
, @RegionID int = -1
) AS BEGIN
	-- get a table with the entire specified date range (date only values)
	DECLARE @AvailDate smalldatetime
	SELECT @AvailDate = @StartDate, @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	CREATE TABLE #DateRange (AvailDate smalldatetime)
	WHILE (@AvailDate <= @EndDate) BEGIN
		INSERT INTO #DateRange VALUES (@AvailDate)
		SET @AvailDate = DATEADD(day, 1, @AvailDate)
	END
	
	SELECT dbo.fnDateMdYY(AvailDateOnly) AS AvailDate, AvailDateTime, CarrierID, Carrier, x.DriverID, Driver, Region
		, MobilePhone, isnull(TOO.OpenOrders, 0) AS OpenOrders, isnull(DOO.OpenOrders, 0) AS DailyOpenOrders
	FROM (
		SELECT DA.ID
			, dbo.fnDateOnly(DR.AvailDate) AS AvailDateOnly, DA.AvailDateTime
			, D.CarrierID, D.Carrier, D.ID AS DriverID, D.FullName AS Driver, D.Region
			, D.MobilePhone
		FROM (#DateRange DR
		CROSS JOIN viewDriver D)
		LEFT JOIN tblDriverAvailability DA ON DA.DriverID = D.ID AND dbo.fnDateOnly(DA.AvailDateTime) = DR.AvailDate
		WHERE (@CarrierID = -1 OR D.CarrierID = @CarrierID)
		  AND D.Active = 1
	) x
	LEFT JOIN (
		SELECT DriverID, count(*) AS OpenOrders 
		FROM tblOrder 
		WHERE StatusID IN (2,7,8) 
		GROUP BY DriverID
	) TOO ON TOO.DriverID = x.DriverID
	LEFT JOIN (
		SELECT DriverID, dbo.fnDateOnly(DueDate) AS DueDate, COUNT(*) AS OpenOrders
		FROM tblOrder 
		WHERE StatusID IN (2,7,8) 
		GROUP BY DriverID, dbo.fnDateOnly(DueDate)
	) DOO ON DOO.DriverID = x.DriverID AND DOO.DueDate = x.AvailDateOnly
	ORDER BY Carrier, Driver, AvailDateOnly
	
END

GO

COMMIT
SET NOEXEC OFF