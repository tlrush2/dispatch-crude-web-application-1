-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.12.1.1'
SELECT  @NewVersion = '3.12.2'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'DCWEB-1166:  Implement QR Scanning'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

--------------------------------------------------------------------------------

-- Add customer QR code field to truck table
ALTER TABLE tblTruck ADD QRCode UNIQUEIDENTIFIER NULL CONSTRAINT DF_Truck_QRCode DEFAULT NEWID()
GO

-- update QR codes for existing trucks to new GUID
UPDATE tblTruck SET QRCode = NEWID() WHERE QRCode IS NULL
GO

-- Refresh view to show new column
EXEC sp_refreshview viewTruck
GO

/***********************************
-- Date Created: 2016-05-10
-- Author: Joe Engler
-- Purpose: Handle null QRCode
-- Changes:
***********************************/
CREATE TRIGGER trigTruck_IU ON tblTruck AFTER INSERT,UPDATE
AS 
BEGIN
    UPDATE tblTruck
	SET QRCode = NEWID() 
	WHERE QRCode IS NULL AND ID IN (select ID from inserted);
END 

GO

-- Add customer QR code field to trailer table
ALTER TABLE tblTrailer ADD QRCode UNIQUEIDENTIFIER NULL CONSTRAINT DF_Trailer_QRCode DEFAULT NEWID()
GO

-- update QR codes for existing trailers to new GUID
UPDATE tblTrailer SET QRCode = NEWID() WHERE QRCode IS NULL
GO

-- Refresh view to show new column
EXEC sp_refreshview viewTrailer
GO

/***********************************
-- Date Created: 2016-05-10
-- Author: Joe Engler
-- Purpose: Handle null QRCode
-- Changes:
***********************************/
CREATE TRIGGER trigTrailer_IU ON tblTrailer AFTER INSERT,UPDATE
AS 
BEGIN
    UPDATE tblTrailer 
	SET QRCode = NEWID() 
	WHERE QRCode IS NULL AND ID IN (select ID from inserted);
END 

GO

/***********************************
-- Date Created: 26 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Truck info with Last Odometer entry 
-- Changes:
		- 3.10.13  - JAE - Add Truck Type
		- 3.11.12.2 - BB - Add Garage State Abbreviation to allow filtering on webpage grid
		- x.x.x - JAE - Add QRCode
***********************************/
ALTER VIEW viewTruckWithLastOdometer AS
	SELECT T.ID
		, CarrierID
		, IDNumber
		, DOTNumber
		, VIN
		, Make
		, Model
		, Year
		, T.CreateDateUTC
		, T.CreatedByUser
		, LastChangeDateUTC = dbo.fnMaxDateTime(T.LastChangeDateUTC, TM.OdometerDateUTC)
		, T.LastChangedByUser
		, LicenseNumber
		, AxleCount
		, MeterType
		, HasSleeper
		, PurchasePrice
		, GarageCity
		, GarageStateID
		, GarageStateAbbrev = S.Abbreviation  -- 3.11.12.2
		, RegistrationDocument
		, RegistrationDocName
		, RegistrationExpiration
		, DeleteDateUTC
		, DeletedByUser
		, InsuranceIDCardDocument
		, InsuranceIDCardDocName
		, InsuranceIDCardExpiration
		, InsuranceIDCardIssue
		, CIDNumber
		, SIDNumber
		, GPSUnit
		, OwnerInfo
		, TireSize
		, Active
		, T.FullName
		, CarrierType
		, Carrier
		, LastOdometer = TM.Odometer, LastOdometerDate = TM.OdometerDate
		, TruckTypeID
		, TruckType
		, QRCode
	FROM dbo.viewTruck T
	LEFT JOIN viewOrderLastTruckMileage TM ON TM.TruckID = T.ID
	LEFT JOIN tblState S ON S.ID = T.GarageStateID

GO

--------------------------------------------------------------------------------

COMMIT 
SET NOEXEC OFF