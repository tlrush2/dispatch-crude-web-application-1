SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20) = '3.13.3.2'
	, @NewVersion varchar(20) = '3.13.3.3'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'DCWEB-1442 HOS Carrier Rules'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


ALTER TABLE tblCarrierRuleType
ADD Description VARCHAR(200) NULL

GO

INSERT INTO tblCarrierRuleType
VALUES
(5, 'HOS - Driving Hours Per Day Max', 3, 1, 'Maximum hours a driver can driver in a given day. (0 = no limit)'),
(6, 'HOS - Driving Hours Per Week Max', 3, 1, 'Maximum hours a driver can driver in a given week. (0 = no limit)'),

(7, 'HOS - On-duty Hours Per Day Max', 3, 1, 'Maximum hours a driver can driver in a given day. (0 = no limit)'),
(8, 'HOS - On-duty Hours Per Week Max', 3, 1, 'Maximum hours a driver can driver in a given day. (0 = no limit)'),

(9, 'HOS - Warn Logoff Hours', 3, 1, 'Hours on-duty before a driver is warned to log off. (0 = no warning)'),
(10, 'HOS - Force Logoff Hours', 3, 1, 'Hours on-duty before a driver is forced to log off.  (0 = not enforced)'),

(11, 'HOS - Driver App Log Retention Days', 3, 1, 'Number of days of HOS history kept on driver app. (0 = no limit/6 months)'),

(12, 'HOS - Hours Required For Reset', 3, 1, 'Number of hours a driver must rest before status is reset.')


UPDATE tblCarrierRuleType
   SET Name = 'HOS Enabled',
       Description = 'Flag to activate the Hours Of Service (HOS) module'
 WHERE ID = 1

 UPDATE tblCarrierRuleType
   SET Description = 'Source for selecting a truck on mobile app'
 WHERE ID = 2

 UPDATE tblCarrierRuleType
   SET Description = 'Source for selecting a trailer on mobile app'
 WHERE ID = 3

 UPDATE tblCarrierRuleType
   SET Description = 'Interval for requiring a driver to fill out a DVIR'
 WHERE ID = 4
 
 GO

--Update HOS table to use DATETIME and not SMALL DATETIME (will track seconds)
ALTER TABLE tblHos DROP CONSTRAINT DF_Hos_CreateDateUTC
GO

ALTER TABLE tblHos
ALTER COLUMN LogDateUTC DATETIME NULL
GO

ALTER TABLE tblHos
ALTER COLUMN CreateDateUTC DATETIME NULL
GO

ALTER TABLE tblHos
ALTER COLUMN LastChangeDateUTC DATETIME NULL
GO

ALTER TABLE tblHos
ALTER COLUMN DeleteDateUTC DATETIME NULL
GO

ALTER TABLE tblHos ADD CONSTRAINT DF_Hos_CreateDateUTC DEFAULT GETUTCDATE() FOR CreateDateUTC
GO


ALTER TABLE tblHosDBAudit
ALTER COLUMN LogDateUTC DATETIME NULL
GO

ALTER TABLE tblHosDBAudit
ALTER COLUMN CreateDateUTC DATETIME NULL
GO

ALTER TABLE tblHosDBAudit
ALTER COLUMN LastChangeDateUTC DATETIME NULL
GO

ALTER TABLE tblHosDBAudit
ALTER COLUMN DeleteDateUTC DATETIME NULL
GO

 /******************************************
** Date Created:	2016/07/15
** Author:			Joe Engler
** Purpose:			Return the historical HOS entries for a driver, calculate the TimeInState for ease of computation on mobile side
** Changes:
******************************************/
CREATE PROCEDURE spDriverHOSLog(@DriverID INT, @LastSyncDate DATETIME) AS
BEGIN
    DECLARE @DaysToKeep INT = COALESCE(
				(SELECT NULLIF(r.Value, 0)
					FROM tblDriver d 
					CROSS APPLY dbo.fnCarrierRules(GETDATE(), null, 11, d.CarrierID, d.ID, d.StateID, d.RegionID, 1) r 
					WHERE d.ID = @DriverID), 30*6) -- Default to 6 months if no order rule

	DECLARE @StartDate DATETIME = DATEADD(DAY, -@DaysToKeep, GETUTCDATE());

	WITH rows AS
	(
 			-- Dummy start record, gets the previous record prior to the start date
			SELECT 0 AS rownum, *
            FROM tblHos
			WHERE ID = (SELECT TOP 1 ID FROM tblHos WHERE DriverID = @DriverID AND LogDateUTC < @StartDate ORDER BY LogDateUTC DESC) 

			UNION

			SELECT ROW_NUMBER() OVER (ORDER BY LogDateUTC) AS rownum, *
			FROM tblHos
			WHERE DriverID = @DriverID
			AND LogDateUTC BETWEEN @StartDate AND GETUTCDATE()

			UNION

			-- Dummy end record, gets the final record again so it will show in the summary list (with TimeInState = 0)
			SELECT (select count(*)+1 FROM tblHos WHERE DriverID = @DriverID AND LogDateUTC BETWEEN @StartDate AND GETUTCDATE()) AS rownum, *
			FROM tblHos
			WHERE DriverID = @DriverID
			AND ID = (SELECT TOP 1 ID FROM tblHos WHERE DriverID = @DriverID AND LogDateUTC <= GETUTCDATE() ORDER BY LogDateUTC DESC)
	)
		SELECT mc.*, CASE WHEN mc.ID = mp.ID THEN null -- send null time in state for current HOS (repeated at end) 
						else DATEDIFF(MINUTE, mc.LogDateUTC, mp.LogDateUTC) END AS TimeInState
		FROM rows mc
		JOIN rows mp
		ON mc.rownum = mp.rownum-1
		CROSS JOIN fnSyncLCDOffset(@LastSyncDate) LCD
		WHERE @LastSyncDate IS NULL 
		   OR mc.CreateDateUTC >= LCD.LCD -- use offset to get nearest city updates for records just sent to the server
		   OR mc.LastChangeDateUTC >= LCD.LCD
		   OR mc.DeleteDateUTC >= LCD.LCD
		ORDER BY mc.LogDateUTC
END

GO


COMMIT
SET NOEXEC OFF