BEGIN TRANSACTION

/***********************************/
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: create new Order (loads) for the specified criteria
/***********************************/
ALTER PROCEDURE [dbo].[spCreateLoads]
(
  @OriginID int
, @DestinationID int
, @TicketTypeID int
, @DueDate datetime
, @CustomerID int
, @CarrierID int = NULL
, @DriverID int = NULL
, @Qty int
, @UserName varchar(100)
, @TankNum varchar(20) = NULL
, @PriorityID int = 3 -- LOW priority
, @StatusID smallint = -10 -- GENERATED
) AS
BEGIN
	DECLARE @i int, @RouteID int, @ActualMiles int
	SELECT @i = 0, @RouteID = ID, @ActualMiles = ActualMiles
	FROM dbo.tblRoute WHERE OriginID = @OriginID AND DestinationID = @DestinationID
	
	IF @RouteID IS NULL
	BEGIN
		RAISERROR('No Route was found for the specified Origin/Destination combination!', 16, 1)
		RETURN
	END
	
	WHILE @i < @Qty BEGIN
		DECLARE @PumperID int, @OperatorID int, @ProducerID int
		SELECT @PumperID = PumperID, @OperatorID = OperatorID, @ProducerID = ProducerID
		FROM tblOrigin WHERE ID = @OriginID
		
		INSERT INTO dbo.tblOrder (OriginID, DestinationID, TicketTypeID, ActualMiles, RouteID, DueDate, CustomerID
				, CarrierID, DriverID, StatusID, PriorityID, OriginTankNum
				, OrderNum, PumperID, OperatorID, ProducerID, CreateDate, CreatedByUser)
			VALUES (@OriginID, @DestinationID, @TicketTypeID, @ActualMiles, @RouteID, @DueDate, @CustomerID
				, @CarrierID, @DriverID, @StatusID, @PriorityID, @TankNum
				, (SELECT isnull((SELECT max(OrderNum) FROM tblOrder), 100000) + 1)
				, @PumperID, @OperatorID, @ProducerID, GETDATE(), @UserName)
		
		IF (@DriverID IS NOT NULL)
		BEGIN
			UPDATE tblOrder SET TruckID = D.TruckID, TrailerID = D.TrailerID, Trailer2ID = D.Trailer2ID
			FROM tblOrder O
			JOIN tblDriver D ON D.ID = O.DriverID
			WHERE O.ID = SCOPE_IDENTITY()
		END
		
		SET @i = @i + 1
	END
END

GO

UPDATE tblSetting SET Value = '1.1.8' WHERE ID=0

COMMIT
