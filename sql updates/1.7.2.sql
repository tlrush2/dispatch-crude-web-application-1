/* quarter inches were being rounded
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '1.7.1', @NewVersion = '1.7.2'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO


/***********************************/
-- Date Created: 9 Dec 2012
-- Author: Kevin Alons
-- Purpose: return the capacity in a tank for the given feet/inches/q and BPI parameters
/***********************************/
ALTER FUNCTION [dbo].[fnTankQtyBarrels](@feet smallint, @inch tinyint, @q tinyint, @bpi decimal(9,6) = 1.67) RETURNS decimal(9,3) AS
BEGIN
	IF (@bpi IS NULL)
		SELECT @bpi = 1.67 -- default value
	RETURN (@bpi * ((@feet * 12) + @inch + (@q * 1.0 / 4)))
END

GO

COMMIT TRAN
SET NOEXEC OFF