/*
	-- fix bug in Inactivate Stale Origins | Destinations logic
	-- update the Adhoc tank selection text to "[Enter Details]"
	-- also add Origin | Dest TimeZone files for DriverApp Sync
	-- include all DELIVERED orders to the driver (up to AUDITED status)
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.0.5'
SELECT  @NewVersion = '3.0.6'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 19 Jun 2014
-- Description:	deactivate any currently active Destinations that haven't had any traffic since the Stale days threshold
-- =============================================
ALTER PROCEDURE [dbo].[spDeactivateStaleDestinations]
(
  @UserName varchar(100)
) 
AS BEGIN
	DECLARE @staleDays int
	SELECT @staleDays = Value FROM tblSetting WHERE ID = 21
	
	UPDATE tblDestination
		SET DeleteDateUTC = GETUTCDATE(), DeletedByUser = @UserName
	WHERE DeleteDateUTC IS NULL
		AND CreateDateUTC < DATEADD(day, -@staleDays, getdate())
		AND ID NOT IN (SELECT DestinationID FROM viewOrder WHERE OrderDate > DATEADD(day, -@staleDays, getdate()))
END

GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 19 Jun 2014
-- Description:	deactivate any currently active Origins that haven't had any traffic since the Stale days threshold
-- =============================================
ALTER PROCEDURE [dbo].[spDeactivateStaleOrigins]
(
  @UserName varchar(100)
) 
AS BEGIN
	DECLARE @staleDays int
	SELECT @staleDays = Value FROM tblSetting WHERE ID = 21
	
	UPDATE tblOrigin 
		SET DeleteDateUTC = GETUTCDATE(), DeletedByUser = @UserName
	WHERE DeleteDateUTC IS NULL
		AND CreateDateUTC < DATEADD(day, -@staleDays, getdate())
		AND ID NOT IN (SELECT OriginID FROM viewOrder WHERE OrderDate > DATEADD(day, -@staleDays, getdate()))
END

GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 13 May 2013
-- Description:	retrieve all currently eligible Destinations for the specified OriginID/ProductID values
-- =============================================
ALTER FUNCTION [dbo].[fnRetrieveEligibleDestinations](@originID int, @productID int, @requireRoute bit = 0) RETURNS TABLE AS RETURN
	SELECT D.*
	FROM dbo.viewDestination D
	WHERE DeleteDateUTC IS NULL 
		AND ((@requireRoute = 0 OR ID IN (SELECT DestinationID FROM viewRoute WHERE OriginID = @OriginID)) 
			AND ID IN (SELECT DestinationID FROM tblDestinationProducts WHERE ProductID=@ProductID) 
			AND (@OriginID = 0 OR ID IN (SELECT DestinationID FROM viewCustomerOriginDestination DC WHERE OriginID=@OriginID)))

GO

/*******************************************/
-- Date Created: 25 Apr 2013
-- Author: Kevin Alons
-- Purpose: return readonly Order data for Driver App sync
/*******************************************/
ALTER FUNCTION [dbo].[fnOrderReadOnly_DriverApp]( @DriverID int, @LastChangeDateUTC datetime ) RETURNS TABLE AS
RETURN 
	SELECT O.ID
		, O.OrderNum
		, O.StatusID
		, O.TicketTypeID
		, PriorityNum = cast(P.PriorityNum as int) 
		, O.Product
		, O.DueDate
		, O.Origin
		, O.OriginFull
		, OO.OriginType
		, O.OriginUomID
		, OriginStation = OO.Station 
		, OriginLeaseNum = OO.LeaseNum 
		, OriginCounty = OO.County 
		, OriginLegalDescription = OO.LegalDescription 
		, O.OriginNDIC
		, O.OriginNDM
		, O.OriginCA
		, O.OriginState
		, OO.WellAPI AS OriginAPI
		, OriginLat = OO.LAT 
		, OriginLon = OO.LON 
		, OriginGeoFenceRadiusMeters = OO.GeoFenceRadiusMeters 
		, O.Destination
		, O.DestinationFull
		, DestType = D.DestinationType 
		, O.DestUomID
		, DestLat = D.LAT 
		, DestLon = D.LON 
		, DestGeoFenceRadiusMeters = D.GeoFenceRadiusMeters 
		, D.Station AS DestinationStation
		, O.CreateDateUTC
		, O.CreatedByUser
		, O.LastChangeDateUTC
		, O.LastChangedByUser
		, DeleteDateUTC = isnull(ODAVD.VirtualDeleteDateUTC, O.DeleteDateUTC) 
		, DeletedByUser = isnull(ODAVD.VirtualDeletedByUser, O.DeletedByUser) 
		, O.OriginID
		, O.DestinationID
		, PriorityID = cast(O.PriorityID AS int) 
		, O.Operator
		, O.OperatorID
		, O.Pumper
		, O.PumperID
		, O.Producer
		, O.ProducerID
		, O.Customer
		, O.CustomerID
		, O.Carrier
		, O.CarrierID
		, O.ProductID
		, O.TicketType
		, PrintHeaderBlob = C.ZPLHeaderBlob 
		, EmergencyInfo = isnull(C.EmergencyInfo, 'For an emergency (spill, leak, fire, or accident) contact CHEMTREC @ 800.424.9300 (toll free)') 
		, O.DestTicketTypeID
		, O.DestTicketType
		, O.OriginTankNum
		, O.OriginTankID
		, O.DispatchNotes
		, O.DispatchConfirmNum
		, RouteActualMiles = isnull(R.ActualMiles, 0)
		, CarrierAuthority = CA.Authority 
		, OriginTimeZone = OO.TimeZone
		, DestTimeZone = D.TimeZone
	FROM dbo.viewOrder O
	JOIN dbo.tblPriority P ON P.ID = O.PriorityID
	JOIN dbo.viewOrigin OO ON OO.ID = O.OriginID
	JOIN dbo.viewDestination D ON D.ID = O.DestinationID
	JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
	JOIN dbo.tblRoute R ON R.ID = O.RouteID
	JOIN dbo.tblCarrier CA ON CA.ID = O.CarrierID
	LEFT JOIN tblOrderDriverAppVirtualDelete ODAVD ON ODAVD.OrderID = O.ID AND ODAVD.DriverID = @DriverID
	CROSS JOIN (SELECT DATEADD(second, -5, @LastChangeDateUTC) AS LCD) LCD
	WHERE (O.StatusID IN (2, 7, 8, 3) -- Dispatched, Accepted, Picked Up, Delivered
		-- or this potentially was a DriverApp record, that no longer is, so should be included (as deleted)
		OR (ODAVD.ID IS NOT NULL AND ODAVD.VirtualDeleteDateUTC >= LCD.LCD))
	  AND (O.DriverID = @DriverID OR ODAVD.DriverID = @DriverID)
	  AND (
		@LastChangeDateUTC IS NULL 
		OR O.CreateDateUTC >= LCD.LCD
		OR O.LastChangeDateUTC >= LCD.LCD
		OR O.DeleteDateUTC >= LCD.LCD
		OR C.LastChangeDateUTC >= LCD.LCD
		OR CA.LastChangeDateUTC >= LCD.LCD
		OR OO.LastChangeDateUTC >= LCD.LCD
		OR R.LastChangeDateUTC >= LCD.LCD
		OR ODAVD.VirtualDeleteDateUTC >= LCD.LCD)

GO

/*******************************************/
-- Date Created: 31 Aug 2013
-- Author: Kevin Alons
-- Purpose: return driver editable Order data for Driver App sync
/*******************************************/
ALTER FUNCTION [dbo].[fnOrderEdit_DriverApp]( @DriverID int, @LastChangeDateUTC datetime ) RETURNS TABLE AS
RETURN 
	SELECT O.ID
		, O.StatusID
		, O.TruckID
		, O.TrailerID
		, O.Trailer2ID
		, O.OriginBOLNum
		, O.OriginArriveTimeUTC
		, O.OriginDepartTimeUTC
		, O.OriginMinutes
		, O.OriginWaitNotes
		, O.OriginTruckMileage
		, O.OriginGrossUnits
		, O.OriginGrossStdUnits
		, O.OriginNetUnits
		, O.ChainUp
		, O.Rejected
		, O.RejectNotes
		, O.OriginTankNum
		, O.DestArriveTimeUTC
		, O.DestDepartTimeUTC
		, O.DestMinutes
		, O.DestWaitNotes
		, O.DestBOLNum
		, O.DestTruckMileage
		, O.DestGrossUnits
		, O.DestNetUnits
		, O.DestProductTemp
		, O.DestProductBSW
		, O.DestProductGravity
		, O.DestOpenMeterUnits
		, O.DestCloseMeterUnits
		, O.CarrierTicketNum
		, O.AcceptLastChangeDateUTC
		, O.PickupLastChangeDateUTC
		, O.DeliverLastChangeDateUTC
		, O.PickupPrintStatusID
		, O.DeliverPrintStatusID
		, O.PickupPrintDateUTC
		, O.DeliverPrintDateUTC
		, O.DriverNotes
	FROM dbo.tblOrder O
	LEFT JOIN tblOrderDriverAppVirtualDelete ODAVD ON ODAVD.OrderID = O.ID AND ODAVD.DriverID = @DriverID
	CROSS JOIN (SELECT DATEADD(second, -5, @LastChangeDateUTC) AS LCD) LCD
	WHERE (O.StatusID IN (2, 7, 8, 3) -- Dispatched, Accepted, Picked Up, Delivered
		-- or this potentially was a DriverApp record, that no longer is, so should be included (as deleted)
		OR (ODAVD.ID IS NOT NULL AND ODAVD.VirtualDeleteDateUTC >= LCD.LCD))
	  AND (O.DriverID = @DriverID OR ODAVD.DriverID = @DriverID)
	  AND (
		@LastChangeDateUTC IS NULL 
		OR O.CreateDateUTC >= LCD.LCD
		OR O.LastChangeDateUTC >= LCD.LCD
		OR O.DeleteDateUTC >= LCD.LCD
		OR ODAVD.VirtualDeleteDateUTC >= LCD.LCD)

GO

/*******************************************/
-- Date Created: 25 Apr 2013
-- Author: Kevin Alons
-- Purpose: return OrderTicket data for Driver App sync
/*******************************************/
ALTER FUNCTION [dbo].[fnOrderTicket_DriverApp]( @DriverID int, @LastChangeDateUTC datetime ) RETURNS  TABLE AS
RETURN 
	SELECT OT.ID
		, OT.UID
		, OT.OrderID
		, OT.CarrierTicketNum
		, OT.OriginTankID
		, OT.TankNum
		, OT.TicketTypeID
		, OT.ProductObsGravity
		, OT.ProductObsTemp
		, OT.ProductHighTemp
		, OT.ProductLowTemp
		, OT.ProductBSW
		, cast(OT.OpeningGaugeFeet as tinyint) AS OpeningGaugeFeet
		, cast(OT.OpeningGaugeInch as tinyint) AS OpeningGaugeInch
		, cast(OT.OpeningGaugeQ as tinyint) AS OpeningGaugeQ
		, cast(OT.ClosingGaugeFeet as tinyint) AS ClosingGaugeFeet
		, cast(OT.ClosingGaugeInch as tinyint) AS ClosingGaugeInch
		, cast(OT.ClosingGaugeQ as tinyint) AS ClosingGaugeQ
		, OT.BottomFeet
		, OT.BottomInches
		, OT.BottomQ
		, OT.GrossUnits
		, OT.GrossStdUnits
		, OT.NetUnits
		, OT.Rejected
		, OT.RejectNotes
		, OT.SealOff
		, OT.SealOn
		, OT.BOLNum
		, OT.CreateDateUTC
		, OT.CreatedByUser
		, OT.LastChangeDateUTC
		, OT.LastChangedByUser
		, OT.DeleteDateUTC
		, OT.DeletedByUser
	FROM dbo.tblOrderTicket OT
	JOIN dbo.tblOrder O ON O.ID = OT.OrderID
	LEFT JOIN tblOrderDriverAppVirtualDelete ODAVD ON ODAVD.OrderID = O.ID AND ODAVD.DriverID = @DriverID
	CROSS JOIN (SELECT DATEADD(second, -5, @LastChangeDateUTC) AS LCD) LCD
	WHERE (O.StatusID IN (2, 7, 8, 3) -- Dispatched, Accepted, Picked Up, Delivered
		-- or this potentially was a DriverApp record, that no longer is, so should be included (as deleted)
		OR (ODAVD.ID IS NOT NULL AND ODAVD.VirtualDeleteDateUTC >= LCD.LCD))
	  AND O.DriverID = @DriverID
	  AND (@LastChangeDateUTC IS NULL 
		OR OT.CreateDateUTC >= LCD.LCD
		OR OT.LastChangeDateUTC >= LCD.LCD
		OR ODAVD.VirtualDeleteDateUTC >= LCD.LCD)

GO

/*******************************************/
-- Date Created: 5 Apr 2014
-- Author: Kevin Alons
-- Purpose: return OriginTank data for Driver App sync
/*******************************************/
ALTER FUNCTION [dbo].[fnOriginTank_DriverApp]( @DriverID int, @LastChangeDateUTC datetime ) RETURNS  TABLE AS
RETURN 
	SELECT DISTINCT OT.ID
		, OT.OriginID
		, OT.TankNum
		, OT.TankDescription
		, OT.CreateDateUTC
		, OT.CreatedByUser
		, OT.LastChangeDateUTC
		, OT.LastChangedByUser
		, OT.DeleteDateUTC
		, OT.DeletedByUser
	FROM dbo.tblOriginTank OT
	JOIN dbo.tblOrder O ON O.OriginID = OT.OriginID
	LEFT JOIN tblOrderDriverAppVirtualDelete ODAVD ON ODAVD.OrderID = O.ID AND ODAVD.DriverID = @DriverID
	CROSS JOIN (SELECT DATEADD(second, -5, @LastChangeDateUTC) AS LCD) LCD
	WHERE (StatusID IN (2, 7, 8, 3) -- Dispatched, Accepted, Picked Up, Delivered
		-- or this potentially was a DriverApp record, that no longer is, so should be included (as deleted)
		OR (ODAVD.ID IS NOT NULL AND ODAVD.VirtualDeleteDateUTC >= LCD.LCD))
	  AND O.DriverID = @DriverID
	  AND (@LastChangeDateUTC IS NULL 
		OR O.CreateDateUTC >= LCD.LCD
		OR O.LastChangeDateUTC >= LCD.LCD
		OR OT.CreateDateUTC >= LCD.LCD
		OR OT.LastChangeDateUTC >= LCD.LCD
		OR OT.DeleteDateUTC >= LCD.LCD
		OR ODAVD.VirtualDeleteDateUTC >= LCD.LCD)

GO

/*******************************************/
-- Date Created: 5 Apr 2014
-- Author: Kevin Alons
-- Purpose: return OriginTankStrapping data for Driver App sync
/*******************************************/
ALTER FUNCTION [dbo].[fnOriginTankStrapping_DriverApp]( @DriverID int, @LastChangeDateUTC datetime ) RETURNS  TABLE AS
RETURN 
	SELECT DISTINCT OTS.*
/*		, OTS.OriginTankID
		, OTS.Feet
		, OTS.Inches
		, OTS.Q
		, OTS.BPQ
		, OTS.IsMinimum
		, OTS.IsMaximum
		, OTS.CreateDateUTC
		, OTS.CreatedByUser
		, OTS.LastChangeDateUTC
		, OTS.LastChangedByUser */
		, cast(CASE WHEN OTSD.ID IS NULL THEN 0 ELSE 1 END as bit) AS IsDeleted
	FROM dbo.tblOriginTankStrapping OTS
	JOIN dbo.tblOriginTank OT ON OT.ID = OTS.OriginTankID
	JOIN dbo.tblOrder O ON O.OriginID = OT.OriginID
	LEFT JOIN tblOrderDriverAppVirtualDelete ODAVD ON ODAVD.OrderID = O.ID AND ODAVD.DriverID = @DriverID
	LEFT JOIN dbo.tblOriginTankStrappingDeleted OTSD ON OTSD.ID = OTS.ID
	CROSS JOIN (SELECT DATEADD(second, -5, @LastChangeDateUTC) AS LCD) LCD
	WHERE (StatusID IN (2, 7, 8, 3) -- Dispatched, Accepted, Picked Up, Delivered
		-- or this potentially was a DriverApp record, that no longer is, so should be included (as deleted)
		OR (ODAVD.ID IS NOT NULL AND ODAVD.VirtualDeleteDateUTC >= LCD.LCD))
	  AND O.DriverID = @DriverID
	  AND (@LastChangeDateUTC IS NULL 
		OR O.CreateDateUTC >= LCD.LCD
		OR O.LastChangeDateUTC >= LCD.LCD
		OR OT.CreateDateUTC >= LCD.LCD
		OR OT.LastChangeDateUTC >= LCD.LCD
		OR OTS.CreateDateUTC >= LCD.LCD
		OR OTS.LastChangeDateUTC >= LCD.LCD
		OR OTSD.DeleteDateUTC >= LCD.LCD)

GO

/***********************************/
-- Date Created: 12 Mar 2014
-- Author: Kevin Alons
-- Purpose: return Origin Tank records with translated values
/***********************************/
ALTER VIEW [dbo].[viewOriginTank] AS
	SELECT OT.*
		, O.Name AS Origin
		, CASE WHEN OT.TankNum = '*' THEN '[Enter Details]' ELSE OT.TankNum END AS TankNum_Unstrapped
	FROM dbo.tblOriginTank OT
	JOIN dbo.viewOrigin O ON O.ID = OT.OriginID

GO

COMMIT
SET NOEXEC OFF

EXEC _spRefreshAllViews
GO