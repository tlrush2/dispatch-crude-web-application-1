/* add linkage between Origin/Destination and Product
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.0.18', @NewVersion = '2.0.19'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

CREATE TABLE tblOriginProducts
(
  ID int identity(1, 1) not null
, OriginID int not null CONSTRAINT FK_OriginProducts_Origin FOREIGN KEY (OriginID) REFERENCES tblOrigin(ID) ON DELETE CASCADE
, ProductID int not null CONSTRAINT FK_OriginProducts_Product FOREIGN KEY (ProductID) REFERENCES tblProduct(ID) ON DELETE CASCADE
, CONSTRAINT PK_OriginProducts PRIMARY KEY (ID)
)
GO

CREATE UNIQUE INDEX udxOriginProducts ON tblOriginProducts(OriginID, ProductID)
GO

INSERT INTO tblOriginProducts (OriginID, ProductID) SELECT ID, 1 FROM tblOrigin
GO

CREATE TABLE tblDestinationProducts
(
  ID int identity(1, 1) not null
, DestinationID int not null CONSTRAINT FK_DestinationProducts_Destination FOREIGN KEY (DestinationID) REFERENCES tblDestination(ID) ON DELETE CASCADE
, ProductID int not null CONSTRAINT FK_DestinationProducts_Product FOREIGN KEY (ProductID) REFERENCES tblProduct(ID) ON DELETE CASCADE
, CONSTRAINT PK_DestinationProducts PRIMARY KEY (ID)
)
GO

CREATE UNIQUE INDEX udxDestinationProducts ON tblDestinationProducts(DestinationID, ProductID)
GO

INSERT INTO tblDestinationProducts (DestinationID, ProductID) SELECT ID, 1 FROM tblDestination
GO

ALTER TABLE tblProduct ADD CreateDate smalldatetime null CONSTRAINT DF_Product_CreateDate DEFAULT (0)
GO
ALTER TABLE tblProduct ADD CreatedByUser varchar(100) null
GO
ALTER TABLE tblProduct ADD LastChangeDate smalldatetime null
GO
ALTER TABLE tblProduct ADD LastChangedByUser varchar(100) null
GO

EXEC _spRefreshAllViews
GO

/***********************************/
-- Date Created: 20 Sep 2013
-- Author: Kevin Alons
-- Purpose: return the orders be displayed on the OrderCreation page
/***********************************/
CREATE PROCEDURE spOrderCreationSource
(
  @RegionID int
, @ProductID int
) AS BEGIN
	SELECT cast(CASE WHEN StatusID=-10 THEN 0 ELSE 0 END as bit) AS Selected
		, isnull(OriginID, 0) AS OriginIDZ
		, * 
	FROM dbo.viewOrder 
	WHERE (@RegionID = -1
		OR OriginID IN (SELECT ID FROM tblOrigin WHERE RegionID = @RegionID)
		OR DestinationID IN (SELECT ID FROM tblDestination WHERE RegionID = @RegionID))
	AND ProductID = @ProductID
	AND StatusID IN (-10, 9) 
	AND DeleteDateUTC IS NULL 
	ORDER BY PriorityNum, StatusID DESC, Origin, Destination, DueDate
END

GO
GRANT EXECUTE ON spOrderCreationSource TO dispatchcrude_iis_acct
GO

COMMIT
SET NOEXEC OFF