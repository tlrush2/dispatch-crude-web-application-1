/*
	-- add Report Inquiry schema/plumbing
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.9.1'
SELECT  @NewVersion = '3.0.0'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

/***********************************/
-- Date Created: 28 Feb 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc for 3rd parties (Customer/Carrier)
/***********************************/
ALTER PROCEDURE [dbo].[spOrdersFullExport]
(
  @StartDate datetime
, @EndDate datetime
, @CarrierID int = 0 -- -1 = all carriers
, @CustomerID int = 0 -- -1 = all customers
, @ProducerID int = 0
, @StatusID_CSV varchar(max) = '4'
) AS BEGIN
	SELECT @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	
	SELECT * 
	FROM dbo.viewOrder_OrderTicket_Full OE
	WHERE (@CarrierID=-1 OR @CustomerID=-1 OR CarrierID=@CarrierID OR CustomerID=@CustomerID OR ProducerID=@ProducerID) 
	  AND OrderDate BETWEEN @StartDate AND @EndDate
	  AND DeleteDateUTC IS NULL
	  AND (StatusID IN (SELECT ID FROM dbo.fnSplitCSVIDs(@StatusID_CSV)))
	ORDER BY OrderDate, OrderNum
END

GO

CREATE TABLE tblReportDefinition
(
  ID int NOT NULL CONSTRAINT PK_ReportDefinition PRIMARY KEY
, Name varchar(25) NOT NULL
, SourceTable varchar(50) NOT NULL
)
GO
CREATE UNIQUE INDEX udxReportDefinition_Name ON tblReportDefinition(Name)
GO
GRANT SELECT ON tblReportDefinition TO dispatchcrude_iis_acct
GO
INSERT INTO tblReportDefinition (ID, Name, SourceTable)
	SELECT 1, 'Orders', 'viewOrder_OrderTicket_Full'
GO

CREATE TABLE tblReportFilterOperator
(
  ID int NOT NULL CONSTRAINT PK_ReportFilterOperator PRIMARY KEY
, Name varchar(25) NOT NULL
)
GO
CREATE UNIQUE INDEX udxReportFilterOperator_Name ON tblReportFilterOperator(Name)
GO
GRANT SELECT ON tblReportFilterOperator TO dispatchcrude_iis_acct
GO
INSERT INTO tblReportFilterOperator (ID, Name)
	SELECT 1, 'No Filter'
	UNION SELECT 2, 'Equals'
	UNION SELECT 3, 'In'
	UNION SELECT 4, 'Between'
	UNION SELECT 5, 'Starts With'
	UNION SELECT 6, 'Contains'
	UNION SELECT 7, 'Is Empty'
	UNION SELECT 8, 'Is Not Empty'
	UNION SELECT 9, 'Less Than'
	UNION SELECT 10, 'Less Than Or Equal'
	UNION SELECT 11, 'Greater Than'
	UNION SELECT 12, 'Greater Than Or Equal'
GO

CREATE TABLE tblReportFilterType
(
  ID int NOT NULL CONSTRAINT PK_ReportFilterType PRIMARY KEY
, Name varchar(25) NOT NULL
, FilterOperatorID_CSV varchar(50) NULL
)
GO
CREATE UNIQUE INDEX udxReportFilterType_Name ON tblReportFilterType(Name)
GO
INSERT INTO tblReportFilterType (ID, Name, FilterOperatorID_CSV)
	SELECT 1, 'Text', '1,5,6,7,8'
	UNION SELECT 2, 'DropDown', '1,3'
	UNION SELECT 3, 'Date Period', '1,2'
	UNION SELECT 4, 'Number', '1,2,7,8,9,10,11,12'
	UNION SELECT 5, 'Yes/No', '1,2'
	UNION SELECT 6, 'Date', '1,2,4'
GO

CREATE TABLE tblReportColumnDefinition
(
  ID int NOT NULL CONSTRAINT PK_ReportColumnDefinition PRIMARY KEY
, ReportID int NOT NULL CONSTRAINT FK_ReportColumnDefinition_ReportDefinition FOREIGN KEY REFERENCES tblReportDefinition(ID)
, DataField varchar(50) NOT NULL
, Caption varchar(50) NULL
, DataFormat varchar(50) NULL
, FilterDataField varchar(50) NULL
, FilterTypeID int NOT NULL CONSTRAINT FK_ReportColumnDefinition_FilterType FOREIGN KEY REFERENCES tblReportFilterType(ID)
, FilterDropDownSql varchar(255) NULL
, FilterAllowCustomText bit NOT NULL CONSTRAINT DF_ReportColumnDefinition_FilterAllowCustomText DEFAULT (0)
)
GO
CREATE UNIQUE INDEX udxReportColumnDefinition_Report_DataField ON tblReportColumnDefinition(ReportID, DataField)
GO
GRANT SELECT ON tblReportColumnDefinition TO dispatchcrude_iis_acct
GO 
INSERT INTO tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterAllowCustomText, FilterDropDownSql)
	--	 id, reportid, datafield			, caption		, dataformat	, fdf				, ft ac sql
		  SELECT 1, 1, 'Carrier'			, 'Carrier'		, NULL			, 'CarrierID'		, 2, 0, 'SELECT ID, Name FROM tblCarrier ORDER BY Name'
	UNION SELECT 2, 1, 'Customer'			, 'Shipper'		, NULL			, 'CustomerID'		, 2, 0, 'SELECT ID, Name FROM tblCustomer ORDER BY Name'
	UNION SELECT 3, 1, 'Destination'		, 'Destination'	, NULL			, 'DestinationID'	, 2, 0, 'SELECT ID, Name FROM tblOrigin ORDER BY Name'
	UNION SELECT 4, 1, 'OrderDate'			, 'Date'		, 'M/d/yyyy'	, NULL				, 3, 0, 'SELECT ID=1, Name=''Week to Date'' UNION SELECT 2, ''Month to Date'' UNION SELECT 3, ''Quarter to Date'' UNION SELECT 4, ''Year to Date'' ORDER BY ID DESC'
	UNION SELECT 5, 1, 'OrderNum'			, 'Order #'		, NULL			, NULL				, 1, 0, NULL
	UNION SELECT 6, 1, 'Origin'				, 'Origin'		, NULL			, 'OriginID'		, 2, 0, 'SELECT ID, Name FROM tblOrigin ORDER BY Name'
	UNION SELECT 7, 1, 'OriginGrossUnits'	, 'Origin GOV'	, '#,##0.00'	, NULL				, 4, 1, NULL
	UNION SELECT 8, 1, 'OriginGrossStdUnits', 'Origin GSV'	, '#,##0.00'	, NULL				, 4, 1, NULL
	UNION SELECT 9, 1, 'OriginNetUnits'		, 'Origin NSV'	, '#,##0.00'	, NULL				, 4, 1, NULL
	UNION SELECT 10, 1, 'Product'			, 'Product'		, NULL			, 'ProductID'		, 2, 0, 'SELECT ID, Name FROM tblProduct ORDER BY Name'
	UNION SELECT 11, 1, 'Rejected'			, 'Rejected?'	, NULL			, NULL				, 5, 0, 'SELECT ID=1, Name=''Yes'' UNION SELECT 2, ''No'''
	UNION SELECT 12, 1, 'Status'			, 'Status'		, NULL			, 'PrintStatusID'	, 2, 0, 'SELECT ID, Name=OrderStatus FROM tblOrderStatus ORDER BY StatusNum'
GO

CREATE VIEW viewReportColumnDefinition AS
	SELECT RCD.ID
		, RCD.ReportID
		, RCD.DataField
		, Caption = ISNULL(RCD.Caption, RCD.DataField)
		, RCD.DataFormat
		, RCD.FilterDataField
		, RCD.FilterTypeID
		, RCD.FilterDropDownSql
		, RCD.FilterAllowCustomText
		, RFT.FilterOperatorID_CSV
		, FilterOperatorID_CSV_Delim = ',' + RFT.FilterOperatorID_CSV + ','
	FROM tblReportColumnDefinition RCD
	JOIN tblReportFilterType RFT ON RFT.ID = RCD.FilterTypeID
GO
GRANT SELECT ON viewReportColumnDefinition TO dispatchcrude_iis_acct
GO

CREATE TABLE tblUserReportDefinition
(
  ID int identity(1, 1) NOT NULL CONSTRAINT PK_UserReportDefinition PRIMARY KEY
, Name varchar(50) NOT NULL
, ReportID int NOT NULL CONSTRAINT FK_UserReportDefinition_ReportID FOREIGN KEY REFERENCES tblReportDefinition(ID)
, UserName nvarchar(256) NULL --CONSTRAINT FK_UserReportDefinition_UserName FOREIGN KEY REFERENCES aspnet_Users(UserName)
, CreateDateUTC datetime NOT NULL CONSTRAINT DF_UserReportDefinition_CreateDateUTC DEFAULT (getutcdate())
, CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_UserReportDefinition_CreatedByUser DEFAULT (SUSER_NAME())
, LastChangeDateUTC datetime NULL
, LastChangedByUser varchar(100) NULL
)
GO
CREATE UNIQUE INDEX udxUserReportDefinition_Name ON tblUserReportDefinition(Name)
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblUserReportDefinition TO dispatchcrude_iis_acct
GO

CREATE TABLE tblUserReportColumnDefinition
(
  ID int identity(1, 1) NOT NULL CONSTRAINT PK_UserReportColumnDefinition PRIMARY KEY
, UserReportID int NOT NULL 
	CONSTRAINT FK_UserReportColumnDefinition_UserReport FOREIGN KEY REFERENCES tblUserReportDefinition(ID)
, ReportColumnID int NOT NULL 
	CONSTRAINT FK_UserReportColumnDefinition_ReportColumn FOREIGN KEY REFERENCES tblReportColumnDefinition(ID)
, Caption varchar(50) NULL
, SortNum int NULL
, DataFormat varchar(50) NULL
, FilterOperatorID int NOT NULL
	CONSTRAINT FK_UserReportColumnDefinition_FilterOperator FOREIGN KEY REFERENCES tblReportFilterOperator(ID) 
	CONSTRAINT DF_UserReportColumnDefinition_FilterOperator DEFAULT (1) -- default to 1 = NO FILTER
, FilterValue1 varchar(255) NULL
, FilterValue2 varchar(255) NULL
, CreateDateUTC datetime NOT NULL CONSTRAINT DF_UserReportColumnDefinition_CreateDateUTC DEFAULT (getutcdate())
, CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_UserReportColumnDefinition_CreatedByUser DEFAULT (SUSER_NAME())
, LastChangeDateUTC datetime NULL
, LastChangedByUser varchar(100) NULL
)
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblUserReportColumnDefinition TO dispatchcrude_iis_acct
GO

CREATE VIEW viewUserReportColumnDefinition AS
	SELECT URCD.*
		, RCD.DataField
		, RCD.Caption AS BaseCaption
		, RCD.FilterTypeID
		, RCD.FilterDataField
		, RCD.FilterDropDownSql
		, RCD.FilterAllowCustomText
		, FilterOperator = RFO.Name
		, FilterValues = CASE 
				WHEN RFO.ID IN (1,4,5) OR RCD.FilterAllowCustomText = 1 THEN FilterValue1 + ISNULL(' - ' + FilterValue2, '')
				ELSE 
					CASE WHEN FilterValue1 IS NOT NULL THEN '&lt;filtered&gt;' 
						 ELSE NULL 
					END 
			END
	FROM tblUserReportColumnDefinition URCD
	JOIN tblReportColumnDefinition RCD ON RCD.ID = URCD.ReportColumnID
	JOIN tblReportFilterOperator RFO ON RFO.ID = URCD.FilterOperatorID
GO
GRANT SELECT ON viewUserReportColumnDefinition TO dispatchcrude_iis_acct
GO

COMMIT
SET NOEXEC OFF

-- test data
insert into tblUserReportDefinition (Name, ReportID, UserName)
	select 'test report', 1, NULL
insert into tblUserReportColumnDefinition (UserReportID, ReportColumnID, FilterOperatorID)
	select 1, 1, 1
