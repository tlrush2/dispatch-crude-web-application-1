SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.6.0'
SELECT  @NewVersion = '4.6.1'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'JT-3108 - Rename Carrier Rule for Require DVIR'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

--Rename Carrier rule type and update decription to match function of rule type
UPDATE tblCarrierRuleType
SET Name = 'Require Pre-Trip DVIR'
	, Description = 'Flag to require driver to fill out a DVIR when switching to HOS on duty status after a daily reset'
WHERE Name = 'Require DVIR'



COMMIT
SET NOEXEC OFF