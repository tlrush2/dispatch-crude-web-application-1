SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.4.13'
SELECT  @NewVersion = '4.4.13.1'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'JT-424 & JT-432: Add error log permission to _DCSupport group.  Add permission for Report Center XML definition file import/export.'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


-- Add the error log role for support group users...not really sure why they didn't already have this though
INSERT INTO aspnet_RolesInGroups (RoleId, GroupId)
SELECT (SELECT RoleId FROM aspnet_Roles WHERE RoleName = 'viewElmahErrorLog'), (SELECT GroupId FROM aspnet_Groups WHERE GroupName = '_DCSupport')
GO

-- Add permission for XML import/export of report center definitions
EXEC spAddNewPermission 'importExportReportCenterDefinitions','Allow user to import/export Report Center XML definition files','Report Center','Import/Export Definitions'
GO


COMMIT
SET NOEXEC OFF