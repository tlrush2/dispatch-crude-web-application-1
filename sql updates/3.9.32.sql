
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.9.31'
SELECT  @NewVersion = '3.9.32'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Default Final columns in Report Center to show blanks/0s when not approved'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


-- JAE 12/8/2015 Update 'Final' column definition fields to display zeroes/nulls/blanks when an order is not yet approved

--TRIP | MILES | Actual Miles Final	
UPDATE tblReportColumnDefinition 
SET DataField = 'SELECT CASE WHEN OA.Approved=1 THEN isnull(OA.OverrideActualMiles, RS.ActualMiles) ELSE NULL END FROM tblOrderApproval OA WHERE OA.OrderID = RS.ID'
WHERE ID = 90010

--TRIP | TIMESTAMPS | Origin Min Final	
UPDATE tblReportColumnDefinition 
SET DataField = 'SELECT CASE WHEN OA.Approved=1 THEN isnull(OA.OverrideOriginMinutes, RS.OriginMinutes) ELSE NULL END FROM tblOrderApproval OA WHERE OA.OrderID = RS.ID'
WHERE ID = 90011

--TRIP | TIMESTAMPS | Total Min Final	
UPDATE tblReportColumnDefinition 
SET DataField = 'SELECT CASE WHEN OA.Approved=1 THEN coalesce(OA.OverrideOriginMinutes, RS.OriginMinutes, 0) + coalesce(OA.OverrideDestMinutes, RS.DestMinutes, 0) ELSE NULL END FROM tblOrderApproval OA WHERE OA.OrderID = RS.ID'
WHERE ID = 90013

--ORIGIN | GENERAL | H2S Final	
UPDATE tblReportColumnDefinition 
SET DataField = 'SELECT CASE WHEN OA.Approved=1 THEN cast(CASE WHEN isnull(OA.OverrideH2S, 0) = 1 THEN 0 ELSE RS.H2S END as bit) ELSE NULL END FROM tblOrderApproval OA WHERE OA.OrderID = RS.ID'	
WHERE ID = 90014

--GENERAL | Chain-Up Final	
UPDATE tblReportColumnDefinition 
SET DataField = 'SELECT CASE WHEN OA.Approved=1 THEN cast(CASE WHEN isnull(OA.OverrideChainup, 0) = 1 THEN 0 ELSE RS.Chainup END as bit) ELSE NULL END FROM tblOrderApproval OA WHERE OA.OrderID = RS.ID'	
WHERE ID = 90015

--TRIP | Reroute Count Final	
UPDATE tblReportColumnDefinition 
SET DataField = 'SELECT CASE WHEN OA.Approved=1 THEN CASE WHEN isnull(OA.OverrideReroute, 0) = 1 THEN 0 ELSE RS.RerouteCount END ELSE NULL END FROM tblOrderApproval OA WHERE OA.OrderID = RS.ID'
WHERE ID = 90016

--TRIP | TIMESTAMPS | Dest Min Final	
UPDATE tblReportColumnDefinition 
SET DataField = 'SELECT CASE WHEN OA.Approved=1 THEN isnull(OA.OverrideDestMinutes, RS.DestMinutes) ELSE NULL END FROM tblOrderApproval OA WHERE OA.OrderID = RS.ID'
WHERE ID = 90017

--TRIP | TRANSFER | Transfer Final Pickup Driver %	
UPDATE tblReportColumnDefinition 
SET DataField = 'SELECT CASE WHEN OA.Approved=1 THEN COALESCE(OA.OverrideTransferPercentComplete, RS.TransferPercent, 100) ELSE NULL END FROM tblOrderApproval OA WHERE OA.OrderID = RS.ID',
FilterDataField = NULL
WHERE ID = 90033

--TRIP | TRANSFER | Transfer Final Deliver Driver %	
UPDATE tblReportColumnDefinition 
SET DataField = 'SELECT CASE WHEN OA.Approved=1 THEN 100 - COALESCE(OA.OverrideTransferPercentComplete, RS.TransferPercent, 100) ELSE NULL END FROM tblOrderApproval OA WHERE OA.OrderID = RS.ID',
FilterDataField = NULL
WHERE ID = 90034


--Clean up column definitions to display miles/min/totals only once and other fields all the time
UPDATE tblReportColumnDefinition 
SET OrderSingleExport = 1 
WHERE ID IN (41, 90010, 17, 22, 90011, 90017, 90012, 90013, 90016, 90029, 90030)

UPDATE tblReportColumnDefinition 
SET OrderSingleExport = 0 
WHERE ID IN (90028)

COMMIT
SET NOEXEC OFF