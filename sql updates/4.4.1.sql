SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.4.0'
SELECT  @NewVersion = '4.4.1'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'Add Destination Chain-Up'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO



-- update tables
EXEC sp_rename 'tblOrder.Chainup', 'OriginChainup', 'COLUMN'
GO
ALTER TABLE tblOrder ADD DestChainup BIT NOT NULL CONSTRAINT DF_Order_DestChainup DEFAULT (0)
GO

EXEC sp_rename 'tblOrderDBAudit.Chainup', 'OriginChainup', 'COLUMN'
GO
ALTER TABLE tblOrderDBAudit ADD DestChainup BIT NOT NULL CONSTRAINT DF_OrderDBAudit_DestChainup DEFAULT (0)
GO

EXEC sp_rename 'tblOrderApproval.OverrideChainup', 'OverrideOriginChainup', 'COLUMN'
GO
ALTER TABLE tblOrderApproval ADD OverrideDestChainup BIT NULL
GO

-- add assessorial rate
SET IDENTITY_INSERT tblAssessorialRateType ON
INSERT INTO tblAssessorialRateType (ID, Name, IsSystem, CreateDateUTC, CreatedByUser)
VALUES (5, 'Destination Chain Up', 1, GETUTCDATE(), 'System')
SET IDENTITY_INSERT tblAssessorialRateType OFF
UPDATE tblAssessorialRateType SET Name = 'Origin Chain Up' WHERE ID = 1

GO

exec sp_refreshview viewOrderBase
GO
exec sp_refreshview viewOrder
GO
exec sp_refreshview viewOrderLocalDates
GO

/*************************************************
-- Date Created: 2015/08/13
-- Author: Kevin Alons
-- Purpose: put all Auto-Approve logic in one module (function)
-- Changes:
	- 3.9.22 - 2015/11/05 - KDA+JAE - add logic to not auto-approve Transferred orders
	- 3.11.17.2 - 2015/04/18 - JAE - Added producer settlement to check
	- 4.4.1 - 2016-11-04 - JAE - Add Destination Chainup
*************************************************/
ALTER FUNCTION fnIsValidAutoApprove
(
  @StatusID int
, @ShipperBatchID int
, @CarrierBatchID int
, @ProducerBatchID int
, @ShipperWaitBillableMin int
, @CarrierWaitBillableMin int
, @WaitFeeParameterID int
, @H2S bit
, @OriginChainup bit
, @DestChainup bit
, @Rerouted bit
, @Transferred bit
, @Approved bit
) RETURNS varchar(1000) AS
BEGIN
	DECLARE @ret varchar(1000)
	SELECT @ret = CASE WHEN ISNULL(@StatusID, 0) NOT IN (4) THEN '|Order not in Audited status' ELSE '' END
		+ CASE WHEN @ShipperBatchID IS NOT NULL THEN '|Order already Shipper Settled' ELSE '' END
		+ CASE WHEN @CarrierBatchID IS NOT NULL THEN '|Order already Carrier Settled' ELSE '' END
		+ CASE WHEN @ProducerBatchID IS NOT NULL THEN '|Order already Producer Settled' ELSE '' END
		+ CASE WHEN ISNULL(@ShipperWaitBillableMin, 0) > 0 THEN '|Order has Shipper Wait Billable Minutes' ELSE '' END
		+ CASE WHEN ISNULL(@CarrierWaitBillableMin, 0) > 0 THEN '|Order has Carrier Wait Billable Minutes' ELSE '' END
		+ CASE WHEN @WaitFeeParameterID IS NULL THEN '|Order does not have matching Wait Fee Parameter Rule' ELSE '' END
		+ CASE WHEN ISNULL(@H2S, 0) = 1 THEN '|Order has H2S = true' ELSE '' END
		+ CASE WHEN ISNULL(@OriginChainup, 0) = 1 THEN '|Order has Origin Chainup = true' ELSE '' END
		+ CASE WHEN ISNULL(@DestChainup, 0) = 1 THEN '|Order has Destination Chainup = true' ELSE '' END
		+ CASE WHEN ISNULL(@Rerouted, 0) = 1 THEN '|Order has Rerouted = true' ELSE '' END
		+ CASE WHEN ISNULL(@Transferred, 0) = 1 THEN '|Order transferred' ELSE '' END
		+ CASE WHEN ISNULL(@Approved, 0) = 1 THEN '|Order is already Approved' ELSE '' END
	RETURN NULLIF(RTRIM(REPLACE(@ret, '|', char(13) + char(10))), '')
END


GO


/*******************************************
 Date Created: 31 Aug 2013
 Author: Kevin Alons
 Purpose: return driver editable Order data for Driver App sync
 Changes: 
 - 3.8.9	- 07/24/15	 - GSM	- Added TickeTypeID field (was read-only before)
 - 3.7.4	- 05/08/15	 - GSM	- Added Rack/Bay field
 - 3.7.11	- 5/18/2015	 - KDA	- renamed RackBay to DestRackBay
 - 3.9.19.3 - 2015/09/29 - GSM	- remove transaction on Order.Accept|Pickup|Deliver LastChangeDateUTC fields (no longer use max of O.LastChangeDateUTC)
 - 3.9.38	- 2016/01/03 - KDA	- add OriginWeightNetUnits
 - 3.9.38	- 2016/01/11 - JAE	- added destination weight fields
 - 3.10.2.1	- 2015/01/28 - JAE	- Added clause to always send dispatched orders
 - 3.10.5	- 2015/01/30 - KDA	- remove workaround clause to always send DISPATCHED orders
 - 3.11.2    - 2015/02/20 - KDA+ - add @OrdersPresent table parameter, and use in logic
								- eliminate @LastChangeDateUTC parameter
								- no longer return DELETED records, only OrderReadonly records are turned when deleted (which will cause all related Driver App records to purge)
 - 4.4.1	- 2016/11/04 - JAE	- Add Destination Chainup
*******************************************/
ALTER FUNCTION fnOrderEdit_DriverApp(@DriverID int, @OrdersPresent OrdersPresent READONLY) RETURNS TABLE AS
RETURN 
      SELECT O.ID
            , O.StatusID
            , O.TruckID
            , O.TrailerID
            , O.Trailer2ID
            , O.OriginBOLNum
            , O.OriginArriveTimeUTC
            , O.OriginDepartTimeUTC
            , O.OriginMinutes
            , O.OriginWaitReasonID
            , O.OriginWaitNotes
            , O.OriginTruckMileage
            , O.OriginGrossUnits
            , O.OriginGrossStdUnits
            , O.OriginNetUnits
            , O.OriginWeightNetUnits
            , O.DestWeightGrossUnits
            , O.DestWeightTareUnits
            , O.DestWeightNetUnits
            , O.OriginChainUp
            , O.DestChainUp
            , O.Rejected
            , O.RejectReasonID
            , O.RejectNotes
            , O.OriginTankNum
            , O.DestArriveTimeUTC
            , O.DestDepartTimeUTC
            , O.DestMinutes
            , O.DestWaitReasonID
            , O.DestWaitNotes
            , O.DestBOLNum
            , O.DestTruckMileage
            , O.DestGrossUnits
            , O.DestNetUnits
            , O.DestProductTemp
            , O.DestProductBSW
            , O.DestProductGravity
            , O.DestOpenMeterUnits
            , O.DestCloseMeterUnits
            , O.CarrierTicketNum
            , O.AcceptLastChangeDateUTC 
            , O.PickupLastChangeDateUTC 
            , O.DeliverLastChangeDateUTC
            , O.PickupPrintStatusID
            , O.DeliverPrintStatusID
            , O.PickupPrintDateUTC
            , O.DeliverPrintDateUTC
            , O.PickupDriverNotes
            , O.DeliverDriverNotes
            , O.DestRackBay
            , O.TicketTypeID
            , OELC = O.LastChangeDateUTC /* "OELC" = "OrderEditLastChangeDateUTC" is named to match the OrderEdit model */
      FROM dbo.tblOrder O
	  LEFT JOIN @OrdersPresent OP ON OP.ID = O.ID
	  LEFT JOIN tblOrderAppChanges OAC ON OAC.OrderID = O.ID
      WHERE DriverID = @driverID AND O.StatusID IN (2,7,8,3) AND O.DeleteDateUTC IS NULL
		/* 3.11.2 - change to ensure that any order missing or out-of-date on the tablet is sync'ed */
        AND (OP.ID IS NULL OR cast(O.LastChangeDateUTC as smalldatetime) > cast(isnull(OP.OELastChangeDateUTC, '1/1/1900') as smalldatetime))

GO

/************************************************
 Author:		Kevin Alons
 Create date: 19 Dec 2012
 Description:	trigger to 
			1) validate any changes, and fail the update if invalid changes are submitted
			2) add a unique, incrementing OrderNum to each new Order (manual Identity column)
			3) recompute wait times (origin|destination based on times provided)
			4) generate route table entry for any newly used origin-destination combination
			5) keep Order consistent with Origin.Producer|Operator|UOM & Dest.UOM then the Origin|Dest change
			6) roll the PrintStatus back to NOTFINALIZED (0) when status is rolled back
			7) when DriverID changes, mark those orders AS DELETED and clone the order with the new DriverID and associated records
			8) update any ticket quantities for open orders when the UOM is changed for the Origin
			9) update the Driver.Truck\Trailer\Trailer2 defaults & related DISPATCHED orders when driver updates them on an order
-REMOVED	10) update the Pickup/DeliverLastChangeDateUTC when Origin/Destination is changed (respectively)
			11) (re) apply Settlement Amounts to orders not yet fully settled when status is changed to DELIVERED
			12) if DBAudit is turned on, save an audit record for this Order change
 Changes: 
-- 3.7.4	- 2015/05/08	- GSM	- Added Rack/Bay field to DBAudit logic
-- 3.7.7	- 15 May 2015	- KDA	- REMOVE #10 above, instead update the LastChangeDateUTC whenever an Origin or Destination is changed
-- 3.7.11	- 2015/05/18	- KDA	- generally use GETUTCDATE for all LastChangeDateUTC revisions (instead of related Order.LastChangeDateUTC, etc)
-- 3.7.12	- 5/19/2015		- KDA	- update any existing OrderTicket records when the Order is DISPATCHED to a driver (so the Driver App will ALWAYS receive the existing TICKETS from the GAUGER)
-- 3.7.23	- 06/05/2015	- GSM	- DCDRV-154: Ticket Type following Origin Change
-- 3.7.23	- 06/05/2015	- GSM	- DCWEB-530 - ensure ASSIGNED orders with a driver defined are updated to DISPATCHED
-- 3.8.1	- 2015/07/04	- KDA	- purge any Driver|Gauger App ZPL related SYNC change records when order is AUDITED
-- 3.9.0	- 2015/08/20	- KDA	- add invocation of spAutoAuditOrder for DELIVERED orders (that qualify) when the Auto-Audit global setting value is TRUE
									- add invocation of spAutoApproveOrder for AUDITED orders (that qualify)
-- 3.9.2	- 2015/08/25	- KDA	- appropriately use new tblOrder.OrderDate date column
-- 3.9.4	- 2015/08/30	- KDA	- performanc optimization to prevent Orderdate computation if no timestamp fields are yet populated
-- 3.9.5	- 2015/08/31	- KDA	- more extensive performance optimizations to minimize performance ramifications or computing OrderDate dynamically from OrderRule
-- 3.9.13	- 2015/09/01	- KDA	- add (but commented out) some code to validate Arrive|Depart time being present when required
									- allow orders to be rolled back from AUDITED to DELIVERED when in AUTO-AUDIT mode
									- always AUTO UNAPPROVE orders when the status is reverted from AUDITED to DELIVERED 
-- 3.9.19	- 2015/09/23	- KDA	- remove obsolete reference to tblDriverAppPrintTicketTemplate
-- 3.9.20	- 2015/09/23	- KDA	- add support for tblOrderTransfer (do not accomplish #7 above - cloning/delete orders that are driver re-assigned)
-- 3.9.29.5 - 2015/12/03	- JAE	- added DestTrailerWaterCapacity and DestRailCarNum to dbaudit trigger
-- 3.9.38	- 2015/12/21	- BB	- Add WeightNetUnits (DCWEB-972)
-- 3.9.38	- 2016/01/11	- JAE	- Recompute net for destination weights
-- 3.10.5	- 2016/01/29	- KDA	- fix to DriverApp/Gauger App purge sync records for ineligible orders [was order.StatusID IN (4), now NOT IN (valid statuses)]
-- 3.10.5.2 - 2016/02/09	- KDA	- preserve OrderDate on OrderTransfer (driver transfer)
									- ensure Accept | Pickup | Deliver LastChangeDateUTC is updated on relevant update
        	- 2016/02/11	- JAE	- Added DispatchNotes since mobile screens display that data and any changes should trigger the view to refresh
-- 3.10.10.1- 2016/02/20	- KDA	- fix to Driver Transfer logic to prevent losing carrier if setting DriverID to NULL (not immediately assigning to another Driver)
-- 3.10.10.3- 2016/02/27	- KDA	- only update Accept|Pickup|Deliver LastChangeDate values when update NOT done by DriverApp
-- 3.11.1	- 2016/02/21	- KDA	- add support for tblDriverAppPrintFooterTemplate table
-- 3.11.17.2- 2016/04/18	- JAE	- Add producer settled orders to list of uneditable orders 
-- 3.11.20.3 (3.13.1.1 & 3.13.7) - 2016/05/09 - BB - Add Job and Contract number fields to list of fields to be copied when orders are transferred
-- 3.13.1	- 2016/07/04	- KDA	- eliminate direct auto-Audit/Approve/Rate process - instead add record to tblOrderProcessStatusChange table (for later processing)
									- add timestamp to PRINT FIRE/COMPLETED messages
-- 3.13.1.1 - 2016/07/05	- JAE	- Added back job/contract number fields. Also added to audit trigger process
-- 3.13.3	- 2016/07/10	- KDA	- only RE-ASSIGN orders after the order has been synced to a Driver at least once
-- 3.13.7   - 2016/07/05	- JAE	- Re-Added back job/contract number fields. Also re-added to audit trigger process
-- 4.1.9.2	- 2016/09/27	- JAE	- Fixed trigger to update miles when null (during create)
-- 4.4.1	- 2016/11/04	- JAE	- Add Destination Chainup
************************************************/
ALTER TRIGGER trigOrder_IU ON tblOrder AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @changesFound bit
	
		-- Note: the TRIGGER_NESTLEVEL( OBJECT_ID('trigOrder_IU') ) = 1 statement prevents recursion
	IF (TRIGGER_NESTLEVEL( OBJECT_ID('trigOrder_IU')) = 1 AND TRIGGER_NESTLEVEL( OBJECT_ID('trigOrderTicket_IU')) = 0) BEGIN

		/**********  START OF VALIDATION SECTION ************************/

		IF (UPDATE(OrderNum) 
			OR UPDATE(PriorityID) 
			OR UPDATE(DueDate) 
			OR UPDATE(RouteID) 
			OR UPDATE(OriginID) 
			OR UPDATE(OriginArriveTimeUTC) 
			OR UPDATE(OriginDepartTimeUTC) 
			OR UPDATE(OriginMinutes) 
			OR UPDATE(OriginWaitNotes) 
			OR UPDATE(OriginBOLNum) 
			OR UPDATE(OriginGrossUnits) 
			OR UPDATE(OriginNetUnits) 
			--OR UPDATE(OriginWeightGrossUnits) -- 3.9.38  Per Maverick 12/21/15 we do not need gross on the order level.
			OR UPDATE(OriginWeightNetUnits) -- 3.9.38
			OR UPDATE(DestinationID) 
			OR UPDATE(DestArriveTimeUTC) 
			OR UPDATE(DestDepartTimeUTC) 
			OR UPDATE(DestMinutes) 
			OR UPDATE(DestWaitNotes) 
			OR UPDATE(DestBOLNum) 
			OR UPDATE(DestGrossUnits) 
			OR UPDATE(DestNetUnits) 
			OR UPDATE(CustomerID) 
			OR UPDATE(CarrierID) 
			OR UPDATE(DriverID) 
			OR UPDATE(TruckID)
			OR UPDATE(TrailerID) 
			OR UPDATE(Trailer2ID) 
			OR UPDATE(OperatorID) 
			OR UPDATE(PumperID) 
			OR UPDATE(TicketTypeID) 
			OR UPDATE(Rejected) 
			OR UPDATE(RejectNotes) 
			OR UPDATE(OriginChainUp) 
			OR UPDATE(DestChainUp) 
			-- allow this to be changed even in audit status
			--OR UPDATE(OriginTruckMileage) 
			OR UPDATE(OriginTankNum) 
			-- allow this to be changed even in audit status
			--OR UPDATE(DestTruckMileage) 
			OR UPDATE(CarrierTicketNum) 
			OR UPDATE(AuditNotes) 
			OR UPDATE(CreateDateUTC) 
			-- it is permissible to change the ActualMiles value on Audited orders up to the point the Order has been Shipper-Settled
			OR (UPDATE(ActualMiles) AND EXISTS (SELECT * FROM deleted i JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = i.ID WHERE OSS.BatchID IS NOT NULL))
			OR UPDATE(ProducerID) 
			OR UPDATE(CreatedByUser) 
			OR UPDATE(LastChangeDateUTC) 
			OR UPDATE(LastChangedByUser) 
			OR UPDATE(DeleteDateUTC) 
			OR UPDATE(DeletedByUser) 
			OR UPDATE(DestProductBSW)
			OR UPDATE(DestProductGravity) 
			OR UPDATE(DestProductTemp) 
			OR UPDATE(ProductID) 
			OR UPDATE(AcceptLastChangeDateUTC) 
			OR UPDATE(PickupLastChangeDateUTC) 
			OR UPDATE(DeliverLastChangeDateUTC) 
			OR UPDATE(OriginUomID) 
			OR UPDATE(DestUomID) 
			OR UPDATE(PickupPrintStatusID) 
			OR UPDATE(DeliverPrintStatusID)
			OR UPDATE(PickupPrintDateUTC) 
			OR UPDATE(DeliverPrintDateUTC) 
			OR UPDATE(OriginTankID) 
			OR UPDATE(OriginGrossStdUnits) 
			OR UPDATE(DispatchConfirmNum) 
			OR UPDATE(DispatchNotes)
			OR UPDATE(PickupDriverNotes)
			OR UPDATE(DeliverDriverNotes)
			OR UPDATE(OriginWaitReasonID)
			OR UPDATE(DestWaitReasonID)
			OR UPDATE(RejectReasonID) 
			OR UPDATE(DestOpenMeterUnits) 
			OR UPDATE(DestCloseMeterUnits)
			OR UPDATE(DestRackBay)
			OR UPDATE(OrderDate)
			OR UPDATE(DestWeightGrossUnits) 
			OR UPDATE(DestWeightTareUnits)
			OR UPDATE(OriginSulfurContent)
		)
			--OR UPDATE(ReassignKey)) we don't do any work if this value changes so just ignore it
		BEGIN
			-- only allow the StatusID value to be changed on an audited order
			IF EXISTS (SELECT * FROM deleted WHERE StatusID = 4)
			BEGIN
				-- if the "Allow Audited Order Update (temp only)" setting is TRUE then only warn when this occurs
				IF (dbo.fnToBool(dbo.fnSettingValue(35)) = 1)
					PRINT 'AUDITED orders is being modified - please investigate why!'
				ELSE BEGIN
					-- otherwise (normal behavior) - prevent AUDITED orders from being modified (except to UN-AUDIT them)
					RAISERROR('AUDITED orders cannot be modified!', 16, 1)
					IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
					RETURN
				END
			END
		END
		ELSE IF NOT UPDATE(StatusID) -- NO CHANGES DETECTED SO JUST QUIT
			RETURN

		PRINT 'trigOrder_IU FIRED: ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
					
		-- ensure the Origin and Destinations are both specified unless the Status is:
		--   (Generated, Assigned, Dispatched or Declined)
		IF  EXISTS(SELECT * FROM inserted O WHERE (O.OriginID IS NULL OR O.DestinationID IS NULL) AND O.StatusID NOT IN (-10,1,2,9))
		BEGIN
			RAISERROR('Invalid Order Status when the Origin and/or Or Destination is not specified', 16, 1)
			IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
			RETURN
		END
		/* this is commented out until we get the Android issues closer to resolved 
		IF EXISTS (SELECT * FROM inserted WHERE StatusID IN (8, 3, 4) AND OriginArriveTimeUTC IS NULL OR OriginDepartTimeUTC IS NULL)
		BEGIN
			RAISERROR('OriginArriveTimeUTC and/or OriginDepartTimeUTC missing when required', 16, 1)
			IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
			RETURN
		END
		IF EXISTS (SELECT * FROM inserted WHERE StatusID IN (3, 4) AND DestArriveTimeUTC IS NULL OR DestDepartTimeUTC IS NULL)
		BEGIN
			RAISERROR('DestArriveTimeUTC and/or DestDepartTimeUTC missing when required', 16, 1)
			IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
			RETURN
		END
		******************************************************************************/
		
		/**********  END OF VALIDATION SECTION ************************/

		WHILE (SELECT count(*) FROM inserted i JOIN tblOrder O ON O.ID = i.ID WHERE O.OrderNum IS NULL) > 0
		BEGIN
			UPDATE tblOrder 
			  SET OrderNum = isnull((SELECT max(OrderNum) FROM tblOrder), 100000) + 1
				, CreateDateUTC = getutcdate()
			WHERE ID = (SELECT min(O.ID) FROM tblOrder O JOIN inserted i ON i.ID = O.ID WHERE O.OrderNum IS NULL)
		END
		
		-- re-compute the OriginMinutes (in case the website failed to compute it properly)
		IF UPDATE(OriginArriveTimeUTC) OR UPDATE(OriginDepartTimeUTC)
		BEGIN
			UPDATE tblOrder SET OriginMinutes = datediff(minute, i.OriginArriveTimeUTC, i.OriginDepartTimeUTC)
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
		END
		-- re-compute the DestMinutes (in case the website failed to compute it properly)
		IF UPDATE(DestArriveTimeUTC) OR UPDATE(DestDepartTimeUTC)
		BEGIN
			UPDATE tblOrder SET DestMinutes = datediff(minute, i.DestArriveTimeUTC, i.DestDepartTimeUTC)
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
		END
		
		/* ensure any changes to the order always update the Order.OrderDate field */
		IF (UPDATE(CustomerID) 
			OR UPDATE(CarrierID) 
			OR UPDATE(ProductID) 
			OR UPDATE(OriginID) 
			OR UPDATE(DestinationID) 
			OR UPDATE(ProducerID) 
			OR UPDATE(OriginArriveTimeUTC) OR UPDATE(OriginDepartTimeUTC)
			OR UPDATE(DestArriveTimeUTC) OR UPDATE(DestDepartTimeUTC))
		BEGIN
			UPDATE tblOrder 
			  SET OrderDate = dbo.fnOrderDate(O.ID)
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
			JOIN deleted d ON d.ID = i.ID
			LEFT JOIN tblOrderSettlementCarrier OSC ON OSC.OrderID = i.ID
			LEFT JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = i.ID
			LEFT JOIN tblOrderSettlementProducer OSP ON OSP.OrderID = i.ID
			WHERE i.StatusID <> 4 
			  -- the order has at least 1 valid timestamp
			  AND (i.OriginArriveTimeUTC IS NOT NULL OR i.OriginDepartTimeUTC IS NOT NULL OR i.DestArriveTimeUTC IS NOT NULL OR i.DestDepartTimeUTC IS NOT NULL)
			  -- the order is not yet settled
			  AND OSC.BatchID IS NULL
			  AND OSS.BatchID IS NULL
			  AND OSP.BatchID IS NULL
		END
		
		-- ensure missing Routes are created and assigned for the specified Origin/Destination combination
		IF UPDATE(OriginID) OR UPDATE(DestinationID)
		BEGIN
			-- create any missing Route records
			INSERT INTO tblRoute (OriginID, DestinationID, CreateDateUTC, CreatedByUser)
				SELECT i.OriginID, i.DestinationID, GETUTCDATE(), ISNULL(i.LastChangedByUser, i.CreatedByUser)
				FROM inserted i
				LEFT JOIN tblRoute R ON R.OriginID = i.OriginID AND R.DestinationID = i.DestinationID
				WHERE i.OriginID IS NOT NULL AND i.DestinationID IS NOT NULL AND R.ID IS NULL
			
			-- ensure the Order records refer to the correct Route (ID)
			UPDATE tblOrder SET RouteID = R.ID
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
			LEFT JOIN tblRoute R ON R.OriginID = O.OriginID AND R.DestinationID = O.DestinationID
			WHERE O.RouteID IS NULL OR O.RouteID <> R.ID
			
			-- update the ActualMiles from the related Route
			UPDATE tblOrder SET ActualMiles = R.ActualMiles
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
			JOIN tblRoute R ON R.ID = O.RouteID
			WHERE O.ActualMiles IS NULL OR O.ActualMiles <> R.ActualMiles
		END
		
		IF (UPDATE(OriginID))
		BEGIN
			-- update Order.ProducerID/OperatorID/PumperID/SulfurContent to match what is assigned to the new Origin
			UPDATE tblOrder 
				SET TicketTypeID = OO.TicketTypeID
					, ProducerID = OO.ProducerID
					, OperatorID = OO.OperatorID
					, PumperID = OO.PumperID
					, OriginSulfurContent = OO.SulfurContent
					, LastChangeDateUTC = GETUTCDATE() 
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			WHERE d.OriginID <> O.OriginID

			-- update Order.OriginUomID to match what is assigned to the new Origin (quantities are updated below)
			UPDATE tblOrder SET OriginUomID = OO.UomID
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			WHERE d.OriginID <> O.OriginID AND d.OriginUomID <> OO.UomID
		END
		
		-- keep the DestUomID in sync with the Destination (units are updated below)
		IF (UPDATE(DestinationID))
		BEGIN
			-- update Order.DestUomID to match what is assigned to the new Destination
			UPDATE tblOrder 
			  SET DestUomID = DD.UomID
				, LastChangeDateUTC = GETUTCDATE()
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID
			JOIN tblDestination DD ON DD.ID = O.DestinationID
			WHERE d.DestinationID <> O.DestinationID AND d.DestUomID <> DD.UomID
		END
		
		-- ensure that any set PrintStatusID values are reset to 0 when backing up the general StatusID value
		IF (UPDATE(StatusID))
		BEGIN
			UPDATE tblOrder 
			  SET DeliverPrintStatusID = 0 
				, LastChangeDateUTC = GETUTCDATE()
			  FROM tblOrder O
			  JOIN deleted d ON d.ID = O.ID
			WHERE O.StatusID <> d.StatusID AND O.StatusID IN (-10, 1, 2, 7, 8) -- Generated, Assigned, Dispatched, Accepted, Picked-Up

			UPDATE tblOrder 
			  SET PickupPrintStatusID = 0 
				, LastChangeDateUTC = GETUTCDATE()
			  FROM tblOrder O
			  JOIN deleted d ON d.ID = O.ID
			WHERE O.StatusID <> d.StatusID AND O.StatusID IN (-10, 1, 2, 7) -- Generated, Assigned, Dispatched, Accepted
			
			/* ensure that any orders that are DISPATCHED - any existing orders are TOUCHED so they are syncable to the Driver App */
			UPDATE tblOrderTicket
				SET LastChangeDateUTC = GETUTCDATE()
			WHERE OrderID IN (
				SELECT i.ID 
				FROM inserted i
				JOIN deleted d ON d.ID = i.ID
				WHERE i.StatusID <> d.StatusID AND i.StatusID IN (2) -- DISPATCHED
			)
		END

		-- just updating the tickets here, trigOrderTicket_IU will in turn update this table
		-- ensure any existing OrderTickets are recomputed with the new OriginUOM
		UPDATE tblOrderTicket
		  SET GrossUnits = dbo.fnConvertUOM(GrossUnits, d.OriginUomID, O.OriginUomID)
			, NetUnits = dbo.fnConvertUOM(NetUnits, d.OriginUomID, O.OriginUomID)
			-- 3.9.38 - added to also force the WeightNetUnits be recomputed
			, WeightGrossUnits = dbo.fnConvertUOM(WeightGrossUnits, d.OriginUomID, O.OriginUomID)
			, LastChangeDateUTC = GETUTCDATE()
		FROM tblOrderTicket OT
		JOIN tblOrder O ON O.ID = OT.OrderID
		JOIN deleted d ON d.ID = O.ID
		WHERE O.OriginUomID <> d.OriginUomID
		
		-- recalculate the Destination Unit values if only if the DestUomID changed but the values didn't
		UPDATE tblOrder
		  SET DestGrossUnits = dbo.fnConvertUOM(O.DestGrossUnits, d.DestUomID, O.DestUomID)
			, DestNetUnits = dbo.fnConvertUOM(O.DestNetUnits, d.DestUomID, O.DestUomID)
			, LastChangeDateUTC = GETUTCDATE()
		FROM tblOrder O
		JOIN deleted d ON d.ID = O.ID
		WHERE d.DestUomID <> O.DestUomID 
		  AND d.DestGrossUnits = O.DestGrossUnits
		  AND d.DestNetUnits = O.DestNetUnits

		/* DCWEB-530 - ensure any ASSIGNED orders with a DRIVER assigned is set to DISPATCHED */
		UPDATE tblOrder SET StatusID = 2 /*DISPATCHED*/
		FROM tblOrder O
		JOIN inserted i ON I.ID = O.ID
		WHERE i.StatusID = 1 /*ASSIGNED*/ AND i.CarrierID IS NOT NULL AND i.DriverID IS NOT NULL AND i.DeleteDateUTC IS NULL

		/**************************************************************************************************************/
		/* 3.10.5.2 - ensure the Accept|Pickup|Deliver LastChangeDateUTC values are updated when any relevant data field changes */
		UPDATE tblOrder 
		  SET AcceptLastChangeDateUTC = LastChangeDateUTC
		WHERE ID IN (
			SELECT ID FROM (
				SELECT ID, TruckID, TrailerID, Trailer2ID, DispatchNotes FROM inserted 
				EXCEPT 
				SELECT ID, TruckID, TrailerID, Trailer2ID, DispatchNotes FROM deleted
			) X
		-- only include records that didn't have the AcceptLastChangeDateUTC explicitly updated (like from the DriverApp) - 3.10.10.3
		) AND ID IN (
			SELECT ID FROM (
				SELECT ID, AcceptLastChangeDateUTC FROM inserted INTERSECT SELECT ID, AcceptLastChangeDateUTC FROM deleted
			) X
		)
		UPDATE tblOrder 
		  SET PickupLastChangeDateUTC = LastChangeDateUTC
		WHERE ID IN (
			SELECT ID FROM (
				SELECT ID, OriginArriveTimeUTC, OriginDepartTimeUTC, OriginMinutes, OriginWaitReasonID, OriginWaitNotes
                    , CarrierTicketNum, OriginBOLNum, Rejected, RejectReasonID, RejectNotes, OriginChainup, OriginTruckMileage
                    , PickupPrintStatusID, PickupPrintDateUTC, OriginGrossUnits, OriginGrossStdUnits, OriginNetUnits, OriginWeightNetUnits
                    , PickupDriverNotes, DispatchNotes
                    , TicketTypeID 
				FROM inserted 
				EXCEPT 
				SELECT ID, OriginArriveTimeUTC, OriginDepartTimeUTC, OriginMinutes, OriginWaitReasonID, OriginWaitNotes
                    , CarrierTicketNum, OriginBOLNum, Rejected, RejectReasonID, RejectNotes, OriginChainup, OriginTruckMileage
                    , PickupPrintStatusID, PickupPrintDateUTC, OriginGrossUnits, OriginGrossStdUnits, OriginNetUnits, OriginWeightNetUnits
                    , PickupDriverNotes, DispatchNotes
                    , TicketTypeID 
				FROM deleted
			) X
		-- only include records that didn't have the PickupLastChangeDateUTC explicitly updated (like from the DriverApp) - 3.10.10.3
		) AND ID IN (
			SELECT ID FROM (
				SELECT ID, PickupLastChangeDateUTC FROM inserted INTERSECT SELECT ID, PickupLastChangeDateUTC FROM deleted
			) X
		)
		UPDATE tblOrder 
		  SET DeliverLastChangeDateUTC = LastChangeDateUTC
		WHERE ID IN (
			SELECT ID FROM (
				SELECT ID, DeliverLastChangeDateUTC, DestArriveTimeUTC, DestDepartTimeUTC, DestMinutes, DestWaitReasonID, DestWaitNotes
                    , DestNetUnits, DestGrossUnits, DestChainup
                    , DestBOLNum, DestRailcarNum, DestTrailerWaterCapacity, DestOpenMeterUnits, DestCloseMeterUnits, DestProductBSW
                    , DestProductGravity, DestProductTemp, DestTruckMileage 
                    , DeliverPrintStatusID, DeliverPrintDateUTC, DeliverDriverNotes, DestRackBay
                    , DestWeightGrossUnits, DestWeightTareUnits, DestWeightNetUnits
                    , DispatchNotes 
				FROM inserted
				EXCEPT
				SELECT ID, DeliverLastChangeDateUTC, DestArriveTimeUTC, DestDepartTimeUTC, DestMinutes, DestWaitReasonID, DestWaitNotes
                    , DestNetUnits, DestGrossUnits, DestChainup
                    , DestBOLNum, DestRailcarNum, DestTrailerWaterCapacity, DestOpenMeterUnits, DestCloseMeterUnits, DestProductBSW
                    , DestProductGravity, DestProductTemp, DestTruckMileage 
                    , DeliverPrintStatusID, DeliverPrintDateUTC, DeliverDriverNotes, DestRackBay
                    , DestWeightGrossUnits, DestWeightTareUnits, DestWeightNetUnits 
                    , DispatchNotes
				FROM deleted
			) X
		-- only include records that didn't have the DeliverLastChangeDateUTC explicitly updated (like from the DriverApp) - 3.10.10.3
		) AND ID IN (
			SELECT ID FROM (
				SELECT ID, DeliverLastChangeDateUTC FROM inserted INTERSECT SELECT ID, DeliverLastChangeDateUTC FROM deleted
			) X
		)
		/**************************************************************************************************************/

		-- 3.9.38 - 2016/01/11 - JAE - recompute the Net Weight of any changed Mineral Run orders
		IF UPDATE(DestWeightGrossUnits) OR UPDATE(DestWeightTareUnits) OR UPDATE(DestUomID) BEGIN
			UPDATE tblOrder
				SET DestWeightNetUnits = O.DestWeightGrossUnits - O.DestWeightTareUnits
			FROM tblOrder O
			JOIN tblDestination D ON D.ID = O.DestinationID
			JOIN inserted i on i.ID = O.ID
			WHERE D.TicketTypeID = 9 -- Mineral Run tickets only
		END 			

		/*************************************************************************************************************/
		/* DRIVER RE-ASSIGNMENT - handle driver changes - "orphan" the order/ticket (with a new ORDERNUM) - and clone it for the new driver */
		IF (UPDATE(DriverID))
		BEGIN
			-- create cloned order/ticket records (these records will be cloned then the original reset to orphaned status)
			SELECT O.*, NewOrderNum = CAST(NULL as int)
			INTO #clone
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID AND d.DriverID IS NOT NULL AND d.DriverID <> isnull(O.DriverID, 0)
			/* 3.13.3 - only RE-ASSIGN orders AFTER they have been SYNCED at least once to a DRIVER (APP) */
			JOIN tblOrderDriverSynced ODS ON ODS.OrderID = O.ID
			/* JOIN to tblOrderTransfer so we can prevent treating an OrderTransfer "driver change" as a Orphaned Order */
			LEFT JOIN tblOrderTransfer OTR ON OTR.OrderID = O.ID  /* 3.9.20 - added */
			WHERE OTR.OrderID IS NULL

			/* generate the next OrderNum values for these new records */
			DECLARE @newOrderNum int; SET @newOrderNum = isnull((SELECT max(OrderNum) FROM tblOrder), 100000) + 1;
			DECLARE @newReassignKey int; SET @newReassignKey = isnull((SELECT max(ReassignKey) FROM tblOrder), 0) + 1;
			WHILE (SELECT count(*) FROM #clone WHERE NewOrderNum IS NULL) > 0
			BEGIN
				UPDATE #clone
				  SET NewOrderNum = @newOrderNum, ReassignKey = isnull(ReassignKey, @newReassignKey)
					, CreateDateUTC = getutcdate()
				WHERE ID = (SELECT min(ID) FROM #clone WHERE NewOrderNum IS NULL)
				-- if the @newReassignKey wasn't used above, then that # will be skipped, but this should not be an issue
				SELECT @newOrderNum = @newOrderNum + 1, @newReassignKey = @newReassignKey + 1
			END
			
			-- reset the existing records as orphaned
			UPDATE tblOrder
				SET DeleteDateUTC = isnull(O.DeleteDateUTC, GETUTCDATE()), DeletedByUser = isnull(O.DeletedByUser, O.LastChangedByUser)
					, CarrierID = d.CarrierID, DriverID = d.DriverID, TruckID = d.TruckID, TrailerID = d.TrailerID, Trailer2ID = d.Trailer2ID
					, DispatchConfirmNum = NULL
					, ReassignKey = C.ReassignKey
			FROM tblOrder O
			JOIN #clone C ON C.ID = O.ID
			JOIN deleted d on D.ID = O.id

			-- create the cloned records (which is essentially the original with a new ID and the Carrrier & default Truck|Trailers of the new driver)
			INSERT INTO tblOrder (OrderDate, OrderNum, StatusID, PriorityID, DueDate, RouteID, OriginID, OriginArriveTimeUTC, OriginDepartTimeUTC
				, OriginMinutes, OriginWaitNotes, OriginBOLNum, OriginGrossUnits, OriginNetUnits, DestinationID, DestArriveTimeUTC, DestDepartTimeUTC
				, DestMinutes, DestWaitNotes, DestBOLNum, DestGrossUnits, DestNetUnits, CustomerID, CarrierID, DriverID, TruckID, TrailerID
				, Trailer2ID, OperatorID, PumperID, TicketTypeID, Rejected, RejectNotes, OriginChainUp, OriginTruckMileage, OriginTankNum, DestTruckMileage
				, CarrierTicketNum, AuditNotes, CreateDateUTC, ActualMiles, ProducerID, CreatedByUser, LastChangeDateUTC, LastChangedByUser
				, DeleteDateUTC, DeletedByUser, DestProductBSW, DestProductGravity, DestProductTemp, ProductID, AcceptLastChangeDateUTC
				, PickupLastChangeDateUTC, DeliverLastChangeDateUTC, OriginUomID, DestUomID, PickupPrintStatusID, DeliverPrintStatusID
				, PickupPrintDateUTC, DeliverPrintDateUTC, OriginTankID, OriginGrossStdUnits, DispatchNotes, PickupDriverNotes, DeliverDriverNotes
				, DispatchConfirmNum, OriginWaitReasonID, DestWaitReasonID, RejectReasonID, DestOpenMeterUnits, DestCloseMeterUnits
				, OriginWeightNetUnits, ReassignKey, JobNumber, ContractNumber, OriginSulfurContent, DestChainup)
				SELECT OrderDate, NewOrderNum, StatusID, PriorityID, DueDate, RouteID, OriginID, OriginArriveTimeUTC, OriginDepartTimeUTC, OriginMinutes, OriginWaitNotes
					, OriginBOLNum, OriginGrossUnits, OriginNetUnits, DestinationID, DestArriveTimeUTC, DestDepartTimeUTC, DestMinutes, DestWaitNotes
					, DestBOLNum, DestGrossUnits, DestNetUnits, CustomerID, isnull(D.CarrierID, O.CarrierID), DriverID, D.TruckID, D.TrailerID, D.Trailer2ID, OperatorID
					, PumperID, TicketTypeID, Rejected, RejectNotes, OriginChainUp, OriginTruckMileage, OriginTankNum, DestTruckMileage, CarrierTicketNum
					, AuditNotes, O.CreateDateUTC, ActualMiles, ProducerID, O.CreatedByUser, GETUTCDATE(), O.LastChangedByUser, O.DeleteDateUTC, O.DeletedByUser
					, DestProductBSW, DestProductGravity, DestProductTemp, ProductID, AcceptLastChangeDateUTC, PickupLastChangeDateUTC, DeliverLastChangeDateUTC
					, OriginUomID, DestUomID, PickupPrintStatusID, DeliverPrintStatusID, PickupPrintDateUTC, DeliverPrintDateUTC, OriginTankID
					, OriginGrossStdUnits, DispatchNotes, PickupDriverNotes, DeliverDriverNotes, DispatchConfirmNum, OriginWaitReasonID, DestWaitReasonID
					, RejectReasonID, DestOpenMeterUnits, DestCloseMeterUnits, OriginWeightNetUnits, ReassignKey, JobNumber, ContractNumber, OriginSulfurContent, DestChainup
				FROM #clone O
				LEFT JOIN tblDriver D ON D.ID = O.DriverID
		
			-- cache the affected tickets
			SELECT OT.*, C.NewOrderNum, OrderDeletedByUser = C.LastChangedByUser
			INTO #cloneTicket
			FROM tblOrderTicket OT
			JOIN #clone C ON C.ID = OT.OrderID
			WHERE OT.DeleteDateUTC IS NULL
			
			-- create the cloned Order Ticket records (copy of existing ticket(s) pointed at new "original" Order)
			INSERT INTO tblOrderTicket (OrderID, CarrierTicketNum, TicketTypeID, TankNum, ProductObsGravity, ProductObsTemp, ProductBSW, OpeningGaugeFeet
				, OpeningGaugeInch, OpeningGaugeQ, ClosingGaugeFeet, ClosingGaugeInch, ClosingGaugeQ, GrossUnits, NetUnits, Rejected, RejectNotes
				, SealOff, SealOn, BOLNum, ProductHighTemp, ProductLowTemp, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser
				, DeleteDateUTC, DeletedByUser, UID, FromMobileApp, OriginTankID, BottomFeet, BottomInches, BottomQ, GrossStdUnits, RejectReasonID
				, MeterFactor, OpenMeterUnits, CloseMeterUnits, WeightTareUnits, WeightGrossUnits, WeightNetUnits)
				SELECT O.ID, CT.CarrierTicketNum, CT.TicketTypeID, CT.TankNum, CT.ProductObsGravity, CT.ProductObsTemp, CT.ProductBSW, CT.OpeningGaugeFeet
					, CT.OpeningGaugeInch, CT.OpeningGaugeQ, CT.ClosingGaugeFeet, CT.ClosingGaugeInch, CT.ClosingGaugeQ, CT.GrossUnits, CT.NetUnits
					, CT.Rejected, CT.RejectNotes, CT.SealOff, CT.SealOn, CT.BOLNum, CT.ProductHighTemp, CT.ProductLowTemp, CT.CreateDateUTC, CT.CreatedByUser
					, GETUTCDATE(), CT.LastChangedByUser, CT.DeleteDateUTC, CT.DeletedByUser, newid(), CT.FromMobileApp, CT.OriginTankID, CT.BottomFeet
					, CT.BottomInches, CT.BottomQ, CT.GrossStdUnits, CT.RejectReasonID, CT.MeterFactor, CT.OpenMeterUnits, CT.CloseMeterUnits, WeightTareUnits
					, WeightGrossUnits, WeightNetUnits
				FROM #cloneTicket CT
				JOIN tblOrder O ON O.OrderNum = CT.NewOrderNum
		END
		/*************************************************************************************************************/
		
		/* HANDLE TRUCK | TRAILER | TRAILER2 default changes when the driver updates his Truck | Trailer | Trailer2 on ACCEPTANCE */
		-- TRUCK
		IF (UPDATE(TruckID) AND EXISTS (
			SELECT i.* 
			FROM inserted i 
			JOIN deleted d ON d.ID = i.ID
			  AND i.DriverID = d.DriverID 
			  AND i.TruckID <> d.TruckID
			WHERE i.StatusID IN (7, 8, 3) -- ACCEPTED, PICKEDUP, DELIVERED
			  AND d.StatusID IN (2))) -- DISPATCHED
		BEGIN
			UPDATE tblDriver
			  SET TruckID = i.TruckID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblDriver DR
			JOIN inserted i ON i.DriverID = DR.ID
			JOIN deleted d ON d.DriverID = DR.ID
			WHERE i.TruckID <> d.TruckID
			
			UPDATE tblOrder
			  SET TruckID = i.TruckID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblOrder O
			JOIN inserted i ON i.DriverID = O.DriverID 
			  AND O.ID <> i.ID
			  AND O.StatusID IN (2) -- DISPATCHED
			  AND i.TruckID <> O.TruckID
			  AND O.DeleteDateUTC IS NULL
		END
		-- TRAILER
		IF (UPDATE(TrailerID) AND EXISTS (
			SELECT i.* 
			FROM inserted i 
			JOIN deleted d ON d.ID = i.ID
			  AND i.DriverID = d.DriverID 
			  AND i.TrailerID <> d.TrailerID
			WHERE i.StatusID IN (7, 8, 3) -- ACCEPTED, PICKEDUP, DELIVERED
			  AND d.StatusID IN (2))) -- DISPATCHED
		BEGIN
			UPDATE tblDriver
			  SET TrailerID = i.TrailerID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblDriver DR
			JOIN inserted i ON i.DriverID = DR.ID
			JOIN deleted d ON d.DriverID = DR.ID
			WHERE i.TrailerID <> d.TrailerID
			
			UPDATE tblOrder
			  SET TrailerID = i.TrailerID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblOrder O
			JOIN inserted i ON i.DriverID = O.DriverID 
			  AND O.ID <> i.ID
			  AND O.StatusID IN (2) -- DISPATCHED
			  AND i.TrailerID <> O.TrailerID
			  AND O.DeleteDateUTC IS NULL
		END
		-- TRAILER 2
		IF (UPDATE(Trailer2ID) AND EXISTS (
			SELECT i.* 
			FROM inserted i 
			JOIN deleted d ON d.ID = i.ID
			  AND i.DriverID = d.DriverID 
			  AND isnull(i.Trailer2ID, 0) <> isnull(d.Trailer2ID, 0)
			WHERE i.StatusID IN (7, 8, 3) -- ACCEPTED, PICKEDUP, DELIVERED
			  AND d.StatusID IN (2))) -- DISPATCHED
		BEGIN
			UPDATE tblDriver
			  SET TrailerID = i.TrailerID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblDriver DR
			JOIN inserted i ON i.DriverID = DR.ID
			JOIN deleted d ON d.DriverID = DR.ID
			WHERE isnull(i.Trailer2ID, 0) <> isnull(d.Trailer2ID, 0)
			
			UPDATE tblOrder
			  SET Trailer2ID = i.Trailer2ID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblOrder O
			JOIN inserted i ON i.DriverID = O.DriverID 
			  AND O.ID <> i.ID
			  AND O.StatusID IN (2) -- DISPATCHED
			  AND isnull(i.Trailer2ID, 0) <> isnull(O.Trailer2ID, 0)
			  AND O.DeleteDateUTC IS NULL
		END
		/*************************************************************************************************************/
		
		-- record any OrderProcessStatusChange records	
		IF (UPDATE(StatusID) OR UPDATE(DeliverPrintStatusID))
			INSERT INTO tblOrderProcessStatusChange (OrderID, OldStatusID, NewStatusID, StatusChangeDateUTC)
				SELECT i.ID, d.StatusID, i.StatusID, getutcdate()
				FROM inserted i
				JOIN tblPrintStatus iPS ON iPS.ID = i.DeliverPrintStatusID
				JOIN deleted d ON d.ID = i.ID
				JOIN tblPrintStatus dPS ON dPS.ID = d.DeliverPrintStatusID
				WHERE (i.StatusID = 3 AND d.StatusID <> 4 AND iPS.IsCompleted = 1 AND i.StatusID + iPS.IsCompleted <> d.StatusID + dPS.IsCompleted) -- just DELIVERED orders
				  OR (i.StatusID = 4 AND d.StatusID <> 4)	-- just AUDITED orders
				  OR (d.StatusID = 4 AND i.StatusID <> 4)	-- just UNAUDITED orders
			
		/*************************************************************************************************************/

		-- purge any DriverApp/Gauger App sync records for any orders that are not in an DRIVER APP eligible status
		IF UPDATE(StatusID) AND EXISTS (SELECT ID FROM inserted WHERE StatusID NOT IN (2,7,8,3))
		BEGIN
			DELETE FROM tblDriverAppPrintHeaderImageSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID NOT IN (2,7,8,3))
			DELETE FROM tblDriverAppPrintPickupTemplateSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID NOT IN (2,7,8,3))
			DELETE FROM tblDriverAppPrintDeliverTemplateSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID NOT IN (2,7,8,3))
			DELETE FROM tblDriverAppPrintFooterTemplateSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID NOT IN (2,7,8,3))
			DELETE FROM tblOrderAppChanges WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID NOT IN (2,7,8,3))
		END
		-- purge any DriverApp/Gauger App sync records for any orders that are not in a GAUGER APP eligible status
		IF UPDATE(StatusID) AND EXISTS (SELECT ID FROM inserted WHERE StatusID NOT IN (-9,-11))
		BEGIN
			DELETE FROM tblGaugerAppPrintHeaderImageSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID NOT IN (-9,-11))
			DELETE FROM tblGaugerAppPrintPickupTemplateSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID NOT IN (-9,-11))
			DELETE FROM tblGaugerAppPrintTicketTemplateSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID NOT IN (-9,-11))
		END
				
		--NOTE: we do not update the DestOpenMeterUnits/DestCloseMeterUnits since they don't auto update
		--  (and they simply represent a reading that is assumed to in the DestUomID UOM at entry)
		--  (if they are updated in the future [manually] the will calculate again using the DestUomID at that time)

		IF EXISTS (SELECT * FROM inserted EXCEPT SELECT * FROM deleted)
		BEGIN
			-- optionally add tblOrderDBAudit records
			BEGIN TRY
				IF EXISTS(SELECT * FROM tblSetting WHERE ID = 32 AND (Value LIKE 'true' OR Value LIKE 'yes'))
					INSERT INTO tblOrderDbAudit (DBAuditDate, ID, OrderNum, StatusID, PriorityID, DueDate, RouteID, OriginID
						, OriginArriveTimeUTC, OriginDepartTimeUTC, OriginMinutes, OriginWaitNotes, OriginBOLNum, OriginGrossUnits
						, OriginNetUnits, DestinationID, DestArriveTimeUTC, DestDepartTimeUTC, DestMinutes, DestWaitNotes, DestBOLNum
						, DestGrossUnits, DestNetUnits, CustomerID, CarrierID, DriverID, TruckID, TrailerID, Trailer2ID, OperatorID
						, PumperID, TicketTypeID, Rejected, RejectNotes, OriginChainUp, OriginTruckMileage, OriginTankNum, DestTruckMileage
						, CarrierTicketNum, AuditNotes, CreateDateUTC, ActualMiles, ProducerID, CreatedByUser, LastChangeDateUTC
						, LastChangedByUser, DeleteDateUTC, DeletedByUser, DestProductBSW, DestProductGravity, DestProductTemp
						, ProductID, AcceptLastChangeDateUTC, PickupLastChangeDateUTC, DeliverLastChangeDateUTC, OriginUomID, DestUomID
						, PickupPrintStatusID, DeliverPrintStatusID, PickupPrintDateUTC, DeliverPrintDateUTC, OriginTankID
						, OriginGrossStdUnits, DispatchNotes, PickupDriverNotes, DeliverDriverNotes, DispatchConfirmNum, OriginWaitReasonID
						, DestWaitReasonID, RejectReasonID, DestOpenMeterUnits, DestCloseMeterUnits, ReassignKey, DestRackBay, OrderDate
						, DestTrailerWaterCapacity, DestRailCarNum, /*OriginWeightGrossUnits,*/ OriginWeightNetUnits, DestWeightGrossUnits
						, DestWeightTareUnits, DestWeightNetUnits, JobNumber, ContractNumber, OriginSulfurContent, DestChainup)
						SELECT GETUTCDATE(), ID, OrderNum, StatusID, PriorityID, DueDate, RouteID, OriginID, OriginArriveTimeUTC
							, OriginDepartTimeUTC, OriginMinutes, OriginWaitNotes, OriginBOLNum, OriginGrossUnits, OriginNetUnits
							, DestinationID, DestArriveTimeUTC, DestDepartTimeUTC, DestMinutes, DestWaitNotes, DestBOLNum, DestGrossUnits
							, DestNetUnits, CustomerID, CarrierID, DriverID, TruckID, TrailerID, Trailer2ID, OperatorID, PumperID
							, TicketTypeID, Rejected, RejectNotes, OriginChainUp, OriginTruckMileage, OriginTankNum, DestTruckMileage
							, CarrierTicketNum, AuditNotes, CreateDateUTC, ActualMiles, ProducerID, CreatedByUser, LastChangeDateUTC
							, LastChangedByUser, DeleteDateUTC, DeletedByUser, DestProductBSW, DestProductGravity, DestProductTemp
							, ProductID, AcceptLastChangeDateUTC, PickupLastChangeDateUTC, DeliverLastChangeDateUTC, OriginUomID
							, DestUomID, PickupPrintStatusID, DeliverPrintStatusID, PickupPrintDateUTC, DeliverPrintDateUTC, OriginTankID
							, OriginGrossStdUnits, DispatchNotes, PickupDriverNotes, DeliverDriverNotes, DispatchConfirmNum
							, OriginWaitReasonID, DestWaitReasonID, RejectReasonID, DestOpenMeterUnits, DestCloseMeterUnits, ReassignKey
							, DestRackBay, OrderDate, DestTrailerWaterCapacity, DestRailCarNum, /*OriginWeightGrossUnits,*/ OriginWeightNetUnits
							, DestWeightGrossUnits, DestWeightTareUnits, DestWeightNetUnits, JobNumber, ContractNumber, OriginSulfurContent, DestChainup
						FROM deleted d
			END TRY
			BEGIN CATCH
				PRINT 'trigOrder_IU.DBAUDIT FAILURE: ' + ERROR_MESSAGE()
			END CATCH
		END
		
		PRINT 'trigOrder_IU COMPLETE: ' + (CONVERT(VARCHAR(24), GETDATE(), 121))

	END
END


GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details + FINANCIAL INFO into a single view
-- Changes:
	-- 3.7.28	2015/06/18 - KDA - add translated "SettlementFactor" column
	-- 4.4.1	2016/11/04 - JAE - Add Destination Chainup
/***********************************/
ALTER VIEW viewOrderSettlementCarrier AS 
	SELECT OSC.*
		, BatchNum = SB.BatchNum
		, SettlementUom = UOM.Name
		, SettlementUomShort = UOM.Abbrev
		, RouteRate = RR.Rate
		, RouteRateType = RRT.Name
		, RateSheetRate = RSRR.Rate
		, RateSheetRateType = RSRT.Name
		, OrderRejectRate = ORR.Rate
		, OrderRejectRateType = ORRT.Name
		, OriginWaitRate = OWR.Rate
		, DestinationWaitRate = DWR.Rate
		, TotalWaitAmount = cast(isnull(OSC.OriginWaitAmount, 0) as money) + cast(isnull(OSC.DestinationWaitAmount, 0) as money)
		, OriginChainupRate = CAR.Rate
		, OriginChainupRateType = CART.Name
		, OriginChainupAmount = CA.Amount
		, DestChainupRate = DCAR.Rate
		, DestChainupRateType = DCART.Name
		, DestChainupAmount = DCA.Amount
		, RerouteRate = RAR.Rate
		, RerouteRateType = RART.Name
		, RerouteAmount = RA.Amount
		, SplitLoadRate = SAR.Rate
		, SplitLoadRateType = SART.Name
		, SplitLoadAmount = SA.Amount
		, H2SRate = HAR.Rate
		, H2SRateType = HART.Name
		, H2SAmount = HA.Amount
		, OtherAmount = OA.Amount
		, WaitFeeSubUnit = isnull(WFSU.Name, 'None') 
		, WaitFeeRoundingType = isnull(WFRT.Name, 'None')
		, SettlementFactor = SF.Name
	FROM tblOrderSettlementCarrier OSC 
	LEFT JOIN tblCarrierSettlementBatch SB ON SB.ID = OSC.BatchID
	LEFT JOIN tblUom UOM ON UOM.ID = OSC.SettlementUomID
	LEFT JOIN tblCarrierOriginWaitRate OWR ON OWR.ID = OSC.OriginWaitRateID
	LEFT JOIN tblCarrierDestinationWaitRate DWR ON DWR.ID = OSC.DestinationWaitRateID
	LEFT JOIN tblCarrierOrderRejectRate ORR ON ORR.ID = OSC.OrderRejectRateID
	LEFT JOIN tblRateType ORRT ON ORRT.ID = ORR.RateTypeID
	-- route rate/ratesheet JOINs
	LEFT JOIN tblCarrierRouteRate RR ON RR.ID = OSC.RouteRateID
	LEFT JOIN tblRateType RRT ON RRT.ID = RR.RateTypeID
	LEFT JOIN viewCarrierRateSheetRangeRate RSRR ON RSRR.ID = OSC.RangeRateID
	LEFT JOIN tblRateType RSRT ON RSRT.ID = RSRR.RateTypeID
	-- chainup
	LEFT JOIN tblOrderSettlementCarrierAssessorialCharge CA ON CA.OrderID = OSC.OrderID AND CA.AssessorialRateTypeID = 1
	LEFT JOIN tblCarrierAssessorialRate CAR ON CAR.ID = CA.AssessorialRateID
	LEFT JOIN tblRateType CART ON CART.ID = CAR.RateTypeID
	LEFT JOIN tblOrderSettlementCarrierAssessorialCharge DCA ON DCA.OrderID = OSC.OrderID AND DCA.AssessorialRateTypeID = 5
	LEFT JOIN tblCarrierAssessorialRate DCAR ON DCAR.ID = DCA.AssessorialRateID
	LEFT JOIN tblRateType DCART ON DCART.ID = DCAR.RateTypeID
	-- Reroute
	LEFT JOIN tblOrderSettlementCarrierAssessorialCharge RA ON RA.OrderID = OSC.OrderID AND RA.AssessorialRateTypeID = 2
	LEFT JOIN tblCarrierAssessorialRate RAR ON RAR.ID = RA.AssessorialRateID
	LEFT JOIN tblRateType RART ON RART.ID = RAR.RateTypeID
	-- Split Load
	LEFT JOIN tblOrderSettlementCarrierAssessorialCharge SA ON SA.OrderID = OSC.OrderID AND SA.AssessorialRateTypeID = 3
	LEFT JOIN tblCarrierAssessorialRate SAR ON SAR.ID = SA.AssessorialRateID
	LEFT JOIN tblRateType SART ON SART.ID = SAR.RateTypeID
	-- H2S
	LEFT JOIN tblOrderSettlementCarrierAssessorialCharge HA ON HA.OrderID = OSC.OrderID AND HA.AssessorialRateTypeID = 4
	LEFT JOIN tblCarrierAssessorialRate HAR ON HAR.ID = HA.AssessorialRateID
	LEFT JOIN tblRateType HART ON HART.ID = HAR.RateTypeID
	-- Other Assessorial Charges (combined)
	LEFT JOIN (
		SELECT OrderID, Amount = SUM(Amount) FROM tblOrderSettlementCarrierAssessorialCharge WHERE AssessorialRateTypeID NOT IN (1,2,3,4) GROUP BY OrderID
	) OA ON OA.OrderID = OSC.OrderID 
	LEFT JOIN tblSettlementFactor SF ON SF.ID = OSC.SettlementFactorID
	LEFT JOIN tblCarrierWaitFeeParameter WFP ON WFP.ID = OSC.WaitFeeParameterID
	LEFT JOIN dbo.tblWaitFeeSubUnit WFSU ON WFSU.ID = WFP.SubUnitID
	LEFT JOIN dbo.tblWaitFeeRoundingType WFRT ON WFRT.ID = WFP.RoundingTypeID

GO

/***********************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details + FINANCIAL INFO into a single view
-- Changes:
	-- 4.4.1	2016/11/04 - JAE - Add Destination Chainup
/***********************************/
ALTER VIEW viewOrderSettlementDriver AS 
	SELECT OSC.*
		, BatchNum = SB.BatchNum
		, SettlementUom = UOM.Name
		, SettlementUomShort = UOM.Abbrev
		, RouteRate = RR.Rate
		, RouteRateType = RRT.Name
		, RateSheetRate = RSRR.Rate
		, RateSheetRateType = RSRT.Name
		, OrderRejectRate = ORR.Rate
		, OrderRejectRateType = ORRT.Name
		, OriginWaitRate = OWR.Rate
		, DestinationWaitRate = DWR.Rate
		, TotalWaitAmount = cast(isnull(OSC.OriginWaitAmount, 0) as money) + cast(isnull(OSC.DestinationWaitAmount, 0) as money)
		, OriginChainupRate = CAR.Rate
		, OriginChainupRateType = CART.Name
		, OriginChainupAmount = CA.Amount
		, DestChainupRate = DCAR.Rate
		, DestChainupRateType = DCART.Name
		, DestChainupAmount = DCA.Amount
		, RerouteRate = RAR.Rate
		, RerouteRateType = RART.Name
		, RerouteAmount = RA.Amount
		, SplitLoadRate = SAR.Rate
		, SplitLoadRateType = SART.Name
		, SplitLoadAmount = SA.Amount
		, H2SRate = HAR.Rate
		, H2SRateType = HART.Name
		, H2SAmount = HA.Amount
		, OtherAmount = OA.Amount
		, WaitFeeSubUnit = isnull(WFSU.Name, 'None') 
		, WaitFeeRoundingType = isnull(WFRT.Name, 'None')
		, SettlementFactor = SF.Name
	FROM tblOrderSettlementDriver OSC 
	LEFT JOIN tblDriverSettlementBatch SB ON SB.ID = OSC.BatchID
	LEFT JOIN tblUom UOM ON UOM.ID = OSC.SettlementUomID
	LEFT JOIN tblDriverOriginWaitRate OWR ON OWR.ID = OSC.OriginWaitRateID
	LEFT JOIN tblDriverDestinationWaitRate DWR ON DWR.ID = OSC.DestinationWaitRateID
	LEFT JOIN tblDriverOrderRejectRate ORR ON ORR.ID = OSC.OrderRejectRateID
	LEFT JOIN tblRateType ORRT ON ORRT.ID = ORR.RateTypeID
	-- route rate/ratesheet JOINs
	LEFT JOIN tblDriverRouteRate RR ON RR.ID = OSC.RouteRateID
	LEFT JOIN tblRateType RRT ON RRT.ID = RR.RateTypeID
	LEFT JOIN viewDriverRateSheetRangeRate RSRR ON RSRR.ID = OSC.RangeRateID
	LEFT JOIN tblRateType RSRT ON RSRT.ID = RSRR.RateTypeID
	-- chainup
	LEFT JOIN tblOrderSettlementDriverAssessorialCharge CA ON CA.OrderID = OSC.OrderID AND CA.AssessorialRateTypeID = 1
	LEFT JOIN tblDriverAssessorialRate CAR ON CAR.ID = CA.AssessorialRateID
	LEFT JOIN tblRateType CART ON CART.ID = CAR.RateTypeID
	LEFT JOIN tblOrderSettlementDriverAssessorialCharge DCA ON DCA.OrderID = OSC.OrderID AND DCA.AssessorialRateTypeID = 5
	LEFT JOIN tblDriverAssessorialRate DCAR ON DCAR.ID = DCA.AssessorialRateID
	LEFT JOIN tblRateType DCART ON DCART.ID = DCAR.RateTypeID
	-- Reroute
	LEFT JOIN tblOrderSettlementDriverAssessorialCharge RA ON RA.OrderID = OSC.OrderID AND RA.AssessorialRateTypeID = 2
	LEFT JOIN tblDriverAssessorialRate RAR ON RAR.ID = RA.AssessorialRateID
	LEFT JOIN tblRateType RART ON RART.ID = RAR.RateTypeID
	-- Split Load
	LEFT JOIN tblOrderSettlementDriverAssessorialCharge SA ON SA.OrderID = OSC.OrderID AND SA.AssessorialRateTypeID = 3
	LEFT JOIN tblDriverAssessorialRate SAR ON SAR.ID = SA.AssessorialRateID
	LEFT JOIN tblRateType SART ON SART.ID = SAR.RateTypeID
	-- H2S
	LEFT JOIN tblOrderSettlementDriverAssessorialCharge HA ON HA.OrderID = OSC.OrderID AND HA.AssessorialRateTypeID = 4
	LEFT JOIN tblDriverAssessorialRate HAR ON HAR.ID = HA.AssessorialRateID
	LEFT JOIN tblRateType HART ON HART.ID = HAR.RateTypeID
	-- Other Assessorial Charges (combined)
	LEFT JOIN (
		SELECT OrderID, Amount = SUM(Amount) FROM tblOrderSettlementDriverAssessorialCharge WHERE AssessorialRateTypeID NOT IN (1,2,3,4) GROUP BY OrderID
	) OA ON OA.OrderID = OSC.OrderID 
	LEFT JOIN tblSettlementFactor SF ON SF.ID = OSC.SettlementFactorID
	LEFT JOIN tblCarrierWaitFeeParameter WFP ON WFP.ID = OSC.WaitFeeParameterID
	LEFT JOIN dbo.tblWaitFeeSubUnit WFSU ON WFSU.ID = WFP.SubUnitID
	LEFT JOIN dbo.tblWaitFeeRoundingType WFRT ON WFRT.ID = WFP.RoundingTypeID


GO





/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details + FINANCIAL INFO into a single view
-- Changes:
	-- 3.7.29 - 2015/06/18 - KDA - add translated "SettlementFactor" column
	-- 4.4.1	2016/11/04 - JAE - Add Destination Chainup
/***********************************/
ALTER VIEW viewOrderSettlementShipper AS 
	SELECT OSC.*
		, BatchNum = SB.BatchNum
		, SettlementUom = UOM.Name
		, SettlementUomShort = UOM.Abbrev
		, RouteRate = RR.Rate
		, RouteRateType = RRT.Name
		, RateSheetRate = RSRR.Rate
		, RateSheetRateType = RSRT.Name
		, OrderRejectRate = ORR.Rate
		, OrderRejectRateType = ORRT.Name
		, OriginWaitRate = OWR.Rate
		, DestinationWaitRate = DWR.Rate
		, TotalWaitAmount = cast(isnull(OSC.OriginWaitAmount, 0) as money) + cast(isnull(OSC.DestinationWaitAmount, 0) as money)
		, OriginChainupRate = CAR.Rate
		, OriginChainupRateType = CART.Name
		, OriginChainupAmount = CA.Amount
		, DestChainupRate = DCAR.Rate
		, DestChainupRateType = DCART.Name
		, DestChainupAmount = DCA.Amount
		, RerouteRate = RAR.Rate
		, RerouteRateType = RART.Name
		, RerouteAmount = RA.Amount
		, SplitLoadRate = SAR.Rate
		, SplitLoadRateType = SART.Name
		, SplitLoadAmount = SA.Amount
		, H2SRate = HAR.Rate
		, H2SRateType = HART.Name
		, H2SAmount = HA.Amount
		, OtherAmount = OA.Amount
		, WaitFeeSubUnit = isnull(WFSU.Name, 'None') 
		, WaitFeeRoundingType = isnull(WFRT.Name, 'None') 
		, SettlementFactor = SF.Name
	FROM tblOrderSettlementShipper OSC 
	LEFT JOIN tblShipperSettlementBatch SB ON SB.ID = OSC.BatchID
	LEFT JOIN tblUom UOM ON UOM.ID = OSC.SettlementUomID
	LEFT JOIN tblShipperOriginWaitRate OWR ON OWR.ID = OSC.OriginWaitRateID
	LEFT JOIN tblShipperDestinationWaitRate DWR ON DWR.ID = OSC.DestinationWaitRateID
	LEFT JOIN tblShipperOrderRejectRate ORR ON ORR.ID = OSC.OrderRejectRateID
	LEFT JOIN tblRateType ORRT ON ORRT.ID = ORR.RateTypeID
	-- route rate/ratesheet JOINs
	LEFT JOIN tblShipperRouteRate RR ON RR.ID = OSC.RouteRateID
	LEFT JOIN tblRateType RRT ON RRT.ID = RR.RateTypeID
	LEFT JOIN viewShipperRateSheetRangeRate RSRR ON RSRR.ID = OSC.RangeRateID
	LEFT JOIN tblRateType RSRT ON RSRT.ID = RSRR.RateTypeID
	-- chainup
	LEFT JOIN tblOrderSettlementShipperAssessorialCharge CA ON CA.OrderID = OSC.OrderID AND CA.AssessorialRateTypeID = 1
	LEFT JOIN tblShipperAssessorialRate CAR ON CAR.ID = CA.AssessorialRateID
	LEFT JOIN tblRateType CART ON CART.ID = CAR.RateTypeID
	LEFT JOIN tblOrderSettlementShipperAssessorialCharge DCA ON DCA.OrderID = OSC.OrderID AND DCA.AssessorialRateTypeID = 5
	LEFT JOIN tblShipperAssessorialRate DCAR ON DCAR.ID = DCA.AssessorialRateID
	LEFT JOIN tblRateType DCART ON DCART.ID = DCAR.RateTypeID
	-- Reroute
	LEFT JOIN tblOrderSettlementShipperAssessorialCharge RA ON RA.OrderID = OSC.OrderID AND RA.AssessorialRateTypeID = 2
	LEFT JOIN tblShipperAssessorialRate RAR ON RAR.ID = RA.AssessorialRateID
	LEFT JOIN tblRateType RART ON RART.ID = RAR.RateTypeID
	-- Split Load
	LEFT JOIN tblOrderSettlementShipperAssessorialCharge SA ON SA.OrderID = OSC.OrderID AND SA.AssessorialRateTypeID = 3
	LEFT JOIN tblShipperAssessorialRate SAR ON SAR.ID = SA.AssessorialRateID
	LEFT JOIN tblRateType SART ON SART.ID = SAR.RateTypeID
	-- H2S
	LEFT JOIN tblOrderSettlementShipperAssessorialCharge HA ON HA.OrderID = OSC.OrderID AND HA.AssessorialRateTypeID = 4
	LEFT JOIN tblShipperAssessorialRate HAR ON HAR.ID = HA.AssessorialRateID
	LEFT JOIN tblRateType HART ON HART.ID = HAR.RateTypeID
	-- Other Assessorial Charges (combined)
	LEFT JOIN (
		SELECT OrderID, Amount = SUM(Amount) FROM tblOrderSettlementShipperAssessorialCharge WHERE AssessorialRateTypeID NOT IN (1,2,3,4) GROUP BY OrderID
	) OA ON OA.OrderID = OSC.OrderID 
	LEFT JOIN tblSettlementFactor SF ON SF.ID = OSC.SettlementFactorID
	LEFT JOIN tblShipperWaitFeeParameter WFP ON WFP.ID = OSC.WaitFeeParameterID
	LEFT JOIN dbo.tblWaitFeeSubUnit WFSU ON WFSU.ID = WFP.SubUnitID
	LEFT JOIN dbo.tblWaitFeeRoundingType WFRT ON WFRT.ID = WFP.RoundingTypeID


GO


exec sp_refreshview viewOrder_OrderTicket_Full
exec sp_refreshview viewOrderSettlementCarrier
exec sp_refreshview viewOrderSettlementShipper
exec sp_refreshview viewOrderSettlementDriver
exec sp_refreshview viewOrderSettlementProducer

GO

/***********************************/
-- Created: ?.?.? - 2016/07/08 - Kevin Alons
-- Purpose: return Report Center Order data without GPS data
-- Changes:
	-- 4.4.1	2016/11/04 - JAE - Add Destination Chainup
/***********************************/
ALTER VIEW viewReportCenter_Orders_NoGPS AS
	SELECT X.*
	FROM (
		SELECT O.*
			--
			, ShipperBatchNum = SS.BatchNum
			, ShipperBatchInvoiceNum = SSB.InvoiceNum
			, ShipperSettlementUomID = SS.SettlementUomID
			, ShipperSettlementUom = SS.SettlementUom
			, ShipperMinSettlementUnits = SS.MinSettlementUnits
			, ShipperSettlementUnits = SS.SettlementUnits
			, ShipperRateSheetRate = SS.RateSheetRate
			, ShipperRateSheetRateType = SS.RateSheetRateType
			, ShipperRouteRate = SS.RouteRate
			, ShipperRouteRateType = SS.RouteRateType
			, ShipperLoadRate = ISNULL(SS.RouteRate, SS.RateSheetRate)
			, ShipperLoadRateType = ISNULL(SS.RouteRateType, SS.RateSheetRateType)
			, ShipperLoadAmount = SS.LoadAmount
			, ShipperOrderRejectRate = SS.OrderRejectRate 
			, ShipperOrderRejectRateType = SS.OrderRejectRateType 
			, ShipperOrderRejectAmount = SS.OrderRejectAmount 
			, ShipperWaitFeeSubUnit = SS.WaitFeeSubUnit  
			, ShipperWaitFeeRoundingType = SS.WaitFeeRoundingType  
			, ShipperOriginWaitBillableHours = SS.OriginWaitBillableHours  
			, ShipperOriginWaitBillableMinutes = SS.OriginWaitBillableMinutes  
			, ShipperOriginWaitRate = SS.OriginWaitRate
			, ShipperOriginWaitAmount = SS.OriginWaitAmount
			, ShipperDestinationWaitBillableHours = SS.DestinationWaitBillableHours 
			, ShipperDestinationWaitBillableMinutes = SS.DestinationWaitBillableMinutes   
			, ShipperDestinationWaitRate = SS.DestinationWaitRate  
			, ShipperDestinationWaitAmount = SS.DestinationWaitAmount  
			, ShipperTotalWaitAmount = SS.TotalWaitAmount
			, ShipperTotalWaitBillableMinutes = NULLIF(ISNULL(SS.OriginWaitBillableMinutes, 0) + ISNULL(SS.DestinationWaitBillableMinutes, 0), 0)
			, ShipperTotalWaitBillableHours = NULLIF(ISNULL(SS.OriginWaitBillableHours, 0) + ISNULL(SS.DestinationWaitBillableHours, 0), 0)		
			, ShipperFuelSurchargeRate = SS.FuelSurchargeRate
			, ShipperFuelSurchargeAmount = SS.FuelSurchargeAmount
			, ShipperOriginChainupRate = SS.OriginChainupRate
			, ShipperOriginChainupRateType = SS.OriginChainupRateType  
			, ShipperOriginChainupAmount = SS.OriginChainupAmount
			, ShipperDestChainupRate = SS.DestChainupRate
			, ShipperDestChainupRateType = SS.DestChainupRateType  
			, ShipperDestChainupAmount = SS.DestChainupAmount
			, ShipperRerouteRate = SS.RerouteRate
			, ShipperRerouteRateType = SS.RerouteRateType  
			, ShipperRerouteAmount = SS.RerouteAmount
			, ShipperSplitLoadRate = SS.SplitLoadRate
			, ShipperSplitLoadRateType = SS.SplitLoadRateType  
			, ShipperSplitLoadAmount = SS.SplitLoadAmount
			, ShipperH2SRate = SS.H2SRate
			, ShipperH2SRateType = SS.H2SRateType  
			, ShipperH2SAmount = SS.H2SAmount
			, ShipperTaxRate = SS.OriginTaxRate
			, ShipperTotalAmount = SS.TotalAmount
			, ShipperDestCode = CDC.Code
			, ShipperTicketType = STT.TicketType
			--
			, CarrierBatchNum = SC.BatchNum
			, CarrierSettlementUomID = SC.SettlementUomID
			, CarrierSettlementUom = SC.SettlementUom
			, CarrierMinSettlementUnits = SC.MinSettlementUnits
			, CarrierSettlementUnits = SC.SettlementUnits
			, CarrierRateSheetRate = SC.RateSheetRate
			, CarrierRateSheetRateType = SC.RateSheetRateType
			, CarrierRouteRate = SC.RouteRate
			, CarrierRouteRateType = SC.RouteRateType
			, CarrierLoadRate = ISNULL(SC.RouteRate, SC.RateSheetRate)
			, CarrierLoadRateType = ISNULL(SC.RouteRateType, SC.RateSheetRateType)
			, CarrierLoadAmount = SC.LoadAmount
			, CarrierOrderRejectRate = SC.OrderRejectRate 
			, CarrierOrderRejectRateType = SC.OrderRejectRateType 
			, CarrierOrderRejectAmount = SC.OrderRejectAmount 
			, CarrierWaitFeeSubUnit = SC.WaitFeeSubUnit  
			, CarrierWaitFeeRoundingType = SC.WaitFeeRoundingType  
			, CarrierOriginWaitBillableHours = SC.OriginWaitBillableHours  
			, CarrierOriginWaitBillableMinutes = SC.OriginWaitBillableMinutes  
			, CarrierOriginWaitRate = SC.OriginWaitRate
			, CarrierOriginWaitAmount = SC.OriginWaitAmount
			, CarrierDestinationWaitBillableHours = SC.DestinationWaitBillableHours 
			, CarrierDestinationWaitBillableMinutes = SC.DestinationWaitBillableMinutes  
			, CarrierDestinationWaitRate = SC.DestinationWaitRate 
			, CarrierDestinationWaitAmount = SC.DestinationWaitAmount  
			, CarrierTotalWaitAmount = SC.TotalWaitAmount
			, CarrierTotalWaitBillableMinutes = NULLIF(ISNULL(SC.OriginWaitBillableMinutes, 0) + ISNULL(SC.DestinationWaitBillableMinutes, 0), 0)
			, CarrierTotalWaitBillableHours = NULLIF(ISNULL(SC.OriginWaitBillableHours, 0) + ISNULL(SC.DestinationWaitBillableHours, 0), 0)		
			, CarrierFuelSurchargeRate = SC.FuelSurchargeRate
			, CarrierFuelSurchargeAmount = SC.FuelSurchargeAmount
			, CarrierOriginChainupRate = SC.OriginChainupRate
			, CarrierOriginChainupRateType = SC.OriginChainupRateType  
			, CarrierOriginChainupAmount = SC.OriginChainupAmount
			, CarrierDestChainupRate = SC.DestChainupRate
			, CarrierDestChainupRateType = SC.DestChainupRateType  
			, CarrierDestChainupAmount = SC.DestChainupAmount
			, CarrierRerouteRate = SC.RerouteRate
			, CarrierRerouteRateType = SC.RerouteRateType  
			, CarrierRerouteAmount = SC.RerouteAmount
			, CarrierSplitLoadRate = SC.SplitLoadRate
			, CarrierSplitLoadRateType = SC.SplitLoadRateType  
			, CarrierSplitLoadAmount = SC.SplitLoadAmount
			, CarrierH2SRate = SC.H2SRate
			, CarrierH2SRateType = SC.H2SRateType  
			, CarrierH2SAmount = SC.H2SAmount
			, CarrierTaxRate = SC.OriginTaxRate
			, CarrierTotalAmount = SC.TotalAmount
			--
			, DriverBatchNum = SD.BatchNum
			, DriverSettlementUomID = SD.SettlementUomID
			, DriverSettlementUom = SD.SettlementUom
			, DriverMinSettlementUnits = SD.MinSettlementUnits
			, DriverSettlementUnits = SD.SettlementUnits
			, DriverRateSheetRate = SD.RateSheetRate
			, DriverRateSheetRateType = SD.RateSheetRateType
			, DriverRouteRate = SD.RouteRate
			, DriverRouteRateType = SD.RouteRateType
			, DriverLoadRate = ISNULL(SD.RouteRate, SD.RateSheetRate)
			, DriverLoadRateType = ISNULL(SD.RouteRateType, SD.RateSheetRateType)
			, DriverLoadAmount = SD.LoadAmount
			, DriverOrderRejectRate = SD.OrderRejectRate 
			, DriverOrderRejectRateType = SD.OrderRejectRateType 
			, DriverOrderRejectAmount = SD.OrderRejectAmount 
			, DriverWaitFeeSubUnit = SD.WaitFeeSubUnit  
			, DriverWaitFeeRoundingType = SD.WaitFeeRoundingType  
			, DriverOriginWaitBillableHours = SD.OriginWaitBillableHours  
			, DriverOriginWaitBillableMinutes = SD.OriginWaitBillableMinutes  
			, DriverOriginWaitRate = SD.OriginWaitRate
			, DriverOriginWaitAmount = SD.OriginWaitAmount
			, DriverDestinationWaitBillableHours = SD.DestinationWaitBillableHours 
			, DriverDestinationWaitBillableMinutes = SD.DestinationWaitBillableMinutes  
			, DriverDestinationWaitRate = SD.DestinationWaitRate 
			, DriverDestinationWaitAmount = SD.DestinationWaitAmount  
			, DriverTotalWaitAmount = SD.TotalWaitAmount
			, DriverTotalWaitBillableMinutes = NULLIF(ISNULL(SD.OriginWaitBillableMinutes, 0) + ISNULL(SD.DestinationWaitBillableMinutes, 0), 0)
			, DriverTotalWaitBillableHours = NULLIF(ISNULL(SD.OriginWaitBillableHours, 0) + ISNULL(SD.DestinationWaitBillableHours, 0), 0)		
			, DriverFuelSurchargeRate = SD.FuelSurchargeRate
			, DriverFuelSurchargeAmount = SD.FuelSurchargeAmount
			, DriverOriginChainupRate = SD.OriginChainupRate
			, DriverOriginChainupRateType = SD.OriginChainupRateType  
			, DriverOriginChainupAmount = SD.OriginChainupAmount
			, DriverDestChainupRate = SD.DestChainupRate
			, DriverDestChainupRateType = SD.DestChainupRateType  
			, DriverDestChainupAmount = SD.DestChainupAmount
			, DriverRerouteRate = SD.RerouteRate
			, DriverRerouteRateType = SD.RerouteRateType  
			, DriverRerouteAmount = SD.RerouteAmount
			, DriverSplitLoadRate = SD.SplitLoadRate
			, DriverSplitLoadRateType = SD.SplitLoadRateType  
			, DriverSplitLoadAmount = SD.SplitLoadAmount
			, DriverH2SRate = SD.H2SRate
			, DriverH2SRateType = SD.H2SRateType  
			, DriverH2SAmount = SD.H2SAmount
			, DriverTaxRate = SD.OriginTaxRate
			, DriverTotalAmount = SD.TotalAmount
			--
			, ProducerBatchNum = SP.BatchNum
			, ProducerSettlementUomID = SP.SettlementUomID
			, ProducerSettlementUom = SP.SettlementUom
			, ProducerSettlementUnits = SP.SettlementUnits
			, ProducerCommodityIndex = SP.CommodityIndex
			, ProducerCommodityIndexID = SP.CommodityIndexID
			, ProducerCommodityMethod = SP.CommodityMethod
			, ProducerCommodityMethodID = SP.CommodityMethodID
			, ProducerIndexPrice = SP.Price
			, ProducerIndexStartDate = SP.IndexStartDate
			, ProducerIndexEndDate = SP.IndexEndDate
			, ProducerPriceAmount = SP.CommodityPurchasePricebookPriceAmount
			, ProducerDeduct = SP.Deduct
			, ProducerDeductType = SP.Deduct
			, ProducerDeductAmount = SP.CommodityPurchasePricebookDeductAmount
			, ProducerPremium = SP.PremiumType
			, ProducerPremiumType = SP.PremiumType
			, ProducerPremiumAmount = SP.CommodityPurchasePricebookPremiumAmount
			, ProducerPremiumDesc = SP.PremiumDesc
			, ProducerTotalAmount = SP.CommodityPurchasePricebookTotalAmount
			--
			, OriginLatLon = LTRIM(OO.LAT) + ',' + LTRIM(OO.LON)
			, OriginGeoFenceRadiusMeters = OO.GeoFenceRadiusMeters
			, OriginCTBNum = OO.CTBNum
			, OriginFieldName = OO.FieldName
			, OriginCity = OO.City																				
			, OriginCityState = OO.City + ', ' + (SELECT Abbreviation FROM tblState WHERE ID = OO.StateID)		
			--
			, DestLatLon = LTRIM(D.LAT) + ',' + LTRIM(D.LON)
			, DestGeoFenceRadiusMeters = D.GeoFenceRadiusMeters
			, DestCity = D.City
			, DestCityState = D.City + ', ' + (SELECT Abbreviation FROM tblState WHERE ID = D.StateID) -- 4.1.0 - fix (was pointing to Origin.StateID)
			--
			, Gauger = GAO.Gauger						
			, GaugerIDNumber = GA.IDNumber
			, GaugerFirstName = GA.FirstName
			, GaugerLastName = GA.LastName
			, GaugerRejected = GAO.Rejected
			, GaugerRejectReasonID = GAO.RejectReasonID
			, GaugerRejectNotes = GAO.RejectNotes
			, GaugerRejectNumDesc = GORR.NumDesc
			, GaugerPrintDateUTC = GAO.PrintDateUTC -- 4.1.0 - added for use by GaugerPrintDate EXPRESSION column for effiency reasons (this is already queried, avoid doing the calculation unless needed)
			--, GaugerPrintDate = dbo.fnUTC_to_Local(GAO.PrintDateUTC, O.OriginTimeZoneID, O.OriginUseDST)
			--
			, T_GaugerCarrierTicketNum = CASE WHEN GAO.TicketCount = 0 THEN LTRIM(GAO.OrderNum) + CASE WHEN GAO.Rejected = 1 THEN 'X' ELSE '' END ELSE GOT.CarrierTicketNum END 
			, T_GaugerTankNum = ISNULL(GOT.OriginTankText, GAO.OriginTankText)
			, T_GaugerIsStrappedTank = GOT.IsStrappedTank 
			, T_GaugerProductObsTemp = GOT.ProductObsTemp
			, T_GaugerProductObsGravity = GOT.ProductObsGravity
			, T_GaugerProductBSW = GOT.ProductBSW		
			, T_GaugerOpeningGaugeFeet = GOT.OpeningGaugeFeet
			, T_GaugerOpeningGaugeInch = GOT.OpeningGaugeInch		
			, T_GaugerOpeningGaugeQ = GOT.OpeningGaugeQ			
			, T_GaugerBottomFeet = GOT.BottomFeet
			, T_GaugerBottomInches = GOT.BottomInches		
			, T_GaugerBottomQ = GOT.BottomQ		
			, T_GaugerRejected = GOT.Rejected
			, T_GaugerRejectReasonID = GOT.RejectReasonID
			, T_GaugerRejectNumDesc = GOT.RejectNumDesc
			, T_GaugerRejectNotes = GOT.RejectNotes	
			--
			--, OrderCreateDate = dbo.fnUTC_to_Local(O.CreateDateUTC, O.OriginTimeZoneID, O.OriginUseDST) -- replaced 4.1.0 with an expression type value (more efficient, only executed when needed)
			, OrderApproved = CAST(ISNULL(OA.Approved, 0) AS BIT)
		FROM viewOrder_OrderTicket_Full O
		JOIN tblOrigin OO ON OO.ID = O.OriginID
		JOIN tblDestination D ON D.ID = O.DestinationID
		--
		LEFT JOIN viewGaugerOrder GAO ON GAO.OrderID = O.ID			            
		LEFT JOIN viewGaugerOrderTicket GOT ON GOT.UID = O.T_UID	            
		LEFT JOIN viewGauger GA ON GA.ID = GAO.GaugerID				            
		LEFT JOIN viewOrderRejectReason GORR ON GORR.ID = GAO.RejectReasonID 
		--
		LEFT JOIN tblCustomerDestinationCode CDC ON CDC.CustomerID = O.CustomerID AND CDC.DestinationID = O.DestinationID
		LEFT JOIN viewOrderSettlementCarrier SC ON SC.OrderID = O.ID
		LEFT JOIN viewOrderSettlementShipper SS ON SS.OrderID = O.ID
		LEFT JOIN viewOrderSettlementDriver SD ON SD.OrderID = O.ID
		LEFT JOIN tblShipperSettlementBatch SSB ON SSB.ID = SS.BatchID
		LEFT JOIN viewOrderSettlementProducer SP ON SP.OrderID = O.ID
		LEFT JOIN tblShipperTicketType STT ON STT.CustomerID = O.CustomerID AND STT.TicketTypeID = O.TicketTypeID
		--
		LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
	) X


GO

exec sp_refreshview viewOrderSettlementUnitsCarrier
exec sp_refreshview viewOrderSettlementUnitsShipper
GO

/*************************************************************
-- Date Created: 15 Jun 2015
-- Author: Kevin Alons
-- Purpose: return the base data for the Order Approval page
-- Changes:
	- 3.9.2  - 2015/08/25 - KDA - remove obsolete OrderDateUTC & DeliverDateUTC fields (which were used to optimize date searches when OrderDate was virtual)
	- 3.9.20 - 2015/10/26 - JAE - Add columns for transfer percentage and override percentage
	- 3.9.25 - 2015/11/10 - JAE - Use origin driver for approval page
	- 3.9.36 - 2015/12/21 - JAE - Add columns for min settlement units (carrier and shipper)
	- 3.10.13  - 2016/02/29 - JAE - Add Truck Type
	- 3.11.17.2 - 2016/04/18 - JAE - Filter producer settled orders
	- 3.11.19 - 2016/05/02 - BB - Add Job number and Contract number
	- 4.4.1	 - 2016/11/04 - JAE - Add Destination Chainup
*************************************************************/
ALTER VIEW viewOrderApprovalSource
AS
	SELECT O.ID
		, O.StatusID
		, Status = O.OrderStatus
		, O.OrderNum
		, O.JobNumber
		, O.ContractNumber
		, O.OrderDate
		, DeliverDate = ISNULL(O.DeliverDate, O.OrderDate)
		, ShipperID = O.CustomerID
		, O.CarrierID
		, DriverID = O.OriginDriverID
		, TruckID = O.OriginTruckID
		, O.TrailerID
		, O.Trailer2ID
		, O.ProducerID
		, O.OperatorID
		, O.PumperID
		, Shipper = O.Customer
		, O.Carrier
		, Driver = O.OriginDriver
		, O.Truck
		, O.Trailer
		, O.Trailer2
		, O.Producer
		, O.Operator
		, O.Pumper
		, O.TicketTypeID
		, O.TicketType
		, O.ProductID
		, O.Product
		, O.ProductGroupID
		, O.ProductGroup
		, O.TruckTypeID
		, O.TruckType
    
		, O.Rejected
		, RejectReason = O.RejectNumDesc
		, O.RejectNotes
		, O.OriginChainUp
		, OA.OverrideOriginChainup
		, O.DestChainUp
		, OA.OverrideDestChainup
		, O.H2S
		, OA.OverrideH2S
		, O.DispatchConfirmNum
		, O.DispatchNotes
		, O.PickupDriverNotes
		, O.DeliverDriverNotes
		, Approved = CAST(ISNULL(OA.Approved, 0) AS BIT)
		, OSS.WaitFeeParameterID, O.AuditNotes
		
		, O.OriginID
		, O.Origin
		, O.OriginStateID
		, O.OriginArriveTime
		, O.OriginDepartTime
		, O.OriginMinutes
		, O.OriginUomID
		, O.OriginUOM
		, O.OriginGrossUnits
		, O.OriginGrossStdUnits
		, O.OriginNetUnits
		, OriginWaitReason = OWR.NumDesc
		, O.OriginWaitNotes
		, ShipperOriginWaitBillableMinutes = OSS.OriginWaitBillableMinutes
		, CarrierOriginWaitBillableMinutes = OSC.OriginWaitBillableMinutes
		, OA.OverrideOriginMinutes 
		
		, O.DestinationID
		, O.Destination
		, O.DestStateID
		, O.DestArriveTime
		, O.DestDepartTime
		, O.DestMinutes
		, O.DestUomID
		, O.DestUOM
		, O.DestGrossUnits
		, O.DestNetUnits
		, O.DestOpenMeterUnits
		, O.DestCloseMeterUnits
		, DestWaitReason = OWR.NumDesc
		, O.DestWaitNotes
		, ShipperDestinationWaitBillableMinutes = OSS.DestinationWaitBillableMinutes
		, CarrierDestinationWaitBillableMinutes = OSC.DestinationWaitBillableMinutes
		, OA.OverrideDestMinutes
	
		, Rerouted = CAST(CASE WHEN ORD.OrderID IS NULL THEN 0 ELSE 1 END AS BIT)
		, OA.OverrideReroute
		, O.ActualMiles
		, RerouteMiles = ROUND(ORD.RerouteMiles, 0)
		, OA.OverrideActualMiles
		, IsTransfer = CAST(CASE WHEN OT.OrderID IS NULL THEN 0 ELSE 1 END AS BIT)
		, TransferPercentComplete = ISNULL(OT.PercentComplete,100)

		, OA.OverrideTransferPercentComplete
		, CarrierMinSettlementUnits = OSC.MinSettlementUnits
		, CarrierMinSettlementUOM = CUOM.Abbrev
		, OA.OverrideCarrierMinSettlementUnits
		, ShipperMinSettlementUnits = OSS.MinSettlementUnits
		, ShipperMinSettlementUOM = CUOM.Abbrev
		, OA.OverrideShipperMinSettlementUnits
		
		, OA.CreateDateUTC
		, OA.CreatedByUser
		
		, OrderID = OSC.OrderID | OSS.OrderID -- NULL if either are NULL - if NULL indicates that the order is not yet rates (used below to rate these)
	FROM viewOrderLocalDates O
	OUTER APPLY dbo.fnOrderRerouteData(O.ID) ORD
	LEFT JOIN tblOrderReroute ORE ON ORE.OrderID = O.ID
	LEFT JOIN tblOrderTransfer OT ON OT.OrderID = O.ID
	LEFT JOIN tblOrderSettlementCarrier OSC ON OSC.OrderID = O.ID
	LEFT JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = O.ID
	LEFT JOIN tblOrderSettlementProducer OSP ON OSP.OrderID = O.ID
	LEFT JOIN viewOriginWaitReason OWR ON OWR.ID = O.OriginWaitReasonID
	LEFT JOIN viewDestinationWaitReason DWR ON DWR.ID = O.DestWaitReasonID
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
	LEFT JOIN viewOrderSettlementUnitsCarrier OSUC ON OSUC.OrderID = O.ID
	LEFT JOIN viewOrderSettlementUnitsShipper OSUS ON OSUS.OrderID = O.ID
	LEFT JOIN tblUom CUOM ON CUOM.ID = OSUC.MinSettlementUomID
	LEFT JOIN tblUom SUOM ON SUOM.ID = OSUS.MinSettlementUomID
	WHERE O.StatusID IN (4) /* only include orders that are marked AUDITED */
	  AND OSC.BatchID IS NULL AND OSS.BatchID IS NULL AND OSP.BatchID IS NULL /* don't show orders that have been settled */

GO


/***********************************/
-- Created: 4.1.0 - 2016/08/15 - Kevin Alons
-- Purpose: return BASE Order details + Carrier FINANCIAL INFO into a single view
-- Changes:
-- 4.1.0.2 - 2016.08.28 - KDA	- remove redundant calculation of ORR.RerouteCount
--								- remove redundant tblOrigin JOIN (for H2S which is already available in viewOrder)
-- 4.1.8.2 - 2016.09.21 - KDA	- Check Final Actual miles (null actual miles ok as long as override)
-- 4.1.23	 2016.10.17 - JAE	- Add weight units for settlement
-- 4.4.1	 2016/11/04 - JAE	- Add Destination Chainup
/***********************************/
ALTER VIEW viewOrder_Financial_Base_Carrier AS 
	SELECT *
		, HasError = cast(CASE WHEN ErrorFieldCSV IS NOT NULL THEN 1 ELSE 0 END as bit)
	FROM (
		SELECT *
			, OrderID = ID
			, ErrorFieldCSV = nullif(substring(CASE WHEN Approved = 0 THEN ',Approved' ELSE '' END
				+ CASE WHEN InvoiceLoadAmount IS NULL THEN ',InvoiceLoadAmount,InvoiceRouteRate,InvoiceRateSheet' ELSE '' END
				+ CASE WHEN FinalOriginChainup = 1 AND InvoiceOriginChainupAmount IS NULL THEN ',InvoiceOriginChainupAmount' ELSE '' END
				+ CASE WHEN FinalDestChainup = 1 AND InvoiceDestChainupAmount IS NULL THEN ',InvoiceDestChainupAmount' ELSE '' END
				+ CASE WHEN FinalRerouteCount > 0 AND InvoiceRerouteAmount IS NULL THEN ',InvoiceRerouteAmount' ELSE '' END
				+ CASE WHEN FinalH2S = 1 AND InvoiceH2SAmount IS NULL THEN ',InvoiceH2SAmount' ELSE '' END
				+ CASE WHEN TicketCount > 1 AND InvoiceSplitLoadAmount IS NULL THEN ',InvoiceSplitLoadAmount' ELSE '' END
				+ CASE WHEN InvoiceOriginWaitBillableMinutes > 0 AND InvoiceOriginWaitAmount IS NULL THEN ',InvoiceOriginWaitAmount' ELSE '' END
				+ CASE WHEN InvoiceDestinationWaitBillableMinutes > 0 AND InvoiceDestinationWaitAmount IS NULL THEN ',InvoiceDestinationWaitAmount' ELSE '' END
				+ CASE WHEN Rejected = 1 AND InvoiceOrderRejectAmount IS NULL THEN ',InvoiceOrderRejectAmount' ELSE '' END
				+ CASE WHEN InvoiceMinSettlementUnitsID IS NULL THEN ',InvoiceMinSettlementUnits' ELSE '' END
				+ CASE WHEN InvoiceSettlementFactorID IS NULL THEN ',InvoiceSettlementFactor' ELSE '' END
				+ CASE WHEN Rejected = 0 AND InvoiceSettlementFactorID IS NULL THEN ',InvoiceSettlementFactor' ELSE '' END
				+ CASE WHEN Rejected = 0 AND InvoiceSettlementFactorID IS NOT NULL AND dbo.fnOrderSettlementUnits(InvoiceSettlementFactorID, OriginGrossUnits, OriginNetUnits, OriginGrossStdUnits, DestGrossUnits, DestNetUnits, OriginWeightNetUnits, DestWeightNetUnits) IS NULL THEN ','+OrderUnitsField ELSE '' END
				+ CASE WHEN InvoiceWaitFeeParameterID IS NULL THEN ',InvoiceOriginWaitBillableMinutes,InvoiceDestinationWaitBillableMinutes' ELSE '' END
				+ CASE WHEN isnull(FinalActualMiles, 0) = 0 THEN ',ActualMiles' ELSE '' END, 2, 100000), '')
		FROM (
			SELECT O.* 
				, InvoiceRatesAppliedDateUTC = OS.CreateDateUTC
				, OS.BatchID
				, Approved = cast(ISNULL(OA.Approved, 0) as bit)
				, FinalActualMiles = ISNULL(OA.OverrideActualMiles, O.ActualMiles)
				, FinalOriginMinutes = ISNULL(OA.OverrideOriginMinutes, O.OriginMinutes)
				, FinalDestMinutes = ISNULL(OA.OverrideDestMinutes, O.DestMinutes)
				, FinalH2S = cast(CASE WHEN isnull(OA.OverrideH2S, 0) = 1 THEN 0 ELSE O.H2S END as bit)
				, FinalOriginChainup = cast(CASE WHEN isnull(OA.OverrideOriginChainup, 0) = 1 THEN 0 ELSE O.OriginChainup END as bit)
				, FinalDestChainup = cast(CASE WHEN isnull(OA.OverrideDestChainup, 0) = 1 THEN 0 ELSE O.DestChainup END as bit)
				, FinalRerouteCount = CASE WHEN isnull(OA.OverrideReroute, 0) = 1 THEN 0 ELSE isnull(O.RerouteCount, 0) END
				, FC.OrderUnitsField
				, InvoiceBatchNum = OS.BatchNum 
				, InvoiceOriginWaitBillableMinutes = OS.OriginWaitBillableMinutes
				, InvoiceDestinationWaitBillableMinutes = OS.DestinationWaitBillableMinutes
				, InvoiceTotalWaitBillableMinutes = isnull(OS.OriginWaitBillableMinutes, 0) + ISNULL(OS.DestinationWaitBillableMinutes, 0) 
				, InvoiceWaitFeeParameterID = WaitFeeParameterID
				, InvoiceWaitFeeSubUnit = WaitFeeSubUnit
				, InvoiceWaitFeeRoundingType = WaitFeeRoundingType
				, InvoiceOriginWaitRate = OS.OriginWaitRate 
				, InvoiceOriginWaitAmount = OS.OriginWaitAmount 		
				, InvoiceDestinationWaitRate = OS.DestinationWaitRate 
				, InvoiceDestinationWaitAmount = OS.DestinationWaitAmount 
				, InvoiceTotalWaitAmount = OS.TotalWaitAmount
				, InvoiceOrderRejectRate = OS.OrderRejectRate	
				, InvoiceOrderRejectRateType = OS.OrderRejectRateType  
				, InvoiceOrderRejectAmount = OS.OrderRejectAmount  
				, InvoiceOriginChainupRate = OS.OriginChainupRate
				, InvoiceOriginChainupRateType = OS.OriginChainupRateType
				, InvoiceOriginChainupAmount = OS.OriginChainupAmount 
				, InvoiceDestChainupRate = OS.DestChainupRate
				, InvoiceDestChainupRateType = OS.DestChainupRateType
				, InvoiceDestChainupAmount = OS.DestChainupAmount 
				, InvoiceRerouteRate = OS.RerouteRate
				, InvoiceRerouteRateType = OS.RerouteRateType
				, InvoiceRerouteAmount = OS.RerouteAmount 
				, InvoiceH2SRate = OS.H2SRate
				, InvoiceH2SRateType = OS.H2SRateType
				, InvoiceH2SAmount = OS.H2SAmount
				, InvoiceSplitLoadRate = OS.SplitLoadRate
				, InvoiceSplitLoadRateType = OS.SplitLoadRateType
				, InvoiceSplitLoadAmount = OS.SplitLoadAmount
				, InvoiceOtherAmount = OS.OtherAmount
				, InvoiceTaxRate = OS.OriginTaxRate
				, InvoiceSettlementUom = OS.SettlementUom 
				, InvoiceSettlementUomShort = OS.SettlementUomShort 
				, InvoiceCarrierSettlementFactorID = OS.CarrierSettlementFactorID 
				, InvoiceSettlementFactorID = OS.SettlementFactorID 
				, InvoiceSettlementFactor = OS.SettlementFactor 
				, InvoiceMinSettlementUnitsID = OS.MinSettlementUnitsID 
				, InvoiceMinSettlementUnits = OS.MinSettlementUnits
				, InvoiceUnits = OS.SettlementUnits
				, InvoiceRouteRate = OS.RouteRate
				, InvoiceRouteRateType = OS.RouteRateType
				, InvoiceRateSheetRate = OS.RateSheetRate
				, InvoiceRateSheetRateType = OS.RateSheetRateType
				, InvoiceFuelSurchargeRate = OS.FuelSurchargeRate
				, InvoiceFuelSurchargeAmount = OS.FuelSurchargeAmount
				, InvoiceLoadAmount = OS.LoadAmount
				, InvoiceTotalAmount = OS.TotalAmount
			FROM dbo.viewOrder O
			LEFT JOIN viewOrderSettlementCarrier OS ON OS.OrderID = O.ID
			LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
			LEFT JOIN tblSettlementFactor FC ON FC.ID = OS.SettlementFactorID
			WHERE O.StatusID NOT IN (-10) -- don't include "Generated" orders (only Assigned+)
		) X
	) X1



GO


/***********************************/
-- Created: 4.1.0 - 2016/08/15 - Kevin Alons
-- Purpose: return BASE Order details + Driver FINANCIAL INFO into a single view
-- Changes:
-- 4.1.0.2 - 2016.08.28 - KDA	- remove redundant calculation of ORR.RerouteCount
--								- remove redundant tblOrigin JOIN (for H2S which is already available in viewOrder)
-- 4.1.8.2 - 2016.09.21 - JAE	- Check Final Actual miles (null actual miles ok as long as override)
-- 4.1.23	 2016.10.17 - JAE	- Add weight units for settlement
-- 4.4.1	 2016/11/04 - JAE	- Add Destination Chainup
/***********************************/
ALTER VIEW viewOrder_Financial_Base_Driver AS 
	SELECT *
		, HasError = cast(CASE WHEN ErrorFieldCSV IS NOT NULL THEN 1 ELSE 0 END as bit)
	FROM (
		SELECT *
			, OrderID = ID
			, ErrorFieldCSV = nullif(substring(CASE WHEN Approved = 0 THEN ',Approved' ELSE '' END
				+ CASE WHEN InvoiceLoadAmount IS NULL THEN ',InvoiceLoadAmount,InvoiceRouteRate,InvoiceRateSheet' ELSE '' END
				+ CASE WHEN FinalOriginChainup = 1 AND InvoiceOriginChainupAmount IS NULL THEN ',InvoiceOriginChainupAmount' ELSE '' END
				+ CASE WHEN FinalDestChainup = 1 AND InvoiceDestChainupAmount IS NULL THEN ',InvoiceDestChainupAmount' ELSE '' END
				+ CASE WHEN FinalRerouteCount > 0 AND InvoiceRerouteAmount IS NULL THEN ',InvoiceRerouteAmount' ELSE '' END
				+ CASE WHEN FinalH2S = 1 AND InvoiceH2SAmount IS NULL THEN ',InvoiceH2SAmount' ELSE '' END
				+ CASE WHEN TicketCount > 1 AND InvoiceSplitLoadAmount IS NULL THEN ',InvoiceSplitLoadAmount' ELSE '' END
				+ CASE WHEN InvoiceOriginWaitBillableMinutes > 0 AND InvoiceOriginWaitAmount IS NULL THEN ',InvoiceOriginWaitAmount' ELSE '' END
				+ CASE WHEN InvoiceDestinationWaitBillableMinutes > 0 AND InvoiceDestinationWaitAmount IS NULL THEN ',InvoiceDestinationWaitAmount' ELSE '' END
				+ CASE WHEN Rejected = 1 AND InvoiceOrderRejectAmount IS NULL THEN ',InvoiceOrderRejectAmount' ELSE '' END
				+ CASE WHEN InvoiceMinSettlementUnitsID IS NULL THEN ',InvoiceMinSettlementUnits' ELSE '' END
				+ CASE WHEN InvoiceSettlementFactorID IS NULL THEN ',InvoiceSettlementFactor' ELSE '' END
				+ CASE WHEN Rejected = 0 AND InvoiceSettlementFactorID IS NULL THEN ',InvoiceSettlementFactor' ELSE '' END
				+ CASE WHEN Rejected = 0 AND InvoiceSettlementFactorID IS NOT NULL AND dbo.fnOrderSettlementUnits(InvoiceSettlementFactorID, OriginGrossUnits, OriginNetUnits, OriginGrossStdUnits, DestGrossUnits, DestNetUnits, OriginWeightNetUnits, DestWeightNetUnits) IS NULL THEN ','+OrderUnitsField ELSE '' END
				+ CASE WHEN InvoiceWaitFeeParameterID IS NULL THEN ',InvoiceOriginWaitBillableMinutes,InvoiceDestinationWaitBillableMinutes' ELSE '' END
				+ CASE WHEN isnull(FinalActualMiles, 0) = 0 THEN ',ActualMiles' ELSE '' END, 2, 100000), '')
		FROM (
			SELECT O.* 
				, DriverGroup = DG.Name
				, InvoiceRatesAppliedDateUTC = OS.CreateDateUTC
				, OS.BatchID
				, Approved = cast(ISNULL(OA.Approved, 0) as bit)
				, FinalActualMiles = ISNULL(OA.OverrideActualMiles, O.ActualMiles)
				, FinalOriginMinutes = ISNULL(OA.OverrideOriginMinutes, O.OriginMinutes)
				, FinalDestMinutes = ISNULL(OA.OverrideDestMinutes, O.DestMinutes)
				, FinalH2S = cast(CASE WHEN isnull(OA.OverrideH2S, 0) = 1 THEN 0 ELSE O.H2S END as bit)
				, FinalOriginChainup = cast(CASE WHEN isnull(OA.OverrideOriginChainup, 0) = 1 THEN 0 ELSE O.OriginChainup END as bit)
				, FinalDestChainup = cast(CASE WHEN isnull(OA.OverrideDestChainup, 0) = 1 THEN 0 ELSE O.DestChainup END as bit)
				, FinalRerouteCount = CASE WHEN isnull(OA.OverrideReroute, 0) = 1 THEN 0 ELSE isnull(O.RerouteCount, 0) END
				, FC.OrderUnitsField
				, InvoiceBatchNum = OS.BatchNum 
				, InvoiceOriginWaitBillableMinutes = OS.OriginWaitBillableMinutes
				, InvoiceDestinationWaitBillableMinutes = OS.DestinationWaitBillableMinutes
				, InvoiceTotalWaitBillableMinutes = isnull(OS.OriginWaitBillableMinutes, 0) + ISNULL(OS.DestinationWaitBillableMinutes, 0) 
				, InvoiceWaitFeeParameterID = WaitFeeParameterID
				, InvoiceWaitFeeSubUnit = WaitFeeSubUnit
				, InvoiceWaitFeeRoundingType = WaitFeeRoundingType
				, InvoiceOriginWaitRate = OS.OriginWaitRate 
				, InvoiceOriginWaitAmount = OS.OriginWaitAmount 		
				, InvoiceDestinationWaitRate = OS.DestinationWaitRate 
				, InvoiceDestinationWaitAmount = OS.DestinationWaitAmount 
				, InvoiceTotalWaitAmount = OS.TotalWaitAmount
				, InvoiceOrderRejectRate = OS.OrderRejectRate	
				, InvoiceOrderRejectRateType = OS.OrderRejectRateType  
				, InvoiceOrderRejectAmount = OS.OrderRejectAmount  
				, InvoiceOriginChainupRate = OS.OriginChainupRate
				, InvoiceOriginChainupRateType = OS.OriginChainupRateType
				, InvoiceOriginChainupAmount = OS.OriginChainupAmount 
				, InvoiceDestChainupRate = OS.DestChainupRate
				, InvoiceDestChainupRateType = OS.DestChainupRateType
				, InvoiceDestChainupAmount = OS.DestChainupAmount 
				, InvoiceRerouteRate = OS.RerouteRate
				, InvoiceRerouteRateType = OS.RerouteRateType
				, InvoiceRerouteAmount = OS.RerouteAmount 
				, InvoiceH2SRate = OS.H2SRate
				, InvoiceH2SRateType = OS.H2SRateType
				, InvoiceH2SAmount = OS.H2SAmount
				, InvoiceSplitLoadRate = OS.SplitLoadRate
				, InvoiceSplitLoadRateType = OS.SplitLoadRateType
				, InvoiceSplitLoadAmount = OS.SplitLoadAmount
				, InvoiceOtherAmount = OS.OtherAmount
				, InvoiceTaxRate = OS.OriginTaxRate
				, InvoiceSettlementUom = OS.SettlementUom 
				, InvoiceSettlementUomShort = OS.SettlementUomShort 
				, InvoiceCarrierSettlementFactorID = OS.CarrierSettlementFactorID 
				, InvoiceSettlementFactorID = OS.SettlementFactorID 
				, InvoiceSettlementFactor = OS.SettlementFactor 
				, InvoiceMinSettlementUnitsID = OS.MinSettlementUnitsID 
				, InvoiceMinSettlementUnits = OS.MinSettlementUnits
				, InvoiceUnits = OS.SettlementUnits
				, InvoiceRouteRate = OS.RouteRate
				, InvoiceRouteRateType = OS.RouteRateType
				, InvoiceRateSheetRate = OS.RateSheetRate
				, InvoiceRateSheetRateType = OS.RateSheetRateType
				, InvoiceFuelSurchargeRate = OS.FuelSurchargeRate
				, InvoiceFuelSurchargeAmount = OS.FuelSurchargeAmount
				, InvoiceLoadAmount = OS.LoadAmount
				, InvoiceTotalAmount = OS.TotalAmount
			FROM dbo.viewOrder O
			LEFT JOIN tblDriverGroup DG ON DG.ID = O.DriverGroupID
			LEFT JOIN viewOrderSettlementDriver OS ON OS.OrderID = O.ID
			LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
			LEFT JOIN tblSettlementFactor FC ON FC.ID = OS.SettlementFactorID
			WHERE O.StatusID NOT IN (-10) -- don't include "Generated" orders (only Assigned+)
		) X
	) X1



GO


/***********************************/
-- Created: 4.1.0 - 2016/08/15 - Kevin Alons
-- Purpose: return BASE Order details + Shipper FINANCIAL INFO into a single view
-- Changes:
--	4.1.0.2 - 2016.08.28 - KDA	- remove redundant calculation of ORR.RerouteCount
--								- remove redundant tblOrigin JOIN (for H2S which is already available in viewOrder)
--	4.1.8.2 - 2016.09.21 - JAE	- Check Final Actual miles (null actual miles ok as long as override)
--  4.1.23	  2016.10.17 - JAE	- Add weight units for settlement
--  4.4.1	  2016/11/04 - JAE	- Add Destination Chainup
/***********************************/
ALTER VIEW viewOrder_Financial_Base_Shipper AS 
	SELECT *
		, HasError = cast(CASE WHEN ErrorFieldCSV IS NOT NULL THEN 1 ELSE 0 END as bit)
	FROM (
		SELECT *
			, OrderID = ID
			, ErrorFieldCSV = nullif(substring(CASE WHEN Approved = 0 THEN ',Approved' ELSE '' END
				+ CASE WHEN InvoiceLoadAmount IS NULL THEN ',InvoiceLoadAmount,InvoiceRouteRate,InvoiceRateSheet' ELSE '' END
				+ CASE WHEN FinalOriginChainup = 1 AND InvoiceOriginChainupAmount IS NULL THEN ',InvoiceOriginChainupAmount' ELSE '' END
				+ CASE WHEN FinalDestChainup = 1 AND InvoiceDestChainupAmount IS NULL THEN ',InvoiceDestChainupAmount' ELSE '' END
				+ CASE WHEN FinalRerouteCount > 0 AND InvoiceRerouteAmount IS NULL THEN ',InvoiceRerouteAmount' ELSE '' END
				+ CASE WHEN FinalH2S = 1 AND InvoiceH2SAmount IS NULL THEN ',InvoiceH2SAmount' ELSE '' END
				+ CASE WHEN TicketCount > 1 AND InvoiceSplitLoadAmount IS NULL THEN ',InvoiceSplitLoadAmount' ELSE '' END
				+ CASE WHEN InvoiceOriginWaitBillableMinutes > 0 AND InvoiceOriginWaitAmount IS NULL THEN ',InvoiceOriginWaitAmount' ELSE '' END
				+ CASE WHEN InvoiceDestinationWaitBillableMinutes > 0 AND InvoiceDestinationWaitAmount IS NULL THEN ',InvoiceDestinationWaitAmount' ELSE '' END
				+ CASE WHEN Rejected = 1 AND InvoiceOrderRejectAmount IS NULL THEN ',InvoiceOrderRejectAmount' ELSE '' END
				+ CASE WHEN InvoiceMinSettlementUnitsID IS NULL THEN ',InvoiceMinSettlementUnits' ELSE '' END
				+ CASE WHEN InvoiceSettlementFactorID IS NULL THEN ',InvoiceSettlementFactor' ELSE '' END
				+ CASE WHEN Rejected = 0 AND InvoiceSettlementFactorID IS NULL THEN ',InvoiceSettlementFactor' ELSE '' END
				+ CASE WHEN Rejected = 0 AND InvoiceSettlementFactorID IS NOT NULL AND dbo.fnOrderSettlementUnits(InvoiceSettlementFactorID, OriginGrossUnits, OriginNetUnits, OriginGrossStdUnits, DestGrossUnits, DestNetUnits, OriginWeightNetUnits, DestWeightNetUnits) IS NULL THEN ','+OrderUnitsField ELSE '' END
				+ CASE WHEN InvoiceWaitFeeParameterID IS NULL THEN ',InvoiceOriginWaitBillableMinutes,InvoiceDestinationWaitBillableMinutes' ELSE '' END
				+ CASE WHEN isnull(FinalActualMiles, 0) = 0 THEN ',ActualMiles' ELSE '' END, 2, 100000), '')
		FROM (
			SELECT O.* 
				, InvoiceRatesAppliedDateUTC = OS.CreateDateUTC
				, OS.BatchID
				, Approved = cast(ISNULL(OA.Approved, 0) as bit)
				, FinalActualMiles = ISNULL(OA.OverrideActualMiles, O.ActualMiles)
				, FinalOriginMinutes = ISNULL(OA.OverrideOriginMinutes, O.OriginMinutes)
				, FinalDestMinutes = ISNULL(OA.OverrideDestMinutes, O.DestMinutes)
				, FinalH2S = cast(CASE WHEN isnull(OA.OverrideH2S, 0) = 1 THEN 0 ELSE O.H2S END as bit)
				, FinalOriginChainup = cast(CASE WHEN isnull(OA.OverrideOriginChainup, 0) = 1 THEN 0 ELSE O.OriginChainup END as bit)
				, FinalDestChainup = cast(CASE WHEN isnull(OA.OverrideDestChainup, 0) = 1 THEN 0 ELSE O.DestChainup END as bit)
				, FinalRerouteCount = CASE WHEN isnull(OA.OverrideReroute, 0) = 1 THEN 0 ELSE isnull(O.RerouteCount, 0) END
				, FC.OrderUnitsField
				, InvoiceBatchNum = OS.BatchNum 
				, InvoiceOriginWaitBillableMinutes = OS.OriginWaitBillableMinutes
				, InvoiceDestinationWaitBillableMinutes = OS.DestinationWaitBillableMinutes
				, InvoiceTotalWaitBillableMinutes = isnull(OS.OriginWaitBillableMinutes, 0) + ISNULL(OS.DestinationWaitBillableMinutes, 0) 
				, InvoiceWaitFeeParameterID = WaitFeeParameterID
				, InvoiceWaitFeeSubUnit = WaitFeeSubUnit
				, InvoiceWaitFeeRoundingType = WaitFeeRoundingType
				, InvoiceOriginWaitRate = OS.OriginWaitRate 
				, InvoiceOriginWaitAmount = OS.OriginWaitAmount 		
				, InvoiceDestinationWaitRate = OS.DestinationWaitRate 
				, InvoiceDestinationWaitAmount = OS.DestinationWaitAmount 
				, InvoiceTotalWaitAmount = OS.TotalWaitAmount
				, InvoiceOrderRejectRate = OS.OrderRejectRate	
				, InvoiceOrderRejectRateType = OS.OrderRejectRateType  
				, InvoiceOrderRejectAmount = OS.OrderRejectAmount  
				, InvoiceOriginChainupRate = OS.OriginChainupRate
				, InvoiceOriginChainupRateType = OS.OriginChainupRateType
				, InvoiceOriginChainupAmount = OS.OriginChainupAmount 
				, InvoiceDestChainupRate = OS.DestChainupRate
				, InvoiceDestChainupRateType = OS.DestChainupRateType
				, InvoiceDestChainupAmount = OS.DestChainupAmount 
				, InvoiceRerouteRate = OS.RerouteRate
				, InvoiceRerouteRateType = OS.RerouteRateType
				, InvoiceRerouteAmount = OS.RerouteAmount 
				, InvoiceH2SRate = OS.H2SRate
				, InvoiceH2SRateType = OS.H2SRateType
				, InvoiceH2SAmount = OS.H2SAmount
				, InvoiceSplitLoadRate = OS.SplitLoadRate
				, InvoiceSplitLoadRateType = OS.SplitLoadRateType
				, InvoiceSplitLoadAmount = OS.SplitLoadAmount
				, InvoiceOtherAmount = OS.OtherAmount
				, InvoiceTaxRate = OS.OriginTaxRate
				, InvoiceSettlementUom = OS.SettlementUom 
				, InvoiceSettlementUomShort = OS.SettlementUomShort 
				, InvoiceShipperSettlementFactorID = OS.ShipperSettlementFactorID 
				, InvoiceSettlementFactorID = OS.SettlementFactorID 
				, InvoiceSettlementFactor = OS.SettlementFactor 
				, InvoiceMinSettlementUnitsID = OS.MinSettlementUnitsID 
				, InvoiceMinSettlementUnits = OS.MinSettlementUnits
				, InvoiceUnits = OS.SettlementUnits
				, InvoiceRouteRate = OS.RouteRate
				, InvoiceRouteRateType = OS.RouteRateType
				, InvoiceRateSheetRate = OS.RateSheetRate
				, InvoiceRateSheetRateType = OS.RateSheetRateType
				, InvoiceFuelSurchargeRate = OS.FuelSurchargeRate
				, InvoiceFuelSurchargeAmount = OS.FuelSurchargeAmount
				, InvoiceLoadAmount = OS.LoadAmount
				, InvoiceTotalAmount = OS.TotalAmount
			FROM dbo.viewOrder O
			LEFT JOIN viewOrderSettlementShipper OS ON OS.OrderID = O.ID
			LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
			LEFT JOIN tblSettlementFactor FC ON FC.ID = OS.SettlementFactorID
			WHERE O.StatusID NOT IN (-10) -- don't include "Generated" orders (only Assigned+)
		) X
	) X1



GO

/*********************************************
-- Date Created: 22 Feb 2014
-- Author: Kevin Alons
-- Purpose: Concatenate the Assessorial Descriptions for a single order into a tab-separated string
--		4.4.1		11/16/2016		JAE			Add check for system (not assume only 4 system settings)
*********************************************/
ALTER FUNCTION dbo.fnOrderCarrierAssessorialAmountsTSV(@OrderID int, @IncludeSystemRates bit = 0) RETURNS varchar(max) AS
BEGIN
	DECLARE @ret VARCHAR(max) 

	SELECT @ret = isnull(@ret + CHAR(13), '') + ltrim(AC.Amount)
	FROM tblOrderSettlementCarrierAssessorialCharge AC
	JOIN tblAssessorialRateType ART ON ART.ID = AC.AssessorialRateTypeID
	WHERE AC.OrderID = @OrderID
		  AND (@IncludeSystemRates = 1 OR ART.IsSystem = 0)
	
	RETURN (@ret)
END

GO


/*********************************************
-- Date Created: 22 Feb 2014
-- Author: Kevin Alons
-- Purpose: Concatenate the Assessorial Descriptions for a single order into a tab-separated string
--		4.4.1		11/16/2016		JAE			Add check for system (not assume only 4 system settings)
*********************************************/
ALTER FUNCTION fnOrderCarrierAssessorialDetailsTSV(@OrderID int, @IncludeSystemRates bit = 0) RETURNS varchar(max) AS
BEGIN
	DECLARE @ret VARCHAR(max) 

	SELECT @ret = isnull(@ret + CHAR(13), '') 
		+ Name + isnull(CHAR(9) + RatePrefix + ltrim(Rate) + RateSuffix + CHAR(9) + ltrim(TypeID), '')
	FROM (
		SELECT ART.Name 
			, TypeID = AC.AssessorialRateTypeID
			, Rate = cast(AR.Rate as decimal(18, 4))
			, RatePrefix = CASE RT.ID WHEN 1 THEN '$' WHEN 2 THEN '$' WHEN 3 THEN '' WHEN 4 THEN '' END
			, RateSuffix = CASE RT.ID WHEN 1 THEN '/' + U.Abbrev WHEN 2 THEN '$' ELSE RT.Name END
			, AC.Amount
		FROM tblOrderSettlementCarrierAssessorialCharge AC
		JOIN tblAssessorialRateType ART ON ART.ID = AC.AssessorialRateTypeID
		LEFT JOIN tblCarrierAssessorialRate AR ON AR.ID = AC.AssessorialRateID
		LEFT JOIN tblRateType RT ON RT.ID = AR.RateTypeID 
		LEFT JOIN tblUom U ON U.ID = AR.UomID
		WHERE AC.OrderID = @OrderID
		  AND (@IncludeSystemRates = 1 OR ART.IsSystem = 0)
	) X
		
	RETURN (@ret)
END

GO


/*********************************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: Concatenate the Assessorial Descriptions for a single order into a tab-separated string
-- Changes:
--		4.4.1		11/16/2016		JAE			Add check for system (not assume only 4 system settings)
/*********************************************/
ALTER FUNCTION fnOrderDriverAssessorialAmountsTSV(@OrderID int, @IncludeSystemRates bit = 0) RETURNS varchar(max) AS
BEGIN
	DECLARE @ret VARCHAR(max) 

	SELECT @ret = isnull(@ret + CHAR(13), '') + ltrim(AC.Amount)
	FROM tblOrderSettlementDriverAssessorialCharge AC
	JOIN tblAssessorialRateType ART ON ART.ID = AC.AssessorialRateTypeID
	WHERE AC.OrderID = @OrderID
		  AND (@IncludeSystemRates = 1 OR ART.IsSystem = 0)
	
	RETURN (@ret)
END


GO


/*********************************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: Concatenate the Assessorial Descriptions for a single order into a tab-separated string
-- Changes:
--		4.4.1		11/16/2016		JAE			Add check for system (not assume only 4 system settings)
/*********************************************/
ALTER FUNCTION fnOrderDriverAssessorialDetailsTSV(@OrderID int, @IncludeSystemRates bit = 0) RETURNS varchar(max) AS
BEGIN
	DECLARE @ret VARCHAR(max) 

	SELECT @ret = isnull(@ret + CHAR(13), '') 
		+ Name + isnull(CHAR(9) + RatePrefix + ltrim(Rate) + RateSuffix + CHAR(9) + ltrim(TypeID), '')
	FROM (
		SELECT ART.Name 
			, TypeID = AC.AssessorialRateTypeID
			, Rate = cast(AR.Rate as decimal(18, 4))
			, RatePrefix = CASE RT.ID WHEN 1 THEN '$' WHEN 2 THEN '$' WHEN 3 THEN '' WHEN 4 THEN '' END
			, RateSuffix = CASE RT.ID WHEN 1 THEN '/' + U.Abbrev WHEN 2 THEN '$' ELSE RT.Name END
			, AC.Amount
		FROM tblOrderSettlementDriverAssessorialCharge AC
		JOIN tblAssessorialRateType ART ON ART.ID = AC.AssessorialRateTypeID
		LEFT JOIN tblDriverAssessorialRate AR ON AR.ID = AC.AssessorialRateID
		LEFT JOIN tblRateType RT ON RT.ID = AR.RateTypeID 
		LEFT JOIN tblUom U ON U.ID = AR.UomID
		WHERE AC.OrderID = @OrderID
		  AND (@IncludeSystemRates = 1 OR ART.IsSystem = 0)
	) X
		
	RETURN (@ret)
END


GO

/*********************************************
-- Date Created: 22 Feb 2014
-- Author: Kevin Alons
-- Purpose: Concatenate the Assessorial Descriptions for a single order into a tab-separated string
-- Changes:
--		4.4.1		11/16/2016		JAE			Add check for system (not assume only 4 system settings)
*********************************************/
ALTER FUNCTION fnOrderShipperAssessorialAmountsTSV(@OrderID int, @IncludeSystemRates bit = 0) RETURNS varchar(max) AS
BEGIN
	DECLARE @ret VARCHAR(max) 

	SELECT @ret = isnull(@ret + CHAR(13), '') + ltrim(AC.Amount)
	FROM tblOrderSettlementShipperAssessorialCharge AC
	JOIN tblAssessorialRateType ART ON ART.ID = AC.AssessorialRateTypeID
	WHERE AC.OrderID = @OrderID
	  AND (@IncludeSystemRates = 1 OR ART.IsSystem = 0)
	
	RETURN (@ret)
END


GO

/*********************************************
-- Date Created: 22 Feb 2014
-- Author: Kevin Alons
-- Purpose: Concatenate the Assessorial Descriptions for a single order into a tab-separated string
-- Changes:
--		4.4.1		11/16/2016		JAE			Add check for system (not assume only 4 system settings)
*********************************************/
ALTER FUNCTION fnOrderShipperAssessorialDetailsTSV(@OrderID int, @IncludeSystemRates bit = 0) RETURNS varchar(max) AS
BEGIN
	DECLARE @ret VARCHAR(max) 

	SELECT @ret = isnull(@ret + CHAR(13), '') 
		+ Name + isnull(CHAR(9) + RatePrefix + ltrim(Rate) + RateSuffix + CHAR(9) + ltrim(TypeID), '')
	FROM (
		SELECT ART.Name 
			, TypeID = AC.AssessorialRateTypeID
			, Rate = cast(AR.Rate as decimal(18, 4))
			, RatePrefix = CASE RT.ID WHEN 1 THEN '$' WHEN 2 THEN '$' WHEN 3 THEN '' WHEN 4 THEN '' END
			, RateSuffix = CASE RT.ID WHEN 1 THEN '/' + U.Abbrev WHEN 2 THEN '$' ELSE RT.Name END
			, AC.Amount
		FROM tblOrderSettlementShipperAssessorialCharge AC
		JOIN tblAssessorialRateType ART ON ART.ID = AC.AssessorialRateTypeID
		LEFT JOIN tblShipperAssessorialRate AR ON AR.ID = AC.AssessorialRateID
		LEFT JOIN tblRateType RT ON RT.ID = AR.RateTypeID 
		LEFT JOIN tblUom U ON U.ID = AR.UomID
		WHERE AC.OrderID = @OrderID
		  AND (@IncludeSystemRates = 1 OR ART.IsSystem = 0)
	) X
		
	RETURN (@ret)
END

GO


/***********************************
-- Date Created: 2015/08/14
-- Author: Kevin Alons
-- Purpose: create/update APPROVAL on an individual order (and re-rate the order)
-- Changes:
	3.9.12 - 2015/09/01 - KDA - add SET NOCOUNT ON to statement
							  - RAISE an ERROR if no record is updated (either due to target record not in valid state or not found
	3.9.20 - 2015/10/03 - JAE - add OverrideTransferPercentComplete parameters/logic
	3.9.37  - 2015/12/21 - JAE - add overrides for carrier and shipper min settlement units
	3.11.17.2 - 2016/04/18 - JAE - add spUpdateOrderApproval
	4.4.1	- 2016/11/04 - JAE - Add Destination Chainup
***********************************/
ALTER PROCEDURE spUpdateOrderApproval
(
  @ID INT
, @UserName varchar(100)
, @OverrideActualMiles INT = NULL
, @OverrideOriginMinutes INT = NULL
, @OverrideDestMinutes INT = NULL
, @OverrideOriginChainup BIT = NULL
, @OverrideDestChainup BIT = NULL
, @OverrideH2S BIT = NULL
, @OverrideReroute BIT = NULL
, @Approved BIT = 1
, @OverrideTransferPercentComplete INT = NULL
, @OverrideCarrierMinSettlementUnits decimal = NULL
, @OverrideShipperMinSettlementUnits decimal = NULL
)
AS BEGIN
	SET NOCOUNT ON;
	
	/* only allowed AUDITED | non-settled orders from being APPROVED */
	IF EXISTS (
		SELECT O.* 
		FROM tblOrder O 
		LEFT JOIN tblOrderSettlementCarrier OSC ON OSC.OrderID = O.ID
		LEFT JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = O.ID
		LEFT JOIN tblOrderSettlementProducer OSP ON OSP.OrderID = O.ID
		WHERE O.ID = @ID AND O.StatusID IN (4) /* 4 = AUDITED */
		  AND OSC.BatchID IS NULL AND OSS.BatchID IS NULL AND OSP.BatchID IS NULL /* this order is not yet SETTLED */
	)
	BEGIN
		DECLARE @startedTX BIT
		BEGIN TRY
			IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRAN
				SET @startedTX = 1
			END
			UPDATE tblOrderApproval 
				SET OverrideActualMiles = @OverrideActualMiles
					, OverrideOriginMinutes = @OverrideOriginMinutes
					, OverrideDestMinutes = @OverrideDestMinutes
					, OverrideOriginChainup = @OverrideOriginChainup
					, OverrideDestChainup = @OverrideDestChainup
					, OverrideH2S = @OverrideH2S
					, OverrideReroute = @OverrideReroute
					, OverrideTransferPercentComplete = @OverrideTransferPercentComplete
					, OverrideCarrierMinSettlementUnits = @OverrideCarrierMinSettlementUnits 
					, OverrideShipperMinSettlementUnits = @OverrideShipperMinSettlementUnits 
					, Approved = @Approved
			WHERE OrderID = @ID
			IF (@@ROWCOUNT = 0)
			BEGIN
				INSERT INTO tblOrderApproval 
					(OrderID, OverrideActualMiles, OverrideOriginMinutes, OverrideDestMinutes, OverrideOriginChainup, OverrideDestChainup, OverrideH2S, OverrideReroute
						, OverrideTransferPercentComplete, OverrideCarrierMinSettlementUnits, OverrideShipperMinSettlementUnits
						, Approved, CreatedByUser, CreateDateUTC) 
				VALUES 
					(@ID, @OverrideActualMiles, @OverrideOriginMinutes, @OverrideDestMinutes, @OverrideOriginChainup, @OverrideDestChainup, @OverrideH2S, @OverrideReroute
						, @OverrideTransferPercentComplete, @OverrideCarrierMinSettlementUnits, @OverrideShipperMinSettlementUnits
						, @Approved, @UserName, GETUTCDATE())
			END
			/* ensure any changes to this Approval are applied - which will affect applied rates */
			EXEC spApplyRatesBoth @ID, @UserName
			
			IF (@startedTX = 1)
				COMMIT TRAN
		END TRY
		BEGIN CATCH
			DECLARE @msg VARCHAR(MAX), @severity INT
			SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY()
			IF (@startedTX = 1)
				ROLLBACK TRAN
			RAISERROR(@msg, @severity, 1)
		END CATCH
	END
	ELSE
	BEGIN
		RAISERROR('Record not valid for Approval or not found', 16, 1)
	END
END



GO

/*************************************************************
-- Date Created: 15 Jun 2015
-- Author: Kevin Alons
-- Purpose: return the data for the Order Approval page
-- Changes:
	- 3.9.2  - 2015/08/26 - KDA - remove obsolete performance optimization using OrderDateUTC to force use of index (now OrderDate is indexed)
	- 3.9.3  - 2015/08/29 - KDA - add new FinalXXX fields
	- 3.9.20 - 2015/10/26 - JAE - update stored procedure to save transfer override values
	- 3.9.37 - 2015/12/21 - JAE - Added carrier and shipper min settlement units
	- 4.4.1	 - 2016/11/04 - JAE	- Add Destination Chainup
*************************************************************/
ALTER PROCEDURE spOrderApprovalSource
(
  @userName VARCHAR(100)
, @ShipperID INT = NULL
, @CarrierID INT = NULL
, @ProductGroupID INT = NULL
, @OriginStateID INT = NULL
, @DestStateID INT = NULL
, @Start DATE = NULL
, @End DATE = NULL
, @IncludeApproved BIT = 0
) AS BEGIN
	SET NOCOUNT ON;
	
	SELECT *
		, FinalActualMiles = ISNULL(OverrideActualMiles, ActualMiles)
		, FinalOriginMinutes = ISNULL(OverrideOriginMinutes, OriginMinutes)
		, FinalDestMinutes = ISNULL(OverrideDestMinutes, DestMinutes)
		, FinalOriginChainup = CAST(ISNULL(1-NULLIF(OverrideOriginChainup, 0), OriginChainup) AS BIT)
		, FinalDestChainup = CAST(ISNULL(1-NULLIF(OverrideDestChainup, 0), DestChainup) AS BIT)
		, FinalH2S = CAST(ISNULL(1-NULLIF(OverrideH2S, 0), H2S) AS BIT)
		, FinalRerouted = CAST(ISNULL(1-NULLIF(OverrideReroute, 0), Rerouted) AS BIT)
		, FinalTransferPercentComplete = ISNULL(OverrideTransferPercentComplete, TransferPercentComplete)
		, FinalCarrierMinSettlementUnits = ISNULL(OverrideCarrierMinSettlementUnits, CarrierMinSettlementUnits)
		, FinalShipperMinSettlementUnits = ISNULL(OverrideShipperMinSettlementUnits, ShipperMinSettlementUnits)
	INTO #X
	FROM viewOrderApprovalSource 
	WHERE (NULLIF(@ShipperID, -1) IS NULL OR ShipperID = @ShipperID)
	  AND (NULLIF(@CarrierID, -1) IS NULL OR CarrierID = @CarrierID)
	  AND (NULLIF(@ProductGroupID, -1) IS NULL OR ProductGroupID = @ProductGroupID)
	  AND (NULLIF(@OriginStateID, -1) IS NULL OR OriginStateID = @OriginStateID)
	  AND (NULLIF(@DestStateID, -1) IS NULL OR DestStateID = @DestStateID)
	  AND OrderDate BETWEEN ISNULL(@Start, '1/1/1900') AND ISNULL(@End, '1/1/2200')
	  AND (@IncludeApproved = 1 OR ISNULL(Approved, 0) = 0) 
	  
	DECLARE @id int
	WHILE EXISTS (SELECT * FROM #x WHERE OrderID IS NULL)
	BEGIN
		SELECT @id = min(ID) FROM #x WHERE OrderID IS NULL
		EXEC spApplyRatesBoth @id, @userName
		UPDATE #x SET OrderID = @id, Approved = (SELECT Approved FROM tblOrderApproval WHERE OrderID = @id) WHERE ID = @id
	END
	ALTER TABLE #X DROP COLUMN OrderID
	
	SELECT * FROM #x WHERE (@IncludeApproved = 1 OR ISNULL(Approved, 0) = 0) 

END



GO


/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier AssessorialRate info for the specified order
-- Changes:
	- 3.7.28 - 2015/06/18 - KDA - add new columns: add DriverID/DriverGroupID parameters, move ProducerID parameter to end (so consistent with all other Best-Match rates/parameters
	- 3.7.30 - 2015/06/19 - KDA - remove DriverID
	- 3.9.19.6 - 2015/09/30 - KDA - honor Order Approval . Override values when computing Rates
	- 3.9.21 - 2015/10/03 - KDA - use [viewOrder] O.OriginDriverGroupID instead of O.DriverGroupID (which could be DestDriver.DriverGroupID)
	- 3.10.13.6 - 2015/03/01 - JAE - add TruckTypeID
	- 4.4.1	 - 2016/11/04 - JAE	- Add Destination Chainup
***********************************/
ALTER FUNCTION fnOrderCarrierAssessorialRates(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, TypeID, Rate, RateTypeID, UomID
	FROM dbo.viewOrder O
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
	CROSS APPLY dbo.fnCarrierAssessorialRates(O.OrderDate, null, null, O.CustomerID, O.CarrierID, O.ProductGroupID, O.TruckTypeID, O.OriginDriverGroupID, O.OriginID, O.DestinationID, O.OriginStateID, O.DestStateID, O.OriginRegionID, O.ProducerID, 1) R
	WHERE O.ID = @ID
	  AND ((R.TypeID = 1 AND CASE WHEN isnull(OA.OverrideOriginChainup, 0) = 1 THEN 0 ELSE O.OriginChainUp END = 1)
		OR (R.TypeID = 5 AND CASE WHEN isnull(OA.OverrideDestChainup, 0) = 1 THEN 0 ELSE O.DestChainUp END = 1)
		OR (R.TypeID = 2 AND CASE WHEN isnull(OA.OverrideReroute, 0) = 1 THEN 0 ELSE O.RerouteCount END > 0)
		OR (R.TypeID = 3 AND O.TicketCount > 1)
		OR (R.TypeID = 4 AND CASE WHEN isnull(OA.OverrideH2S, 0) = 1 THEN 0 ELSE O.H2S END = 1)
		OR R.TypeID IN (SELECT ID FROM tblAssessorialRateType WHERE IsSystem = 0)) -- custom assessorial rate


GO


/***********************************
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: retrieve and return the Driver AssessorialRate info for the specified order
-- Changes:
	- 4.4.1	 - 2016/11/04 - JAE	- Add Destination Chainup
***********************************/
ALTER FUNCTION fnOrderDriverAssessorialRates(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, TypeID, Rate, RateTypeID, UomID
	FROM dbo.viewOrder O
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
	CROSS APPLY dbo.fnDriverAssessorialRates(O.OrderDate, null, null, O.CustomerID, O.CarrierID, O.ProductGroupID, O.TruckTypeID, O.OriginDriverGroupID, O.DriverID, O.OriginID, O.DestinationID, O.OriginStateID, O.DestStateID, O.OriginRegionID, O.ProducerID, 1) R
	WHERE O.ID = @ID
	  AND ((R.TypeID = 1 AND CASE WHEN isnull(OA.OverrideOriginChainup, 0) = 1 THEN 0 ELSE O.OriginChainUp END = 1)
		OR (R.TypeID = 5 AND CASE WHEN isnull(OA.OverrideDestChainup, 0) = 1 THEN 0 ELSE O.DestChainUp END = 1)
		OR (R.TypeID = 2 AND CASE WHEN isnull(OA.OverrideReroute, 0) = 1 THEN 0 ELSE O.RerouteCount END > 0)
		OR (R.TypeID = 3 AND O.TicketCount > 1)
		OR (R.TypeID = 4 AND CASE WHEN isnull(OA.OverrideH2S, 0) = 1 THEN 0 ELSE O.H2S END = 1)
		OR R.TypeID IN (SELECT ID FROM tblAssessorialRateType WHERE IsSystem = 0)) -- custom assessorial rate

GO


/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper AssessorialRate info for the specified order
-- Changes:
	- 3.9.19.6 - 2015/09/30 - KDA - honor Order Approval . Override values when computing Rates
	- 3.10.13.6 - 2015/03/01 - JAE - add TruckTypeID
	- 4.4.1	 - 2016/11/04 - JAE	- Add Destination Chainup
***********************************/
ALTER FUNCTION fnOrderShipperAssessorialRates(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, TypeID, Rate, RateTypeID, UomID
	FROM dbo.viewOrder O
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
	CROSS APPLY dbo.fnShipperAssessorialRates(O.OrderDate, null, null, O.CustomerID, O.ProductGroupID, O.TruckTypeID, O.OriginID, O.DestinationID, O.OriginStateID, O.DestStateID, O.OriginRegionID, O.ProducerID, 1) R
	WHERE O.ID = @ID
	  AND ((R.TypeID = 1 AND CASE WHEN isnull(OA.OverrideOriginChainup, 0) = 1 THEN 0 ELSE O.OriginChainUp END = 1)
		OR (R.TypeID = 5 AND CASE WHEN isnull(OA.OverrideDestChainup, 0) = 1 THEN 0 ELSE O.DestChainUp END = 1)
		OR (R.TypeID = 2 AND CASE WHEN isnull(OA.OverrideReroute, 0) = 1 THEN 0 ELSE O.RerouteCount END > 0)
		OR (R.TypeID = 3 AND O.TicketCount > 1)
		OR (R.TypeID = 4 AND CASE WHEN isnull(OA.OverrideH2S, 0) = 1 THEN 0 ELSE O.H2S END = 1)
		OR R.TypeID IN (SELECT ID FROM tblAssessorialRateType WHERE IsSystem = 0)) -- custom assessorial rate


GO




/*************************************************/
-- Created: 2013.06.02 - Kevin Alons
-- Purpose: compute and add the various Carrier "Settlement" $$ values to an Delivered/Audited order
-- Changes:
-- 3.7.26	- 2015/06/13	- KDA	- add support for: Driver|DriverGroup, Producer to some rates, separate Carrier|Shipper MinSettlementUnits|SettlementFactor best-match values, 
-- 3.9.0	- 2015/08/13	- KDA	- honor Approve overrides in rating process
-- 3.9.9	- 2015/08/31	- KDA	- add new @AllowReApplyPostBatch parameter to allow re-applying after batch is applied
-- 3.9.19.2	- 2015/09/24	- KDA	- fix erroneous logic that computed Asssessorial Settlement results before the Order Level results were posted
-- 3.9.37	- 2015/12/22	- JAE	- Use tblOrderApproval to get override for Carrier Min Settlement Units
-- 3.9.39	- 2016/01/15	- JAE	- Also need to recalculate settlement units since the calculated/max values was already passed
-- 3.13.1	- 2016/07/04	- KDA	- add START/DONE PRINT statements (for debugging purposes)
-- 4.1.8.6	- 2016.09.21	- KDA	- move populate #IA table before transaction starts
--									- @debugStatements parameter + add additional START/DONE PRINT statements (for debugging purposes)
-- 4.1.9.1	- 2016.09.26	- KDA	- restore regressed 3.9.19.2 changes that accidentally got reverted with 4.1.8.6 (added a NOTE to hopefully prevent this again)
-- 4.4.1	- 2016/11/04	- JAE	- Add Destination Chainup
/*************************************************/
ALTER PROCEDURE spApplyRatesCarrier
(
  @ID int
, @UserName varchar(100)
, @OriginWaitAmount money = NULL
, @DestWaitAmount money = NULL
, @RejectionAmount money = NULL
, @FuelSurchargeAmount money = NULL
, @LoadAmount money = NULL
, @AssessorialRateTypeID_CSV varchar(max) = NULL
, @AssessorialAmount_CSV varchar(max) = null
, @ResetOverrides bit = 0
, @AllowReApplyPostBatch bit = 0
, @debugStatements bit = 0 -- turn on to get debugging statements during execution
) AS BEGIN

	SET NOCOUNT ON

	IF (@debugStatements = 1) PRINT 'spApplyRatesCarrier (' + ltrim(@ID) + ') START ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	
	IF (@AllowReApplyPostBatch = 0) AND EXISTS (SELECT * FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND BatchID IS NOT NULL)
	BEGIN
		RAISERROR('Invoice has already been Carrier Settled', 16, 1)
		RETURN
	END
	
	/* ensure that Shipper Rates have been applied prior to applying Carrier rates (since they could be dependent on Shipper rates) */
	IF NOT EXISTS (SELECT * FROM tblOrderSettlementShipper WHERE OrderID = @ID)
		EXEC spApplyRatesShipper @ID, @UserName

	/* transform the provided manual Assessorial Rates (if any) into a queryable table variable */
	DECLARE @ManualAssessorialRates TABLE (ID int primary key, Amount money)
	INSERT INTO @ManualAssessorialRates (ID, Amount) 
		SELECT ID.ID, R.Amount
		FROM (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), ID FROM dbo.fnSplitCSVIDs(@AssessorialRateTypeID_CSV)) ID
		JOIN (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), amount = CAST(value as money) FROM dbo.fnSplitCSV(@AssessorialAmount_CSV)) R ON R.sortid = ID.sortid

	/* persist all existing MANUAL Assessorial Rates from the existing Order Settlement (if any present and not resetting them) */
	INSERT INTO @ManualAssessorialRates (ID, Amount)
		SELECT AssessorialRateTypeID, Amount 
		FROM dbo.tblOrderSettlementCarrierAssessorialCharge 
		WHERE OrderID = @ID AND AssessorialRateID IS NULL 
			AND ID NOT IN (SELECT ID FROM @ManualAssessorialRates)
			AND @ResetOverrides = 0

	/* persist all existing settlement amount overrides from the existing Order Settlement - if not resetting them */
	IF (@ResetOverrides = 0)
	BEGIN 
		SELECT @OriginWaitAmount = OriginWaitAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @OriginWaitAmount IS NULL AND OriginWaitRateID IS NULL
		SELECT @DestWaitAmount = DestinationWaitAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @DestWaitAmount IS NULL AND DestinationWaitRateID IS NULL
		SELECT @RejectionAmount = OrderRejectAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @RejectionAmount IS NULL AND OrderRejectRateID IS NULL
		SELECT @FuelSurchargeAmount = FuelSurchargeAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @FuelSurchargeAmount IS NULL AND FuelSurchargeRateID IS NULL
		SELECT @LoadAmount = LoadAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @LoadAmount IS NULL AND RouteRateID IS NULL AND RangeRateID IS NULL
	END

	/* compute the basic settlement units from the Order being invoices */
	DECLARE @SettlementUnits TABLE
	(
		OrderID int
	  , SettlementUomID int
	  , SettlementFactorID int
	  , CarrierSettlementFactorID int
	  , MinSettlementUnits decimal(18, 10)
	  , MinSettlementUnitsID int
	  , ActualUnits decimal(18, 10)
	  , SettlementUnits decimal(18, 10) -- calculate value basically max(actual, minimum)
	)
	INSERT INTO @SettlementUnits (OrderID, SettlementUomID, SettlementFactorID, CarrierSettlementFactorID, MinSettlementUnits, MinSettlementUnitsID, ActualUnits, SettlementUnits)
		SELECT OSU.OrderID, OSU.SettlementUomID, OSU.SettlementFactorID, OSU.CarrierSettlementFactorID, ISNULL(OA.OverrideCarrierMinSettlementUnits, OSU.MinSettlementUnits), OSU.MinSettlementUnitsID, OSU.ActualUnits, 
			-- use passed settlement (already calculated) or pick the max between the actual and overrride min - JAE 1/15/16 - 3.9.39
			CASE WHEN OA.OverrideCarrierMinSettlementUnits IS NULL THEN OSU.SettlementUnits
						ELSE dbo.fnMaxDecimal(OSU.ActualUnits, OA.OverrideCarrierMinSettlementUnits) END
		FROM dbo.viewOrderSettlementUnitsCarrier OSU
		LEFT JOIN tblOrderApproval OA ON OA.OrderID = OSU.OrderID
		WHERE OSU.OrderID = @ID

	/* compute the order level settlement rateIDs & settlement amounts - into temp table #I */
	SELECT OrderID = @ID
		, OrderDate
		, CarrierSettlementFactorID, MinSettlementUnitsID
		, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
		, RouteRateID, RangeRateID, LoadAmount
		, WaitFeeParameterID
		, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount 
		, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount
		, OrderRejectRateID, OrderRejectAmount
		, FuelSurchargeRateID, FuelSurchargeRate, FuelSurchargeAmount
		, OriginTaxRate
		, TotalAmount = ISNULL(LoadAmount, 0) 
			+ ISNULL(OriginWaitAmount, 0) 
			+ ISNULL(DestWaitAmount, 0) 
			+ ISNULL(OrderRejectAmount, 0) 
			+ ISNULL(FuelSurchargeAmount, 0)
		, CreatedByUser = @UserName
		, OriginChainUp, DestChainUp, H2S, Rerouted
	INTO #I
	FROM (
		SELECT OrderDate
			, CarrierSettlementFactorID, MinSettlementUnitsID
			, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
			, RouteRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RouteRateID ELSE NULL END
			, RangeRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RangeRateID ELSE NULL END
			, LoadAmount = round(ISNULL(@LoadAmount, RR.Amount), 2)
			, WaitFeeParameterID
			, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount = ROUND(OriginWaitAmount, 2)
			, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount = ROUND(DestWaitAmount, 2)
			, OrderRejectRateID = CASE WHEN @RejectionAmount IS NULL THEN RD.RateID ELSE NULL END
			, OrderRejectAmount = round(isnull(@RejectionAmount, RD.Amount), 2)
			, FuelSurchargeRateID = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.RateID ELSE NULL END
			, FuelSurchargeRate = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.Rate ELSE NULL END
			, FuelSurchargeAmount = round(isnull(@FuelSurchargeAmount, FSR.Amount), 2)
			, OriginTaxRate
			, OriginChainUp, DestChainup, H2S, Rerouted = cast(CASE WHEN RerouteCount = 0 THEN 0 ELSE 1 END as bit) /* used for the Auto-Approve logic below */
		FROM (
			SELECT O.ID
				, O.OriginID
				, O.CarrierID
				, O.RouteID
				, SU.CarrierSettlementFactorID
				, SU.SettlementFactorID
				, SU.SettlementUomID
				, SU.ActualUnits
				, SU.MinSettlementUnitsID
				, SU.MinSettlementUnits
				, SettlementUnits = CASE WHEN O.Rejected = 1 THEN 0 ELSE SU.SettlementUnits END
				, RouteMiles = isnull(OA.OverrideActualMiles, O.ActualMiles)
				, OrderDate = isnull(O.OrderDate, O.DueDate)
				, OriginChainUp = cast(CASE WHEN OA.OverrideOriginChainup = 1 THEN 0 ELSE O.OriginChainup END as bit)
				, DestChainUp = cast(CASE WHEN OA.OverrideDestChainup = 1 THEN 0 ELSE O.DestChainup END as bit)
				, H2S = cast(CASE WHEN OA.OverrideH2S = 1 THEN 0 ELSE O.H2S END as bit)
				, O.Rejected
				, RerouteCount = CASE WHEN OA.OverrideReroute = 1 THEN 0 ELSE O.RerouteCount END
				, IsSplit = CASE WHEN O.Rejected = 0 AND O.TicketCount > 1 THEN 1 ELSE 0 END
				, WaitFeeParameterID = WFP.ID
				, OriginWaitRateID = CASE WHEN @OriginWaitAmount IS NULL THEN OWD.RateID ELSE NULL END
				, OriginWaitBillableMinutes = OWD.BillableMinutes
				, OriginWaitBillableHours = OWD.BillableHours
				, OriginWaitAmount = isnull(@OriginWaitAmount, OWD.Amount)
				, DestWaitRateID = CASE WHEN @DestWaitAmount IS NULL THEN DWD.RateID ELSE NULL END
				, DestWaitBillableMinutes = DWD.BillableMinutes
				, DestWaitBillableHours = DWD.BillableHours
				, DestWaitAmount = isnull(@DestWaitAmount, DWD.Amount)
				, OriginTaxRate = OO.TaxRate
			FROM dbo.viewOrder O
			JOIN @SettlementUnits SU ON SU.OrderID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
			OUTER APPLY dbo.fnOrderCarrierOriginWaitData(@ID) OWD 
			OUTER APPLY dbo.fnOrderCarrierDestinationWaitData(@ID) DWD 
			OUTER APPLY dbo.fnOrderCarrierWaitFeeParameter(@ID) WFP
			WHERE O.ID = @ID
		) X
		OUTER APPLY dbo.fnOrderCarrierLoadAmount(@ID, SettlementUnits, SettlementUomID) RR 
		OUTER APPLY dbo.fnOrderCarrierOrderRejectData(@ID, SettlementUnits, SettlementUomID) RD
		OUTER APPLY dbo.fnOrderCarrierFuelSurchargeData(@ID) FSR
	) X2

	DECLARE @CreatedTran bit; SET @CreatedTran = CASE WHEN @@TRANCOUNT = 0 THEN 1 ELSE 0 END
	BEGIN TRY
		IF (@CreatedTran = 1)
			BEGIN TRAN

		-- remove the existing settlment record (if any)
		DELETE FROM tblOrderSettlementCarrierAssessorialCharge WHERE OrderID = @ID
		DELETE FROM tblOrderSettlementCarrier WHERE OrderID = @ID
	
		-- all Units and Rates are first normalized to the Order.OriginUOM then consistent processing
		-- and persisted in the Invoice (Settlement) record in this Order.OriginUOM 
		INSERT INTO tblOrderSettlementCarrier 
			(OrderID
			, OrderDate
			, CarrierSettlementFactorID
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnitsID
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestinationWaitRateID 
			, DestinationWaitBillableMinutes 
			, DestinationWaitBillableHours
			, DestinationWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser)
		SELECT OrderID
			, OrderDate
			, CarrierSettlementFactorID
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnitsID
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestWaitRateID 
			, DestWaitBillableMinutes 
			, DestWaitBillableHours
			, DestWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser
		FROM #I

		/* compute the new Assessorial Rates for this order - into temp table #IA */
		/* NOTE: while it would be preferred for this to be out of the TX, it can't because it relies on the final Settlement changes being present */
		SELECT OrderID = @ID, AssessorialRateTypeID, AssessorialRateID = RateID, Amount = ROUND(Amount, 2), CreatedByUser = @UserName 
		INTO #IA
		FROM @SettlementUnits SU
		CROSS APPLY dbo.fnOrderCarrierAssessorialAmounts(@ID, SU.SettlementUnits, SU.SettlementUomID) CAA
		WHERE CAA.AssessorialRateTypeID NOT IN (SELECT ID FROM @ManualAssessorialRates)

		/* apply the computed settlement assessorial rate/amount records to the DB */
		INSERT INTO tblOrderSettlementCarrierAssessorialCharge (OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser)
			SELECT OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser FROM #IA
			UNION
			SELECT @ID, ID, NULL, Amount, @UserName FROM @ManualAssessorialRates

		IF (@CreatedTran = 1)
			COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @msg varchar(max), @severity int
		SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY()
		ROLLBACK
		RAISERROR(@msg, @severity, 1)
		RETURN
	END CATCH	

	IF (@debugStatements = 1) PRINT 'spApplyRatesCarrier (' + ltrim(@ID) + ') DONE ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	
END


GO


/*************************************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: compute and add the various Driver "Settlement" $$ values to an Delivered/Audited order
-- Changes:
-- 4.1.8.6	- 2016.09.21	- KDA	- move populate #IA table before transaction starts
--									- @debugStatements parameter + add additional START/DONE PRINT statements (for debugging purposes)
-- 4.1.9.1	- 2016.09.26	- KDA	- restore regressed 3.9.19.2 changes that accidentally got reverted with 4.1.8.6 (added a NOTE to hopefully prevent this again)
-- 4.4.1	- 2016/11/04	- JAE	- Add Destination Chainup
/*************************************************/
ALTER PROCEDURE spApplyRatesDriver
(
  @ID int
, @UserName varchar(100)
, @OriginWaitAmount money = NULL
, @DestWaitAmount money = NULL
, @RejectionAmount money = NULL
, @FuelSurchargeAmount money = NULL
, @LoadAmount money = NULL
, @AssessorialRateTypeID_CSV varchar(max) = NULL
, @AssessorialAmount_CSV varchar(max) = null
, @ResetOverrides bit = 0
, @AllowReApplyPostBatch bit = 0
, @debugStatements bit = 0 -- turn on to get debugging statements during execution
) AS BEGIN

	SET NOCOUNT ON

	IF (@debugStatements = 1) PRINT 'spApplyRatesDriver (' + ltrim(@ID) + ') START ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	
	IF (@AllowReApplyPostBatch = 0) AND EXISTS (SELECT * FROM tblOrderSettlementDriver WHERE OrderID = @ID AND BatchID IS NOT NULL)
	BEGIN
		RAISERROR('Invoice has already been Driver Settled', 16, 1)
		RETURN
	END
	
	/* ensure that Carrier Rates have been applied prior to applying Driver rates (since they could be dependent on Carrier rates) */
	IF NOT EXISTS (SELECT * FROM tblOrderSettlementCarrier WHERE OrderID = @ID)
		EXEC spApplyRatesCarrier @ID, @UserName

	/* transform the provided manual Assessorial Rates (if any) into a queryable table variable */
	DECLARE @ManualAssessorialRates TABLE (ID int primary key, Amount money)
	INSERT INTO @ManualAssessorialRates (ID, Amount) 
		SELECT ID.ID, R.Amount
		FROM (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), ID FROM dbo.fnSplitCSVIDs(@AssessorialRateTypeID_CSV)) ID
		JOIN (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), amount = CAST(value as money) FROM dbo.fnSplitCSV(@AssessorialAmount_CSV)) R ON R.sortid = ID.sortid

	/* persist all existing MANUAL Assessorial Rates from the existing Order Settlement (if any present and not resetting them) */
	INSERT INTO @ManualAssessorialRates (ID, Amount)
		SELECT AssessorialRateTypeID, Amount 
		FROM dbo.tblOrderSettlementDriverAssessorialCharge 
		WHERE OrderID = @ID AND AssessorialRateID IS NULL 
			AND ID NOT IN (SELECT ID FROM @ManualAssessorialRates)
			AND @ResetOverrides = 0

	/* persist all existing settlement amount overrides from the existing Order Settlement - if not resetting them */
	IF (@ResetOverrides = 0)
	BEGIN 
		SELECT @OriginWaitAmount = OriginWaitAmount FROM tblOrderSettlementDriver WHERE OrderID = @ID AND @OriginWaitAmount IS NULL AND OriginWaitRateID IS NULL
		SELECT @DestWaitAmount = DestinationWaitAmount FROM tblOrderSettlementDriver WHERE OrderID = @ID AND @DestWaitAmount IS NULL AND DestinationWaitRateID IS NULL
		SELECT @RejectionAmount = OrderRejectAmount FROM tblOrderSettlementDriver WHERE OrderID = @ID AND @RejectionAmount IS NULL AND OrderRejectRateID IS NULL
		SELECT @FuelSurchargeAmount = FuelSurchargeAmount FROM tblOrderSettlementDriver WHERE OrderID = @ID AND @FuelSurchargeAmount IS NULL AND FuelSurchargeRateID IS NULL
		SELECT @LoadAmount = LoadAmount FROM tblOrderSettlementDriver WHERE OrderID = @ID AND @LoadAmount IS NULL AND RouteRateID IS NULL AND RangeRateID IS NULL
	END

	/* compute the basic settlement units from the Order being invoices */
	DECLARE @SettlementUnits TABLE
	(
		OrderID int
	  , SettlementUomID int
	  , SettlementFactorID int
	  , CarrierSettlementFactorID int
	  , MinSettlementUnits decimal(18, 10)
	  , MinSettlementUnitsID int
	  , ActualUnits decimal(18, 10)
	  , SettlementUnits decimal(18, 10) -- calculate value basically max(actual, minimum)
	)
	INSERT INTO @SettlementUnits (OrderID, SettlementUomID, SettlementFactorID, CarrierSettlementFactorID, MinSettlementUnits, MinSettlementUnitsID, ActualUnits, SettlementUnits)
		SELECT OSU.OrderID, OSU.SettlementUomID, OSU.SettlementFactorID, OSU.CarrierSettlementFactorID, ISNULL(OA.OverrideCarrierMinSettlementUnits, OSU.MinSettlementUnits), OSU.MinSettlementUnitsID, OSU.ActualUnits
			-- use passed settlement (already calculated) or pick the max between the actual and overrride min - JAE 1/15/16 - 3.9.39
			, CASE WHEN OA.OverrideCarrierMinSettlementUnits IS NULL THEN OSU.SettlementUnits
					ELSE dbo.fnMaxDecimal(OSU.ActualUnits, OA.OverrideCarrierMinSettlementUnits) END
		FROM dbo.viewOrderSettlementUnitsCarrier OSU
		LEFT JOIN tblOrderApproval OA ON OA.OrderID = OSU.OrderID
		WHERE OSU.OrderID = @ID

	/* compute the order level settlement rateIDs & settlement amounts - into temp table #I */
	SELECT OrderID = @ID
		, OrderDate
		, CarrierSettlementFactorID, MinSettlementUnitsID
		, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
		, RouteRateID, RangeRateID, LoadAmount
		, WaitFeeParameterID
		, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount 
		, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount
		, OrderRejectRateID, OrderRejectAmount
		, FuelSurchargeRateID, FuelSurchargeRate, FuelSurchargeAmount
		, OriginTaxRate
		, TotalAmount = ISNULL(LoadAmount, 0) 
			+ ISNULL(OriginWaitAmount, 0) 
			+ ISNULL(DestWaitAmount, 0) 
			+ ISNULL(OrderRejectAmount, 0) 
			+ ISNULL(FuelSurchargeAmount, 0)
		, CreatedByUser = @UserName
		, OriginChainUp, DestChainUp, H2S, Rerouted
	INTO #I
	FROM (
		SELECT OrderDate
			, CarrierSettlementFactorID, MinSettlementUnitsID
			, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
			, RouteRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RouteRateID ELSE NULL END
			, RangeRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RangeRateID ELSE NULL END
			, LoadAmount = round(ISNULL(@LoadAmount, RR.Amount), 2)
			, WaitFeeParameterID
			, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount = ROUND(OriginWaitAmount, 2)
			, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount = ROUND(DestWaitAmount, 2)
			, OrderRejectRateID = CASE WHEN @RejectionAmount IS NULL THEN RD.RateID ELSE NULL END
			, OrderRejectAmount = round(isnull(@RejectionAmount, RD.Amount), 2)
			, FuelSurchargeRateID = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.RateID ELSE NULL END
			, FuelSurchargeRate = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.Rate ELSE NULL END
			, FuelSurchargeAmount = round(isnull(@FuelSurchargeAmount, FSR.Amount), 2)
			, OriginTaxRate
			, OriginChainUp, DestChainup, H2S, Rerouted = cast(CASE WHEN RerouteCount = 0 THEN 0 ELSE 1 END as bit) /* used for the Auto-Approve logic below */
		FROM (
			SELECT O.ID
				, O.OriginID
				, O.CarrierID
				, O.RouteID
				, SU.CarrierSettlementFactorID
				, SU.SettlementFactorID
				, SU.SettlementUomID
				, SU.ActualUnits
				, SU.MinSettlementUnitsID
				, SU.MinSettlementUnits
				, SettlementUnits = CASE WHEN O.Rejected = 1 THEN 0 ELSE SU.SettlementUnits END
				, RouteMiles = isnull(OA.OverrideActualMiles, O.ActualMiles)
				, OrderDate = isnull(O.OrderDate, O.DueDate)
				, OriginChainUp = cast(CASE WHEN OA.OverrideOriginChainup = 1 THEN 0 ELSE O.OriginChainup END as bit)
				, DestChainUp = cast(CASE WHEN OA.OverrideDestChainup = 1 THEN 0 ELSE O.DestChainup END as bit)
				, H2S = cast(CASE WHEN OA.OverrideH2S = 1 THEN 0 ELSE O.H2S END as bit)
				, O.Rejected
				, RerouteCount = CASE WHEN OA.OverrideReroute = 1 THEN 0 ELSE O.RerouteCount END
				, IsSplit = CASE WHEN O.Rejected = 0 AND O.TicketCount > 1 THEN 1 ELSE 0 END
				, WaitFeeParameterID = WFP.ID
				, OriginWaitRateID = CASE WHEN @OriginWaitAmount IS NULL THEN OWD.RateID ELSE NULL END
				, OriginWaitBillableMinutes = OWD.BillableMinutes
				, OriginWaitBillableHours = OWD.BillableHours
				, OriginWaitAmount = isnull(@OriginWaitAmount, OWD.Amount)
				, DestWaitRateID = CASE WHEN @DestWaitAmount IS NULL THEN DWD.RateID ELSE NULL END
				, DestWaitBillableMinutes = DWD.BillableMinutes
				, DestWaitBillableHours = DWD.BillableHours
				, DestWaitAmount = isnull(@DestWaitAmount, DWD.Amount)
				, OriginTaxRate = OO.TaxRate
			FROM dbo.viewOrder O
			JOIN @SettlementUnits SU ON SU.OrderID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
			OUTER APPLY dbo.fnOrderDriverOriginWaitData(@ID) OWD 
			OUTER APPLY dbo.fnOrderDriverDestinationWaitData(@ID) DWD 
			OUTER APPLY dbo.fnOrderCarrierWaitFeeParameter(@ID) WFP
			WHERE O.ID = @ID
		) X
		OUTER APPLY dbo.fnOrderDriverLoadAmount(@ID, SettlementUnits, SettlementUomID) RR 
		OUTER APPLY dbo.fnOrderDriverOrderRejectData(@ID, SettlementUnits, SettlementUomID) RD
		OUTER APPLY dbo.fnOrderDriverFuelSurchargeData(@ID) FSR
	) X2

	DECLARE @CreatedTran bit; SET @CreatedTran = CASE WHEN @@TRANCOUNT = 0 THEN 1 ELSE 0 END
	BEGIN TRY
		IF (@CreatedTran = 1)
			BEGIN TRAN

		-- remove the existing settlment record (if any)
		DELETE FROM tblOrderSettlementDriverAssessorialCharge WHERE OrderID = @ID
		DELETE FROM tblOrderSettlementDriver WHERE OrderID = @ID
	
		-- all Units and Rates are first normalized to the Order.OriginUOM then consistent processing
		-- and persisted in the Invoice (Settlement) record in this Order.OriginUOM 
		INSERT INTO tblOrderSettlementDriver 
			(OrderID
			, OrderDate
			, CarrierSettlementFactorID
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnitsID
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestinationWaitRateID 
			, DestinationWaitBillableMinutes 
			, DestinationWaitBillableHours
			, DestinationWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser)
		SELECT OrderID
			, OrderDate
			, CarrierSettlementFactorID
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnitsID
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestWaitRateID 
			, DestWaitBillableMinutes 
			, DestWaitBillableHours
			, DestWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser
		FROM #I

		/* compute the new Assessorial Rates for this order - into temp table #IA */
		/* NOTE: while it would be preferred for this to be out of the TX, it can't because it relies on the final Settlement changes being present */
		SELECT OrderID = @ID, AssessorialRateTypeID, AssessorialRateID = RateID, Amount = ROUND(Amount, 2), CreatedByUser = @UserName 
		INTO #IA
		FROM @SettlementUnits SU
		CROSS APPLY dbo.fnOrderDriverAssessorialAmounts(@ID, SU.SettlementUnits, SU.SettlementUomID) AA
		WHERE AA.AssessorialRateTypeID NOT IN (SELECT ID FROM @ManualAssessorialRates)

		/* apply the computed settlement assessorial rate/amount records to the DB */
		INSERT
		 INTO tblOrderSettlementDriverAssessorialCharge (OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser)
			SELECT OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser FROM #IA
			UNION
			SELECT @ID, ID, NULL, Amount, @UserName FROM @ManualAssessorialRates

		IF (@CreatedTran = 1)
			COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @msg varchar(max), @severity int
		SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY()
		ROLLBACK
		RAISERROR(@msg, @severity, 1)
		RETURN
	END CATCH	

	IF (@debugStatements = 1) PRINT 'spApplyRatesDriver (' + ltrim(@ID) + ') DONE ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	
END


GO


/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Shipper "Settlement" $$ values to an Delivered/Audited order
-- Changes:
-- 3.9.0	- 2015/08/13	- KDA	- add Auto-Approve logic
--										- honor Approve overrides in rating process
-- 3.9.9	- 2015/08/31	- KDA	- add new @AllowReApplyPostBatch parameter to allow re-applying after batch is applied
-- 3.9.19.2	- 2015/09/24	- KDA	- fix erroneous logic that computed Asssessorial Settlement results before the Order Level results were posted
-- 3.9.37	- 2015/12/22	- JAE	- Use tblOrderApproval to get override for Shipper Min Settlement Units
-- 3.9.39	- 2016/01/15	- JAE	- Also need to recalculate settlement units since the calculated/max values was already passed
-- 3.13.1	- 2016/07/04	- KDA	- add START/DONE PRINT statements (for debugging purposes)
-- 4.1.8.6	- 2016.09.21	- KDA	- move populate #IA table before transaction starts
--									- fix erroneous OR ActualMiles criteria (needed to be wrapped in () to avoid unnecessary processing)
--									- @debugStatements parameter + add additional START/DONE PRINT statements (for debugging purposes)
-- 4.1.9.1	- 2016.09.26	- KDA	- restore regressed 3.9.19.2 changes that accidentally got reverted with 4.1.8.6 (added a NOTE to hopefully prevent this again)
-- 4.4.1	- 2016/11/04	- JAE	- Add Destination Chainup
/**********************************/
ALTER PROCEDURE spApplyRatesShipper
(
  @ID int
, @UserName varchar(100)
, @OriginWaitAmount money = NULL
, @DestWaitAmount money = NULL
, @RejectionAmount money = NULL
, @FuelSurchargeAmount money = NULL
, @LoadAmount money = NULL
, @AssessorialRateTypeID_CSV varchar(max) = NULL
, @AssessorialAmount_CSV varchar(max) = null
, @ResetOverrides bit = 0
, @AllowReApplyPostBatch bit = 0
, @debugStatements bit = 0 -- turn on to get debugging statements during execution
) AS BEGIN

	SET NOCOUNT ON
	
	IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ') START ' + (CONVERT(VARCHAR(24), GETDATE(), 121))

	-- ensure this order hasn't yet been fully settled
	IF (@AllowReApplyPostBatch = 0) AND EXISTS (SELECT * FROM tblOrderSettlementShipper WHERE OrderID = @ID AND BatchID IS NOT NULL)
	BEGIN
		RAISERROR('Invoice has already been Shipper Settled', 16, 1)
		RETURN
	END

	IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ').UpdateActualMiles START ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	-- ensure the current Route.ActualMiles is assigned to the order
	UPDATE tblOrder SET ActualMiles = R.ActualMiles
	FROM tblOrder O
	JOIN tblRoute R ON R.ID = O.RouteID
	LEFT JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = O.ID
	WHERE O.ID = @ID
	  AND (O.ActualMiles IS NULL OR O.ActualMiles <> R.ActualMiles)
	  AND (@AllowReApplyPostBatch = 1 OR OSS.BatchID IS NULL)
	IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ').UpdateActualMiles DONE ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	
	/* transform the provided manual Assessorial Rates (if any) into a queryable table variable */
	DECLARE @ManualAssessorialRates TABLE (ID int primary key, Amount money)
	INSERT INTO @ManualAssessorialRates (ID, Amount) 
		SELECT ID.ID, R.Amount
		FROM (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), ID FROM dbo.fnSplitCSVIDs(@AssessorialRateTypeID_CSV)) ID
		JOIN (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), amount = CAST(value as money) FROM dbo.fnSplitCSV(@AssessorialAmount_CSV)) R ON R.sortid = ID.sortid

	/* persist all existing MANUAL Assessorial Rates from the existing Order Settlement (if any present and not resetting them) */
	INSERT INTO @ManualAssessorialRates (ID, Amount)
		SELECT AssessorialRateTypeID, Amount 
		FROM dbo.tblOrderSettlementShipperAssessorialCharge 
		WHERE OrderID = @ID AND AssessorialRateID IS NULL 
			AND ID NOT IN (SELECT ID FROM @ManualAssessorialRates)
			AND @ResetOverrides = 0

	/* persist all existing settlement amount overrides from the existing Order Settlement - if not resetting them */
	IF (@ResetOverrides = 0)
	BEGIN 
		SELECT @OriginWaitAmount = OriginWaitAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @OriginWaitAmount IS NULL AND OriginWaitRateID IS NULL
		SELECT @DestWaitAmount = DestinationWaitAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @DestWaitAmount IS NULL AND DestinationWaitRateID IS NULL
		SELECT @RejectionAmount = OrderRejectAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @RejectionAmount IS NULL AND OrderRejectRateID IS NULL
		SELECT @FuelSurchargeAmount = FuelSurchargeAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @FuelSurchargeAmount IS NULL AND FuelSurchargeRateID IS NULL
		SELECT @LoadAmount = LoadAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @LoadAmount IS NULL AND RouteRateID IS NULL AND RangeRateID IS NULL
	END

	/* compute the basic settlement units from the Order being invoices */
	DECLARE @SettlementUnits TABLE
	(
		OrderID int
	  , SettlementUomID int
	  , SettlementFactorID int
	  , ShipperSettlementFactorID int
	  , MinSettlementUnits decimal(18, 10)
	  , MinSettlementUnitsID int
	  , ActualUnits decimal(18, 10)
	  , SettlementUnits decimal(18, 10)
	)
	IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ').RetrieveSettlementUnits START ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	INSERT INTO @SettlementUnits (OrderID, SettlementUomID, SettlementFactorID, ShipperSettlementFactorID, MinSettlementUnits, MinSettlementUnitsID, ActualUnits, SettlementUnits)
		SELECT OSU.OrderID, OSU.SettlementUomID, OSU.SettlementFactorID, OSU.ShipperSettlementFactorID, ISNULL(OA.OverrideShipperMinSettlementUnits, OSU.MinSettlementUnits), OSU.MinSettlementUnitsID, OSU.ActualUnits, 
			-- use passed settlement (already calculated) or pick the max between the actual and overrride min - JAE 1/15/16 - 3.9.39
			CASE WHEN OA.OverrideShipperMinSettlementUnits IS NULL THEN OSU.SettlementUnits
						ELSE dbo.fnMaxDecimal(OSU.ActualUnits, OA.OverrideShipperMinSettlementUnits) END
		FROM dbo.viewOrderSettlementUnitsShipper OSU
		LEFT JOIN tblOrderApproval OA ON OA.OrderID = OSU.OrderID
		WHERE OSU.OrderID = @ID
	IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ').RetrieveSettlementUnits DONE ' + (CONVERT(VARCHAR(24), GETDATE(), 121))

	/* compute the order level settlement rateIDs & settlement amounts - into temp table #I */
	IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ').RetrieveData START ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	SELECT OrderID = @ID
		, OrderDate
		, ShipperSettlementFactorID, MinSettlementUnitsID
		, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
		, RouteRateID, RangeRateID, LoadAmount
		, WaitFeeParameterID
		, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount 
		, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount
		, OrderRejectRateID, OrderRejectAmount
		, FuelSurchargeRateID, FuelSurchargeRate, FuelSurchargeAmount
		, OriginTaxRate
		, TotalAmount = ISNULL(LoadAmount, 0) 
			+ ISNULL(OriginWaitAmount, 0) 
			+ ISNULL(DestWaitAmount, 0) 
			+ ISNULL(OrderRejectAmount, 0) 
			+ ISNULL(FuelSurchargeAmount, 0)
		, CreatedByUser = @UserName
		, OriginChainup, DestChainup, H2S, Rerouted /* used for the Auto-Approve logic below */
	INTO #I
	FROM (
		SELECT OrderDate
			, ShipperSettlementFactorID, MinSettlementUnitsID
			, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
			, RouteRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RouteRateID ELSE NULL END
			, RangeRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RangeRateID ELSE NULL END
			, LoadAmount = round(ISNULL(@LoadAmount, RR.Amount), 2)
			, WaitFeeParameterID
			, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount = ROUND(OriginWaitAmount, 2)
			, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount = ROUND(DestWaitAmount, 2)
			, OrderRejectRateID = CASE WHEN @RejectionAmount IS NULL THEN RD.RateID ELSE NULL END
			, OrderRejectAmount = round(isnull(@RejectionAmount, RD.Amount), 2)
			, FuelSurchargeRateID = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.RateID ELSE NULL END
			, FuelSurchargeRate = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.Rate ELSE NULL END
			, FuelSurchargeAmount = round(isnull(@FuelSurchargeAmount, FSR.Amount), 2)
			, OriginTaxRate
			, OriginChainup, DestChainup, H2S, Rerouted = cast(CASE WHEN RerouteCount = 0 THEN 0 ELSE 1 END as bit) /* used for the Auto-Approve logic below */
		FROM (
			SELECT O.ID
				, O.OriginID
				, O.CustomerID
				, O.RouteID
				, SU.ShipperSettlementFactorID
				, SU.SettlementFactorID
				, SU.SettlementUomID
				, SU.ActualUnits
				, SU.MinSettlementUnitsID
				, SU.MinSettlementUnits
				, SettlementUnits = CASE WHEN O.Rejected = 1 THEN 0 ELSE SU.SettlementUnits END
				, RouteMiles = isnull(OA.OverrideActualMiles, O.ActualMiles)
				, OrderDate = isnull(O.OrderDate, O.DueDate)
				, OriginChainUp = cast(CASE WHEN OA.OverrideOriginChainup = 1 THEN 0 ELSE O.OriginChainup END as bit)
				, DestChainUp = cast(CASE WHEN OA.OverrideDestChainup = 1 THEN 0 ELSE O.DestChainup END as bit)
				, H2S = cast(CASE WHEN OA.OverrideH2S = 1 THEN 0 ELSE O.H2S END as bit)
				, O.Rejected
				, RerouteCount = CASE WHEN OA.OverrideReroute = 1 THEN 0 ELSE O.RerouteCount END
				, IsSplit = CASE WHEN O.Rejected = 0 AND O.TicketCount > 1 THEN 1 ELSE 0 END
				, WaitFeeParameterID = WFP.ID
				, OriginWaitRateID = CASE WHEN @OriginWaitAmount IS NULL THEN OWD.RateID ELSE NULL END
				, OriginWaitBillableMinutes = OWD.BillableMinutes
				, OriginWaitBillableHours = OWD.BillableHours
				, OriginWaitAmount = isnull(@OriginWaitAmount, OWD.Amount)
				, DestWaitRateID = CASE WHEN @DestWaitAmount IS NULL THEN DWD.RateID ELSE NULL END
				, DestWaitBillableMinutes = DWD.BillableMinutes
				, DestWaitBillableHours = DWD.BillableHours
				, DestWaitAmount = isnull(@DestWaitAmount, DWD.Amount)
				, OriginTaxRate = OO.TaxRate
			FROM dbo.viewOrder O
			JOIN @SettlementUnits SU ON SU.OrderID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
			OUTER APPLY dbo.fnOrderShipperOriginWaitData(@ID) OWD 
			OUTER APPLY dbo.fnOrderShipperDestinationWaitData(@ID) DWD 
			OUTER APPLY dbo.fnOrderShipperWaitFeeParameter(@ID) WFP
			WHERE O.ID = @ID
		) X
		OUTER APPLY dbo.fnOrderShipperLoadAmount(@ID, SettlementUnits, SettlementUomID) RR 
		OUTER APPLY dbo.fnOrderShipperOrderRejectData(@ID, SettlementUnits, SettlementUomID) RD
		OUTER APPLY dbo.fnOrderShipperFuelSurchargeData(@ID) FSR
	) X2
	IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ').RetrieveData DONE ' + (CONVERT(VARCHAR(24), GETDATE(), 121))

	IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ').StoreData START ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	DECLARE @CreatedTran bit; SET @CreatedTran = CASE WHEN @@TRANCOUNT = 0 THEN 1 ELSE 0 END
	BEGIN TRY
		IF (@CreatedTran = 1)
			BEGIN TRAN

		-- remove the existing settlment record (if any)
		DELETE FROM tblOrderSettlementShipperAssessorialCharge WHERE OrderID = @ID
		DELETE FROM tblOrderSettlementShipper WHERE OrderID = @ID

		-- all Units and Rates are first normalized to the Order.OriginUOM then consistent processing
		-- and persisted in the Invoice (Settlement) record in this Order.OriginUOM 
		INSERT INTO tblOrderSettlementShipper (
/*1*/		OrderID
/*2*/		, OrderDate
/*3*/		, ShipperSettlementFactorID
/*4*/		, SettlementFactorID 
/*5*/		, SettlementUomID 
/*6*/		, MinSettlementUnitsID
/*7*/		, MinSettlementUnits 
/*8*/		, SettlementUnits
/*9*/		, RouteRateID
/*10*/		, RangeRateID 
/*11*/		, LoadAmount
/*12*/		, WaitFeeParameterID 
/*13*/		, OriginWaitRateID 
/*14*/		, OriginWaitBillableMinutes 
/*15*/		, OriginWaitBillableHours
/*16*/		, OriginWaitAmount 
/*17*/		, DestinationWaitRateID 
/*18*/		, DestinationWaitBillableMinutes 
/*19*/		, DestinationWaitBillableHours
/*20*/		, DestinationWaitAmount 
/*21*/		, OrderRejectRateID 
/*22*/		, OrderRejectAmount 
/*23*/		, FuelSurchargeRateID 
/*24*/		, FuelSurchargeRate 
/*25*/		, FuelSurchargeAmount 
/*26*/		, OriginTaxRate 
/*27*/		, TotalAmount 
/*28*/		, CreatedByUser)
		SELECT 
/*01*/		OrderID
/*02*/		, OrderDate
/*03*/		, ShipperSettlementFactorID
/*04*/		, SettlementFactorID 
/*05*/		, SettlementUomID 
/*06*/		, MinSettlementUnitsID
/*07*/		, MinSettlementUnits 
/*08*/		, SettlementUnits
/*09*/		, RouteRateID
/*10*/		, RangeRateID 
/*11*/		, LoadAmount
/*12*/		, WaitFeeParameterID 
/*13*/		, OriginWaitRateID 
/*14*/		, OriginWaitBillableMinutes 
/*15*/		, OriginWaitBillableHours
/*16*/		, OriginWaitAmount 
/*17*/		, DestWaitRateID 
/*18*/		, DestWaitBillableMinutes 
/*19*/		, DestWaitBillableHours
/*20*/		, DestWaitAmount 
/*21*/		, OrderRejectRateID 
/*22*/		, OrderRejectAmount 
/*23*/		, FuelSurchargeRateID 
/*24*/		, FuelSurchargeRate 
/*25*/		, FuelSurchargeAmount 
/*26*/		, OriginTaxRate 
/*27*/		, TotalAmount 
/*28*/		, CreatedByUser
		FROM #I

		IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ').RetrieveAssessorialData START ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
		/* compute the new Assessorial Rates for this order - into temp table #IA */
		/* NOTE: while it would be preferred for this to be out of the TX, it can't because it relies on the final Settlement changes being present */
		SELECT OrderID = @ID, AssessorialRateTypeID, AssessorialRateID = RateID, Amount = ROUND(Amount, 2), CreatedByUser = @UserName 
		INTO #IA
		FROM @SettlementUnits SU
		CROSS APPLY dbo.fnOrderShipperAssessorialAmounts(@ID, SU.SettlementUnits, SU.SettlementUomID) CAA
		WHERE CAA.AssessorialRateTypeID NOT IN (SELECT ID FROM @ManualAssessorialRates)
		IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ').RetrieveAssessorialData DONE ' + (CONVERT(VARCHAR(24), GETDATE(), 121))

		/* apply the computed settlement assessorial rate/amount records to the DB */
		INSERT INTO tblOrderSettlementShipperAssessorialCharge (OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser)
			SELECT OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser FROM #IA
			UNION
			SELECT @ID, ID, NULL, Amount, @UserName FROM @ManualAssessorialRates

		IF (@CreatedTran = 1)
			COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @msg varchar(max), @severity int
		SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY()
		ROLLBACK
		RAISERROR(@msg, @severity, 1)
		RETURN
	END CATCH	
	IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ').StoreData DONE ' + (CONVERT(VARCHAR(24), GETDATE(), 121))

	IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ') DONE ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	
END


GO


exec sp_refreshview viewOrder_Financial_Shipper
GO

/*************************************************
-- Date Created: 2015/08/13
-- Author: Kevin Alons
-- Purpose: return AutoApprove message for a single order 
-- Returns: NULL is valid, non-null is Invalid reason(s)
-- Changes:
	- 3.9.22 - 2015/11/05 - KDA+JAE - pass new @Transferred parameter to fnIsValidAutoApprove() function
	- 3.11.17.2 - 2015/04/18 - JAE - Added producer settlement to check
	- 4.4.1 - 2016/11/07 - JAE - Fix swap between passed parameters H2S and (Origin) Chainup
								- Add new destination chainup column
*************************************************/
ALTER FUNCTION fnOrderIsValidAutoApprove
(
  @ID int
) RETURNS varchar(1000) AS
BEGIN
	DECLARE @ret varchar(1000)

	SELECT @ret = dbo.fnIsValidAutoApprove(ISH.StatusID, ISH.BatchID, OSC.BatchID, OSP.BatchID
		, ISH.InvoiceTotalWaitBillableMinutes, ISNULL(OSC.OriginWaitBillableMinutes, 0) + ISNULL(OSC.DestinationWaitBillableMinutes, 0)
		, ISH.InvoiceWaitFeeParameterID
		, ISH.H2S, ISH.OriginChainUp, ISH.DestChainUp, ISH.RerouteCount, ISNULL(OTR.TransferComplete, 0)
		, OA.Approved)
	FROM viewOrder_Financial_Shipper ISH
	LEFT JOIN tblOrderTransfer OTR ON OTR.OrderID = ISH.ID
	LEFT JOIN tblOrderSettlementCarrier OSC ON OSC.OrderID = ISH.ID
	LEFT JOIN tblOrderSettlementProducer OSP ON OSP.OrderID = ISH.ID
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = ISH.ID AND OA.Approved = 1
	WHERE ISH.ID = @ID

	RETURN @ret
END


GO



-- report center fields

UPDATE tblReportColumnDefinition 
SET DataField = 'OriginChainUp', 
	Caption = 'GENERAL | Origin Chain-Up'
WHERE ID = 35

UPDATE tblReportColumnDefinition 
SET DataField = 'ShipperOriginChainupAmount', 
	Caption = 'SETTLEMENT | SHIPPER | Shipper Origin Chain-Up $$'
WHERE ID = 148

UPDATE tblReportColumnDefinition 
SET DataField = 'CarrierOriginChainupAmount', 
	Caption = 'SETTLEMENT | CARRIER | Carrier Origin Chain-Up $$'  
WHERE ID = 166

UPDATE tblReportColumnDefinition 
SET DataField = 'ShipperOriginChainupRateType', 
	Caption = 'SETTLEMENT | SHIPPER | Shipper Origin Chain-Up Rate Type'  
WHERE ID = 223

UPDATE tblReportColumnDefinition 
SET DataField = 'CarrierOriginChainupRateType', 
	Caption = 'SETTLEMENT | CARRIER | Carrier Origin Chain-Up Rate Type'  
WHERE ID = 227

UPDATE tblReportColumnDefinition 
SET DataField = 'ShipperOriginChainupRate', 
	Caption = 'SETTLEMENT | SHIPPER | Shipper Origin Chain-Up Rate'  
WHERE ID = 231

UPDATE tblReportColumnDefinition 
SET DataField = 'CarrierOriginChainupRate', 
	Caption = 'SETTLEMENT | CARRIER | Carrier Origin Chain-Up Rate'  
WHERE ID = 232

UPDATE tblReportColumnDefinition 
SET DataField = 'DriverOriginChainupAmount', 
	Caption = 'SETTLEMENT | DRIVER | Driver Origin Chain-Up Fee'  
WHERE ID = 308

UPDATE tblReportColumnDefinition 
SET DataField = 'DriverOriginChainupRateType', 
	Caption = 'SETTLEMENT | DRIVER | Driver Origin Chain-Up Rate Type'  
WHERE ID = 330

UPDATE tblReportColumnDefinition 
SET DataField = 'DriverOriginChainupRate', 
	Caption = 'SETTLEMENT | DRIVER | Driver Origin Chain-Up Rate'  
WHERE ID = 334

UPDATE tblReportColumnDefinition 
SET DataField = 'SELECT CASE WHEN OA.Approved=1 THEN cast(CASE WHEN isnull(OA.OverrideOriginChainup, 0) = 1 THEN 0 ELSE RS.OriginChainup END as bit) ELSE NULL END FROM tblOrderApproval OA WHERE OA.OrderID = RS.ID', 
	Caption = 'GENERAL | Origin Chain-Up Final'  
WHERE ID = 90015



SET IDENTITY_INSERT tblReportColumnDefinition ON
INSERT INTO tblReportColumnDefinition 
(ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport, IsTicketField)
VALUES
(342,	1,	
	'DestChainUp',
	'GENERAL | Destination Chain-Up', NULL,	NULL,	5,	'SELECT ID=1, Name=''Yes'' UNION SELECT 0, ''No''',	0,	'*',	0,	0),

(343,	1,	
	'CarrierDestChainupAmount',
	'SETTLEMENT | CARRIER | Carrier Destination Chain-Up $$', '#0.00',	NULL,	4,	NULL,	1,	'viewReportCenterCarrierFinancials',	1,	0),
(344,	1,	
	'CarrierDestChainupRateType',
	'SETTLEMENT | CARRIER | Carrier Destination Chain-Up Rate Type',	NULL,	NULL,	1,	NULL,	1,	'*',	1,	0),
(345,	1,	
	'CarrierDestChainupRate', 
	'SETTLEMENT | CARRIER | Carrier Destination Chain-Up Rate', '#0.00',	NULL,	4,	NULL,	1,	'viewReportCenterCarrierFinancials',	1,	0),

(346,	1,	
	'DriverDestChainupAmount', 
	'SETTLEMENT | DRIVER | Driver Destination Chain-Up Fee', '#0.00',	NULL,	4,	NULL,	1,	'viewReportCenterDriverFinancials',	1,	0),
(347,	1,	
	'DriverDestChainupRateType',
	'SETTLEMENT | DRIVER | Driver Destination Chain-Up Rate Type',	NULL,	NULL,	1,	NULL,	1,	'*',	1,	0),
(348,	1,	
	'DriverDestChainupRate',
	'SETTLEMENT | DRIVER | Driver Destination Chain-Up Rate',	'#0.00',	NULL,	4,	NULL,	1,	'viewReportCenterDriverFinancials',	1,	0),

(349,	1,	
	'ShipperDestChainupAmount', 
	'SETTLEMENT | SHIPPER | Shipper Destination Chain-Up $$', '#0.00',	NULL,	4,	NULL,	1,	'viewReportCenterShipperFinancials',	1,	0),
(350,	1,	
	'ShipperDestChainupRateType',
	'SETTLEMENT | SHIPPER | Shipper Destination Chain-Up Rate Type', NULL,	NULL,	1,	NULL,	1,	'*',	1,	0),
(351,	1,	
	'ShipperDestChainupRate',
	'SETTLEMENT | SHIPPER | Shipper Destination Chain-Up Rate',	'#0.00',	NULL,	4,	NULL,	1,	'viewReportCenterShipperFinancials',	1,	0),

(90071,	1,	
	'SELECT CASE WHEN OA.Approved=1 THEN cast(CASE WHEN isnull(OA.OverrideDestChainup, 0) = 1 THEN 0 ELSE RS.DestChainup END as bit) ELSE NULL END FROM tblOrderApproval OA WHERE OA.OrderID = RS.ID',
	'GENERAL | Destination Chain-Up Final',	NULL,	NULL,	5,	'SELECT ID=1, Name=''Yes'' UNION SELECT 0, ''No''',	0,	'*',	0,	0)

SET IDENTITY_INSERT tblReportColumnDefinition OFF

GO


-- bulk refresh to cover bases
exec _spRefreshAllViews
exec _spRefreshAllViews
exec _spRefreshAllViews
exec _spRefreshAllViews
exec _spRefreshAllViews
GO

exec _spRecompileAllStoredProcedures
exec _spRecompileAllStoredProcedures
GO

exec _spRebuildAllObjects
exec _spRebuildAllObjects
GO



COMMIT
SET NOEXEC OFF
