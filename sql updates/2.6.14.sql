/* 
	-- fix major bugs in Carrier|Customer Route Rate management	
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.6.13'
SELECT  @NewVersion = '2.6.14'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 19 Dec 2012
-- Description:	trigger to ensure the UomID for Carrier/Customer Route Rates matches that assigned to the Origin
-- =============================================
ALTER TRIGGER [dbo].[trigOrigin_IU] ON [dbo].[tblOrigin] AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	IF (UPDATE(UomID))
	BEGIN
		-- update matching CarrierRouteRates.UomID to match what is assigned to the new Origin
		UPDATE tblCarrierRouteRates 
		  SET UomID = i.UomID, LastChangeDateUTC = i.LastChangeDateUTC, LastChangedByUser = i.LastChangedByUser
		FROM tblCarrierRouteRates CRR
		JOIN tblRoute R ON R.ID = CRR.RouteID
		JOIN inserted i ON i.ID = R.OriginID
		JOIN deleted d ON d.ID = i.ID
		WHERE d.UomID <> i.UomID AND CRR.EffectiveDate <= dbo.fnDateOnly(GETDATE())

		-- update matching CustomerRouteRates.UomID to match what is assigned to the new Origin
		UPDATE tblCustomerRouteRates 
		  SET UomID = i.UomID, LastChangeDateUTC = i.LastChangeDateUTC, LastChangedByUser = i.LastChangedByUser
		FROM tblCustomerRouteRates CRR
		JOIN tblRoute R ON R.ID = CRR.RouteID
		JOIN inserted i ON i.ID = R.OriginID
		JOIN deleted d ON d.ID = i.ID
		WHERE d.UomID <> i.UomID AND CRR.EffectiveDate <= dbo.fnDateOnly(GETDATE())
	END
END

GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 19 Dec 2012
-- Description:	trigger to ensure the UomID for Carrier/Customer Route Rates matches that assigned to the Origin
-- =============================================
ALTER TRIGGER [dbo].[trigCarrierRouteRates_IU] ON [dbo].[tblCarrierRouteRates] AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	IF (UPDATE(UomID))
	BEGIN
		-- this conversion is inverted (since it is a rate, not the actual units)
		UPDATE tblCarrierRouteRates
		  SET Rate = dbo.fnConvertRateUOM(CRR.Rate, d.UomID, CRR.UomID)
		FROM tblCarrierRouteRates CRR
		JOIN deleted d ON d.ID = CRR.ID
		WHERE d.UomID <> CRR.UomID
		  AND d.Rate = CRR.Rate
	END
END

GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 6 Dec 2013
-- Description:	trigger to ensure the UomID for Customer/Customer Route Rates matches that assigned to the Origin
-- =============================================
ALTER TRIGGER [dbo].[trigCustomerRouteRates_IU] ON [dbo].[tblCustomerRouteRates] AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	IF (UPDATE(UomID))
	BEGIN
		-- this conversion is inverted (since it is a rate, not the actual units)
		UPDATE tblCustomerRouteRates
		  SET Rate = dbo.fnConvertRateUOM(CRR.Rate, d.UomID, CRR.UomID)
		FROM tblCustomerRouteRates CRR
		JOIN deleted d ON d.ID = CRR.ID
		WHERE d.UomID <> CRR.UomID
		  AND d.Rate = CRR.Rate
	END
END

GO

COMMIT
SET NOEXEC OFF