-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.9.22.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.9.22.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0

SET NOEXEC OFF
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.9.22'
SELECT  @NewVersion = '3.9.23'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Order Date fix to ensure the correct order rule is used'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/********************************************************
	Author: Kevin Alons
	Create Date: 2015/08/31 - 3.9.5
	Purpose: compute and return the current OrderDate for the specified Order
	Changes:
		3.9.23 - 2015/11/05 - KDA+JAE - revise OOR to only return TypeID = 8 OrderRule records
********************************************************/
ALTER FUNCTION fnOrderDate(@id int) RETURNS date AS
BEGIN
	DECLARE @ret date 
	SELECT @ret = cast(CASE OOR.InternalData WHEN 'OriginArriveTime' THEN OLD.OriginArriveTime
											WHEN 'OriginDepartTime' THEN OLD.OriginDepartTime
											WHEN 'DestArriveTime' THEN OLD.DestArriveTime
											WHEN 'DestDepartTime' THEN OLD.DestDepartTime
				END as date)
	FROM viewOrderLocalDates OLD 
	CROSS APPLY (SELECT * FROM dbo.fnOrderOrderRules(OLD.ID) WHERE TypeID = 8) OOR
	WHERE OLD.ID = @id

	RETURN @ret
END
GO

EXEC _spRebuildAllObjects
GO

EXEC _spRefreshAllViews
GO


COMMIT
SET NOEXEC OFF