SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.1.8.4'
SELECT  @NewVersion = '4.1.8.5'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1787: Driver Settlement - exception with CarrierSettlementFactor (was referencing wrong field)'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

ALTER TABLE [dbo].[tblOrderSettlementDriver] DROP CONSTRAINT [FK_OrderSettlementDriver_SettlementFactorID]
GO

ALTER TABLE tblOrderSettlementDriver WITH CHECK ADD CONSTRAINT FK_OrderSettlementDriver_CarrierSettlementFactor FOREIGN KEY(CarrierSettlementFactorID)
REFERENCES tblCarrierSettlementFactor (ID)
GO

COMMIT
SET NOEXEC OFF