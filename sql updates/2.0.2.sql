/* add viewPrintTemplate
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.0.1', @NewVersion = '2.0.2'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

/***********************************/
-- Date Created: 4 Apr 2013
-- Author: Kevin Alons
-- Purpose: return the PrintTemplate with translated "friendly" names
/***********************************/
CREATE VIEW viewPrintTemplate AS
	SELECT PT.*
		, TT.Name AS TicketType
	FROM tblPrintTemplate PT
	JOIN tblTicketType TT ON TT.ID = PT.TicketTypeID
GO
GRANT SELECT ON viewPrintTemplate TO dispatchcrude_iis_acct
GO

COMMIT
SET NOEXEC OFF