/* add tblOrigin.Station varchar(10) null
   add tbl.OrderStatus.NextID column
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '1.7.0', @NewVersion = '1.7.1'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

ALTER TABLE tblOrigin ADD Station varchar(10) NULL
GO

ALTER TABLE tblOrderStatus ADD NextID int NULL
GO

UPDATE tblOrderStatus SET NextID = 1 WHERE ID = -10
UPDATE tblOrderStatus SET NextID = 2 WHERE ID = 1
UPDATE tblOrderStatus SET NextID = 7 WHERE ID = 2
UPDATE tblOrderStatus SET NextID = 8 WHERE ID = 7
UPDATE tblOrderStatus SET NextID = 3 WHERE ID = 8
UPDATE tblOrderStatus SET NextID = 4 WHERE ID = 3
GO

EXEC _spRefreshAllViews
GO

COMMIT
SET NOEXEC OFF