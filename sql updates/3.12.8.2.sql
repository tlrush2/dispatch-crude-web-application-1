SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.12.8.1'
SELECT  @NewVersion = '3.12.8.2'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Report Center: expose any ORDER & TICKET custom data fields as Report Center CUSTOM export fields'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

EXEC _spDropFunction 'fnObjectCustomData'
GO
/*********************************************
 Creation Info: 2.12.8.2 - 2016/06/25
 Author: Kevin Alons
 Purpose: find and return the custom data value (or default if not available) for the specified parameters
 Changes:
 *********************************************/
CREATE FUNCTION fnObjectCustomData(@ObjectFieldID int, @RecordID int, @DefaultValue varchar(max)) RETURNS varchar(max) AS
BEGIN
	DECLARE @ret varchar(max)
	SET @ret = isnull((SELECT Value FROM tblObjectCustomData WHERE RecordID = @RecordID), @DefaultValue)
	RETURN (@ret)
END
GO
GRANT EXECUTE ON fnObjectCustomData TO role_iis_acct
GO

IF (OBJECT_ID('tblObjectCustomDataReportColumn') IS NOT NULL)
	DROP TABLE tblObjectCustomDataReportColumn
GO
CREATE TABLE tblObjectCustomDataReportColumn
(
  ObjectFieldID int NOT NULL
, ReportColumnDefinitionID int NOT NULL
, CONSTRAINT PK_ObjectCustomDataReportColumn PRIMARY KEY (ObjectFieldID, ReportColumnDefinitionID)
)
GO

EXEC _spDropTrigger 'trigObjectField_IUD'
GO
/*********************************************
 Creation Info: 2.12.8.2 - 2016/06/25
 Author: Kevin Alons
 Purpose: ensure any OBJECT and TICKET Custom Data Fields are exposed as ReportCenter fields when created/modified and removed when deleted
 Changes:
 *********************************************/
CREATE TRIGGER trigObjectField_IUD ON tblObjectField FOR INSERT, UPDATE, DELETE AS
BEGIN
	DECLARE @work TABLE (ID int, ObjectID int, ObjectFieldTypeID int, Name varchar(255), Object varchar(255), Action varchar(25), RCID int)
	INSERT INTO @work
		SELECT i.ID, IO.ObjectID, IO.ObjectFieldTypeID, IO.Name, IO.Object, CASE WHEN d.ID IS NULL OR CD.ObjectFieldID IS NULL THEN 'INSERT' ELSE 'UPDATE' END, CD.ReportColumnDefinitionID
		FROM inserted i
		JOIN viewObjectField IO ON IO.ID = i.ID
		LEFT JOIN deleted d ON d.ID = i.ID
		LEFT JOIN tblObjectCustomDataReportColumn CD ON CD.ObjectFieldID = i.ID
		WHERE IO.ObjectID IN (1,2)
		  AND IO.IsCustom = 1
			
		UNION
		SELECT d.ID, NULL, NULL, NULL, NULL, 'DELETE', CD.ReportColumnDefinitionID
		FROM deleted d
		JOIN tblObjectCustomDataReportColumn CD ON CD.ObjectFieldID = d.id
		LEFT JOIN inserted i ON i.ID = d.ID
		WHERE d.ObjectID IN (1,2)
		  AND i.ID IS NULL
		  
	DECLARE @id int, @objectID int, @action varchar(25), @RCID int, @Name varchar(255), @OFTID int, @DataField varchar(max), @Caption varchar(255), @FilterTypeID int
	WHILE EXISTS (SELECT * FROM @work)
	BEGIN
		SELECT TOP 1 @id = ID, @objectID = ObjectID, @action = Action, @RCID = RCID, @Name = Name, @OFTID = ObjectFieldTypeID 
			, @DataField = 'dbo.fnObjectCustomData(' + ltrim(@ID) + ', RS.' + CASE WHEN @ObjectID = 1 THEN 'ID' ELSE 'T_ID' END + ', '''')'
			, @Caption = CASE WHEN @ObjectID = 2 THEN 'TICKET | ' ELSE '' END + 'CUSTOM | ' + Name
			, @FilterTypeID = CASE @OFTID WHEN 1 THEN 1 WHEN 2 THEN 5 WHEN 3 THEN 4 WHEN 4 THEN 4 WHEN 5 THEN 3 WHEN 6 THEN 3 WHEN 7 THEN 0 WHEN 8 THEN 1 END
		FROM @work
		IF (@action = 'INSERT') 
		BEGIN
			INSERT INTO tblReportColumnDefinition (ReportID, DataField, Caption, FilterDataField, FilterTypeID, FilterAllowCustomText, AllowedRoles, OrderSingleExport)
				VALUES (
					1
					, @DataField
					, @Caption
					, @DataField
					, @FilterTypeID
					, 1
					, '*'
					, CASE WHEN @ObjectID = 1 THEN 1 ELSE 0 END
				)
			INSERT INTO tblObjectCustomDataReportColumn (ObjectFieldID, ReportColumnDefinitionID) VALUES (@id, SCOPE_IDENTITY())
		END
		ELSE IF (@action = 'UPDATE')
			UPDATE tblReportColumnDefinition
				SET ReportID = 1
					, DataField = @DataField
					, Caption = @Caption
					, FilterDataField = @DataField
					, FilterTypeID = @FilterTypeID
					, FilterAllowCustomText = 1
					, AllowedRoles = '*'
					, OrderSingleExport = CASE WHEN @ObjectID = 1 THEN 1 ELSE 0 END
			WHERE ID = @RCID
		ELSE IF (@action = 'DELETE')
		BEGIN
			DELETE FROM tblObjectCustomDataReportColumn WHERE ObjectFieldID = @ID
			DELETE FROM tblUserReportColumnDefinition WHERE ReportColumnID = @RCID
			DELETE FROM tblReportColumnDefinition WHERE ID = @RCID
		END
		DELETE FROM @work WHERE id = @id
	END
END
GO

ALTER TABLE dbo.tblObjectCustomData DROP CONSTRAINT FK_ObjectCustomData_ObjectField
GO
ALTER TABLE dbo.tblObjectCustomData ADD CONSTRAINT FK_ObjectCustomData_ObjectField FOREIGN KEY (ObjectFieldID) REFERENCES dbo.tblObjectField(ID) ON UPDATE NO ACTION ON DELETE CASCADE 
GO

UPDATE tblObjectField SET IsCustom = IsCustom WHERE IsCustom = 1
GO

COMMIT
SET NOEXEC OFF

/*
select top 100 orderdate from viewReportCenter_Orders order by orderdate desc
select * from tblobjectfield where iscustom = 1

select top 1 * from tblObjectCustomData
insert into tblobjectcustomdata (objectfieldid, recordid, value, CreateDateUTC, createdbyuser)
	select 90018, 34722, '34722', getutcdate(), 'System'
	union select 90018, 34723, '34723', getutcdate(), 'System'
	union select 90018, 34724, '34724', getutcdate(), 'System'
	union select 90018, 34737, '34737', getutcdate(), 'System'

select * from tblobjectfield where id between 410 and 430
select * From tblobjectfield where objectid in (1,2) and IsCustom = 1
select * from tbldestination where Name like 'Northern Point Terminal'
select top 100 * from viewOrder_ObjectBase
select top 10 * from vieworigin
select * from viewObjectField where iscustom = 1
select * from tblReportColumnDefinition
select * from tblObjectCustomData
	select * from tblobjectfieldtype
	select * from tblReportFilterType
	select * from tblObjectCustomDataReportColumn
	delete from tblObjectCustomDataReportColumn
	select * from tblReportColumnDefinition order by id desc
	
go

update tblobjectfield set allowedroles = allowedroles where objectid in (1,2)
insert into tblobjectfield (objectid, FieldName, name, objectfieldtypeid, allownullid, iskey, iscustom)
	values (1, 'TEST', 'TEST', 1, 1, 0, 1)
insert into tblobjectfield (objectid, FieldName, name, objectfieldtypeid, allownullid, iskey, iscustom)
	values (2, 'TEST', 'TEST', 1, 1, 0, 1)
update tblobjectfield set iscustom = iscustom where iscustom = 1
delete from tblobjectfield where name = 'TEST'
*/