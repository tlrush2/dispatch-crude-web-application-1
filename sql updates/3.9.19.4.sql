-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.9.19.3.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.9.19.3.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0

SET NOEXEC OFF
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.9.19.3'
SELECT  @NewVersion = '3.9.19.4'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'tblOrderTicket - make all LastChangeDate + CreateDate + DeleteDate UTC fields be datetime (vs smalldatetime)'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

ALTER TABLE dbo.tblOrderTicket
	DROP CONSTRAINT FK_OrderTicket_Order
GO
ALTER TABLE dbo.tblOrder SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderTicket
	DROP CONSTRAINT FK_OrderTicket_RejectReason
GO
ALTER TABLE dbo.tblOrderTicketRejectReason SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderTicket
	DROP CONSTRAINT FK_OrderTicket_OriginTank
GO
ALTER TABLE dbo.tblOriginTank SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderTicket
	DROP CONSTRAINT FK_OrderTicket_TicketType
GO
ALTER TABLE dbo.tblTicketType SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderTicket
	DROP CONSTRAINT DF_OrderTicket_TicketType
GO
ALTER TABLE dbo.tblOrderTicket
	DROP CONSTRAINT DF_OrderTicket_Rejected
GO
ALTER TABLE dbo.tblOrderTicket
	DROP CONSTRAINT DF_OrderTicket_CreateDateUTC
GO
ALTER TABLE dbo.tblOrderTicket
	DROP CONSTRAINT DF_tblOrderTicket_UID
GO
ALTER TABLE dbo.tblOrderTicket
	DROP CONSTRAINT DF_OrderTicket_FromMobileApp
GO
CREATE TABLE dbo.Tmp_tblOrderTicket
	(
	ID int NOT NULL IDENTITY (1, 1),
	OrderID int NOT NULL,
	CarrierTicketNum varchar(15) NOT NULL,
	TicketTypeID int NOT NULL,
	TankNum varchar(20) NULL,
	ProductObsGravity decimal(9, 3) NULL,
	ProductObsTemp decimal(9, 3) NULL,
	ProductBSW decimal(9, 3) NULL,
	OpeningGaugeFeet tinyint NULL,
	OpeningGaugeInch tinyint NULL,
	OpeningGaugeQ tinyint NULL,
	ClosingGaugeFeet tinyint NULL,
	ClosingGaugeInch tinyint NULL,
	ClosingGaugeQ tinyint NULL,
	GrossUnits decimal(9, 3) NULL,
	NetUnits decimal(9, 3) NULL,
	Rejected bit NOT NULL,
	RejectNotes varchar(255) NULL,
	SealOff varchar(10) NULL,
	SealOn varchar(10) NULL,
	BOLNum varchar(15) NULL,
	ProductHighTemp decimal(9, 3) NULL,
	ProductLowTemp decimal(9, 3) NULL,
	CreateDateUTC datetime NULL,
	CreatedByUser varchar(100) NULL,
	LastChangeDateUTC datetime NULL,
	LastChangedByUser varchar(100) NULL,
	DeleteDateUTC datetime NULL,
	DeletedByUser varchar(100) NULL,
	UID uniqueidentifier NULL,
	FromMobileApp bit NOT NULL,
	OriginTankID int NULL,
	GrossStdUnits decimal(18, 6) NULL,
	BottomFeet tinyint NULL,
	BottomInches tinyint NULL,
	BottomQ tinyint NULL,
	RejectReasonID int NULL,
	MeterFactor varchar(15) NULL,
	OpenMeterUnits decimal(18, 3) NULL,
	CloseMeterUnits decimal(18, 3) NULL,
	DispatchConfirmNum varchar(25) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_tblOrderTicket SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_tblOrderTicket ADD CONSTRAINT
	DF_OrderTicket_TicketType DEFAULT ((1)) FOR TicketTypeID
GO
ALTER TABLE dbo.Tmp_tblOrderTicket ADD CONSTRAINT
	DF_OrderTicket_Rejected DEFAULT ((0)) FOR Rejected
GO
ALTER TABLE dbo.Tmp_tblOrderTicket ADD CONSTRAINT
	DF_OrderTicket_CreateDateUTC DEFAULT (getutcdate()) FOR CreateDateUTC
GO
ALTER TABLE dbo.Tmp_tblOrderTicket ADD CONSTRAINT
	DF_tblOrderTicket_UID DEFAULT (newid()) FOR UID
GO
ALTER TABLE dbo.Tmp_tblOrderTicket ADD CONSTRAINT
	DF_OrderTicket_FromMobileApp DEFAULT ((0)) FOR FromMobileApp
GO
SET IDENTITY_INSERT dbo.Tmp_tblOrderTicket ON
GO
IF EXISTS(SELECT * FROM dbo.tblOrderTicket)
	 EXEC('INSERT INTO dbo.Tmp_tblOrderTicket (ID, OrderID, CarrierTicketNum, TicketTypeID, TankNum, ProductObsGravity, ProductObsTemp, ProductBSW, OpeningGaugeFeet, OpeningGaugeInch, OpeningGaugeQ, ClosingGaugeFeet, ClosingGaugeInch, ClosingGaugeQ, GrossUnits, NetUnits, Rejected, RejectNotes, SealOff, SealOn, BOLNum, ProductHighTemp, ProductLowTemp, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser, UID, FromMobileApp, OriginTankID, GrossStdUnits, BottomFeet, BottomInches, BottomQ, RejectReasonID, MeterFactor, OpenMeterUnits, CloseMeterUnits, DispatchConfirmNum)
		SELECT ID, OrderID, CarrierTicketNum, TicketTypeID, TankNum, ProductObsGravity, ProductObsTemp, ProductBSW, OpeningGaugeFeet, OpeningGaugeInch, OpeningGaugeQ, ClosingGaugeFeet, ClosingGaugeInch, ClosingGaugeQ, GrossUnits, NetUnits, Rejected, RejectNotes, SealOff, SealOn, BOLNum, ProductHighTemp, ProductLowTemp, CONVERT(datetime, CreateDateUTC), CreatedByUser, CONVERT(datetime, LastChangeDateUTC), LastChangedByUser, CONVERT(datetime, DeleteDateUTC), DeletedByUser, UID, FromMobileApp, OriginTankID, GrossStdUnits, BottomFeet, BottomInches, BottomQ, RejectReasonID, MeterFactor, OpenMeterUnits, CloseMeterUnits, DispatchConfirmNum FROM dbo.tblOrderTicket WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_tblOrderTicket OFF
GO
DROP TABLE dbo.tblOrderTicket
GO
EXECUTE sp_rename N'dbo.Tmp_tblOrderTicket', N'tblOrderTicket', 'OBJECT' 
GO
ALTER TABLE dbo.tblOrderTicket ADD CONSTRAINT
	PK_OrderTicket PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE NONCLUSTERED INDEX idxOrderTicket_OrderID_TicketNum ON dbo.tblOrderTicket
	(
	OrderID,
	CarrierTicketNum
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxOrderTicket_DeleteDate ON dbo.tblOrderTicket
	(
	DeleteDateUTC
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX idxOrderTicket_UID ON dbo.tblOrderTicket
	(
	UID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxOrderTicket_TicketType_DeleteDateUTC ON dbo.tblOrderTicket
	(
	TicketTypeID,
	DeleteDateUTC
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.tblOrderTicket ADD CONSTRAINT
	FK_OrderTicket_TicketType FOREIGN KEY
	(
	TicketTypeID
	) REFERENCES dbo.tblTicketType
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderTicket ADD CONSTRAINT
	FK_OrderTicket_OriginTank FOREIGN KEY
	(
	OriginTankID
	) REFERENCES dbo.tblOriginTank
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderTicket ADD CONSTRAINT
	FK_OrderTicket_RejectReason FOREIGN KEY
	(
	RejectReasonID
	) REFERENCES dbo.tblOrderTicketRejectReason
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderTicket ADD CONSTRAINT
	FK_OrderTicket_Order FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.tblOrder
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
-- =============================================
-- Author:		Kevin Alons
-- Create date: 3 Oct 2013
-- Description:	trigger to ensure the GrossUnits and NetUnits are computed for valid, entered data
-- =============================================
CREATE TRIGGER [dbo].[trigOrderTicket_D] ON dbo.tblOrderTicket AFTER DELETE AS
BEGIN
	SET NOCOUNT ON;

	-- ensure the Order record is in-sync with the Tickets
	UPDATE tblOrder 
	SET OriginGrossUnits = (SELECT sum(GrossUnits) FROM tblOrderTicket WHERE OrderID = O.ID AND Rejected = 0 AND DeleteDateUTC IS NULL)
	  , OriginNetUnits = (SELECT sum(NetUnits) FROM tblOrderTicket WHERE OrderID = O.ID AND Rejected = 0 AND DeleteDateUTC IS NULL)
		-- use the first MeterRun/BasicRun CarrierTicketNum num as the Order BOL Num
	  , OriginBOLNum = (SELECT min(BOLNum) FROM tblOrderTicket WHERE OrderID = O.ID AND TicketTypeID NOT IN (1,2) AND DeleteDateUTC IS NULL)
	  , CarrierTicketNum = (SELECT min(CarrierTicketNum) FROM tblOrderTicket WHERE ID IN 
		(SELECT min(ID) FROM tblOrderTicket WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL))
	  , LastChangeDateUTC = (SELECT MAX(isnull(LastChangeDateUTC, CreateDateUTC)) FROM deleted WHERE OrderID = O.ID)
	  , LastChangedByUser = (SELECT MIN(isnull(LastChangedByUser, CreatedByUser)) FROM deleted WHERE OrderID = O.ID)
	FROM tblOrder O
	WHERE ID IN (SELECT DISTINCT OrderID FROM deleted)
END
GO
/*
	BB  8/20/15 - DCWEB-844: Added Gauge Net tickets to trigger on line 327.  Updates OriginTankID field.
*/

/*****************************************
-- Date Created: ??
-- Author: Kevin Alons, Modified by Geoff Mochau
-- Purpose: specialized, db-level logic enforcing business rules, audit changes, etc
-- Changes:
    - 3.8.12 - 2015/08/04 - GSM - use the revised (3-temperature) API function
	- 3.8.10 - 2015/07/26 - KDA - always update the associated tblOrder.LastChangeDateUTC to NOW when a Ticket is changed)
*****************************************/
CREATE TRIGGER [dbo].[trigOrderTicket_IU] ON dbo.tblOrderTicket AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	-- Note: the TRIGGER_NESTLEVEL( OBJECT_ID('trigOrderTicket_IU') ) = 1 statement prevents recursion
	IF (TRIGGER_NESTLEVEL( OBJECT_ID('trigOrderTicket_IU')) = 1) BEGIN
		
		-- only do anything if something actually changed
		IF EXISTS (SELECT * FROM inserted EXCEPT SELECT * FROM deleted)
		BEGIN
			PRINT 'trigOrderTicket_IU FIRED'
			
			/**********  START OF VALIDATION SECTION ************************/
			DECLARE @errorString varchar(1000); SET @errorString = ''

			IF EXISTS (
				SELECT * 
				FROM tblOrder 
				WHERE ID IN (SELECT OrderID FROM inserted) 
				  -- prevent changes to any tickets belonging to a Delivered or Audited Order
				  AND StatusID IN (4))
			BEGIN
				IF (dbo.fnToBool(dbo.fnSettingValue(35)) = 1)
					PRINT 'Ticket for an AUDITED orders is being modified - please investigate why!'
				ELSE 				
					SET @errorString = @errorString + '|Tickets of Audited Orders cannot be modified'
			END

			-- prevent reassigning a ticket from one order to another
			IF EXISTS (
				SELECT i.* 
				FROM inserted i
				JOIN deleted d ON i.UID = d.UID
				WHERE i.OrderID <> d.OrderID)
			BEGIN
				SET @errorString = @errorString + '|Tickets cannot be moved to a different Order'
			END
			
			IF EXISTS (
				SELECT OT.OrderID
				FROM tblOrderTicket OT
				JOIN tblOrder O ON O.ID = OT.OrderID
				JOIN inserted i ON i.OrderID = OT.OrderID AND i.CarrierTicketNum = OT.CarrierTicketNum AND i.ID <> OT.ID
				WHERE OT.DeleteDateUTC IS NULL and I.DeleteDateUTC IS NULL
				GROUP BY OT.OrderID, OT.CarrierTicketNum
				HAVING COUNT(*) > 1
			)
			BEGIN
				SET @errorString = @errorString + '|Duplicate Ticket Numbers are not allowed'
			END
			
			-- store all the tickets for orders that are not in Generated status and not deleted (only these need validation)
			SELECT i.*, O.StatusID 
			INTO #i
			FROM inserted i
			JOIN tblOrder O ON O.ID = i.OrderID
			WHERE i.DeleteDateUTC IS NULL
			
			/* -- removed with version 3.7.5 
			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (3) AND BOLNum IS NULL)
			BEGIN
				SET @errorString = @errorString + '|BOL # value is required for Meter Run tickets'
			END
			*/
			
			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (1, 2) AND OriginTankID IS NULL AND TankNum IS NULL AND Rejected = 0)
			BEGIN
				SET @errorString = @errorString + '|Tank is required for Gauge Run & Net Volume tickets'
			END

			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (1) AND CarrierTicketNum IS NULL)
			BEGIN
				SET @errorString = @errorString + '|Ticket # value is required for Gauge Run tickets'
			END

			/* require all parameters for orders NOT IN (GAUGER, GENERATED, ASSIGNED, DISPATCHED) */
			IF  EXISTS  (SELECT  ID FROM #i WHERE  Rejected =  0
				AND  ((TicketTypeID IN  (1, 2)  AND  (ProductObsTemp IS  NULL  OR  ProductObsGravity IS  NULL  OR  ProductBSW IS  NULL))
					OR  (TicketTypeID IN  (1)  AND  StatusID NOT  IN  (-9, -10, 1, 2)  AND  (ProductHighTemp IS  NULL  OR  ProductLowTemp IS  NULL)))
				)

			BEGIN
				SET @errorString = @errorString + '|All Product Measurement values are required for Gauge Run tickets'
			END
			
			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (1) AND Rejected = 0 
				AND (OpeningGaugeFeet IS NULL OR OpeningGaugeInch IS NULL OR OpeningGaugeQ IS NULL))
			BEGIN
				SET @errorString = @errorString + '|All Opening Gauge values are required for Gauge Run tickets'
			END
			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (1) AND Rejected = 0 AND StatusID NOT IN (-9, -10, 1, 2)
				AND (ClosingGaugeFeet IS NULL OR ClosingGaugeInch IS NULL OR ClosingGaugeQ IS NULL))
			BEGIN
				SET @errorString = @errorString + '|All Closing Gauge values are required for Gauge Run tickets'
			END

			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (2) AND Rejected = 0 AND StatusID NOT IN (-9, -10, 1, 2) AND (GrossUnits IS NULL))
			BEGIN
				SET @errorString = @errorString + '|Gross Volume value is required for Net Volume tickets'
			END

			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (7) AND Rejected = 0 AND StatusID NOT IN (-9, -10, 1, 2) AND (GrossUnits IS NULL))
			BEGIN
				SET @errorString = @errorString + '|Gross Volume value is required for Gauge Net tickets'
			END

			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (3) AND Rejected = 0 AND StatusID NOT IN (-9, -10, 1, 2) AND (GrossUnits IS NULL OR NetUnits IS NULL))
			BEGIN
				SET @errorString = @errorString + '|Gross & Net Volume values are required for Meter Run tickets'
			END

			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (1) AND Rejected = 0 AND StatusID NOT IN (-9, -10, 1, 2) AND (SealOff IS NULL OR SealOn IS NULL))
			BEGIN
				SET @errorString = @errorString + '|All Seal Off & Seal On values are required for Gauge Run tickets'
			END

			-- if any errors were detected, cancel the entire operation (transaction) and return a LINEFEED-deliminated list of errors
			IF (len(@errorString) > 0)
			BEGIN
				SET @errorString = replace(substring(@errorString, 2, 1000), '|', char(13) + char(10))
				RAISERROR(@errorString, 16, 1)
				IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
				RETURN
			END
			/**********  END OF VALIDATION SECTION ************************/
			
			-- re-compute GaugeRun ticket Gross barrels
			IF UPDATE(OriginTankID) OR UPDATE(OpeningGaugeFeet) OR UPDATE(OpeningGaugeInch) OR UPDATE(OpeningGaugeQ)
				OR UPDATE (ClosingGaugeFeet) OR UPDATE(ClosingGaugeInch) OR UPDATE(ClosingGaugeQ) 
				OR UPDATE (GrossUnits)
			BEGIN
				UPDATE tblOrderTicket
				  SET GrossUnits = dbo.fnConvertUom(dbo.fnOriginTankStrappingLevelDeltaBarrels(
						OT.OriginTankID
					  , OpeningGaugeFeet
					  , OpeningGaugeInch
					  , OpeningGaugeQ
					  , ClosingGaugeFeet
					  , ClosingGaugeInch
					  , ClosingGaugeQ), 1, O.OriginUomID) -- ensure this value is always normalized from BARRELS
				FROM tblOrderTicket OT
				JOIN tblOrder O ON O.ID = OT.OrderID
				WHERE OT.ID IN (SELECT ID FROM inserted) 
				  AND OT.TicketTypeID = 1 -- Gauge Run Ticket
				  AND OT.OpeningGaugeFeet IS NOT NULL
				  AND OT.OpeningGaugeInch IS NOT NULL
				  AND OT.OpeningGaugeQ IS NOT NULL
				  AND OT.ClosingGaugeFeet IS NOT NULL
				  AND OT.ClosingGaugeInch IS NOT NULL
				  AND OT.ClosingGaugeQ IS NOT NULL
			END
			-- recompute the GrossUnits of any changed Meter Run tickets
			IF UPDATE(OpenMeterUnits) OR UPDATE(CloseMeterUnits) BEGIN
				--Print 'updating tblOrderTicket GOV from Open/Close Meter Units'
				UPDATE tblOrderTicket 
					SET GrossUnits = CloseMeterUnits - OpenMeterUnits
				WHERE TicketTypeID = 3  -- meter run tickets ONLY
				  AND OpenMeterUnits IS NOT NULL AND CloseMeterUnits IS NOT NULL
				
			END
			-- re-compute GaugeRun | Net Barrel | Meter Run tickets NetUnits
			IF UPDATE(OpeningGaugeFeet) OR UPDATE(OpeningGaugeInch) OR UPDATE(OpeningGaugeQ)
				OR UPDATE (ClosingGaugeFeet) OR UPDATE(ClosingGaugeInch) OR UPDATE(ClosingGaugeQ) 
				OR UPDATE(OriginTankID) OR UPDATE(GrossUnits) OR UPDATE(GrossStdUnits) OR UPDATE(NetUnits)
				OR UPDATE(ProductObsTemp) OR UPDATE(ProductObsGravity) OR UPDATE(ProductBSW)
				OR UPDATE(OpenMeterUnits) OR UPDATE(CloseMeterUnits)
				OR UPDATE(ProductHighTemp) OR UPDATE(ProductLowTemp)
			BEGIN
				--PRINT 'updating tblOrderTicket NSV/GSV'
				UPDATE tblOrderTicket
				  SET NetUnits = dbo.fnCrudeNetCalculator3T(GrossUnits, ProductObsTemp, ProductHighTemp, ProductLowTemp, ProductObsGravity, isnull(ProductBSW, 0))
					, GrossStdUnits = dbo.fnCrudeNetCalculator3T(GrossUnits, ProductObsTemp, ProductHighTemp, ProductLowTemp, ProductObsGravity, 0)
				WHERE ID IN (SELECT ID FROM inserted)
				  AND TicketTypeID IN (1, 2, 7) -- GAUGE RUN, NET VOLUME, GAUGE NET
				  AND GrossUnits IS NOT NULL
				  AND ProductHighTemp IS NOT NULL
				  AND ProductLowTemp IS NOT NULL
				  AND ProductObsTemp IS NOT NULL
				  AND ProductObsGravity IS NOT NULL
			END
			
			-- ensure the Order record is in-sync with the Tickets
			/* test lines below
			declare @gov decimal(9,3); SELECT TOP 1 @gov = OT.GrossUnits FROM tblOrderTicket OT JOIN inserted i ON i.ID = OT.ID;
			PRINT 'updating tblOrder GOV/GSV/NSV GOV = ' + ltrim(@gov)
			--*/
			UPDATE tblOrder 
			SET OriginGrossUnits = (SELECT sum(GrossUnits) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND OT.Rejected = 0 AND OT.DeleteDateUTC IS NULL)
			  , OriginNetUnits = (SELECT sum(NetUnits) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND OT.Rejected = 0 AND OT.DeleteDateUTC IS NULL)
			  , OriginGrossStdUnits = (SELECT sum(GrossStdUnits) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND OT.Rejected = 0 AND OT.DeleteDateUTC IS NULL)
				-- use the first MeterRun/BasicRun BOLNum as the Order BOL Num (favor the first un-Rejected)
			  , OriginBOLNum = (
					SELECT TOP 1 BOLNum FROM (
						SELECT TOP 1 BOLNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID NOT IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
						UNION SELECT TOP 1 BOLNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID NOT IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X)
				-- use the first GaugeRun|NetVolume CarrierTicketNum as the Order.CarrierTicketNum (favor first un-Rejected)
			  , CarrierTicketNum = (
					SELECT TOP 1 CarrierTicketNum FROM (
						SELECT TOP 1 CarrierTicketNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
						UNION SELECT TOP 1 CarrierTicketNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X)
				-- use the first GaugeRun|NetVolume OriginTankID for the Order (favor first un-Rejected ticket)
				-- BB included ticket additional ticket type (Gauge Net-7)
			  , OriginTankID = (
					SELECT TOP 1 OriginTankID FROM (
						SELECT TOP 1 OriginTankID FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2,7) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
						UNION SELECT TOP 1 OriginTankID FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2,7) AND DeleteDateUTC IS NULL ORDER BY ID) X)
				-- use the first GaugeRun|NetVolume OriginTankNum for the Order (favor first un-Rejected ticket)
			  , OriginTankNum = (
					SELECT TOP 1 OriginTankNum FROM (
						SELECT TOP 1 OriginTankNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
						UNION SELECT TOP 1 OriginTankNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X)
			  , LastChangeDateUTC = GETUTCDATE() -- 3.8.10 update
			  , LastChangedByUser = (SELECT MIN(isnull(LastChangedByUser, CreatedByUser)) FROM inserted WHERE OrderID = O.ID)
			FROM tblOrder O
			WHERE ID IN (SELECT DISTINCT OrderID FROM inserted)

			/* START DB AUDIT *********************************************************/
			BEGIN TRY
				IF EXISTS(SELECT * FROM tblSetting WHERE ID = 32 AND (Value LIKE 'true' OR Value LIKE 'yes'))
					INSERT INTO tblOrderTicketDbAudit (DBAuditDate, ID, OrderID, CarrierTicketNum, TicketTypeID, TankNum, ProductObsGravity, ProductObsTemp, OpeningGaugeFeet, OpeningGaugeInch, OpeningGaugeQ, ClosingGaugeFeet, ClosingGaugeInch, ClosingGaugeQ, GrossUnits, NetUnits, Rejected, RejectNotes, SealOff, SealOn, BOLNum, ProductHighTemp, ProductLowTemp, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser, UID, FromMobileApp, OriginTankID, GrossStdUnits, BottomFeet, BottomInches, BottomQ, RejectReasonID, MeterFactor, OpenMeterUnits, CloseMeterUnits)
						SELECT GETUTCDATE(), ID, OrderID, CarrierTicketNum, TicketTypeID, TankNum, ProductObsGravity, ProductObsTemp, OpeningGaugeFeet, OpeningGaugeInch, OpeningGaugeQ, ClosingGaugeFeet, ClosingGaugeInch, ClosingGaugeQ, GrossUnits, NetUnits, Rejected, RejectNotes, SealOff, SealOn, BOLNum, ProductHighTemp, ProductLowTemp, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser, UID, FromMobileApp, OriginTankID, GrossStdUnits, BottomFeet, BottomInches, BottomQ, RejectReasonID, MeterFactor, OpenMeterUnits, CloseMeterUnits
						FROM deleted d
			END TRY
			BEGIN CATCH
				PRINT 'trigOrderTicket_IU.DBAUDIT FAILURE: ' + ERROR_MESSAGE()
			END CATCH
			/* END DB AUDIT *********************************************************/

			PRINT 'trigOrderTicket_IU COMPLETE'
		END
	END	
END
GO
sp_settriggerorder N'trigOrderTicket_IU', N'first', N'insert'
GO
sp_settriggerorder N'trigOrderTicket_IU', N'first', N'update'
GO

EXEC _spRebuildAllObjects
GO

COMMIT
SET NOEXEC OFF