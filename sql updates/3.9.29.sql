-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.9.28.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.9.28.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0

SET NOEXEC OFF  -- since this is 
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.9.28'
SELECT  @NewVersion = '3.9.29'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'ReportCenter: Add "Yesterday" & "Last 7 Days" Order Date Filter options'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

---------------------------------------------------------------------------------------------
/* increase tblReportColumnDefinition.FilterDropDownSql from varchar(255) -> varchar(1000) */
ALTER TABLE dbo.tblReportColumnDefinition
	DROP CONSTRAINT FK_ReportColumnDefinition_ReportDefinition
GO
ALTER TABLE dbo.tblReportDefinition SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblReportColumnDefinition
	DROP CONSTRAINT FK_ReportColumnDefinition_FilterType
GO
ALTER TABLE dbo.tblReportFilterType SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblReportColumnDefinition
	DROP CONSTRAINT DF_ReportColumnDefinition_FilterAllowCustomText
GO
ALTER TABLE dbo.tblReportColumnDefinition
	DROP CONSTRAINT DF_ReportColumnDefinition_AllowedRoles
GO
ALTER TABLE dbo.tblReportColumnDefinition
	DROP CONSTRAINT FK_ReportColumnDefinition_OrderSingleExport
GO
CREATE TABLE dbo.Tmp_tblReportColumnDefinition
	(
	ID int NOT NULL IDENTITY (100000, 1),
	ReportID int NOT NULL,
	DataField varchar(800) NOT NULL,
	Caption varchar(255) NULL,
	DataFormat varchar(255) NULL,
	FilterDataField varchar(255) NULL,
	FilterTypeID int NOT NULL,
	FilterDropDownSql varchar(1000) NULL,
	FilterAllowCustomText bit NOT NULL,
	AllowedRoles varchar(1000) NOT NULL,
	OrderSingleExport bit NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_tblReportColumnDefinition SET (LOCK_ESCALATION = TABLE)
GO
GRANT SELECT ON dbo.Tmp_tblReportColumnDefinition TO role_iis_acct  AS dbo
GO
ALTER TABLE dbo.Tmp_tblReportColumnDefinition ADD CONSTRAINT
	DF_ReportColumnDefinition_FilterAllowCustomText DEFAULT ((0)) FOR FilterAllowCustomText
GO
ALTER TABLE dbo.Tmp_tblReportColumnDefinition ADD CONSTRAINT
	DF_ReportColumnDefinition_AllowedRoles DEFAULT ('*') FOR AllowedRoles
GO
ALTER TABLE dbo.Tmp_tblReportColumnDefinition ADD CONSTRAINT
	FK_ReportColumnDefinition_OrderSingleExport DEFAULT ((0)) FOR OrderSingleExport
GO
SET IDENTITY_INSERT dbo.Tmp_tblReportColumnDefinition ON
GO
IF EXISTS(SELECT * FROM dbo.tblReportColumnDefinition)
	 EXEC('INSERT INTO dbo.Tmp_tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport)
		SELECT ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport FROM dbo.tblReportColumnDefinition WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_tblReportColumnDefinition OFF
GO
ALTER TABLE dbo.tblReportColumnDefinitionBaseFilter
	DROP CONSTRAINT FK_ReportColumnDefinitionBaseFilter_ReportColumn
GO
ALTER TABLE dbo.tblReportColumnDefinitionCountryException
	DROP CONSTRAINT FK_ReportColumnDefinitionCountryException_ReportColumnDefinition
GO
ALTER TABLE dbo.tblUserReportColumnDefinition
	DROP CONSTRAINT FK_UserReportColumnDefinition_ReportColumn
GO
DROP TABLE dbo.tblReportColumnDefinition
GO
EXECUTE sp_rename N'dbo.Tmp_tblReportColumnDefinition', N'tblReportColumnDefinition', 'OBJECT' 
GO
ALTER TABLE dbo.tblReportColumnDefinition ADD CONSTRAINT
	PK_ReportColumnDefinition PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE NONCLUSTERED INDEX udxReportColumnDefinition_Report_Caption ON dbo.tblReportColumnDefinition
	(
	ReportID,
	Caption
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.tblReportColumnDefinition ADD CONSTRAINT
	FK_ReportColumnDefinition_FilterType FOREIGN KEY
	(
	FilterTypeID
	) REFERENCES dbo.tblReportFilterType
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblReportColumnDefinition ADD CONSTRAINT
	FK_ReportColumnDefinition_ReportDefinition FOREIGN KEY
	(
	ReportID
	) REFERENCES dbo.tblReportDefinition
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblUserReportColumnDefinition ADD CONSTRAINT
	FK_UserReportColumnDefinition_ReportColumn FOREIGN KEY
	(
	ReportColumnID
	) REFERENCES dbo.tblReportColumnDefinition
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblUserReportColumnDefinition SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblReportColumnDefinitionCountryException ADD CONSTRAINT
	FK_ReportColumnDefinitionCountryException_ReportColumnDefinition FOREIGN KEY
	(
	ReportColumnID
	) REFERENCES dbo.tblReportColumnDefinition
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblReportColumnDefinitionCountryException SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblReportColumnDefinitionBaseFilter ADD CONSTRAINT
	FK_ReportColumnDefinitionBaseFilter_ReportColumn FOREIGN KEY
	(
	ReportColumnID
	) REFERENCES dbo.tblReportColumnDefinition
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblReportColumnDefinitionBaseFilter SET (LOCK_ESCALATION = TABLE)
GO
---------------------------------------------------------------------------------------------

/* add the "Yesterday" & "Last 7 Days" OrderDate filtering options */
UPDATE tblReportColumnDefinition
  SET FilterDropDownSql = 
	'SELECT TOP 100 PERCENT ID, Name
	 FROM (
		SELECT ID=1, Name=''Week to Date'', SortID = 1 
		UNION SELECT 2, ''Month to Date'', 2 
		UNION SELECT 3, ''Last Month'', 5 
		UNION SELECT 4, ''Quarter to Date'', 6
		UNION SELECT 5, ''Year to Date'', 7 
		UNION SELECT 6, ''Yesterday'', 3
		UNION SELECT 7, ''Last 7 Days'', 4
	) X
	ORDER BY SortID'
WHERE ID = 4
GO

/* add 2 new Report Center Grouping Functions to facilitate Daily Avg / Sum - Last Day summary features */
INSERT INTO tblReportGroupingFunction (ID, Name)
	SELECT 15, 'Daily Avg'
	UNION
	SELECT 16, 'Sum - Last Day'
GO

/***********************************
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Report Center Order data
-- Changes:
   -		 - 05/21/15	 - GSM - Adding Gauger Process data elements to Report Center
   -		 - 06/08/15	 - BB  - Add "OriginCity" and "OriginCityState" columns			
   -		 - 06/17/15  - BB  - Add "DestCity" and "DestCityState" columns
   - 3.7.39 - 2015/06/30 - BB  - Add "Shipper Ticket Type" column
   - 3.8.20 - 2015/08/26 - BB  - Add Order Create Date column
   - 3.9.14 - 2015/09/08 - BB  - Add Order Approved column
   - 3.9.20 - 2015/10/22 - BB  - Add Origin|Dest Driver fields
						 - JAE - Added Origin Truck field 
   - 3.9.25 - 2015/11/09 - BB  - DCWEB-925 - Add OriginDriverGroup field
            - 2015/11/10 - JAE - Drop driver and truck fields since they now appear in viewOrder
            - 2015/11/16 - BB  - Add Driver GPS & DTP columns for Pickup/Delivery
			- 2015/11/17 - BB  - Changed new GPS & DTP columns to use fnReportCenterDriverGPS & fnReportCenterDriverDTP
   - 3.9.29 - 2015/11/21 - KDA - reorganize view to optimize performance (use a sub-query to avoid executing the costly GPS First@Point views over the entire table)
							   - remove some GPS fields (using the fnOrderDriverLocationDTP & similar functions), for performance reasons
***********************************/
ALTER VIEW viewReportCenter_Orders AS
	SELECT X.*
		, OriginGpsLatLon = LTRIM(DLO.LAT) + ',' + LTRIM(DLO.LON)
		, OriginAccuracyMeters = DLO.SourceAccuracyMeters
		, OriginDistance = ISNULL(cast(DLO.DistanceToPoint AS INT), 99999)
		, OriginGpsArrived = CASE WHEN DLO.DistanceToPoint IS NOT NULL AND DLO.DistanceToPoint BETWEEN 0 AND OriginGeoFenceRadiusMeters THEN 1 ELSE 0 END
		, DestGpsLatLon = LTRIM(DLD.LAT) + ',' + LTRIM(DLD.Lon)
		, DestAccuracyMeters = DLD.SourceAccuracyMeters
		, DestDistance = ISNULL(CAST(DLD.DistanceToPoint AS INT), 99999)
		, DestGpsArrived = CASE WHEN DLD.DistanceToPoint IS NOT NULL AND DLD.DistanceToPoint BETWEEN 0 AND DestGeoFenceRadiusMeters THEN 1 ELSE 0 END		

/*	-- these should be ultimately be created as an EXPRESSION (90000 series) tblReportColumnDefinition record with DataField LIKE 'dbo.fnOrderDriverLocationDTP(RS.ID, 2, RS.OriginID, NULL)' 
		, DriverDTPPickupArrive = dbo.fnOrderDriverLocationDTP(X.ID, 2, X.OriginID, NULL)
		, DriverDTPPickupDepart = dbo.fnOrderDriverLocationDTP(X.ID, 3, X.OriginID, NULL)
		, DriverGPSPickupArrive = dbo.fnOrderDriverLocation(X.ID, 2, 1)
		, DriverGPSPickupDepart = dbo.fnOrderDriverLocation(X.ID, 3, 1)
		--
		, DriverDTPDeliverArrive = dbo.fnOrderDriverLocationDTP(X.ID, 2, NULL, X.DestinationID)
		, DriverDTPDeliverDepart = dbo.fnOrderDriverLocationDTP(X.ID, 3, NULL, X.DestinationID)
		, DriverGPSDeliverArrive = dbo.fnOrderDriverLocation(X.ID, 2, 0)
		, DriverGPSDeliverDepart = dbo.fnOrderDriverLocation(X.ID, 3, 0)
*/
	FROM (
		SELECT O.*
			--
			, ShipperBatchNum = SS.BatchNum
			, ShipperBatchInvoiceNum = SSB.InvoiceNum
			, ShipperSettlementUomID = SS.SettlementUomID
			, ShipperSettlementUom = SS.SettlementUom
			, ShipperMinSettlementUnits = SS.MinSettlementUnits
			, ShipperSettlementUnits = SS.SettlementUnits
			, ShipperRateSheetRate = SS.RateSheetRate
			, ShipperRateSheetRateType = SS.RateSheetRateType
			, ShipperRouteRate = SS.RouteRate
			, ShipperRouteRateType = SS.RouteRateType
			, ShipperLoadRate = ISNULL(SS.RouteRate, SS.RateSheetRate)
			, ShipperLoadRateType = ISNULL(SS.RouteRateType, SS.RateSheetRateType)
			, ShipperLoadAmount = SS.LoadAmount
			, ShipperOrderRejectRate = SS.OrderRejectRate 
			, ShipperOrderRejectRateType = SS.OrderRejectRateType 
			, ShipperOrderRejectAmount = SS.OrderRejectAmount 
			, ShipperWaitFeeSubUnit = SS.WaitFeeSubUnit  
			, ShipperWaitFeeRoundingType = SS.WaitFeeRoundingType  
			, ShipperOriginWaitBillableHours = SS.OriginWaitBillableHours  
			, ShipperOriginWaitBillableMinutes = SS.OriginWaitBillableMinutes  
			, ShipperOriginWaitRate = SS.OriginWaitRate
			, ShipperOriginWaitAmount = SS.OriginWaitAmount
			, ShipperDestinationWaitBillableHours = SS.DestinationWaitBillableHours 
			, ShipperDestinationWaitBillableMinutes = SS.DestinationWaitBillableMinutes   
			, ShipperDestinationWaitRate = SS.DestinationWaitRate  
			, ShipperDestinationWaitAmount = SS.DestinationWaitAmount  
			, ShipperTotalWaitAmount = SS.TotalWaitAmount
			, ShipperTotalWaitBillableMinutes = NULLIF(ISNULL(SS.OriginWaitBillableMinutes, 0) + ISNULL(SS.DestinationWaitBillableMinutes, 0), 0)
			, ShipperTotalWaitBillableHours = NULLIF(ISNULL(SS.OriginWaitBillableHours, 0) + ISNULL(SS.DestinationWaitBillableHours, 0), 0)		
			, ShipperFuelSurchargeRate = SS.FuelSurchargeRate
			, ShipperFuelSurchargeAmount = SS.FuelSurchargeAmount
			, ShipperChainupRate = SS.ChainupRate
			, ShipperChainupRateType = SS.ChainupRateType  
			, ShipperChainupAmount = SS.ChainupAmount
			, ShipperRerouteRate = SS.RerouteRate
			, ShipperRerouteRateType = SS.RerouteRateType  
			, ShipperRerouteAmount = SS.RerouteAmount
			, ShipperSplitLoadRate = SS.SplitLoadRate
			, ShipperSplitLoadRateType = SS.SplitLoadRateType  
			, ShipperSplitLoadAmount = SS.SplitLoadAmount
			, ShipperH2SRate = SS.H2SRate
			, ShipperH2SRateType = SS.H2SRateType  
			, ShipperH2SAmount = SS.H2SAmount
			, ShipperTaxRate = SS.OriginTaxRate
			, ShipperTotalAmount = SS.TotalAmount
			, ShipperDestCode = CDC.Code
			, ShipperTicketType = STT.TicketType
			--
			, CarrierBatchNum = SC.BatchNum
			, CarrierSettlementUomID = SC.SettlementUomID
			, CarrierSettlementUom = SC.SettlementUom
			, CarrierMinSettlementUnits = SC.MinSettlementUnits
			, CarrierSettlementUnits = SC.SettlementUnits
			, CarrierRateSheetRate = SC.RateSheetRate
			, CarrierRateSheetRateType = SC.RateSheetRateType
			, CarrierRouteRate = SC.RouteRate
			, CarrierRouteRateType = SC.RouteRateType
			, CarrierLoadRate = ISNULL(SC.RouteRate, SC.RateSheetRate)
			, CarrierLoadRateType = ISNULL(SC.RouteRateType, SC.RateSheetRateType)
			, CarrierLoadAmount = SC.LoadAmount
			, CarrierOrderRejectRate = SC.OrderRejectRate 
			, CarrierOrderRejectRateType = SC.OrderRejectRateType 
			, CarrierOrderRejectAmount = SC.OrderRejectAmount 
			, CarrierWaitFeeSubUnit = SC.WaitFeeSubUnit  
			, CarrierWaitFeeRoundingType = SC.WaitFeeRoundingType  
			, CarrierOriginWaitBillableHours = SS.OriginWaitBillableHours  
			, CarrierOriginWaitBillableMinutes = SC.OriginWaitBillableMinutes  
			, CarrierOriginWaitRate = SC.OriginWaitRate
			, CarrierOriginWaitAmount = SC.OriginWaitAmount
			, CarrierDestinationWaitBillableHours = SS.DestinationWaitBillableHours 
			, CarrierDestinationWaitBillableMinutes = SC.DestinationWaitBillableMinutes  
			, CarrierDestinationWaitRate = SC.DestinationWaitRate 
			, CarrierDestinationWaitAmount = SC.DestinationWaitAmount  
			, CarrierTotalWaitAmount = SC.TotalWaitAmount
			, CarrierTotalWaitBillableMinutes = NULLIF(ISNULL(SS.OriginWaitBillableMinutes, 0) + ISNULL(SS.DestinationWaitBillableMinutes, 0), 0)
			, CarrierTotalWaitBillableHours = NULLIF(ISNULL(SS.OriginWaitBillableHours, 0) + ISNULL(SS.DestinationWaitBillableHours, 0), 0)		
			, CarrierFuelSurchargeRate = SC.FuelSurchargeRate
			, CarrierFuelSurchargeAmount = SC.FuelSurchargeAmount
			, CarrierChainupRate = SC.ChainupRate
			, CarrierChainupRateType = SC.ChainupRateType  
			, CarrierChainupAmount = SC.ChainupAmount
			, CarrierRerouteRate = SC.RerouteRate
			, CarrierRerouteRateType = SC.RerouteRateType  
			, CarrierRerouteAmount = SC.RerouteAmount
			, CarrierSplitLoadRate = SC.SplitLoadRate
			, CarrierSplitLoadRateType = SC.SplitLoadRateType  
			, CarrierSplitLoadAmount = SC.SplitLoadAmount
			, CarrierH2SRate = SC.H2SRate
			, CarrierH2SRateType = SC.H2SRateType  
			, CarrierH2SAmount = SC.H2SAmount
			, CarrierTaxRate = SC.OriginTaxRate
			, CarrierTotalAmount = SC.TotalAmount
			--
			, OriginLatLon = LTRIM(OO.LAT) + ',' + LTRIM(OO.LON)
			, OriginGeoFenceRadiusMeters = OO.GeoFenceRadiusMeters
			, OriginCTBNum = OO.CTBNum
			, OriginFieldName = OO.FieldName
			, OriginCity = OO.City																				
			, OriginCityState = OO.City + ', ' + (SELECT Abbreviation FROM tblState WHERE ID = OO.StateID)		
			--
			, DestLatLon = LTRIM(D.LAT) + ',' + LTRIM(D.LON)
			, DestGeoFenceRadiusMeters = D.GeoFenceRadiusMeters
			, DestCity = D.City
			, DestCityState = D.City + ', ' + (SELECT Abbreviation FROM tblState WHERE ID = OO.StateID)
			--
			, Gauger = GAO.Gauger						
			, GaugerIDNumber = GA.IDNumber
			, GaugerFirstName = GA.FirstName
			, GaugerLastName = GA.LastName
			, GaugerRejected = GAO.Rejected
			, GaugerRejectReasonID = GAO.RejectReasonID
			, GaugerRejectNotes = GAO.RejectNotes
			, GaugerRejectNumDesc = GORR.NumDesc
			, GaugerPrintDate = dbo.fnUTC_to_Local(GAO.PrintDateUTC, O.OriginTimeZoneID, O.OriginUseDST)
			--
			, T_GaugerCarrierTicketNum = CASE WHEN GAO.TicketCount = 0 THEN LTRIM(GAO.OrderNum) + CASE WHEN GAO.Rejected = 1 THEN 'X' ELSE '' END ELSE GOT.CarrierTicketNum END 
			, T_GaugerTankNum = ISNULL(GOT.OriginTankText, GAO.OriginTankText)
			, T_GaugerIsStrappedTank = GOT.IsStrappedTank 
			, T_GaugerProductObsTemp = GOT.ProductObsTemp
			, T_GaugerProductObsGravity = GOT.ProductObsGravity
			, T_GaugerProductBSW = GOT.ProductBSW		
			, T_GaugerOpeningGaugeFeet = GOT.OpeningGaugeFeet
			, T_GaugerOpeningGaugeInch = GOT.OpeningGaugeInch		
			, T_GaugerOpeningGaugeQ = GOT.OpeningGaugeQ			
			, T_GaugerBottomFeet = GOT.BottomFeet
			, T_GaugerBottomInches = GOT.BottomInches		
			, T_GaugerBottomQ = GOT.BottomQ		
			, T_GaugerRejected = GOT.Rejected
			, T_GaugerRejectReasonID = GOT.RejectReasonID
			, T_GaugerRejectNumDesc = GOT.RejectNumDesc
			, T_GaugerRejectNotes = GOT.RejectNotes	
			--
			, OrderCreateDate = dbo.fnUTC_to_Local(O.CreateDateUTC, O.OriginTimeZoneID, O.OriginUseDST)
			, OrderApproved = CAST(ISNULL(OA.Approved,0) AS BIT)
		FROM viewOrder_OrderTicket_Full O
		JOIN tblOrigin OO ON OO.ID = O.OriginID
		JOIN tblDestination D ON D.ID = O.DestinationID
		--
		LEFT JOIN viewGaugerOrder GAO ON GAO.OrderID = O.ID			            
		LEFT JOIN viewGaugerOrderTicket GOT ON GOT.UID = O.T_UID	            
		LEFT JOIN viewGauger GA ON GA.ID = GAO.GaugerID				            
		LEFT JOIN viewOrderRejectReason GORR ON GORR.ID = GAO.RejectReasonID 
		--
		LEFT JOIN tblCustomerDestinationCode CDC ON CDC.CustomerID = O.CustomerID AND CDC.DestinationID = O.DestinationID
		LEFT JOIN viewOrderSettlementCarrier SC ON SC.OrderID = O.ID
		LEFT JOIN viewOrderSettlementShipper SS ON SS.OrderID = O.ID
		LEFT JOIN tblShipperSettlementBatch SSB ON SSB.ID = SS.BatchID
		LEFT JOIN tblShipperTicketType AS STT ON STT.CustomerID = O.CustomerID AND STT.TicketTypeID = O.TicketTypeID
		--
		LEFT JOIN tblOrderApproval AS OA ON OA.OrderID = O.ID
	) X
	LEFT JOIN viewDriverLocation_OriginFirstArrive DLO ON DLO.OrderID = X.ID AND DLO.OriginID = X.OriginID
	LEFT JOIN viewDriverLocation_DestinationFirstArrive DLD ON DLD.OrderID = X.ID AND DLD.DestinationID = X.DestinationID
GO

/* this is here to correct the last comment - was missing the version stamp of 3.9.25 */
go
/***********************************
Date Created: 9 Mar 2013
Author: Kevin Alons
Purpose: return Order JOIN OrderTicket + computed Reroute details into a single view
Special: 
	1) all TICKET fields should be preceded with T_ to ensure they are unique and to explicitly identify them as TICKET (not ORDER) level values
Changes:
	- 3.7.16 - 2015/05/22 - KDA - added T_UID field
	- 3.8.15 - 2015/08/15 -  BB - added T_TankID field
	- 3.9.25 - 2015/11/06 -  BB - DCWEB-921 - Added T_TicketTypeID field for filtering on ticket ticket type
***********************************/
ALTER VIEW [dbo].[viewOrder_OrderTicket_Full] AS 
	SELECT OE.* 
        , T_UID =  OT.UID
		, T_ID = OT.ID
		, T_TicketType = CASE WHEN OE.TicketCount = 0 THEN OE.TicketType ELSE OT.TicketType END 
		, T_TicketTypeID = OT.TicketTypeID
		, T_CarrierTicketNum = CASE WHEN OE.TicketCount = 0 THEN ltrim(OE.OrderNum) + CASE WHEN OE.Rejected = 1 THEN 'X' ELSE '' END ELSE OT.CarrierTicketNum END 
		, T_BOLNum = CASE WHEN OE.TicketCount = 0 THEN OE.OriginBOLNum ELSE OT.BOLNum END 
		, T_TankNum = isnull(OT.OriginTankText, OE.OriginTankText)
		, T_TankID = OT.OriginTankID
		, T_IsStrappedTank = OT.IsStrappedTank 
		, T_BottomFeet = OT.BottomFeet
		, T_BottomInches = OT.BottomInches
		, T_BottomQ = OT.BottomQ 
		, T_OpeningGaugeFeet = OT.OpeningGaugeFeet 
		, T_OpeningGaugeInch = OT.OpeningGaugeInch 
		, T_OpeningGaugeQ = OT.OpeningGaugeQ 
		, T_ClosingGaugeFeet = OT.ClosingGaugeFeet 
		, T_ClosingGaugeInch = OT.ClosingGaugeInch 
		, T_ClosingGaugeQ = OT.ClosingGaugeQ 
		, T_OpenTotalQ = dbo.fnGaugeQtrInches(OT.OpeningGaugeFeet, OT.OpeningGaugeInch, OT.OpeningGaugeQ) 
		, T_CloseTotalQ = dbo.fnGaugeQtrInches(OT.ClosingGaugeFeet, OT.ClosingGaugeInch, ot.ClosingGaugeQ) 
		, T_OpenReading = ltrim(OT.OpeningGaugeFeet) + 'ft ' + ltrim(OT.OpeningGaugeInch) + 'in ' + ltrim(OT.OpeningGaugeQ) + 'q' 
		, T_CloseReading = ltrim(OT.ClosingGaugeFeet) + 'ft ' + ltrim(OT.ClosingGaugeInch) + 'in ' + ltrim(OT.ClosingGaugeQ) + 'q' 
		, T_CorrectedAPIGravity = round(cast(OT.Gravity60F as decimal(9,4)), 9, 4) 
		, T_GrossStdUnits = OT.GrossStdUnits
		, T_SealOff = ltrim(OT.SealOff) 
		, T_SealOn = ltrim(OT.SealOn) 
		, T_HighTemp = OT.ProductHighTemp
		, T_LowTemp = OT.ProductLowTemp
		, T_ProductObsTemp = OT.ProductObsTemp 
		, T_ProductObsGravity = OT.ProductObsGravity 
		, T_ProductBSW = OT.ProductBSW 
		, T_Rejected = isnull(OT.Rejected, OE.Rejected)
		, T_RejectReasonID = OT.RejectReasonID
		, T_RejectNum = OT.RejectNum
		, T_RejectDesc = OT.RejectDesc
		, T_RejectNumDesc = OT.RejectNumDesc
		, T_RejectNotes = OT.RejectNotes 
		, T_GrossUnits = OT.GrossUnits 
		, T_NetUnits = OT.NetUnits 
		, T_MeterFactor = OT.MeterFactor
		, T_OpenMeterUnits = OT.OpenMeterUnits
		, T_CloseMeterUnits = OT.CloseMeterUnits
		, T_DispatchConfirmNum = CASE WHEN OE.TicketCount = 0 THEN OE.DispatchConfirmNum ELSE isnull(OT.DispatchConfirmNum, OE.DispatchConFirmNum) END
		, PreviousDestinations = dbo.fnRerouteDetails(OE.ID, 'PreviousDestinations', '<br/>') 
		, RerouteUsers = dbo.fnRerouteDetails(OE.ID, 'RerouteUsers', '<br/>') 
		, RerouteDates = dbo.fnRerouteDetails(OE.ID, 'RerouteDates', '<br/>') 
		, RerouteNotes = dbo.fnRerouteDetails(OE.ID, 'RerouteNotes', '<br/>')
	FROM dbo.viewOrderLocalDates OE
	LEFT JOIN viewOrderTicket OT ON OT.OrderID = OE.ID AND OT.DeleteDateUTC IS NULL
	WHERE OE.DeleteDateUTC IS NULL

GO

/* add a new NOT IN filter operator */
insert into tblreportfilteroperator
	select 13, 'Not In'
go
update tblreportfiltertype
  set FilterOperatorID_CSV = '1,3,13'
where id = 2
go

COMMIT
SET NOEXEC OFF