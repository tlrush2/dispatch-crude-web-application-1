/* 
	fix UOM conversion issues with settlement logic
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.6.10'
SELECT  @NewVersion = '2.6.11'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

ALTER TABLE dbo.tblOrderInvoiceCarrier
	DROP CONSTRAINT FK_OrderInvoiceCarrier_WaitFeeRoundingTypeID
GO
ALTER TABLE dbo.tblWaitFeeRoundingType SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderInvoiceCarrier
	DROP CONSTRAINT FK_OrderInvoiceCarrier_WaitFeeSubUnitID
GO
ALTER TABLE dbo.tblWaitFeeSubUnit SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderInvoiceCarrier
	DROP CONSTRAINT FK_tblOrderInvoiceCarrier_tblOrder
GO
ALTER TABLE dbo.tblOrder SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderInvoiceCarrier
	DROP CONSTRAINT FK_OrderInvoiceCarrier_Uom
GO
ALTER TABLE dbo.tblUom SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderInvoiceCarrier
	DROP CONSTRAINT FK_OrderInvoiceCarrier_SettlementFactor
GO
ALTER TABLE dbo.tblSettlementFactor SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderInvoiceCarrier
	DROP CONSTRAINT FK_OrderInvoiceCarrier_SettlementBatch
GO
ALTER TABLE dbo.tblCarrierSettlementBatch SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderInvoiceCarrier
	DROP CONSTRAINT DF_OrderInvoiceCarrier_CreateDateUTC
GO
ALTER TABLE dbo.tblOrderInvoiceCarrier
	DROP CONSTRAINT DF_OrderInvoiceCarrier_H2SRate
GO
ALTER TABLE dbo.tblOrderInvoiceCarrier
	DROP CONSTRAINT DF_OrderInvoiceCarrier_H2SFee
GO
ALTER TABLE dbo.tblOrderInvoiceCarrier
	DROP CONSTRAINT DF_OrderInvoiceCarrier_TaxRate
GO
ALTER TABLE dbo.tblOrderInvoiceCarrier
	DROP CONSTRAINT DF_OrderInvoiceCarrier_Uom
GO
CREATE TABLE dbo.Tmp_tblOrderInvoiceCarrier
	(
	ID int NOT NULL IDENTITY (1, 1),
	OrderID int NOT NULL,
	ChainupFee money NOT NULL,
	RerouteFee money NOT NULL,
	WaitFee money NOT NULL,
	RejectionFee money NOT NULL,
	LoadFee money NOT NULL,
	TotalFee money NOT NULL,
	CreateDateUTC smalldatetime NULL,
	CreatedByUser varchar(100) NULL,
	LastChangeDateUTC smalldatetime NULL,
	LastChangedByUser varchar(100) NULL,
	SettlementFactorID int NULL,
	RouteRate decimal(9, 4) NULL,
	WaitRate decimal(9, 4) NULL,
	BillableWaitMinutes int NULL,
	WaitFeeSubUnitID int NULL,
	WaitFeeRoundingTypeID int NULL,
	FuelSurcharge smallmoney NULL,
	H2SRate decimal(9, 4) NOT NULL,
	H2SFee smallmoney NOT NULL,
	TaxRate decimal(9, 4) NOT NULL,
	BatchID int NULL,
	UomID int NOT NULL,
	MinSettlementUnits decimal(18, 6) NULL,
	Units decimal(18, 6) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_tblOrderInvoiceCarrier SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_tblOrderInvoiceCarrier ADD CONSTRAINT
	DF_OrderInvoiceCarrier_CreateDateUTC DEFAULT (getutcdate()) FOR CreateDateUTC
GO
ALTER TABLE dbo.Tmp_tblOrderInvoiceCarrier ADD CONSTRAINT
	DF_OrderInvoiceCarrier_H2SRate DEFAULT ((0)) FOR H2SRate
GO
ALTER TABLE dbo.Tmp_tblOrderInvoiceCarrier ADD CONSTRAINT
	DF_OrderInvoiceCarrier_H2SFee DEFAULT ((0)) FOR H2SFee
GO
ALTER TABLE dbo.Tmp_tblOrderInvoiceCarrier ADD CONSTRAINT
	DF_OrderInvoiceCarrier_TaxRate DEFAULT ((0)) FOR TaxRate
GO
ALTER TABLE dbo.Tmp_tblOrderInvoiceCarrier ADD CONSTRAINT
	DF_OrderInvoiceCarrier_Uom DEFAULT ((1)) FOR UomID
GO
SET IDENTITY_INSERT dbo.Tmp_tblOrderInvoiceCarrier ON
GO
IF EXISTS(SELECT * FROM dbo.tblOrderInvoiceCarrier)
	 EXEC('INSERT INTO dbo.Tmp_tblOrderInvoiceCarrier (ID, OrderID, ChainupFee, RerouteFee, WaitFee, RejectionFee, LoadFee, TotalFee, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, SettlementFactorID, RouteRate, WaitRate, BillableWaitMinutes, WaitFeeSubUnitID, WaitFeeRoundingTypeID, FuelSurcharge, H2SRate, H2SFee, TaxRate, BatchID, UomID, MinSettlementUnits, Units)
		SELECT ID, OrderID, ChainupFee, RerouteFee, WaitFee, RejectionFee, LoadFee, TotalFee, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, SettlementFactorID, RouteRate, WaitRate, BillableWaitMinutes, WaitFeeSubUnitID, WaitFeeRoundingTypeID, FuelSurcharge, H2SRate, H2SFee, TaxRate, BatchID, UomID, MinSettlementUnits, Units FROM dbo.tblOrderInvoiceCarrier WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_tblOrderInvoiceCarrier OFF
GO
DROP TABLE dbo.tblOrderInvoiceCarrier
GO
EXECUTE sp_rename N'dbo.Tmp_tblOrderInvoiceCarrier', N'tblOrderInvoiceCarrier', 'OBJECT' 
GO
ALTER TABLE dbo.tblOrderInvoiceCarrier ADD CONSTRAINT
	PK_OrderInvoiceCarrier PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.tblOrderInvoiceCarrier ADD CONSTRAINT
	uqOrderInvoiceCarrier UNIQUE NONCLUSTERED 
	(
	OrderID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.tblOrderInvoiceCarrier ADD CONSTRAINT
	FK_OrderInvoiceCarrier_SettlementBatch FOREIGN KEY
	(
	BatchID
	) REFERENCES dbo.tblCarrierSettlementBatch
	(
	ID
	) ON UPDATE  CASCADE 
	 ON DELETE  SET NULL 
	
GO
ALTER TABLE dbo.tblOrderInvoiceCarrier ADD CONSTRAINT
	FK_OrderInvoiceCarrier_SettlementFactor FOREIGN KEY
	(
	SettlementFactorID
	) REFERENCES dbo.tblSettlementFactor
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderInvoiceCarrier ADD CONSTRAINT
	FK_OrderInvoiceCarrier_Uom FOREIGN KEY
	(
	UomID
	) REFERENCES dbo.tblUom
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderInvoiceCarrier ADD CONSTRAINT
	FK_tblOrderInvoiceCarrier_tblOrder FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.tblOrder
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	
GO
ALTER TABLE dbo.tblOrderInvoiceCarrier ADD CONSTRAINT
	FK_OrderInvoiceCarrier_WaitFeeSubUnitID FOREIGN KEY
	(
	WaitFeeSubUnitID
	) REFERENCES dbo.tblWaitFeeSubUnit
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderInvoiceCarrier ADD CONSTRAINT
	FK_OrderInvoiceCarrier_WaitFeeRoundingTypeID FOREIGN KEY
	(
	WaitFeeRoundingTypeID
	) REFERENCES dbo.tblWaitFeeRoundingType
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO

ALTER TABLE dbo.tblOrderInvoiceCustomer
	DROP CONSTRAINT FK_OrderInvoiceCustomer_WaitFeeRoundingTypeID
GO
ALTER TABLE dbo.tblWaitFeeRoundingType SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderInvoiceCustomer
	DROP CONSTRAINT FK_OrderInvoiceCustomer_WaitFeeSubUnitID
GO
ALTER TABLE dbo.tblWaitFeeSubUnit SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderInvoiceCustomer
	DROP CONSTRAINT FK_tblOrderInvoiceCustomer_tblOrder
GO
ALTER TABLE dbo.tblOrder SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderInvoiceCustomer
	DROP CONSTRAINT FK_OrderInvoiceCustomer_Uom
GO
ALTER TABLE dbo.tblUom SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderInvoiceCustomer
	DROP CONSTRAINT FK_OrderInvoiceCustomer_SettlementFactor
GO
ALTER TABLE dbo.tblSettlementFactor SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderInvoiceCustomer
	DROP CONSTRAINT FK_OrderInvoiceCustomer_SettlementBatch
GO
ALTER TABLE dbo.tblCustomerSettlementBatch SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderInvoiceCustomer
	DROP CONSTRAINT DF_OrderInvoiceCustomer_CreateDateUTC
GO
ALTER TABLE dbo.tblOrderInvoiceCustomer
	DROP CONSTRAINT DF_OrderInvoiceCustomer_H2SRate
GO
ALTER TABLE dbo.tblOrderInvoiceCustomer
	DROP CONSTRAINT DF_OrderInvoiceCustomer_H2SFee
GO
ALTER TABLE dbo.tblOrderInvoiceCustomer
	DROP CONSTRAINT DF_OrderInvoiceCustomer_TaxRate
GO
ALTER TABLE dbo.tblOrderInvoiceCustomer
	DROP CONSTRAINT DF_OrderInvoiceCustomer_Uom
GO
CREATE TABLE dbo.Tmp_tblOrderInvoiceCustomer
	(
	ID int NOT NULL IDENTITY (1, 1),
	OrderID int NOT NULL,
	ChainupFee money NOT NULL,
	RerouteFee money NOT NULL,
	WaitFee money NOT NULL,
	RejectionFee money NOT NULL,
	LoadFee money NOT NULL,
	TotalFee money NOT NULL,
	CreateDateUTC smalldatetime NULL,
	CreatedByUser varchar(100) NULL,
	LastChangeDateUTC smalldatetime NULL,
	LastChangedByUser varchar(100) NULL,
	SettlementFactorID int NULL,
	RouteRate decimal(9, 4) NULL,
	WaitRate decimal(9, 4) NULL,
	BillableWaitMinutes int NULL,
	WaitFeeSubUnitID int NULL,
	WaitFeeRoundingTypeID int NULL,
	FuelSurcharge smallmoney NULL,
	H2SRate decimal(9, 4) NOT NULL,
	H2SFee smallmoney NOT NULL,
	TaxRate decimal(9, 4) NOT NULL,
	BatchID int NULL,
	UomID int NOT NULL,
	MinSettlementUnits decimal(18, 6) NULL,
	Units decimal(18, 6) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_tblOrderInvoiceCustomer SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_tblOrderInvoiceCustomer ADD CONSTRAINT
	DF_OrderInvoiceCustomer_CreateDateUTC DEFAULT (getutcdate()) FOR CreateDateUTC
GO
ALTER TABLE dbo.Tmp_tblOrderInvoiceCustomer ADD CONSTRAINT
	DF_OrderInvoiceCustomer_H2SRate DEFAULT ((0)) FOR H2SRate
GO
ALTER TABLE dbo.Tmp_tblOrderInvoiceCustomer ADD CONSTRAINT
	DF_OrderInvoiceCustomer_H2SFee DEFAULT ((0)) FOR H2SFee
GO
ALTER TABLE dbo.Tmp_tblOrderInvoiceCustomer ADD CONSTRAINT
	DF_OrderInvoiceCustomer_TaxRate DEFAULT ((0)) FOR TaxRate
GO
ALTER TABLE dbo.Tmp_tblOrderInvoiceCustomer ADD CONSTRAINT
	DF_OrderInvoiceCustomer_Uom DEFAULT ((1)) FOR UomID
GO
SET IDENTITY_INSERT dbo.Tmp_tblOrderInvoiceCustomer ON
GO
IF EXISTS(SELECT * FROM dbo.tblOrderInvoiceCustomer)
	 EXEC('INSERT INTO dbo.Tmp_tblOrderInvoiceCustomer (ID, OrderID, ChainupFee, RerouteFee, WaitFee, RejectionFee, LoadFee, TotalFee, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, SettlementFactorID, RouteRate, WaitRate, BillableWaitMinutes, WaitFeeSubUnitID, WaitFeeRoundingTypeID, FuelSurcharge, H2SRate, H2SFee, TaxRate, BatchID, UomID, MinSettlementUnits, Units)
		SELECT ID, OrderID, ChainupFee, RerouteFee, WaitFee, RejectionFee, LoadFee, TotalFee, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, SettlementFactorID, RouteRate, WaitRate, BillableWaitMinutes, WaitFeeSubUnitID, WaitFeeRoundingTypeID, FuelSurcharge, H2SRate, H2SFee, TaxRate, BatchID, UomID, MinSettlementUnits, Units FROM dbo.tblOrderInvoiceCustomer WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_tblOrderInvoiceCustomer OFF
GO
DROP TABLE dbo.tblOrderInvoiceCustomer
GO
EXECUTE sp_rename N'dbo.Tmp_tblOrderInvoiceCustomer', N'tblOrderInvoiceCustomer', 'OBJECT' 
GO
ALTER TABLE dbo.tblOrderInvoiceCustomer ADD CONSTRAINT
	PK_OrderInvoiceCustomer PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.tblOrderInvoiceCustomer ADD CONSTRAINT
	uqOrderInvoiceCustomer UNIQUE NONCLUSTERED 
	(
	OrderID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.tblOrderInvoiceCustomer ADD CONSTRAINT
	FK_OrderInvoiceCustomer_SettlementBatch FOREIGN KEY
	(
	BatchID
	) REFERENCES dbo.tblCustomerSettlementBatch
	(
	ID
	) ON UPDATE  CASCADE 
	 ON DELETE  SET NULL 
	
GO
ALTER TABLE dbo.tblOrderInvoiceCustomer ADD CONSTRAINT
	FK_OrderInvoiceCustomer_SettlementFactor FOREIGN KEY
	(
	SettlementFactorID
	) REFERENCES dbo.tblSettlementFactor
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderInvoiceCustomer ADD CONSTRAINT
	FK_OrderInvoiceCustomer_Uom FOREIGN KEY
	(
	UomID
	) REFERENCES dbo.tblUom
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderInvoiceCustomer ADD CONSTRAINT
	FK_tblOrderInvoiceCustomer_tblOrder FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.tblOrder
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	
GO
ALTER TABLE dbo.tblOrderInvoiceCustomer ADD CONSTRAINT
	FK_OrderInvoiceCustomer_WaitFeeSubUnitID FOREIGN KEY
	(
	WaitFeeSubUnitID
	) REFERENCES dbo.tblWaitFeeSubUnit
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderInvoiceCustomer ADD CONSTRAINT
	FK_OrderInvoiceCustomer_WaitFeeRoundingTypeID FOREIGN KEY
	(
	WaitFeeRoundingTypeID
	) REFERENCES dbo.tblWaitFeeRoundingType
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Carrier "Settlement" $$ values to an Audited order
/***********************************/
ALTER PROCEDURE [dbo].[spProcessCarrierInvoice]
(
  @ID int
, @UserName varchar(100)
, @ChainupFee smallmoney = NULL
, @WaitFee smallmoney = NULL
, @RerouteFee smallmoney = NULL
, @RejectionFee smallmoney = NULL
, @H2SFee smallmoney = NULL
, @LoadFee smallmoney = NULL
) AS BEGIN
	DELETE FROM tblOrderInvoiceCarrier WHERE OrderID = @ID
	DECLARE @ReportUomID int
	SELECT @ReportUomID = Value FROM tblSetting WHERE ID = 16
	
	-- all Units and Rates are first normalized to GALLONS UOM then processed consistently in Gallons
	-- but then converted to the System.ReportUomID setting UOM units for saving in the Invoice|Settlement record (for display purposes)
	INSERT INTO tblOrderInvoiceCarrier (OrderID
		, ChainupFee, RerouteFee, WaitFeeSubUnitID, WaitFeeRoundingTypeID, BillableWaitMinutes, WaitRate, WaitFee
		, UomID, MinSettlementUnits, Units
		, RouteRate, H2SRate, TaxRate
		, RejectionFee, LoadFee, H2SFee, FuelSurcharge
		, TotalFee, CreateDateUTC, CreatedByUser)
	SELECT D.ID
		, ChainupFee, RerouteFee, WaitFeeSubUnitID, WaitFeeRoundingTypeID, BillableWaitMinutes, WaitRate, WaitFee
		-- normalize all of these values to the current @ReportUomID UNIT OF MEASURE (for reporting purposes)
		, @ReportUomID, dbo.fnConvertUOM(MinSettlementGallons, 2, @ReportUomID), dbo.fnConvertUOM(ActualGallons, 2, @ReportUomID)
		, dbo.fnConvertRateUOM(RouteRate, 2, @ReportUomID), dbo.fnConvertRateUOM(H2SRate, 2, @ReportUomID), TaxRate
		, RejectionFee, LoadFee, H2SFee, FuelSurcharge
		, RejectionFee + ChainupFee + RerouteFee + WaitFee + H2SFee + LoadFee AS TotalFee
		, GETUTCDATE(), @UserName
	FROM (
		-- compute the actual fees or use the manual overide fee values (if provided)
		SELECT ID
			, coalesce(@ChainupFee, Chainup * ChainupFee, 0) AS ChainupFee
			, coalesce(@RerouteFee, RerouteFee * RerouteCount, 0) AS RerouteFee
			, WaitFeeSubUnitID
			, WaitFeeRoundingTypeID
			, cast(round(BillableWaitHours * 60, 0) as int) AS BillableWaitMinutes
			, WaitFee AS WaitRate
			, coalesce(@WaitFee, BillableWaitHours * WaitFee, 0) AS WaitFee
			, coalesce(@RejectionFee, Rejected * RejectionFee, 0) AS RejectionFee
			, H2SRate
			, MinSettlementGallons
			, ActualGallons
			, coalesce(@H2SFee, H2S * dbo.fnMaxDecimal(MinSettlementGallons, ActualGallons) * H2SRate, 0) AS H2SFee
			, isnull(TaxRate, 0) AS TaxRate
			, isnull(RouteRate, 0) AS RouteRate
			-- if rejected, use 0 AS the Rate
			, round(coalesce(@LoadFee, (1 - Rejected) * dbo.fnMaxDecimal(MinSettlementGallons, ActualGallons) * RouteRate, 0), 4) AS LoadFee
			, isnull(FuelSurcharge, 0) AS FuelSurcharge
		FROM (
			-- normalize the Accessorial Rates to GALLONS + add normalized Route Rate and other time normalization
			SELECT S.ID
				, S.ChainUp
				, CR.ChainupFee
				, CR.RerouteFee
				, S.RerouteCount
				, S.TotalWaitMinutes
				, CR.WaitFeeSubUnitID
				, CR.WaitFeeRoundingTypeID
				, dbo.fnComputeBillableWaitHours(S.TotalWaitMinutes, CR.WaitFeeSubUnitID, CR.WaitFeeRoundingTypeID) AS BillableWaitHours
				, CR.WaitFee
				, S.Rejected
				, CR.RejectionFee
				, S.H2S
				-- normalize the Origin.UOM H2SRate for "Gallons" UOM
				, dbo.fnConvertRateUOM(isnull(S.H2S * CR.H2SRate, 0), OriginUomID, 2) AS H2SRate
				, S.TaxRate
				-- normalize the Origin.UOM Route Rate for "Gallons" UOM
				, dbo.fnConvertRateUom(dbo.fnCarrierRouteRate(S.CarrierID, S.RouteID, S.OrderDate), OriginUomID, 2) AS RouteRate
				, S.MinSettlementGallons
				, isnull(S.ActualGallons, 0) AS ActualGallons
				, CR.FuelSurcharge
			FROM (
				-- get the Order raw data (with Units Normalized to GALLONS) and matching Carrier Accessorial Rate ID
				SELECT O.ID
					, O.CarrierID
					, O.RouteID
					, O.ChainUp
					-- normalize the Order.OriginUOM ActualUnits for "Gallons" UOM
					, dbo.fnConvertUOM(CASE WHEN C.SettlementFactorID = 1 THEN O.OriginGrossUnits ELSE O.OriginNetUnits END, O.OriginUomID, 2) AS ActualGallons
					, O.OriginUomID
					, O.RerouteCount
					, O.TotalWaitMinutes
					, O.Rejected
					, OO.H2S
					, O.OrderDate
					, coalesce(CRXX.ID, CRXA.ID, CRAX.ID, CRAA.ID) AS CRID
					-- normalize the Origin.UOM MinSettlementUnits for "Gallons" UOM
					, isnull(dbo.fnConvertUOM(C.MinSettlementUnits, C.MinSettlementUomID, 2), 0) AS MinSettlementGallons
					, OO.TaxRate
				FROM dbo.viewOrderExportFull O
				JOIN tblOrigin OO ON OO.ID = O.OriginID
				JOIN tblCarrier C ON C.ID = O.CarrierID
				LEFT JOIN tblCarrierRates CRXX ON CRXX.CarrierID = O.CarrierID AND CRXX.RegionID = OO.RegionID
				LEFT JOIN tblCarrierRates CRXA ON CRXA.CarrierID = O.CarrierID AND CRXA.RegionID = -1
				LEFT JOIN tblCarrierRates CRAX ON CRAX.CarrierID = -1 AND CRAX.RegionID = OO.RegionID
				LEFT JOIN tblCarrierRates CRAA ON CRAA.CarrierID = -1 AND CRAA.RegionID = -1
				WHERE O.ID = @ID
			) S
			LEFT JOIN tblCarrierRates CR ON CR.ID = S.CRID
		) SS
	) D
END

GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Customer "Settlement" $$ values to an Audited order
/***********************************/
ALTER PROCEDURE [dbo].[spProcessCustomerInvoice]
(
  @ID int
, @UserName varchar(100)
, @ChainupFee smallmoney = NULL
, @WaitFee smallmoney = NULL
, @RerouteFee smallmoney = NULL
, @RejectionFee smallmoney = NULL
, @H2SFee smallmoney = NULL
, @LoadFee smallmoney = NULL
) AS BEGIN
	DELETE FROM tblOrderInvoiceCustomer WHERE OrderID = @ID
	DECLARE @ReportUomID int
	SELECT @ReportUomID = Value FROM tblSetting WHERE ID = 16
	
	-- all Units and Rates are first normalized to GALLONS UOM then processed consistently in Gallons
	-- but then converted to the System.ReportUomID setting UOM units for saving in the Invoice|Settlement record (for display purposes)
	INSERT INTO tblOrderInvoiceCustomer (OrderID
		, ChainupFee, RerouteFee, WaitFeeSubUnitID, WaitFeeRoundingTypeID, BillableWaitMinutes, WaitRate, WaitFee
		, UomID, MinSettlementUnits, Units
		, RouteRate, H2SRate, TaxRate
		, RejectionFee, LoadFee, H2SFee, FuelSurcharge
		, TotalFee, CreateDateUTC, CreatedByUser)
	SELECT D.ID
		, ChainupFee, RerouteFee, WaitFeeSubUnitID, WaitFeeRoundingTypeID, BillableWaitMinutes, WaitRate, WaitFee
		-- normalize all of these values to the current @ReportUomID UNIT OF MEASURE (for reporting purposes)
		, @ReportUomID, dbo.fnConvertUOM(MinSettlementGallons, 2, @ReportUomID), dbo.fnConvertUOM(ActualGallons, 2, @ReportUomID)
		, dbo.fnConvertRateUOM(RouteRate, 2, @ReportUomID), dbo.fnConvertRateUOM(H2SRate, 2, @ReportUomID), TaxRate
		, RejectionFee, LoadFee, H2SFee, FuelSurcharge
		, RejectionFee + ChainupFee + RerouteFee + WaitFee + H2SFee + LoadFee AS TotalFee
		, GETUTCDATE(), @UserName
	FROM (
		-- compute the actual fees or use the manual overide fee values (if provided)
		SELECT ID
			, coalesce(@ChainupFee, Chainup * ChainupFee, 0) AS ChainupFee
			, coalesce(@RerouteFee, RerouteFee * RerouteCount, 0) AS RerouteFee
			, WaitFeeSubUnitID
			, WaitFeeRoundingTypeID
			, cast(round(BillableWaitHours * 60, 0) as int) AS BillableWaitMinutes
			, WaitFee AS WaitRate
			, coalesce(@WaitFee, BillableWaitHours * WaitFee, 0) AS WaitFee
			, coalesce(@RejectionFee, Rejected * RejectionFee, 0) AS RejectionFee
			, H2SRate
			, MinSettlementGallons
			, ActualGallons
			, coalesce(@H2SFee, H2S * dbo.fnMaxDecimal(MinSettlementGallons, ActualGallons) * H2SRate, 0) AS H2SFee
			, isnull(TaxRate, 0) AS TaxRate
			, isnull(RouteRate, 0) AS RouteRate
			-- if rejected, use 0 AS the Rate
			, round(coalesce(@LoadFee, (1 - Rejected) * dbo.fnMaxDecimal(MinSettlementGallons, ActualGallons) * RouteRate, 0), 4) AS LoadFee
			, isnull(FuelSurcharge, 0) AS FuelSurcharge
		FROM (
			-- normalize the Accessorial Rates to GALLONS + add normalized Route Rate and other time normalization
			SELECT S.ID
				, S.ChainUp
				, CR.ChainupFee
				, CR.RerouteFee
				, S.RerouteCount
				, S.TotalWaitMinutes
				, CR.WaitFeeSubUnitID
				, CR.WaitFeeRoundingTypeID
				, dbo.fnComputeBillableWaitHours(S.TotalWaitMinutes, CR.WaitFeeSubUnitID, CR.WaitFeeRoundingTypeID) AS BillableWaitHours
				, CR.WaitFee
				, S.Rejected
				, CR.RejectionFee
				, S.H2S
				-- normalize the Origin.UOM H2SRate for "Gallons" UOM
				, dbo.fnConvertRateUOM(isnull(S.H2S * CR.H2SRate, 0), OriginUomID, 2) AS H2SRate
				, S.TaxRate
				-- normalize the Origin.UOM Route Rate for "Gallons" UOM
				, dbo.fnConvertRateUom(dbo.fnCustomerRouteRate(S.CustomerID, S.RouteID, S.OrderDate), OriginUomID, 2) AS RouteRate
				, S.MinSettlementGallons
				, isnull(S.ActualGallons, 0) AS ActualGallons
				, CR.FuelSurcharge
			FROM (
				-- get the Order raw data (with Units Normalized to GALLONS) and matching Customer Accessorial Rate ID
				SELECT O.ID
					, O.CustomerID
					, O.RouteID
					, O.ChainUp
					-- normalize the Order.OriginUOM ActualUnits for "Gallons" UOM
					, dbo.fnConvertUOM(CASE WHEN C.SettlementFactorID = 1 THEN O.OriginGrossUnits ELSE O.OriginNetUnits END, O.OriginUomID, 2) AS ActualGallons
					, O.OriginUomID
					, O.RerouteCount
					, O.TotalWaitMinutes
					, O.Rejected
					, OO.H2S
					, O.OrderDate
					, coalesce(CRXX.ID, CRXA.ID, CRAX.ID, CRAA.ID) AS CRID
					-- normalize the Origin.UOM MinSettlementUnits for "Gallons" UOM
					, isnull(dbo.fnConvertUOM(C.MinSettlementUnits, C.MinSettlementUomID, 2), 0) AS MinSettlementGallons
					, OO.TaxRate
				FROM dbo.viewOrderExportFull O
				JOIN tblOrigin OO ON OO.ID = O.OriginID
				JOIN tblCustomer C ON C.ID = O.CustomerID
				LEFT JOIN tblCustomerRates CRXX ON CRXX.CustomerID = O.CustomerID AND CRXX.RegionID = OO.RegionID
				LEFT JOIN tblCustomerRates CRXA ON CRXA.CustomerID = O.CustomerID AND CRXA.RegionID = -1
				LEFT JOIN tblCustomerRates CRAX ON CRAX.CustomerID = -1 AND CRAX.RegionID = OO.RegionID
				LEFT JOIN tblCustomerRates CRAA ON CRAA.CustomerID = -1 AND CRAA.RegionID = -1
				WHERE O.ID = @ID
			) S
			LEFT JOIN tblCustomerRates CR ON CR.ID = S.CRID
		) SS
	) D
END

GO

/*************************************************************
** Date Created: 26 Nov 2013
** Author: Kevin Alons
** Purpose: convert any Units Qty into a Gallons Qty
*************************************************************/
ALTER FUNCTION [dbo].[fnConvertUOM](@units decimal(18, 10), @fromUomID int, @toUomID int) RETURNS decimal(18,10) AS BEGIN
	DECLARE @ret decimal(18,10)
	IF (@fromUomID = @toUomID OR @units = 0)
		SELECT @ret = @units
	ELSE
		SELECT @ret = (
			SELECT @units * GallonEquivalent FROM tblUom WHERE ID=@fromUomID
		) / GallonEquivalent FROM tblUom WHERE ID=@toUomID
	RETURN (@ret)
END

GO

/*************************************************************
** Date Created: 7 Dec 2013
** Author: Kevin Alons
** Purpose: convert any RATE from/to a UomID
*************************************************************/
ALTER FUNCTION [dbo].[fnConvertRateUOM](@units decimal(18,10), @fromUomID int, @toUomID int) RETURNS decimal(18,10) AS BEGIN
	-- note that RATE conversions are INVERTED (switched TO<->FROM)
	RETURN (SELECT dbo.fnConvertUOM(@units, @toUomID, @fromUomID))
END

GO

COMMIT
SET NOEXEC OFF