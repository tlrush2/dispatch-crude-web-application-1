SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.1.16'
SELECT  @NewVersion = '4.1.17'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1818 - Remove Print Status from front end'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


DELETE FROM aspnet_UsersInRoles
WHERE RoleId = (SELECT RoleId FROM aspnet_Roles WHERE RoleName = 'editOrderPrintStatus')
GO

DELETE FROM aspnet_RolesInGroups
WHERE RoleId = (SELECT RoleId FROM aspnet_Roles WHERE RoleName = 'editOrderPrintStatus')
GO

DELETE FROM aspnet_Roles
WHERE RoleId = (SELECT RoleId FROM aspnet_Roles WHERE RoleName = 'editOrderPrintStatus')
GO

COMMIT
SET NOEXEC OFF