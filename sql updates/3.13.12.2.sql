SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20) = '3.13.12.1'
	, @NewVersion varchar(20) = '3.13.12.2'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Import Center - Add recently added fields as Importable in Import Center'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

-- generated using : _spAddNewObjectFields (run this after you add any fields to the system to make them available to the Import Center)
SET IDENTITY_INSERT tblObjectField ON
INSERT INTO tblObjectField (ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, AllowNullID, IsKey, IsCustom, ParentObjectID, ParentObjectIDFieldName)
SELECT 416,'1','ContractNumber','Contract Number','1',NULL,'1','0','0',NULL,NULL
INSERT INTO tblObjectField (ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, AllowNullID, IsKey, IsCustom, ParentObjectID, ParentObjectIDFieldName)
SELECT 417,'1','JobNumber','Job Number','1',NULL,'1','0','0',NULL,NULL
INSERT INTO tblObjectField (ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, AllowNullID, IsKey, IsCustom, ParentObjectID, ParentObjectIDFieldName)
SELECT 418,'1','OriginSulfurContent','Origin Sulfur Content','4',NULL,'1','0','0',NULL,NULL
INSERT INTO tblObjectField (ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, AllowNullID, IsKey, IsCustom, ParentObjectID, ParentObjectIDFieldName)
SELECT 419,'2','ScaleTicketNum','Scale Ticket Num','1',NULL,'1','0','0',NULL,NULL
INSERT INTO tblObjectField (ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, AllowNullID, IsKey, IsCustom, ParentObjectID, ParentObjectIDFieldName)
SELECT 420,'14','QRCode','QR Code','8','(newid())','1','0','0',NULL,NULL
INSERT INTO tblObjectField (ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, AllowNullID, IsKey, IsCustom, ParentObjectID, ParentObjectIDFieldName)
SELECT 421,'15','QRCode','QR Code','8','(newid())','1','0','0',NULL,NULL
INSERT INTO tblObjectField (ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, AllowNullID, IsKey, IsCustom, ParentObjectID, ParentObjectIDFieldName)
SELECT 422,'19','SulfurContent','Sulfur Content','4',NULL,'1','0','0',NULL,NULL
INSERT INTO tblObjectField (ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, AllowNullID, IsKey, IsCustom, ParentObjectID, ParentObjectIDFieldName)
SELECT 423,'30','AltTankNum','Alt Tank Num','1',NULL,'1','0','0',NULL,NULL
INSERT INTO tblObjectField (ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, AllowNullID, IsKey, IsCustom, ParentObjectID, ParentObjectIDFieldName)
SELECT 424,'38','SignificantDigits','Significant Digits','3','((0))','1','0','0',NULL,NULL
SET IDENTITY_INSERT tblObjectField OFF

COMMIT
SET NOEXEC OFF