SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.6.7'
SELECT  @NewVersion = '4.6.8'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'JT-2569 - Allow HOS edits (proposed changes) from website'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/***********************************************************************
AUTHOR		:	Joseph Engler
CREATED		:	2017-04-24 (4.6.8)
DESCRIPTION :	Performs bitwise right-shift: y = x >> s
					Use binary size to mask the result
************************************************************************/
CREATE FUNCTION fnShiftLeft(@x INT, @s INT, @binarySize INT = 2) RETURNS INT
AS
BEGIN
	RETURN CAST(CAST(@x * 		-- 2^31 will overflow, cast to BIGINT
		POWER(CAST(2 AS BIGINT), @s & 0x1F) -- Wrap around 31
		AS BINARY(4)) AS INT) & (POWER(2, 8*@binarySize)-1)
END
GO

/***********************************************************************
AUTHOR		:	Joseph Engler
CREATED		:	2017-04-24 (4.6.8)
DESCRIPTION :	Performs bitwise right-shift with sign-extension: y = x >> s
					Use binary size to mask the result
************************************************************************/
CREATE FUNCTION fnShiftRight(@x INT, @s INT, @binarySize INT = 2) RETURNS INT
AS
BEGIN
	RETURN CAST((@x & (POWER(2, 8*@binarySize)-1)) / -- 2^31 will overflow, cast to BIGINT
		POWER(CAST(2 AS BIGINT), @s & 0x1F) -- Wrap around 31
		AS INT) & (POWER(2, 8*@binarySize)-1)
END
GO


/***********************************************************************
AUTHOR		:	Joseph Engler
CREATED		:	2017-04-24 (4.6.8)
DESCRIPTION :	Performs bitwise circular left-shift
					Use binary size to mask the result
************************************************************************/
CREATE FUNCTION fnRotateLeft(@x INT, @s INT, @binarySize INT = 2) RETURNS INT
AS
BEGIN
	RETURN (dbo.fnShiftLeft(@x, @s, @binarySize) | dbo.fnShiftRight(@x, (8*@binarySize - @s), @binarySize))
				& (POWER(2, 8*@binarySize) - 1)
END
GO


/***********************************************************************
AUTHOR		:	Joseph Engler
CREATED		:	2017-04-24 (4.6.8)
DESCRIPTION :	Performs bitwise circular right-shift: y = x >> s
					Use binary size to mask the result
************************************************************************/
CREATE FUNCTION fnRotateRight(@x INT, @s INT, @binarySize INT = 2) RETURNS INT
AS
BEGIN
	RETURN (dbo.fnShiftRight(@x, @s, @binarySize) | dbo.fnShiftLeft(@x, (8*@binarySize - @s), @binarySize))
				& (POWER(2, 8*@binarySize)-1)
END
GO



/****************************************** 
** Date Created:     2016/07/15 
** Author:               Joe Engler 
** Purpose:               Return the historical HOS entries for a driver, calculate the TimeInState for ease of computation on mobile side 
** Changes: 
--			4.2.5			JAE			2016-10-26			Use operating state (instead of state) to carrier rule call 
--			4.4.15			JAE			2017-01-06			Use policy (not carrier rule) to get days to keep 
--			4.5.0			BSB			2017-01-26			Add Terminal 
--			4.6.0			JAE			2017-03-23			Removed reference to delete fields, allowed text for date 
--															What about null driver? need to use eld device to push unidentified for that device 
--          4.6.8			JAE			2017-05-05			Pass varbinary as string (cast) 
--			4.6.8			BSB			2017-05-05			Add back missing columns to selects (CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, LogTimeZoneID)
--			4.6.8			BSB	& JAE	2017-05-11			Eliminate Type entries <> 1 from time calulations, Include Type entries <> 1 in "send back"
******************************************/ 
ALTER PROCEDURE spDriverHOSLog(@DriverID INT, @LastSyncDate DATETIME) AS 
BEGIN 
	DECLARE @DaysToKeep INT =  
				(SELECT DriverAppLogRetentionDays  
				FROM tblHosPolicy  
				WHERE ID = COALESCE( 
						(SELECT ID  
						FROM tblHosPolicy  
						WHERE Name = (SELECT R.Value 
									 FROM tblDriver D  
									 CROSS APPLY dbo.fnCarrierRules(GETDATE(), NULL, 1, D.ID, D.CarrierID, D.TerminalID, D.OperatingStateID, D.RegionID, 1) R  
									 WHERE D.ID = @DriverID)), 1) -- Use default policy if none set 
				) 
 
	DECLARE @StartDate DATETIME = DATEADD(DAY, - @DaysToKeep, GETUTCDATE()); 
 
	WITH rows AS 
	( 
		-- Dummy start record, gets the previous record prior to the start date 
		SELECT 0 AS rownum
			, * 
		FROM tblHos 
		WHERE ID = (SELECT TOP 1 ID  
					FROM tblHos  
					WHERE ISNULL(DriverID, -1) = ISNULL(@DriverID, -1)  
						AND LogDateUTC < @StartDate 
						AND EventRecordStatusID = 1 
						AND HosEventTypeID = 1 
					ORDER BY LogDateUTC DESC)  
 
		UNION 
 
		SELECT ROW_NUMBER() OVER (ORDER BY LogDateUTC) AS rownum 
			, * 
		FROM tblHos 
		WHERE ISNULL(DriverID, -1) = ISNULL(@DriverID, -1) 
			AND LogDateUTC BETWEEN @StartDate AND GETUTCDATE() 
			AND EventRecordStatusID = 1 
			AND HosEventTypeID = 1 
 
		UNION 
 
		-- Dummy end record, gets the final record again so it will show in the summary list (with TimeInState = 0) 
		SELECT (SELECT COUNT(*) + 1  
				FROM tblHos  
				WHERE ISNULL(DriverID, -1) = ISNULL(@DriverID, -1)  
					AND LogDateUTC BETWEEN @StartDate AND GETUTCDATE() 
					AND EventRecordStatusID = 1
					AND HosEventTypeID = 1 ) AS rownum 
				, * 
		FROM tblHos 
		WHERE ISNULL(DriverID, - 1) = ISNULL(@DriverID, - 1) 
			AND ID = (SELECT TOP 1 ID  
					 FROM tblHos  
				  	 WHERE ISNULL(DriverID, -1) = ISNULL(@DriverID, -1)  
						AND LogDateUTC <= GETUTCDATE() 
						AND EventRecordStatusID = 1 
						AND HosEventTypeID = 1 
					 ORDER BY LogDateUTC DESC) 
     ) 
     SELECT MC.rownum
		, MC.ID
		, MC.UID
		, MC.DriverID
		, MC.HosDriverStatusID
		, MC.Lat
		, MC.Lon
		, MC.PersonalUse
		, MC.YardMove
		, MC.LogDateUTC
		, MC.CreateDateUTC
		, MC.CreatedByUser
		, MC.LastChangeDateUTC
		, MC.LastChangedByUser
		, MC.LogTimeZoneID
		, MC.Notes
		, MC.ELDUserName
		, MC.DriverTimeZoneOffset
		, MC.CoDriverID
		, MC.HosEventTypeID
		, MC.HosEventCode
		, MC.DistanceSinceLastValidCoordinates
		, EventDataCheckValue = CONVERT(VARCHAR(10), MC.EventDataCheckValue, 2)
		, MC.EventRecordOriginID
		, MC.EventRecordStatusID
		, MC.EventSequenceNumber
		, MC.LocationDescription
		, MC.Geolocation
		, MC.VIN
		, MC.EngineHours
		, MC.VehicleMiles
		, MC.DataDiagnosticEventIndicator
		, MC.MalfunctionIndicator
		, MC.DiagnosticCodeID
		, MC.ELDIdentifier
		, MC.ELDAuthenticationValue
		, MC.HosParentUID
		, MC.HosSignatureUID
		, CASE WHEN MC.ID = MP.ID THEN NULL -- send null time in state for current HOS (repeated at end)  
                    ELSE DATEDIFF(MINUTE, MC.LogDateUTC, MP.LogDateUTC)  
                    END AS TimeInState 
     FROM rows MC 
		JOIN rows MP ON MC.rownum = MP.rownum - 1 
		CROSS JOIN fnSyncLCDOffset(@LastSyncDate) LCD 
		-- BSB - Commented out on 4/14/17 per Joe - REASON: Removing the where clause allows duration to update (mobile HOS) for any record preceding an edited/added record 
     --WHERE @LastSyncDate IS NULL  
     --     OR MC.CreateDateUTC >= LCD.LCD -- use offset to get nearest city updates for records just sent to the server 
     --     OR MC.LastChangeDateUTC >= LCD.LCD 
 
     UNION 
 
     -- include deleted records if relevant 
     SELECT NULL
		, ID
		, UID
		, DriverID
		, HosDriverStatusID
		, Lat
		, Lon
		, PersonalUse
		, YardMove
		, LogDateUTC
		, CreateDateUTC
		, CreatedByUser
		, LastChangeDateUTC
		, LastChangedByUser
		, LogTimeZoneID
		, Notes
		, ELDUserName
		, DriverTimeZoneOffset
		, CoDriverID
		, HosEventTypeID
		, HosEventCode
		, DistanceSinceLastValidCoordinates
		, EventDataCheckValue = CONVERT(VARCHAR(10), h.EventDataCheckValue, 2)
		, EventRecordOriginID
		, EventRecordStatusID
		, EventSequenceNumber
		, LocationDescription
		, Geolocation
		, VIN
		, EngineHours
		, VehicleMiles
		, DataDiagnosticEventIndicator
		, MalfunctionIndicator
		, DiagnosticCodeID
		, ELDIdentifier
		, ELDAuthenticationValue
		, HosParentUID
		, HosSignatureUID		
		, NULL 
     FROM tblHos h 
     CROSS JOIN fnSyncLCDOffset(@LastSyncDate) LCD 
 
     WHERE ISNULL(DriverID, -1) = ISNULL(@DriverID, -1) 
       AND (EventRecordStatusID <> 1 OR HosEventTypeID <> 1)
       AND (@LastSyncDate IS NULL  
               OR CreateDateUTC >= LCD.LCD -- use offset to get nearest city updates for records just sent to the server 
               OR LastChangeDateUTC >= LCD.LCD) 
 
     ORDER BY LogDateUTC 
END  
GO



COMMIT
SET NOEXEC OFF