SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.10.13.4'
SELECT  @NewVersion = '3.10.13.5'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Truck Type - Update settlement scripts'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


-- PART 6
-- UPDATE THE CORE SETTLEMENT SCRIPTS
--------------------------------------------------------------------------------------------

-- UPDATE CARRIER FUNCTIONS/STORED PROCEDURES


/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier AssessorialRate info for the specified order
-- Changes:
	- 3.7.28 - 2015/06/18 - KDA - add new columns: move ProducerID parameter to end (so consistent with all other Best-Match rates/parameters
								 - add DriverID/DriverGroup parameters
	- 3.7.30 - 2015/06/19 - KDA - remove DriverID							 
	- 3.10.13.5 - 2016/02/29 - JAE - Add truck type
***********************************/
ALTER FUNCTION [dbo].[fnCarrierAssessorialRates]
(
  @StartDate date
, @EndDate date
, @TypeID int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @TruckTypeID int
, @DriverGroupID int
, @OriginID int
, @DestinationID int
, @OriginStateID int
, @DestStateID int
, @RegionID int
, @ProducerID int
, @BestMatchOnly bit = 0
)
RETURNS 
	@ret TABLE (
		ID int
	  , TypeID int
	  , ShipperID int
	  , CarrierID int
	  , ProductGroupID int
	  , TruckTypeID int
	  , DriverGroupID int
	  , OriginID int
	  , DestinationID int
	  , OriginStateID int
	  , DestStateID int
	  , RegionID int
	  , ProducerID int
	  , Rate decimal(18, 10)
	  , RateTypeID int
	  , UomID int
	  , EffectiveDate date
	  , EndDate date
	  , MaxEffectiveDate date
	  , MinEndDate date
	  , NextEffectiveDate date
	  , PriorEndDate date	  
	  , BestMatch bit
	  , Ranking smallmoney
	  , Locked bit
	  , CreateDateUTC datetime
	  , CreatedByUser varchar(100)
	  , LastChangeDateUTC datetime
	  , LastChangedByUser varchar(100)
	)
AS BEGIN
	-- get the raw data (all matched rows)
	DECLARE @src TABLE (ID int, TypeID int, Ranking smallmoney)
	INSERT INTO @src (ID, TypeID, Ranking)
		SELECT ID, TypeID
			, Ranking =	dbo.fnRateRanking(@TruckTypeID, R.TruckTypeID, 1024, 0)
					  + dbo.fnRateRanking(@ShipperID, R.ShipperID, 512, 0)
					  + dbo.fnRateRanking(@CarrierID, R.CarrierID, 256, 0)
					  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 128, 0)
					  + dbo.fnRateRanking(@DriverGroupID, R.DriverGroupID, 64, 1)
					  + dbo.fnRateRanking(@OriginID, R.OriginID, 32, 0)
					  + dbo.fnRateRanking(@DestinationID, R.DestinationID, 16, 0)
					  + dbo.fnRateRanking(@OriginStateID, R.OriginStateID, 8, 0)
					  + dbo.fnRateRanking(@DestStateID, R.DestStateID, 4, 0)
					  + dbo.fnRateRanking(@RegionID, R.RegionID, 2, 0)
					  + dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 1)
		FROM dbo.viewCarrierAssessorialRate R
		WHERE coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(R.ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@TruckTypeID, 0), R.TruckTypeID, 0) = coalesce(R.TruckTypeID, nullif(@TruckTypeID, 0), 0)
		  AND coalesce(nullif(@DriverGroupID, 0), R.DriverGroupID, 0) = coalesce(DriverGroupID, nullif(@DriverGroupID, 0), 0)
		  AND coalesce(nullif(@OriginID, 0), R.OriginID, 0) = coalesce(R.OriginID, nullif(@OriginID, 0), 0)
		  AND coalesce(nullif(@DestinationID, 0), R.DestinationID, 0) = coalesce(R.DestinationID, nullif(@DestinationID, 0), 0)
		  AND coalesce(nullif(@OriginStateID, 0), R.OriginStateID, 0) = coalesce(R.OriginStateID, nullif(@OriginStateID, 0), 0)
		  AND coalesce(nullif(@DestStateID, 0), R.DestStateID, 0) = coalesce(R.DestStateID, nullif(@DestStateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(R.RegionID, nullif(@RegionID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(R.ProducerID, nullif(@ProducerID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
		  AND (nullif(@TypeID, 0) IS NULL OR TypeID = @TypeID)

	-- return the rate data for the best-match rates 
	INSERT INTO @ret (ID, TypeID, ShipperID, CarrierID, ProductGroupID, TruckTypeID, DriverGroupID, OriginID, DestinationID, OriginStateID, DestStateID, RegionID, ProducerID, Rate, RateTypeID, UomID, EffectiveDate, EndDate
			, MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate, BestMatch, Ranking, Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser)
		SELECT CAR.ID, TypeID, ShipperID, CarrierID, ProductGroupID, TruckTypeID, DriverGroupID, OriginID, DestinationID, OriginStateID, DestStateID, RegionID, ProducerID, Rate, RateTypeID, UomID, EffectiveDate, EndDate
			, MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate, BestMatch, Ranking, Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser
		FROM viewCarrierAssessorialRate CAR
		JOIN (
			SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.TypeID IS NULL THEN 0 ELSE 1 END as bit)
			FROM @src S
			LEFT JOIN (
				SELECT TypeID, Ranking = MAX(Ranking)
				FROM @src
				WHERE @StartDate = ISNULL(@EndDate, @StartDate)
				  AND Ranking % 1 = 0.01 * 10  -- ensure some type of match occurred on all criteria choices
				GROUP BY TypeID
			) X ON X.TypeID = S.TypeID AND X.Ranking = S.Ranking
		) X ON X.ID = CAR.ID
		WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
		
	RETURN
END

GO


/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier AssessorialRate rows for the specified criteria
-- Changes:
	- 3.7.28 - 2015/06/18 - KDA - add new columns: move ProducerID parameter to end (so consistent with all other Best-Match rates/parameters
								 - add DriverID/DriverGroup parameters
	- 3.7.30 - 2015/06/19 - KDA - remove DriverID							 
	- 3.10.13.5 - 2016/02/29 - JAE - Add truck type
**********************************/
ALTER FUNCTION [dbo].[fnCarrierAssessorialRatesDisplay]
(  
  @StartDate date
, @EndDate date
, @TypeID int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @TruckTypeID int
, @DriverGroupID int
, @OriginID int
, @DestinationID int
, @OriginStateID int
, @DestStateID int
, @RegionID int
, @ProducerID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.TruckTypeID, R.DriverGroupID, R.OriginID, R.DestinationID, R.OriginStateID, R.DestStateID, R.RegionID, R.ProducerID, R.TypeID, R.Rate, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Type = RT.Name
		, Shipper = S.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, TruckType = TT.Name
		, DriverGroup = DG.Name
		, Origin = O.Name
		, OriginFull = O.FullName
		, Destination = D.Name
		, DestinationFull = D.FullName
		, OriginState = OS.FullName
		, OriginStateAbbrev = OS.Abbreviation
		, DestState = DS.FullName
		, DestStateAbbrev = DS.Abbreviation
		, Region = RE.Name
		, Producer = PR.Name
		, RateType = RT.Name
		, Uom = U.Name
		, UomShort = U.Abbrev
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch, Ranking
	FROM dbo.fnCarrierAssessorialRates(@StartDate, @EndDate, @TypeID, @ShipperID, @CarrierID, @ProductGroupID, @TruckTypeID, @DriverGroupID, @OriginID, @DestinationID, @OriginStateID, @DestStateID, @RegionID, @ProducerID, 0) R
	JOIN tblAssessorialRateType RT ON RT.ID = R.TypeID
	LEFT JOIN tblCustomer S ON S.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblTruckType TT ON TT.ID = R.TruckTypeID
	LEFT JOIN tblDriverGroup DG ON DG.ID = R.DriverGroupID
	LEFT JOIN viewOrigin O ON O.ID = R.OriginID
	LEFT JOIN viewDestination D ON D.ID = R.DestinationID
	LEFT JOIN tblState OS ON OS.ID = R.OriginStateID
	LEFT JOIN tblState DS ON DS.ID = R.DestStateID
	LEFT JOIN tblRegion RE ON RE.ID = R.RegionID
	LEFT JOIN tblProducer PR ON PR.ID = R.ProducerID
	LEFT JOIN tblUom U ON U.ID = R.UomID
	ORDER BY EffectiveDate

GO


/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier DestinationWaitRate info for the specified criteria
-- Changes:
	- 3.7.28 - 2015/06/13 - KDA - Add DriverID | DriverGroupID | ProducerID
	- 3.7.30 - 2015/06/19 - KDA - remove DriverID
	- 3.10.13.5 - 2016/02/29 - JAE - Add truck type
***********************************/
ALTER FUNCTION [dbo].[fnCarrierDestinationWaitRate]
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @TruckTypeID int
, @DriverGroupID int
, @DestinationID int
, @StateID int
, @RegionID int
, @ProducerID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID, ReasonID
			, Ranking =	dbo.fnRateRanking(@ReasonID, R.ReasonID, 512, 1)
				  + dbo.fnRateRanking(@TruckTypeID, R.TruckTypeID, 256, 1)
				  + dbo.fnRateRanking(@ShipperID, R.ShipperID, 128, 0)
				  + dbo.fnRateRanking(@CarrierID, R.CarrierID, 64, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 32, 0)
				  + dbo.fnRateRanking(@DriverGroupID, R.DriverGroupID, 16, 1)
				  + dbo.fnRateRanking(@DestinationID, R.DestinationID, 8, 0)
				  + dbo.fnRateRanking(@StateID, R.StateID, 4, 0)
				  + dbo.fnRateRanking(@RegionID, R.RegionID, 2, 0)
				  + dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 0)
		FROM  dbo.viewCarrierDestinationWaitRate R
		WHERE coalesce(nullif(@ReasonID, 0), R.ReasonID, 0) = coalesce(ReasonID, nullif(@ReasonID, 0), 0)
		  AND coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@TruckTypeID, 0), R.TruckTypeID, 0) = coalesce(TruckTypeID, nullif(@TruckTypeID, 0), 0)
		  AND coalesce(nullif(@DriverGroupID, 0), R.DriverGroupID, 0) = coalesce(DriverGroupID, nullif(@DriverGroupID, 0), 0)
		  AND coalesce(nullif(@DestinationID, 0), R.DestinationID, 0) = coalesce(DestinationID, nullif(@DestinationID, 0), 0)
		  AND coalesce(nullif(@StateID, 0), R.StateID, 0) = coalesce(StateID, nullif(@StateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(ProducerID, nullif(@ProducerID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	),
	cte2 AS
	(
		SELECT ID, S.ReasonID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT ReasonID = ISNULL(ReasonID, 0), Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 9  -- ensure some type of match occurred on all criteria choices
			GROUP BY ReasonID
		) X ON X.ReasonID = isnull(S.ReasonID, 0) AND X.Ranking = S.Ranking
	), 
	cte3 AS
	(	-- when both ReasonID = NULL AND ReasonID = @ReasonID records are BestMatch = 1, favor the ReasonID = @ReasonID record
		SELECT ID, X.ReasonID, X.Ranking, BestMatch = CASE WHEN BEST.ReasonID IS NULL THEN X.BestMatch ELSE 0 END
		FROM cte2 X
		LEFT JOIN (
			SELECT TOP 1 ReasonID FROM cte2 WHERE ReasonID = @ReasonID AND BestMatch = 1 ORDER BY Ranking DESC
		) BEST ON X.ReasonID IS NULL AND X.BestMatch = 1
	)
	
	SELECT TOP (CASE WHEN @BestMatchOnly = 1 THEN 1 ELSE 10000 END) R.ID, R.ReasonID, ShipperID, CarrierID, ProductGroupID, TruckTypeID, DriverGroupID, DestinationID, StateID, RegionID, ProducerID
		, Rate, EffectiveDate, EndDate
	  , MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate
	  , BestMatch, Ranking
	  , Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewCarrierDestinationWaitRate R
	JOIN cte3 X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
	ORDER BY Ranking DESC
)

GO


/***********************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier DestinationWaitRate rows for the specified criteria
-- Changes:
	- 3.7.28 - 2015/06/13 - KDA - Add DriverID | DriverGroupID | ProducerID
	- 3.7.30 - 2015/06/19 - KDA - remove DriverID
	- 3.10.13.5 - 2016/02/29 - JAE - Add truck type
***********************************/
ALTER FUNCTION [dbo].[fnCarrierDestinationWaitRatesDisplay]
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @TruckTypeID int
, @DriverGroupID int
, @DestinationID int
, @StateID int
, @RegionID int
, @ProducerID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ReasonID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.TruckTypeID, R.DriverGroupID, R.DestinationID, R.StateID, R.RegionID, R.ProducerID, R.Rate, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Shipper = SH.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, TruckType = TT.Name
		, DriverGroup = DG.Name
		, Destination = D.Name
		, DestinationFull = D.FullName
		, State = S.FullName
		, StateAbbrev = S.Abbreviation
		, Region = REG.Name
		, Producer = P.Name
		, Reason = REA.Description
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
		, Ranking
	FROM dbo.fnCarrierDestinationWaitRate(@StartDate, @EndDate, @ReasonID, @ShipperID, @CarrierID, @ProductGroupID, @TruckTypeID, @DriverGroupID, @DestinationID, @StateID, @RegionID, @ProducerID, 0) R
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblTruckType TT ON TT.ID = R.TruckTypeID
	LEFT JOIN tblDriverGroup DG ON DG.ID = R.DriverGroupID
	LEFT JOIN viewDestination D ON D.ID = R.DestinationID
	LEFT JOIN tblState S ON S.ID = R.StateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblProducer P ON P.ID = R.ProducerID
	LEFT JOIN tblDestinationWaitReason REA ON REA.ID = R.ReasonID
	ORDER BY EffectiveDate

GO


/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier DestinationWaitRate info for the specified criteria
-- Changes:
	- 3.7.28 - 2015/06/18 - KDA - add DriverID/DriverGroupID parameters
	- 3.7.30 - 2015/06/19 - KDA - remove DriverID
	- 3.10.13.5 - 2016/02/29 - JAE - Add truck type
***********************************/
ALTER FUNCTION [dbo].[fnCarrierFuelSurchargeRate]
(
  @StartDate date
, @EndDate date
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @TruckTypeID int
, @DriverGroupID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID
			, Ranking =	dbo.fnRateRanking(@TruckTypeID, R.TruckTypeID, 16, 0)
				  + dbo.fnRateRanking(@ShipperID, R.ShipperID, 8, 0)
				  + dbo.fnRateRanking(@CarrierID, R.CarrierID, 4, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 2, 0)
				  + dbo.fnRateRanking(@DriverGroupID, R.DriverGroupID, 1, 1)
		FROM  dbo.viewCarrierFuelSurchargeRate R
		WHERE coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@TruckTypeID, 0), R.TruckTypeID, 0) = coalesce(TruckTypeID, nullif(@TruckTypeID, 0), 0)
		  AND coalesce(nullif(@DriverGroupID, 0), R.DriverGroupID, 0) = coalesce(DriverGroupID, nullif(@DriverGroupID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	)
	SELECT R.ID, ShipperID, CarrierID, ProductGroupID, TruckTypeID, DriverGroupID, FuelPriceFloor, IntervalAmount, IncrementAmount, EffectiveDate, EndDate
	  , MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate
	  , BestMatch, Ranking
	  , Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewCarrierFuelSurchargeRate R
	JOIN (
		SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 4  -- ensure some type of match occurred on all criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)

GO


/***********************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RouteRate rows for the specified criteria
-- Changes:
	- 3.7.28 - 2015/06/18 - KDA - add DriverID/DriverGroupID parameters
	- 3.7.30 - 2015/06/19 - KDA - remove DriverID
	- 3.10.13.5 - 2016/02/29 - JAE - Add truck type
***********************************/
ALTER FUNCTION [dbo].[fnCarrierFuelSurchargeRateDisplay]
(
  @StartDate date
, @EndDate date
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @TruckTypeID int
, @DriverGroupID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.TruckTypeID, R.DriverGroupID, R.FuelPriceFloor, R.IntervalAmount, R.IncrementAmount, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Shipper = S.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, TruckType = TT.Name
		, DriverGroup = DG.Name
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch, Ranking
	FROM dbo.fnCarrierFuelSurchargeRate(@StartDate, @EndDate, @ShipperID, @CarrierID, @ProductGroupID, @TruckTypeID, @DriverGroupID, 0) R
	LEFT JOIN tblCustomer S ON S.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblTruckType TT ON TT.ID = R.TruckTypeID
	LEFT JOIN tblDriverGroup DG ON DG.ID = R.DriverGroupID
	ORDER BY EffectiveDate

GO


/***********************************
-- Date Created: 12 June 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier MinSettlementUnits info for the specified criteria
-- Changes: 
	- 3.7.28 - 2015/06/18 - KDA - ADDED
	- 3.10.13.5 - 2016/02/29 - JAE - Add truck type
***********************************/
ALTER FUNCTION [dbo].[fnCarrierMinSettlementUnits]
(
  @StartDate date
, @EndDate date
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @TruckTypeID int
, @OriginID int
, @OriginStateID int
, @DestinationID int
, @DestinationStateID int
, @ProducerID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID
			, Ranking =	dbo.fnRateRanking(@TruckTypeID, R.TruckTypeID, 256, 0)
				  + dbo.fnRateRanking(@ShipperID, R.ShipperID, 128, 0)
				  + dbo.fnRateRanking(@CarrierID, R.CarrierID, 64, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 32, 0)
				  + dbo.fnRateRanking(@OriginID, R.OriginID, 16, 0)
				  + dbo.fnRateRanking(@OriginStateID, R.OriginStateID, 8, 0)
				  + dbo.fnRateRanking(@DestinationID, R.DestinationID, 4, 0)
				  + dbo.fnRateRanking(@DestinationStateID, R.DestinationStateID, 2, 0)
				  + dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 1)
		FROM  dbo.viewCarrierMinSettlementUnits R
		WHERE coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@TruckTypeID, 0), R.TruckTypeID, 0) = coalesce(TruckTypeID, nullif(@TruckTypeID, 0), 0)
		  AND coalesce(nullif(@OriginID, 0), R.OriginID, 0) = coalesce(OriginID, nullif(@OriginID, 0), 0)
		  AND coalesce(nullif(@OriginStateID, 0), R.OriginStateID, 0) = coalesce(OriginStateID, nullif(@OriginStateID, 0), 0)
		  AND coalesce(nullif(@DestinationID, 0), R.DestinationID, 0) = coalesce(DestinationID, nullif(@DestinationID, 0), 0)
		  AND coalesce(nullif(@DestinationStateID, 0), R.DestinationStateID, 0) = coalesce(DestinationStateID, nullif(@DestinationStateID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(ProducerID, nullif(@ProducerID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	)
	
	SELECT TOP (CASE WHEN @BestMatchOnly = 1 THEN 1 ELSE 10000 END) R.ID, ShipperID, CarrierID, ProductGroupID, TruckTypeID, OriginID, OriginStateID, DestinationID, DestinationStateID, ProducerID
	  , MinSettlementUnits, UomID, EffectiveDate, EndDate
	  , MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate
	  , BestMatch, Ranking
	  , Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewCarrierMinSettlementUnits R
	JOIN (
		SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 8  -- ensure some type of match occurred on all criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)

GO


/***********************************
-- Date Created: 12 June 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier MinSettlementUnits rows for the specified criteria
-- Changes: 
	- 3.7.28 - 2015/06/18 - KDA - ADDED
	- 3.10.13.5 - 2016/02/29 - JAE - Add truck type
***********************************/
ALTER FUNCTION [dbo].[fnCarrierMinSettlementUnitsDisplay]
(
  @StartDate date
, @EndDate date
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @TruckTypeID int
, @OriginID int
, @OriginStateID int
, @DestinationID int
, @DestinationStateID int
, @ProducerID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.TruckTypeID, R.OriginID, R.OriginStateID, R.DestinationID, R.DestinationStateID, R.ProducerID
		, R.MinSettlementUnits, R.UomID, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Shipper = SH.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, TruckType = TT.Name
		, Origin = O.Name
		, OriginFull = O.FullName
		, OriginState = O.State
		, OriginStateAbbrev = O.StateAbbrev
		, Destination = D.Name
		, DestinationFull = D.FullName
		, DestinationState = D.State
		, DestinationStateAbbrev = D.StateAbbrev
		, Producer = P.Name
		, Uom = U.Name
		, UomShort = U.Abbrev
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch, Ranking
	FROM dbo.fnCarrierMinSettlementUnits(@StartDate, @EndDate, @ShipperID, @CarrierID, @ProductGroupID, @TruckTypeID, @OriginID, @OriginStateID, @DestinationID, @DestinationStateID, @ProducerID, 0) R
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblTruckType TT ON TT.ID = R.TruckTypeID
	LEFT JOIN viewOrigin O ON O.ID = R.OriginID
	LEFT JOIN viewDestination D ON D.ID = R.DestinationID
	LEFT JOIN tblProducer P ON P.ID = R.ProducerID
	LEFT JOIN tblUom U ON U.ID = R.UomID
	ORDER BY EffectiveDate


GO


/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OrderRejectRate info for the specified criteria
-- Changes:
	- 3.7.28 - 2015/06/18 - KDA - add DriverID/DriverGroupID/Producer parameters
	- 3.7.30 - 2015/06/18 - KDA - remove DriverID
	- 3.10.13.5 - 2016/02/29 - JAE - Add truck type
***********************************/
ALTER FUNCTION [dbo].[fnCarrierOrderRejectRate]
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @TruckTypeID int
, @DriverGroupID int
, @OriginID int
, @StateID int
, @RegionID int
, @ProducerID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID, ReasonID
			, Ranking =	dbo.fnRateRanking(@ReasonID, R.ReasonID, 512, 1)
				  + dbo.fnRateRanking(@TruckTypeID, R.TruckTypeID, 256, 1)
				  + dbo.fnRateRanking(@ShipperID, R.ShipperID, 128, 0)
				  + dbo.fnRateRanking(@CarrierID, R.CarrierID, 64, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 32, 0)
				  + dbo.fnRateRanking(@DriverGroupID, R.DriverGroupID, 16, 1)
				  + dbo.fnRateRanking(@OriginID, R.OriginID, 8, 0)
				  + dbo.fnRateRanking(@StateID, R.StateID, 4, 0)
				  + dbo.fnRateRanking(@RegionID, R.RegionID, 2, 0)
				  + dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 1)
		FROM  dbo.viewCarrierOrderRejectRate R
		WHERE coalesce(nullif(@ReasonID, 0), R.ReasonID, 0) = coalesce(ReasonID, nullif(@ReasonID, 0), 0)
		  AND coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@TruckTypeID, 0), R.TruckTypeID, 0) = coalesce(TruckTypeID, nullif(@TruckTypeID, 0), 0)
		  AND coalesce(nullif(@DriverGroupID, 0), R.DriverGroupID, 0) = coalesce(DriverGroupID, nullif(@DriverGroupID, 0), 0)
		  AND coalesce(nullif(@OriginID, 0), R.OriginID, 0) = coalesce(OriginID, nullif(@OriginID, 0), 0)
		  AND coalesce(nullif(@StateID, 0), R.StateID, 0) = coalesce(StateID, nullif(@StateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(ProducerID, nullif(@ProducerID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	),
	cte2 AS
	(
		SELECT ID, S.ReasonID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT ReasonID = ISNULL(ReasonID, 0), Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 9  -- ensure some type of match occurred on all criteria choices
			GROUP BY ReasonID
		) X ON X.ReasonID = isnull(S.ReasonID, 0) AND X.Ranking = S.Ranking
	), 
	cte3 AS
	(	-- when both ReasonID = NULL AND ReasonID = @ReasonID records are BestMatch = 1, favor the ReasonID = @ReasonID record
		SELECT ID, X.ReasonID, X.Ranking, BestMatch = CASE WHEN BEST.ReasonID IS NULL THEN X.BestMatch ELSE 0 END
		FROM cte2 X
		LEFT JOIN (
			SELECT TOP 1 ReasonID FROM cte2 WHERE ReasonID = @ReasonID AND BestMatch = 1 ORDER BY Ranking DESC
		) BEST ON X.ReasonID IS NULL AND X.BestMatch = 1
	)
	
	SELECT R.ID, R.ReasonID, ShipperID, CarrierID, ProductGroupID, TruckTypeID, DriverGroupID, OriginID, StateID, RegionID, ProducerID
	  , Rate, RateTypeID, UomID, EffectiveDate, EndDate
	  , MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate
	  , BestMatch, Ranking
	  , Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewCarrierOrderRejectRate R
	JOIN cte3 X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)

GO


/***********************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OrderRejectRate rows for the specified criteria
-- Changes:
	- 3.7.28 - 2015/06/18 - KDA - add DriverID/DriverGroupID/ProducerID parameters
	- 3.7.28 - 2015/06/19 - KDA - remove DriverID
	- 3.10.13.5 - 2016/02/29 - JAE - Add truck type
***********************************/
ALTER FUNCTION [dbo].[fnCarrierOrderRejectRatesDisplay]
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @TruckTypeID int
, @DriverGroupID int
, @OriginID int
, @StateID int
, @RegionID int
, @ProducerID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ReasonID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.TruckTypeID, R.DriverGroupID, R.OriginID, R.StateID, R.RegionID, R.ProducerID
		, R.Rate, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Shipper = SH.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, TruckType = TT.Name
		, DriverGroup = DG.Name
		, Origin = D.Name
		, OriginFull = D.FullName
		, State = S.FullName
		, StateAbbrev = S.Abbreviation
		, Region = REG.Name
		, Producer = PR.Name
		, Reason = REA.Description
		, RateType = RT.Name
		, Uom = U.Name
		, UomShort = U.Abbrev
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
	FROM dbo.fnCarrierOrderRejectRate(@StartDate, @EndDate, @ReasonID, @ShipperID, @CarrierID, @ProductGroupID, @TruckTypeID, @DriverGroupID, @OriginID, @StateID, @RegionID, @ProducerID, 0) R
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblTruckType TT ON TT.ID = R.TruckTypeID
	LEFT JOIN tblDriverGroup DG ON DG.ID = R.DriverGroupID
	LEFT JOIN viewOrigin D ON D.ID = R.OriginID
	LEFT JOIN tblState S ON S.ID = R.StateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblProducer PR ON PR.ID = R.ProducerID
	LEFT JOIN tblOrderRejectReason REA ON REA.ID = R.ReasonID
	LEFT JOIN tblRateType RT ON RT.ID = R.RateTypeID
	LEFT JOIN tblUom U ON U.ID = R.UomID
	ORDER BY EffectiveDate

GO


/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OriginWaitRate info for the specified criteria
-- Changes:
	- 3.7.28 - 2015/06/18 - KDA - add DriverID/DriverGroupID/ProducerID parameters
	- 3.7.28 - 2015/06/18 - KDA - remove DriveID
	- 3.10.13.5 - 2016/02/29 - JAE - Add truck type
***********************************/
ALTER FUNCTION [dbo].[fnCarrierOriginWaitRate]
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @TruckTypeID int
, @DriverGroupID int
, @OriginID int
, @StateID int
, @RegionID int
, @ProducerID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID, ReasonID
			, Ranking =	dbo.fnRateRanking(@ReasonID, R.ReasonID, 512, 1)
				  + dbo.fnRateRanking(@TruckTypeID, R.TruckTypeID, 256, 1)
				  + dbo.fnRateRanking(@ShipperID, R.ShipperID, 128, 0)
				  + dbo.fnRateRanking(@CarrierID, R.CarrierID, 64, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 32, 0)
				  + dbo.fnRateRanking(@DriverGroupID, R.DriverGroupID, 16, 1)
				  + dbo.fnRateRanking(@OriginID, R.OriginID, 8, 0)
				  + dbo.fnRateRanking(@StateID, R.StateID, 4, 0)
				  + dbo.fnRateRanking(@RegionID, R.RegionID, 2, 0)
				  + dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 1)
		FROM  dbo.viewCarrierOriginWaitRate R
		WHERE coalesce(nullif(@ReasonID, 0), R.ReasonID, 0) = coalesce(ReasonID, nullif(@ReasonID, 0), 0)
		  AND coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@TruckTypeID, 0), R.TruckTypeID, 0) = coalesce(TruckTypeID, nullif(@TruckTypeID, 0), 0)
		  AND coalesce(nullif(@DriverGroupID, 0), R.DriverGroupID, 0) = coalesce(DriverGroupID, nullif(@DriverGroupID, 0), 0)
		  AND coalesce(nullif(@OriginID, 0), R.OriginID, 0) = coalesce(OriginID, nullif(@OriginID, 0), 0)
		  AND coalesce(nullif(@StateID, 0), R.StateID, 0) = coalesce(StateID, nullif(@StateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(ProducerID, nullif(@ProducerID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	),
	cte2 AS
	(
		SELECT ID, S.ReasonID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT ReasonID = ISNULL(ReasonID, 0), Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 9  -- ensure some type of match occurred on all criteria choices
			GROUP BY ReasonID
		) X ON X.ReasonID = isnull(S.ReasonID, 0) AND X.Ranking = S.Ranking
	), 
	cte3 AS
	(	-- when both ReasonID = NULL AND ReasonID = @ReasonID records are BestMatch = 1, favor the ReasonID = @ReasonID record
		SELECT ID, X.ReasonID, X.Ranking, BestMatch = CASE WHEN BEST.ReasonID IS NULL THEN X.BestMatch ELSE 0 END
		FROM cte2 X
		LEFT JOIN (
			SELECT TOP 1 ReasonID FROM cte2 WHERE ReasonID = @ReasonID AND BestMatch = 1 ORDER BY Ranking DESC
		) BEST ON X.ReasonID IS NULL AND X.BestMatch = 1
	)
	
	SELECT TOP (CASE WHEN @BestMatchOnly = 1 THEN 1 ELSE 10000 END) R.ID, R.ReasonID, ShipperID, CarrierID, ProductGroupID, TruckTypeID, DriverGroupID, OriginID, StateID, RegionID, ProducerID
	  , Rate, EffectiveDate, EndDate
	  , R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
	  , BestMatch, Ranking
	  , Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewCarrierOriginWaitRate R
	JOIN cte3 X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
	ORDER BY Ranking DESC
)

GO


/***********************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OriginWaitRate rows for the specified criteria
-- Changes:
	- 3.7.28 - 2015/06/18 - KDA - add DriverID/DriverGroupID/ProducerID parameters
	- 3.7.30 - 2015/06/18 - KDA - remove DriverID
	- 3.10.13.5 - 2016/02/29 - JAE - Add truck type
***********************************/
ALTER FUNCTION [dbo].[fnCarrierOriginWaitRatesDisplay]
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @TruckTypeID int
, @OriginID int
, @DriverGroupID int
, @StateID int
, @RegionID int
, @ProducerID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ReasonID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.TruckTypeID, R.DriverGroupID, R.OriginID, R.StateID, R.RegionID, R.ProducerID
		, R.Rate, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Shipper = SH.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, TruckType = TT.Name
		, DriverGroup = DG.Name
		, Origin = D.Name
		, OriginFull = D.FullName
		, State = S.FullName
		, StateAbbrev = S.Abbreviation
		, Region = REG.Name
		, Producer = PR.Name
		, Reason = REA.Description
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
		, Ranking
	FROM dbo.fnCarrierOriginWaitRate(@StartDate, @EndDate, @ReasonID, @ShipperID, @CarrierID, @ProductGroupID, @TruckTypeID, @DriverGroupID, @OriginID, @StateID, @RegionID, @ProducerID, 0) R
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblTruckType TT ON TT.ID = R.TruckTypeID
	LEFT JOIN tblDriverGroup DG ON DG.ID = R.DriverGroupID
	LEFT JOIN viewOrigin D ON D.ID = R.OriginID
	LEFT JOIN tblState S ON S.ID = R.StateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblProducer PR ON PR.ID = R.ProducerID
	LEFT JOIN tblOriginWaitReason REA ON REA.ID = R.ReasonID
	ORDER BY EffectiveDate

GO


/***********************************
-- Date Created: 5 May 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RateSheet info for the specified criteria
-- Changes:
	- 3.7.28 - 2015/06/18 - KDA - add DriverID/DriverGroupID/ProducerID parameters
	- 3.7.30 - 2015/06/19 - KDA - remove DriverID
	- 3.10.13.5 - 2016/02/29 - JAE - Add truck type
***********************************/
ALTER FUNCTION [dbo].[fnCarrierRateSheet]
(
  @StartDate date
, @EndDate date
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @TruckTypeID int
, @DriverGroupID int
, @OriginStateID int
, @DestStateID int
, @RegionID int
, @ProducerID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT R.ID
			, Ranking = dbo.fnRateRanking(@TruckTypeID, R.TruckTypeID, 256, 0)
				+ dbo.fnRateRanking(@ShipperID, R.ShipperID, 128, 0)
				+ dbo.fnRateRanking(@CarrierID, R.CarrierID, 64, 0)
				+ dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 32, 0)
				+ dbo.fnRateRanking(@DriverGroupID, R.DriverGroupID, 16, 1)
				+ dbo.fnRateRanking(@OriginStateID, R.OriginStateID, 8, 0)
				+ dbo.fnRateRanking(@DestStateID, R.DestStateID, 4, 0)
				+ dbo.fnRateRanking(@RegionID, R.RegionID, 2, 0)
				+ dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 1)
		FROM dbo.viewCarrierRateSheet R
		WHERE coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@TruckTypeID, 0), R.TruckTypeID, 0) = coalesce(TruckTypeID, nullif(@TruckTypeID, 0), 0)
		  AND coalesce(nullif(@DriverGroupID, 0), R.DriverGroupID, 0) = coalesce(DriverGroupID, nullif(@DriverGroupID, 0), 0)
		  AND coalesce(nullif(@OriginStateID, 0), R.OriginStateID, 0) = coalesce(OriginStateID, nullif(@OriginStateID, 0), 0)
		  AND coalesce(nullif(@DestStateID, 0), R.DestStateID, 0) = coalesce(DestStateID, nullif(@DestStateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(ProducerID, nullif(@ProducerID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	)
	
	SELECT R.ID, ShipperID, CarrierID, ProductGroupID, TruckTypeID, DriverGroupID, OriginStateID, DestStateID, RegionID, ProducerID, RateTypeID, UomID, EffectiveDate, EndDate
	  , MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate
	  , BestMatch, Ranking
	  , Locked, R.CreateDateUTC, R.CreatedByUser, R.LastChangeDateUTC, R.LastChangedByUser 
	FROM dbo.viewCarrierRateSheet R
	JOIN (
		SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 8  -- ensure some type of match occurred on all criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.ID = R.ID 
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)

GO


/***********************************
-- Date Created: 5 May 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RateSheetRangeRate info for the specified criteria
-- Changes:
	- 3.7.28 - 2015/06/13 - KDA - Add DriverID | DriverGroupID | ProducerID
	- 3.7.30 - 2015/06/19 - KDA - remove DriverID
	- 3.10.13.5 - 2016/02/29 - JAE - Add truck type
***********************************/
ALTER FUNCTION [dbo].[fnCarrierRateSheetDisplay]
(
  @StartDate date
, @EndDate date
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @TruckTypeID int
, @DriverGroupID int
, @OriginStateID int
, @DestStateID int
, @RegionID int
, @ProducerID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.TruckTypeID, R.DriverGroupID, R.OriginStateID, R.DestStateID, R.RegionID, R.ProducerID, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Shipper = SH.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, TruckType = TT.Name
		, DriverGroup = DG.Name
		, OriginState = OS.FullName
		, OriginStateAbbrev = OS.Abbreviation
		, DestState = OS.FullName
		, DestStateAbbrev = OS.Abbreviation
		, Region = REG.Name
		, Producer = P.Name
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
		, Ranking
	FROM dbo.fnCarrierRateSheet(@StartDate, @EndDate, @ShipperID, @CarrierID, @ProductGroupID, @TruckTypeID, @DriverGroupID, @OriginStateID, @DestStateID, @RegionID, @ProducerID, 0) R
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblTruckType TT ON TT.ID = R.TruckTypeID
	LEFT JOIN tblDriverGroup DG ON DG.ID = R.DriverGroupID
	LEFT JOIN tblState OS ON OS.ID = R.OriginStateID
	LEFT JOIN tblState DS ON DS.ID = R.DestStateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblProducer P ON P.ID = R.ProducerID
	ORDER BY EffectiveDate

GO


/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RateSheetRangeRate info for the specified criteria
-- Changes:
	- 3.7.28 - 2015/06/13 - KDA - Add DriverID | DriverGroupID | ProducerID
	- 3.7.30 - 2015/06/19 - KDA - remove DriverID
	- 3.10.13.5 - 2016/02/29 - JAE - Add truck type
***********************************/
ALTER FUNCTION [dbo].[fnCarrierRateSheetRangeRate]
(
  @StartDate date
, @EndDate date
, @RouteMiles int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @TruckTypeID int
, @DriverGroupID int
, @OriginStateID int
, @DestStateID int
, @RegionID int
, @ProducerID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT RateSheetID = R.ID, RangeRateID = RR.ID
			-- the manually added .01 is normally added via the fnRateRanking routine
			, Ranking = CASE WHEN @RouteMiles BETWEEN RR.MinRange AND RR.MaxRange THEN 512.01 ELSE 0 END
				+ dbo.fnRateRanking(@TruckTypeID, R.TruckTypeID, 256, 0)
				+ dbo.fnRateRanking(@ShipperID, R.ShipperID, 128, 0)
				+ dbo.fnRateRanking(@CarrierID, R.CarrierID, 64, 0)
				+ dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 32, 0)
				+ dbo.fnRateRanking(@DriverGroupID, R.DriverGroupID, 16, 1)
				+ dbo.fnRateRanking(@OriginStateID, R.OriginStateID, 8, 0)
				+ dbo.fnRateRanking(@DestStateID, R.DestStateID, 4, 0)
				+ dbo.fnRateRanking(@RegionID, R.RegionID, 2, 0)
				+ dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 1)
		FROM dbo.viewCarrierRateSheet R
		JOIN dbo.tblCarrierRangeRate RR ON RR.RateSheetID = R.ID 
		  AND isnull(@RouteMiles, RR.MinRange) BETWEEN RR.MinRange AND RR.MaxRange
		WHERE coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@TruckTypeID, 0), R.TruckTypeID, 0) = coalesce(TruckTypeID, nullif(@TruckTypeID, 0), 0)
		  AND coalesce(nullif(@DriverGroupID, 0), R.DriverGroupID, 0) = coalesce(DriverGroupID, nullif(@DriverGroupID, 0), 0)
		  AND coalesce(nullif(@OriginStateID, 0), R.OriginStateID, 0) = coalesce(OriginStateID, nullif(@OriginStateID, 0), 0)
		  AND coalesce(nullif(@DestStateID, 0), R.DestStateID, 0) = coalesce(DestStateID, nullif(@DestStateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(ProducerID, nullif(@ProducerID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	)
	
	SELECT RR.ID, RateSheetID = R.ID, ShipperID, CarrierID, ProductGroupID, TruckTypeID, DriverGroupID, OriginStateID, DestStateID, RegionID, ProducerID
	  , Rate, MinRange, MaxRange, RateTypeID, UomID, EffectiveDate, EndDate
	  , MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate
	  , BestMatch, Ranking
	  , Locked, R.CreateDateUTC, R.CreatedByUser, R.LastChangeDateUTC, R.LastChangedByUser 
	FROM dbo.viewCarrierRateSheet R
	JOIN dbo.tblCarrierRangeRate RR ON RR.RateSheetID = R.ID
	JOIN (
		SELECT RateSheetID, RangeRateID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 9  -- ensure some type of match occurred on all criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.RateSheetID = R.ID AND X.RangeRateID = RR.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)

GO


/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RateSheetRangeRate info for the specified criteria
-- Changes:
	- 3.7.28 - 2015/06/13 - KDA - Add DriverID | DriverGroupID | ProducerID
	- 3.7.30 - 2015/06/19 - KDA - remove DriverID
	- 3.10.13.5 - 2016/02/29 - JAE - Add truck type
***********************************/
ALTER FUNCTION [dbo].[fnCarrierRateSheetRangeRatesDisplay]
(
  @StartDate date
, @EndDate date
, @RouteMiles int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @TruckTypeID int
, @DriverGroupID int
, @OriginStateID int
, @DestStateID int
, @RegionID int
, @ProducerID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.RateSheetID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.TruckTypeID, R.DriverGroupID, R.OriginStateID, R.DestStateID, R.RegionID, R.ProducerID, R.MinRange, R.MaxRange, R.Rate, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Shipper = SH.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, TruckType = TT.Name
		, DriverGroup = DG.Name
		, OriginState = OS.FullName
		, OriginStateAbbrev = OS.Abbreviation
		, DestState = OS.FullName
		, DestStateAbbrev = OS.Abbreviation
		, Region = REG.Name
		, Producer = P.Name
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
		, Ranking
	FROM dbo.fnCarrierRateSheetRangeRate(@StartDate, @EndDate, @RouteMiles, @ShipperID, @CarrierID, @ProductGroupID, @TruckTypeID, @DriverGroupID, @OriginStateID, @DestStateID, @RegionID, @ProducerID, 0) R
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblTruckType TT ON TT.ID = R.TruckTypeID
	LEFT JOIN tblDriverGroup DG ON DG.ID = R.DriverGroupID
	LEFT JOIN tblState OS ON OS.ID = R.OriginStateID
	LEFT JOIN tblState DS ON DS.ID = R.DestStateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblProducer P ON P.ID = R.ProducerID
	ORDER BY EffectiveDate

GO


/****************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RouteRate info for the specified criteria
-- Changes:
	- 3.7.28 - 2015/06/13 - KDA - Add DriverID | DriverGroupID | ProducerID
	- 3.7.30 - 2015/06/19 - KDA - remove DriverID
	- 3.10.13.5 - 2016/02/29 - JAE - Add truck type
*****************************************************/
ALTER FUNCTION [dbo].[fnCarrierRouteRate]
(
  @StartDate date
, @EndDate date
, @OriginID int
, @DestinationID int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @TruckTypeID int
, @DriverGroupID int
, @BestMatchOnly bit = 0
) RETURNS @ret TABLE 
(
  ID int
, RouteID int
, OriginID int
, DestinationID int
, ShipperID int
, CarrierID int
, ProductGroupID int
, TruckTypeID int
, DriverGroupID int
, Rate decimal(18, 10)
, RateTypeID int
, UomID int
, EffectiveDate date
, EndDate date
, MaxEffectiveDate date
, MinEndDate date
, NextEffectiveDate date
, PriorEndDate date
, BestMatch bit
, Ranking decimal(10, 2)
, Locked bit
, CreateDateUTC datetime
, CreatedByUser varchar(100)
, LastChangeDateUTC datetime
, LastChangedByUser varchar(100)
) AS BEGIN

	DECLARE @data TABLE (ID int, RouteID int, Ranking decimal(10, 2))
	INSERT INTO @data
		SELECT R.ID
			, R.RouteID
			, Ranking =	dbo.fnRateRanking(@TruckTypeID, R.TruckTypeID, 16, 0)
				  + dbo.fnRateRanking(@ShipperID, R.ShipperID, 8, 0)
				  + dbo.fnRateRanking(@CarrierID, R.CarrierID, 4, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 2, 0)
				  + dbo.fnRateRanking(@DriverGroupID, R.DriverGroupID, 1, 1)
		FROM dbo.viewCarrierRouteRate R
		JOIN tblRoute RO ON RO.ID = R.RouteID
		WHERE (nullif(@OriginID, 0) IS NULL OR @OriginID = RO.OriginID)
		  AND (nullif(@DestinationID, 0) IS NULL OR @DestinationID = RO.DestinationID)
		  AND coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@TruckTypeID, 0), R.TruckTypeID, 0) = coalesce(TruckTypeID, nullif(@TruckTypeID, 0), 0)
		  AND coalesce(nullif(@DriverGroupID, 0), R.DriverGroupID, 0) = coalesce(DriverGroupID, nullif(@DriverGroupID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)

	INSERT INTO @ret
	SELECT R.ID, RouteID, OriginID, DestinationID, ShipperID, CarrierID, ProductGroupID, TruckTypeID, DriverGroupID, Rate, RateTypeID, UomID, EffectiveDate, EndDate
		, MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate
		, BestMatch, Ranking
		, Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewCarrierRouteRate R
	JOIN (
		SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM @data S
		LEFT JOIN (
			SELECT RouteID, Ranking = MAX(Ranking)
			FROM @data
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 4  -- ensure some type of match occurred on all criteria choices
			GROUP BY RouteID			  
		) X ON X.RouteID = S.RouteID AND X.Ranking = S.Ranking
	) X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
	
	RETURN
END

GO


/****************************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RouteRate rows for the specified criteria
-- Changes:
	- 3.7.28 - 2015/06/13 - KDA - Add DriverID | DriverGroupID | ProducerID
	- 3.7.30 - 2015/06/19 - KDA - remove DriverID
	- 3.10.13.5 - 2016/02/29 - JAE - Add truck type
*****************************************************/
ALTER FUNCTION [dbo].[fnCarrierRouteRatesDisplay]
(
  @StartDate date
, @EndDate date
, @OriginID int
, @DestinationID int
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @TruckTypeID int
, @DriverGroupID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.RouteID, RO.ActualMiles, R.OriginID, R.DestinationID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.TruckTypeID, R.DriverGroupID, R.Rate, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Origin = O.Name
		, OriginFull = O.FullName
		, Destination = D.Name
		, DestinationFull = D.FullName
		, Shipper = SH.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, TruckType = TT.Name
		, DriverGroup = DG.Name
		, RateType = RT.Name
		, Uom = U.Name
		, UomShort = U.Abbrev
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch, Ranking
	FROM dbo.fnCarrierRouteRate(@StartDate, @EndDate, @OriginID, @DestinationID, @ShipperID, @CarrierID, @ProductGroupID, @TruckTypeID, @DriverGroupID, 0) R
	JOIN tblRoute RO ON RO.ID = R.RouteID
	JOIN viewOrigin O ON O.ID = R.OriginID
	JOIN viewDestination D ON D.ID = R.DestinationID
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblTruckType TT ON TT.ID = R.TruckTypeID
	LEFT JOIN tblDriverGroup DG ON DG.ID = R.DriverGroupID
	LEFT JOIN tblRateType RT ON RT.ID = R.RateTypeID
	LEFT JOIN tblUom U ON U.ID = R.UomID
	ORDER BY EffectiveDate

GO


/***********************************
-- Date Created: 12 June 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier SettlementFactor info for the specified criteria
-- Changes: 
	- 3.7.28 - 2015/06/18 - KDA - ADDED
	- 3.10.13.5 - 2016/02/29 - JAE - Add truck type
***********************************/
ALTER FUNCTION [dbo].[fnCarrierSettlementFactor]
(
  @StartDate date
, @EndDate date
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @TruckTypeID int
, @OriginID int
, @OriginStateID int
, @DestinationID int
, @DestinationStateID int
, @ProducerID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID
			, Ranking =	dbo.fnRateRanking(@TruckTypeID, R.TruckTypeID, 256, 0)
				  + dbo.fnRateRanking(@ShipperID, R.ShipperID, 128, 0)
				  + dbo.fnRateRanking(@CarrierID, R.CarrierID, 64, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 32, 0)
				  + dbo.fnRateRanking(@OriginID, R.OriginID, 16, 0)
				  + dbo.fnRateRanking(@OriginStateID, R.OriginStateID, 8, 0)
				  + dbo.fnRateRanking(@DestinationID, R.DestinationID, 4, 0)
				  + dbo.fnRateRanking(@DestinationStateID, R.DestinationStateID, 2, 0)
				  + dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 1)
		FROM  dbo.viewCarrierSettlementFactor R
		WHERE coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@TruckTypeID, 0), R.TruckTypeID, 0) = coalesce(TruckTypeID, nullif(@TruckTypeID, 0), 0)
		  AND coalesce(nullif(@OriginID, 0), R.OriginID, 0) = coalesce(OriginID, nullif(@OriginID, 0), 0)
		  AND coalesce(nullif(@OriginStateID, 0), R.OriginStateID, 0) = coalesce(OriginStateID, nullif(@OriginStateID, 0), 0)
		  AND coalesce(nullif(@DestinationID, 0), R.DestinationID, 0) = coalesce(DestinationID, nullif(@DestinationID, 0), 0)
		  AND coalesce(nullif(@DestinationStateID, 0), R.DestinationStateID, 0) = coalesce(DestinationStateID, nullif(@DestinationStateID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(ProducerID, nullif(@ProducerID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	)
	
	SELECT TOP (CASE WHEN @BestMatchOnly = 1 THEN 1 ELSE 10000 END) R.ID, ShipperID, CarrierID, ProductGroupID, TruckTypeID, OriginID, OriginStateID, DestinationID, DestinationStateID, ProducerID
	  , SettlementFactorID, EffectiveDate, EndDate
	  , MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate
	  , BestMatch, Ranking
	  , Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewCarrierSettlementFactor R
	JOIN (
		SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 8  -- ensure some type of match occurred on all criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)

GO


/***********************************
-- Date Created: 12 June 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier SettlementFactor rows for the specified criteria
-- Changes: 
	- 3.7.28 - 2015/06/18 - KDA - ADDED
	- 3.10.13.5 - 2016/02/29 - JAE - Add truck type
***********************************/
ALTER FUNCTION [dbo].[fnCarrierSettlementFactorDisplay]
(
  @StartDate date
, @EndDate date
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @TruckTypeID int
, @OriginID int
, @OriginStateID int
, @DestinationID int
, @DestinationStateID int
, @ProducerID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.TruckTypeID, R.OriginID, R.OriginStateID, R.DestinationID, R.DestinationStateID, R.ProducerID
		, R.SettlementFactorID, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, SettlementFactor = SF.Name
		, Shipper = SH.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, TruckType = TT.Name
		, Origin = O.Name
		, OriginFull = O.FullName
		, OriginState = O.State
		, OriginStateAbbrev = O.StateAbbrev
		, Destination = D.Name
		, DestinationFull = D.FullName
		, DestinationState = D.State
		, DestinationStateAbbrev = D.StateAbbrev
		, Producer = P.Name
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch, Ranking
	FROM dbo.fnCarrierSettlementFactor(@StartDate, @EndDate, @ShipperID, @CarrierID, @ProductGroupID, @TruckTypeID, @OriginID, @OriginStateID, @DestinationID, @DestinationStateID, @ProducerID, 0) R
	JOIN dbo.tblSettlementFactor SF ON SF.ID = R.SettlementFactorID
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblTruckType TT ON TT.ID = R.TruckTypeID
	LEFT JOIN viewOrigin O ON O.ID = R.OriginID
	LEFT JOIN viewDestination D ON D.ID = R.DestinationID
	LEFT JOIN tblProducer P ON P.ID = R.ProducerID
	ORDER BY EffectiveDate


GO


/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier WaitFeeParameter info for the specified order
-- Changes:
	-- 3.7.28 - 2015/06/18 - KDA - add Producer parameter
	- 3.10.13.5 - 2016/02/29 - JAE - Add truck type
***********************************/
ALTER FUNCTION [dbo].[fnCarrierWaitFeeParameter]
(
  @StartDate date
, @EndDate date
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @TruckTypeID int
, @OriginStateID int
, @DestStateID int
, @RegionID int
, @ProducerID int
, @bestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID
			, Ranking = dbo.fnRateRanking(@TruckTypeID, R.TruckTypeID, 128, 0)
				  + dbo.fnRateRanking(@ShipperID, R.ShipperID, 64, 0)
				  + dbo.fnRateRanking(@CarrierID, R.CarrierID, 32, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 16, 0)
				  + dbo.fnRateRanking(@OriginStateID, R.OriginStateID, 8, 0)
				  + dbo.fnRateRanking(@DestStateID, R.DestStateID, 4, 0)
				  + dbo.fnRateRanking(@RegionID, R.RegionID, 2, 0)
				  + dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 1)
		FROM  dbo.viewCarrierWaitFeeParameter R
		WHERE coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@TruckTypeID, 0), R.TruckTypeID, 0) = coalesce(TruckTypeID, nullif(@TruckTypeID, 0), 0)
		  AND coalesce(nullif(@OriginStateID, 0), R.OriginStateID, 0) = coalesce(OriginStateID, nullif(@OriginStateID, 0), 0)
		  AND coalesce(nullif(@DestStateID, 0), R.DestStateID, 0) = coalesce(DestStateID, nullif(@DestStateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(ProducerID, nullif(@ProducerID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	)
	
	SELECT R.ID, ShipperID, CarrierID, ProductGroupID, TruckTypeID, OriginStateID, DestStateID, RegionID, ProducerID, EffectiveDate, EndDate
		, SubUnitID, RoundingTypeID, OriginThresholdMinutes, DestThresholdMinutes, OriginMinBillableMinutes, OriginMaxBillableMinutes, DestMinBillableMinutes, DestMaxBillableMinutes
		, MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate
		, BestMatch, Ranking
		, Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewCarrierWaitFeeParameter R
	JOIN (
		SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 7  -- ensure some type of match occurred on all criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)

GO


/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier WaitFeeParameter rows for the specified criteria
-- Changes:
	-- 3.7.28 - 2015/06/18 - KDA - add Producer parameter
	- 3.10.13.5 - 2016/02/29 - JAE - Add truck type
***********************************/
ALTER FUNCTION [dbo].[fnCarrierWaitFeeParametersDisplay]
(
  @StartDate date
, @EndDate date
, @ShipperID int
, @CarrierID int
, @ProductGroupID int
, @TruckTypeID int
, @OriginStateID int
, @DestStateID int
, @RegionID int
, @ProducerID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.TruckTypeID, R.OriginStateID, R.DestStateID, R.RegionID, R.ProducerID
		, R.SubUnitID, R.RoundingTypeID, R.OriginThresholdMinutes, R.DestThresholdMinutes
		, R.OriginMinBillableMinutes, R.OriginMaxBillableMinutes, R.DestMinBillableMinutes, R.DestMaxBillableMinutes
		, R.EffectiveDate, R.EndDate, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Shipper = S.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, TruckType = TT.Name
		, OriginState = OS.FullName
		, OriginStateAbbrev = OS.Abbreviation
		, DestinationState = DS.FullName
		, DestinationStateAbbrev = DS.Abbreviation
		, Region = REG.Name
		, Producer = PR.Name
		, RoundingType = WFRT.Name
		, SubUnit = WFSU.Name
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch, Ranking
	FROM dbo.fnCarrierWaitFeeParameter(@StartDate, @EndDate, @ShipperID, @CarrierID, @ProductGroupID, @TruckTypeID, @OriginStateID, @DestStateID, @RegionID, @ProducerID, 0) R 
	JOIN tblWaitFeeRoundingType WFRT ON WFRT.ID = R.RoundingTypeID
	JOIN tblWaitFeeSubUnit WFSU ON WFSU.ID = R.SubUnitID
	LEFT JOIN tblCustomer S ON S.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblTruckType TT ON TT.ID = R.TruckTypeID
	LEFT JOIN tblState OS ON OS.ID = R.OriginStateID
	LEFT JOIN tblState DS ON DS.ID = R.DestStateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblProducer PR ON PR.ID = R.ProducerID
	ORDER BY EffectiveDate

GO



-- UPDATE SHIPPER FUNCTIONS/STORED PROCEDURES


/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper AssessorialRate info for the specified order
-- Changes:
	- 3.7.29 - 2015/06/18 - KDA - move ProducerID parameter/field to end of criteria to be consistent with other "Best-Match" tables
	- 3.10.13.5 - 2016/02/29 - JAE - Add truck type
***********************************/
ALTER FUNCTION [dbo].[fnShipperAssessorialRates](@StartDate date, @EndDate date, @TypeID int, @ShipperID int, @ProductGroupID int, @TruckTypeID int, @OriginID int, @DestinationID int, @OriginStateID int, @DestStateID int, @RegionID int, @ProducerID int, @BestMatchOnly bit = 0)
RETURNS 
	@ret TABLE (
		ID int
	  , TypeID int
	  , ShipperID int
	  , ProductGroupID int
	  , TruckTypeID int
	  , OriginID int
	  , DestinationID int
	  , OriginStateID int
	  , DestStateID int
	  , RegionID int
	  , ProducerID int
	  , Rate decimal(18, 10)
	  , RateTypeID int
	  , UomID int
	  , EffectiveDate date
	  , EndDate date
	  , MaxEffectiveDate date
	  , MinEndDate date
	  , NextEffectiveDate date
	  , PriorEndDate date
	  , BestMatch bit
	  , Ranking smallmoney
	  , Locked bit
	  , CreateDateUTC datetime
	  , CreatedByUser varchar(100)
	  , LastChangeDateUTC datetime
	  , LastChangedByUser varchar(100)
	)
AS BEGIN
	-- get the raw data (all matched rows)
	DECLARE @src TABLE (ID int, TypeID int, Ranking smallmoney)
	INSERT INTO @src (ID, TypeID, Ranking)
		SELECT ID, TypeID
			, Ranking =	dbo.fnRateRanking(@TruckTypeID, R.TruckTypeID, 256, 0)
					  + dbo.fnRateRanking(@ShipperID, R.ShipperID, 128, 0)
					  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 64, 0)
					  + dbo.fnRateRanking(@OriginID, R.OriginID, 32, 0)
					  + dbo.fnRateRanking(@DestinationID, R.DestinationID, 16, 0)
					  + dbo.fnRateRanking(@OriginStateID, R.OriginStateID, 8, 0)
					  + dbo.fnRateRanking(@DestStateID, R.DestStateID, 4, 0)
					  + dbo.fnRateRanking(@RegionID, R.RegionID, 2, 0)
					  + dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 1)
		FROM dbo.viewShipperAssessorialRate R
		WHERE coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(R.ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@TruckTypeID, 0), R.TruckTypeID, 0) = coalesce(R.TruckTypeID, nullif(@TruckTypeID, 0), 0)
		  AND coalesce(nullif(@OriginID, 0), R.OriginID, 0) = coalesce(R.OriginID, nullif(@OriginID, 0), 0)
		  AND coalesce(nullif(@DestinationID, 0), R.DestinationID, 0) = coalesce(R.DestinationID, nullif(@DestinationID, 0), 0)
		  AND coalesce(nullif(@OriginStateID, 0), R.OriginStateID, 0) = coalesce(R.OriginStateID, nullif(@OriginStateID, 0), 0)
		  AND coalesce(nullif(@DestStateID, 0), R.DestStateID, 0) = coalesce(R.DestStateID, nullif(@DestStateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(R.RegionID, nullif(@RegionID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(R.ProducerID, nullif(@ProducerID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
		  AND (nullif(@TypeID, 0) IS NULL OR TypeID = @TypeID)

	-- return the rate data for the best-match rates 
	INSERT INTO @ret (ID, TypeID, ShipperID, ProductGroupID, TruckTypeID, OriginID, DestinationID, OriginStateID, DestStateID, RegionID, ProducerID, Rate, RateTypeID, UomID, EffectiveDate, EndDate
			, MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate, BestMatch, Ranking, Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser)
		SELECT CAR.ID, TypeID, ShipperID, ProductGroupID, TruckTypeID, OriginID, DestinationID, OriginStateID, DestStateID, RegionID, ProducerID, Rate, RateTypeID, UomID, EffectiveDate, EndDate
			, MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate, BestMatch, Ranking, Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser
		FROM viewShipperAssessorialRate CAR
		JOIN (
			SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.TypeID IS NULL THEN 0 ELSE 1 END as bit)
			FROM @src S
			LEFT JOIN (
				SELECT TypeID, Ranking = MAX(Ranking)
				FROM @src
				WHERE @StartDate = ISNULL(@EndDate, @StartDate)
				  AND Ranking % 1 = 0.01 * 8  -- ensure some type of match occurred on all criteria choices
				GROUP BY TypeID
			) X ON X.TypeID = S.TypeID AND X.Ranking = S.Ranking
		) X ON X.ID = CAR.ID
		WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
		
	RETURN
END

GO


/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper AssessorialRate rows for the specified criteria
-- Changes:
	- 3.7.29 - 2015/06/18 - KDA - move ProducerID parameter/field to end of criteria to be consistent with other "Best-Match" tables
	- 3.10.13.5 - 2016/02/29 - JAE - Add truck type
***********************************/
ALTER FUNCTION [dbo].[fnShipperAssessorialRatesDisplay](@StartDate date, @EndDate date, @TypeID int, @ShipperID int, @ProductGroupID int, @TruckTypeID int, @OriginID int, @DestinationID int, @OriginStateID int, @DestStateID int, @RegionID int, @ProducerID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ShipperID, R.ProductGroupID, R.TruckTypeID, R.OriginID, R.DestinationID, R.OriginStateID, R.DestStateID, R.RegionID, R.ProducerID, R.TypeID, R.Rate, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Type = RT.Name
		, Shipper = C.Name
		, ProductGroup = PG.Name
		, TruckType = TT.Name
		, Origin = O.Name
		, OriginFull = O.FullName
		, Destination = D.Name
		, DestinationFull = D.FullName
		, OriginState = OS.FullName
		, OriginStateAbbrev = OS.Abbreviation
		, DestState = DS.FullName
		, DestStateAbbrev = DS.Abbreviation
		, Region = RE.Name
		, Producer = PR.Name
		, RateType = RT.Name
		, Uom = U.Name
		, UomShort = U.Abbrev
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch, Ranking
	FROM dbo.fnShipperAssessorialRates(@StartDate, @EndDate, @TypeID, @ShipperID, @ProductGroupID, @TruckTypeID, @OriginID, @DestinationID, @OriginStateID, @DestStateID, @RegionID, @ProducerID, 0) R
	JOIN tblAssessorialRateType RT ON RT.ID = R.TypeID
	LEFT JOIN tblCustomer C ON C.ID = R.ShipperID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblTruckType TT ON TT.ID = R.TruckTypeID
	LEFT JOIN viewOrigin O ON O.ID = R.OriginID
	LEFT JOIN viewDestination D ON D.ID = R.DestinationID
	LEFT JOIN tblState OS ON OS.ID = R.OriginStateID
	LEFT JOIN tblState DS ON DS.ID = R.DestStateID
	LEFT JOIN tblRegion RE ON RE.ID = R.RegionID
	LEFT JOIN tblProducer PR ON PR.ID = R.ProducerID
	LEFT JOIN tblUom U ON U.ID = R.UomID
	ORDER BY EffectiveDate

GO


/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper DestinationWaitRate info for the specified criteria
-- Changes:
	- 3.7.29 - 2015/06/18 - KDA - add ProducerID parameter/field as new Best-Match criteria
	- 3.10.13.5 - 2016/02/29 - JAE - Add truck type
***********************************/
ALTER FUNCTION [dbo].[fnShipperDestinationWaitRate]
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @ProductGroupID int
, @TruckTypeID int
, @DestinationID int
, @StateID int
, @RegionID int
, @ProducerID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID, ReasonID
			, Ranking =	dbo.fnRateRanking(@TruckTypeID, R.TruckTypeID, 128, 1)
				  + dbo.fnRateRanking(@ReasonID, R.ReasonID, 64, 1)
				  + dbo.fnRateRanking(@ShipperID, R.ShipperID, 32, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 16, 0)
				  + dbo.fnRateRanking(@DestinationID, R.DestinationID, 8, 0)
				  + dbo.fnRateRanking(@StateID, R.StateID, 4, 0)
				  + dbo.fnRateRanking(@RegionID, R.RegionID, 2, 0)
				  + dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 1)
		FROM  dbo.viewShipperDestinationWaitRate R
		WHERE coalesce(nullif(@ReasonID, 0), R.ReasonID, 0) = coalesce(ReasonID, nullif(@ReasonID, 0), 0)
		  AND coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@TruckTypeID, 0), R.TruckTypeID, 0) = coalesce(TruckTypeID, nullif(@TruckTypeID, 0), 0)
		  AND coalesce(nullif(@DestinationID, 0), R.DestinationID, 0) = coalesce(DestinationID, nullif(@DestinationID, 0), 0)
		  AND coalesce(nullif(@StateID, 0), R.StateID, 0) = coalesce(StateID, nullif(@StateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(ProducerID, nullif(@ProducerID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	),
	cte2 AS
	(
		SELECT ID, S.ReasonID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT ReasonID = ISNULL(ReasonID, 0), Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 7  -- ensure some type of match occurred on all criteria choices
			GROUP BY ReasonID
		) X ON X.ReasonID = isnull(S.ReasonID, 0) AND X.Ranking = S.Ranking
	), 
	cte3 AS
	(	-- when both ReasonID = NULL AND ReasonID = @ReasonID records are BestMatch = 1, favor the ReasonID = @ReasonID record
		SELECT ID, X.ReasonID, X.Ranking, BestMatch = CASE WHEN BEST.ReasonID IS NULL THEN X.BestMatch ELSE 0 END
		FROM cte2 X
		LEFT JOIN (
			SELECT TOP 1 ReasonID FROM cte2 WHERE ReasonID = @ReasonID AND BestMatch = 1 ORDER BY Ranking DESC
		) BEST ON X.ReasonID IS NULL AND X.BestMatch = 1
	)
	
	SELECT TOP (CASE WHEN @BestMatchOnly = 1 THEN 1 ELSE 10000 END) R.ID, R.ReasonID, ShipperID, ProductGroupID, TruckTypeID, DestinationID, StateID, RegionID, ProducerID, Rate, EffectiveDate, EndDate
		, MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate
		, BestMatch, Ranking
		, Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewShipperDestinationWaitRate R
	JOIN cte3 X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
	ORDER BY Ranking DESC
)

GO


/***********************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper DestinationWaitRate rows for the specified criteria
-- Changes:
	- 3.7.29 - 2015/06/18 - KDA - add ProducerID parameter/field as new Best-Match criteria
	- 3.10.13.5 - 2016/02/29 - JAE - Add truck type
***********************************/
ALTER FUNCTION [dbo].[fnShipperDestinationWaitRateDisplay]
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @ProductGroupID int
, @TruckTypeID int
, @DestinationID int
, @StateID int
, @RegionID int
, @ProducerID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ReasonID, R.ShipperID, R.ProductGroupID, R.TruckTypeID, R.DestinationID, R.StateID, R.RegionID, R.ProducerID, R.Rate, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Shipper = SH.Name
		, ProductGroup = PG.Name
		, TruckType = TT.Name
		, Destination = D.Name
		, DestinationFull = D.FullName
		, State = S.FullName
		, StateAbbrev = S.Abbreviation
		, Region = REG.Name
		, Producer = P.Name
		, Reason = REA.Description
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
		, Ranking
	FROM dbo.fnShipperDestinationWaitRate(@StartDate, @EndDate, @ReasonID, @ShipperID, @ProductGroupID, @TruckTypeID, @DestinationID, @StateID, @RegionID, @ProducerID, 0) R
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblTruckType TT ON TT.ID = R.TruckTypeID
	LEFT JOIN viewDestination D ON D.ID = R.DestinationID
	LEFT JOIN tblState S ON S.ID = R.StateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblProducer P ON P.ID = R.ProducerID
	LEFT JOIN tblDestinationWaitReason REA ON REA.ID = R.ReasonID
	ORDER BY EffectiveDate

GO


/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper DestinationWaitRate info for the specified criteria
-- Changes
	- 3.10.13.5 - 2016/02/29 - JAE - Add truck type
***********************************/
ALTER FUNCTION [dbo].[fnShipperFuelSurchargeRate]
(
  @StartDate date
, @EndDate date
, @ShipperID int
, @ProductGroupID int
, @TruckTypeID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID
			, Ranking =	dbo.fnRateRanking(@TruckTypeID, R.TruckTypeID, 4, 0)
				  + dbo.fnRateRanking(@ShipperID, R.ShipperID, 2, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 1, 0)
		FROM  dbo.viewShipperFuelSurchargeRate R
		WHERE coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@TruckTypeID, 0), R.TruckTypeID, 0) = coalesce(TruckTypeID, nullif(@TruckTypeID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	)
	SELECT R.ID, ShipperID, ProductGroupID, TruckTypeID, FuelPriceFloor, IntervalAmount, IncrementAmount, EffectiveDate, EndDate
		, MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate
		, BestMatch, Ranking
		, Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewShipperFuelSurchargeRate R
	JOIN (
		SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 2  -- ensure some type of match occurred on all 6 criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)

GO


/***********************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper RouteRate rows for the specified criteria
-- Changes
	- 3.10.13.5 - 2016/02/29 - JAE - Add truck type
***********************************/
ALTER FUNCTION [dbo].[fnShipperFuelSurchargeRateDisplay](@StartDate date, @EndDate date, @ShipperID int, @ProductGroupID int, @TruckTypeID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ShipperID, R.ProductGroupID, R.TruckTypeID, R.FuelPriceFloor, R.IntervalAmount, R.IncrementAmount, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Shipper = C.Name
		, ProductGroup = PG.Name
		, TruckType = TT.Name
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch, Ranking
	FROM dbo.fnShipperFuelSurchargeRate(@StartDate, @EndDate, @ShipperID, @ProductGroupID, @TruckTypeID, 0) R
	LEFT JOIN tblCustomer C ON C.ID = R.ShipperID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblTruckType TT ON TT.ID = R.TruckTypeID
	ORDER BY EffectiveDate

GO


/***********************************
-- Date Created: 12 June 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper MinSettlementUnits info for the specified criteria
-- Changes:
	- 3.7.29 - 2015/06/18 - KDA - ADDED
	- 3.10.13.5 - 2016/02/29 - JAE - Add truck type
***********************************/
ALTER FUNCTION [dbo].[fnShipperMinSettlementUnits]
(
  @StartDate date
, @EndDate date
, @ShipperID int
, @ProductGroupID int
, @TruckTypeID int
, @OriginID int
, @OriginStateID int
, @DestinationID int
, @DestinationStateID int
, @ProducerID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID
			, Ranking = dbo.fnRateRanking(@TruckTypeID, R.TruckTypeID, 128, 0)
				  + dbo.fnRateRanking(@ShipperID, R.ShipperID, 64, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 32, 0)
				  + dbo.fnRateRanking(@OriginID, R.OriginID, 16, 0)
				  + dbo.fnRateRanking(@OriginStateID, R.OriginStateID, 8, 0)
				  + dbo.fnRateRanking(@DestinationID, R.DestinationID, 4, 0)
				  + dbo.fnRateRanking(@DestinationStateID, R.DestinationStateID, 2, 0)
				  + dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 1)
		FROM  dbo.viewShipperMinSettlementUnits R
		WHERE coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@TruckTypeID, 0), R.TruckTypeID, 0) = coalesce(TruckTypeID, nullif(@TruckTypeID, 0), 0)
		  AND coalesce(nullif(@OriginID, 0), R.OriginID, 0) = coalesce(OriginID, nullif(@OriginID, 0), 0)
		  AND coalesce(nullif(@OriginStateID, 0), R.OriginStateID, 0) = coalesce(OriginStateID, nullif(@OriginStateID, 0), 0)
		  AND coalesce(nullif(@DestinationID, 0), R.DestinationID, 0) = coalesce(DestinationID, nullif(@DestinationID, 0), 0)
		  AND coalesce(nullif(@DestinationStateID, 0), R.DestinationStateID, 0) = coalesce(DestinationStateID, nullif(@DestinationStateID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(ProducerID, nullif(@ProducerID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	)
	
	SELECT TOP (CASE WHEN @BestMatchOnly = 1 THEN 1 ELSE 10000 END) R.ID, ShipperID, ProductGroupID, TruckTypeID, OriginID, OriginStateID, DestinationID, DestinationStateID, ProducerID
	  , MinSettlementUnits, UomID, EffectiveDate, EndDate
	  , MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate
	  , BestMatch, Ranking
	  , Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewShipperMinSettlementUnits R
	JOIN (
		SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 7  -- ensure some type of match occurred on all criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)

GO


/***********************************
-- Date Created: 12 June 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper MinSettlementUnits rows for the specified criteria
-- Changes:
	- 3.7.29 - 2015/06/18 - KDA - ADDED
	- 3.10.13.5 - 2016/02/29 - JAE - Add truck type
***********************************/
ALTER FUNCTION [dbo].[fnShipperMinSettlementUnitsDisplay]
(
  @StartDate date
, @EndDate date
, @ShipperID int
, @ProductGroupID int
, @TruckTypeID int
, @OriginID int
, @OriginStateID int
, @DestinationID int
, @DestinationStateID int
, @ProducerID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ShipperID, R.ProductGroupID, R.TruckTypeID, R.OriginID, R.OriginStateID, R.DestinationID, R.DestinationStateID, R.ProducerID
		, R.MinSettlementUnits, R.UomID, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Shipper = SH.Name
		, ProductGroup = PG.Name
		, TruckType = TT.Name
		, Origin = O.Name
		, OriginFull = O.FullName
		, OriginState = O.State
		, OriginStateAbbrev = O.StateAbbrev
		, Destination = D.Name
		, DestinationFull = D.FullName
		, DestinationState = D.State
		, DestinationStateAbbrev = D.StateAbbrev
		, Producer = P.Name
		, Uom = U.Name
		, UomShort = U.Abbrev
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch, Ranking
	FROM dbo.fnShipperMinSettlementUnits(@StartDate, @EndDate, @ShipperID, @ProductGroupID, @TruckTypeID, @OriginID, @OriginStateID, @DestinationID, @DestinationStateID, @ProducerID, 0) R
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblTruckType TT ON TT.ID = R.TruckTypeID
	LEFT JOIN viewOrigin O ON O.ID = R.OriginID
	LEFT JOIN viewDestination D ON D.ID = R.DestinationID
	LEFT JOIN tblProducer P ON P.ID = R.ProducerID
	LEFT JOIN tblUom U ON U.ID = R.UomID
	ORDER BY EffectiveDate


GO


/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper OrderRejectRate info for the specified criteria
-- Changes:
	- 3.7.29 - 2015/06/18 - KDA - add ProducerID parameter/field as new Best-Match criteria
	- 3.10.13.5 - 2016/02/29 - JAE - Add truck type
***********************************/
ALTER FUNCTION [dbo].[fnShipperOrderRejectRate]
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @ProductGroupID int
, @TruckTypeID int
, @OriginID int
, @StateID int
, @RegionID int
, @ProducerID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID, ReasonID
			, Ranking =	dbo.fnRateRanking(@ReasonID, R.ReasonID, 128, 1)
				  + dbo.fnRateRanking(@ReasonID, R.ReasonID, 64, 1)
				  + dbo.fnRateRanking(@ShipperID, R.ShipperID, 32, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 16, 0)
				  + dbo.fnRateRanking(@OriginID, R.OriginID, 8, 0)
				  + dbo.fnRateRanking(@StateID, R.StateID, 4, 0)
				  + dbo.fnRateRanking(@RegionID, R.RegionID, 2, 0)
				  + dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 1)
		FROM  dbo.viewShipperOrderRejectRate R
		WHERE coalesce(nullif(@ReasonID, 0), R.ReasonID, 0) = coalesce(ReasonID, nullif(@ReasonID, 0), 0)
		  AND coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@TruckTypeID, 0), R.TruckTypeID, 0) = coalesce(TruckTypeID, nullif(@TruckTypeID, 0), 0)
		  AND coalesce(nullif(@OriginID, 0), R.OriginID, 0) = coalesce(OriginID, nullif(@OriginID, 0), 0)
		  AND coalesce(nullif(@StateID, 0), R.StateID, 0) = coalesce(StateID, nullif(@StateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(ProducerID, nullif(@ProducerID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	),
	cte2 AS
	(
		SELECT ID, S.ReasonID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT ReasonID = ISNULL(ReasonID, 0), Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 7  -- ensure some type of match occurred on all criteria choices
			GROUP BY ReasonID
		) X ON X.ReasonID = isnull(S.ReasonID, 0) AND X.Ranking = S.Ranking
	), 
	cte3 AS
	(	-- when both ReasonID = NULL AND ReasonID = @ReasonID records are BestMatch = 1, favor the ReasonID = @ReasonID record
		SELECT ID, X.ReasonID, X.Ranking, BestMatch = CASE WHEN BEST.ReasonID IS NULL THEN X.BestMatch ELSE 0 END
		FROM cte2 X
		LEFT JOIN (
			SELECT TOP 1 ReasonID FROM cte2 WHERE ReasonID = @ReasonID AND BestMatch = 1 ORDER BY Ranking DESC
		) BEST ON X.ReasonID IS NULL AND X.BestMatch = 1
	)
	
	SELECT R.ID, R.ReasonID, ShipperID, ProductGroupID, TruckTypeID, OriginID, StateID, RegionID, ProducerID, Rate, RateTypeID, UomID, EffectiveDate, EndDate
		, MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate
		, BestMatch, Ranking
		, Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewShipperOrderRejectRate R
	JOIN cte3 X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)

GO


/***********************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper OrderRejectRate rows for the specified criteria
-- Changes:
	- 3.7.29 - 2015/06/18 - KDA - add ProducerID parameter/field as new Best-Match criteria
	- 3.10.13.5 - 2016/02/29 - JAE - Add truck type
***********************************/
ALTER FUNCTION [dbo].[fnShipperOrderRejectRatesDisplay]
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @ProductGroupID int
, @TruckTypeID int
, @OriginID int
, @StateID int
, @RegionID int
, @ProducerID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ReasonID, R.ShipperID, R.ProductGroupID, R.TruckTypeID, R.OriginID, R.StateID, R.RegionID, R.ProducerID, R.Rate, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Shipper = C.Name
		, ProductGroup = PG.Name
		, TruckType = TT.Name
		, Origin = D.Name
		, OriginFull = D.FullName
		, State = S.FullName
		, StateAbbrev = S.Abbreviation
		, Region = REG.Name
		, Producer = PR.Name
		, Reason = REA.Description
		, RateType = RT.Name
		, Uom = U.Name
		, UomShort = U.Abbrev
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
	FROM dbo.fnShipperOrderRejectRate(@StartDate, @EndDate, @ReasonID, @ShipperID, @ProductGroupID, @TruckTypeID, @OriginID, @StateID, @RegionID, @ProducerID, 0) R
	LEFT JOIN tblCustomer C ON C.ID = R.ShipperID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblTruckType TT ON TT.ID = R.TruckTypeID
	LEFT JOIN viewOrigin D ON D.ID = R.OriginID
	LEFT JOIN tblState S ON S.ID = R.StateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblProducer PR ON PR.ID = R.ProducerID
	LEFT JOIN tblOrderRejectReason REA ON REA.ID = R.ReasonID
	LEFT JOIN tblRateType RT ON RT.ID = R.RateTypeID
	LEFT JOIN tblUom U ON U.ID = R.UomID
	ORDER BY EffectiveDate

GO


/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper OriginWaitRate info for the specified criteria
-- Changes:
	- 3.7.29 - 2015/06/18 - KDA - add ProducerID parameter/field as new Best-Match criteria
	- 3.10.13.5 - 2016/02/29 - JAE - Add truck type
***********************************/
ALTER FUNCTION [dbo].[fnShipperOriginWaitRate]
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @ProductGroupID int
, @TruckTypeID int
, @OriginID int
, @StateID int
, @RegionID int
, @ProducerID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID, ReasonID
			, Ranking =	dbo.fnRateRanking(@ReasonID, R.ReasonID, 128, 1)
				  + dbo.fnRateRanking(@TruckTypeID, R.TruckTypeID, 64, 1)
				  + dbo.fnRateRanking(@ShipperID, R.ShipperID, 32, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 16, 0)
				  + dbo.fnRateRanking(@OriginID, R.OriginID, 8, 0)
				  + dbo.fnRateRanking(@StateID, R.StateID, 4, 0)
				  + dbo.fnRateRanking(@RegionID, R.RegionID, 2, 0)
				  + dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 1)
		FROM  dbo.viewShipperOriginWaitRate R
		WHERE coalesce(nullif(@ReasonID, 0), R.ReasonID, 0) = coalesce(ReasonID, nullif(@ReasonID, 0), 0)
		  AND coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@TruckTypeID, 0), R.TruckTypeID, 0) = coalesce(TruckTypeID, nullif(@TruckTypeID, 0), 0)
		  AND coalesce(nullif(@OriginID, 0), R.OriginID, 0) = coalesce(OriginID, nullif(@OriginID, 0), 0)
		  AND coalesce(nullif(@StateID, 0), R.StateID, 0) = coalesce(StateID, nullif(@StateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(ProducerID, nullif(@ProducerID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	),
	cte2 AS
	(
		SELECT ID, S.ReasonID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT ReasonID = ISNULL(ReasonID, 0), Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 7  -- ensure some type of match occurred on all criteria choices
			GROUP BY ReasonID
		) X ON X.ReasonID = isnull(S.ReasonID, 0) AND X.Ranking = S.Ranking
	), 
	cte3 AS
	(	-- when both ReasonID = NULL AND ReasonID = @ReasonID records are BestMatch = 1, favor the ReasonID = @ReasonID record
		SELECT ID, X.ReasonID, X.Ranking, BestMatch = CASE WHEN BEST.ReasonID IS NULL THEN X.BestMatch ELSE 0 END
		FROM cte2 X
		LEFT JOIN (
			SELECT TOP 1 ReasonID FROM cte2 WHERE ReasonID = @ReasonID AND BestMatch = 1 ORDER BY Ranking DESC
		) BEST ON X.ReasonID IS NULL AND X.BestMatch = 1
	)
	
	SELECT TOP (CASE WHEN @BestMatchOnly = 1 THEN 1 ELSE 10000 END) R.ID, R.ReasonID, ShipperID, ProductGroupID, TruckTypeID, OriginID, StateID, RegionID, ProducerID, Rate, EffectiveDate, EndDate
		, MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate
		, BestMatch, Ranking
		, Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewShipperOriginWaitRate R
	JOIN cte3 X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
	ORDER BY Ranking DESC
)

GO


/***********************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper OriginWaitRate rows for the specified criteria
-- Changes:
	- 3.7.29 - 2015/06/18 - KDA - add ProducerID parameter/field as new Best-Match criteria
	- 3.10.13.5 - 2016/02/29 - JAE - Add truck type
***********************************/
ALTER FUNCTION [dbo].[fnShipperOriginWaitRatesDisplay]
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @ProductGroupID int
, @TruckTypeID int
, @OriginID int
, @StateID int
, @RegionID int
, @ProducerID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ReasonID, R.ShipperID, R.ProductGroupID, R.TruckTypeID, R.OriginID, R.StateID, R.RegionID, R.ProducerID, R.Rate, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Shipper = C.Name
		, ProductGroup = PG.Name
		, TruckType = TT.Name
		, Origin = D.Name
		, OriginFull = D.FullName
		, State = S.FullName
		, StateAbbrev = S.Abbreviation
		, Region = REG.Name
		, Producer = PR.Name
		, Reason = REA.Description
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
	FROM dbo.fnShipperOriginWaitRate(@StartDate, @EndDate, @ReasonID, @ShipperID, @ProductGroupID, @TruckTypeID, @OriginID, @StateID, @RegionID, @ProducerID, 0) R
	LEFT JOIN tblCustomer C ON C.ID = R.ShipperID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblTruckType TT ON TT.ID = R.TruckTypeID
	LEFT JOIN viewOrigin D ON D.ID = R.OriginID
	LEFT JOIN tblState S ON S.ID = R.StateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblProducer PR ON PR.ID = R.ProducerID
	LEFT JOIN tblOriginWaitReason REA ON REA.ID = R.ReasonID
	ORDER BY EffectiveDate

GO


/***********************************
-- Date Created: 5 May 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper RateSheetRangeRate info for the specified criteria
-- Changes:
	- 3.7.29 - 2015/06/18 - KDA - add ProducerID parameter/field as new Best-Match criteria
	- 3.10.13.5 - 2016/02/29 - JAE - Add truck type
***********************************/
ALTER FUNCTION [dbo].[fnShipperRateSheet](@StartDate date, @EndDate date, @ShipperID int, @ProductGroupID int, @TruckTypeID int, @OriginStateID int, @DestStateID int, @RegionID int, @ProducerID int, @BestMatchOnly bit = 0)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT R.ID
			, Ranking = dbo.fnRateRanking(@TruckTypeID, R.TruckTypeID, 64, 0)
				+ dbo.fnRateRanking(@ShipperID, R.ShipperID, 32, 0)
				+ dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 16, 0)
				+ dbo.fnRateRanking(@OriginStateID, R.OriginStateID, 8, 0)
				+ dbo.fnRateRanking(@DestStateID, R.DestStateID, 4, 0)
				+ dbo.fnRateRanking(@RegionID, R.RegionID, 2, 0)
				+ dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 1)
		FROM dbo.viewShipperRateSheet R
		WHERE coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@TruckTypeID, 0), R.TruckTypeID, 0) = coalesce(TruckTypeID, nullif(@TruckTypeID, 0), 0)
		  AND coalesce(nullif(@OriginStateID, 0), R.OriginStateID, 0) = coalesce(OriginStateID, nullif(@OriginStateID, 0), 0)
		  AND coalesce(nullif(@DestStateID, 0), R.DestStateID, 0) = coalesce(DestStateID, nullif(@DestStateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(ProducerID, nullif(@ProducerID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	)
	
	SELECT R.ID, RateSheetID = R.ID, ShipperID, ProductGroupID, TruckTypeID, OriginStateID, DestStateID, RegionID, ProducerID, RateTypeID, UomID, EffectiveDate, EndDate
		, MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate
		, BestMatch, Ranking
		, Locked, R.CreateDateUTC, R.CreatedByUser, R.LastChangeDateUTC, R.LastChangedByUser 
	FROM dbo.viewShipperRateSheet R
	JOIN (
		SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 6  -- ensure some type of match occurred on all criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)

GO


/***********************************
-- Date Created: 5 May 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper RateSheetRangeRate info for the specified criteria
-- Changes:
	- 3.7.29 - 2015/06/18 - KDA - add ProducerID parameter/field as new Best-Match criteria
	- 3.10.13.5 - 2016/02/29 - JAE - Add truck type
***********************************/
ALTER FUNCTION [dbo].[fnShipperRateSheetDisplay](@StartDate date, @EndDate date, @ShipperID int, @ProductGroupID int, @TruckTypeID int, @OriginStateID int, @DestStateID int, @RegionID int, @ProducerID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ShipperID, R.ProductGroupID, R.TruckTypeID, R.ProducerID, R.OriginStateID, R.DestStateID, R.RegionID, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Shipper = S.Name
		, ProductGroup = PG.Name
		, TruckType = TT.Name
		, OriginState = OS.FullName
		, OriginStateAbbrev = OS.Abbreviation
		, DestState = OS.FullName
		, DestStateAbbrev = OS.Abbreviation
		, Region = REG.Name
		, Producer = P.Name
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
		, Ranking
	FROM dbo.fnShipperRateSheet(@StartDate, @EndDate, @ShipperID, @ProductGroupID, @TruckTypeID, @OriginStateID, @DestStateID, @RegionID, @ProducerID, 0) R
	LEFT JOIN tblCustomer S ON S.ID = R.ShipperID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblTruckType TT ON TT.ID = R.TruckTypeID
	LEFT JOIN tblState OS ON OS.ID = R.OriginStateID
	LEFT JOIN tblState DS ON DS.ID = R.DestStateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblProducer P ON P.ID = R.ProducerID
	ORDER BY EffectiveDate

GO


/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper RateSheetRangeRate info for the specified criteria
-- Changes
	- 3.10.13.5 - 2016/02/29 - JAE - Add truck type
***********************************/
ALTER FUNCTION [dbo].[fnShipperRateSheetRangeRate](@StartDate date, @EndDate date, @RouteMiles int, @ShipperID int, @ProductGroupID int, @TruckTypeID int, @ProducerID int, @OriginStateID int, @DestStateID int, @RegionID int, @BestMatchOnly bit = 0)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT RateSheetID = R.ID, RangeRateID = RR.ID
			-- the manually added .01 is normally added via the fnRateRanking routine
			, Ranking = CASE WHEN @RouteMiles BETWEEN RR.MinRange AND RR.MaxRange THEN 128.01 ELSE 0 END
				+ dbo.fnRateRanking(@TruckTypeID, R.TruckTypeID, 64, 0)
				+ dbo.fnRateRanking(@ShipperID, R.ShipperID, 32, 0)
				+ dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 16, 0)
				+ dbo.fnRateRanking(@ProducerID, R.ProducerID, 8, 0)
				+ dbo.fnRateRanking(@OriginStateID, R.OriginStateID, 4, 0)
				+ dbo.fnRateRanking(@DestStateID, R.DestStateID, 2, 0)
				+ dbo.fnRateRanking(@RegionID, R.RegionID, 1, 0)
		FROM dbo.viewShipperRateSheet R
		JOIN dbo.tblShipperRangeRate RR ON RR.RateSheetID = R.ID AND isnull(@RouteMiles, RR.MinRange) BETWEEN RR.MinRange AND RR.MaxRange
		WHERE coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@TruckTypeID, 0), R.TruckTypeID, 0) = coalesce(TruckTypeID, nullif(@TruckTypeID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(ProducerID, nullif(@ProducerID, 0), 0)
		  AND coalesce(nullif(@OriginStateID, 0), R.OriginStateID, 0) = coalesce(OriginStateID, nullif(@OriginStateID, 0), 0)
		  AND coalesce(nullif(@DestStateID, 0), R.DestStateID, 0) = coalesce(DestStateID, nullif(@DestStateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	)
	
	SELECT RR.ID, RateSheetID = R.ID, ShipperID, ProductGroupID, TruckTypeID, ProducerID, OriginStateID, DestStateID, RegionID, Rate, MinRange, MaxRange, RateTypeID, UomID, EffectiveDate, EndDate
		, MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate
		, BestMatch, Ranking
		, Locked, R.CreateDateUTC, R.CreatedByUser, R.LastChangeDateUTC, R.LastChangedByUser 
	FROM dbo.viewShipperRateSheet R
	JOIN dbo.tblShipperRangeRate RR ON RR.RateSheetID = R.ID
	JOIN (
		SELECT RateSheetID, RangeRateID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 7  -- ensure some type of match occurred on all 6 criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.RateSheetID = R.ID AND X.RangeRateID = RR.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)

GO


/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper RateSheetRangeRate info for the specified criteria
-- Changes
	- 3.10.13.5 - 2016/02/29 - JAE - Add truck type
***********************************/
ALTER FUNCTION [dbo].[fnShipperRateSheetRangeRatesDisplay](@StartDate date, @EndDate date, @RouteMiles int, @ShipperID int, @ProductGroupID int, @TruckTypeID int, @ProducerID int, @OriginStateID int, @DestStateID int, @RegionID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.RateSheetID, R.ShipperID, R.ProductGroupID, R.TruckTypeID, R.ProducerID, R.OriginStateID, R.DestStateID, R.RegionID, R.MinRange, R.MaxRange, R.Rate, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Shipper = S.Name
		, ProductGroup = PG.Name
		, TruckType = TT.Name
		, Producer = P.Name
		, OriginState = OS.FullName
		, OriginStateAbbrev = OS.Abbreviation
		, DestState = OS.FullName
		, DestStateAbbrev = OS.Abbreviation
		, Region = REG.Name
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
		, Ranking
	FROM dbo.fnShipperRateSheetRangeRate(@StartDate, @EndDate, @RouteMiles, @ShipperID, @ProductGroupID, @TruckTypeID, @ProducerID, @OriginStateID, @DestStateID, @RegionID, 0) R
	LEFT JOIN tblCustomer S ON S.ID = R.ShipperID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblTruckType TT ON TT.ID = R.TruckTypeID
	LEFT JOIN tblProducer P ON P.ID = R.ProducerID
	LEFT JOIN tblState OS ON OS.ID = R.OriginStateID
	LEFT JOIN tblState DS ON DS.ID = R.DestStateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	ORDER BY EffectiveDate

GO


/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper RouteRate info for the specified criteria
-- Changes
	- 3.10.13.5 - 2016/02/29 - JAE - Add truck type
***********************************/
ALTER FUNCTION [dbo].[fnShipperRouteRate](@StartDate date, @EndDate date, @OriginID int, @DestinationID int, @ShipperID int, @ProductGroupID int, @TruckTypeID int, @BestMatchOnly bit = 0) 
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT R.ID
			, Ranking =	dbo.fnRateRanking(@TruckTypeID, R.TruckTypeID, 4, 0)
			      + dbo.fnRateRanking(@ShipperID, R.ShipperID, 2, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 1, 0)
		FROM dbo.viewShipperRouteRate R
		JOIN tblRoute RO ON RO.ID = R.RouteID
		WHERE (nullif(@OriginID, 0) IS NULL OR @OriginID = RO.OriginID)
		  AND (nullif(@DestinationID, 0) IS NULL OR @DestinationID = RO.DestinationID)
		  AND coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@TruckTypeID, 0), R.TruckTypeID, 0) = coalesce(TruckTypeID, nullif(@TruckTypeID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	)
	
	SELECT R.ID, RouteID, OriginID, DestinationID, ShipperID, ProductGroupID, TruckTypeID, Rate, RateTypeID, UomID, EffectiveDate, EndDate
		, MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate
		, BestMatch, Ranking
		, Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewShipperRouteRate R
	JOIN (
		SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 2  -- ensure some type of match occurred on all 6 criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)

GO


/***********************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper RouteRate rows for the specified criteria
-- Changes
	- 3.10.13.5 - 2016/02/29 - JAE - Add truck type
***********************************/
ALTER FUNCTION [dbo].[fnShipperRouteRatesDisplay](@StartDate date, @EndDate date, @OriginID int, @DestinationID int, @ShipperID int, @ProductGroupID int, @TruckTypeID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.RouteID, RO.ActualMiles, R.OriginID, R.DestinationID, R.ShipperID, R.ProductGroupID, R.TruckTypeID, R.Rate, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Origin = O.Name
		, OriginFull = O.FullName
		, Destination = D.Name
		, DestinationFull = D.FullName
		, Shipper = C.Name
		, ProductGroup = PG.Name
		, TruckType = TT.Name
		, RateType = RT.Name
		, Uom = U.Name
		, UomShort = U.Abbrev
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch, Ranking
	FROM dbo.fnShipperRouteRate(@StartDate, @EndDate, @OriginID, @DestinationID, @ShipperID, @ProductGroupID, @TruckTypeID, 0) R
	JOIN tblRoute RO ON RO.ID = R.RouteID
	JOIN viewOrigin O ON O.ID = R.OriginID
	JOIN viewDestination D ON D.ID = R.DestinationID
	LEFT JOIN tblCustomer C ON C.ID = R.ShipperID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblTruckType TT ON TT.ID = R.TruckTypeID
	LEFT JOIN tblRateType RT ON RT.ID = R.RateTypeID
	LEFT JOIN tblUom U ON U.ID = R.UomID
	ORDER BY EffectiveDate

GO


/***********************************
-- Date Created: 12 June 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper SettlementFactor info for the specified criteria
-- Changes:
	- 3.7.29 - 2015/06/18 - KDA - ADDED
	- 3.10.13.5 - 2016/02/29 - JAE - Add truck type
***********************************/
ALTER FUNCTION [dbo].[fnShipperSettlementFactor]
(
  @StartDate date
, @EndDate date
, @ShipperID int
, @ProductGroupID int
, @TruckTypeID int
, @OriginID int
, @OriginStateID int
, @DestinationID int
, @DestinationStateID int
, @ProducerID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID
			, Ranking =	dbo.fnRateRanking(@TruckTypeID, R.TruckTypeID, 128, 0)
				  + dbo.fnRateRanking(@ShipperID, R.ShipperID, 64, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 32, 0)
				  + dbo.fnRateRanking(@OriginID, R.OriginID, 16, 0)
				  + dbo.fnRateRanking(@OriginStateID, R.OriginStateID, 8, 0)
				  + dbo.fnRateRanking(@DestinationID, R.DestinationID, 4, 0)
				  + dbo.fnRateRanking(@DestinationStateID, R.DestinationStateID, 2, 0)
				  + dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 1)
		FROM  dbo.viewShipperSettlementFactor R
		WHERE coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@TruckTypeID, 0), R.TruckTypeID, 0) = coalesce(TruckTypeID, nullif(@TruckTypeID, 0), 0)
		  AND coalesce(nullif(@OriginID, 0), R.OriginID, 0) = coalesce(OriginID, nullif(@OriginID, 0), 0)
		  AND coalesce(nullif(@OriginStateID, 0), R.OriginStateID, 0) = coalesce(OriginStateID, nullif(@OriginStateID, 0), 0)
		  AND coalesce(nullif(@DestinationID, 0), R.DestinationID, 0) = coalesce(DestinationID, nullif(@DestinationID, 0), 0)
		  AND coalesce(nullif(@DestinationStateID, 0), R.DestinationStateID, 0) = coalesce(DestinationStateID, nullif(@DestinationStateID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(ProducerID, nullif(@ProducerID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	)
	
	SELECT TOP (CASE WHEN @BestMatchOnly = 1 THEN 1 ELSE 10000 END) R.ID, ShipperID, ProductGroupID, TruckTypeID, OriginID, OriginStateID, DestinationID, DestinationStateID, ProducerID
	  , SettlementFactorID, EffectiveDate, EndDate
	  , MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate
	  , BestMatch, Ranking
	  , Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewShipperSettlementFactor R
	JOIN (
		SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 7  -- ensure some type of match occurred on all criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)

GO


/***********************************
-- Date Created: 12 June 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper SettlementFactor rows for the specified criteria
-- Changes:
	- 3.7.29 - 2015/06/18 - KDA - ADDED
	- 3.10.13.5 - 2016/02/29 - JAE - Add truck type
***********************************/
ALTER FUNCTION [dbo].[fnShipperSettlementFactorDisplay]
(
  @StartDate date
, @EndDate date
, @ShipperID int
, @ProductGroupID int
, @TruckTypeID int
, @OriginID int
, @OriginStateID int
, @DestinationID int
, @DestinationStateID int
, @ProducerID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ShipperID, R.ProductGroupID, R.TruckTypeID, R.OriginID, R.OriginStateID, R.DestinationID, R.DestinationStateID, R.ProducerID
		, R.SettlementFactorID, R.EffectiveDate, R.EndDate
		, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, SettlementFactor = SF.Name
		, Shipper = SH.Name
		, ProductGroup = PG.Name
		, TruckType = TT.Name
		, Origin = O.Name
		, OriginFull = O.FullName
		, OriginState = O.State
		, OriginStateAbbrev = O.StateAbbrev
		, Destination = D.Name
		, DestinationFull = D.FullName
		, DestinationState = D.State
		, DestinationStateAbbrev = D.StateAbbrev
		, Producer = P.Name
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch, Ranking
	FROM dbo.fnShipperSettlementFactor(@StartDate, @EndDate, @ShipperID, @ProductGroupID, @TruckTypeID, @OriginID, @OriginStateID, @DestinationID, @DestinationStateID, @ProducerID, 0) R
	JOIN dbo.tblSettlementFactor SF ON SF.ID = R.SettlementFactorID
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblCustomer C ON C.ID = R.ShipperID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblTruckType TT ON TT.ID = R.TruckTypeID
	LEFT JOIN viewOrigin O ON O.ID = R.OriginID
	LEFT JOIN viewDestination D ON D.ID = R.DestinationID
	LEFT JOIN tblProducer P ON P.ID = R.ProducerID
	ORDER BY EffectiveDate


GO


/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper WaitFeeParameter info for the specified order
-- Changes:
	- 3.7.29 - 2015/06/18 - KDA - add ProducerID parameter/field as new Best-Match criteria
	- 3.10.13.5 - 2016/02/29 - JAE - Add truck type
***********************************/
ALTER FUNCTION [dbo].[fnShipperWaitFeeParameter](@StartDate date, @EndDate date, @ShipperID int, @ProductGroupID int, @TruckTypeID int, @OriginStateID int, @DestStateID int, @RegionID int, @ProducerID int, @bestMatchOnly bit = 0)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID
			, Ranking = dbo.fnRateRanking(@TruckTypeID, R.TruckTypeID, 64, 0)
				  + dbo.fnRateRanking(@ShipperID, R.ShipperID, 32, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 16, 0)
				  + dbo.fnRateRanking(@OriginStateID, R.OriginStateID, 8, 0)
				  + dbo.fnRateRanking(@DestStateID, R.DestStateID, 4, 0)
				  + dbo.fnRateRanking(@RegionID, R.RegionID, 2, 0)
				  + dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 1)
		FROM  dbo.viewShipperWaitFeeParameter R
		WHERE coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@TruckTypeID, 0), R.TruckTypeID, 0) = coalesce(TruckTypeID, nullif(@TruckTypeID, 0), 0)
		  AND coalesce(nullif(@OriginStateID, 0), R.OriginStateID, 0) = coalesce(OriginStateID, nullif(@OriginStateID, 0), 0)
		  AND coalesce(nullif(@DestStateID, 0), R.DestStateID, 0) = coalesce(DestStateID, nullif(@DestStateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(ProducerID, nullif(@ProducerID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	)
	
	SELECT R.ID, ShipperID, ProductGroupID, TruckTypeID, OriginStateID, DestStateID, RegionID, ProducerID, EffectiveDate, EndDate
		, SubUnitID, RoundingTypeID, OriginThresholdMinutes, DestThresholdMinutes, OriginMinBillableMinutes, OriginMaxBillableMinutes, DestMinBillableMinutes, DestMaxBillableMinutes
		, MaxEffectiveDate, MinEndDate, NextEffectiveDate, PriorEndDate
		, BestMatch, Ranking
		, Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewShipperWaitFeeParameter R
	JOIN (
		SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 6  -- ensure some type of match occurred on all criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)

GO


/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper WaitFeeParameter rows for the specified criteria
-- Changes:
	- 3.7.29 - 2015/06/18 - KDA - add ProducerID parameter/field as new Best-Match criteria
	- 3.7.35 - 2015/06/23 - KDA - add missing EffectiveDate, EndDate returned columns
	- 3.10.13.5 - 2016/02/29 - JAE - Add truck type
***********************************/
ALTER FUNCTION [dbo].[fnShipperWaitFeeParametersDisplay](@StartDate date, @EndDate date, @ShipperID int, @ProductGroupID int, @TruckTypeID int, @OriginStateID int, @DestStateID int, @RegionID int, @ProducerID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ShipperID, R.ProductGroupID, R.TruckTypeID, R.OriginStateID, R.DestStateID, R.RegionID, R.ProducerID
		, R.SubUnitID, R.RoundingTypeID, R.OriginThresholdMinutes, R.DestThresholdMinutes
		, R.OriginMinBillableMinutes, R.OriginMaxBillableMinutes, R.DestMinBillableMinutes, R.DestMaxBillableMinutes
		, R.EffectiveDate, R.EndDate, R.MaxEffectiveDate, R.MinEndDate, R.NextEffectiveDate, R.PriorEndDate
		, Shipper = C.Name
		, ProductGroup = PG.Name
		, TruckType = TT.Name
		, OriginState = OS.FullName
		, OriginStateAbbrev = OS.Abbreviation
		, DestinationState = DS.FullName
		, DestinationStateAbbrev = DS.Abbreviation
		, Region = REG.Name
		, Producer = PR.Name
		, RoundingType = WFRT.Name
		, SubUnit = WFSU.Name
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch, Ranking
	FROM dbo.fnShipperWaitFeeParameter(@StartDate, @EndDate, @ShipperID, @ProductGroupID, @TruckTypeID, @OriginStateID, @DestStateID, @RegionID, @ProducerID, 0) R 
	JOIN tblWaitFeeRoundingType WFRT ON WFRT.ID = R.RoundingTypeID
	JOIN tblWaitFeeSubUnit WFSU ON WFSU.ID = R.SubUnitID
	LEFT JOIN tblCustomer C ON C.ID = R.ShipperID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblTruckType TT ON TT.ID = R.TruckTypeID
	LEFT JOIN tblState OS ON OS.ID = R.OriginStateID
	LEFT JOIN tblState DS ON DS.ID = R.DestStateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblProducer PR ON PR.ID = R.ProducerID
	ORDER BY EffectiveDate

GO



COMMIT
SET NOEXEC OFF