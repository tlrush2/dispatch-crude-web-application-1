-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.6.1'
SELECT  @NewVersion = '4.6.2'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'PDF Export: add ability to generate Driver Settlement Statements'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

alter table tblCarrierSettlementBatch Add PeriodEndDate date not null constraint DF_CarrierSettlementBatch_PeriodEndDate DEFAULT (getdate())
go
alter table tblDriverSettlementBatch Add PeriodEndDate date not null constraint DF_DriverSettlementBatch_PeriodEndDate DEFAULT (getdate())
go
alter table tblShipperSettlementBatch Add PeriodEndDate date not null constraint DF_ShipperSettlementBatch_PeriodEndDate DEFAULT (getdate())
go
exec _spRefreshAllViews
go

alter table tblOrderSettlementCarrier add Notes varchar(255)
go
alter table tblOrderSettlementDriver add Notes varchar(255)
go
alter table tblOrderSettlementShipper add Notes varchar(255)
go

exec _spRefreshAllViews
go

create table tblPdfExportType
(
  ID int identity(1000,1) NOT NULL constraint PK_PdfExportType primary key clustered 
, Name varchar(30) NOT NULL constraint CKU_PdfExportType_Name unique nonclustered
, CreateDateUTC datetime NOT NULL constraint DF_PdfExportType_CreateDateUTC  default (getutcdate())
, CreatedByUser varchar(100) NOT NULL constraint DF_PdfExportType_CreatedByUser  default ('System')
, LastChangeDateUTC datetime NULL
, LastChangedByUser varchar(100) NULL
, DeleteDateUTC datetime NULL
, DeletedByUser varchar(100) NULL
) 
GO

set identity_insert tblPdfExportType ON
insert into tblPdfExportType (ID, Name) values (1, 'Driver Settlement Statement')
go
set identity_insert tblPdfExportType OFF

create TABLE tblPdfExport
(
  ID int identity(1,1) NOT NULL constraint PK_PdfExport primary key nonclustered 
  , Name varchar(30) NOT NULL constraint CKU_PdfExport_Name unique nonclustered 
  , PdfExportTypeID int NOT NULL constraint FK_PdfExport_PdfExportType foreign key references tblPdfExportType(ID)
  , ShipperID int NULL constraint FK_PdfExport_Shipper foreign key references tblCustomer(ID)
  , CarrierID int NULL constraint FK_PdfExport_Carrier foreign key references tblCarrier(ID)
  , ProducerID int NULL constraint FK_PdfExport_Producer foreign key references tblProducer(ID)
  , ProductID int NULL constraint FK_PdfExport_Product foreign key references tblProduct(ID)
  , WordDocument varbinary(max) NOT NULL
  , WordDocName varchar(100) NOT NULL
  , CreateDateUTC datetime NOT NULL constraint DF_PdfExport_CreateDateUTC  default (getutcdate())
  , CreatedByUser varchar(100) NOT NULL constraint DF_PdfExport_CreatedByUser  default ('System')
  , LastChangeDateUTC datetime NULL
  , LastChangedByUser varchar(100) NULL
  , DeleteDateUTC datetime NULL
  , DeletedByUser varchar(100) NULL,
)
GO

create unique nonclustered index udxPdfExport_Main on tblPdfExport ( ShipperID, CarrierID, ProducerID,ProductID )
GO

exec spAddNewPermission 'viewPdfExportType', 'Users in this role can view Pdf Export Types', 'Pdf Export', 'View Pdf Export Types'
exec spAddNewPermission 'createPdfExportType', 'Users in this role can create Pdf Export Types', 'Pdf Export', 'Create Pdf Export Types'
exec spAddNewPermission 'editPdfExportType', 'Users in this role can edit Pdf Export Types', 'Pdf Export', 'Edit Pdf Export Types'
exec spAddNewPermission 'deactivatePdfExportType', 'Users in this role can deactivate Pdf Export Types', 'Pdf Export', 'Deactivate Pdf Export Types'

exec spAddNewPermission 'viewPdfExport', 'Users in this role can view Pdf Exports', 'Pdf Export', 'View Pdf Exports'
exec spAddNewPermission 'createPdfExport', 'Users in this role can create Pdf Exports', 'Pdf Export', 'Create Pdf Exports'
exec spAddNewPermission 'editPdfExport', 'Users in this role can edit Pdf Exports', 'Pdf Export', 'Edit Pdf Exports'
exec spAddNewPermission 'deactivatePdfExport', 'Users in this role can deactivate Pdf Exports', 'Pdf Export', 'Deactivate Pdf Exports'
go

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details + FINANCIAL INFO into a single view
-- Changes:
--  3.7.28	- 2015/06/18 - KDA	- add translated "SettlementFactor" column
--  4.4.1	- 2016/11/04 - JAE 	- Add Destination Chainup
--  4.4.1.2	- 2016.11.17 - KDA	- update "other" assessorial rates JOIN to use new IsSystem flag (instead of 1,2,3,4 criteria)
--  4.6.2	- 2017.04.20 - KDA	- add BatchDate, PeriodEndDate
/***********************************/
ALTER VIEW viewOrderSettlementCarrier AS 
	SELECT OSC.*
		, SB.BatchNum
		, SB.BatchDate
		, SettlementUom = UOM.Name
		, SettlementUomShort = UOM.Abbrev
		, RouteRate = RR.Rate
		, RouteRateType = RRT.Name
		, RateSheetRate = RSRR.Rate
		, RateSheetRateType = RSRT.Name
		, OrderRejectRate = ORR.Rate
		, OrderRejectRateType = ORRT.Name
		, OriginWaitRate = OWR.Rate
		, DestinationWaitRate = DWR.Rate
		, TotalWaitAmount = cast(isnull(OSC.OriginWaitAmount, 0) as money) + cast(isnull(OSC.DestinationWaitAmount, 0) as money)
		, OriginChainupRate = CAR.Rate
		, OriginChainupRateType = CART.Name
		, OriginChainupAmount = CA.Amount
		, DestChainupRate = DCAR.Rate
		, DestChainupRateType = DCART.Name
		, DestChainupAmount = DCA.Amount
		, RerouteRate = RAR.Rate
		, RerouteRateType = RART.Name
		, RerouteAmount = RA.Amount
		, SplitLoadRate = SAR.Rate
		, SplitLoadRateType = SART.Name
		, SplitLoadAmount = SA.Amount
		, H2SRate = HAR.Rate
		, H2SRateType = HART.Name
		, H2SAmount = HA.Amount
		, OtherAmount = OA.Amount
		, WaitFeeSubUnit = isnull(WFSU.Name, 'None') 
		, WaitFeeRoundingType = isnull(WFRT.Name, 'None')
		, SettlementFactor = SF.Name
		, SB.PeriodEndDate
	FROM tblOrderSettlementCarrier OSC 
	LEFT JOIN tblCarrierSettlementBatch SB ON SB.ID = OSC.BatchID
	LEFT JOIN tblUom UOM ON UOM.ID = OSC.SettlementUomID
	LEFT JOIN tblCarrierOriginWaitRate OWR ON OWR.ID = OSC.OriginWaitRateID
	LEFT JOIN tblCarrierDestinationWaitRate DWR ON DWR.ID = OSC.DestinationWaitRateID
	LEFT JOIN tblCarrierOrderRejectRate ORR ON ORR.ID = OSC.OrderRejectRateID
	LEFT JOIN tblRateType ORRT ON ORRT.ID = ORR.RateTypeID
	-- route rate/ratesheet JOINs
	LEFT JOIN tblCarrierRouteRate RR ON RR.ID = OSC.RouteRateID
	LEFT JOIN tblRateType RRT ON RRT.ID = RR.RateTypeID
	LEFT JOIN viewCarrierRateSheetRangeRate RSRR ON RSRR.ID = OSC.RangeRateID
	LEFT JOIN tblRateType RSRT ON RSRT.ID = RSRR.RateTypeID
	-- chainup
	LEFT JOIN tblOrderSettlementCarrierAssessorialCharge CA ON CA.OrderID = OSC.OrderID AND CA.AssessorialRateTypeID = 1
	LEFT JOIN tblCarrierAssessorialRate CAR ON CAR.ID = CA.AssessorialRateID
	LEFT JOIN tblRateType CART ON CART.ID = CAR.RateTypeID
	LEFT JOIN tblOrderSettlementCarrierAssessorialCharge DCA ON DCA.OrderID = OSC.OrderID AND DCA.AssessorialRateTypeID = 5
	LEFT JOIN tblCarrierAssessorialRate DCAR ON DCAR.ID = DCA.AssessorialRateID
	LEFT JOIN tblRateType DCART ON DCART.ID = DCAR.RateTypeID
	-- Reroute
	LEFT JOIN tblOrderSettlementCarrierAssessorialCharge RA ON RA.OrderID = OSC.OrderID AND RA.AssessorialRateTypeID = 2
	LEFT JOIN tblCarrierAssessorialRate RAR ON RAR.ID = RA.AssessorialRateID
	LEFT JOIN tblRateType RART ON RART.ID = RAR.RateTypeID
	-- Split Load
	LEFT JOIN tblOrderSettlementCarrierAssessorialCharge SA ON SA.OrderID = OSC.OrderID AND SA.AssessorialRateTypeID = 3
	LEFT JOIN tblCarrierAssessorialRate SAR ON SAR.ID = SA.AssessorialRateID
	LEFT JOIN tblRateType SART ON SART.ID = SAR.RateTypeID
	-- H2S
	LEFT JOIN tblOrderSettlementCarrierAssessorialCharge HA ON HA.OrderID = OSC.OrderID AND HA.AssessorialRateTypeID = 4
	LEFT JOIN tblCarrierAssessorialRate HAR ON HAR.ID = HA.AssessorialRateID
	LEFT JOIN tblRateType HART ON HART.ID = HAR.RateTypeID
	-- Other Assessorial Charges (combined)
	LEFT JOIN (
		SELECT OrderID, Amount = SUM(Amount) 
		FROM tblOrderSettlementCarrierAssessorialCharge 
		WHERE AssessorialRateTypeID IN (SELECT ID FROM tblAssessorialRateType WHERE IsSystem = 0) -- 4.4.1.2 - use tblAssessorialRateType.IsSystem
		GROUP BY OrderID
	) OA ON OA.OrderID = OSC.OrderID 
	LEFT JOIN tblSettlementFactor SF ON SF.ID = OSC.SettlementFactorID
	LEFT JOIN tblCarrierWaitFeeParameter WFP ON WFP.ID = OSC.WaitFeeParameterID
	LEFT JOIN dbo.tblWaitFeeSubUnit WFSU ON WFSU.ID = WFP.SubUnitID
	LEFT JOIN dbo.tblWaitFeeRoundingType WFRT ON WFRT.ID = WFP.RoundingTypeID

GO

/***********************************/
-- Created: 4.1.0 - 2016/08/15 - Kevin Alons
-- Purpose: return BASE Order details + Carrier FINANCIAL INFO into a single view
-- Changes:
--  4.1.0.2 - 2016.08.28 - KDA	- remove redundant calculation of ORR.RerouteCount
--								- remove redundant tblOrigin JOIN (for H2S which is already available in viewOrder)
--  4.1.8.2 - 2016.09.21 - KDA	- Check Final Actual miles (null actual miles ok as long as override)
--  4.1.23	- 2016.10.17 - JAE	- Add weight units for settlement
--  4.4.1	- 2016/11/04 - JAE	- Add Destination Chainup
--  4.6.2	- 2017.04.21 - KDA	- Add InvoiceBatchDate, InvoiceBatchNum, InvoicePeriodEnd, InvoiceNotes fields
/***********************************/
ALTER VIEW viewOrder_Financial_Base_Carrier AS 
	SELECT *
		, HasError = cast(CASE WHEN ErrorFieldCSV IS NOT NULL THEN 1 ELSE 0 END as bit)
	FROM (
		SELECT *
			, OrderID = ID
			, ErrorFieldCSV = nullif(substring(CASE WHEN Approved = 0 THEN ',Approved' ELSE '' END
				+ CASE WHEN InvoiceLoadAmount IS NULL THEN ',InvoiceLoadAmount,InvoiceRouteRate,InvoiceRateSheet' ELSE '' END
				+ CASE WHEN FinalOriginChainup = 1 AND InvoiceOriginChainupAmount IS NULL THEN ',InvoiceOriginChainupAmount' ELSE '' END
				+ CASE WHEN FinalDestChainup = 1 AND InvoiceDestChainupAmount IS NULL THEN ',InvoiceDestChainupAmount' ELSE '' END
				+ CASE WHEN FinalRerouteCount > 0 AND InvoiceRerouteAmount IS NULL THEN ',InvoiceRerouteAmount' ELSE '' END
				+ CASE WHEN FinalH2S = 1 AND InvoiceH2SAmount IS NULL THEN ',InvoiceH2SAmount' ELSE '' END
				+ CASE WHEN TicketCount > 1 AND InvoiceSplitLoadAmount IS NULL THEN ',InvoiceSplitLoadAmount' ELSE '' END
				+ CASE WHEN InvoiceOriginWaitBillableMinutes > 0 AND InvoiceOriginWaitAmount IS NULL THEN ',InvoiceOriginWaitAmount' ELSE '' END
				+ CASE WHEN InvoiceDestinationWaitBillableMinutes > 0 AND InvoiceDestinationWaitAmount IS NULL THEN ',InvoiceDestinationWaitAmount' ELSE '' END
				+ CASE WHEN Rejected = 1 AND InvoiceOrderRejectAmount IS NULL THEN ',InvoiceOrderRejectAmount' ELSE '' END
				+ CASE WHEN InvoiceMinSettlementUnitsID IS NULL THEN ',InvoiceMinSettlementUnits' ELSE '' END
				+ CASE WHEN InvoiceSettlementFactorID IS NULL THEN ',InvoiceSettlementFactor' ELSE '' END
				+ CASE WHEN Rejected = 0 AND InvoiceSettlementFactorID IS NULL THEN ',InvoiceSettlementFactor' ELSE '' END
				+ CASE WHEN Rejected = 0 AND InvoiceSettlementFactorID IS NOT NULL AND dbo.fnOrderSettlementUnits(InvoiceSettlementFactorID, OriginGrossUnits, OriginNetUnits, OriginGrossStdUnits, DestGrossUnits, DestNetUnits, OriginWeightNetUnits, DestWeightNetUnits) IS NULL THEN ','+OrderUnitsField ELSE '' END
				+ CASE WHEN InvoiceWaitFeeParameterID IS NULL THEN ',InvoiceOriginWaitBillableMinutes,InvoiceDestinationWaitBillableMinutes' ELSE '' END
				+ CASE WHEN isnull(FinalActualMiles, 0) = 0 THEN ',ActualMiles' ELSE '' END, 2, 100000), '')
		FROM (
			SELECT O.* 
				, InvoiceRatesAppliedDateUTC = OS.CreateDateUTC
				, OS.BatchID
				, Approved = cast(ISNULL(OA.Approved, 0) as bit)
				, FinalActualMiles = ISNULL(OA.OverrideActualMiles, O.ActualMiles)
				, FinalOriginMinutes = ISNULL(OA.OverrideOriginMinutes, O.OriginMinutes)
				, FinalDestMinutes = ISNULL(OA.OverrideDestMinutes, O.DestMinutes)
				, FinalH2S = cast(CASE WHEN isnull(OA.OverrideH2S, 0) = 1 THEN 0 ELSE O.H2S END as bit)
				, FinalOriginChainup = cast(CASE WHEN isnull(OA.OverrideOriginChainup, 0) = 1 THEN 0 ELSE O.OriginChainup END as bit)
				, FinalDestChainup = cast(CASE WHEN isnull(OA.OverrideDestChainup, 0) = 1 THEN 0 ELSE O.DestChainup END as bit)
				, FinalRerouteCount = CASE WHEN isnull(OA.OverrideReroute, 0) = 1 THEN 0 ELSE isnull(O.RerouteCount, 0) END
				, FC.OrderUnitsField
				, InvoiceBatchNum = OS.BatchNum 
				, InvoiceBatchDate = OS.BatchDate
				, InvoiceOriginWaitBillableMinutes = OS.OriginWaitBillableMinutes
				, InvoiceDestinationWaitBillableMinutes = OS.DestinationWaitBillableMinutes
				, InvoiceTotalWaitBillableMinutes = isnull(OS.OriginWaitBillableMinutes, 0) + ISNULL(OS.DestinationWaitBillableMinutes, 0) 
				, InvoiceWaitFeeParameterID = WaitFeeParameterID
				, InvoiceWaitFeeSubUnit = WaitFeeSubUnit
				, InvoiceWaitFeeRoundingType = WaitFeeRoundingType
				, InvoiceOriginWaitRate = OS.OriginWaitRate 
				, InvoiceOriginWaitAmount = OS.OriginWaitAmount 		
				, InvoiceDestinationWaitRate = OS.DestinationWaitRate 
				, InvoiceDestinationWaitAmount = OS.DestinationWaitAmount 
				, InvoiceTotalWaitAmount = OS.TotalWaitAmount
				, InvoiceOrderRejectRate = OS.OrderRejectRate	
				, InvoiceOrderRejectRateType = OS.OrderRejectRateType  
				, InvoiceOrderRejectAmount = OS.OrderRejectAmount  
				, InvoiceOriginChainupRate = OS.OriginChainupRate
				, InvoiceOriginChainupRateType = OS.OriginChainupRateType
				, InvoiceOriginChainupAmount = OS.OriginChainupAmount 
				, InvoiceDestChainupRate = OS.DestChainupRate
				, InvoiceDestChainupRateType = OS.DestChainupRateType
				, InvoiceDestChainupAmount = OS.DestChainupAmount 
				, InvoiceRerouteRate = OS.RerouteRate
				, InvoiceRerouteRateType = OS.RerouteRateType
				, InvoiceRerouteAmount = OS.RerouteAmount 
				, InvoiceH2SRate = OS.H2SRate
				, InvoiceH2SRateType = OS.H2SRateType
				, InvoiceH2SAmount = OS.H2SAmount
				, InvoiceSplitLoadRate = OS.SplitLoadRate
				, InvoiceSplitLoadRateType = OS.SplitLoadRateType
				, InvoiceSplitLoadAmount = OS.SplitLoadAmount
				, InvoiceOtherAmount = OS.OtherAmount
				, InvoiceTaxRate = OS.OriginTaxRate
				, InvoiceSettlementUom = OS.SettlementUom 
				, InvoiceSettlementUomShort = OS.SettlementUomShort 
				, InvoiceCarrierSettlementFactorID = OS.CarrierSettlementFactorID 
				, InvoiceSettlementFactorID = OS.SettlementFactorID 
				, InvoiceSettlementFactor = OS.SettlementFactor 
				, InvoiceMinSettlementUnitsID = OS.MinSettlementUnitsID 
				, InvoiceMinSettlementUnits = OS.MinSettlementUnits
				, InvoiceUnits = OS.SettlementUnits
				, InvoiceRouteRate = OS.RouteRate
				, InvoiceRouteRateType = OS.RouteRateType
				, InvoiceRateSheetRate = OS.RateSheetRate
				, InvoiceRateSheetRateType = OS.RateSheetRateType
				, InvoiceFuelSurchargeRate = OS.FuelSurchargeRate
				, InvoiceFuelSurchargeAmount = OS.FuelSurchargeAmount
				, InvoiceLoadAmount = OS.LoadAmount
				, InvoiceTotalAmount = OS.TotalAmount
				, InvoicePeriodEnd = OS.PeriodEndDate
				, InvoiceNotes = OS.Notes
			FROM dbo.viewOrder O
			LEFT JOIN viewOrderSettlementCarrier OS ON OS.OrderID = O.ID
			LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
			LEFT JOIN tblSettlementFactor FC ON FC.ID = OS.SettlementFactorID
			WHERE O.StatusID NOT IN (-10) -- don't include "Generated" orders (only Assigned+)
		) X
	) X1

GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details + FINANCIAL INFO into a single view
-- Changes:
	-- 3.7.28 - 2015/06/18 - KDA - add new columns: CarrierSettlementFactorID, SettlementFactorID, MinSettlementUnitsID
	-- 4.1.0	2016/08/08	KDA		add Shipper field (renamed "clone" of Customer field) to match viewOrder_Financial_Shipper VIEW
/***********************************/
ALTER VIEW viewOrder_Financial_Carrier AS 
	SELECT O.* 
		, Shipper = isnull(C.Name, 'N/A')
		, ShipperSettled = cast(CASE WHEN OSS.BatchID IS NOT NULL THEN 1 ELSE 0 END as bit)
		, TicketNums = dbo.fnOrderTicketDetails(O.ID, 'TicketNums', '<br/>') 
		, TankNums = dbo.fnOrderTicketDetails(O.ID, 'TankNums', '<br/>') 
		, PreviousDestinations = dbo.fnRerouteDetails(O.ID, 'PreviousDestinations', '<br/>') 
		, RerouteUsers = dbo.fnRerouteDetails(O.ID, 'RerouteUsers', '<br/>') 
		, RerouteDates = dbo.fnRerouteDetails(O.ID, 'RerouteDates', '<br/>') 
		, RerouteNotes = dbo.fnRerouteDetails(O.ID, 'RerouteNotes', '<br/>')
		, InvoiceOtherDetailsTSV = dbo.fnOrderCarrierAssessorialDetailsTSV(O.ID, 0)
		, InvoiceOtherAmountsTSV = dbo.fnOrderCarrierAssessorialAmountsTSV(O.ID, 0)
	FROM dbo.viewOrder_Financial_Base_Carrier O
	LEFT JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = O.ID
	LEFT JOIN tblCustomer C ON C.ID = O.CustomerID

GO

/***********************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details + FINANCIAL INFO into a single view
-- Changes:
--  4.4.1	- 2016/11/04 - JAE	- Add Destination Chainup
--  4.4.1.2	- 2016.11.17 - KDA	- update "other" assessorial rates JOIN to use new IsSystem flag (instead of 1,2,3,4 criteria)
--  4.6.2	- 2017.04.20 - KDA	- add BatchDate, PeriodEndDate fields
/***********************************/
ALTER VIEW viewOrderSettlementDriver AS 
	SELECT OSC.*
		, SB.BatchNum
		, SB.BatchDate
		, SettlementUom = UOM.Name
		, SettlementUomShort = UOM.Abbrev
		, RouteRate = RR.Rate
		, RouteRateType = RRT.Name
		, RateSheetRate = RSRR.Rate
		, RateSheetRateType = RSRT.Name
		, OrderRejectRate = ORR.Rate
		, OrderRejectRateType = ORRT.Name
		, OriginWaitRate = OWR.Rate
		, DestinationWaitRate = DWR.Rate
		, TotalWaitAmount = cast(isnull(OSC.OriginWaitAmount, 0) as money) + cast(isnull(OSC.DestinationWaitAmount, 0) as money)
		, OriginChainupRate = CAR.Rate
		, OriginChainupRateType = CART.Name
		, OriginChainupAmount = CA.Amount
		, DestChainupRate = DCAR.Rate
		, DestChainupRateType = DCART.Name
		, DestChainupAmount = DCA.Amount
		, RerouteRate = RAR.Rate
		, RerouteRateType = RART.Name
		, RerouteAmount = RA.Amount
		, SplitLoadRate = SAR.Rate
		, SplitLoadRateType = SART.Name
		, SplitLoadAmount = SA.Amount
		, H2SRate = HAR.Rate
		, H2SRateType = HART.Name
		, H2SAmount = HA.Amount
		, OtherAmount = OA.Amount
		, WaitFeeSubUnit = isnull(WFSU.Name, 'None') 
		, WaitFeeRoundingType = isnull(WFRT.Name, 'None')
		, SettlementFactor = SF.Name
		, SB.PeriodEndDate
	FROM tblOrderSettlementDriver OSC 
	LEFT JOIN tblDriverSettlementBatch SB ON SB.ID = OSC.BatchID
	LEFT JOIN tblUom UOM ON UOM.ID = OSC.SettlementUomID
	LEFT JOIN tblDriverOriginWaitRate OWR ON OWR.ID = OSC.OriginWaitRateID
	LEFT JOIN tblDriverDestinationWaitRate DWR ON DWR.ID = OSC.DestinationWaitRateID
	LEFT JOIN tblDriverOrderRejectRate ORR ON ORR.ID = OSC.OrderRejectRateID
	LEFT JOIN tblRateType ORRT ON ORRT.ID = ORR.RateTypeID
	-- route rate/ratesheet JOINs
	LEFT JOIN tblDriverRouteRate RR ON RR.ID = OSC.RouteRateID
	LEFT JOIN tblRateType RRT ON RRT.ID = RR.RateTypeID
	LEFT JOIN viewDriverRateSheetRangeRate RSRR ON RSRR.ID = OSC.RangeRateID
	LEFT JOIN tblRateType RSRT ON RSRT.ID = RSRR.RateTypeID
	-- chainup
	LEFT JOIN tblOrderSettlementDriverAssessorialCharge CA ON CA.OrderID = OSC.OrderID AND CA.AssessorialRateTypeID = 1
	LEFT JOIN tblDriverAssessorialRate CAR ON CAR.ID = CA.AssessorialRateID
	LEFT JOIN tblRateType CART ON CART.ID = CAR.RateTypeID
	LEFT JOIN tblOrderSettlementDriverAssessorialCharge DCA ON DCA.OrderID = OSC.OrderID AND DCA.AssessorialRateTypeID = 5
	LEFT JOIN tblDriverAssessorialRate DCAR ON DCAR.ID = DCA.AssessorialRateID
	LEFT JOIN tblRateType DCART ON DCART.ID = DCAR.RateTypeID
	-- Reroute
	LEFT JOIN tblOrderSettlementDriverAssessorialCharge RA ON RA.OrderID = OSC.OrderID AND RA.AssessorialRateTypeID = 2
	LEFT JOIN tblDriverAssessorialRate RAR ON RAR.ID = RA.AssessorialRateID
	LEFT JOIN tblRateType RART ON RART.ID = RAR.RateTypeID
	-- Split Load
	LEFT JOIN tblOrderSettlementDriverAssessorialCharge SA ON SA.OrderID = OSC.OrderID AND SA.AssessorialRateTypeID = 3
	LEFT JOIN tblDriverAssessorialRate SAR ON SAR.ID = SA.AssessorialRateID
	LEFT JOIN tblRateType SART ON SART.ID = SAR.RateTypeID
	-- H2S
	LEFT JOIN tblOrderSettlementDriverAssessorialCharge HA ON HA.OrderID = OSC.OrderID AND HA.AssessorialRateTypeID = 4
	LEFT JOIN tblDriverAssessorialRate HAR ON HAR.ID = HA.AssessorialRateID
	LEFT JOIN tblRateType HART ON HART.ID = HAR.RateTypeID
	-- Other Assessorial Charges (combined)
	LEFT JOIN (
		SELECT OrderID, Amount = SUM(Amount) 
		FROM tblOrderSettlementDriverAssessorialCharge 
		WHERE AssessorialRateTypeID IN (SELECT ID FROM tblAssessorialRateType WHERE IsSystem = 0) -- 4.4.1.2 - use tblAssessorialRateType.IsSystem
		GROUP BY OrderID
	) OA ON OA.OrderID = OSC.OrderID 
	LEFT JOIN tblSettlementFactor SF ON SF.ID = OSC.SettlementFactorID
	LEFT JOIN tblCarrierWaitFeeParameter WFP ON WFP.ID = OSC.WaitFeeParameterID
	LEFT JOIN dbo.tblWaitFeeSubUnit WFSU ON WFSU.ID = WFP.SubUnitID
	LEFT JOIN dbo.tblWaitFeeRoundingType WFRT ON WFRT.ID = WFP.RoundingTypeID

GO

/***********************************/
-- Created: 4.1.0 - 2016/08/15 - Kevin Alons
-- Purpose: return BASE Order details + Driver FINANCIAL INFO into a single view
-- Changes:
--  4.1.0.2 - 2016.08.28 - KDA	- remove redundant calculation of ORR.RerouteCount
--								- remove redundant tblOrigin JOIN (for H2S which is already available in viewOrder)
--  4.1.8.2 - 2016.09.21 - JAE	- Check Final Actual miles (null actual miles ok as long as override)
--  4.1.23	- 2016.10.17 - JAE	- Add weight units for settlement
--  4.4.1	- 2016/11/04 - JAE	- Add Destination Chainup
--  4.6.2	- 2017.04.21 - KDA	- Add InvoiceBatchNum, InvoiceBatchDate, InvoicePeriodEnd, InvoiceNotes fields
--								- Add BatchOriginDriverID & BatchOriginDriver fields (for Driver Settlement Statement logic)
/***********************************/
ALTER VIEW viewOrder_Financial_Base_Driver AS 
	SELECT *
		, HasError = cast(CASE WHEN ErrorFieldCSV IS NOT NULL THEN 1 ELSE 0 END as bit)
	FROM (
		SELECT *
			, OrderID = ID
			, ErrorFieldCSV = nullif(substring(CASE WHEN Approved = 0 THEN ',Approved' ELSE '' END
				+ CASE WHEN InvoiceLoadAmount IS NULL THEN ',InvoiceLoadAmount,InvoiceRouteRate,InvoiceRateSheet' ELSE '' END
				+ CASE WHEN FinalOriginChainup = 1 AND InvoiceOriginChainupAmount IS NULL THEN ',InvoiceOriginChainupAmount' ELSE '' END
				+ CASE WHEN FinalDestChainup = 1 AND InvoiceDestChainupAmount IS NULL THEN ',InvoiceDestChainupAmount' ELSE '' END
				+ CASE WHEN FinalRerouteCount > 0 AND InvoiceRerouteAmount IS NULL THEN ',InvoiceRerouteAmount' ELSE '' END
				+ CASE WHEN FinalH2S = 1 AND InvoiceH2SAmount IS NULL THEN ',InvoiceH2SAmount' ELSE '' END
				+ CASE WHEN TicketCount > 1 AND InvoiceSplitLoadAmount IS NULL THEN ',InvoiceSplitLoadAmount' ELSE '' END
				+ CASE WHEN InvoiceOriginWaitBillableMinutes > 0 AND InvoiceOriginWaitAmount IS NULL THEN ',InvoiceOriginWaitAmount' ELSE '' END
				+ CASE WHEN InvoiceDestinationWaitBillableMinutes > 0 AND InvoiceDestinationWaitAmount IS NULL THEN ',InvoiceDestinationWaitAmount' ELSE '' END
				+ CASE WHEN Rejected = 1 AND InvoiceOrderRejectAmount IS NULL THEN ',InvoiceOrderRejectAmount' ELSE '' END
				+ CASE WHEN InvoiceMinSettlementUnitsID IS NULL THEN ',InvoiceMinSettlementUnits' ELSE '' END
				+ CASE WHEN InvoiceSettlementFactorID IS NULL THEN ',InvoiceSettlementFactor' ELSE '' END
				+ CASE WHEN Rejected = 0 AND InvoiceSettlementFactorID IS NULL THEN ',InvoiceSettlementFactor' ELSE '' END
				+ CASE WHEN Rejected = 0 AND InvoiceSettlementFactorID IS NOT NULL AND dbo.fnOrderSettlementUnits(InvoiceSettlementFactorID, OriginGrossUnits, OriginNetUnits, OriginGrossStdUnits, DestGrossUnits, DestNetUnits, OriginWeightNetUnits, DestWeightNetUnits) IS NULL THEN ','+OrderUnitsField ELSE '' END
				+ CASE WHEN InvoiceWaitFeeParameterID IS NULL THEN ',InvoiceOriginWaitBillableMinutes,InvoiceDestinationWaitBillableMinutes' ELSE '' END
				+ CASE WHEN isnull(FinalActualMiles, 0) = 0 THEN ',ActualMiles' ELSE '' END, 2, 100000), '')
		FROM (
			SELECT O.* 
				, DriverGroup = DG.Name
				, InvoiceRatesAppliedDateUTC = OS.CreateDateUTC
				, OS.BatchID
				, Approved = cast(ISNULL(OA.Approved, 0) as bit)
				, FinalActualMiles = ISNULL(OA.OverrideActualMiles, O.ActualMiles)
				, FinalOriginMinutes = ISNULL(OA.OverrideOriginMinutes, O.OriginMinutes)
				, FinalDestMinutes = ISNULL(OA.OverrideDestMinutes, O.DestMinutes)
				, FinalH2S = cast(CASE WHEN isnull(OA.OverrideH2S, 0) = 1 THEN 0 ELSE O.H2S END as bit)
				, FinalOriginChainup = cast(CASE WHEN isnull(OA.OverrideOriginChainup, 0) = 1 THEN 0 ELSE O.OriginChainup END as bit)
				, FinalDestChainup = cast(CASE WHEN isnull(OA.OverrideDestChainup, 0) = 1 THEN 0 ELSE O.DestChainup END as bit)
				, FinalRerouteCount = CASE WHEN isnull(OA.OverrideReroute, 0) = 1 THEN 0 ELSE isnull(O.RerouteCount, 0) END
				, FC.OrderUnitsField
				, InvoiceBatchNum = OS.BatchNum 
				, InvoiceBatchDate = OS.BatchDate
				, InvoiceOriginWaitBillableMinutes = OS.OriginWaitBillableMinutes
				, InvoiceDestinationWaitBillableMinutes = OS.DestinationWaitBillableMinutes
				, InvoiceTotalWaitBillableMinutes = isnull(OS.OriginWaitBillableMinutes, 0) + ISNULL(OS.DestinationWaitBillableMinutes, 0) 
				, InvoiceWaitFeeParameterID = WaitFeeParameterID
				, InvoiceWaitFeeSubUnit = WaitFeeSubUnit
				, InvoiceWaitFeeRoundingType = WaitFeeRoundingType
				, InvoiceOriginWaitRate = OS.OriginWaitRate 
				, InvoiceOriginWaitAmount = OS.OriginWaitAmount 		
				, InvoiceDestinationWaitRate = OS.DestinationWaitRate 
				, InvoiceDestinationWaitAmount = OS.DestinationWaitAmount 
				, InvoiceTotalWaitAmount = OS.TotalWaitAmount
				, InvoiceOrderRejectRate = OS.OrderRejectRate	
				, InvoiceOrderRejectRateType = OS.OrderRejectRateType  
				, InvoiceOrderRejectAmount = OS.OrderRejectAmount  
				, InvoiceOriginChainupRate = OS.OriginChainupRate
				, InvoiceOriginChainupRateType = OS.OriginChainupRateType
				, InvoiceOriginChainupAmount = OS.OriginChainupAmount 
				, InvoiceDestChainupRate = OS.DestChainupRate
				, InvoiceDestChainupRateType = OS.DestChainupRateType
				, InvoiceDestChainupAmount = OS.DestChainupAmount 
				, InvoiceRerouteRate = OS.RerouteRate
				, InvoiceRerouteRateType = OS.RerouteRateType
				, InvoiceRerouteAmount = OS.RerouteAmount 
				, InvoiceH2SRate = OS.H2SRate
				, InvoiceH2SRateType = OS.H2SRateType
				, InvoiceH2SAmount = OS.H2SAmount
				, InvoiceSplitLoadRate = OS.SplitLoadRate
				, InvoiceSplitLoadRateType = OS.SplitLoadRateType
				, InvoiceSplitLoadAmount = OS.SplitLoadAmount
				, InvoiceOtherAmount = OS.OtherAmount
				, InvoiceTaxRate = OS.OriginTaxRate
				, InvoiceSettlementUom = OS.SettlementUom 
				, InvoiceSettlementUomShort = OS.SettlementUomShort 
				, InvoiceCarrierSettlementFactorID = OS.CarrierSettlementFactorID 
				, InvoiceSettlementFactorID = OS.SettlementFactorID 
				, InvoiceSettlementFactor = OS.SettlementFactor 
				, InvoiceMinSettlementUnitsID = OS.MinSettlementUnitsID 
				, InvoiceMinSettlementUnits = OS.MinSettlementUnits
				, InvoiceUnits = OS.SettlementUnits
				, InvoiceRouteRate = OS.RouteRate
				, InvoiceRouteRateType = OS.RouteRateType
				, InvoiceRateSheetRate = OS.RateSheetRate
				, InvoiceRateSheetRateType = OS.RateSheetRateType
				, InvoiceFuelSurchargeRate = OS.FuelSurchargeRate
				, InvoiceFuelSurchargeAmount = OS.FuelSurchargeAmount
				, InvoiceLoadAmount = OS.LoadAmount
				, InvoiceTotalAmount = OS.TotalAmount
				, InvoicePeriodEnd = OS.PeriodEndDate
				, InvoiceNotes = OS.Notes
				/* used for Driver Settlement Statement logic */
				, BatchOriginDriverID = rtrim(OS.BatchID) + '.' + rtrim(O.OriginDriverID)
				, BatchOriginDriver = O.OriginDriver
			FROM dbo.viewOrder O
			LEFT JOIN tblDriverGroup DG ON DG.ID = O.DriverGroupID
			LEFT JOIN viewOrderSettlementDriver OS ON OS.OrderID = O.ID
			LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
			LEFT JOIN tblSettlementFactor FC ON FC.ID = OS.SettlementFactorID
			WHERE O.StatusID NOT IN (-10) -- don't include "Generated" orders (only Assigned+)
		) X
	) X1

GO

/***********************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details + FINANCIAL INFO into a single view
-- Changes:
/***********************************/
ALTER VIEW viewOrder_Financial_Driver AS 
	SELECT O.* 
		, Shipper = isnull(C.Name, 'N/A')
		, CarrierSettled = cast(CASE WHEN OS.BatchID IS NOT NULL THEN 1 ELSE 0 END as bit)
		, TicketNums = dbo.fnOrderTicketDetails(O.ID, 'TicketNums', '<br/>') 
		, TankNums = dbo.fnOrderTicketDetails(O.ID, 'TankNums', '<br/>') 
		, PreviousDestinations = dbo.fnRerouteDetails(O.ID, 'PreviousDestinations', '<br/>') 
		, RerouteUsers = dbo.fnRerouteDetails(O.ID, 'RerouteUsers', '<br/>') 
		, RerouteDates = dbo.fnRerouteDetails(O.ID, 'RerouteDates', '<br/>') 
		, RerouteNotes = dbo.fnRerouteDetails(O.ID, 'RerouteNotes', '<br/>')
		, InvoiceOtherDetailsTSV = dbo.fnOrderDriverAssessorialDetailsTSV(O.ID, 0)
		, InvoiceOtherAmountsTSV = dbo.fnOrderDriverAssessorialAmountsTSV(O.ID, 0)
	FROM dbo.viewOrder_Financial_Base_Driver O
	LEFT JOIN tblOrderSettlementCarrier OS ON OS.OrderID = O.ID
	LEFT JOIN tblCustomer C ON C.ID = O.CustomerID

GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details + FINANCIAL INFO into a single view
-- Changes:
--  3.7.29	- 2015/06/18 - KDA	- add translated "SettlementFactor" column
--  4.4.1	- 2016/11/04 - JAE	- Add Destination Chainup
--  4.4.1.2	- 2016.11.17 - KDA	- update "other" assessorial rates JOIN to use new IsSystem flag (instead of 1,2,3,4 criteria)
--  4.6.2	- 2017.04.20 - KDA	- add BatchDate, PeriodEndDate fields
/***********************************/
ALTER VIEW viewOrderSettlementShipper AS 
	SELECT OSC.*
		, SB.BatchNum
		, SB.BatchDate
		, SettlementUom = UOM.Name
		, SettlementUomShort = UOM.Abbrev
		, RouteRate = RR.Rate
		, RouteRateType = RRT.Name
		, RateSheetRate = RSRR.Rate
		, RateSheetRateType = RSRT.Name
		, OrderRejectRate = ORR.Rate
		, OrderRejectRateType = ORRT.Name
		, OriginWaitRate = OWR.Rate
		, DestinationWaitRate = DWR.Rate
		, TotalWaitAmount = cast(isnull(OSC.OriginWaitAmount, 0) as money) + cast(isnull(OSC.DestinationWaitAmount, 0) as money)
		, OriginChainupRate = CAR.Rate
		, OriginChainupRateType = CART.Name
		, OriginChainupAmount = CA.Amount
		, DestChainupRate = DCAR.Rate
		, DestChainupRateType = DCART.Name
		, DestChainupAmount = DCA.Amount
		, RerouteRate = RAR.Rate
		, RerouteRateType = RART.Name
		, RerouteAmount = RA.Amount
		, SplitLoadRate = SAR.Rate
		, SplitLoadRateType = SART.Name
		, SplitLoadAmount = SA.Amount
		, H2SRate = HAR.Rate
		, H2SRateType = HART.Name
		, H2SAmount = HA.Amount
		, OtherAmount = OA.Amount
		, WaitFeeSubUnit = isnull(WFSU.Name, 'None') 
		, WaitFeeRoundingType = isnull(WFRT.Name, 'None') 
		, SettlementFactor = SF.Name
		, SB.PeriodEndDate
	FROM tblOrderSettlementShipper OSC 
	LEFT JOIN tblShipperSettlementBatch SB ON SB.ID = OSC.BatchID
	LEFT JOIN tblUom UOM ON UOM.ID = OSC.SettlementUomID
	LEFT JOIN tblShipperOriginWaitRate OWR ON OWR.ID = OSC.OriginWaitRateID
	LEFT JOIN tblShipperDestinationWaitRate DWR ON DWR.ID = OSC.DestinationWaitRateID
	LEFT JOIN tblShipperOrderRejectRate ORR ON ORR.ID = OSC.OrderRejectRateID
	LEFT JOIN tblRateType ORRT ON ORRT.ID = ORR.RateTypeID
	-- route rate/ratesheet JOINs
	LEFT JOIN tblShipperRouteRate RR ON RR.ID = OSC.RouteRateID
	LEFT JOIN tblRateType RRT ON RRT.ID = RR.RateTypeID
	LEFT JOIN viewShipperRateSheetRangeRate RSRR ON RSRR.ID = OSC.RangeRateID
	LEFT JOIN tblRateType RSRT ON RSRT.ID = RSRR.RateTypeID
	-- chainup
	LEFT JOIN tblOrderSettlementShipperAssessorialCharge CA ON CA.OrderID = OSC.OrderID AND CA.AssessorialRateTypeID = 1
	LEFT JOIN tblShipperAssessorialRate CAR ON CAR.ID = CA.AssessorialRateID
	LEFT JOIN tblRateType CART ON CART.ID = CAR.RateTypeID
	LEFT JOIN tblOrderSettlementShipperAssessorialCharge DCA ON DCA.OrderID = OSC.OrderID AND DCA.AssessorialRateTypeID = 5
	LEFT JOIN tblShipperAssessorialRate DCAR ON DCAR.ID = DCA.AssessorialRateID
	LEFT JOIN tblRateType DCART ON DCART.ID = DCAR.RateTypeID
	-- Reroute
	LEFT JOIN tblOrderSettlementShipperAssessorialCharge RA ON RA.OrderID = OSC.OrderID AND RA.AssessorialRateTypeID = 2
	LEFT JOIN tblShipperAssessorialRate RAR ON RAR.ID = RA.AssessorialRateID
	LEFT JOIN tblRateType RART ON RART.ID = RAR.RateTypeID
	-- Split Load
	LEFT JOIN tblOrderSettlementShipperAssessorialCharge SA ON SA.OrderID = OSC.OrderID AND SA.AssessorialRateTypeID = 3
	LEFT JOIN tblShipperAssessorialRate SAR ON SAR.ID = SA.AssessorialRateID
	LEFT JOIN tblRateType SART ON SART.ID = SAR.RateTypeID
	-- H2S
	LEFT JOIN tblOrderSettlementShipperAssessorialCharge HA ON HA.OrderID = OSC.OrderID AND HA.AssessorialRateTypeID = 4
	LEFT JOIN tblShipperAssessorialRate HAR ON HAR.ID = HA.AssessorialRateID
	LEFT JOIN tblRateType HART ON HART.ID = HAR.RateTypeID
	-- Other Assessorial Charges (combined)
	LEFT JOIN (
		SELECT OrderID, Amount = SUM(Amount) 
		FROM tblOrderSettlementShipperAssessorialCharge 
		WHERE AssessorialRateTypeID IN (SELECT ID FROM tblAssessorialRateType WHERE IsSystem = 0) -- 4.4.1.2 - use tblAssessorialRateType.IsSystem
		GROUP BY OrderID
	) OA ON OA.OrderID = OSC.OrderID 
	LEFT JOIN tblSettlementFactor SF ON SF.ID = OSC.SettlementFactorID
	LEFT JOIN tblShipperWaitFeeParameter WFP ON WFP.ID = OSC.WaitFeeParameterID
	LEFT JOIN dbo.tblWaitFeeSubUnit WFSU ON WFSU.ID = WFP.SubUnitID
	LEFT JOIN dbo.tblWaitFeeRoundingType WFRT ON WFRT.ID = WFP.RoundingTypeID

GO

/***********************************/
-- Created: 4.1.0 - 2016/08/15 - Kevin Alons
-- Purpose: return BASE Order details + Shipper FINANCIAL INFO into a single view
-- Changes:
--	4.1.0.2 - 2016.08.28 - KDA	- remove redundant calculation of ORR.RerouteCount
--								- remove redundant tblOrigin JOIN (for H2S which is already available in viewOrder)
--	4.1.8.2 - 2016.09.21 - JAE	- Check Final Actual miles (null actual miles ok as long as override)
--  4.1.23	  2016.10.17 - JAE	- Add weight units for settlement
--  4.4.1	  2016/11/04 - JAE	- Add Destination Chainup
--  4.6.2	- 2017.04.21 - KDA	- Add InvoiceBatchNum, InvoiceBatchDate, InvoicePeriodEnd, InvoiceNotes fields
/***********************************/
ALTER VIEW viewOrder_Financial_Base_Shipper AS 
	SELECT *
		, HasError = cast(CASE WHEN ErrorFieldCSV IS NOT NULL THEN 1 ELSE 0 END as bit)
	FROM (
		SELECT *
			, OrderID = ID
			, ErrorFieldCSV = nullif(substring(CASE WHEN Approved = 0 THEN ',Approved' ELSE '' END
				+ CASE WHEN InvoiceLoadAmount IS NULL THEN ',InvoiceLoadAmount,InvoiceRouteRate,InvoiceRateSheet' ELSE '' END
				+ CASE WHEN FinalOriginChainup = 1 AND InvoiceOriginChainupAmount IS NULL THEN ',InvoiceOriginChainupAmount' ELSE '' END
				+ CASE WHEN FinalDestChainup = 1 AND InvoiceDestChainupAmount IS NULL THEN ',InvoiceDestChainupAmount' ELSE '' END
				+ CASE WHEN FinalRerouteCount > 0 AND InvoiceRerouteAmount IS NULL THEN ',InvoiceRerouteAmount' ELSE '' END
				+ CASE WHEN FinalH2S = 1 AND InvoiceH2SAmount IS NULL THEN ',InvoiceH2SAmount' ELSE '' END
				+ CASE WHEN TicketCount > 1 AND InvoiceSplitLoadAmount IS NULL THEN ',InvoiceSplitLoadAmount' ELSE '' END
				+ CASE WHEN InvoiceOriginWaitBillableMinutes > 0 AND InvoiceOriginWaitAmount IS NULL THEN ',InvoiceOriginWaitAmount' ELSE '' END
				+ CASE WHEN InvoiceDestinationWaitBillableMinutes > 0 AND InvoiceDestinationWaitAmount IS NULL THEN ',InvoiceDestinationWaitAmount' ELSE '' END
				+ CASE WHEN Rejected = 1 AND InvoiceOrderRejectAmount IS NULL THEN ',InvoiceOrderRejectAmount' ELSE '' END
				+ CASE WHEN InvoiceMinSettlementUnitsID IS NULL THEN ',InvoiceMinSettlementUnits' ELSE '' END
				+ CASE WHEN InvoiceSettlementFactorID IS NULL THEN ',InvoiceSettlementFactor' ELSE '' END
				+ CASE WHEN Rejected = 0 AND InvoiceSettlementFactorID IS NULL THEN ',InvoiceSettlementFactor' ELSE '' END
				+ CASE WHEN Rejected = 0 AND InvoiceSettlementFactorID IS NOT NULL AND dbo.fnOrderSettlementUnits(InvoiceSettlementFactorID, OriginGrossUnits, OriginNetUnits, OriginGrossStdUnits, DestGrossUnits, DestNetUnits, OriginWeightNetUnits, DestWeightNetUnits) IS NULL THEN ','+OrderUnitsField ELSE '' END
				+ CASE WHEN InvoiceWaitFeeParameterID IS NULL THEN ',InvoiceOriginWaitBillableMinutes,InvoiceDestinationWaitBillableMinutes' ELSE '' END
				+ CASE WHEN isnull(FinalActualMiles, 0) = 0 THEN ',ActualMiles' ELSE '' END, 2, 100000), '')
		FROM (
			SELECT O.* 
				, InvoiceRatesAppliedDateUTC = OS.CreateDateUTC
				, OS.BatchID
				, Approved = cast(ISNULL(OA.Approved, 0) as bit)
				, FinalActualMiles = ISNULL(OA.OverrideActualMiles, O.ActualMiles)
				, FinalOriginMinutes = ISNULL(OA.OverrideOriginMinutes, O.OriginMinutes)
				, FinalDestMinutes = ISNULL(OA.OverrideDestMinutes, O.DestMinutes)
				, FinalH2S = cast(CASE WHEN isnull(OA.OverrideH2S, 0) = 1 THEN 0 ELSE O.H2S END as bit)
				, FinalOriginChainup = cast(CASE WHEN isnull(OA.OverrideOriginChainup, 0) = 1 THEN 0 ELSE O.OriginChainup END as bit)
				, FinalDestChainup = cast(CASE WHEN isnull(OA.OverrideDestChainup, 0) = 1 THEN 0 ELSE O.DestChainup END as bit)
				, FinalRerouteCount = CASE WHEN isnull(OA.OverrideReroute, 0) = 1 THEN 0 ELSE isnull(O.RerouteCount, 0) END
				, FC.OrderUnitsField
				, InvoiceBatchNum = OS.BatchNum 
				, InvoiceBatchDate = OS.BatchDate
				, InvoiceOriginWaitBillableMinutes = OS.OriginWaitBillableMinutes
				, InvoiceDestinationWaitBillableMinutes = OS.DestinationWaitBillableMinutes
				, InvoiceTotalWaitBillableMinutes = isnull(OS.OriginWaitBillableMinutes, 0) + ISNULL(OS.DestinationWaitBillableMinutes, 0) 
				, InvoiceWaitFeeParameterID = WaitFeeParameterID
				, InvoiceWaitFeeSubUnit = WaitFeeSubUnit
				, InvoiceWaitFeeRoundingType = WaitFeeRoundingType
				, InvoiceOriginWaitRate = OS.OriginWaitRate 
				, InvoiceOriginWaitAmount = OS.OriginWaitAmount 		
				, InvoiceDestinationWaitRate = OS.DestinationWaitRate 
				, InvoiceDestinationWaitAmount = OS.DestinationWaitAmount 
				, InvoiceTotalWaitAmount = OS.TotalWaitAmount
				, InvoiceOrderRejectRate = OS.OrderRejectRate	
				, InvoiceOrderRejectRateType = OS.OrderRejectRateType  
				, InvoiceOrderRejectAmount = OS.OrderRejectAmount  
				, InvoiceOriginChainupRate = OS.OriginChainupRate
				, InvoiceOriginChainupRateType = OS.OriginChainupRateType
				, InvoiceOriginChainupAmount = OS.OriginChainupAmount 
				, InvoiceDestChainupRate = OS.DestChainupRate
				, InvoiceDestChainupRateType = OS.DestChainupRateType
				, InvoiceDestChainupAmount = OS.DestChainupAmount 
				, InvoiceRerouteRate = OS.RerouteRate
				, InvoiceRerouteRateType = OS.RerouteRateType
				, InvoiceRerouteAmount = OS.RerouteAmount 
				, InvoiceH2SRate = OS.H2SRate
				, InvoiceH2SRateType = OS.H2SRateType
				, InvoiceH2SAmount = OS.H2SAmount
				, InvoiceSplitLoadRate = OS.SplitLoadRate
				, InvoiceSplitLoadRateType = OS.SplitLoadRateType
				, InvoiceSplitLoadAmount = OS.SplitLoadAmount
				, InvoiceOtherAmount = OS.OtherAmount
				, InvoiceTaxRate = OS.OriginTaxRate
				, InvoiceSettlementUom = OS.SettlementUom 
				, InvoiceSettlementUomShort = OS.SettlementUomShort 
				, InvoiceShipperSettlementFactorID = OS.ShipperSettlementFactorID 
				, InvoiceSettlementFactorID = OS.SettlementFactorID 
				, InvoiceSettlementFactor = OS.SettlementFactor 
				, InvoiceMinSettlementUnitsID = OS.MinSettlementUnitsID 
				, InvoiceMinSettlementUnits = OS.MinSettlementUnits
				, InvoiceUnits = OS.SettlementUnits
				, InvoiceRouteRate = OS.RouteRate
				, InvoiceRouteRateType = OS.RouteRateType
				, InvoiceRateSheetRate = OS.RateSheetRate
				, InvoiceRateSheetRateType = OS.RateSheetRateType
				, InvoiceFuelSurchargeRate = OS.FuelSurchargeRate
				, InvoiceFuelSurchargeAmount = OS.FuelSurchargeAmount
				, InvoiceLoadAmount = OS.LoadAmount
				, InvoiceTotalAmount = OS.TotalAmount
				, InvoicePeriodEnd = OS.PeriodEndDate
				, InvoiceNotes = OS.Notes
			FROM dbo.viewOrder O
			LEFT JOIN viewOrderSettlementShipper OS ON OS.OrderID = O.ID
			LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
			LEFT JOIN tblSettlementFactor FC ON FC.ID = OS.SettlementFactorID
			WHERE O.StatusID NOT IN (-10) -- don't include "Generated" orders (only Assigned+)
		) X
	) X1

GO

/***********************************/
-- Created: ?.?.? - 2013.03/09 - Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details + FINANCIAL INFO into a single view
-- Changes:
--	3.7.27	- 2015/06/18 - KDA	- add new columns: ShipperSettlementFactorID, SettlementFactorID, MinSettlementUnitsID
--  4.1.0	- 2016.08/24 - KDA	- use new viewOrder_Financial_Base_Shipper for core values
/***********************************/
ALTER VIEW viewOrder_Financial_Shipper AS 
	SELECT O.* 
		, Shipper = isnull(C.Name, 'N/A')
		, TicketNums = dbo.fnOrderTicketDetails(O.ID, 'TicketNums', '<br/>') 
		, TankNums = dbo.fnOrderTicketDetails(O.ID, 'TankNums', '<br/>') 
		, PreviousDestinations = dbo.fnRerouteDetails(O.ID, 'PreviousDestinations', '<br/>') 
		, RerouteUsers = dbo.fnRerouteDetails(O.ID, 'RerouteUsers', '<br/>') 
		, RerouteDates = dbo.fnRerouteDetails(O.ID, 'RerouteDates', '<br/>') 
		, RerouteNotes = dbo.fnRerouteDetails(O.ID, 'RerouteNotes', '<br/>')
		, InvoiceOtherDetailsTSV = dbo.fnOrderShipperAssessorialDetailsTSV(O.ID, 0)
		, InvoiceOtherAmountsTSV = dbo.fnOrderShipperAssessorialAmountsTSV(O.ID, 0)
	FROM dbo.viewOrder_Financial_Base_Shipper O
	LEFT JOIN tblCustomer C ON C.ID = O.CustomerID

GO

/*************************************************/
-- Created: 2013.06.02 - Kevin Alons
-- Purpose: compute and add the various Carrier "Settlement" $$ values to an Delivered/Audited order
-- Changes:
-- 3.7.26	- 2015/06/13	- KDA	- add support for: Driver|DriverGroup, Producer to some rates, separate Carrier|Shipper MinSettlementUnits|SettlementFactor best-match values, 
-- 3.9.0	- 2015/08/13	- KDA	- honor Approve overrides in rating process
-- 3.9.9	- 2015/08/31	- KDA	- add new @AllowReApplyPostBatch parameter to allow re-applying after batch is applied
-- 3.9.19.2	- 2015/09/24	- KDA	- fix erroneous logic that computed Asssessorial Settlement results before the Order Level results were posted
-- 3.9.37	- 2015/12/22	- JAE	- Use tblOrderApproval to get override for Carrier Min Settlement Units
-- 3.9.39	- 2016/01/15	- JAE	- Also need to recalculate settlement units since the calculated/max values was already passed
-- 3.13.1	- 2016/07/04	- KDA	- add START/DONE PRINT statements (for debugging purposes)
-- 4.1.8.6	- 2016.09.21	- KDA	- move populate #IA table before transaction starts
--									- @debugStatements parameter + add additional START/DONE PRINT statements (for debugging purposes)
-- 4.1.9.1	- 2016.09.26	- KDA	- restore regressed 3.9.19.2 changes that accidentally got reverted with 4.1.8.6 (added a NOTE to hopefully prevent this again)
-- 4.4.1	- 2016/11/04	- JAE	- Add Destination Chainup
-- 4.6.2	- 2017.04.20	- KDA	- add @Notes parameter (and logic to persist to DB)
/*************************************************/
ALTER PROCEDURE spApplyRatesCarrier
(
  @ID int
, @UserName varchar(100)
, @OriginWaitAmount money = NULL
, @DestWaitAmount money = NULL
, @RejectionAmount money = NULL
, @FuelSurchargeAmount money = NULL
, @LoadAmount money = NULL
, @Notes varchar(255) = NULL
, @AssessorialRateTypeID_CSV varchar(max) = NULL
, @AssessorialAmount_CSV varchar(max) = null
, @ResetOverrides bit = 0
, @AllowReApplyPostBatch bit = 0
, @debugStatements bit = 0 -- turn on to get debugging statements during execution
) AS BEGIN

	SET NOCOUNT ON

	IF (@debugStatements = 1) PRINT 'spApplyRatesCarrier (' + ltrim(@ID) + ') START ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	
	IF (@AllowReApplyPostBatch = 0) AND EXISTS (SELECT * FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND BatchID IS NOT NULL)
	BEGIN
		RAISERROR('Invoice has already been Carrier Settled', 16, 1)
		RETURN
	END
	
	/* ensure that Shipper Rates have been applied prior to applying Carrier rates (since they could be dependent on Shipper rates) */
	IF NOT EXISTS (SELECT * FROM tblOrderSettlementShipper WHERE OrderID = @ID)
		EXEC spApplyRatesShipper @ID, @UserName

	/* transform the provided manual Assessorial Rates (if any) into a queryable table variable */
	DECLARE @ManualAssessorialRates TABLE (ID int primary key, Amount money)
	INSERT INTO @ManualAssessorialRates (ID, Amount) 
		SELECT ID.ID, R.Amount
		FROM (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), ID FROM dbo.fnSplitCSVIDs(@AssessorialRateTypeID_CSV)) ID
		JOIN (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), amount = CAST(value as money) FROM dbo.fnSplitCSV(@AssessorialAmount_CSV)) R ON R.sortid = ID.sortid

	/* persist all existing MANUAL Assessorial Rates from the existing Order Settlement (if any present and not resetting them) */
	INSERT INTO @ManualAssessorialRates (ID, Amount)
		SELECT AssessorialRateTypeID, Amount 
		FROM dbo.tblOrderSettlementCarrierAssessorialCharge 
		WHERE OrderID = @ID AND AssessorialRateID IS NULL 
			AND ID NOT IN (SELECT ID FROM @ManualAssessorialRates)
			AND @ResetOverrides = 0

	/* persist all existing settlement amount overrides from the existing Order Settlement - if not resetting them */
	IF (@ResetOverrides = 0)
	BEGIN 
		SELECT @OriginWaitAmount = OriginWaitAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @OriginWaitAmount IS NULL AND OriginWaitRateID IS NULL
		SELECT @DestWaitAmount = DestinationWaitAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @DestWaitAmount IS NULL AND DestinationWaitRateID IS NULL
		SELECT @RejectionAmount = OrderRejectAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @RejectionAmount IS NULL AND OrderRejectRateID IS NULL
		SELECT @FuelSurchargeAmount = FuelSurchargeAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @FuelSurchargeAmount IS NULL AND FuelSurchargeRateID IS NULL
		SELECT @LoadAmount = LoadAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @LoadAmount IS NULL AND RouteRateID IS NULL AND RangeRateID IS NULL
	END
	/* persist any existing notes by preserving them if no new note is defined */
	SELECT @Notes = Notes FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @Notes IS NULL AND Notes IS NOT NULL

	/* compute the basic settlement units from the Order being invoices */
	DECLARE @SettlementUnits TABLE
	(
		OrderID int
	  , SettlementUomID int
	  , SettlementFactorID int
	  , CarrierSettlementFactorID int
	  , MinSettlementUnits decimal(18, 10)
	  , MinSettlementUnitsID int
	  , ActualUnits decimal(18, 10)
	  , SettlementUnits decimal(18, 10) -- calculate value basically max(actual, minimum)
	)
	INSERT INTO @SettlementUnits (OrderID, SettlementUomID, SettlementFactorID, CarrierSettlementFactorID, MinSettlementUnits, MinSettlementUnitsID, ActualUnits, SettlementUnits)
		SELECT OSU.OrderID, OSU.SettlementUomID, OSU.SettlementFactorID, OSU.CarrierSettlementFactorID, ISNULL(OA.OverrideCarrierMinSettlementUnits, OSU.MinSettlementUnits), OSU.MinSettlementUnitsID, OSU.ActualUnits, 
			-- use passed settlement (already calculated) or pick the max between the actual and overrride min - JAE 1/15/16 - 3.9.39
			CASE WHEN OA.OverrideCarrierMinSettlementUnits IS NULL THEN OSU.SettlementUnits
						ELSE dbo.fnMaxDecimal(OSU.ActualUnits, OA.OverrideCarrierMinSettlementUnits) END
		FROM dbo.viewOrderSettlementUnitsCarrier OSU
		LEFT JOIN tblOrderApproval OA ON OA.OrderID = OSU.OrderID
		WHERE OSU.OrderID = @ID

	/* compute the order level settlement rateIDs & settlement amounts - into temp table #I */
	SELECT OrderID = @ID
		, OrderDate
		, CarrierSettlementFactorID, MinSettlementUnitsID
		, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
		, RouteRateID, RangeRateID, LoadAmount
		, WaitFeeParameterID
		, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount 
		, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount
		, OrderRejectRateID, OrderRejectAmount
		, FuelSurchargeRateID, FuelSurchargeRate, FuelSurchargeAmount
		, OriginTaxRate
		, TotalAmount = ISNULL(LoadAmount, 0) 
			+ ISNULL(OriginWaitAmount, 0) 
			+ ISNULL(DestWaitAmount, 0) 
			+ ISNULL(OrderRejectAmount, 0) 
			+ ISNULL(FuelSurchargeAmount, 0)
		, CreatedByUser = @UserName
		, OriginChainUp, DestChainUp, H2S, Rerouted
	INTO #I
	FROM (
		SELECT OrderDate
			, CarrierSettlementFactorID, MinSettlementUnitsID
			, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
			, RouteRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RouteRateID ELSE NULL END
			, RangeRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RangeRateID ELSE NULL END
			, LoadAmount = round(ISNULL(@LoadAmount, RR.Amount), 2)
			, WaitFeeParameterID
			, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount = ROUND(OriginWaitAmount, 2)
			, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount = ROUND(DestWaitAmount, 2)
			, OrderRejectRateID = CASE WHEN @RejectionAmount IS NULL THEN RD.RateID ELSE NULL END
			, OrderRejectAmount = round(isnull(@RejectionAmount, RD.Amount), 2)
			, FuelSurchargeRateID = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.RateID ELSE NULL END
			, FuelSurchargeRate = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.Rate ELSE NULL END
			, FuelSurchargeAmount = round(isnull(@FuelSurchargeAmount, FSR.Amount), 2)
			, OriginTaxRate
			, OriginChainUp, DestChainup, H2S, Rerouted = cast(CASE WHEN RerouteCount = 0 THEN 0 ELSE 1 END as bit) /* used for the Auto-Approve logic below */
		FROM (
			SELECT O.ID
				, O.OriginID
				, O.CarrierID
				, O.RouteID
				, SU.CarrierSettlementFactorID
				, SU.SettlementFactorID
				, SU.SettlementUomID
				, SU.ActualUnits
				, SU.MinSettlementUnitsID
				, SU.MinSettlementUnits
				, SettlementUnits = CASE WHEN O.Rejected = 1 THEN 0 ELSE SU.SettlementUnits END
				, RouteMiles = isnull(OA.OverrideActualMiles, O.ActualMiles)
				, OrderDate = isnull(O.OrderDate, O.DueDate)
				, OriginChainUp = cast(CASE WHEN OA.OverrideOriginChainup = 1 THEN 0 ELSE O.OriginChainup END as bit)
				, DestChainUp = cast(CASE WHEN OA.OverrideDestChainup = 1 THEN 0 ELSE O.DestChainup END as bit)
				, H2S = cast(CASE WHEN OA.OverrideH2S = 1 THEN 0 ELSE O.H2S END as bit)
				, O.Rejected
				, RerouteCount = CASE WHEN OA.OverrideReroute = 1 THEN 0 ELSE O.RerouteCount END
				, IsSplit = CASE WHEN O.Rejected = 0 AND O.TicketCount > 1 THEN 1 ELSE 0 END
				, WaitFeeParameterID = WFP.ID
				, OriginWaitRateID = CASE WHEN @OriginWaitAmount IS NULL THEN OWD.RateID ELSE NULL END
				, OriginWaitBillableMinutes = OWD.BillableMinutes
				, OriginWaitBillableHours = OWD.BillableHours
				, OriginWaitAmount = isnull(@OriginWaitAmount, OWD.Amount)
				, DestWaitRateID = CASE WHEN @DestWaitAmount IS NULL THEN DWD.RateID ELSE NULL END
				, DestWaitBillableMinutes = DWD.BillableMinutes
				, DestWaitBillableHours = DWD.BillableHours
				, DestWaitAmount = isnull(@DestWaitAmount, DWD.Amount)
				, OriginTaxRate = OO.TaxRate
			FROM dbo.viewOrder O
			JOIN @SettlementUnits SU ON SU.OrderID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
			OUTER APPLY dbo.fnOrderCarrierOriginWaitData(@ID) OWD 
			OUTER APPLY dbo.fnOrderCarrierDestinationWaitData(@ID) DWD 
			OUTER APPLY dbo.fnOrderCarrierWaitFeeParameter(@ID) WFP
			WHERE O.ID = @ID
		) X
		OUTER APPLY dbo.fnOrderCarrierLoadAmount(@ID, SettlementUnits, SettlementUomID) RR 
		OUTER APPLY dbo.fnOrderCarrierOrderRejectData(@ID, SettlementUnits, SettlementUomID) RD
		OUTER APPLY dbo.fnOrderCarrierFuelSurchargeData(@ID) FSR
	) X2

	DECLARE @CreatedTran bit; SET @CreatedTran = CASE WHEN @@TRANCOUNT = 0 THEN 1 ELSE 0 END
	BEGIN TRY
		IF (@CreatedTran = 1)
			BEGIN TRAN

		-- remove the existing settlment record (if any)
		DELETE FROM tblOrderSettlementCarrierAssessorialCharge WHERE OrderID = @ID
		DELETE FROM tblOrderSettlementCarrier WHERE OrderID = @ID
	
		-- all Units and Rates are first normalized to the Order.OriginUOM then consistent processing
		-- and persisted in the Invoice (Settlement) record in this Order.OriginUOM 
		INSERT INTO tblOrderSettlementCarrier 
			(OrderID
			, OrderDate
			, CarrierSettlementFactorID
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnitsID
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestinationWaitRateID 
			, DestinationWaitBillableMinutes 
			, DestinationWaitBillableHours
			, DestinationWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser
			, Notes)
		SELECT OrderID
			, OrderDate
			, CarrierSettlementFactorID
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnitsID
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestWaitRateID 
			, DestWaitBillableMinutes 
			, DestWaitBillableHours
			, DestWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser
			, left(@Notes, 255)
		FROM #I

		/* compute the new Assessorial Rates for this order - into temp table #IA */
		/* NOTE: while it would be preferred for this to be out of the TX, it can't because it relies on the final Settlement changes being present */
		SELECT OrderID = @ID, AssessorialRateTypeID, AssessorialRateID = RateID, Amount = ROUND(Amount, 2), CreatedByUser = @UserName 
		INTO #IA
		FROM @SettlementUnits SU
		CROSS APPLY dbo.fnOrderCarrierAssessorialAmounts(@ID, SU.SettlementUnits, SU.SettlementUomID) CAA
		WHERE CAA.AssessorialRateTypeID NOT IN (SELECT ID FROM @ManualAssessorialRates)

		/* apply the computed settlement assessorial rate/amount records to the DB */
		INSERT INTO tblOrderSettlementCarrierAssessorialCharge (OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser)
			SELECT OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser FROM #IA
			UNION
			SELECT @ID, ID, NULL, Amount, @UserName FROM @ManualAssessorialRates

		IF (@CreatedTran = 1)
			COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @msg varchar(max), @severity int
		SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY()
		ROLLBACK
		RAISERROR(@msg, @severity, 1)
		RETURN
	END CATCH	

	IF (@debugStatements = 1) PRINT 'spApplyRatesCarrier (' + ltrim(@ID) + ') DONE ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	
END

GO

/*************************************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: compute and add the various Driver "Settlement" $$ values to an Delivered/Audited order
-- Changes:
-- 4.1.8.6	- 2016.09.21	- KDA	- move populate #IA table before transaction starts
--									- @debugStatements parameter + add additional START/DONE PRINT statements (for debugging purposes)
-- 4.1.9.1	- 2016.09.26	- KDA	- restore regressed 3.9.19.2 changes that accidentally got reverted with 4.1.8.6 (added a NOTE to hopefully prevent this again)
-- 4.4.1	- 2016/11/04	- JAE	- Add Destination Chainup
-- 4.6.2	- 2017.04.20	- KDA	- add @Notes parameter (and logic to persist to DB)
/*************************************************/
ALTER PROCEDURE spApplyRatesDriver
(
  @ID int
, @UserName varchar(100)
, @OriginWaitAmount money = NULL
, @DestWaitAmount money = NULL
, @RejectionAmount money = NULL
, @FuelSurchargeAmount money = NULL
, @LoadAmount money = NULL
, @Notes varchar(255) = NULL
, @AssessorialRateTypeID_CSV varchar(max) = NULL
, @AssessorialAmount_CSV varchar(max) = null
, @ResetOverrides bit = 0
, @AllowReApplyPostBatch bit = 0
, @debugStatements bit = 0 -- turn on to get debugging statements during execution
) AS BEGIN

	SET NOCOUNT ON

	IF (@debugStatements = 1) PRINT 'spApplyRatesDriver (' + ltrim(@ID) + ') START ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	
	IF (@AllowReApplyPostBatch = 0) AND EXISTS (SELECT * FROM tblOrderSettlementDriver WHERE OrderID = @ID AND BatchID IS NOT NULL)
	BEGIN
		RAISERROR('Invoice has already been Driver Settled', 16, 1)
		RETURN
	END
	
	/* ensure that Carrier Rates have been applied prior to applying Driver rates (since they could be dependent on Carrier rates) */
	IF NOT EXISTS (SELECT * FROM tblOrderSettlementCarrier WHERE OrderID = @ID)
		EXEC spApplyRatesCarrier @ID, @UserName

	/* transform the provided manual Assessorial Rates (if any) into a queryable table variable */
	DECLARE @ManualAssessorialRates TABLE (ID int primary key, Amount money)
	INSERT INTO @ManualAssessorialRates (ID, Amount) 
		SELECT ID.ID, R.Amount
		FROM (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), ID FROM dbo.fnSplitCSVIDs(@AssessorialRateTypeID_CSV)) ID
		JOIN (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), amount = CAST(value as money) FROM dbo.fnSplitCSV(@AssessorialAmount_CSV)) R ON R.sortid = ID.sortid

	/* persist all existing MANUAL Assessorial Rates from the existing Order Settlement (if any present and not resetting them) */
	INSERT INTO @ManualAssessorialRates (ID, Amount)
		SELECT AssessorialRateTypeID, Amount 
		FROM dbo.tblOrderSettlementDriverAssessorialCharge 
		WHERE OrderID = @ID AND AssessorialRateID IS NULL 
			AND ID NOT IN (SELECT ID FROM @ManualAssessorialRates)
			AND @ResetOverrides = 0

	/* persist all existing settlement amount overrides from the existing Order Settlement - if not resetting them */
	IF (@ResetOverrides = 0)
	BEGIN 
		SELECT @OriginWaitAmount = OriginWaitAmount FROM tblOrderSettlementDriver WHERE OrderID = @ID AND @OriginWaitAmount IS NULL AND OriginWaitRateID IS NULL
		SELECT @DestWaitAmount = DestinationWaitAmount FROM tblOrderSettlementDriver WHERE OrderID = @ID AND @DestWaitAmount IS NULL AND DestinationWaitRateID IS NULL
		SELECT @RejectionAmount = OrderRejectAmount FROM tblOrderSettlementDriver WHERE OrderID = @ID AND @RejectionAmount IS NULL AND OrderRejectRateID IS NULL
		SELECT @FuelSurchargeAmount = FuelSurchargeAmount FROM tblOrderSettlementDriver WHERE OrderID = @ID AND @FuelSurchargeAmount IS NULL AND FuelSurchargeRateID IS NULL
		SELECT @LoadAmount = LoadAmount FROM tblOrderSettlementDriver WHERE OrderID = @ID AND @LoadAmount IS NULL AND RouteRateID IS NULL AND RangeRateID IS NULL
	END
	/* persist any existing notes by preserving them if no new note is defined */
	SELECT @Notes = Notes FROM tblOrderSettlementDriver WHERE OrderID = @ID AND @Notes IS NULL AND Notes IS NOT NULL

	/* compute the basic settlement units from the Order being invoices */
	DECLARE @SettlementUnits TABLE
	(
		OrderID int
	  , SettlementUomID int
	  , SettlementFactorID int
	  , CarrierSettlementFactorID int
	  , MinSettlementUnits decimal(18, 10)
	  , MinSettlementUnitsID int
	  , ActualUnits decimal(18, 10)
	  , SettlementUnits decimal(18, 10) -- calculate value basically max(actual, minimum)
	)
	INSERT INTO @SettlementUnits (OrderID, SettlementUomID, SettlementFactorID, CarrierSettlementFactorID, MinSettlementUnits, MinSettlementUnitsID, ActualUnits, SettlementUnits)
		SELECT OSU.OrderID, OSU.SettlementUomID, OSU.SettlementFactorID, OSU.CarrierSettlementFactorID, ISNULL(OA.OverrideCarrierMinSettlementUnits, OSU.MinSettlementUnits), OSU.MinSettlementUnitsID, OSU.ActualUnits
			-- use passed settlement (already calculated) or pick the max between the actual and overrride min - JAE 1/15/16 - 3.9.39
			, CASE WHEN OA.OverrideCarrierMinSettlementUnits IS NULL THEN OSU.SettlementUnits
					ELSE dbo.fnMaxDecimal(OSU.ActualUnits, OA.OverrideCarrierMinSettlementUnits) END
		FROM dbo.viewOrderSettlementUnitsCarrier OSU
		LEFT JOIN tblOrderApproval OA ON OA.OrderID = OSU.OrderID
		WHERE OSU.OrderID = @ID

	/* compute the order level settlement rateIDs & settlement amounts - into temp table #I */
	SELECT OrderID = @ID
		, OrderDate
		, CarrierSettlementFactorID, MinSettlementUnitsID
		, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
		, RouteRateID, RangeRateID, LoadAmount
		, WaitFeeParameterID
		, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount 
		, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount
		, OrderRejectRateID, OrderRejectAmount
		, FuelSurchargeRateID, FuelSurchargeRate, FuelSurchargeAmount
		, OriginTaxRate
		, TotalAmount = ISNULL(LoadAmount, 0) 
			+ ISNULL(OriginWaitAmount, 0) 
			+ ISNULL(DestWaitAmount, 0) 
			+ ISNULL(OrderRejectAmount, 0) 
			+ ISNULL(FuelSurchargeAmount, 0)
		, CreatedByUser = @UserName
		, OriginChainUp, DestChainUp, H2S, Rerouted
	INTO #I
	FROM (
		SELECT OrderDate
			, CarrierSettlementFactorID, MinSettlementUnitsID
			, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
			, RouteRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RouteRateID ELSE NULL END
			, RangeRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RangeRateID ELSE NULL END
			, LoadAmount = round(ISNULL(@LoadAmount, RR.Amount), 2)
			, WaitFeeParameterID
			, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount = ROUND(OriginWaitAmount, 2)
			, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount = ROUND(DestWaitAmount, 2)
			, OrderRejectRateID = CASE WHEN @RejectionAmount IS NULL THEN RD.RateID ELSE NULL END
			, OrderRejectAmount = round(isnull(@RejectionAmount, RD.Amount), 2)
			, FuelSurchargeRateID = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.RateID ELSE NULL END
			, FuelSurchargeRate = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.Rate ELSE NULL END
			, FuelSurchargeAmount = round(isnull(@FuelSurchargeAmount, FSR.Amount), 2)
			, OriginTaxRate
			, OriginChainUp, DestChainup, H2S, Rerouted = cast(CASE WHEN RerouteCount = 0 THEN 0 ELSE 1 END as bit) /* used for the Auto-Approve logic below */
		FROM (
			SELECT O.ID
				, O.OriginID
				, O.CarrierID
				, O.RouteID
				, SU.CarrierSettlementFactorID
				, SU.SettlementFactorID
				, SU.SettlementUomID
				, SU.ActualUnits
				, SU.MinSettlementUnitsID
				, SU.MinSettlementUnits
				, SettlementUnits = CASE WHEN O.Rejected = 1 THEN 0 ELSE SU.SettlementUnits END
				, RouteMiles = isnull(OA.OverrideActualMiles, O.ActualMiles)
				, OrderDate = isnull(O.OrderDate, O.DueDate)
				, OriginChainUp = cast(CASE WHEN OA.OverrideOriginChainup = 1 THEN 0 ELSE O.OriginChainup END as bit)
				, DestChainUp = cast(CASE WHEN OA.OverrideDestChainup = 1 THEN 0 ELSE O.DestChainup END as bit)
				, H2S = cast(CASE WHEN OA.OverrideH2S = 1 THEN 0 ELSE O.H2S END as bit)
				, O.Rejected
				, RerouteCount = CASE WHEN OA.OverrideReroute = 1 THEN 0 ELSE O.RerouteCount END
				, IsSplit = CASE WHEN O.Rejected = 0 AND O.TicketCount > 1 THEN 1 ELSE 0 END
				, WaitFeeParameterID = WFP.ID
				, OriginWaitRateID = CASE WHEN @OriginWaitAmount IS NULL THEN OWD.RateID ELSE NULL END
				, OriginWaitBillableMinutes = OWD.BillableMinutes
				, OriginWaitBillableHours = OWD.BillableHours
				, OriginWaitAmount = isnull(@OriginWaitAmount, OWD.Amount)
				, DestWaitRateID = CASE WHEN @DestWaitAmount IS NULL THEN DWD.RateID ELSE NULL END
				, DestWaitBillableMinutes = DWD.BillableMinutes
				, DestWaitBillableHours = DWD.BillableHours
				, DestWaitAmount = isnull(@DestWaitAmount, DWD.Amount)
				, OriginTaxRate = OO.TaxRate
			FROM dbo.viewOrder O
			JOIN @SettlementUnits SU ON SU.OrderID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
			OUTER APPLY dbo.fnOrderDriverOriginWaitData(@ID) OWD 
			OUTER APPLY dbo.fnOrderDriverDestinationWaitData(@ID) DWD 
			OUTER APPLY dbo.fnOrderCarrierWaitFeeParameter(@ID) WFP
			WHERE O.ID = @ID
		) X
		OUTER APPLY dbo.fnOrderDriverLoadAmount(@ID, SettlementUnits, SettlementUomID) RR 
		OUTER APPLY dbo.fnOrderDriverOrderRejectData(@ID, SettlementUnits, SettlementUomID) RD
		OUTER APPLY dbo.fnOrderDriverFuelSurchargeData(@ID) FSR
	) X2

	DECLARE @CreatedTran bit; SET @CreatedTran = CASE WHEN @@TRANCOUNT = 0 THEN 1 ELSE 0 END
	BEGIN TRY
		IF (@CreatedTran = 1)
			BEGIN TRAN

		-- remove the existing settlment record (if any)
		DELETE FROM tblOrderSettlementDriverAssessorialCharge WHERE OrderID = @ID
		DELETE FROM tblOrderSettlementDriver WHERE OrderID = @ID
	
		-- all Units and Rates are first normalized to the Order.OriginUOM then consistent processing
		-- and persisted in the Invoice (Settlement) record in this Order.OriginUOM 
		INSERT INTO tblOrderSettlementDriver 
			(OrderID
			, OrderDate
			, CarrierSettlementFactorID
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnitsID
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestinationWaitRateID 
			, DestinationWaitBillableMinutes 
			, DestinationWaitBillableHours
			, DestinationWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser
			, Notes)
		SELECT OrderID
			, OrderDate
			, CarrierSettlementFactorID
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnitsID
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestWaitRateID 
			, DestWaitBillableMinutes 
			, DestWaitBillableHours
			, DestWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser
			, left(@Notes, 255)
		FROM #I

		/* compute the new Assessorial Rates for this order - into temp table #IA */
		/* NOTE: while it would be preferred for this to be out of the TX, it can't because it relies on the final Settlement changes being present */
		SELECT OrderID = @ID, AssessorialRateTypeID, AssessorialRateID = RateID, Amount = ROUND(Amount, 2), CreatedByUser = @UserName 
		INTO #IA
		FROM @SettlementUnits SU
		CROSS APPLY dbo.fnOrderDriverAssessorialAmounts(@ID, SU.SettlementUnits, SU.SettlementUomID) AA
		WHERE AA.AssessorialRateTypeID NOT IN (SELECT ID FROM @ManualAssessorialRates)

		/* apply the computed settlement assessorial rate/amount records to the DB */
		INSERT
		 INTO tblOrderSettlementDriverAssessorialCharge (OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser)
			SELECT OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser FROM #IA
			UNION
			SELECT @ID, ID, NULL, Amount, @UserName FROM @ManualAssessorialRates

		IF (@CreatedTran = 1)
			COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @msg varchar(max), @severity int
		SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY()
		ROLLBACK
		RAISERROR(@msg, @severity, 1)
		RETURN
	END CATCH	

	IF (@debugStatements = 1) PRINT 'spApplyRatesDriver (' + ltrim(@ID) + ') DONE ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	
END

GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Shipper "Settlement" $$ values to an Delivered/Audited order
-- Changes:
-- 3.9.0	- 2015/08/13	- KDA	- add Auto-Approve logic
--										- honor Approve overrides in rating process
-- 3.9.9	- 2015/08/31	- KDA	- add new @AllowReApplyPostBatch parameter to allow re-applying after batch is applied
-- 3.9.19.2	- 2015/09/24	- KDA	- fix erroneous logic that computed Asssessorial Settlement results before the Order Level results were posted
-- 3.9.37	- 2015/12/22	- JAE	- Use tblOrderApproval to get override for Shipper Min Settlement Units
-- 3.9.39	- 2016/01/15	- JAE	- Also need to recalculate settlement units since the calculated/max values was already passed
-- 3.13.1	- 2016/07/04	- KDA	- add START/DONE PRINT statements (for debugging purposes)
-- 4.1.8.6	- 2016.09.21	- KDA	- move populate #IA table before transaction starts
--									- fix erroneous OR ActualMiles criteria (needed to be wrapped in () to avoid unnecessary processing)
--									- @debugStatements parameter + add additional START/DONE PRINT statements (for debugging purposes)
-- 4.1.9.1	- 2016.09.26	- KDA	- restore regressed 3.9.19.2 changes that accidentally got reverted with 4.1.8.6 (added a NOTE to hopefully prevent this again)
-- 4.4.1	- 2016/11/04	- JAE	- Add Destination Chainup
-- 4.6.2	- 2017.04.20	- KDA	- add @Notes parameter (and logic to persist to DB)
/**********************************/
ALTER PROCEDURE spApplyRatesShipper
(
  @ID int
, @UserName varchar(100)
, @OriginWaitAmount money = NULL
, @DestWaitAmount money = NULL
, @RejectionAmount money = NULL
, @FuelSurchargeAmount money = NULL
, @LoadAmount money = NULL
, @Notes varchar(255) = NULL
, @AssessorialRateTypeID_CSV varchar(max) = NULL
, @AssessorialAmount_CSV varchar(max) = null
, @ResetOverrides bit = 0
, @AllowReApplyPostBatch bit = 0
, @debugStatements bit = 0 -- turn on to get debugging statements during execution
) AS BEGIN

	SET NOCOUNT ON
	
	IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ') START ' + (CONVERT(VARCHAR(24), GETDATE(), 121))

	-- ensure this order hasn't yet been fully settled
	IF (@AllowReApplyPostBatch = 0) AND EXISTS (SELECT * FROM tblOrderSettlementShipper WHERE OrderID = @ID AND BatchID IS NOT NULL)
	BEGIN
		RAISERROR('Invoice has already been Shipper Settled', 16, 1)
		RETURN
	END

	IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ').UpdateActualMiles START ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	-- ensure the current Route.ActualMiles is assigned to the order
	UPDATE tblOrder SET ActualMiles = R.ActualMiles
	FROM tblOrder O
	JOIN tblRoute R ON R.ID = O.RouteID
	LEFT JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = O.ID
	WHERE O.ID = @ID
	  AND (O.ActualMiles IS NULL OR O.ActualMiles <> R.ActualMiles)
	  AND (@AllowReApplyPostBatch = 1 OR OSS.BatchID IS NULL)
	IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ').UpdateActualMiles DONE ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	
	/* transform the provided manual Assessorial Rates (if any) into a queryable table variable */
	DECLARE @ManualAssessorialRates TABLE (ID int primary key, Amount money)
	INSERT INTO @ManualAssessorialRates (ID, Amount) 
		SELECT ID.ID, R.Amount
		FROM (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), ID FROM dbo.fnSplitCSVIDs(@AssessorialRateTypeID_CSV)) ID
		JOIN (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), amount = CAST(value as money) FROM dbo.fnSplitCSV(@AssessorialAmount_CSV)) R ON R.sortid = ID.sortid

	/* persist all existing MANUAL Assessorial Rates from the existing Order Settlement (if any present and not resetting them) */
	INSERT INTO @ManualAssessorialRates (ID, Amount)
		SELECT AssessorialRateTypeID, Amount 
		FROM dbo.tblOrderSettlementShipperAssessorialCharge 
		WHERE OrderID = @ID AND AssessorialRateID IS NULL 
			AND ID NOT IN (SELECT ID FROM @ManualAssessorialRates)
			AND @ResetOverrides = 0

	/* persist all existing settlement amount overrides from the existing Order Settlement - if not resetting them */
	IF (@ResetOverrides = 0)
	BEGIN 
		SELECT @OriginWaitAmount = OriginWaitAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @OriginWaitAmount IS NULL AND OriginWaitRateID IS NULL
		SELECT @DestWaitAmount = DestinationWaitAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @DestWaitAmount IS NULL AND DestinationWaitRateID IS NULL
		SELECT @RejectionAmount = OrderRejectAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @RejectionAmount IS NULL AND OrderRejectRateID IS NULL
		SELECT @FuelSurchargeAmount = FuelSurchargeAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @FuelSurchargeAmount IS NULL AND FuelSurchargeRateID IS NULL
		SELECT @LoadAmount = LoadAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @LoadAmount IS NULL AND RouteRateID IS NULL AND RangeRateID IS NULL
	END
	/* persist any existing notes by preserving them if no new note is defined */
	SELECT @Notes = Notes FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @Notes IS NULL AND Notes IS NOT NULL

	/* compute the basic settlement units from the Order being invoices */
	DECLARE @SettlementUnits TABLE
	(
		OrderID int
	  , SettlementUomID int
	  , SettlementFactorID int
	  , ShipperSettlementFactorID int
	  , MinSettlementUnits decimal(18, 10)
	  , MinSettlementUnitsID int
	  , ActualUnits decimal(18, 10)
	  , SettlementUnits decimal(18, 10)
	)
	IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ').RetrieveSettlementUnits START ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	INSERT INTO @SettlementUnits (OrderID, SettlementUomID, SettlementFactorID, ShipperSettlementFactorID, MinSettlementUnits, MinSettlementUnitsID, ActualUnits, SettlementUnits)
		SELECT OSU.OrderID, OSU.SettlementUomID, OSU.SettlementFactorID, OSU.ShipperSettlementFactorID, ISNULL(OA.OverrideShipperMinSettlementUnits, OSU.MinSettlementUnits), OSU.MinSettlementUnitsID, OSU.ActualUnits, 
			-- use passed settlement (already calculated) or pick the max between the actual and overrride min - JAE 1/15/16 - 3.9.39
			CASE WHEN OA.OverrideShipperMinSettlementUnits IS NULL THEN OSU.SettlementUnits
						ELSE dbo.fnMaxDecimal(OSU.ActualUnits, OA.OverrideShipperMinSettlementUnits) END
		FROM dbo.viewOrderSettlementUnitsShipper OSU
		LEFT JOIN tblOrderApproval OA ON OA.OrderID = OSU.OrderID
		WHERE OSU.OrderID = @ID
	IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ').RetrieveSettlementUnits DONE ' + (CONVERT(VARCHAR(24), GETDATE(), 121))

	/* compute the order level settlement rateIDs & settlement amounts - into temp table #I */
	IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ').RetrieveData START ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	SELECT OrderID = @ID
		, OrderDate
		, ShipperSettlementFactorID, MinSettlementUnitsID
		, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
		, RouteRateID, RangeRateID, LoadAmount
		, WaitFeeParameterID
		, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount 
		, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount
		, OrderRejectRateID, OrderRejectAmount
		, FuelSurchargeRateID, FuelSurchargeRate, FuelSurchargeAmount
		, OriginTaxRate
		, TotalAmount = ISNULL(LoadAmount, 0) 
			+ ISNULL(OriginWaitAmount, 0) 
			+ ISNULL(DestWaitAmount, 0) 
			+ ISNULL(OrderRejectAmount, 0) 
			+ ISNULL(FuelSurchargeAmount, 0)
		, CreatedByUser = @UserName
		, OriginChainup, DestChainup, H2S, Rerouted /* used for the Auto-Approve logic below */
	INTO #I
	FROM (
		SELECT OrderDate
			, ShipperSettlementFactorID, MinSettlementUnitsID
			, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
			, RouteRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RouteRateID ELSE NULL END
			, RangeRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RangeRateID ELSE NULL END
			, LoadAmount = round(ISNULL(@LoadAmount, RR.Amount), 2)
			, WaitFeeParameterID
			, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount = ROUND(OriginWaitAmount, 2)
			, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount = ROUND(DestWaitAmount, 2)
			, OrderRejectRateID = CASE WHEN @RejectionAmount IS NULL THEN RD.RateID ELSE NULL END
			, OrderRejectAmount = round(isnull(@RejectionAmount, RD.Amount), 2)
			, FuelSurchargeRateID = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.RateID ELSE NULL END
			, FuelSurchargeRate = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.Rate ELSE NULL END
			, FuelSurchargeAmount = round(isnull(@FuelSurchargeAmount, FSR.Amount), 2)
			, OriginTaxRate
			, OriginChainup, DestChainup, H2S, Rerouted = cast(CASE WHEN RerouteCount = 0 THEN 0 ELSE 1 END as bit) /* used for the Auto-Approve logic below */
		FROM (
			SELECT O.ID
				, O.OriginID
				, O.CustomerID
				, O.RouteID
				, SU.ShipperSettlementFactorID
				, SU.SettlementFactorID
				, SU.SettlementUomID
				, SU.ActualUnits
				, SU.MinSettlementUnitsID
				, SU.MinSettlementUnits
				, SettlementUnits = CASE WHEN O.Rejected = 1 THEN 0 ELSE SU.SettlementUnits END
				, RouteMiles = isnull(OA.OverrideActualMiles, O.ActualMiles)
				, OrderDate = isnull(O.OrderDate, O.DueDate)
				, OriginChainUp = cast(CASE WHEN OA.OverrideOriginChainup = 1 THEN 0 ELSE O.OriginChainup END as bit)
				, DestChainUp = cast(CASE WHEN OA.OverrideDestChainup = 1 THEN 0 ELSE O.DestChainup END as bit)
				, H2S = cast(CASE WHEN OA.OverrideH2S = 1 THEN 0 ELSE O.H2S END as bit)
				, O.Rejected
				, RerouteCount = CASE WHEN OA.OverrideReroute = 1 THEN 0 ELSE O.RerouteCount END
				, IsSplit = CASE WHEN O.Rejected = 0 AND O.TicketCount > 1 THEN 1 ELSE 0 END
				, WaitFeeParameterID = WFP.ID
				, OriginWaitRateID = CASE WHEN @OriginWaitAmount IS NULL THEN OWD.RateID ELSE NULL END
				, OriginWaitBillableMinutes = OWD.BillableMinutes
				, OriginWaitBillableHours = OWD.BillableHours
				, OriginWaitAmount = isnull(@OriginWaitAmount, OWD.Amount)
				, DestWaitRateID = CASE WHEN @DestWaitAmount IS NULL THEN DWD.RateID ELSE NULL END
				, DestWaitBillableMinutes = DWD.BillableMinutes
				, DestWaitBillableHours = DWD.BillableHours
				, DestWaitAmount = isnull(@DestWaitAmount, DWD.Amount)
				, OriginTaxRate = OO.TaxRate
			FROM dbo.viewOrder O
			JOIN @SettlementUnits SU ON SU.OrderID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
			OUTER APPLY dbo.fnOrderShipperOriginWaitData(@ID) OWD 
			OUTER APPLY dbo.fnOrderShipperDestinationWaitData(@ID) DWD 
			OUTER APPLY dbo.fnOrderShipperWaitFeeParameter(@ID) WFP
			WHERE O.ID = @ID
		) X
		OUTER APPLY dbo.fnOrderShipperLoadAmount(@ID, SettlementUnits, SettlementUomID) RR 
		OUTER APPLY dbo.fnOrderShipperOrderRejectData(@ID, SettlementUnits, SettlementUomID) RD
		OUTER APPLY dbo.fnOrderShipperFuelSurchargeData(@ID) FSR
	) X2
	IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ').RetrieveData DONE ' + (CONVERT(VARCHAR(24), GETDATE(), 121))

	IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ').StoreData START ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	DECLARE @CreatedTran bit; SET @CreatedTran = CASE WHEN @@TRANCOUNT = 0 THEN 1 ELSE 0 END
	BEGIN TRY
		IF (@CreatedTran = 1)
			BEGIN TRAN

		-- remove the existing settlment record (if any)
		DELETE FROM tblOrderSettlementShipperAssessorialCharge WHERE OrderID = @ID
		DELETE FROM tblOrderSettlementShipper WHERE OrderID = @ID

		-- all Units and Rates are first normalized to the Order.OriginUOM then consistent processing
		-- and persisted in the Invoice (Settlement) record in this Order.OriginUOM 
		INSERT INTO tblOrderSettlementShipper (
/*1*/		OrderID
/*2*/		, OrderDate
/*3*/		, ShipperSettlementFactorID
/*4*/		, SettlementFactorID 
/*5*/		, SettlementUomID 
/*6*/		, MinSettlementUnitsID
/*7*/		, MinSettlementUnits 
/*8*/		, SettlementUnits
/*9*/		, RouteRateID
/*10*/		, RangeRateID 
/*11*/		, LoadAmount
/*12*/		, WaitFeeParameterID 
/*13*/		, OriginWaitRateID 
/*14*/		, OriginWaitBillableMinutes 
/*15*/		, OriginWaitBillableHours
/*16*/		, OriginWaitAmount 
/*17*/		, DestinationWaitRateID 
/*18*/		, DestinationWaitBillableMinutes 
/*19*/		, DestinationWaitBillableHours
/*20*/		, DestinationWaitAmount 
/*21*/		, OrderRejectRateID 
/*22*/		, OrderRejectAmount 
/*23*/		, FuelSurchargeRateID 
/*24*/		, FuelSurchargeRate 
/*25*/		, FuelSurchargeAmount 
/*26*/		, OriginTaxRate 
/*27*/		, TotalAmount 
/*28*/		, CreatedByUser
/*29*/		, Notes)
SELECT 
/*01*/		OrderID
/*02*/		, OrderDate
/*03*/		, ShipperSettlementFactorID
/*04*/		, SettlementFactorID 
/*05*/		, SettlementUomID 
/*06*/		, MinSettlementUnitsID
/*07*/		, MinSettlementUnits 
/*08*/		, SettlementUnits
/*09*/		, RouteRateID
/*10*/		, RangeRateID 
/*11*/		, LoadAmount
/*12*/		, WaitFeeParameterID 
/*13*/		, OriginWaitRateID 
/*14*/		, OriginWaitBillableMinutes 
/*15*/		, OriginWaitBillableHours
/*16*/		, OriginWaitAmount 
/*17*/		, DestWaitRateID 
/*18*/		, DestWaitBillableMinutes 
/*19*/		, DestWaitBillableHours
/*20*/		, DestWaitAmount 
/*21*/		, OrderRejectRateID 
/*22*/		, OrderRejectAmount 
/*23*/		, FuelSurchargeRateID 
/*24*/		, FuelSurchargeRate 
/*25*/		, FuelSurchargeAmount 
/*26*/		, OriginTaxRate 
/*27*/		, TotalAmount 
/*28*/		, CreatedByUser
/*29*/		, left(@Notes, 255)
		FROM #I

		IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ').RetrieveAssessorialData START ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
		/* compute the new Assessorial Rates for this order - into temp table #IA */
		/* NOTE: while it would be preferred for this to be out of the TX, it can't because it relies on the final Settlement changes being present */
		SELECT OrderID = @ID, AssessorialRateTypeID, AssessorialRateID = RateID, Amount = ROUND(Amount, 2), CreatedByUser = @UserName 
		INTO #IA
		FROM @SettlementUnits SU
		CROSS APPLY dbo.fnOrderShipperAssessorialAmounts(@ID, SU.SettlementUnits, SU.SettlementUomID) CAA
		WHERE CAA.AssessorialRateTypeID NOT IN (SELECT ID FROM @ManualAssessorialRates)
		IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ').RetrieveAssessorialData DONE ' + (CONVERT(VARCHAR(24), GETDATE(), 121))

		/* apply the computed settlement assessorial rate/amount records to the DB */
		INSERT INTO tblOrderSettlementShipperAssessorialCharge (OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser)
			SELECT OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser FROM #IA
			UNION
			SELECT @ID, ID, NULL, Amount, @UserName FROM @ManualAssessorialRates

		IF (@CreatedTran = 1)
			COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @msg varchar(max), @severity int
		SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY()
		ROLLBACK
		RAISERROR(@msg, @severity, 1)
		RETURN
	END CATCH	
	IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ').StoreData DONE ' + (CONVERT(VARCHAR(24), GETDATE(), 121))

	IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ') DONE ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	
END

GO

exec _spRebuildAllObjects
go

/***********************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: return OrderSettlementDriverAssessorialCharge records with BatchId included (to determine whether actually "Settled" or not)
-- Changes:
--  4.6.2	- 2017.04.08 - KDA	- add RateType (JOIN + new field)
--								- add IsSystem field
/***********************************/
ALTER VIEW viewOrderSettlementDriverAssessorialCharge AS
	SELECT AC.*
		, SC.OrderDate
		 , SC.BatchID
		 , RateType = ART.Name
		 , ART.IsSystem
	FROM tblOrderSettlementDriverAssessorialCharge AC
	JOIN tblOrderSettlementDriver SC ON SC.OrderID = AC.OrderID
	JOIN tblAssessorialRateType ART ON ART.ID = AC.AssessorialRateTypeID

GO

exec _spDropView 'viewDriverSettlementBatch_PerDriver'
go
/***********************************/
-- Created: 4.6.2 - 2017.04.20 - Kevin Alons
-- Purpose: return DriverSettlementBatch records split into separate records per included Driver
-- Changes:
/***********************************/
CREATE VIEW viewDriverSettlementBatch_PerDriver AS
	SELECT SB.*, OS.*
	FROM tblDriverSettlementBatch SB
	CROSS APPLY (
		SELECT BatchOriginDriverID, BatchOriginDriver, TotalAmount = sum(InvoiceTotalAmount), OrderCount = count(*)
		FROM viewOrder_Financial_Base_Driver OS
		WHERE OS.BatchID = SB.ID
		GROUP BY BatchOriginDriverID, BatchOriginDriver
	) OS
GO

exec _spDropView 'viewOrderSettlementDriver_Amounts'
go
/***********************************/
-- Created: 4.6.2 - 2017.04.20 - Kevin Alons
-- Purpose: return all OrderSettlementDriver "Amounts" as individual rows (only present if value valid), used for "Driver Settlement Statement" pdf exports
-- Changes:
/***********************************/
CREATE VIEW viewOrderSettlementDriver_Amounts AS
	SELECT X.BatchID, X.OrderID, X.Name, Amount = isnull(X.Amount, 0.0), X.SortPos
		, OS.BatchOriginDriverID
	FROM (
		SELECT BatchID, OrderID, Name = 'Origin Demurrage', Amount = OriginWaitAmount, SortPos = 1 FROM tblOrderSettlementDriver WHERE OriginWaitAmount IS NOT NULL
		UNION ALL
		SELECT BatchID, OrderID, Name = 'Destination Demurrage', DestinationWaitAmount, SortPos = 2 FROM tblOrderSettlementDriver WHERE DestinationWaitAmount IS NOT NULL
		UNION ALL
		SELECT BatchID, OrderID, Name = 'Origin Chain-up', OriginChainupAmount, SortPos = 3 FROM viewOrderSettlementDriver WHERE OriginChainupAmount IS NOT NULL
		UNION ALL
		SELECT BatchID, OrderID, Name = 'Destination Chain-up', DestChainupAmount, SortPos = 4 FROM viewOrderSettlementDriver WHERE DestChainupAmount IS NOT NULL
		UNION ALL
		SELECT BatchID, OrderID, Name = 'Reject', OrderRejectAmount, SortPos = 5 FROM tblOrderSettlementDriver WHERE OrderRejectAmount IS NOT NULL
		UNION ALL
		SELECT BatchID, OrderID, Name = 'Reroute', RerouteAmount, SortPos = 6 FROM viewOrderSettlementDriver WHERE RerouteAmount IS NOT NULL
		UNION ALL
		SELECT BatchID, OrderID, Name = 'H2S', H2SAmount, SortPos = 7 FROM viewOrderSettlementDriver WHERE H2SAmount IS NOT NULL
		UNION ALL
		SELECT BatchID, OrderID, Name = 'Split Load', SplitLoadAmount, SortPos = 8 FROM viewOrderSettlementDriver WHERE SplitLoadAmount IS NOT NULL
		UNION ALL
		SELECT BatchID, OrderID, Name = 'Load Pay', LoadAmount, SortPos = 9 FROM viewOrderSettlementDriver
		UNION ALL
		SELECT BatchID, OrderID, Name = 'Load Rate', isnull(RateSheetRate, RouteRate), SortPos = 10 FROM viewOrderSettlementDriver
		UNION ALL
		/* add any non-system assessorial charges */
		SELECT BatchID, OrderID, RateType, Amount, SortPos = 100 FROM viewOrderSettlementDriverAssessorialCharge WHERE IsSystem = 0
	) X
	JOIN viewOrder_Financial_Base_Driver OS ON OS.BatchID = X.BatchID AND OS.OrderID = X.OrderID

GO

/***********************************/
-- Created: ?.?.? - 2013/06/02 - Kevin Alons
-- Purpose: compute and add the various Carrier "Settlement" $$ values to an Audited order
-- Changes:
-- 4.1.0	- 2016/08/21 - KDA	- use new @SessionID parameter to create the entire settlement record in a single invocation
-- 4.1.8.6	- 2016.09.24 - KDA	- optimize data retrieval into 2 separate steps (MAJOR performance improvement workaround)
-- 4.3.0.2	- 2016.11.11 - KDA	- prevent execution if any orders in the session are already Settled
--								- remove related tblOrderSettlementSessionCarrier record at end of settlement if all records are settled
-- 4.6.2	- 2017.04.18 - KDA	- add @PeriodEndDate parameter and add to tblCarrierSettlementBatch on SettlementBatch creation
/***********************************/
ALTER PROCEDURE spCreateCarrierSettlementBatch
(
  @SessionID varchar(100)
, @CarrierID int
, @PeriodEndDate date
, @InvoiceNum varchar(50)
, @Notes varchar(255)
, @UserName varchar(255)
, @BatchID int = NULL output
, @BatchNum int = NULL output
, @Outcome varchar(25) = 'Success' output
, @Msg varchar(255) = NULL output
) AS BEGIN
	IF @PeriodEndDate IS NULL SET @PeriodEndDate = cast(getdate() as date)

	-- retrieve the basic session order data first
	SELECT OS.OrderID, IsRated = cast(0 as bit), HasError = cast(0 as bit), IsSettled = cast(0 as bit)
	INTO #data
	FROM tblOrderSettlementSelectionCarrier OS 
	WHERE OS.SessionID = @SessionID AND OS.BatchSel = 1
	-- add the Financial information next (done in 2 steps for MAJOR performance reasons - a workaround but a more intuitive way was not identified)
	UPDATE #data
	  SET IsRated = cast(CASE WHEN OFB.InvoiceRatesAppliedDateUTC IS NULL THEN 0 ELSE 1 END as bit)
		, HasError = OFB.HasError
		, IsSettled = CASE WHEN BatchID IS NULL THEN 0 ELSE 1 END
	FROM #data O
	JOIN (
		SELECT OrderID, InvoiceRatesAppliedDateUTC, HasError , BatchID
		FROM viewOrder_Financial_Base_Carrier 
		WHERE OrderID IN (SELECT OrderID FROM #data)
	) OFB ON OFB.OrderID = O.OrderID

	-- validate the data next
	IF NOT EXISTS (SELECT * FROM #data)
	BEGIN
		SELECT @Outcome = 'Error', @Msg = 'At least order must be selected to complete settlement!'
		RETURN
	END
	ELSE IF EXISTS (SELECT * FROM #data WHERE IsRated = 0 OR HasError <> 0)
	BEGIN
		SELECT @Outcome = 'Error', @Msg = 'All selected orders must be fully rated with no errors to complete Settlement!'
		RETURN
	END
	ELSE IF EXISTS (SELECT * FROM #data WHERE IsSettled = 1)
	BEGIN
		SELECT @Outcome = 'Error', @Msg = 'At least 1 order is already settled - Refresh to re-query and continue!'
		RETURN
	END

	-- if validation passed, complete the settlement process below
	SET XACT_ABORT ON
	BEGIN TRAN CarrierSettlement

	INSERT INTO dbo.tblCarrierSettlementBatch(CarrierID, PeriodEndDate, InvoiceNum, BatchNum, BatchDate, Notes, CreateDateUTC, CreatedByUser)
		SELECT @CarrierID
			, @PeriodEndDate
			, @InvoiceNum
			, isnull(max(BatchNum), 0) + 1
			, dbo.fnDateOnly(getdate()), @Notes, getutcdate(), @UserName 
		FROM dbo.tblCarrierSettlementBatch
	SELECT @BatchID = scope_identity()
	SELECT @BatchNum = BatchNum FROM dbo.tblCarrierSettlementBatch WHERE ID = @BatchID

	UPDATE tblOrderSettlementCarrier 
		SET BatchID = @BatchID
	FROM tblOrderSettlementCarrier OSC
	JOIN #data D ON D.OrderID = OSC.OrderID
	WHERE BatchID IS NULL  -- double-check that we aren't re-settling the order

	SELECT @Outcome = 'Success', @Msg = 'Settlement process complete: ' + ltrim(count(1)) + ' record(s) were added to batch: ' + ltrim(@BatchNum)
	FROM #data

	DELETE FROM tblOrderSettlementSelectionCarrier WHERE SessionID = @SessionID AND BatchSel = 1
	IF NOT EXISTS (SELECT SessionID FROM tblOrderSettlementSelectionCarrier WHERE SessionID = @SessionID)
		DELETE FROM tblOrderSettlementSessionCarrier WHERE ID = @SessionID

	COMMIT TRAN
END

GO

/***********************************/
-- Created: ?.?.? - 2013/06/02 - Kevin Alons
-- Purpose: compute and add the various Driver "Settlement" $$ values to an Audited order
-- Changes:
-- 4.1.8.6	- 2016.09.24 - KDA	- optimize data retrieval into 2 separate steps (MAJOR performance improvement workaround)
-- 4.3.0.2	- 2016.11.11 - KDA	- prevent execution if any orders in the session are already Settled
--								- remove related tblOrderSettlementSessionDriver record at end of settlement if all records are settled
-- 4.6.2	- 2017.04.18 - KDA	- add @PeriodEndDate parameter and add to tblDriverSettlementBatch on SettlementBatch creation
/***********************************/
ALTER PROCEDURE spCreateDriverSettlementBatch
(
  @SessionID varchar(100)
, @CarrierID int
, @DriverGroupID int = NULL
, @DriverID int = NULL
, @PeriodEndDate date
, @InvoiceNum varchar(50) = NULL
, @Notes varchar(255) = NULL
, @UserName varchar(255) 
, @BatchID int = NULL output
, @BatchNum int = NULL output
, @Outcome varchar(25) = 'Success' output
, @Msg varchar(255) = NULL output
) AS BEGIN
	IF (@DriverGroupID = 0) SET @DriverGroupID = NULL
	IF (@DriverID = 0) SET @DriverID = NULL
	IF @PeriodEndDate IS NULL SET @PeriodEndDate = cast(getdate() as date)

	PRINT 'spCreateDriverSettlementBatch(' + ltrim(@SessionID) + ').Retrieve START ' + (CONVERT(VARCHAR(24), GETDATE(), 121))

	-- retrieve the basic session order data first
	SELECT OS.OrderID, IsRated = cast(0 as bit), HasError = cast(0 as bit), IsSettled = cast(0 as bit)
	INTO #data
	FROM tblOrderSettlementSelectionDriver OS 
	WHERE OS.SessionID = @SessionID AND OS.BatchSel = 1
	-- add the Financial information next (done in 2 steps for MAJOR performance reasons - a workaround but a more intuitive way was not identified)
	UPDATE #data
	  SET IsRated = cast(CASE WHEN OFB.InvoiceRatesAppliedDateUTC IS NULL THEN 0 ELSE 1 END as bit)
		, HasError = OFB.HasError
		, IsSettled = CASE WHEN BatchID IS NULL THEN 0 ELSE 1 END
	FROM #data O
	JOIN (
		SELECT OrderID, InvoiceRatesAppliedDateUTC, HasError, BatchID
		FROM viewOrder_Financial_Base_Driver 
		WHERE OrderID IN (SELECT OrderID FROM #data)
	) OFB ON OFB.OrderID = O.OrderID

	PRINT 'spCreateDriverSettlementBatch(' + ltrim(@SessionID) + ').Retrieve DONE ' + (CONVERT(VARCHAR(24), GETDATE(), 121))

	-- validate the data next
	IF NOT EXISTS (SELECT * FROM #data)
	BEGIN
		SELECT @Outcome = 'Error', @Msg = 'At least 1 order must be selected to complete settlement!'
		RETURN
	END
	ELSE IF EXISTS (SELECT * FROM #data WHERE IsRated = 0 OR HasError <> 0)
	BEGIN
		SELECT @Outcome = 'Error', @Msg = 'All selected orders must be fully rated with no errors to complete Settlement!'
		RETURN
	END
	ELSE IF EXISTS (SELECT * FROM #data WHERE IsSettled = 1)
	BEGIN
		SELECT @Outcome = 'Error', @Msg = 'At least 1 order is already settled - Refresh to re-query and continue!'
		RETURN
	END

	-- if validation passed, complete the settlement process below
	SET XACT_ABORT ON
	BEGIN TRAN DriverSettlement

	INSERT INTO dbo.tblDriverSettlementBatch(CarrierID, DriverGroupID, DriverID, PeriodEndDate, InvoiceNum, BatchNum, BatchDate, Notes, CreateDateUTC, CreatedByUser)
		SELECT @CarrierID, @DriverGroupID, @DriverID
			, @PeriodEndDate
			, @InvoiceNum
			, isnull(max(BatchNum), 0) + 1
			, dbo.fnDateOnly(getdate()), @Notes, getutcdate(), @UserName 
		FROM dbo.tblDriverSettlementBatch
	SELECT @BatchID = scope_identity()
	SELECT @BatchNum = BatchNum FROM dbo.tblDriverSettlementBatch WHERE ID = @BatchID

	UPDATE tblOrderSettlementDriver 
		SET BatchID = @BatchID
	FROM tblOrderSettlementDriver OSC
	JOIN #data D ON D.OrderID = OSC.OrderID
	WHERE BatchID IS NULL  -- double-check that we aren't re-settling the order

	SELECT @Outcome = 'Success', @Msg = 'Settlement process complete: ' + ltrim(count(1)) + ' record(s) were added to batch: ' + ltrim(@BatchNum)
	FROM #data

	DELETE FROM tblOrderSettlementSelectionDriver WHERE SessionID = @SessionID AND BatchSel = 1
	IF NOT EXISTS (SELECT SessionID FROM tblOrderSettlementSelectionDriver WHERE SessionID = @SessionID)
		DELETE FROM tblOrderSettlementSessionDriver WHERE ID = @SessionID

	COMMIT TRAN
END

GO

/***********************************/
-- Created: ?.?.? - 2013/06/02 - Kevin Alons
-- Purpose: compute and add the various Shipper "Settlement" $$ values to an Audited order
-- Changes:
-- 4.1.0 - 2016/08/21 - KDA - use new @SessionID parameter to create the entire settlement record in a single invocation
-- 4.1.8.6	- 2016.09.24 - KDA	- optimize data retrieval into 2 separate steps (MAJOR performance improvement workaround)
-- 4.3.0.2	- 2016.11.11 - KDA	- prevent execution if any orders in the session are already Settled
--								- remove related tblOrderSettlementSessionShipper record at end of settlement if all records are settled
-- 4.6.2	- 2017.04.18 - KDA	- add @PeriodEndDate parameter and add to tblShipperSettlementBatch on SettlementBatch creation
/***********************************/
ALTER PROCEDURE spCreateShipperSettlementBatch
(
  @SessionID varchar(100)
, @ShipperID int
, @PeriodEndDate date
, @InvoiceNum varchar(50)
, @Notes varchar(255)
, @UserName varchar(255)
, @BatchID int = NULL output
, @BatchNum int = NULL output
, @Outcome varchar(25) = 'Success' output
, @Msg varchar(255) = NULL output
) AS BEGIN
	IF @PeriodEndDate IS NULL SET @PeriodEndDate = cast(getdate() as date)

	-- retrieve the basic session order data first
	SELECT OS.OrderID, IsRated = cast(0 as bit), HasError = cast(0 as bit), IsSettled = cast(0 as bit)
	INTO #data
	FROM tblOrderSettlementSelectionShipper OS 
	WHERE OS.SessionID = @SessionID AND OS.BatchSel = 1
	-- add the Financial information next (done in 2 steps for MAJOR performance reasons - a workaround but a more intuitive way was not identified)
	UPDATE #data
	  SET IsRated = cast(CASE WHEN OFB.InvoiceRatesAppliedDateUTC IS NULL THEN 0 ELSE 1 END as bit)
		, HasError = OFB.HasError
		, IsSettled = CASE WHEN BatchID IS NULL THEN 0 ELSE 1 END
	FROM #data O
	JOIN (
		SELECT OrderID, InvoiceRatesAppliedDateUTC, HasError, BatchID
		FROM viewOrder_Financial_Base_Shipper 
		WHERE OrderID IN (SELECT OrderID FROM #data)
	) OFB ON OFB.OrderID = O.OrderID

	-- validate the data next
	IF NOT EXISTS (SELECT * FROM #data)
	BEGIN
		SELECT @Outcome = 'Error', @Msg = 'At least order must be selected to complete settlement!'
		RETURN
	END
	ELSE IF EXISTS (SELECT * FROM #data WHERE IsRated = 0 OR HasError <> 0)
	BEGIN
		SELECT @Outcome = 'Error', @Msg = 'All selected orders must be fully rated with no errors to complete Settlement!'
		RETURN
	END
	ELSE IF EXISTS (SELECT * FROM #data WHERE IsSettled = 1)
	BEGIN
		SELECT @Outcome = 'Error', @Msg = 'At least 1 order is already settled - Refresh to re-query and continue!'
		RETURN
	END

	-- if validation passed, complete the settlement process below
	SET XACT_ABORT ON
	BEGIN TRAN ShipperSettlement

	INSERT INTO dbo.tblShipperSettlementBatch(ShipperID, PeriodEndDate, InvoiceNum, BatchNum, BatchDate, Notes, CreateDateUTC, CreatedByUser)
		SELECT @ShipperID
			, @PeriodEndDate
			, @InvoiceNum
			, isnull(max(BatchNum), 0) + 1
			, dbo.fnDateOnly(getdate()), @Notes, getutcdate(), @UserName 
		FROM dbo.tblShipperSettlementBatch
	SELECT @BatchID = scope_identity()
	SELECT @BatchNum = BatchNum FROM dbo.tblShipperSettlementBatch WHERE ID = @BatchID

	UPDATE tblOrderSettlementShipper 
		SET BatchID = @BatchID
	FROM tblOrderSettlementShipper OSC
	JOIN #data D ON D.OrderID = OSC.OrderID
	WHERE BatchID IS NULL  -- double-check that we aren't re-settling the order

	SELECT @Outcome = 'Success', @Msg = 'Settlement process complete: ' + ltrim(count(1)) + ' record(s) were added to batch: ' + ltrim(@BatchNum)
	FROM #data

	DELETE FROM tblOrderSettlementSelectionShipper WHERE SessionID = @SessionID AND BatchSel = 1
	IF NOT EXISTS (SELECT SessionID FROM tblOrderSettlementSelectionShipper WHERE SessionID = @SessionID)
		DELETE FROM tblOrderSettlementSessionShipper WHERE ID = @SessionID

	COMMIT TRAN
END

GO

commit
go