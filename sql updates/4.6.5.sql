SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.6.4'
SELECT  @NewVersion = '4.6.5'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'JT-3465 - Fix Export error with Misc Details'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO




-- clear report column definitions with "NULL" string (instead of NULL)
UPDATE tblReportColumnDefinition 
SET DataFormat = NULL, FilterDataField = NULL, FilterDropDownSql = NULL
WHERE ID IN (90053, 90054, 90055)

-- Fix currency number formatting for misc $$ columns
UPDATE tblReportColumnDefinition 
SET DataFormat = '#0.00'
WHERE ID IN (90004, 90006, 90007, 90008, 90054, 90055)

GO


-- clear any reports that may have accidentally saved with "NULL" string
UPDATE tblUserReportColumnDefinition 
SET DataFormat = NULL
WHERE DataFormat = 'null'

GO


COMMIT
SET NOEXEC OFF