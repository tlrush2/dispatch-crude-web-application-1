SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.5.17'
SELECT  @NewVersion = '4.5.18'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'JT-2535 Fix deactivate stale locations to use duedate'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO



-- =============================================
-- Author:		Kevin Alons
-- Create date: 19 Jun 2014
-- Description:	deactivate any currently active Origins that haven't had any traffic since the Stale days threshold
-- Changes:
--			4.1.10 - 2016/09/23 - BB - Add code to additionlly deactivate the routes associated with the origins that get deactivated
--          4.5.18 - 2017/03/10 - JAE - Add check to use due date since tickets not begun can have a null order date
-- =============================================
ALTER PROCEDURE spDeactivateStaleOrigins
(
  @UserName varchar(100)
, @affectedRoutes int = null output
, @affectedOrigins int = null output
) 
AS BEGIN
	DECLARE @staleDays int
	SELECT @staleDays = Value FROM tblSetting WHERE ID = 21
	
	--Deactivate routes
	UPDATE tblRoute
		SET DeleteDateUTC = GETUTCDATE(), DeletedByUser = @UserName
	WHERE ID IN 
	(
		SELECT ID 
		FROM tblRoute
		WHERE DeleteDateUTC IS NULL			
			AND CreateDateUTC < DATEADD(day, -@staleDays, getdate())
			AND OriginID NOT IN (SELECT OriginID FROM viewOrder WHERE ISNULL(OrderDate, DueDate) > DATEADD(day, -@staleDays, getdate()))
	)

	SET @affectedRoutes = @@ROWCOUNT

	--Deactivate origins
	UPDATE tblOrigin 
		SET DeleteDateUTC = GETUTCDATE(), DeletedByUser = @UserName
	WHERE DeleteDateUTC IS NULL
		AND CreateDateUTC < DATEADD(day, -@staleDays, getdate())
		AND ID NOT IN (SELECT OriginID FROM viewOrder WHERE ISNULL(OrderDate, DueDate) > DATEADD(day, -@staleDays, getdate()))

	SET @affectedOrigins = @@ROWCOUNT
END

GO


-- =============================================
-- Author:		Kevin Alons
-- Create date: 19 Jun 2014
-- Description:	deactivate any currently active Destinations that haven't had any traffic since the Stale days threshold
-- Changes:
--			4.1.10 - 2016/09/23 - BB - Add code to additionlly deactivate the routes associated with the destinations that get deactivated
--									- Also corrected setting id to #22 (Destination Stale) because it was checking origin stale (#21)
--          4.5.18 - 2017/03/10 - JAE - Add check to use due date since tickets not begun can have a null order date
-- =============================================
ALTER PROCEDURE spDeactivateStaleDestinations
(
  @UserName varchar(100)
, @affectedRoutes int = null output
, @affectedDestinations int = null output
) 
AS BEGIN
	DECLARE @staleDays int
	SELECT @staleDays = Value FROM tblSetting WHERE ID = 22
	
	--Deactivate routes
	UPDATE tblRoute
		SET DeleteDateUTC = GETUTCDATE(), DeletedByUser = @UserName
	WHERE ID IN 
	(
		SELECT ID 
		FROM tblRoute
		WHERE DeleteDateUTC IS NULL
			AND CreateDateUTC < DATEADD(day, -@staleDays, getdate())
			AND DestinationID NOT IN (SELECT DestinationID FROM viewOrder WHERE ISNULL(OrderDate, DueDate) > DATEADD(day, -@staleDays, getdate()))
	)

	SET @affectedRoutes = @@ROWCOUNT

	--Deactivate destinations
	UPDATE tblDestination
		SET DeleteDateUTC = GETUTCDATE(), DeletedByUser = @UserName
	WHERE DeleteDateUTC IS NULL
		AND CreateDateUTC < DATEADD(day, -@staleDays, getdate())
		AND ID NOT IN (SELECT DestinationID FROM viewOrder WHERE ISNULL(OrderDate, DueDate) > DATEADD(day, -@staleDays, getdate()))

	SET @affectedDestinations = @@ROWCOUNT
END

GO

COMMIT
SET NOEXEC OFF