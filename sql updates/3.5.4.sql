-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.5.3'
SELECT  @NewVersion = '3.5.4'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'logic/modularity improvements to Rate/Parameter aspects of Financial Settlement module'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/***********************************************************/
--  Date Created: 22 June 2013
--  Author: Kevin Alons
--  Purpose: Round the Billable Wait minutes into a decimal hour value (based on rounding parameters)
/***********************************************************/
ALTER FUNCTION fnComputeBillableWaitHours(@min int, @SubUnitID int, @RoundingTypeID int) RETURNS decimal (18, 6) AS
BEGIN
	DECLARE @ret decimal(18, 6)
	SELECT @ret = @min
	
	IF (@SubUnitID <> 1 AND @RoundingTypeID <> 1)
	BEGIN
		SELECT @ret = @ret / @SubUnitID
		IF (@RoundingTypeID = 2)
			SELECT @ret = floor(@ret)
		ELSE IF (@RoundingTypeID = 3)
			SELECT @ret = round(@ret - 0.01, 0)
		ELSE IF (@RoundingTypeID = 4)
			SELECT @ret = round(@ret, 0)
		ELSE IF (@RoundingTypeID = 5)
			SELECT @ret = ceiling(@ret)
		ELSE
			SELECT @ret = @min
	END
	
	RETURN @ret / (60.0 / CASE WHEN (@SubUnitID = 1 OR @RoundingTypeID = 1) THEN 1 ELSE (SELECT TOP 1 cast(Name as int) FROM tblWaitFeeSubUnit WHERE ID = @SubUnitID) END)
END
GO

/********************************************************
-- Date Created: 8 Feb 2015
-- Author: Kevin Alons
-- Purpose: streamline rate ranking
********************************************************/
CREATE FUNCTION fnRateRanking(@searchID int, @rateID int, @matchRankVal int, @nullableOrderField bit = 0) RETURNS smallmoney AS 
BEGIN
	DECLARE @ret smallmoney
	SET @ret = CASE WHEN @searchID = @rateID THEN @matchRankVal + 0.01 WHEN (@nullableOrderField = 1 OR nullif(@searchID, 0) IS NOT NULL) AND @rateID IS NULL THEN 0.01 ELSE 0 END
	RETURN (@ret)
END

GO
GRANT EXECUTE ON fnRateRanking TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier AssessorialRate info for the specified order
/***********************************/
ALTER FUNCTION fnCarrierAssessorialRates(@StartDate date, @EndDate date, @TypeID int, @CarrierID int, @ProductGroupID int, @OriginID int, @DestinationID int, @OriginStateID int, @DestStateID int, @RegionID int, @ProducerID int, @OperatorID int, @BestMatchOnly bit = 0)
RETURNS 
	@ret TABLE (
		ID int
	  , TypeID int
	  , CarrierID int
	  , ProductGroupID int
	  , OriginID int
	  , DestinationID int
	  , OriginStateID int
	  , DestStateID int
	  , RegionID int
	  , ProducerID int
	  , OperatorID int
	  , Rate decimal(18, 10)
	  , RateTypeID int
	  , UomID int
	  , EffectiveDate date
	  , EndDate date
	  , NextEffectiveDate date
	  , BestMatch bit
	  , Ranking smallmoney
	  , Locked bit
	  , CreateDateUTC datetime
	  , CreatedByUser varchar(100)
	  , LastChangeDateUTC datetime
	  , LastChangedByUser varchar(100)
	)
AS BEGIN
	-- get the raw data (all matched rows)
	DECLARE @src TABLE (ID int, TypeID int, Ranking smallmoney)
	INSERT INTO @src (ID, TypeID, Ranking)
		SELECT ID, TypeID
			, Ranking =	dbo.fnRateRanking(@CarrierID, R.CarrierID, 256, 0)
					  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 128, 0)
					  + dbo.fnRateRanking(@OriginID, R.OriginID, 64, 0)
					  + dbo.fnRateRanking(@DestinationID, R.DestinationID, 32, 0)
					  + dbo.fnRateRanking(@OriginStateID, R.OriginStateID, 16, 0)
					  + dbo.fnRateRanking(@DestStateID, R.DestStateID, 8, 0)
					  + dbo.fnRateRanking(@RegionID, R.RegionID, 4, 0)
					  + dbo.fnRateRanking(@ProducerID, R.ProducerID, 2, 1)
					  + dbo.fnRateRanking(@OperatorID, R.OperatorID, 1, 1)
		FROM dbo.viewCarrierAssessorialRate R
		WHERE coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(R.ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@OriginID, 0), R.OriginID, 0) = coalesce(R.OriginID, nullif(@OriginID, 0), 0)
		  AND coalesce(nullif(@DestinationID, 0), R.DestinationID, 0) = coalesce(R.DestinationID, nullif(@DestinationID, 0), 0)
		  AND coalesce(nullif(@OriginStateID, 0), R.OriginStateID, 0) = coalesce(R.OriginStateID, nullif(@OriginStateID, 0), 0)
		  AND coalesce(nullif(@DestStateID, 0), R.DestStateID, 0) = coalesce(R.DestStateID, nullif(@DestStateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(R.RegionID, nullif(@RegionID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(R.ProducerID, nullif(@ProducerID, 0), 0)
		  AND coalesce(nullif(@OperatorID, 0), R.OperatorID, 0) = coalesce(R.OperatorID, nullif(@OperatorID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
		  AND (nullif(@TypeID, 0) IS NULL OR TypeID = @TypeID)

	-- return the rate data for the best-match rates 
	INSERT INTO @ret (ID, TypeID, CarrierID, ProductGroupID, OriginID, DestinationID, OriginStateID, DestStateID, RegionID, ProducerID, OperatorID, Rate, RateTypeID, UomID, EffectiveDate, EndDate, NextEffectiveDate, BestMatch, Ranking, Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser)
		SELECT CAR.ID, TypeID, CarrierID, ProductGroupID, OriginID, DestinationID, OriginStateID, DestStateID, RegionID, ProducerID, OperatorID, Rate, RateTypeID, UomID, EffectiveDate, EndDate, NextEffectiveDate, BestMatch, Ranking, Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser
		FROM viewCarrierAssessorialRate CAR
		JOIN (
			SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.TypeID IS NULL THEN 0 ELSE 1 END as bit)
			FROM @src S
			LEFT JOIN (
				SELECT TypeID, Ranking = MAX(Ranking)
				FROM @src
				WHERE @StartDate = ISNULL(@EndDate, @StartDate)
				  AND Ranking % 1 = 0.01 * 9  -- ensure some type of match occurred on all 9 criteria choices
				GROUP BY TypeID
			) X ON X.TypeID = S.TypeID AND X.Ranking = S.Ranking
		) X ON X.ID = CAR.ID
		WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
		
	RETURN
END
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier AssessorialRate info for the specified order
/***********************************/
ALTER FUNCTION [dbo].[fnOrderCarrierAssessorialRates](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, TypeID, Rate, RateTypeID, UomID
	FROM dbo.viewOrder O
	CROSS APPLY dbo.fnCarrierAssessorialRates(O.OrderDate, null, null, O.CarrierID, O.ProductGroupID, O.OriginID, O.DestinationID, O.OriginStateID, O.DestStateID, O.OriginRegionID, O.ProducerID, O.OperatorID, 1) R
	WHERE O.ID = @ID
	  AND ((R.TypeID = 1 AND O.ChainUp = 1)
		OR (R.TypeID = 2 AND O.RerouteCount > 0)
		OR (R.TypeID = 3 AND O.TicketCount > 1)
		OR (R.TypeID = 4 AND O.H2S = 1)
		OR R.TypeID > 4)
GO

DROP FUNCTION fnCarrierAssessorialRates_ForRange
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier AssessorialRate rows for the specified criteria
/***********************************/
CREATE FUNCTION fnCarrierAssessorialRatesDisplay(@StartDate date, @EndDate date, @TypeID int, @CarrierID int, @ProductGroupID int, @OriginID int, @DestinationID int, @OriginStateID int, @DestStateID int, @RegionID int, @ProducerID int, @OperatorID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.CarrierID, R.ProductGroupID, R.OriginID, R.DestinationID, R.OriginStateID, R.DestStateID, R.RegionID, R.ProducerID, R.OperatorID, R.TypeID, R.Rate, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate, R.NextEffectiveDate
		, Type = RT.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, Origin = O.Name
		, OriginFull = O.FullName
		, Destination = D.Name
		, DestinationFull = D.FullName
		, OriginState = OS.FullName
		, OriginStateAbbrev = OS.Abbreviation
		, DestState = DS.FullName
		, DestStateAbbrev = DS.Abbreviation
		, Region = RE.Name
		, RateType = RT.Name
		, Uom = U.Name
		, UomShort = U.Abbrev
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch, Ranking
	FROM dbo.fnCarrierAssessorialRates(@StartDate, @EndDate, @TypeID, @CarrierID, @ProductGroupID, @OriginID, @DestinationID, @OriginStateID, @DestStateID, @RegionID, @ProducerID, @OperatorID, 0) R
	JOIN tblAssessorialRateType RT ON RT.ID = R.TypeID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN viewOrigin O ON O.ID = R.OriginID
	LEFT JOIN viewDestination D ON D.ID = R.DestinationID
	LEFT JOIN tblState OS ON OS.ID = R.OriginStateID
	LEFT JOIN tblState DS ON DS.ID = R.DestStateID
	LEFT JOIN tblRegion RE ON RE.ID = R.RegionID
	LEFT JOIN tblUom U ON U.ID = R.UomID
	ORDER BY EffectiveDate
GO
GRANT SELECT ON fnCarrierAssessorialRatesDisplay TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier DestinationWaitRate info for the specified criteria
/***********************************/
ALTER FUNCTION fnCarrierDestinationWaitRate
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @CarrierID int
, @ProductGroupID int
, @DestinationID int
, @StateID int
, @RegionID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID, ReasonID
			, Ranking =	dbo.fnRateRanking(@ReasonID, R.ReasonID, 32, 1)
				  + dbo.fnRateRanking(@CarrierID, R.CarrierID, 16, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 8, 0)
				  + dbo.fnRateRanking(@DestinationID, R.DestinationID, 4, 0)
				  + dbo.fnRateRanking(@StateID, R.StateID, 2, 0)
				  + dbo.fnRateRanking(@RegionID, R.RegionID, 1, 0)
		FROM  dbo.viewCarrierDestinationWaitRate R
		WHERE coalesce(nullif(@ReasonID, 0), R.ReasonID, 0) = coalesce(ReasonID, nullif(@ReasonID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@DestinationID, 0), R.DestinationID, 0) = coalesce(DestinationID, nullif(@DestinationID, 0), 0)
		  AND coalesce(nullif(@StateID, 0), R.StateID, 0) = coalesce(StateID, nullif(@StateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	),
	cte2 AS
	(
		SELECT ID, S.ReasonID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT ReasonID = ISNULL(ReasonID, 0), Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 6  -- ensure some type of match occurred on all 6 criteria choices
			GROUP BY ReasonID
		) X ON X.ReasonID = isnull(S.ReasonID, 0) AND X.Ranking = S.Ranking
	), 
	cte3 AS
	(	-- when both ReasonID = NULL AND ReasonID = @ReasonID records are BestMatch = 1, favor the ReasonID = @ReasonID record
		SELECT ID, X.ReasonID, X.Ranking, BestMatch = CASE WHEN BEST.ReasonID IS NULL THEN X.BestMatch ELSE 0 END
		FROM cte2 X
		LEFT JOIN (
			SELECT TOP 1 ReasonID FROM cte2 WHERE ReasonID = @ReasonID AND BestMatch = 1 ORDER BY Ranking DESC
		) BEST ON X.ReasonID IS NULL AND X.BestMatch = 1
	)
	
	SELECT TOP (CASE WHEN @BestMatchOnly = 1 THEN 1 ELSE 10000 END) R.ID, R.ReasonID, CarrierID, ProductGroupID, DestinationID, StateID, RegionID, Rate, EffectiveDate, EndDate, NextEffectiveDate
	  , BestMatch, Ranking
	  , Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewCarrierDestinationWaitRate R
	JOIN cte3 X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
	ORDER BY Ranking DESC
)
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier DestinationWaitRate info for the specified order
/***********************************/
ALTER FUNCTION fnOrderCarrierDestinationWaitRate(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, Minutes = O.DestMinutes, R.Rate
	FROM viewOrder O
	CROSS APPLY dbo.fnCarrierDestinationWaitRate(O.OrderDate, null, O.DestWaitReasonID, O.CarrierID, O.ProductGroupID, O.OriginID, O.DestStateID, O.DestRegionID, 1) R
	WHERE O.ID = @ID

GO

DROP FUNCTION fnCarrierDestinationWaitRates_ForRange
GO

/***********************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier DestinationWaitRate rows for the specified criteria
/***********************************/
CREATE FUNCTION fnCarrierDestinationWaitRatesDisplay
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @CarrierID int
, @ProductGroupID int
, @DestinationID int
, @StateID int
, @RegionID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ReasonID, R.CarrierID, R.ProductGroupID, R.DestinationID, R.StateID, R.RegionID, R.Rate, R.EffectiveDate, R.EndDate, R.NextEffectiveDate
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, Destination = D.Name
		, DestinationFull = D.FullName
		, State = S.FullName
		, StateAbbrev = S.Abbreviation
		, Region = REG.Name
		, Reason = REA.Description
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
	FROM dbo.fnCarrierDestinationWaitRate(@StartDate, @EndDate, @ReasonID, @CarrierID, @ProductGroupID, @DestinationID, @StateID, @RegionID, 0) R
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN viewDestination D ON D.ID = R.DestinationID
	LEFT JOIN tblState S ON S.ID = R.StateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblDestinationWaitReason REA ON REA.ID = R.ReasonID
	ORDER BY EffectiveDate
GO
GRANT SELECT ON fnCarrierDestinationWaitRatesDisplay TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OriginWaitRate info for the specified criteria
/***********************************/
ALTER FUNCTION fnCarrierOriginWaitRate
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @CarrierID int
, @ProductGroupID int
, @OriginID int
, @StateID int
, @RegionID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID, ReasonID
			, Ranking =	dbo.fnRateRanking(@ReasonID, R.ReasonID, 32, 1)
				  + dbo.fnRateRanking(@CarrierID, R.CarrierID, 16, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 8, 0)
				  + dbo.fnRateRanking(@OriginID, R.OriginID, 4, 0)
				  + dbo.fnRateRanking(@StateID, R.StateID, 2, 0)
				  + dbo.fnRateRanking(@RegionID, R.RegionID, 1, 0)
		FROM  dbo.viewCarrierOriginWaitRate R
		WHERE coalesce(nullif(@ReasonID, 0), R.ReasonID, 0) = coalesce(ReasonID, nullif(@ReasonID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@OriginID, 0), R.OriginID, 0) = coalesce(OriginID, nullif(@OriginID, 0), 0)
		  AND coalesce(nullif(@StateID, 0), R.StateID, 0) = coalesce(StateID, nullif(@StateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	),
	cte2 AS
	(
		SELECT ID, S.ReasonID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT ReasonID = ISNULL(ReasonID, 0), Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 6  -- ensure some type of match occurred on all 6 criteria choices
			GROUP BY ReasonID
		) X ON X.ReasonID = isnull(S.ReasonID, 0) AND X.Ranking = S.Ranking
	), 
	cte3 AS
	(	-- when both ReasonID = NULL AND ReasonID = @ReasonID records are BestMatch = 1, favor the ReasonID = @ReasonID record
		SELECT ID, X.ReasonID, X.Ranking, BestMatch = CASE WHEN BEST.ReasonID IS NULL THEN X.BestMatch ELSE 0 END
		FROM cte2 X
		LEFT JOIN (
			SELECT TOP 1 ReasonID FROM cte2 WHERE ReasonID = @ReasonID AND BestMatch = 1 ORDER BY Ranking DESC
		) BEST ON X.ReasonID IS NULL AND X.BestMatch = 1
	)
	
	SELECT TOP (CASE WHEN @BestMatchOnly = 1 THEN 1 ELSE 10000 END) R.ID, R.ReasonID, CarrierID, ProductGroupID, OriginID, StateID, RegionID, Rate, EffectiveDate, EndDate, NextEffectiveDate
	  , BestMatch, Ranking
	  , Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewCarrierOriginWaitRate R
	JOIN cte3 X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
	ORDER BY Ranking DESC
)
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OriginWaitRate info for the specified order
/***********************************/
ALTER FUNCTION fnOrderCarrierOriginWaitRate(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, Minutes = O.DestMinutes, R.Rate
	FROM viewOrder O
	CROSS APPLY dbo.fnCarrierOriginWaitRate(O.OrderDate, null, O.DestWaitReasonID, O.CarrierID, O.ProductGroupID, O.OriginID, O.DestStateID, O.DestRegionID, 1) R
	WHERE O.ID = @ID

GO

DROP FUNCTION fnCarrierOriginWaitRates_ForRange
GO

/***********************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OriginWaitRate rows for the specified criteria
/***********************************/
CREATE FUNCTION fnCarrierOriginWaitRatesDisplay
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @CarrierID int
, @ProductGroupID int
, @OriginID int
, @StateID int
, @RegionID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ReasonID, R.CarrierID, R.ProductGroupID, R.OriginID, R.StateID, R.RegionID, R.Rate, R.EffectiveDate, R.EndDate, R.NextEffectiveDate
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, Origin = D.Name
		, OriginFull = D.FullName
		, State = S.FullName
		, StateAbbrev = S.Abbreviation
		, Region = REG.Name
		, Reason = REA.Description
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
	FROM dbo.fnCarrierOriginWaitRate(@StartDate, @EndDate, @ReasonID, @CarrierID, @ProductGroupID, @OriginID, @StateID, @RegionID, 0) R
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN viewOrigin D ON D.ID = R.OriginID
	LEFT JOIN tblState S ON S.ID = R.StateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblOriginWaitReason REA ON REA.ID = R.ReasonID
	ORDER BY EffectiveDate
GO
GRANT SELECT ON fnCarrierOriginWaitRatesDisplay TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OrderRejectRate info for the specified criteria
/***********************************/
ALTER FUNCTION fnCarrierOrderRejectRate
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @CarrierID int
, @ProductGroupID int
, @OriginID int
, @StateID int
, @RegionID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID, ReasonID
			, Ranking =	dbo.fnRateRanking(@ReasonID, R.ReasonID, 32, 1)
				  + dbo.fnRateRanking(@CarrierID, R.CarrierID, 16, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 8, 0)
				  + dbo.fnRateRanking(@OriginID, R.OriginID, 4, 0)
				  + dbo.fnRateRanking(@StateID, R.StateID, 2, 0)
				  + dbo.fnRateRanking(@RegionID, R.RegionID, 1, 0)
		FROM  dbo.viewCarrierOrderRejectRate R
		WHERE coalesce(nullif(@ReasonID, 0), R.ReasonID, 0) = coalesce(ReasonID, nullif(@ReasonID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@OriginID, 0), R.OriginID, 0) = coalesce(OriginID, nullif(@OriginID, 0), 0)
		  AND coalesce(nullif(@StateID, 0), R.StateID, 0) = coalesce(StateID, nullif(@StateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	),
	cte2 AS
	(
		SELECT ID, S.ReasonID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT ReasonID = ISNULL(ReasonID, 0), Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 6  -- ensure some type of match occurred on all 6 criteria choices
			GROUP BY ReasonID
		) X ON X.ReasonID = isnull(S.ReasonID, 0) AND X.Ranking = S.Ranking
	), 
	cte3 AS
	(	-- when both ReasonID = NULL AND ReasonID = @ReasonID records are BestMatch = 1, favor the ReasonID = @ReasonID record
		SELECT ID, X.ReasonID, X.Ranking, BestMatch = CASE WHEN BEST.ReasonID IS NULL THEN X.BestMatch ELSE 0 END
		FROM cte2 X
		LEFT JOIN (
			SELECT TOP 1 ReasonID FROM cte2 WHERE ReasonID = @ReasonID AND BestMatch = 1 ORDER BY Ranking DESC
		) BEST ON X.ReasonID IS NULL AND X.BestMatch = 1
	)
	
	SELECT R.ID, R.ReasonID, CarrierID, ProductGroupID, OriginID, StateID, RegionID, Rate, RateTypeID, UomID, EffectiveDate, EndDate, NextEffectiveDate
	  , BestMatch, Ranking
	  , Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewCarrierOrderRejectRate R
	JOIN cte3 X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OrderRejectRate info for the specified order
/***********************************/
ALTER FUNCTION fnOrderCarrierOrderRejectRate(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, Minutes = O.DestMinutes, R.Rate, R.RateTypeID, R.UomID
	FROM viewOrder O
	CROSS APPLY dbo.fnCarrierOrderRejectRate(O.OrderDate, null, O.DestWaitReasonID, O.CarrierID, O.ProductGroupID, O.OriginID, O.DestStateID, O.DestRegionID, 1) R
	WHERE O.ID = @ID

GO

DROP FUNCTION fnCarrierOrderRejectRates_ForRange
GO

/***********************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OrderRejectRate rows for the specified criteria
/***********************************/
CREATE FUNCTION fnCarrierOrderRejectRatesDisplay
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @CarrierID int
, @ProductGroupID int
, @OriginID int
, @StateID int
, @RegionID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ReasonID, R.CarrierID, R.ProductGroupID, R.OriginID, R.StateID, R.RegionID, R.Rate, R.EffectiveDate, R.EndDate, R.NextEffectiveDate
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, Origin = D.Name
		, OriginFull = D.FullName
		, State = S.FullName
		, StateAbbrev = S.Abbreviation
		, Region = REG.Name
		, Reason = REA.Description
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
	FROM dbo.fnCarrierOrderRejectRate(@StartDate, @EndDate, @ReasonID, @CarrierID, @ProductGroupID, @OriginID, @StateID, @RegionID, 0) R
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN viewOrigin D ON D.ID = R.OriginID
	LEFT JOIN tblState S ON S.ID = R.StateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblOrderRejectReason REA ON REA.ID = R.ReasonID
	ORDER BY EffectiveDate
GO
GRANT SELECT ON fnCarrierOrderRejectRatesDisplay TO dispatchcrude_iis_acct
GO

ALTER TABLE dbo.tblCarrierRangeRate
	DROP CONSTRAINT FK_CarrierRangeRate
GO
ALTER TABLE dbo.tblCarrierRateSheet SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblCarrierRangeRate ADD CONSTRAINT
	FK_CarrierRangeRate_CarrierRateSheet FOREIGN KEY
	(
	RateSheetID
	) REFERENCES dbo.tblCarrierRateSheet
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	
GO
ALTER TABLE dbo.tblCarrierRangeRate SET (LOCK_ESCALATION = TABLE)
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RateSheetRangeRate info for the specified criteria
/***********************************/
ALTER FUNCTION fnCarrierRateSheetRangeRate(@StartDate date, @EndDate date, @RouteMiles int, @CarrierID int, @ProductGroupID int, @OriginStateID int, @DestStateID int, @RegionID int, @BestMatchOnly bit = 0)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT RateSheetID = R.ID, RangeRateID = RR.ID
			, Ranking = CASE WHEN @RouteMiles BETWEEN RR.MinRange AND RR.MaxRange THEN 32.01 ELSE 0 END
				+ dbo.fnRateRanking(@CarrierID, R.CarrierID, 16, 0)
				+ dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 8, 0)
				+ dbo.fnRateRanking(@OriginStateID, R.OriginStateID, 4, 0)
				+ dbo.fnRateRanking(@DestStateID, R.DestStateID, 2, 0)
				+ dbo.fnRateRanking(@RegionID, R.RegionID, 1, 0)
		FROM dbo.viewCarrierRateSheet R
		JOIN dbo.tblCarrierRangeRate RR ON RR.RateSheetID = R.ID AND isnull(@RouteMiles, RR.MinRange) BETWEEN RR.MinRange AND RR.MaxRange
		WHERE coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@OriginStateID, 0), R.OriginStateID, 0) = coalesce(OriginStateID, nullif(@OriginStateID, 0), 0)
		  AND coalesce(nullif(@DestStateID, 0), R.DestStateID, 0) = coalesce(DestStateID, nullif(@DestStateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	)
	
	SELECT RR.ID, RateSheetID = R.ID, CarrierID, ProductGroupID, OriginStateID, DestStateID, RegionID, Rate, MinRange, MaxRange, RateTypeID, UomID, EffectiveDate, EndDate, NextEffectiveDate
	  , BestMatch, Ranking
	  , Locked, R.CreateDateUTC, R.CreatedByUser, R.LastChangeDateUTC, R.LastChangedByUser 
	FROM dbo.viewCarrierRateSheet R
	JOIN dbo.tblCarrierRangeRate RR ON RR.RateSheetID = R.ID
	JOIN (
		SELECT RateSheetID, RangeRateID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 6  -- ensure some type of match occurred on all 6 criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.RateSheetID = R.ID AND X.RangeRateID = RR.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RateSheetRangeRate info for the specified order
/***********************************/
ALTER FUNCTION fnOrderCarrierRateSheetRangeRate(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, RateTypeID, Rate, UomID
	FROM dbo.viewOrder O
	CROSS APPLY dbo.fnCarrierRateSheetRangeRate(O.OrderDate, null, O.ActualMiles, O.CarrierID, O.ProductGroupID, O.OriginStateID, O.DestStateID, O.OriginRegionID, 1) R
	WHERE O.ID = @ID
GO

DROP FUNCTION fnCarrierRateSheetRangeRates_ForRange
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RateSheetRangeRate info for the specified criteria
/***********************************/
CREATE FUNCTION fnCarrierRateSheetRangeRatesDisplay(@StartDate date, @EndDate date, @RouteMiles int, @CarrierID int, @ProductGroupID int, @OriginStateID int, @DestStateID int, @RegionID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.CarrierID, R.ProductGroupID, R.OriginStateID, R.DestStateID, R.RegionID, R.Rate, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate, R.NextEffectiveDate
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, OriginState = OS.FullName
		, OriginStateAbbrev = OS.Abbreviation
		, DestState = OS.FullName
		, DestStateAbbrev = OS.Abbreviation
		, Region = REG.Name
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
		, Ranking
	FROM dbo.fnCarrierRateSheetRangeRate(@StartDate, @EndDate, @RouteMiles, @CarrierID, @ProductGroupID, @OriginStateID, @DestStateID, @RegionID, 0) R
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblState OS ON OS.ID = R.OriginStateID
	LEFT JOIN tblState DS ON DS.ID = R.DestStateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	ORDER BY EffectiveDate
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RouteRate info for the specified criteria
/***********************************/
ALTER FUNCTION fnCarrierRouteRate(@StartDate date, @EndDate date, @RouteID int, @CarrierID int, @ProductGroupID int, @BestMatchOnly bit = 0) 
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID
			, Ranking =	dbo.fnRateRanking(@CarrierID, R.CarrierID, 2, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 1, 0)
		FROM  dbo.viewCarrierRouteRate R
		WHERE RouteID = @RouteID
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	)
	
	SELECT R.ID, RouteID, OriginID, DestinationID, CarrierID, ProductGroupID, Rate, RateTypeID, UomID, EffectiveDate, EndDate, NextEffectiveDate
	  , BestMatch, Ranking
	  , Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewCarrierRouteRate R
	JOIN (
		SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 2  -- ensure some type of match occurred on all 6 criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RouteRate info for the specified order
/***********************************/
ALTER FUNCTION fnOrderCarrierRouteRate(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, RateTypeID, Rate, R.UomID
	FROM dbo.viewOrder O
	CROSS APPLY dbo.fnCarrierRouteRate(O.OrderDate, null, O.RouteID, O.CarrierID, O.ProductGroupID, 1) R
	WHERE O.ID = @ID 
GO

DROP FUNCTION fnCarrierRouteRates_ForRange
GO

/***********************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier RouteRate rows for the specified criteria
/***********************************/
CREATE FUNCTION fnCarrierRouteRatesDisplay(@StartDate date, @EndDate date, @CarrierID int, @ProductGroupID int, @OriginID int, @DestinationID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.RouteID, RO.ActualMiles, R.OriginID, R.DestinationID, R.CarrierID, R.ProductGroupID, R.Rate, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate, R.NextEffectiveDate
		, Origin = O.Name
		, OriginFull = O.FullName
		, Destination = D.Name
		, DestinationFull = D.FullName
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, RateType = RT.Name
		, Uom = U.Name
		, UomShort = U.Abbrev
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch, Ranking
	FROM dbo.fnCarrierRouteRate(@StartDate, @EndDate, @CarrierID, @ProductGroupID, @OriginID, @DestinationID) R
	JOIN tblRoute RO ON RO.ID = R.RouteID
	JOIN viewOrigin O ON O.ID = R.OriginID
	JOIN viewDestination D ON D.ID = R.DestinationID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblRateType RT ON RT.ID = R.RateTypeID
	LEFT JOIN tblUom U ON U.ID = R.UomID
	ORDER BY EffectiveDate
GO
GRANT SELECT ON fnCarrierRouteRatesDisplay TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier WaitFeeParameter info for the specified order
/***********************************/
ALTER FUNCTION fnCarrierWaitFeeParameter(@StartDate date, @EndDate date, @CarrierID int, @ProductGroupID int, @OriginStateID int, @DestStateID int, @RegionID int, @bestMatchOnly bit = 0)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID
			, Ranking = dbo.fnRateRanking(@CarrierID, R.CarrierID, 16, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 8, 0)
				  + dbo.fnRateRanking(@OriginStateID, R.OriginStateID, 4, 0)
				  + dbo.fnRateRanking(@DestStateID, R.DestStateID, 2, 0)
				  + dbo.fnRateRanking(@RegionID, R.RegionID, 1, 0)
		FROM  dbo.viewCarrierWaitFeeParameter R
		WHERE coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@OriginStateID, 0), R.OriginStateID, 0) = coalesce(OriginStateID, nullif(@OriginStateID, 0), 0)
		  AND coalesce(nullif(@DestStateID, 0), R.DestStateID, 0) = coalesce(DestStateID, nullif(@DestStateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	)
	
	SELECT R.ID, CarrierID, ProductGroupID, OriginStateID, DestStateID, RegionID, SubUnitID, RoundingTypeID, ThresholdMinutes, EffectiveDate, EndDate, NextEffectiveDate
	  , BestMatch, Ranking
	  , Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewCarrierWaitFeeParameter R
	JOIN (
		SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 5  -- ensure some type of match occurred on all 6 criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier WaitFeeParameter info for the specified order
/***********************************/
ALTER FUNCTION fnOrderCarrierWaitFeeParameter(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, SubUnitID, RoundingTypeID, ThresholdMinutes
	FROM dbo.viewOrder O
	CROSS APPLY dbo.fnCarrierWaitFeeParameter(O.OrderDate, null, O.CarrierID, O.ProductGroupID, O.OriginStateID, O.DestStateID, O.OriginRegionID, 1) R
	WHERE O.ID = @ID 
GO

DROP FUNCTION fnCarrierWaitFeeParameters_ForRange
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier WaitFeeParameter rows for the specified criteria
/***********************************/
CREATE FUNCTION fnCarrierWaitFeeParametersDisplay(@StartDate date, @EndDate date, @CarrierID int, @ProductGroupID int, @OriginStateID int, @DestStateID int, @RegionID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.CarrierID, R.ProductGroupID, R.OriginStateID, R.DestStateID, R.RegionID, R.SubUnitID, R.RoundingTypeID, R.ThresholdMinutes, R.EffectiveDate, R.EndDate, R.NextEffectiveDate
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, OriginState = OS.FullName
		, OriginStateAbbrev = OS.Abbreviation
		, DestinationState = DS.FullName
		, DestinationStateAbbrev = DS.Abbreviation
		, Region = REG.Name
		, RoundingType = WFRT.Name
		, SubUnit = WFSU.Name
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch, Ranking
	FROM dbo.fnCarrierWaitFeeParameter(@StartDate, @EndDate, @CarrierID, @ProductGroupID, @OriginStateID, @DestStateID, @RegionID, 0) R 
	JOIN tblWaitFeeRoundingType WFRT ON WFRT.ID = R.RoundingTypeID
	JOIN tblWaitFeeSubUnit WFSU ON WFSU.ID = R.SubUnitID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblState OS ON OS.ID = R.OriginStateID
	LEFT JOIN tblState DS ON DS.ID = R.DestStateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	ORDER BY EffectiveDate
GO
GRANT SELECT ON fnCarrierWaitFeeParametersDisplay TO dispatchcrude_iis_acct
GO

---------------------------------------------------
-- SHIPPER CHANGES
---------------------------------------------------

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper AssessorialRate info for the specified order
/***********************************/
ALTER FUNCTION fnShipperAssessorialRates(@StartDate date, @EndDate date, @TypeID int, @ShipperID int, @ProductGroupID int, @OriginID int, @DestinationID int, @OriginStateID int, @DestStateID int, @RegionID int, @ProducerID int, @OperatorID int, @BestMatchOnly bit = 0)
RETURNS 
	@ret TABLE (
		ID int
	  , TypeID int
	  , ShipperID int
	  , ProductGroupID int
	  , OriginID int
	  , DestinationID int
	  , OriginStateID int
	  , DestStateID int
	  , RegionID int
	  , ProducerID int
	  , OperatorID int
	  , Rate decimal(18, 10)
	  , RateTypeID int
	  , UomID int
	  , EffectiveDate date
	  , EndDate date
	  , NextEffectiveDate date
	  , BestMatch bit
	  , Ranking smallmoney
	  , Locked bit
	  , CreateDateUTC datetime
	  , CreatedByUser varchar(100)
	  , LastChangeDateUTC datetime
	  , LastChangedByUser varchar(100)
	)
AS BEGIN
	-- get the raw data (all matched rows)
	DECLARE @src TABLE (ID int, TypeID int, Ranking smallmoney)
	INSERT INTO @src (ID, TypeID, Ranking)
		SELECT ID, TypeID
			, Ranking =	dbo.fnRateRanking(@ShipperID, R.ShipperID, 256, 0)
					  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 128, 0)
					  + dbo.fnRateRanking(@OriginID, R.OriginID, 64, 0)
					  + dbo.fnRateRanking(@DestinationID, R.DestinationID, 32, 0)
					  + dbo.fnRateRanking(@OriginStateID, R.OriginStateID, 16, 0)
					  + dbo.fnRateRanking(@DestStateID, R.DestStateID, 8, 0)
					  + dbo.fnRateRanking(@RegionID, R.RegionID, 4, 0)
					  + dbo.fnRateRanking(@ProducerID, R.ProducerID, 2, 1)
					  + dbo.fnRateRanking(@OperatorID, R.OperatorID, 1, 1)
		FROM dbo.viewShipperAssessorialRate R
		WHERE coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(R.ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@OriginID, 0), R.OriginID, 0) = coalesce(R.OriginID, nullif(@OriginID, 0), 0)
		  AND coalesce(nullif(@DestinationID, 0), R.DestinationID, 0) = coalesce(R.DestinationID, nullif(@DestinationID, 0), 0)
		  AND coalesce(nullif(@OriginStateID, 0), R.OriginStateID, 0) = coalesce(R.OriginStateID, nullif(@OriginStateID, 0), 0)
		  AND coalesce(nullif(@DestStateID, 0), R.DestStateID, 0) = coalesce(R.DestStateID, nullif(@DestStateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(R.RegionID, nullif(@RegionID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(R.ProducerID, nullif(@ProducerID, 0), 0)
		  AND coalesce(nullif(@OperatorID, 0), R.OperatorID, 0) = coalesce(R.OperatorID, nullif(@OperatorID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
		  AND (nullif(@TypeID, 0) IS NULL OR TypeID = @TypeID)

	-- return the rate data for the best-match rates 
	INSERT INTO @ret (ID, TypeID, ShipperID, ProductGroupID, OriginID, DestinationID, OriginStateID, DestStateID, RegionID, ProducerID, OperatorID, Rate, RateTypeID, UomID, EffectiveDate, EndDate, NextEffectiveDate, BestMatch, Ranking, Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser)
		SELECT CAR.ID, TypeID, ShipperID, ProductGroupID, OriginID, DestinationID, OriginStateID, DestStateID, RegionID, ProducerID, OperatorID, Rate, RateTypeID, UomID, EffectiveDate, EndDate, NextEffectiveDate, BestMatch, Ranking, Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser
		FROM viewShipperAssessorialRate CAR
		JOIN (
			SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.TypeID IS NULL THEN 0 ELSE 1 END as bit)
			FROM @src S
			LEFT JOIN (
				SELECT TypeID, Ranking = MAX(Ranking)
				FROM @src
				WHERE @StartDate = ISNULL(@EndDate, @StartDate)
				  AND Ranking % 1 = 0.01 * 9  -- ensure some type of match occurred on all 9 criteria choices
				GROUP BY TypeID
			) X ON X.TypeID = S.TypeID AND X.Ranking = S.Ranking
		) X ON X.ID = CAR.ID
		WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
		
	RETURN
END
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper AssessorialRate info for the specified order
/***********************************/
ALTER FUNCTION [dbo].[fnOrderShipperAssessorialRates](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, TypeID, Rate, RateTypeID, UomID
	FROM dbo.viewOrder O
	CROSS APPLY dbo.fnShipperAssessorialRates(O.OrderDate, null, null, O.CustomerID, O.ProductGroupID, O.OriginID, O.DestinationID, O.OriginStateID, O.DestStateID, O.OriginRegionID, O.ProducerID, O.OperatorID, 1) R
	WHERE O.ID = @ID
	  AND ((R.TypeID = 1 AND O.ChainUp = 1)
		OR (R.TypeID = 2 AND O.RerouteCount > 0)
		OR (R.TypeID = 3 AND O.TicketCount > 1)
		OR (R.TypeID = 4 AND O.H2S = 1)
		OR R.TypeID > 4)
GO

DROP FUNCTION fnShipperAssessorialRates_ForRange
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper AssessorialRate rows for the specified criteria
/***********************************/
CREATE FUNCTION fnShipperAssessorialRatesDisplay(@StartDate date, @EndDate date, @TypeID int, @ShipperID int, @ProductGroupID int, @OriginID int, @DestinationID int, @OriginStateID int, @DestStateID int, @RegionID int, @ProducerID int, @OperatorID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ShipperID, R.ProductGroupID, R.OriginID, R.DestinationID, R.OriginStateID, R.DestStateID, R.RegionID, R.ProducerID, R.OperatorID, R.TypeID, R.Rate, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate, R.NextEffectiveDate
		, Type = RT.Name
		, Shipper = C.Name
		, ProductGroup = PG.Name
		, Origin = O.Name
		, OriginFull = O.FullName
		, Destination = D.Name
		, DestinationFull = D.FullName
		, OriginState = OS.FullName
		, OriginStateAbbrev = OS.Abbreviation
		, DestState = DS.FullName
		, DestStateAbbrev = DS.Abbreviation
		, Region = RE.Name
		, RateType = RT.Name
		, Uom = U.Name
		, UomShort = U.Abbrev
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch, Ranking
	FROM dbo.fnShipperAssessorialRates(@StartDate, @EndDate, @TypeID, @ShipperID, @ProductGroupID, @OriginID, @DestinationID, @OriginStateID, @DestStateID, @RegionID, @ProducerID, @OperatorID, 0) R
	JOIN tblAssessorialRateType RT ON RT.ID = R.TypeID
	LEFT JOIN tblCustomer C ON C.ID = R.ShipperID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN viewOrigin O ON O.ID = R.OriginID
	LEFT JOIN viewDestination D ON D.ID = R.DestinationID
	LEFT JOIN tblState OS ON OS.ID = R.OriginStateID
	LEFT JOIN tblState DS ON DS.ID = R.DestStateID
	LEFT JOIN tblRegion RE ON RE.ID = R.RegionID
	LEFT JOIN tblUom U ON U.ID = R.UomID
	ORDER BY EffectiveDate
GO
GRANT SELECT ON fnShipperAssessorialRatesDisplay TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper DestinationWaitRate info for the specified criteria
/***********************************/
ALTER FUNCTION fnShipperDestinationWaitRate
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @ProductGroupID int
, @DestinationID int
, @StateID int
, @RegionID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID, ReasonID
			, Ranking =	dbo.fnRateRanking(@ReasonID, R.ReasonID, 32, 1)
				  + dbo.fnRateRanking(@ShipperID, R.ShipperID, 16, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 8, 0)
				  + dbo.fnRateRanking(@DestinationID, R.DestinationID, 4, 0)
				  + dbo.fnRateRanking(@StateID, R.StateID, 2, 0)
				  + dbo.fnRateRanking(@RegionID, R.RegionID, 1, 0)
		FROM  dbo.viewShipperDestinationWaitRate R
		WHERE coalesce(nullif(@ReasonID, 0), R.ReasonID, 0) = coalesce(ReasonID, nullif(@ReasonID, 0), 0)
		  AND coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@DestinationID, 0), R.DestinationID, 0) = coalesce(DestinationID, nullif(@DestinationID, 0), 0)
		  AND coalesce(nullif(@StateID, 0), R.StateID, 0) = coalesce(StateID, nullif(@StateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	),
	cte2 AS
	(
		SELECT ID, S.ReasonID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT ReasonID = ISNULL(ReasonID, 0), Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 6  -- ensure some type of match occurred on all 6 criteria choices
			GROUP BY ReasonID
		) X ON X.ReasonID = isnull(S.ReasonID, 0) AND X.Ranking = S.Ranking
	), 
	cte3 AS
	(	-- when both ReasonID = NULL AND ReasonID = @ReasonID records are BestMatch = 1, favor the ReasonID = @ReasonID record
		SELECT ID, X.ReasonID, X.Ranking, BestMatch = CASE WHEN BEST.ReasonID IS NULL THEN X.BestMatch ELSE 0 END
		FROM cte2 X
		LEFT JOIN (
			SELECT TOP 1 ReasonID FROM cte2 WHERE ReasonID = @ReasonID AND BestMatch = 1 ORDER BY Ranking DESC
		) BEST ON X.ReasonID IS NULL AND X.BestMatch = 1
	)
	
	SELECT TOP (CASE WHEN @BestMatchOnly = 1 THEN 1 ELSE 10000 END) R.ID, R.ReasonID, ShipperID, ProductGroupID, DestinationID, StateID, RegionID, Rate, EffectiveDate, EndDate, NextEffectiveDate
	  , BestMatch, Ranking
	  , Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewShipperDestinationWaitRate R
	JOIN cte3 X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
	ORDER BY Ranking DESC
)
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper DestinationWaitRate info for the specified order
/***********************************/
ALTER FUNCTION fnOrderShipperDestinationWaitRate(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, Minutes = O.DestMinutes, R.Rate
	FROM viewOrder O
	CROSS APPLY dbo.fnShipperDestinationWaitRate(O.OrderDate, null, O.DestWaitReasonID, O.CustomerID, O.ProductGroupID, O.OriginID, O.DestStateID, O.DestRegionID, 1) R
	WHERE O.ID = @ID

GO

DROP FUNCTION fnShipperDestinationWaitRates_ForRange
GO

/***********************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper DestinationWaitRate rows for the specified criteria
/***********************************/
CREATE FUNCTION fnShipperDestinationWaitRatesDisplay
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @ProductGroupID int
, @DestinationID int
, @StateID int
, @RegionID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ReasonID, R.ShipperID, R.ProductGroupID, R.DestinationID, R.StateID, R.RegionID, R.Rate, R.EffectiveDate, R.EndDate, R.NextEffectiveDate
		, Shipper = C.Name
		, ProductGroup = PG.Name
		, Destination = D.Name
		, DestinationFull = D.FullName
		, State = S.FullName
		, StateAbbrev = S.Abbreviation
		, Region = REG.Name
		, Reason = REA.Description
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
	FROM dbo.fnShipperDestinationWaitRate(@StartDate, @EndDate, @ReasonID, @ShipperID, @ProductGroupID, @DestinationID, @StateID, @RegionID, 0) R
	LEFT JOIN tblCustomer C ON C.ID = R.ShipperID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN viewDestination D ON D.ID = R.DestinationID
	LEFT JOIN tblState S ON S.ID = R.StateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblDestinationWaitReason REA ON REA.ID = R.ReasonID
	ORDER BY EffectiveDate
GO
GRANT SELECT ON fnShipperDestinationWaitRatesDisplay TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper OriginWaitRate info for the specified criteria
/***********************************/
ALTER FUNCTION fnShipperOriginWaitRate
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @ProductGroupID int
, @OriginID int
, @StateID int
, @RegionID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID, ReasonID
			, Ranking =	dbo.fnRateRanking(@ReasonID, R.ReasonID, 32, 1)
				  + dbo.fnRateRanking(@ShipperID, R.ShipperID, 16, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 8, 0)
				  + dbo.fnRateRanking(@OriginID, R.OriginID, 4, 0)
				  + dbo.fnRateRanking(@StateID, R.StateID, 2, 0)
				  + dbo.fnRateRanking(@RegionID, R.RegionID, 1, 0)
		FROM  dbo.viewShipperOriginWaitRate R
		WHERE coalesce(nullif(@ReasonID, 0), R.ReasonID, 0) = coalesce(ReasonID, nullif(@ReasonID, 0), 0)
		  AND coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@OriginID, 0), R.OriginID, 0) = coalesce(OriginID, nullif(@OriginID, 0), 0)
		  AND coalesce(nullif(@StateID, 0), R.StateID, 0) = coalesce(StateID, nullif(@StateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	),
	cte2 AS
	(
		SELECT ID, S.ReasonID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT ReasonID = ISNULL(ReasonID, 0), Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 6  -- ensure some type of match occurred on all 6 criteria choices
			GROUP BY ReasonID
		) X ON X.ReasonID = isnull(S.ReasonID, 0) AND X.Ranking = S.Ranking
	), 
	cte3 AS
	(	-- when both ReasonID = NULL AND ReasonID = @ReasonID records are BestMatch = 1, favor the ReasonID = @ReasonID record
		SELECT ID, X.ReasonID, X.Ranking, BestMatch = CASE WHEN BEST.ReasonID IS NULL THEN X.BestMatch ELSE 0 END
		FROM cte2 X
		LEFT JOIN (
			SELECT TOP 1 ReasonID FROM cte2 WHERE ReasonID = @ReasonID AND BestMatch = 1 ORDER BY Ranking DESC
		) BEST ON X.ReasonID IS NULL AND X.BestMatch = 1
	)
	
	SELECT TOP (CASE WHEN @BestMatchOnly = 1 THEN 1 ELSE 10000 END) R.ID, R.ReasonID, ShipperID, ProductGroupID, OriginID, StateID, RegionID, Rate, EffectiveDate, EndDate, NextEffectiveDate
	  , BestMatch, Ranking
	  , Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewShipperOriginWaitRate R
	JOIN cte3 X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
	ORDER BY Ranking DESC
)
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper OriginWaitRate info for the specified order
/***********************************/
ALTER FUNCTION fnOrderShipperOriginWaitRate(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, Minutes = O.DestMinutes, R.Rate
	FROM viewOrder O
	CROSS APPLY dbo.fnShipperOriginWaitRate(O.OrderDate, null, O.DestWaitReasonID, O.CustomerID, O.ProductGroupID, O.OriginID, O.DestStateID, O.DestRegionID, 1) R
	WHERE O.ID = @ID

GO

DROP FUNCTION fnShipperOriginWaitRates_ForRange
GO

/***********************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper OriginWaitRate rows for the specified criteria
/***********************************/
CREATE FUNCTION fnShipperOriginWaitRatesDisplay
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @ProductGroupID int
, @OriginID int
, @StateID int
, @RegionID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ReasonID, R.ShipperID, R.ProductGroupID, R.OriginID, R.StateID, R.RegionID, R.Rate, R.EffectiveDate, R.EndDate, R.NextEffectiveDate
		, Shipper = C.Name
		, ProductGroup = PG.Name
		, Origin = D.Name
		, OriginFull = D.FullName
		, State = S.FullName
		, StateAbbrev = S.Abbreviation
		, Region = REG.Name
		, Reason = REA.Description
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
	FROM dbo.fnShipperOriginWaitRate(@StartDate, @EndDate, @ReasonID, @ShipperID, @ProductGroupID, @OriginID, @StateID, @RegionID, 0) R
	LEFT JOIN tblCustomer C ON C.ID = R.ShipperID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN viewOrigin D ON D.ID = R.OriginID
	LEFT JOIN tblState S ON S.ID = R.StateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblOriginWaitReason REA ON REA.ID = R.ReasonID
	ORDER BY EffectiveDate
GO
GRANT SELECT ON fnShipperOriginWaitRatesDisplay TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper OrderRejectRate info for the specified criteria
/***********************************/
ALTER FUNCTION fnShipperOrderRejectRate
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @ProductGroupID int
, @OriginID int
, @StateID int
, @RegionID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID, ReasonID
			, Ranking =	dbo.fnRateRanking(@ReasonID, R.ReasonID, 32, 1)
				  + dbo.fnRateRanking(@ShipperID, R.ShipperID, 16, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 8, 0)
				  + dbo.fnRateRanking(@OriginID, R.OriginID, 4, 0)
				  + dbo.fnRateRanking(@StateID, R.StateID, 2, 0)
				  + dbo.fnRateRanking(@RegionID, R.RegionID, 1, 0)
		FROM  dbo.viewShipperOrderRejectRate R
		WHERE coalesce(nullif(@ReasonID, 0), R.ReasonID, 0) = coalesce(ReasonID, nullif(@ReasonID, 0), 0)
		  AND coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@OriginID, 0), R.OriginID, 0) = coalesce(OriginID, nullif(@OriginID, 0), 0)
		  AND coalesce(nullif(@StateID, 0), R.StateID, 0) = coalesce(StateID, nullif(@StateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	),
	cte2 AS
	(
		SELECT ID, S.ReasonID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT ReasonID = ISNULL(ReasonID, 0), Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 6  -- ensure some type of match occurred on all 6 criteria choices
			GROUP BY ReasonID
		) X ON X.ReasonID = isnull(S.ReasonID, 0) AND X.Ranking = S.Ranking
	), 
	cte3 AS
	(	-- when both ReasonID = NULL AND ReasonID = @ReasonID records are BestMatch = 1, favor the ReasonID = @ReasonID record
		SELECT ID, X.ReasonID, X.Ranking, BestMatch = CASE WHEN BEST.ReasonID IS NULL THEN X.BestMatch ELSE 0 END
		FROM cte2 X
		LEFT JOIN (
			SELECT TOP 1 ReasonID FROM cte2 WHERE ReasonID = @ReasonID AND BestMatch = 1 ORDER BY Ranking DESC
		) BEST ON X.ReasonID IS NULL AND X.BestMatch = 1
	)
	
	SELECT R.ID, R.ReasonID, ShipperID, ProductGroupID, OriginID, StateID, RegionID, Rate, RateTypeID, UomID, EffectiveDate, EndDate, NextEffectiveDate
	  , BestMatch, Ranking
	  , Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewShipperOrderRejectRate R
	JOIN cte3 X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper OrderRejectRate info for the specified order
/***********************************/
ALTER FUNCTION fnOrderShipperOrderRejectRate(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, Minutes = O.DestMinutes, R.Rate, R.RateTypeID, R.UomID
	FROM viewOrder O
	CROSS APPLY dbo.fnShipperOrderRejectRate(O.OrderDate, null, O.DestWaitReasonID, O.CustomerID, O.ProductGroupID, O.OriginID, O.DestStateID, O.DestRegionID, 1) R
	WHERE O.ID = @ID

GO

DROP FUNCTION fnShipperOrderRejectRates_ForRange
GO

/***********************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper OrderRejectRate rows for the specified criteria
/***********************************/
CREATE FUNCTION fnShipperOrderRejectRatesDisplay
(
  @StartDate date
, @EndDate date
, @ReasonID int
, @ShipperID int
, @ProductGroupID int
, @OriginID int
, @StateID int
, @RegionID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ReasonID, R.ShipperID, R.ProductGroupID, R.OriginID, R.StateID, R.RegionID, R.Rate, R.EffectiveDate, R.EndDate, R.NextEffectiveDate
		, Shipper = C.Name
		, ProductGroup = PG.Name
		, Origin = D.Name
		, OriginFull = D.FullName
		, State = S.FullName
		, StateAbbrev = S.Abbreviation
		, Region = REG.Name
		, Reason = REA.Description
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
	FROM dbo.fnShipperOrderRejectRate(@StartDate, @EndDate, @ReasonID, @ShipperID, @ProductGroupID, @OriginID, @StateID, @RegionID, 0) R
	LEFT JOIN tblCustomer C ON C.ID = R.ShipperID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN viewOrigin D ON D.ID = R.OriginID
	LEFT JOIN tblState S ON S.ID = R.StateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblOrderRejectReason REA ON REA.ID = R.ReasonID
	ORDER BY EffectiveDate
GO
GRANT SELECT ON fnShipperOrderRejectRatesDisplay TO dispatchcrude_iis_acct
GO

ALTER TABLE dbo.tblShipperRangeRate
	DROP CONSTRAINT FK_ShipperRangeRate
GO
ALTER TABLE dbo.tblShipperRateSheet SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblShipperRangeRate ADD CONSTRAINT
	FK_ShipperRangeRate_ShipperRateSheet FOREIGN KEY
	(
	RateSheetID
	) REFERENCES dbo.tblShipperRateSheet
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	
GO
ALTER TABLE dbo.tblShipperRangeRate SET (LOCK_ESCALATION = TABLE)
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper RateSheetRangeRate info for the specified criteria
/***********************************/
ALTER FUNCTION fnShipperRateSheetRangeRate(@StartDate date, @EndDate date, @RouteMiles int, @ShipperID int, @ProductGroupID int, @OriginStateID int, @DestStateID int, @RegionID int, @BestMatchOnly bit = 0)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT RateSheetID = R.ID, RangeRateID = RR.ID
			, Ranking = CASE WHEN @RouteMiles BETWEEN RR.MinRange AND RR.MaxRange THEN 32.01 ELSE 0 END
				+ dbo.fnRateRanking(@ShipperID, R.ShipperID, 16, 0)
				+ dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 8, 0)
				+ dbo.fnRateRanking(@OriginStateID, R.OriginStateID, 4, 0)
				+ dbo.fnRateRanking(@DestStateID, R.DestStateID, 2, 0)
				+ dbo.fnRateRanking(@RegionID, R.RegionID, 1, 0)
		FROM dbo.viewShipperRateSheet R
		JOIN dbo.tblShipperRangeRate RR ON RR.RateSheetID = R.ID AND isnull(@RouteMiles, RR.MinRange) BETWEEN RR.MinRange AND RR.MaxRange
		WHERE coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@OriginStateID, 0), R.OriginStateID, 0) = coalesce(OriginStateID, nullif(@OriginStateID, 0), 0)
		  AND coalesce(nullif(@DestStateID, 0), R.DestStateID, 0) = coalesce(DestStateID, nullif(@DestStateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	)
	
	SELECT RR.ID, RateSheetID = R.ID, ShipperID, ProductGroupID, OriginStateID, DestStateID, RegionID, Rate, MinRange, MaxRange, RateTypeID, UomID, EffectiveDate, EndDate, NextEffectiveDate
	  , BestMatch, Ranking
	  , Locked, R.CreateDateUTC, R.CreatedByUser, R.LastChangeDateUTC, R.LastChangedByUser 
	FROM dbo.viewShipperRateSheet R
	JOIN dbo.tblShipperRangeRate RR ON RR.RateSheetID = R.ID
	JOIN (
		SELECT RateSheetID, RangeRateID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 6  -- ensure some type of match occurred on all 6 criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.RateSheetID = R.ID AND X.RangeRateID = RR.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper RateSheetRangeRate info for the specified order
/***********************************/
ALTER FUNCTION fnOrderShipperRateSheetRangeRate(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, RateTypeID, Rate, UomID
	FROM dbo.viewOrder O
	CROSS APPLY dbo.fnShipperRateSheetRangeRate(O.OrderDate, null, O.ActualMiles, O.CustomerID, O.ProductGroupID, O.OriginStateID, O.DestStateID, O.OriginRegionID, 1) R
	WHERE O.ID = @ID
GO

DROP FUNCTION fnShipperRateSheetRangeRates_ForRange
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper RateSheetRangeRate info for the specified criteria
/***********************************/
CREATE FUNCTION fnShipperRateSheetRangeRatesDisplay(@StartDate date, @EndDate date, @RouteMiles int, @ShipperID int, @ProductGroupID int, @OriginStateID int, @DestStateID int, @RegionID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ShipperID, R.ProductGroupID, R.OriginStateID, R.DestStateID, R.RegionID, R.Rate, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate, R.NextEffectiveDate
		, Shipper = S.Name
		, ProductGroup = PG.Name
		, OriginState = OS.FullName
		, OriginStateAbbrev = OS.Abbreviation
		, DestState = OS.FullName
		, DestStateAbbrev = OS.Abbreviation
		, Region = REG.Name
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
		, Ranking
	FROM dbo.fnShipperRateSheetRangeRate(@StartDate, @EndDate, @RouteMiles, @ShipperID, @ProductGroupID, @OriginStateID, @DestStateID, @RegionID, 0) R
	LEFT JOIN tblCustomer S ON S.ID = R.ShipperID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblState OS ON OS.ID = R.OriginStateID
	LEFT JOIN tblState DS ON DS.ID = R.DestStateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	ORDER BY EffectiveDate
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper RouteRate info for the specified criteria
/***********************************/
ALTER FUNCTION fnShipperRouteRate(@StartDate date, @EndDate date, @RouteID int, @ShipperID int, @ProductGroupID int, @BestMatchOnly bit = 0) 
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID
			, Ranking =	dbo.fnRateRanking(@ShipperID, R.ShipperID, 2, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 1, 0)
		FROM  dbo.viewShipperRouteRate R
		WHERE RouteID = @RouteID
		  AND coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	)
	
	SELECT R.ID, RouteID, OriginID, DestinationID, ShipperID, ProductGroupID, Rate, RateTypeID, UomID, EffectiveDate, EndDate, NextEffectiveDate
	  , BestMatch, Ranking
	  , Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewShipperRouteRate R
	JOIN (
		SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 2  -- ensure some type of match occurred on all 6 criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper RouteRate info for the specified order
/***********************************/
ALTER FUNCTION fnOrderShipperRouteRate(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, RateTypeID, Rate, R.UomID
	FROM dbo.viewOrder O
	CROSS APPLY dbo.fnShipperRouteRate(O.OrderDate, null, O.RouteID, O.CustomerID, O.ProductGroupID, 1) R
	WHERE O.ID = @ID 
GO

DROP FUNCTION fnShipperRouteRates_ForRange
GO

/***********************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper RouteRate rows for the specified criteria
/***********************************/
CREATE FUNCTION fnShipperRouteRatesDisplay(@StartDate date, @EndDate date, @ShipperID int, @ProductGroupID int, @OriginID int, @DestinationID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.RouteID, RO.ActualMiles, R.OriginID, R.DestinationID, R.ShipperID, R.ProductGroupID, R.Rate, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate, R.NextEffectiveDate
		, Origin = O.Name
		, OriginFull = O.FullName
		, Destination = D.Name
		, DestinationFull = D.FullName
		, Shipper = C.Name
		, ProductGroup = PG.Name
		, RateType = RT.Name
		, Uom = U.Name
		, UomShort = U.Abbrev
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch, Ranking
	FROM dbo.fnShipperRouteRate(@StartDate, @EndDate, @ShipperID, @ProductGroupID, @OriginID, @DestinationID) R
	JOIN tblRoute RO ON RO.ID = R.RouteID
	JOIN viewOrigin O ON O.ID = R.OriginID
	JOIN viewDestination D ON D.ID = R.DestinationID
	LEFT JOIN tblCustomer C ON C.ID = R.ShipperID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblRateType RT ON RT.ID = R.RateTypeID
	LEFT JOIN tblUom U ON U.ID = R.UomID
	ORDER BY EffectiveDate
GO
GRANT SELECT ON fnShipperRouteRatesDisplay TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper WaitFeeParameter info for the specified order
/***********************************/
ALTER FUNCTION fnShipperWaitFeeParameter(@StartDate date, @EndDate date, @ShipperID int, @ProductGroupID int, @OriginStateID int, @DestStateID int, @RegionID int, @bestMatchOnly bit = 0)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID
			, Ranking = dbo.fnRateRanking(@ShipperID, R.ShipperID, 16, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 8, 0)
				  + dbo.fnRateRanking(@OriginStateID, R.OriginStateID, 4, 0)
				  + dbo.fnRateRanking(@DestStateID, R.DestStateID, 2, 0)
				  + dbo.fnRateRanking(@RegionID, R.RegionID, 1, 0)
		FROM  dbo.viewShipperWaitFeeParameter R
		WHERE coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@OriginStateID, 0), R.OriginStateID, 0) = coalesce(OriginStateID, nullif(@OriginStateID, 0), 0)
		  AND coalesce(nullif(@DestStateID, 0), R.DestStateID, 0) = coalesce(DestStateID, nullif(@DestStateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	)
	
	SELECT R.ID, ShipperID, ProductGroupID, OriginStateID, DestStateID, RegionID, SubUnitID, RoundingTypeID, ThresholdMinutes, EffectiveDate, EndDate, NextEffectiveDate
	  , BestMatch, Ranking
	  , Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewShipperWaitFeeParameter R
	JOIN (
		SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 5  -- ensure some type of match occurred on all 6 criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper WaitFeeParameter info for the specified order
/***********************************/
ALTER FUNCTION fnOrderShipperWaitFeeParameter(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, SubUnitID, RoundingTypeID, ThresholdMinutes
	FROM dbo.viewOrder O
	CROSS APPLY dbo.fnShipperWaitFeeParameter(O.OrderDate, null, O.CustomerID, O.ProductGroupID, O.OriginStateID, O.DestStateID, O.OriginRegionID, 1) R
	WHERE O.ID = @ID 
GO

DROP FUNCTION fnShipperWaitFeeParameters_ForRange
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper WaitFeeParameter rows for the specified criteria
/***********************************/
CREATE FUNCTION fnShipperWaitFeeParametersDisplay(@StartDate date, @EndDate date, @ShipperID int, @ProductGroupID int, @OriginStateID int, @DestStateID int, @RegionID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ShipperID, R.ProductGroupID, R.OriginStateID, R.DestStateID, R.RegionID, R.SubUnitID, R.RoundingTypeID, R.ThresholdMinutes, R.EffectiveDate, R.EndDate, R.NextEffectiveDate
		, Shipper = C.Name
		, ProductGroup = PG.Name
		, OriginState = OS.FullName
		, OriginStateAbbrev = OS.Abbreviation
		, DestinationState = DS.FullName
		, DestinationStateAbbrev = DS.Abbreviation
		, Region = REG.Name
		, RoundingType = WFRT.Name
		, SubUnit = WFSU.Name
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch, Ranking
	FROM dbo.fnShipperWaitFeeParameter(@StartDate, @EndDate, @ShipperID, @ProductGroupID, @OriginStateID, @DestStateID, @RegionID, 0) R 
	JOIN tblWaitFeeRoundingType WFRT ON WFRT.ID = R.RoundingTypeID
	JOIN tblWaitFeeSubUnit WFSU ON WFSU.ID = R.SubUnitID
	LEFT JOIN tblCustomer C ON C.ID = R.ShipperID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblState OS ON OS.ID = R.OriginStateID
	LEFT JOIN tblState DS ON DS.ID = R.DestStateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	ORDER BY EffectiveDate
GO
GRANT SELECT ON fnShipperWaitFeeParametersDisplay TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier DestinationWait data info for the specified order
/***********************************/
ALTER FUNCTION [dbo].[fnOrderCarrierDestinationWaitData](@ID int) RETURNS TABLE AS RETURN
	SELECT RateID, BillableMinutes, BillableHours, Amount = BillableHours * Rate
	FROM (
		SELECT RateID, BillableMinutes
			, BillableHours = dbo.fnComputeBillableWaitHours(BillableMinutes, SubUnitID, RoundingTypeID)
			, Rate
		FROM (
			SELECT RateID = OWR.ID
				, Rate
				, BillableMinutes = dbo.fnMaxInt(OWR.Minutes - WFP.ThresholdMinutes, 0)
				, WFP.SubUnitID, WFP.RoundingTypeID
			FROM dbo.fnOrderCarrierDestinationWaitRate(@ID) OWR 
			CROSS JOIN dbo.fnOrderCarrierWaitFeeParameter(@ID) WFP 
		) X
	) X2
	WHERE BillableHours > 0.0
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OriginWait data info for the specified order
/***********************************/
ALTER FUNCTION [dbo].[fnOrderCarrierOriginWaitData](@ID int) RETURNS TABLE AS RETURN
	SELECT RateID, BillableMinutes, BillableHours, Amount = BillableHours * Rate, WaitFeeParameterID
	FROM (
		SELECT RateID, BillableMinutes
			, BillableHours = dbo.fnComputeBillableWaitHours(BillableMinutes, SubUnitID, RoundingTypeID)
			, Rate
			, WaitFeeParameterID
		FROM (
			SELECT RateID = OWR.ID
				, Rate
				, BillableMinutes = dbo.fnMaxInt(OWR.Minutes - WFP.ThresholdMinutes, 0)
				, WaitFeeParameterID = WFP.ID, WFP.SubUnitID, WFP.RoundingTypeID
			FROM dbo.fnOrderCarrierOriginWaitRate(@ID) OWR 
			CROSS APPLY dbo.fnOrderCarrierWaitFeeParameter(@ID) WFP
		) X
	) X2
	WHERE BillableHours > 0.0
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OrderReject data info for the specified order
/***********************************/
ALTER FUNCTION [dbo].[fnOrderCarrierOrderRejectData](@ID int, @Units decimal(18, 10), @UomID int) RETURNS TABLE AS RETURN
	SELECT RateID = OWR.ID
		, Amount = dbo.fnRateToAmount(RateTypeID, @Units, @UomID, Rate, UomID
			, CASE WHEN RateTypeID = 3 THEN (SELECT TOP 1 OrderRejectAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID) ELSE NULL END)
	FROM dbo.fnOrderCarrierOrderRejectRate(@ID) OWR 
	JOIN tblOrder O ON O.ID = @ID
	WHERE O.Rejected = 1
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper DestinationWait data info for the specified order
/***********************************/
ALTER FUNCTION [dbo].[fnOrderShipperDestinationWaitData](@ID int) RETURNS TABLE AS RETURN
	SELECT RateID, BillableMinutes, BillableHours, Amount = BillableHours * Rate
	FROM (
		SELECT RateID, BillableMinutes
			, BillableHours = dbo.fnComputeBillableWaitHours(BillableMinutes, SubUnitID, RoundingTypeID)
			, Rate
		FROM (
			SELECT RateID = OWR.ID
				, Rate
				, BillableMinutes = dbo.fnMaxInt(OWR.Minutes - WFP.ThresholdMinutes, 0)
				, WFP.SubUnitID, WFP.RoundingTypeID
			FROM dbo.fnOrderShipperDestinationWaitRate(@ID) OWR 
			CROSS JOIN dbo.fnOrderShipperWaitFeeParameter(@ID) WFP 
		) X
	) X2
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper OriginWait data info for the specified order
/***********************************/
ALTER FUNCTION [dbo].[fnOrderShipperOriginWaitData](@ID int) RETURNS TABLE AS RETURN
	SELECT RateID, BillableMinutes, BillableHours, Amount = BillableHours * Rate, WaitFeeParameterID
	FROM (
		SELECT RateID, BillableMinutes
			, BillableHours = dbo.fnComputeBillableWaitHours(BillableMinutes, SubUnitID, RoundingTypeID)
			, Rate
			, WaitFeeParameterID
		FROM (
			SELECT RateID = OWR.ID
				, Rate
				, BillableMinutes = dbo.fnMaxInt(OWR.Minutes - WFP.ThresholdMinutes, 0)
				, WaitFeeParameterID = WFP.ID, WFP.SubUnitID, WFP.RoundingTypeID
			FROM dbo.fnOrderShipperOriginWaitRate(@ID) OWR 
			CROSS APPLY dbo.fnOrderShipperWaitFeeParameter(@ID) WFP
		) X
	) X2
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper OrderReject data info for the specified order
/***********************************/
ALTER FUNCTION [dbo].[fnOrderShipperOrderRejectData](@ID int, @Units decimal(18, 10), @UomID int) RETURNS TABLE AS RETURN
	SELECT RateID = OWR.ID
		, Amount = dbo.fnRateToAmount(RateTypeID, @Units, @UomID, Rate, UomID, NULL)
	FROM dbo.fnOrderShipperOrderRejectRate(@ID) OWR 
	JOIN tblOrder O ON O.ID = @ID
	WHERE O.Rejected = 1
GO

-- this part was added
/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details into a single view
/***********************************/
ALTER VIEW [dbo].[viewOrderExportFull_Reroute] AS 
	SELECT O.* 
		, dbo.fnRerouteDetails(O.ID, 'PreviousDestinations', '<br/>') AS PreviousDestinations
		, dbo.fnRerouteDetails(O.ID, 'RerouteUsers', '<br/>') AS RerouteUsers
		, dbo.fnRerouteDetails(O.ID, 'RerouteDates', '<br/>') AS RerouteDates
		, dbo.fnRerouteDetails(O.ID, 'RerouteNotes', '<br/>') AS RerouteNotes
	FROM dbo.viewOrderLocalDates O
	WHERE O.StatusID NOT IN (-10) -- don't include "Generated" orders (only Assigned+)
GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details into a single view
/***********************************/
ALTER VIEW [dbo].[viewOrder_OrderTicket_Full] AS 
	SELECT OE.* 
		, T_TicketType = CASE WHEN OE.TicketCount = 0 THEN OE.TicketType ELSE OT.TicketType END 
		, T_CarrierTicketNum = CASE WHEN OE.TicketCount = 0 THEN OE.CarrierTicketNum ELSE OT.CarrierTicketNum END 
		, T_BOLNum = CASE WHEN OE.TicketCount = 0 THEN OE.OriginBOLNum ELSE OT.BOLNum END 
		, T_TankNum = OT.OriginTankText 
		, T_IsStrappedTank = OT.IsStrappedTank 
		, T_BottomFeet = OT.BottomFeet
		, T_BottomInches = OT.BottomInches
		, T_BottomQ = OT.BottomQ 
		, T_OpeningGaugeFeet = OT.OpeningGaugeFeet 
		, T_OpeningGaugeInch = OT.OpeningGaugeInch 
		, T_OpeningGaugeQ = OT.OpeningGaugeQ 
		, T_ClosingGaugeFeet = OT.ClosingGaugeFeet 
		, T_ClosingGaugeInch = OT.ClosingGaugeInch 
		, T_ClosingGaugeQ = OT.ClosingGaugeQ 
		, T_OpenTotalQ = dbo.fnGaugeQtrInches(OT.OpeningGaugeFeet, OT.OpeningGaugeInch, OT.OpeningGaugeQ) 
		, T_CloseTotalQ = dbo.fnGaugeQtrInches(OT.ClosingGaugeFeet, OT.ClosingGaugeInch, ot.ClosingGaugeQ) 
		, T_OpenReading = ltrim(OT.OpeningGaugeFeet) + 'ft ' + ltrim(OT.OpeningGaugeInch) + 'in ' + ltrim(OT.OpeningGaugeQ) + 'q' 
		, T_CloseReading = ltrim(OT.ClosingGaugeFeet) + 'ft ' + ltrim(OT.ClosingGaugeInch) + 'in ' + ltrim(OT.ClosingGaugeQ) + 'q' 
		, T_CorrectedAPIGravity = round(cast(OT.Gravity60F as decimal(9,4)), 9, 4) 
		, T_GrossStdUnits = OT.GrossStdUnits
		, T_SealOff = ltrim(OT.SealOff) 
		, T_SealOn = ltrim(OT.SealOn) 
		, T_HighTemp = OT.ProductHighTemp
		, T_LowTemp = OT.ProductLowTemp
		, T_ProductObsTemp = OT.ProductObsTemp 
		, T_ProductObsGravity = OT.ProductObsGravity 
		, T_ProductBSW = OT.ProductBSW 
		, T_Rejected = OT.Rejected 
		, T_RejectReasonID = OT.RejectReasonID
		, T_RejectNum = OT.RejectNum
		, T_RejectDesc = OT.RejectDesc
		, T_RejectNumDesc = OT.RejectNumDesc
		, T_RejectNotes = OT.RejectNotes 
		, T_GrossUnits = OT.GrossUnits 
		, T_NetUnits = OT.NetUnits 
		, T_MeterFactor = OT.MeterFactor
		, T_OpenMeterUnits = OT.OpenMeterUnits
		, T_CloseMeterUnits = OT.CloseMeterUnits
		, PreviousDestinations = dbo.fnRerouteDetails(OE.ID, 'PreviousDestinations', '<br/>') 
		, RerouteUsers = dbo.fnRerouteDetails(OE.ID, 'RerouteUsers', '<br/>') 
		, RerouteDates = dbo.fnRerouteDetails(OE.ID, 'RerouteDates', '<br/>') 
		, RerouteNotes = dbo.fnRerouteDetails(OE.ID, 'RerouteNotes', '<br/>') 
	FROM dbo.viewOrderLocalDates OE
	LEFT JOIN viewOrderTicket OT ON OT.OrderID = OE.ID AND OT.DeleteDateUTC IS NULL
	WHERE OE.DeleteDateUTC IS NULL
GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details + FINANCIAL INFO into a single view
/***********************************/
ALTER VIEW [dbo].[viewOrder_Financial_Shipper] AS 
	SELECT O.* 
		, Shipper = Customer
		, TicketNums = dbo.fnOrderTicketDetails(O.ID, 'TicketNums', '<br/>') 
		, TankNums = dbo.fnOrderTicketDetails(O.ID, 'TankNums', '<br/>') 
		, PreviousDestinations = dbo.fnRerouteDetails(O.ID, 'PreviousDestinations', '<br/>') 
		, RerouteUsers = dbo.fnRerouteDetails(O.ID, 'RerouteUsers', '<br/>') 
		, RerouteDates = dbo.fnRerouteDetails(O.ID, 'RerouteDates', '<br/>') 
		, RerouteNotes = dbo.fnRerouteDetails(O.ID, 'RerouteNotes', '<br/>') 
		, InvoiceRatesAppliedDate = dbo.fnUTC_To_Local(OIC.CreateDateUTC, O.OriginTimeZoneID, O.OriginUseDST)
		, OIC.BatchID
		, InvoiceBatchNum = OIC.BatchNum 
		, InvoiceOriginWaitBillableMinutes = OIC.OriginWaitBillableMinutes
		, InvoiceDestinationWaitBillableMinutes = OIC.DestinationWaitBillableMinutes
		, InvoiceTotalWaitBillableMinutes = isnull(OIC.OriginWaitBillableMinutes, 0) + ISNULL(OIC.DestinationWaitBillableMinutes, 0) 
		, InvoiceWaitFeeSubUnit = WaitFeeSubUnit
		, InvoiceWaitFeeRoundingType = WaitFeeRoundingType
		, InvoiceOriginWaitRate = OIC.OriginWaitRate 
		, InvoiceOriginWaitAmount = OIC.OriginWaitAmount 		
		, InvoiceDestinationWaitRate = OIC.DestinationWaitRate 
		, InvoiceDestinationWaitAmount = OIC.DestinationWaitAmount 
		, InvoiceTotalWaitAmount = OIC.TotalWaitAmount
		, InvoiceOrderRejectRate = OIC.OrderRejectRate  -- changed
		, InvoiceOrderRejectRateType = OIC.OrderRejectRateType -- changed
		, InvoiceOrderRejectAmount = OIC.OrderRejectAmount  -- changed
		, InvoiceChainupRate = OIC.ChainupRate
		, InvoiceChainupRateType = OIC.ChainupRateType
		, InvoiceChainupAmount = OIC.ChainupAmount 
		, InvoiceRerouteRate = OIC.RerouteRate
		, InvoiceRerouteRateType = OIC.RerouteRateType
		, InvoiceRerouteAmount = OIC.RerouteAmount 
		, InvoiceH2SRate = OIC.H2SRate
		, InvoiceH2SRateType = OIC.H2SRateType
		, InvoiceH2SAmount = OIC.H2SAmount
		, InvoiceSplitLoadRate = OIC.SplitLoadRate
		, InvoiceSplitLoadRateType = OIC.SplitLoadRateType
		, InvoiceSplitLoadAmount = OIC.SplitLoadAmount
		, InvoiceTaxRate = OIC.OriginTaxRate
		, InvoiceSettlementUom = OIC.SettlementUom -- changed
		, InvoiceSettlementUomShort = OIC.SettlementUomShort -- changed
		, InvoiceMinSettlementUnits = OIC.MinSettlementUnits
		, InvoiceUnits = OIC.SettlementUnits
		, InvoiceRouteRate = OIC.RouteRate
		, InvoiceRouteRateType = OIC.RouteRateType
		, InvoiceRateSheetRate = OIC.RateSheetRate
		, InvoiceRateSheetRateType = OIC.RateSheetRateType
		, InvoiceFuelSurchargeRate = OIC.FuelSurchargeRate
		, InvoiceFuelSurchargeAmount = OIC.FuelSurchargeAmount
		, InvoiceLoadAmount = OIC.LoadAmount
		, InvoiceTotalAmount = OIC.TotalAmount
	FROM dbo.viewOrderLocalDates O
	LEFT JOIN viewOrderSettlementShipper OIC ON OIC.OrderID = O.ID
	WHERE O.StatusID NOT IN (-10) -- don't include "Generated" orders (only Assigned+)
GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details + FINANCIAL INFO into a single view
/***********************************/
ALTER VIEW [dbo].[viewOrder_Financial_Carrier] AS 
	SELECT O.* 
		, TicketNums = dbo.fnOrderTicketDetails(O.ID, 'TicketNums', '<br/>') 
		, TankNums = dbo.fnOrderTicketDetails(O.ID, 'TankNums', '<br/>') 
		, PreviousDestinations = dbo.fnRerouteDetails(O.ID, 'PreviousDestinations', '<br/>') 
		, RerouteUsers = dbo.fnRerouteDetails(O.ID, 'RerouteUsers', '<br/>') 
		, RerouteDates = dbo.fnRerouteDetails(O.ID, 'RerouteDates', '<br/>') 
		, RerouteNotes = dbo.fnRerouteDetails(O.ID, 'RerouteNotes', '<br/>') 
		, InvoiceRatesAppliedDate = dbo.fnUTC_To_Local(OIC.CreateDateUTC, O.OriginTimeZoneID, O.OriginUseDST)
		, OIC.BatchID
		, InvoiceBatchNum = OIC.BatchNum 
		, InvoiceOriginWaitBillableMinutes = OIC.OriginWaitBillableMinutes
		, InvoiceDestinationWaitBillableMinutes = OIC.DestinationWaitBillableMinutes
		, InvoiceTotalWaitBillableMinutes = isnull(OIC.OriginWaitBillableMinutes, 0) + ISNULL(OIC.DestinationWaitBillableMinutes, 0) 
		, InvoiceWaitFeeSubUnit = WaitFeeSubUnit
		, InvoiceWaitFeeRoundingType = WaitFeeRoundingType
		, InvoiceOriginWaitRate = OIC.OriginWaitRate 
		, InvoiceOriginWaitAmount = OIC.OriginWaitAmount 		
		, InvoiceDestinationWaitRate = OIC.DestinationWaitRate 
		, InvoiceDestinationWaitAmount = OIC.DestinationWaitAmount 
		, InvoiceTotalWaitAmount = OIC.TotalWaitAmount
		, InvoiceOrderRejectRate = OIC.OrderRejectRate	-- changed
		, InvoiceOrderRejectRateType = OIC.OrderRejectRateType  -- changed
		, InvoiceOrderRejectAmount = OIC.OrderRejectAmount  -- changed
		, InvoiceChainupRate = OIC.ChainupRate
		, InvoiceChainupRateType = OIC.ChainupRateType
		, InvoiceChainupAmount = OIC.ChainupAmount 
		, InvoiceRerouteRate = OIC.RerouteRate
		, InvoiceRerouteRateType = OIC.RerouteRateType
		, InvoiceRerouteAmount = OIC.RerouteAmount 
		, InvoiceH2SRate = OIC.H2SRate
		, InvoiceH2SRateType = OIC.H2SRateType
		, InvoiceH2SAmount = OIC.H2SAmount
		, InvoiceSplitLoadRate = OIC.SplitLoadRate
		, InvoiceSplitLoadRateType = OIC.SplitLoadRateType
		, InvoiceSplitLoadAmount = OIC.SplitLoadAmount
		, InvoiceTaxRate = OIC.OriginTaxRate
		, InvoiceSettlementUom = OIC.SettlementUom -- changed
		, InvoiceSettlementUomShort = OIC.SettlementUomShort -- changed
		, InvoiceMinSettlementUnits = OIC.MinSettlementUnits
		, InvoiceUnits = OIC.SettlementUnits
		, InvoiceRouteRate = OIC.RouteRate
		, InvoiceRouteRateType = OIC.RouteRateType
		, InvoiceRateSheetRate = OIC.RateSheetRate
		, InvoiceRateSheetRateType = OIC.RateSheetRateType
		, InvoiceFuelSurchargeRate = OIC.FuelSurchargeRate
		, InvoiceFuelSurchargeAmount = OIC.FuelSurchargeAmount
		, InvoiceLoadAmount = OIC.LoadAmount
		, InvoiceTotalAmount = OIC.TotalAmount
	FROM dbo.viewOrderLocalDates O
	LEFT JOIN viewOrderSettlementCarrier OIC ON OIC.OrderID = O.ID
	WHERE O.StatusID NOT IN (-10) -- don't include "Generated" orders (only Assigned+)
GO

DROP VIEW viewOrderExportFull
GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Report Center Order data
/***********************************/
ALTER VIEW [dbo].[viewReportCenter_Orders] AS
	SELECT O.*
		, ShipperBatchNum = SS.BatchNum
		, ShipperBatchInvoiceNum = SSB.InvoiceNum
		, ShipperSettlementUomID = SS.SettlementUomID
		, ShipperSettlementUom = SS.SettlementUom
		, ShipperMinSettlementUnits = SS.MinSettlementUnits
		, ShipperSettlementUnits = SS.SettlementUnits
		, ShipperRateSheetRate = SS.RateSheetRate
		, ShipperRateSheetRateType = SS.RateSheetRateType
		, ShipperRouteRate = SS.RouteRate
		, ShipperRouteRateType = SS.RouteRateType
		, ShipperLoadRate = isnull(SS.RouteRate, SS.RateSheetRate)
		, ShipperLoadRateType = isnull(SS.RouteRateType, SS.RateSheetRateType)
		, ShipperLoadAmount = SS.LoadAmount
		, ShipperOrderRejectRate = SS.OrderRejectRate -- changed
		, ShipperOrderRejectRateType = SS.OrderRejectRateType -- new
		, ShipperOrderRejectAmount = SS.OrderRejectAmount -- changed
		, ShipperWaitFeeSubUnit = SS.WaitFeeSubUnit  -- new
		, ShipperWaitFeeRoundingType = SS.WaitFeeRoundingType  -- new
		, ShipperOriginWaitBillableHours = SS.OriginWaitBillableHours  -- new
		, ShipperOriginWaitBillableMinutes = SS.OriginWaitBillableMinutes  -- new
		, ShipperOriginWaitRate = SS.OriginWaitRate
		, ShipperOriginWaitAmount = SS.OriginWaitAmount
		, ShipperDestinationWaitBillableHours = SS.DestinationWaitBillableHours  -- new
		, ShipperDestinationWaitBillableMinutes = SS.DestinationWaitBillableMinutes   -- new
		, ShipperDestinationWaitRate = SS.DestinationWaitRate  -- changed
		, ShipperDestinationWaitAmount = SS.DestinationWaitAmount  -- changed
		, ShipperTotalWaitAmount = SS.TotalWaitAmount
		, ShipperTotalWaitBillableMinutes = nullif(ISNULL(SS.OriginWaitBillableMinutes, 0) + ISNULL(SS.DestinationWaitBillableMinutes, 0), 0)
		, ShipperTotalWaitBillableHours = nullif(ISNULL(SS.OriginWaitBillableHours, 0) + ISNULL(SS.DestinationWaitBillableHours, 0), 0)
		
		, ShipperFuelSurchargeRate = SS.FuelSurchargeRate
		, ShipperFuelSurchargeAmount = SS.FuelSurchargeAmount

		, ShipperChainupRate = SS.ChainupRate
		, ShipperChainupRateType = SS.ChainupRateType  -- new
		, ShipperChainupAmount = SS.ChainupAmount
		, ShipperRerouteRate = SS.RerouteRate
		, ShipperRerouteRateType = SS.RerouteRateType  -- new
		, ShipperRerouteAmount = SS.RerouteAmount
		, ShipperSplitLoadRate = SS.SplitLoadRate
		, ShipperSplitLoadRateType = SS.SplitLoadRateType  -- new
		, ShipperSplitLoadAmount = SS.SplitLoadAmount
		, ShipperH2SRate = SS.H2SRate
		, ShipperH2SRateType = SS.H2SRateType  -- new
		, ShipperH2SAmount = SS.H2SAmount

		, ShipperTaxRate = SS.OriginTaxRate
		, ShipperTotalAmount = SS.TotalAmount

		, CarrierBatchNum = SC.BatchNum
		, CarrierSettlementUomID = SC.SettlementUomID
		, CarrierSettlementUom = SC.SettlementUom
		, CarrierMinSettlementUnits = SC.MinSettlementUnits
		, CarrierSettlementUnits = SC.SettlementUnits
		, CarrierRateSheetRate = SC.RateSheetRate
		, CarrierRateSheetRateType = SC.RateSheetRateType
		, CarrierRouteRate = SC.RouteRate
		, CarrierRouteRateType = SC.RouteRateType
		, CarrierLoadRate = isnull(SC.RouteRate, SC.RateSheetRate)
		, CarrierLoadRateType = isnull(SC.RouteRateType, SC.RateSheetRateType)
		, CarrierLoadAmount = SC.LoadAmount
		, CarrierOrderRejectRate = SC.OrderRejectRate -- changed
		, CarrierOrderRejectRateType = SC.OrderRejectRateType -- new
		, CarrierOrderRejectAmount = SC.OrderRejectAmount -- changed
		, CarrierWaitFeeSubUnit = SC.WaitFeeSubUnit  -- new
		, CarrierWaitFeeRoundingType = SC.WaitFeeRoundingType  -- new
		, CarrierOriginWaitBillableHours = SS.OriginWaitBillableHours  -- new
		, CarrierOriginWaitBillableMinutes = SC.OriginWaitBillableMinutes  -- new
		, CarrierOriginWaitRate = SC.OriginWaitRate
		, CarrierOriginWaitAmount = SC.OriginWaitAmount
		, CarrierDestinationWaitBillableHours = SS.DestinationWaitBillableHours  -- new
		, CarrierDestinationWaitBillableMinutes = SC.DestinationWaitBillableMinutes  -- new
		, CarrierDestinationWaitRate = SC.DestinationWaitRate -- changed
		, CarrierDestinationWaitAmount = SC.DestinationWaitAmount  -- changed
		, CarrierTotalWaitAmount = SC.TotalWaitAmount
		, CarrierTotalWaitBillableMinutes = nullif(ISNULL(SS.OriginWaitBillableMinutes, 0) + ISNULL(SS.DestinationWaitBillableMinutes, 0), 0)
		, CarrierTotalWaitBillableHours = nullif(ISNULL(SS.OriginWaitBillableHours, 0) + ISNULL(SS.DestinationWaitBillableHours, 0), 0)
		
		, CarrierFuelSurchargeRate = SC.FuelSurchargeRate
		, CarrierFuelSurchargeAmount = SC.FuelSurchargeAmount

		, CarrierChainupRate = SC.ChainupRate
		, CarrierChainupRateType = SC.ChainupRateType  -- new
		, CarrierChainupAmount = SC.ChainupAmount
		, CarrierRerouteRate = SC.RerouteRate
		, CarrierRerouteRateType = SC.RerouteRateType  -- new
		, CarrierRerouteAmount = SC.RerouteAmount
		, CarrierSplitLoadRate = SC.SplitLoadRate
		, CarrierSplitLoadRateType = SC.SplitLoadRateType  -- new
		, CarrierSplitLoadAmount = SC.SplitLoadAmount
		, CarrierH2SRate = SC.H2SRate
		, CarrierH2SRateType = SC.H2SRateType  -- new
		, CarrierH2SAmount = SC.H2SAmount

		, CarrierTaxRate = SC.OriginTaxRate
		, CarrierTotalAmount = SC.TotalAmount

		, OriginGpsLatLon = ltrim(DLO.Lat) + ',' + ltrim(DLO.Lon)
		, OriginLatLon = ltrim(OO.LAT) + ',' + ltrim(OO.LON)
		, OriginAccuracyMeters = DLO.SourceAccuracyMeters
		, OriginDistance = isnull(cast(DLO.DistanceToPoint as int), 99999)
		, OriginGpsArrived = CASE WHEN DLO.DistanceToPoint IS NOT NULL AND DLO.DistanceToPoint <= OO.GeoFenceRadiusMeters THEN 1 ELSE 0 END
		, DestGpsLatLon = ltrim(DLD.Lat) + ',' + ltrim(DLD.Lon)
		, DestLatLon = ltrim(D.LAT) + ',' + ltrim(D.LON)
		, DestAccuracyMeters = DLD.SourceAccuracyMeters
		, DestDistance = isnull(cast(DLD.DistanceToPoint as int), 99999)
		, DestGpsArrived = CASE WHEN DLD.DistanceToPoint IS NOT NULL AND DLD.DistanceToPoint <= OO.GeoFenceRadiusMeters THEN 1 ELSE 0 END
		, ShipperDestCode = CDC.Code
	FROM viewOrder_OrderTicket_Full O
	JOIN tblOrigin OO ON OO.ID = O.OriginID
	JOIN tblDestination D ON D.ID = O.DestinationID
	LEFT JOIN tblCustomerDestinationCode CDC ON CDC.CustomerID = O.CustomerID AND CDC.DestinationID = O.DestinationID
	LEFT JOIN viewDriverLocation_OriginFirstArrive DLO ON DLO.OrderID = O.ID AND DLO.OriginID = O.OriginID
	LEFT JOIN viewDriverLocation_DestinationFirstArrive DLD ON DLD.OrderID = O.ID AND DLD.DestinationID = O.DestinationID
	LEFT JOIN viewOrderSettlementCarrier SC ON SC.OrderID = O.ID
	LEFT JOIN viewOrderSettlementShipper SS ON SS.OrderID = O.ID
	LEFT JOIN tblShipperSettlementBatch SSB ON SSB.ID = SS.BatchID
GO

-- use CASCADE DELETES for UserReports -> UserReportColumns
ALTER TABLE dbo.tblUserReportColumnDefinition
	DROP CONSTRAINT FK_UserReportColumnDefinition_UserReport
GO
ALTER TABLE dbo.tblUserReportDefinition SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblUserReportColumnDefinition ADD CONSTRAINT
	FK_UserReportColumnDefinition_UserReport FOREIGN KEY
	(
	UserReportID
	) REFERENCES dbo.tblUserReportDefinition
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	
GO
ALTER TABLE dbo.tblUserReportColumnDefinition SET (LOCK_ESCALATION = TABLE)
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'ReplaceCS') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION ReplaceCS
GO
/***************************************************
-- Date Created: 11 Feb 2015
-- Author: Kevin Alons
-- Purpose: case-insensitive REPLACE function
***************************************************/
CREATE FUNCTION ReplaceCS(@expr varchar(max), @find varchar(max), @repl varchar(max)) RETURNS varchar(max) AS
BEGIN
	RETURN replace(@expr collate Latin1_General_CS_AS, @find collate Latin1_General_CS_AS, @repl collate Latin1_General_CS_AS)
END
GO
GRANT EXECUTE ON dbo.ReplaceCS to dispatchcrude_iis_acct
GO

IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'viewUserRoles'))
	DROP VIEW viewUserRoles
GO
/***************************************************
-- Date Created: 11 Feb 2014
-- Author: Kevin Alons
-- Purpose: return all UserNames with a concatenated list of roles they belong to
***************************************************/
CREATE VIEW viewUserRoles AS
	WITH cte(UserName,combined,rn) AS
	(
		SELECT UserName, RoleName, rn=ROW_NUMBER() over (PARTITION by UserName order by RoleName)
		FROM aspnet_Users U
		JOIN aspnet_UsersInRoles UR ON UR.UserId = U.UserId
		JOIN aspnet_Roles R ON R.RoleId = UR.RoleId
	)
	,cte2(UserName,finalstatus,rn) AS
	(
		SELECT UserName, convert(varchar(max),combined), 1 FROM cte WHERE rn=1
		UNION all
		SELECT cte2.UserName, convert(varchar(max),cte2.finalstatus+', ' + cte.combined), cte2.rn + 1
		FROM cte2
		JOIN cte ON cte.UserName = cte2.UserName and cte.rn=cte2.rn+1
	)
	SELECT UserName, Roles = MAX(finalstatus) FROM cte2 GROUP BY UserName
GO
GRANT SELECT ON viewUserRoles TO dispatchcrude_iis_acct
GO

-- remove references to Origin|Dest|Total WaitMinutes fields in Report Center (and replace with 
UPDATE tblReportColumnDefinition SET DataField = 'ShipperOriginWaitBillableHours', Caption = 'SETTLEMENT | SHIPPER | Shipper Origin Wait Billable Hours', AllowedRoles = 'Administrator,Management,Customer' WHERE ID = 83
UPDATE tblReportColumnDefinition SET DataField = 'ShipperDestinationWaitBillableHours', Caption = 'SETTLEMENT | SHIPPER | Shipper Destination Wait Billable Hours', AllowedRoles = 'Administrator,Management,Customer' WHERE ID = 129
UPDATE tblReportColumnDefinition SET DataField = 'ShipperTotalWaitBillableMinutes', Caption = 'SETTLEMENT | SHIPPER | Shipper Total Wait Billable Minutes', AllowedRoles = 'Administrator,Management,Customer' WHERE ID = 130
SET IDENTITY_INSERT tblReportColumnDefinition ON
INSERT INTO tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport)
	SELECT ROW_NUMBER() over (order by id) + 236, ReportID, REPLACE(DataField, 'Shipper', 'Carrier'), dbo.replacecs(dbo.replacecs(Caption, 'Shipper', 'Carrier'), 'SHIPPER', 'CARRIER'), dataformat, filterdatafield, filtertypeid, filterdropdownsql, filterallowcustomtext, replace(allowedroles, 'Customer', 'Carrier'), ordersingleexport
	FROM tblReportColumnDefinition
	WHERE ID IN (83, 129, 130)
SET IDENTITY_INSERT tblReportColumnDefinition OFF

-- update any user reports to reference the updated report columns
UPDATE tblUserReportColumnDefinition
  SET ReportColumnID = 
	CASE WHEN X.Name LIKE '%Shipper%' OR X.Roles LIKE '%Customer%'
				THEN CASE ReportColumnID WHEN 83 THEN 130 WHEN 129 THEN 219 ELSE 220 END
		 WHEN X.Name LIKE '%Carrier%' OR X.Roles LIKE '%Carrier%'
				THEN CASE ReportColumnID WHEN 83 THEN 239 WHEN 129 THEN 221 ELSE 222 END
		ELSE CASE ReportColumnID WHEN 83 THEN 130 WHEN 129 THEN 219 ELSE 220 END
	END
FROM tblUserReportColumnDefinition URCD
JOIN (
	SELECT URD.ID, URD.username, name, UR.Roles
	FROM tblUserReportDefinition URD
	LEFT JOIN viewUserRoles UR ON UR.UserName = URD.UserName
	WHERE ID IN (select UserReportID from tblUserReportColumnDefinition where ReportColumnID in (83, 129, 130))
) X ON X.ID = URCD.UserReportID
WHERE URCD.ReportColumnID IN (83, 129, 130)
GO

UPDATE tblReportColumnDefinition SET DataField = REPLACE(DataField, 'TotalWaitMinutes', 'ShipperTotalWaitBillableMinutes') WHERE DataField LIKE '%TotalWaitMinutes%'
GO

EXEC _spRebuildAllObjects
GO

COMMIT
SET NOEXEC OFF
