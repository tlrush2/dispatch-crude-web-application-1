-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.12.2'
SELECT  @NewVersion = '3.12.3'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-944: Revise ReportCenter to UserName instead of HttpContext (to allow access by non ASP.NET projects)'
	UNION SELECT @NewVersion, 1, 'DCWEB-944: Email Subscriptions use Time instead of Execute Hour UTC'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

EXEC _spDropFunction 'fnSplitString'
GO
/**************************************
Creation Info: 3.12.3 - 2016/04/20
Author: Kevin Alons
Purpose: Split a string by the specified delimiter (into the returned table)
**************************************/
CREATE FUNCTION fnSplitString
(
    @expr varchar(500),
    @deliminator varchar(10)
)
RETURNS 
@ret TABLE 
(
    id int IDENTITY(1,1) NOT NULL
  , value varchar(255) NULL
)
AS
BEGIN
        DECLARE @iSpaces int = charindex(@deliminator, @expr, 0)
			, @value varchar(50)

        WHILE @iSpaces > 0
        BEGIN
            SET @value = substring(@expr, 0, charindex(@deliminator, @expr, 0))

            INSERT INTO @ret(value)
				SELECT @value

			SET @expr = substring(@expr, charindex(@deliminator, @expr, 0)+ len(@deliminator), len(@expr) - charindex(' ', @expr, 0))
			SET @iSpaces = charindex(@deliminator, @expr, 0)
        END

        IF len(@expr) > 0
			INSERT INTO @ret(value)
				SELECT @expr
    RETURN
END
GO
GRANT SELECT ON fnSplitString TO role_iis_acct
GO

EXEC _spDropFunction 'fnUserAllProfileValues'
GO
/**************************************
Creation Info: 3.12.3 - 2016/04/20
Author: Kevin Alons
Purpose: retrieve all profile values for the specified user
**************************************/
CREATE FUNCTION fnUserAllProfileValues 
(
	@UserName varchar(100)
)
RETURNS @ret TABLE (name varchar(100), value varchar(100)) AS
BEGIN
	DECLARE @propertyNames varchar(max)
		, @propertyValues varchar(max)
	SELECT @propertyNames = cast(PropertyNames as varchar(max)) + 'A'
		, @propertyValues = PropertyValuesString
	FROM aspnet_Profile
	WHERE UserID IN (SELECT UserID FROM aspnet_Users WHERE UserName LIKE @UserName)

	DECLARE @delimPos int = patindex('%[0-9]:[^-0-9]%', @propertyNames)
	DECLARE @propName varchar(100), @type char(1), @start int, @len int, @propValue varchar(100)
	WHILE @delimPos > 0
	BEGIN
		SELECT @propName = CASE WHEN id = 1 THEN value ELSE @propName END
			, @type = CASE WHEN id = 2 THEN value ELSE @type END
			, @start = CASE WHEN id = 3 THEN cast(value as int) ELSE @start END
			, @len = CASE WHEN id = 4 THEN cast(value as int) ELSE @len END
		FROM dbo.fnSplitString(substring(@propertyNames, 1, @delimPos), ':') 

		SET @propValue = CASE WHEN @type = 'S' THEN substring(@propertyValues, @start+1, @len) ELSE NULL END
		INSERT INTO @ret VALUES(@propName, @propValue)

		SET @propertyNames = substring(@propertyNames, @delimPos + 2, 4000)
		SET @delimPos = patindex('%[0-9]:[^-0-9]%', @propertyNames)
	END
	RETURN
END
GO
GRANT SELECT ON fnUserAllProfileValues TO role_iis_acct
GO

CREATE TABLE tblUserReportEmailSubscriptionLog
(
  ID int identity(1, 1) NOT NULL CONSTRAINT PK_UserReportEmailSubscriptionLog PRIMARY KEY
, UserReportEmailSubscriptionID int NOT NULL CONSTRAINT FK_UserReportEmailSubscriptionLog_Subscription FOREIGN KEY REFERENCES tblUserReportEmailSubscription(ID)
, ExecutionDateUTC datetime NOT NULL CONSTRAINT DF_UserReportEmailSubscriptionLog_ExecutionDateUTC DEFAULT (getutcdate())
, ExecutionDurationSeconds decimal(8, 3) NULL
, Successful bit NOT NULL CONSTRAINT DF_UserReportEmailSubscriptionLog_Successful DEFAULT (1)
, Errors varchar(1000) NULL
)
GO
GRANT INSERT ON tblUserReportEmailSubscriptionLog TO role_iis_acct
GO

INSERT INTO tblSettingType (ID, Name, CreateDateUTC, CreatedByUser) VALUES (5, 'Html', getutcdate(), 'System')
GO

--------------------------------------------------
-- UPDATE tblSetting.Value to varchar(max) [from varchar(255)]
ALTER TABLE dbo.tblSetting
	DROP CONSTRAINT FK_Setting_SettingType
GO
ALTER TABLE dbo.tblSettingType SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblSetting
	DROP CONSTRAINT DF_Setting_CreateDateUTC
GO
ALTER TABLE dbo.tblSetting
	DROP CONSTRAINT DF_tblSetting_ReadOnly
GO
CREATE TABLE dbo.Tmp_tblSetting
	(
	ID int NOT NULL,
	Name varchar(75) NOT NULL,
	SettingTypeID tinyint NOT NULL,
	Value varchar(MAX) NULL,
	Category varchar(35) NULL,
	CreateDateUTC smalldatetime NULL,
	CreatedByUser varchar(100) NULL,
	LastChangeDateUTC smalldatetime NULL,
	LastChangedByUser varchar(100) NULL,
	ReadOnly bit NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_tblSetting SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_tblSetting ADD CONSTRAINT
	DF_Setting_CreateDateUTC DEFAULT (getutcdate()) FOR CreateDateUTC
GO
ALTER TABLE dbo.Tmp_tblSetting ADD CONSTRAINT
	DF_tblSetting_ReadOnly DEFAULT ((0)) FOR ReadOnly
GO
IF EXISTS(SELECT * FROM dbo.tblSetting)
	 EXEC('INSERT INTO dbo.Tmp_tblSetting (ID, Name, SettingTypeID, Value, Category, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, ReadOnly)
		SELECT ID, Name, SettingTypeID, CONVERT(varchar(MAX), Value), Category, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, ReadOnly FROM dbo.tblSetting WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.tblSetting
GO
EXECUTE sp_rename N'dbo.Tmp_tblSetting', N'tblSetting', 'OBJECT' 
GO
ALTER TABLE dbo.tblSetting ADD CONSTRAINT
	PK_Setting PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.tblSetting ADD CONSTRAINT
	FK_Setting_SettingType FOREIGN KEY
	(
	SettingTypeID
	) REFERENCES dbo.tblSettingType
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
--------------------------------------------------
-- end UPDATE tblSetting
--------------------------------------------------
GO

INSERT INTO tblSetting (ID, Category, Name, SettingTypeID, Value, CreateDateUTC, CreatedByUser) 
	VALUES (60, 'Report Center', 'Email Subscription html body', 5,
		'<center><b>Test Body</b></center><p><p>This is a test e-mail of the E-mail Subscription Program.  This attests that the program has successfully sent an e-mail with the proper Subject (Report) and attachment. Thank you!<br/><p>Customer Service<br/><a href="http://www.dispatchcrude.com">DispatchCrude.com</a><br/>1112 South Villa Drive<br/>Evansville, IN. 47714<br/>(812) 303 - HELP(4357) / Office<br/>(812) 496 - 4195 / Direct<br/>(812) 205 - 2911 / Fax<br/>'
		, getutcdate(), 'System')
GO

COMMIT
SET NOEXEC OFF