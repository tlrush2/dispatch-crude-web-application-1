-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.7.5.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.7.5.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.7.5'
SELECT  @NewVersion = '3.7.6'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Settings: fix some Name/Categories of Driver | Gauger App settings'
	UNION
	SELECT @NewVersion, 0, 'Gauger App: add missing fnOrderRules_GaugerApp table-level function'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

UPDATE tblSetting SET Category = 'Driver App Settings' WHERE ID = 41
UPDATE tblSetting SET Name = 'Log Sync JSON', Category = 'Driver App Settings' WHERE ID = 43
UPDATE tblSetting SET Name = 'Log Sync JSON' WHERE ID = 49
UPDATE tblSettingType SET Name = 'Boolean' WHERE ID = 2
GO

/*******************************************/
-- Date Created: 1 Mar 2015
-- Author: Kevin Alons
-- Purpose: return Order Rules for every changed Order
/*******************************************/
CREATE FUNCTION fnOrderRules_GaugerApp(@GaugerID int, @LastChangeDateUTC datetime) 
RETURNS 
	@ret TABLE (
		ID int
	  , OrderID int
	  , TypeID int
	  , Value varchar(255)
	)
AS BEGIN
	DECLARE @IDS TABLE (ID int)
	INSERT INTO @IDS
		SELECT O.ID
		FROM dbo.viewOrder O
		JOIN tblGaugerOrder GAO ON GAO.OrderID = O.ID
		JOIN dbo.viewOrigin OO ON OO.ID = O.OriginID
		JOIN dbo.viewDestination D ON D.ID = O.DestinationID
		JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
		JOIN dbo.tblRoute R ON R.ID = O.RouteID
		LEFT JOIN tblGaugerOrderVirtualDelete GOVD ON GOVD.OrderID = O.ID AND GOVD.GaugerID = GAO.GaugerID
		CROSS JOIN (SELECT DATEADD(second, -5, @LastChangeDateUTC) AS LCD) LCD
	WHERE GAO.StatusID NOT IN (1) -- don't include REQUESTED Gauger Order records
	  AND O.ID IN (
		SELECT OrderID FROM tblGaugerOrder WHERE GaugerID = @GaugerID
		UNION 
		SELECT OrderID FROM tblGaugerOrderVirtualDelete WHERE GaugerID = @GaugerID
	)
	  AND (@LastChangeDateUTC IS NULL
			OR O.CreateDateUTC >= LCD.LCD
			OR O.LastChangeDateUTC >= LCD.LCD
			OR O.DeleteDateUTC >= LCD.LCD
			OR C.LastChangeDateUTC >= LCD.LCD
			OR OO.LastChangeDateUTC >= LCD.LCD
			OR R.LastChangeDateUTC >= LCD.LCD
		)
	
	DECLARE @ID int
	WHILE EXISTS (SELECT * FROM @IDS)
	BEGIN
		SELECT TOP 1 @ID = ID FROM @IDS
		INSERT INTO @ret (ID, OrderID, TypeID, Value)
			SELECT ID, @ID, TypeID, Value FROM dbo.fnOrderOrderRules(@ID)
		DELETE FROM @IDS WHERE ID = @ID
		-- if no records exist, then return a NULL record to tell the Mobile App to delete any existing records for this order
		IF NOT EXISTS (SELECT * FROM @ret WHERE OrderID = @ID)
			INSERT INTO @ret (OrderID) VALUES (@ID)
	END
	RETURN
END
GO
GRANT SELECT ON fnOrderRules_GaugerApp TO dispatchcrude_iis_acct
GO

COMMIT
SET NOEXEC OFF