-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.7.13.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.7.13.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.7.13'
SELECT  @NewVersion = '3.7.14'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Settlement: fix issue preventing deletion of Assessorial Rates that are referenced by pending Settlement records (not yet batched)'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/*************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: handle specialized logic related to Effective/End dates (all handled through the associated view)
-- Changes:
	-- 3.7.15 - 5/21/15 - KDA - remove any un-Settled Assessorial Charges referencing the deleted Assessorial Rate(s)
/*************************************/
ALTER TRIGGER trigCarrierAssessorialRate_IOD ON tblCarrierAssessorialRate INSTEAD OF DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewCarrierAssessorialRate X ON X.ID = d.ID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('Locked (in use) records cannot be deleted', 16, 1)
		ROLLBACK
	END
	ELSE
	BEGIN
		-- remove all Un-Settled references to this deleted rate
		DELETE FROM tblOrderSettlementCarrierAssessorialCharge 
		FROM tblOrderSettlementCarrierAssessorialCharge SCAC
		JOIN tblOrderSettlementCarrier SC ON SC.OrderID = SCAC.OrderID
		JOIN deleted d ON d.ID = SCAC.AssessorialRateID
		WHERE SC.BatchID IS NULL
		-- do the actual delete action
		DELETE FROM tblCarrierAssessorialRate WHERE ID IN (SELECT ID FROM deleted)
	END
END
GO

/*************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: handle specialized logic related to Effective/End dates (all handled through the associated view)
-- Changes:
	-- 3.7.15 - 5/21/15 - KDA - remove any un-Settled Assessorial Charges referencing the deleted Assessorial Rate(s)
/*************************************/
ALTER TRIGGER trigShipperAssessorialRate_IOD ON tblShipperAssessorialRate INSTEAD OF DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewShipperAssessorialRate X ON X.ID = d.ID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('Locked (in use) records cannot be deleted', 16, 1)
		ROLLBACK
	END
	ELSE
	BEGIN
		-- remove all Un-Settled references to this deleted rate
		DELETE FROM tblOrderSettlementShipperAssessorialCharge 
		FROM tblOrderSettlementShipperAssessorialCharge SCAC
		JOIN tblOrderSettlementShipper SC ON SC.OrderID = SCAC.OrderID
		JOIN deleted d ON d.ID = SCAC.AssessorialRateID
		WHERE SC.BatchID IS NULL
		-- do the actual delete action now
		DELETE FROM tblShipperAssessorialRate WHERE ID IN (SELECT ID FROM deleted)
	END
END
GO

COMMIT
SET NOEXEC OFF