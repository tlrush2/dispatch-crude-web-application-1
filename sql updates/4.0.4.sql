-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.0.3'
SELECT  @NewVersion = '4.0.4'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1670: Add Elmah error log permission'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/* Add two new permissions */
EXEC spAddNewPermission 'viewElmahErrorLog', 'Allow user to view the Elmah error log (Admin, Mgmt., and Support ONLY)','Security','Elmah Error Log'
GO
EXEC spAddNewPermission 'unsettleProducerBatches', 'Allow user to unsettle a producer settlement batch','Settlement','Unsettle Producer Batches'
GO



COMMIT
SET NOEXEC OFF