-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.6.0'
SELECT  @NewVersion = '3.6.1'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'Report Center: Add basic grouping/aggregate capability'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

CREATE TABLE tblReportGroupingFunction
(
  ID int NOT NULL CONSTRAINT PK_ReportGroupingFunction PRIMARY KEY CLUSTERED
, Name varchar(25) NOT NULL
)
GO
GRANT SELECT ON tblReportGroupingFunction TO dispatchcrude_iis_acct
GO
CREATE UNIQUE INDEX udxReportGroupingFunction_Name ON tblReportGroupingFunction(Name)
GO 
INSERT INTO tblReportGroupingFunction(ID, Name)
	SELECT 1, 'None'
--	UNION SELECT 2, 'Grouped - Only' -- WILL NOT BE USED
--	UNION SELECT 3, 'Grouped - Expanded' -- WILL BE USED by Excel Grouping
--	UNION SELECT 4, 'Grouped - Collapsed'
	UNION SELECT 10, 'Sum'
	UNION SELECT 11, 'Avg'
	UNION SELECT 12, 'Count'
	UNION SELECT 13, 'Min'
	UNION SELECT 14, 'Max'
GO

ALTER TABLE tblUserReportColumnDefinition ADD GroupingFunctionID int NOT NULL 
	CONSTRAINT FK_UserReportColumnDefinition_GroupingFunction FOREIGN KEY REFERENCES tblReportGroupingFunction(ID)
	CONSTRAINT DF_UserReportColumnDefinition_GroupingFunction DEFAULT (1) -- None
GO

/***********************************************/
-- Date Created: 27 Jul 2014
-- Author: Kevin Alons
-- Purpose: add related JOINed fields to the tblUserReportColumnDefinition table results
/***********************************************/
ALTER VIEW [dbo].[viewUserReportColumnDefinition] AS
	SELECT URCD.ID
		, URCD.UserReportID
		, URCD.ReportColumnID
		, URCD.Caption
		, DataFormat = isnull(URCD.DataFormat, RCD.DataFormat)
		, URCD.SortNum
		, URCD.FilterOperatorID
		, URCD.FilterValue1
		, URCD.FilterValue2
		, URCD.Export
		, URCD.DataSort
		, URCD.GroupingFunctionID
		, DataSortABS = abs(URCD.DataSort)
		, URCD.CreateDateUTC, URCD.CreatedByUser
		, URCD.LastChangeDateUTC, URCD.LastChangedByUser
		, RCD.DataField
		, BaseCaption = dbo.fnCaptionRoot(RCD.Caption)
		, OutputCaption = coalesce(URCD.Caption, dbo.fnCaptionRoot(RCD.Caption), RCD.DataField)
		, RCD.FilterTypeID
		, FilterDataField = isnull(RCD.FilterDataField, RCD.DataField)
		, RCD.FilterDropDownSql
		, RCD.FilterAllowCustomText
		, RCD.AllowedRoles
		, RCD.OrderSingleExport
		, FilterOperator = RFO.Name
		, FilterValues = CASE 
				WHEN RFO.ID IN (1,4,5) OR RCD.FilterAllowCustomText = 1 THEN FilterValue1 + ISNULL(' - ' + FilterValue2, '')
				ELSE 
					CASE WHEN FilterValue1 IS NOT NULL THEN '&lt;filtered&gt;' 
						 ELSE NULL 
					END 
			END
	FROM tblUserReportColumnDefinition URCD
	JOIN viewReportColumnDefinition RCD ON RCD.ID = URCD.ReportColumnID
	LEFT JOIN tblReportFilterOperator RFO ON RFO.ID = URCD.FilterOperatorID;

GO

/*******************************************/
-- Date Created: 23 Jul 2014
-- Author: Kevin Alons
-- Purpose: clone an existing UserReport (defaults to a full clone with columns, but can do an AddNew otherwise)
/*******************************************/
ALTER PROCEDURE [dbo].[spCloneUserReport]
(
  @userReportID int
, @reportName varchar(50)
, @userName varchar(100)
, @includeColumns bit = 1
, @newID int = 0 OUTPUT 
) AS
BEGIN
	SET NOCOUNT ON
	INSERT INTO tblUserReportDefinition (Name, ReportID, UserName, CreatedByUser)
		SELECT @reportName, ReportID, @userName, isnull(@userName, 'Administrator')
		FROM tblUserReportDefinition WHERE ID = @userReportID
	SET @newID = SCOPE_IDENTITY()
	IF (@includeColumns = 1)
	BEGIN
		INSERT INTO tblUserReportColumnDefinition (UserReportID, ReportColumnID, Caption
			, SortNum, DataSort, GroupingFunctionID, DataFormat, FilterOperatorID, FilterValue1, FilterValue2
			, CreatedByUser)
			SELECT @newID, ReportColumnID, Caption
				, SortNum, DataSort, GroupingFunctionID, DataFormat, FilterOperatorID, FilterValue1, FilterValue2
				, isnull(@userName, 'Administrator')
			FROM tblUserReportColumnDefinition WHERE UserReportID = @userReportID
	END
	
	SELECT NEWID=@newID
	RETURN (isnull(@newID, 0))
END
GO

EXEC _spRebuildAllObjects
GO

COMMIT
SET NOEXEC OFF