-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.11.16'
SELECT  @NewVersion = '3.11.16.1'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1195: Convert shipper maintenance page to MVC'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO



/* Add delete columns to shipper/customer table in order to allow an Active column and the Deactivate option */
ALTER TABLE tblCustomer
ADD DeleteDateUTC SMALLDATETIME NULL
	, DeletedByUser VARCHAR(100) NULL
GO

	
/* Adding permissions to roles table for the shiper maintenance page */
INSERT INTO aspnet_Roles (ApplicationId, RoleId, RoleName, LoweredRoleName, Description)
SELECT '446467D6-39CD-45E3-B3E0-CAC7945AF3E8', NEWID(), 'viewShippers', 'viewshippers', 'Allow user to see the shippers maintenance page'
UNION
SELECT '446467D6-39CD-45E3-B3E0-CAC7945AF3E8', NEWID(), 'createShippers', 'createshippers', 'Allow user to create new shippers'
UNION
SELECT '446467D6-39CD-45E3-B3E0-CAC7945AF3E8', NEWID(), 'editShippers', 'editshippers', 'Allow user to edit existing shippers'
UNION
SELECT '446467D6-39CD-45E3-B3E0-CAC7945AF3E8', NEWID(), 'deactivateShippers', 'deactivateshippers', 'Allow user to deactivate (and reactivate) shippers'
EXCEPT SELECT ApplicationId, RoleId, RoleName, LoweredRoleName, Description
FROM aspnet_Roles
GO


/*******************************************
Date Created: 28 Apr 2016
Author: Ben Bloodworth
Purpose: I created this to make it easier for us to add new permissions to existing users as we transition
		to the Users/Groups/Permissions model.  This can be used in SQL update files to add new permission(s)
		to all users of a certain permission (currently called roles).  
Usage:  Users with permission/role 'Administrator' can be all be given a new permission like this:  
			EXEC spAddUserPermissionsByRole 'Administrator', 'viewShippers'
Extra Notes:  Once we've fully transitioned to the new permissions model this code will not likely be useful and
				can be removed at that time.
*******************************************/
CREATE PROCEDURE spAddUserPermissionsByRole
(
	  @ROLENAME VARCHAR(50)
	, @PERMISSIONNAME VARCHAR(50)
) AS
BEGIN

	DECLARE @USERID VARCHAR(100)
		  , @USERCOUNT INT
		  , @PERMISSIONID VARCHAR(100)
		  , @i INT = 0

	/* Get the new permission GUID value from the name specified */
	SET @PERMISSIONID = (SELECT RoleId FROM aspnet_Roles WHERE RoleName = @PERMISSIONNAME)

	/* Get the count of the users that are in the specified existing role(permission), that are not already given the new permission */
	SET @USERCOUNT = (SELECT COUNT(U.UserID)
						FROM aspnet_Users U
							LEFT JOIN aspnet_UsersInRoles UIR ON UIR.UserId = U.UserId
							LEFT JOIN aspnet_Roles R ON R.RoleId = UIR.RoleId
						WHERE
							R.RoleName = @ROLENAME
							AND U.UserId NOT IN (SELECT UserId 
														FROM aspnet_UsersInRoles 
														WHERE RoleId = @PERMISSIONID))

	WHILE (@i < @USERCOUNT)
		BEGIN
			/* Get the GUID of each user in the specified existing role(permission), that has not not already been given the new permission */
			SET @USERID = (SELECT TOP 1 U.UserID
							FROM aspnet_Users U
								LEFT JOIN aspnet_UsersInRoles UIR ON UIR.UserId = U.UserId
								LEFT JOIN aspnet_Roles R ON R.RoleId = UIR.RoleId
							WHERE
								R.RoleName = @ROLENAME
								AND U.UserId NOT IN (SELECT UserId 
														FROM aspnet_UsersInRoles 
														WHERE RoleId = @PERMISSIONID))
																	
			/* Insert a record for each user specified to give them the new permission */
			INSERT INTO aspnet_UsersInRoles (UserId, RoleId)
				SELECT @USERID, @PERMISSIONID

			SET @i = @i + 1
		END
END
GO


/* Add all new shipper page permissions to current Administrator users on each site */
EXEC spAddUserPermissionsByRole 'Administrator', 'viewShippers'
GO
EXEC spAddUserPermissionsByRole 'Administrator', 'createShippers'
GO
EXEC spAddUserPermissionsByRole 'Administrator', 'editShippers'
GO
EXEC spAddUserPermissionsByRole 'Administrator', 'deactivateShippers'
GO


/* Add all new shipper page permissions to current Management users on each site */
EXEC spAddUserPermissionsByRole 'Management', 'viewShippers'
GO
EXEC spAddUserPermissionsByRole 'Management', 'createShippers'
GO
EXEC spAddUserPermissionsByRole 'Management', 'editShippers'
GO
EXEC spAddUserPermissionsByRole 'Management', 'deactivateShippers'
GO


/* Add all new shipper page permissions to current Operations users on each site */
EXEC spAddUserPermissionsByRole 'Operations', 'viewShippers'
GO
EXEC spAddUserPermissionsByRole 'Operations', 'createShippers'
GO
EXEC spAddUserPermissionsByRole 'Operations', 'editShippers'
GO
EXEC spAddUserPermissionsByRole 'Operations', 'deactivateShippers'
GO


/* Add all new shipper page permissions to current Compliance users on each site */
EXEC spAddUserPermissionsByRole 'Compliance', 'viewShippers'
GO
EXEC spAddUserPermissionsByRole 'Compliance', 'createShippers'
GO
EXEC spAddUserPermissionsByRole 'Compliance', 'editShippers'
GO
EXEC spAddUserPermissionsByRole 'Compliance', 'deactivateShippers'
GO


COMMIT 
SET NOEXEC OFF