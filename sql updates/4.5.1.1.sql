SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.5.1'
SELECT  @NewVersion = '4.5.1.1'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'JT-169 Hotfix for allowing null terminals in the carrier rule'	
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/***********************************
-- Date Created: 2016-05-03
-- Author: Joe Engler
-- Purpose: retrieve and return the Carrier Rule info for the specified order
-- Changes:
--		4.4.14		2016/12/26		JAE		Require a carrier at a minimum
--		4.5.0		2017/01/25		BSB		Add terminal, Readjust best match order
--		4.5.1.1		2017/02/08		JAE		Hotfix to allow nullable terminals in carrier rule
***********************************/
ALTER FUNCTION fnCarrierRules(@StartDate date, @EndDate date, @TypeID int, @DriverID int, @CarrierID int, @TerminalID int, @StateID int, @RegionID int, @BestMatchOnly bit = 0)
RETURNS 
	@ret TABLE (
		ID int
	  , TypeID int
	  , DriverID int
	  , CarrierID int
	  , TerminalID int
	  , StateID int
	  , RegionID int
	  , Value varchar(255)
	  , RuleTypeID int
	  , EffectiveDate date
	  , EndDate date
	  , MaxEffectiveDate date
	  , MinEndDate date
	  , NextEffectiveDate date
	  , PriorEndDate date
	  , BestMatch bit
	  , Ranking smallmoney
	  , CreateDateUTC datetime
	  , CreatedByUser varchar(100)
	  , LastChangeDateUTC datetime
	  , LastChangedByUser varchar(100)
	)
AS BEGIN
	-- get the raw data (all matched rows)
	DECLARE @src TABLE (ID int, TypeID int, Ranking smallmoney)
	INSERT INTO @src (ID, TypeID, Ranking)
		SELECT ID, TypeID
			, Ranking =	dbo.fnRateRanking(@DriverID, R.DriverID, 16, 1)
					  + dbo.fnRateRanking(@CarrierID, R.CarrierID, 8, 0)
					  + dbo.fnRateRanking(@TerminalID, R.TerminalID, 4, 1)
					  + dbo.fnRateRanking(@StateID, R.StateID, 2, 1)
					  + dbo.fnRateRanking(@RegionID, R.RegionID, 1, 1)
		FROM viewCarrierRule R
		WHERE coalesce(nullif(@DriverID, 0), R.DriverID, 0) = coalesce(R.DriverID, nullif(@DriverID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@TerminalID, 0), R.TerminalID, 0) = coalesce(TerminalID, nullif(@TerminalID, 0), 0)
		  AND coalesce(nullif(@StateID, 0), R.StateID, 0) = coalesce(R.StateID, nullif(@StateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(R.RegionID, nullif(@RegionID, 0), 0)
		  AND (nullif(@TypeID, 0) IS NULL OR TypeID = @TypeID)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)

	-- return the rate data for the best-match rates 
	INSERT INTO @ret (ID, TypeID, DriverID, CarrierID, TerminalID, StateID, RegionID, Value, RuleTypeID, EffectiveDate, EndDate
			, NextEffectiveDate, PriorEndDate, BestMatch, Ranking, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser)
		SELECT R.ID, TypeID, DriverID, CarrierID, TerminalID, StateID, RegionID, R.Value, RuleTypeID, EffectiveDate, EndDate
			, NextEffectiveDate, PriorEndDate, BestMatch, Ranking, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser
		FROM viewCarrierRule R
		JOIN (
			SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.TypeID IS NULL THEN 0 ELSE 1 END as bit)
			FROM @src S
			LEFT JOIN (
				SELECT TypeID, Ranking = MAX(Ranking)
				FROM @src
				WHERE @StartDate = ISNULL(@EndDate, @StartDate)
				  AND Ranking % 1 = 0.01 * 5 -- ensure a complete match is found for BESTMATCH (last number needs to match count of best match fields)
				GROUP BY TypeID
			) X ON X.TypeID = S.TypeID AND X.Ranking = S.Ranking
		) X ON X.ID = R.ID
		WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)		
	RETURN
END
GO


COMMIT
SET NOEXEC OFF