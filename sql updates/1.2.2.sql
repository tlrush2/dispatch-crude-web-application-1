BEGIN TRANSACTION
GO

UPDATE tblSetting SET Value = '1.2.2' WHERE ID=0
GO

ALTER TABLE tblDriver ADD DrugScreenBackgroundReleaseDocument varbinary(max) null
GO
ALTER TABLE tblDriver ADD DrugScreenBackgroundReleaseDocName varchar(100) null
GO

EXEC _spRefreshAllViews
GO

EXEC _spRecompileAllStoredProcedures
GO

COMMIT