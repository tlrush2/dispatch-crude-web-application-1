-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.7.20.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.7.20.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0

-- backup database [dispatchcrude.dev] to disk = 'C:\Program Files\Microsoft SQL Server\MSSQL10_50.SQLEXPRESSDEV02\MSSQL\Backup\3.7.23-gsm.bak'

DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.7.23'
SELECT  @NewVersion = '3.7.24'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-759: Clone Order Rule: Destination NSV Tolerance % (from Origin NSV)'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/* 
	SELECT * FROM tblOrderRule
	SELECT * FROM tblOrderRuleType
*/

/* 3.7.24 - 06/08/2015 - GSM - DCWEB-759 - Clone Order Rule: Destination NSV Tolerance % (from Origin NSV) [like GOV->NSV] */
INSERT tblOrderRuleType (ID,Name,RuleTypeID)
     VALUES (4,'Destination NSV tolerance % (from Origin NSV)',3)
GO


COMMIT
SET NOEXEC OFF
