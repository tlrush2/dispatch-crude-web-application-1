SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.10.13.6'
SELECT  @NewVersion = '3.10.13.7'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Truck Type - Final updates'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


-- PART 8
-- FINAL UPDATES
--------------------------------------------------------------------------------------------

-- add truckType column to report center
SET IDENTITY_INSERT tblReportColumnDefinition ON
  INSERT tblReportColumnDefinition (ID , ReportID, DataField, Caption , DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql , FilterAllowCustomText, AllowedRoles, OrderSingleExport)
  SELECT 282 ,1, 'TruckType', 'GENERAL | Truck Type', NULL, 'TruckTypeID', 2, 'SELECT ID, Name FROM tblTruckType ORDER BY Name', 0, '*', 1 
  EXCEPT SELECT ID, ReportID, DataField , Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText , AllowedRoles, OrderSingleExport FROM tblReportColumnDefinition
SET IDENTITY_INSERT tblReportColumnDefinition OFF
GO


-- rebuild/refresh
exec _spRebuildAllObjects
GO

COMMIT
SET NOEXEC OFF