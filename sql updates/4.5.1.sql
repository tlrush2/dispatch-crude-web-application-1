SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.5.0'
SELECT  @NewVersion = '4.5.1'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'JT-1494 - Fix Terminal permissions category after page move'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/* Update the terminal permissions' category */
UPDATE aspnet_Roles
SET Category = 'Assets - Locations - Terminals'
WHERE RoleName = 'viewTerminals'
	OR RoleName = 'createTerminals'
	OR RoleName = 'editTerminals'
	OR RoleName = 'deactivateTerminals'
GO


/* Fix a "lost" permission category from a previous category change */
UPDATE aspnet_Roles
SET Category = 'Assets - Resources - Drivers'
	, Description = 'Allow user to deactivate Drivers'
WHERE RoleName = 'deactivateDriverMaintenance'
GO


COMMIT
SET NOEXEC OFF