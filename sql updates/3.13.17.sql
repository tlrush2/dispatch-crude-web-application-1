-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.13.16'
SELECT  @NewVersion = '3.13.17'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1651: Add system setting for displaying driver locations (hrs)'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


INSERT INTO tblSetting (ID, Category, Name, SettingTypeID, Value, CreateDateUTC, CreatedByUser) 
	VALUES (63, 'Order Entry Rules', 'Recent driver locations limit (hrs)', 3,
		'2'
		, getutcdate(), 'System')
GO

COMMIT
SET NOEXEC OFF