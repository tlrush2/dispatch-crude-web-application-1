/* fix the Accessorial Rate Clone stored procedures (to update all relevant values)
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '1.6.5', @NewVersion = '1.6.6'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 1 Jun 2013
-- Description:	provide a way to "clone" an existing set of Accessorial rates
-- =============================================
ALTER PROCEDURE [dbo].[spCloneCarrierAccessorialRates](@ID int, @UserName varchar(100)) AS
BEGIN
	DELETE FROM tblCarrierRates WHERE CarrierID IS NULL AND RegionID IS NULL
	INSERT INTO tblCarrierRates (CarrierID, RegionID, ChainupFee, WaitFee, RejectionFee, RerouteFee, H2SRate, FuelSurcharge
		, WaitFeeSubUnitID, WaitFeeRoundingTypeID, CreateDate, CreatedByUser)
		
		SELECT 0, 0, ChainupFee, WaitFee, RejectionFee, RerouteFee, H2SRate, FuelSurcharge
			, WaitFeeSubUnitID, WaitFeeRoundingTypeID, GETDATE(), @UserName
		FROM tblCarrierRates WHERE ID = @ID
END

GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 1 Jun 2013
-- Description:	provide a way to "clone" an existing set of Accessorial rates
-- =============================================
ALTER PROCEDURE [dbo].[spCloneCustomerAccessorialRates](@ID int, @UserName varchar(100)) AS
BEGIN
	DELETE FROM tblCustomerRates WHERE CustomerID IS NULL AND RegionID IS NULL
	INSERT INTO tblCustomerRates (CustomerID, RegionID, ChainupFee, WaitFee, RejectionFee, RerouteFee, H2SRate, FuelSurcharge
		, WaitFeeSubUnitID, WaitFeeRoundingTypeID, CreateDate, CreatedByUser)
		
		SELECT 0, 0, ChainupFee, WaitFee, RejectionFee, RerouteFee, H2SRate, FuelSurcharge
			, WaitFeeSubUnitID, WaitFeeRoundingTypeID, GETDATE(), @UserName
		FROM tblCustomerRates WHERE ID = @ID
END

GO

COMMIT
SET NOEXEC OFF