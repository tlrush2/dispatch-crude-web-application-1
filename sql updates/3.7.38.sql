-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.7.37.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.7.37.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0

DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.7.37'
SELECT  @NewVersion = '3.7.38'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Report Center: Added "Shipper Ticket Type" column.'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/*
	BB 6/29/15 - DCWEB-797: Add "custom shipper ticket type" table
*/
CREATE TABLE tblShipperTicketType (
ID INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_ShipperTicketType PRIMARY KEY,
CustomerID INT NOT NULL CONSTRAINT FK_ShipperTicketType_Customer REFERENCES tblCustomer(ID),
TicketTypeID INT NOT NULL CONSTRAINT FK_ShipperTicketType_TicketType REFERENCES tblTicketType(ID),
TicketType VARCHAR(20) NOT NULL 
)
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblShipperTicketType TO role_iis_acct
GO
CREATE UNIQUE INDEX udxShipperTicketType_Unique ON tblShipperTicketType(CustomerID, TicketTypeID)
GO



/****** Object:  View [dbo].[viewReportCenter_Orders]    Script Date: 06/29/2015 08:45:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Report Center Order data
-- Changes:
   -- 05/21/15 GSM Adding Gauger Process data elements to Report Center
   -- 06/08/15 BB - Add "OriginCity" and "OriginCityState" columns			
   -- 06/17/15 BB - Add "DestCity" and "DestCityState" columns
   -- 06/29/15 BB - Add "Shipper Ticket Type" column
/***********************************/
ALTER VIEW [dbo].[viewReportCenter_Orders] AS
	SELECT O.*
		, ShipperBatchNum = SS.BatchNum
		, ShipperBatchInvoiceNum = SSB.InvoiceNum
		, ShipperSettlementUomID = SS.SettlementUomID
		, ShipperSettlementUom = SS.SettlementUom
		, ShipperMinSettlementUnits = SS.MinSettlementUnits
		, ShipperSettlementUnits = SS.SettlementUnits
		, ShipperRateSheetRate = SS.RateSheetRate
		, ShipperRateSheetRateType = SS.RateSheetRateType
		, ShipperRouteRate = SS.RouteRate
		, ShipperRouteRateType = SS.RouteRateType
		, ShipperLoadRate = isnull(SS.RouteRate, SS.RateSheetRate)
		, ShipperLoadRateType = isnull(SS.RouteRateType, SS.RateSheetRateType)
		, ShipperLoadAmount = SS.LoadAmount
		, ShipperOrderRejectRate = SS.OrderRejectRate 
		, ShipperOrderRejectRateType = SS.OrderRejectRateType 
		, ShipperOrderRejectAmount = SS.OrderRejectAmount 
		, ShipperWaitFeeSubUnit = SS.WaitFeeSubUnit  
		, ShipperWaitFeeRoundingType = SS.WaitFeeRoundingType  
		, ShipperOriginWaitBillableHours = SS.OriginWaitBillableHours  
		, ShipperOriginWaitBillableMinutes = SS.OriginWaitBillableMinutes  
		, ShipperOriginWaitRate = SS.OriginWaitRate
		, ShipperOriginWaitAmount = SS.OriginWaitAmount
		, ShipperDestinationWaitBillableHours = SS.DestinationWaitBillableHours 
		, ShipperDestinationWaitBillableMinutes = SS.DestinationWaitBillableMinutes   
		, ShipperDestinationWaitRate = SS.DestinationWaitRate  
		, ShipperDestinationWaitAmount = SS.DestinationWaitAmount  
		, ShipperTotalWaitAmount = SS.TotalWaitAmount
		, ShipperTotalWaitBillableMinutes = nullif(ISNULL(SS.OriginWaitBillableMinutes, 0) + ISNULL(SS.DestinationWaitBillableMinutes, 0), 0)
		, ShipperTotalWaitBillableHours = nullif(ISNULL(SS.OriginWaitBillableHours, 0) + ISNULL(SS.DestinationWaitBillableHours, 0), 0)		
		, ShipperFuelSurchargeRate = SS.FuelSurchargeRate
		, ShipperFuelSurchargeAmount = SS.FuelSurchargeAmount
		, ShipperChainupRate = SS.ChainupRate
		, ShipperChainupRateType = SS.ChainupRateType  
		, ShipperChainupAmount = SS.ChainupAmount
		, ShipperRerouteRate = SS.RerouteRate
		, ShipperRerouteRateType = SS.RerouteRateType  
		, ShipperRerouteAmount = SS.RerouteAmount
		, ShipperSplitLoadRate = SS.SplitLoadRate
		, ShipperSplitLoadRateType = SS.SplitLoadRateType  
		, ShipperSplitLoadAmount = SS.SplitLoadAmount
		, ShipperH2SRate = SS.H2SRate
		, ShipperH2SRateType = SS.H2SRateType  
		, ShipperH2SAmount = SS.H2SAmount
		, ShipperTaxRate = SS.OriginTaxRate
		, ShipperTotalAmount = SS.TotalAmount
		, ShipperDestCode = CDC.Code
		, ShipperTicketType = (SELECT STT.TicketType FROM tblShipperTicketType AS STT WHERE STT.CustomerID =  O.CustomerID AND STT.TicketTypeID = O.TicketTypeID) -- BB 6/29/15
		--
		, CarrierBatchNum = SC.BatchNum
		, CarrierSettlementUomID = SC.SettlementUomID
		, CarrierSettlementUom = SC.SettlementUom
		, CarrierMinSettlementUnits = SC.MinSettlementUnits
		, CarrierSettlementUnits = SC.SettlementUnits
		, CarrierRateSheetRate = SC.RateSheetRate
		, CarrierRateSheetRateType = SC.RateSheetRateType
		, CarrierRouteRate = SC.RouteRate
		, CarrierRouteRateType = SC.RouteRateType
		, CarrierLoadRate = isnull(SC.RouteRate, SC.RateSheetRate)
		, CarrierLoadRateType = isnull(SC.RouteRateType, SC.RateSheetRateType)
		, CarrierLoadAmount = SC.LoadAmount
		, CarrierOrderRejectRate = SC.OrderRejectRate 
		, CarrierOrderRejectRateType = SC.OrderRejectRateType 
		, CarrierOrderRejectAmount = SC.OrderRejectAmount 
		, CarrierWaitFeeSubUnit = SC.WaitFeeSubUnit  
		, CarrierWaitFeeRoundingType = SC.WaitFeeRoundingType  
		, CarrierOriginWaitBillableHours = SS.OriginWaitBillableHours  
		, CarrierOriginWaitBillableMinutes = SC.OriginWaitBillableMinutes  
		, CarrierOriginWaitRate = SC.OriginWaitRate
		, CarrierOriginWaitAmount = SC.OriginWaitAmount
		, CarrierDestinationWaitBillableHours = SS.DestinationWaitBillableHours 
		, CarrierDestinationWaitBillableMinutes = SC.DestinationWaitBillableMinutes  
		, CarrierDestinationWaitRate = SC.DestinationWaitRate 
		, CarrierDestinationWaitAmount = SC.DestinationWaitAmount  
		, CarrierTotalWaitAmount = SC.TotalWaitAmount
		, CarrierTotalWaitBillableMinutes = nullif(ISNULL(SS.OriginWaitBillableMinutes, 0) + ISNULL(SS.DestinationWaitBillableMinutes, 0), 0)
		, CarrierTotalWaitBillableHours = nullif(ISNULL(SS.OriginWaitBillableHours, 0) + ISNULL(SS.DestinationWaitBillableHours, 0), 0)		
		, CarrierFuelSurchargeRate = SC.FuelSurchargeRate
		, CarrierFuelSurchargeAmount = SC.FuelSurchargeAmount
		, CarrierChainupRate = SC.ChainupRate
		, CarrierChainupRateType = SC.ChainupRateType  
		, CarrierChainupAmount = SC.ChainupAmount
		, CarrierRerouteRate = SC.RerouteRate
		, CarrierRerouteRateType = SC.RerouteRateType  
		, CarrierRerouteAmount = SC.RerouteAmount
		, CarrierSplitLoadRate = SC.SplitLoadRate
		, CarrierSplitLoadRateType = SC.SplitLoadRateType  
		, CarrierSplitLoadAmount = SC.SplitLoadAmount
		, CarrierH2SRate = SC.H2SRate
		, CarrierH2SRateType = SC.H2SRateType  
		, CarrierH2SAmount = SC.H2SAmount
		, CarrierTaxRate = SC.OriginTaxRate
		, CarrierTotalAmount = SC.TotalAmount
		--
		, OriginGpsLatLon = ltrim(DLO.Lat) + ',' + ltrim(DLO.Lon)
		, OriginLatLon = ltrim(OO.LAT) + ',' + ltrim(OO.LON)
		, OriginAccuracyMeters = DLO.SourceAccuracyMeters
		, OriginDistance = isnull(cast(DLO.DistanceToPoint as int), 99999)
		, OriginGpsArrived = CASE WHEN DLO.DistanceToPoint IS NOT NULL AND DLO.DistanceToPoint BETWEEN 0 AND OO.GeoFenceRadiusMeters THEN 1 ELSE 0 END
		, OriginCTBNum = OO.CTBNum
		, OriginFieldName = OO.FieldName
		, OriginCity = OO.City																				
		, OriginCityState = OO.City + ', ' + (SELECT Abbreviation FROM tblState WHERE ID = OO.StateID)		
		--
		, DestGpsLatLon = ltrim(DLD.Lat) + ',' + ltrim(DLD.Lon)
		, DestLatLon = ltrim(D.LAT) + ',' + ltrim(D.LON)
		, DestAccuracyMeters = DLD.SourceAccuracyMeters
		, DestDistance = isnull(cast(DLD.DistanceToPoint as int), 99999)
		, DestGpsArrived = CASE WHEN DLD.DistanceToPoint IS NOT NULL AND DLD.DistanceToPoint BETWEEN 0 AND D.GeoFenceRadiusMeters THEN 1 ELSE 0 END		
		, DestCity = D.City
		, DestCityState = D.City + ', ' + (SELECT Abbreviation FROM tblState WHERE ID = OO.StateID)
		--
		, Gauger = GAO.Gauger						
		, GaugerIDNumber = GA.IDNumber
		, GaugerFirstName = GA.FirstName
		, GaugerLastName = GA.LastName
		, GaugerRejected = GAO.Rejected
		, GaugerRejectReasonID = GAO.RejectReasonID
		, GaugerRejectNotes = GAO.RejectNotes
		, GaugerRejectNumDesc = GORR.NumDesc
		, GaugerPrintDate = dbo.fnUTC_to_Local(GAO.PrintDateUTC, O.OriginTimeZoneID, O.OriginUseDST)
		--
		, T_GaugerCarrierTicketNum = CASE WHEN GAO.TicketCount = 0 THEN ltrim(GAO.OrderNum) + CASE WHEN GAO.Rejected = 1 THEN 'X' ELSE '' END ELSE GOT.CarrierTicketNum END 
		, T_GaugerTankNum = isnull(GOT.OriginTankText, GAO.OriginTankText)
		, T_GaugerIsStrappedTank = GOT.IsStrappedTank 
		, T_GaugerProductObsTemp = GOT.ProductObsTemp
		, T_GaugerProductObsGravity = GOT.ProductObsGravity
		, T_GaugerProductBSW = GOT.ProductBSW		
		, T_GaugerOpeningGaugeFeet = GOT.OpeningGaugeFeet
		, T_GaugerOpeningGaugeInch = GOT.OpeningGaugeInch		
		, T_GaugerOpeningGaugeQ = GOT.OpeningGaugeQ			
		, T_GaugerBottomFeet = GOT.BottomFeet
		, T_GaugerBottomInches = GOT.BottomInches		
		, T_GaugerBottomQ = GOT.BottomQ		
		, T_GaugerRejected = GOT.Rejected
		, T_GaugerRejectReasonID = GOT.RejectReasonID
		, T_GaugerRejectNumDesc = GOT.RejectNumDesc
		, T_GaugerRejectNotes = GOT.RejectNotes			
	FROM viewOrder_OrderTicket_Full O
	JOIN tblOrigin OO ON OO.ID = O.OriginID
	JOIN tblDestination D ON D.ID = O.DestinationID
	--
    LEFT JOIN viewGaugerOrder GAO ON GAO.OrderID = O.ID			            
    LEFT JOIN viewGaugerOrderTicket GOT ON GOT.UID = O.T_UID	            
    LEFT JOIN viewGauger GA ON GA.ID = GAO.GaugerID				            
    LEFT JOIN viewOrderRejectReason GORR ON GORR.ID = GAO.RejectReasonID 
    --
    LEFT JOIN tblCustomerDestinationCode CDC ON CDC.CustomerID = O.CustomerID AND CDC.DestinationID = O.DestinationID
	LEFT JOIN viewDriverLocation_OriginFirstArrive DLO ON DLO.OrderID = O.ID AND DLO.OriginID = O.OriginID
	LEFT JOIN viewDriverLocation_DestinationFirstArrive DLD ON DLD.OrderID = O.ID AND DLD.DestinationID = O.DestinationID
	LEFT JOIN viewOrderSettlementCarrier SC ON SC.OrderID = O.ID
	LEFT JOIN viewOrderSettlementShipper SS ON SS.OrderID = O.ID
	LEFT JOIN tblShipperSettlementBatch SSB ON SSB.ID = SS.BatchID
	LEFT JOIN tblShipperTicketType AS STT ON STT.TicketTypeID = OO.TicketTypeID		-- BB 6/29/15
GO


/*
	BB 6/29/15 - Add Shipper Ticket Type column to report center
*/
SET IDENTITY_INSERT tblReportColumnDefinition ON
  INSERT tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport)
  SELECT 274,1,'ShipperTicketType','TICKET | GENERAL | Shipper Ticket Type',NULL,NULL,0,NULL,1,'*',0  
SET IDENTITY_INSERT tblReportColumnDefinition OFF
GO


COMMIT
SET NOEXEC OFF