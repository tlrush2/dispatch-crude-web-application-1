-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.9.26.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.9.26.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0

SET NOEXEC OFF
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.9.26'
SELECT  @NewVersion = '3.9.27'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Add logic to origin signature sync to always get transfers'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/*******************************************
-- Date Created: 2015/09/01
-- Author: Kevin Alons
-- Purpose: return Signature records for the specified driver (that are still relevant at this time)
-- Changes: 
    - 3.9.27 - 2015/11/10 - KDA+JAE - return the Origin Signatures for a Transferred Order (since the origin signatures weren't entered by this driver)
*******************************************/
ALTER PROCEDURE spOrderSignatures_DriverApp(@DriverID int, @LastChangeDateUTC datetime = NULL) AS
BEGIN
    SELECT OS.ID, OS.UID, OS.DriverID, OS.OrderID, OS.OriginID, OS.DestinationID, OS.SignatureBlob, OS.CreateDateUTC, OS.CreatedByUser
    FROM dbo.tblOrder O
    JOIN dbo.tblOrderSignature OS ON OS.OrderID = O.ID
    LEFT JOIN tblOrderTransfer OTR ON OTR.OrderID = O.ID
    CROSS JOIN dbo.fnSyncLCDOffset(@LastChangeDateUTC) LCD
    -- all orders are being fully resent to the client (otherwise the client already sent us this record and won't change on the server so don't emit back)
    WHERE 
        /* always include all signatures if a NULL @LastChangeDateUTC is specified */
        (@LastChangeDateUTC IS NULL 
            /* or ensure the Origin Signatures are returned if this is a Transferred order */
            OR (OTR.OrderID IS NOT NULL AND OS.OriginID IS NOT NULL)
        )
        AND O.DriverID = @driverID 
        AND O.StatusID IN (2,7,8,3) 
        AND (
            O.CreateDateUTC >= LCD.LCD
            OR O.LastChangeDateUTC >= LCD.LCD
            OR O.AcceptLastChangeDateUTC >= LCD.LCD
            OR O.PickupLastChangeDateUTC >= LCD.LCD
            OR O.DeliverLastChangeDateUTC >= LCD.LCD
            OR OS.CreateDateUTC >= LCD.LCD
            OR OTR.CreateDateUTC >= LCD.LCD
            )
END

GO


COMMIT
SET NOEXEC OFF
