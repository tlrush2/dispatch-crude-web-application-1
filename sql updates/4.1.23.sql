SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.1.22'
SELECT  @NewVersion = '4.1.23'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1862: Add weight units to settlment pages'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO



SET IDENTITY_INSERT tblUom ON
  
INSERT INTO tblUom
	(ID, Name, Abbrev, GallonEquivalent, CreatedByUser, UomTypeID, SignificantDigits)
SELECT 4, 'Litre', 'LTR', 0.2642, 'System', 1, 0
EXCEPT SELECT ID, Name, Abbrev, GallonEquivalent, 'System', UomTypeID, SignificantDigits from tblUom
  
INSERT INTO tblUom
	(ID, Name, Abbrev, GallonEquivalent, CreatedByUser, UomTypeID, SignificantDigits)
SELECT 7, 'Metric Tonne', 'Tonnes', 2204.6, 'System', 2, 3
EXCEPT SELECT ID, Name, Abbrev, GallonEquivalent, 'System', UomTypeID, SignificantDigits from tblUom

SET IDENTITY_INSERT tblUom OFF

UPDATE tblUom SET CreatedByUser = 'System'


GO
INSERT INTO tblSettlementFactor
VALUES
  (6, 'Origin Net Weight', 'OriginWeightNetUnits'),
  (7, 'Dest Net Weight', 'DestWeightNetUnits')

GO



/**************************************************/
-- Created: 2016/08/15 - 4.1.0 - Kevin Alons
-- Purpose: return the order settlement units (based on the specified settlementFactorID parameter value)
-- Changes:
--		4.1.23		2016/10/17		JAE			Add weight units to order settlement
/**************************************************/
ALTER FUNCTION fnOrderSettlementUnits
(
  @SettlementFactorID int
, @OriginGrossUnits decimal(9, 3)
, @OriginNetUnits decimal(9, 3)
, @OriginGrossStdUnits decimal(9, 3)
, @DestGrossUnits decimal(9, 3)
, @DestNetUnits decimal(9, 3)
, @OriginNetWeight decimal(9,3)
, @DestNetWeight decimal(9,3)
) RETURNS money AS
BEGIN
	DECLARE @ret money = 0
	
	SELECT @ret = CASE @SettlementFactorID 
		WHEN 1 THEN @OriginGrossUnits
		WHEN 2 THEN @OriginNetUnits
		WHEN 3 THEN @OriginGrossStdUnits
		WHEN 4 THEN	@DestGrossUnits
		WHEN 5 THEN @DestNetUnits 
		WHEN 6 THEN @OriginNetWeight
		WHEN 7 THEN @DestNetWeight
				ELSE NULL END

	RETURN (@ret)
END

GO


/***********************************
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Carrier "Settlement" $$ values to an Delivered/Audited order
-- Changes:
	- 3.7.40 - 2015/06/30 - KDA - use specific SettlementFactor (not fall thru to less precise if specified one is missing)
								- use the DestinationUomID for the Settlement Uom if using a Destination Settlement Factor
	- 3.9.0 - 2015/08/16 - KDA  - use OUTER APPLY for SettlementUnits/MinSettlementUnits
	- 3.9.19.13 - 2015/08/16 - KDA  - fix typo that used Destination UOM as settlement UOM due to use of MSF.ID instead of correct MSF.SettlementFactorID in logic
	- 3.9.37  - 2015/12/22 - JAE - Propagate Min Settlement UOM for display on approval page
	- 4.1.23  - 2016/10/17 - JAE - Add weight units to order settlement
***********************************/
ALTER VIEW [dbo].[viewOrderSettlementUnitsCarrier] AS
	SELECT X3.*
		, SettlementUnits = CASE WHEN X3.ActualUnits IS NULL THEN 0 ELSE dbo.fnMaxDecimal(X3.ActualUnits, MinSettlementUnits) END
	FROM (
		SELECT OrderID
			, CarrierID
			, SettlementUomID
			, CarrierSettlementFactorID
			, SettlementFactorID
			, MinSettlementUnits = dbo.fnMinDecimal(isnull(X2.WRMSVUnits, X2.MinSettlementUnits), X2.MinSettlementUnits)
			, MinSettlementUnitsID
			, MinSettlementUomID
			, ActualUnits
		FROM (
			SELECT OrderID
				, CarrierID
				, SettlementUomID
				, CarrierSettlementFactorID
				, SettlementFactorID
				, WRMSVUnits = dbo.fnConvertUOM(WRMSVUnits, WRMSVUomID, SettlementUomID)
				, MinSettlementUnits = dbo.fnConvertUOM(MinSettlementUnits, MinSettlementUomID, SettlementUomID)
				, MinSettlementUomID
				, MinSettlementUnitsID
				, ActualUnits 
			FROM (
				SELECT OrderID = O.ID
					, O.CarrierID
					, SettlementUomID = CASE WHEN MSF.SettlementFactorID IN (1,2,3) THEN O.OriginUomID ELSE O.DestUomID END
					, CarrierSettlementFactorID = MSF.ID
					, MSF.SettlementFactorID
					, R.WRMSVUomID
					, R.WRMSVUnits
					, MinSettlementUnitsID = MSU.ID
					, MinSettlementUomID = MSU.UomID
					, MinSettlementUnits = isnull(MSU.MinSettlementUnits, 0) 
					, ActualUnits = CASE MSF.SettlementFactorID
							WHEN 1 THEN O.OriginGrossUnits 
							WHEN 3 THEN O.OriginGrossStdUnits 
							WHEN 2 THEN O.OriginNetUnits 
							WHEN 4 THEN O.DestGrossUnits
							WHEN 5 THEN O.DestNetUnits
							WHEN 6 THEN O.OriginWeightNetUnits
							WHEN 7 THEN O.DestWeightNetUnits END
				FROM dbo.tblOrder O
				OUTER APPLY dbo.fnOrderCarrierSettlementFactor(O.ID) MSF
				OUTER APPLY dbo.fnOrderCarrierMinSettlementUnits(O.ID) MSU 
				JOIN dbo.tblRoute R ON R.ID = O.RouteID
				JOIN tblCarrier C ON C.ID = O.CarrierID
			) X
		) X2
	) X3



GO

/***********************************
-- Date Created: 4/1/2016
-- Author: Joe Engler
-- Purpose: return Order JOIN OrderTicket + FINANCIAL INFO into a single view
-- Changes:
	- 4.1.23   - 2016/10/17 - JAE - Add weight units to order settlement
***********************************/
ALTER VIEW [dbo].[viewOrderSettlementUnitsProducer] AS
	SELECT OrderID
		 , CarrierID
		 , SettlementUomID
		 , SettlementFactorID
		 , SettlementUnits = CASE WHEN SettlementUnits IS NULL THEN 0 ELSE SettlementUnits END
		 , CommodityPurchasePricebookID
	FROM (
			SELECT OrderID = O.ID
				, O.CarrierID
				, SettlementUomID = CASE WHEN CPP.SettlementFactorID IN (1,2,3) THEN O.OriginUomID ELSE O.DestUomID END
				, CPP.SettlementFactorID
				, SettlementUnits = CASE CPP.SettlementFactorID
							WHEN 1 THEN O.OriginGrossUnits 
							WHEN 3 THEN O.OriginGrossStdUnits 
							WHEN 2 THEN O.OriginNetUnits 
							WHEN 4 THEN O.DestGrossUnits
							WHEN 5 THEN O.DestNetUnits
							WHEN 6 THEN O.OriginWeightNetUnits
							WHEN 7 THEN O.DestWeightNetUnits END
				, CommodityPurchasePricebookID = CPP.ID
			FROM dbo.tblOrder O
			OUTER APPLY dbo.fnOrderProducerPurchasePricebook(O.ID) OPPP
			LEFT JOIN tblCommodityPurchasePricebook CPP ON CPP.ID = OPPP.ID
--			JOIN tblCarrier C ON C.ID = O.CarrierID
	) X


GO

/***********************************
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Shipper "Settlement" $$ values to an Delivered/Audited order
-- Changes:
	- 3.7.40 - 2015/06/30 - KDA - use specific SettlementFactor (not fall thru to less precise if specified one is missing)
								- use the DestinationUomID for the Settlement Uom if using a Destination Settlement Factor
	- 3.9.0 - 2015/08/16 - KDA  - use OUTER APPLY for SettlementUnits/MinSettlementUnits
	- 3.9.19.13 - 2015/08/16 - KDA  - fix typo that used Destination UOM as settlement UOM due to use of MSF.ID instead of correct MSF.SettlementFactorID in logic
	- 3.9.37  - 2015/12/22 - JAE - Propagate Min Settlement UOM for display on approval page
	- 4.1.23  - 2016/10/17 - JAE - Add weight units to order settlement
***********************************/
ALTER VIEW [dbo].[viewOrderSettlementUnitsShipper] AS
	SELECT X3.*
		, SettlementUnits = CASE WHEN X3.ActualUnits IS NULL THEN 0 ELSE dbo.fnMaxDecimal(X3.ActualUnits, MinSettlementUnits) END
	FROM (
		SELECT OrderID
			, ShipperID
			, SettlementUomID
			, ShipperSettlementFactorID
			, SettlementFactorID
			, MinSettlementUnits = dbo.fnMinDecimal(isnull(X2.WRMSVUnits, X2.MinSettlementUnits), X2.MinSettlementUnits)
			, MinSettlementUnitsID
			, MinSettlementUomID
			, ActualUnits
		FROM (
			SELECT OrderID
				, ShipperID
				, SettlementUomID
				, ShipperSettlementFactorID
				, SettlementFactorID
				, WRMSVUnits = dbo.fnConvertUOM(WRMSVUnits, WRMSVUomID, SettlementUomID)
				, MinSettlementUnits = dbo.fnConvertUOM(MinSettlementUnits, MinSettlementUomID, SettlementUomID)
				, MinSettlementUnitsID
				, MinSettlementUomID
				, ActualUnits 
			FROM (
				SELECT OrderID = O.ID
					, ShipperID = O.CustomerID
					, SettlementUomID = CASE WHEN MSF.SettlementFactorID IN (1,2,3) THEN O.OriginUomID ELSE O.DestUomID END
					, ShipperSettlementFactorID = MSF.ID
					, MSF.SettlementFactorID
					, R.WRMSVUomID
					, R.WRMSVUnits
					, MinSettlementUnitsID = MSU.ID
					, MinSettlementUomID = MSU.UomID
					, MinSettlementUnits = isnull(MSU.MinSettlementUnits, 0) 
					, ActualUnits = CASE MSF.SettlementFactorID
							WHEN 1 THEN O.OriginGrossUnits 
							WHEN 3 THEN O.OriginGrossStdUnits 
							WHEN 2 THEN O.OriginNetUnits 
							WHEN 4 THEN O.DestGrossUnits
							WHEN 5 THEN O.DestNetUnits
							WHEN 6 THEN O.OriginWeightNetUnits
							WHEN 7 THEN O.DestWeightNetUnits END
				FROM dbo.tblOrder O
				OUTER APPLY dbo.fnOrderShipperSettlementFactor(O.ID) MSF
				OUTER APPLY dbo.fnOrderShipperMinSettlementUnits(O.ID) MSU 
				JOIN dbo.tblRoute R ON R.ID = O.RouteID
				JOIN tblCustomer C ON C.ID = O.CustomerID
			) X
		) X2
	) X3


GO




  
/***********************************/
-- Created: 4.1.0 - 2016/08/15 - Kevin Alons
-- Purpose: return BASE Order details + Carrier FINANCIAL INFO into a single view
-- Changes:
-- 4.1.0.2 - 2016.08.28 - KDA	- remove redundant calculation of ORR.RerouteCount
--								- remove redundant tblOrigin JOIN (for H2S which is already available in viewOrder)
-- 4.1.8.2 - 2016.09.21 - KDA	- Check Final Actual miles (null actual miles ok as long as override)
-- 4.1.23	 2016.10.17 - JAE	- Add weight units for settlement
/***********************************/
ALTER VIEW [dbo].[viewOrder_Financial_Base_Carrier] AS 
	SELECT *
		, HasError = cast(CASE WHEN ErrorFieldCSV IS NOT NULL THEN 1 ELSE 0 END as bit)
	FROM (
		SELECT *
			, OrderID = ID
			, ErrorFieldCSV = nullif(substring(CASE WHEN Approved = 0 THEN ',Approved' ELSE '' END
				+ CASE WHEN InvoiceLoadAmount IS NULL THEN ',InvoiceLoadAmount,InvoiceRouteRate,InvoiceRateSheet' ELSE '' END
				+ CASE WHEN FinalChainup = 1 AND InvoiceChainupAmount IS NULL THEN ',InvoiceChainupAmount' ELSE '' END
				+ CASE WHEN FinalRerouteCount > 0 AND InvoiceRerouteAmount IS NULL THEN ',InvoiceRerouteAmount' ELSE '' END
				+ CASE WHEN FinalH2S = 1 AND InvoiceH2SAmount IS NULL THEN ',InvoiceH2SAmount' ELSE '' END
				+ CASE WHEN TicketCount > 1 AND InvoiceSplitLoadAmount IS NULL THEN ',InvoiceSplitLoadAmount' ELSE '' END
				+ CASE WHEN InvoiceOriginWaitBillableMinutes > 0 AND InvoiceOriginWaitAmount IS NULL THEN ',InvoiceOriginWaitAmount' ELSE '' END
				+ CASE WHEN InvoiceDestinationWaitBillableMinutes > 0 AND InvoiceDestinationWaitAmount IS NULL THEN ',InvoiceDestinationWaitAmount' ELSE '' END
				+ CASE WHEN Rejected = 1 AND InvoiceOrderRejectAmount IS NULL THEN ',InvoiceOrderRejectAmount' ELSE '' END
				+ CASE WHEN InvoiceMinSettlementUnitsID IS NULL THEN ',InvoiceMinSettlementUnits' ELSE '' END
				+ CASE WHEN InvoiceSettlementFactorID IS NULL THEN ',InvoiceSettlementFactor' ELSE '' END
				+ CASE WHEN Rejected = 0 AND InvoiceSettlementFactorID IS NULL THEN ',InvoiceSettlementFactor' ELSE '' END
				+ CASE WHEN Rejected = 0 AND InvoiceSettlementFactorID IS NOT NULL AND dbo.fnOrderSettlementUnits(InvoiceSettlementFactorID, OriginGrossUnits, OriginNetUnits, OriginGrossStdUnits, DestGrossUnits, DestNetUnits, OriginWeightNetUnits, DestWeightNetUnits) IS NULL THEN ','+OrderUnitsField ELSE '' END
				+ CASE WHEN InvoiceWaitFeeParameterID IS NULL THEN ',InvoiceOriginWaitBillableMinutes,InvoiceDestinationWaitBillableMinutes' ELSE '' END
				+ CASE WHEN isnull(FinalActualMiles, 0) = 0 THEN ',ActualMiles' ELSE '' END, 2, 100000), '')
		FROM (
			SELECT O.* 
				, InvoiceRatesAppliedDateUTC = OS.CreateDateUTC
				, OS.BatchID
				, Approved = cast(ISNULL(OA.Approved, 0) as bit)
				, FinalActualMiles = ISNULL(OA.OverrideActualMiles, O.ActualMiles)
				, FinalOriginMinutes = ISNULL(OA.OverrideOriginMinutes, O.OriginMinutes)
				, FinalDestMinutes = ISNULL(OA.OverrideDestMinutes, O.DestMinutes)
				, FinalH2S = cast(CASE WHEN isnull(OA.OverrideH2S, 0) = 1 THEN 0 ELSE O.H2S END as bit)
				, FinalChainup = cast(CASE WHEN isnull(OA.OverrideChainup, 0) = 1 THEN 0 ELSE O.Chainup END as bit)
				, FinalRerouteCount = CASE WHEN isnull(OA.OverrideReroute, 0) = 1 THEN 0 ELSE isnull(O.RerouteCount, 0) END
				, FC.OrderUnitsField
				, InvoiceBatchNum = OS.BatchNum 
				, InvoiceOriginWaitBillableMinutes = OS.OriginWaitBillableMinutes
				, InvoiceDestinationWaitBillableMinutes = OS.DestinationWaitBillableMinutes
				, InvoiceTotalWaitBillableMinutes = isnull(OS.OriginWaitBillableMinutes, 0) + ISNULL(OS.DestinationWaitBillableMinutes, 0) 
				, InvoiceWaitFeeParameterID = WaitFeeParameterID
				, InvoiceWaitFeeSubUnit = WaitFeeSubUnit
				, InvoiceWaitFeeRoundingType = WaitFeeRoundingType
				, InvoiceOriginWaitRate = OS.OriginWaitRate 
				, InvoiceOriginWaitAmount = OS.OriginWaitAmount 		
				, InvoiceDestinationWaitRate = OS.DestinationWaitRate 
				, InvoiceDestinationWaitAmount = OS.DestinationWaitAmount 
				, InvoiceTotalWaitAmount = OS.TotalWaitAmount
				, InvoiceOrderRejectRate = OS.OrderRejectRate	
				, InvoiceOrderRejectRateType = OS.OrderRejectRateType  
				, InvoiceOrderRejectAmount = OS.OrderRejectAmount  
				, InvoiceChainupRate = OS.ChainupRate
				, InvoiceChainupRateType = OS.ChainupRateType
				, InvoiceChainupAmount = OS.ChainupAmount 
				, InvoiceRerouteRate = OS.RerouteRate
				, InvoiceRerouteRateType = OS.RerouteRateType
				, InvoiceRerouteAmount = OS.RerouteAmount 
				, InvoiceH2SRate = OS.H2SRate
				, InvoiceH2SRateType = OS.H2SRateType
				, InvoiceH2SAmount = OS.H2SAmount
				, InvoiceSplitLoadRate = OS.SplitLoadRate
				, InvoiceSplitLoadRateType = OS.SplitLoadRateType
				, InvoiceSplitLoadAmount = OS.SplitLoadAmount
				, InvoiceOtherAmount = OS.OtherAmount
				, InvoiceTaxRate = OS.OriginTaxRate
				, InvoiceSettlementUom = OS.SettlementUom 
				, InvoiceSettlementUomShort = OS.SettlementUomShort 
				, InvoiceCarrierSettlementFactorID = OS.CarrierSettlementFactorID 
				, InvoiceSettlementFactorID = OS.SettlementFactorID 
				, InvoiceSettlementFactor = OS.SettlementFactor 
				, InvoiceMinSettlementUnitsID = OS.MinSettlementUnitsID 
				, InvoiceMinSettlementUnits = OS.MinSettlementUnits
				, InvoiceUnits = OS.SettlementUnits
				, InvoiceRouteRate = OS.RouteRate
				, InvoiceRouteRateType = OS.RouteRateType
				, InvoiceRateSheetRate = OS.RateSheetRate
				, InvoiceRateSheetRateType = OS.RateSheetRateType
				, InvoiceFuelSurchargeRate = OS.FuelSurchargeRate
				, InvoiceFuelSurchargeAmount = OS.FuelSurchargeAmount
				, InvoiceLoadAmount = OS.LoadAmount
				, InvoiceTotalAmount = OS.TotalAmount
			FROM dbo.viewOrder O
			LEFT JOIN viewOrderSettlementCarrier OS ON OS.OrderID = O.ID
			LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
			LEFT JOIN tblSettlementFactor FC ON FC.ID = OS.SettlementFactorID
			WHERE O.StatusID NOT IN (-10) -- don't include "Generated" orders (only Assigned+)
		) X
	) X1


GO


/***********************************/
-- Created: 4.1.0 - 2016/08/15 - Kevin Alons
-- Purpose: return BASE Order details + Driver FINANCIAL INFO into a single view
-- Changes:
-- 4.1.0.2 - 2016.08.28 - KDA	- remove redundant calculation of ORR.RerouteCount
--								- remove redundant tblOrigin JOIN (for H2S which is already available in viewOrder)
-- 4.1.8.2 - 2016.09.21 - JAE	- Check Final Actual miles (null actual miles ok as long as override)
-- 4.1.23	 2016.10.17 - JAE	- Add weight units for settlement
/***********************************/
ALTER VIEW [dbo].[viewOrder_Financial_Base_Driver] AS 
	SELECT *
		, HasError = cast(CASE WHEN ErrorFieldCSV IS NOT NULL THEN 1 ELSE 0 END as bit)
	FROM (
		SELECT *
			, OrderID = ID
			, ErrorFieldCSV = nullif(substring(CASE WHEN Approved = 0 THEN ',Approved' ELSE '' END
				+ CASE WHEN InvoiceLoadAmount IS NULL THEN ',InvoiceLoadAmount,InvoiceRouteRate,InvoiceRateSheet' ELSE '' END
				+ CASE WHEN FinalChainup = 1 AND InvoiceChainupAmount IS NULL THEN ',InvoiceChainupAmount' ELSE '' END
				+ CASE WHEN FinalRerouteCount > 0 AND InvoiceRerouteAmount IS NULL THEN ',InvoiceRerouteAmount' ELSE '' END
				+ CASE WHEN FinalH2S = 1 AND InvoiceH2SAmount IS NULL THEN ',InvoiceH2SAmount' ELSE '' END
				+ CASE WHEN TicketCount > 1 AND InvoiceSplitLoadAmount IS NULL THEN ',InvoiceSplitLoadAmount' ELSE '' END
				+ CASE WHEN InvoiceOriginWaitBillableMinutes > 0 AND InvoiceOriginWaitAmount IS NULL THEN ',InvoiceOriginWaitAmount' ELSE '' END
				+ CASE WHEN InvoiceDestinationWaitBillableMinutes > 0 AND InvoiceDestinationWaitAmount IS NULL THEN ',InvoiceDestinationWaitAmount' ELSE '' END
				+ CASE WHEN Rejected = 1 AND InvoiceOrderRejectAmount IS NULL THEN ',InvoiceOrderRejectAmount' ELSE '' END
				+ CASE WHEN InvoiceMinSettlementUnitsID IS NULL THEN ',InvoiceMinSettlementUnits' ELSE '' END
				+ CASE WHEN InvoiceSettlementFactorID IS NULL THEN ',InvoiceSettlementFactor' ELSE '' END
				+ CASE WHEN Rejected = 0 AND InvoiceSettlementFactorID IS NULL THEN ',InvoiceSettlementFactor' ELSE '' END
				+ CASE WHEN Rejected = 0 AND InvoiceSettlementFactorID IS NOT NULL AND dbo.fnOrderSettlementUnits(InvoiceSettlementFactorID, OriginGrossUnits, OriginNetUnits, OriginGrossStdUnits, DestGrossUnits, DestNetUnits, OriginWeightNetUnits, DestWeightNetUnits) IS NULL THEN ','+OrderUnitsField ELSE '' END
				+ CASE WHEN InvoiceWaitFeeParameterID IS NULL THEN ',InvoiceOriginWaitBillableMinutes,InvoiceDestinationWaitBillableMinutes' ELSE '' END
				+ CASE WHEN isnull(FinalActualMiles, 0) = 0 THEN ',ActualMiles' ELSE '' END, 2, 100000), '')
		FROM (
			SELECT O.* 
				, DriverGroup = DG.Name
				, InvoiceRatesAppliedDateUTC = OS.CreateDateUTC
				, OS.BatchID
				, Approved = cast(ISNULL(OA.Approved, 0) as bit)
				, FinalActualMiles = ISNULL(OA.OverrideActualMiles, O.ActualMiles)
				, FinalOriginMinutes = ISNULL(OA.OverrideOriginMinutes, O.OriginMinutes)
				, FinalDestMinutes = ISNULL(OA.OverrideDestMinutes, O.DestMinutes)
				, FinalH2S = cast(CASE WHEN isnull(OA.OverrideH2S, 0) = 1 THEN 0 ELSE O.H2S END as bit)
				, FinalChainup = cast(CASE WHEN isnull(OA.OverrideChainup, 0) = 1 THEN 0 ELSE O.Chainup END as bit)
				, FinalRerouteCount = CASE WHEN isnull(OA.OverrideReroute, 0) = 1 THEN 0 ELSE isnull(O.RerouteCount, 0) END
				, FC.OrderUnitsField
				, InvoiceBatchNum = OS.BatchNum 
				, InvoiceOriginWaitBillableMinutes = OS.OriginWaitBillableMinutes
				, InvoiceDestinationWaitBillableMinutes = OS.DestinationWaitBillableMinutes
				, InvoiceTotalWaitBillableMinutes = isnull(OS.OriginWaitBillableMinutes, 0) + ISNULL(OS.DestinationWaitBillableMinutes, 0) 
				, InvoiceWaitFeeParameterID = WaitFeeParameterID
				, InvoiceWaitFeeSubUnit = WaitFeeSubUnit
				, InvoiceWaitFeeRoundingType = WaitFeeRoundingType
				, InvoiceOriginWaitRate = OS.OriginWaitRate 
				, InvoiceOriginWaitAmount = OS.OriginWaitAmount 		
				, InvoiceDestinationWaitRate = OS.DestinationWaitRate 
				, InvoiceDestinationWaitAmount = OS.DestinationWaitAmount 
				, InvoiceTotalWaitAmount = OS.TotalWaitAmount
				, InvoiceOrderRejectRate = OS.OrderRejectRate	
				, InvoiceOrderRejectRateType = OS.OrderRejectRateType  
				, InvoiceOrderRejectAmount = OS.OrderRejectAmount  
				, InvoiceChainupRate = OS.ChainupRate
				, InvoiceChainupRateType = OS.ChainupRateType
				, InvoiceChainupAmount = OS.ChainupAmount 
				, InvoiceRerouteRate = OS.RerouteRate
				, InvoiceRerouteRateType = OS.RerouteRateType
				, InvoiceRerouteAmount = OS.RerouteAmount 
				, InvoiceH2SRate = OS.H2SRate
				, InvoiceH2SRateType = OS.H2SRateType
				, InvoiceH2SAmount = OS.H2SAmount
				, InvoiceSplitLoadRate = OS.SplitLoadRate
				, InvoiceSplitLoadRateType = OS.SplitLoadRateType
				, InvoiceSplitLoadAmount = OS.SplitLoadAmount
				, InvoiceOtherAmount = OS.OtherAmount
				, InvoiceTaxRate = OS.OriginTaxRate
				, InvoiceSettlementUom = OS.SettlementUom 
				, InvoiceSettlementUomShort = OS.SettlementUomShort 
				, InvoiceCarrierSettlementFactorID = OS.CarrierSettlementFactorID 
				, InvoiceSettlementFactorID = OS.SettlementFactorID 
				, InvoiceSettlementFactor = OS.SettlementFactor 
				, InvoiceMinSettlementUnitsID = OS.MinSettlementUnitsID 
				, InvoiceMinSettlementUnits = OS.MinSettlementUnits
				, InvoiceUnits = OS.SettlementUnits
				, InvoiceRouteRate = OS.RouteRate
				, InvoiceRouteRateType = OS.RouteRateType
				, InvoiceRateSheetRate = OS.RateSheetRate
				, InvoiceRateSheetRateType = OS.RateSheetRateType
				, InvoiceFuelSurchargeRate = OS.FuelSurchargeRate
				, InvoiceFuelSurchargeAmount = OS.FuelSurchargeAmount
				, InvoiceLoadAmount = OS.LoadAmount
				, InvoiceTotalAmount = OS.TotalAmount
			FROM dbo.viewOrder O
			LEFT JOIN tblDriverGroup DG ON DG.ID = O.DriverGroupID
			LEFT JOIN viewOrderSettlementDriver OS ON OS.OrderID = O.ID
			LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
			LEFT JOIN tblSettlementFactor FC ON FC.ID = OS.SettlementFactorID
			WHERE O.StatusID NOT IN (-10) -- don't include "Generated" orders (only Assigned+)
		) X
	) X1


GO



/***********************************/
-- Created: 4.1.0 - 2016/08/15 - Kevin Alons
-- Purpose: return BASE Order details + Shipper FINANCIAL INFO into a single view
-- Changes:
--	4.1.0.2 - 2016.08.28 - KDA	- remove redundant calculation of ORR.RerouteCount
--								- remove redundant tblOrigin JOIN (for H2S which is already available in viewOrder)
--	4.1.8.2 - 2016.09.21 - JAE	- Check Final Actual miles (null actual miles ok as long as override)
--  4.1.23	  2016.10.17 - JAE	- Add weight units for settlement
/***********************************/
ALTER VIEW [dbo].[viewOrder_Financial_Base_Shipper] AS 
	SELECT *
		, HasError = cast(CASE WHEN ErrorFieldCSV IS NOT NULL THEN 1 ELSE 0 END as bit)
	FROM (
		SELECT *
			, OrderID = ID
			, ErrorFieldCSV = nullif(substring(CASE WHEN Approved = 0 THEN ',Approved' ELSE '' END
				+ CASE WHEN InvoiceLoadAmount IS NULL THEN ',InvoiceLoadAmount,InvoiceRouteRate,InvoiceRateSheet' ELSE '' END
				+ CASE WHEN FinalChainup = 1 AND InvoiceChainupAmount IS NULL THEN ',InvoiceChainupAmount' ELSE '' END
				+ CASE WHEN FinalRerouteCount > 0 AND InvoiceRerouteAmount IS NULL THEN ',InvoiceRerouteAmount' ELSE '' END
				+ CASE WHEN FinalH2S = 1 AND InvoiceH2SAmount IS NULL THEN ',InvoiceH2SAmount' ELSE '' END
				+ CASE WHEN TicketCount > 1 AND InvoiceSplitLoadAmount IS NULL THEN ',InvoiceSplitLoadAmount' ELSE '' END
				+ CASE WHEN InvoiceOriginWaitBillableMinutes > 0 AND InvoiceOriginWaitAmount IS NULL THEN ',InvoiceOriginWaitAmount' ELSE '' END
				+ CASE WHEN InvoiceDestinationWaitBillableMinutes > 0 AND InvoiceDestinationWaitAmount IS NULL THEN ',InvoiceDestinationWaitAmount' ELSE '' END
				+ CASE WHEN Rejected = 1 AND InvoiceOrderRejectAmount IS NULL THEN ',InvoiceOrderRejectAmount' ELSE '' END
				+ CASE WHEN InvoiceMinSettlementUnitsID IS NULL THEN ',InvoiceMinSettlementUnits' ELSE '' END
				+ CASE WHEN InvoiceSettlementFactorID IS NULL THEN ',InvoiceSettlementFactor' ELSE '' END
				+ CASE WHEN Rejected = 0 AND InvoiceSettlementFactorID IS NULL THEN ',InvoiceSettlementFactor' ELSE '' END
				+ CASE WHEN Rejected = 0 AND InvoiceSettlementFactorID IS NOT NULL AND dbo.fnOrderSettlementUnits(InvoiceSettlementFactorID, OriginGrossUnits, OriginNetUnits, OriginGrossStdUnits, DestGrossUnits, DestNetUnits, OriginWeightNetUnits, DestWeightNetUnits) IS NULL THEN ','+OrderUnitsField ELSE '' END
				+ CASE WHEN InvoiceWaitFeeParameterID IS NULL THEN ',InvoiceOriginWaitBillableMinutes,InvoiceDestinationWaitBillableMinutes' ELSE '' END
				+ CASE WHEN isnull(FinalActualMiles, 0) = 0 THEN ',ActualMiles' ELSE '' END, 2, 100000), '')
		FROM (
			SELECT O.* 
				, InvoiceRatesAppliedDateUTC = OS.CreateDateUTC
				, OS.BatchID
				, Approved = cast(ISNULL(OA.Approved, 0) as bit)
				, FinalActualMiles = ISNULL(OA.OverrideActualMiles, O.ActualMiles)
				, FinalOriginMinutes = ISNULL(OA.OverrideOriginMinutes, O.OriginMinutes)
				, FinalDestMinutes = ISNULL(OA.OverrideDestMinutes, O.DestMinutes)
				, FinalH2S = cast(CASE WHEN isnull(OA.OverrideH2S, 0) = 1 THEN 0 ELSE O.H2S END as bit)
				, FinalChainup = cast(CASE WHEN isnull(OA.OverrideChainup, 0) = 1 THEN 0 ELSE O.Chainup END as bit)
				, FinalRerouteCount = CASE WHEN isnull(OA.OverrideReroute, 0) = 1 THEN 0 ELSE isnull(O.RerouteCount, 0) END
				, FC.OrderUnitsField
				, InvoiceBatchNum = OS.BatchNum 
				, InvoiceOriginWaitBillableMinutes = OS.OriginWaitBillableMinutes
				, InvoiceDestinationWaitBillableMinutes = OS.DestinationWaitBillableMinutes
				, InvoiceTotalWaitBillableMinutes = isnull(OS.OriginWaitBillableMinutes, 0) + ISNULL(OS.DestinationWaitBillableMinutes, 0) 
				, InvoiceWaitFeeParameterID = WaitFeeParameterID
				, InvoiceWaitFeeSubUnit = WaitFeeSubUnit
				, InvoiceWaitFeeRoundingType = WaitFeeRoundingType
				, InvoiceOriginWaitRate = OS.OriginWaitRate 
				, InvoiceOriginWaitAmount = OS.OriginWaitAmount 		
				, InvoiceDestinationWaitRate = OS.DestinationWaitRate 
				, InvoiceDestinationWaitAmount = OS.DestinationWaitAmount 
				, InvoiceTotalWaitAmount = OS.TotalWaitAmount
				, InvoiceOrderRejectRate = OS.OrderRejectRate	
				, InvoiceOrderRejectRateType = OS.OrderRejectRateType  
				, InvoiceOrderRejectAmount = OS.OrderRejectAmount  
				, InvoiceChainupRate = OS.ChainupRate
				, InvoiceChainupRateType = OS.ChainupRateType
				, InvoiceChainupAmount = OS.ChainupAmount 
				, InvoiceRerouteRate = OS.RerouteRate
				, InvoiceRerouteRateType = OS.RerouteRateType
				, InvoiceRerouteAmount = OS.RerouteAmount 
				, InvoiceH2SRate = OS.H2SRate
				, InvoiceH2SRateType = OS.H2SRateType
				, InvoiceH2SAmount = OS.H2SAmount
				, InvoiceSplitLoadRate = OS.SplitLoadRate
				, InvoiceSplitLoadRateType = OS.SplitLoadRateType
				, InvoiceSplitLoadAmount = OS.SplitLoadAmount
				, InvoiceOtherAmount = OS.OtherAmount
				, InvoiceTaxRate = OS.OriginTaxRate
				, InvoiceSettlementUom = OS.SettlementUom 
				, InvoiceSettlementUomShort = OS.SettlementUomShort 
				, InvoiceShipperSettlementFactorID = OS.ShipperSettlementFactorID 
				, InvoiceSettlementFactorID = OS.SettlementFactorID 
				, InvoiceSettlementFactor = OS.SettlementFactor 
				, InvoiceMinSettlementUnitsID = OS.MinSettlementUnitsID 
				, InvoiceMinSettlementUnits = OS.MinSettlementUnits
				, InvoiceUnits = OS.SettlementUnits
				, InvoiceRouteRate = OS.RouteRate
				, InvoiceRouteRateType = OS.RouteRateType
				, InvoiceRateSheetRate = OS.RateSheetRate
				, InvoiceRateSheetRateType = OS.RateSheetRateType
				, InvoiceFuelSurchargeRate = OS.FuelSurchargeRate
				, InvoiceFuelSurchargeAmount = OS.FuelSurchargeAmount
				, InvoiceLoadAmount = OS.LoadAmount
				, InvoiceTotalAmount = OS.TotalAmount
			FROM dbo.viewOrder O
			LEFT JOIN viewOrderSettlementShipper OS ON OS.OrderID = O.ID
			LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
			LEFT JOIN tblSettlementFactor FC ON FC.ID = OS.SettlementFactorID
			WHERE O.StatusID NOT IN (-10) -- don't include "Generated" orders (only Assigned+)
		) X
	) X1


GO



COMMIT
SET NOEXEC OFF