-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.7.36.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.7.36.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0
SET NOEXEC OFF
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.7.36'
SELECT  @NewVersion = '3.7.37'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Order Changes: Changed "ChangeDateUTC" to "ChangeDate" because time in that column is not displayed in UTC.'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 11 Sep 2014
-- Description:	compute and return the order changes for a single order (specified by an Order.ID value)
--				6/25/15 - BB - Changed "ChangeDateUTC" to "ChangeDate" because time in that column is not displayed in UTC.
-- =============================================
ALTER PROCEDURE [dbo].[spGetOrderChanges](@id int) 
AS BEGIN
	DECLARE @ret TABLE 
	(
		FieldName varchar(255)
	  , NewValue varchar(max)
	  , OldValue varchar(max)
	  , ChangeDate varchar(100)
	  , ChangedByUser varchar(100)
	  , IsCurrent bit
	)

	DECLARE @columns TABLE (id int, name varchar(100))
	INSERT INTO @columns 		
		SELECT ROW_NUMBER() OVER (ORDER BY COLUMN_NAME), COLUMN_NAME 
		FROM INFORMATION_SCHEMA.COLUMNS 
		WHERE TABLE_NAME = 'tblOrder' AND COLUMN_NAME NOT IN ('ID','LastChangeDateUTC')
	
	SELECT rowID = ROW_NUMBER() OVER (ORDER BY sortnum, dbauditdate desc, RouteID desc), * 
	INTO #history
	FROM (
		SELECT dbauditdate = NULL, *, sortnum = 0 from tblOrder where ID = @id
		UNION ALL SELECT *, sortnum = 1 from tblOrderDbAudit where ID = @id
	) X

	DECLARE @rowID int, @colID int, @colName varchar(100), @newValue varchar(max), @oldValue varchar(max), @sql nvarchar(max), @newColID int
	DECLARE @ChangeDate datetime, @changeUser varchar(100), @isCurrent bit

	SELECT @rowID = MIN(rowID) FROM #history
	WHILE EXISTS(SELECT * FROM #history WHERE rowID = @rowID + 1) BEGIN
		SELECT TOP 1 @colName = name, @colID = id FROM @columns WHERE id = 1
		WHILE (@colName IS NOT NULL) BEGIN

			-- get the "now" value
			SET @sql = N'SELECT TOP 1 @value=' + @colName + N', @ChangeDate=LastChangeDateUTC, @changeUser=LastChangedByUser FROM #history WHERE rowID = @rowID'
			EXEC sp_executesql @sql
				, N'@rowID int, @ChangeDate datetime OUTPUT, @changeUser varchar(100) OUTPUT, @value varchar(max) OUTPUT'
				, @rowID = @rowID, @ChangeDate = @ChangeDate OUTPUT, @changeUser = @changeUser OUTPUT, @value = @newValue OUTPUT

			-- get the "from" value
			SET @sql = N'SELECT TOP 1 @value=' + @colName + N' FROM #history WHERE rowID = @rowID + 1'
			EXEC sp_executesql @sql, N'@rowID int, @value varchar(max) OUTPUT', @rowID = @rowID, @value = @oldValue OUTPUT
			
			IF (isnull(@newValue, '\r') <> isnull(@oldValue, '\r')) BEGIN
				INSERT INTO @ret 
					SELECT @colName, @newValue, @oldValue, @ChangeDate, @changeUser, CASE WHEN @rowID = 1 THEN 1 ELSE 0 END
			END
			
			SET @colName = NULL
			SELECT TOP 1 @colName = name, @colID = id FROM @columns WHERE id > @colID ORDER BY ID
			
		END

		SET @rowID = @rowID + 1
	END
	
	UPDATE @ret
	  SET NewValue = OSNV.OrderStatus, OldValue = OSOV.OrderStatus
	FROM @ret R 
	LEFT JOIN tblOrderStatus OSNV ON OSNV.ID = R.NewValue
	LEFT JOIN tblOrderStatus OSOV ON OSOV.ID = R.OldValue
	WHERE R.FieldName LIKE 'StatusID'
	
	SELECT * FROM @ret

END



GO

COMMIT 
SET NOEXEC OFF