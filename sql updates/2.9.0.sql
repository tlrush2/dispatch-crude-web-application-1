-- rollback
/*
	-- add Origin|Destination GeoFenceRadiusMeters int field
	-- add tblLocationType|tblDriverLocation for GPS location logging
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.8.10'
SELECT  @NewVersion = '2.9.0'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

ALTER TABLE tblOrigin ADD GeoFenceRadiusMeters int NULL
ALTER TABLE tblDestination ADD GeoFenceRadiusMeters int NULL
GO

-- set default values
UPDATE tblOrigin SET GeoFenceRadiusMeters = 100
GO
UPDATE tblDestination SET GeoFenceRadiusMeters = 100
GO

CREATE TABLE tblLocationType
(
  ID tinyint NOT NULL CONSTRAINT PK_LocationType PRIMARY KEY
, Name varchar(25) NOT NULL
, CreateDateUTC smalldatetime NOT NULL CONSTRAINT DF_LocationType DEFAULT getutcdate()
, CreatedByUser varchar(100) NULL
)
GO
CREATE UNIQUE INDEX udxLocationType_Name ON tblLocationType(Name)
GO
GRANT SELECT ON tblLocationType TO dispatchcrude_iis_acct
GO

INSERT INTO tblLocationType (ID, Name)
	SELECT 1, 'Auto'
	UNION
	SELECT 2, 'Driver Arrive'
	UNION
	SELECT 3, 'Driver Depart'
	UNION
	SELECT 4, 'Geo Fence In'
	UNION
	SELECT 5, 'Geo Fence Out'
GO
	
CREATE TABLE tblDriverLocation
(
  ID int identity(1, 1) NOT NULL CONSTRAINT PK_DriverLocation PRIMARY KEY
, UID uniqueidentifier NOT NULL CONSTRAINT DF_DriverLocation_UID DEFAULT newid()
, DriverID int NOT NULL CONSTRAINT FK_DriverLocation_Driver REFERENCES tblDriver(ID)
, OrderID int NULL CONSTRAINT FK_DriverLocation_Order REFERENCES tblOrder(ID)
, OriginID int NULL CONSTRAINT FK_DriverLocation_Origin REFERENCES tblOrigin(ID)
, DestinationID int NULL CONSTRAINT FK_DriverLocation_Destination REFERENCES tblDestination(ID)
, LocationTypeID tinyint NOT NULL CONSTRAINT FK_DriverLocation_LocationType REFERENCES tblLocationType(ID)
, Lat decimal(9, 6) NOT NULL
, Lon decimal(9, 6) NOT NULL
, DistanceToPoint int NULL
, SourceAccuracyMeters smallint NULL
, SourceDateUTC smalldatetime NULL
, CreateDateUTC smalldatetime NOT NULL CONSTRAINT DF_DriverLocation_CreateDateUTC DEFAULT getutcdate()
, CreatedByUser varchar(100) NULL
)
GO
GRANT SELECT, INSERT, DELETE ON tblDriverLocation TO dispatchcrude_iis_acct
GO

EXEC _spRefreshAllViews
GO

/*******************************************/
-- Date Created: 25 Apr 2013
-- Author: Kevin Alons
-- Purpose: return readonly Order data for Driver App sync
/*******************************************/
ALTER FUNCTION [dbo].[fnOrderReadOnly_DriverApp]( @DriverID int, @LastChangeDateUTC datetime ) RETURNS TABLE AS
RETURN 
	SELECT O.ID
		, O.OrderNum
		, O.StatusID
		, O.TicketTypeID
		, cast(P.PriorityNum as int) AS PriorityNum
		, O.Product
		, O.DueDate
		, O.Origin
		, O.OriginFull
		, OO.OriginType
		, O.OriginUomID
		, OO.Station AS OriginStation
		, OO.LeaseNum AS OriginLeaseNum
		, OO.County AS OriginCounty
		, OO.LegalDescription AS OriginLegalDescription
		, O.OriginNDIC
		, O.OriginNDM
		, O.OriginCA
		, O.OriginState
		, OO.WellAPI AS OriginAPI
		, OO.LAT AS OriginLat
		, OO.LON AS OriginLon
		, OO.GeoFenceRadiusMeters AS OriginGeoFenceRadiusMeters
		, O.Destination
		, O.DestinationFull
		, D.DestinationType AS DestType 
		, O.DestUomID
		, D.LAT AS DestLat
		, D.LON as DestLon
		, D.GeoFenceRadiusMeters AS DestGeoFenceRadiusMeters
		, D.Station AS DestinationStation
		, O.CreateDateUTC
		, O.CreatedByUser
		, O.LastChangeDateUTC
		, O.LastChangedByUser
		, isnull(ODAVD.VirtualDeleteDateUTC, O.DeleteDateUTC) AS DeleteDateUTC
		, isnull(ODAVD.VirtualDeletedByUser, O.DeletedByUser) AS DeletedByUser
		, O.OriginID
		, O.DestinationID
		, cast(O.PriorityID AS int) AS PriorityID
		, O.Operator
		, O.OperatorID
		, O.Pumper
		, O.PumperID
		, O.Producer
		, O.ProducerID
		, O.Customer
		, O.CustomerID
		, O.Carrier
		, O.CarrierID
		, O.ProductID
		, O.TicketType
		, C.ZPLHeaderBlob AS PrintHeaderBlob
		, isnull(C.EmergencyInfo, 'For an emergency (spill, leak, fire, or accident) contact CHEMTREC @ 800.424.9300 (toll free)') AS EmergencyInfo
		, O.DestTicketTypeID
		, O.DestTicketType
		, O.OriginTankNum
		, O.OriginTankID
		, O.DispatchNotes
		, O.DispatchConfirmNum
		, isnull(R.ActualMiles, 0) AS RouteActualMiles
		, CA.Authority AS CarrierAuthority
	FROM dbo.viewOrder O
	JOIN dbo.tblPriority P ON P.ID = O.PriorityID
	JOIN dbo.viewOrigin OO ON OO.ID = O.OriginID
	JOIN dbo.viewDestination D ON D.ID = O.DestinationID
	JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
	JOIN dbo.tblRoute R ON R.ID = O.RouteID
	JOIN dbo.tblCarrier CA ON CA.ID = O.CarrierID
	LEFT JOIN dbo.tblPrintStatus DPS ON DPS.ID = O.DeliverPrintStatusID
	LEFT JOIN tblOrderDriverAppVirtualDelete ODAVD ON ODAVD.OrderID = O.ID AND ODAVD.DriverID = @DriverID
	CROSS JOIN (SELECT DATEADD(second, -5, @LastChangeDateUTC) AS LCD) LCD
	WHERE (O.StatusID IN (2, 7, 8) -- Dispatched, Accepted, Picked Up
		-- or status = Delivered and the driver still has access to the record
		OR (StatusID = 3 AND isnull(DPS.IsCompleted, 0) = 0)
		-- or this potentially was a DriverApp record, that no longer is, so should be included (as deleted)
		OR (ODAVD.ID IS NOT NULL AND ODAVD.VirtualDeleteDateUTC >= LCD.LCD))
	  AND (O.DriverID = @DriverID OR ODAVD.DriverID = @DriverID)
	  AND (
		@LastChangeDateUTC IS NULL 
		OR O.CreateDateUTC >= LCD.LCD
		OR O.LastChangeDateUTC >= LCD.LCD
		OR O.DeleteDateUTC >= LCD.LCD
		OR C.LastChangeDateUTC >= LCD.LCD
		OR CA.LastChangeDateUTC >= LCD.LCD
		OR OO.LastChangeDateUTC >= LCD.LCD
		OR R.LastChangeDateUTC >= LCD.LCD
		OR ODAVD.VirtualDeleteDateUTC >= LCD.LCD)
GO

COMMIT
SET NOEXEC OFF
	
EXEC _spRefreshAllViews