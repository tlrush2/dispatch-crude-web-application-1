SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.4.21'
SELECT  @NewVersion = '4.4.22'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'JT-729 - Have DVIR submissions update truck/trailer for driver profile and dispatched orders'	
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO



CREATE TRIGGER trigQuestionnaireSubmission_IU ON tblQuestionnaireSubmission AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	PRINT 'trigQuestionnaireSubmission_IU START: ' + (CONVERT(VARCHAR(24), GETDATE(), 121))

	/* HANDLE TRUCK | TRAILER | TRAILER2 default changes when the driver submits a system template */
	IF (EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblQuestionnaireTemplate qt ON qt.ID = i.QuestionnaireTemplateID
		JOIN tblQuestionnaireTemplateType qtt ON qtt.ID = qt.QuestionnaireTemplateTypeID
		WHERE qtt.IsSystem = 1))
	BEGIN

		-- TRUCK
		-- update driver defaults
		UPDATE tblDriver
			SET TruckID = i.TruckID
			, LastChangeDateUTC = GETUTCDATE()
			, LastChangedByUser = i.CreatedByUser
		FROM tblDriver DR
		JOIN inserted i ON i.DriverID = DR.ID
		WHERE i.TruckID <> DR.TruckID
			
		-- update any dispatched orders
		UPDATE tblOrder
			SET TruckID = i.TruckID
			, LastChangeDateUTC = GETUTCDATE()
			, LastChangedByUser = i.CreatedByUser
		FROM tblOrder O
		JOIN inserted i ON i.DriverID = O.DriverID 
			AND O.StatusID IN (2) -- DISPATCHED
			AND i.TruckID <> O.TruckID
			AND O.DeleteDateUTC IS NULL

		-- TRAILER
		-- update driver defaults
		UPDATE tblDriver
			SET TrailerID = i.TrailerID
			, LastChangeDateUTC = GETUTCDATE()
			, LastChangedByUser = i.CreatedByUser
		FROM tblDriver DR
		JOIN inserted i ON i.DriverID = DR.ID
		WHERE i.TrailerID <> DR.TrailerID
			
		-- update any dispatched orders
		UPDATE tblOrder
			SET TrailerID = ISNULL(i.TrailerID, O.TrailerID)
			, LastChangeDateUTC = GETUTCDATE()
			, LastChangedByUser = i.CreatedByUser
		FROM tblOrder O
		JOIN inserted i ON i.DriverID = O.DriverID 
			AND O.StatusID IN (2) -- DISPATCHED
			AND i.TrailerID <> O.TrailerID
			AND O.DeleteDateUTC IS NULL

		-- TRAILER 2
		-- update driver defaults
		UPDATE tblDriver
			SET Trailer2ID = i.Trailer2ID
			, LastChangeDateUTC = GETUTCDATE()
			, LastChangedByUser = i.CreatedByUser
		FROM tblDriver DR
		JOIN inserted i ON i.DriverID = DR.ID
			AND isnull(i.Trailer2ID, 0) <> isnull(DR.Trailer2ID, 0)
			
		-- update any dispatched orders
		UPDATE tblOrder
			SET Trailer2ID = i.Trailer2ID
			, LastChangeDateUTC = GETUTCDATE()
			, LastChangedByUser = i.CreatedByUser
		FROM tblOrder O
		JOIN inserted i ON i.DriverID = O.DriverID 
			AND O.StatusID IN (2) -- DISPATCHED
			AND isnull(i.Trailer2ID, 0) <> isnull(O.Trailer2ID, 0)
			AND O.DeleteDateUTC IS NULL
	END
		

	PRINT 'trigQuestionnaireSubmission_IU COMPLETE: ' + (CONVERT(VARCHAR(24), GETDATE(), 121))

END

GO


COMMIT
SET NOEXEC OFF