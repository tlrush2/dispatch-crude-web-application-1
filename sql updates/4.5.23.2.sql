SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.5.23.1'
SELECT  @NewVersion = '4.5.23.2'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'JT-3306 - Fix ImportCenter "Clone" - was dropping "CSharp Expression" values during clone operation'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/*****************************************************
Created: 3.10.1 - 2015.12.15 - Kevin Alons
Purpose: clone an existing ImportCenterDefinition with a new name
Changes:
- 3.10.12	- 2016.02.06 - KDA	- remove IsKey & DoUpdate tblImportCenterFieldDefinition fields from processing
- 4.4.7		- 2016.12.01 - KDA	- add/honor @UserNames parameter
- 4.4.9		- 2016.12.15 - KDA	- also clone InputFile
- 4.5.23.2	- 2017.04.24 - KDA	- JT-3306 - fix issue causing CSharpExpression & KeyComparisonTypeID from being properly "cloned"
*****************************************************/
ALTER PROCEDURE spCloneImportCenterDefinition
(
  @importCenterDefinitionID int
, @Name varchar(50)
, @UserNames varchar(max)
, @UserName varchar(100)
, @NewID int = NULL OUTPUT
) AS
BEGIN
	BEGIN TRAN CICD

	-- copy the basic ImportCenterDefinition first
	INSERT INTO tblImportCenterDefinition (RootObjectID, Name, UserNames, CreatedByUser)
		SELECT RootObjectID, @Name, @UserNames, @UserName FROM tblImportCenterDefinition WHERE ID = @importCenterDefinitionID
	SET @NewID = SCOPE_IDENTITY()

	-- add the root ImportFieldDefinitions (with no ParentFieldID)
	INSERT INTO tblImportCenterFieldDefinition (ImportCenterDefinitionID, ObjectFieldID, CSharpExpression, KeyComparisonTypeID, CreatedByUser)
		SELECT @NewID, ObjectFieldID, CSharpExpression, KeyComparisonTypeID, CreatedByUser
		FROM tblImportCenterFieldDefinition
		WHERE ImportCenterDefinitionID = @importCenterDefinitionID
		  AND ParentFieldID IS NULL
	-- also clone the InputFile record if defined
	INSERT INTO tblImportCenterInputFile (ImportCenterDefinitionID, OriginalFileName, FieldNameCSV, InputDataTableXml, CreatedByUser)
		SELECT @NewID, OriginalFileName, FieldNameCSV, InputDataTableXml, @UserName 
		FROM tblImportCenterInputFile 
		WHERE ImportCenterDefinitionID = @importCenterDefinitionID

	DECLARE @Level int; SET @Level = 0

	-- create a table to store the old-new ImportField mappings (will be used later to add the ImportFieldDefinition_Fields)
	DECLARE @Map TABLE (OID int, NID int, Level int)
	-- populate with the just added root ImportFieldDefinitions
	INSERT INTO @Map 
		SELECT OICFD.ID, NICFD.ID, @Level
		FROM tblImportCenterFieldDefinition OICFD
		JOIN tblImportCenterFieldDefinition NICFD ON NICFD.ImportCenterDefinitionID = @NewID AND NICFD.ObjectFieldID = OICFD.ObjectFieldID
		WHERE OICFD.ImportCenterDefinitionID = @importCenterDefinitionID AND OICFD.ParentFieldID IS NULL

	--DECLARE @debugMap XML = (SELECT * FROM @Map FOR XML AUTO)

	-- create table to remember newly added child FieldDefinition records
	DECLARE @NID IDTABLE
	-- populate with the first child FieldDefinitions to be added
	INSERT INTO @NID 
		SELECT ID FROM tblImportCenterFieldDefinition X 
		JOIN @Map M ON M.OID = X.ParentFieldID 
		WHERE ImportCenterDefinitionID = @importCenterDefinitionID 
		EXCEPT SELECT OID FROM @Map

	--DECLARE @debugNID XML = (SELECT * FROM @NID FOR XML AUTO)

	WHILE EXISTS (SELECT ID FROM @NID)
	BEGIN
		-- add the next level of children FieldDefinitions
		INSERT INTO tblImportCenterFieldDefinition (ImportCenterDefinitionID, ObjectFieldID, ParentFieldID, CSharpExpression, KeyComparisonTypeID, CreatedByUser)
			SELECT @NewID, ObjectFieldID, M.NID, CSharpExpression, KeyComparisonTypeID, CreatedByUser
			FROM tblImportCenterFieldDefinition ICFD
			JOIN @NID NID ON NID.ID = ICFD.ID
			JOIN @Map M ON M.OID = ICFD.ParentFieldID

		-- record the old-new mappings for these new FieldDefinitions
		INSERT INTO @Map 
			SELECT OICFD.ID, NICFD.ID, @Level
			FROM tblImportCenterFieldDefinition OICFD
			JOIN tblImportCenterFieldDefinition NICFD ON NICFD.ImportCenterDefinitionID = @NewID AND NICFD.ObjectFieldID = OICFD.ObjectFieldID
			WHERE OICFD.ID IN (SELECT ID FROM @NID)

		--SELECT @debugMap = (SELECT * FROM @Map FOR XML AUTO)

		-- see if any next level new FieldMappings need to be processed
		DELETE FROM @NID
		INSERT INTO @NID 
			SELECT ID FROM tblImportCenterFieldDefinition X JOIN @Map M ON M.OID = X.ParentFieldID WHERE ImportCenterDefinitionID = @importCenterDefinitionID EXCEPT SELECT OID FROM @Map

		--SELECT @debugNID = (SELECT * FROM @NID FOR XML AUTO)
		
		-- increment @Level
		SET @Level = @Level + 1
	END

	-- add all the FieldDefinitionFields based on the accumulated @Map records
	INSERT INTO tblImportCenterFieldDefinitionField (ImportCenterFieldID, ImportFieldName, Position)
		SELECT M.NID, DF.ImportFieldName, DF.Position
		FROM tblImportCenterFieldDefinitionField DF
		JOIN @Map M ON M.OID = DF.ImportCenterFieldID

	COMMIT TRAN CICD
END

GO

COMMIT
SET NOEXEC OFF