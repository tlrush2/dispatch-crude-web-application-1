SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.5.10'
SELECT  @NewVersion = '4.5.11'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'JT-1008 - Contract Number'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


-- --------------------------------
-- Add new Contracts permissions 
-- --------------------------------
-- Permissions do not get added to users by default when they are new permissions.  They will be added on request when needed by a user.
EXEC spAddNewPermission 'viewContracts', 'Allow user to view the Contracts page', 'Business Rules', 'Contracts - View'
GO
EXEC spAddNewPermission 'createContracts', 'Allow user to create Contracts', 'Business Rules', 'Contracts - Create'
GO
EXEC spAddNewPermission 'editContracts', 'Allow user to edit Contracts', 'Business Rules', 'Contracts - Edit'
GO
EXEC spAddNewPermission 'deactivateContracts', 'Allow user to deactivate Contracts', 'Business Rules', 'Contracts - Deactivate'
GO


-- --------------------------------
-- Add new Contract table/views/triggers/functions
-- --------------------------------
CREATE TABLE tblContract
(
	ID INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_Contract PRIMARY KEY NONCLUSTERED,

	ContractNumber VARCHAR(20) NOT NULL, -- current value
	PrintDescription VARCHAR(100) NULL,
	Notes VARCHAR(MAX) NULL,

	ShipperID INT NOT NULL CONSTRAINT FK_Contract_Shipper REFERENCES tblCustomer(ID),
	ProductGroupID INT NOT NULL CONSTRAINT FK_Contract_ProductGroup REFERENCES tblProductGroup(ID),
	ProducerID INT NOT NULL CONSTRAINT FK_Contract_Producer REFERENCES tblProducer(ID),
	Units INT NOT NULL,
	UomID INT NOT NULL CONSTRAINT FK_Contract_Uom REFERENCES tblUom(ID),
	EffectiveDate DATE NOT NULL,
	EndDate DATE NOT NULL,

	CreateDateUTC DATETIME NOT NULL CONSTRAINT DF_Contract_CreateDateUTC DEFAULT GETUTCDATE(),
	CreatedByUser VARCHAR(100) NOT NULL CONSTRAINT DF_Contract_CreatedByUser DEFAULT SUSER_NAME(),
	LastChangeDateUTC DATETIME NULL,
	LastChangedByUser VARCHAR(100) NULL,
	DeleteDateUTC DATETIME NULL,
	DeletedByUser VARCHAR(100) NULL
)

ALTER TABLE tblContract WITH CHECK ADD CONSTRAINT CK_Contract_EndAfter CHECK (EffectiveDate <= EndDate)
GO
ALTER TABLE dbo.tblContract CHECK CONSTRAINT CK_Contract_EndAfter
GO


-- Insert Job #, Contract #, and Contract Description into Custom BOL fields table
INSERT INTO tblCustomBOLFields (DataField, InternalLabel, DataLabel, DisplayOnBOL)
	SELECT 'JobNumber', 'Job Number', 'Job Number', 0
		UNION
	SELECT 'Contract.ContractNumber', 'Contract Number', 'Contract Number', 0
		UNION
	SELECT 'Contract.PrintDescription', 'Contract Description', 'Contract Description', 0
GO


/****************************************************
-- Date Created: 2017-02-07 (4.5.11)
-- Author: Joe Engler
-- Purpose: Calculate DailyUnits value
-- Changes:
****************************************************/
CREATE VIEW viewContract AS
	SELECT C.*
		, DailyUnits = Units / CAST(DATEDIFF(DAY, EffectiveDate, EndDate) + 1 as decimal(18, 10))
		, Shipper = S.Name
		, ProductGroup = PG.Name
		, Producer = P.Name
		, Uom = U.Name
		, NumDays = DATEDIFF(day, EffectiveDate, EndDate) + 1
		, DaysLeft = CASE WHEN DATEDIFF(day, GETDATE(), EndDate) < 0 THEN 0 -- target date already past, set to zero
		                  ELSE DATEDIFF(day, GETDATE(), EndDate) + 1 END -- include today
	FROM tblContract C
	JOIN tblCustomer S ON S.ID = C.ShipperID
	JOIN tblProductGroup PG ON PG.ID = C.ProductGroupID
	JOIN tblProducer P ON P.ID = C.ProducerID
	JOIN tblUom U ON U.ID = C.UomID
GO



/***********************************************
-- Date Created: 2017-02-07 (4.5.11)
-- Author: Joe Engler
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
-- Changes:
***********************************************/
CREATE TRIGGER trigContract_IU ON tblContract AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblContract X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.ProducerID, X.ProducerID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Contracts are not allowed'
	END

	IF @error IS NOT NULL 
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
END
GO


/********************************************
 Date Created: 2016/05/16
 Author: Joe Engler
 Purpose: Retrieve the current units fulfilled for a contract for the specified selection criteria + Date Range
 Changes:
********************************************/
CREATE FUNCTION fnContractTotals(@ShipperID int = -1, @ProductGroupID int = -1, @ProducerID int = -1, @StartDate date = NULL, @EndDate date = NULL, @UomID int = 1) 
RETURNS TABLE AS RETURN
	SELECT ID,
		ContractNumber,
		TicketCount,
		TotalGross,
		ShipperID,
		Shipper,
		ProductGroupID,
		ProductGroup,
		ProducerID,
		Producer,
		Target,
		DailyUnits,
		AllocationStartDate = qStart,
		AllocationEndDate = qEnd,
		AllocatedUnits = DailyUnits * (DATEDIFF(DAY, qStart, qEnd) + 1), -- broken down into daily average * number of days
 		QualifiedDaysLeft = CASE WHEN DATEDIFF(DAY, GETDATE(), qEnd) < 0 THEN 0 -- target date already past, set to zero
									ELSE DATEDIFF(DAY, GETDATE(), qEnd) + 1 END, -- include today
		Fulfilled = CASE WHEN TotalGross >= DailyUnits * (DATEDIFF(DAY, qStart, qEnd) + 1) THEN 1 ELSE 0 END
	FROM (
		    -- Get orders for the given date range and merge with any allocations
			-- Null allocations will be considered excess
			SELECT c.ID,
				c.ContractNumber,
				TicketCount = COUNT(*),
				TotalGross = SUM(dbo.fnConvertUOM(COALESCE(NULLIF(DestGrossUnits,0), NULLIF(OriginGrossUnits,0), 0), DestUomID, @UomID)), 
				c.ShipperID,
				c.Shipper,
				c.ProductGroupID,
				c.ProductGroup,
				c.ProducerID,
				c.Producer,
				Target = dbo.fnConvertUom(Units, c.UomID, @UomID),
				DailyUnits = dbo.fnConvertUom(DailyUnits, c.UomID, @UomID),
				qStart = EffectiveDate,
				qEnd = EndDate
			FROM (
				SELECT *
				FROM viewContract
 				WHERE (EffectiveDate BETWEEN @StartDate AND @EndDate OR EndDate BETWEEN @StartDate AND @EndDate OR EffectiveDate <= @StartDate AND EndDate >= @EndDate)
				AND (@ShipperID = -1 OR ShipperID = @ShipperID)
				AND (@ProductGroupID = -1 OR ProductGroupID = @ProductGroupID)
				AND (@ProducerID = -1 OR ProducerID = @ProducerID)
			) c
 			OUTER APPLY (SELECT * FROM viewOrder WHERE StatusID IN (8, 3, 4) --PICKED UP, DELIVERED, OR AUDITED
					AND CustomerID = c.ShipperID
					AND ProductGroupID = c.ProductGroupID
					AND ProducerID = c.ProducerID
					AND ISNULL(OrderDate, DueDate) BETWEEN EffectiveDate AND EndDate
					AND Rejected = 0 
					AND DeleteDateUTC IS NULL) o

		GROUP BY c.ID, c.ContractNumber,
				c.ShipperID, c.Shipper, 
				c.ProductGroupID, c.ProductGroup,
				c.ProducerID, c.Producer,
				Units,
				c.UomID, 
				DailyUnits,
				EffectiveDate,
				EndDate

	) q2
GO


/********************************************
 Date Created: 2017/02/08
 Author: Joe Engler
 Purpose: Retrieve the relevant contract for the specified selection criteria + Date Range.
		Will be null if no match or if contract is already fulfillled
 Changes:
********************************************/
CREATE FUNCTION fnContractID(@ShipperID int, @ProductGroupID int, @ProducerID int, @OrderDate date) 
RETURNS INT AS 
BEGIN
	DECLARE @ret INT

	SELECT @ret = CASE WHEN Fulfilled = 1 THEN NULL ELSE ID END
	FROM dbo.fnContractTotals(@ShipperID, @ProductGroupID, @ProducerID, @OrderDate, @OrderDate, default)

	RETURN @ret
END
GO




-- --------------------------------
-- Update order table
-- Add ContractID as Foreign Key and update trigger
-- --------------------------------

ALTER TABLE tblOrder ADD ContractID INT CONSTRAINT FK_Order_Contract REFERENCES tblContract(ID)
GO
ALTER TABLE tblOrderDbAudit ADD ContractID INT
GO

-- Remove from Import Center
-- Order.ContractNumber
DELETE FROM tblImportCenterFieldDefinition WHERE ObjectFieldID = 416
GO
DELETE FROM tblObjectField WHERE ID = 416 
GO


/************************************************
 Author:		Kevin Alons
 Create date: 19 Dec 2012
 Description:	trigger to 
			1) validate any changes, and fail the update if invalid changes are submitted
			2) add a unique, incrementing OrderNum to each new Order (manual Identity column)
			3) recompute wait times (origin|destination based on times provided)
			4) generate route table entry for any newly used origin-destination combination
			5) keep Order consistent with Origin.Producer|Operator|UOM & Dest.UOM then the Origin|Dest change
			6) roll the PrintStatus back to NOTFINALIZED (0) when status is rolled back
			7) when DriverID changes, mark those orders AS DELETED and clone the order with the new DriverID and associated records
			8) update any ticket quantities for open orders when the UOM is changed for the Origin
			9) update the Driver.Truck\Trailer\Trailer2 defaults & related DISPATCHED orders when driver updates them on an order
-REMOVED	10) update the Pickup/DeliverLastChangeDateUTC when Origin/Destination is changed (respectively)
			11) (re) apply Settlement Amounts to orders not yet fully settled when status is changed to DELIVERED
			12) if DBAudit is turned on, save an audit record for this Order change
 Changes: 
-- 3.7.4	- 2015/05/08	- GSM	- Added Rack/Bay field to DBAudit logic
-- 3.7.7	- 15 May 2015	- KDA	- REMOVE #10 above, instead update the LastChangeDateUTC whenever an Origin or Destination is changed
-- 3.7.11	- 2015/05/18	- KDA	- generally use GETUTCDATE for all LastChangeDateUTC revisions (instead of related Order.LastChangeDateUTC, etc)
-- 3.7.12	- 5/19/2015		- KDA	- update any existing OrderTicket records when the Order is DISPATCHED to a driver (so the Driver App will ALWAYS receive the existing TICKETS from the GAUGER)
-- 3.7.23	- 06/05/2015	- GSM	- DCDRV-154: Ticket Type following Origin Change
-- 3.7.23	- 06/05/2015	- GSM	- DCWEB-530 - ensure ASSIGNED orders with a driver defined are updated to DISPATCHED
-- 3.8.1	- 2015/07/04	- KDA	- purge any Driver|Gauger App ZPL related SYNC change records when order is AUDITED
-- 3.9.0	- 2015/08/20	- KDA	- add invocation of spAutoAuditOrder for DELIVERED orders (that qualify) when the Auto-Audit global setting value is TRUE
									- add invocation of spAutoApproveOrder for AUDITED orders (that qualify)
-- 3.9.2	- 2015/08/25	- KDA	- appropriately use new tblOrder.OrderDate date column
-- 3.9.4	- 2015/08/30	- KDA	- performanc optimization to prevent Orderdate computation if no timestamp fields are yet populated
-- 3.9.5	- 2015/08/31	- KDA	- more extensive performance optimizations to minimize performance ramifications or computing OrderDate dynamically from OrderRule
-- 3.9.13	- 2015/09/01	- KDA	- add (but commented out) some code to validate Arrive|Depart time being present when required
									- allow orders to be rolled back from AUDITED to DELIVERED when in AUTO-AUDIT mode
									- always AUTO UNAPPROVE orders when the status is reverted from AUDITED to DELIVERED 
-- 3.9.19	- 2015/09/23	- KDA	- remove obsolete reference to tblDriverAppPrintTicketTemplate
-- 3.9.20	- 2015/09/23	- KDA	- add support for tblOrderTransfer (do not accomplish #7 above - cloning/delete orders that are driver re-assigned)
-- 3.9.29.5 - 2015/12/03	- JAE	- added DestTrailerWaterCapacity and DestRailCarNum to dbaudit trigger
-- 3.9.38	- 2015/12/21	- BB	- Add WeightNetUnits (DCWEB-972)
-- 3.9.38	- 2016/01/11	- JAE	- Recompute net for destination weights
-- 3.10.5	- 2016/01/29	- KDA	- fix to DriverApp/Gauger App purge sync records for ineligible orders [was order.StatusID IN (4), now NOT IN (valid statuses)]
-- 3.10.5.2 - 2016/02/09	- KDA	- preserve OrderDate on OrderTransfer (driver transfer)
									- ensure Accept | Pickup | Deliver LastChangeDateUTC is updated on relevant update
        	- 2016/02/11	- JAE	- Added DispatchNotes since mobile screens display that data and any changes should trigger the view to refresh
-- 3.10.10.1- 2016/02/20	- KDA	- fix to Driver Transfer logic to prevent losing carrier if setting DriverID to NULL (not immediately assigning to another Driver)
-- 3.10.10.3- 2016/02/27	- KDA	- only update Accept|Pickup|Deliver LastChangeDate values when update NOT done by DriverApp
-- 3.11.1	- 2016/02/21	- KDA	- add support for tblDriverAppPrintFooterTemplate table
-- 3.11.17.2- 2016/04/18	- JAE	- Add producer settled orders to list of uneditable orders 
-- 3.11.20.3 (3.13.1.1 & 3.13.7) - 2016/05/09 - BB - Add Job and Contract number fields to list of fields to be copied when orders are transferred
-- 3.13.1	- 2016/07/04	- KDA	- eliminate direct auto-Audit/Approve/Rate process - instead add record to tblOrderProcessStatusChange table (for later processing)
									- add timestamp to PRINT FIRE/COMPLETED messages
-- 3.13.1.1 - 2016/07/05	- JAE	- Added back job/contract number fields. Also added to audit trigger process
-- 3.13.3	- 2016/07/10	- KDA	- only RE-ASSIGN orders after the order has been synced to a Driver at least once
-- 3.13.7   - 2016/07/05	- JAE	- Re-Added back job/contract number fields. Also re-added to audit trigger process
-- 4.1.9.2	- 2016/09/27	- JAE	- Fixed trigger to update miles when null (during create)
-- 4.4.1	- 2016/11/04	- JAE	- Add Destination Chainup
-- 4.4.2	- 2016/11/17	- JAE	- Update last changed by user/date during a reassign/clone
-- 4.4.4	- 2016.11.20	- KDA	- add db enforcement of unique Shipper PO (Order.DispatchConfirmNum) values (when required by OrderRule(Type=2)
--									- ensure this trigger is FIRST executed on INSERT/UPDATE operations on the tblOrder table (see trailing statements) - please always add these in the future
-- 4.4.5	- 2016/11/23	- JAE	- Update last changed by user/date during a reassign/clone
-- 4.5.11	- 2017/02/08	- JAE	- Set the ContractID based on shipper/product/producer/duedate/orderdate (similar to orderdate), remove ContractNumber and use FK instead
************************************************/
ALTER TRIGGER trigOrder_IU ON tblOrder AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @changesFound bit
	
		-- Note: the TRIGGER_NESTLEVEL( OBJECT_ID('trigOrder_IU') ) = 1 statement prevents recursion
	IF (TRIGGER_NESTLEVEL( OBJECT_ID('trigOrder_IU')) = 1 AND TRIGGER_NESTLEVEL( OBJECT_ID('trigOrderTicket_IU')) = 0) BEGIN

		/**********  START OF VALIDATION SECTION ************************/

		IF (UPDATE(OrderNum) 
			OR UPDATE(PriorityID) 
			OR UPDATE(DueDate) 
			OR UPDATE(RouteID) 
			OR UPDATE(OriginID) 
			OR UPDATE(OriginArriveTimeUTC) 
			OR UPDATE(OriginDepartTimeUTC) 
			OR UPDATE(OriginMinutes) 
			OR UPDATE(OriginWaitNotes) 
			OR UPDATE(OriginBOLNum) 
			OR UPDATE(OriginGrossUnits) 
			OR UPDATE(OriginNetUnits) 
			--OR UPDATE(OriginWeightGrossUnits) -- 3.9.38  Per Maverick 12/21/15 we do not need gross on the order level.
			OR UPDATE(OriginWeightNetUnits) -- 3.9.38
			OR UPDATE(DestinationID) 
			OR UPDATE(DestArriveTimeUTC) 
			OR UPDATE(DestDepartTimeUTC) 
			OR UPDATE(DestMinutes) 
			OR UPDATE(DestWaitNotes) 
			OR UPDATE(DestBOLNum) 
			OR UPDATE(DestGrossUnits) 
			OR UPDATE(DestNetUnits) 
			OR UPDATE(CustomerID) 
			OR UPDATE(CarrierID) 
			OR UPDATE(DriverID) 
			OR UPDATE(TruckID)
			OR UPDATE(TrailerID) 
			OR UPDATE(Trailer2ID) 
			OR UPDATE(OperatorID) 
			OR UPDATE(PumperID) 
			OR UPDATE(TicketTypeID) 
			OR UPDATE(Rejected) 
			OR UPDATE(RejectNotes) 
			OR UPDATE(OriginChainUp) 
			OR UPDATE(DestChainUp) 
			-- allow this to be changed even in audit status
			--OR UPDATE(OriginTruckMileage) 
			OR UPDATE(OriginTankNum) 
			-- allow this to be changed even in audit status
			--OR UPDATE(DestTruckMileage) 
			OR UPDATE(CarrierTicketNum) 
			OR UPDATE(AuditNotes) 
			OR UPDATE(CreateDateUTC) 
			-- it is permissible to change the ActualMiles value on Audited orders up to the point the Order has been Shipper-Settled
			OR (UPDATE(ActualMiles) AND EXISTS (SELECT * FROM deleted i JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = i.ID WHERE OSS.BatchID IS NOT NULL))
			OR UPDATE(ProducerID) 
			OR UPDATE(CreatedByUser) 
			OR UPDATE(LastChangeDateUTC) 
			OR UPDATE(LastChangedByUser) 
			OR UPDATE(DeleteDateUTC) 
			OR UPDATE(DeletedByUser) 
			OR UPDATE(DestProductBSW)
			OR UPDATE(DestProductGravity) 
			OR UPDATE(DestProductTemp) 
			OR UPDATE(ProductID) 
			OR UPDATE(AcceptLastChangeDateUTC) 
			OR UPDATE(PickupLastChangeDateUTC) 
			OR UPDATE(DeliverLastChangeDateUTC) 
			OR UPDATE(OriginUomID) 
			OR UPDATE(DestUomID) 
			OR UPDATE(PickupPrintStatusID) 
			OR UPDATE(DeliverPrintStatusID)
			OR UPDATE(PickupPrintDateUTC) 
			OR UPDATE(DeliverPrintDateUTC) 
			OR UPDATE(OriginTankID) 
			OR UPDATE(OriginGrossStdUnits) 
			OR UPDATE(DispatchConfirmNum) 
			OR UPDATE(DispatchNotes)
			OR UPDATE(PickupDriverNotes)
			OR UPDATE(DeliverDriverNotes)
			OR UPDATE(OriginWaitReasonID)
			OR UPDATE(DestWaitReasonID)
			OR UPDATE(RejectReasonID) 
			OR UPDATE(DestOpenMeterUnits) 
			OR UPDATE(DestCloseMeterUnits)
			OR UPDATE(DestRackBay)
			OR UPDATE(OrderDate)
			OR UPDATE(DestWeightGrossUnits) 
			OR UPDATE(DestWeightTareUnits)
			OR UPDATE(OriginSulfurContent)
		)
			--OR UPDATE(ReassignKey)) we don't do any work if this value changes so just ignore it
		BEGIN
			-- only allow the StatusID value to be changed on an audited order
			IF EXISTS (SELECT * FROM deleted WHERE StatusID = 4)
			BEGIN
				-- if the "Allow Audited Order Update (temp only)" setting is TRUE then only warn when this occurs
				IF (dbo.fnToBool(dbo.fnSettingValue(35)) = 1)
					PRINT 'AUDITED orders is being modified - please investigate why!'
				ELSE BEGIN
					-- otherwise (normal behavior) - prevent AUDITED orders from being modified (except to UN-AUDIT them)
					RAISERROR('AUDITED orders cannot be modified!', 16, 1)
					IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
					RETURN
				END
			END
		END
		ELSE IF NOT UPDATE(StatusID) -- NO CHANGES DETECTED SO JUST QUIT
			RETURN

		PRINT 'trigOrder_IU FIRED: ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
					
		-- ensure the Origin and Destinations are both specified unless the Status is:
		--   (Generated, Assigned, Dispatched or Declined)
		IF EXISTS(SELECT * FROM inserted O WHERE (O.OriginID IS NULL OR O.DestinationID IS NULL) AND O.StatusID NOT IN (-10,1,2,9))
		BEGIN
			RAISERROR('Invalid Order Status when the Origin and/or Or Destination is not specified', 16, 1)
			IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
			RETURN
		END
		/* 4.4.4 - enforce the DispatchConfirmNum (ShipperPO) is provided and is unique (per Shipper) - if it is required by the related Order Rule */
		ELSE IF (UPDATE(CustomerID) OR UPDATE(DispatchConfirmNum)) AND EXISTS (
			SELECT O.DispatchConfirmNum
			FROM inserted i 
			/* is there another undeleted order for this Shipper with the same ShipperPO value? - should not match on a NULL ShipperPO value */
			LEFT JOIN tblOrder O ON O.CustomerID = i.CustomerID AND rtrim(O.DispatchConfirmNum) = rtrim(i.DispatchConfirmNum) AND O.ID <> i.ID AND O.DeleteDateUTC IS NULL 
			WHERE i.DeleteDateUTC IS NULL 
				/* either a ShipperPO value was not supplied or it is a duplicate */
				AND (nullif(rtrim(i.DispatchConfirmNum), '') IS NULL OR O.ID IS NOT NULL)
				/* and a ShipperPO value is required for this order (at this time) */
				AND EXISTS (SELECT ID FROM fnOrderOrderRules(i.ID) OOR WHERE OOR.TypeID = 2 AND dbo.fnToBool(OOR.Value) = 1)
		) BEGIN
			RAISERROR('Shipper PO is missing or is not unique', 16, 1)
			IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
			RETURN
		END
		 
		/* this is commented out until we get the Android issues closer to resolved 
		IF EXISTS (SELECT * FROM inserted WHERE StatusID IN (8, 3, 4) AND OriginArriveTimeUTC IS NULL OR OriginDepartTimeUTC IS NULL)
		BEGIN
			RAISERROR('OriginArriveTimeUTC and/or OriginDepartTimeUTC missing when required', 16, 1)
			IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
			RETURN
		END
		IF EXISTS (SELECT * FROM inserted WHERE StatusID IN (3, 4) AND DestArriveTimeUTC IS NULL OR DestDepartTimeUTC IS NULL)
		BEGIN
			RAISERROR('DestArriveTimeUTC and/or DestDepartTimeUTC missing when required', 16, 1)
			IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
			RETURN
		END
		******************************************************************************/
		
		/**********  END OF VALIDATION SECTION ************************/

		WHILE (SELECT count(*) FROM inserted i JOIN tblOrder O ON O.ID = i.ID WHERE O.OrderNum IS NULL) > 0
		BEGIN
			UPDATE tblOrder 
			  SET OrderNum = isnull((SELECT max(OrderNum) FROM tblOrder), 100000) + 1
				, CreateDateUTC = getutcdate()
			WHERE ID = (SELECT min(O.ID) FROM tblOrder O JOIN inserted i ON i.ID = O.ID WHERE O.OrderNum IS NULL)
		END
		
		-- re-compute the OriginMinutes (in case the website failed to compute it properly)
		IF UPDATE(OriginArriveTimeUTC) OR UPDATE(OriginDepartTimeUTC)
		BEGIN
			UPDATE tblOrder SET OriginMinutes = datediff(minute, i.OriginArriveTimeUTC, i.OriginDepartTimeUTC)
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
		END
		-- re-compute the DestMinutes (in case the website failed to compute it properly)
		IF UPDATE(DestArriveTimeUTC) OR UPDATE(DestDepartTimeUTC)
		BEGIN
			UPDATE tblOrder SET DestMinutes = datediff(minute, i.DestArriveTimeUTC, i.DestDepartTimeUTC)
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
		END
		
		/* ensure any changes to the order always update the Order.OrderDate field */
		IF (UPDATE(CustomerID) 
			OR UPDATE(CarrierID) 
			OR UPDATE(ProductID) 
			OR UPDATE(OriginID) 
			OR UPDATE(DestinationID) 
			OR UPDATE(ProducerID) 
			OR UPDATE(OriginArriveTimeUTC) OR UPDATE(OriginDepartTimeUTC)
			OR UPDATE(DestArriveTimeUTC) OR UPDATE(DestDepartTimeUTC))
		BEGIN
			UPDATE tblOrder 
			  SET OrderDate = dbo.fnOrderDate(O.ID)
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
			JOIN deleted d ON d.ID = i.ID
			LEFT JOIN tblOrderSettlementCarrier OSC ON OSC.OrderID = i.ID
			LEFT JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = i.ID
			LEFT JOIN tblOrderSettlementProducer OSP ON OSP.OrderID = i.ID
			WHERE i.StatusID <> 4 
			  -- the order has at least 1 valid timestamp
			  AND (i.OriginArriveTimeUTC IS NOT NULL OR i.OriginDepartTimeUTC IS NOT NULL OR i.DestArriveTimeUTC IS NOT NULL OR i.DestDepartTimeUTC IS NOT NULL)
			  -- the order is not yet settled
			  AND OSC.BatchID IS NULL
			  AND OSS.BatchID IS NULL
			  AND OSP.BatchID IS NULL
		END
		
		/* ensure any changes to the order always update the Order.ContractID field */
		IF (UPDATE(CustomerID) 
			OR UPDATE(ProductID) 
			OR UPDATE(ProductID) 
			OR UPDATE(DueDate) OR UPDATE(OrderDate))
		BEGIN
			UPDATE tblOrder 
			  SET ContractID = dbo.fnContractID(O.CustomerID, (SELECT ProductGroupID FROM tblProduct WHERE ID = O.ProductID), O.ProducerID, ISNULL(O.OrderDate, O.DueDate))
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
			LEFT JOIN tblOrderSettlementCarrier OSC ON OSC.OrderID = i.ID
			LEFT JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = i.ID
			LEFT JOIN tblOrderSettlementProducer OSP ON OSP.OrderID = i.ID
			WHERE i.StatusID <> 4 
			  -- the order is not yet settled
			  AND OSC.BatchID IS NULL
			  AND OSS.BatchID IS NULL
			  AND OSP.BatchID IS NULL
		END

		-- ensure missing Routes are created and assigned for the specified Origin/Destination combination
		IF UPDATE(OriginID) OR UPDATE(DestinationID)
		BEGIN
			-- create any missing Route records
			INSERT INTO tblRoute (OriginID, DestinationID, CreateDateUTC, CreatedByUser)
				SELECT i.OriginID, i.DestinationID, GETUTCDATE(), ISNULL(i.LastChangedByUser, i.CreatedByUser)
				FROM inserted i
				LEFT JOIN tblRoute R ON R.OriginID = i.OriginID AND R.DestinationID = i.DestinationID
				WHERE i.OriginID IS NOT NULL AND i.DestinationID IS NOT NULL AND R.ID IS NULL
			
			-- ensure the Order records refer to the correct Route (ID)
			UPDATE tblOrder SET RouteID = R.ID
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
			LEFT JOIN tblRoute R ON R.OriginID = O.OriginID AND R.DestinationID = O.DestinationID
			WHERE O.RouteID IS NULL OR O.RouteID <> R.ID
			
			-- update the ActualMiles from the related Route
			UPDATE tblOrder SET ActualMiles = R.ActualMiles
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
			JOIN tblRoute R ON R.ID = O.RouteID
			WHERE O.ActualMiles IS NULL OR O.ActualMiles <> R.ActualMiles
		END
		
		IF (UPDATE(OriginID))
		BEGIN
			-- update Order.ProducerID/OperatorID/PumperID/SulfurContent to match what is assigned to the new Origin
			UPDATE tblOrder 
				SET TicketTypeID = OO.TicketTypeID
					, ProducerID = OO.ProducerID
					, OperatorID = OO.OperatorID
					, PumperID = OO.PumperID
					, OriginSulfurContent = OO.SulfurContent
					, LastChangeDateUTC = GETUTCDATE() 
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			WHERE d.OriginID <> O.OriginID

			-- update Order.OriginUomID to match what is assigned to the new Origin (quantities are updated below)
			UPDATE tblOrder SET OriginUomID = OO.UomID
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			WHERE d.OriginID <> O.OriginID AND d.OriginUomID <> OO.UomID
		END
		
		-- keep the DestUomID in sync with the Destination (units are updated below)
		IF (UPDATE(DestinationID))
		BEGIN
			-- update Order.DestUomID to match what is assigned to the new Destination
			UPDATE tblOrder 
			  SET DestUomID = DD.UomID
				, LastChangeDateUTC = GETUTCDATE()
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID
			JOIN tblDestination DD ON DD.ID = O.DestinationID
			WHERE d.DestinationID <> O.DestinationID AND d.DestUomID <> DD.UomID
		END
		
		-- ensure that any set PrintStatusID values are reset to 0 when backing up the general StatusID value
		IF (UPDATE(StatusID))
		BEGIN
			UPDATE tblOrder 
			  SET DeliverPrintStatusID = 0 
				, LastChangeDateUTC = GETUTCDATE()
			  FROM tblOrder O
			  JOIN deleted d ON d.ID = O.ID
			WHERE O.StatusID <> d.StatusID AND O.StatusID IN (-10, 1, 2, 7, 8) -- Generated, Assigned, Dispatched, Accepted, Picked-Up

			UPDATE tblOrder 
			  SET PickupPrintStatusID = 0 
				, LastChangeDateUTC = GETUTCDATE()
			  FROM tblOrder O
			  JOIN deleted d ON d.ID = O.ID
			WHERE O.StatusID <> d.StatusID AND O.StatusID IN (-10, 1, 2, 7) -- Generated, Assigned, Dispatched, Accepted
			
			/* ensure that any orders that are DISPATCHED - any existing orders are TOUCHED so they are syncable to the Driver App */
			UPDATE tblOrderTicket
				SET LastChangeDateUTC = GETUTCDATE()
			WHERE OrderID IN (
				SELECT i.ID 
				FROM inserted i
				JOIN deleted d ON d.ID = i.ID
				WHERE i.StatusID <> d.StatusID AND i.StatusID IN (2) -- DISPATCHED
			)
		END

		-- just updating the tickets here, trigOrderTicket_IU will in turn update this table
		-- ensure any existing OrderTickets are recomputed with the new OriginUOM
		UPDATE tblOrderTicket
		  SET GrossUnits = dbo.fnConvertUOM(GrossUnits, d.OriginUomID, O.OriginUomID)
			, NetUnits = dbo.fnConvertUOM(NetUnits, d.OriginUomID, O.OriginUomID)
			-- 3.9.38 - added to also force the WeightNetUnits be recomputed
			, WeightGrossUnits = dbo.fnConvertUOM(WeightGrossUnits, d.OriginUomID, O.OriginUomID)
			, LastChangeDateUTC = GETUTCDATE()
		FROM tblOrderTicket OT
		JOIN tblOrder O ON O.ID = OT.OrderID
		JOIN deleted d ON d.ID = O.ID
		WHERE O.OriginUomID <> d.OriginUomID
		
		-- recalculate the Destination Unit values if only if the DestUomID changed but the values didn't
		UPDATE tblOrder
		  SET DestGrossUnits = dbo.fnConvertUOM(O.DestGrossUnits, d.DestUomID, O.DestUomID)
			, DestNetUnits = dbo.fnConvertUOM(O.DestNetUnits, d.DestUomID, O.DestUomID)
			, LastChangeDateUTC = GETUTCDATE()
		FROM tblOrder O
		JOIN deleted d ON d.ID = O.ID
		WHERE d.DestUomID <> O.DestUomID 
		  AND d.DestGrossUnits = O.DestGrossUnits
		  AND d.DestNetUnits = O.DestNetUnits

		/* DCWEB-530 - ensure any ASSIGNED orders with a DRIVER assigned is set to DISPATCHED */
		UPDATE tblOrder SET StatusID = 2 /*DISPATCHED*/
		FROM tblOrder O
		JOIN inserted i ON I.ID = O.ID
		WHERE i.StatusID = 1 /*ASSIGNED*/ AND i.CarrierID IS NOT NULL AND i.DriverID IS NOT NULL AND i.DeleteDateUTC IS NULL

		/**************************************************************************************************************/
		/* 3.10.5.2 - ensure the Accept|Pickup|Deliver LastChangeDateUTC values are updated when any relevant data field changes */
		UPDATE tblOrder 
		  SET AcceptLastChangeDateUTC = LastChangeDateUTC
		WHERE ID IN (
			SELECT ID FROM (
				SELECT ID, TruckID, TrailerID, Trailer2ID, DispatchNotes FROM inserted 
				EXCEPT 
				SELECT ID, TruckID, TrailerID, Trailer2ID, DispatchNotes FROM deleted
			) X
		-- only include records that didn't have the AcceptLastChangeDateUTC explicitly updated (like from the DriverApp) - 3.10.10.3
		) AND ID IN (
			SELECT ID FROM (
				SELECT ID, AcceptLastChangeDateUTC FROM inserted INTERSECT SELECT ID, AcceptLastChangeDateUTC FROM deleted
			) X
		)
		UPDATE tblOrder 
		  SET PickupLastChangeDateUTC = LastChangeDateUTC
		WHERE ID IN (
			SELECT ID FROM (
				SELECT ID, OriginArriveTimeUTC, OriginDepartTimeUTC, OriginMinutes, OriginWaitReasonID, OriginWaitNotes
                    , CarrierTicketNum, OriginBOLNum, Rejected, RejectReasonID, RejectNotes, OriginChainup, OriginTruckMileage
                    , PickupPrintStatusID, PickupPrintDateUTC, OriginGrossUnits, OriginGrossStdUnits, OriginNetUnits, OriginWeightNetUnits
                    , PickupDriverNotes, DispatchNotes
                    , TicketTypeID 
				FROM inserted 
				EXCEPT 
				SELECT ID, OriginArriveTimeUTC, OriginDepartTimeUTC, OriginMinutes, OriginWaitReasonID, OriginWaitNotes
                    , CarrierTicketNum, OriginBOLNum, Rejected, RejectReasonID, RejectNotes, OriginChainup, OriginTruckMileage
                    , PickupPrintStatusID, PickupPrintDateUTC, OriginGrossUnits, OriginGrossStdUnits, OriginNetUnits, OriginWeightNetUnits
                    , PickupDriverNotes, DispatchNotes
                    , TicketTypeID 
				FROM deleted
			) X
		-- only include records that didn't have the PickupLastChangeDateUTC explicitly updated (like from the DriverApp) - 3.10.10.3
		) AND ID IN (
			SELECT ID FROM (
				SELECT ID, PickupLastChangeDateUTC FROM inserted INTERSECT SELECT ID, PickupLastChangeDateUTC FROM deleted
			) X
		)
		UPDATE tblOrder 
		  SET DeliverLastChangeDateUTC = LastChangeDateUTC
		WHERE ID IN (
			SELECT ID FROM (
				SELECT ID, DeliverLastChangeDateUTC, DestArriveTimeUTC, DestDepartTimeUTC, DestMinutes, DestWaitReasonID, DestWaitNotes
                    , DestNetUnits, DestGrossUnits, DestChainup
                    , DestBOLNum, DestRailcarNum, DestTrailerWaterCapacity, DestOpenMeterUnits, DestCloseMeterUnits, DestProductBSW
                    , DestProductGravity, DestProductTemp, DestTruckMileage 
                    , DeliverPrintStatusID, DeliverPrintDateUTC, DeliverDriverNotes, DestRackBay
                    , DestWeightGrossUnits, DestWeightTareUnits, DestWeightNetUnits
                    , DispatchNotes 
				FROM inserted
				EXCEPT
				SELECT ID, DeliverLastChangeDateUTC, DestArriveTimeUTC, DestDepartTimeUTC, DestMinutes, DestWaitReasonID, DestWaitNotes
                    , DestNetUnits, DestGrossUnits, DestChainup
                    , DestBOLNum, DestRailcarNum, DestTrailerWaterCapacity, DestOpenMeterUnits, DestCloseMeterUnits, DestProductBSW
                    , DestProductGravity, DestProductTemp, DestTruckMileage 
                    , DeliverPrintStatusID, DeliverPrintDateUTC, DeliverDriverNotes, DestRackBay
                    , DestWeightGrossUnits, DestWeightTareUnits, DestWeightNetUnits 
                    , DispatchNotes
				FROM deleted
			) X
		-- only include records that didn't have the DeliverLastChangeDateUTC explicitly updated (like from the DriverApp) - 3.10.10.3
		) AND ID IN (
			SELECT ID FROM (
				SELECT ID, DeliverLastChangeDateUTC FROM inserted INTERSECT SELECT ID, DeliverLastChangeDateUTC FROM deleted
			) X
		)
		/**************************************************************************************************************/

		-- 3.9.38 - 2016/01/11 - JAE - recompute the Net Weight of any changed Mineral Run orders
		IF UPDATE(DestWeightGrossUnits) OR UPDATE(DestWeightTareUnits) OR UPDATE(DestUomID) BEGIN
			UPDATE tblOrder
				SET DestWeightNetUnits = O.DestWeightGrossUnits - O.DestWeightTareUnits
			FROM tblOrder O
			JOIN tblDestination D ON D.ID = O.DestinationID
			JOIN inserted i on i.ID = O.ID
			WHERE D.TicketTypeID = 9 -- Mineral Run tickets only
		END 			

		/*************************************************************************************************************/
		/* DRIVER RE-ASSIGNMENT - handle driver changes - "orphan" the order/ticket (with a new ORDERNUM) - and clone it for the new driver */
		IF (UPDATE(DriverID))
		BEGIN
			-- create cloned order/ticket records (these records will be cloned then the original reset to orphaned status)
			SELECT O.*, NewOrderNum = CAST(NULL as int)
			INTO #clone
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID AND d.DriverID IS NOT NULL AND d.DriverID <> isnull(O.DriverID, 0)
			/* 3.13.3 - only RE-ASSIGN orders AFTER they have been SYNCED at least once to a DRIVER (APP) */
			JOIN tblOrderDriverSynced ODS ON ODS.OrderID = O.ID
			/* JOIN to tblOrderTransfer so we can prevent treating an OrderTransfer "driver change" as a Orphaned Order */
			LEFT JOIN tblOrderTransfer OTR ON OTR.OrderID = O.ID  /* 3.9.20 - added */
			WHERE OTR.OrderID IS NULL

			/* generate the next OrderNum values for these new records */
			DECLARE @newOrderNum int; SET @newOrderNum = isnull((SELECT max(OrderNum) FROM tblOrder), 100000) + 1;
			DECLARE @newReassignKey int; SET @newReassignKey = isnull((SELECT max(ReassignKey) FROM tblOrder), 0) + 1;
			WHILE (SELECT count(*) FROM #clone WHERE NewOrderNum IS NULL) > 0
			BEGIN
				UPDATE #clone
				  SET NewOrderNum = @newOrderNum, ReassignKey = isnull(ReassignKey, @newReassignKey)
					, CreateDateUTC = getutcdate(), CreatedByUser = LastChangedByUser
					, LastChangeDateUTC = null, LastChangedByUser = null
				WHERE ID = (SELECT min(ID) FROM #clone WHERE NewOrderNum IS NULL)
				-- if the @newReassignKey wasn't used above, then that # will be skipped, but this should not be an issue
				SELECT @newOrderNum = @newOrderNum + 1, @newReassignKey = @newReassignKey + 1
			END
			
			-- reset the existing records as orphaned
			UPDATE tblOrder
				SET DeleteDateUTC = isnull(O.DeleteDateUTC, GETUTCDATE()), DeletedByUser = isnull(O.DeletedByUser, O.LastChangedByUser)
					, CarrierID = d.CarrierID, DriverID = d.DriverID, TruckID = d.TruckID, TrailerID = d.TrailerID, Trailer2ID = d.Trailer2ID
					, DispatchConfirmNum = NULL
					, ReassignKey = C.ReassignKey
			FROM tblOrder O
			JOIN #clone C ON C.ID = O.ID
			JOIN deleted d on D.ID = O.id

			-- create the cloned records (which is essentially the original with a new ID and the Carrrier & default Truck|Trailers of the new driver)
			INSERT INTO tblOrder (OrderDate, OrderNum, StatusID, PriorityID, DueDate, RouteID, OriginID, OriginArriveTimeUTC, OriginDepartTimeUTC
				, OriginMinutes, OriginWaitNotes, OriginBOLNum, OriginGrossUnits, OriginNetUnits, DestinationID, DestArriveTimeUTC, DestDepartTimeUTC
				, DestMinutes, DestWaitNotes, DestBOLNum, DestGrossUnits, DestNetUnits, CustomerID, CarrierID, DriverID, TruckID, TrailerID
				, Trailer2ID, OperatorID, PumperID, TicketTypeID, Rejected, RejectNotes, OriginChainUp, OriginTruckMileage, OriginTankNum, DestTruckMileage
				, CarrierTicketNum, AuditNotes, ActualMiles, ProducerID, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser
				, DeleteDateUTC, DeletedByUser, DestProductBSW, DestProductGravity, DestProductTemp, ProductID, AcceptLastChangeDateUTC
				, PickupLastChangeDateUTC, DeliverLastChangeDateUTC, OriginUomID, DestUomID, PickupPrintStatusID, DeliverPrintStatusID
				, PickupPrintDateUTC, DeliverPrintDateUTC, OriginTankID, OriginGrossStdUnits, DispatchNotes, PickupDriverNotes, DeliverDriverNotes
				, DispatchConfirmNum, OriginWaitReasonID, DestWaitReasonID, RejectReasonID, DestOpenMeterUnits, DestCloseMeterUnits
				, OriginWeightNetUnits, ReassignKey, JobNumber, OriginSulfurContent, DestChainup)
				SELECT OrderDate, NewOrderNum, StatusID, PriorityID, DueDate, RouteID, OriginID, OriginArriveTimeUTC, OriginDepartTimeUTC, OriginMinutes, OriginWaitNotes
					, OriginBOLNum, OriginGrossUnits, OriginNetUnits, DestinationID, DestArriveTimeUTC, DestDepartTimeUTC, DestMinutes, DestWaitNotes
					, DestBOLNum, DestGrossUnits, DestNetUnits, CustomerID, isnull(D.CarrierID, O.CarrierID), DriverID, D.TruckID, D.TrailerID, D.Trailer2ID, OperatorID
					, PumperID, TicketTypeID, Rejected, RejectNotes, OriginChainUp, OriginTruckMileage, OriginTankNum, DestTruckMileage, CarrierTicketNum
					, AuditNotes, ActualMiles, ProducerID, O.CreateDateUTC, O.CreatedByUser, null, null, null, null
					, DestProductBSW, DestProductGravity, DestProductTemp, ProductID, AcceptLastChangeDateUTC, PickupLastChangeDateUTC, DeliverLastChangeDateUTC
					, OriginUomID, DestUomID, PickupPrintStatusID, DeliverPrintStatusID, PickupPrintDateUTC, DeliverPrintDateUTC, OriginTankID
					, OriginGrossStdUnits, DispatchNotes, PickupDriverNotes, DeliverDriverNotes, DispatchConfirmNum, OriginWaitReasonID, DestWaitReasonID
					, RejectReasonID, DestOpenMeterUnits, DestCloseMeterUnits, OriginWeightNetUnits, ReassignKey, JobNumber, OriginSulfurContent, DestChainup
				FROM #clone O
				LEFT JOIN tblDriver D ON D.ID = O.DriverID
		
			-- cache the affected tickets
			SELECT OT.*, C.NewOrderNum, OrderDeletedByUser = C.LastChangedByUser
			INTO #cloneTicket
			FROM tblOrderTicket OT
			JOIN #clone C ON C.ID = OT.OrderID
			WHERE OT.DeleteDateUTC IS NULL
			
			-- create the cloned Order Ticket records (copy of existing ticket(s) pointed at new "original" Order)
			INSERT INTO tblOrderTicket (OrderID, CarrierTicketNum, TicketTypeID, TankNum, ProductObsGravity, ProductObsTemp, ProductBSW, OpeningGaugeFeet
				, OpeningGaugeInch, OpeningGaugeQ, ClosingGaugeFeet, ClosingGaugeInch, ClosingGaugeQ, GrossUnits, NetUnits, Rejected, RejectNotes
				, SealOff, SealOn, BOLNum, ProductHighTemp, ProductLowTemp, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser
				, DeleteDateUTC, DeletedByUser, UID, FromMobileApp, OriginTankID, BottomFeet, BottomInches, BottomQ, GrossStdUnits, RejectReasonID
				, MeterFactor, OpenMeterUnits, CloseMeterUnits, WeightTareUnits, WeightGrossUnits, WeightNetUnits)
				SELECT O.ID, CT.CarrierTicketNum, CT.TicketTypeID, CT.TankNum, CT.ProductObsGravity, CT.ProductObsTemp, CT.ProductBSW, CT.OpeningGaugeFeet
					, CT.OpeningGaugeInch, CT.OpeningGaugeQ, CT.ClosingGaugeFeet, CT.ClosingGaugeInch, CT.ClosingGaugeQ, CT.GrossUnits, CT.NetUnits
					, CT.Rejected, CT.RejectNotes, CT.SealOff, CT.SealOn, CT.BOLNum, CT.ProductHighTemp, CT.ProductLowTemp, GETUTCDATE(), CT.OrderDeletedByUser
					, null, null, null, null, newid(), CT.FromMobileApp, CT.OriginTankID, CT.BottomFeet
					, CT.BottomInches, CT.BottomQ, CT.GrossStdUnits, CT.RejectReasonID, CT.MeterFactor, CT.OpenMeterUnits, CT.CloseMeterUnits, WeightTareUnits
					, WeightGrossUnits, WeightNetUnits
				FROM #cloneTicket CT
				JOIN tblOrder O ON O.OrderNum = CT.NewOrderNum
		END
		/*************************************************************************************************************/
		
		/* HANDLE TRUCK | TRAILER | TRAILER2 default changes when the driver updates his Truck | Trailer | Trailer2 on ACCEPTANCE */
		-- TRUCK
		IF (UPDATE(TruckID) AND EXISTS (
			SELECT i.* 
			FROM inserted i 
			JOIN deleted d ON d.ID = i.ID
			  AND i.DriverID = d.DriverID 
			  AND i.TruckID <> d.TruckID
			WHERE i.StatusID IN (7, 8, 3) -- ACCEPTED, PICKEDUP, DELIVERED
			  AND d.StatusID IN (2))) -- DISPATCHED
		BEGIN
			UPDATE tblDriver
			  SET TruckID = i.TruckID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblDriver DR
			JOIN inserted i ON i.DriverID = DR.ID
			JOIN deleted d ON d.DriverID = DR.ID
			WHERE i.TruckID <> d.TruckID
			
			UPDATE tblOrder
			  SET TruckID = i.TruckID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblOrder O
			JOIN inserted i ON i.DriverID = O.DriverID 
			  AND O.ID <> i.ID
			  AND O.StatusID IN (2) -- DISPATCHED
			  AND i.TruckID <> O.TruckID
			  AND O.DeleteDateUTC IS NULL
		END
		-- TRAILER
		IF (UPDATE(TrailerID) AND EXISTS (
			SELECT i.* 
			FROM inserted i 
			JOIN deleted d ON d.ID = i.ID
			  AND i.DriverID = d.DriverID 
			  AND i.TrailerID <> d.TrailerID
			WHERE i.StatusID IN (7, 8, 3) -- ACCEPTED, PICKEDUP, DELIVERED
			  AND d.StatusID IN (2))) -- DISPATCHED
		BEGIN
			UPDATE tblDriver
			  SET TrailerID = i.TrailerID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblDriver DR
			JOIN inserted i ON i.DriverID = DR.ID
			JOIN deleted d ON d.DriverID = DR.ID
			WHERE i.TrailerID <> d.TrailerID
			
			UPDATE tblOrder
			  SET TrailerID = i.TrailerID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblOrder O
			JOIN inserted i ON i.DriverID = O.DriverID 
			  AND O.ID <> i.ID
			  AND O.StatusID IN (2) -- DISPATCHED
			  AND i.TrailerID <> O.TrailerID
			  AND O.DeleteDateUTC IS NULL
		END
		-- TRAILER 2
		IF (UPDATE(Trailer2ID) AND EXISTS (
			SELECT i.* 
			FROM inserted i 
			JOIN deleted d ON d.ID = i.ID
			  AND i.DriverID = d.DriverID 
			  AND isnull(i.Trailer2ID, 0) <> isnull(d.Trailer2ID, 0)
			WHERE i.StatusID IN (7, 8, 3) -- ACCEPTED, PICKEDUP, DELIVERED
			  AND d.StatusID IN (2))) -- DISPATCHED
		BEGIN
			UPDATE tblDriver
			  SET TrailerID = i.TrailerID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblDriver DR
			JOIN inserted i ON i.DriverID = DR.ID
			JOIN deleted d ON d.DriverID = DR.ID
			WHERE isnull(i.Trailer2ID, 0) <> isnull(d.Trailer2ID, 0)
			
			UPDATE tblOrder
			  SET Trailer2ID = i.Trailer2ID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblOrder O
			JOIN inserted i ON i.DriverID = O.DriverID 
			  AND O.ID <> i.ID
			  AND O.StatusID IN (2) -- DISPATCHED
			  AND isnull(i.Trailer2ID, 0) <> isnull(O.Trailer2ID, 0)
			  AND O.DeleteDateUTC IS NULL
		END
		/*************************************************************************************************************/
		
		-- record any OrderProcessStatusChange records	
		IF (UPDATE(StatusID) OR UPDATE(DeliverPrintStatusID))
			INSERT INTO tblOrderProcessStatusChange (OrderID, OldStatusID, NewStatusID, StatusChangeDateUTC)
				SELECT i.ID, d.StatusID, i.StatusID, getutcdate()
				FROM inserted i
				JOIN tblPrintStatus iPS ON iPS.ID = i.DeliverPrintStatusID
				JOIN deleted d ON d.ID = i.ID
				JOIN tblPrintStatus dPS ON dPS.ID = d.DeliverPrintStatusID
				WHERE (i.StatusID = 3 AND d.StatusID <> 4 AND iPS.IsCompleted = 1 AND i.StatusID + iPS.IsCompleted <> d.StatusID + dPS.IsCompleted) -- just DELIVERED orders
				  OR (i.StatusID = 4 AND d.StatusID <> 4)	-- just AUDITED orders
				  OR (d.StatusID = 4 AND i.StatusID <> 4)	-- just UNAUDITED orders
			
		/*************************************************************************************************************/

		-- purge any DriverApp/Gauger App sync records for any orders that are not in an DRIVER APP eligible status
		IF UPDATE(StatusID) AND EXISTS (SELECT ID FROM inserted WHERE StatusID NOT IN (2,7,8,3))
		BEGIN
			DELETE FROM tblDriverAppPrintHeaderImageSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID NOT IN (2,7,8,3))
			DELETE FROM tblDriverAppPrintPickupTemplateSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID NOT IN (2,7,8,3))
			DELETE FROM tblDriverAppPrintDeliverTemplateSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID NOT IN (2,7,8,3))
			DELETE FROM tblDriverAppPrintFooterTemplateSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID NOT IN (2,7,8,3))
			DELETE FROM tblOrderAppChanges WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID NOT IN (2,7,8,3))
		END
		-- purge any DriverApp/Gauger App sync records for any orders that are not in a GAUGER APP eligible status
		IF UPDATE(StatusID) AND EXISTS (SELECT ID FROM inserted WHERE StatusID NOT IN (-9,-11))
		BEGIN
			DELETE FROM tblGaugerAppPrintHeaderImageSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID NOT IN (-9,-11))
			DELETE FROM tblGaugerAppPrintPickupTemplateSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID NOT IN (-9,-11))
			DELETE FROM tblGaugerAppPrintTicketTemplateSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID NOT IN (-9,-11))
		END
				
		--NOTE: we do not update the DestOpenMeterUnits/DestCloseMeterUnits since they don't auto update
		--  (and they simply represent a reading that is assumed to in the DestUomID UOM at entry)
		--  (if they are updated in the future [manually] the will calculate again using the DestUomID at that time)

		IF EXISTS (SELECT * FROM inserted EXCEPT SELECT * FROM deleted)
		BEGIN
			-- optionally add tblOrderDBAudit records
			BEGIN TRY
				IF EXISTS(SELECT * FROM tblSetting WHERE ID = 32 AND (Value LIKE 'true' OR Value LIKE 'yes'))
					INSERT INTO tblOrderDbAudit (DBAuditDate, ID, OrderNum, StatusID, PriorityID, DueDate, RouteID, OriginID
						, OriginArriveTimeUTC, OriginDepartTimeUTC, OriginMinutes, OriginWaitNotes, OriginBOLNum, OriginGrossUnits
						, OriginNetUnits, DestinationID, DestArriveTimeUTC, DestDepartTimeUTC, DestMinutes, DestWaitNotes, DestBOLNum
						, DestGrossUnits, DestNetUnits, CustomerID, CarrierID, DriverID, TruckID, TrailerID, Trailer2ID, OperatorID
						, PumperID, TicketTypeID, Rejected, RejectNotes, OriginChainUp, OriginTruckMileage, OriginTankNum, DestTruckMileage
						, CarrierTicketNum, AuditNotes, CreateDateUTC, ActualMiles, ProducerID, CreatedByUser, LastChangeDateUTC
						, LastChangedByUser, DeleteDateUTC, DeletedByUser, DestProductBSW, DestProductGravity, DestProductTemp
						, ProductID, AcceptLastChangeDateUTC, PickupLastChangeDateUTC, DeliverLastChangeDateUTC, OriginUomID, DestUomID
						, PickupPrintStatusID, DeliverPrintStatusID, PickupPrintDateUTC, DeliverPrintDateUTC, OriginTankID
						, OriginGrossStdUnits, DispatchNotes, PickupDriverNotes, DeliverDriverNotes, DispatchConfirmNum, OriginWaitReasonID
						, DestWaitReasonID, RejectReasonID, DestOpenMeterUnits, DestCloseMeterUnits, ReassignKey, DestRackBay, OrderDate
						, DestTrailerWaterCapacity, DestRailCarNum, /*OriginWeightGrossUnits,*/ OriginWeightNetUnits, DestWeightGrossUnits
						, DestWeightTareUnits, DestWeightNetUnits, JobNumber, ContractID, OriginSulfurContent, DestChainup)
						SELECT GETUTCDATE(), ID, OrderNum, StatusID, PriorityID, DueDate, RouteID, OriginID, OriginArriveTimeUTC
							, OriginDepartTimeUTC, OriginMinutes, OriginWaitNotes, OriginBOLNum, OriginGrossUnits, OriginNetUnits
							, DestinationID, DestArriveTimeUTC, DestDepartTimeUTC, DestMinutes, DestWaitNotes, DestBOLNum, DestGrossUnits
							, DestNetUnits, CustomerID, CarrierID, DriverID, TruckID, TrailerID, Trailer2ID, OperatorID, PumperID
							, TicketTypeID, Rejected, RejectNotes, OriginChainUp, OriginTruckMileage, OriginTankNum, DestTruckMileage
							, CarrierTicketNum, AuditNotes, CreateDateUTC, ActualMiles, ProducerID, CreatedByUser, LastChangeDateUTC
							, LastChangedByUser, DeleteDateUTC, DeletedByUser, DestProductBSW, DestProductGravity, DestProductTemp
							, ProductID, AcceptLastChangeDateUTC, PickupLastChangeDateUTC, DeliverLastChangeDateUTC, OriginUomID
							, DestUomID, PickupPrintStatusID, DeliverPrintStatusID, PickupPrintDateUTC, DeliverPrintDateUTC, OriginTankID
							, OriginGrossStdUnits, DispatchNotes, PickupDriverNotes, DeliverDriverNotes, DispatchConfirmNum
							, OriginWaitReasonID, DestWaitReasonID, RejectReasonID, DestOpenMeterUnits, DestCloseMeterUnits, ReassignKey
							, DestRackBay, OrderDate, DestTrailerWaterCapacity, DestRailCarNum, /*OriginWeightGrossUnits,*/ OriginWeightNetUnits
							, DestWeightGrossUnits, DestWeightTareUnits, DestWeightNetUnits, JobNumber, ContractID, OriginSulfurContent, DestChainup
						FROM deleted d
			END TRY
			BEGIN CATCH
				PRINT 'trigOrder_IU.DBAUDIT FAILURE: ' + ERROR_MESSAGE()
			END CATCH
		END
		
		PRINT 'trigOrder_IU COMPLETE: ' + (CONVERT(VARCHAR(24), GETDATE(), 121))

	END
END
GO

/*** NOTE: !!!!! PLEASE ENSURE YOU PRESERVE THESE LINES IF YOU ALTER trigOrder_IU - this causes the trigger to execute FIRST !!!!!!! ****/
EXEC sp_settriggerorder @triggername=N'trigOrder_IU', @order=N'First', @stmttype=N'INSERT'
GO
EXEC sp_settriggerorder @triggername=N'trigOrder_IU', @order=N'First', @stmttype=N'UPDATE'
GO


-- --------------------------------
-- Deal with existing Contract numbers
-- --------------------------------

-- replace job number with contract number
UPDATE tblOrder SET JobNumber = ContractNumber, ContractNumber = NULL
WHERE ContractNumber IS NOT NULL AND ContractNumber <> ''
AND (JobNumber IS NULL OR JobNumber = '')
GO

-- blast away contract number since job number is set
UPDATE tblOrder SET ContractNumber = NULL
WHERE ContractNumber IS NOT NULL AND ContractNumber <> ''
AND JobNumber <> ''
GO

-- clear blank contract numbers
UPDATE tblOrder SET ContractNumber = NULL 
WHERE ContractNumber = ''
GO


-- --------------------------------
-- Remove old ContractNumber column (replaced with FK ContractID)
-- --------------------------------

ALTER TABLE tblOrder DROP COLUMN ContractNumber
GO

ALTER TABLE tblOrderDbAudit DROP COLUMN ContractNumber
GO

exec sp_refreshview viewOrderBase -- pull in the new contractID
GO

/***************************************/
-- Date Created: ?.?.? - 2012.11.25 - Kevin Alons
-- Purpose: return Order records with "translated friendly" values for FK relationships
-- Changes:
-- 3.9.20  - 2015/10/22 - KDA	- add Origin|Dest DriverID fields (using new tblOrderTransfer table) 
--			2015/10/28  - JAE	- added all Order Transfer fields for ease of use in reporting
--			2015/11/03  - BB	- added cast to make TransferComplet BIT type to avoid "Operand type clash" error when running this update script
-- 3.9.21  - 2015/11/03 - KDA	- add OriginDriverGroupID field (from OrderTransfer.OriginDriverID JOIN)
-- 3.9.25  - 2015/11/10 - JAE	- added origin driver and truck to view
-- 3.10.10 - 2016/02/15 - BB	- Add driver region ID to facilitate new user/region filtering feature
-- 3.10.11 - 2016/02/24 - JAE	- Add destination region (name) to view
-- 3.10.13 - 2016/02/29 - JAE	- Add TruckType
-- 3.13.8  - 2016/07/26	- BB	- Add Destination Station
-- 4.1.0.2 - 2016.08.28 - KDA	- rewrite RerouteCount, TicketCount to use a normal JOIN (instead of uncorrelated sub-query)
--								- use viewDriverBase instead of viewDriver
--								- eliminate viewGaugerBase (was unused), use tblOrderStatus instead of viewOrderPrintStatus
-- 4.1.3.3	- 2016/09/14 - BB	- Add origin and destination driving directions (initially for dispatch and truck order create pages)
-- 4.1.8.6	- 2016.09.24 - KDA	- move H2S | TicketCount | RerouteCount fields from viewOrder down to viewOrderBase
-- 4.5.0	- 2017/01/26 - BSB/JAE	- Add driver terminal
-- 4.5.11	- 2017/02/08 - JAE	- Add Contract info
/***************************************/
ALTER VIEW viewOrder AS
SELECT O.*
	-- GENERATED orders that were GAUGED to COMPLETION or SKIPPED will show "Gauger [gauger status]"
	, PrintStatus = CASE WHEN O.StatusID = -10 AND GOS.IsComplete = 1 THEN 'Gauger ' + GOS.Name ELSE OPS.OrderStatus END
FROM (
	SELECT O.*
	, Origin = vO.Name 
	, OriginFull = vO.FullName 
	, OriginState = vO.State 
	, OriginStateAbbrev = vO.StateAbbrev 
	, OriginStation = vO.Station
	, OriginCounty = vO.County 
	, OriginDrivingDirections = vO.DrivingDirections	-- 4.1.3.3
	, vO.LeaseName
	, vO.LeaseNum
	, OriginTerminal = vO.Terminal
	, OriginRegion = vO.Region
	, OriginCountryID = vO.CountryID
	, OriginCountry = vO.Country
	, OriginCountryShort = vO.CountryShort
	, OriginLegalDescription = vO.LegalDescription 
	, OriginNDIC = vO.NDICFileNum 
	, OriginNDM = vO.NDM 
	, OriginCA = vO.CA 
	, OriginTimeZoneID = vO.TimeZoneID 
	, OriginUseDST = vO.UseDST 
	, Destination = vD.Name 
	, DestinationFull = vD.FullName 
	, DestinationState = vD.State 
	, DestinationStateAbbrev = vD.StateAbbrev 
	, DestinationDrivingDirections = vD.DrivingDirections	-- 4.1.3.3
	, DestinationTypeID = vD.ID
	, DestinationStation = vD.Station -- 3.13.8
	, vD.DestinationType
	, DestStation = vD.Station 
	, DestTimeZoneID = vD.TimeZoneID 
	, DestUseDST = vD.UseDST 
	, DestTerminal = vD.Terminal
	, DestRegion = vD.Region -- 3.10.13
	, DestCountryID = vD.CountryID
	, DestCountry = vD.Country
	, DestCountryShort = vD.CountryShort
	, Customer = C.Name 
	, Carrier = CA.Name 
	, CarrierType = CT.Name 
	, OS.OrderStatus
	, OS.StatusNum
	, Driver = D.FullName 
	, DriverFirst = D.FirstName 
	, DriverLast = D.LastName 
	, DriverRegionID = D.RegionID  -- 3.10.10
	, DriverTerminalID = D.TerminalID
	, DriverTerminal = D.Terminal
	, Truck = TRU.FullName 
	, Trailer = TR1.FullName 
	, TrailerTerminalID = TR1.TerminalID
	, TrailerTerminal = TR1.Terminal
	, Trailer2 = TR2.FullName 
	, Trailer2TerminalID = TR2.TerminalID
	, Trailer2Terminal = TR2.Terminal
	, P.PriorityNum
	, TicketType = TT.Name 
	, DestTicketTypeID = vD.TicketTypeID 
	, DestTicketType = vD.TicketType 
	, Operator = OP.Name 
	, Producer = PR.Name 
	, Pumper = PU.FullName 
	, DriverNumber = D.IDNumber
	, CarrierNumber = CA.IDNumber
	, CarrierTypeID = CA.CarrierTypeID
	, TruckNumber = TRU.IDNumber
	, TruckType = TRU.TruckType 
	, TrailerNumber = TR1.IDNumber 
	, Trailer2Number = TR2.IDNumber 
	, Product = PRO.Name
	, ProductShort = PRO.ShortName 
	, PRO.ProductGroup
	, OriginUOM = OUom.Name 
	, OriginUomShort = OUom.Abbrev 
	, OriginTankID_Text = coalesce(ORT.TankNum, O.OriginTankNum, '?')
	, DestUOM = DUom.Name 
	, DestUomShort = DUom.Abbrev 
	, Active = CAST((CASE WHEN O.DeleteDateUTC IS NULL THEN 1 ELSE 0 END) AS BIT) 
	, IsDeleted = CAST((CASE WHEN O.DeleteDateUTC IS NOT NULL THEN 1 ELSE 0 END) AS BIT) 
	, PickupPrintStatus = PPS.Name 
	, PickupCompleted = PPS.IsCompleted 
	, DeliverPrintStatus = DPS.Name 
	, DeliverCompleted = DPS.IsCompleted 
	, PrintStatusID = CASE WHEN O.StatusID = -9 THEN ISNULL(-GAO.StatusID, O.StatusID)
						   WHEN O.StatusID = 8 AND PPS.IsCompleted = 0 THEN 7
						   WHEN O.StatusID = 3 AND DPS.IsCompleted = 0 THEN 8
						ELSE O.StatusID END 
	, OriginTankText = CASE WHEN O.OriginTankID IS NULL OR ORT.TankNum = '*' THEN O.OriginTankNum ELSE ORT.TankNum END
	, OriginWaitNum = OWR.Num
	, OriginWaitDesc = OWR.Description
	, OriginWaitNumDesc = OWR.NumDesc
	, DestWaitNum = DWR.Num
	, DestWaitDesc = DWR.Description
	, DestWaitNumDesc = DWR.NumDesc
	, RejectNum = ORR.Num
	, RejectDesc = ORR.Description
	, RejectNumDesc = ORR.NumDesc
	, TotalMinutes = ISNULL(OriginMinutes, 0) + ISNULL(DestMinutes, 0)
	-- TRANSFER FIELDS
	, OriginDriverID = ISNULL(OTR.OriginDriverID, O.DriverID)
	, OriginDriverGroupID = ISNULL(ODG.ID, DDG.ID) 
	, OriginDriverGroup = ISNULL(ODG.Name, DDG.Name)
	, OriginDriver = ISNULL(vODR.FullName, vDDR.FullName)
	, OriginDriverFirst = ISNULL(vODR.FirstName, vDDR.FirstName)
	, OriginDriverLast = ISNULL(vODR.LastName, vDDR.LastName)
	, OriginTruckID = ISNULL(OTR.OriginTruckID, O.TruckID)
	, OriginTruck = ISNULL(vOTRU.FullName, vDTRU.FullName)
	, OriginTruckTerminalID = ISNULL(vOTRU.TerminalID, vDTRU.TerminalID)
	, OriginTruckTerminal = ISNULL(vOTRU.Terminal, vDTRU.Terminal)
	, OriginDriverTerminalID = ISNULL(vODR.TerminalID, D.TerminalID)
	, OriginDriverTerminal = ISNULL(vODR.Terminal, D.Terminal)
	, DestDriverID = CASE WHEN OTR.OrderID IS NULL THEN NULL ELSE O.DriverID END
	, DestDriver = CASE WHEN OTR.OrderID IS NULL THEN NULL ELSE D.FullName END
	, DestDriverFirst = CASE WHEN OTR.OrderID IS NULL THEN NULL ELSE D.FirstName END
	, DestDriverLast = CASE WHEN OTR.OrderID IS NULL THEN NULL ELSE D.LastName END
	, DestTruckID = CASE WHEN OTR.OrderID IS NULL THEN NULL ELSE O.TruckID END
	, DestTruck = CASE WHEN OTR.OrderID IS NULL THEN NULL ELSE TRU.FullName END
	, DestTruckTerminalID = CASE WHEN OTR.OrderID IS NULL THEN NULL ELSE TRU.TerminalID END
	, DestTruckTerminal = CASE WHEN OTR.OrderID IS NULL THEN NULL ELSE TRU.Terminal END
	, DestDriverTerminalID = CASE WHEN OTR.OrderID IS NULL THEN NULL ELSE D.TerminalID END
	, DestDriverTerminal = CASE WHEN OTR.OrderID IS NULL THEN NULL ELSE D.Terminal END
	, OriginTruckEndMileage = OTR.OriginTruckEndMileage
	, DestTruckStartMileage = OTR.DestTruckStartMileage
	, TransferPercent = OTR.PercentComplete
	, TransferNotes = OTR.Notes
	, TransferDateUTC = OTR.CreateDateUTC
	, TransferComplete = CAST(OTR.TransferComplete AS BIT)
	, IsTransfer = CAST((CASE WHEN OTR.OrderID IS NOT NULL THEN 1 ELSE 0 END) AS BIT)	
	-- 
	, GaugerStatusID = GAO.StatusID
	, GaugerID = GAO.GaugerID	
	--
	, ContractNumber = CN.ContractNumber
	, ContractPrintDescription = CN.PrintDescription
	, ContractNotes = CN.Notes
	, ContractUomID = CN.UomID
	, ContractVolume = CN.Units
	FROM viewOrderBase O
	JOIN tblOrderStatus OS ON OS.ID = O.StatusID
	LEFT JOIN tblOrderTransfer OTR ON OTR.OrderID = O.ID
	LEFT JOIN viewOrigin vO ON vO.ID = O.OriginID
	LEFT JOIN viewDestination vD ON vD.ID = O.DestinationID
	LEFT JOIN tblCustomer C ON C.ID = O.CustomerID
	LEFT JOIN tblCarrier CA ON CA.ID = O.CarrierID
	LEFT JOIN tblCarrierType CT ON CT.ID = CA.CarrierTypeID
	LEFT JOIN viewDriverBase D ON D.ID = O.DriverID
	LEFT JOIN viewDriverBase vODR ON vODR.ID = OTR.OriginDriverID
	LEFT JOIN viewDriver vDDR ON vDDR.ID = O.DriverID
	LEFT JOIN tblDriverGroup ODG ON ODG.ID = vODR.DriverGroupID
	LEFT JOIN tblDriverGroup DDG ON DDG.ID = O.DriverGroupID
	LEFT JOIN viewTruck TRU ON TRU.ID = O.TruckID
	LEFT JOIN viewTruck vOTRU ON vOTRU.ID = OTR.OriginTruckID
	LEFT JOIN viewTruck vDTRU ON vDTRU.ID = O.TruckID
	LEFT JOIN viewTrailer TR1 ON TR1.ID = O.TrailerID
	LEFT JOIN viewTrailer TR2 ON TR2.ID = O.Trailer2ID
	LEFT JOIN tblPriority P ON P.ID = O.PriorityID
	LEFT JOIN tblTicketType TT ON TT.ID = O.TicketTypeID
	LEFT JOIN tblOriginTank ORT ON ORT.ID = O.OriginTankID
	LEFT JOIN tblOperator OP ON OP.ID = O.OperatorID
	LEFT JOIN viewPumper PU ON PU.ID = O.PumperID
	LEFT JOIN tblProducer PR ON PR.ID = O.ProducerID
	LEFT JOIN viewProduct PRO ON PRO.ID = O.ProductID
	LEFT JOIN tblUom OUom ON OUom.ID = O.OriginUomID
	LEFT JOIN tblUom DUom ON DUom.ID = O.DestUomID
	LEFT JOIN tblPrintStatus PPS ON PPS.ID = O.PickupPrintStatusID
	LEFT JOIN tblPrintStatus DPS ON DPS.ID = O.DeliverPrintStatusID
	LEFT JOIN viewOriginWaitReason OWR ON OWR.ID = O.OriginWaitReasonID
	LEFT JOIN viewDestinationWaitReason DWR ON DWR.ID = O.DestWaitReasonID
	LEFT JOIN viewOrderRejectReason ORR ON ORR.ID = O.RejectReasonID	
	LEFT JOIN tblGaugerOrder GAO ON GAO.OrderID = O.ID
	LEFT JOIN tblContract CN ON CN.ID = O.ContractID
) O
LEFT JOIN tblOrderStatus OPS ON OPS.ID = O.PrintStatusID
LEFT JOIN tblGaugerOrderStatus GOS ON GOS.ID = O.GaugerStatusID
GO


/****************************************************
 Date Created: 2016/02/24
 Author: Kevin Alons
 Purpose: return all currently eligible DriverApp OrderReadOnly records (for all Drivers) 
 Changes:
	-	3.13.13		2016/08/04		JAE		Add Sequence # for sorting orders
	-	4.4.14		2016/11/30		JAE		Switch to viewDriverBase
	-   4.5.11		2017/02/09		JAE		Add Job Number and ContractNumber for ZPL
****************************************************/
ALTER VIEW viewOrderReadOnlyAllEligible_DriverApp AS
SELECT O.ID
	, O.OrderNum
	, O.StatusID
	, O.TicketTypeID
	, PriorityNum = CAST(P.PriorityNum AS INT) 
	, Product = PRO.Name
	, O.DueDate
	, Origin = OO.Name
	, OriginFull = OO.FullName
	, OO.OriginType
	, O.OriginUomID
	, OriginStation = OO.Station 
	, OriginLeaseNum = OO.LeaseNum 
	, OriginCounty = OO.County 
	, OriginLegalDescription = OO.LegalDescription 
	, OriginNDIC = OO.NDICFileNum
	, OriginNDM = OO.NDM
	, OriginCA = OO.CA
	, OriginState = OO.State
	, OriginAPI = OO.WellAPI 
	, OriginLat = OO.LAT 
	, OriginLon = OO.LON 
	, OriginGeoFenceRadiusMeters = OO.GeoFenceRadiusMeters 
	, Destination = D.Name
	, DestinationFull = D.FullName
	, DestType = D.DestinationType 
	, O.DestUomID
	, DestLat = D.LAT 
	, DestLon = D.LON 
	, DestGeoFenceRadiusMeters = D.GeoFenceRadiusMeters 
	, DestinationStation = D.Station 
	, O.CreateDateUTC
	, O.CreatedByUser
	, O.DeleteDateUTC 
	, O.DeletedByUser 
	, O.OriginID
	, O.DestinationID
	, PriorityID = CAST(O.PriorityID AS INT) 
	, Operator = OO.Operator
	, O.OperatorID
	, Pumper = OO.Pumper
	, O.PumperID
	, Producer = OO.Producer
	, O.ProducerID
	, Customer = C.Name
	, O.CustomerID
	, Carrier = CA.Name
	, O.CarrierID
	, O.ProductID
	, TicketType = OO.TicketType
	, EmergencyInfo = ISNULL(C.EmergencyInfo, 'For an emergency (spill, leak, fire, or accident) contact CHEMTREC @ 800.424.9300 (toll free)') 
	, DestTicketTypeID = D.TicketTypeID
	, DestTicketType = D.TicketType
	, O.OriginTankNum
	, O.OriginTankID
	, O.DispatchNotes
	, O.DispatchConfirmNum
	, RouteActualMiles = ISNULL(R.ActualMiles, 0)
	, CarrierAuthority = CA.Authority 
	, OriginTimeZone = OO.TimeZone
	, DestTimeZone = D.TimeZone
	, OCTM.OriginThresholdMinutes
	, OCTM.DestThresholdMinutes
	, ShipperHelpDeskPhone = C.HelpDeskPhone
	, OriginDrivingDirections = OO.DrivingDirections
	, DestDrivingDirections = D.DrivingDirections
	, O.AcceptLastChangeDateUTC
	, O.PickupLastChangeDateUTC
	, O.DeliverLastChangeDateUTC
    , CustomerLastChangeDateUTC = C.LastChangeDateUTC 
    , CarrierLastChangeDateUTC = CA.LastChangeDateUTC 
    , OriginLastChangeDateUTC = OO.LastChangeDateUTC 
    , DestLastChangeDateUTC = D.LastChangeDateUTC
    , RouteLastChangeDateUTC = R.LastChangeDateUTC
    , DriverID = O.DriverID
    , OriginDriver = OD.FullName
    , OriginTruckID = ISNULL(OTR.OriginTruckID, O.TruckID)
    , DestDriver = DD.FullName
	, O.SequenceNum
	, O.JobNumber
	, CN.ContractNumber
	, ContractPrintDescription = CN.PrintDescription
FROM dbo.tblOrder O
	JOIN dbo.tblPriority P ON P.ID = O.PriorityID
	JOIN dbo.viewOrigin OO ON OO.ID = O.OriginID
	JOIN dbo.viewDestination D ON D.ID = O.DestinationID
	JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
	JOIN dbo.tblRoute R ON R.ID = O.RouteID
	JOIN dbo.tblCarrier CA ON CA.ID = O.CarrierID
	JOIN dbo.tblProduct PRO ON PRO.ID = O.ProductID		
	LEFT JOIN dbo.tblOrderTransfer OTR ON OTR.OrderID = O.ID
	LEFT JOIN dbo.viewDriverBase OD ON OD.ID = ISNULL(OTR.OriginDriverID, O.DriverID)
	LEFT JOIN dbo.viewDriverBase DD ON DD.ID = O.DriverID
	LEFT JOIN tblContract CN ON CN.ID = O.ContractID
	OUTER APPLY dbo.fnOrderBestMatchDriverAppPrintHeaderImage(O.ID) DAHI
	OUTER APPLY dbo.fnOrderCombinedThresholdMinutes(O.ID) OCTM
WHERE StatusID IN (2,7,8,3) AND O.DeleteDateUTC IS NULL
GO



-- --------------------------------
-- Update other scripts that used contract number
-- --------------------------------
GO

/***********************************
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: create new Order (loads) for the specified criteria
-- Changes:
	3.9.12 - 2015/09/01 - KDA - adding in @DispatchNotes parameter (this must have been placed here previously by Joe?)
	3.11.19 - 2016/04/29 - BB - Adding Job number and contract number fields
	3.13.7 - 2016/07/26 - JAE - Copy SulfurContent from origin to order
	4.5.11 - 2017/02/08 - JAE - Remove contract number.  Now handled by the trigger
***********************************/
ALTER PROCEDURE spCreateLoads
(
  @OriginID INT
, @DestinationID INT
, @TicketTypeID INT
, @DueDate DATETIME
, @CustomerID INT
, @CarrierID INT = NULL
, @DriverID INT = NULL
, @Qty INT
, @UserName VARCHAR(100)
, @OriginTankID INT = NULL
, @OriginTankNum VARCHAR(20) = NULL
, @OriginBOLNum VARCHAR(15) = NULL
, @PriorityID INT = 3 -- LOW priority
, @StatusID SMALLINT = -10 -- GENERATED
, @ProductID INT = 1 -- basic Crude Oil
, @DispatchConfirmNum VARCHAR(30) = NULL
, @JobNumber VARCHAR(20) = NULL
, @DispatchNotes VARCHAR(500) = NULL
, @IDs_CSV VARCHAR(max) = NULL OUTPUT
, @count INT = 0 OUTPUT
) AS
BEGIN

	DECLARE @PumperID INT, @OperatorID INT, @ProducerID INT, @OriginUomID INT, @OriginSulfurContent DECIMAL(10,4), @DestUomID INT
	SELECT @PumperID = PumperID, @OperatorID = OperatorID, @ProducerID = ProducerID, 
			@OriginUomID = UomID, @OriginSulfurContent = SulfurContent, @count = 0
	FROM tblOrigin 
	WHERE ID = @OriginID
	SELECT @DestUomID = UomID FROM tblDestination WHERE ID = @DestinationID

	-- ensure that a driver is never assigned without a CarrierID
	IF (@DriverID IS NOT NULL AND @CarrierID IS NULL)
		SELECT @CarrierID = CarrierID FROM tblDriver WHERE ID = @DriverID
		
	DECLARE @incrementBOL BIT
	SELECT @incrementBOL = CASE WHEN @OriginBOLNum LIKE '%+' THEN 1 ELSE 0 END
	IF (@incrementBOL = 1) SELECT @OriginBOLNum = LEFT(@OriginBOLNum, LEN(@OriginBOLNum) - 1)
	
	DECLARE @i INT, @id INT
	SELECT @i = 0, @IDs_CSV = ''
	
	WHILE @i < @Qty BEGIN
		
		INSERT INTO dbo.tblOrder (OriginID, OriginUomID, DestinationID, DestUomID, TicketTypeID, DueDate, CustomerID
				, CarrierID, DriverID, StatusID, PriorityID, OriginTankID, OriginTankNum, OriginBOLNum, OriginSulfurContent
				, OrderNum, ProductID, PumperID, OperatorID, ProducerID, DispatchConfirmNum, JobNumber, DispatchNotes
				, CreateDateUTC, CreatedByUser)
			VALUES (@OriginID, @OriginUomID, @DestinationID, @DestUomID, @TicketTypeID, @DueDate, @CustomerID
				, @CarrierID, @DriverID, @StatusID, @PriorityID, @OriginTankID, @OriginTankNum, @OriginBOLNum, @OriginSulfurContent
				, (SELECT ISNULL((SELECT MAX(OrderNum) FROM tblOrder), 100000) + 1), @ProductID
				, @PumperID, @OperatorID, @ProducerID, @DispatchConfirmNum, @JobNumber, @DispatchNotes
				, GETUTCDATE(), @UserName)
		
		SELECT @IDs_CSV = @IDs_CSV + ',' + LTRIM(SCOPE_IDENTITY()), @count = @count + 1
		
		IF (@incrementBOL = 1)
		BEGIN
			IF (isnumeric(@OriginBOLNum) = 1) SELECT @OriginBOLNum = RTRIM(CAST(@OriginBOLNum AS BIGINT) + 1)
		END

		SET @i = @i + 1
	END
	IF (@DriverID IS NOT NULL)
	BEGIN
		UPDATE tblOrder SET TruckID = D.TruckID, TrailerID = D.TrailerID, Trailer2ID = D.Trailer2ID
		FROM tblOrder O
		JOIN tblDriver D ON D.ID = O.DriverID
		WHERE O.ID IN (SELECT ID FROM dbo.fnSplitCSVIDs(@IDs_CSV))
	END
	SET @IDs_CSV = SUBSTRING(@IDs_CSV, 2, 8000)
END
GO


/***********************************
-- Date Created: 18 Apr 2015
-- Author: Kevin Alons
-- Purpose: create new Gauger Order (loads) for the specified criteria
-- Changes:
	- 3.8.11 - 2015/07/28 - KDA - add explicit @ShipperID parameter (Customer no longer an field of tblOrigin
	- 3.11.19 - 2016/04/29 - BB - Adding Job number and contract number fields
	4.5.11 - 2017/02/08 - JAE - Remove contract number.  Now handled by the trigger
***********************************/
ALTER PROCEDURE spCreateGaugerLoads
(
  @OriginID INT
, @ShipperID INT
, @DestinationID INT
, @GaugerTicketTypeID INT
, @DueDate DATETIME
, @OriginTankID INT
, @ProductID INT
, @UserName VARCHAR(100)
, @GaugerID INT = NULL
, @Qty INT = 1
, @PriorityID INT = 3 -- LOW priority
, @DispatchConfirmNum VARCHAR(30) = NULL
, @JobNumber VARCHAR(20) = NULL
, @IDs_CSV VARCHAR(max) = NULL OUTPUT
, @count INT = 0 OUTPUT
) AS
BEGIN
	DECLARE @TicketTypeID INT
	SELECT @TicketTypeID = TicketTypeID FROM tblGaugerTicketType WHERE ID = @GaugerTicketTypeID
	EXEC spCreateLoads @StatusID=-9, @OriginID=@OriginID, @OriginTankID=@OriginTankID, @ProductID=@ProductID, @DestinationID=@DestinationID
		, @CustomerID=@ShipperID, @PriorityID=@PriorityID, @TicketTypeID=@TicketTypeID, @DispatchConfirmNum=@DispatchConfirmNum
		, @JobNumber=@JobNumber
		, @DueDate=@DueDate, @Qty=@Qty, @UserName=@UserName
		, @IDs_CSV=@IDs_CSV OUTPUT
		, @count=@count OUTPUT
	
	INSERT INTO tblGaugerOrder (OrderID, TicketTypeID, StatusID, OriginTankID, OriginTankNum, GaugerID
		, Rejected, Handwritten, DueDate, PriorityID, CreateDateUTC, CreatedByUser)
		SELECT ID, @GaugerTicketTypeID, CASE WHEN @GaugerID IS NULL THEN 1 ELSE 2 END, @OriginTankID, NULL, @GaugerID
			, 0, 0, @DueDate, @PriorityID, GETUTCDATE(), @UserName
		FROM tblOrder 
		WHERE ID IN (SELECT ID FROM dbo.fnSplitCSVIDs(@IDs_CSV))
END
GO


/**********************************/
-- Created: 2013/03/09 - ?.?.? - Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
-- Changes:
-- 3.7.31	- 2015/06/19 - KDA	- reorder input parameters (move ProviderID down) and add @DriverGroupID parameter
-- 3.9.0	- 2/15/08/14 - KDA	- return Approved column
-- 3.9.19.6	- 2015/09/30 - KDA	- add OverrideXXX fields from OrderApproval table
-- 3.9.21	- 2015/10/03 - KDA	- use [viewOrder] O.OriginDriverGroupID instead of O.DriverGroupID (which could be DestDriver.DriverGroupID)
-- 3.9.34	- 2015/12/16 - BB	- Remove deleted orders from the results (DCWEB-851)
-- 3.10.13.6 - 2015/03/01 - JAE	- add TruckTypeID
-- 3.11.3.1	- 2016/03/22 - KDA	- fix to no-op when @BatchID = 0
-- 3.11.19	- 2016/05/02 - BB	- Add Job number and Contract number
-- 3.11.20	- 2016/05/04 - JAE	- 3 month limit was always being applied, skip if batch is provided
-- 3.11.20.1 - 2016/05/04 - JAE	- Undoing change as timeouts reoccurring, need to investigate
-- 3.11.20.3 - 2016/05/11 - KDA	- optimize @BatchID filtering to query directly from tblOrderSettlementCarrier table
--								- use tblOrder and minimal JOINs intead of expensive viewOrder
-- 4.1.0	- 2016/08/08 - KDA	- use new @StartSession & @SessionID parameters to persist the retrieved records as a "Session"
--								- stop defaulting @StartDate to 3 months prior if not supplied (null parameter value provided)
--								- use simplified viewOrder_Financial_Carrier for return results - which is now again optimized for "reasonable" performance
-- 4.1.3.5	- 2016/09/17 - BB	- DCWEB-1770: Add reject filter
-- 4.1.8.6	- 2016.09.21 - KDA	- remove dynamic sql and retrieve data in 2 stages (like before) getting the orderID first, then the final data (expensive query)
--								- use new fnTrimToNull() to ensure optional string parameters are trimmed to NULL
-- 4.1.9.3	- 2026.09.28 - KDA	- ensure @StartSession = 0 when @BatchID is not null
-- 4.1.25.3	- 2016.10.28 - KDA	- ensure we don't reuse a sessionID when generating a new session
-- 4.3.0.2	- 2016.11.11 - KDA	- prevent including the same order multiple times (error in LEFT JOIN)
--								- don't remove existing session on overlapping creation, only "purge" after 1 week (obsolete)
--	4.5.11	- 2017.02.08 - JAE	- Replace contract number with contractID foreign key
/***********************************/
ALTER PROCEDURE spRetrieveOrdersFinancialCarrier
(
  @StartDate DATE = NULL -- will default to first day of 3rd prior month for efficiency sake
, @EndDate DATE = NULL
, @ShipperID INT = -1 -- all
, @CarrierID INT = -1 -- all
, @ProductGroupID INT = -1 -- all 
, @TruckTypeID INT = -1 -- all
, @DriverGroupID INT = -1 -- all
, @OriginStateID INT = -1 -- all 
, @DestStateID INT = -1 -- all
, @ProducerID INT = -1 -- all
, @BatchID INT = NULL -- either show unbatched (NULL), or the specified batch orders
, @OnlyShipperSettled BIT = 0 
, @JobNumber VARCHAR(20) = NULL -- 3.11.19
, @ContractNumber VARCHAR(20) = NULL -- 3.11.19
, @Rejected INT = -1 -- all (rejected + not rejected) -- 4.1.3.5
, @UserName VARCHAR(100)
, @StartSession BIT = 1 -- when set to 1, assign the retrieved records to a new Session (ID), when 0 and a sessionID was assigned, then retrieve it
, @SessionID VARCHAR(100) = NULL OUTPUT -- if @startSession = 1, then this return a new value, otherwise will retrieve the existing records in that session
) AS 
BEGIN
	IF (@BatchID IS NOT NULL) SET @StartSession = 0  -- if selecting orders by BatchID, do NOT start a session
	IF (@StartSession IS NULL) SET @StartSession = 0  -- default to FALSE if not supplied
	IF (@SessionID IS NULL) SET @SessionID = 0 -- default value

	-- if an empty/blank string parameter is provided, trim it to NULL
	SELECT @JobNumber = dbo.fnTrimToNull(@JobNumber)
		, @ContractNumber = dbo.fnTrimToNull(@ContractNumber)

	-- validate the incoming parameters
	IF (@BatchID IS NOT NULL
		AND (@ShipperID <> -1 
			OR @CarrierID <> -1 
			OR @ProductGroupID <> -1
			OR @TruckTypeID <> -1
			OR @DriverGroupID <> -1
			OR @OriginStateID <> -1
			OR @DestStateID <> -1
			OR @ProducerID <> -1))
	BEGIN
		RAISERROR('@BatchID parameter cannot be specified with other parameters', 16, 1)
		RETURN
	END

	DECLARE @IDs IDTABLE

	-- if a @BatchID value was specified then just use this criteria alone
    IF @BatchID IS NOT NULL
	BEGIN
		INSERT INTO @IDs SELECT OrderID FROM tblOrderSettlementCarrier WHERE BatchID = @BatchID
	END
	ELSE IF (@StartSession = 0 AND @SessionID IS NOT NULL)
	BEGIN
		INSERT INTO @IDs SELECT OrderID FROM tblOrderSettlementSelectionCarrier WHERE SessionID = @SessionID
	END
	ELSE
	BEGIN
		INSERT INTO @IDs 
			SELECT O.ID
			FROM tblOrder O
			JOIN tblProduct P ON P.ID = O.ProductID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			JOIN tblDestination D ON D.ID = O.DestinationID
			JOIN tblTruck T1 ON T1.ID = O.TruckID
			LEFT JOIN dbo.tblOrderTransfer OTR ON OTR.OrderID = O.ID
			LEFT JOIN tblTruck T2 ON T2.ID = OTR.OriginTruckID
			LEFT JOIN dbo.viewDriverBase vODR ON vODR.ID = OTR.OriginDriverID
			LEFT JOIN dbo.viewDriverBase vDDR ON vDDR.ID = O.DriverID
			LEFT JOIN tblOrderSettlementCarrier OS ON OS.OrderID = O.ID
			LEFT JOIN tblOrderSettlementShipper OSP ON OSP.OrderID = O.ID
			LEFT JOIN tblContract CN ON CN.ID = ContractID
			WHERE O.StatusID IN (4)  
			  AND O.DeleteDateUTC IS NULL  -- 3.9.34
			  AND OS.BatchID IS NULL -- only get order if it's not part of a batch
			  AND (@ShipperID=-1 OR O.CustomerID=@ShipperID) 
			  AND (@CarrierID=-1 OR O.CarrierID=@CarrierID) 
			  AND (@ProductGroupID=-1 OR P.ProductGroupID=@ProductGroupID) 
			  AND (@TruckTypeID=-1 OR T1.TruckTypeID=@TruckTypeID OR T2.TruckTypeID=@TruckTypeID)
			  AND (@DriverGroupID=-1 OR vODR.DriverGroupID = @DriverGroupID OR vDDR.DriverGroupID = @DriverGroupID) 
			  AND (@ProducerID=-1 OR O.ProducerID=@ProducerID) 
			  AND (@OriginStateID=-1 OR OO.StateID=@OriginStateID) 
			  AND (@DestStateID=-1 OR D.StateID=@DestStateID) 
			  AND (@StartDate IS NULL OR O.OrderDate >= @StartDate) 
			  AND (@EndDate IS NULL OR O.OrderDate <= @EndDate)
			  AND (@OnlyShipperSettled = 0 OR OSP.BatchID IS NOT NULL)
			  AND (@JobNumber IS NULL OR O.JobNumber = @JobNumber)
			  AND (@ContractNumber IS NULL OR CN.ContractNumber = @ContractNumber)
			  AND (@Rejected=-1 OR O.Rejected=@Rejected) -- 4.1.3.5
	END

	-- create the temp table to store the data
	SELECT *, RateApplySel = CAST(1 AS BIT), BatchSel = CAST(1 AS BIT), SessionID = CAST(NULL AS VARCHAR(100))
	INTO #ret 
	FROM viewOrder_Financial_Carrier
	WHERE 1 = 0
	
	-- get the data into the temp table
	INSERT INTO #ret
		SELECT O.*
			, OS.RateApplySel, OS.BatchSel, OS.SessionID
		FROM viewOrder_Financial_Carrier O
		LEFT JOIN tblOrderSettlementSelectionCarrier OS ON OS.OrderID = O.ID AND SessionID = @sessionID -- 4.3.0.2 - added @sessionID criteria
		WHERE O.OrderID IN (SELECT ID FROM @IDs)
	
	-- do the SessionID logic
	IF (@StartSession = 1 AND EXISTS (SELECT * FROM #ret))
	BEGIN
		BEGIN TRAN RetrieveOrdersFinancialCarrier
	
		/* 4.3.0.2 - generate the "next" sessionID record */
		INSERT INTO tblOrderSettlementSessionCarrier(UserName) VALUES (@UserName)
		SELECT @sessionID = SCOPE_IDENTITY()

		UPDATE #ret SET SessionID = @sessionID, RateApplySel = 1, BatchSel = CASE WHEN HasError = 1 THEN 0 ELSE 1 END
		INSERT INTO tblOrderSettlementSelectionCarrier (SessionID, OrderID, RateApplySel, BatchSel)
			SELECT @sessionID, ID, RateApplySel, BatchSel FROM #ret

		COMMIT TRAN RetrieveOrdersFinancialCarrier

		/* "purge" the tblOrderSettlementSessionShipper table of "obsolete" records (older than a week old)
			NOTE: if this ends up being a performance hit, we should instead execute this via a scheduled task */
		DELETE FROM tblOrderSettlementSessionCarrier WHERE CreateDateUTC < DATEADD(WEEK, -1, GETDATE())
	END

	-- return the data to the caller
	SELECT * FROM #ret
END
GO


/**********************************/
-- Created: 2013/03/09 - ?.?.? - Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
-- Changes:
-- 4.1.0.2	- 2016.08.28 - KDA	- add some PRINT instrumentation statements for performance debugging purposes
-- 4.1.3.5	- 2016/09/17 - BB	- DCWEB-1770: Add reject filter
-- 4.1.8.6	- 2016.09.21 - KDA	- remove dynamic sql and retrieve data in 2 stages (like before) getting the orderID first, then the final data (expensive query)
--								- use new fnTrimToNull() to ensure optional string parameters are trimmed to NULL
-- 4.1.9.3	- 2026.09.28 - KDA	- ensure @StartSession = 0 when @BatchID is not null
-- 4.1.9.4	- 2016.09.28 - JAE	- (omitted from prev update) use new fnTrimToNull() to ensure optional string parameters are trimmed to NULL 
-- 4.1.25.3	- 2016.10.28 - KDA	- ensure we don't reuse a sessionID when generating a new session
-- 4.3.0.2	- 2016.11.11 - KDA	- prevent including the same order multiple times (error in LEFT JOIN)
--								- don't remove existing session on overlapping creation, only "purge" after 1 week (obsolete)
--	4.5.11	- 2017.02.08 - JAE	- Replace contract number with contractID foreign key
/***********************************/
ALTER PROCEDURE spRetrieveOrdersFinancialDriver
(
  @StartDate DATE = NULL -- will default to first day of 3rd prior month for efficiency sake
, @EndDate DATE = NULL
, @ShipperID INT = -1 -- all
, @CarrierID INT = -1 -- all
, @ProductGroupID INT = -1 -- all 
, @TruckTypeID INT = -1 -- all
, @DriverGroupID INT = -1 -- all
, @DriverID INT = -1 -- all
, @OriginStateID INT = -1 -- all 
, @DestStateID INT = -1 -- all
, @ProducerID INT = -1 -- all
, @BatchID INT = NULL -- either show unbatched (NULL), or the specified batch orders
, @OnlyCarrierSettled BIT = 0 
, @JobNumber VARCHAR(20) = NULL -- 3.11.19
, @ContractNumber VARCHAR(20) = NULL -- 3.11.19
, @Rejected INT = -1 -- all (rejected + not rejected) -- 4.1.3.5
, @UserName VARCHAR(100)
, @StartSession BIT = 1 -- when set to 1, assign the retrieved records to a new Session (ID), when 0 and a sessionID was assigned, then retrieve it
, @SessionID VARCHAR(100) = NULL OUTPUT -- if @startSession = 1, then this return a new value, otherwise will retrieve the existing records in that session
) AS 
BEGIN
	PRINT 'Start=' + CONVERT(VARCHAR(24), GETDATE(), 121)

	IF (@BatchID IS NOT NULL) SET @StartSession = 0  -- if selecting orders by BatchID, do NOT start a session
	IF (@StartSession IS NULL) SET @StartSession = 0  -- default to FALSE if not supplied
	IF (@SessionID IS NULL) SET @SessionID = 0 -- default value

	-- if an empty/blank string parameter is provided, trim it to NULL
	SELECT @JobNumber = dbo.fnTrimToNull(@JobNumber)
		, @ContractNumber = dbo.fnTrimToNull(@ContractNumber)

	-- validate the incoming parameters
	IF (@BatchID IS NOT NULL
		AND (@ShipperID <> -1 
			OR @CarrierID <> -1 
			OR @ProductGroupID <> -1
			OR @TruckTypeID <> -1
			OR @DriverGroupID <> -1
			OR @DriverID <> -1
			OR @OriginStateID <> -1
			OR @DestStateID <> -1
			OR @ProducerID <> -1))
	BEGIN
		RAISERROR('@BatchID parameter cannot be specified with other parameters', 16, 1)
		RETURN
	END

	DECLARE @IDs IDTABLE

	-- if a @BatchID value was specified then just use this criteria alone
    IF @BatchID IS NOT NULL
	BEGIN
		INSERT INTO @IDs SELECT OrderID FROM tblOrderSettlementDriver WHERE BatchID = @BatchID
	END
	ELSE IF (@StartSession = 0 AND @SessionID IS NOT NULL)
	BEGIN
		INSERT INTO @IDs SELECT OrderID FROM tblOrderSettlementSelectionDriver WHERE SessionID = @SessionID 
	END
	ELSE
	BEGIN
		INSERT INTO @IDs 
			SELECT O.ID
			FROM tblOrder O
			JOIN tblProduct P ON P.ID = O.ProductID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			JOIN tblDestination D ON D.ID = O.DestinationID
			JOIN tblTruck T1 ON T1.ID = O.TruckID
			LEFT JOIN dbo.tblOrderTransfer OTR ON OTR.OrderID = O.ID
			LEFT JOIN tblTruck T2 ON T2.ID = OTR.OriginTruckID
			LEFT JOIN dbo.viewDriverBase vODR ON vODR.ID = OTR.OriginDriverID
			LEFT JOIN dbo.viewDriverBase vDDR ON vDDR.ID = O.DriverID
			LEFT JOIN tblOrderSettlementDriver OS ON OS.OrderID = O.ID
			LEFT JOIN tblOrderSettlementCarrier OSP ON OSP.OrderID = O.ID
			LEFT JOIN tblContract CN ON CN.ID = O.ContractID
			WHERE O.StatusID IN (4)  
			  AND O.DeleteDateUTC IS NULL  -- 3.9.34
			  AND OS.BatchID IS NULL -- only get order if it's not part of a batch
			  AND (@ShipperID=-1 OR O.CustomerID=@ShipperID) 
			  AND (@CarrierID=-1 OR O.CarrierID=@CarrierID) 
			  AND (@ProductGroupID=-1 OR P.ProductGroupID=@ProductGroupID) 
			  AND (@TruckTypeID=-1 OR T1.TruckTypeID=@TruckTypeID OR T2.TruckTypeID=@TruckTypeID)
			  AND (@DriverGroupID=-1 OR vODR.DriverGroupID = @DriverGroupID OR vDDR.DriverGroupID = @DriverGroupID) 
			  AND (@DriverID=-1 OR vODR.ID = @DriverID OR vDDR.ID = @DriverID) 
			  AND (@OriginStateID=-1 OR OO.StateID=@OriginStateID) 
			  AND (@DestStateID=-1 OR D.StateID=@DestStateID) 
			  AND (@ProducerID=-1 OR O.ProducerID=@ProducerID) 
			  AND (@StartDate IS NULL OR O.OrderDate >= @StartDate) 
			  AND (@EndDate IS NULL OR O.OrderDate <= @EndDate)
			  AND (@OnlyCarrierSettled = 0 OR OSP.BatchID IS NOT NULL)
			  AND (@JobNumber IS NULL OR O.JobNumber = @JobNumber)
			  AND (@ContractNumber IS NULL OR CN.ContractNumber = @ContractNumber)
			  AND (@Rejected=-1 OR O.Rejected=@Rejected)  -- 4.1.3.5
	END

	-- create the temp table to store the data
	SELECT *, RateApplySel = CAST(1 AS BIT), BatchSel = CAST(1 AS BIT), SessionID = CAST(NULL AS VARCHAR(100))
	INTO #ret 
	FROM viewOrder_Financial_Driver
	WHERE 1 = 0
	
	-- get the data into the temp table
	INSERT INTO #ret
		SELECT O.*
			, OS.RateApplySel, OS.BatchSel, OS.SessionID
		FROM viewOrder_Financial_Driver O
		LEFT JOIN tblOrderSettlementSelectionDriver OS ON OS.OrderID = O.ID AND SessionID = @sessionID -- 4.3.0.2 - added @sessionID criteria
		WHERE O.OrderID IN (SELECT ID FROM @IDs)

	-- do the SessionID logic
	IF (@StartSession = 1 AND EXISTS (SELECT * FROM #ret))
	BEGIN
		PRINT 'SessionCreate:Start=' + CONVERT(VARCHAR(24), GETDATE(), 121)
		BEGIN TRAN RetrieveOrdersFinancialDriver

		/* 4.3.0.2 - generate the "next" sessionID record */
		INSERT INTO tblOrderSettlementSessionDriver(UserName) VALUES (@UserName)
		SELECT @sessionID = SCOPE_IDENTITY()

		UPDATE #ret SET SessionID = @sessionID, RateApplySel = 1, BatchSel = CASE WHEN HasError = 1 THEN 0 ELSE 1 END
		INSERT INTO tblOrderSettlementSelectionDriver (SessionID, OrderID, RateApplySel, BatchSel)
			SELECT @sessionID, ID, RateApplySel, BatchSel FROM #ret
	
		COMMIT TRAN RetrieveOrdersFinancialDriver
		PRINT 'SessionCreate:Finish=' + CONVERT(VARCHAR(24), GETDATE(), 121)

		/* "purge" the tblOrderSettlementSessionDriver table of "obsolete" records (older than a week old)
			NOTE: if this ends up being a performance hit, we should instead execute this via a scheduled task */
		DELETE FROM tblOrderSettlementSessionDriver WHERE CreateDateUTC < DATEADD(WEEK, -1, GETDATE())
	END

	-- return the data to the caller
	SELECT * FROM #ret

	PRINT 'Finish=' + CONVERT(VARCHAR(24), GETDATE(), 121)
END
GO


/***********************************/
-- Created: ?.?.? - 2013/03/09 - Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
-- Changes:
-- 3.8.11	- 2015/07/28 - KDA	- remove OriginShipperRegion field
-- 3.9.0	- 2/15/08/14 - KDA	- return Approved column
-- 3.9.19.6	- 2015/09/30 - KDA	- add OverrideXXX fields from OrderApproval table
-- 3.9.34	- 2015/12/16 - BB	- Remove deleted orders from the results (DCWEB-851)
-- 3.10.13.6 - 2015/03/01 - JAE	- add TruckTypeID
-- 3.11.3.1	- 2016/03/22 - KDA	- fix to no-op when @BatchID = 0
-- 3.11.19	- 2016/05/02 - BB	- Add Job number and Contract number
-- 3.11.20.3 - 2016/05/11 - KDA	- optimize @BatchID filtering to query directly from tblOrderSettlementShipper table
--								- use tblOrder and minimal JOINs intead of expensive viewOrder
-- 4.1.0	- 2016/08/08 - KDA	- use new @StartSession & @SessionID parameters to persist the retrieved records as a "Session"
--								- stop defaulting @StartDate to 3 months prior if not supplied (null parameter value provided)
--								- use simplified viewOrder_Financial_Shipper for return results - which is now again optimized for "reasonable" performance
-- 4.1.3.5	- 2016/09/17 - BB	- DCWEB-1770: Add reject filter
-- 4.1.8.6	- 2016.09.21 - KDA	- remove dynamic sql and retrieve data in 2 stages (like before) getting the orderID first, then the final data (expensive query)
--								- use new fnTrimToNull() to ensure optional string parameters are trimmed to NULL
-- 4.1.9.3	- 2026.09.28 - KDA	- ensure @StartSession = 0 when @BatchID is not null
-- 4.1.25.3	- 2016.10.28 - KDA	- ensure we don't reuse a sessionID when generating a new session
-- 4.3.0.2	- 2016.11.11 - KDA	- prevent including the same order multiple times (error in LEFT JOIN)
--								- don't remove existing session on overlapping creation, only "purge" after 1 week (obsolete)
--	4.5.11	- 2017.02.08 - JAE	- Replace contract number with contractID foreign key
/***********************************/
ALTER PROCEDURE spRetrieveOrdersFinancialShipper
(
  @StartDate DATE = NULL -- will default to first day of 3rd prior month for efficiency sake
, @EndDate DATE = NULL
, @ShipperID INT = -1 -- all
, @ProductGroupID INT = -1 -- all 
, @TruckTypeID INT = -1 -- all
, @OriginStateID INT = -1 -- all 
, @DestStateID INT = -1 -- all
, @ProducerID INT = -1 -- all
, @BatchID INT = NULL -- either show unbatched (NULL), or the specified batch orders
, @JobNumber VARCHAR(20) = NULL -- 3.11.19
, @ContractNumber VARCHAR(20) = NULL -- 3.11.19
, @Rejected INT = -1 -- all (rejected + not rejected) -- 4.1.3.5
, @UserName VARCHAR(100)
, @StartSession BIT = 1 -- when set to 1, assign the retrieved records to a new Session (ID), when 0 and a sessionID was assigned, then retrieve it
, @SessionID VARCHAR(100) = NULL OUTPUT -- if @startSession = 1, then this return a new value, otherwise will retrieve the existing records in that session
) AS 
BEGIN
	IF (@BatchID IS NOT NULL) SET @StartSession = 0  -- if selecting orders by BatchID, do NOT start a session
	IF (@StartSession IS NULL) SET @StartSession = 0  -- default to FALSE if not supplied
	IF (@SessionID IS NULL) SET @SessionID = 0 -- default value

	-- if an empty/blank string parameter is provided, trim it to NULL
	SELECT @JobNumber = dbo.fnTrimToNull(@JobNumber)
		, @ContractNumber = dbo.fnTrimToNull(@ContractNumber)

	-- validate the incoming parameters
	IF (@BatchID IS NOT NULL
		AND (@ShipperID <> -1 
			OR @ProductGroupID <> -1
			OR @TruckTypeID <> -1
			OR @OriginStateID <> -1
			OR @DestStateID <> -1
			OR @ProducerID <> -1))
	BEGIN
		RAISERROR('@BatchID parameter cannot be specified with other parameters', 16, 1)
		RETURN
	END

	DECLARE @IDs IDTABLE

	-- if a @BatchID value was specified then just use this criteria alone
    IF @BatchID IS NOT NULL
	BEGIN
		INSERT INTO @IDs SELECT OrderID FROM tblOrderSettlementShipper WHERE BatchID = @BatchID
	END
	ELSE IF (@StartSession = 0 AND @SessionID IS NOT NULL)
	BEGIN
		INSERT INTO @IDs SELECT OrderID FROM tblOrderSettlementSelectionShipper WHERE SessionID = @SessionID
	END
	ELSE
	BEGIN
		INSERT INTO @IDs 
			SELECT O.ID
			FROM tblOrder O
			JOIN tblProduct P ON P.ID = O.ProductID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			JOIN tblDestination D ON D.ID = O.DestinationID
			JOIN tblTruck T1 ON T1.ID = O.TruckID
			LEFT JOIN dbo.tblOrderTransfer OTR ON OTR.OrderID = O.ID
			LEFT JOIN tblTruck T2 ON T2.ID = OTR.OriginTruckID
			LEFT JOIN tblOrderSettlementShipper OS ON OS.OrderID = O.ID
			LEFT JOIN tblContract CN ON CN.ID = O.ContractID
			WHERE O.StatusID IN (4)  
			  AND O.DeleteDateUTC IS NULL  -- 3.9.34
			  AND OS.BatchID IS NULL -- only get order if it's not part of a batch
			  AND (@ShipperID=-1 OR O.CustomerID=@ShipperID) 
			  AND (@ProductGroupID=-1 OR P.ProductGroupID=@ProductGroupID) 
			  AND (@TruckTypeID=-1 OR T1.TruckTypeID=@TruckTypeID OR T2.TruckTypeID=@TruckTypeID)
			  AND (@ProducerID=-1 OR O.ProducerID=@ProducerID) 
			  AND (@OriginStateID=-1 OR OO.StateID=@OriginStateID) 
			  AND (@DestStateID=-1 OR D.StateID=@DestStateID) 
			  AND (@StartDate IS NULL OR O.OrderDate >= @StartDate) 
			  AND (@EndDate IS NULL OR O.OrderDate <= @EndDate)
			  AND (@JobNumber IS NULL OR O.JobNumber = @JobNumber)
			  AND (@ContractNumber IS NULL OR CN.ContractNumber = @ContractNumber)
			  AND (@Rejected=-1 OR O.Rejected=@Rejected) -- 4.1.3.5
	END
	
	-- create the temp table to store the data
	SELECT *, RateApplySel = CAST(1 AS BIT), BatchSel = CAST(1 AS BIT), SessionID = CAST(NULL AS VARCHAR(100))
	INTO #ret 
	FROM viewOrder_Financial_Shipper 
	WHERE 1 = 0
	
	-- get the data into the temp table
	INSERT INTO #ret
		SELECT O.*
			, OS.RateApplySel, OS.BatchSel, OS.SessionID
		FROM viewOrder_Financial_Shipper O
		LEFT JOIN tblOrderSettlementSelectionShipper OS ON OS.OrderID = O.ID AND SessionID = @sessionID -- 4.3.0.2 - added @sessionID criteria
		WHERE O.OrderID IN (SELECT ID FROM @IDs)

	-- do the SessionID logic
	IF (@StartSession = 1 AND EXISTS (SELECT * FROM #ret))
	BEGIN
		BEGIN TRAN RetrieveOrdersFinancialShipper
	
		/* 4.3.0.2 - generate the "next" sessionID record */
		INSERT INTO tblOrderSettlementSessionShipper(UserName) VALUES (@UserName)
		SELECT @sessionID = SCOPE_IDENTITY()

		UPDATE #ret SET SessionID = @sessionID, RateApplySel = 1, BatchSel = CASE WHEN HasError = 1 THEN 0 ELSE 1 END
		INSERT INTO tblOrderSettlementSelectionShipper (SessionID, OrderID, RateApplySel, BatchSel)
			SELECT @sessionID, ID, RateApplySel, BatchSel FROM #ret
	
		COMMIT TRAN RetrieveOrdersFinancialShipper

		/* "purge" the tblOrderSettlementSessionShipper table of "obsolete" records (older than a week old)
			NOTE: if this ends up being a performance hit, we should instead execute this via a scheduled task */
		DELETE FROM tblOrderSettlementSessionShipper WHERE CreateDateUTC < DATEADD(WEEK, -1, GETDATE())
	END
	
	-- return the data to the caller
	SELECT * FROM #ret
END
GO


-- --------------------------------
-- Add new ReportCenter fields
-- --------------------------------


UPDATE tblReportColumnDefinition 
SET Caption = 'GENERAL | CONTRACT | Contract #' 
WHERE ID = 303
GO


SET IDENTITY_INSERT tblReportColumnDefinition ON
INSERT INTO tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport, IsTicketField)
	SELECT 371, 1, 'ContractPrintDescription', 'GENERAL | CONTRACT | Contract Description', null, null, 1, null, 1, '*', 1, 0
	UNION
	SELECT 372, 1, 'ContractNotes', 'GENERAL | CONTRACT | Contract Notes', null, null, 1, null, 1, '*', 1, 0
	UNION
	SELECT 90073, 1, '(SELECT Name FROM tblUom WHERE ID = RS.ContractUomID)', 'GENERAL | Contract UOM', null, 'ContractUomID', 2, 'SELECT ID, Name = Abbrev FROM tblUom ORDER BY Abbrev', 1, '*', 1, 0
	UNION
	SELECT 90074, 1, 'dbo.fnConvertUOM(RS.ContractVolume, RS.ContractUomID, 1)', 'GENERAL | CONTRACT | Contract Volume (BBLS)', null, null, 1, null, 1, '*', 1, 0
	UNION
	SELECT 90075, 1, 'dbo.fnConvertUOM(RS.ContractVolume, RS.ContractUomID, 2)', 'GENERAL | CONTRACT | Contract Volume (GAL)', null, null, 1, null, 1, '*', 1, 0
	UNION
	SELECT 90076, 1, 'dbo.fnConvertUOM(RS.ContractVolume, RS.ContractUomID, 3)', 'GENERAL | CONTRACT | Contract Volume (CM)', null, null, 1, null, 1, '*', 1, 0
	UNION
	SELECT 90077, 1, 'dbo.fnConvertUOM(RS.ContractVolume, RS.ContractUomID, 4)', 'GENERAL | CONTRACT | Contract Volume (LTR)', null, null, 1, null, 1, '*', 1, 0
	EXCEPT SELECT * FROM tblReportColumnDefinition
SET IDENTITY_INSERT tblReportColumnDefinition OFF
GO


-- --------------------------------
-- Refresh everything
-- --------------------------------
GO
EXEC sp_refreshview viewOrder
GO
EXEC sp_refreshview viewOrderLocalDates
GO
EXEC sp_refreshview viewOrderSettlementShipper
GO
EXEC sp_refreshview viewOrder_Financial_Base_Shipper 
GO
EXEC sp_refreshview viewOrder_Financial_Shipper
GO

EXEC _spRebuildAllObjects
GO
EXEC _spRecompileAllStoredProcedures
GO
EXEC _spRebuildAllObjects
GO


EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRecompileAllStoredProcedures
GO
EXEC _spRebuildAllObjects
GO
EXEC _spRebuildAllObjects
GO



COMMIT
SET NOEXEC OFF