SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.1.24'
SELECT  @NewVersion = '4.1.25'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1865 - Convert Producer Maintenance page to MVC.'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO



/* Update existing page view permission to fit with new permissions being added below */
UPDATE aspnet_Roles
SET FriendlyName = 'Producers - View'
WHERE RoleName = 'viewProducerMaintenance'
GO



/* Add new permissions for the Producer maintenance page */
EXEC spAddNewPermission 'createProducerMaintenance', 'Allow user to create Producers', 'Maintenance', 'Producers - Create'
GO
EXEC spAddNewPermission 'editProducerMaintenance', 'Allow user to edit Producers', 'Maintenance', 'Producers - Edit'
GO
EXEC spAddNewPermission 'deactivateProducerMaintenance', 'Allow user to deactivate Producers', 'Maintenance', 'Producers - Deactivate'
GO



COMMIT
SET NOEXEC OFF