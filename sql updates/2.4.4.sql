/* 
	record all units/unit-relatd rates normalized to the Settings.ReportUomID setting
	(and associated changes to invoice|settlement processing, etc)
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.4.3'
SELECT  @NewVersion = '2.4.4'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

/*************************************************************
** Date Created: 7 Dec 2013
** Author: Kevin Alons
** Purpose: convert any RATE from/to a UomID
*************************************************************/
CREATE FUNCTION [dbo].[fnConvertRateUOM](@units decimal(9,4), @fromUomID int, @toUomID int) RETURNS decimal(18,6) AS BEGIN
	-- not that RATE conversions are INVERTED (switched TO<->FROM)
	RETURN (SELECT dbo.fnConvertUOM(@units, @toUomID, @fromUomID))
END
GO
GRANT EXECUTE ON fnConvertRateUOM to dispatchcrude_iis_acct
GO

ALTER TABLE tblOrderInvoiceCarrier ADD UomID int NOT NULL 
	CONSTRAINT FK_OrderInvoiceCarrier_Uom FOREIGN KEY REFERENCES tblUom(ID)
	CONSTRAINT DF_OrderInvoiceCarrier_Uom DEFAULT (1) -- default to Barrels
GO
ALTER TABLE tblOrderInvoiceCarrier ADD MinSettlementUnits decimal(18,6) NULL
GO
ALTER TABLE tblOrderInvoiceCarrier ADD Units decimal(18,6) NULL
GO 
ALTER TABLE tblOrderInvoiceCustomer ADD UomID int NOT NULL 
	CONSTRAINT FK_OrderInvoiceCustomer_Uom FOREIGN KEY REFERENCES tblUom(ID)
	CONSTRAINT DF_OrderInvoiceCustomer_Uom DEFAULT (1) -- default to Barrels
GO
ALTER TABLE tblOrderInvoiceCustomer ADD MinSettlementUnits decimal(18,6) NULL
GO
ALTER TABLE tblOrderInvoiceCustomer ADD Units decimal(18,6) NULL
GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Carrier "Settlement" $$ values to an Audited order
/***********************************/
ALTER PROCEDURE [dbo].[spProcessCarrierInvoice]
(
  @ID int
, @UserName varchar(100)
, @ChainupFee smallmoney = NULL
, @WaitFee smallmoney = NULL
, @RerouteFee smallmoney = NULL
, @RejectionFee smallmoney = NULL
, @H2SFee smallmoney = NULL
, @LoadFee smallmoney = NULL
) AS BEGIN
	DELETE FROM tblOrderInvoiceCarrier WHERE OrderID = @ID
	DECLARE @ReportUomID int
	SELECT @ReportUomID = Value FROM tblSetting WHERE ID = 16
	
	-- all Units and Rates are first normalized to GALLONS UOM then processed consistently in Gallons
	-- but then converted to the System.ReportUomID setting UOM units for saving in the Invoice|Settlement record (for display purposes)
	INSERT INTO tblOrderInvoiceCarrier (OrderID
		, ChainupFee, RerouteFee, WaitFeeSubUnitID, WaitFeeRoundingTypeID, BillableWaitMinutes, WaitRate, WaitFee
		, UomID, MinSettlementUnits, Units
		, RouteRate, H2SRate, TaxRate
		, RejectionFee, LoadFee, H2SFee, FuelSurcharge
		, TotalFee, CreateDateUTC, CreatedByUser)
	SELECT D.ID
		, ChainupFee, RerouteFee, WaitFeeSubUnitID, WaitFeeRoundingTypeID, BillableWaitMinutes, WaitRate, WaitFee
		-- normalize all of these values to the current @ReportUomID UNIT OF MEASURE (for reporting purposes)
		, @ReportUomID, dbo.fnConvertUOM(MinSettlementGallons, 2, @ReportUomID), dbo.fnConvertUOM(ActualGallons, 2, @ReportUomID)
		, dbo.fnConvertRateUOM(RouteRate, 2, @ReportUomID), dbo.fnConvertRateUOM(H2SRate, 2, @ReportUomID), TaxRate
		, RejectionFee, LoadFee, H2SFee, FuelSurcharge
		, RejectionFee + ChainupFee + RerouteFee + WaitFee + H2SFee + LoadFee AS TotalFee
		, GETUTCDATE(), @UserName
	FROM (
		-- compute the actual fees or use the manual overide fee values (if provided)
		SELECT ID
			, coalesce(@ChainupFee, Chainup * ChainupFee, 0) AS ChainupFee
			, coalesce(@RerouteFee, RerouteFee * RerouteCount, 0) AS RerouteFee
			, WaitFeeSubUnitID
			, WaitFeeRoundingTypeID
			, cast(round(BillableWaitHours * 60, 0) as int) AS BillableWaitMinutes
			, WaitFee AS WaitRate
			, coalesce(@WaitFee, BillableWaitHours * WaitFee, 0) AS WaitFee
			, coalesce(@RejectionFee, Rejected * RejectionFee, 0) AS RejectionFee
			, H2SRate
			, MinSettlementGallons
			, ActualGallons
			, coalesce(@H2SFee, H2S * dbo.fnMaxDecimal(MinSettlementGallons, ActualGallons) * H2SRate, 0) AS H2SFee
			, isnull(TaxRate, 0) AS TaxRate
			, isnull(RouteRate, 0) AS RouteRate
			-- if rejected, use 0 AS the Rate
			, round(coalesce(@LoadFee, (1 - Rejected) * dbo.fnMaxDecimal(MinSettlementGallons, ActualGallons) * RouteRate, 0), 4) AS LoadFee
			, isnull(FuelSurcharge, 0) AS FuelSurcharge
		FROM (
			-- normalize the Accessorial Rates to GALLONS + add normalized Route Rate and other time normalization
			SELECT S.ID
				, S.ChainUp
				, CR.ChainupFee
				, CR.RerouteFee
				, S.RerouteCount
				, S.TotalWaitMinutes
				, CR.WaitFeeSubUnitID
				, CR.WaitFeeRoundingTypeID
				, dbo.fnComputeBillableWaitHours(S.TotalWaitMinutes, CR.WaitFeeSubUnitID, CR.WaitFeeRoundingTypeID) AS BillableWaitHours
				, CR.WaitFee
				, S.Rejected
				, CR.RejectionFee
				, S.H2S
				-- normalize the Origin.UOM H2SRate for "Gallons" UOM (RATE conversions are INVERTED)
				, dbo.fnConvertUOM(isnull(S.H2S * CR.H2SRate, 0), 2, OriginUomID) AS H2SRate
				, S.TaxRate
				-- normalize the Origin.UOM Route Rate for "Gallons" UOM (rate conversions are INVERTED)
				, dbo.fnConvertUom(dbo.fnCarrierRouteRate(S.CarrierID, S.RouteID, S.OrderDate), 2, OriginUomID) AS RouteRate
				, S.MinSettlementGallons
				, isnull(S.ActualGallons, 0) AS ActualGallons
				, CR.FuelSurcharge
			FROM (
				-- get the Order raw data (with Units Normalized to GALLONS) and matching Carrier Accessorial Rate ID
				SELECT O.ID
					, O.CarrierID
					, O.RouteID
					, O.ChainUp
					-- normalize the Order.OriginUOM ActualUnits for "Gallons" UOM
					, dbo.fnConvertUOM(CASE WHEN C.SettlementFactorID = 1 THEN O.OriginGrossUnits ELSE O.OriginNetUnits END, O.OriginUomID, 2) AS ActualGallons
					, O.OriginUomID
					, O.RerouteCount
					, O.TotalWaitMinutes
					, O.Rejected
					, OO.H2S
					, O.OrderDate
					, coalesce(CRXX.ID, CRXA.ID, CRAX.ID, CRAA.ID) AS CRID
					-- normalize the Origin.UOM MinSettlementUnits for "Gallons" UOM
					, isnull(dbo.fnConvertUOM(C.MinSettlementUnits, C.MinSettlementUomID, 2), 0) AS MinSettlementGallons
					, OO.TaxRate
				FROM dbo.viewOrderExportFull O
				JOIN tblOrigin OO ON OO.ID = O.OriginID
				JOIN tblCarrier C ON C.ID = O.CarrierID
				LEFT JOIN tblCarrierRates CRXX ON CRXX.CarrierID = O.CarrierID AND CRXX.RegionID = OO.RegionID
				LEFT JOIN tblCarrierRates CRXA ON CRXA.CarrierID = O.CarrierID AND CRXA.RegionID = -1
				LEFT JOIN tblCarrierRates CRAX ON CRAX.CarrierID = -1 AND CRAX.RegionID = OO.RegionID
				LEFT JOIN tblCarrierRates CRAA ON CRAA.CarrierID = -1 AND CRAA.RegionID = -1
				WHERE O.ID = @ID
			) S
			LEFT JOIN tblCarrierRates CR ON CR.ID = S.CRID
		) SS
	) D
END

GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Customer "Settlement" $$ values to an Audited order
/***********************************/
ALTER PROCEDURE [dbo].[spProcessCustomerInvoice]
(
  @ID int
, @UserName varchar(100)
, @ChainupFee smallmoney = NULL
, @WaitFee smallmoney = NULL
, @RerouteFee smallmoney = NULL
, @RejectionFee smallmoney = NULL
, @H2SFee smallmoney = NULL
, @LoadFee smallmoney = NULL
) AS BEGIN
	DELETE FROM tblOrderInvoiceCustomer WHERE OrderID = @ID
	DECLARE @ReportUomID int
	SELECT @ReportUomID = Value FROM tblSetting WHERE ID = 16
	
	-- all Units and Rates are first normalized to GALLONS UOM then processed consistently in Gallons
	-- but then converted to the System.ReportUomID setting UOM units for saving in the Invoice|Settlement record (for display purposes)
	INSERT INTO tblOrderInvoiceCustomer (OrderID
		, ChainupFee, RerouteFee, WaitFeeSubUnitID, WaitFeeRoundingTypeID, BillableWaitMinutes, WaitRate, WaitFee
		, UomID, MinSettlementUnits, Units
		, RouteRate, H2SRate, TaxRate
		, RejectionFee, LoadFee, H2SFee, FuelSurcharge
		, TotalFee, CreateDateUTC, CreatedByUser)
	SELECT D.ID
		, ChainupFee, RerouteFee, WaitFeeSubUnitID, WaitFeeRoundingTypeID, BillableWaitMinutes, WaitRate, WaitFee
		-- normalize all of these values to the current @ReportUomID UNIT OF MEASURE (for reporting purposes)
		, @ReportUomID, dbo.fnConvertUOM(MinSettlementGallons, 2, @ReportUomID), dbo.fnConvertUOM(ActualGallons, 2, @ReportUomID)
		, dbo.fnConvertRateUOM(RouteRate, 2, @ReportUomID), dbo.fnConvertRateUOM(H2SRate, 2, @ReportUomID), TaxRate
		, RejectionFee, LoadFee, H2SFee, FuelSurcharge
		, RejectionFee + ChainupFee + RerouteFee + WaitFee + H2SFee + LoadFee AS TotalFee
		, GETUTCDATE(), @UserName
	FROM (
		-- compute the actual fees or use the manual overide fee values (if provided)
		SELECT ID
			, coalesce(@ChainupFee, Chainup * ChainupFee, 0) AS ChainupFee
			, coalesce(@RerouteFee, RerouteFee * RerouteCount, 0) AS RerouteFee
			, WaitFeeSubUnitID
			, WaitFeeRoundingTypeID
			, cast(round(BillableWaitHours * 60, 0) as int) AS BillableWaitMinutes
			, WaitFee AS WaitRate
			, coalesce(@WaitFee, BillableWaitHours * WaitFee, 0) AS WaitFee
			, coalesce(@RejectionFee, Rejected * RejectionFee, 0) AS RejectionFee
			, H2SRate
			, MinSettlementGallons
			, ActualGallons
			, coalesce(@H2SFee, H2S * dbo.fnMaxDecimal(MinSettlementGallons, ActualGallons) * H2SRate, 0) AS H2SFee
			, isnull(TaxRate, 0) AS TaxRate
			, isnull(RouteRate, 0) AS RouteRate
			-- if rejected, use 0 AS the Rate
			, round(coalesce(@LoadFee, (1 - Rejected) * dbo.fnMaxDecimal(MinSettlementGallons, ActualGallons) * RouteRate, 0), 4) AS LoadFee
			, isnull(FuelSurcharge, 0) AS FuelSurcharge
		FROM (
			-- normalize the Accessorial Rates to GALLONS + add normalized Route Rate and other time normalization
			SELECT S.ID
				, S.ChainUp
				, CR.ChainupFee
				, CR.RerouteFee
				, S.RerouteCount
				, S.TotalWaitMinutes
				, CR.WaitFeeSubUnitID
				, CR.WaitFeeRoundingTypeID
				, dbo.fnComputeBillableWaitHours(S.TotalWaitMinutes, CR.WaitFeeSubUnitID, CR.WaitFeeRoundingTypeID) AS BillableWaitHours
				, CR.WaitFee
				, S.Rejected
				, CR.RejectionFee
				, S.H2S
				-- normalize the Origin.UOM H2SRate for "Gallons" UOM (RATE conversions are INVERTED)
				, dbo.fnConvertUOM(isnull(S.H2S * CR.H2SRate, 0), 2, OriginUomID) AS H2SRate
				, S.TaxRate
				-- normalize the Origin.UOM Route Rate for "Gallons" UOM (rate conversions are INVERTED)
				, dbo.fnConvertUom(dbo.fnCustomerRouteRate(S.CustomerID, S.RouteID, S.OrderDate), 2, OriginUomID) AS RouteRate
				, S.MinSettlementGallons
				, isnull(S.ActualGallons, 0) AS ActualGallons
				, CR.FuelSurcharge
			FROM (
				-- get the Order raw data (with Units Normalized to GALLONS) and matching Customer Accessorial Rate ID
				SELECT O.ID
					, O.CustomerID
					, O.RouteID
					, O.ChainUp
					-- normalize the Order.OriginUOM ActualUnits for "Gallons" UOM
					, dbo.fnConvertUOM(CASE WHEN C.SettlementFactorID = 1 THEN O.OriginGrossUnits ELSE O.OriginNetUnits END, O.OriginUomID, 2) AS ActualGallons
					, O.OriginUomID
					, O.RerouteCount
					, O.TotalWaitMinutes
					, O.Rejected
					, OO.H2S
					, O.OrderDate
					, coalesce(CRXX.ID, CRXA.ID, CRAX.ID, CRAA.ID) AS CRID
					-- normalize the Origin.UOM MinSettlementUnits for "Gallons" UOM
					, isnull(dbo.fnConvertUOM(C.MinSettlementUnits, C.MinSettlementUomID, 2), 0) AS MinSettlementGallons
					, OO.TaxRate
				FROM dbo.viewOrderExportFull O
				JOIN tblOrigin OO ON OO.ID = O.OriginID
				JOIN tblCustomer C ON C.ID = O.CustomerID
				LEFT JOIN tblCustomerRates CRXX ON CRXX.CustomerID = O.CustomerID AND CRXX.RegionID = OO.RegionID
				LEFT JOIN tblCustomerRates CRXA ON CRXA.CustomerID = O.CustomerID AND CRXA.RegionID = -1
				LEFT JOIN tblCustomerRates CRAX ON CRAX.CustomerID = -1 AND CRAX.RegionID = OO.RegionID
				LEFT JOIN tblCustomerRates CRAA ON CRAA.CustomerID = -1 AND CRAA.RegionID = -1
				WHERE O.ID = @ID
			) S
			LEFT JOIN tblCustomerRates CR ON CR.ID = S.CRID
		) SS
	) D
END

GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details + FINANCIAL INFO into a single view
/***********************************/
ALTER VIEW [dbo].[viewOrder_Financial_Carrier] AS 
	SELECT OE.* 
		, dbo.fnOrderTicketDetails(OE.ID, 'TicketNums', '<br/>') AS TicketNums
		, dbo.fnOrderTicketDetails(OE.ID, 'TankNums', '<br/>') AS TankNums
		, dbo.fnRerouteDetails(OE.ID, 'PreviousDestinations', '<br/>') AS PreviousDestinations
		, dbo.fnRerouteDetails(OE.ID, 'RerouteUsers', '<br/>') AS RerouteUsers
		, dbo.fnRerouteDetails(OE.ID, 'RerouteDates', '<br/>') AS RerouteDates
		, dbo.fnRerouteDetails(OE.ID, 'RerouteNotes', '<br/>') AS RerouteNotes
		, dbo.fnLocal_to_UTC(ISNULL(OIC.LastChangeDateUTC, OIC.CreateDateUTC), OE.OriginTimeZoneID, OE.OriginUseDST) AS InvoiceRatesAppliedDate
		, OIC.BatchID
		, SB.BatchNum AS InvoiceBatchNum
		, OIC.ChainupFee AS InvoiceChainupFee
		, OIC.RerouteFee AS InvoiceRerouteFee
		, OIC.BillableWaitMinutes AS InvoiceBillableWaitMinutes
		, isnull(WFSU.Name, 'None') AS InvoiceWaitFeeSubUnit
		, isnull(WFRT.Name, 'None') AS InvoiceWaitFeeRoundingType
		, OIC.WaitRate AS InvoiceWaitRate
		, OIC.WaitFee AS InvoiceWaitFee
		, OIC.RejectionFee AS InvoiceRejectionFee
		, OIC.H2SRate AS InvoiceH2SRate
		, OIC.H2SFee AS InvoiceH2SFee
		, OIC.TaxRate AS InvoiceTaxRate
		, U.Name AS InvoiceUom
		, U.Abbrev AS InvoiceUomShort
		, OIC.MinSettlementUnits AS InvoiceMinSettlementUnits
		, OIC.Units AS InvoiceUnits
		, OIC.RouteRate AS InvoiceRouteRate
		, OIC.LoadFee AS InvoiceLoadFee
		, OIC.TotalFee AS InvoiceTotalFee
		, OIC.FuelSurcharge AS InvoiceFuelSurcharge
	FROM dbo.viewOrderExportFull OE
	LEFT JOIN dbo.tblOrderInvoiceCarrier OIC ON OIC.OrderID = OE.ID
	LEFT JOIN dbo.tblCarrierSettlementBatch SB ON SB.ID = OIC.BatchID
	LEFT JOIN dbo.tblUom U ON U.ID = OIC.UomID	
	LEFT JOIN dbo.tblWaitFeeSubUnit WFSU ON WFSU.ID = OIC.WaitFeeSubUnitID
	LEFT JOIN dbo.tblWaitFeeRoundingType WFRT ON WFRT.ID = OIC.WaitFeeRoundingTypeID

GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details + FINANCIAL INFO into a single view
/***********************************/
ALTER VIEW [dbo].[viewOrder_Financial_Customer] AS 
	SELECT OE.* 
		, dbo.fnOrderTicketDetails(OE.ID, 'TicketNums', '<br/>') AS TicketNums
		, dbo.fnOrderTicketDetails(OE.ID, 'TankNums', '<br/>') AS TankNums
		, dbo.fnRerouteDetails(OE.ID, 'PreviousDestinations', '<br/>') AS PreviousDestinations
		, dbo.fnRerouteDetails(OE.ID, 'RerouteUsers', '<br/>') AS RerouteUsers
		, dbo.fnRerouteDetails(OE.ID, 'RerouteDates', '<br/>') AS RerouteDates
		, dbo.fnRerouteDetails(OE.ID, 'RerouteNotes', '<br/>') AS RerouteNotes
		, dbo.fnLocal_to_UTC(ISNULL(OIC.LastChangeDateUTC, OIC.CreateDateUTC), OE.OriginTimeZoneID, OE.OriginUseDST) AS InvoiceRatesAppliedDate
		, OIC.BatchID
		, SB.BatchNum AS InvoiceBatchNum
		, OIC.ChainupFee AS InvoiceChainupFee
		, OIC.RerouteFee AS InvoiceRerouteFee
		, OIC.BillableWaitMinutes AS InvoiceBillableWaitMinutes
		, isnull(WFSU.Name, 'None') AS InvoiceWaitFeeSubUnit
		, isnull(WFRT.Name, 'None') AS InvoiceWaitFeeRoundingType
		, OIC.WaitRate AS InvoiceWaitRate
		, OIC.WaitFee AS InvoiceWaitFee
		, OIC.RejectionFee AS InvoiceRejectionFee
		, OIC.H2SRate AS InvoiceH2SRate
		, OIC.H2SFee AS InvoiceH2SFee
		, OIC.TaxRate AS InvoiceTaxRate
		, U.Name AS InvoiceUom
		, U.Abbrev AS InvoiceUomShort
		, OIC.MinSettlementUnits AS InvoiceMinSettlementUnits
		, OIC.Units AS InvoiceUnits
		, OIC.RouteRate AS InvoiceRouteRate
		, OIC.LoadFee AS InvoiceLoadFee
		, OIC.TotalFee AS InvoiceTotalFee
		, OIC.FuelSurcharge AS InvoiceFuelSurcharge
	FROM dbo.viewOrderExportFull OE
	LEFT JOIN dbo.tblOrderInvoiceCustomer OIC ON OIC.OrderID = OE.ID
	LEFT JOIN dbo.tblCustomerSettlementBatch SB ON SB.ID = OIC.BatchID
	LEFT JOIN dbo.tblUom U ON U.ID = OIC.UomID	
	LEFT JOIN dbo.tblWaitFeeSubUnit WFSU ON WFSU.ID = OIC.WaitFeeSubUnitID
	LEFT JOIN dbo.tblWaitFeeRoundingType WFRT ON WFRT.ID = OIC.WaitFeeRoundingTypeID

GO

UPDATE tblOrderInvoiceCarrier 
  SET Units = O.OriginNetUnits
	, MinSettlementUnits = C.MinSettlementUnits
FROM tblOrderInvoiceCarrier OIC
JOIN tblOrder O ON O.ID = OIC.OrderID  
LEFT JOIN tblCarrier C ON C.ID = O.CarrierID
GO

UPDATE tblOrderInvoiceCustomer 
  SET Units = O.OriginNetUnits
	, MinSettlementUnits = C.MinSettlementUnits
FROM tblOrderInvoiceCustomer OIC
JOIN tblOrder O ON O.ID = OIC.OrderID  
LEFT JOIN tblCustomer C ON C.ID = O.CustomerID
GO

COMMIT
SET NOEXEC OFF

EXEC _spRefreshAllViews
GO
EXEC _spRecompileAllStoredProcedures
GO