SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.10.10.3'
SELECT  @NewVersion = '3.10.10.4'

-- only ensure the curr version matches when we are not testing with an "x.x.x" DEV version
IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END


BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'DCWEB-1107 - Update unaudit view non-settled orders (neither carrier or shipper settled)'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/*************************************************
- Date Created: 16 Feb 2015
- Author: Kevin Alons
- Purpose: return the data used by the Un-Audit MVC page
- Changes:
-		JAE - 11/10/2015 - Used origin driver instead of (destination) driver (3.9.25)
-       3.10.4 - JAE+KDA - 02/29/2016 - Filtered carrier settled orders as well
*************************************************/
ALTER VIEW viewOrderUnaudit AS
	SELECT O.ID
		, OrderNum
		, OrderDate = dbo.fnDateMdYY(O.OrderDate)
		, Status = OrderStatus
		, Rejected
		, DispatchConfirmNum
		, Customer
		, Carrier
		, OriginDriver
		, Origin
		, OriginGrossUnits
		, OriginNetUnits
		, Destination 
	FROM viewOrder O 
	LEFT JOIN tblOrderSettlementCarrier OSC ON OSC.OrderID = O.ID 
	LEFT JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = O.ID 
	WHERE O.StatusID=4 
		AND O.DeleteDateUTC IS NULL 
		AND OSC.BatchID IS NULL
		AND OSS.BatchID IS NULL 

GO

COMMIT
SET NOEXEC OFF