/* add some missing OrderPrintLog | OrderPrintLogTicket fields (for DriverApp.Sync)
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.1.11'
SELECT  @NewVersion = '2.1.12'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

-- DO THE ACTUAL UPDATE HERE
/***********************************/

ALTER TABLE tblOrderPrintLogTicket ADD TankNum varchar(20) NULL
GO
ALTER TABLE tblOrderPrintLogTicket ADD BarrelsPerInch decimal(9, 6) NULL
GO


COMMIT
SET NOEXEC OFF