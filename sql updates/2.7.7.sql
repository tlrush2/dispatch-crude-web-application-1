/*
	-- fix rounding issues with UOM conversions
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.7.6'
SELECT  @NewVersion = '2.7.7'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

ALTER TABLE dbo.tblUom
	DROP CONSTRAINT DF_UOM_CreateDateUTC
GO
CREATE TABLE dbo.Tmp_tblUom
	(
	ID int NOT NULL IDENTITY (1, 1),
	Name varchar(25) NOT NULL,
	Abbrev varchar(10) NOT NULL,
	GallonEquivalent decimal(18, 12) NOT NULL,
	CreateDateUTC smalldatetime NULL,
	CreatedByUser varchar(100) NULL,
	LastChangeDateUTC smalldatetime NULL,
	LastChangedByUser varchar(100) NULL,
	DeleteDateUTC smalldatetime NULL,
	DeletedByUser varchar(100) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_tblUom SET (LOCK_ESCALATION = TABLE)
GO
GRANT DELETE ON dbo.Tmp_tblUom TO dispatchcrude_iis_acct  AS dbo
GO
GRANT INSERT ON dbo.Tmp_tblUom TO dispatchcrude_iis_acct  AS dbo
GO
GRANT SELECT ON dbo.Tmp_tblUom TO dispatchcrude_iis_acct  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_tblUom TO dispatchcrude_iis_acct  AS dbo
GO
ALTER TABLE dbo.Tmp_tblUom ADD CONSTRAINT
	DF_UOM_CreateDateUTC DEFAULT (getutcdate()) FOR CreateDateUTC
GO
SET IDENTITY_INSERT dbo.Tmp_tblUom ON
GO
IF EXISTS(SELECT * FROM dbo.tblUom)
	 EXEC('INSERT INTO dbo.Tmp_tblUom (ID, Name, Abbrev, GallonEquivalent, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser)
		SELECT ID, Name, Abbrev, GallonEquivalent, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser FROM dbo.tblUom WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_tblUom OFF
GO
ALTER TABLE dbo.tblOrderInvoiceCarrier
	DROP CONSTRAINT FK_OrderInvoiceCarrier_Uom
GO
ALTER TABLE dbo.tblCustomerRouteRates
	DROP CONSTRAINT FK_CustomerRouteRates_Uom
GO
ALTER TABLE dbo.tblOrderInvoiceCustomer
	DROP CONSTRAINT FK_OrderInvoiceCustomer_Uom
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_OriginUom
GO
ALTER TABLE dbo.tblOrder
	DROP CONSTRAINT FK_Order_DestUom
GO
ALTER TABLE dbo.tblDestination
	DROP CONSTRAINT FK_Destination_Uom
GO
ALTER TABLE dbo.tblCarrierRouteRates
	DROP CONSTRAINT FK_CarrierRouteRates_Uom
GO
ALTER TABLE dbo.tblTrailer
	DROP CONSTRAINT FK_Trailer_Uom
GO
ALTER TABLE dbo.tblCarrier
	DROP CONSTRAINT FK_Carrier_MinSettlementUom
GO
ALTER TABLE dbo.tblCustomer
	DROP CONSTRAINT FK_Customer_MinSettlementUom
GO
ALTER TABLE dbo.tblCarrierRates
	DROP CONSTRAINT FK_CarrierRates_Uom
GO
ALTER TABLE dbo.tblCustomerRates
	DROP CONSTRAINT FK_CustomerRates_Uom
GO
ALTER TABLE dbo.tblOrigin
	DROP CONSTRAINT FK_Origin_Uom
GO
DROP TABLE dbo.tblUom
GO
EXECUTE sp_rename N'dbo.Tmp_tblUom', N'tblUom', 'OBJECT' 
GO
ALTER TABLE dbo.tblUom ADD CONSTRAINT
	PK_Uom PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE UNIQUE NONCLUSTERED INDEX udxUom_Name ON dbo.tblUom
	(
	Name
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX udxUom_Abbrev ON dbo.tblUom
	(
	Abbrev
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.tblOrigin ADD CONSTRAINT
	FK_Origin_Uom FOREIGN KEY
	(
	UomID
	) REFERENCES dbo.tblUom
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrigin SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblCustomerRates ADD CONSTRAINT
	FK_CustomerRates_Uom FOREIGN KEY
	(
	UomID
	) REFERENCES dbo.tblUom
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblCustomerRates SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblCarrierRates ADD CONSTRAINT
	FK_CarrierRates_Uom FOREIGN KEY
	(
	UomID
	) REFERENCES dbo.tblUom
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblCarrierRates SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblCustomer ADD CONSTRAINT
	FK_Customer_MinSettlementUom FOREIGN KEY
	(
	MinSettlementUomID
	) REFERENCES dbo.tblUom
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblCustomer SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblCarrier ADD CONSTRAINT
	FK_Carrier_MinSettlementUom FOREIGN KEY
	(
	MinSettlementUomID
	) REFERENCES dbo.tblUom
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblCarrier SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblTrailer ADD CONSTRAINT
	FK_Trailer_Uom FOREIGN KEY
	(
	UomID
	) REFERENCES dbo.tblUom
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblTrailer SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblCarrierRouteRates ADD CONSTRAINT
	FK_CarrierRouteRates_Uom FOREIGN KEY
	(
	UomID
	) REFERENCES dbo.tblUom
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblCarrierRouteRates SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblDestination ADD CONSTRAINT
	FK_Destination_Uom FOREIGN KEY
	(
	UomID
	) REFERENCES dbo.tblUom
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblDestination SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_OriginUom FOREIGN KEY
	(
	OriginUomID
	) REFERENCES dbo.tblUom
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrder ADD CONSTRAINT
	FK_Order_DestUom FOREIGN KEY
	(
	DestUomID
	) REFERENCES dbo.tblUom
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrder SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderInvoiceCustomer ADD CONSTRAINT
	FK_OrderInvoiceCustomer_Uom FOREIGN KEY
	(
	UomID
	) REFERENCES dbo.tblUom
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderInvoiceCustomer SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblCustomerRouteRates ADD CONSTRAINT
	FK_CustomerRouteRates_Uom FOREIGN KEY
	(
	UomID
	) REFERENCES dbo.tblUom
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblCustomerRouteRates SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblOrderInvoiceCarrier ADD CONSTRAINT
	FK_OrderInvoiceCarrier_Uom FOREIGN KEY
	(
	UomID
	) REFERENCES dbo.tblUom
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblOrderInvoiceCarrier SET (LOCK_ESCALATION = TABLE)
GO

ALTER TABLE dbo.tblCarrierRouteRates
	DROP CONSTRAINT FK_CarrierRouteRates_Uom
GO
ALTER TABLE dbo.tblUom SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblCarrierRouteRates
	DROP CONSTRAINT FK_CarrierRouteRates_Route
GO
ALTER TABLE dbo.tblRoute SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblCarrierRouteRates
	DROP CONSTRAINT FK_CarrierRouteRates_Carrier
GO
ALTER TABLE dbo.tblCarrier SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblCarrierRouteRates
	DROP CONSTRAINT DF_CarrierRouteRates_CreateDateUTC
GO
ALTER TABLE dbo.tblCarrierRouteRates
	DROP CONSTRAINT DF_CarrierRouteRates_UomID
GO
CREATE TABLE dbo.Tmp_tblCarrierRouteRates
	(
	ID int NOT NULL IDENTITY (1, 1),
	CarrierID int NOT NULL,
	RouteID int NOT NULL,
	Rate decimal(18, 10) NOT NULL,
	EffectiveDate smalldatetime NOT NULL,
	CreateDateUTC smalldatetime NULL,
	CreatedByUser varchar(100) NULL,
	LastChangeDateUTC smalldatetime NULL,
	LastChangedByUser varchar(100) NULL,
	UomID int NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_tblCarrierRouteRates SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_tblCarrierRouteRates ADD CONSTRAINT
	DF_CarrierRouteRates_CreateDateUTC DEFAULT (getutcdate()) FOR CreateDateUTC
GO
ALTER TABLE dbo.Tmp_tblCarrierRouteRates ADD CONSTRAINT
	DF_CarrierRouteRates_UomID DEFAULT ((1)) FOR UomID
GO
SET IDENTITY_INSERT dbo.Tmp_tblCarrierRouteRates ON
GO
IF EXISTS(SELECT * FROM dbo.tblCarrierRouteRates)
	 EXEC('INSERT INTO dbo.Tmp_tblCarrierRouteRates (ID, CarrierID, RouteID, Rate, EffectiveDate, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, UomID)
		SELECT ID, CarrierID, RouteID, CONVERT(decimal(18, 10), Rate), EffectiveDate, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, UomID FROM dbo.tblCarrierRouteRates WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_tblCarrierRouteRates OFF
GO
DROP TABLE dbo.tblCarrierRouteRates
GO
EXECUTE sp_rename N'dbo.Tmp_tblCarrierRouteRates', N'tblCarrierRouteRates', 'OBJECT' 
GO
ALTER TABLE dbo.tblCarrierRouteRates ADD CONSTRAINT
	PK_tblCarrierRouteRates PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE UNIQUE NONCLUSTERED INDEX udxCarrierRouteRates_Unique ON dbo.tblCarrierRouteRates
	(
	CarrierID,
	RouteID,
	EffectiveDate
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.tblCarrierRouteRates ADD CONSTRAINT
	FK_CarrierRouteRates_Carrier FOREIGN KEY
	(
	CarrierID
	) REFERENCES dbo.tblCarrier
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	
GO
ALTER TABLE dbo.tblCarrierRouteRates ADD CONSTRAINT
	FK_CarrierRouteRates_Route FOREIGN KEY
	(
	RouteID
	) REFERENCES dbo.tblRoute
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	
GO
ALTER TABLE dbo.tblCarrierRouteRates ADD CONSTRAINT
	FK_CarrierRouteRates_Uom FOREIGN KEY
	(
	UomID
	) REFERENCES dbo.tblUom
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
-- =============================================
-- Author:		Kevin Alons
-- Create date: 19 Dec 2012
-- Description:	trigger to ensure the UomID for Carrier/Customer Route Rates matches that assigned to the Origin
-- =============================================
CREATE TRIGGER [dbo].[trigCarrierRouteRates_IU] ON dbo.tblCarrierRouteRates AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	IF (UPDATE(UomID))
	BEGIN
		-- this conversion is inverted (since it is a rate, not the actual units)
		UPDATE tblCarrierRouteRates
		  SET Rate = dbo.fnConvertRateUOM(CRR.Rate, d.UomID, CRR.UomID)
		FROM tblCarrierRouteRates CRR
		JOIN deleted d ON d.ID = CRR.ID
		WHERE d.UomID <> CRR.UomID
		  AND d.Rate = CRR.Rate
	END
END
GO

ALTER TABLE dbo.tblCustomerRouteRates
	DROP CONSTRAINT FK_CustomerRouteRates_Route
GO
ALTER TABLE dbo.tblRoute SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblCustomerRouteRates
	DROP CONSTRAINT FK_CustomerRouteRates_Customer
GO
ALTER TABLE dbo.tblCustomer SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblCustomerRouteRates
	DROP CONSTRAINT FK_CustomerRouteRates_Uom
GO
ALTER TABLE dbo.tblUom SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblCustomerRouteRates
	DROP CONSTRAINT DF_CustomerRouteRates_CreateDateUTC
GO
ALTER TABLE dbo.tblCustomerRouteRates
	DROP CONSTRAINT DF_CustomerRouteRates_UomID
GO
CREATE TABLE dbo.Tmp_tblCustomerRouteRates
	(
	ID int NOT NULL IDENTITY (1, 1),
	CustomerID int NOT NULL,
	RouteID int NOT NULL,
	Rate decimal(18, 10) NOT NULL,
	EffectiveDate smalldatetime NOT NULL,
	CreateDateUTC smalldatetime NULL,
	CreatedByUser varchar(100) NULL,
	LastChangeDateUTC smalldatetime NULL,
	LastChangedByUser varchar(100) NULL,
	UomID int NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_tblCustomerRouteRates SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_tblCustomerRouteRates ADD CONSTRAINT
	DF_CustomerRouteRates_CreateDateUTC DEFAULT (getutcdate()) FOR CreateDateUTC
GO
ALTER TABLE dbo.Tmp_tblCustomerRouteRates ADD CONSTRAINT
	DF_CustomerRouteRates_UomID DEFAULT ((1)) FOR UomID
GO
SET IDENTITY_INSERT dbo.Tmp_tblCustomerRouteRates ON
GO
IF EXISTS(SELECT * FROM dbo.tblCustomerRouteRates)
	 EXEC('INSERT INTO dbo.Tmp_tblCustomerRouteRates (ID, CustomerID, RouteID, Rate, EffectiveDate, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, UomID)
		SELECT ID, CustomerID, RouteID, CONVERT(decimal(18, 10), Rate), EffectiveDate, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, UomID FROM dbo.tblCustomerRouteRates WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_tblCustomerRouteRates OFF
GO
DROP TABLE dbo.tblCustomerRouteRates
GO
EXECUTE sp_rename N'dbo.Tmp_tblCustomerRouteRates', N'tblCustomerRouteRates', 'OBJECT' 
GO
ALTER TABLE dbo.tblCustomerRouteRates ADD CONSTRAINT
	PK_tblCustomerRouteRates PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE UNIQUE NONCLUSTERED INDEX udxCustomerRouteRates_Unique ON dbo.tblCustomerRouteRates
	(
	CustomerID,
	RouteID,
	EffectiveDate
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.tblCustomerRouteRates ADD CONSTRAINT
	FK_CustomerRouteRates_Uom FOREIGN KEY
	(
	UomID
	) REFERENCES dbo.tblUom
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblCustomerRouteRates ADD CONSTRAINT
	FK_CustomerRouteRates_Customer FOREIGN KEY
	(
	CustomerID
	) REFERENCES dbo.tblCustomer
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	
GO
ALTER TABLE dbo.tblCustomerRouteRates ADD CONSTRAINT
	FK_CustomerRouteRates_Route FOREIGN KEY
	(
	RouteID
	) REFERENCES dbo.tblRoute
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	
GO
-- =============================================
-- Author:		Kevin Alons
-- Create date: 6 Dec 2013
-- Description:	trigger to ensure the UomID for Customer/Customer Route Rates matches that assigned to the Origin
-- =============================================
CREATE TRIGGER [dbo].[trigCustomerRouteRates_IU] ON dbo.tblCustomerRouteRates AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	IF (UPDATE(UomID))
	BEGIN
		-- this conversion is inverted (since it is a rate, not the actual units)
		UPDATE tblCustomerRouteRates
		  SET Rate = dbo.fnConvertRateUOM(CRR.Rate, d.UomID, CRR.UomID)
		FROM tblCustomerRouteRates CRR
		JOIN deleted d ON d.ID = CRR.ID
		WHERE d.UomID <> CRR.UomID
		  AND d.Rate = CRR.Rate
	END
END
GO


/*************************************************************
** Date Created: 26 Nov 2013
** Author: Kevin Alons
** Purpose: convert any Units Qty into a Gallons Qty
*************************************************************/
ALTER FUNCTION [dbo].[fnConvertUOM](@units decimal(18, 10), @fromUomID int, @toUomID int) RETURNS decimal(18,10) AS BEGIN
	DECLARE @ret decimal(18,10)
	IF (@fromUomID = @toUomID OR @units = 0)
		SELECT @ret = @units
	ELSE
		SELECT @ret = (
			SELECT @units * cast(GallonEquivalent as decimal(18, 10)) FROM tblUom WHERE ID=@fromUomID
		) / cast(GallonEquivalent as decimal(18, 10)) FROM tblUom WHERE ID=@toUomID
	RETURN (@ret)
END

GO

/*************************************************************
** Date Created: 7 Dec 2013
** Author: Kevin Alons
** Purpose: convert any RATE from/to a UomID
*************************************************************/
ALTER FUNCTION [dbo].[fnConvertRateUOM](@units decimal(18,10), @fromUomID int, @toUomID int) RETURNS decimal(18,10) AS BEGIN
	-- note that RATE conversions are INVERTED (switched TO<->FROM)
	RETURN (SELECT dbo.fnConvertUOM(@units, @toUomID, @fromUomID))
END

GO

COMMIT
SET NOEXEC OFF

EXEC _spRefreshAllViews
EXEC _spRefreshAllViews
EXEC _spRecompileAllStoredProcedures