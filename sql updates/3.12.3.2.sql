-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.12.3.1'
SELECT  @NewVersion = '3.12.3.2'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1401 Fix max length issue on Destination maintenance page'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/* Increase the length of the station column to allow longer station names */
ALTER TABLE tblDestination ALTER COLUMN Station VARCHAR(25) NULL
GO


/* Adjusting origin table column lengths */
ALTER TABLE tblOrigin ALTER COLUMN LeaseName VARCHAR(50) NULL
GO
ALTER TABLE tblOrigin ALTER COLUMN Station VARCHAR(25) NULL
GO
ALTER TABLE tblOrigin ALTER COLUMN Zip VARCHAR(10) NULL
GO

COMMIT
SET NOEXEC OFF