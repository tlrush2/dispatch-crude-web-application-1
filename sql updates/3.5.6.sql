-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.5.5'
SELECT  @NewVersion = '3.5.6'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Issue #624: Split WaitFeeParameter.ThresholdMinutes to Origin|Dest ThresholdMinutes and related changes'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

ALTER TABLE dbo.tblCarrierWaitFeeParameter
	DROP CONSTRAINT FK_CarrierWaitFeeParameter_SubUnit
GO
ALTER TABLE dbo.tblWaitFeeSubUnit SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblCarrierWaitFeeParameter
	DROP CONSTRAINT FK_CarrierWaitFeeParameter_RoundingType
GO
ALTER TABLE dbo.tblWaitFeeRoundingType SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblCarrierWaitFeeParameter
	DROP CONSTRAINT FK_CarrierWaitFeeParameter
GO
ALTER TABLE dbo.tblCustomer SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblCarrierWaitFeeParameter
	DROP CONSTRAINT FK_CarrierWaitFeeParameter_Region
GO
ALTER TABLE dbo.tblRegion SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblCarrierWaitFeeParameter
	DROP CONSTRAINT FK_CarrierWaitFeeParameter_OriginState
GO
ALTER TABLE dbo.tblCarrierWaitFeeParameter
	DROP CONSTRAINT FK_CarrierWaitFeeParameter_DestState
GO
ALTER TABLE dbo.tblState SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblCarrierWaitFeeParameter
	DROP CONSTRAINT FK_CarrierWaitFeeParameter_ProductGroup
GO
ALTER TABLE dbo.tblProductGroup SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblCarrierWaitFeeParameter
	DROP CONSTRAINT FK_CarrierWaitFeeParameter_Carrier
GO
ALTER TABLE dbo.tblCarrier SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblCarrierWaitFeeParameter
	DROP CONSTRAINT DF_CarrierWaitFeeParameter_ThresholdMinutes
GO
ALTER TABLE dbo.tblCarrierWaitFeeParameter
	DROP CONSTRAINT DF_CarrierWaitFeeParameter_CreateDateUTC
GO
ALTER TABLE dbo.tblCarrierWaitFeeParameter
	DROP CONSTRAINT DF_CarrierWaitFeeParameter_CreatedByUser
GO
CREATE TABLE dbo.Tmp_tblCarrierWaitFeeParameter
	(
	ID int NOT NULL IDENTITY (1, 1),
	CarrierID int NULL,
	ProductGroupID int NULL,
	OriginStateID int NULL,
	DestStateID int NULL,
	RegionID int NULL,
	EffectiveDate date NOT NULL,
	EndDate date NOT NULL,
	RoundingTypeID int NOT NULL,
	SubUnitID int NOT NULL,
	OriginThresholdMinutes int NOT NULL,
	DestThresholdMinutes int NOT NULL,
	CreateDateUTC smalldatetime NOT NULL,
	CreatedByUser varchar(100) NOT NULL,
	LastChangeDateUTC smalldatetime NULL,
	LastChangedByUser varchar(100) NULL,
	ShipperID int NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_tblCarrierWaitFeeParameter SET (LOCK_ESCALATION = TABLE)
GO
GRANT INSERT ON dbo.Tmp_tblCarrierWaitFeeParameter TO dispatchcrude_iis_acct  AS dbo
GO
GRANT SELECT ON dbo.Tmp_tblCarrierWaitFeeParameter TO dispatchcrude_iis_acct  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_tblCarrierWaitFeeParameter TO dispatchcrude_iis_acct  AS dbo
GO
ALTER TABLE dbo.Tmp_tblCarrierWaitFeeParameter ADD CONSTRAINT
	DF_CarrierWaitFeeParameter_OriginThresholdMinutes DEFAULT ((60)) FOR OriginThresholdMinutes
GO
ALTER TABLE dbo.Tmp_tblCarrierWaitFeeParameter ADD CONSTRAINT
	DF_tblCarrierWaitFeeParameter_DestThresholdMinutes DEFAULT 60 FOR DestThresholdMinutes
GO
ALTER TABLE dbo.Tmp_tblCarrierWaitFeeParameter ADD CONSTRAINT
	DF_CarrierWaitFeeParameter_CreateDateUTC DEFAULT (getutcdate()) FOR CreateDateUTC
GO
ALTER TABLE dbo.Tmp_tblCarrierWaitFeeParameter ADD CONSTRAINT
	DF_CarrierWaitFeeParameter_CreatedByUser DEFAULT (suser_name()) FOR CreatedByUser
GO
SET IDENTITY_INSERT dbo.Tmp_tblCarrierWaitFeeParameter ON
GO
IF EXISTS(SELECT * FROM dbo.tblCarrierWaitFeeParameter)
	 EXEC('INSERT INTO dbo.Tmp_tblCarrierWaitFeeParameter (ID, CarrierID, ProductGroupID, OriginStateID, DestStateID, RegionID, EffectiveDate, EndDate, RoundingTypeID, SubUnitID, OriginThresholdMinutes, DestThresholdMinutes, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, ShipperID)
		SELECT ID, CarrierID, ProductGroupID, OriginStateID, DestStateID, RegionID, EffectiveDate, EndDate, RoundingTypeID, SubUnitID, ThresholdMinutes, ThresholdMinutes, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, ShipperID FROM dbo.tblCarrierWaitFeeParameter WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_tblCarrierWaitFeeParameter OFF
GO
ALTER TABLE dbo.tblOrderSettlementCarrier
	DROP CONSTRAINT FK_OrderSettlementCarrier_WaitFeeParameter
GO
DROP TABLE dbo.tblCarrierWaitFeeParameter
GO
EXECUTE sp_rename N'dbo.Tmp_tblCarrierWaitFeeParameter', N'tblCarrierWaitFeeParameter', 'OBJECT' 
GO
ALTER TABLE dbo.tblCarrierWaitFeeParameter ADD CONSTRAINT
	PK_CarrierWaitFeeParameter PRIMARY KEY NONCLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE UNIQUE CLUSTERED INDEX udxCarrierWaitFeeParameter_Main ON dbo.tblCarrierWaitFeeParameter
	(
	CarrierID,
	ProductGroupID,
	OriginStateID,
	DestStateID,
	RegionID,
	EffectiveDate
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxCarrierWaitFeeParameter_Carrier ON dbo.tblCarrierWaitFeeParameter
	(
	CarrierID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxCarrierWaitFeeParameter_Region ON dbo.tblCarrierWaitFeeParameter
	(
	RegionID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxCarrierWaitFeeParameter_OriginState ON dbo.tblCarrierWaitFeeParameter
	(
	OriginStateID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxCarrierWaitFeeParameter_DestState ON dbo.tblCarrierWaitFeeParameter
	(
	DestStateID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxCarrierWaitFeeParameter_ProductGroup ON dbo.tblCarrierWaitFeeParameter
	(
	ProductGroupID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.tblCarrierWaitFeeParameter ADD CONSTRAINT
	CK_CarrierWaitFeeParameter_OriginThresholdMinutes_Positive CHECK (([OriginThresholdMinutes] >= (0)))
GO
ALTER TABLE dbo.tblCarrierWaitFeeParameter ADD CONSTRAINT
	CK_CarrierWaitFeeParameter_EndDate_Greater CHECK (([EndDate]>=[EffectiveDate]))
GO
ALTER TABLE dbo.tblCarrierWaitFeeParameter ADD CONSTRAINT
	CK_CarrierWaitFeeParameter_DestThresholdMinutes_Positive CHECK (([DestThresholdMinutes] >= (0)))
GO
ALTER TABLE dbo.tblCarrierWaitFeeParameter ADD CONSTRAINT
	FK_CarrierWaitFeeParameter_Carrier FOREIGN KEY
	(
	CarrierID
	) REFERENCES dbo.tblCarrier
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblCarrierWaitFeeParameter ADD CONSTRAINT
	FK_CarrierWaitFeeParameter_ProductGroup FOREIGN KEY
	(
	ProductGroupID
	) REFERENCES dbo.tblProductGroup
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblCarrierWaitFeeParameter ADD CONSTRAINT
	FK_CarrierWaitFeeParameter_OriginState FOREIGN KEY
	(
	OriginStateID
	) REFERENCES dbo.tblState
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblCarrierWaitFeeParameter ADD CONSTRAINT
	FK_CarrierWaitFeeParameter_DestState FOREIGN KEY
	(
	DestStateID
	) REFERENCES dbo.tblState
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblCarrierWaitFeeParameter ADD CONSTRAINT
	FK_CarrierWaitFeeParameter_Region FOREIGN KEY
	(
	RegionID
	) REFERENCES dbo.tblRegion
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblCarrierWaitFeeParameter ADD CONSTRAINT
	FK_CarrierWaitFeeParameter FOREIGN KEY
	(
	ShipperID
	) REFERENCES dbo.tblCustomer
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblCarrierWaitFeeParameter ADD CONSTRAINT
	FK_CarrierWaitFeeParameter_RoundingType FOREIGN KEY
	(
	RoundingTypeID
	) REFERENCES dbo.tblWaitFeeRoundingType
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblCarrierWaitFeeParameter ADD CONSTRAINT
	FK_CarrierWaitFeeParameter_SubUnit FOREIGN KEY
	(
	SubUnitID
	) REFERENCES dbo.tblWaitFeeSubUnit
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
/***********************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			prevent changes to Locked records (except for EndDate)
***********************************************/
CREATE TRIGGER trigCarrierWaitFeeParameter_IU ON dbo.tblCarrierWaitFeeParameter AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblCarrierWaitFeeParameter X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(i.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Parameters are not allowed'
	END
	ElSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewCarrierWaitFeeParameter X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.CarrierID, X.CarrierID) = 0
			OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
			OR dbo.fnCompareNullableInts(d.OriginStateID, X.OriginStateID) = 0
			OR dbo.fnCompareNullableInts(d.DestStateID, X.DestStateID) = 0
			OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
			OR d.EffectiveDate <> X.EffectiveDate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	
	IF (@error IS NOT NULL)
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
END
GO
/*************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent deletion of Locked (in use) records
/*************************************/
CREATE TRIGGER trigCarrierWaitFeeParameter_D ON dbo.tblCarrierWaitFeeParameter AFTER DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewCarrierWaitFeeParameter X ON X.ID = d.ID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('Locked (in use) records cannot be deleted', 16, 1)
		RETURN
	END
END
GO
ALTER TABLE dbo.tblOrderSettlementCarrier ADD CONSTRAINT
	FK_OrderSettlementCarrier_WaitFeeParameter FOREIGN KEY
	(
	WaitFeeParameterID
	) REFERENCES dbo.tblCarrierWaitFeeParameter
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  SET NULL 
	
GO
ALTER TABLE dbo.tblOrderSettlementCarrier SET (LOCK_ESCALATION = TABLE)
GO

ALTER TABLE dbo.tblShipperWaitFeeParameter
	DROP CONSTRAINT FK_ShipperWaitFeeParameter_SubUnit
GO
ALTER TABLE dbo.tblWaitFeeSubUnit SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblShipperWaitFeeParameter
	DROP CONSTRAINT FK_ShipperWaitFeeParameter_RoundingType
GO
ALTER TABLE dbo.tblWaitFeeRoundingType SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblShipperWaitFeeParameter
	DROP CONSTRAINT FK_ShipperWaitFeeParameter_Region
GO
ALTER TABLE dbo.tblRegion SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblShipperWaitFeeParameter
	DROP CONSTRAINT FK_ShipperWaitFeeParameter_OriginState
GO
ALTER TABLE dbo.tblShipperWaitFeeParameter
	DROP CONSTRAINT FK_ShipperWaitFeeParameter_DestState
GO
ALTER TABLE dbo.tblState SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblShipperWaitFeeParameter
	DROP CONSTRAINT FK_ShipperWaitFeeParameter_ProductGroup
GO
ALTER TABLE dbo.tblProductGroup SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblShipperWaitFeeParameter
	DROP CONSTRAINT FK_ShipperWaitFeeParameter_Shipper
GO
ALTER TABLE dbo.tblCustomer SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblShipperWaitFeeParameter
	DROP CONSTRAINT DF_ShipperWaitFeeParameter_ThresholdMinutes
GO
ALTER TABLE dbo.tblShipperWaitFeeParameter
	DROP CONSTRAINT DF_ShipperWaitFeeParameter_CreateDateUTC
GO
ALTER TABLE dbo.tblShipperWaitFeeParameter
	DROP CONSTRAINT DF_ShipperWaitFeeParameter_CreatedByUser
GO
CREATE TABLE dbo.Tmp_tblShipperWaitFeeParameter
	(
	ID int NOT NULL IDENTITY (1, 1),
	ShipperID int NULL,
	ProductGroupID int NULL,
	OriginStateID int NULL,
	DestStateID int NULL,
	RegionID int NULL,
	EffectiveDate date NOT NULL,
	EndDate date NOT NULL,
	RoundingTypeID int NOT NULL,
	SubUnitID int NOT NULL,
	OriginThresholdMinutes int NOT NULL,
	DestThresholdMinutes int NOT NULL,
	CreateDateUTC smalldatetime NOT NULL,
	CreatedByUser varchar(100) NOT NULL,
	LastChangeDateUTC smalldatetime NULL,
	LastChangedByUser varchar(100) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_tblShipperWaitFeeParameter SET (LOCK_ESCALATION = TABLE)
GO
GRANT INSERT ON dbo.Tmp_tblShipperWaitFeeParameter TO dispatchcrude_iis_acct  AS dbo
GO
GRANT SELECT ON dbo.Tmp_tblShipperWaitFeeParameter TO dispatchcrude_iis_acct  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_tblShipperWaitFeeParameter TO dispatchcrude_iis_acct  AS dbo
GO
ALTER TABLE dbo.Tmp_tblShipperWaitFeeParameter ADD CONSTRAINT
	DF_ShipperWaitFeeParameter_OriginThresholdMinutes DEFAULT ((60)) FOR OriginThresholdMinutes
GO
ALTER TABLE dbo.Tmp_tblShipperWaitFeeParameter ADD CONSTRAINT
	DF_tblShipperWaitFeeParameter_DestThresholdMinutes DEFAULT 0 FOR DestThresholdMinutes
GO
ALTER TABLE dbo.Tmp_tblShipperWaitFeeParameter ADD CONSTRAINT
	DF_ShipperWaitFeeParameter_CreateDateUTC DEFAULT (getutcdate()) FOR CreateDateUTC
GO
ALTER TABLE dbo.Tmp_tblShipperWaitFeeParameter ADD CONSTRAINT
	DF_ShipperWaitFeeParameter_CreatedByUser DEFAULT (suser_name()) FOR CreatedByUser
GO
SET IDENTITY_INSERT dbo.Tmp_tblShipperWaitFeeParameter ON
GO
IF EXISTS(SELECT * FROM dbo.tblShipperWaitFeeParameter)
	 EXEC('INSERT INTO dbo.Tmp_tblShipperWaitFeeParameter (ID, ShipperID, ProductGroupID, OriginStateID, DestStateID, RegionID, EffectiveDate, EndDate, RoundingTypeID, SubUnitID, OriginThresholdMinutes, DestThresholdMinutes, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser)
		SELECT ID, ShipperID, ProductGroupID, OriginStateID, DestStateID, RegionID, EffectiveDate, EndDate, RoundingTypeID, SubUnitID, ThresholdMinutes, ThresholdMinutes, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser FROM dbo.tblShipperWaitFeeParameter WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_tblShipperWaitFeeParameter OFF
GO
ALTER TABLE dbo.tblOrderSettlementShipper
	DROP CONSTRAINT FK_OrderSettlementShipper_WaitFeeParameter
GO
DROP TABLE dbo.tblShipperWaitFeeParameter
GO
EXECUTE sp_rename N'dbo.Tmp_tblShipperWaitFeeParameter', N'tblShipperWaitFeeParameter', 'OBJECT' 
GO
ALTER TABLE dbo.tblShipperWaitFeeParameter ADD CONSTRAINT
	PK_ShipperWaitFeeParameter PRIMARY KEY NONCLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE UNIQUE CLUSTERED INDEX udxShipperWaitFeeParameter_Main ON dbo.tblShipperWaitFeeParameter
	(
	ShipperID,
	ProductGroupID,
	OriginStateID,
	DestStateID,
	RegionID,
	EffectiveDate
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxShipperWaitFeeParameter_Shipper ON dbo.tblShipperWaitFeeParameter
	(
	ShipperID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxShipperWaitFeeParameter_Region ON dbo.tblShipperWaitFeeParameter
	(
	RegionID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxShipperWaitFeeParameter_OriginState ON dbo.tblShipperWaitFeeParameter
	(
	OriginStateID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxShipperWaitFeeParameter_DestState ON dbo.tblShipperWaitFeeParameter
	(
	DestStateID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idxShipperWaitFeeParameter_ProductGroup ON dbo.tblShipperWaitFeeParameter
	(
	ProductGroupID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.tblShipperWaitFeeParameter ADD CONSTRAINT
	CK_ShipperWaitFeeParameter_OriginThresholdMinutes_Positive CHECK (([OriginThresholdMinutes] >= (0)))
GO
ALTER TABLE dbo.tblShipperWaitFeeParameter ADD CONSTRAINT
	CK_ShipperWaitFeeParameter_EndDate_Greater CHECK (([EndDate]>=[EffectiveDate]))
GO
ALTER TABLE dbo.tblShipperWaitFeeParameter ADD CONSTRAINT
	CK_ShipperWaitFeeParameter_DestThresholdMinutes_Positive CHECK (([DestThresholdMinutes] >= (0)))
GO
ALTER TABLE dbo.tblShipperWaitFeeParameter ADD CONSTRAINT
	FK_ShipperWaitFeeParameter_Shipper FOREIGN KEY
	(
	ShipperID
	) REFERENCES dbo.tblCustomer
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblShipperWaitFeeParameter ADD CONSTRAINT
	FK_ShipperWaitFeeParameter_ProductGroup FOREIGN KEY
	(
	ProductGroupID
	) REFERENCES dbo.tblProductGroup
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblShipperWaitFeeParameter ADD CONSTRAINT
	FK_ShipperWaitFeeParameter_OriginState FOREIGN KEY
	(
	OriginStateID
	) REFERENCES dbo.tblState
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblShipperWaitFeeParameter ADD CONSTRAINT
	FK_ShipperWaitFeeParameter_DestState FOREIGN KEY
	(
	DestStateID
	) REFERENCES dbo.tblState
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblShipperWaitFeeParameter ADD CONSTRAINT
	FK_ShipperWaitFeeParameter_Region FOREIGN KEY
	(
	RegionID
	) REFERENCES dbo.tblRegion
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblShipperWaitFeeParameter ADD CONSTRAINT
	FK_ShipperWaitFeeParameter_RoundingType FOREIGN KEY
	(
	RoundingTypeID
	) REFERENCES dbo.tblWaitFeeRoundingType
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblShipperWaitFeeParameter ADD CONSTRAINT
	FK_ShipperWaitFeeParameter_SubUnit FOREIGN KEY
	(
	SubUnitID
	) REFERENCES dbo.tblWaitFeeSubUnit
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
/***********************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			prevent changes to Locked records (except for EndDate)
***********************************************/
CREATE TRIGGER trigShipperWaitFeeParameter_IU ON dbo.tblShipperWaitFeeParameter AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblShipperWaitFeeParameter X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(i.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Parameters are not allowed'
	END
	ElSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewShipperWaitFeeParameter X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
			OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
			OR dbo.fnCompareNullableInts(d.OriginStateID, X.OriginStateID) = 0
			OR dbo.fnCompareNullableInts(d.DestStateID, X.DestStateID) = 0
			OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
			OR d.EffectiveDate <> X.EffectiveDate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	
	IF (@error IS NOT NULL)
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
END
GO
/*************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent deletion of Locked (in use) records
/*************************************/
CREATE TRIGGER trigShipperWaitFeeParameter_D ON dbo.tblShipperWaitFeeParameter AFTER DELETE AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM deleted d JOIN viewShipperWaitFeeParameter X ON X.ID = d.ID WHERE X.Locked = 1)
	BEGIN
		RAISERROR('Locked (in use) records cannot be deleted', 16, 1)
		RETURN
	END
END
GO
ALTER TABLE dbo.tblOrderSettlementShipper ADD CONSTRAINT
	FK_OrderSettlementShipper_WaitFeeParameter FOREIGN KEY
	(
	WaitFeeParameterID
	) REFERENCES dbo.tblShipperWaitFeeParameter
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  SET NULL 
	
GO
ALTER TABLE dbo.tblOrderSettlementShipper SET (LOCK_ESCALATION = TABLE)
GO

EXEC _spRefreshAllViews
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier WaitFeeParameter info for the specified order
/***********************************/
ALTER FUNCTION [dbo].[fnCarrierWaitFeeParameter](@StartDate date, @EndDate date, @ShipperID int, @CarrierID int, @ProductGroupID int, @OriginStateID int, @DestStateID int, @RegionID int, @bestMatchOnly bit = 0)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID
			, Ranking = dbo.fnRateRanking(@ShipperID, R.ShipperID, 32, 0)
				  + dbo.fnRateRanking(@CarrierID, R.CarrierID, 16, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 8, 0)
				  + dbo.fnRateRanking(@OriginStateID, R.OriginStateID, 4, 0)
				  + dbo.fnRateRanking(@DestStateID, R.DestStateID, 2, 0)
				  + dbo.fnRateRanking(@RegionID, R.RegionID, 1, 0)
		FROM  dbo.viewCarrierWaitFeeParameter R
		WHERE coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@CarrierID, 0), R.CarrierID, 0) = coalesce(CarrierID, nullif(@CarrierID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@OriginStateID, 0), R.OriginStateID, 0) = coalesce(OriginStateID, nullif(@OriginStateID, 0), 0)
		  AND coalesce(nullif(@DestStateID, 0), R.DestStateID, 0) = coalesce(DestStateID, nullif(@DestStateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	)
	
	SELECT R.ID, ShipperID, CarrierID, ProductGroupID, OriginStateID, DestStateID, RegionID, SubUnitID, RoundingTypeID, OriginThresholdMinutes, DestThresholdMinutes, EffectiveDate, EndDate, NextEffectiveDate
	  , BestMatch, Ranking
	  , Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewCarrierWaitFeeParameter R
	JOIN (
		SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 6  -- ensure some type of match occurred on all 6 criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier WaitFeeParameter rows for the specified criteria
/***********************************/
ALTER FUNCTION [dbo].[fnCarrierWaitFeeParametersDisplay](@StartDate date, @EndDate date, @ShipperID int, @CarrierID int, @ProductGroupID int, @OriginStateID int, @DestStateID int, @RegionID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.OriginStateID, R.DestStateID, R.RegionID
		, R.SubUnitID, R.RoundingTypeID, R.OriginThresholdMinutes, R.DestThresholdMinutes, R.EffectiveDate, R.EndDate, R.NextEffectiveDate
		, Shipper = S.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, OriginState = OS.FullName
		, OriginStateAbbrev = OS.Abbreviation
		, DestinationState = DS.FullName
		, DestinationStateAbbrev = DS.Abbreviation
		, Region = REG.Name
		, RoundingType = WFRT.Name
		, SubUnit = WFSU.Name
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch, Ranking
	FROM dbo.fnCarrierWaitFeeParameter(@StartDate, @EndDate, @ShipperID, @CarrierID, @ProductGroupID, @OriginStateID, @DestStateID, @RegionID, 0) R 
	JOIN tblWaitFeeRoundingType WFRT ON WFRT.ID = R.RoundingTypeID
	JOIN tblWaitFeeSubUnit WFSU ON WFSU.ID = R.SubUnitID
	LEFT JOIN tblCustomer S ON S.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblState OS ON OS.ID = R.OriginStateID
	LEFT JOIN tblState DS ON DS.ID = R.DestStateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	ORDER BY EffectiveDate
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier WaitFeeParameter info for the specified order
/***********************************/
ALTER FUNCTION fnOrderCarrierWaitFeeParameter(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, SubUnitID, RoundingTypeID, OriginThresholdMinutes, DestThresholdMinutes
	FROM dbo.viewOrder O
	CROSS APPLY dbo.fnCarrierWaitFeeParameter(isnull(O.OrderDate, O.DueDate), null, O.CustomerID, O.CarrierID, O.ProductGroupID, O.OriginStateID, O.DestStateID, O.OriginRegionID, 1) R
	WHERE O.ID = @ID 
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OriginWait data info for the specified order
/***********************************/
ALTER FUNCTION [dbo].[fnOrderCarrierOriginWaitData](@ID int) RETURNS TABLE AS RETURN
	SELECT RateID, BillableMinutes, BillableHours, Amount = BillableHours * Rate, WaitFeeParameterID
	FROM (
		SELECT RateID, BillableMinutes
			, BillableHours = dbo.fnComputeBillableWaitHours(BillableMinutes, SubUnitID, RoundingTypeID)
			, Rate
			, WaitFeeParameterID
		FROM (
			SELECT RateID = OWR.ID
				, Rate
				, BillableMinutes = dbo.fnMaxInt(OWR.Minutes - WFP.OriginThresholdMinutes, 0)
				, WaitFeeParameterID = WFP.ID, WFP.SubUnitID, WFP.RoundingTypeID
			FROM dbo.fnOrderCarrierOriginWaitRate(@ID) OWR 
			CROSS APPLY dbo.fnOrderCarrierWaitFeeParameter(@ID) WFP
		) X
	) X2
	WHERE BillableHours > 0.0
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier DestinationWait data info for the specified order
/***********************************/
ALTER FUNCTION [dbo].[fnOrderCarrierDestinationWaitData](@ID int) RETURNS TABLE AS RETURN
	SELECT RateID, BillableMinutes, BillableHours, Amount = BillableHours * Rate
	FROM (
		SELECT RateID, BillableMinutes
			, BillableHours = dbo.fnComputeBillableWaitHours(BillableMinutes, SubUnitID, RoundingTypeID)
			, Rate
		FROM (
			SELECT RateID = OWR.ID
				, Rate
				, BillableMinutes = dbo.fnMaxInt(OWR.Minutes - WFP.DestThresholdMinutes, 0)
				, WFP.SubUnitID, WFP.RoundingTypeID
			FROM dbo.fnOrderCarrierDestinationWaitRate(@ID) OWR 
			CROSS JOIN dbo.fnOrderCarrierWaitFeeParameter(@ID) WFP 
		) X
	) X2
	WHERE BillableHours > 0.0
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper WaitFeeParameter info for the specified order
/***********************************/
ALTER FUNCTION [dbo].[fnShipperWaitFeeParameter](@StartDate date, @EndDate date, @ShipperID int, @ProductGroupID int, @OriginStateID int, @DestStateID int, @RegionID int, @bestMatchOnly bit = 0)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID
			, Ranking = dbo.fnRateRanking(@ShipperID, R.ShipperID, 16, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 8, 0)
				  + dbo.fnRateRanking(@OriginStateID, R.OriginStateID, 4, 0)
				  + dbo.fnRateRanking(@DestStateID, R.DestStateID, 2, 0)
				  + dbo.fnRateRanking(@RegionID, R.RegionID, 1, 0)
		FROM  dbo.viewShipperWaitFeeParameter R
		WHERE coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@OriginStateID, 0), R.OriginStateID, 0) = coalesce(OriginStateID, nullif(@OriginStateID, 0), 0)
		  AND coalesce(nullif(@DestStateID, 0), R.DestStateID, 0) = coalesce(DestStateID, nullif(@DestStateID, 0), 0)
		  AND coalesce(nullif(@RegionID, 0), R.RegionID, 0) = coalesce(RegionID, nullif(@RegionID, 0), 0)
		  AND (@StartDate BETWEEN EffectiveDate AND EndDate
			 OR @EndDate BETWEEN EffectiveDate AND EndDate
			 OR EffectiveDate BETWEEN @StartDate AND @EndDate)
	)
	
	SELECT R.ID, ShipperID, ProductGroupID, OriginStateID, DestStateID, RegionID, SubUnitID, RoundingTypeID, OriginThresholdMinutes, DestThresholdMinutes, EffectiveDate, EndDate, NextEffectiveDate
	  , BestMatch, Ranking
	  , Locked, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM viewShipperWaitFeeParameter R
	JOIN (
		SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE @StartDate = ISNULL(@EndDate, @StartDate)
			  AND Ranking % 1 = 0.01 * 5  -- ensure some type of match occurred on all 6 criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper WaitFeeParameter rows for the specified criteria
/***********************************/
ALTER FUNCTION [dbo].[fnShipperWaitFeeParametersDisplay](@StartDate date, @EndDate date, @ShipperID int, @ProductGroupID int, @OriginStateID int, @DestStateID int, @RegionID int)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.ShipperID, R.ProductGroupID, R.OriginStateID, R.DestStateID, R.RegionID, R.SubUnitID, R.RoundingTypeID
		, R.OriginThresholdMinutes, R.DestThresholdMinutes, R.EffectiveDate, R.EndDate, R.NextEffectiveDate
		, Shipper = C.Name
		, ProductGroup = PG.Name
		, OriginState = OS.FullName
		, OriginStateAbbrev = OS.Abbreviation
		, DestinationState = DS.FullName
		, DestinationStateAbbrev = DS.Abbreviation
		, Region = REG.Name
		, RoundingType = WFRT.Name
		, SubUnit = WFSU.Name
		, Locked
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch, Ranking
	FROM dbo.fnShipperWaitFeeParameter(@StartDate, @EndDate, @ShipperID, @ProductGroupID, @OriginStateID, @DestStateID, @RegionID, 0) R 
	JOIN tblWaitFeeRoundingType WFRT ON WFRT.ID = R.RoundingTypeID
	JOIN tblWaitFeeSubUnit WFSU ON WFSU.ID = R.SubUnitID
	LEFT JOIN tblCustomer C ON C.ID = R.ShipperID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblState OS ON OS.ID = R.OriginStateID
	LEFT JOIN tblState DS ON DS.ID = R.DestStateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	ORDER BY EffectiveDate
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper WaitFeeParameter info for the specified order
/***********************************/
ALTER FUNCTION fnOrderShipperWaitFeeParameter(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, SubUnitID, RoundingTypeID, OriginThresholdMinutes, DestThresholdMinutes
	FROM dbo.viewOrder O
	CROSS APPLY dbo.fnShipperWaitFeeParameter(isnull(O.OrderDate, O.DueDate), null, O.CustomerID, O.ProductGroupID, O.OriginStateID, O.DestStateID, O.OriginRegionID, 1) R
	WHERE O.ID = @ID 
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper OriginWait data info for the specified order
/***********************************/
ALTER FUNCTION [dbo].[fnOrderShipperOriginWaitData](@ID int) RETURNS TABLE AS RETURN
	SELECT RateID, BillableMinutes, BillableHours, Amount = BillableHours * Rate, WaitFeeParameterID
	FROM (
		SELECT RateID, BillableMinutes
			, BillableHours = dbo.fnComputeBillableWaitHours(BillableMinutes, SubUnitID, RoundingTypeID)
			, Rate
			, WaitFeeParameterID
		FROM (
			SELECT RateID = OWR.ID
				, Rate
				, BillableMinutes = dbo.fnMaxInt(OWR.Minutes - WFP.OriginThresholdMinutes, 0)
				, WaitFeeParameterID = WFP.ID, WFP.SubUnitID, WFP.RoundingTypeID
			FROM dbo.fnOrderShipperOriginWaitRate(@ID) OWR 
			CROSS APPLY dbo.fnOrderShipperWaitFeeParameter(@ID) WFP
		) X
	) X2
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper DestinationWait data info for the specified order
/***********************************/
ALTER FUNCTION [dbo].[fnOrderShipperDestinationWaitData](@ID int) RETURNS TABLE AS RETURN
	SELECT RateID, BillableMinutes, BillableHours, Amount = BillableHours * Rate
	FROM (
		SELECT RateID, BillableMinutes
			, BillableHours = dbo.fnComputeBillableWaitHours(BillableMinutes, SubUnitID, RoundingTypeID)
			, Rate
		FROM (
			SELECT RateID = OWR.ID
				, Rate
				, BillableMinutes = dbo.fnMaxInt(OWR.Minutes - WFP.DestThresholdMinutes, 0)
				, WFP.SubUnitID, WFP.RoundingTypeID
			FROM dbo.fnOrderShipperDestinationWaitRate(@ID) OWR 
			CROSS JOIN dbo.fnOrderShipperWaitFeeParameter(@ID) WFP 
		) X
	) X2
GO

/**********************************************
Date Created: 13 Feb 2015
Author: Kevin Alons
Purpose: retrieve the combined thresholdminutes for a single order
**********************************************/
CREATE FUNCTION fnOrderCombinedThresholdMinutes(@ID int)
RETURNS TABLE AS RETURN
	SELECT OriginThresholdMinutes = dbo.fnMaxInt(dbo.fnMaxInt(OCWFP.OriginThresholdMinutes, OSWFP.OriginThresholdMinutes), coalesce(OCWFP.OriginThresholdMinutes, OSWFP.OriginThresholdMinutes, STM.Value))
		, DestThresholdMinutes = dbo.fnMaxInt(dbo.fnMaxInt(OCWFP.DestThresholdMinutes, OSWFP.DestThresholdMinutes), coalesce(OCWFP.DestThresholdMinutes, OSWFP.DestThresholdMinutes, STM.Value))
	FROM dbo.fnOrderCarrierWaitFeeParameter(@ID) OCWFP
	CROSS JOIN dbo.fnOrderShipperWaitFeeParameter(@ID) OSWFP
	CROSS JOIN (SELECT Value FROM tblSetting WHERE ID = 6) STM
GO
GRANT SELECT ON fnOrderCombinedThresholdMinutes TO dispatchcrude_iis_acct
GO

/*******************************************/
-- Date Created: 25 Apr 2013
-- Author: Kevin Alons
-- Purpose: return readonly Order data for Driver App sync
/*******************************************/
ALTER FUNCTION [dbo].[fnOrderReadOnly_DriverApp]( @DriverID int, @LastChangeDateUTC datetime ) RETURNS TABLE AS
RETURN 
	SELECT O.ID
		, O.OrderNum
		, O.StatusID
		, O.TicketTypeID
		, PriorityNum = cast(P.PriorityNum as int) 
		, O.Product
		, O.DueDate
		, O.Origin
		, O.OriginFull
		, OO.OriginType
		, O.OriginUomID
		, OriginStation = OO.Station 
		, OriginLeaseNum = OO.LeaseNum 
		, OriginCounty = OO.County 
		, OriginLegalDescription = OO.LegalDescription 
		, O.OriginNDIC
		, O.OriginNDM
		, O.OriginCA
		, O.OriginState
		, OO.WellAPI AS OriginAPI
		, OriginLat = OO.LAT 
		, OriginLon = OO.LON 
		, OriginGeoFenceRadiusMeters = OO.GeoFenceRadiusMeters 
		, O.Destination
		, O.DestinationFull
		, DestType = D.DestinationType 
		, O.DestUomID
		, DestLat = D.LAT 
		, DestLon = D.LON 
		, DestGeoFenceRadiusMeters = D.GeoFenceRadiusMeters 
		, D.Station AS DestinationStation
		, O.CreateDateUTC
		, O.CreatedByUser
		, O.LastChangeDateUTC
		, O.LastChangedByUser
		, DeleteDateUTC = isnull(ODAVD.VirtualDeleteDateUTC, O.DeleteDateUTC) 
		, DeletedByUser = isnull(ODAVD.VirtualDeletedByUser, O.DeletedByUser) 
		, O.OriginID
		, O.DestinationID
		, PriorityID = cast(O.PriorityID AS int) 
		, O.Operator
		, O.OperatorID
		, O.Pumper
		, O.PumperID
		, O.Producer
		, O.ProducerID
		, O.Customer
		, O.CustomerID
		, O.Carrier
		, O.CarrierID
		, O.ProductID
		, O.TicketType
		, PrintHeaderBlob = C.ZPLHeaderBlob 
		, EmergencyInfo = isnull(C.EmergencyInfo, 'For an emergency (spill, leak, fire, or accident) contact CHEMTREC @ 800.424.9300 (toll free)') 
		, O.DestTicketTypeID
		, O.DestTicketType
		, O.OriginTankNum
		, O.OriginTankID
		, O.DispatchNotes
		, O.DispatchConfirmNum
		, RouteActualMiles = isnull(R.ActualMiles, 0)
		, CarrierAuthority = CA.Authority 
		, OriginTimeZone = OO.TimeZone
		, DestTimeZone = D.TimeZone
		, OCTM.OriginThresholdMinutes
		, OCTM.DestThresholdMinutes
	FROM dbo.viewOrder O
	JOIN dbo.tblPriority P ON P.ID = O.PriorityID
	JOIN dbo.viewOrigin OO ON OO.ID = O.OriginID
	JOIN dbo.viewDestination D ON D.ID = O.DestinationID
	JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
	JOIN dbo.tblRoute R ON R.ID = O.RouteID
	JOIN dbo.tblCarrier CA ON CA.ID = O.CarrierID
	LEFT JOIN tblOrderDriverAppVirtualDelete ODAVD ON ODAVD.OrderID = O.ID AND ODAVD.DriverID = @DriverID
	CROSS JOIN (SELECT DATEADD(second, -5, @LastChangeDateUTC) AS LCD) LCD
	OUTER APPLY dbo.fnOrderCombinedThresholdMinutes(O.ID) OCTM
	WHERE O.ID IN (
		SELECT id FROM tblOrder WHERE DriverID = @driverID AND StatusID IN (2,7,8,3) 
		UNION 
		SELECT OrderID FROM tblOrderDriverAppVirtualDelete WHERE DriverID = @driverID
	)
	  AND (
		@LastChangeDateUTC IS NULL 
		OR O.CreateDateUTC >= LCD.LCD
		OR O.LastChangeDateUTC >= LCD.LCD
		OR O.DeleteDateUTC >= LCD.LCD
		OR C.LastChangeDateUTC >= LCD.LCD
		OR CA.LastChangeDateUTC >= LCD.LCD
		OR OO.LastChangeDateUTC >= LCD.LCD
		OR R.LastChangeDateUTC >= LCD.LCD
		OR ODAVD.VirtualDeleteDateUTC >= LCD.LCD)
GO

EXEC _spRebuildAllObjects
GO

COMMIT
SET NOEXEC OFF