DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '1.4.6', @NewVersion = '1.4.7'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

/***********************************/
-- Date Created: 26 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Drivers table records with "friendly" translated values included
/***********************************/
ALTER VIEW [dbo].[viewDriver] AS
SELECT *
	, CASE WHEN DeleteDate IS NULL THEN '' ELSE 'Deleted: ' END + FullName AS FullNameD
	, CASE WHEN DeleteDate IS NULL THEN 1 ELSE 0 END AS Active
FROM (
	SELECT D.*
	, D.FirstName + ' ' + D.LastName As FullName
	, D.LastName + ', ' + D.FirstName AS FullNameLF
	, isnull(CT.Name, 'Unknown') AS CarrierType
	, C.Name AS Carrier
	, S.Abbreviation AS StateAbbrev 
	, T.FullName AS Truck
	, T1.FullName AS Trailer
	, T2.FullName AS Trailer2
	FROM dbo.tblDriver D 
	JOIN dbo.tblCarrier C ON C.ID = D.CarrierID 
	LEFT JOIN dbo.tblCarrierType CT ON CT.ID = C.CarrierTypeID
	LEFT JOIN tblState S ON S.ID = D.StateID
	LEFT JOIN viewTruck T ON T.ID = D.TruckID
	LEFT JOIN viewTrailer T1 ON T1.ID = D.TrailerID
	LEFT JOIN viewTrailer T2 ON T2.ID = D.Trailer2ID
) X

GO

/***********************************/
-- Date Created: 26 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Trailer records with FullName & translated "friendly" values
/***********************************/
ALTER VIEW [dbo].[viewTrailer] AS
SELECT T.*
	, ISNULL(Compartment1Barrels, 0) 
		+ ISNULL(Compartment2Barrels, 0) 
		+ ISNULL(Compartment3Barrels, 0) 
		+ ISNULL(Compartment4Barrels, 0) 
		+ ISNULL(Compartment5Barrels, 0) AS TotalCapacityBarrels
	, CASE WHEN isnull(Compartment1Barrels, 0) > 0 THEN 1 ELSE 0 END 
		+ CASE WHEN isnull(Compartment2Barrels, 0) > 0 THEN 1 ELSE 0 END 
		+ CASE WHEN isnull(Compartment3Barrels, 0) > 0 THEN 1 ELSE 0 END
		+ CASE WHEN isnull(Compartment4Barrels, 0) > 0 THEN 1 ELSE 0 END 
		+ CASE WHEN isnull(Compartment5Barrels, 0) > 0 THEN 1 ELSE 0 END AS CompartmentCount
	, T.IDNumber AS FullName
	, TT.Name AS TrailerType
	, isnull(CT.Name, 'Unknown') AS CarrierType
	, C.Name AS Carrier
FROM dbo.tblTrailer T
LEFT JOIN dbo.tblTrailerType TT ON TT.ID = T.TrailerTypeID
JOIN dbo.tblCarrier C ON C.ID = T.CarrierID
LEFT JOIN dbo.tblCarrierType CT ON CT.ID = C.CarrierTypeID

GO

/***********************************/
-- Date Created: 26 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Truck records with FullName & translated "friendly" values
/***********************************/
ALTER VIEW [dbo].[viewTruck] AS
SELECT T.*
	, T.IDNumber AS FullName
	, isnull(CT.Name, 'Unknown') AS CarrierType
	, C.Name AS Carrier
FROM dbo.tblTruck T
JOIN dbo.tblCarrier C ON C.ID = T.CarrierID
LEFT JOIN dbo.tblCarrierType CT ON CT.ID = C.CarrierTypeID

GO

EXEC _spRefreshAllViews
GO

COMMIT TRANSACTION
SET NOEXEC OFF

