delete from ELMAH_Error
delete from tblOrderDriverAppVirtualDelete
delete from tblorderticket
delete from tblOrderInvoiceCarrier
delete from tblOrderInvoiceCustomer
delete from tblOrderReroute
delete from tblOrderSignature
delete from tblDriverLocation
update tblOrder set StatusID = 3
update tblOrder set StatusID = -10
delete from tblorder
delete from tblDriverAvailability
delete from tbldriver
delete from tblTruckInspection
delete from tblTruckMaintenance
delete from tbltruck
delete from tblTrailerInspection
delete from tblTrailerMaintenance
delete from tblTrailer
delete from tblCarrierRates
delete from tblCarrierRouteRates
delete from tblroute
delete from tblCarrierFuelSurchargeRates
delete from tblcarrier
delete from tblShipperFuelSurchargeRates
delete from tblCustomerRates
delete from tblCustomerRouteRates
delete from tblCustomer
delete from tblCustomerTaxReport
delete from tblDestination
delete from tblOriginProducts
delete from tblOriginTankStrappingDeleted
delete from tblOriginTankStrapping
delete from tblOriginTank
delete from tblorigin
delete from tblProducer
delete from tblregion
delete from tblDestinationCustomers
delete from tblDriver_Sync
delete from tblDriverAvailability
delete from tblPumper
delete from tblOperator

delete from aspnet_Profile where UserId in (select UserId from aspnet_Users where username not in ('kalons', 'bbloodworth', 'mtaylor', 'jpaul', 'jcole'))
delete from aspnet_UsersInRoles where UserId in (select UserId from aspnet_Users where username not in ('kalons', 'bbloodworth', 'mtaylor', 'jpaul', 'jcole'))
delete from aspnet_Membership where UserId in (select UserId from aspnet_Users where username not in ('kalons', 'bbloodworth', 'mtaylor', 'jpaul', 'jcole'))
delete from aspnet_users where username not in ('kalons', 'bbloodworth', 'mtaylor', 'jpaul', 'jcole')