-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.9.17.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.9.17.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0

SET NOEXEC OFF
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.9.17'
SELECT  @NewVersion = '3.9.18'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Origin Table Mx Page - performance optimization for page load times by caching CSV values for each origin'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

CREATE TABLE tblOriginProductsCSV
(
  OriginID int CONSTRAINT PK_OriginProductsCSV PRIMARY KEY
, ProductID_CSV varchar(max)
, Products_CSV varchar(max)
)
GO
GRANT SELECT, INSERT, DELETE ON tblOriginProductsCSV TO role_iis_acct
GO

/***************************************
-- Date Created: 2015/09/22 - 3.9.18
-- Author: Kevin Alons
-- Purpose: maintain a "cached" csv copy of the current OriginProducts (ID & Names) in tblOriginProductsCSV
***************************************/
CREATE TRIGGER trigOriginProducts_I ON tblOriginProducts FOR INSERT AS
BEGIN
	DELETE FROM tblOriginProductsCSV WHERE OriginID IN (SELECT OriginID FROM inserted)
	INSERT INTO tblOriginProductsCSV (OriginID, ProductID_CSV, Products_CSV)
		SELECT OriginID, dbo.fnOriginProductIDCSV(OriginID), dbo.fnOriginProductsCSV(OriginID)
		FROM tblOriginProducts 
		WHERE OriginID IN (SELECT OriginID FROM inserted)
		GROUP BY OriginID
END
GO

CREATE TABLE tblOriginCustomersCSV
(
  OriginID int CONSTRAINT PK_OriginCustomersCSV PRIMARY KEY
, CustomerID_CSV varchar(max)
, Customers_CSV varchar(max)
)
GO
GRANT SELECT, INSERT, DELETE ON tblOriginCustomersCSV TO role_iis_acct
GO

/***************************************
-- Date Created: 2015/09/22 - 3.9.18
-- Author: Kevin Alons
-- Purpose: maintain a "cached" csv copy of the current OriginCustomers (ID & Names) in tblOriginCustomersCSV
***************************************/
CREATE TRIGGER trigOriginCustomers_I ON tblOriginCustomers FOR INSERT AS
BEGIN
	DELETE FROM tblOriginCustomersCSV WHERE OriginID IN (SELECT OriginID FROM inserted)
	INSERT INTO tblOriginCustomersCSV (OriginID, CustomerID_CSV, Customers_CSV)
		SELECT OriginID, dbo.fnOriginCustomerIDCSV(OriginID), dbo.fnOriginCustomersCSV(OriginID)
		FROM tblOriginCustomers 
		WHERE OriginID IN (SELECT OriginID FROM inserted)
		GROUP BY OriginID
END
GO

INSERT INTO tblOriginProductsCSV (OriginID, ProductID_CSV, Products_CSV)
	SELECT OriginID, dbo.fnOriginProductIDCSV(OriginID), dbo.fnOriginProductsCSV(OriginID)
	FROM tblOriginProducts 
	GROUP BY OriginID
GO
INSERT INTO tblOriginCustomersCSV (OriginID, CustomerID_CSV, Customers_CSV)
	SELECT OriginID, dbo.fnOriginCustomerIDCSV(OriginID), dbo.fnOriginCustomersCSV(OriginID)
	FROM tblOriginCustomers 
	GROUP BY OriginID
GO

COMMIT
SET NOEXEC OFF

/*
SELECT *, Products = P.Products_CSV, P.ProductID_CSV, Customers = C.Customers_CSV, C.CustomerID_CSV
FROM viewOrigin O
JOIN tblOriginProductsCSV P ON P.OriginID = O.ID
JOIN tblOriginCustomersCSV C ON C.OriginID = O.ID
ORDER BY Name
*/