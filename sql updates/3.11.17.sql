SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.11.16.2'
SELECT  @NewVersion = '3.11.17'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'DCWEB-1117: Commodity Pricing'
	UNION 
	SELECT @NewVersion, 0, 'DCWEB-1117: Create core Commodity Pricing tables/views/functions'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

-- Create core table and views

CREATE TABLE tblCommodityIndex
(  
	ID INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_CommodityIndex PRIMARY KEY CLUSTERED,
	Name VARCHAR(100) NOT NULL CONSTRAINT udxCommodityIndex_Name UNIQUE(Name),
	ShortName VARCHAR(15) NOT NULL CONSTRAINT udxCommodityIndex_ShortName UNIQUE(ShortName),
	Description VARCHAR(255) NULL,
	ProductGroupID INT NOT NULL CONSTRAINT FK_CommodityIndex_ProductGroup REFERENCES tblProductGroup(ID),
	UomID INT NOT NULL CONSTRAINT FK_CommodityIndex_Uom REFERENCES tblUom(ID)
) 
GO


CREATE TABLE tblCommodityMethod
(
	ID INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_CommodityMethod PRIMARY KEY CLUSTERED,
	Name VARCHAR(100) NOT NULL CONSTRAINT udxCommodityMethod_Name UNIQUE(Name),
	ShortName VARCHAR(15) NOT NULL CONSTRAINT udxCommodityMethod_ShortName UNIQUE(ShortName),
	Description VARCHAR(255) NULL,
	TradeDaysOnly BIT NOT NULL CONSTRAINT DF_CommodityMethod_TradeDaysOnly DEFAULT 0
)
GO

SET IDENTITY_INSERT tblCommodityMethod ON
INSERT INTO tblCommodityMethod 
(ID, Name, ShortName, Description, TradeDaysOnly)
VALUES (1, 'CMA', 'CMA', 'Calculated Monthly Average', 0),
       (2, 'TMA', 'TMA', 'Trade Month Average', 1),
	   (3, 'Pick', 'PICK', 'User Pick', 0)
SET IDENTITY_INSERT tblCommodityMethod OFF
GO

CREATE TABLE tblCommodityPrice
(
	ID INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_CommodityPrice PRIMARY KEY CLUSTERED,
	PriceDate DATE NOT NULL,
	CommodityIndexID INT NOT NULL CONSTRAINT FK_CommodityPrice_CommodityIndex REFERENCES tblCommodityIndex(ID),
	IndexPrice MONEY NOT NULL,
)
GO
CREATE UNIQUE INDEX udxCommodityPrice ON tblCommodityPrice(CommodityIndexID, PriceDate) 
GO




CREATE TABLE tblCommodityPurchasePricebook
(
	ID INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_CommodityPurchasePricebook PRIMARY KEY CLUSTERED,
	OrderStartDate DATETIME NOT NULL,
	OrderEndDate DATETIME NOT NULL,
	ProducerID INT NULL CONSTRAINT FK_CommodityPurchasePricebook_Producer REFERENCES tblProducer(ID),
	OperatorID INT NULL CONSTRAINT FK_CommodityPurchasePricebook_Operator REFERENCES tblOperator(ID),
	ProductID INT NULL CONSTRAINT FK_CommodityPurchasePricebook_Product REFERENCES tblProduct(ID),
	ProductGroupID INT NULL CONSTRAINT FK_CommodityPurchasePricebook_ProductGroup REFERENCES tblProductGroup(ID),
	OriginID INT NULL CONSTRAINT FK_CommodityPurchasePricebook_Origin REFERENCES tblOrigin(ID),
	OriginStateID INT NULL CONSTRAINT FK_CommodityPurchasePricebook_OriginState REFERENCES tblState(ID),
	OriginRegionID INT NULL CONSTRAINT FK_CommodityPurchasePricebook_OriginRegion REFERENCES tblRegion(ID),
	CommodityIndexID INT NULL CONSTRAINT FK_CommodityPurchasePricebook_Index REFERENCES tblCommodityIndex(ID),
	CommodityMethodID INT NULL CONSTRAINT FK_CommodityPurhcasePricebook_Method REFERENCES tblCommodityMethod(ID),
	IndexStartDate DATETIME NOT NULL,
	IndexEndDate DATETIME NOT NULL,
	SettlementFactorID INT NOT NULL CONSTRAINT FK_CommodityPurchasePricebook_SettlementFactor REFERENCES tblSettlementFactor(ID),
	Deduct DECIMAL NULL,
	DeductType CHAR NULL CONSTRAINT CK_CommodityPurchasePricebook_DeductType CHECK(DeductType IN ('$', '%')),
	Premium DECIMAL NULL,
	PremiumType CHAR NULL CONSTRAINT CK_CommodityPurchasePricebook_PremiumType CHECK(PremiumType IN ('$', '%')),
	PremiumDesc VARCHAR(100) NULL,

	CreateDateUTC DATETIME NOT NULL CONSTRAINT DF_CommodityPurchasePricebook_CreateDateUTC DEFAULT (GETUTCDATE()),
	CreatedByUser VARCHAR(100) NOT NULL CONSTRAINT DF_CommodityPurchasePricebook_CreatedByUser DEFAULT ('N/A'),
	LastChangeDateUTC DATETIME NULL,
	LastChangedByUser VARCHAR(100) NULL
)
GO





CREATE TABLE tblTradeHolidays
(  
	HolidayDate DATE NOT NULL CONSTRAINT PK_TradeHolidays PRIMARY KEY CLUSTERED,
	Name VARCHAR(100) NOT NULL
) 
GO
INSERT INTO tblTradeHolidays 
VALUES ('2016-01-01', 'New Years Day'), 
       ('2016-01-18', 'Martin Luther King, Jr Day'),
	   ('2016-02-15', 'Washington''s Birthday'),
	   ('2016-03-25', 'Good Friday'),
	   ('2016-05-30', 'Memorial Day'),
	   ('2016-07-04', 'Independence Day'),
	   ('2016-09-05', 'Labor Day'),
	   ('2016-11-24', 'Thanksgiving Day'),
	   ('2016-12-26', 'Christmas'),

	   ('2017-01-02', 'New Years Day'), 
       ('2017-01-16', 'Martin Luther King, Jr Day'),
	   ('2017-02-20', 'Washington''s Birthday'),
	   ('2017-03-14', 'Good Friday'),
	   ('2017-05-29', 'Memorial Day'),
	   ('2017-07-04', 'Independence Day'),
	   ('2017-09-04', 'Labor Day'),
	   ('2017-11-23', 'Thanksgiving Day'),
	   ('2017-12-25', 'Christmas')
GO

/*********************************************
-- Date Created: 21 Apr 2016
-- Author: Joe Engler
-- Purpose: Calculate the commodity price for a given date range
*********************************************/
CREATE FUNCTION fnCalculateCommodityPriceFromMethod(@StartDate DATE, @EndDate DATE, @CommodityIndexID INT, @TradeDaysOnly BIT) RETURNS MONEY AS
BEGIN
	DECLARE @ret MONEY

	SELECT @ret = ISNULL(AVG(IndexPrice), 0)
	FROM tblCommodityPrice CP
	LEFT JOIN tblTradeHolidays H ON h.HolidayDate = CP.PriceDate
	WHERE CommodityIndexID = @CommodityIndexID
	  AND PriceDate BETWEEN @StartDate AND @EndDate
	  AND (   (@TradeDaysOnly = 0) -- Include non trade days (per method)
		   OR (DATEPART(WEEKDAY, CP.PriceDate) not in (1,7) -- skip weekends
		      AND H.HolidayDate IS NULL)) -- skip holidays

	RETURN (@ret)
END

GO 


/*********************************************
-- Date Created: 21 Apr 2016
-- Author: Joe Engler
-- Purpose: Calculate the commodity prices for a given purchase pricebook entry
*********************************************/
CREATE FUNCTION fnCalculateCommodityPrice(@CommodityPurchasePricebookID INT) RETURNS TABLE AS
RETURN
(
	SELECT CommodityIndexID
	     , Price
		 , Deduct = CASE WHEN DeductType = '%' THEN Price * Deduct/100.0 ELSE Deduct END
		 , Premium = CASE WHEN PremiumType = '%' THEN Price * Premium/100.0 ELSE Premium END
	     , CI.UomID
	FROM (
		SELECT CommodityIndexID
		     , Price = dbo.fnCalculateCommodityPriceFromMethod(CPP.IndexStartDate, CPP.IndexEndDate, CPP.CommodityIndexID, CM.TradeDaysOnly)
		     , Premium
		     , PremiumType
		     , Deduct
		     , DeductType
		FROM tblCommodityPurchasePricebook CPP
		LEFT JOIN tblCommodityMethod CM ON CM.ID = CPP.CommodityMethodID
		WHERE CPP.ID = @CommodityPurchasePricebookID
	) X
	LEFT JOIN tblCommodityIndex CI ON CI.ID = X.CommodityIndexID

)

GO 


COMMIT 
SET NOEXEC OFF