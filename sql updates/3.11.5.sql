SET NOEXEC OFF  -- since this is 
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.11.4'
SELECT  @NewVersion = '3.11.5'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Add tblDriverUpdate, Add SP spDriver_Update'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


CREATE TABLE [dbo].[tblDriverUpdate](
	[DriverID] [int] NOT NULL,
	[UpdateLastUTC] [datetime] NULL,
	[MobileAppVersion] [varchar](25) NULL,
	[TabletID] [varchar](20) NULL,
	[PrinterSerial] [varchar](100) NULL
) ON [PRIMARY]

GO

/*******************************************
-- Date Created: 16 Mar 2016
-- Author: Jeremiah Silliman
-- Purpose: Insert record with sync data listing driver, sync time and most recent mobile application version
-- Changes:
*******************************************/

CREATE PROCEDURE [dbo].[spDriver_Update]
(
  @TabletID varchar(20)
, @PrinterSerial varchar(100)
, @MobileAppVersion varchar(100)
, @DriverID int
) AS
BEGIN
	IF (@MobileAppVersion NOT IN (SELECT MobileAppVersion FROM tblDriverUpdate WHERE DriverID=@DriverID))
		INSERT INTO tblDriverUpdate (DriverID, MobileAppVersion, UpdateLastUTC, TabletID, PrinterSerial)
			SELECT @DriverID, @MobileAppVersion, GETDATE(), @TabletID, @PrinterSerial
			FROM tblDriver D
			WHERE D.ID = @DriverID
	END
GO

COMMIT
SET NOEXEC OFF