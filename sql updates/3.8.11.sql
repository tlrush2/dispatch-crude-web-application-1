-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.8.10.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.8.10.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0

DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.8.10'
SELECT  @NewVersion = '3.8.11'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Fix trigOrderTicket_IU logic to ensure the tblOrder.LastChangeDateUTC value is always set to NOW if updated'
	UNION
	SELECT @NewVersion, 0, 'Fix fnOrderEdit_DriverApp function to include records with an updated Accept|Pickup|Deliver LastChangeDateUTC value'
	UNION
	SELECT @NewVersion, 0, 'Fix fnOrderTicket_DriverApp function to include records with a new DeleteDateUTC value'
	UNION
	SELECT @NewVersion, 0, 'Audit page: fix query to use the new OrderRule.Max_Order_Qty_GAL value to determine orders that exceed max'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

CREATE TABLE tblOriginCustomers
(
	ID int IDENTITY(1,1) NOT NULL constraint PK_OriginCustomers primary key,
	CustomerID int NOT NULL constraint FK_OriginCustomers_Customer foreign key references tblCustomer(ID) on delete cascade,
	OriginID int NOT NULL constraint FK_OriginCustomers_Origin foreign key references tblOrigin(ID) on delete cascade
) 
GO
grant select, insert, delete on tblorigincustomers to role_iis_acct
go

insert into tblOriginCustomers (OriginID, CustomerID)
	select id, customerid from tblOrigin	
go

EXEC _spDropIndex 'idxOrigin_Customer'
GO
ALTER TABLE tblOrigin DROP COLUMN CustomerID
GO
ALTER TABLE tblOrigin DROP COLUMN ShipperRegion
GO

/*****************************************
-- Date Created: 19 Jan 2015
-- Author: Kevin Alons
-- Purpose: return the tblOrder table witha Local OrderDate field added
-- Changes:
	- 5/17/2015 - KDA - add local DeliverDate field
	- 3.8.11 - 2015/07/28 - KDA - remove OriginShipperRegion field
*****************************************/
ALTER VIEW viewOrderBase AS
	SELECT O.*
		, OriginStateID = OO.StateID
		, OriginRegionID = OO.RegionID
		, DestStateID = D.StateID
		, DestRegionID = D.RegionID
		, P.ProductGroupID
		, DR.DriverGroupID
		, OrderDate = cast(dbo.fnUTC_to_Local(O.OriginDepartTimeUTC, OO.TimeZoneID, OO.UseDST) as date) 
		, DeliverDate = cast(dbo.fnUTC_to_Local(O.DestDepartTimeUTC, D.TimeZoneID, D.UseDST) as date) 
	FROM tblOrder O
	JOIN tblOrigin OO ON OO.ID = O.OriginID
	JOIN tblDestination D ON D.ID = O.DestinationID
	LEFT JOIN tblProduct P ON P.ID = O.ProductID
	LEFT JOIN tblDriver DR ON DR.ID = O.DriverID
GO

/***********************************************
-- Author:		Kevin Alons
-- Create date: 2015/07/27
-- Description:	return Customers ID CSV field for the specified Origin
***********************************************/
CREATE FUNCTION fnOriginCustomerIDCSV(@originID int) RETURNS varchar(1000) AS BEGIN
	DECLARE @ret varchar(max)
	SELECT @ret = 
	  STUFF(
		(
		  SELECT ',' + ltrim(CustomerID)
		  FROM tblOriginCustomers
		  WHERE OriginID = @originID
		  FOR XML PATH(''),TYPE
		  ).value('.','VARCHAR(MAX)'
		), 1, 1, '')

	RETURN (@ret)
END
GO
GRANT EXECUTE ON fnOriginCustomerIDCSV TO role_iis_acct
GO

/***********************************************
-- Author:		Kevin Alons
-- Create date: 2015/07/27
-- Description:	return Customers CSV field for the specified Origin
***********************************************/
CREATE FUNCTION fnOriginCustomersCSV(@originID int) RETURNS varchar(1000) AS BEGIN
	DECLARE @ret varchar(max)
	SELECT @ret = 
	  STUFF(
		(
		  SELECT ', ' + Name
		  FROM tblCustomer P
		  WHERE ID IN (SELECT CustomerID FROM tblOriginCustomers OP WHERE OP.OriginID = @originID)
		  ORDER BY Name
		  FOR XML PATH(''),TYPE
		  ).value('.','VARCHAR(MAX)'
		), 1, 1, '')

	RETURN (@ret)
END
GO
GRANT EXECUTE ON fnOriginCustomersCSV TO role_iis_acct
GO

/***********************************
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Origin records with translated value and FullName field (which includes Origin Type)
-- Changes:
	- 3.8.11 - 2015/07/28 - KDA - remove Customer info (no longer 1 to 1 relationship with Origin
***********************************/
ALTER VIEW viewOrigin AS
SELECT O.*
	, FullName = CASE WHEN O.H2S = 1 THEN 'H2S-' ELSE '' END + OT.OriginType + ' - ' + O.Name
	, OT.OriginType
	, State = S.FullName 
	, StateAbbrev = S.Abbreviation 
	, S.CountryID
	, Country = CO.Name, CountryShort = CO.Abbrev
	, Operator = OP.Name
	, Pumper = P.FullName
	, TicketType = TT.Name
	, TT.ForTanksOnly
	, Region = R.Name
	, Producer = PR.Name 
	, TimeZone = TZ.Name 
	, UOM = UOM.Name 
	, UomShort = UOM.Abbrev 
	, OT.HasTanks
	, Active = cast(CASE WHEN O.DeleteDateUTC IS NULL THEN 1 ELSE 0 END as bit)
	, TankCount = (SELECT COUNT(*) FROM tblOriginTank WHERE OriginID = O.ID AND DeleteDateUTC IS NULL) 
FROM dbo.tblOrigin O
JOIN dbo.tblOriginType OT ON OT.ID = O.OriginTypeID
LEFT JOIN tblState S ON S.ID = O.StateID
LEFT JOIN tblCountry CO ON CO.ID = S.CountryID
LEFT JOIN dbo.tblOperator OP ON OP.ID = O.OperatorID
LEFT JOIN dbo.viewPumper P ON P.ID = O.PumperID
LEFT JOIN dbo.tblTicketType TT ON TT.ID = O.TicketTypeID
LEFT JOIN dbo.tblRegion R ON R.ID = O.RegionID
LEFT JOIN dbo.tblProducer PR ON PR.ID = O.ProducerID
LEFT JOIN dbo.tblTimeZone TZ ON TZ.ID = O.TimeZoneID
LEFT JOIN dbo.tblUom UOM ON UOM.ID = O.UomID

GO

/**********************************************************
-- Create Date: 2 Nov 2013
-- Author: Kevin Alons
-- Purpose: return all Origin|Destinations for each Customer
-- Changes:
	- 3.8.11 - 2015/07/28 - KDA - remove references to tblOrigin.CustomerID (no longer a 1 to 1 relationship)
**********************************************************/
ALTER VIEW viewCustomerOriginDestination AS
SELECT O.ID AS OriginID, OC.CustomerID, DC.DestinationID
FROM tblOrigin O
JOIN tblOriginCustomers OC ON OC.OriginID = O.ID
JOIN tblDestinationCustomers DC ON DC.CustomerID = OC.CustomerID

GO

/***********************************
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Order records with "translated friendly" values for FK relationships
-- Changes:
	-- 3.7.28 - 2015/06/18 - KDA - remove explicit columns not defined in "viewOrderBase"
***********************************/
ALTER VIEW [dbo].[viewOrder] AS
SELECT O.*
	-- GENERATED orders that were GAUGED to COMPLETION or SKIPPED will show "Gauger [gauger status]"
	, PrintStatus = CASE WHEN O.StatusID = -10 AND GOS.IsComplete = 1 THEN 'Gauger ' + GOS.Name ELSE OPS.OrderStatus END
FROM (
	SELECT O.*
	, Origin = vO.Name 
	, OriginFull = vO.FullName 
	, OriginState = vO.State 
	, OriginStateAbbrev = vO.StateAbbrev 
	, OriginStation = vO.Station
	, OriginCounty = vO.County 
	, vO.LeaseName
	, vO.LeaseNum
	, OriginRegion = vO.Region
	, OriginCountryID = vO.CountryID
	, OriginCountry = vO.Country
	, OriginCountryShort = vO.CountryShort
	, OriginLegalDescription = vO.LegalDescription 
	, OriginNDIC = vO.NDICFileNum 
	, OriginNDM = vO.NDM 
	, OriginCA = vO.CA 
	, OriginTimeZoneID = vO.TimeZoneID 
	, OriginUseDST = vO.UseDST 
	, vO.H2S
	, Destination = vD.Name 
	, DestinationFull = vD.FullName 
	, DestinationState = vD.State 
	, DestinationStateAbbrev = vD.StateAbbrev 
	, DestinationTypeID = vD.ID
	, vD.DestinationType
	, DestStation = vD.Station 
	, DestTimeZoneID = vD.TimeZoneID 
	, DestUseDST = vD.UseDST 
	, DestCountryID = vD.CountryID
	, DestCountry = vD.Country
	, DestCountryShort = vD.CountryShort
	, Customer = C.Name 
	, Carrier = CA.Name 
	, CarrierType = CT.Name 
	, OS.OrderStatus
	, OS.StatusNum
	, Driver = D.FullName 
	, DriverFirst = D.FirstName 
	, DriverLast = D.LastName 
	, Truck = TRU.FullName 
	, Trailer = TR1.FullName 
	, Trailer2 = TR2.FullName 
	, P.PriorityNum
	, TicketType = TT.Name 
	, DestTicketTypeID = vD.TicketTypeID 
	, DestTicketType = vD.TicketType 
	, Operator = OP.Name 
	, Producer = PR.Name 
	, Pumper = PU.FullName 
	, DriverNumber = D.IDNumber
	, CarrierNumber = CA.IDNumber
	, CarrierTypeID = CA.CarrierTypeID
	, TruckNumber = TRU.IDNumber 
	, TrailerNumber = TR1.IDNumber 
	, Trailer2Number = TR2.IDNumber 
	, Product = PRO.Name
	, ProductShort = PRO.ShortName 
	, PRO.ProductGroup
	, OriginUOM = OUom.Name 
	, OriginUomShort = OUom.Abbrev 
	, OriginTankID_Text = CASE WHEN ORT.TankNum = '*' THEN '*' + isnull(O.OriginTankNum, '?') ELSE ORT.TankNum END 
	, DestUOM = DUom.Name 
	, DestUomShort = DUom.Abbrev 
	, Active = cast((CASE WHEN O.DeleteDateUTC IS NULL THEN 1 ELSE 0 END) as bit) 
	, IsDeleted = cast((CASE WHEN O.DeleteDateUTC IS NOT NULL THEN 1 ELSE 0 END) as bit) 
	, PickupPrintStatus = PPS.Name 
	, PickupCompleted = PPS.IsCompleted 
	, DeliverPrintStatus = DPS.Name 
	, DeliverCompleted = DPS.IsCompleted 
	, PrintStatusID = CASE WHEN O.StatusID = -9 THEN ISNULL(-GAO.StatusID, O.StatusID)
						   WHEN O.StatusID = 8 AND PPS.IsCompleted = 0 THEN 7
						   WHEN O.StatusID = 3 AND DPS.IsCompleted = 0 THEN 8
						ELSE O.StatusID END 
	, OriginTankText = CASE WHEN O.OriginTankID IS NULL OR ORT.TankNum = '*' THEN O.OriginTankNum ELSE ORT.TankNum END
	, OriginWaitNum = OWR.Num
	, OriginWaitDesc = OWR.Description
	, OriginWaitNumDesc = OWR.NumDesc
	, DestWaitNum = DWR.Num
	, DestWaitDesc = DWR.Description
	, DestWaitNumDesc = DWR.NumDesc
	, RejectNum = ORR.Num
	, RejectDesc = ORR.Description
	, RejectNumDesc = ORR.NumDesc
	, RerouteCount = (SELECT COUNT(1) FROM tblOrderReroute ORE WHERE ORE.OrderID = O.ID)
	, TicketCount = (SELECT COUNT(1) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND OT.DeleteDateUTC IS NULL)
	, TotalMinutes = isnull(OriginMinutes, 0) + ISNULL(DestMinutes, 0)
	, GaugerStatusID = GAO.StatusID
	, GaugerID = GAO.GaugerID
	FROM dbo.viewOrderBase O
	JOIN dbo.tblOrderStatus AS OS ON OS.ID = O.StatusID
	LEFT JOIN dbo.viewOrigin vO ON vO.ID = O.OriginID
	LEFT JOIN dbo.viewDestination vD ON vD.ID = O.DestinationID
	LEFT JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
	LEFT JOIN dbo.tblCarrier CA ON CA.ID = O.CarrierID
	LEFT JOIN dbo.tblCarrierType CT ON CT.ID = CA.CarrierTypeID
	LEFT JOIN dbo.viewDriverBase D ON D.ID = O.DriverID
	LEFT JOIN dbo.viewTruck TRU ON TRU.ID = O.TruckID
	LEFT JOIN dbo.viewTrailer TR1 ON TR1.ID = O.TrailerID
	LEFT JOIN dbo.viewTrailer TR2 ON TR2.ID = O.Trailer2ID
	LEFT JOIN dbo.tblPriority P ON P.ID = O.PriorityID
	LEFT JOIN dbo.tblTicketType TT ON TT.ID = O.TicketTypeID
	LEFT JOIN dbo.tblOriginTank ORT ON ORT.ID = O.OriginTankID
	LEFT JOIN dbo.tblOperator OP ON OP.ID = O.OperatorID
	LEFT JOIN dbo.viewPumper PU ON PU.ID = O.PumperID
	LEFT JOIN dbo.tblProducer PR ON PR.ID = O.ProducerID
	LEFT JOIN dbo.viewProduct PRO ON PRO.ID = O.ProductID
	LEFT JOIN dbo.tblUom OUom ON OUom.ID = O.OriginUomID
	LEFT JOIN dbo.tblUom DUom ON DUom.ID = O.DestUomID
	LEFT JOIN dbo.tblPrintStatus PPS ON PPS.ID = O.PickupPrintStatusID
	LEFT JOIN dbo.tblPrintStatus DPS ON DPS.ID = O.DeliverPrintStatusID
	LEFT JOIN dbo.viewOriginWaitReason OWR ON OWR.ID = O.OriginWaitReasonID
	LEFT JOIN dbo.viewDestinationWaitReason DWR ON DWR.ID = O.DestWaitReasonID
	LEFT JOIN dbo.viewOrderRejectReason ORR ON ORR.ID = O.RejectReasonID
	LEFT JOIN dbo.tblGaugerOrder GAO ON GAO.OrderID = O.ID
	LEFT JOIN dbo.viewGaugerBase G ON G.ID = GAO.GaugerID
) O
LEFT JOIN dbo.viewOrderPrintStatus OPS ON OPS.ID = O.PrintStatusID
LEFT JOIN dbo.tblGaugerOrderStatus GOS ON GOS.ID = O.GaugerStatusID

GO

/***********************************/
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return the Local Arrive|Depart Times + their respective TimeZone abbreviations
/***********************************/
ALTER VIEW viewOrderLocalDates AS
SELECT O.*
	, dbo.fnUTC_to_Local(O.OriginArriveTimeUTC, O.OriginTimeZoneID, O.OriginUseDST) AS OriginArriveTime
	, dbo.fnUTC_to_Local(O.OriginDepartTimeUTC, O.OriginTimeZoneID, O.OriginUseDST) AS OriginDepartTime
	, dbo.fnUTC_to_Local(O.DestArriveTimeUTC, O.DestTimeZoneID, O.DestUseDST) AS DestArriveTime
	, dbo.fnUTC_to_Local(O.DestDepartTimeUTC, O.DestTimeZoneID, O.DestUseDST) AS DestDepartTime
	, dbo.fnTimeZoneAbbrev(O.OriginArriveTimeUTC, O.OriginTimeZoneID, O.OriginUseDST) AS OriginTimeZone
	, dbo.fnTimeZoneAbbrev(O.DestArriveTimeUTC, O.DestTimeZoneID, O.DestUseDST) AS DestTimeZone
	, DATEDIFF(minute, O.OriginDepartTimeUTC, O.DestArriveTimeUTC) AS TransitMinutes
	FROM viewOrder O

GO

/***********************************
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details + FINANCIAL INFO into a single view
-- Changes:
	- 3.7.27 - 2015/06/18 - KDA - add new columns: ShipperSettlementFactorID, SettlementFactorID, MinSettlementUnitsID
***********************************/
ALTER VIEW viewOrder_Financial_Shipper AS 
	SELECT O.* 
		, Shipper = Customer
		, TicketNums = dbo.fnOrderTicketDetails(O.ID, 'TicketNums', '<br/>') 
		, TankNums = dbo.fnOrderTicketDetails(O.ID, 'TankNums', '<br/>') 
		, PreviousDestinations = dbo.fnRerouteDetails(O.ID, 'PreviousDestinations', '<br/>') 
		, RerouteUsers = dbo.fnRerouteDetails(O.ID, 'RerouteUsers', '<br/>') 
		, RerouteDates = dbo.fnRerouteDetails(O.ID, 'RerouteDates', '<br/>') 
		, RerouteNotes = dbo.fnRerouteDetails(O.ID, 'RerouteNotes', '<br/>') 
		, InvoiceRatesAppliedDate = dbo.fnUTC_To_Local(OS.CreateDateUTC, O.OriginTimeZoneID, O.OriginUseDST)
		, OS.BatchID
		, InvoiceBatchNum = OS.BatchNum 
		, InvoiceOriginWaitBillableMinutes = OS.OriginWaitBillableMinutes
		, InvoiceDestinationWaitBillableMinutes = OS.DestinationWaitBillableMinutes
		, InvoiceTotalWaitBillableMinutes = isnull(OS.OriginWaitBillableMinutes, 0) + ISNULL(OS.DestinationWaitBillableMinutes, 0) 
		, InvoiceWaitFeeParameterID = WaitFeeParameterID
		, InvoiceWaitFeeSubUnit = WaitFeeSubUnit
		, InvoiceWaitFeeRoundingType = WaitFeeRoundingType
		, InvoiceOriginWaitRate = OS.OriginWaitRate 
		, InvoiceOriginWaitAmount = OS.OriginWaitAmount 		
		, InvoiceDestinationWaitRate = OS.DestinationWaitRate 
		, InvoiceDestinationWaitAmount = OS.DestinationWaitAmount 
		, InvoiceTotalWaitAmount = OS.TotalWaitAmount
		, InvoiceOrderRejectRate = OS.OrderRejectRate  -- changed
		, InvoiceOrderRejectRateType = OS.OrderRejectRateType -- changed
		, InvoiceOrderRejectAmount = OS.OrderRejectAmount  -- changed
		, InvoiceChainupRate = OS.ChainupRate
		, InvoiceChainupRateType = OS.ChainupRateType
		, InvoiceChainupAmount = OS.ChainupAmount 
		, InvoiceRerouteRate = OS.RerouteRate
		, InvoiceRerouteRateType = OS.RerouteRateType
		, InvoiceRerouteAmount = OS.RerouteAmount 
		, InvoiceH2SRate = OS.H2SRate
		, InvoiceH2SRateType = OS.H2SRateType
		, InvoiceH2SAmount = OS.H2SAmount
		, InvoiceSplitLoadRate = OS.SplitLoadRate
		, InvoiceSplitLoadRateType = OS.SplitLoadRateType
		, InvoiceSplitLoadAmount = OS.SplitLoadAmount
		, InvoiceOtherAmount = OS.OtherAmount
		, InvoiceTaxRate = OS.OriginTaxRate
		, InvoiceSettlementUom = OS.SettlementUom -- changed
		, InvoiceSettlementUomShort = OS.SettlementUomShort -- changed
		, InvoiceShipperSettlementFactorID = OS.ShipperSettlementFactorID -- 2015/06/18 - KDA - added
		, InvoiceSettlementFactorID = OS.SettlementFactorID -- 2015/06/18 - KDA - added
		, InvoiceSettlementFactor = OS.SettlementFactor -- 2015/06/18 - KDA - added
		, InvoiceMinSettlementUnitsID = OS.MinSettlementUnitsID -- 2015/06/18 - KDA - added
		, InvoiceMinSettlementUnits = OS.MinSettlementUnits
		, InvoiceUnits = OS.SettlementUnits
		, InvoiceRouteRate = OS.RouteRate
		, InvoiceRouteRateType = OS.RouteRateType
		, InvoiceRateSheetRate = OS.RateSheetRate
		, InvoiceRateSheetRateType = OS.RateSheetRateType
		, InvoiceFuelSurchargeRate = OS.FuelSurchargeRate
		, InvoiceFuelSurchargeAmount = OS.FuelSurchargeAmount
		, InvoiceLoadAmount = OS.LoadAmount
		, InvoiceTotalAmount = OS.TotalAmount
	FROM dbo.viewOrderLocalDates O
	LEFT JOIN viewOrderSettlementShipper OS ON OS.OrderID = O.ID
	WHERE O.StatusID NOT IN (-10) -- don't include "Generated" orders (only Assigned+)

GO

EXEC _spRefreshAllViews
GO

/********************************************
-- Author:		Kevin Alons
-- Create date: 13 May 2013
-- Description:	retrieve all currently eligible Destinations for the specified OriginID/ProductID values
-- Changes:
	- 3.8.11 - 2015/07/28 - KDA - revamp to use revamped viewCustomerOriginDestination
								- add new @shipper parameter
*********************************************/
ALTER FUNCTION fnRetrieveEligibleDestinations(@originID int, @shipperID int, @productID int, @requireRoute bit = 0) RETURNS TABLE 
AS RETURN
	SELECT D.*
	FROM viewDestination D
	JOIN viewCustomerOriginDestination COD ON COD.DestinationID = D.ID
	LEFT JOIN tblOriginProducts OP ON OP.OriginID = COD.OriginID
	LEFT JOIN tblDestinationProducts DP ON DP.DestinationID = D.ID AND DP.ProductID = OP.ProductID
	WHERE D.DeleteDateUTC IS NULL
	  AND (isnull(@originID, 0) = 0 OR COD.OriginID = @originID)
	  AND (ISNULL(@shipperID, 0) = 0 OR COD.CustomerID = @shipperID)
	  AND (ISNULL(@productID, 0) = 0 OR OP.ProductID = @productID)
	  AND (@requireRoute = 0 OR D.ID IN (SELECT DestinationID FROM viewRoute WHERE OriginID = @OriginID)) 
GO

/**********************************************************
-- Author: Kevin Alons
-- Date Created: 8 Oct 2014
-- Purpose: return the Audit table records (optionally filtered to just the specified driver and/or ordernum)
-- Changes:
	- 3.7.28 - 2015/06/18 - KDA - add filter to ensure APPROVED orders are not displayed
	- 3.7.44 - 2015/07/06 - BB - Added error catching rule and message for zero volume loads not marked rejected.
	- 3.8.11 - 2015/07/28 - KDA - revise error mesages for excessive load volumes to honor new associated OrderRule
***********************************************************/
ALTER FUNCTION fnOrders_AllTickets_Audit(@carrierID int, @driverID int, @orderNum int, @id int) RETURNS TABLE AS RETURN
SELECT *
	, OriginDistanceText = CASE WHEN OriginGpsArrived = 1 THEN 'Verified' ELSE isnull(ltrim(OriginDistance), 'N/A') END
	, DestDistanceText = CASE WHEN DestGpsArrived = 1 THEN 'Verified' ELSE isnull(ltrim(DestDistance), 'N/A') END
	, HasError = cast(CASE WHEN Errors IS NULL THEN 0 ELSE 1 END as bit)
FROM (
	SELECT O.* 
		, OriginGpsLatLon = ltrim(DLO.Lat) + ',' + ltrim(DLO.Lon)
		, OriginLatLon = ltrim(OO.LAT) + ',' + ltrim(OO.LON)
		, OriginAccuracyMeters = DLO.SourceAccuracyMeters
		, OriginDistance = CASE WHEN DLO.DistanceToPoint < 0 THEN NULL ELSE DLO.DistanceToPoint END
		, OriginGpsArrived = CASE WHEN DLO.DistanceToPoint IS NOT NULL AND DLO.DistanceToPoint BETWEEN 0 AND OO.GeoFenceRadiusMeters THEN 1 ELSE 0 END
		, DestGpsLatLon = ltrim(DLD.Lat) + ',' + ltrim(DLD.Lon)
		, DestLatLon = ltrim(D.LAT) + ',' + ltrim(D.LON)
		, DestAccuracyMeters = DLD.SourceAccuracyMeters
		, DestDistance = CASE WHEN DLD.DistanceToPoint < 0 THEN NULL ELSE DLD.DistanceToPoint END
		, DestGpsArrived = CASE WHEN DLD.DistanceToPoint IS NOT NULL AND DLD.DistanceToPoint BETWEEN 0 AND D.GeoFenceRadiusMeters THEN 1 ELSE 0 END
		, OriginGrossBBLS = dbo.fnConvertUOM(O.OriginGrossUnits, O.OriginUomID, 1)
		, Errors = 
			nullif(
				SUBSTRING(
					CASE WHEN O.OriginArriveTimeUTC IS NULL THEN ',Missing Pickup Arrival' ELSE '' END
				  + CASE WHEN O.OriginDepartTimeUTC IS NULL THEN ',Missing Pickup Departure' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND O.Tickets IS NULL THEN ',No Active Tickets' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND O.DestArriveTimeUTC IS NULL THEN ',Missing Delivery Arrival' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND O.DestDepartTimeUTC IS NULL THEN ',Missing Delivery Departure' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND O.OriginGrossUnits = 0 AND O.OriginNetUnits = 0 THEN ',No Origin Units are entered. '+CHAR(13)+CHAR(10)+' This may be an unmarked rejected load.' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND dbo.fnConvertUOM(O.OriginGrossUnits, O.OriginUomID, 2) > isnull(OORMG.MaxGallons, 320) THEN ',Origin GOV Units out of limits' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND dbo.fnConvertUOM(O.OriginGrossStdUnits, O.OriginUomID, 2) > isnull(OORMG.MaxGallons, 320) THEN ',Origin GSV Units out of limits' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND dbo.fnConvertUOM(O.OriginNetUnits, O.OriginUomID, 2) > isnull(OORMG.MaxGallons, 320) THEN ',Origin NSV Units out of limits' ELSE '' END				  
				  + CASE WHEN dbo.fnToBool(OOR.Value) = 1 AND (SELECT count(*) FROM tblOrderTicket OT WHERE OrderID = O.ID AND OT.DeleteDateUTC IS NULL AND OT.DispatchConfirmNum IS NULL) > 0 THEN ',Missing Ticket Shipper PO' ELSE '' END
				  + CASE WHEN EXISTS(SELECT CustomerID FROM viewOrder_OrderTicket_Full WHERE ID = O.ID AND DeleteDateUTC IS NULL AND nullif(rtrim(T_DispatchConfirmNum), '') IS NOT NULL GROUP BY CustomerID, T_DispatchConfirmNum HAVING COUNT(*) > 1) THEN ',Duplicate Shipper PO' ELSE '' END
				  + CASE WHEN O.TruckID IS NULL THEN ',Truck Missing' ELSE '' END
				  + CASE WHEN O.TrailerID IS NULL THEN ',Trailer 1 Missing' ELSE '' END
				, 2, 8000) 
			, '')
		, IsEditable = cast(CASE WHEN O.StatusID = 3 THEN 1 ELSE 0 END as bit)
	FROM viewOrder_AllTickets O
	JOIN tblOrigin OO ON OO.ID = O.OriginID
	JOIN tblCustomer C ON C.ID = O.CustomerID
	JOIN tblDestination D ON D.ID = O.DestinationID
	-- max Gallons for this order
	OUTER APPLY (SELECT MaxGallons = cast(Value as decimal(18, 2)) FROM dbo.fnOrderOrderRules(O.ID) WHERE TypeID = 6) OORMG 
	-- ShipperPO_Required OrderRule
	OUTER APPLY (SELECT Value FROM dbo.fnOrderOrderRules(O.ID) WHERE TypeID = 2) OOR
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
	LEFT JOIN tblOrderSettlementShipper OIC ON OIC.OrderID = O.ID
	LEFT JOIN viewDriverLocation_OriginFirstArrive DLO ON DLO.OrderID = O.ID AND DLO.OriginID = O.OriginID
	LEFT JOIN viewDriverLocation_DestinationFirstArrive DLD ON DLD.OrderID = O.ID AND DLD.DestinationID = O.DestinationID
	WHERE OIC.BatchID IS NULL /* don't even show SETTLED orders on the AUDIT page ever */
	  AND nullif(OA.Approved, 0) IS NULL /* don't show Approved orders on the AUDIT page */
	  AND (isnull(@carrierID, 0) = -1 OR O.CarrierID = @carrierID)
	  AND (isnull(@driverID, 0) = -1 OR O.DriverID = @driverID)
	  AND ((nullif(@orderNum, 0) IS NULL AND O.DeleteDateUTC IS NULL AND (O.StatusID = 3 AND DeliverPrintStatusID IN (SELECT ID FROM tblPrintStatus WHERE IsCompleted = 1))) OR O.OrderNum = @orderNum)
	  AND (nullif(@id, 0) IS NULL OR O.ID LIKE @id)
) X
GO

/***********************************
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
-- Changes
	- 3.8.11 - 2015/07/28 - KDA - remove OriginShipperRegion field
***********************************/
ALTER PROCEDURE spRetrieveOrdersFinancialShipper
(
  @StartDate datetime = NULL
, @EndDate datetime = NULL
, @ShipperID int = -1 -- all customers
, @ProductGroupID int = -1 -- all product groups
, @ProducerID int = -1 -- all
, @OriginStateID int = -1 -- all 
, @DestStateID int = -1 -- all
, @BatchID int = NULL  -- either show unbatched (NULL), or the specified batch orders
) AS BEGIN

	SELECT @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	
	SELECT DISTINCT OE.* 
		, InvoiceOtherDetailsTSV = dbo.fnOrderShipperAssessorialDetailsTSV(OE.ID, 0)
		, InvoiceOtherAmountsTSV = dbo.fnOrderShipperAssessorialAmountsTSV(OE.ID, 0)
	FROM viewOrder_Financial_Shipper OE
	WHERE ID IN (
		SELECT O.ID
		FROM viewOrderBase O
		JOIN tblProduct P ON P.ID = O.ProductID
		JOIN tblOrigin OO ON OO.ID = O.OriginID
		JOIN tblDestination D ON D.ID = O.DestinationID
		LEFT JOIN tblOrderSettlementShipper ISC ON ISC.OrderID = O.ID
		WHERE O.StatusID IN (4)  
		  AND (@ShipperID=-1 OR O.CustomerID=@ShipperID) 
		  AND (@ProductGroupID=-1 OR P.ProductGroupID=@ProductGroupID) 
		  AND (@ProducerID=-1 OR O.ProducerID=@ProducerID) 
		  AND (@OriginStateID=-1 OR OO.StateID=@OriginStateID) 
		  AND (@DestStateID=-1 OR D.StateID=@DestStateID) 
		  AND (@StartDate IS NULL OR O.OrderDate >= @StartDate) 
		  AND (@EndDate IS NULL OR O.OrderDate <= @EndDate)
		  AND ((@BatchID IS NULL AND ISC.BatchID IS NULL) OR ISC.BatchID = @BatchID)
	)
	ORDER BY OE.OriginDepartTimeUTC
END 
GO

/***********************************
-- Date Created: 18 Apr 2015
-- Author: Kevin Alons
-- Purpose: create new Gauger Order (loads) for the specified criteria
-- Changes:
	- 3.8.11 - 2015/07/28 - KDA - add explicit @ShipperID parameter (Customer no longer an field of tblOrigin
***********************************/
ALTER PROCEDURE spCreateGaugerLoads
(
  @OriginID int
, @ShipperID int
, @DestinationID int
, @GaugerTicketTypeID int
, @DueDate datetime
, @OriginTankID int
, @ProductID int
, @UserName varchar(100)
, @GaugerID int = NULL
, @Qty int = 1
, @PriorityID int = 3 -- LOW priority
, @DispatchConfirmNum varchar(30) = NULL
, @IDs_CSV varchar(max) = NULL OUTPUT
, @count int = 0 OUTPUT
) AS
BEGIN
	DECLARE @TicketTypeID int
	SELECT @TicketTypeID = TicketTypeID FROM tblGaugerTicketType WHERE ID = @GaugerTicketTypeID
	EXEC spCreateLoads @StatusID=-9, @OriginID=@OriginID, @OriginTankID=@OriginTankID, @ProductID=@ProductID, @DestinationID=@DestinationID
		, @CustomerID=@ShipperID, @PriorityID=@PriorityID, @TicketTypeID=@TicketTypeID, @DispatchConfirmNum=@DispatchConfirmNum
		, @DueDate=@DueDate, @Qty=@Qty, @UserName=@UserName
		, @IDs_CSV=@IDs_CSV OUTPUT
		, @count=@count OUTPUT
	
	INSERT INTO tblGaugerOrder (OrderID, TicketTypeID, StatusID, OriginTankID, OriginTankNum, GaugerID
		, Rejected, Handwritten, DueDate, PriorityID, CreateDateUTC, CreatedByUser)
		SELECT ID, @GaugerTicketTypeID, CASE WHEN @GaugerID IS NULL THEN 1 ELSE 2 END, @OriginTankID, NULL, @GaugerID
			, 0, 0, @DueDate, @PriorityID, getutcdate(), @UserName
		FROM tblOrder 
		WHERE ID IN (SELECT ID FROM dbo.fnSplitCSVIDs(@IDs_CSV))
END

GO

/*******************************************
-- Date Created: 13 Apr 2015
-- Author: Kevin Alons
-- Purpose: return readonly Order data for Gauger App sync
-- Changes:
	- 3.8.11 - 2015/07/28 - KDA - remove use of tblOrigin.CustomerID field (no longer 1 to 1 relationship)
*******************************************/
ALTER FUNCTION fnOrderReadOnly_GaugerApp(@GaugerID int, @LastChangeDateUTC datetime) RETURNS TABLE AS
RETURN 
	WITH cteBase AS
	(
		SELECT O.ID
			, O.OrderNum
			, GAO.StatusID
			, GAO.TicketTypeID
			, GAO.GaugerID
			, PriorityNum = cast(P.PriorityNum as int) 
			, Product = PRO.Name
			, GAO.DueDate
			, Origin = OO.Name
			, OriginFull = OO.FullName
			, OO.OriginType
			, O.OriginUomID
			, OriginStation = OO.Station 
			, OriginLeaseNum = OO.LeaseNum 
			, OriginCounty = OO.County 
			, OriginLegalDescription = OO.LegalDescription 
			, OriginNDIC = OO.NDICFileNum
			, OriginNDM = OO.NDM
			, OriginCA = OO.CA
			, OriginState = OO.State
			, OriginAPI = OO.WellAPI
			, OriginLat = OO.LAT 
			, OriginLon = OO.LON 
			, OriginGeoFenceRadiusMeters = OO.GeoFenceRadiusMeters 
			, O.CreateDateUTC
			, O.CreatedByUser
			, O.LastChangeDateUTC
			, O.LastChangedByUser
			, DeleteDateUTC = isnull(GOVD.VirtualDeleteDateUTC, O.DeleteDateUTC) 
			, DeletedByUser = isnull(GOVD.VirtualDeletedByUser, O.DeletedByUser) 
			, O.OriginID
			, PriorityID = cast(O.PriorityID AS int) 
			, OO.Operator
			, O.OperatorID
			, OO.Pumper
			, O.PumperID
			, OO.Producer
			, O.ProducerID
			, Customer = S.Name
			, O.CustomerID
			, O.ProductID
			, TicketType = GTT.Name
			, EmergencyInfo = isnull(S.EmergencyInfo, 'For an emergency (spill, leak, fire, or accident) contact CHEMTREC @ 800.424.9300 (toll free)') 
			, O.OriginTankID
			, O.OriginTankNum
			, GAO.DispatchNotes
			, O.CarrierTicketNum
			, O.DispatchConfirmNum
			, OriginTimeZone = OO.TimeZone
			, OCTM.OriginThresholdMinutes
			, ShipperHelpDeskPhone = S.HelpDeskPhone
			, OriginDrivingDirections = OO.DrivingDirections
			, LCD.LCD
			, CustomerLastChangeDateUTC = S.LastChangeDateUTC 
			, OriginLastChangeDateUTC = OO.LastChangeDateUTC 
			, DestLastChangeDateUTC = D.LastChangeDateUTC
			, RouteLastChangeDateUTC = R.LastChangeDateUTC
		FROM dbo.tblOrder O
		JOIN dbo.tblGaugerOrder GAO ON GAO.OrderID = O.ID
		JOIN tblGaugerTicketType GTT ON GTT.ID = GAO.TicketTypeID
		JOIN dbo.tblPriority P ON P.ID = GAO.PriorityID
		JOIN dbo.viewOrigin OO ON OO.ID = O.OriginID
		JOIN dbo.viewDestination D ON D.ID = O.DestinationID
		JOIN dbo.tblCustomer S ON S.ID = O.CustomerID
		JOIN dbo.tblRoute R ON R.ID = O.RouteID
		JOIN tblProduct PRO ON PRO.ID = O.ProductID
		LEFT JOIN tblGaugerOrderVirtualDelete GOVD ON GOVD.OrderID = O.ID AND GOVD.GaugerID = GAO.GaugerID
		CROSS JOIN fnSyncLCDOffset(@LastChangeDateUTC) LCD
		OUTER APPLY dbo.fnOrderCombinedThresholdMinutes(O.ID) OCTM
		WHERE GAO.StatusID NOT IN (1) -- don't include REQUESTED Gauger Order records
		  AND O.ID IN (
			SELECT OrderID FROM tblGaugerOrder WHERE GaugerID = @GaugerID
			UNION 
			SELECT OrderID FROM tblGaugerOrderVirtualDelete WHERE GaugerID = @GaugerID
		)
	)
	SELECT O.* 
		, HeaderImageID = GAHI.ID
		, PrintHeaderBlob = GAHI.ImageBlob
		, HeaderImageLeft = GAHI.ImageLeft
		, HeaderImageTop = GAHI.ImageTop
		, HeaderImageWidth = GAHI.ImageWidth
		, HeaderImageHeight = GAHI.ImageHeight
		, PickupTemplateID = GAPT.ID
		, PickupTemplateText = GAPT.TemplateText
		, TicketTemplateID = GATT.ID
		, TicketTemplateText = GATT.TemplateText
	FROM cteBase O
	OUTER APPLY dbo.fnOrderBestMatchGaugerAppPrintHeaderImage(O.ID) GAHI
	LEFT JOIN tblGaugerAppPrintHeaderImageSync GAHIS ON GAHIS.OrderID = O.ID AND GAHIS.GaugerID = @GaugerID AND GAHIS.RecordID <> GAHI.ID
	OUTER APPLY dbo.fnOrderBestMatchGaugerAppPrintPickupTemplate(O.ID) GAPT
	LEFT JOIN tblGaugerAppPrintPickupTemplateSync GAPTS ON GAPTS.OrderID = O.ID AND GAPTS.GaugerID = @GaugerID AND GAPTS.RecordID <> GAPT.ID
	OUTER APPLY dbo.fnOrderBestMatchGaugerAppPrintTicketTemplate(O.ID) GATT
	LEFT JOIN tblGaugerAppPrintTicketTemplateSync GATTS ON GATTS.OrderID = O.ID AND GATTS.GaugerID = @GaugerID AND GATTS.RecordID <> GATT.ID
	WHERE (@LastChangeDateUTC IS NULL
		OR CreateDateUTC >= LCD
		OR O.LastChangeDateUTC >= LCD
		OR O.LastChangeDateUTC >= LCD
		-- if any print related record was changed or a different template/image is now valid
		OR GAHI.LastChangeDateUTC >= LCD
		OR GAHIS.RecordID IS NOT NULL
		OR GAPT.LastChangeDateUTC >= LCD
		OR GAPTS.RecordID IS NOT NULL
		OR GATT.LastChangeDateUTC >= LCD
		OR GATTS.RecordID IS NOT NULL
	)
GO

EXEC _spRebuildAllObjects
GO

COMMIT
SET NOEXEC OFF