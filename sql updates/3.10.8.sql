SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.10.7'
SELECT  @NewVersion = '3.10.8'

-- only ensure the curr version matches when we are not testing with an "x.x.x" DEV version
IF @NewVersion <> 'x.x.x' AND (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

-- only update the DB VERSION when not testing with an "x.x.x" DEV version
IF @NewVersion <> 'x.x.x' 
BEGIN
	UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

	INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
		SELECT @NewVersion, 0, 'ensure  indices are present in PRODUCTION and add fnPointFromLatPlusLon()'
		EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
END
GO

EXEC _spDropIndex 'idxOrderDriverAppVirtualDelete_DriverID'
GO
CREATE NONCLUSTERED INDEX idxOrderDriverAppVirtualDelete_DriverID ON tblOrderDriverAppVirtualDelete
(
	[DriverID] ASC
)
GO

EXEC _spDropIndex udxOrderExportFinalCustomer
GO
CREATE UNIQUE NONCLUSTERED INDEX udxOrderExportFinalCustomer ON tblOrderExportFinalCustomer
(
	OrderID, OrderLastChangeDateUTC
)
GO

EXEC _spDropFunction 'fnPointFromLatPlusLon'
GO
/***********************************
 Date Created: 2015/11/18
 Author: Kevin Alons
 Purpose: compute the geography POINT for the specified LAT+LON combined coordinate string (by splitting the coordinates into lat/lon then processing)
 Changes: 
***********************************/
CREATE FUNCTION fnPointFromLatPlusLon(@LatLon varchar(50)) RETURNS geography AS
BEGIN
	DECLARE @delim tinyint, @lat varchar(25), @lon varchar(25)
	SELECT @delim = CHARINDEX(',', @LatLon), @lat = substring(@LatLon, 1, @delim - 1), @lon = substring(@LatLon, @delim + 1, 1000)
	
	RETURN (dbo.fnPointFromLatLon(@lat, @lon))
END

GO
GRANT EXECUTE ON fnPointFromLatPlusLon TO role_iis_acct
GO

COMMIT
SET NOEXEC OFF