SET NOEXEC OFF  -- since this is 
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.11.3.1'
SELECT  @NewVersion = '3.11.4'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'add db objects & logic to support Report Email Subscription module'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/*********************************************************
Create Info: 2016/02/14 - 3.11.4
Author: Ben Bloodworth/KDA
Description: This table stores report center email subscriptions
		Each subscription represents an the automated generation and emailing of a single ReportCenter report to the specified email addresses.  (DCWEB-940)
Notes:  Default days to export to TRUE (1)
Changes:
*********************************************************/
CREATE TABLE tblUserReportEmailSubscription
(
	ID INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_UserReportEmailSubscription PRIMARY KEY
	, Name varchar(100) NOT NULL  /* this field will be used to generate the output file name */
	, UserName VARCHAR(100) NOT NULL
	, UserReportID INT NOT NULL CONSTRAINT FK_UserReportEmailSubscription_UserReportID FOREIGN KEY (UserReportID) REFERENCES tblUserReportDefinition(ID)
	, Sun BIT NOT NULL CONSTRAINT DF_UserReportEmailSubscription_Sun DEFAULT (1) 
	, Mon BIT NOT NULL CONSTRAINT DF_UserReportEmailSubscription_Mon DEFAULT (1)
	, Tue BIT NOT NULL CONSTRAINT DF_UserReportEmailSubscription_Tue DEFAULT (1)
	, Wed BIT NOT NULL CONSTRAINT DF_UserReportEmailSubscription_Wed DEFAULT (1)
	, Thu BIT NOT NULL CONSTRAINT DF_UserReportEmailSubscription_Thu DEFAULT (1)
	, Fri BIT NOT NULL CONSTRAINT DF_UserReportEmailSubscription_Fri DEFAULT (1)
	, Sat BIT NOT NULL CONSTRAINT DF_UserReportEmailSubscription_Sat DEFAULT (1)
	, ExecuteHourUTC TINYINT NOT NULL CONSTRAINT CK_UserReportEmailSubscription_ExecuteHourUTC CHECK (ExecuteHourUTC BETWEEN 0 AND 23)
	, EmailAddressCSV VARCHAR(MAX) NOT NULL
	, CreateDateUTC datetime NOT NULL CONSTRAINT DF_UserReportEmailSubscription_CreateDateUTC DEFAULT (getutcdate())
	, CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_UserReportEmailSubscription_CreatedByUser DEFAULT ('System')
	, LastChangeDateUTC datetime NULL 
	, LastChangedByUser varchar(100) NULL 
)
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblUserReportEmailSubscription TO role_iis_acct
GO
CREATE UNIQUE INDEX udxUserReportEmailSubscription_Name_UserName ON tblUserReportEmailSubscription(Name, UserName)
GO

COMMIT
SET NOEXEC OFF