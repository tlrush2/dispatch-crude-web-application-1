-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.7.9.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.7.9.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.7.9'
SELECT  @NewVersion = '3.7.10'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Allocations: rename/rewrite of fnRetrieveAllocationDestionShipper_ForRange to fnAllocationDestinationShipper_Daily'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

_spDropFunction 'fnDaysInDateRange'
GO

/*****************************************
-- Date Created: 17 May 2015
-- Author: Kevin Alons
-- Purpose: return a table of all day dates between a date range
-- Changes:
*****************************************/
CREATE FUNCTION fnDaysInDateRange(@start date, @end date) RETURNS @ret TABLE (DayDate date) AS
BEGIN
	WHILE (@start < @end) BEGIN
		INSERT INTO @ret (DayDate)
			values(@start)
		SET @start = DATEADD(day, 1, @start)
	END
	RETURN
END
GO
GRANT SELECT ON fnDaysInDateRange TO dispatchcrude_iis_acct
GO

/*****************************************
-- Date Created: 19 Jan 2015
-- Author: Kevin Alons
-- Purpose: return the tblOrder table witha Local OrderDate field added
-- Changes:
	-- 5/17/2015 - KDA - add local DeliverDate field
*****************************************/
ALTER VIEW viewOrderBase AS
	SELECT O.*
		, OriginShipperRegion = OO.ShipperRegion
		, OrderDate = cast(dbo.fnUTC_to_Local(O.OriginDepartTimeUTC, OO.TimeZoneID, OO.UseDST) as date) 
		, DeliverDate = cast(dbo.fnUTC_to_Local(O.DestDepartTimeUTC, D.TimeZoneID, D.UseDST) as date) 
	FROM tblOrder O
	JOIN tblOrigin OO ON OO.ID = O.OriginID
	JOIN tblDestination D ON D.ID = O.DestinationID

GO

EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRebuildAllObjects
GO

EXEC _spDropFunction 'fnRetrieveAllocationDestinationShipper'
GO

/********************************************
-- Date Created: 25 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve all matching AllocationDestinationShipper records for the specified criteria
********************************************/
CREATE FUNCTION fnAllocationDestinationShipper
(
  @DestinationID int = -1
, @ShipperID int = -1
, @ProductGroupID int = -1
, @StartDate date
, @EndDate date
) RETURNS TABLE AS RETURN
	SELECT *
	FROM viewAllocationDestinationShipper
	WHERE @DestinationID IN (-1, DestinationID) 
	  AND @ShipperID IN (-1, ShipperID) 
	  AND @ProductGroupID IN (-1, ProductGroupID) 
	  AND (@StartDate BETWEEN EffectiveDate AND EndDate
		OR @EndDate BETWEEN EffectiveDate AND EndDate
		OR EffectiveDate BETWEEN @StartDate AND @EndDate)
GO
GRANT SELECT ON fnAllocationDestinationShipper TO dispatchcrude_iis_acct
GO

EXEC _spDropFunction 'fnAllocationDestinationShipper_Daily'
GO

/********************************************
-- Date Created: 25 Jan 2015
-- Author: Kevin Alons
-- Purpose: retrieve the DAILY Allocation + Production Units for the specified selection criteria + Date Range
--			done as a function (vs a PROCEDURE) to allow the results to be easily Grouped as needed
********************************************/
CREATE FUNCTION fnAllocationDestinationShipper_Daily
( 
  @DestinationID int = -1
, @ShipperID int = -1
, @ProductGroupID int = -1
, @StartDate date
, @EndDate date
, @UomID int
) RETURNS @ret TABLE 
(
  DestinationID int
, Destination varchar(100)
, ShipperID int
, Shipper varchar(100)
, ProductGroupID int
, ProductGroup varchar(100)
, UomID int
, Uom varchar(100)
, DayDate date
, AllocatedUnits money
, ProductionUnits money
) AS BEGIN
	-- retrieve the relevant defined Allocation records (by DayDate), normalized to BBLS UOM
	INSERT INTO @ret (DestinationID, ShipperID, ProductGroupID, UomID, DayDate, AllocatedUnits)
	SELECT ret.DestinationID, ret.ShipperID, ret.ProductGroupID, @UomID, DD.DayDate, Units = sum(dbo.fnConvertUOM(DailyUnits, UomID, @UomID))
	FROM viewAllocationDestinationShipper ret
	JOIN fnDaysInDateRange(@StartDate, @EndDate) DD ON DD.DayDate BETWEEN ret.EffectiveDate AND ret.EndDate
	WHERE (@DestinationID = -1 OR ret.DestinationID = @DestinationID)
	  AND (@ShipperID = -1 OR ret.ShipperID = @ShipperID)
	  AND (@ProductGroupID = -1 OR ret.ProductGroupID = @ProductGroupID)
	GROUP BY ret.DestinationID, ret.ShipperID, ret.ProductGroupID, DD.DayDate

	-- retrieve the relevant, eligible OriginNetUnit totals (normalized to BBLS UOM)
	DECLARE @PDS TABLE (DID int, SID int, PGID int, DayDate date, ProdUnits decimal(18, 4))
	INSERT INTO @PDS
	SELECT O.DestinationID, O.CustomerID, P.ProductGroupID, O.DeliverDate, Units = sum(dbo.fnConvertUOM(O.OriginNetUnits, OriginUomID, @UomID))
	FROM viewOrderBase O
	JOIN tblProduct P ON P.ID = O.ProductID
	WHERE DeliverDate BETWEEN @StartDate AND @EndDate
	  AND DeleteDateUTC IS NULL
	  AND Rejected = 0
	  AND StatusID IN (3, 4)
	  AND (@DestinationID = -1 OR O.DestinationID = @DestinationID)
	  AND (@ShipperID = -1 OR O.CustomerID = @ShipperID)
	  AND (@ProductGroupID = -1 OR P.ProductGroupID = @ProductGroupID)
	GROUP BY O.DestinationID, O.CustomerID, P.ProductGroupID, O.DeliverDate

	-- add the PRODUCTION units to the already existing ALLOCATION records
	UPDATE @ret
	  SET ProductionUnits = PDS.ProdUnits
	FROM @ret ret
	JOIN @PDS PDS ON PDS.DID = ret.DestinationID AND PDS.SID = ret.ShipperID AND PDS.PGID = ret.ProductGroupID AND PDS.DayDate = ret.DayDate

	-- add the PRODUCTION units where there was no ALLOCATION record to update
	INSERT INTO @ret (DestinationID, ShipperID, ProductGroupID, UomID, DayDate, ProductionUnits)
	SELECT PDS.DID, PDS.SID, PDS.PGID, @UomID, PDS.DayDate, PDS.ProdUnits
	FROM @PDS PDS
	LEFT JOIN @ret ret ON ret.DestinationID = PDS.DID AND ret.ShipperID = PDS.SID AND ret.ProductGroupID = PDS.PGID AND ret.DayDate = PDS.DayDate
	WHERE ret.DayDate IS NULL

	UPDATE @ret
		SET Destination = D.Name
			, Shipper = C.Name
			, ProductGroup = PG.Name
			, Uom = U.Name
	FROM @ret X
	JOIN tblDestination D ON D.ID = X.DestinationID
	JOIN tblCustomer C ON C.ID = X.ShipperID
	JOIN tblProductGroup PG ON PG.ID = X.ProductGroupID
	JOIN tblUom U ON U.ID = X.UomID

	RETURN
END
GO
GRANT SELECT ON fnAllocationDestinationShipper_Daily TO dispatchcrude_iis_acct
GO

COMMIT
SET NOEXEC OFF
