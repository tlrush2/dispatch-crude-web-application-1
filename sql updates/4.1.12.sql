SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.1.11'
SELECT  @NewVersion = '4.1.12'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1804 - Add ticket types page.  Add more columns to ticket types table.'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


-- Add new columns to tblTicketType
ALTER TABLE tblTicketType
	ADD DeleteDateUTC SMALLDATETIME NULL
		, DeletedByUser VARCHAR(100) NULL
		, InternalName VARCHAR(25) NULL
		, Description VARCHAR(256) NULL
GO


/* Insert original ticket names into internal name column and add Maverick's initial descriptions for each ticket type */
UPDATE tblTicketType 
SET InternalName = 'Gauge Run' 
, Description = 'Standard Oil Field Gauge Ticket'
WHERE ID = 1
GO

UPDATE tblTicketType 
SET InternalName = 'Net Volume' 
, Description = 'Oilfield for Frack or Production Tank where Truck Meter is used'
WHERE ID = 2
GO

UPDATE tblTicketType 
SET InternalName = 'Meter Run' 
, Description = 'LACT unit at Oil Well with metered output rather than gauged tank'
WHERE ID = 3
GO

UPDATE tblTicketType 
SET InternalName = 'Basic Run' 
, Description = 'This ticket simply records enter Gross & net Volume from a BOL'
WHERE ID = 4
GO

UPDATE tblTicketType 
SET InternalName = 'Gross Volume' 
, Description = 'For Propane hauls only. this ticket supports the entry fields required for Propane.'
WHERE ID = 5
GO

UPDATE tblTicketType 
SET InternalName = 'CAN Meter Run' 
, Description = 'Canadian Meter Run'
WHERE ID = 6
GO

UPDATE tblTicketType 
SET InternalName = 'Gauge Net' 
, Description = 'Net Volume with Production Tank Gauges Recorded, but not used to calculate the volume'
WHERE ID = 7
GO

UPDATE tblTicketType 
SET InternalName = 'CAN Basic Run' 
, Description = 'Canadian Basic Run'
WHERE ID = 8
GO

UPDATE tblTicketType 
SET InternalName = 'Mineral Run' 
, Description = 'Gross, Tare, and Net Weight Scale Ticket'
WHERE ID = 9
GO

UPDATE tblTicketType 
SET InternalName = 'Propane with Scale' 
, Description = 'This is a Propane Ticket combined with a Scale Ticket'
WHERE ID = 10
GO

UPDATE tblTicketType 
SET InternalName = 'Production Water Run' 
, Description = 'For hauling Production water away from Oil field tanks with Gauges.'
WHERE ID = 11
GO


/* Reset all created by users to System */
UPDATE tblTicketType SET CreatedByUser = 'System'
GO


/* Set InternalName to a required field now that it's been filled (above) */
ALTER TABLE tblTicketType ALTER COLUMN InternalName VARCHAR(25) NOT NULL
GO


-- Add new permissions for ticket types page
EXEC spAddNewPermission 'viewTicketTypes', 'Allow user to view the ZPL ticket types page', 'Print Configuration', 'Ticket Types - View'
GO
EXEC spAddNewPermission 'editTicketTypes', 'Allow user to edit ZPL ticket types', 'Print Configuration', 'Ticket Types - Edit'
GO
EXEC spAddNewPermission 'deactivateTicketTypes', 'Allow user to deactivate ZPL ticket types', 'Print Configuration', 'Ticket Types - Deactivate'
GO


/* Ensure all functions/views/sp's are working correctly after the above changes */
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRebuildAllObjects
GO
EXEC _spRebuildAllObjects
GO
EXEC _spRecompileAllStoredProcedures
GO


COMMIT
SET NOEXEC OFF