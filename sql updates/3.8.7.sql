-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.8.6.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.8.6.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0

DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.8.6'
SELECT  @NewVersion = '3.8.7'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'Split Carrier documents to a separate set of tables/pages'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

create table tblCarrierDocumentType
(
  ID int not null identity(1, 1) constraint PK_CarrierDocumentType primary key
, Name varchar(100) not null
, CreateDateUTC datetime not null constraint DF_CarrierDocumentType_CreateDateUTC default (getutcdate())
, CreatedByUser varchar(100) not null constraint DF_CarrierDocumentType_CreatedByUser default (SUSER_SNAME())
, LastChangeDateUTC datetime null
, LastChangedByUser varchar(100) null
, DeleteDateUTC datetime null
, DeletedByUser varchar(100) null
)
go
create unique index udxCarrierDocumentType_Name ON tblCarrierDocumentType(Name)
GO
grant select, insert, update, delete on tblCarrierDocumentType TO role_iis_acct
GO

create table tblCarrierDocument
(
  ID int not null identity(1, 1) constraint PK_CarrierDocument primary key
, DocumentTypeID int not null constraint FK_CarrierDocument_DocumentType foreign key references tblCarrierDocumentType(ID)
, CarrierID int not null constraint FK_CarrierDocument_Carrier foreign key references tblCarrier(ID)
, DocName varchar(255) null
, Document varbinary(max) null
, DocumentDate date not null constraint DF_CarrierDocument_DocumentDate default (getdate())
, ExpirationDate datetime null
, Notes text null
, CreateDateUTC datetime not null constraint DF_CarrierDocument_CreateDateUTC default (getutcdate())
, CreatedByUser varchar(100) not null constraint DF_CarrierDocument_CreatedByUser default (SUSER_SNAME())
, LastChangeDateUTC datetime null
, LastChangedByUser varchar(100) null
, DeleteDateUTC datetime null
, DeletedByUser varchar(100) null
)
go
grant select, insert, update, delete on tblCarrierDocument TO role_iis_acct
GO

/******************************************
-- Date Created: 2015/07/22
-- Author: Kevin Alons
-- Purpose: return CarrierDocument records with translated "friendly" values
-- Changes:
******************************************/
CREATE VIEW viewCarrierDocument AS
	SELECT CD.*
		, Carrier = C.Name
		, DocumentType = DT.Name
	FROM tblCarrierDocument CD
	JOIN tblCarrier C ON C.ID = CD.CarrierID
	JOIN tblCarrierDocumentType DT ON DT.ID = CD.DocumentTypeID
GO
GRANT SELECT ON viewCarrierDocument TO role_iis_acct
GO
set identity_insert tblcarrierdocumenttype on
go

insert into tblCarrierDocumentType (ID, Name, CreatedByUser)
	select 1, 'Hazmat Certificate', 'System'
	union
	select 2, 'Liability Insurance', 'System'
	union
	select 3, 'W9', 'System'
	union
	select 4, 'Carrier Agreement', 'System'
	union
	select 5, 'Workers Comp', 'System'
	union
	select 6, 'ACH Instructions', 'System'
go

set identity_insert tblcarrierdocumenttype off
go

insert into tblCarrierDocument (DocumentTypeID, CarrierID, DocName, Document, DocumentDate, ExpirationDate, CreatedByUser)
	select 1, ID, HazmatCertificateDocName, HazmatCertificateDocument, createdateutc, HazmatExpiration, 'System' from tblCarrier where HazmatCertificateDocName is not null
	union
	select 2, ID, LiabilityInsuranceDocName, LiabilityInsuranceDocument, createdateutc, LiabilityInsuranceExpiration, 'System' from tblCarrier where LiabilityInsuranceDocName is not null
	union
	select 3, ID, W9DocName, W9Document, createdateutc, null, 'System' from tblCarrier where W9DocName is not null
	union
	select 4, ID, CarrierAgreementDocName, CarrierAgreementDocument, createdateutc, null, 'System' from tblCarrier where CarrierAgreementDocName is not null
	union
	select 5, ID, WorkersCompDocName, WorkersCompDocument, createdateutc, WorkersCompExpiration, 'System' from tblCarrier where WorkersCompDocName is not null
	union
	select 6, ID, ACHInstructionsDocName, ACHInstructionsDocument, createdateutc, null, 'System' from tblCarrier where ACHInstructionsDocName is not null
go

alter table tblcarrier drop column hazmatcertificatedocname
alter table tblcarrier drop column hazmatcertificatedocument
alter table tblcarrier drop column hazmatexpiration

alter table tblcarrier drop column liabilityinsurancedocname
alter table tblcarrier drop column liabilityinsurancedocument
alter table tblcarrier drop column liabilityinsuranceexpiration

alter table tblcarrier drop column w9docname
alter table tblcarrier drop column w9document

alter table tblcarrier drop column carrieragreementdocname
alter table tblcarrier drop column carrieragreementdocument

alter table tblcarrier drop column workerscompdocname
alter table tblcarrier drop column workerscompdocument
alter table tblcarrier drop column workerscompexpiration

alter table tblcarrier drop column achinstructionsdocname
alter table tblcarrier drop column achinstructionsdocument

exec _spRebuildAllObjects
go

commit 
set noexec off