BEGIN TRANSACTION
GO

UPDATE tblSetting SET Value = '1.1.22' WHERE ID=0
GO

/******************************************************/
-- Date Created: 16 May 2013
-- Author:		 Kevin Alons
-- Purpose:		 return the translated "friendly" info for a Reroute
/******************************************************/
ALTER VIEW [dbo].[viewReroute] AS
SELECT ORR.ID
	, ORR.OrderID
	, PD.Name AS PrevDestination
	, PD.DestinationType AS PrevDestType
	, ORR.Notes
	, ORR.UserName
	, ORR.RerouteDate
FROM tblOrderReroute  ORR
JOIN viewDestination PD ON PD.ID = ORR.PreviousDestinationID

GO

COMMIT