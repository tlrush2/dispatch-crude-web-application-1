SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.11.15.2'
SELECT  @NewVersion = '3.11.15.3'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1292-1293: Fixing delete button issue and column filtering issue'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/***********************************/
-- Date Created: 27 Jan 2013
-- Author: Kevin Alons
-- Purpose: return Pumper records with FullName computed field
-- Changes:  04/25/2016 - DCWEB-1292 - Adding active column to allow hiding of delete button when pumper is used on an origin
/***********************************/
ALTER VIEW viewPumper AS
SELECT P.*
	, Active = CAST(CASE WHEN (SELECT COUNT(ID) FROM tblOrigin WHERE PumperID = P.ID) > 0 THEN 1 ELSE 0 END AS BIT)
	, FirstName + ' ' + LastName AS FullName
	, O.Name AS Operator
FROM tblPumper P
LEFT JOIN tblOperator O ON O.ID = P.OperatorID
GO


/* Refresh any/all views that may be affected by this change */
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO



COMMIT 
SET NOEXEC OFF