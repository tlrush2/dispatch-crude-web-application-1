-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.6.20'
SELECT  @NewVersion = '3.6.21'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Settings: Add new "Driver App Settings.Limit to single active order" boolean setting'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

INSERT INTO tblSetting (ID, Category, Name, Value, SettingTypeID, CreatedByUser, ReadOnly)
	VALUES (44, 'Driver App Settings', 'Limit to single active order', 'True', 2, 'System', 0)
GO

ALTER TABLE tblDriverSyncLog ADD OutgoingJson text NULL
GO

COMMIT
SET NOEXEC OFF