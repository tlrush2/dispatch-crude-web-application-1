SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.1.3'
SELECT  @NewVersion = '4.1.3.1'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1746 Update order rule names to support new photo types'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO



UPDATE tblOrderRuleType SET NAME = 'Require Origin (BOL) Photo?' WHERE ID = 10
GO

UPDATE tblOrderRuleType SET NAME = 'Require Destination (BOL) Photo?' WHERE ID = 11
GO



COMMIT
SET NOEXEC OFF