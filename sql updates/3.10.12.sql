-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.10.11'
SELECT  @NewVersion = '3.10.12'

-- only ensure the curr version matches when we are not testing with an "x.x.x" DEV version
IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-936: Remove unnecessary columns from tblImportCenterFieldDefinition (IsKey, DoUpdate, DeleteDateUTC, DeletedByUser)'
	UNION 
	SELECT @NewVersion, 0, 'DCWEB-936: tblObjectCustomData.ObjectID -> RecordID and other table changes'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

-- purge any currently "marked deleted" ImportCenterFieldDefinition records
DELETE FROM tblImportCenterFieldDefinition WHERE DeleteDateUTC IS NOT NULL

ALTER TABLE tblImportCenterFieldDefinition DROP CONSTRAINT DF_ImportCenterFieldDefinition_IsKey, DF_ImportCenterFieldDefinition_DoUpdate
ALTER TABLE tblImportCenterFieldDefinition DROP COLUMN IsKey, DoUpdate, DeleteDateUTC, DeletedByUser
GO

/*****************************************************
 Creation Info: 3.10.1 - 2015/12/15
 Author: Kevin Alons
 Purpose: clone an existing ImportCenterDefinition with a new name
 Changes:
 x.x.x - 2016/02/06 - KDA - remove IsKey & DoUpdate tblImportCenterFieldDefinition fields from processing
*****************************************************/
ALTER PROCEDURE spCloneImportCenterDefinition(@importCenterDefinitionID int, @Name varchar(50), @UserName varchar(100), @NewID int = NULL OUTPUT) AS
BEGIN
	BEGIN TRAN CICD

	-- copy the basic ImportCenterDefinition first
	INSERT INTO tblImportCenterDefinition (RootObjectID, Name, UserNames, CreatedByUser)
		SELECT RootObjectID, @Name, UserNames, @UserName FROM tblImportCenterDefinition WHERE ID = @importCenterDefinitionID
	SET @NewID = SCOPE_IDENTITY()

	-- add the root ImportFieldDefinitions (with no ParentFieldID)
	INSERT INTO tblImportCenterFieldDefinition (ImportCenterDefinitionID, ObjectFieldID, CreatedByUser)
		SELECT @NewID, ObjectFieldID, CreatedByUser
		FROM tblImportCenterFieldDefinition
		WHERE ImportCenterDefinitionID = @importCenterDefinitionID
		  AND ParentFieldID IS NULL

	DECLARE @Level int; SET @Level = 0

	-- create a table to store the old-new ImportField mappings (will be used later to add the ImportFieldDefinition_Fields)
	DECLARE @Map TABLE (OID int, NID int, Level int)
	-- populate with the just added root ImportFieldDefinitions
	INSERT INTO @Map 
		SELECT OICFD.ID, NICFD.ID, @Level
		FROM tblImportCenterFieldDefinition OICFD
		JOIN tblImportCenterFieldDefinition NICFD ON NICFD.ImportCenterDefinitionID = @NewID AND NICFD.ObjectFieldID = OICFD.ObjectFieldID
		WHERE OICFD.ImportCenterDefinitionID = @importCenterDefinitionID AND OICFD.ParentFieldID IS NULL

	--DECLARE @debugMap XML = (SELECT * FROM @Map FOR XML AUTO)

	-- create table to remember newly added child FieldDefinition records
	DECLARE @NID IDTABLE
	-- populate with the first child FieldDefinitions to be added
	INSERT INTO @NID 
		SELECT ID FROM tblImportCenterFieldDefinition X JOIN @Map M ON M.OID = X.ParentFieldID WHERE ImportCenterDefinitionID = @importCenterDefinitionID EXCEPT SELECT OID FROM @Map

	--DECLARE @debugNID XML = (SELECT * FROM @NID FOR XML AUTO)

	WHILE EXISTS (SELECT ID FROM @NID)
	BEGIN
		-- add the next level of children FieldDefinitions
		INSERT INTO tblImportCenterFieldDefinition (ImportCenterDefinitionID, ObjectFieldID, ParentFieldID, CSharpExpression, KeyComparisonTypeID, CreatedByUser)
			SELECT @NewID, ObjectFieldID, M.NID, CSharpExpression, KeyComparisonTypeID, CreatedByUser
			FROM tblImportCenterFieldDefinition ICFD
			JOIN @NID NID ON NID.ID = ICFD.ID
			JOIN @Map M ON M.OID = ICFD.ParentFieldID

		-- record the old-new mappings for these new FieldDefinitions
		INSERT INTO @Map 
			SELECT OICFD.ID, NICFD.ID, @Level
			FROM tblImportCenterFieldDefinition OICFD
			JOIN tblImportCenterFieldDefinition NICFD ON NICFD.ImportCenterDefinitionID = @NewID AND NICFD.ObjectFieldID = OICFD.ObjectFieldID
			WHERE OICFD.ID IN (SELECT ID FROM @NID)

		--SELECT @debugMap = (SELECT * FROM @Map FOR XML AUTO)

		-- see if any next level new FieldMappings need to be processed
		DELETE FROM @NID
		INSERT INTO @NID 
			SELECT ID FROM tblImportCenterFieldDefinition X JOIN @Map M ON M.OID = X.ParentFieldID WHERE ImportCenterDefinitionID = @importCenterDefinitionID EXCEPT SELECT OID FROM @Map

		--SELECT @debugNID = (SELECT * FROM @NID FOR XML AUTO)
		
		-- increment @Level
		SET @Level = @Level + 1
	END

	-- add all the FieldDefinitionFields based on the accumulated @Map records
	INSERT INTO tblImportCenterFieldDefinitionField (ImportCenterFieldID, ImportFieldName, Position)
		SELECT M.NID, DF.ImportFieldName, DF.Position
		FROM tblImportCenterFieldDefinitionField DF
		JOIN @Map M ON M.OID = DF.ImportCenterFieldID

	COMMIT TRAN CICD
END
GO

ALTER TABLE tblObjectCustomData DROP CONSTRAINT FK_ObjectCustomData_Field
GO

DROP INDEX udxObjectCustomData_Main ON tblObjectCustomData
GO

EXECUTE sp_rename N'dbo.tblObjectCustomData.FieldID', N'ObjectFieldID', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.tblObjectCustomData.ObjectID', N'RecordID', 'COLUMN' 
GO

ALTER TABLE tblObjectCustomData WITH CHECK ADD CONSTRAINT FK_ObjectCustomData_ObjectField FOREIGN KEY(ObjectFieldID)
REFERENCES tblObjectField(ID)
GO

ALTER TABLE tblObjectCustomData DROP 
	CONSTRAINT DF_ObjectCustomData_CreateDateUTC, DF_ObjectCustomData_CreatedByUser
	, COLUMN DeleteDateUTC, DeletedByUser
GO
ALTER TABLE tblObjectCustomData ADD CONSTRAINT DF_ObjectCustomData_CreateDateUTC DEFAULT (getutcdate()) FOR CreateDateUTC 
GO
CREATE UNIQUE NONCLUSTERED INDEX udxObjectCustomData_Main ON tblObjectCustomData(ObjectFieldID, RecordID)
GO

EXEC _spRebuildAllObjects
GO

/*******************************************************
 Creation Info:	x.x.x - 2016/01/04
 Author:		Kevin Alons
 Purpose:		return all fields + some support fields for tblImportCenterFieldDefinition
 Changes:
 x.x.x - 2016/02/06 - KDA - add IsCustom field (from tblObjectField)
*******************************************************/
ALTER VIEW viewImportCenterFieldDefinition AS
	SELECT ICDF.* 
		, HasChildren = CASE WHEN EXISTS (SELECT ID FROM tblImportCenterFieldDefinition WHERE ParentFieldID = ICDF.ID) THEN 1 ELSE 0 END
		, Name = OBF.Name , ObjectName = OBF.Object, OBF.FieldName, OBF.ObjectFieldTypeID, OBF.ObjectID, OBF.ObjectSqlSourceName, OBF.ObjectSqlTargetName
		, IDFieldName = (SELECT TOP 1 FieldName FROM tblObjectField WHERE ObjectID = OBF.ObjectID AND IsKey = 1)
		, OBF.AllowNull, OBF.IsCustom
		, OBF.ParentObjectID
		, OBF.DefaultValue
		, NestLevel = dbo.fnICFNestLevel(ICDF.ID) 
	FROM tblImportCenterFieldDefinition ICDF
	JOIN viewObjectField OBF ON OBF.ID = ICDF.ObjectFieldID
	LEFT JOIN tblImportCenterFieldDefinition PICDF ON PICDF.ID = ICDF.ParentFieldID
	LEFT JOIN viewObjectField POBF ON POBF.ID = PICDF.ObjectFieldID
GO

-- correct existing records that have the default "ID" ParentObjectIDFieldName when ParentObjectID IS NULL
UPDATE tblObjectField SET ParentObjectIDFieldName = NULL WHERE ParentObjectID IS NULL
GO

/*******************************************************
 Creation Info:	3.10.1 - 2016/01/04
 Author:		Kevin Alons
 Purpose:		recursive function to determine the nestLevel of a single ImportCenterFieldDefinition record
 Changes:
*******************************************************/
ALTER FUNCTION fnICFNestLevel(@id int) RETURNS int AS
BEGIN
	DECLARE @ParentFieldID int
	SELECT @ParentFieldID = ParentFieldID FROM tblImportCenterFieldDefinition WHERE ID = @id
	RETURN (CASE WHEN @ParentFieldID IS NULL THEN 0 ELSE 1 + dbo.fnICFNestLevel(@ParentFieldID) END)
END
GO


/* 02/25/2016 - KDA & BB - Changing the Identity Seed on tblObjectField.ID */
ALTER TABLE dbo.tblObjectField
	DROP CONSTRAINT FK_ObjectField_AllowNull
GO

ALTER TABLE dbo.tblObjectFieldAllowNull SET (LOCK_ESCALATION = TABLE)
GO

ALTER TABLE dbo.tblObjectField
	DROP CONSTRAINT FK_ObjectField_ObjectFieldType
GO
ALTER TABLE dbo.tblObjectFieldType SET (LOCK_ESCALATION = TABLE)
GO

ALTER TABLE dbo.tblObjectField
	DROP CONSTRAINT FK_ObjectField_Object
GO

ALTER TABLE dbo.tblObjectField
	DROP CONSTRAINT FK_ObjectField_ParentObject
GO

ALTER TABLE dbo.tblObject SET (LOCK_ESCALATION = TABLE)
GO

ALTER TABLE dbo.tblObjectField
	DROP CONSTRAINT DF_ObjectField_ReadOnly
GO

ALTER TABLE dbo.tblObjectField
	DROP CONSTRAINT DF_ObjectField_AllowNull
GO

ALTER TABLE dbo.tblObjectField
	DROP CONSTRAINT DF_ObjectField_IsKey
GO

ALTER TABLE dbo.tblObjectField
	DROP CONSTRAINT DF_ObjectField_IsCustom
GO

ALTER TABLE dbo.tblObjectField
	DROP CONSTRAINT DF_ObjectField_ParentObjectIDFieldName
GO

ALTER TABLE dbo.tblObjectField
	DROP CONSTRAINT DF_ObjectField_CreateDateUTC
GO

ALTER TABLE dbo.tblObjectField
	DROP CONSTRAINT DF_ObjectField_CreatedByUser
GO

CREATE TABLE dbo.Tmp_tblObjectField
	(
	ID int NOT NULL IDENTITY (100000, 1),
	ObjectID int NOT NULL,
	FieldName varchar(50) NOT NULL,
	Name varchar(50) NOT NULL,
	ObjectFieldTypeID smallint NOT NULL,
	DefaultValue varchar(MAX) NULL,
	ReadOnly bit NOT NULL,
	AllowNullID int NOT NULL,
	IsKey bit NOT NULL,
	IsCustom bit NOT NULL,
	ParentObjectID int NULL,
	ParentObjectIDFieldName varchar(50) NULL,
	CreateDateUTC datetime NOT NULL,
	CreatedByUser varchar(100) NOT NULL,
	LastChangeDateUTC datetime NULL,
	LastChangedByUser varchar(100) NULL,
	DeleteDateUTC datetime NULL,
	DeletedByUser varchar(100) NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE dbo.Tmp_tblObjectField SET (LOCK_ESCALATION = TABLE)
GO

GRANT DELETE ON dbo.Tmp_tblObjectField TO role_iis_acct  AS dbo
GO

GRANT INSERT ON dbo.Tmp_tblObjectField TO role_iis_acct  AS dbo
GO

GRANT SELECT ON dbo.Tmp_tblObjectField TO role_iis_acct  AS dbo
GO

GRANT UPDATE ON dbo.Tmp_tblObjectField TO role_iis_acct  AS dbo
GO

ALTER TABLE dbo.Tmp_tblObjectField ADD CONSTRAINT
	DF_ObjectField_ReadOnly DEFAULT ((0)) FOR ReadOnly
GO

ALTER TABLE dbo.Tmp_tblObjectField ADD CONSTRAINT
	DF_ObjectField_AllowNull DEFAULT ((1)) FOR AllowNullID
GO

ALTER TABLE dbo.Tmp_tblObjectField ADD CONSTRAINT
	DF_ObjectField_IsKey DEFAULT ((0)) FOR IsKey
GO

ALTER TABLE dbo.Tmp_tblObjectField ADD CONSTRAINT
	DF_ObjectField_IsCustom DEFAULT ((1)) FOR IsCustom
GO

ALTER TABLE dbo.Tmp_tblObjectField ADD CONSTRAINT
	DF_ObjectField_ParentObjectIDFieldName DEFAULT ('ID') FOR ParentObjectIDFieldName
GO

ALTER TABLE dbo.Tmp_tblObjectField ADD CONSTRAINT
	DF_ObjectField_CreateDateUTC DEFAULT (getutcdate()) FOR CreateDateUTC
GO

ALTER TABLE dbo.Tmp_tblObjectField ADD CONSTRAINT
	DF_ObjectField_CreatedByUser DEFAULT ('System') FOR CreatedByUser
GO

SET IDENTITY_INSERT dbo.Tmp_tblObjectField ON
GO

IF EXISTS(SELECT * FROM dbo.tblObjectField)
	 EXEC('INSERT INTO dbo.Tmp_tblObjectField (ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, ReadOnly, AllowNullID, IsKey, IsCustom, ParentObjectID, ParentObjectIDFieldName, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser)
		SELECT ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, ReadOnly, AllowNullID, IsKey, IsCustom, ParentObjectID, ParentObjectIDFieldName, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser FROM dbo.tblObjectField WITH (HOLDLOCK TABLOCKX)')
GO

SET IDENTITY_INSERT dbo.Tmp_tblObjectField OFF
GO

ALTER TABLE dbo.tblImportCenterFieldDefinition
	DROP CONSTRAINT FK_ImportCenterField_Object
GO

ALTER TABLE dbo.tblObjectCustomData
	DROP CONSTRAINT FK_ObjectCustomData_ObjectField
GO

DROP TABLE dbo.tblObjectField
GO

EXECUTE sp_rename N'dbo.Tmp_tblObjectField', N'tblObjectField', 'OBJECT' 
GO

ALTER TABLE dbo.tblObjectField ADD CONSTRAINT
	PK_ObjectField PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX udxObjectField_Main ON dbo.tblObjectField
	(
	ObjectID,
	FieldName
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX udxObjectField_Main_Name ON dbo.tblObjectField
	(
	ObjectID,
	Name
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

ALTER TABLE dbo.tblObjectField WITH NOCHECK ADD CONSTRAINT
	CK_ObjectField_Custom_NoParent CHECK (([IsCustom]=(0) OR [ParentObjectID] IS NULL))
GO

ALTER TABLE dbo.tblObjectField ADD CONSTRAINT
	FK_ObjectField_Object FOREIGN KEY
	(
	ObjectID
	) REFERENCES dbo.tblObject
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 	
GO

ALTER TABLE dbo.tblObjectField ADD CONSTRAINT
	FK_ObjectField_ObjectFieldType FOREIGN KEY
	(
	ObjectFieldTypeID
	) REFERENCES dbo.tblObjectFieldType
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 	
GO

ALTER TABLE dbo.tblObjectField ADD CONSTRAINT
	FK_ObjectField_AllowNull FOREIGN KEY
	(
	AllowNullID
	) REFERENCES dbo.tblObjectFieldAllowNull
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 	
GO

ALTER TABLE dbo.tblObjectField ADD CONSTRAINT
	FK_ObjectField_ParentObject FOREIGN KEY
	(
	ParentObjectID
	) REFERENCES dbo.tblObject
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 	
GO

ALTER TABLE dbo.tblObjectCustomData ADD CONSTRAINT
	FK_ObjectCustomData_ObjectField FOREIGN KEY
	(
	ObjectFieldID
	) REFERENCES dbo.tblObjectField
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO

ALTER TABLE dbo.tblObjectCustomData SET (LOCK_ESCALATION = TABLE)
GO

ALTER TABLE dbo.tblImportCenterFieldDefinition ADD CONSTRAINT
	FK_ImportCenterField_Object FOREIGN KEY
	(
	ObjectFieldID
	) REFERENCES dbo.tblObjectField
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO

ALTER TABLE dbo.tblImportCenterFieldDefinition SET (LOCK_ESCALATION = TABLE)
GO


COMMIT
SET NOEXEC OFF