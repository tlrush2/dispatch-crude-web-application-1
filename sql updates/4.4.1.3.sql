SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.4.1.2'
SELECT  @NewVersion = '4.4.1.3'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1954: Add Driver (Last, First) column to Report Center'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


  -- Insert New Driver Last name then First name field
  SET IDENTITY_INSERT tblReportColumnDefinition ON
  INSERT INTO tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport, IsTicketField)
  SELECT 90072, 1, 'OriginDriverLast + '', '' + OriginDriverFirst', 'GENERAL | Driver (Last, First)', null, 'OriginDriverID', 2, 'SELECT ID, Name = FullName FROM viewDriver ORDER BY FullName', 0, '*', 0, 0
  SET IDENTITY_INSERT tblReportColumnDefinition OFF
  GO

  -- Update current driver First name then Last name field to indicate this order for identification against the new field
  UPDATE tblReportColumnDefinition
  SET Caption = 'GENERAL | Driver (First Last)'
  WHERE ID = 27
  GO
  
  
COMMIT
SET NOEXEC OFF
