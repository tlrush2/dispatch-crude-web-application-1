-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.7.39.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.7.39.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0

DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.7.39'
SELECT  @NewVersion = '3.7.40'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-801: fix settlement issues where wrong Settlement Factor (Unit type) was used'
	UNION SELECT @NewVersion, 0, 'DCWEB-801: fix settlement issues where Min Settlement units was applied when specified Settlement Factor was missing from order'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/***********************************
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Carrier "Settlement" $$ values to an Delivered/Audited order
-- Changes:
	- 2016/06/40 - 3.7.40 - KDA - use specific SettlementFactor (not fall thru to less precise if specified one is missing)
								- use the DestinationUomID for the Settlement Uom if using a Destination Settlement Factor
***********************************/
ALTER VIEW viewOrderSettlementUnitsCarrier AS
	SELECT X3.*
		, SettlementUnits = CASE WHEN X3.ActualUnits IS NULL THEN 0 ELSE dbo.fnMaxDecimal(X3.ActualUnits, MinSettlementUnits) END
	FROM (
		SELECT OrderID
			, CarrierID
			, SettlementUomID
			, CarrierSettlementFactorID
			, SettlementFactorID
			, MinSettlementUnits = dbo.fnMinDecimal(isnull(X2.WRMSVUnits, X2.MinSettlementUnits), X2.MinSettlementUnits)
			, MinSettlementUnitsID
			, ActualUnits
		FROM (
			SELECT OrderID
				, CarrierID
				, SettlementUomID
				, CarrierSettlementFactorID
				, SettlementFactorID
				, WRMSVUnits = dbo.fnConvertUOM(WRMSVUnits, WRMSVUomID, SettlementUomID)
				, MinSettlementUnits = dbo.fnConvertUOM(MinSettlementUnits, MinSettlementUomID, SettlementUomID)
				, MinSettlementUnitsID
				, ActualUnits 
			FROM (
				SELECT OrderID = O.ID
					, O.CarrierID
					, SettlementUomID = CASE WHEN MSF.ID IN (1,2,3) THEN O.OriginUomID ELSE O.DestUomID END
					, CarrierSettlementFactorID = MSF.ID
					, MSF.SettlementFactorID
					, R.WRMSVUomID
					, R.WRMSVUnits
					, MinSettlementUnitsID = MSU.ID
					, MinSettlementUomID = MSU.UomID
					, MinSettlementUnits = isnull(MSU.MinSettlementUnits, 0) 
					, ActualUnits = CASE MSF.SettlementFactorID
							WHEN 1 THEN O.OriginGrossUnits 
							WHEN 3 THEN O.OriginGrossStdUnits 
							WHEN 2 THEN O.OriginNetUnits 
							WHEN 4 THEN O.DestGrossUnits
							WHEN 5 THEN O.DestNetUnits END
				FROM dbo.tblOrder O
				CROSS APPLY dbo.fnOrderCarrierSettlementFactor(O.ID) MSF
				CROSS APPLY dbo.fnOrderCarrierMinSettlementUnits(O.ID) MSU 
				JOIN dbo.tblRoute R ON R.ID = O.RouteID
				JOIN tblCarrier C ON C.ID = O.CarrierID
			) X
		) X2
	) X3

GO

/***********************************
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Shipper "Settlement" $$ values to an Delivered/Audited order
-- Changes:
	- 2016/06/40 - 3.7.40 - KDA - use specific SettlementFactor (not fall thru to less precise if specified one is missing)
								- use the DestinationUomID for the Settlement Uom if using a Destination Settlement Factor
***********************************/
ALTER VIEW viewOrderSettlementUnitsShipper AS
	SELECT X3.*
		, SettlementUnits = CASE WHEN X3.ActualUnits IS NULL THEN 0 ELSE dbo.fnMaxDecimal(X3.ActualUnits, MinSettlementUnits) END
	FROM (
		SELECT OrderID
			, ShipperID
			, SettlementUomID
			, ShipperSettlementFactorID
			, SettlementFactorID
			, MinSettlementUnits = dbo.fnMinDecimal(isnull(X2.WRMSVUnits, X2.MinSettlementUnits), X2.MinSettlementUnits)
			, MinSettlementUnitsID
			, ActualUnits
		FROM (
			SELECT OrderID
				, ShipperID
				, SettlementUomID
				, ShipperSettlementFactorID
				, SettlementFactorID
				, WRMSVUnits = dbo.fnConvertUOM(WRMSVUnits, WRMSVUomID, SettlementUomID)
				, MinSettlementUnits = dbo.fnConvertUOM(MinSettlementUnits, MinSettlementUomID, SettlementUomID)
				, MinSettlementUnitsID
				, ActualUnits 
			FROM (
				SELECT OrderID = O.ID
					, ShipperID = O.CustomerID
					, SettlementUomID = CASE WHEN MSF.ID IN (1,2,3) THEN O.OriginUomID ELSE O.DestUomID END
					, ShipperSettlementFactorID = MSF.ID
					, MSF.SettlementFactorID
					, R.WRMSVUomID
					, R.WRMSVUnits
					, MinSettlementUnitsID = MSU.ID
					, MinSettlementUomID = MSU.UomID
					, MinSettlementUnits = isnull(MSU.MinSettlementUnits, 0) 
					, ActualUnits = CASE MSF.SettlementFactorID
							WHEN 1 THEN O.OriginGrossUnits 
							WHEN 3 THEN O.OriginGrossStdUnits 
							WHEN 2 THEN O.OriginNetUnits 
							WHEN 4 THEN O.DestGrossUnits
							WHEN 5 THEN O.DestNetUnits END
				FROM dbo.tblOrder O
				CROSS APPLY dbo.fnOrderShipperSettlementFactor(O.ID) MSF
				CROSS APPLY dbo.fnOrderShipperMinSettlementUnits(O.ID) MSU 
				JOIN dbo.tblRoute R ON R.ID = O.RouteID
				JOIN tblCustomer C ON C.ID = O.CustomerID
			) X
		) X2
	) X3

GO

ALTER TABLE tblSettlementFactor ADD OrderUnitsField varchar(25) NULL
GO
UPDATE tblSettlementFactor SET OrderUnitsField = 'OriginGrossUnits' WHERE ID = 1
UPDATE tblSettlementFactor SET OrderUnitsField = 'OriginNetUnits' WHERE ID = 2
UPDATE tblSettlementFactor SET OrderUnitsField = 'OriginGrossStdUnits' WHERE ID = 3
UPDATE tblSettlementFactor SET OrderUnitsField = 'DestGrossUnits' WHERE ID = 4
UPDATE tblSettlementFactor SET OrderUnitsField = 'DestNetUnits' WHERE ID = 5

EXEC _spRefreshAllViews
GO

COMMIT
SET NOEXEC OFF