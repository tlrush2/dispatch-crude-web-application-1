-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.6.11'
SELECT  @NewVersion = '3.6.12'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Add tblDriverSyncLog + related system setting'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

CREATE TABLE tblDriverSyncLog
(
  ID int identity(1, 1) not null constraint PK_DriverSyncLog PRIMARY KEY
, DriverID int null
, SyncDate datetime constraint DF_DriverSyncLog_SyncDate DEFAULT (getdate())
, IncomingJson text
)
GRANT SELECT, INSERT, UPDATE, DELETE ON tblDriverSyncLog TO dispatchcrude_iis_acct
GO

INSERT INTO tblSetting (ID, Name, SettingTypeID, Value, Category, CreateDateUTC, CreatedByUser, ReadOnly)
VALUES (43, 'Log Driver Sync JSON', 2, 'False', 'System Wide', GETUTCDATE(), 'System', 0)
GO

COMMIT
SET NOEXEC OFF