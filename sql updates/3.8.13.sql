-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.8.12.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.8.12.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0

DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.8.12'
SELECT  @NewVersion = '3.8.13'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-729: Added C.O.D.E. export functionality. Only front end change is addition of "Shipper CODE ID column" in the shipper maintenance page.'	
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/* Add CODE columns to tables */
ALTER TABLE tblCustomer ADD CodeDXCode VARCHAR(2) NULL
ALTER TABLE tblOriginType ADD CodeDXCode INT NULL
ALTER TABLE tblTicketType ADD CodeDXCode INT NULL
GO

/* Update tblTicketType with default CODE ID numbers */
UPDATE tblTicketType SET CodeDXCode = 2 WHERE ID = 1 --gauge run
UPDATE tblTicketType SET CodeDXCode = 3 WHERE ID = 2 --net volume
UPDATE tblTicketType SET CodeDXCode = 1 WHERE ID = 3 --meter run
UPDATE tblTicketType SET CodeDXCode = 6 WHERE ID = 4 --basic run
UPDATE tblTicketType SET CodeDXCode = 6 WHERE ID = 5 --gross volume
UPDATE tblTicketType SET CodeDXCode = 1 WHERE ID = 6 --can meter run
UPDATE tblTicketType SET CodeDXCode = 3 WHERE ID = 7 --gauge net
UPDATE tblTicketType SET CodeDXCode = 6 WHERE ID = 8 --can basic run
GO


/* Created function to convert date to MMDDYY format */
EXEC _spDropFunction 'fnDateMMDDYY' 
GO
-- =============================================
-- Author:		Ben Bloodworth
-- Create date: 5 Jun 2015
-- Description:	Return the provided date value in MMDDYY format
-- =============================================
CREATE FUNCTION [dbo].[fnDateMMDDYY]
(
  @value datetime
)
RETURNS varchar(25)
AS
BEGIN
	DECLARE @ret varchar(25)

	SELECT @ret = right('0' +  ltrim(MONTH(@value)), 2) 
		+ right('0' +  ltrim(DAY(@value)), 2) 
		+ right(ltrim(year(@value)),2)
	RETURN @ret
END
GO
GRANT EXECUTE ON fnDateMMDDYY TO role_iis_acct
GO



/* Created function to convert date to MMDD format */
EXEC _spDropFunction 'fnDateMMDD' 
GO
-- =============================================
-- Author:		Ben Bloodworth
-- Create date: 5 Jun 2015
-- Description:	Return the provided date value in MMDD format
-- =============================================
CREATE FUNCTION [dbo].[fnDateMMDD]
(
  @value datetime
)
RETURNS varchar(25)
AS
BEGIN
	DECLARE @ret varchar(25)

	SELECT @ret = right('0' +  ltrim(MONTH(@value)), 2) 
		+ right('0' +  ltrim(DAY(@value)), 2)
	RETURN @ret
END
GO
GRANT EXECUTE ON fnDateMMDD TO role_iis_acct
GO



/* Created function to convert date to MMDD format */
EXEC _spDropFunction 'fnOpenCloseReadingCODEFormat' 
GO
-- =============================================
-- Author:		Ben Bloodworth
-- Create date: 5 Jun 2015
-- Description:	Convert open/close gauge readings to CODE format spec. Also converts meter readings.
--				1=Meter, 2=Gauge, anything else returns zeros
--				0=Open Reading, 1=Close Reading
-- =============================================
CREATE FUNCTION [dbo].[fnOpenCloseReadingCODEFormat]
(
  @TicketID int
  ,@ticketType int
  ,@closingReading bit
)
RETURNS varchar(10)
AS
BEGIN
	DECLARE @ret varchar(10)
		
	IF @closingReading = 0 
		BEGIN
			SELECT @ret = 
				CASE @ticketType
					WHEN 1 
						THEN dbo.fnFixedLenNum(OT.OpenMeterUnits,8,2,1)
					WHEN 2 
						THEN dbo.fnFixedLenNum(OT.OpeningGaugeFeet,4,0,1) 
							+ dbo.fnFixedLenNum(OT.OpeningGaugeInch,3,0,1) 
							+ dbo.fnFixedLenNum(OT.OpeningGaugeQ,2,0,1) 
							+ '4'
					ELSE '0000000000'		
				END	
			FROM tblOrderTicket OT
			WHERE ID = @TicketID
		END
	ELSE
		BEGIN
			SELECT @ret = 
				CASE @ticketType
					WHEN 1 
						THEN dbo.fnFixedLenNum(OT.CloseMeterUnits,8,2,1)
					WHEN 2 
						THEN dbo.fnFixedLenNum(OT.ClosingGaugeFeet,4,0,1) 
							+ dbo.fnFixedLenNum(OT.ClosingGaugeInch,3,0,1) 
							+ dbo.fnFixedLenNum(OT.ClosingGaugeQ,2,0,1) 
							+ '4'
					ELSE '0000000000'		
				END	
			FROM tblOrderTicket OT
			WHERE ID = @TicketID			
		END
		
	RETURN @ret
END
GO
GRANT EXECUTE ON fnOpenCloseReadingCODEFormat TO role_iis_acct
GO



/*******************************************
-- Date Created: 28 May 2015
-- Author: Kevin Alons
-- Purpose: format a number to a fixed length
-- Changes:
	-- 3.7.x - 6/5/2015 - BBloodworth - remove @fixedLen parameter, add @omitDot parameter to omit the DOT
*******************************************/
ALTER FUNCTION [dbo].[fnFixedLenNum](@num decimal(38, 10), @leftDigits int, @decimalDigits int, @omitDot bit) RETURNS varchar(255) AS
BEGIN
	DECLARE @ret varchar(255)
	SET @ret = ''
	
	DECLARE @strNum varchar(255)
	SET @strNum = CAST(round(@num, @decimalDigits) as varchar(255))

	DECLARE @decimalPos int
	SET @decimalPos = CHARINDEX('.', @strNum, 1)
	IF @decimalPos = 0 SET @decimalPos = LEN(@strNum) + 1
	
	SET @ret = SUBSTRING(@strNum, 1, @decimalPos - 1)
	SET @ret = RIGHT(REPLICATE('0', @leftDigits) + @ret, @leftDigits)
	
	IF @decimalDigits > 0 
	BEGIN
		DECLARE @strDecimal varchar(255)
		SET @strDecimal = SUBSTRING(@strNum + REPLICATE('0', @decimalDigits), @decimalPos + 1, @decimalDigits)
		IF (@omitDot = 0) SET @ret = @ret + '.' 
		SET @ret = @ret + @strDecimal
	END

	RETURN (@ret)
END
GO


-- =============================================
-- Author:		Ben Bloodworth
-- Create date: 5 Jun 2015
-- Description:	Displays the base information for the CODE export. Please note that some values could change per DispatchCrude customer and thus it may
--              be better to retrieve these settings from the Data Exchange settings instead of this view.
-- =============================================
CREATE VIEW viewOrderExport_CODE_Base AS
SELECT 
	OrderID = O.ID
	,OrderTicketID = T.ID
	,Record_ID = '2'
	,Company_ID = C.CodeDXCode
	,Ticket_Type = TT.CodeDXCode
	,Transmit_Ind = '2'  -- Hard coded per Angie Toews (PDS Energy) but may need to be changed to dynamic if the customer says so
	,Run_Type = 1
	,Ticket_Number = CASE WHEN ISNUMERIC(RIGHT(T.CarrierTicketNum,1)) = 0 
							THEN dbo.fnFixedLenStr(T.CarrierTicketNum,7) 
							ELSE dbo.fnFixedLenNum(T.CarrierTicketNum,7,0,0) 
					 END
	,Run_Ticket_Date = dbo.fnDateMMDDYY(O.OrderDate)
	,Property_Code = CASE WHEN O.LeaseNum IS NULL 
							THEN '00000000000000' 
							ELSE
								CASE WHEN ISNUMERIC(O.LeaseNum) = 1								
									THEN dbo.fnFixedLenNum(O.LeaseNum,14,0,0)								
									ELSE dbo.fnFixedLenStr(O.LeaseNum,14) 
								END
					 END
	,Gathering_Charge = '0'  -- Hard coded per Angie Toews (PDS Energy) but may need to be changed to dynamic if the customer says so
	,Tank_Meter_Num = CASE WHEN ISNUMERIC(T.TankNum) = 1 AND T.TankNum IS NOT NULL THEN dbo.fnFixedLenNum(T.TankNum, 7, 0, 1)
						   WHEN ISNUMERIC(T.TankNum) = 0 AND T.TankNum IS NOT NULL THEN '0000000'
						   ELSE '0000000' --This accounts for when there is a NULL for the tank/meter number
					  END	
	,Adjustment_Ind = '0' -- Hard coded per Angie Toews (PDS Energy) but may need to be changed to dynamic if the customer says so
	,Opening_Reading = dbo.fnOpenCloseReadingCODEFormat(T.ID,TT.CodeDXCode,0)
	,Opening_Date = dbo.fnDateMMDD(O.OrderDate)
	,Closing_Reading = dbo.fnOpenCloseReadingCODEFormat(T.ID,TT.CodeDXCode,1)
	,Closing_Date = dbo.fnDateMMDD(O.DestDepartTime)
	,Meter_Factor = CASE WHEN T.MeterFactor IS NULL 
						THEN '00000000' 
						ELSE dbo.fnFixedLenNum(T.MeterFactor, 2, 6, 1) 
					END
	,Shrinkage_Incrustation = '1000000' -- Hard coded per Angie Toews (PDS Energy) but may need to be changed to dynamic if the customer says so
	,Opening_Temperature = CASE WHEN T.ProductHighTemp IS NULL 
								THEN '0000' 
								ELSE dbo.fnFixedLenNum(T.ProductHighTemp, 3, 1, 1) 
						   END
	,Closing_Temperature = CASE WHEN T.ProductLowTemp IS NULL 
								THEN '0000' 
								ELSE dbo.fnFixedLenNum(T.ProductLowTemp, 3, 1, 1) 
						   END
	,BSW = CASE WHEN TT.ID IN (1,3) --Non-Estmated ticket types (Gauge Run, Meter Run)
				THEN dbo.fnFixedLenNum(T.ProductBSW, 2, 2, 1)
				ELSE '0000'
		   END	
	,Observed_Gravity = dbo.fnFixedLenNum(T.ProductObsGravity, 2, 1, 1)
	,Observed_Temperature = dbo.fnFixedLenNum(T.ProductObsTemp, 3, 1, 1)
	,Pos_Neg_Code = '0' -- Hard coded per Angie Toews (PDS Energy) but may need to be changed to dynamic if the customer says so
	,Total_Net_Volume = dbo.fnFixedLenNum(T.NetUnits, 7, 2, 1)
	,Shippers_Net_Volume = dbo.fnFixedLenNum(T.NetUnits, 7, 2, 1)
	,Corrected_Gravity = dbo.fnFixedLenNum(T.Gravity60F, 2, 1, 1)
	,Product_Code = dbo.fnFixedLenStr('',3)	--Leave 3 blank spaces unless product is not crude oil. For others lookup PETROEX code	
	,Transmission_Date = dbo.fnFixedLenStr('',3) --Leave 3 blank spaces. Filled in by export reciever.
FROM viewOrderLocalDates O
	JOIN dbo.viewOrderCustomerFinalExportPending EP ON EP.ID = O.ID
	LEFT JOIN viewOrderTicket T ON T.OrderID = O.ID AND T.DeleteDateUTC IS NULL
	LEFT JOIN tblTicketType TT ON TT.ID = O.TicketTypeID
	LEFT JOIN tblCustomer C ON C.ID = O.CustomerID
	LEFT JOIN tblProduct P ON P.ID = O.ProductID
WHERE O.DeleteDateUTC IS NULL
AND O.Rejected = 0
GO


/* This viewCustomer alter is just here to "referesh" the view to include the new shipper CODE column */

/***********************************
-- Date Created: 27 Jan 2013
-- Author: Kevin Alons
-- Purpose: return Customer records with "translated friendly" values for FK relationships
-- Changes:
	- 3.7.41 - 2015/07/01 - KDA - remove UOM/SettlementFactor related columns
***********************************/
ALTER VIEW [dbo].[viewCustomer] AS
SELECT C.*
	, S.Abbreviation AS StateAbbrev
	, S.FullName AS State
FROM dbo.tblCustomer C
LEFT JOIN dbo.tblState S ON S.ID = C.StateID
GO


-- =============================================
-- Author:		Ben Bloodworth
-- Create date: 15 Jun 2015
-- Description:	View to concatenate all CODE values from viewOrderExport_CODE_Base
-- =============================================
CREATE VIEW viewOrderExport_CODE AS
SELECT 
	OrderID = OECB.OrderID
	,OrderTicketID = OECB.OrderTicketID
	,CompanyID = OECB.Company_ID
	,ExportText = (OECB.Record_ID
				+OECB.Company_ID
				+CAST(OECB.Ticket_Type AS VARCHAR)
				+OECB.Transmit_Ind
				+CAST(OECB.Run_Type AS VARCHAR)
				+CAST(OECB.Ticket_Number AS VARCHAR)
				+CAST(OECB.Run_Ticket_Date AS VARCHAR)
				+CAST(OECB.Property_Code AS VARCHAR)
				+OECB.Gathering_Charge
				+CAST(OECB.Tank_Meter_Num AS VARCHAR)
				+OECB.Adjustment_Ind
				+CAST(OECB.Opening_Reading AS VARCHAR)
				+CAST(OECB.Opening_Date AS VARCHAR)
				+CAST(OECB.Closing_Reading AS VARCHAR)
				+CAST(OECB.Closing_Date AS VARCHAR)
				+CAST(OECB.Meter_Factor AS VARCHAR)
				+OECB.Shrinkage_Incrustation
				+CAST(OECB.Opening_Temperature AS VARCHAR)
				+CAST(OECB.Closing_Temperature AS VARCHAR)
				+CAST(OECB.BSW AS VARCHAR)
				+CAST(OECB.Observed_Gravity AS VARCHAR)
				+CAST(OECB.Observed_Temperature AS VARCHAR)
				+OECB.Pos_Neg_Code
				+CAST(OECB.Total_Net_Volume AS VARCHAR)
				+CAST(OECB.Shippers_Net_Volume AS VARCHAR)
				+CAST(OECB.Corrected_Gravity AS VARCHAR)
				+CAST(OECB.Product_Code AS VARCHAR)
				+CAST(OECB.Transmission_Date AS VARCHAR))
FROM viewOrderExport_CODE_Base AS OECB
GO


/*****************************************************************************************/
-- Author: Kevin Alons
-- Date Created: 27 Feb 2014
-- Purpose: with the provided parameters, export the data and mark done (if doing finalExport)
--		8/12/15 - Ben B. - Expanded procedure to include C.O.D.E. export options
/*****************************************************************************************/
ALTER PROCEDURE [dbo].[spOrderExportFinalCustomer]
( 
  @customerID int 
, @exportedByUser varchar(100) = NULL -- will default to SUSER_NAME() -- default value for now
, @exportFormat varchar(100) -- Options are: 'SUNOCO_SUNDEX' or 'CODE_STANDARD'
, @finalExport bit = 1 -- defualt to TRUE
) 
AS BEGIN
	SET NOCOUNT ON
	-- default to the current user if not supplied
	IF @exportedByUser IS NULL SET @exportedByUser = SUSER_NAME()
	
	-- Create variable for error catching with CODE export
	DECLARE @CODEErrorFlag BIT = 0 -- Default to false
	
	-- retrieve the order records to export
	DECLARE @orderIDs IDTABLE
	INSERT INTO @orderIDs (ID)
	SELECT ID
	FROM dbo.viewOrderCustomerFinalExportPending 
	WHERE CustomerID = @customerID 

	BEGIN TRAN exportCustomer
	
	-- export the orders in the specified format
	IF (@exportFormat = 'SUNOCO_SUNDEX') BEGIN
		SELECT E.* 
		INTO #output
		FROM dbo.viewSunocoSundex E
		JOIN @orderIDs IDs ON IDs.ID = E._ID
		WHERE E._CustomerID = @customerID
		ORDER BY _OrderNum
	END
	ELSE
	IF (@exportFormat = 'CODE_STANDARD') BEGIN
		DECLARE @CodeDXCode varchar(2) = (SELECT CodeDXCode FROM tblCustomer WHERE ID = @customerID)
		IF LEN(@CodeDXCode) = 2 BEGIN
			SELECT E.ExportText
			INTO #output2
			FROM dbo.viewOrderExport_CODE E
				JOIN @orderIDs IDs ON IDs.ID = E.OrderID
			WHERE E.CompanyID = @CodeDXCode
		END
		ELSE SET @CODEErrorFlag = 1	
	END

	-- mark the orders as exported FINAL (for a Customer)
	IF (@finalExport = 1) BEGIN
		EXEC spMarkOrderExportFinalCustomer @orderIDs, @exportedByUser
	END
	
	COMMIT TRAN exportCustomer

	-- return the data to 
	IF (@exportFormat = 'SUNOCO_SUNDEX') BEGIN
		SELECT * FROM #output
	END
	
	IF (@exportFormat = 'CODE_STANDARD') BEGIN
		IF @CODEErrorFlag = 0 BEGIN
			SELECT * FROM #output2
		END
		ELSE SELECT 'Invalid Shipper CODE ID specified.  Fix in Shipper maintenance table.'
	END
		
END
GO


EXEC _spRebuildAllObjects
GO

COMMIT
SET NOEXEC OFF