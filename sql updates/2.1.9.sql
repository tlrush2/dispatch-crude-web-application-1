/* add Settlement "Product" filtering/processing capability
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.1.8'
SELECT  @NewVersion = '2.1.9'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

-- DO THE ACTUAL UPDATE HERE
/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
/***********************************/
ALTER PROCEDURE [dbo].[spOrdersExportCarrier]
(
  @StartDate datetime = NULL
, @EndDate datetime = NULL
, @CarrierID int = -1 -- all carriers
, @ProductID int = -1 -- all products
, @BatchID int = NULL  -- either show unbatched (NULL), or the specified batch orders
) AS BEGIN
	SELECT @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	
	SELECT OE.* 
		, dbo.fnLocal_to_UTC(ISNULL(OIC.LastChangeDateUTC, OIC.CreateDateUTC), OE.OriginTimeZoneID, OE.OriginUseDST) AS RatesAppliedDate
		, SB.BatchNum
		, OIC.ChainupFee
		, OIC.RerouteFee
		, OIC.BillableWaitMinutes
		, OIC.WaitFeeSubUnitID
		, OIC.WaitFeeRoundingTypeID
		, OIC.WaitRate
		, OIC.WaitFee
		, OIC.RejectionFee
		, OIC.H2SRate
		, OIC.H2SFee
		, OIC.TaxRate
		, OIC.RouteRate
		, OIC.LoadFee
		, OIC.TotalFee
		, OIC.FuelSurcharge
		, C.MinSettlementBarrels
		, cast(CASE WHEN OE.TicketCount = 0 THEN OE.TicketType ELSE NULL END as varchar(max)) AS TicketTypes
		, cast(CASE WHEN OE.TicketCount = 0 THEN OE.CarrierTicketNum ELSE NULL END as varchar(max)) AS TicketNums
		, cast(NULL AS varchar(max)) AS TankNums
		, cast(NULL as varchar(max)) AS ProductObsTemps
		, cast(NULL as varchar(max)) AS ProductObsGravities
		, cast(NULL as varchar(max)) AS ProductBSWs
		, cast(NULL as varchar(max)) AS TicketsRejected
		, cast(NULL as varchar(max)) AS TicketsRejectNotes
		, cast(NULL as varchar(max)) AS TicketsGrossBBLS
		, cast(NULL as varchar(max)) AS TicketsNetBBLS
		, cast(NULL as varchar(max)) AS PreviousDestinations
		, cast(NULL as varchar(max)) AS RerouteUsers
		, cast(NULL as varchar(max)) AS RerouteDates
		, cast(NULL as varchar(max)) AS RerouteNotes
	INTO #Orders
	FROM dbo.viewOrderExportFull OE
	JOIN dbo.tblCarrier C ON C.ID = OE.CarrierID
	LEFT JOIN dbo.tblOrderInvoiceCarrier OIC ON OIC.OrderID = OE.ID
	LEFT JOIN dbo.tblCarrierSettlementBatch SB ON SB.ID = OIC.BatchID
	WHERE OE.StatusID IN (4)  
	  AND (@CarrierID=-1 OR OE.CarrierID=@CarrierID) 
	  AND (@ProductID=-1 OR OE.ProductID=@ProductID) 
	  AND (@StartDate IS NULL OR OE.OrderDate >= @StartDate) 
	  AND (@EndDate IS NULL OR OE.OrderDate <= @EndDate)
	  AND ((@BatchID IS NULL AND (OIC.ID IS NULL OR OIC.BatchID IS NULL)) OR OIC.BatchID = @BatchID)
	  AND OE.DeleteDateUTC IS NULL
	ORDER BY OE.OriginDepartTimeUTC
	
	DECLARE @OrderID int, @TicketID int, @RerouteID int
	SELECT @OrderID = min(ID) FROM #Orders WHERE ID > 0
	
	WHILE @OrderID IS NOT NULL
	BEGIN
		-- add the Tickets information to the order (done this way since there could be multiple)
		SELECT @TicketID = min(ID) FROM dbo.tblOrderTicket WHERE OrderID = @OrderID AND DeleteDateUTC IS NULL AND ID > 0
		WHILE @TicketID IS NOT NULL
		BEGIN
			UPDATE #Orders 
				SET TicketTypes = isnull(O.TicketTypes + '<br/>', '') + OT.TicketType
					, TicketNums = isnull(O.TicketNums + '<br/>', '') + OT.CarrierTicketNum
					, TankNums = isnull(O.TankNums + '<br/>', '') + OT.TankNum
					, ProductObsTemps = isnull(O.ProductObsTemps + '<br/>', '') + isnull(ltrim(OT.ProductObsTemp), '')
					, ProductObsGravities = isnull(O.ProductObsGravities + '<br/>', '') + isnull(ltrim(OT.ProductObsGravity), '')
					, ProductBSWs = isnull(O.ProductBSWs + '<br/>', '') + isnull(ltrim(OT.ProductBSW), '')
					, TicketsRejected = isnull(O.TicketsRejected + '<br/>', '') + (CASE WHEN OT.Rejected = 0 THEN 'Valid' ELSE 'Rejected' END)
					, TicketsRejectNotes = isnull(O.TicketsRejectNotes + '<br/>', '') + isnull(OT.RejectNotes, '')
					, TicketsGrossBBLS = isnull(O.TicketsGrossBBLS + '<br/>', '') + isnull(ltrim(GrossBarrels), '')
					, TicketsNetBBLS = isnull(O.TicketsNetBBLS + '<br/>', '') + isnull(ltrim(NetBarrels), '')
			FROM #Orders O, viewOrderTicket OT
			WHERE O.ID = @OrderID AND OT.ID = @TicketID
			SELECT @TicketID = min(ID) FROM tblOrderTicket WHERE OrderID = @OrderID AND ID > @TicketID AND DeleteDateUTC IS NULL
		END	
		-- add the Reroutes information to the order (done this way since there could be multiple)
		SELECT @RerouteID = min(ID) FROM dbo.tblOrderReroute WHERE OrderID = @OrderID AND ID > 0
		WHILE @RerouteID IS NOT NULL
		BEGIN
			UPDATE #Orders
				SET PreviousDestinations = isnull(O.PreviousDestinations + '<br/>', '') + O_R.PreviousDestinationFull
					, RerouteUsers = isnull(O.RerouteUsers + '<br/>', '') + O_R.UserName
					, RerouteDates = isnull(O.RerouteDates + '<br/>', '') + dbo.fnDateMdYY(O_R.RerouteDate)
					, RerouteNotes = isnull(O.RerouteNotes + '<br/>', '') + isnull(O_R.Notes, '')
			FROM #Orders O, viewOrderReroute O_R
			WHERE O.ID = @OrderID AND O_R.ID = @RerouteID
			SELECT @RerouteID = min(ID) FROM tblOrderReroute WHERE OrderID = @OrderID AND ID > @RerouteID
		END
		SELECT @OrderID = min(ID) FROM tblOrder WHERE ID > @OrderID
	END
	SELECT * FROM #Orders
END

GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
/***********************************/
ALTER PROCEDURE [dbo].[spOrdersExportCustomer]
(
  @StartDate datetime = NULL
, @EndDate datetime = NULL
, @CustomerID int = -1 -- all carriers
, @ProductID int = -1 -- all products
, @BatchID int = NULL  -- either show unbatched (NULL), or the specified batch orders
) AS BEGIN
	SELECT @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	
	SELECT OE.* 
		, dbo.fnLocal_to_UTC(ISNULL(OIC.LastChangeDateUTC, OIC.CreateDateUTC), OE.OriginTimeZoneID, OE.OriginUseDST) AS RatesAppliedDate
		, SB.BatchNum
		, OIC.ChainupFee
		, OIC.RerouteFee
		, OIC.BillableWaitMinutes
		, OIC.WaitFeeSubUnitID
		, OIC.WaitFeeRoundingTypeID
		, OIC.WaitRate
		, OIC.WaitFee
		, OIC.RejectionFee
		, OIC.H2SRate
		, OIC.H2SFee
		, OIC.TaxRate
		, OIC.RouteRate
		, OIC.LoadFee
		, OIC.TotalFee
		, OIC.FuelSurcharge
		, C.MinSettlementBarrels
		, cast(CASE WHEN OE.TicketCount = 0 THEN OE.TicketType ELSE NULL END as varchar(max)) AS TicketTypes
		, cast(CASE WHEN OE.TicketCount = 0 THEN OE.CarrierTicketNum ELSE NULL END as varchar(max)) AS TicketNums
		, cast(NULL AS varchar(max)) AS TankNums
		, cast(NULL as varchar(max)) AS ProductObsTemps
		, cast(NULL as varchar(max)) AS ProductObsGravities
		, cast(NULL as varchar(max)) AS ProductBSWs
		, cast(NULL as varchar(max)) AS TicketsRejected
		, cast(NULL as varchar(max)) AS TicketsRejectNotes
		, cast(NULL as varchar(max)) AS TicketsGrossBBLS
		, cast(NULL as varchar(max)) AS TicketsNetBBLS
		, cast(NULL as varchar(max)) AS PreviousDestinations
		, cast(NULL as varchar(max)) AS RerouteUsers
		, cast(NULL as varchar(max)) AS RerouteDates
		, cast(NULL as varchar(max)) AS RerouteNotes
	INTO #Orders
	FROM dbo.viewOrderExportFull OE
	JOIN dbo.tblCustomer C ON C.ID = OE.CustomerID
	LEFT JOIN dbo.tblOrderInvoiceCustomer OIC ON OIC.OrderID = OE.ID
	LEFT JOIN dbo.tblCustomerSettlementBatch SB ON SB.ID = OIC.BatchID
	WHERE OE.StatusID IN (4)  
	  AND (@CustomerID=-1 OR OE.CustomerID=@CustomerID) 
	  AND (@ProductID=-1 OR OE.ProductID=@ProductID) 
	  AND (@StartDate IS NULL OR OE.OrderDate >= @StartDate) 
	  AND (@EndDate IS NULL OR OE.OrderDate <= @EndDate)
	  AND ((@BatchID IS NULL AND (OIC.ID IS NULL OR OIC.BatchID IS NULL)) OR OIC.BatchID = @BatchID)
	  AND OE.DeleteDateUTC IS NULL
	ORDER BY OE.OriginDepartTimeUTC
	
	DECLARE @OrderID int, @TicketID int, @RerouteID int
	SELECT @OrderID = min(ID) FROM #Orders WHERE ID > 0
	
	WHILE @OrderID IS NOT NULL
	BEGIN
		-- add the Tickets information to the order (done this way since there could be multiple)
		SELECT @TicketID = min(ID) FROM dbo.tblOrderTicket WHERE OrderID = @OrderID AND DeleteDateUTC IS NULL AND ID > 0
		WHILE @TicketID IS NOT NULL
		BEGIN
			UPDATE #Orders 
				SET TicketTypes = isnull(O.TicketTypes + '<br/>', '') + OT.TicketType
					, TicketNums = isnull(O.TicketNums + '<br/>', '') + OT.CarrierTicketNum
					, TankNums = isnull(O.TankNums + '<br/>', '') + OT.TankNum
					, ProductObsTemps = isnull(O.ProductObsTemps + '<br/>', '') + isnull(ltrim(OT.ProductObsTemp), '')
					, ProductObsGravities = isnull(O.ProductObsGravities + '<br/>', '') + isnull(ltrim(OT.ProductObsGravity), '')
					, ProductBSWs = isnull(O.ProductBSWs + '<br/>', '') + isnull(ltrim(OT.ProductBSW), '')
					, TicketsRejected = isnull(O.TicketsRejected + '<br/>', '') + (CASE WHEN OT.Rejected = 0 THEN 'Valid' ELSE 'Rejected' END)
					, TicketsRejectNotes = isnull(O.TicketsRejectNotes + '<br/>', '') + isnull(OT.RejectNotes, '')
					, TicketsGrossBBLS = isnull(O.TicketsGrossBBLS + '<br/>', '') + isnull(ltrim(GrossBarrels), '')
					, TicketsNetBBLS = isnull(O.TicketsNetBBLS + '<br/>', '') + isnull(ltrim(NetBarrels), '')
			FROM #Orders O, viewOrderTicket OT
			WHERE O.ID = @OrderID AND OT.ID = @TicketID
			SELECT @TicketID = min(ID) FROM tblOrderTicket WHERE OrderID = @OrderID AND ID > @TicketID AND DeleteDateUTC IS NULL
		END	
		-- add the Reroutes information to the order (done this way since there could be multiple)
		SELECT @RerouteID = min(ID) FROM dbo.tblOrderReroute WHERE OrderID = @OrderID AND ID > 0
		WHILE @RerouteID IS NOT NULL
		BEGIN
			UPDATE #Orders
				SET PreviousDestinations = isnull(O.PreviousDestinations + '<br/>', '') + O_R.PreviousDestinationFull
					, RerouteUsers = isnull(O.RerouteUsers + '<br/>', '') + O_R.UserName
					, RerouteDates = isnull(O.RerouteDates + '<br/>', '') + dbo.fnDateMdYY(O_R.RerouteDate)
					, RerouteNotes = isnull(O.RerouteNotes + '<br/>', '') + isnull(O_R.Notes, '')
			FROM #Orders O, viewOrderReroute O_R
			WHERE O.ID = @OrderID AND O_R.ID = @RerouteID
			SELECT @RerouteID = min(ID) FROM tblOrderReroute WHERE OrderID = @OrderID AND ID > @RerouteID
		END
		SELECT @OrderID = min(ID) FROM tblOrder WHERE ID > @OrderID
	END
	SELECT * FROM #Orders
END

GO

ALTER TABLE tblCarrierSettlementBatch ADD ProductID int NULL CONSTRAINT FK_CarrierSettlementBatch_Product FOREIGN KEY REFERENCES tblProduct(ID)
GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Carrier "Settlement" $$ values to an Audited order
/***********************************/
ALTER PROCEDURE [dbo].[spCreateCarrierSettlementBatch]
(
  @CarrierID int
, @ProductID int
, @Notes varchar(255)
, @UserName varchar(255)
, @BatchID int = NULL output
, @BatchNum int = NULL output
) AS BEGIN
	INSERT INTO dbo.tblCarrierSettlementBatch(CarrierID, ProductID, BatchNum, BatchDate, Notes, CreateDateUTC, CreatedByUser)
		SELECT @CarrierID, CASE WHEN @ProductID = -1 THEN NULL ELSE @ProductID END, isnull(max(BatchNum), 0) + 1
			, dbo.fnDateOnly(getdate()), @Notes, getutcdate(), @UserName 
		FROM dbo.tblCarrierSettlementBatch
		
		SELECT @BatchID = scope_identity()
		SELECT @BatchNum = BatchNum FROM dbo.tblCarrierSettlementBatch WHERE ID = @BatchID
END

GO

ALTER TABLE tblCustomerSettlementBatch ADD ProductID int NULL CONSTRAINT FK_CustomerSettlementBatch_Product FOREIGN KEY REFERENCES tblProduct(ID)
GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Customer "Settlement" $$ values to an Audited order
/***********************************/
ALTER PROCEDURE [dbo].[spCreateCustomerSettlementBatch]
(
  @CustomerID int
, @ProductID int
, @Notes varchar(255)
, @UserName varchar(255)
, @BatchID int = NULL output
, @BatchNum int = NULL output
) AS BEGIN
	INSERT INTO dbo.tblCustomerSettlementBatch(CustomerID, ProductID, BatchNum, BatchDate, Notes, CreateDateUTC, CreatedByUser)
		SELECT @CustomerID, CASE WHEN @ProductID = -1 THEN NULL ELSE @ProductID END, isnull(max(BatchNum), 0) + 1
			, dbo.fnDateOnly(getdate()), @Notes, getutcdate(), @UserName 
		FROM dbo.tblCustomerSettlementBatch
		
		SELECT @BatchID = scope_identity()
		SELECT @BatchNum = BatchNum FROM dbo.tblCustomerSettlementBatch WHERE ID = @BatchID
END

GO

/***********************************/
-- Date Created: 15 Aug 2013
-- Author: Kevin Alons
-- Purpose: return the CarrierSettlementBatch table contents with the CreateDate formatted DateOnly (for the BatchDate)
/***********************************/
ALTER VIEW [dbo].[viewCarrierSettlementBatch] 
AS
SELECT x.*
	, P.Name AS Product, P.ShortName AS ShortProduct
	, '#' + ltrim(BatchNum) + ' [' + dbo.fnDateMdYY(BatchDate) 
		+ '] ' + ltrim(OrderCount) + ' / ' + LTRIM(dbo.fnFormatWithCommas(round(isnull(GrossBarrels, 0), 0))) + 'BBLS' AS FullName
FROM (
	SELECT SB.*
		, CASE WHEN C.Active = 1 THEN '' ELSE 'Deleted: ' END + C.Name AS Carrier
		, (SELECT COUNT(*) FROM tblOrderInvoiceCarrier WHERE BatchID = SB.ID) AS OrderCount
		, (SELECT sum(O.OriginGrossBarrels) FROM tblOrderInvoiceCarrier OIC JOIN tblOrder O ON O.ID = OIC.OrderID WHERE BatchID = SB.ID) AS GrossBarrels
	FROM dbo.tblCarrierSettlementBatch SB
	JOIN dbo.viewCarrier C ON C.ID = SB.CarrierID
) x
LEFT JOIN dbo.tblProduct P ON P.ID = x.ProductID

GO

/***********************************/
-- Date Created: 15 Aug 2013
-- Author: Kevin Alons
-- Purpose: return the CustomerSettlementBatch table contents with the CreateDate formatted DateOnly (for the BatchDate)
/***********************************/
ALTER VIEW [dbo].[viewCustomerSettlementBatch] 
AS
SELECT x.*
	, P.Name AS Product, P.ShortName AS ShortProduct
	, '#' + ltrim(BatchNum) + ' [' + dbo.fnDateMdYY(BatchDate) 
		+ '] ' + ltrim(OrderCount) + ' / ' + LTRIM(dbo.fnFormatWithCommas(round(isnull(GrossBarrels, 0), 0))) + 'BBLS' AS FullName
FROM (
	SELECT SB.*
		, C.Name AS Customer
		, (SELECT COUNT(*) FROM tblOrderInvoiceCustomer WHERE BatchID = SB.ID) AS OrderCount
		, (SELECT sum(O.OriginGrossBarrels) FROM tblOrderInvoiceCustomer OIC JOIN tblOrder O ON O.ID = OIC.OrderID WHERE BatchID = SB.ID) AS GrossBarrels
	FROM dbo.tblCustomerSettlementBatch SB
	JOIN dbo.viewCustomer C ON C.ID = SB.CustomerID
) x
LEFT JOIN dbo.tblProduct P ON P.ID = x.ProductID

GO

EXEC _spRefreshAllViews
GO

COMMIT
SET NOEXEC OFF