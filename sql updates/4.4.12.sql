SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.4.11'
SELECT  @NewVersion = '4.4.12'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'JT-294 - Failure to fully import Shipper Rate Sheet import (changes)'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

exec _spDropIndex 'idxOrderSettlementCarrier_RangeRate_BatchID'
go
CREATE NONCLUSTERED INDEX idxOrderSettlementCarrier_RangeRate_BatchID ON [dbo].[tblOrderSettlementCarrier] ([RangeRateID],[BatchID])
GO

exec _spDropIndex 'idxOrderSettlementDriver_RangeRate_BatchID'
go
CREATE NONCLUSTERED INDEX idxOrderSettlementDriver_RangeRate_BatchID ON [dbo].[tblOrderSettlementDriver] ([RangeRateID],[BatchID])
GO

exec _spDropIndex 'idxOrderSettlementShipper_RangeRate_BatchID'
go
CREATE NONCLUSTERED INDEX idxOrderSettlementShipper_RangeRate_BatchID ON [dbo].[tblOrderSettlementShipper] ([RangeRateID],[BatchID])
GO

declare @ShipperID int = 0
	, @ProductGroupID int = 0
	, @TruckTypeID int = 0
	, @OriginStateID int = 0
	, @DestStateID int = 0
	, @RegionID int = 0
	, @ProducerID int = 0
	, @StartDate nvarchar(20) = '1/3/2017'
	, @EndDate nvarchar(4000) = null

SELECT *, ImportAction='None', spacer=NULL
FROM dbo.fnShipperRateSheetDisplay(isnull(@StartDate, getdate()), coalesce(@EndDate, @StartDate, getdate()), @ShipperID, @ProductGroupID, @TruckTypeID, @OriginStateID, @DestStateID, @RegionID, @ProducerID) ORDER BY Shipper, EffectiveDate DESC

go

COMMIT
SET NOEXEC OFF