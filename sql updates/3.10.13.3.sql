SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.10.13.2'
SELECT  @NewVersion = '3.10.13.3'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Truck Type - Update settlement views'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


-- PART 4
-- UPDATE THE CORE SETTLEMENT VIEWS
--------------------------------------------------------------------------------------------

-- UPDATE CARRIER VIEWS

/***********************************/
-- Date Created: 20 Sep 2014
-- Author: Kevin Alons
-- Purpose: return Carrier metrics for all Order Statuses
-- Changes:
--		3.10.13.3 - 2016/02/29 - JAE - Added Truck Type
/***********************************/
ALTER VIEW [dbo].[viewCarrier_RouteMetrics] AS
	SELECT O.CarrierID
		, O.RouteID
		, O.ProductGroupID
		, O.TruckTypeID
		, O.OriginID, O.DestinationID
		, OrderCount = COUNT(1)
		, MinOrderDate = cast(MIN(O.OriginDepartTimeUTC) as date)
		, O.StatusID
		, IsSettled = CASE WHEN ISC.BatchID IS NULL THEN 0 ELSE 1 END
	FROM viewOrder O
	LEFT JOIN tblOrderSettlementCarrier ISC ON ISC.OrderID = O.ID
	WHERE DeleteDateUTC IS NULL 
		AND RouteID IS NOT NULL 
		AND CarrierID IS NOT NULL
	GROUP BY CarrierID, RouteID, ProductGroupID, TruckTypeID
		, O.OriginID, O.DestinationID
		, StatusID, CASE WHEN ISC.BatchID IS NULL THEN 0 ELSE 1 END


GO


/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all CarrierAssessorialRate records
-- Changes:
	- 3.7.28 - 2015/06/18 - KDA - add new columns: move ProducerID column to end (so consistent with all other Best-Match rates/parameters
								- add DriverID/DriverGroup columns
	- 3.7.30 - 2015/06/19 - KDA - remove DriverID
	- 3.10.13.3 - 2016/02/29 - JAE - Added Truck Type
******************************************************/
ALTER VIEW [dbo].[viewCarrierAssessorialRate] AS
	SELECT X.*
		, MaxEffectiveDate = (SELECT min(OrderDate) FROM viewOrderSettlementCarrierAssessorialCharge WHERE AssessorialRateID = X.ID AND BatchID IS NOT NULL)
		, MinEndDate = (SELECT max(OrderDate) FROM viewOrderSettlementCarrierAssessorialCharge WHERE AssessorialRateID = X.ID AND BatchID IS NOT NULL)
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM viewOrderSettlementCarrierAssessorialCharge WHERE AssessorialRateID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblCarrierAssessorialRate XN 
			WHERE XN.TypeID = X.TypeID
			  AND dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EffectiveDate > X.EffectiveDate)
		, PriorEndDate = (
			SELECT max(XN.EndDate) 
			FROM tblCarrierAssessorialRate XN 
			WHERE XN.TypeID = X.TypeID
			  AND dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EndDate < X.EffectiveDate)
	FROM tblCarrierAssessorialRate X


GO


/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all CarrierDestinationWaitRate records
-- Changes:
	- 3.7.28 - 2015/06/18 - KDA - Add DriverID | DriverGroupID | ProducerID
	- 3.7.30 - 2015/06/19 - KDA - remove DriverID
	- 3.10.13.3 - 2016/02/29 - JAE - Added Truck Type
******************************************************/
ALTER VIEW [dbo].[viewCarrierDestinationWaitRate] AS
	SELECT X.*
		, MaxEffectiveDate = (SELECT Min(OrderDate) FROM tblOrderSettlementCarrier WHERE DestinationWaitRateID = X.ID AND BatchID IS NOT NULL)
		, MinEndDate = (SELECT Max(OrderDate) FROM tblOrderSettlementCarrier WHERE DestinationWaitRateID = X.ID AND BatchID IS NOT NULL)
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementCarrier WHERE DestinationWaitRateID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate)
			FROM tblCarrierDestinationWaitRate XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(XN.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EffectiveDate > X.EffectiveDate)
		, PriorEndDate = (
			SELECT max(XN.EndDate)
			FROM tblCarrierDestinationWaitRate XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(XN.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EndDate < X.EffectiveDate)
	FROM tblCarrierDestinationWaitRate X


GO


/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all CarrierFuelSurcharge records
	- 3.7.28 - 2015/06/18 - KDA - add ProducerID|DriverID|DriverGroupID
	- 3.7.30 - 2015/06/19 - KDA - remove DriverID
	- 3.10.13.3 - 2016/02/29 - JAE - Added Truck Type
******************************************************/
ALTER VIEW [dbo].[viewCarrierFuelSurchargeRate] AS
	SELECT X.*
		, MaxEffectiveDate = (SELECT min(OrderDate) FROM tblOrderSettlementCarrier WHERE FuelSurchargeRateID = X.ID AND BatchID IS NOT NULL)
		, MinEndDate = (SELECT max(OrderDate) FROM tblOrderSettlementCarrier WHERE FuelSurchargeRateID = X.ID AND BatchID IS NOT NULL)
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementCarrier WHERE FuelSurchargeRateID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate)
			FROM tblCarrierDestinationWaitRate XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverGroupID, X.DriverGroupID) = 1
			  AND XN.EffectiveDate > X.EffectiveDate)
		, PriorEndDate = (
			SELECT max(XN.EndDate)
			FROM tblCarrierDestinationWaitRate XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverGroupID, X.DriverGroupID) = 1
			  AND XN.EndDate < X.EffectiveDate)
	FROM tblCarrierFuelSurchargeRate X


GO


/******************************************************
-- Date Created: 12 June 2015
-- Author: Kevin Alons
-- Purpose: add a computed fields to the CarrierMinSettlementUnits table data
-- Changes: 
	- 3.7.28 - 2015/06/18 - KDA - ADDED
	- 3.10.13.3 - 2016/02/29 - JAE - Added Truck Type
******************************************************/
ALTER VIEW [dbo].[viewCarrierMinSettlementUnits] AS
	SELECT X.*
		, MaxEffectiveDate = (SELECT MIN(OrderDate) FROM tblOrderSettlementCarrier WHERE MinSettlementUnitsID = X.ID AND BatchID IS NOT NULL)
		, MinEndDate = (SELECT MAX(OrderDate) FROM tblOrderSettlementCarrier WHERE MinSettlementUnitsID = X.ID AND BatchID IS NOT NULL)
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementCarrier WHERE MinSettlementUnitsID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblCarrierMinSettlementUnits XN 
			WHERE isnull(XN.ShipperID, 0) = isnull(X.ShipperID, 0) 
			  AND isnull(XN.CarrierID, 0) = isnull(X.CarrierID, 0) 
			  AND isnull(XN.ProductGroupID, 0) = isnull(X.ProductGroupID, 0) 
			  AND isnull(XN.TruckTypeID, 0) = isnull(X.TruckTypeID, 0)
			  AND isnull(XN.OriginID, 0) = isnull(X.OriginID, 0) 
			  AND isnull(XN.OriginStateID, 0) = isnull(X.OriginStateID, 0) 
			  AND isnull(XN.DestinationID, 0) = isnull(X.DestinationID, 0) 
			  AND isnull(XN.DestinationStateID, 0) = isnull(X.DestinationStateID, 0) 
			  AND isnull(XN.ProducerID, 0) = isnull(X.ProducerID, 0) 
			  AND XN.EffectiveDate > X.EffectiveDate)
		, PriorEndDate = (
			SELECT max(XN.EndDate) 
			FROM tblCarrierMinSettlementUnits XN 
			WHERE isnull(XN.ShipperID, 0) = isnull(X.ShipperID, 0) 
			  AND isnull(XN.CarrierID, 0) = isnull(X.CarrierID, 0) 
			  AND isnull(XN.ProductGroupID, 0) = isnull(X.ProductGroupID, 0) 
			  AND isnull(XN.TruckTypeID, 0) = isnull(X.TruckTypeID, 0)
			  AND isnull(XN.OriginID, 0) = isnull(X.OriginID, 0) 
			  AND isnull(XN.OriginStateID, 0) = isnull(X.OriginStateID, 0) 
			  AND isnull(XN.DestinationID, 0) = isnull(X.DestinationID, 0) 
			  AND isnull(XN.DestinationStateID, 0) = isnull(X.DestinationStateID, 0) 
			  AND isnull(XN.ProducerID, 0) = isnull(X.ProducerID, 0) 
			  AND XN.EndDate < X.EffectiveDate)
	FROM tblCarrierMinSettlementUnits X


GO


/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all CarrierOrderRejectRate records
-- Changes:
	- 3.7.28 - 2015/06/18 - KDA - Add DriverID | DriverGroupID | ProducerID
	- 3.7.30 - 2015/06/19 - KDA - remove DriverID
	- 3.10.13.3 - 2016/02/29 - JAE - Added Truck Type
******************************************************/
ALTER VIEW [dbo].[viewCarrierOrderRejectRate] AS
	SELECT X.*
		, MaxEffectiveDate = (SELECT MIN(OrderDate) FROM tblOrderSettlementCarrier WHERE OrderRejectRateID = X.ID AND BatchID IS NOT NULL)
		, MinEndDate = (SELECT MAX(OrderDate) FROM tblOrderSettlementCarrier WHERE OrderRejectRateID = X.ID AND BatchID IS NOT NULL)
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementCarrier WHERE OrderRejectRateID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblCarrierOrderRejectRate XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(XN.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EffectiveDate > X.EffectiveDate)
		, PriorEndDate = (
			SELECT max(XN.EndDate) 
			FROM tblCarrierOrderRejectRate XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(XN.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EndDate < X.EffectiveDate)
	FROM tblCarrierOrderRejectRate X


GO


/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all CarrierOriginWaitRate records
-- Changes:
	- 3.7.28 - 2015/06/18 - KDA - Add DriverID | DriverGroupID | ProducerID
	- 3.7.30 - 2015/06/19 - KDA - remove DriverID
	- 3.10.13.3 - 2016/02/29 - JAE - Added Truck Type
******************************************************/
ALTER VIEW [dbo].[viewCarrierOriginWaitRate] AS
	SELECT X.*
		, MaxEffectiveDate = (SELECT MIN(OrderDate) FROM tblOrderSettlementCarrier WHERE OriginWaitRateID = X.ID AND BatchID IS NOT NULL)
		, MinEndDate = (SELECT Max(OrderDate) FROM tblOrderSettlementCarrier WHERE OriginWaitRateID = X.ID AND BatchID IS NOT NULL)
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementCarrier WHERE OriginWaitRateID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate)
			FROM tblCarrierOriginWaitRate XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(XN.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EffectiveDate > X.EffectiveDate)
		, PriorEndDate = (
			SELECT max(XN.EndDate)
			FROM tblCarrierOriginWaitRate XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(XN.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EndDate < X.EffectiveDate)
	FROM tblCarrierOriginWaitRate X


GO


/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all CarrierRateSheet records
-- Changes:
	- 3.7.28 - 2015/06/18 - KDA - Add DriverID | DriverGroupID | ProducerID
	- 3.7.30 - 2015/06/19 - KDA - remove DriverID
	- 3.10.13.3 - 2016/02/29 - JAE - Added Truck Type
******************************************************/
ALTER VIEW [dbo].[viewCarrierRateSheet] AS
	SELECT X.*
		, MaxEffectiveDate = (SELECT min(OrderDate) FROM tblOrderSettlementCarrier SC JOIN tblCarrierRangeRate RR ON RR.ID = SC.RangeRateID WHERE RR.RateSheetID = X.ID AND SC.BatchID IS NOT NULL)
		, MinEndDate = (SELECT max(OrderDate) FROM tblOrderSettlementCarrier SC JOIN tblCarrierRangeRate RR ON RR.ID = SC.RangeRateID WHERE RR.RateSheetID = X.ID AND SC.BatchID IS NOT NULL)
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementCarrier SC JOIN tblCarrierRangeRate RR ON RR.ID = SC.RangeRateID WHERE RR.RateSheetID = X.ID AND SC.BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblCarrierRateSheet XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EffectiveDate > X.EffectiveDate)
		, PriorEndDate = (
			SELECT max(XN.EndDate) 
			FROM tblCarrierRateSheet XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EndDate < X.EffectiveDate)
	FROM tblCarrierRateSheet X


GO


/************************************************
-- Date Created: 18 Jan 2015
-- Author: Kevin Alons
-- Purpose: return combined RateSheet + RangeRate data + friendly translated values
			allow updating of both RateSheet & RangeRate data together
-- Changes:
	- 3.7.28 - 2015/06/18 - KDA - Add DriverID | DriverGroupID | ProducerID
	- 3.7.30 - 2015/06/19 - KDA - remove DriverID
	- 3.10.13.3 - 2016/02/29 - JAE - Added Truck Type
************************************************/
ALTER VIEW [dbo].[viewCarrierRateSheetRangeRate] AS
	SELECT RR.ID, RateSheetID = R.ID, R.ShipperID, R.CarrierID, R.ProductGroupID, R.TruckTypeID, R.DriverGroupID, R.OriginStateID, R.DestStateID, R.RegionID, R.ProducerID, RR.Rate, RR.MinRange, RR.MaxRange, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate, R.NextEffectiveDate
		, R.Locked
		, RR.CreateDateUTC, RR.CreatedByUser
		, RR.LastChangeDateUTC, RR.LastChangedByUser
	FROM dbo.viewCarrierRateSheet R
	JOIN dbo.tblCarrierRangeRate RR ON RR.RateSheetID = R.ID

GO


/***************************************************
-- Date Created: 6 May 2015
-- Author: Kevin Alons
-- Purpose: return the RateSheet JOIN RangeRate data with translated "friendly" values
-- Changes:
	- 3.7.28 - 2015/06/18 - KDA - Add DriverID | DriverGroupID | ProducerID
	- 3.7.30 - 2015/06/19 - KDA - remove DriverID
	- 3.10.13.3 - 2016/02/29 - JAE - Added Truck Type
***************************************************/
ALTER VIEW [dbo].[viewCarrierRateSheetRangeRatesDisplay] AS
	SELECT R.*
		, Shipper = SH.Name
		, Carrier = C.Name
		, ProductGroup = PG.Name
		, TruckType = TT.Name
		, DriverGroup = DG.Name
		, OriginState = OS.FullName
		, OriginStateAbbrev = OS.Abbreviation
		, DestinationState = DS.FullName
		, DestinationStateAbbrev = DS.Abbreviation
		, Region = REG.Name
		, Producer = P.Name
		, RateType = RT.Name
		, Uom = U.Name
		, UomShort = U.Abbrev
	FROM viewCarrierRateSheetRangeRate R
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblCarrier C ON C.ID = R.CarrierID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblTruckType TT ON TT.ID = R.TruckTypeID
	LEFT JOIN tblDriverGroup DG ON DG.ID = R.DriverGroupID
	LEFT JOIN tblState OS ON OS.ID = R.OriginStateID
	LEFT JOIN tblState DS ON DS.ID = R.DestStateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblProducer P ON P.ID = R.ProducerID
	LEFT JOIN tblRateType RT ON RT.ID = R.RateTypeID
	LEFT JOIN tblUom U ON U.ID = R.UomID

GO


/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all CarrierRouteRate records
-- Changes:
	- 3.7.28 - 2015/06/18 - KDA - ADDED
	- 3.7.30 - 2015/06/19 - KDA - remvoe DriverID
	- 3.10.13.3 - 2016/02/29 - JAE - Added Truck Type
******************************************************/
ALTER VIEW [dbo].[viewCarrierRouteRate] AS
	SELECT X.*
		, MaxEffectiveDate = (SELECT MIN(OrderDate) FROM tblOrderSettlementCarrier WHERE RouteRateID = X.ID AND BatchID IS NOT NULL)
		, MinEndDate = (SELECT MAX(OrderDate) FROM tblOrderSettlementCarrier WHERE RouteRateID = X.ID AND BatchID IS NOT NULL)
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementCarrier WHERE RouteRateID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, R.OriginID
		, R.DestinationID
		, R.ActualMiles
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblCarrierRouteRate XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverGroupID, X.DriverGroupID) = 1
			  AND XN.RouteID = X.RouteID
			  AND XN.EffectiveDate > X.EffectiveDate)
		, PriorEndDate = (
			SELECT min(XN.EndDate) 
			FROM tblCarrierRouteRate XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(XN.DriverGroupID, X.DriverGroupID) = 1
			  AND XN.RouteID = X.RouteID
			  AND XN.EffectiveDate < X.EffectiveDate)
	FROM tblCarrierRouteRate X
	JOIN tblRoute R ON R.ID = X.RouteID

GO


/******************************************************
-- Date Created: 12 June 2015
-- Author: Kevin Alons
-- Purpose: add a computed fields to the CarrierSettlementFactor table data
-- Changes: 
	- 3.7.28 - 2015/06/18 - KDA - ADDED
	- 3.10.13.3 - 2016/02/29 - JAE - Added Truck Type
******************************************************/
ALTER VIEW [dbo].[viewCarrierSettlementFactor] AS
	SELECT X.*
		, MaxEffectiveDate = (SELECT MIN(OrderDate) FROM tblOrderSettlementCarrier WHERE CarrierSettlementFactorID = X.ID AND BatchID IS NOT NULL)
		, MinEndDate = (SELECT MAX(OrderDate) FROM tblOrderSettlementCarrier WHERE CarrierSettlementFactorID = X.ID AND BatchID IS NOT NULL)
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementCarrier WHERE CarrierSettlementFactorID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblCarrierSettlementFactor XN 
			WHERE isnull(XN.ShipperID, 0) = isnull(X.ShipperID, 0) 
			  AND isnull(XN.CarrierID, 0) = isnull(X.CarrierID, 0) 
			  AND isnull(XN.ProductGroupID, 0) = isnull(X.ProductGroupID, 0) 
			  AND isnull(XN.TruckTypeID, 0) = isnull(X.TruckTypeID, 0)
			  AND isnull(XN.OriginID, 0) = isnull(X.OriginID, 0) 
			  AND isnull(XN.OriginStateID, 0) = isnull(X.OriginStateID, 0) 
			  AND isnull(XN.DestinationID, 0) = isnull(X.DestinationID, 0) 
			  AND isnull(XN.DestinationStateID, 0) = isnull(X.DestinationStateID, 0) 
			  AND isnull(XN.ProducerID, 0) = isnull(X.ProducerID, 0) 
			  AND XN.EffectiveDate > X.EffectiveDate)
		, PriorEndDate = (
			SELECT max(XN.EndDate) 
			FROM tblCarrierSettlementFactor XN 
			WHERE isnull(XN.ShipperID, 0) = isnull(X.ShipperID, 0) 
			  AND isnull(XN.CarrierID, 0) = isnull(X.CarrierID, 0) 
			  AND isnull(XN.ProductGroupID, 0) = isnull(X.ProductGroupID, 0) 
			  AND isnull(XN.TruckTypeID, 0) = isnull(X.TruckTypeID, 0)
			  AND isnull(XN.OriginID, 0) = isnull(X.OriginID, 0) 
			  AND isnull(XN.OriginStateID, 0) = isnull(X.OriginStateID, 0) 
			  AND isnull(XN.DestinationID, 0) = isnull(X.DestinationID, 0) 
			  AND isnull(XN.DestinationStateID, 0) = isnull(X.DestinationStateID, 0) 
			  AND isnull(XN.ProducerID, 0) = isnull(X.ProducerID, 0) 
			  AND XN.EndDate < X.EffectiveDate)
	FROM tblCarrierSettlementFactor X


GO


/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all CarrierWaitFeeParameter records
-- Changes:
	- 3.7.28 - 2015/06/18 - KDA - ADDED
	- 3.7.30 - 2015/06/19 - KDA - remvoe DriverID
	- 3.10.13.3 - 2016/02/29 - JAE - Added Truck Type
******************************************************/
ALTER VIEW [dbo].[viewCarrierWaitFeeParameter] AS
	SELECT X.*
		, MaxEffectiveDate = (SELECT MIN(OrderDate) FROM tblOrderSettlementCarrier WHERE WaitFeeParameterID = X.ID AND BatchID IS NOT NULL)
		, MinEndDate = (SELECT MAX(OrderDate) FROM tblOrderSettlementCarrier WHERE WaitFeeParameterID = X.ID AND BatchID IS NOT NULL)
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementCarrier WHERE WaitFeeParameterID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblCarrierWaitFeeParameter XN
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EffectiveDate > X.EffectiveDate)
		, PriorEndDate = (
			SELECT max(XN.EndDate) 
			FROM tblCarrierWaitFeeParameter XN
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EndDate < X.EffectiveDate)
	FROM tblCarrierWaitFeeParameter X


GO


-- UPDATE SHIPPER VIEWS

/***********************************/
-- Date Created: 20 Sep 2014
-- Author: Kevin Alons
-- Purpose: return Shipper metrics for all Order Statuses
-- Changes:
--		 3.10.13.3 - 2016/02/29 - JAE - Added Truck Type
/***********************************/
ALTER VIEW [dbo].[viewShipper_RouteMetrics] AS
	SELECT O.CustomerID
		, O.RouteID
		, O.ProductGroupID
		, O.TruckTypeID
		, O.OriginID, O.DestinationID
		, OrderCount = COUNT(1)
		, MinOrderDate = cast(MIN(O.OriginDepartTimeUTC) as date)
		, O.StatusID
		, IsSettled = CASE WHEN ISC.BatchID IS NULL THEN 0 ELSE 1 END
	FROM viewOrder O
	LEFT JOIN tblOrderSettlementShipper ISC ON ISC.OrderID = O.ID
	WHERE DeleteDateUTC IS NULL 
		AND RouteID IS NOT NULL 
		AND CustomerID IS NOT NULL
	GROUP BY CustomerID, RouteID, ProductGroupID, TruckTypeID
		, O.OriginID, O.DestinationID
		, StatusID, CASE WHEN ISC.BatchID IS NULL THEN 0 ELSE 1 END


GO


/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all ShipperAssessorialRate records
-- Changes:
	- 3.7.29 - 2015/06/18 - KDA - move ProducerID parameter/field to end of criteria to be consistent with other "Best-Match" tables
	- 3.10.13.3 - 2016/02/29 - JAE - Added Truck Type
******************************************************/
ALTER VIEW [dbo].[viewShipperAssessorialRate] AS
	SELECT X.*
		, MaxEffectiveDate = (SELECT MIN(OrderDate) FROM viewOrderSettlementShipperAssessorialCharge WHERE AssessorialRateID = X.ID AND BatchID IS NOT NULL)
		, MinEndDate = (SELECT MAX(OrderDate) FROM viewOrderSettlementShipperAssessorialCharge WHERE AssessorialRateID = X.ID AND BatchID IS NOT NULL)
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM viewOrderSettlementShipperAssessorialCharge WHERE AssessorialRateID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblShipperAssessorialRate XN 
			WHERE XN.TypeID = X.TypeID
			  AND dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EffectiveDate > X.EffectiveDate)
		, PriorEndDate = (
			SELECT max(XN.EndDate) 
			FROM tblShipperAssessorialRate XN 
			WHERE XN.TypeID = X.TypeID
			  AND dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EndDate < X.EffectiveDate)
	FROM tblShipperAssessorialRate X


GO


/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all ShipperDestinationWaitRate records
-- Changes:
	- 3.7.28 - 2015/06/19 - KDA - add ProducerID criteria
	- 3.7.30 - 2015/06/19 - KDA - convert to use of fnCompareNullableInts()
	- 3.10.13.3 - 2016/02/29 - JAE - Added Truck Type
******************************************************/
ALTER VIEW [dbo].[viewShipperDestinationWaitRate] AS
	SELECT X.*
		, MaxEffectiveDate = (SELECT MIN(OrderDate) FROM tblOrderSettlementShipper WHERE DestinationWaitRateID = X.ID AND BatchID IS NOT NULL)
		, MinEndDate = (SELECT MAX(OrderDate) FROM tblOrderSettlementShipper WHERE DestinationWaitRateID = X.ID AND BatchID IS NOT NULL)
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementShipper WHERE DestinationWaitRateID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate)
			FROM tblShipperDestinationWaitRate XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(XN.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EffectiveDate > X.EffectiveDate)
		, PriorEndDate = (
			SELECT max(XN.EndDate)
			FROM tblShipperDestinationWaitRate XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(XN.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EndDate < X.EffectiveDate)
	FROM tblShipperDestinationWaitRate X

GO


/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all ShipperFuelSurcharge records
-- Changes:
	- 3.7.28 - 2015/06/19 - KDA - add ProducerID criteria
	- 3.7.30 - 2015/06/19 - KDA - convert to use of fnCompareNullableInts()
	- 3.10.13.3 - 2016/02/29 - JAE - Added Truck Type
******************************************************/
ALTER VIEW [dbo].[viewShipperFuelSurchargeRate] AS
	SELECT X.*
		, MaxEffectiveDate = (SELECT MIN(OrderDate) FROM tblOrderSettlementShipper WHERE FuelSurchargeRateID = X.ID AND BatchID IS NOT NULL)
		, MinEndDate = (SELECT MAX(OrderDate) FROM tblOrderSettlementShipper WHERE FuelSurchargeRateID = X.ID AND BatchID IS NOT NULL)
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementShipper WHERE FuelSurchargeRateID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate)
			FROM tblShipperDestinationWaitRate XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.TruckTypeID, X.TruckTypeID) = 1
			  AND XN.EffectiveDate > X.EffectiveDate)
		, PriorEndDate = (
			SELECT max(XN.EndDate)
			FROM tblShipperDestinationWaitRate XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.TruckTypeID, X.TruckTypeID) = 1
			  AND XN.EndDate < X.EffectiveDate)
	FROM tblShipperFuelSurchargeRate X


GO


/******************************************************
-- Date Created: 12 June 2015
-- Author: Kevin Alons
-- Purpose: add a computed fields to the ShipperMinSettlementUnits table data
-- Changes:
	- 3.7.29 - 2015/06/18 - KDA - ADDED
	- 3.10.13.3 - 2016/02/29 - JAE - Added Truck Type
******************************************************/
ALTER VIEW [dbo].[viewShipperMinSettlementUnits] AS
	SELECT X.*
		, MaxEffectiveDate = (SELECT MIN(OrderDate) FROM tblOrderSettlementShipper WHERE MinSettlementUnitsID = X.ID AND BatchID IS NOT NULL)
		, MinEndDate = (SELECT MAX(OrderDate) FROM tblOrderSettlementShipper WHERE MinSettlementUnitsID = X.ID AND BatchID IS NOT NULL)
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementShipper WHERE MinSettlementUnitsID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblShipperMinSettlementUnits XN 
			WHERE isnull(XN.ShipperID, 0) = isnull(X.ShipperID, 0) 
			  AND isnull(XN.ProductGroupID, 0) = isnull(X.ProductGroupID, 0) 
			  AND isnull(XN.TruckTypeID, 0) = isnull(X.TruckTypeID, 0)
			  AND isnull(XN.OriginID, 0) = isnull(X.OriginID, 0) 
			  AND isnull(XN.OriginStateID, 0) = isnull(X.OriginStateID, 0) 
			  AND isnull(XN.DestinationID, 0) = isnull(X.DestinationID, 0) 
			  AND isnull(XN.DestinationStateID, 0) = isnull(X.DestinationStateID, 0) 
			  AND isnull(XN.ProducerID, 0) = isnull(X.ProducerID, 0) 
			  AND XN.EffectiveDate > X.EffectiveDate)
		, PriorEndDate = (
			SELECT max(XN.EndDate) 
			FROM tblShipperMinSettlementUnits XN 
			WHERE isnull(XN.ShipperID, 0) = isnull(X.ShipperID, 0) 
			  AND isnull(XN.ProductGroupID, 0) = isnull(X.ProductGroupID, 0) 
			  AND isnull(XN.TruckTypeID, 0) = isnull(X.TruckTypeID, 0)
			  AND isnull(XN.OriginID, 0) = isnull(X.OriginID, 0) 
			  AND isnull(XN.OriginStateID, 0) = isnull(X.OriginStateID, 0) 
			  AND isnull(XN.DestinationID, 0) = isnull(X.DestinationID, 0) 
			  AND isnull(XN.DestinationStateID, 0) = isnull(X.DestinationStateID, 0) 
			  AND isnull(XN.ProducerID, 0) = isnull(X.ProducerID, 0) 
			  AND XN.EndDate < X.EffectiveDate)
	FROM tblShipperMinSettlementUnits X


GO


/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all ShipperOrderRejectRate records
-- Changes:
	- 3.7.28 - 2015/06/19 - KDA - add ProducerID criteria
	- 3.7.30 - 2015/06/19 - KDA - convert to use of fnCompareNullableInts()
	- 3.10.13.3 - 2016/02/29 - JAE - Added Truck Type
******************************************************/
ALTER VIEW [dbo].[viewShipperOrderRejectRate] AS
	SELECT X.*
		, MaxEffectiveDate = (SELECT MIN(OrderDate) FROM tblOrderSettlementShipper WHERE OrderRejectRateID = X.ID AND BatchID IS NOT NULL)
		, MinEndDate = (SELECT MAX(OrderDate) FROM tblOrderSettlementShipper WHERE OrderRejectRateID = X.ID AND BatchID IS NOT NULL)
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementShipper WHERE OrderRejectRateID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblShipperOrderRejectRate XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(XN.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EffectiveDate > X.EffectiveDate)
		, PriorEndDate = (
			SELECT max(XN.EndDate) 
			FROM tblShipperOrderRejectRate XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(XN.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EndDate < X.EffectiveDate)
	FROM tblShipperOrderRejectRate X


GO


/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all ShipperOriginWaitRate records
-- Changes:
	- 3.7.28 - 2015/06/19 - KDA - add ProducerID criteria
	- 3.7.30 - 2015/06/19 - KDA - convert to use of fnCompareNullableInts()
	- 3.10.13.3 - 2016/02/29 - JAE - Added Truck Type
******************************************************/
ALTER VIEW [dbo].[viewShipperOriginWaitRate] AS
	SELECT X.*
		, MaxEffectiveDate = (SELECT MIN(OrderDate) FROM tblOrderSettlementShipper WHERE OriginWaitRateID = X.ID AND BatchID IS NOT NULL)
		, MinEndDate = (SELECT MAX(OrderDate) FROM tblOrderSettlementShipper WHERE OriginWaitRateID = X.ID AND BatchID IS NOT NULL)
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementShipper WHERE OriginWaitRateID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblShipperOriginWaitRate XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(XN.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EffectiveDate > X.EffectiveDate)
		, PriorEndDate = (
			SELECT max(XN.EndDate) 
			FROM tblShipperOriginWaitRate XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(XN.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EndDate < X.EffectiveDate)
	FROM tblShipperOriginWaitRate X


GO


/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all ShipperRateSheet records
-- Changes:
	- 3.7.28 - 2015/06/19 - KDA - add ProducerID criteria
	- 3.7.30 - 2015/06/19 - KDA - convert to use of fnCompareNullableInts()
	- 3.10.13.3 - 2016/02/29 - JAE - Added Truck Type
******************************************************/
ALTER VIEW [dbo].[viewShipperRateSheet] AS
	SELECT X.*
		, MaxEffectiveDate = (SELECT MIN(OrderDate) FROM tblOrderSettlementShipper SC JOIN tblShipperRangeRate RR ON RR.ID = SC.RangeRateID WHERE RR.RateSheetID = X.ID AND SC.BatchID IS NOT NULL)
		, MinEndDate = (SELECT MAX(OrderDate) FROM tblOrderSettlementShipper SC JOIN tblShipperRangeRate RR ON RR.ID = SC.RangeRateID WHERE RR.RateSheetID = X.ID AND SC.BatchID IS NOT NULL)
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementShipper SC JOIN tblShipperRangeRate RR ON RR.ID = SC.RangeRateID WHERE RR.RateSheetID = X.ID AND SC.BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblShipperRateSheet XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EffectiveDate > X.EffectiveDate)
		, PriorEndDate = (
			SELECT max(XN.EndDate) 
			FROM tblShipperRateSheet XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EndDate < X.EffectiveDate)
	FROM tblShipperRateSheet X


GO


/************************************************
-- Date Created: 18 Jan 2015
-- Author: Kevin Alons
-- Purpose: return combined RateSheet + RangeRate data + friendly translated values
			allow updating of both RateSheet & RangeRate data together
-- Changes:
	- 3.7.28 - 2015/06/19 - KDA - add ProducerID criteria
	- 3.7.30 - 2015/06/19 - KDA - remove "Display" fields
	- 3.10.13.3 - 2016/02/29 - JAE - Added Truck Type
************************************************/
ALTER VIEW [dbo].[viewShipperRateSheetRangeRate] AS
	SELECT RR.ID, RateSheetID = R.ID, R.ShipperID, R.ProductGroupID, R.TruckTypeID, R.OriginStateID, R.DestStateID, R.RegionID, R.ProducerID, RR.Rate, RR.MinRange, RR.MaxRange, R.RateTypeID, R.UomID, R.EffectiveDate, R.EndDate, R.NextEffectiveDate
		, R.Locked
		, RR.CreateDateUTC, RR.CreatedByUser
		, RR.LastChangeDateUTC, RR.LastChangedByUser
	FROM dbo.viewShipperRateSheet R
	JOIN dbo.tblShipperRangeRate RR ON RR.RateSheetID = R.ID

GO


/***************************************************
-- Date Created: 6 May 2015
-- Author: Kevin Alons
-- Purpose: return the RateSheet JOIN RangeRate data with translated "friendly" values
-- Changes:
	- 3.7.28 - 2015/06/19 - KDA - add ProducerID criteria
	- 3.7.30 - 2015/06/19 - KDA - cleanup
	- 3.10.13.3 - 2016/02/29 - JAE - Added Truck Type
***************************************************/
ALTER VIEW [dbo].[viewShipperRateSheetRangeRatesDisplay] AS
	SELECT R.*
		, Shipper = SH.Name
		, ProductGroup = PG.Name
		, TruckType = TT.Name
		, OriginState = OS.FullName
		, OriginStateAbbrev = OS.Abbreviation
		, DestinationState = DS.FullName
		, DestinationStateAbbrev = DS.Abbreviation
		, Region = REG.Name
		, Producer = P.Name
		, RateType = RT.Name
		, Uom = U.Name
		, UomShort = U.Abbrev
	FROM viewShipperRateSheetRangeRate R
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblTruckType TT ON TT.ID = R.TruckTypeID
	LEFT JOIN tblState OS ON OS.ID = R.OriginStateID
	LEFT JOIN tblState DS ON DS.ID = R.DestStateID
	LEFT JOIN tblRegion REG ON REG.ID = R.RegionID
	LEFT JOIN tblProducer P ON P.ID = R.ProducerID
	LEFT JOIN tblRateType RT ON RT.ID = R.RateTypeID
	LEFT JOIN tblUom U ON U.ID = R.UomID

GO


/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all ShipperRouteRate records
-- Changes:
	- 3.10.13.3 - 2016/02/29 - JAE - Added Truck Type
******************************************************/
ALTER VIEW [dbo].[viewShipperRouteRate] AS
	SELECT X.*
		, MaxEffectiveDate = (SELECT MIN(OrderDate) FROM tblOrderSettlementShipper WHERE RouteRateID = X.ID AND BatchID IS NOT NULL)
		, MinEndDate = (SELECT MAX(OrderDate) FROM tblOrderSettlementShipper WHERE RouteRateID = X.ID AND BatchID IS NOT NULL)
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementShipper WHERE RouteRateID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, R.OriginID
		, R.DestinationID
		, R.ActualMiles
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblShipperRouteRate XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.TruckTypeID, X.TruckTypeID) = 1
			  AND XN.RouteID = X.RouteID
			  AND XN.EffectiveDate > X.EffectiveDate)
		, PriorEndDate = (
			SELECT max(XN.EndDate) 
			FROM tblShipperRouteRate XN 
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.TruckTypeID, X.TruckTypeID) = 1
			  AND XN.RouteID = X.RouteID
			  AND XN.EndDate < X.EffectiveDate)
	FROM tblShipperRouteRate X
	JOIN tblRoute R ON R.ID = X.RouteID

GO


/******************************************************
-- Date Created: 12 June 2015
-- Author: Kevin Alons
-- Purpose: add a computed fields to the ShipperSettlementFactor table data
-- Changes:
	- 3.7.29 - 2015/06/18 - KDA - ADDED
	- 3.10.13.3 - 2016/02/29 - JAE - Added Truck Type
******************************************************/
ALTER VIEW [dbo].[viewShipperSettlementFactor] AS
	SELECT X.*
		, MaxEffectiveDate = (SELECT MIN(OrderDate) FROM tblOrderSettlementShipper WHERE ShipperSettlementFactorID = X.ID AND BatchID IS NOT NULL)
		, MinEndDate = (SELECT MAX(OrderDate) FROM tblOrderSettlementShipper WHERE ShipperSettlementFactorID = X.ID AND BatchID IS NOT NULL)
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementShipper WHERE ShipperSettlementFactorID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblShipperSettlementFactor XN 
			WHERE isnull(XN.ShipperID, 0) = isnull(X.ShipperID, 0) 
			  AND isnull(XN.ProductGroupID, 0) = isnull(X.ProductGroupID, 0) 
			  AND isnull(XN.TruckTypeID, 0) = isnull(X.TruckTypeID, 0)
			  AND isnull(XN.OriginID, 0) = isnull(X.OriginID, 0) 
			  AND isnull(XN.OriginStateID, 0) = isnull(X.OriginStateID, 0) 
			  AND isnull(XN.DestinationID, 0) = isnull(X.DestinationID, 0) 
			  AND isnull(XN.DestinationStateID, 0) = isnull(X.DestinationStateID, 0) 
			  AND isnull(XN.ProducerID, 0) = isnull(X.ProducerID, 0) 
			  AND XN.EffectiveDate > X.EffectiveDate)
		, PriorEndDate = (
			SELECT max(XN.EndDate) 
			FROM tblShipperSettlementFactor XN 
			WHERE isnull(XN.ShipperID, 0) = isnull(X.ShipperID, 0) 
			  AND isnull(XN.ProductGroupID, 0) = isnull(X.ProductGroupID, 0) 
			  AND isnull(XN.TruckTypeID, 0) = isnull(X.TruckTypeID, 0)
			  AND isnull(XN.OriginID, 0) = isnull(X.OriginID, 0) 
			  AND isnull(XN.OriginStateID, 0) = isnull(X.OriginStateID, 0) 
			  AND isnull(XN.DestinationID, 0) = isnull(X.DestinationID, 0) 
			  AND isnull(XN.DestinationStateID, 0) = isnull(X.DestinationStateID, 0) 
			  AND isnull(XN.ProducerID, 0) = isnull(X.ProducerID, 0) 
			  AND XN.EndDate < X.EffectiveDate)
	FROM tblShipperSettlementFactor X


GO


/******************************************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: add a computed "EndDate" value to all ShipperWaitFeeParameter records
-- Changes:
	- 3.7.30 - 2015/06/19 - KDA - add ProducerID criteria
								- convert to use of fnCompareNullableInts()
	- 3.10.13.3 - 2016/02/29 - JAE - Added Truck Type
******************************************************/
ALTER VIEW [dbo].[viewShipperWaitFeeParameter] AS
	SELECT X.*
		, MaxEffectiveDate = (SELECT MIN(OrderDate) FROM tblOrderSettlementShipper WHERE WaitFeeParameterID = X.ID AND BatchID IS NOT NULL)
		, MinEndDate = (SELECT MAX(OrderDate) FROM tblOrderSettlementShipper WHERE WaitFeeParameterID = X.ID AND BatchID IS NOT NULL)
		, Locked = cast(CASE WHEN EXISTS (SELECT * FROM tblOrderSettlementShipper WHERE WaitFeeParameterID = X.ID AND BatchID IS NOT NULL) THEN 1 ELSE 0 END as bit)
		, NextEffectiveDate = (
			SELECT min(XN.EffectiveDate) 
			FROM tblShipperWaitFeeParameter XN
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EffectiveDate > X.EffectiveDate)
		, PriorEndDate = (
			SELECT max(XN.EndDate) 
			FROM tblShipperWaitFeeParameter XN
			WHERE dbo.fnCompareNullableInts(XN.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(XN.TruckTypeID, X.TruckTypeID) = 1
			  AND dbo.fnCompareNullableInts(XN.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(XN.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(XN.ProducerID, X.ProducerID) = 1
			  AND XN.EndDate < X.EffectiveDate)
	FROM tblShipperWaitFeeParameter X

GO



COMMIT
SET NOEXEC OFF