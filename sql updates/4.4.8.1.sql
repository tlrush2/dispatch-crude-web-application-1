SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.4.8'
SELECT  @NewVersion = '4.4.8.1'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-2024 - Update UGP related triggers'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

-- ----------------------------------------------------------------
-- Trigger Changes (Users/Groups/Permissions)
-- ----------------------------------------------------------------

/********************************************************
* Created: 6/28/2016												
* Author: Ben Bloodworth
* SQL Update: 4.0.0
* Purpose: To add permissions to users when the group(s) they are in are given more permissions.
*          The permissions will only be added if they are not already provided by another group.
* Changes: 
*		07/21/16 - 4.0.0	- BB	- Removed Delete portion and turned this into an insert only trigger. spDeleteGroupAndUserPermissions now takes care of the delete task.
*		08/27/16 - 4.1.0.1	- KDA	- cleanup code and ensure it won't fail if an insert is invoked that affects multiple rows
*		09/14/16 - 4.1.3.2	- BB	- Restored original code, Kevin's cleanup produced and undetected error that I'm fixing by restoring this for now.  We need to go back and find out what
*									  is not working (what got missed) on the cleanup version.  See DCWEB-1714 for more details in the notes/comments.
*       12/26/16 - 4.4.8.1	- BB	- Restored Kevin's changes from 4.1.0.1 +fix.  The original had a typo that he fixed and then sent to me for testing.
********************************************************/
ALTER TRIGGER trigRolesInGroups_I ON aspnet_RolesInGroups AFTER INSERT AS 
BEGIN
    -- Create the new permission record for each user that is currently in the affected group
    INSERT INTO aspnet_UsersInRoles (UserId, RoleId)
    SELECT DISTINCT UG.UserId, i.RoleId
    FROM inserted i
		JOIN aspnet_UsersInGroups UG ON UG.GroupId = i.GroupId
    EXCEPT SELECT UserId, RoleId FROM aspnet_UsersInRoles
END 
GO


/********************************************************
* Created: 6/22/2016
* Author: Ben Bloodworth
* SQL Update: 4.0.0
* Purpose: When a group is assigned to a user this trigger will add records to
*          aspnet_UsersInRoles matching the permissions associated with the users' new group.
*          Permissions the user already has assigned to them from another group will not be added a second time.
* Changes: 
*	08/27/16 - 4.1.0.1	- KDA	- cleanup code and ensure it won't fail if an insert is invoked that affects multiple rows
*	09/14/16 - 4.1.3.2	- BB	- Restored original code for good measure after problem with trigRolesInGroups_I was found.  Need to look into these "cleanup" versions before putting them back.
*	12/26/16 - 4.4.8.1	- BB	- Restored Kevin's changes from 4.1.0.1. Kevin fixed the issue in trigRolesInGroups_I that was causing issues and this  trigger appears to work correctly as well.
********************************************************/
ALTER TRIGGER trigUsersInGroups_I ON aspnet_UsersInGroups AFTER INSERT AS 
BEGIN                       
    INSERT INTO aspnet_UsersInRoles (UserId, RoleId)
    SELECT DISTINCT i.UserId, RG.RoleId
    FROM inserted i
		JOIN aspnet_RolesInGroups RG ON RG.GroupId = i.GroupId
    EXCEPT SELECT Userid, RoleId FROM aspnet_UsersInRoles
END
GO



-- ----------------------------------------------------------------
-- New Stored Procedure
-- ----------------------------------------------------------------

/********************************************************
* Created: 12/26/2016
* Author: Ben Bloodworth
* SQL Update: 4.4.8.1
* Purpose: Find all groups with the specified existing permission and add the new permission to those groups.
*          To be explicit, this does not "Add" a new role, it simply is a shortcut to add an existing "new" role
*          to a group that did not previously have that role and it's using another role as a lookup.
* Usage: spAddNewPermissionToGroupsWithExistingRole('viewDrivers','createDrivers').  This is most useful when adding new permissions for pages that did not
*        previously have extended permissions (like Create, Edit, and Deactivate on MVC page conversions)
* Changes: 
********************************************************/
CREATE PROCEDURE spAddNewPermissionToGroupsWithExistingRole (@ExistingRoleName VARCHAR(256), @NewRoleName VARCHAR(256)) AS
BEGIN

	DECLARE @ExistingRoleId VARCHAR(100)
		, @NewRoleId VARCHAR(100)

	SET @ExistingRoleId = (SELECT RoleId FROM aspnet_Roles WHERE RoleName = @ExistingRoleName)
	SET @NewRoleId = (SELECT RoleId FROM aspnet_Roles WHERE RoleName = @NewRoleName)	

	-- Insert the new permission into aspnet_RolesInGroups for each group that contains the specified existing role.
	-- The trigger on aspnet_RolesInGroups will take care of adding the role to all the users in each group.
	INSERT INTO aspnet_RolesInGroups (RoleId, GroupId)
	SELECT @NewRoleId, G.GroupId
	FROM aspnet_Groups G
	WHERE EXISTS (SELECT RIG.GroupId 
				  FROM aspnet_RolesInGroups RIG
				  WHERE RIG.RoleId = @ExistingRoleId
					  AND RIG.GroupId = G.GroupId)
	EXCEPT SELECT X.RoleID
				, X.GroupID
	FROM aspnet_RolesInGroups X

END
GO


COMMIT
SET NOEXEC OFF