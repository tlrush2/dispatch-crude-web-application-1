SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.5.7.1'
SELECT  @NewVersion = '4.5.8'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'JT-2126 - Convert Truck and Trailer Maintenance pages to MVC and create truck/trailer maintenance types page'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


-- Update existing truck master data page view permission so that it will not get mixed up below
UPDATE aspnet_Roles
SET RoleName = 'viewTrucks'
	, LoweredRoleName = 'viewtrucks'
WHERE RoleName = 'viewTruckMaintenance'
GO
-- Update existing Trailer master data page view permission so that it will not get mixed up below
UPDATE aspnet_Roles
SET RoleName = 'viewTrailers'
	, LoweredRoleName = 'viewtrailers'
WHERE RoleName = 'viewTrailerMaintenance'
GO


-- Add new Truck Maintenance permissions
EXEC spAddNewPermission 'viewTruckMaintenance', 'Allow user to view the Truck Maintenance documents page', 'Assets - Resources - Maintenance', 'Trucks - View'
GO
EXEC spAddNewPermission 'createTruckMaintenance', 'Allow user to create Truck Maintenance documents', 'Assets - Resources - Maintenance', 'Trucks - Create'
GO
EXEC spAddNewPermission 'editTruckMaintenance', 'Allow user to edit Truck Maintenance documents', 'Assets - Resources - Maintenance', 'Trucks - Edit'
GO
EXEC spAddNewPermission 'deactivateTruckMaintenance', 'Allow user to deactivate Truck Maintenance documents', 'Assets - Resources - Maintenance', 'Trucks - Deactivate'
GO
-- Add new Trailer Maintenance permissions
EXEC spAddNewPermission 'viewTrailerMaintenance', 'Allow user to view the Trailer Maintenance documents page', 'Assets - Resources - Maintenance', 'Trailers - View'
GO
EXEC spAddNewPermission 'createTrailerMaintenance', 'Allow user to create Trailer Maintenance documents', 'Assets - Resources - Maintenance', 'Trailers - Create'
GO
EXEC spAddNewPermission 'editTrailerMaintenance', 'Allow user to edit Trailer Maintenance documents', 'Assets - Resources - Maintenance', 'Trailers - Edit'
GO
EXEC spAddNewPermission 'deactivateTrailerMaintenance', 'Allow user to deactivate Trailer Maintenance documents', 'Assets - Resources - Maintenance', 'Trailers - Deactivate'
GO
-- Add new Truck/Trailer Maintenance Types permissions (These maintenance types are combined into one table)
EXEC spAddNewPermission 'viewTruckTrailerMaintenanceTypes', 'Allow user to view the Truck and Trailer Maintenance Types page', 'Assets - Resources - Maintenance', 'Types - View'
GO
EXEC spAddNewPermission 'createTruckTrailerMaintenanceTypes', 'Allow user to create Truck and Trailer Maintenance Types', 'Assets - Resources - Maintenance', 'Types - Create'
GO
EXEC spAddNewPermission 'editTruckTrailerMaintenanceTypes', 'Allow user to edit Truck and Trailer Maintenance Types', 'Assets - Resources - Maintenance', 'Types - Edit'
GO
EXEC spAddNewPermission 'deactivateTruckTrailerMaintenanceTypes', 'Allow user to deactivate Truck and Trailer Maintenance Types', 'Assets - Resources - Maintenance', 'Types - Deactivate'
GO


-- Add new Truck maintenance permissions to any groups that already had the "view" permission for compliance
EXEC spAddNewPermissionToGroupsWithExistingRole 'viewTruckCompliance', 'viewTruckMaintenance'
GO
EXEC spAddNewPermissionToGroupsWithExistingRole 'viewTruckCompliance', 'createTruckMaintenance'
GO
EXEC spAddNewPermissionToGroupsWithExistingRole 'viewTruckCompliance', 'editTruckMaintenance'
GO
EXEC spAddNewPermissionToGroupsWithExistingRole 'viewTruckCompliance', 'deactivateTruckMaintenance'
GO
-- Add new Trailer maintenance permissions to any groups that already had the "view" permission for compliance
EXEC spAddNewPermissionToGroupsWithExistingRole 'viewTrailerCompliance', 'viewTrailerMaintenance'
GO
EXEC spAddNewPermissionToGroupsWithExistingRole 'viewTrailerCompliance', 'createTrailerMaintenance'
GO
EXEC spAddNewPermissionToGroupsWithExistingRole 'viewTrailerCompliance', 'editTrailerMaintenance'
GO
EXEC spAddNewPermissionToGroupsWithExistingRole 'viewTrailerCompliance', 'deactivateTrailerMaintenance'
GO


-- Create the new Truck and Trailer Maintenance Types table
CREATE TABLE tblTruckTrailerMaintenanceType
(
	ID INT IDENTITY(1, 1) NOT NULL CONSTRAINT PK_TruckTrailerMaintenanceType PRIMARY KEY CLUSTERED
	, Name VARCHAR(50) NOT NULL CONSTRAINT UQ_TruckTrailerMaintenanceType_Name UNIQUE
	, ForTruck BIT NULL
	, ForTrailer BIT NULL
	, CreateDateUTC SMALLDATETIME NULL CONSTRAINT DF_TruckTrailerMaintenanceType_CreateDateUTC  DEFAULT (GETUTCDATE())
	, CreatedByUser VARCHAR(100) NULL CONSTRAINT DF_TruckTrailerMaintenanceType_CreatedByUser  DEFAULT 'System'
	, LastChangeDateUTC SMALLDATETIME NULL
	, LastChangedByUser VARCHAR(100) NULL
	, DeleteDateUTC SMALLDATETIME NULL
	, DeletedByUser VARCHAR(100) NULL
)
GRANT SELECT, INSERT, UPDATE, DELETE ON tblTruckTrailerMaintenanceType TO role_iis_acct 
GO


-- Add existing records to the new types table using what customers have already entered in each of their own databases
INSERT INTO tblTruckTrailerMaintenanceType (Name, ForTruck, ForTrailer)
	SELECT 'Basic Maintenance', 1, 1
	UNION
	SELECT DISTINCT LTRIM(RTRIM(Description)), 1, 0 FROM tblTruckMaintenance
GO
INSERT INTO tblTruckTrailerMaintenanceType (Name, ForTruck, ForTrailer)
	SELECT DISTINCT LTRIM(RTRIM(Description)), 0, 1 FROM tblTrailerMaintenance TM WHERE LTRIM(RTRIM(Description)) NOT IN (SELECT Name FROM tblTruckTrailerMaintenanceType)
GO
UPDATE tblTruckTrailerMaintenanceType
SET ForTrailer = 1
WHERE Name IN (SELECT DISTINCT LTRIM(RTRIM(Description)) FROM tblTruckMaintenance)
	AND Name IN (SELECT DISTINCT LTRIM(RTRIM(Description)) FROM tblTrailerMaintenance)
GO


-- Add cost field to tblTruckMaintenance
ALTER TABLE tblTruckMaintenance
	ADD Cost MONEY NULL
GO
-- Add cost field to tblTrailerMaintenance
ALTER TABLE tblTrailerMaintenance
	ADD Cost MONEY NULL
GO


-- Add maintenance type foreign key to tblTruckMaintenance (must be null at first so that the ID can be entered)
ALTER TABLE tblTruckMaintenance
	ADD MaintenanceTypeID INT NULL CONSTRAINT FK_tblTruckMaintenance_TruckTrailerMaintenanceType REFERENCES tblTruckTrailerMaintenanceType(ID)
GO
-- Add maintenance type foreign key to tblTrailerMaintenance (must be null at first so that the ID can be entered)
ALTER TABLE tblTrailerMaintenance
	ADD MaintenanceTypeID INT NULL CONSTRAINT FK_tblTrailerMaintenance_TruckTrailerMaintenanceType REFERENCES tblTruckTrailerMaintenanceType(ID)
GO


-- Set the foreign key values in tblTruckMaintenance
UPDATE M
	SET M.MaintenanceTypeID = T.ID
	FROM tblTruckMaintenance M
	LEFT JOIN tblTruckTrailerMaintenanceType T ON T.Name = M.Description		
GO
-- Set the foreign key values in tblTrailerMaintenance
UPDATE M
	SET M.MaintenanceTypeID = T.ID
	FROM tblTrailerMaintenance M
	LEFT JOIN tblTruckTrailerMaintenanceType T ON T.Name = M.Description		
GO


-- Remove the description field from tblTruckMaintenance now that its contents have been preserved
ALTER TABLE tblTruckMaintenance
	DROP COLUMN Description
GO
-- Remove the description field from tblTrailerMaintenance now that its contents have been preserved
ALTER TABLE tblTrailerMaintenance
	DROP COLUMN Description
GO


/***********************************/
-- Date Created: 8 Aug 2013
-- Author: Kevin Alons
-- Purpose: return Truck Maintenance records with translated "friendly" values
--	4.5.8 -		2017/02/16 -	BSB -	Added maintenanceType and Cost.  Removed Description
/***********************************/
ALTER VIEW viewTruckMaintenance AS
SELECT TM.ID
	, T.ID AS TruckID
	, TM.MaintenanceDate
	, TM.MaintenanceTypeID
	, TM.Cost
	, TM.Document
	, TM.DocName
	, TM.Notes
	, TM.CreateDateUTC
	, TM.CreatedByUser
	, TM.LastChangeDateUTC
	, TM.LastChangedByUser
	, T.IdNumber
	, T.VIN
	, T.CarrierID
	, T.Carrier
	, T.CarrierType
	, T.TruckType
FROM viewTruck T
JOIN tblTruckMaintenance TM ON TM.TruckID = T.ID
WHERE T.Active = 1
GO


/***********************************/
-- Date Created: 8 Aug 2013
-- Author: Kevin Alons
-- Purpose: return Trailer Maintenance records with translated "friendly" values
--	4.5.8 -		2017/02/16 -	BSB -	Added maintenanceType and Cost.  Removed Description
/***********************************/
ALTER VIEW viewTrailerMaintenance AS
SELECT TM.ID
	, T.ID AS TrailerID
	, TM.MaintenanceDate
	, TM.MaintenanceTypeID
	, TM.Cost
	, TM.Document
	, TM.DocName
	, TM.Notes
	, TM.CreateDateUTC
	, TM.CreatedByUser
	, TM.LastChangeDateUTC
	, TM.LastChangedByUser
	, T.IdNumber
	, T.VIN
	, T.CarrierID
	, T.Carrier
	, T.CarrierType
	, T.TrailerType
FROM viewTrailer T
JOIN tblTrailerMaintenance TM ON TM.TrailerID = T.ID
WHERE T.Active = 1
GO


-- ----------------------------------------------------------------
-- Refresh all
-- ----------------------------------------------------------------
EXEC _spRefreshAllViews
GO
EXEC _spRebuildAllObjects
GO


COMMIT
SET NOEXEC OFF