/*
	-- more Report Center functionality
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.0.1'
SELECT  @NewVersion = '3.0.2'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 23 Jul 2014
-- Description:	trigger to advance any duplicated SortNums in a UserReport to make reordering easier
-- =============================================
CREATE TRIGGER trigUserReportColumnDefinition_IU_SortNum ON tblUserReportColumnDefinition FOR INSERT, UPDATE AS
BEGIN
	IF (UPDATE(SortNum))
	BEGIN
		SELECT i.ID, i.UserReportID, i.SortNum 
		INTO #dupes
		FROM inserted i
		JOIN tblUserReportColumnDefinition URCD ON URCD.UserReportID = i.UserReportID AND URCD.SortNum = i.SortNum
		
		IF ((SELECT COUNT(*) FROM #dupes) > 0) BEGIN
			UPDATE tblUserReportColumnDefinition SET SortNum = URCD.SortNum + 1
			FROM tblUserReportColumnDefinition URCD
			JOIN #dupes DU on DU.UserReportID = URCD.UserReportID AND URCD.SortNum >= DU.SortNum 
			WHERE URCD.ID <> DU.ID
		END	
	END
END
GO

COMMIT
SET NOEXEC OFF