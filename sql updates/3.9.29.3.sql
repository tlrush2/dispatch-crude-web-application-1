-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.9.29.2.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.9.29.2.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0

SET NOEXEC OFF 
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.9.29.2'
SELECT  @NewVersion = '3.9.29.3'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'Rename three ticket types: Net Vol -> Estimated Barrels, Gross Vol -> Propane Run, and Gauge Net -> Est. BBLS w/Gauges'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


-- 12/2/15 - DCWEB-951: Update Ticket Type Names per Maverick
UPDATE tblTicketType SET Name = 'Estimated Barrels' WHERE ID = 2
UPDATE tblTicketType SET Name = 'Propane Run' WHERE ID = 5
UPDATE tblTicketType SET Name = 'Est. BBLS w/Gauges' WHERE ID = 7


COMMIT
SET NOEXEC OFF