-- backup database [dispatchcrude.dev] to disk = 'C:\files\Consulting\cts\dc_backups\dispatchcrude.dev.3.8.3.bak'
-- restore database [DispatchCrude.Dev] from disk = 'C:\files\Consulting\cts\dc_backups\dispatchcrude.dev.3.8.3.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.8.3'
SELECT  @NewVersion = '3.8.4'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Destination Allocations: fix DailyUnits calculation (was short by 1 day)'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/****************************************************
-- Date Created: 25 Jan 2015
-- Author: Kevin Alons
-- Purpose: calculate DailyUnits value
** Changes:
	- 3.8.4 - 2015/07/10 - KDA - fix DailyUnits being shorted by 1 day (was causing a DIVIDE BY ZERO error for 1 day allocation entry
****************************************************/
ALTER VIEW viewAllocationDestinationShipper AS
	SELECT ADS.*
		, DailyUnits = Units / cast(DATEDIFF(day, EffectiveDate, EndDate) + 1 as decimal(18, 10))
		, Destination = D.FullName
		, Shipper = C.Name
		, ProductGroup = PG.Name
		, Uom = U.Name
	FROM tblAllocationDestinationShipper ADS
	JOIN viewDestination D ON D.ID = ADS.DestinationID
	JOIN tblCustomer C ON C.ID = ADS.ShipperID
	JOIN tblProductGroup PG ON PG.ID = ADS.ProductGroupID
	JOIN tblUom U ON U.ID = ADS.UomID
GO

COMMIT
SET NOEXEC OFF