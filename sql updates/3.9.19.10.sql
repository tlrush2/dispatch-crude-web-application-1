-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.9.19.9.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.9.19.9.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0

SET NOEXEC OFF
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.9.19.9'
SELECT  @NewVersion = '3.9.19.10'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'DCWEB-837 - Data Exchange with ShipXpress for BVL - Added export views and T/F field on Shippers page.'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/* Add ShipXpress Client Identification column and boolean column indicating if the shipper uses this export */
ALTER TABLE tblCustomer ADD ShipXpressDXClient VARCHAR(10) NULL, ShipXpressDXExport BIT NULL
GO

/* Set all shippers to false initially so that 'true' must be selected on purpose for each shipper */
UPDATE tblCustomer SET ShipXpressDXExport = 0
GO

/* Added this before creating the views.  10/8/15.  This prevents the views below from erroring out due to the supporting views not being refreshed to show the new table columns */
EXEC _spRefreshAllViews
GO

/*=============================================
	Author:			Ben Bloodworth
	Create date:	29 Sep 2015
	Description:	Displays the base information for the ShipXpress export. 					
	Updates:		10/05/15 - BB: Added logic to show/hide tank/meter information depending upon ticket type and dynamic 
									clientID and shipper selection for instances (BVL) when more than one shipper uses this export
					10/07/15 - BB: Moved joins around to allow for pulling in rejected loads.  Used <ordernum>+X format for rejected tickets. 
									Also changed to using order reject notes instead of ticket reject notes.
					10/08/15 - BB: Changed spaces ' ' to nothing '' so that the created XML tags come out like <xmltag/> instead of <xmltag></xmltag>
									This change was required by ShipXpress.  Also addes ISNULL to just about every field to make sure they show up
									even when they are not full of data (like with rejects)									
=============================================*/
CREATE VIEW viewOrderExport_ShipXpress_Base AS
SELECT
	OLD.ID AS ExportedOrderID	
	, Client = C.ShipXpressDXClient
	, ISNULL(OLD.DispatchConfirmNum,'') AS TMWOrderNumber
	, ISNULL(OT.CarrierTicketNum, LTRIM(OLD.OrderNum) + 'X') AS ClientOrderNumber
	, ISNULL(OLD.OrderStatus,'') AS TicketStatus
	, ISNULL(OLD.LeaseNum,'') AS LeaseID
	, ISNULL(OLD.LeaseName,'') AS LeaseName
	, ISNULL(OLD.DestStation,'') AS IPID
	, ISNULL(RTRIM(LTRIM(OLD.Destination)),'') AS IPName
	, ISNULL(OLD.DriverNumber,'') AS DriverID
	, ISNULL((OLD.DriverFirst + ' ' + OLD.DriverLast),'') AS DriverName
	, ISNULL(OLD.Trailer,'') AS TrailerID
	, ISNULL(OLD.Truck,'') AS TruckID
	, '' AS CarrierID
	, '' AS CarrierName
	, ISNULL(OLD.OriginDepartTime,'') AS TicketDate
	, ISNULL((CASE WHEN OT.TicketTypeID = 3 THEN '' ELSE OT.OriginTankText END),'') AS TankID  -- Do not show for Meter Run
	, ISNULL((CASE WHEN OT.TicketTypeID = 3 THEN '' ELSE OLD.LeaseName END),'') AS TankName  -- Do not show for Meter Run
	, ISNULL((CASE WHEN OT.TicketTypeID = 3 THEN OT.OriginTankText ELSE '' END),'') AS MeterID  -- Do not show unless Meter Run
	, ISNULL((CASE WHEN OT.TicketTypeID = 3 THEN OLD.LeaseName ELSE '' END),'') AS MeterName  -- Do not show unless Meter Run
	, ISNULL(CAST(OT.GrossUnits AS VARCHAR),'') AS GrossVolume
	, ISNULL(CAST(OT.NetUnits AS VARCHAR),'') AS NetVolume
	, ISNULL(CAST(OT.ProductObsGravity AS VARCHAR),'') AS ObsGravity
	, ISNULL(CAST(OT.ProductObsTemp AS VARCHAR),'') AS ObsTemp
	, ISNULL(CAST(OT.ProductBSW AS VARCHAR),'') AS BSW
	, ISNULL(OLD.OriginArriveTimeUTC,'') AS BeginReadingDate
	, ISNULL(OLD.OriginDepartTimeUTC,'') AS EndReadingDate
	, ISNULL(CAST(CAST(((OT.OpeningGaugeFeet * 12) + OT.OpeningGaugeInch + (CAST(OT.OpeningGaugeQ AS DECIMAL) / 4)) AS DECIMAL (18,5)) AS VARCHAR),'') AS TopGauge
	, ISNULL(CAST(CAST(((OT.ClosingGaugeFeet * 12) + OT.ClosingGaugeInch + (CAST(OT.ClosingGaugeQ AS DECIMAL) / 4)) AS DECIMAL (18,5)) AS VARCHAR),'') AS BottomGauge
	, ISNULL(CAST(OT.ProductHighTemp AS VARCHAR),'') AS TopTemp
	, ISNULL(CAST(OT.ProductLowTemp AS VARCHAR),'') AS BotTemp
	, ISNULL(CAST(OT.OpenMeterUnits AS VARCHAR),'') AS MeterStart
	, ISNULL(CAST(OT.CloseMeterUnits AS VARCHAR),'') AS MeterEnd
	, ISNULL(OLD.PickupDriverNotes,'') AS Comments
	, ISNULL(OLD.RejectDesc,'') AS RejectReason		
	, ISNULL(OT.SealOn,'') AS SealOn
	, ISNULL(OT.SealOff,'') AS SealOff
	, ISNULL(CAST(OLD.DestGrossUnits AS VARCHAR),'') AS StationLogMeterGross
	, ISNULL(CAST(OLD.DestOpenMeterUnits AS VARCHAR),'') AS StationLogMeterOpen
	, ISNULL(CAST(OLD.DestCloseMeterUnits AS VARCHAR),'') AS StationLogMeterClose
	, ISNULL(OLD.DeliverDriverNotes,'') AS StationLogComments
	, ISNULL(OLD.DestDepartTime,'') AS StationLogDate
	, ISNULL(OLD.Product,'') AS Product
	, '' AS CallinDate
	, ISNULL((SELECT MIN(CarrierTicketNum) 
			FROM tblOrderTicket TOT 
			WHERE TOT.OrderID = OT.OrderID 
				AND DeleteDateUTC IS NULL 
				AND TOT.CarrierTicketNum <> OT.CarrierTicketNum),'') AS RelatedOrderNumber			
FROM 
	viewOrderLocalDates OLD	
	JOIN viewOrderCustomerFinalExportPending EP ON EP.ID = OLD.ID	
	LEFT JOIN viewOrderTicket OT ON OT.OrderID = OLD.ID AND OT.DeleteDateUTC IS NULL
	LEFT JOIN viewCustomer C ON C.ID = OLD.CustomerID	
WHERE C.ShipXpressDXExport = 1 -- Only export loads from shippers who have this setting set to TRUE
GO


/*=============================================
	Author:			Ben Bloodworth
	Create date:	05 Oct 2015
	Description:	Turns the information collected by viewOrderExport_ShipXpress_Base into XML in the format specified by shipXpress 					
	Updates:		
=============================================*/
CREATE VIEW viewOrderExport_ShipXpress AS
SELECT 
(
	SELECT 
		[@Client] = SXB.Client
		, SXB.TMWOrderNumber
		, SXB.ClientOrderNumber
		, SXB.TicketStatus
		, SXB.LeaseID
		, SXB.LeaseName
		, SXB.IPID
		, SXB.IPName
		, SXB.DriverID
		, SXB.DriverName
		, SXB.TrailerID
		, SXB.TruckID
		, SXB.CarrierID
		, SXB.CarrierName
		, SXB.TicketDate
		, SXB.TankID
		, SXB.TankName
		, SXB.MeterID
		, SXB.MeterName
		, SXB.GrossVolume
		, SXB.NetVolume
		, SXB.ObsGravity
		, SXB.ObsTemp
		, SXB.BSW
		, SXB.BeginReadingDate
		, SXB.EndReadingDate
		, SXB.TopGauge
		, SXB.BottomGauge
		, SXB.TopTemp
		, SXB.BotTemp
		, SXB.MeterStart
		, SXB.MeterEnd
		, SXB.Comments
		, SXB.RejectReason
		, SXB.SealOn
		, SXB.SealOff
		, SXB.StationLogMeterGross
		, SXB.StationLogMeterOpen
		, SXB.StationLogMeterClose
		, SXB.StationLogComments
		, SXB.StationLogDate
		, SXB.Product
		, SXB.CallinDate
		, SXB.RelatedOrderNumber		
	FROM viewOrderExport_ShipXpress_Base SXB
	FOR XML PATH('Order'), ROOT('Orders'), TYPE
) XML
GO


/*****************************************************************************************
Author: Ben Bloodworth
Date Created: 08 Oct 2015
Purpose: The XML export for shipxpress does not work with temporary tables.  This was created to replace spOrderExportFinalCustomer
			for this one report.  This was thrown together in an "emergency" and probably really needs to be revisitied and redone
			properly as soon as possible and there is time to do so.
*****************************************************************************************/
CREATE PROCEDURE [dbo].[spOrderExportFinalCustomer_SXP]
( 
  @customerID int 
, @exportedByUser varchar(100) = NULL -- will default to SUSER_NAME() -- default value for now
, @exportFormat varchar(100) -- Options are: 'SUNOCO_SUNDEX' or 'CODE_STANDARD' or 'SHIPXPRESS'
, @finalExport bit = 1 -- defualt to TRUE
) 
AS BEGIN
	SET NOCOUNT ON
	-- default to the current user if not supplied
	IF @exportedByUser IS NULL SET @exportedByUser = SUSER_NAME()
	
	-- Create variable for error catching with CODE export
	DECLARE @CODEErrorFlag BIT = 0 -- Default to false
	DECLARE @output3 TABLE (RESULT XML) --Declare xml table variable
	
	-- retrieve the order records to export
	DECLARE @orderIDs IDTABLE
	INSERT INTO @orderIDs (ID)
	SELECT ID
	FROM dbo.viewOrderCustomerFinalExportPending 
	WHERE CustomerID = @customerID 

	BEGIN TRAN exportCustomer
	
	-- export the orders in the specified format
	IF (@exportFormat = 'SHIPXPRESS') BEGIN
		INSERT INTO @output3
			SELECT *
			FROM dbo.viewOrderExport_ShipXpress E
	END

	-- mark the orders as exported FINAL (for a Customer)
	IF (@finalExport = 1) BEGIN
		-- The SHIPXPRESS export has to be one whole/combined export, yet the customer who needs it (BVL) has the shipper divided into two shippers
		-- this if statement is to try to work around that problem by overriding the way spMarkOrderExportFinalCustomer was originally
		-- designed to work and adding any shippers that have the boolean value in tblCustomer set to true instead of just using one customerID
		IF (@exportFormat = 'SHIPXPRESS') BEGIN
		
			DECLARE @SXPids IDTABLE
			INSERT INTO @SXPids (ID)
			SELECT DISTINCT ExportedOrderID
			FROM dbo.viewOrderExport_ShipXpress_Base
		
			EXEC spMarkOrderExportFinalCustomer @SXPids, @exportedByUser
		END
	END
	
	COMMIT TRAN exportCustomer

	-- return the data to 
	IF (@exportFormat = 'SHIPXPRESS') BEGIN
		SELECT * FROM @output3
	END
END
GO

EXEC _spRefreshAllViews
GO

COMMIT
SET NOEXEC OFF