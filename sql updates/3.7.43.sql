-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.7.20.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.7.20.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0

-- backup database [dispatchcrude.dev] to disk = 'C:\Program Files\Microsoft SQL Server\MSSQL10_50.SQLEXPRESSDEV02\MSSQL\Backup\3.7.23-gsm.bak'

DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.7.42'
SELECT  @NewVersion = '3.7.43'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCDRV-219: New Order Rule for Visual Meter Destination (Calculate Meter Differences)'
	SELECT @NewVersion, 0, 'DCDRV-191: New Order Rule for Maximum Order Quantity (in Gallons)'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/* 3.7.43 - 07/03/2015 - GSM - DCDRV-219 - Add Order Rule to calculate GOV from meter entires for Visual Meter Destination */
/* 3.7.43 - 07/03/2015 - GSM - DCDRV-191 - Add Order Rule for maximum order quantity in gallons (with 13340 default) */

INSERT tblOrderRuleType (ID,Name,RuleTypeID)
     VALUES	(5,'Calculate VM Delivery Volume from Meter Entries',2)
		,(6,'Maximum Order Quantity Gallons',3)
GO


COMMIT
SET NOEXEC OFF
