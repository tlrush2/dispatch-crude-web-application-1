/*
	-- add tblSetting.DefaultGeoFenceRadiusMeters
	-- remove obsolete Is_StrappedTank report column definition
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.1.1'
SELECT  @NewVersion = '3.1.2'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

INSERT INTO tblSetting (ID, Name, SettingTypeID, Value, Category, CreatedByUser)
	SELECT 31, 'GPS GeoFence Radius Meters (default)', 4, 100, 'System Wide', 'System'

IF EXISTS (SELECT * FROM tblReportColumnDefinition WHERE ID = 88 AND Caption <> '<blank>')
BEGIN
	-- remove the Is_StrappedTank report column definition
	DELETE FROM tblUserReportColumnDefinition WHERE ReportColumnID = 88
	DELETE FROM tblReportColumnDefinition WHERE ID = 88

	INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) 
		VALUES (88, 1, N'(blank)', N'(blank)', NULL, NULL, 0, NULL, 0)
END

UPDATE tblReportColumnDefinition SET Caption = 'Ticket #' WHERE ID = 85
GO

UPDATE tblReportColumnDefinitionBaseFilter
  SET IncludeWhereClause = '(@CustomerID = -1 OR ID IN (SELECT O.ID FROM tblOrigin O WHERE CustomerID = @CustomerID UNION SELECT DISTINCT OriginID FROM tblOrder WHERE CustomerID = @CustomerID))'
WHERE ID = 8

UPDATE tblReportColumnDefinition SET FilterDropDownSql = 'SELECT ID=1, Name=''Yes'' UNION SELECT 0, ''No'''
WHERE filterDropDownSql = 'SELECT ID=1, Name=''Yes'' UNION SELECT 2, ''No'''

COMMIT
SET NOEXEC OFF