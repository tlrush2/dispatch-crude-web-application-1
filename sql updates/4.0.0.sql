-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.13.17'
SELECT  @NewVersion = '4.0.0'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'DCWEB-1420: Add users/groups/permissions functionality'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO



/*
	CREATE NEW U/G/P RELEATED TABLES AND ALTER SOME EXISTING TABLES
*/

-- 5/23/16 - Create the Groups table 
CREATE TABLE aspnet_Groups (
	ApplicationId UNIQUEIDENTIFIER NOT NULL
	, GroupId UNIQUEIDENTIFIER NOT NULL DEFAULT (NEWID())
	, GroupName NVARCHAR(256) NOT NULL	
	, LoweredGroupName NVARCHAR(256) NOT NULL	
	, Description NVARCHAR(256) NULL
	, CreateDateUTC SMALLDATETIME NULL DEFAULT (GETUTCDATE())
	, CreatedByUser VARCHAR(100) NULL DEFAULT ('System')
	, LastChangeDateUTC SMALLDATETIME NULL
	, LastChangedByUser VARCHAR(100) NULL
	, DeleteDateUTC SMALLDATETIME NULL
	, DeletedByUser VARCHAR(100) NULL
PRIMARY KEY NONCLUSTERED 
(GroupId ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE aspnet_Groups WITH CHECK ADD CONSTRAINT FK_Groups_ApplicationId FOREIGN KEY(ApplicationId)
REFERENCES aspnet_Applications (ApplicationId)
GO



-- 5/23/16 - Create Users in Groups table
CREATE TABLE aspnet_UsersInGroups (
	UserId UNIQUEIDENTIFIER NOT NULL
	, GroupId UNIQUEIDENTIFIER NOT NULL
PRIMARY KEY CLUSTERED 
(
	UserId ASC
	, GroupId ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE aspnet_UsersInGroups  WITH CHECK ADD CONSTRAINT FK_UsersInGroups_GroupId FOREIGN KEY(GroupId)
REFERENCES aspnet_Groups (GroupId)
GO

ALTER TABLE aspnet_UsersInGroups  WITH CHECK ADD CONSTRAINT FK_UsersInGroups_UserId FOREIGN KEY(UserId)
REFERENCES aspnet_Users (UserId)
GO



-- 5/23/16 - Create Roles (permissions) in Groups table
CREATE TABLE aspnet_RolesInGroups (
	RoleId UNIQUEIDENTIFIER NOT NULL
	, GroupId UNIQUEIDENTIFIER NOT NULL
PRIMARY KEY CLUSTERED 
(
	RoleId ASC
	, GroupId ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE aspnet_RolesInGroups  WITH CHECK ADD CONSTRAINT FK_RolesInGroups_GroupId FOREIGN KEY(GroupId)
REFERENCES aspnet_Groups (GroupId)
GO

ALTER TABLE aspnet_RolesInGroups  WITH CHECK ADD CONSTRAINT FK_RolesInGroups_RoleId FOREIGN KEY(RoleId)
REFERENCES aspnet_Roles (RoleId)
GO



-- 6/24/16 - Add date tracking columns to user membership table.  This allows us to activate/deactivate users from now on instead of deleting them.
ALTER TABLE aspnet_Membership
ADD  
	  CreatedByUser VARCHAR(100) NULL DEFAULT ('System')
	, LastChangeDateUTC SMALLDATETIME NULL
	, LastChangedByUser VARCHAR(100) NULL
	, DeleteDateUTC SMALLDATETIME NULL
	, DeletedByUser VARCHAR(100) NULL
GO



-- 6/28/16 - Add (permission) category field to aspnet_Roles table for better organization/display on the front end
ALTER TABLE aspnet_Roles
ADD	Category VARCHAR(100) NULL	
  , FriendlyName VARCHAR(100) NULL	  
GO


/*
	CREATE PERMISSION GROUPS
*/


--  5/23/16 - Add the default Administrator, Management, and Support groups to the Groups table to ensure they exist in each database.  These three groups will be made "undeleteable" from the front end.
INSERT INTO aspnet_Groups (ApplicationId, GroupId, GroupName, LoweredGroupName, Description)
SELECT '446467D6-39CD-45E3-B3E0-CAC7945AF3E8', NEWID(), '_DCAdministrator', '_dcadministrator', 'Highest level permission group.  All permissions are automatically available.'
UNION
SELECT '446467D6-39CD-45E3-B3E0-CAC7945AF3E8', NEWID(), '_DCSystemManager', '_dcsystemmanager', 'High level permission group for DC Managers.  Only editable by users in the group _DCAdministrator.'
UNION
SELECT '446467D6-39CD-45E3-B3E0-CAC7945AF3E8', NEWID(), '_DCSupport', '_dcsupport', 'High level permission group for software support techs.  Only editable by users in the groups _DCAdministrator and _DCSystemManager.'
EXCEPT SELECT ApplicationId, GroupId, GroupName, LoweredGroupName, Description
FROM aspnet_Groups
GO



--  5/23/16 - Add groups to the new Groups table that match the role names currently in place.  These are specifically for the original set of roles. (This task is to make the user transition easier below)
INSERT INTO aspnet_Groups (ApplicationId, GroupId, GroupName, LoweredGroupName, Description)
SELECT '446467D6-39CD-45E3-B3E0-CAC7945AF3E8', NEWID(), 'Carrier', 'carrier', 'LEGACY GROUP: This group provides the same access the Carrier role provided in the past'
UNION
SELECT '446467D6-39CD-45E3-B3E0-CAC7945AF3E8', NEWID(), 'Compliance', 'compliance', 'LEGACY GROUP: This group provides the same access the Compliance role provided in the past'
UNION
SELECT '446467D6-39CD-45E3-B3E0-CAC7945AF3E8', NEWID(), 'Customer', 'customer', 'LEGACY GROUP: This group provides the same access the Customer role provided in the past'
UNION
SELECT '446467D6-39CD-45E3-B3E0-CAC7945AF3E8', NEWID(), 'Driver', 'driver', 'LEGACY GROUP: This group provides the same access the Driver role provided in the past'
UNION
SELECT '446467D6-39CD-45E3-B3E0-CAC7945AF3E8', NEWID(), 'Gauger', 'gauger', 'LEGACY GROUP: This group provides the same access the Gauger role provided in the past'
UNION
SELECT '446467D6-39CD-45E3-B3E0-CAC7945AF3E8', NEWID(), 'Logistics', 'logistics', 'LEGACY GROUP: This group provides the same access the Logistics role provided in the past'
UNION
SELECT '446467D6-39CD-45E3-B3E0-CAC7945AF3E8', NEWID(), 'Management', 'management', 'LEGACY GROUP: This group provides the same access the Management role provided in the past'
UNION
SELECT '446467D6-39CD-45E3-B3E0-CAC7945AF3E8', NEWID(), 'Operations', 'operations', 'LEGACY GROUP: This group provides the same access the Operations role provided in the past'
UNION
SELECT '446467D6-39CD-45E3-B3E0-CAC7945AF3E8', NEWID(), 'Producer', 'producer', 'LEGACY GROUP: This group provides the same access the Producer role provided in the past'
EXCEPT SELECT ApplicationId, GroupId, GroupName, LoweredGroupName, Description
FROM aspnet_Groups
GO






/*
	TRANSITION ROLES TO PERMISSIONS AND ADD USERS TO PERMISSION GROUPS
*/


/* 
5/31/16 - Create groups for any non-original roles that have been put in place, then assign all of those roles to their new matching groups.
This will create a one role per groups scenario so that each users current roles can be retained.
(This procedure is only good for the transition and will be deleted at the end of this sql update file)
*/
CREATE PROCEDURE spCreateGroupsForRoles AS
BEGIN

	/* Create temporary groups for roles that are not part of the original role set */
	INSERT INTO aspnet_Groups (ApplicationId, GroupId, GroupName, LoweredGroupName, Description)
	SELECT '446467D6-39CD-45E3-B3E0-CAC7945AF3E8'
		, NEWID()
		, RoleName
		, LoweredRoleName
		, 'TRANSITION GROUP: Please assign the single permission in this group to another more appropriate group and then delete/deactivate this group.'	
	FROM aspnet_Roles
	WHERE LoweredRoleName NOT IN (SELECT LoweredGroupName FROM aspnet_Groups)
		AND LoweredRoleName <> 'administrator'

	/* Associate roles with their matching groups */
	INSERT INTO aspnet_RolesInGroups (RoleId, GroupId)
	SELECT R.RoleId, G.GroupId
	FROM aspnet_Roles R
		INNER JOIN aspnet_Groups G ON G.LoweredGroupName = R.LoweredRoleName
END
GO
-- Execute the above defined procedure
EXEC spCreateGroupsForRoles
GO



/*	
5/31/16 - Associate all users with the newly created groups matching the roles they are currently assigned.
(This procedure is only good for the transition and will be deleted at the end of this sql update file)
*/
CREATE PROCEDURE spTransitionUserRolesToGroups AS
BEGIN
	DECLARE @USERCOUNT INT
		  , @USERID VARCHAR(100)
		  , @GROUPID VARCHAR(100)		  
		  , @i INT = 0

	CREATE TABLE #tempCompletedUsers (UserId VARCHAR(100))
	CREATE TABLE #tempUsersRoles (UserName VARCHAR(100), UserId VARCHAR(100), RoleName VARCHAR(100), RoleId VARCHAR(100))

	/* Get the count of the users table */
	SET @USERCOUNT = (SELECT COUNT(UserID) FROM aspnet_Users)

	/* Loop through all users and add them to the group that matches the name of the role(s) they are in */
	WHILE (@i < @USERCOUNT)
		BEGIN
			
			/* Get the next user that is not in the completed temp table */
			SET @USERID = (SELECT TOP 1 UserId 
						   FROM aspnet_Users 
						   WHERE UserId NOT IN (SELECT UserId 
												FROM #tempCompletedUsers
											   )
						  )

			/* Add one record to the temp table for each role the current user is currenlty assigned */
			INSERT INTO #tempUsersRoles (UserName, UserId, RoleName, RoleId)
			SELECT U.UserName, U.UserId, R.RoleName, R.RoleId			
			FROM aspnet_UsersInRoles UR
				INNER JOIN aspnet_Users U ON U.UserId = UR.UserId
				INNER JOIN aspnet_Roles R ON R.RoleId = UR.RoleId
			WHERE UR.UserId = @USERID

			/* Insert records from the temp table that was just created - if the user was assigned to any roles */
			IF ((SELECT COUNT(*) FROM #tempUsersRoles) > 0)
				BEGIN
					INSERT INTO aspnet_UsersInGroups (UserId, GroupId)
					SELECT tUR.UserId, G.GroupId
					FROM #tempUsersRoles tUR
						INNER JOIN aspnet_Groups G ON G.GroupName = tUR.RoleName
					WHERE TUR.UserId = @USERID					
				END

			/* Mark "this" user as complete */
			INSERT INTO #tempCompletedUsers (UserId) SELECT @USERID

			SET @i = @i + 1
		END
END
GO
-- Execute the above defined procedure
EXEC spTransitionUserRolesToGroups
GO



/* 5/31/16 - Add current CTS users to the appropriate new groups based upon their job role 
(This procedure is only good for the transition and will be deleted at the end of this sql update file)
*/
CREATE PROCEDURE spTransitionInternalRolesToGroups AS
BEGIN
	DECLARE @GROUPID VARCHAR(100)

	SET @GROUPID = (SELECT GroupId FROM aspnet_Groups WHERE GroupName = '_DCAdministrator')
	INSERT INTO aspnet_UsersInGroups (UserId, GroupId)
	SELECT UserId, @GROUPID
	FROM aspnet_Users
	WHERE UserName IN ('bbloodworth','gmochau','jengler','kalons','mtaylor')

	SET @GROUPID = (SELECT GroupId FROM aspnet_Groups WHERE GroupName = '_DCSystemManager')
	INSERT INTO aspnet_UsersInGroups (UserId, GroupId)
	SELECT UserId, @GROUPID
	FROM aspnet_Users
	WHERE UserName IN ('jpaul','trush')

	SET @GROUPID = (SELECT GroupId FROM aspnet_Groups WHERE GroupName = '_DCSupport')
	INSERT INTO aspnet_UsersInGroups (UserId, GroupId)
	SELECT UserId, @GROUPID
	FROM aspnet_Users
	WHERE UserName IN ('bpoff','fendres','lmundy','mrivera', 'gaker')
END
GO
-- Execute the above defined procedure
EXEC spTransitionInternalRolesToGroups
GO



/* 6/9/16 - Add all existing permissions to _DCAdministrator permission group */
INSERT INTO aspnet_RolesInGroups (RoleId, GroupId)
SELECT R.RoleId, (SELECT GroupId FROM aspnet_Groups WHERE GroupName = '_DCAdministrator')
FROM aspnet_Roles R
WHERE R.RoleName NOT IN ('Administrator','Carrier','Compliance','Customer','Driver','Gauger','Logistics','Management','Operations','Producer')
GO

/* 8/3/16 - Add all existing permissions to _DCSystemManager permission group */
INSERT INTO aspnet_RolesInGroups (RoleId, GroupId)
SELECT R.RoleId, (SELECT GroupId FROM aspnet_Groups WHERE GroupName = '_DCSystemManager')
FROM aspnet_Roles R
WHERE R.RoleName NOT IN ('Administrator','Carrier','Compliance','Customer','Driver','Gauger','Logistics','Management','Operations','Producer')
GO

/* 8/3/16 - Add all existing permissions to _DCSupport permission group */
INSERT INTO aspnet_RolesInGroups (RoleId, GroupId)
SELECT R.RoleId, (SELECT GroupId FROM aspnet_Groups WHERE GroupName = '_DCSupport')
FROM aspnet_Roles R
WHERE R.RoleName NOT IN ('Administrator','Carrier','Compliance','Customer','Driver','Gauger','Logistics','Management','Operations','Producer')
GO




/*
	ADD U/G/P RELATED TRIGGERS
*/


/********************************************************
* Created: 6/22/2016
* Author: Ben Bloodworth
* SQL Update: 4.0.0
* Purpose: When a group is assigned to a user this trigger will add records to
*          aspnet_UsersInRoles matching the permissions associated with the users new group.
*          Permissions the user already has assigned to them from another group will not be added a second time.
* Changes: 
********************************************************/
CREATE TRIGGER trigUsersInGroups_I ON aspnet_UsersInGroups AFTER INSERT AS 
BEGIN		

	DECLARE @USER VARCHAR(100) = (SELECT UserId FROM inserted)
		  , @GROUP VARCHAR(100) = (SELECT GroupId FROM inserted)

	INSERT INTO aspnet_UsersInRoles (UserId, RoleId)
	SELECT @USER, RIG.RoleId 
	FROM (SELECT RoleId, GroupId 
		  FROM aspnet_RolesInGroups 
		  WHERE GroupId = @GROUP) RIG
	WHERE NOT EXISTS (SELECT UserId, RoleId 
					  FROM aspnet_UsersInRoles 
					  WHERE UserId = @USER 
						AND RoleId = RIG.RoleId)
	
END
GO



/********************************************************
* Created: 6/28/2016												
* Author: Ben Bloodworth
* SQL Update: 4.0.0
* Purpose: To add permissions to users when the group(s) they are in are given more permissions.
*          The permissions will only be added if they are not already provided by another group.
* Changes: 
*		7/21/16 - BB - Removed Delete portion and turned this into an insert only trigger. spDeleteGroupAndUserPermissions now takes care of the delete task.
********************************************************/
CREATE TRIGGER trigRolesInGroups_I ON aspnet_RolesInGroups AFTER INSERT AS 
BEGIN

	IF OBJECT_ID('tempdb..#tempUsersInsert') IS NOT NULL DROP TABLE #tempUsersInsert	
	
	DECLARE @GROUP VARCHAR(100)
		  , @PERMISSION VARCHAR(100)
					
	SET @PERMISSION = (SELECT RoleId FROM inserted)
	SET @GROUP = (SELECT GroupId FROM inserted)

	-- Get a list of users (in #tempUsersInsert) who are currently in the group which has just been given a/new permission(s)
	SELECT UserId
	INTO #tempUsersInsert
	FROM aspnet_UsersInGroups
	WHERE GroupId = @GROUP

	-- Create the new permission record for each user that is currently in the affected group
	INSERT INTO aspnet_UsersInRoles (UserId, RoleId)
	SELECT TUI.UserId, @PERMISSION
	FROM #tempUsersInsert TUI
	WHERE NOT EXISTS (SELECT UserId, RoleId
						FROM aspnet_UsersInRoles
						WHERE UserId = TUI.UserId
						AND RoleId = @PERMISSION)

END 
GO




/*
	ADD U/G/P RELATED STORED PROCEDURES
*/


/********************************************************
* Created: 6/9/2016
* Author: Ben Bloodworth
* SQL Update: 4.0.0
* Purpose: This procedure was created to make adding permissions easier in the SQL update files now and going forward.
* Changes: 6/9/16 - BB & JAE - Added code to also add the new permission to the admin group and admin users.  That code was
*							   going to be a trigger before it was moved here instead to consolidate things.
*          8/3/16 - BB - Added _DCSystemManager and _DCSupport groups to the recipients list per Maverick.  All internal groups 
*                        should recieve new permissions initially then admins can remove them where needed.
********************************************************/
CREATE PROCEDURE spAddNewPermission (@NAME VARCHAR(256), @DESCRIPTION VARCHAR(256), @CATEGORY VARCHAR(100), @FRIENDLYNAME VARCHAR(100)) AS
BEGIN
		
	DECLARE @ADMINGROUPID VARCHAR(100)
		, @SYSMGRGROUPID VARCHAR(100)
		, @SUPPORTGROUPID VARCHAR(100)
		, @ROLEID VARCHAR(100)

	SET @ADMINGROUPID = (SELECT GroupId FROM aspnet_Groups WHERE GroupName = '_DCAdministrator')
	SET @SYSMGRGROUPID = (SELECT GroupId FROM aspnet_Groups WHERE GroupName = '_DCSystemManager')
	SET @SUPPORTGROUPID = (SELECT GroupId FROM aspnet_Groups WHERE GroupName = '_DCSupport')

	-- Insert the permission
	INSERT INTO aspnet_Roles (ApplicationId, RoleId, RoleName, LoweredRoleName, Description, Category, FriendlyName)
	SELECT '446467D6-39CD-45E3-B3E0-CAC7945AF3E8', NEWID(), @NAME, LOWER(@NAME), @DESCRIPTION, @CATEGORY, @FRIENDLYNAME
	WHERE NOT EXISTS (SELECT 1 FROM aspnet_roles WHERE Rolename = @NAME)

	-- Get ID of new permission
	SET @ROLEID = (SELECT RoleId FROM aspnet_Roles WHERE RoleName = @NAME)

	-- Add the new permission to the _DCAdministrator group	
	INSERT INTO aspnet_RolesInGroups (RoleId, GroupId)
	SELECT @ROLEID, @ADMINGROUPID
	EXCEPT SELECT RoleId, GroupId
	FROM aspnet_RolesInGroups

	-- Add the new permission to the _DCSystemManager group	
	INSERT INTO aspnet_RolesInGroups (RoleId, GroupId)
	SELECT @ROLEID, @SYSMGRGROUPID
	EXCEPT SELECT RoleId, GroupId
	FROM aspnet_RolesInGroups

	-- Add the new permission to the _DCSupport group	
	INSERT INTO aspnet_RolesInGroups (RoleId, GroupId)
	SELECT @ROLEID, @SUPPORTGROUPID
	EXCEPT SELECT RoleId, GroupId
	FROM aspnet_RolesInGroups

	-- Add the new permission to every user in the _DCAdministrator, _DCSystemManager, and _DCSupport groups
	INSERT INTO aspnet_UsersInRoles (UserId, RoleId)
	SELECT UserId, @ROLEID
	FROM aspnet_UsersInGroups UIG
	WHERE GroupId IN (@ADMINGROUPID, @SYSMGRGROUPID, @SUPPORTGROUPID)
		AND NOT EXISTS (SELECT 1 
						FROM aspnet_UsersInRoles UIR 
						WHERE UIR.RoleId = @ROLEID 
							AND UIR.UserId = UIG.UserId)

END
GO



/********************************************************
* Created: 7/20/2016
* Author: Ben Bloodworth
* SQL Update: 4.0.0
* Purpose: This procedure deletes one or more records associating groups with a user depending upon how it is called.
*          It also removes the records associationg users with permissions (roles) that were contained in any deleted groups.
********************************************************/
CREATE PROCEDURE spDeleteUserGroupsAndPermissions (@USER VARCHAR(256), @GROUP VARCHAR(256) = NULL) AS
BEGIN		

	/* Delete the record associating the user with the specified group. If all groups have been deselected, delete all the user's current groups. */
	IF @GROUP IS NULL
		BEGIN
			-- Delete any remaining group records
			DELETE FROM aspnet_UsersInGroups WHERE UserId = @USER

			-- Delete any remaining permissions since all groups are gone
			DELETE FROM aspnet_UsersInRoles WHERE UserId = @USER

		END
	ELSE
		BEGIN
			-- Delete the user_group association record for the specified group
			DELETE FROM aspnet_UsersInGroups WHERE UserId = @USER AND GroupId = @GROUP
			
			/* Delete any  permissions the user has that were associated with the group they've been removed from
			   (but only if the permission is not ALSO given by another group the user is part of */
			DELETE FROM aspnet_UsersInRoles
			WHERE UserId = @USER
			AND RoleId IN (
							SELECT RoleId
							FROM aspnet_Roles 
							WHERE RoleId IN (
											 SELECT UIR.RoleId
											 FROM aspnet_UsersInRoles UIR
											 WHERE UIR.UserId = @USER
												AND EXISTS (
															-- All Permissions included in the group that the user is being removed from.
															SELECT 1 
															FROM aspnet_RolesInGroups RIG 
															WHERE UIR.RoleId = RIG.RoleId 
																AND RIG.GroupId = @GROUP
															)
												AND NOT EXISTS (
																-- All Permissions that should not be deleted because they are also provided to the user in another group they are a still a member of.
																SELECT 1 
																FROM aspnet_RolesInGroups RIG_KEEP
																   , aspnet_UsersInGroups UIG 
																WHERE UIG.GroupId = RIG_KEEP.GroupId 
																	AND RIG_KEEP.GroupId <> @GROUP
																	AND UIG.UserId = @USER 
																	AND UIR.RoleId = RIG_KEEP.RoleId
																)
											) 
							)
		END
END
GO



/********************************************************
* Created: 7/21/2016
* Author: Ben Bloodworth & Joe Engler
* SQL Update: 4.0.0
* Purpose: Removes a permission (role) from a group and also removes that permission from any users in that group who do not have the permission through another group.
********************************************************/
CREATE PROCEDURE spDeleteGroupAndUserPermissions (@GROUP VARCHAR(256), @PERMISSION VARCHAR(256)) AS
BEGIN			

	-- Delete the record associating the permission with the group
	DELETE FROM aspnet_RolesInGroups WHERE GroupId = @GROUP AND RoleId = @PERMISSION			

	-- Delete the permission from all users in the modified group (EXCEPT) where the user already has this permission from another group
	DELETE
	FROM aspnet_UsersInRoles
	WHERE RoleId = @PERMISSION
		AND UserId IN 
					(
					SELECT UIR.UserId--, UIR.RoleId
					FROM aspnet_UsersInRoles UIR
					WHERE UIR.RoleId = @PERMISSION
					AND EXISTS (
								-- All users in the group
								SELECT 1
								FROM aspnet_UsersInGroups UIG
								WHERE UIG.GroupId = @GROUP
									AND UIR.UserId = UIG.UserId
								)
					AND NOT EXISTS (				
									-- All Users that recieve this permission from another group (and therefore it cannot be deleted from their permissions)
									SELECT 1
									FROM aspnet_UsersInGroups UIG
										, aspnet_RolesInGroups RIG
									WHERE RIG.GroupId = UIG.GroupId
										AND RIG.GroupId <> @GROUP
										AND RIG.RoleId = @PERMISSION
										AND UIR.UserId = UIG.UserId
									)
					)
	
END
GO



/*
	ADD U/G/P RELATED FUNCTIONS
*/


/********************************************************
* Created: 7/15/2016
* Author: Ben Bloodworth
* SQL Update: 4.0.0
* Purpose: This returns whether or not the user is in a certain group
********************************************************/
CREATE FUNCTION fnIsUserInGroup (@USERID VARCHAR(256), @GROUPID VARCHAR(256)) RETURNS BIT AS
BEGIN
	DECLARE @RET AS BIT

	IF EXISTS (SELECT * FROM aspnet_UsersInGroups WHERE UserId = @USERID AND GroupId = @GROUPID)
		SET @RET = 1
	ELSE
		SET @RET = 0

	RETURN @RET
END
GO



/********************************************************
* Created: 7/19/2016
* Author: Ben Bloodworth
* SQL Update: 4.0.0
* Purpose: This returns whether or not the specified group contains the specified permission (role)
********************************************************/
CREATE FUNCTION fnIsPermissionInGroup (@GROUPID VARCHAR(256), @PERMISSIONID VARCHAR(256)) RETURNS BIT AS
BEGIN
	DECLARE @RET AS BIT

	IF EXISTS (SELECT * FROM aspnet_RolesInGroups WHERE RoleId = @PERMISSIONID AND GroupId = @GROUPID)
		SET @RET = 1
	ELSE
		SET @RET = 0

	RETURN @RET
END
GO




/*
	ADD NEW PERMISSIONS FOR SECURITY MENU ACCESS
*/

/* 6/1/16 - Add permissions for the new security menu item and sub-items */   
EXEC spAddNewPermission 'viewUsers', 'Allow user to view User management pages', 'Users', 'View'
GO
EXEC spAddNewPermission 'createUsers', 'Allow user to create other users', 'Users', 'Create'
GO
EXEC spAddNewPermission 'editUsers', 'Allow user to edit other users', 'Users', 'Edit'
GO
EXEC spAddNewPermission 'deactivateUsers', 'Allow user to deactivate other users', 'Users', 'Deactivate'
GO

EXEC spAddNewPermission 'viewGroups', 'Allow user to view Security Group management pages', 'Users - Security Groups', 'View'
GO
EXEC spAddNewPermission 'createGroups', 'Allow user to create Security Groups', 'Users - Security Groups', 'Create'
GO
EXEC spAddNewPermission 'editGroups', 'Allow user to edit Security Groups', 'Users - Security Groups', 'Edit'
GO
EXEC spAddNewPermission 'deactivateGroups', 'Allow user to deactivate Security Groups', 'Users - Security Groups', 'Deactivate'
GO

EXEC spAddNewPermission 'switchUser', 'Allow user to impersonate another user (Admin, Mgmt., and Support ONLY)', 'Security', 'Switch User'
GO


/*
	ADD NEW PERMISSIONS FOR EXISTING PAGES
*/
/* 7/28/16 - Add new permissions for pages that already exist but do not have any specific permissions yet */
EXEC spAddNewPermission 'viewAllocationDashboard', 'Allow user to view the Destination Allocation Dashboard', 'Dashboards', 'Dest. Allocation'
GO
EXEC spAddNewPermission 'viewCarrierTypes', 'Allow user to view Carrier Types page', 'System Data Types', 'Carrier'
GO
EXEC spAddNewPermission 'viewDestinationTypes', 'Allow user to view Destination Types page', 'System Data Types', 'Destination'
GO
EXEC spAddNewPermission 'viewOriginTypes', 'Allow user to view Origin Types page', 'System Data Types', 'Origin'
GO
EXEC spAddNewPermission 'viewTruckTypes', 'Allow user to view Truck Types page', 'System Data Types', 'Truck'
GO
EXEC spAddNewPermission 'viewTrailerTypes', 'Allow user to view Trailer Types page', 'System Data Types', 'Trailer'
GO
EXEC spAddNewPermission 'viewDriverPrintConfig', 'Allow user to view Driver App print configuration pages', 'Print Configuration', 'Driver App'
GO
EXEC spAddNewPermission 'viewGaugerPrintConfig', 'Allow user to view Gauger App print configuration pages', 'Print Configuration', 'Gauger App'
GO
EXEC spAddNewPermission 'viewProducts', 'Allow user to view Products page', 'Products', 'Products'
GO
EXEC spAddNewPermission 'viewProductGroups', 'Allow user to view Product Groups page', 'Products', 'Product Groups'
GO
EXEC spAddNewPermission 'viewReasonCodes', 'Allow user to view Reason Code configuration pages', 'Reason Codes', 'View'
GO
EXEC spAddNewPermission 'viewSystemSettings', 'Allow user to view System Settings page', 'System Settings', 'Settings'
GO
EXEC spAddNewPermission 'viewUnitsOfMeasure', 'Allow user to view Units of Measure page', 'System Settings', 'Units Of Measure'
GO
EXEC spAddNewPermission 'viewCarrierMaintenance', 'Allow user to view the Carrier Maintenance page', 'Maintenance', 'Carriers'
GO
EXEC spAddNewPermission 'viewDestinationMaintenance', 'Allow user to view the Destination Maintenance page', 'Maintenance', 'Destinations'
GO
EXEC spAddNewPermission 'viewDriverMaintenance', 'Allow user to view the Driver Maintenance page', 'Maintenance', 'Drivers'
GO
EXEC spAddNewPermission 'viewGaugerMaintenance', 'Allow user to view the Gauger Maintenance page', 'Maintenance', 'Gaugers'
GO
EXEC spAddNewPermission 'viewOperatorMaintenance', 'Allow user to view the Operator Maintenance page', 'Maintenance', 'Operators'
GO
EXEC spAddNewPermission 'viewOriginMaintenance', 'Allow user to view the Origin Maintenance page', 'Maintenance', 'Origins'
GO
EXEC spAddNewPermission 'viewOriginTankMaintenance', 'Allow user to view the Origin Tank Maintenance page', 'Maintenance', 'Origin Tanks'
GO
EXEC spAddNewPermission 'viewPumperMaintenance', 'Allow user to view the Pumper Maintenance page', 'Maintenance', 'Pumpers'
GO
EXEC spAddNewPermission 'viewProducerMaintenance', 'Allow user to view the Producer Maintenance page', 'Maintenance', 'Producers'
GO
EXEC spAddNewPermission 'viewRegionMaintenance', 'Allow user to view the Region Maintenance page', 'Maintenance', 'Regions'
GO
EXEC spAddNewPermission 'viewRouteMaintenance', 'Allow user to view the Route Maintenance page', 'Maintenance', 'Routes'
GO
EXEC spAddNewPermission 'viewTrailerMaintenance', 'Allow user to view the Trailer Maintenance page', 'Maintenance', 'Trailers'
GO
EXEC spAddNewPermission 'viewTruckMaintenance', 'Allow user to view the Truck Maintenance page', 'Maintenance', 'Trucks'
GO
EXEC spAddNewPermission 'viewDriverEquipment', 'Allow user to view the Driver Equipment page', 'Maintenance - Driver Equipment', 'Equipment'
GO
EXEC spAddNewPermission 'viewDriverEquipmentManufacturers', 'Allow user to view the Driver Equipment Manufacturers page', 'Maintenance - Driver Equipment', 'Manufacturers'
GO
EXEC spAddNewPermission 'viewDriverAppHistory', 'Allow user to view the Driver App History page', 'Maintenance - Driver Equipment', 'App History'
GO
EXEC spAddNewPermission 'viewComplianceCarrierDocuments', 'Allow user to view the Carrier Compliace Documents page', 'Compliance', 'Carrier Documents'
GO
EXEC spAddNewPermission 'viewComplianceCarrierDocumentTypes', 'Allow user to view the Carrier Compliance Document Types page', 'System Data Types', 'Carrier Compliance Document'
GO
EXEC spAddNewPermission 'viewComplianceInspectionTypes', 'Allow user to view the Compliance Inspection Types pages', 'System Data Types', 'Compliance Inspection'
GO
EXEC spAddNewPermission 'viewComplianceDriverDocuments', 'Allow user to view the Driver Compliance pages', 'Compliance', 'Driver Documents'
GO
EXEC spAddNewPermission 'viewComplianceTrailer', 'Allow user to view the Trailer Compliance pages', 'Compliance', 'Trailer Documents'
GO
EXEC spAddNewPermission 'viewComplianceTruck', 'Allow user to view the Truck Compliance pages', 'Compliance', 'Truck Documents'
GO
EXEC spAddNewPermission 'viewDestinationShipperAllocation', 'Allow user to view the Destination Allocation page', 'Operations', 'Destination Allocations'
GO
EXEC spAddNewPermission 'viewDriverScheduling', 'Allow user to view the Driver Scheduling pages', 'Operations', 'Driver Scheduling'
GO
EXEC spAddNewPermission 'viewOdometerLog', 'Allow user to view the Odometer Log page', 'Operations', 'Odometer Log'
GO
EXEC spAddNewPermission 'viewGaugerOrders', 'Allow user to view the Gauger Order Create page', 'Orders', 'Gauger Orders'
GO
EXEC spAddNewPermission 'viewTruckOrders', 'Allow user to view the Truck Order Create page', 'Orders', 'Truck Orders'
GO
EXEC spAddNewPermission 'viewOrderRules', 'Allow user to view the Order Rules (Business Rules) page', 'Best Match Rules', 'Order Rules'
GO
EXEC spAddNewPermission 'viewOrderAudit', 'Allow user to view the Order Audit page', 'Orders', 'Order Audit'
GO
EXEC spAddNewPermission 'viewOrderApproval', 'Allow user to view the Order Approval page', 'Orders', 'Order Approval'
GO
EXEC spAddNewPermission 'viewDispatch', 'Allow user to view the Order Dispatch page', 'Dispatch', 'View'
GO
EXEC spAddNewPermission 'rerouteOrders', 'Allow user to Reroute Orders (Requires Edit Orders permission for full effect)', 'Dispatch', 'Reroute Orders'
GO
EXEC spAddNewPermission 'transferOrders', 'Allow user to Transfer Orders (Requires Edit Orders permission for full effect)', 'Dispatch', 'Transfer Orders'
GO
EXEC spAddNewPermission 'viewHelp', 'Allow user to view Help Documents page', 'Help', 'Help Documents'
GO
EXEC spAddNewPermission 'viewAppSyncErrors', 'Allow user to view Sync Errors page', 'Help', 'App Sync Errors'
GO
EXEC spAddNewPermission 'viewDriverAppSyncLog', 'Allow user to view Driver App Sync Log page', 'Help', 'Driver App Sync Log'
GO
EXEC spAddNewPermission 'viewGaugerAppSyncLog', 'Allow user to view Gauger App Sync Log page', 'Help', 'Gauger App Sync Log'
GO
EXEC spAddNewPermission 'viewAppChangeHistory', 'Allow user to view DC website Version History page', 'Help', 'Version History'
GO
EXEC spAddNewPermission 'viewAssessorialRateTypes', 'Allow user to view the Assessorial Rate Types page', 'Settlement', 'Assessorial Rate Types'
GO
EXEC spAddNewPermission 'viewCarrierAssorialRates', 'Allow user to view the Carrier Assessorial Rates page', 'Settlement - Carrier Rates', 'Assessorial'
GO
EXEC spAddNewPermission 'viewCarrierFuelSurchargeRates', 'Allow user to view the Carrier Fuel Surcharge Rates page', 'Settlement - Carrier Rates', 'Fuel Surcharge'
GO
EXEC spAddNewPermission 'viewCarrierOriginWaitRates', 'Allow user to view the Carrier Origin Wait Rates page', 'Settlement - Carrier Rates', 'Origin Wait'
GO
EXEC spAddNewPermission 'viewCarrierDestinationWaitRates', 'Allow user to view the Carrier Destination Wait Rates page', 'Settlement - Carrier Rates', 'Destination Wait'
GO
EXEC spAddNewPermission 'viewCarrierOrderRejectRates', 'Allow user to view the Carrier Order Reject Rates page', 'Settlement - Carrier Rates', 'Order Reject'
GO
EXEC spAddNewPermission 'viewCarrierRateSheets', 'Allow user to view the Carrier Rate Sheets page', 'Settlement - Carrier Rates', 'Rate Sheets'
GO
EXEC spAddNewPermission 'viewCarrierRouteRates', 'Allow user to view the Carrier Route Rates page', 'Settlement - Carrier Rates', 'Route Rates'
GO
EXEC spAddNewPermission 'viewCarrierWaitFeeParameters', 'Allow user to view the Carrier Wait Fee Parameters page', 'Settlement - Carrier Rates', 'Wait Fee Params'
GO
EXEC spAddNewPermission 'viewCarrierSettlement', 'Allow user to view Carrier Order Settlement page', 'Settlement', 'Settle Carriers'
GO
EXEC spAddNewPermission 'viewCarrierSettlementUnits', 'Allow user to view the Carrier Settlement Units page', 'Settlement - Carrier Rates', 'Settlement Units'
GO
EXEC spAddNewPermission 'viewCarrierMinSettlementUnits', 'Allow user to view the Carrier Minimum Settlement Units page', 'Settlement - Carrier Rates', 'Min. Settlement Units'
GO
EXEC spAddNewPermission 'viewCarrierSettlementBatches', 'Allow user to view the Carrier Settlement Batches page', 'Settlement', 'Carrier Batches'
GO
EXEC spAddNewPermission 'viewCarrierDriverSettlementGroups', 'Allow user to view the Driver Groups page', 'Settlement - Carrier Rates', 'Driver Groups'
GO
EXEC spAddNewPermission 'viewCarrierDriverSettlementGroupAssignments', 'Allow user to view the Driver Group Assignment page', 'Settlement - Carrier Rates', 'Driver Group Assignment'
GO
EXEC spAddNewPermission 'viewShipperAssorialRates', 'Allow user to view the Shipper Assessorial Rates page', 'Settlement - Shipper Rates', 'Assessorial'
GO
EXEC spAddNewPermission 'viewShipperFuelSurchargeRates', 'Allow user to view the Shipper Fuel Surcharge Rates page', 'Settlement - Shipper Rates', 'Fuel Surcharge'
GO
EXEC spAddNewPermission 'viewShipperOriginWaitRates', 'Allow user to view the Shipper Origin Wait Rates page', 'Settlement - Shipper Rates', 'Origin Wait'
GO
EXEC spAddNewPermission 'viewShipperDestinationWaitRates', 'Allow user to view the Shipper Destination Wait Rates page', 'Settlement - Shipper Rates', 'Destination Wait'
GO
EXEC spAddNewPermission 'viewShipperOrderRejectRates', 'Allow user to view the Shipper Order Reject Rates page', 'Settlement - Shipper Rates', 'Order Reject'
GO
EXEC spAddNewPermission 'viewShipperRateSheets', 'Allow user to view the Shipper Rate Sheets page', 'Settlement - Shipper Rates', 'Rate Sheets'
GO
EXEC spAddNewPermission 'viewShipperRouteRates', 'Allow user to view the Shipper Route Rates page', 'Settlement - Shipper Rates', 'Route Rates'
GO
EXEC spAddNewPermission 'viewShipperWaitFeeParameters', 'Allow user to view the Shipper Wait Fee Parameters page', 'Settlement - Shipper Rates', 'Wait Fee Params'
GO
EXEC spAddNewPermission 'viewShipperSettlement', 'Allow user to view Shipper Order Settlement page', 'Settlement', 'Settle Shippers'
GO
EXEC spAddNewPermission 'viewShipperSettlementUnits', 'Allow user to view the Shipper Settlement Units page', 'Settlement - Shipper Rates', 'Settlement Units'
GO
EXEC spAddNewPermission 'viewShipperMinSettlementUnits', 'Allow user to view the Shipper Minimum Settlement Units page', 'Settlement - Shipper Rates', 'Min. Settlement Units'
GO
EXEC spAddNewPermission 'viewShipperSettlementBatches', 'Allow user to view the Shipper Settlement Batches page', 'Settlement', 'Shipper Batches'
GO
EXEC spAddNewPermission 'viewPADDFuelPrices', 'Allow user to view PADD Fuel Prices page', 'Settlement', 'PADD Fuel Prices'
GO
EXEC spAddNewPermission 'viewOrderMap', 'Allow user to view Order Map page', 'Orders', 'Order Map'
GO
EXEC spAddNewPermission 'viewPrintBOL', 'Allow user to view eBOL Print page', 'Orders', 'View eBOLs'
GO
EXEC spAddNewPermission 'viewReportCenter', 'Allow user to view Report Center page', 'Reports', 'Report Center'
GO
EXEC spAddNewPermission 'viewEmailSubscription', 'Allow user to view Email Subscription page', 'Reports', 'Email Subscriptions'
GO
EXEC spAddNewPermission 'viewImportCenterCustomDataFields', 'Allow user to view Import Center Custom Data fields page', 'Import Center', 'Custom Data Fields'
GO
EXEC spAddNewPermission 'viewImportCenterGroups', 'Allow user to view Import Center Groups page', 'Import Center', 'Groups'
GO




/**********************************************************************************************************/
/* 7/21/16 - Add Categories and Friendly Names to permissions where possible (for permissions existing at the point this is commited to the code) */
-- Create temp table to store new descriptions, categories,  and friendly names
CREATE TABLE #TEMP (RoleName VARCHAR(256), Description VARCHAR(256), Category VARCHAR(100), FriendlyName VARCHAR(100))
GO

INSERT INTO #TEMP (RoleName, Description, Category, FriendlyName)
	  SELECT 'viewOrderSummary', 'Allow user to see and use Order Summary page', 'Reports', 'Order Summary'		
UNION SELECT 'viewVolumeAnalysis', 'Allow user to see and use Volume Analysis page', 'Reports', 'Volume Analysis'	
UNION SELECT 'Dispatch3P', 'Allow user to see and use the 3rd party dispatch page', 'Dispatch', '3rd Party Dispatch'
--ATTENTION! This following record should be split out into the four base permissions
UNION SELECT 'manageQuestionnaires', 'Allow user to see and use any Questionnaire pages', 'Questionnaires', NULL
--ATTENTION! This following record should be split out into the four base permissions
UNION SELECT 'manageCarrierRules', 'Allow user to see and use the Carrier Rules page', 'Best Match Rules', NULL
UNION SELECT 'viewImportCenter', 'Allow user to view Import Center page', 'Import Center', 'View'
UNION SELECT 'viewDispatchMap', 'Allow user to see the Dispatch Map page (requires Dispatch - View permission)', 'Dispatch', 'Dispatch Map'
UNION SELECT 'viewDispatchBoard', 'Allow user to see the Dispatch Board page (requires Dispatch - View permission)', 'Dispatch', 'Dispatch Board'
UNION SELECT 'viewDriverMap', 'Allow user to see the Driver Map page', 'Reports', 'Driver Map'
UNION SELECT 'viewDemurrageReport', 'Allow user to see the Wait Audit page (Bad Demurrage)', 'Reports', 'Wait Audit'
--ATTENTION! This following record should be split out into the four base permissions
UNION SELECT 'eLogManager', 'View/Create/Edit/Deactivate (Manage HOS/eLogs)', 'eLogs', NULL										
--ATTENTION! This following record should be split out into the four base permissions
UNION SELECT 'eLogReports', 'View and Run Reports on HOS/eLogs', 'eLogs', NULL										
UNION SELECT 'viewCommodityPricing', 'Allow user to view Commodity Pricing pages', 'Commodity Pricing', 'View'
UNION SELECT 'editCommodityPricing', 'Allow user to edit Commodity Pricing records', 'Commodity Pricing', 'Edit'
UNION SELECT 'createCommodityPricing', 'Allow user to create Commodity Pricing records', 'Commodity Pricing', 'Create'
UNION SELECT 'deactivateCommodityPricing', 'Allow user to deactivate Commodity Pricing records', 'Commodity Pricing', 'Deactivate'
UNION SELECT 'viewOrders', 'Allow user to view orders (order edit pages specifically)', 'Orders', 'View'
UNION SELECT 'editOrders', 'Allow user to edit orders  (order edit pages specifically)', 'Orders', 'Edit'
UNION SELECT 'searchOrders', 'Allow user to use the quick search box and advanced search page', 'Orders', 'Search'
UNION SELECT 'unauditOrders', 'Allow user to un-audit orders', 'Orders', 'Un-audit'
UNION SELECT 'undeleteOrders', 'Allow user to un-delete orders', 'Orders', 'Un-delete'
UNION SELECT 'settleProducer', 'Allow user to settle orders for producers', 'Settlement', 'Settle Producers'
UNION SELECT 'unsettleCarrierBatches', 'Allow user to unsettle a carrier settlement batch', 'Settlement', 'Unsettle Carrier Batches'
UNION SELECT 'unsettleShipperBatches', 'Allow user to unsettle a shipper settlement batch', 'Settlement', 'Unsettle Shipper Batches'
UNION SELECT 'viewShippers', 'Allow user to view the Shippers maintenance page', 'Shippers', 'View'
UNION SELECT 'editShippers', 'Allow user to edit shippers', 'Shippers', 'Edit'
UNION SELECT 'createShippers', 'Allow user to create new shippers', 'Shippers', 'Create'
UNION SELECT 'deactivateShippers', 'Allow user to deactivate shippers', 'Shippers', 'Deactivate'
-- NOW FOR THE LEGACY PERMISSIONS(ROLES)...
UNION SELECT 'Administrator', NULL, '_Legacy Roles', NULL
UNION SELECT 'Carrier', NULL, '_Legacy Roles', NULL
UNION SELECT 'Compliance', NULL, '_Legacy Roles', NULL
UNION SELECT 'Customer', NULL, '_Legacy Roles', NULL
UNION SELECT 'Driver', NULL, '_Legacy Roles', NULL
UNION SELECT 'Gauger', NULL, '_Legacy Roles', NULL
UNION SELECT 'Logistics', NULL, '_Legacy Roles', NULL
UNION SELECT 'Management', NULL, '_Legacy Roles', NULL
UNION SELECT 'Operations', NULL, '_Legacy Roles', NULL
UNION SELECT 'Producer', NULL, '_Legacy Roles', NULL
GO

-- Insert new categories and friendly names
UPDATE aspnet_Roles
SET Description = T.Description
  , Category = T.Category
  , FriendlyName = T.FriendlyName
FROM #TEMP T
WHERE T.RoleName = aspnet_Roles.RoleName
GO

DROP TABLE #TEMP
GO

/**********************************************************************************************************/



/* 5/31/16 - Drop "helper" procedures that I created just for use in this one-time transtion update */
DROP PROCEDURE spCreateGroupsForRoles
GO
DROP PROCEDURE spTransitionUserRolesToGroups
GO
DROP PROCEDURE spTransitionInternalRolesToGroups
GO



COMMIT
SET NOEXEC OFF