/* revert the Route.ActualMiles update logic to only update those orders that are not already settled
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.0.16', @NewVersion = '2.0.17'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 11 Sep 2013
-- Description:	trigger to ensure changes to Routes also update related (relevant) orders
-- =============================================
ALTER TRIGGER [dbo].[trigRoute_U] ON [dbo].[tblRoute] AFTER UPDATE AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE tblOrder
	  SET ActualMiles = R.ActualMiles
	FROM tblOrder O
	JOIN inserted R ON R.ID = O.RouteID
	LEFT JOIN tblOrderInvoiceCarrier OIC ON OIC.OrderID = O.ID
	LEFT JOIN tblOrderInvoiceCustomer OIS ON OIS.OrderID = O.ID
	WHERE OIC.BatchID IS NULL AND OIS.BatchID IS NULL
END
GO

COMMIT
SET NOEXEC OFF