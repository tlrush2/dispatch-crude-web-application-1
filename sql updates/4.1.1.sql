SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.1.0.4'
SELECT  @NewVersion = '4.1.1'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1719 Convert System Settings page to MVC'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/* Add new edit permission for settings page */
EXEC spAddNewPermission 'editSystemSettings','Allow user to edit System Settings page','System Settings','Edit'
GO


/* Add new description column for better description of what a setting is and/or does */
ALTER TABLE tblSetting
	ADD Description VARCHAR(256) NULL
GO


/* update T/F values for several settings (and one category) to ensure the settings are correct on all sites */
UPDATE tblSetting SET Value = 'TRUE' WHERE ID = 5
GO
UPDATE tblSetting SET Value = 'FALSE' WHERE ID = 24
GO
UPDATE tblSetting SET Value = 'TRUE' WHERE ID = 32
GO
UPDATE tblSetting SET Value = 'FALSE' WHERE ID = 35
GO
UPDATE tblSetting SET Value = 'FALSE' WHERE ID = 38
GO
UPDATE tblSetting SET Value = 'TRUE' WHERE ID = 44
GO
UPDATE tblSetting SET Value = 'TRUE' WHERE ID = 49
GO
UPDATE tblSetting SET Value = 'FALSE' WHERE ID = 51
GO
UPDATE tblSetting SET Category = 'Mobile App - All' WHERE ID = 52
GO
	
	
/* Set a few settings to read only (front end) so that users without db access cannot change them without talking to someone with db access first */	
UPDATE tblSetting SET ReadOnly = 1 WHERE ID IN (5,24,32,35,38,44,49,51,52) 	
GO


/* Add descriptions to most settings and change a few settings' categories per Maverick's request */
UPDATE tblSetting SET Description = 'Set to [-1] will set all entries to 12:00PM, [0] will set entries to NOW using the tablet system current time.  Anything greater than zero allows the user to adjust from NOW +/- the number of minutes in this setting.' WHERE ID = 41
GO
UPDATE tblSetting SET Description = 'Set to [-1] will set all entries to 12:00PM, [0] will set entries to NOW using the tablet system current time.  Anything greater than zero allows the user to adjust from NOW +/- the number of minutes in this setting.' WHERE ID = 50
GO
UPDATE tblSetting SET Description = 'When set to TRUE, orders will be automatically Audited (from Delivered status) unless there are errors that need to be corrected.' WHERE ID = 54
GO
UPDATE tblSetting SET Description = 'Maximum Allowable Data Entry for Driver App' WHERE ID = 2
GO
UPDATE tblSetting SET Description = 'Minimum Allowable Data Entry for Driver App' WHERE ID = 1
GO
UPDATE tblSetting SET Description = 'This is updated by our developers to tell us what version of the system we are currently running.' WHERE ID = 0
GO
UPDATE tblSetting SET Category = 'Legacy - To Be Removed', Description = 'Legacy - To Be Removed.' WHERE ID = 32
GO
UPDATE tblSetting SET Category = 'Legacy - To Be Removed', Description = 'Legacy - To Be Removed.' WHERE ID = 15
GO
UPDATE tblSetting SET Description = 'For Technical Troubleshooting Only.' WHERE ID = 38
GO
UPDATE tblSetting SET Description = 'Design the email body for email subscriptions of report center.' WHERE ID = 60
GO
UPDATE tblSetting SET Description = 'When set to TRUE, orders are trapped in the Approval process, but only when they have assessorial events that can create assessorial fees.' WHERE ID = 55
GO
UPDATE tblSetting SET Description = 'Prevents users from logging in on multiple devices without acknowledging that the previous session will be dropped.' WHERE ID = 47
GO
UPDATE tblSetting SET Description = 'Prevents users from logging in on multiple devices without acknowledging that the previous session will be dropped.' WHERE ID = 13
GO
UPDATE tblSetting SET Description = 'At a minimum, collect a GPS entry for every "n" number of meters outside (coarse) of the geofence.  (TIME)' WHERE ID = 26
GO
UPDATE tblSetting SET Description = 'At a minimum, collect a GPS entry for every "n" number of seconds outside (coarse) of the geofence.  (DISTANCE)' WHERE ID = 27
GO
UPDATE tblSetting SET Description = 'At a minimum, collect a GPS entry for every "n" number of meters inside (fine) of the geofence.  (TIME)' WHERE ID = 28
GO
UPDATE tblSetting SET Description = 'At a minimum, collect a GPS entry for every "n" number of seconds inside (fine) of the geofence.  (DISTANCE)' WHERE ID = 29
GO
UPDATE tblSetting SET Description = 'Within the range of the geofence, determined by this setting, switch from coarse to fine data collection.' WHERE ID = 30
GO
UPDATE tblSetting SET Description = 'This is the default geofence size setting when creating a new Origin, or bulk importing new origins.' WHERE ID = 31
GO
UPDATE tblSetting SET Description = 'This is the latest available version of the mobile app.  Mobile users get this info when checking to see what the latest version available is.' WHERE ID = 12
GO
UPDATE tblSetting SET Description = 'This is the latest available version of the mobile app.  Mobile users get this info when checking to see what the latest version available is.' WHERE ID = 46
GO
UPDATE tblSetting SET Description = 'If TRUE, users are forced to work on one order at a time.  If another order is selected, all others will be reset to Dispatched status.  This keeps the web user aware of which order they are currently working on.' WHERE ID = 43
GO
UPDATE tblSetting SET Description = 'Tolerance of variance between rate miles and actual driver miles.  Displays informational warning/notice to driver on odometer entry when [Require Truck Odometer Logs] = TRUE.' WHERE ID = 42
GO
UPDATE tblSetting SET Description = 'Populates the Driver app JSON log when turned on.  This is now always set to on so that problems can be found in the web log tool after they have happened instead of having to try to recreate the issues in house.' WHERE ID = 44
GO
UPDATE tblSetting SET Description = 'Populates the Gauger app JSON log when turned on.  This is now always set to on so that problems can be found in the web log tool after they have happened instead of having to try to recreate the issues in house.' WHERE ID = 49
GO
UPDATE tblSetting SET Category = 'Mobile App - All', Description = 'This value is the maximum difference allowed between temperature entries on mobile apps.' WHERE ID = 23
GO
UPDATE tblSetting SET Description = 'Minimum Allowable Driver App Version.  Older versions are no longer supported and users are forced to upgrade.' WHERE ID = 33
GO
UPDATE tblSetting SET Description = 'Minimum Allowable Gauger App Version.  Older versions are no longer supported and users are forced to upgrade.' WHERE ID = 48
GO
UPDATE tblSetting SET Description = 'Maximum Allowable Observed Gravity Data Entry for Driver App' WHERE ID = 4
GO
UPDATE tblSetting SET Description = 'Minimum Allowable Observed Gravity Data Entry for Driver App' WHERE ID = 3
GO
UPDATE tblSetting SET Description = 'Maximum Allowable Observed Temperature Data Entry for Driver App' WHERE ID = 20
GO
UPDATE tblSetting SET Description = 'Minimum Allowable Observed Temperature Data Entry for Driver App' WHERE ID = 19
GO
UPDATE tblSetting SET Description = 'To adjust the compromise between the quality/size of photo files and the amount of data being synced.  Adjust down when mobile users are taking lots of photos and having trouble syncing.' WHERE ID = 57
GO
UPDATE tblSetting SET Description = 'Legacy - To Be Removed.' WHERE ID = 14
GO
UPDATE tblSetting SET Description = 'This is the number of days our subscriber would like to be able to recover (undelete) deleted orders before they are purged from the database.' WHERE ID = 34
GO
UPDATE tblSetting SET Description = 'Default UOM used to standardize volumes in all reports unless otherwise specified within a specific report.' WHERE ID = 16
GO
UPDATE tblSetting SET Description = 'This should be categorized under Driver App Settings. When Set to TRUE, drivers are forced to enter odometer readings, and they must continually increment with reasonability.' WHERE ID = 39
GO
UPDATE tblSetting SET Description = 'Legacy - To Be Removed.' WHERE ID = 24
GO
UPDATE tblSetting SET Description = 'Legacy - To Be Removed.' WHERE ID = 51
GO
UPDATE tblSetting SET Description = 'When set to TRUE, we display the Rate Miles for the order on the Order Card for the DriverApp.' WHERE ID = 56
GO
UPDATE tblSetting SET Description = 'When users click "Deactivate Stale" on the Destination maintenance page, Destinations not used in an order within "n" number of days in recent history are considered stale and are deactivated.' WHERE ID = 22
GO
UPDATE tblSetting SET Description = 'When users click "Deactivate Stale" on the Origin maintenance page, Origins not used in an order within "n" number of days in recent history are considered stale and are deactivated.' WHERE ID = 21
GO
UPDATE tblSetting SET Description = 'Frequency of auto-sync when app is logged in but otherwise idle.  (Default 30)' WHERE ID = 11
GO
UPDATE tblSetting SET Description = 'Frequency of auto-sync when app is logged in but otherwise idle.  (Default 30)' WHERE ID = 45
GO
UPDATE tblSetting SET Description = 'This is the number of records that will display in a grid before a second grid page is generated.  For subscribers with large amounts of daily data a smaller setting improves overall site performance.  (Default 100)' WHERE ID = 37
GO
UPDATE tblSetting SET Description = 'Related to [Require Truck Mileage].' WHERE ID = 40
GO


COMMIT
SET NOEXEC OFF