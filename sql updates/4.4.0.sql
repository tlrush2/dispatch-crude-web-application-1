SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.3.3'
SELECT  @NewVersion = '4.4.0'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'Dispatch Messaging feature'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


IF OBJECT_ID('tblDispatchMessage', 'U') IS NOT NULL DROP TABLE tblDispatchMessage

CREATE TABLE tblDispatchMessage
(
	ID INT IDENTITY (1000,1) NOT NULL,
	UID UNIQUEIDENTIFIER NOT NULL CONSTRAINT DF_DispatchMessage_UID DEFAULT NEWID(),
	ThreadID INT, --default tie to ID of parent message
	FromUser VARCHAR(100) NOT NULL,
	ToUser VARCHAR(100) NOT NULL,
	Message VARCHAR(8000),
	Seen BIT NOT NULL CONSTRAINT DF_DispatchMessage_Seen DEFAULT 0,
	MessageDateUTC DATETIME NOT NULL CONSTRAINT DF_DispatchMessage_MessageDate DEFAULT GETUTCDATE()
)

GO


INSERT INTO tblSetting (ID, Name, SettingTypeID, Value, Category, CreateDateUTC, CreatedByUser, ReadOnly, Description)
SELECT 67, 'Enable Dispatch Messaging', 2, 'False', 'System Wide', GETUTCDATE(), 'System', 0, 'Turn on messaging capability between dispatch and drivers'
WHERE NOT EXISTS (SELECT 1 FROM tblSetting WHERE ID = 67 AND Name = 'Enable Dispatch Messaging')

INSERT INTO tblCarrierRuleType
SELECT 20, 'Driver Messaging history (days)', 3, 1, 'Number of days back to send dispatch messages (default is 30 days)'
WHERE NOT EXISTS (SELECT 1 FROM tblCarrierRuleType WHERE ID=20 AND Name = 'Driver Messaging history (days)')

GO


EXEC _spDropFunction fnDriverMessages
GO

/*************************************************************/
-- Date Created: 2016/10/14
-- Author: Joe Engler
-- Purpose: Get Driver Messages, last 30 days or whatever is in the carrier rule
-- Changes
/*************************************************************/
CREATE FUNCTION fnDriverMessages (@DriverID INT) RETURNS TABLE AS
RETURN
	SELECT m.* ,
			ToUserName = t.Value,
			FromUserName = f.Value
	  FROM tblDispatchMessage m
	  CROSS APPLY (

			SELECT du.UserNames, DaysBack = CAST(ISNULL(cr.Value, 30) AS INT)
			  FROM viewDriverBase d
			  JOIN tblCACHE_DriverUserNames du ON du.DriverID = d.ID
			 OUTER APPLY dbo.fnCarrierRules(GETUTCDATE(), null, 20, d.CarrierID, d.ID, d.OperatingStateID, d.RegionID, 1) cr -- Driver Messaging History
			 WHERE du.DriverID = @DriverID
	 ) q
	 cross apply (select * from fnUserAllProfileValues(m.ToUser) where Name = 'FullName') t
	 cross apply (select * from fnUserAllProfileValues(m.FromUser) where Name = 'FullName') f
	WHERE MessageDateUTC >= DATEADD(DAY, -DaysBack, GETUTCDATE())
      AND (','+UserNames+',' LIKE '%,'+ToUser+',%' OR ','+UserNames+',' LIKE '%,'+FromUser+',%') -- wrap in commas to handle multiple users using the same DriverID

GO


EXEC _spDropProcedure spMessageDrivers
GO

/*************************************************************/
-- Date Created: 2016/10/14
-- Author: Joe Engler
-- Purpose: Send blast email to drivers based on filter criteria
-- Changes
/*************************************************************/
CREATE PROCEDURE spMessageDrivers(@Message VARCHAR(8000), @CarrierID INT = null, @DriverGroupID INT = null, @DriverID INT = null, @RegionID INT = null, @StateID INT = null, @Username VARCHAR(100))
AS BEGIN

	INSERT INTO tblDispatchMessage
		SELECT NEWID(), null, @Username, cs.value, @Message, 0, GETUTCDATE()
		FROM viewDriverBase d 
		JOIN tblCACHE_DriverUserNames du ON du.DriverID = d.ID
		OUTER APPLY dbo.fnSplitString(du.UserNames, ',') cs
		WHERE (ISNULL(@CarrierID, 0) <= 0 OR d.CarrierID = @CarrierID)
		AND (ISNULL(@DriverGroupID, 0) <= 0 OR d.DriverGroupID = @DriverGroupID)
		AND (ISNULL(@DriverID, 0) <= 0 OR d.ID = @DriverID)
		AND (ISNULL(@RegionID, 0) <= 0 OR d.RegionID = @RegionID)
		AND (ISNULL(@StateID, 0) <= 0 OR d.OperatingStateID = @StateID)

END

GO


/* Add new permissions for the Producer maintenance page */
EXEC spAddNewPermission 'sendDispatchMessages', 'Allow users to send dispatch messages to drivers', 'Dispatch', 'Messaging'
GO

COMMIT
SET NOEXEC OFF
