-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.10.12'
SELECT  @NewVersion = '3.10.12.1'

-- only ensure the curr version matches when we are not testing with an "x.x.x" DEV version
IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'DCWEB-1118: Fixed password reset bug'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/*******************************************/
-- Date Created: 22 Apr 2013
-- Author: Kevin Alons
-- Purpose: provide a way to reset the existing Password Hash value for a Driver (when the acct password is changed)
-- Changes:
--        3.10.12.1 - JAE+GM - Update Password reset to include driver equipment fields for a valid sync (overlooked when added originally)
/*******************************************/
ALTER PROCEDURE [dbo].[spDriver_ResetPasswordHash]
(
  @UserName varchar(100)
, @DriverID int
, @Valid bit = NULL output
, @Message varchar(255) = NULL output
) AS BEGIN
	SELECT @Valid = (SELECT COUNT(*) FROM tblDriver WHERE ID = @DriverID)
	IF (@Valid = 0)
		SELECT @Message = 'DriverID was not valid'
	ELSE BEGIN
		-- if a sync record already exists, just update the new LastSync value
		UPDATE tblDriver_Sync SET PasswordHash = dbo.fnGeneratePasswordHash() WHERE DriverID = @DriverID
		-- return the current "Master" data
		SELECT *, TabletID = NULL, PrinterSerial = NULL FROM dbo.fnDriverMasterData(@Valid, @DriverID, @UserName)
	END		
END


GO


COMMIT
SET NOEXEC OFF