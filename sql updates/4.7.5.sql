SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.7.4'
SELECT  @NewVersion = '4.7.5'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'JT-4492 - prevent deactivating Driver Group with assigned (active) Drivers'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/*****************************************
  Created: x.x.x - 2017.06.08 - KDA
  Purpose: return the tblDriverGroup table + related assigned Driver metrics
  Changes:
*****************************************/
CREATE VIEW viewDriverGroup AS
	SELECT *
		, Locked = cast(AssignedUserCount as bit)
	FROM (
		SELECT DG.*
			, AssignedUserCount = (SELECT count(*) FROM tblDriver WHERE DriverGroupID = DG.ID AND DeleteDateUTC IS NULL)
		FROM tblDriverGroup DG
	) X
GO

COMMIT
SET NOEXEC OFF