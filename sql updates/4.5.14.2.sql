SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.5.14.1'
SELECT  @NewVersion = '4.5.14.2'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Add Dest Wait Reason to ImportCenter'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


SET IDENTITY_INSERT tblObjectField ON
INSERT INTO tblObjectField (ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, ReadOnly, AllowNullID, IsKey, IsCustom, ParentObjectID, ParentObjectIDFieldName, CreateDateUTC, CreatedByUser)
SELECT 471, 1, 'DestWaitReasonID', 'Dest Wait Reason', 3, NULL, 0, 1, 0, 0, 32, 'ID', GETUTCDATE(), 'System'
WHERE NOT EXISTS (SELECT 1 FROM tblObjectField WHERE ID = 471)
SET IDENTITY_INSERT tblObjectField OFF

GO


COMMIT
SET NOEXEC OFF