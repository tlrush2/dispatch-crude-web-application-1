SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.1.3.2'
SELECT  @NewVersion = '4.1.3.3'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1755: Add driving directions fields to truck order create and dispatch pages'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO



/* 
Driving directions were originally added as TEXT data type which is depreciated and actually causes an error when you rebuild our stored
procedures.  I'm changing the fields datatypes to varchar(max) to avoid this problem going forward
*/
ALTER TABLE tblOrigin ALTER COLUMN DrivingDirections VARCHAR(MAX) NULL
GO
ALTER TABLE tblDestination ALTER COLUMN DrivingDirections VARCHAR(MAX) NULL
GO



/***************************************/
-- Date Created: ?.?.? - 2012.11.25 - Kevin Alons
-- Purpose: return Order records with "translated friendly" values for FK relationships
-- Changes:
-- 3.9.20  - 2015/10/22 - KDA	- add Origin|Dest DriverID fields (using new tblOrderTransfer table) 
--			2015/10/28  - JAE	- Added all Order Transfer fields for ease of use in reporting
--			2015/11/03  - BB		- added cast to make TransferComplet BIT type to avoid "Operand type clash" error when running this update script
-- 3.9.21  - 2015/11/03 - KDA	- add OriginDriverGroupID field (from OrderTransfer.OriginDriverID JOIN)
-- 3.9.25  - 2015/11/10 - JAE	- added origin driver and truck to view
-- 3.10.10 - 2016/02/15 - BB	- Add driver region ID to facilitate new user/region filtering feature
-- 3.10.11 - 2016/02/24 - JAE	- Add destination region (name) to view
-- 3.10.13 - 2016/02/29 - JAE	- Add TruckType
-- 3.13.8  - 2016/07/26	- BB	- Add Destination Station
-- 4.1.0.2 - 2016.08.28 - KDA	- rewrite RerouteCount, TicketCount to use a normal JOIN (instead of uncorrelated sub-query)
--								- use viewDriverBase instead of viewDriver
--								- eliminate viewGaugerBase (was unused), use tblOrderStatus instead of viewOrderPrintStatus
-- 4.1.3.3 - 2016/09/14 - BB	- Add origin and destination driving directions (initially for dispatch and truck order create pages)
/***************************************/
ALTER VIEW [dbo].[viewOrder] AS
SELECT O.*
	-- GENERATED orders that were GAUGED to COMPLETION or SKIPPED will show "Gauger [gauger status]"
	, PrintStatus = CASE WHEN O.StatusID = -10 AND GOS.IsComplete = 1 THEN 'Gauger ' + GOS.Name ELSE OPS.OrderStatus END
FROM (
	SELECT O.*
	, Origin = vO.Name 
	, OriginFull = vO.FullName 
	, OriginState = vO.State 
	, OriginStateAbbrev = vO.StateAbbrev 
	, OriginStation = vO.Station
	, OriginCounty = vO.County 
	, OriginDrivingDirections = vO.DrivingDirections	-- 4.1.3.3
	, vO.LeaseName
	, vO.LeaseNum
	, OriginRegion = vO.Region
	, OriginCountryID = vO.CountryID
	, OriginCountry = vO.Country
	, OriginCountryShort = vO.CountryShort
	, OriginLegalDescription = vO.LegalDescription 
	, OriginNDIC = vO.NDICFileNum 
	, OriginNDM = vO.NDM 
	, OriginCA = vO.CA 
	, OriginTimeZoneID = vO.TimeZoneID 
	, OriginUseDST = vO.UseDST 
	, vO.H2S
	, Destination = vD.Name 
	, DestinationFull = vD.FullName 
	, DestinationState = vD.State 
	, DestinationDrivingDirections = vD.DrivingDirections	-- 4.1.3.3
	, DestinationStateAbbrev = vD.StateAbbrev 
	, DestinationTypeID = vD.ID
	, DestinationStation = vD.Station -- 3.13.8
	, vD.DestinationType
	, DestStation = vD.Station 
	, DestTimeZoneID = vD.TimeZoneID 
	, DestUseDST = vD.UseDST 
	, DestRegion = vD.Region -- 3.10.13
	, DestCountryID = vD.CountryID
	, DestCountry = vD.Country
	, DestCountryShort = vD.CountryShort
	, Customer = C.Name 
	, Carrier = CA.Name 
	, CarrierType = CT.Name 
	, OS.OrderStatus
	, OS.StatusNum
	, Driver = D.FullName 
	, DriverFirst = D.FirstName 
	, DriverLast = D.LastName 
	, DriverRegionID = D.RegionID  -- 3.10.10
	, Truck = TRU.FullName 
	, Trailer = TR1.FullName 
	, Trailer2 = TR2.FullName 
	, P.PriorityNum
	, TicketType = TT.Name 
	, DestTicketTypeID = vD.TicketTypeID 
	, DestTicketType = vD.TicketType 
	, Operator = OP.Name 
	, Producer = PR.Name 
	, Pumper = PU.FullName 
	, DriverNumber = D.IDNumber
	, CarrierNumber = CA.IDNumber
	, CarrierTypeID = CA.CarrierTypeID
	, TruckNumber = TRU.IDNumber
	, TruckType = TRU.TruckType 
	, TrailerNumber = TR1.IDNumber 
	, Trailer2Number = TR2.IDNumber 
	, Product = PRO.Name
	, ProductShort = PRO.ShortName 
	, PRO.ProductGroup
	, OriginUOM = OUom.Name 
	, OriginUomShort = OUom.Abbrev 
	, OriginTankID_Text = coalesce(ORT.TankNum, O.OriginTankNum, '?')
	, DestUOM = DUom.Name 
	, DestUomShort = DUom.Abbrev 
	, Active = CAST((CASE WHEN O.DeleteDateUTC IS NULL THEN 1 ELSE 0 END) AS BIT) 
	, IsDeleted = CAST((CASE WHEN O.DeleteDateUTC IS NOT NULL THEN 1 ELSE 0 END) AS BIT) 
	, PickupPrintStatus = PPS.Name 
	, PickupCompleted = PPS.IsCompleted 
	, DeliverPrintStatus = DPS.Name 
	, DeliverCompleted = DPS.IsCompleted 
	, PrintStatusID = CASE WHEN O.StatusID = -9 THEN ISNULL(-GAO.StatusID, O.StatusID)
						   WHEN O.StatusID = 8 AND PPS.IsCompleted = 0 THEN 7
						   WHEN O.StatusID = 3 AND DPS.IsCompleted = 0 THEN 8
						ELSE O.StatusID END 
	, OriginTankText = CASE WHEN O.OriginTankID IS NULL OR ORT.TankNum = '*' THEN O.OriginTankNum ELSE ORT.TankNum END
	, OriginWaitNum = OWR.Num
	, OriginWaitDesc = OWR.Description
	, OriginWaitNumDesc = OWR.NumDesc
	, DestWaitNum = DWR.Num
	, DestWaitDesc = DWR.Description
	, DestWaitNumDesc = DWR.NumDesc
	, RejectNum = ORR.Num
	, RejectDesc = ORR.Description
	, RejectNumDesc = ORR.NumDesc
	, ORE.RerouteCount
	, OT.TicketCount
	, TotalMinutes = ISNULL(OriginMinutes, 0) + ISNULL(DestMinutes, 0)
	-- TRANSFER FIELDS
	, OriginDriverID = ISNULL(OTR.OriginDriverID, O.DriverID)
	, OriginDriverGroupID = ISNULL(ODG.ID, DDG.ID) 
	, OriginDriverGroup = ISNULL(ODG.Name, DDG.Name)
	, OriginDriver = ISNULL(vODR.FullName, vDDR.FullName)
	, OriginDriverFirst = ISNULL(vODR.FirstName, vDDR.FirstName)
	, OriginDriverLast = ISNULL(vODR.LastName, vDDR.LastName)
	, OriginTruckID = ISNULL(OTR.OriginTruckID, O.TruckID)
	, OriginTruck = ISNULL(vOTRU.FullName, vDTRU.FullName)
	, DestDriverID = O.DriverID
	, DestDriver = D.FullName
	, DestDriverFirst = D.FirstName 
	, DestDriverLast = D.LastName 
	, DestTruckID = O.TruckID
	, DestTruck = TRU.FullName 
	, OriginTruckEndMileage = OTR.OriginTruckEndMileage
	, DestTruckStartMileage = OTR.DestTruckStartMileage
	, TransferPercent = OTR.PercentComplete
	, TransferNotes = OTR.Notes
	, TransferDateUTC = OTR.CreateDateUTC
	, TransferComplete = CAST(OTR.TransferComplete AS BIT)
	, IsTransfer = CAST((CASE WHEN OTR.OrderID IS NOT NULL THEN 1 ELSE 0 END) AS BIT)	
	-- 
	, GaugerStatusID = GAO.StatusID
	, GaugerID = GAO.GaugerID	
/* 
*/
	FROM dbo.viewOrderBase O
	JOIN dbo.tblOrderStatus OS ON OS.ID = O.StatusID
	LEFT JOIN (SELECT OrderID, TicketCount = COUNT(1) FROM tblOrderTicket OT WHERE OT.DeleteDateUTC IS NULL GROUP BY OrderID) OT ON OT.OrderID = O.ID
	LEFT JOIN (SELECT OrderID, RerouteCount = COUNT(1) FROM tblOrderReroute GROUP BY OrderID) ORE ON ORE.OrderID = O.ID
	LEFT JOIN dbo.tblOrderTransfer OTR ON OTR.OrderID = O.ID
	LEFT JOIN dbo.viewOrigin vO ON vO.ID = O.OriginID
	LEFT JOIN dbo.viewDestination vD ON vD.ID = O.DestinationID
	LEFT JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
	LEFT JOIN dbo.tblCarrier CA ON CA.ID = O.CarrierID
	LEFT JOIN dbo.tblCarrierType CT ON CT.ID = CA.CarrierTypeID
	LEFT JOIN dbo.viewDriverBase D ON D.ID = O.DriverID
	LEFT JOIN dbo.viewDriverBase vODR ON vODR.ID = OTR.OriginDriverID
	LEFT JOIN dbo.viewDriver vDDR ON vDDR.ID = O.DriverID
	LEFT JOIN dbo.tblDriverGroup ODG ON ODG.ID = vODR.DriverGroupID
	LEFT JOIN dbo.tblDriverGroup DDG ON DDG.ID = O.DriverGroupID
	LEFT JOIN dbo.viewTruck TRU ON TRU.ID = O.TruckID
	LEFT JOIN dbo.viewTruck vOTRU ON vOTRU.ID = OTR.OriginTruckID
	LEFT JOIN dbo.viewTruck vDTRU ON vDTRU.ID = O.TruckID
	LEFT JOIN dbo.viewTrailer TR1 ON TR1.ID = O.TrailerID
	LEFT JOIN dbo.viewTrailer TR2 ON TR2.ID = O.Trailer2ID
	LEFT JOIN dbo.tblPriority P ON P.ID = O.PriorityID
	LEFT JOIN dbo.tblTicketType TT ON TT.ID = O.TicketTypeID
	LEFT JOIN dbo.tblOriginTank ORT ON ORT.ID = O.OriginTankID
	LEFT JOIN dbo.tblOperator OP ON OP.ID = O.OperatorID
	LEFT JOIN dbo.viewPumper PU ON PU.ID = O.PumperID
	LEFT JOIN dbo.tblProducer PR ON PR.ID = O.ProducerID
	LEFT JOIN dbo.viewProduct PRO ON PRO.ID = O.ProductID
	LEFT JOIN dbo.tblUom OUom ON OUom.ID = O.OriginUomID
	LEFT JOIN dbo.tblUom DUom ON DUom.ID = O.DestUomID
	LEFT JOIN dbo.tblPrintStatus PPS ON PPS.ID = O.PickupPrintStatusID
	LEFT JOIN dbo.tblPrintStatus DPS ON DPS.ID = O.DeliverPrintStatusID
	LEFT JOIN dbo.viewOriginWaitReason OWR ON OWR.ID = O.OriginWaitReasonID
	LEFT JOIN dbo.viewDestinationWaitReason DWR ON DWR.ID = O.DestWaitReasonID
	LEFT JOIN dbo.viewOrderRejectReason ORR ON ORR.ID = O.RejectReasonID	
	LEFT JOIN dbo.tblGaugerOrder GAO ON GAO.OrderID = O.ID
) O
LEFT JOIN dbo.tblOrderStatus OPS ON OPS.ID = O.PrintStatusID
LEFT JOIN dbo.tblGaugerOrderStatus GOS ON GOS.ID = O.GaugerStatusID
GO



/* Since this is viewOrder, the base of a whole lot of things, I'm not taking any chances that dependent views/SP's/etc. do not get updated/refreshed  :) */
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRecompileAllStoredProcedures
GO
EXEC _spRebuildAllObjects



COMMIT
SET NOEXEC OFF