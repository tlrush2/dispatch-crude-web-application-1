SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.0.7'
SELECT  @NewVersion = '4.0.8'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1698 Convert carrier types page to MVC'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

-- Add Delete columns to tblCarrierType
ALTER TABLE tblCarrierType
	ADD DeleteDateUTC SMALLDATETIME NULL
		, DeletedByUser VARCHAR(100) NULL
GO
	
	
-- Update existing permission to fit with new permissions below
UPDATE aspnet_Roles
SET Description = 'Allow user to view Carrier Types page'
	, Category = 'System Data Types - Carriers'
	, FriendlyName = 'View'
WHERE RoleName = 'viewCarrierTypes'
GO


-- Add new carrier types page permissions		
EXEC spAddNewPermission 'createCarrierTypes','Allow user to create Carrier Types','System Data Types - Carriers','Create'
GO
EXEC spAddNewPermission 'editCarrierTypes','Allow user to edit Carrier Types','System Data Types - Carriers','Edit'
GO
EXEC spAddNewPermission 'deactivateCarrierTypes','Allow user to deactivate Carrier Types','System Data Types - Carriers','Deactivate'
GO



COMMIT
SET NOEXEC OFF