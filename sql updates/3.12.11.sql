﻿-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.12.10'
SELECT  @NewVersion = '3.12.11'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
  DECLARE @msg varchar(255)
  SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
  RAISERROR(@msg, 16, 1)
  SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
  SELECT @NewVersion, 1, 'DCWEB-1530 - Dispatch map'
  EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

-- Add new permission for dispatch map page
INSERT INTO aspnet_Roles (ApplicationId, RoleId, RoleName, LoweredRoleName, Description)
SELECT '446467D6-39CD-45E3-B3E0-CAC7945AF3E8', NEWID(), 'viewDispatchMap', 'viewdispatchmap', 'Allow user to see the Dispatch Map page'
EXCEPT SELECT ApplicationId, RoleId, RoleName, LoweredRoleName, Description
FROM aspnet_Roles
GO

-- Add new permission to Administrators
EXEC spAddUserPermissionsByRole 'Administrator', 'viewDispatchMap'
GO


COMMIT
SET NOEXEC OFF