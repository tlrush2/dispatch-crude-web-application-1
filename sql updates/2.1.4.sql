/* add new 'Basic Run' Ticket Types (both Origin & Destination)
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.1.3', @NewVersion = '2.1.4'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

SET IDENTITY_INSERT tblTicketType ON

INSERT INTO tblTicketType (ID, Name, CreateDateUTC, CreatedByUser) VALUES (4, 'Basic Run', GETUTCDATE(), 'System')

SET IDENTITY_INSERT tblTicketType OFF

COMMIT
SET NOEXEC OFF

select * from tbldesttickettype