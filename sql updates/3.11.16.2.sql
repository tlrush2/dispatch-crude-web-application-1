-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.11.16.1'
SELECT  @NewVersion = '3.11.16.2'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1248: Added deactivate/reactivate/locking functionality to Carrier Compliance Document Types page'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO



/******************************************************
-- Date Created: 21 April 2016
-- Author: Jeremiah Silliman
-- Purpose: Created View for CarrierDocumentTypes.aspx to enable locking and unlocking of items on page
-- Changes:
******************************************************/
CREATE VIEW [dbo].[viewCarrierDocumentTypes] AS
	SELECT X.*
		, Locked = cast(CASE WHEN ID IN (SELECT DocumentTypeID FROM tblCarrierDocument) THEN 1 ELSE 0 END as bit)
	FROM tblCarrierDocumentType X
GO


COMMIT 
SET NOEXEC OFF