SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.5.23.2'
SELECT  @NewVersion = '4.6.0'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'JT-1486 - Expansion of HOS table for ELD mandate'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/******************************
*			CREATE TABLES
******************************/
--select * from tblHosStatus
CREATE TABLE tblELDEventType
(
	ID INT NOT NULL CONSTRAINT PK_ELDEventType PRIMARY KEY CLUSTERED
	, Name VARCHAR(100) NOT NULL
	, ShortName VARCHAR(40) NULL
)
GO

INSERT INTO tblELDEventType
	SELECT 1, 'A change in driver''s duty-status', 'Status Change'
	UNION
	SELECT 2, 'An intermediate log', 'Regular Check'
	UNION
	SELECT 3, 'A change in driver''s indication of authorized personal use of CMV or yard moves', 'Personal Use/Yard Move Change'
	UNION
	SELECT 4, 'A driver''s certification/re-certification of records', 'Certification/Recertification'
	UNION
	SELECT 5, 'A driver''s login/logout activity', 'Login/Logout'
	UNION
	SELECT 6, 'CMV''s engine power up / shut down activity', 'Engine On/Off'
	UNION
	SELECT 7, 'A malfunction or data diagnostic detection occurrence', 'Error/Diagnostic'
GO


CREATE TABLE tblELDEventRecordOrigin
(
	ID INT NOT NULL CONSTRAINT PK_ELDEventRecordOrigin PRIMARY KEY CLUSTERED
	, Name VARCHAR(100) NOT NULL
	, ShortName VARCHAR(40) NULL
)
GO

INSERT INTO tblELDEventRecordOrigin
	SELECT 1, 'Automatically recorded by ELD', 'ELD'
	UNION
	SELECT 2, 'Edited or entered by the driver', 'Driver'
	UNION
	SELECT 3, 'Edit requested by an authenticated user other than the driver', 'Other'
	UNION
	SELECT 4, 'Assumed from unidentified driver profile', 'Unidentified Driver'
GO


CREATE TABLE tblELDEventRecordStatus
(
	ID INT NOT NULL CONSTRAINT PK_ELDEventRecordStatus PRIMARY KEY CLUSTERED
	, Name VARCHAR(40) NOT NULL
)
GO

INSERT INTO tblELDEventRecordStatus
SELECT 1, 'Active'
UNION
SELECT 2, 'Inactive - Changed'
UNION
SELECT 3, 'Inactive - Change Requested'
UNION
SELECT 4, 'Inactive - Change Rejected'
-- approved by driver, awaiting codriver
-- approved by codriver, awaiting driver
GO


CREATE TABLE tblELDDiagnosticCode
(
	ID INT NOT NULL CONSTRAINT PK_ELDDiagnosticCode PRIMARY KEY CLUSTERED
	, Code CHAR(1) NOT NULL
	, Description VARCHAR(100) NULL
	, IsMalfunction BIT NOT NULL
)
GO

INSERT INTO tblELDDiagnosticCode
	SELECT 1, '1', '"Power data diagnostic" event', 0
	UNION
	SELECT 2, '2', '"Engine synchronization data diagnostic" event', 0
	UNION
	SELECT 3, '3', '"Missing required data elements data diagnostic" event', 0
	UNION
	SELECT 4, '4', '"Data transfer data diagnostic" event', 0
	UNION
	SELECT 5, '5', '"Unidentified driving record data diagnostic" event', 0
	UNION
	SELECT 6, '6', '"Other" ELD identified diagnostic event', 0

	UNION

	SELECT 7, 'P', '"Power compliance" malfunction', 1
	UNION
	SELECT 8, 'E', '"Engine synchronization compliance" malfunction', 1
	UNION
	SELECT 9, 'T', '"Timing compliance" malfunction', 1
	UNION
	SELECT 10, 'L', '"Positioning compliance" malfunction', 1
	UNION
	SELECT 11, 'R', '"Data recording compliance" malfunction', 1
	UNION
	SELECT 12, 'S', '"Data transfer compliance" malfunction', 1
	UNION
	SELECT 13, 'O', '"Other" ELD detected malfunction', 1
GO

CREATE TABLE tblELDDriverDrivingCategory
(
	ID INT NOT NULL CONSTRAINT PK_ELDDriverDrivingCategory PRIMARY KEY CLUSTERED
	, Name VARCHAR(40) NOT NULL
	, Abbreviation VARCHAR(5) NOT NULL
)
GO

INSERT INTO tblELDDriverDrivingCategory
	SELECT 1, 'Authorized Personal Use of CMV', 'PC'
	UNION
	SELECT 2, 'Yard Moves', 'YM'
	-- convert personal use and yard move flags (bits) to driving category (FK)?
GO



/******************************
*			ALTER TABLES
******************************/

ALTER TABLE tblCarrier ALTER COLUMN Name VARCHAR(120) NOT NULL
GO

ALTER TABLE tblHosDriverStatus ADD Abbreviation VARCHAR(5)
GO

ALTER TABLE tblHosSignature ADD PRIMARY KEY(ID)
GO


-- -----------------------------------------
-- ALTER DRIVER
-- -----------------------------------------

ALTER TABLE tblDriver ALTER COLUMN FirstName VARCHAR(30) NOT NULL
GO
ALTER TABLE tblDriver ALTER COLUMN LastName VARCHAR(30) NOT NULL
GO
ALTER TABLE tblDriver ADD HOSExempt BIT NOT NULL CONSTRAINT DF_Driver_HOSExempt DEFAULT 0
GO
ALTER TABLE tblDriver ADD HOSExemptionNotes VARCHAR(200)
GO
ALTER TABLE tblDriver ADD HOSAllowPersonalChoice BIT CONSTRAINT DF_Driver_HOSAllowPersonalChoice DEFAULT 0
GO
ALTER TABLE tblDriver ADD HOSAllowYardMove BIT CONSTRAINT DF_Driver_HOSAllowYardMove DEFAULT 0
GO


-- -----------------------------------------
-- ALTER HOS
-- -----------------------------------------
-- ALTER
ALTER TABLE tblHOS ALTER COLUMN DriverID INT -- Allow for UNIDENTIFIED DRIVER
GO
-- ADD
ALTER TABLE tblHOS ADD ELDUserName VARCHAR(60)
GO
ALTER TABLE tblHOS ADD DriverTimeZoneOffset INT
GO
ALTER TABLE tblHOS ADD CoDriverID INT CONSTRAINT FK_ELDEvent_CoDriver REFERENCES tblDriver(ID)
GO
ALTER TABLE tblHOS ADD HosEventTypeID INT CONSTRAINT FK_ELDEvent_HosEventType REFERENCES tblELDEventType(ID)
GO
ALTER TABLE tblHOS ADD HosEventCode CHAR -- AKA Subtype
GO
ALTER TABLE tblHOS ADD DistanceSinceLastValidCoordinates TINYINT
GO
ALTER TABLE tblHOS ADD EventDataCheckValue VARBINARY(1)
GO
ALTER TABLE tblHOS ADD EventRecordOriginID INT NOT NULL CONSTRAINT FK_ELDEvent_EventRecordOrigin REFERENCES tblELDEventRecordOrigin(ID) 
														CONSTRAINT DF_ELDEvent_EventRecordOrigin DEFAULT 2
GO
ALTER TABLE tblHOS ADD EventRecordStatusID INT NOT NULL CONSTRAINT FK_ELDEvent_EventRecordStatus REFERENCES tblELDEventRecordStatus(ID)
														CONSTRAINT DF_ELDEvent_EventRecordStatus DEFAULT 1
GO
ALTER TABLE tblHOS ADD EventSequenceNumber VARBINARY(2)
GO
ALTER TABLE tblHOS ADD LocationDescription VARCHAR(60) -- From driver when auto fails
GO
ALTER TABLE tblHOS ADD Geolocation VARCHAR(60) -- From ELD
GO
ALTER TABLE tblHOS ADD VIN VARCHAR(17)
GO
ALTER TABLE tblHOS ADD EngineHours DECIMAL(6,1)
GO
ALTER TABLE tblHOS ADD VehicleMiles INT
GO
ALTER TABLE tblHOS ADD DataDiagnosticEventIndicator BIT NOT NULL CONSTRAINT DF_ELDEvent_DataDiagnosticEventIndicator DEFAULT 0
GO
ALTER TABLE tblHOS ADD MalfunctionIndicator BIT NOT NULL CONSTRAINT DF_ELDEvent_MalfunctionIndicator DEFAULT 0
GO
ALTER TABLE tblHOS ADD DiagnosticCodeID INT CONSTRAINT FK_ELDEvent_DiagnosticCode REFERENCES tblELDDiagnosticCode(ID)
GO
ALTER TABLE tblHOS ADD ELDIdentifier VARCHAR(6) 
GO
ALTER TABLE tblHOS ADD ELDAuthenticationValue VARCHAR(32)
GO
ALTER TABLE tblHOS ADD HosParentUID UNIQUEIDENTIFIER  -- ties to UID (mobile app key) for tracking original record
GO
ALTER TABLE tblHOS ADD HosSignatureUID UNIQUEIDENTIFIER -- ties to UID of HOS signature (mobile app key) for certification (no foreign key because it would be too much to maintain)
GO







/******************************
*			UPDATE TABLES
******************************/

UPDATE tblState 
	SET Abbreviation = 'NF'
	, LastChangedByUser = 'System'
	, LastChangeDateUTC = GETUTCDATE() 
WHERE ID = 56 -- newfoundland 
GO


INSERT INTO tblCountry
	VALUES (3, 'Mexico', 'MEX', 'Celsius', 'C')
GO


SET IDENTITY_INSERT tblState ON
INSERT INTO tblState (ID, Abbreviation, FullName, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, EIAPADDRegionID, CountryID)
	SELECT 65, 'AG', 'Aguascalientes', GETUTCDATE(), 'System', NULL, NULL, NULL, 3
	UNION
	SELECT 66, 'BN', 'Baja California Norte', GETUTCDATE(), 'System', NULL, NULL, NULL, 3
	UNION
	SELECT 67, 'BS', 'Baja California Sur', GETUTCDATE(), 'System', NULL, NULL, NULL, 3
	UNION
	SELECT 68, 'CP', 'Campeche', GETUTCDATE(), 'System', NULL, NULL, NULL, 3
	UNION
	SELECT 69, 'CS', 'Chiapas', GETUTCDATE(), 'System', NULL, NULL, NULL, 3
	UNION
	SELECT 70, 'CI', 'Chihuahua', GETUTCDATE(), 'System', NULL, NULL, NULL, 3
	UNION
	SELECT 71, 'CH', 'Coahuila', GETUTCDATE(), 'System', NULL, NULL, NULL, 3
	UNION
	SELECT 72, 'CL', 'Colima', GETUTCDATE(), 'System', NULL, NULL, NULL, 3
	UNION
	SELECT 73, 'DF', 'Distrito Federal', GETUTCDATE(), 'System', NULL, NULL, NULL, 3
	UNION
	SELECT 74, 'DG', 'Durango', GETUTCDATE(), 'System', NULL, NULL, NULL, 3
	UNION
	SELECT 75, 'GJ', 'Guanajuato', GETUTCDATE(), 'System', NULL, NULL, NULL, 3
	UNION
	SELECT 76, 'GE', 'Guerrero', GETUTCDATE(), 'System', NULL, NULL, NULL, 3
	UNION
	SELECT 77, 'HD', 'Hidalgo', GETUTCDATE(), 'System', NULL, NULL, NULL, 3
	UNION
	SELECT 78, 'JA', 'Jalisco', GETUTCDATE(), 'System', NULL, NULL, NULL, 3
	UNION
	SELECT 79, 'MX', 'Mexico', GETUTCDATE(), 'System', NULL, NULL, NULL, 3
	UNION
	SELECT 80, 'MC', 'Michoacan', GETUTCDATE(), 'System', NULL, NULL, NULL, 3
	UNION
	SELECT 81, 'MR', 'Morelos', GETUTCDATE(), 'System', NULL, NULL, NULL, 3
	UNION
	SELECT 82, 'NA', 'Nayarit', GETUTCDATE(), 'System', NULL, NULL, NULL, 3
	UNION
	SELECT 83, 'NL', 'Nuevo Leon', GETUTCDATE(), 'System', NULL, NULL, NULL, 3
	UNION
	SELECT 84, 'OA', 'Oaxaca', GETUTCDATE(), 'System', NULL, NULL, NULL, 3
	UNION
	SELECT 85, 'PU', 'Puebla', GETUTCDATE(), 'System', NULL, NULL, NULL, 3
	UNION
	SELECT 86, 'QE', 'Queretaro', GETUTCDATE(), 'System', NULL, NULL, NULL, 3
	UNION
	SELECT 87, 'QI', 'Quintana Roo', GETUTCDATE(), 'System', NULL, NULL, NULL, 3
	UNION
	SELECT 88, 'SL', 'San Luis Potosi', GETUTCDATE(), 'System', NULL, NULL, NULL, 3
	UNION
	SELECT 89, 'SI', 'Sinaloa', GETUTCDATE(), 'System', NULL, NULL, NULL, 3
	UNION
	SELECT 90, 'SO', 'Sonora', GETUTCDATE(), 'System', NULL, NULL, NULL, 3
	UNION
	SELECT 91, 'TB', 'Tabasco', GETUTCDATE(), 'System', NULL, NULL, NULL, 3
	UNION
	SELECT 92, 'TA', 'Tamaulipas', GETUTCDATE(), 'System', NULL, NULL, NULL, 3
	UNION
	SELECT 93, 'TL', 'Tlaxcala', GETUTCDATE(), 'System', NULL, NULL, NULL, 3
	UNION
	SELECT 94, 'VC', 'Veracruz', GETUTCDATE(), 'System', NULL, NULL, NULL, 3
	UNION
	SELECT 95, 'YU', 'Yucatan', GETUTCDATE(), 'System', NULL, NULL, NULL, 3
	UNION
	SELECT 96, 'ZA', 'Zacatecas', GETUTCDATE(), 'System', NULL, NULL, NULL, 3
SET IDENTITY_INSERT tblState OFF
GO


UPDATE tblHosDriverStatus SET Abbreviation = 'OFF' WHERE ID = 1
GO

UPDATE tblHOS SET LocationDescription = NearestCity	
GO

UPDATE tblHosDriverStatus 
	SET Abbreviation = 'SB'
	, Name = 'Sleeper Berth' 
WHERE ID = 2
GO

UPDATE tblHosDriverStatus 
	SET Abbreviation = 'D'
	, Name = 'Driving' 
WHERE ID = 3
GO

UPDATE tblHosDriverStatus 
	SET Abbreviation = 'ON'
	, Name = 'On-duty Not Driving' 
WHERE ID = 4
GO

UPDATE tblCountry 
	SET Abbrev = 'USA'
	, UOT = 'Fahrenheit'
	, ShortUOT = 'F' 
WHERE ID = 1
GO

UPDATE tblCountry 
	SET UOT = 'Celsius'
	, ShortUOT = 'C' 
WHERE ID = 2
GO

-- quick fix to a typo from driver compliance
UPDATE tblDriverComplianceType 
	SET Name = 'Orientation Test'
WHERE ID = 10 
	AND Name = 'Orentation Test'
GO




/******************************
*			DROPS 
******************************/
DROP TABLE tblHosDbAudit
GO

DROP VIEW viewHOSwithHistory
GO

DROP TRIGGER trigHos_IU -- was used for copying to audit table.  no longer needed
-- may want a trigger to prevent deletes
GO

ALTER TABLE tblHOS DROP CONSTRAINT FK_Hos_HosStatus
GO
ALTER TABLE tblHOS DROP COLUMN HosStatusID
GO
ALTER TABLE tblHOS DROP COLUMN NearestCity
GO
ALTER TABLE tblHOS DROP COLUMN ApproveDateUTC
GO
ALTER TABLE tblHOS DROP COLUMN ApprovedByUser
GO
ALTER TABLE tblHOS DROP COLUMN DeleteDateUTC
GO
ALTER TABLE tblHOS DROP COLUMN DeletedByUser
GO

DROP TABLE tblHosStatus
GO





/******************************
*			SP & FN
******************************/

/*******************************************
Date Created: 2017-02-17 - 4.6.0
Author: Joe Engler
Purpose: Calculate the checksum for ELD components per the documentation (4.4.5.3.3 Table 3)
Changes: 
*******************************************/
CREATE FUNCTION fnELDCheckSum(@str VARCHAR(MAX)) RETURNS INT AS
BEGIN
	DECLARE @sum INT = 0
	DECLARE @i INT = 0
	DECLARE @c CHAR

	WHILE @i < LEN(@str)
	BEGIN
		SELECT @i = @i + 1
		SELECT @c = SUBSTRING(@str, @i, 1)

		IF ASCII(@c) BETWEEN 49 AND 122 -- 1-9 or A-Z or a-z
			SELECT @sum = @sum + ASCII(@c) - 48
	END

	RETURN @sum
END
GO


/********************************************
-- Date Created: 2016 Jun 23
-- Author: Joe Engler
-- Purpose: Get summary counts from HOS records
--		4.2.0		JAE		2016/09/21		Don't sum up columns (in another fn)
--		4.2.5		JAE		2016/10/26		Added HOS ID for reference
--		4.6.0		JAE		2017/04/07		Add Status to only include "Active" records
********************************************/
ALTER FUNCTION fnHosSummary(@DriverID INT, @StartDate DATETIME, @EndDate DATETIME) 
RETURNS TABLE AS RETURN
	WITH rows AS (
		-- Dummy start record, gets the previous status when the start date occurred
		SELECT 0 AS rownum,
			ISNULL((SELECT TOP 1 ID FROM tblHos WHERE DriverID = @DriverID AND LogDateUTC < @StartDate AND EventRecordStatusID = 1 ORDER BY LogDateUTC DESC), 0) AS ID,
			ISNULL((SELECT TOP 1 HosDriverStatusID FROM tblHos WHERE DriverID = @DriverID AND LogDateUTC < @StartDate AND EventRecordStatusID = 1 ORDER BY LogDateUTC DESC), 1) AS HosDriverStatusID,
			@StartDate AS LogDateUTC

		UNION

		SELECT ROW_NUMBER() OVER (ORDER BY LogDateUTC) AS rownum,
			ID,
			HosDriverStatusID, 
			LogDateUTC
		FROM tblHos
		WHERE DriverID = @DriverID
		AND LogDateUTC BETWEEN @StartDate AND @EndDate
		AND EventRecordStatusID = 1

		UNION

		-- Dummy end record, gets the final status when the end date occurred
		SELECT (select count(*)+1 FROM tblHos WHERE DriverID = @DriverID AND LogDateUTC BETWEEN @StartDate AND @EndDate AND EventRecordStatusID = 1) AS rownum,
			ID=null,
			ISNULL((SELECT TOP 1 HosDriverStatusID FROM tblHos WHERE DriverID = @DriverID AND LogDateUTC <= @EndDate AND EventRecordStatusID = 1 ORDER BY LogDateUTC DESC), 1), 
			CASE WHEN @EndDate > GETUTCDATE() THEN GETUTCDATE() ELSE @EndDate END
	) 
	SELECT mc.ID, mc.LogDateUTC,
		EndDateUTC = mp.LogDateUTC,
		DATEDIFF(MINUTE, mc.LogDateUTC, mp.LogDateUTC)/60.0 AS TotalHours, 
		mc.HosDriverStatusID,
		SleeperStatusID = CASE WHEN mc.HosDriverStatusID = 2 THEN 1 
							--WHEN mc.HosDriverStatusID = 4 THEN 3
							ELSE mc.hosdriverstatusid END -- group sleeping and off duty together for aggregating rest time
	FROM rows mc
	JOIN rows mp
	ON mc.rownum = mp.rownum-1
	WHERE mp.LogDateUTC <> @StartDate AND mc.LogDateUTC <> @EndDate  -- skip repeated extremes (finishing at start time or ending at end time)
	
GO


/******************************************
** Date Created:	2016/07/15
** Author:			Joe Engler
** Purpose:			Return the historical HOS entries for a driver, calculate the TimeInState for ease of computation on mobile side
** Changes:
--		4.2.5		JAE		2016-10-26		Use operating state (instead of state) to carrier rule call
--		4.4.15		JAE		2017-01-06		Use policy (not carrier rule) to get days to keep
--		4.5.0		BSB		2017-01-26		Add Terminal
--		4.6.0		JAE		2017-03-23		Removed reference to delete fields, allowed text for date
												What about null driver? need to use eld device to push unidentified for that device
******************************************/
ALTER PROCEDURE spDriverHOSLog(@DriverID INT, @LastSyncDate DATETIME) AS
BEGIN
    DECLARE @DaysToKeep INT = 
			(SELECT DriverAppLogRetentionDays 
				FROM tblHosPolicy 
				WHERE ID = COALESCE(
					(SELECT ID 
					 FROM tblHosPolicy 
					 WHERE Name = (SELECT R.Value
									FROM tblDriver D 
									CROSS APPLY dbo.fnCarrierRules(GETDATE(), NULL, 1, D.ID, D.CarrierID, D.TerminalID, D.OperatingStateID, D.RegionID, 1) R 
									WHERE D.ID = @DriverID)), 1) -- Use default policy if none set
			)

	DECLARE @StartDate DATETIME = DATEADD(DAY, - @DaysToKeep, GETUTCDATE());

	WITH rows AS
	(
 			-- Dummy start record, gets the previous record prior to the start date
			SELECT 0 AS rownum, *
            FROM tblHos
			WHERE ID = (SELECT TOP 1 ID 
						FROM tblHos 
						WHERE ISNULL(DriverID, -1) = ISNULL(@DriverID, -1) 
							AND LogDateUTC < @StartDate
							AND EventRecordStatusID = 1 
						ORDER BY LogDateUTC DESC) 

			UNION

			SELECT ROW_NUMBER() OVER (ORDER BY LogDateUTC) AS rownum
				, *
			FROM tblHos
			WHERE ISNULL(DriverID, -1) = ISNULL(@DriverID, -1)
				AND LogDateUTC BETWEEN @StartDate AND GETUTCDATE()
				AND EventRecordStatusID = 1

			UNION

			-- Dummy end record, gets the final record again so it will show in the summary list (with TimeInState = 0)
			SELECT (SELECT COUNT(*) + 1 
					FROM tblHos 
					WHERE ISNULL(DriverID, -1) = ISNULL(@DriverID, -1) 
						AND LogDateUTC BETWEEN @StartDate AND GETUTCDATE()
						AND EventRecordStatusID = 1) AS rownum
					, *
			FROM tblHos
			WHERE ISNULL(DriverID, - 1) = ISNULL(@DriverID, - 1)
				AND ID = (SELECT TOP 1 ID 
						  FROM tblHos 
						  WHERE ISNULL(DriverID, -1) = ISNULL(@DriverID, -1) 
							AND LogDateUTC <= GETUTCDATE()
							AND EventRecordStatusID = 1
						  ORDER BY LogDateUTC DESC)
	)
	SELECT MC.*
		, CASE WHEN MC.ID = MP.ID THEN NULL -- send null time in state for current HOS (repeated at end) 
				ELSE DATEDIFF(MINUTE, MC.LogDateUTC, MP.LogDateUTC) 
				END AS TimeInState
	FROM rows MC
	JOIN rows MP ON MC.rownum = MP.rownum - 1
	CROSS JOIN fnSyncLCDOffset(@LastSyncDate) LCD
	-- BSB - Commented out on 4/14/17 per Joe - REASON: Removing the where clause allows duration to update (mobile HOS) for any record preceding an edited/added record
	--WHERE @LastSyncDate IS NULL 
	--	OR MC.CreateDateUTC >= LCD.LCD -- use offset to get nearest city updates for records just sent to the server
	--	OR MC.LastChangeDateUTC >= LCD.LCD

	UNION

	-- include deleted records if relevant
	SELECT null, h.* , null
	FROM tblHos h
	CROSS JOIN fnSyncLCDOffset(@LastSyncDate) LCD

	WHERE ISNULL(DriverID, -1) = ISNULL(@DriverID, -1)
	  AND EventRecordStatusID <> 1
	  AND (@LastSyncDate IS NULL 
			OR CreateDateUTC >= LCD.LCD -- use offset to get nearest city updates for records just sent to the server
			OR LastChangeDateUTC >= LCD.LCD)

	ORDER BY LogDateUTC
END
GO


/******************************
*			REFRESH
******************************/

EXEC sp_refreshview viewDriverBase
GO
EXEC sp_refreshview viewDriver
GO


EXEC _spRefreshAllViews
GO
EXEC _spRebuildAllObjects
GO
EXEC _spRebuildAllObjects
GO
EXEC _spRecompileAllStoredProcedures
GO



COMMIT
SET NOEXEC OFF