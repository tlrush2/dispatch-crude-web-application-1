SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.10.1'
SELECT  @NewVersion = '3.10.2'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Add New Destination Type - Visual Meter with BOL'
	UNION
	SELECT @NewVersion, 0, 'Add order rule - When Destination Type = BOL Available, Require BOL'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/* 3.10.2 - 2016/01/15 - GM - DCWEB-989: Add 'Visual Meter with BOL' destination ticket type */
INSERT tblDestTicketType (ID, Name)
SELECT 7, 'Visual Meter with BOL'
EXCEPT SELECT ID, Name FROM tblDestTicketType
GO

/* 3.10.2 - 2016/01/14 - GM - DCDRV-279: Added Order Rule: BOL Required */
INSERT INTO tblOrderRuleType (ID, Name, RuleTypeID)
VALUES (13, 'Require BOL', 2)
GO

COMMIT
SET NOEXEC OFF