-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.9.19.4.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.9.19.4.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0

SET NOEXEC OFF
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.9.19.4'
SELECT  @NewVersion = '3.9.19.5'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'Settlement - display the Order Approval overridden values on the Settlement pages'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/*******************************************
-- Date Created: 2013/04/25
-- Author: Kevin Alons
-- Purpose: return readonly Order data for Driver App sync
-- Changes:
	- 3.8.1 - 2015/07/02 - KDA - add new split DriverApp printing capability (HeaderImageLeft|Top|Width|Height & Pickup|Ticket|Deliver TemplateText)
	- 3.9.19 - 2015/07/23 - KDA - remove TicketTemplate column (it will be provided as a separate TicketTemplates sync table)
	- 3.9.19.5 - 2015/09/30 - KDA - fix LCD offset seconds to use fnSyncLCDOffset
*******************************************/
ALTER FUNCTION fnOrderReadOnly_DriverApp(@DriverID int, @LastChangeDateUTC datetime) RETURNS TABLE AS
RETURN 
	WITH cteBase AS
	(
		SELECT O.ID
			, O.OrderNum
			, O.StatusID
			, O.TicketTypeID
			, PriorityNum = cast(P.PriorityNum as int) 
			, Product = PRO.Name
			, O.DueDate
			, Origin = OO.Name
			, OriginFull = OO.FullName
			, OO.OriginType
			, O.OriginUomID
			, OriginStation = OO.Station 
			, OriginLeaseNum = OO.LeaseNum 
			, OriginCounty = OO.County 
			, OriginLegalDescription = OO.LegalDescription 
			, OriginNDIC = OO.NDICFileNum
			, OriginNDM = OO.NDM
			, OriginCA = OO.CA
			, OriginState = OO.State
			, OriginAPI = OO.WellAPI 
			, OriginLat = OO.LAT 
			, OriginLon = OO.LON 
			, OriginGeoFenceRadiusMeters = OO.GeoFenceRadiusMeters 
			, Destination = D.Name
			, DestinationFull = D.FullName
			, DestType = D.DestinationType 
			, O.DestUomID
			, DestLat = D.LAT 
			, DestLon = D.LON 
			, DestGeoFenceRadiusMeters = D.GeoFenceRadiusMeters 
			, DestinationStation = D.Station 
			, O.CreateDateUTC
			, O.CreatedByUser
			, O.LastChangeDateUTC
			, O.LastChangedByUser
			, DeleteDateUTC = isnull(ODAVD.VirtualDeleteDateUTC, O.DeleteDateUTC) 
			, DeletedByUser = isnull(ODAVD.VirtualDeletedByUser, O.DeletedByUser) 
			, O.OriginID
			, O.DestinationID
			, PriorityID = cast(O.PriorityID AS int) 
			, Operator = OO.Operator
			, O.OperatorID
			, Pumper = OO.Pumper
			, O.PumperID
			, Producer = OO.Producer
			, O.ProducerID
			, Customer = C.Name
			, O.CustomerID
			, Carrier = CA.Name
			, O.CarrierID
			, O.ProductID
			, TicketType = OO.TicketType
			, EmergencyInfo = isnull(C.EmergencyInfo, 'For an emergency (spill, leak, fire, or accident) contact CHEMTREC @ 800.424.9300 (toll free)') 
			, DestTicketTypeID = D.TicketTypeID
			, DestTicketType = D.TicketType
			, O.OriginTankNum
			, O.OriginTankID
			, O.DispatchNotes
			, O.DispatchConfirmNum
			, RouteActualMiles = isnull(R.ActualMiles, 0)
			, CarrierAuthority = CA.Authority 
			, OriginTimeZone = OO.TimeZone
			, DestTimeZone = D.TimeZone
			, OCTM.OriginThresholdMinutes
			, OCTM.DestThresholdMinutes
			, ShipperHelpDeskPhone = C.HelpDeskPhone
			, OriginDrivingDirections = OO.DrivingDirections
			, LCD.LCD
			, CustomerLastChangeDateUTC = C.LastChangeDateUTC 
			, CarrierLastChangeDateUTC = CA.LastChangeDateUTC 
			, OriginLastChangeDateUTC = OO.LastChangeDateUTC 
			, DestLastChangeDateUTC = D.LastChangeDateUTC
			, RouteLastChangeDateUTC = R.LastChangeDateUTC
			, DriverID = @DriverID
		FROM dbo.tblOrder O
		JOIN dbo.tblPriority P ON P.ID = O.PriorityID
		JOIN dbo.viewOrigin OO ON OO.ID = O.OriginID
		JOIN dbo.viewDestination D ON D.ID = O.DestinationID
		JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
		JOIN dbo.tblRoute R ON R.ID = O.RouteID
		JOIN dbo.tblCarrier CA ON CA.ID = O.CarrierID
		JOIN dbo.tblProduct PRO ON PRO.ID = O.ProductID
		OUTER APPLY dbo.fnOrderBestMatchDriverAppPrintHeaderImage(O.ID) DAHI
		LEFT JOIN tblOrderDriverAppVirtualDelete ODAVD ON ODAVD.OrderID = O.ID AND ODAVD.DriverID = @DriverID
		CROSS JOIN fnSyncLCDOffset(@LastChangeDateUTC) LCD
		OUTER APPLY dbo.fnOrderCombinedThresholdMinutes(O.ID) OCTM
		WHERE O.ID IN (
			SELECT id FROM tblOrder WHERE DriverID = @driverID AND StatusID IN (2,7,8,3) 
			UNION 
			SELECT OrderID FROM tblOrderDriverAppVirtualDelete WHERE DriverID = @driverID
		)
	)

	SELECT O.*
		, HeaderImageID = DAHI.ID
		, PrintHeaderBlob = DAHI.ImageBlob
		, HeaderImageLeft = DAHI.ImageLeft
		, HeaderImageTop = DAHI.ImageTop
		, HeaderImageWidth = DAHI.ImageWidth
		, HeaderImageHeight = DAHI.ImageHeight
		, PickupTemplateID = DAPT.ID
		, PickupTemplateText = DAPT.TemplateText
		, DeliverTemplateID = DADT.ID
		, DeliverTemplateText = DADT.TemplateText
	FROM cteBase O
	OUTER APPLY dbo.fnOrderBestMatchDriverAppPrintHeaderImage(O.ID) DAHI
	LEFT JOIN tblDriverAppPrintHeaderImageSync DAHIS ON DAHIS.OrderID = O.ID AND DAHIS.DriverID = O.DriverID AND DAHIS.RecordID <> DAHI.ID
	OUTER APPLY dbo.fnOrderBestMatchDriverAppPrintPickupTemplate(O.ID) DAPT
	LEFT JOIN tblDriverAppPrintPickupTemplateSync DAPTS ON DAPTS.OrderID = O.ID AND DAPTS.DriverID = O.DriverID AND DAPTS.RecordID <> DAPT.ID
	OUTER APPLY dbo.fnOrderBestMatchDriverAppPrintDeliverTemplate(O.ID) DADT
	LEFT JOIN tblDriverAppPrintDeliverTemplateSync DADTS ON DADTS.OrderID = O.ID AND DADTS.DriverID = O.DriverID AND DADTS.RecordID <> DADT.ID
	WHERE (
		@LastChangeDateUTC IS NULL 
		OR O.CreateDateUTC >= LCD
		OR O.LastChangeDateUTC >= LCD
		OR O.DeleteDateUTC >= LCD
		OR CustomerLastChangeDateUTC >= LCD
		OR CarrierLastChangeDateUTC >= LCD
		OR OriginLastChangeDateUTC >= LCD
		OR DestLastChangeDateUTC >= LCD
		OR RouteLastChangeDateUTC >= LCD
		-- if any print related record was changed or a different template/image is now valid
		OR DAHI.LastChangeDateUTC >= LCD
		OR DAHIS.RecordID IS NOT NULL
		OR DAPT.LastChangeDateUTC >= LCD
		OR DAPTS.RecordID IS NOT NULL
		OR DADT.LastChangeDateUTC >= LCD
		OR DADTS.RecordID IS NOT NULL
	)
GO

COMMIT
SET NOEXEC OFF