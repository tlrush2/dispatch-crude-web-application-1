SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.9.35'
SELECT  @NewVersion = '3.9.36'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-957: Add missing foreign keys to tables where appropriate.'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

-- !!!! RUN SELECT QUERIES FIRST TO ENSURE NO ISSUES WHEN ADDING FOREIGN KEYS, SHOULD BE EMPTY RESULTS !!!


-- No foreign keys used on audit, log or sync tables
-- * tblOrderDBAudit, tblOrderTicketDBAudit
-- * tblGaugerSyncLog, tblGaugerAppExceptionLog, tblGaugeOrderTicketDbAudit, tblGaugeOrderDbAudit
-- * tblDriverSyncLog, tblDriverAppExceptionLog


DISABLE TRIGGER trigOrder_IU ON tblOrder
GO
UPDATE tblOrder SET ProducerID = NULL WHERE ProducerID NOT IN (SELECT ID FROM tblProducer)
UPDATE tblOrder SET OriginTankID = NULL WHERE OriginTankID NOT IN (SELECT ID FROM tblOriginTank)
GO
ENABLE TRIGGER trigOrder_IU ON tblOrder
GO

ALTER TABLE tblOrder WITH CHECK ADD CONSTRAINT FK_Order_Product FOREIGN KEY(ProductID)
REFERENCES tblProduct (ID)
GO
ALTER TABLE tblOrder CHECK CONSTRAINT FK_Order_Product
GO

ALTER TABLE tblOrder WITH CHECK ADD CONSTRAINT FK_Order_OriginTank FOREIGN KEY(OriginTankID)
REFERENCES tblOriginTank (ID)
GO
ALTER TABLE tblOrder CHECK CONSTRAINT FK_Order_OriginTank
GO


UPDATE tblDriver SET RegionID = NULL WHERE RegionID NOT IN (SELECT ID FROM tblRegion)
GO
ALTER TABLE tblDriver WITH CHECK ADD CONSTRAINT FK_Driver_Region FOREIGN KEY(RegionID)
REFERENCES tblRegion (ID)
GO

ALTER TABLE tblDriver CHECK CONSTRAINT FK_Driver_Region
GO

UPDATE tblDriver SET OperatingStateID = NULL WHERE OperatingStateID NOT IN (SELECT ID FROM tblState)
GO
ALTER TABLE tblDriver WITH CHECK ADD CONSTRAINT FK_Driver_OperatingState FOREIGN KEY(OperatingStateID)
REFERENCES tblState (ID)
GO

ALTER TABLE tblDriver CHECK CONSTRAINT FK_Driver_OperatingState
GO


UPDATE tblTruck SET GarageStateID = NULL WHERE GarageStateID NOT IN (SELECT ID FROM tblState)
GO
ALTER TABLE tblTruck WITH CHECK ADD CONSTRAINT FK_Truck_GarageState FOREIGN KEY(GarageStateID)
REFERENCES tblState (ID)
GO

ALTER TABLE tblTruck CHECK CONSTRAINT FK_Truck_GarageState
GO


UPDATE tblTrailer SET GarageStateID = NULL WHERE GarageStateID NOT IN (SELECT ID FROM tblState)
ALTER TABLE tblTrailer WITH CHECK ADD CONSTRAINT FK_Trailer_GarageState FOREIGN KEY(GarageStateID)
REFERENCES tblState (ID)
GO

ALTER TABLE tblTrailer CHECK CONSTRAINT FK_Trailer_GarageState
GO


UPDATE tblShipperWaitFeeParameter SET ProducerID = NULL WHERE ProducerID NOT IN (SELECT ID FROM tblProducer)
ALTER TABLE tblShipperWaitFeeParameter WITH CHECK ADD CONSTRAINT FK_ShipperWaitFee_Producer FOREIGN KEY(ProducerID)
REFERENCES tblProducer (ID)
GO

ALTER TABLE tblShipperWaitFeeParameter CHECK CONSTRAINT FK_ShipperWaitFee_Producer
GO


UPDATE tblShipperOriginWaitRate SET ProducerID = NULL WHERE ProducerID NOT IN (SELECT ID FROM tblProducer)
ALTER TABLE tblShipperOriginWaitRate WITH CHECK ADD CONSTRAINT FK_ShipperOriginWait_Producer FOREIGN KEY(ProducerID)
REFERENCES tblProducer (ID)
GO

ALTER TABLE tblShipperOriginWaitRate CHECK CONSTRAINT FK_ShipperOriginWait_Producer
GO


UPDATE tblShipperOrderRejectRate SET ProducerID = NULL WHERE ProducerID NOT IN (SELECT ID FROM tblProducer)
ALTER TABLE tblShipperOrderRejectRate WITH CHECK ADD CONSTRAINT FK_ShipperOrderReject_Producer FOREIGN KEY(ProducerID)
REFERENCES tblProducer (ID)
GO

ALTER TABLE tblShipperOrderRejectRate CHECK CONSTRAINT FK_ShipperOrderReject_Producer
GO


UPDATE tblShipperDestinationWaitRate SET ProducerID = NULL WHERE ProducerID NOT IN (SELECT ID FROM tblProducer)
ALTER TABLE tblShipperDestinationWaitRate WITH CHECK ADD CONSTRAINT FK_ShipperDestinationWait_Producer FOREIGN KEY(ProducerID)
REFERENCES tblProducer (ID)
GO

ALTER TABLE tblShipperDestinationWaitRate CHECK CONSTRAINT FK_ShipperDestinationWait_Producer
GO


UPDATE tblProduct SET ProductGroupID = (SELECT min(ID) FROM tblProductGroup) WHERE ProductGroupID NOT IN (SELECT ID FROM tblProductGroup)
ALTER TABLE tblProduct WITH CHECK ADD CONSTRAINT FK_Product_ProductGroup FOREIGN KEY(ProductGroupID)
REFERENCES tblProductGroup (ID)
GO

ALTER TABLE tblProduct CHECK CONSTRAINT FK_Product_ProductGroup
GO

--select * from tblOrderSettlementShipper where WaitFeeParameterID in (select id from tblshipperwaitfeeparameter)
UPDATE tblOrderSettlementShipper SET WaitFeeParameterID = NULL WHERE WaitFeeParameterID NOT IN (SELECT ID FROM tblShipperWaitFeeParameter)
ALTER TABLE tblOrderSettlementShipper WITH CHECK ADD CONSTRAINT FK_OrderSettlementShipper_ShipperWaitFeeParameter FOREIGN KEY(WaitFeeParameterID)
REFERENCES tblShipperWaitFeeParameter (ID)
GO

ALTER TABLE tblOrderSettlementShipper CHECK CONSTRAINT FK_OrderSettlementShipper_ShipperWaitFeeParameter
GO


UPDATE tblOrderSettlementCarrier SET WaitFeeParameterID = NULL WHERE WaitFeeParameterID NOT IN (SELECT ID FROM tblShipperWaitFeeParameter)
ALTER TABLE tblOrderSettlementCarrier WITH CHECK ADD CONSTRAINT FK_OrderSettlementCarrier_CarrierWaitFeeParameter FOREIGN KEY(WaitFeeParameterID)
REFERENCES tblCarrierWaitFeeParameter (ID)
GO

ALTER TABLE tblOrderSettlementCarrier CHECK CONSTRAINT FK_OrderSettlementCarrier_CarrierWaitFeeParameter
GO


UPDATE tblOrderReroute SET PreviousDestinationID = (SELECT min(ID) FROM tblDestination WHERE DeleteDateUTC IS NULL) WHERE PreviousDestinationID NOT IN (SELECT ID FROM tblDestination)
ALTER TABLE tblOrderReroute WITH CHECK ADD CONSTRAINT FK_OrderReroute_PrevDestination FOREIGN KEY(PreviousDestinationID)
REFERENCES tblDestination (ID)
GO

ALTER TABLE tblOrderReroute CHECK CONSTRAINT FK_OrderReroute_PrevDestination
GO

DELETE FROM tblOrderExportFinalCustomer WHERE OrderID NOT IN (SELECT ID FROM tblOrder)
ALTER TABLE tblOrderExportFinalCustomer WITH CHECK ADD CONSTRAINT FK_OrderExportFinalCustomer_Order FOREIGN KEY(OrderID)
REFERENCES tblOrder (ID)
GO

ALTER TABLE tblOrderExportFinalCustomer CHECK CONSTRAINT FK_OrderExportFinalCustomer_Order
GO

UPDATE tblDestination SET RegionID = NULL WHERE RegionID NOT IN (SELECT ID FROM tblRegion)
ALTER TABLE tblDestination WITH CHECK ADD CONSTRAINT FK_Destination_Region FOREIGN KEY(RegionID)
REFERENCES tblRegion (ID)
GO

ALTER TABLE tblDestination CHECK CONSTRAINT FK_Destination_Region
GO


UPDATE tblCarrierWaitFeeParameter SET ProducerID = NULL WHERE ProducerID NOT IN (SELECT ID FROM tblProducer)
ALTER TABLE tblCarrierWaitFeeParameter WITH CHECK ADD CONSTRAINT FK_CarrierWaitFee_Producer FOREIGN KEY(ProducerID)
REFERENCES tblProducer (ID)
GO

ALTER TABLE tblCarrierWaitFeeParameter CHECK CONSTRAINT FK_CarrierWaitFee_Producer
GO


UPDATE tblCarrierRouteRate SET DriverGroupID = NULL WHERE DriverGroupID NOT IN (SELECT ID FROM tblDriverGroup)
ALTER TABLE tblCarrierRouteRate WITH CHECK ADD CONSTRAINT FK_CarrierRouteRate_DriverGroup FOREIGN KEY(DriverGroupID)
REFERENCES tblDriverGroup (ID)
GO

ALTER TABLE tblCarrierRouteRate CHECK CONSTRAINT FK_CarrierRouteRate_DriverGroup
GO


UPDATE tblCarrierRateSheet SET DriverGroupID = NULL WHERE DriverGroupID NOT IN (SELECT ID FROM tblDriverGroup)
ALTER TABLE tblCarrierRateSheet WITH CHECK ADD CONSTRAINT FK_CarrierRateSheet_DriverGroup FOREIGN KEY(DriverGroupID)
REFERENCES tblDriverGroup (ID)
GO

ALTER TABLE tblCarrierRateSheet CHECK CONSTRAINT FK_CarrierRateSheet_DriverGroup
GO


UPDATE tblCarrierOriginWaitRate SET ProducerID = NULL WHERE ProducerID NOT IN (SELECT ID FROM tblProducer)
ALTER TABLE tblCarrierOriginWaitRate WITH CHECK ADD CONSTRAINT FK_CarrierOriginWait_Producer FOREIGN KEY(ProducerID)
REFERENCES tblProducer (ID)
GO

ALTER TABLE tblCarrierOriginWaitRate CHECK CONSTRAINT FK_CarrierOriginWait_Producer
GO


UPDATE tblCarrierOriginWaitRate SET DriverGroupID = NULL WHERE DriverGroupID NOT IN (SELECT ID FROM tblDriverGroup)
ALTER TABLE tblCarrierOriginWaitRate WITH CHECK ADD CONSTRAINT FK_CarrierOriginWait_DriverGroup FOREIGN KEY(DriverGroupID)
REFERENCES tblDriverGroup (ID)
GO

ALTER TABLE tblCarrierOriginWaitRate CHECK CONSTRAINT FK_CarrierOriginWait_DriverGroup
GO


UPDATE tblCarrierOrderRejectRate SET ProducerID = NULL WHERE ProducerID NOT IN (SELECT ID FROM tblProducer)
ALTER TABLE tblCarrierOrderRejectRate WITH CHECK ADD CONSTRAINT FK_CarrierOrderReject_Producer FOREIGN KEY(ProducerID)
REFERENCES tblProducer (ID)
GO

ALTER TABLE tblCarrierOrderRejectRate CHECK CONSTRAINT FK_CarrierOrderReject_Producer
GO


UPDATE tblCarrierOrderRejectRate SET DriverGroupID = NULL WHERE DriverGroupID NOT IN (SELECT ID FROM tblDriverGroup)
ALTER TABLE tblCarrierOrderRejectRate WITH CHECK ADD CONSTRAINT FK_CarrierOrderReject_DriverGroup FOREIGN KEY(DriverGroupID)
REFERENCES tblDriverGroup (ID)
GO

ALTER TABLE tblCarrierOrderRejectRate CHECK CONSTRAINT FK_CarrierOrderReject_DriverGroup
GO


UPDATE tblCarrierFuelSurchargeRate SET DriverGroupID = NULL WHERE DriverGroupID NOT IN (SELECT ID FROM tblDriverGroup)
ALTER TABLE tblCarrierFuelSurchargeRate WITH CHECK ADD CONSTRAINT FK_CarrierFuelSurcharge_DriverGroup FOREIGN KEY(DriverGroupID)
REFERENCES tblDriverGroup (ID)
GO

ALTER TABLE tblCarrierFuelSurchargeRate CHECK CONSTRAINT FK_CarrierFuelSurcharge_DriverGroup
GO


UPDATE tblCarrierDestinationWaitRate SET ProducerID = NULL WHERE ProducerID NOT IN (SELECT ID FROM tblProducer)
ALTER TABLE tblCarrierDestinationWaitRate WITH CHECK ADD CONSTRAINT FK_CarrierDestinationWait_Producer FOREIGN KEY(ProducerID)
REFERENCES tblProducer (ID)
GO

ALTER TABLE tblCarrierDestinationWaitRate CHECK CONSTRAINT FK_CarrierDestinationWait_Producer
GO


UPDATE tblCarrierDestinationWaitRate SET DriverGroupID = NULL WHERE DriverGroupID NOT IN (SELECT ID FROM tblDriverGroup)
ALTER TABLE tblCarrierDestinationWaitRate WITH CHECK ADD CONSTRAINT FK_CarrierDestinationWait_DriverGroup FOREIGN KEY(DriverGroupID)
REFERENCES tblDriverGroup (ID)
GO

ALTER TABLE tblCarrierDestinationWaitRate CHECK CONSTRAINT FK_CarrierDestinationWait_DriverGroup
GO


UPDATE tblCarrierAssessorialRate SET DriverGroupID = NULL WHERE DriverGroupID NOT IN (SELECT ID FROM tblDriverGroup)
ALTER TABLE tblCarrierAssessorialRate WITH CHECK ADD CONSTRAINT FK_CarrierAssessorialRate_DriverGroup FOREIGN KEY(DriverGroupID)
REFERENCES tblDriverGroup (ID)
GO

ALTER TABLE tblCarrierAssessorialRate CHECK CONSTRAINT FK_CarrierAssessorialRate_DriverGroup
GO


UPDATE tblCarrier SET CarrierTypeID = (SELECT min(ID) FROM tblCarrierType WHERE DeleteDateUTC IS NULL) WHERE CarrierTypeID NOT IN (SELECT ID FROM tblCarrierType)
ALTER TABLE tblCarrier WITH CHECK ADD CONSTRAINT FK_Carrier_CarrierType FOREIGN KEY(CarrierTypeID)
REFERENCES tblCarrierType (ID)
GO

ALTER TABLE tblCarrier CHECK CONSTRAINT FK_Carrier_CarrierType
GO


-- SELECT DISTINCT NextID FROM tblOrderStatus o WHERE NOT EXISTS (SELECT 1 FROM tblOrderStatus os WHERE os.ID = o.NextID) AND o.NextID IS NOT NULL
ALTER TABLE tblOrderStatus WITH CHECK ADD CONSTRAINT FK_OrderStatus_Next FOREIGN KEY(NextID)
REFERENCES tblOrderStatus (ID)
GO

ALTER TABLE tblOrderStatus CHECK CONSTRAINT FK_OrderStatus_Next
GO



COMMIT
SET NOEXEC OFF