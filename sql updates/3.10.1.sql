-- rollback

-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.9.28.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.9.28.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0

SET NOEXEC OFF
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.9.39'
SELECT  @NewVersion = '3.10.1'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Add new Import Center functionality (DB level)'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/************************************************
 Creation Info: 2015/11/18 - 3.10.1
 Author: Kevin Alons
 Purpose: retrieve OrderTicket Plus lookup fields for ImportCenter usage
 Changes:
************************************************/
CREATE VIEW viewImportCenterOrderTicket AS
	SELECT OT.*
		, OrderDate = O.OrderDate
	FROM tblOrderTicket OT
	JOIN tblOrder O ON O.ID = OT.OrderID
GO

CREATE TABLE tblObject
(
  ID int NOT NULL CONSTRAINT PK_Object PRIMARY KEY
, Name varchar(50) NOT NULL
, SqlSourceName varchar(255) NOT NULL
, SqlTargetName varchar(255) NOT NULL
, CreateDateUTC datetime NOT NULL CONSTRAINT DF_Object_CreateDateUTC DEFAULT (getutcdate())
, CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_Object_CreatedByUser DEFAULT ('System')
)
GO
CREATE UNIQUE INDEX udxObject_Main ON tblObject(Name)
GO
GRANT SELECT ON tblObject to role_iis_acct
GO

INSERT INTO tblObject (ID, Name, SqlSourceName, SqlTargetName)
	SELECT 1, 'Order', 'tblOrder', 'tblOrder'
	UNION
	SELECT 2, 'Ticket', 'viewImportCenterOrderTicket', 'tblOrderTicket'
	UNION
	SELECT 3, 'Product', 'tblProduct', 'tblProduct'
	UNION
	SELECT 10, 'Region', 'tblRegion', 'tblRegion'
	UNION
	SELECT 11, 'Shipper', 'tblCustomer', 'tblCustomer'
	UNION
	SELECT 12, 'Carrier', 'tblCarrier', 'tblCarrier'
	UNION
	SELECT 13, 'Driver', 'tblDriver', 'tblDriver'
	UNION
	SELECT 14, 'Truck', 'tblTruck', 'tblTruck'
	UNION
	SELECT 15, 'Trailer', 'tblTrailer', 'tblTrailer'
	UNION
	SELECT 16, 'Producer', 'tblProducer', 'tblProducer'
	UNION
	SELECT 17, 'Pumper', 'tblPumper', 'tblPumper'
	UNION
	SELECT 18, 'Country', 'tblCountry', 'tblCountry'
	UNION
	SELECT 19, 'Origin', 'tblOrigin', 'tblOrigin'
	UNION
	SELECT 20, 'Destination', 'tblDestination', 'tblDestination'
	UNION
	SELECT 21, 'Ticket Type', 'tblTicketType', 'tblTicketType'
	UNION
	SELECT 22, 'Destination Ticket Type', 'tblDestTicketType', 'tblDestTicketType'
	UNION
	SELECT 23, 'Driver Group', 'tblDriverGroup', 'tblDriverGroup'
	UNION
	SELECT 24, 'Gauger', 'tblGauger', 'tblGauger'
	UNION
	SELECT 25, 'Operator', 'tblOperator', 'tblOperator'
	UNION
	SELECT 26, 'Reroute', 'tblOrderReroute', 'tblOrderReroute'
	UNION
	SELECT 27, 'Order Transfer', 'tblOrderTransfer', 'tblOrderTransfer'
	UNION
	SELECT 28, 'Order Status', 'tblOrderStatus', 'tblOrderStatus'
	UNION
	SELECT 29, 'Origin Type', 'tblOriginType', 'tblOriginType'
	UNION
	SELECT 30, 'Origin Tank', 'tblOriginTank', 'tblOriginTank'
	UNION
	SELECT 31, 'Origin Tank Strapping', 'tblOriginTankStrapping', 'tblOriginTankStrapping'
	UNION
	SELECT 32, 'Destination Wait Reason', 'tblDestinationWaitReason', 'tblDestinationWaitReason'
	UNION 
	SELECT 33, 'Origin Wait Reason', 'tblOriginWaitReason', 'tblOriginWaitReason'
	UNION
	SELECT 34, 'Order Reject Reason', 'tblOrderRejectReason', 'tblOrderRejectReason'
	UNION
	SELECT 35, 'State', 'tblState', 'tblState'
	UNION
	SELECT 36, 'Carrier Type', 'tblCarrierType', 'tblCarrierType'
	UNION
	SELECT 37, 'Print Status', 'tblPrintStatus', 'tblPrintStatus'
	UNION
	SELECT 38, 'UOM', 'tblUom', 'tblUom'
GO

CREATE TABLE tblObjectFieldType
(
  ID smallint NOT NULL CONSTRAINT PK_ObjectFieldType PRIMARY KEY
, Name varchar(50) NOT NULL
, CreateDateUTC datetime NOT NULL CONSTRAINT DF_ObjectFieldType_CreateDateUTC DEFAULT (getutcdate())
, CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_ObjectFieldType_CreatedByUser DEFAULT ('System')
)
GO
CREATE UNIQUE INDEX udxObjectFieldType_Main ON tblObjectFieldType(Name)
GO
GRANT SELECT ON tblObjectFieldType TO role_iis_acct
GO
INSERT INTO tblObjectFieldType (ID, Name, CreatedByUser)
	SELECT ID, Name, 'System' FROM tblSettingType
	UNION
	SELECT 5, 'DateTime', 'System'
	UNION
	SELECT 6, 'Date', 'System'
	UNION
	SELECT 7, 'Time', 'System'
	UNION
	SELECT 8, 'GUID', 'System'
GO

CREATE TABLE tblObjectFieldAllowNull
(
  ID int NOT NULL CONSTRAINT PK_ObjectFieldAllowNull PRIMARY KEY CLUSTERED
, Name varchar(25) NOT NULL CONSTRAINT UC_ObjectFieldAllowNull UNIQUE
, AllowNull bit NULL
)
GO
GRANT SELECT ON tblObjectFieldAllowNull TO role_iis_acct
GO
INSERT INTO tblObjectFieldAllowNull (ID, Name, AllowNull)
	SELECT 0, 'False', 0 UNION SELECT 1, 'True', 1 UNION SELECT 2, 'Dynamic', NULL
GO

CREATE TABLE tblObjectField
(
  ID int identity(1, 1) NOT NULL CONSTRAINT PK_ObjectField PRIMARY KEY
--, ProfileTableID int NOT NULL CONSTRAINT FK_ObjectField_ProfileTable FOREIGN KEY REFERENCES tblObject(ID)
, ObjectID int NOT NULL CONSTRAINT FK_ObjectField_Object FOREIGN KEY REFERENCES tblObject(ID)
, FieldName varchar(50) NOT NULL
, Name varchar(50) NOT NULL
, ObjectFieldTypeID smallint NOT NULL CONSTRAINT FK_ObjectField_ObjectFieldType FOREIGN KEY REFERENCES tblObjectFieldType(ID)
, DefaultValue varchar(max) NULL
, ReadOnly bit NOT NULL CONSTRAINT DF_ObjectField_ReadOnly DEFAULT (0)
, AllowNullID int NOT NULL CONSTRAINT FK_ObjectField_AllowNull FOREIGN KEY REFERENCES tblObjectFieldAllowNull(ID) CONSTRAINT DF_ObjectField_AllowNull DEFAULT (1)
, IsKey bit NOT NULL CONSTRAINT DF_ObjectField_IsKey DEFAULT (0)
, IsCustom bit NOT NULL CONSTRAINT DF_ObjectField_IsCustom DEFAULT (1)
, ParentObjectID int NULL CONSTRAINT FK_ObjectField_ParentObject FOREIGN KEY REFERENCES tblObject(ID)
, ParentObjectIDFieldName varchar(50) NULL CONSTRAINT DF_ObjectField_ParentObjectIDFieldName DEFAULT ('ID')
, CreateDateUTC datetime NOT NULL CONSTRAINT DF_ObjectField_CreateDateUTC DEFAULT (getutcdate())
, CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_ObjectField_CreatedByUser DEFAULT ('System')
, LastChangeDateUTC datetime NULL 
, LastChangedByUser varchar(100) NULL 
, DeleteDateUTC datetime NULL 
, DeletedByUser varchar(100) NULL 
)
GO
CREATE UNIQUE INDEX udxObjectField_Main ON tblObjectField(ObjectID, FieldName)
GO
CREATE UNIQUE INDEX udxObjectField_Main_Name ON tblObjectField(ObjectID, Name)
GO
/* add constraint to ensure a custom field never has Parent info recorded - which would be invalid */
ALTER TABLE tblObjectField WITH NOCHECK
	ADD CONSTRAINT CK_ObjectField_Custom_NoParent
	CHECK (IsCustom = 0 OR ParentObjectID IS NULL)
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblObjectField to role_iis_acct
GO

exec _spDropFunction 'fnFriendlyName'
go
/**********************************************
 Creation Info: 2015/11/18 - 3.10.1
 Author: Kevin Alons
 Purpose: Translate the the camel-case Value into a more readable (friendly) name
 Changes:
**********************************************/
CREATE FUNCTION fnFriendlyName(@value varchar(255)) RETURNS varchar(255) AS
BEGIN
	DECLARE @ret varchar(255)
	SET @ret = LEFT(@value, 1)
	DECLARE @i int, @len int
	SELECT @i = 2
	IF @value LIKE '%ID'
				/* get the position of "UID" */
		SELECT @len = CHARINDEX('UID', @value)
				/* if "UID" not found, look for "ID" */
			, @len = isnull(nullif(@len, 0), CHARINDEX('ID', @value))
				/* if either was found @ 1, then it is the entire string, so just use the entire value as the "Name" */
			, @len = isnull(nullif(@len, 1), len(@value)+1)
	ELSE
		SELECT @len = len(@value)+1

	WHILE @i < @len
	BEGIN
		IF ASCII(SUBSTRING(@value, @i, 1)) BETWEEN ASCII('A') AND ASCII('Z')
			AND (
				(SUBSTRING(@value, @i-1, 1) NOT IN ('2', '_') AND ASCII(SUBSTRING(@value, @i-1, 1)) NOT BETWEEN ASCII('A') AND ASCII('Z'))
				OR (@i > 1 AND @i < @len-1 AND SUBSTRING(@value, @i+1, 1) NOT IN ('2', '_') AND ASCII(SUBSTRING(@value, @i+1, 1)) NOT BETWEEN ASCII('A') AND ASCII('Z')))
			SET @ret = @ret + ' '
		SET @ret = @ret + SUBSTRING(@value, @i, 1)
		SET @i = @i + 1
	END
	RETURN (@ret)
END
GO
GRANT EXECUTE ON dbo.fnFriendlyName TO role_iis_acct
GO

SET IDENTITY_INSERT [dbo].[tblObjectField] ON 
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (1, 12, N'Address', N'Address', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (2, 12, N'Authority', N'Authority', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (3, 12, N'City', N'City', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (4, 12, N'ContactEmail', N'Contact Email', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (5, 12, N'ContactName', N'Contact Name', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (6, 12, N'ContactPhone', N'Contact Phone', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (7, 12, N'DOTNumber', N'DOT Number', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (8, 12, N'FEINNumber', N'FEIN Number', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (9, 12, N'IDNumber', N'ID Number', 1, NULL, 0, 0, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (10, 12, N'MotorCarrierNumber', N'Motor Carrier Number', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (11, 12, N'Name', N'Name', 1, NULL, 0, 0, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (12, 12, N'Notes', N'Notes', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (13, 12, N'Zip', N'Zip', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (14, 36, N'Name', N'Name', 1, NULL, 0, 0, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (15, 18, N'Abbrev', N'Abbrev', 1, NULL, 0, 0, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (16, 18, N'Name', N'Name', 1, NULL, 0, 0, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (17, 18, N'ShortUOT', N'Short UOT', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (18, 18, N'UOT', N'UOT', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (19, 11, N'Address', N'Address', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (20, 11, N'City', N'City', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (21, 11, N'CodeDXCode', N'Code DX Code', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (22, 11, N'ContactEmail', N'Contact Email', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (23, 11, N'ContactName', N'Contact Name', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (24, 11, N'ContactPhone', N'Contact Phone', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (25, 11, N'EmergencyInfo', N'Emergency Info', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (26, 11, N'HelpDeskPhone', N'Help Desk Phone', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (27, 11, N'MinSettlementUnits', N'Min Settlement Units', 3, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (28, 11, N'Name', N'Name', 1, NULL, 0, 0, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (29, 11, N'ShipXpressDXClient', N'Ship Xpress DX Client', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (30, 11, N'ShipXpressDXExport', N'Ship Xpress DX Export', 2, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (31, 11, N'Zip', N'Zip', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (32, 20, N'Address', N'Address', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (33, 20, N'City', N'City', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (34, 20, N'GeoFenceRadiusMeters', N'Geo Fence Radius Meters', 3, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (35, 20, N'LAT', N'LAT', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (36, 20, N'LON', N'LON', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (37, 20, N'Name', N'Name', 1, NULL, 0, 0, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (38, 20, N'Station', N'Station', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (39, 20, N'UseDST', N'Use DST', 2, N'1', 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (40, 20, N'Zip', N'Zip', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (41, 32, N'Description', N'Description', 1, NULL, 0, 0, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (42, 32, N'Num', N'Num', 1, NULL, 0, 0, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (43, 32, N'RequireNotes', N'Require Notes', 2, N'0', 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (44, 22, N'Name', N'Name', 1, NULL, 0, 0, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (45, 13, N'Address', N'Address', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (46, 13, N'City', N'City', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (47, 13, N'DLExpiration', N'DL Expiration', 6, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (48, 13, N'DLNumber', N'DL Number', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (49, 13, N'DOB', N'DOB', 6, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (50, 13, N'Email', N'Email', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (51, 13, N'Endorsements', N'Endorsements', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (52, 13, N'FirstName', N'First Name', 1, NULL, 0, 0, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (53, 13, N'HireDate', N'Hire Date', 5, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (54, 13, N'IDNumber', N'ID Number', 1, NULL, 0, 0, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (55, 13, N'LastName', N'Last Name', 1, NULL, 0, 0, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (56, 13, N'LongFormPhysicalDate', N'Long Form Physical Date', 5, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (57, 13, N'MedicalCardDate', N'Medical Card Date', 5, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (58, 13, N'MobileApp', N'Mobile App', 2, N'0', 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (59, 13, N'MobilePhone', N'Mobile Phone', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (60, 13, N'MobilePrint', N'Mobile Print', 2, N'0', 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (61, 13, N'MobileProvider', N'Mobile Provider', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (62, 13, N'Restrictions', N'Restrictions', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (63, 13, N'SSN', N'SSN', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (64, 13, N'Zip', N'Zip', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (65, 23, N'Name', N'Name', 1, NULL, 0, 0, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (66, 24, N'Email', N'Email', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (67, 24, N'FirstName', N'First Name', 1, NULL, 0, 0, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (68, 24, N'IDNumber', N'ID Number', 1, NULL, 0, 0, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (69, 24, N'LastName', N'Last Name', 1, NULL, 0, 0, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (70, 24, N'MobileApp', N'Mobile App', 2, NULL, 0, 0, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (71, 24, N'MobilePhone', N'Mobile Phone', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (72, 24, N'MobilePrint', N'Mobile Print', 2, NULL, 0, 0, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (73, 24, N'MobileProvider', N'Mobile Provider', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (74, 25, N'Address', N'Address', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (75, 25, N'City', N'City', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (76, 25, N'ContactEmail', N'Contact Email', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (77, 25, N'ContactName', N'Contact Name', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (78, 25, N'ContactPhone', N'Contact Phone', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (79, 25, N'Name', N'Name', 1, NULL, 0, 0, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (80, 25, N'Notes', N'Notes', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (81, 25, N'Zip', N'Zip', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (82, 1, N'ActualMiles', N'Actual Miles', 3, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (83, 1, N'AuditNotes', N'Audit Notes', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (84, 1, N'CarrierTicketNum', N'Carrier Ticket Num', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (85, 1, N'ChainUp', N'Chain Up', 2, N'0', 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (86, 1, N'DeliverDriverNotes', N'Deliver Driver Notes', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (87, 1, N'DeliverPrintDateUTC', N'Deliver Print Date UTC', 5, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (88, 1, N'DestArriveTimeUTC', N'Dest Arrive Time UTC', 5, NULL, 0, 2, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (89, 1, N'DestBOLNum', N'Dest BOL Num', 1, NULL, 0, 2, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (90, 1, N'DestCloseMeterUnits', N'Dest Close Meter Units', 4, NULL, 0, 2, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (91, 1, N'DestDepartTimeUTC', N'Dest Depart Time UTC', 5, NULL, 0, 2, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (92, 1, N'DestGrossUnits', N'Dest Gross Units', 4, NULL, 0, 2, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (93, 1, N'DestMinutes', N'Dest Minutes', 3, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (94, 1, N'DestNetUnits', N'Dest Net Units', 4, NULL, 0, 2, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (95, 1, N'DestOpenMeterUnits', N'Dest Open Meter Units', 4, NULL, 0, 2, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (96, 1, N'DestProductBSW', N'Dest Product BSW', 4, NULL, 0, 2, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (97, 1, N'DestProductGravity', N'Dest Product Gravity', 4, NULL, 0, 2, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (98, 1, N'DestProductTemp', N'Dest Product Temp', 4, NULL, 0, 2, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (99, 1, N'DestRackBay', N'Dest Rack Bay', 1, NULL, 0, 2, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (100, 1, N'DestRailCarNum', N'Dest Rail Car Num', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (101, 1, N'DestTrailerWaterCapacity', N'Dest Trailer Water Capacity', 3, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (102, 1, N'DestTruckMileage', N'Dest Truck Mileage', 3, NULL, 0, 2, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (103, 1, N'DestWaitNotes', N'Dest Wait Notes', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (104, 1, N'DispatchConfirmNum', N'Dispatch Confirm Num', 1, NULL, 0, 2, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (105, 1, N'DispatchNotes', N'Dispatch Notes', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (106, 1, N'DueDate', N'Due Date', 6, NULL, 0, 0, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (107, 1, N'OrderDate', N'Date', 6, NULL, 0, 2, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (108, 1, N'OrderNum', N'Order Num', 3, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (109, 1, N'OriginArriveTimeUTC', N'Origin Arrive Time UTC', 5, NULL, 0, 2, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (110, 1, N'OriginBOLNum', N'Origin BOL Num', 1, NULL, 0, 2, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (111, 1, N'OriginDepartTimeUTC', N'Origin Depart Time UTC', 5, NULL, 0, 2, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (112, 1, N'OriginGrossStdUnits', N'Origin Gross Std Units', 4, NULL, 0, 2, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (113, 1, N'OriginGrossUnits', N'Origin Gross Units', 4, NULL, 0, 2, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (114, 1, N'OriginMinutes', N'Origin Minutes', 3, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (115, 1, N'OriginNetUnits', N'Origin Net Units', 4, NULL, 0, 2, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (116, 1, N'OriginTankNum', N'Origin Tank Num', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (117, 1, N'OriginTruckMileage', N'Origin Truck Mileage', 3, NULL, 0, 2, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (118, 1, N'OriginWaitNotes', N'Origin Wait Notes', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (119, 1, N'PickupDriverNotes', N'Pickup Driver Notes', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (120, 1, N'PickupPrintDateUTC', N'Pickup Print Date UTC', 5, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (121, 1, N'ReassignKey', N'Reassign Key', 3, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (122, 1, N'Rejected', N'Rejected', 2, N'0', 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (123, 1, N'RejectNotes', N'Reject Notes', 1, NULL, 0, 2, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (124, 34, N'Description', N'Description', 1, NULL, 0, 0, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (125, 34, N'Num', N'Num', 1, NULL, 0, 0, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (126, 34, N'RequireNotes', N'Require Notes', 2, N'0', 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (127, 26, N'Notes', N'Notes', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (128, 26, N'RerouteDate', N'Reroute Date', 5, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (129, 26, N'UserName', N'User Name', 1, NULL, 0, 0, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (132, 28, N'OrderStatus', N'Name', 1, NULL, 0, 0, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (133, 28, N'StatusNum', N'Status Num', 3, NULL, 0, 0, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (134, 2, N'BOLNum', N'BOL Num', 1, NULL, 0, 2, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (135, 2, N'BottomFeet', N'Bottom Feet', 3, NULL, 0, 2, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (136, 2, N'BottomInches', N'Bottom Inches', 3, NULL, 0, 2, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (137, 2, N'BottomQ', N'Bottom Q', 3, NULL, 0, 2, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (138, 2, N'CarrierTicketNum', N'Carrier Ticket Num', 1, NULL, 0, 0, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (139, 2, N'CloseMeterUnits', N'Close Meter Units', 4, NULL, 0, 2, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (140, 2, N'ClosingGaugeFeet', N'Closing Gauge Feet', 3, NULL, 0, 2, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (141, 2, N'ClosingGaugeInch', N'Closing Gauge Inch', 3, NULL, 0, 2, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (142, 2, N'ClosingGaugeQ', N'Closing Gauge Q', 3, NULL, 0, 2, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (143, 2, N'DispatchConfirmNum', N'Dispatch Confirm Num', 1, NULL, 0, 2, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (144, 2, N'FromMobileApp', N'From Mobile App', 2, N'0', 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (145, 2, N'GrossStdUnits', N'Gross Std Units', 4, NULL, 0, 2, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (146, 2, N'GrossUnits', N'Gross Units', 4, NULL, 0, 2, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (147, 2, N'MeterFactor', N'Meter Factor', 1, NULL, 0, 2, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (148, 2, N'NetUnits', N'Net Units', 4, NULL, 0, 2, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (149, 2, N'OpeningGaugeFeet', N'Opening Gauge Feet', 3, NULL, 0, 2, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (150, 2, N'OpeningGaugeInch', N'Opening Gauge Inch', 3, NULL, 0, 2, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (151, 2, N'OpeningGaugeQ', N'Opening Gauge Q', 3, NULL, 0, 2, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (152, 2, N'OpenMeterUnits', N'Open Meter Units', 4, NULL, 0, 2, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (153, 2, N'ProductBSW', N'Product BSW', 4, NULL, 0, 2, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (154, 2, N'ProductHighTemp', N'Product High Temp', 4, NULL, 0, 2, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (155, 2, N'ProductLowTemp', N'Product Low Temp', 4, NULL, 0, 2, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (156, 2, N'ProductObsGravity', N'Product Obs Gravity', 4, NULL, 0, 2, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (157, 2, N'ProductObsTemp', N'Product Obs Temp', 4, NULL, 0, 2, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (158, 2, N'Rejected', N'Rejected', 2, N'0', 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (159, 2, N'RejectNotes', N'Reject Notes', 1, NULL, 0, 2, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (160, 2, N'SealOff', N'Seal Off', 1, NULL, 0, 2, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (161, 2, N'SealOn', N'Seal On', 1, NULL, 0, 2, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (162, 2, N'TankNum', N'Tank Num', 1, NULL, 0, 2, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (163, 27, N'DestTruckStartMileage', N'Dest Truck Start Mileage', 3, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (164, 27, N'Notes', N'Notes', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (165, 27, N'OriginTruckEndMileage', N'Origin Truck End Mileage', 3, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (166, 27, N'PercentComplete', N'Percent Complete', 3, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (167, 27, N'TransferComplete', N'Transfer Complete', 2, N'0', 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (168, 19, N'Address', N'Address', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (169, 19, N'CA', N'CA', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (170, 19, N'City', N'City', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (171, 19, N'County', N'County', 1, NULL, 0, 0, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (172, 19, N'CTBNum', N'CTB Num', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (173, 19, N'DrivingDirections', N'Driving Directions', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (174, 19, N'FieldName', N'Field Name', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (175, 19, N'GaugerRequired', N'Gauger Required', 2, N'0', 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (176, 19, N'GeoFenceRadiusMeters', N'Geo Fence Radius Meters', 3, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (177, 19, N'H2S', N'H2S', 2, N'0', 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (178, 19, N'LAT', N'LAT', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (179, 19, N'LeaseName', N'Lease Name', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (180, 19, N'LeaseNum', N'Lease Num', 1, NULL, 0, 0, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (181, 19, N'LegalDescription', N'Legal Description', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (182, 19, N'LON', N'LON', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (183, 19, N'Name', N'Name', 1, NULL, 0, 0, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (184, 19, N'NDICFileNum', N'NDIC File Num', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (185, 19, N'NDM', N'NDM', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (186, 19, N'SpudDate', N'Spud Date', 5, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (187, 19, N'Station', N'Station', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (188, 19, N'TaxRate', N'Tax Rate', 4, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (189, 19, N'TotalDepth', N'Total Depth', 3, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (190, 19, N'UseDST', N'Use DST', 2, N'1', 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (191, 19, N'wellAPI', N'well API', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (192, 19, N'Zip', N'Zip', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (193, 30, N'TankDescription', N'Tank Description', 1, NULL, 0, 0, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (194, 30, N'TankNum', N'Tank Num', 1, NULL, 0, 0, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (195, 31, N'BPQ', N'BPQ', 4, NULL, 0, 0, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (196, 31, N'Feet', N'Feet', 3, NULL, 0, 0, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (197, 31, N'Inches', N'Inches', 3, NULL, 0, 0, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (198, 31, N'IsMaximum', N'Is Maximum', 2, N'0', 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (199, 31, N'IsMinimum', N'Is Minimum', 2, N'0', 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (200, 31, N'Q', N'Q', 3, NULL, 0, 0, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (201, 29, N'CodeDXCode', N'Code DX Code', 3, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (202, 29, N'HasTanks', N'Has Tanks', 2, N'0', 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (203, 29, N'OriginType', N'Origin Type', 1, NULL, 0, 0, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (204, 33, N'Description', N'Description', 1, NULL, 0, 0, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (205, 33, N'Num', N'Num', 1, NULL, 0, 0, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (206, 33, N'RequireNotes', N'Require Notes', 2, N'0', 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (207, 16, N'Address', N'Address', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (208, 16, N'City', N'City', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (209, 16, N'ContactEmail', N'Contact Email', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (210, 16, N'ContactName', N'Contact Name', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (211, 16, N'ContactPhone', N'Contact Phone', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (212, 16, N'Name', N'Name', 1, NULL, 0, 0, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (213, 16, N'Notes', N'Notes', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (214, 16, N'Zip', N'Zip', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (215, 3, N'Name', N'Name', 1, NULL, 0, 0, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (216, 3, N'ShortName', N'Short Name', 1, NULL, 0, 0, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (217, 17, N'ContactEmail', N'Contact Email', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (218, 17, N'ContactPhone', N'Contact Phone', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (219, 17, N'FirstName', N'First Name', 1, NULL, 0, 0, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (220, 17, N'LastName', N'Last Name', 1, NULL, 0, 0, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (221, 10, N'Name', N'Name', 1, NULL, 0, 0, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (222, 35, N'Abbreviation', N'Abbreviation', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (223, 35, N'FullName', N'Full Name', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (224, 21, N'CodeDXCode', N'Code DX Code', 3, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (225, 21, N'CountryID_CSV', N'Country ID_CSV', 1, N'''1''', 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (226, 21, N'ForTanksOnly', N'For Tanks Only', 2, N'0', 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (227, 21, N'Name', N'Name', 1, NULL, 0, 0, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (228, 15, N'AxleCount', N'Axle Count', 3, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (229, 15, N'CIDNumber', N'CID Number', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (230, 15, N'Compartment1Units', N'Compartment1 Units', 4, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (231, 15, N'Compartment2Units', N'Compartment2 Units', 4, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (232, 15, N'Compartment3Units', N'Compartment3 Units', 4, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (233, 15, N'Compartment4Units', N'Compartment4 Units', 4, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (234, 15, N'Compartment5Units', N'Compartment5 Units', 4, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (235, 15, N'Composition', N'Composition', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (236, 15, N'DOTNumber', N'DOT Number', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (237, 15, N'GarageCity', N'Garage City', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (238, 15, N'IDNumber', N'ID Number', 1, NULL, 0, 0, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (239, 15, N'InsuranceIDCardExpiration', N'Insurance ID Card Expiration', 5, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (240, 15, N'InsuranceIDCardIssue', N'Insurance ID Card Issue', 5, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (241, 15, N'LicenseNumber', N'License Number', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (242, 15, N'Make', N'Make', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (243, 15, N'MeterType', N'Meter Type', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (244, 15, N'Model', N'Model', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (245, 15, N'OwnerInfo', N'Owner Info', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (246, 15, N'PurchasePrice', N'Purchase Price', 4, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (247, 15, N'RegistrationExpiration', N'Registration Expiration', 5, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (248, 15, N'SIDNumber', N'SID Number', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (249, 15, N'TireSize', N'Tire Size', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (250, 15, N'TrailerCertDate', N'Trailer Cert Date', 6, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (251, 15, N'VIN', N'VIN', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (252, 15, N'Year', N'Year', 3, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (253, 14, N'AxleCount', N'Axle Count', 3, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (254, 14, N'CIDNumber', N'CID Number', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (255, 14, N'DOTNumber', N'DOT Number', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (256, 14, N'GarageCity', N'Garage City', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (257, 14, N'GPSUnit', N'GPS Unit', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (258, 14, N'HasSleeper', N'Has Sleeper', 2, N'0', 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (259, 14, N'IDNumber', N'ID Number', 1, NULL, 0, 0, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (260, 14, N'InsuranceIDCardExpiration', N'Insurance ID Card Expiration', 5, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (261, 14, N'InsuranceIDCardIssue', N'Insurance ID Card Issue', 5, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (262, 14, N'LicenseNumber', N'License Number', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (263, 14, N'Make', N'Make', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (264, 14, N'MeterType', N'Meter Type', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (265, 14, N'Model', N'Model', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (266, 14, N'OwnerInfo', N'Owner Info', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (267, 14, N'PurchasePrice', N'Purchase Price', 4, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (268, 14, N'RegistrationExpiration', N'Registration Expiration', 5, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (269, 14, N'SIDNumber', N'SID Number', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (270, 14, N'TireSize', N'Tire Size', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (271, 14, N'VIN', N'VIN', 1, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (272, 14, N'Year', N'Year', 3, NULL, 0, 1, 0, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.893' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (273, 12, N'ID', N'ID', 3, NULL, 0, 1, 1, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.980' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (274, 36, N'ID', N'ID', 3, NULL, 0, 1, 1, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.980' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (275, 18, N'ID', N'ID', 3, NULL, 0, 0, 1, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.980' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (276, 11, N'ID', N'ID', 3, NULL, 0, 1, 1, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.980' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (277, 20, N'ID', N'ID', 3, NULL, 0, 1, 1, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.980' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (278, 32, N'ID', N'ID', 3, NULL, 0, 1, 1, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.980' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (279, 22, N'ID', N'ID', 3, NULL, 0, 0, 1, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.980' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (280, 13, N'ID', N'ID', 3, NULL, 0, 1, 1, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.980' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (281, 23, N'ID', N'ID', 3, NULL, 0, 1, 1, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.980' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (282, 24, N'ID', N'ID', 3, NULL, 0, 1, 1, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.980' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (283, 25, N'ID', N'ID', 3, NULL, 0, 1, 1, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.980' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (284, 1, N'ID', N'ID', 3, NULL, 0, 1, 1, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.980' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (285, 34, N'ID', N'ID', 3, NULL, 0, 1, 1, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.980' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (286, 26, N'ID', N'ID', 3, NULL, 0, 1, 1, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.980' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (287, 28, N'ID', N'ID', 3, NULL, 0, 0, 1, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.980' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (288, 2, N'ID', N'ID', 3, NULL, 0, 1, 1, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.980' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (289, 2, N'UID', N'UID', 8, NULL, 0, 1, 1, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.980' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (290, 19, N'ID', N'ID', 3, NULL, 0, 1, 1, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.980' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (291, 30, N'ID', N'ID', 3, NULL, 0, 1, 1, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.980' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (292, 31, N'ID', N'ID', 3, NULL, 0, 1, 1, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.980' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (293, 29, N'ID', N'ID', 3, NULL, 0, 1, 1, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.980' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (294, 33, N'ID', N'ID', 3, NULL, 0, 1, 1, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.980' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (295, 16, N'ID', N'ID', 3, NULL, 0, 1, 1, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.980' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (296, 3, N'ID', N'ID', 3, NULL, 0, 1, 1, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.980' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (297, 17, N'ID', N'ID', 3, NULL, 0, 1, 1, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.980' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (298, 10, N'ID', N'ID', 3, NULL, 0, 1, 1, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.980' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (299, 35, N'ID', N'ID', 3, NULL, 0, 1, 1, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.980' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (300, 21, N'ID', N'ID', 3, NULL, 0, 1, 1, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.980' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (301, 15, N'ID', N'ID', 3, NULL, 0, 1, 1, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.980' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (302, 14, N'ID', N'ID', 3, NULL, 0, 1, 1, 0, NULL, N'ID', CAST(N'2015-12-21 18:49:44.980' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (303, 1, N'CarrierID', N'Carrier', 3, NULL, 0, 2, 0, 0, 12, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (304, 1, N'DestinationID', N'Destination', 3, NULL, 0, 2, 0, 0, 20, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (305, 1, N'DriverID', N'Driver', 3, NULL, 0, 2, 0, 0, 13, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (306, 1, N'OperatorID', N'Operator', 3, NULL, 0, 1, 0, 0, 25, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (307, 1, N'OriginID', N'Origin', 3, NULL, 0, 0, 0, 0, 19, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (308, 1, N'OriginTankID', N'Origin Tank', 3, NULL, 0, 1, 0, 0, 30, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (309, 1, N'OriginWaitReasonID', N'Origin Wait Reason', 3, NULL, 0, 1, 0, 0, 33, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (310, 1, N'ProducerID', N'Producer', 3, NULL, 0, 1, 0, 0, 16, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (311, 1, N'ProductID', N'Product', 3, NULL, 0, 0, 0, 0, 3, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (312, 1, N'PumperID', N'Pumper', 3, NULL, 0, 1, 0, 0, 17, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (313, 1, N'TicketTypeID', N'Ticket Type', 3, NULL, 0, 0, 0, 0, 21, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (314, 1, N'TrailerID', N'Trailer', 3, NULL, 0, 2, 0, 0, 15, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (315, 1, N'TruckID', N'Truck', 3, NULL, 0, 2, 0, 0, 14, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (316, 2, N'OrderID', N'Order', 3, NULL, 0, 0, 0, 0, 1, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (317, 2, N'OriginTankID', N'Origin Tank', 3, NULL, 0, 2, 0, 0, 30, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (318, 2, N'TicketTypeID', N'Ticket Type', 3, NULL, 0, 0, 0, 0, 21, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (319, 11, N'StateID', N'State', 3, NULL, 0, 1, 0, 0, 35, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (320, 12, N'CarrierTypeID', N'Carrier Type', 3, N'1', 0, 0, 0, 0, 36, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (321, 12, N'StateID', N'State', 3, NULL, 0, 1, 0, 0, 35, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (322, 13, N'CarrierID', N'Carrier', 3, NULL, 0, 0, 0, 0, 12, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (323, 13, N'DriverGroupID', N'Driver Group', 3, NULL, 0, 1, 0, 0, 23, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (324, 13, N'OperatingStateID', N'Operating State', 3, NULL, 0, 1, 0, 0, 35, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (325, 13, N'RegionID', N'Region', 3, NULL, 0, 1, 0, 0, 10, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (326, 13, N'StateID', N'State', 3, NULL, 0, 1, 0, 0, 35, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (327, 13, N'TrailerID', N'Trailer', 3, NULL, 0, 1, 0, 0, 15, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (328, 13, N'TruckID', N'Truck', 3, NULL, 0, 1, 0, 0, 14, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (329, 14, N'CarrierID', N'Carrier', 3, NULL, 0, 0, 0, 0, 12, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (330, 14, N'GarageStateID', N'Garage State', 3, NULL, 0, 1, 0, 0, 35, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (331, 15, N'CarrierID', N'Carrier', 3, NULL, 0, 0, 0, 0, 12, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (332, 15, N'GarageStateID', N'Garage State', 3, NULL, 0, 1, 0, 0, 35, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (333, 16, N'StateID', N'State', 3, NULL, 0, 1, 0, 0, 35, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (334, 17, N'OperatorID', N'Operator', 3, NULL, 0, 0, 0, 0, 25, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (335, 19, N'GaugerID', N'Gauger', 3, NULL, 0, 1, 0, 0, 24, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (336, 19, N'GaugerTicketTypeID', N'Gauger Ticket Type', 3, NULL, 0, 1, 0, 0, 21, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (337, 19, N'OperatorID', N'Operator', 3, NULL, 0, 0, 0, 0, 25, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (338, 19, N'OriginTypeID', N'Origin Type', 3, NULL, 0, 0, 0, 0, 29, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (339, 19, N'ProducerID', N'Producer', 3, NULL, 0, 0, 0, 0, 16, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (340, 19, N'PumperID', N'Pumper', 3, NULL, 0, 1, 0, 0, 17, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (341, 19, N'RegionID', N'Region', 3, NULL, 0, 1, 0, 0, 10, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (342, 19, N'StateID', N'State', 3, NULL, 0, 0, 0, 0, 35, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (343, 19, N'TicketTypeID', N'Ticket Type', 3, NULL, 0, 0, 0, 0, 21, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (344, 20, N'RegionID', N'Region', 3, NULL, 0, 0, 0, 0, 10, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (345, 20, N'StateID', N'State', 3, NULL, 0, 0, 0, 0, 35, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (346, 20, N'TicketTypeID', N'Ticket Type', 3, NULL, 0, 0, 0, 0, 21, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (347, 24, N'HomeStateID', N'Home State', 3, NULL, 0, 1, 0, 0, 35, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (348, 24, N'RegionID', N'Region', 3, NULL, 0, 1, 0, 0, 10, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (349, 25, N'StateID', N'State', 3, NULL, 0, 1, 0, 0, 35, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (350, 26, N'OrderID', N'Order', 3, NULL, 0, 0, 0, 0, 1, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (351, 26, N'PreviousDestinationID', N'Previous Destination', 3, NULL, 0, 0, 0, 0, 20, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (352, 27, N'OrderID', N'Order', 3, NULL, 0, 0, 0, 0, 1, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (353, 27, N'OriginDriverID', N'Origin Driver', 3, NULL, 0, 0, 0, 0, 13, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (354, 27, N'OriginTruckID', N'Origin Truck', 3, NULL, 0, 0, 0, 0, 14, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (355, 30, N'OriginID', N'Origin', 3, NULL, 0, 0, 0, 0, 19, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (356, 31, N'OriginTankID', N'Origin Tank', 3, NULL, 0, 0, 0, 0, 30, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (357, 35, N'CountryID', N'Country', 3, NULL, 0, 1, 0, 0, 18, N'ID', CAST(N'2015-12-21 18:49:45.067' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (359, 1, N'OriginWeightNetUnits', N'Origin Weight Net Units', 4, NULL, 0, 2, 0, 0, NULL, N'ID', CAST(N'2016-01-05 17:42:09.377' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (360, 2, N'WeightGrossUnits', N'Weight Gross Units', 4, NULL, 0, 2, 0, 0, NULL, N'ID', CAST(N'2016-01-05 17:42:09.377' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (361, 2, N'WeightNetUnits', N'Weight Net Units', 4, NULL, 0, 2, 0, 0, NULL, N'ID', CAST(N'2016-01-05 17:42:09.377' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (362, 2, N'WeightTareUnits', N'Weight Tare Units', 4, NULL, 0, 2, 0, 0, NULL, N'ID', CAST(N'2016-01-05 17:42:09.377' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (363, 37, N'IsCompleted', N'Is Completed', 2, N'0', 0, 1, 0, 0, NULL, N'ID', CAST(N'2016-01-05 17:42:09.377' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (364, 37, N'Name', N'Name', 1, NULL, 0, 0, 0, 0, NULL, N'ID', CAST(N'2016-01-05 17:42:09.377' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (365, 37, N'ID', N'ID', 3, NULL, 0, 0, 1, 0, NULL, N'ID', CAST(N'2016-01-05 17:43:04.587' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (366, 1, N'DeliverPrintStatusID', N'Deliver Print Status', 3, NULL, 0, 2, 0, 0, 37, N'ID', CAST(N'2016-01-05 17:43:09.980' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (367, 1, N'PickupPrintStatusID', N'Pickup Print Status', 3, NULL, 0, 2, 0, 0, 37, N'ID', CAST(N'2016-01-05 17:43:09.980' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (368, 1, N'StatusID', N'Status', 3, NULL, 0, 0, 0, 0, 28, N'ID', CAST(N'2016-01-05 17:43:09.980' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (369, 38, N'Abbrev', N'Abbrev', 1, NULL, 0, 0, 0, 0, NULL, NULL, CAST(N'2016-01-18 04:08:13.997' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (370, 38, N'GallonEquivalent', N'Gallon Equivalent', 4, NULL, 0, 0, 0, 0, NULL, NULL, CAST(N'2016-01-18 04:08:13.997' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (371, 38, N'ID', N'ID', 3, NULL, 0, 1, 1, 0, NULL, NULL, CAST(N'2016-01-18 04:08:13.997' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (372, 38, N'Name', N'Name', 1, NULL, 0, 0, 0, 0, NULL, NULL, CAST(N'2016-01-18 04:08:13.997' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (373, 1, N'DestUomID', N'Dest Uom', 3, NULL, 0, 1, 0, 0, 38, N'ID', CAST(N'2016-01-18 04:08:36.510' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (374, 1, N'OriginUomID', N'Origin Uom', 3, NULL, 0, 1, 0, 0, 38, N'ID', CAST(N'2016-01-18 04:08:36.510' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (375, 15, N'UomID', N'Uom', 3, NULL, 0, 1, 0, 0, 38, N'ID', CAST(N'2016-01-18 04:08:36.510' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (376, 19, N'UomID', N'Uom', 3, NULL, 0, 1, 0, 0, 38, N'ID', CAST(N'2016-01-18 04:08:36.510' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[tblObjectField] ([ID], [ObjectID], [FieldName], [Name], [ObjectFieldTypeID], [DefaultValue], [ReadOnly], [AllowNullID], [IsKey], [IsCustom], [ParentObjectID], [ParentObjectIDFieldName], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser]) VALUES (377, 20, N'UomID', N'Uom', 3, NULL, 0, 1, 0, 0, 38, N'ID', CAST(N'2016-01-18 04:08:36.510' AS DateTime), N'System', NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[tblObjectField] OFF
GO

/************************************************
 Creation Info: 2016/01/14 - 3.10.1
 Author: Kevin Alons
 Purpose: support routine to generate new tblObjectField records for new fields after they are added to a table/view (and the tblObject table)
 Notes: only intended to be used in the back office to assist future database updates
************************************************/
CREATE PROCEDURE _spAddNewObjectFields(@viewOnly bit = 1) AS
BEGIN
	/* create basic object "data" field definitions*/
	SELECT X.*
	INTO #X
	FROM (
		SELECT ObjectID = O.ID, FieldName = C.COLUMN_NAME, Name = replace(dbo.fnFriendlyName(C.COLUMN_NAME), 'Order Date', 'Date')
			, OFTID = CASE	WHEN C.DATA_TYPE LIKE '%char%' OR C.DATA_TYPE LIKE 'TEXT' THEN 1
							WHEN C.DATA_TYPE IN ('bit') THEN 2
							WHEN C.DATA_TYPE LIKE '%int%' THEN 3
							WHEN C.DATA_TYPE in ('decimal', 'float', 'money', 'smallmoney') THEN 4
							WHEN C.DATA_TYPE LIKE '%datetime%' THEN 5
							WHEN C.DATA_TYPE LIKE '%date%' THEN 6
							WHEN C.DATA_TYPE LIKE '%time%' THEN 7
							WHEN C.DATA_TYPE LIKE 'unique%' THEN 8
							ELSE NULL END
			, DefaultValue = C.COLUMN_DEFAULT
			, AllowNullID = CASE WHEN C.IS_NULLABLE = 'YES' OR C.COLUMN_DEFAULT IS NOT NULL OR COLUMNPROPERTY(object_id(TABLE_NAME), COLUMN_NAME, 'IsIdentity') = 1 THEN 1 ELSE 0 END
			, IsKey = 0
			, IsCustom = 0
			, ParentObjectID = NULL, ParentObjectIDFieldName = NULL
		FROM INFORMATION_SCHEMA.COLUMNS C
		JOIN tblObject O ON O.SqlTargetName like C.TABLE_NAME
		WHERE C.COLUMN_NAME NOT LIKE '%ID' 
			AND C.DATA_TYPE NOT IN ('varbinary')
			AND C.COLUMN_NAME NOT LIKE '%DocName'
			AND C.COLUMN_NAME NOT LIKE '%FileName'
			AND C.COLUMN_NAME NOT LIKE '%CDL%'
			AND C.COLUMN_NAME NOT LIKE 'Create%'
			AND C.COLUMN_NAME NOT LIKE '%LastChange%'
			AND C.COLUMN_NAME NOT LIKE 'Delete%'

		/* add PK fields for each object */
		UNION SELECT ObjectID = O.ID, FieldName = C.COLUMN_NAME, Name = dbo.fnFriendlyName(C.COLUMN_NAME)
			, OFTID = CASE	WHEN C.DATA_TYPE LIKE '%char%' OR C.DATA_TYPE LIKE 'TEXT' THEN 1
							WHEN C.DATA_TYPE IN ('bit') THEN 2
							WHEN C.DATA_TYPE LIKE '%int%' THEN 3
							WHEN C.DATA_TYPE in ('decimal', 'float', 'money', 'smallmoney') THEN 4
							WHEN C.DATA_TYPE LIKE '%datetime%' THEN 5
							WHEN C.DATA_TYPE LIKE '%date%' THEN 6
							WHEN C.DATA_TYPE LIKE '%time%' THEN 7
							WHEN C.DATA_TYPE LIKE 'unique%' THEN 8
							ELSE NULL END
			, DefaultValue = NULL
			, AllowNullID = CASE WHEN C.IS_NULLABLE = 'YES' OR C.COLUMN_DEFAULT IS NOT NULL OR COLUMNPROPERTY(object_id(TABLE_NAME), COLUMN_NAME, 'IsIdentity') = 1 THEN 1 ELSE 0 END
			, IsKey = 1
			, IsCustom = 0
			, ParentObjectID = NULL, ParentObjectIDFieldName = NULL
		FROM INFORMATION_SCHEMA.COLUMNS C
		JOIN tblObject O ON O.SqlTargetName like C.TABLE_NAME
		WHERE C.COLUMN_NAME IN ('ID', 'UID')

		-- add in LINKAGE (foreign key) columns
		UNION SELECT ObjectID = O.ID, FieldName = C.COLUMN_NAME, Name = dbo.fnFriendlyName(C.COLUMN_NAME)
			, OFTID = CASE	WHEN C.DATA_TYPE LIKE '%char%' OR C.DATA_TYPE LIKE 'TEXT' THEN 1
							WHEN C.DATA_TYPE IN ('bit') THEN 2
							WHEN C.DATA_TYPE LIKE '%int%' THEN 3
							WHEN C.DATA_TYPE in ('decimal', 'float', 'money', 'smallmoney') THEN 4
							WHEN C.DATA_TYPE LIKE '%datetime%' THEN 5
							WHEN C.DATA_TYPE LIKE '%date%' THEN 6
							WHEN C.DATA_TYPE LIKE '%time%' THEN 7
							WHEN C.DATA_TYPE LIKE 'unique%' THEN 8
							ELSE NULL END
			, DefaultValue = NULL
			, AllowNullID = CASE WHEN C.IS_NULLABLE = 'YES' OR C.COLUMN_DEFAULT IS NOT NULL OR COLUMNPROPERTY(object_id(TABLE_NAME), COLUMN_NAME, 'IsIdentity') = 1 THEN 1 ELSE 0 END
			, IsKey = 0
			, IsCustom = 0
			, ParentObjectID = OFP.ObjectID
			, ParentObjectIDFieldName = OFP.Name
		FROM tblObject O
		JOIN INFORMATION_SCHEMA.COLUMNS C ON C.TABLE_NAME = O.SqlTargetName AND C.COLUMN_NAME NOT IN ('EIAPADDRegionID')
		JOIN ( 
			SELECT OFP.ID, OFP.ObjectID, OFP.Name, CName = replace(OP.Name, ' ', '') + OFP.FieldName
			FROM tblObject OP 
			JOIN tblObjectField OFP ON OFP.ObjectID = OP.ID
			WHERE OFP.IsKey = 1
		) OFP ON CASE WHEN C.COLUMN_NAME = 'StatusID' THEN 'OrderStatusID' ELSE C.COLUMN_NAME END LIKE '%' + OFP.CName 
	) X
	LEFT JOIN tblObjectField OBF ON OBF.ObjectID = X.ObjectID AND OBF.FieldName = X.FieldName
	WHERE OBF.ID IS NULL AND X.ObjectID NOT IN (28)
	ORDER BY X.ObjectID, X.FieldName

	IF (@viewOnly = 0)
		INSERT INTO tblObjectField (ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, AllowNullID, IsKey, IsCustom, ParentObjectID, ParentObjectIDFieldName)
			SELECT * FROM #X

	SELECT * FROM #X
END
GO

CREATE TABLE tblObjectCustomData
(
  ID int identity(1, 1) NOT NULL CONSTRAINT PK_ObjectCustomData PRIMARY KEY
, FieldID int NOT NULL CONSTRAINT FK_ObjectCustomData_Field FOREIGN KEY REFERENCES tblObjectField(ID)
, ObjectID int NOT NULL
, Value varchar(max) NULL
, CreateDateUTC datetime NOT NULL CONSTRAINT DF_ObjectCustomData_CreateDateUTC DEFAULT (getutcdate())
, CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_ObjectCustomData_CreatedByUser DEFAULT ('System')
, LastChangeDateUTC datetime NULL 
, LastChangedByUser varchar(100) NULL 
, DeleteDateUTC datetime NULL 
, DeletedByUser varchar(100) NULL 
)
GO
CREATE UNIQUE INDEX udxObjectCustomData_Main ON tblObjectCustomData(FieldID, ObjectID)
GO
GRANT SELECT, INSERT, UPDATE ON tblObjectCustomData to role_iis_acct
GO

/***********************************************
Author: Kevin Alons
Date Created: 2015/10/08- 3.9.19.11
Purpose: workaround issue with built-in ISNUMERIC function where a string containing , is considered NUMERIC
Changes:
	-- 3.10.1 - 2015/11/11 - KDA - fix logic issue evaluating @ret contents before they are assigned
***********************************************/
ALTER FUNCTION fnIsNumeric(@expression varchar(max)) RETURNS bit AS
BEGIN
	DECLARE @ret bit
	SET @ret = CASE WHEN @expression IS NULL OR @expression LIKE '%,%' THEN 0 ELSE ISNUMERIC(@expression) END
	RETURN (@ret)
END
GO

/***************************************
Date Created: 18 Jan 2015
Author: Kevin Alons
Purpose: convert a string to an Integer value
Changes:
	3.10.1 - 2015/11/11 - KDA - update logic to handle converting decimal values to INT
***************************************/	
ALTER FUNCTION fnToInt(@value varchar(255)) RETURNS int AS
BEGIN
	DECLARE @ret int
	IF dbo.fnIsNumeric(@value) = 1
		SET @ret = cast(CAST(@value as decimal(30, 0)) as int)
	RETURN (@ret)
END
GO

/****************************************
Creation Info: 3.10.1 - 2015/11/11
Author: Kevin Alons
Purpose: convert the specified value to a consistent varchar(max) value based on TypeID specified
Changes:
****************************************/
CREATE FUNCTION fnConvertValue(@TypeID int, @Value varchar(max)) RETURNS varchar(max) AS
BEGIN 
	SET @Value = CASE @TypeID 
			/* string */	WHEN 1 THEN LTRIM(RTRIM(@Value))
			/* boolean */	WHEN 2 THEN dbo.fnToBool(@Value) 
			/* int */		WHEN 3 THEN CASE WHEN dbo.fnIsNumeric(@Value) = 1 THEN dbo.fnToInt(@Value) ELSE NULL END
			/* double */	WHEN 4 THEN CASE WHEN dbo.fnIsNumeric(@Value) = 1 THEN RTRIM(CAST(@Value as decimal(38, 10))) ELSE NULL END
			/* datetime */	WHEN 5 THEN CASE WHEN ISDATE(@Value) = 1 THEN RTRIM(CAST(@Value as datetime)) ELSE NULL END
			/* date */		WHEN 6 THEN CASE WHEN ISDATE(@Value) = 1 THEN RTRIM(CAST(@Value as date)) ELSE NULL END
			/* time */		WHEN 7 THEN CASE WHEN ISDATE(@Value) = 1 THEN RTRIM(CAST(@Value as time)) ELSE NULL END
			/* guid */		WHEN 8 THEN LTRIM(RTRIM(@value))
							ELSE 'Unknown Type' 
		END
	RETURN (@Value)
END
GO
GRANT EXECUTE ON fnConvertValue TO role_iis_acct
GO

CREATE TABLE tblImportCenterDefinition
(
  ID int identity(1, 1) NOT NULL CONSTRAINT PK_ImportCenterDefinition PRIMARY KEY
, RootObjectID int NOT NULL CONSTRAINT FK_ImportCenterDefinition_Object FOREIGN KEY REFERENCES tblObject(ID) ON DELETE CASCADE
, Name varchar(50) NOT NULL
, UserNames varchar(max) NULL
, CreateDateUTC datetime NOT NULL CONSTRAINT DF_ImportCenterDefinition_CreateDateUTC DEFAULT(getutcdate())
, CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_ImportCenterDefinition_CreatedByUser DEFAULT ('System') 
, LastChangeDateUTC datetime NULL 
, LastChangedByUser varchar(100) NULL 
, DeleteDateUTC datetime NULL 
, DeletedByUser varchar(100) NULL 
)
GO
GRANT SELECT, INSERT, UPDATE ON tblImportCenterDefinition TO role_iis_acct
GO

CREATE TABLE tblImportCenterComparisonType
(
  ID int NOT NULL CONSTRAINT PK_ImportCenterComparisonType PRIMARY KEY
, Name varchar(25) NOT NULL CONSTRAINT UC_ImportCenterComparisonType_Name UNIQUE
, ObjectFieldTypeID_CSV varchar(100) NULL
)
GO
--CREATE UNIQUE INDEX udxImportCenterComparisonType_Name ON tblImportCenterComparisonType(Name)
--GO
GRANT SELECT ON tblImportCenterComparisonType TO role_iis_acct
GO
INSERT INTO tblImportCenterComparisonType
	SELECT 1, 'Equals', NULL
	UNION 
	SELECT 2, '+/- 1 Day', '5,6'
	UNION
	SELECT 3, '+/- 1 Hour', '6,7'
GO

CREATE TABLE tblImportCenterFieldDefinition
(
  ID int identity(1, 1) NOT NULL CONSTRAINT PK_ImportCenterFieldDefinition PRIMARY KEY
, ImportCenterDefinitionID int NOT NULL CONSTRAINT FK_ImportCenterField_ImportCenter FOREIGN KEY REFERENCES tblImportCenterDefinition(ID)
, ObjectFieldID int NOT NULL CONSTRAINT FK_ImportCenterField_Object FOREIGN KEY REFERENCES tblObjectField(ID)
, ParentFieldID int NULL CONSTRAINT FK_ImportCenterField_ImportCenterField FOREIGN KEY REFERENCES tblImportCenterFieldDefinition(ID) 
, CSharpExpression varchar(max) NULL
, IsKey bit NOT NULL CONSTRAINT DF_ImportCenterFieldDefinition_IsKey DEFAULT (0)
, KeyComparisonTypeID int NULL CONSTRAINT FK_ImportCenterFieldDefinition_KeyComparisonType FOREIGN KEY REFERENCES tblImportCenterComparisonType(ID)
, DoUpdate bit NOT NULL CONSTRAINT DF_ImportCenterFieldDefinition_DoUpdate DEFAULT (1)
, CreateDateUTC datetime NOT NULL CONSTRAINT DF_ImportCenterFieldDefinition_CreateDateUTC DEFAULT(getutcdate())
, CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_ImportCenterFieldDefinition_CreatedByUser DEFAULT ('System') 
, LastChangeDateUTC datetime NULL 
, LastChangedByUser varchar(100) NULL 
, DeleteDateUTC datetime NULL 
, DeletedByUser varchar(100) NULL 
)
GO
GRANT SELECT, INSERT, UPDATE ON tblImportCenterFieldDefinition TO role_iis_acct
GO
CREATE UNIQUE INDEX udxImportCenterFieldDefinition_Main ON tblImportCenterFieldDefinition(ImportCenterDefinitionID, ObjectFieldID, ParentFieldID) 
GO
/**********************************************************
 Date Created: 2015/12/21
 Author: Kevin Alons
 Purpose: achieve ON CASCADE DELETE ON the self-referencing ParentFieldID field (can't be done conventionally due to this being a self-referencing table)
 *********************************************************/
CREATE TRIGGER trigImportCenterFieldDefinition_IOD ON tblImportCenterFieldDefinition INSTEAD OF DELETE AS
BEGIN
	DECLARE @ID TABLE (ID int);

	WITH WithChildren (ID, Level) AS (
		SELECT d.ID, Level = 0 FROM deleted d
		UNION ALL
		SELECT ICFD.ID, WC.Level + 1
		FROM tblImportCenterFieldDefinition ICFD
		JOIN WithChildren WC ON WC.ID = ICFD.ParentFieldID
	)

	INSERT INTO @ID SELECT ID FROM WithChildren ORDER BY Level DESC

	DELETE FROM tblImportCenterFieldDefinition 
	FROM tblImportCenterFieldDefinition ICFD
	JOIN @ID I ON I.ID = ICFD.ID
END
GO

CREATE TABLE tblImportCenterFieldDefinitionField
(
  ID int identity(1, 1) NOT NULL CONSTRAINT PK_ImportCenterFieldDefinitionField PRIMARY KEY CLUSTERED
, ImportCenterFieldID int NOT NULL CONSTRAINT FK_ImportCenterFieldDefinitionField_ImportCenterField FOREIGN KEY REFERENCES tblImportCenterFieldDefinition(ID) ON DELETE CASCADE
, ImportFieldName varchar(100) NOT NULL
, Position tinyint NOT NULL 
)
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblImportCenterFieldDefinitionField TO role_iis_acct
GO

/*****************************************************
 Creation Info: 2015/11/19 - 3.10.1
 Author: Kevin Alons
 Purpose: count the number of times the @charToFind exists in the provided @expresson string
 Changes:
*****************************************************/
CREATE FUNCTION fnCountChars(@expression varchar(max), @charToFind char(1)) RETURNS int AS
BEGIN
	DECLARE @ret int

	SET @ret = LEN(@expression) - LEN(REPLACE(@expression, @charToFind, ''))

	RETURN (@ret)
END
GO
GRANT EXECUTE ON fnCountChars TO role_iis_acct
GO

/************************************************
 Creation Info: 3.10.1 - 2015/11/13
 Author: Kevin Alons
 Purpose: return the tblObjectField data + relevant parent tblObject data
 Changes:
************************************************/
CREATE VIEW viewObjectField AS
	SELECT ObF.*
		, AllowNull = cast(AllowNullID as bit) -- this cast will convert 1 and 2 to TRUE, leaving 0 as FALSE
		, Object = O.Name
		, ObjectSqlSourceName = O.SqlSourceName
		, ObjectSqlTargetName = O.SqlTargetName
	FROM tblObjectField ObF
	JOIN tblObject O ON O.ID = ObF.ObjectID
GO
GRANT SELECT ON viewObjectField TO role_iis_acct
GO

EXEC _spDropProcedure 'spCloneImportCenterDefinition'
GO
/*****************************************************
 Creation Info: 3.10.1 - 2015/12/15
 Author: Kevin Alons
 Purpose: clone an existing ImportCenterDefinition with a new name
 Changes:
*****************************************************/
CREATE PROCEDURE spCloneImportCenterDefinition(@importCenterDefinitionID int, @Name varchar(50), @UserName varchar(100), @NewID int = NULL OUTPUT) AS
BEGIN
	BEGIN TRAN CICD

	-- copy the basic ImportCenterDefinition first
	INSERT INTO tblImportCenterDefinition (RootObjectID, Name, UserNames, CreatedByUser)
		SELECT RootObjectID, @Name, UserNames, @UserName FROM tblImportCenterDefinition WHERE ID = @importCenterDefinitionID
	SET @NewID = SCOPE_IDENTITY()

	-- add the root ImportFieldDefinitions (with no ParentFieldID)
	INSERT INTO tblImportCenterFieldDefinition (ImportCenterDefinitionID, ObjectFieldID, IsKey, DoUpdate, CreatedByUser)
		SELECT @NewID, ObjectFieldID, IsKey, DoUpdate, CreatedByUser
		FROM tblImportCenterFieldDefinition
		WHERE ImportCenterDefinitionID = @importCenterDefinitionID
		  AND ParentFieldID IS NULL

	DECLARE @Level int; SET @Level = 0

	-- create a table to store the old-new ImportField mappings (will be used later to add the ImportFieldDefinition_Fields)
	DECLARE @Map TABLE (OID int, NID int, Level int)
	-- populate with the just added root ImportFieldDefinitions
	INSERT INTO @Map 
		SELECT OICFD.ID, NICFD.ID, @Level
		FROM tblImportCenterFieldDefinition OICFD
		JOIN tblImportCenterFieldDefinition NICFD ON NICFD.ImportCenterDefinitionID = @NewID AND NICFD.ObjectFieldID = OICFD.ObjectFieldID
		WHERE OICFD.ImportCenterDefinitionID = @importCenterDefinitionID AND OICFD.ParentFieldID IS NULL

	--DECLARE @debugMap XML = (SELECT * FROM @Map FOR XML AUTO)

	-- create table to remember newly added child FieldDefinition records
	DECLARE @NID IDTABLE
	-- populate with the first child FieldDefinitions to be added
	INSERT INTO @NID 
		SELECT ID FROM tblImportCenterFieldDefinition X JOIN @Map M ON M.OID = X.ParentFieldID WHERE ImportCenterDefinitionID = @importCenterDefinitionID EXCEPT SELECT OID FROM @Map

	--DECLARE @debugNID XML = (SELECT * FROM @NID FOR XML AUTO)

	WHILE EXISTS (SELECT ID FROM @NID)
	BEGIN
		-- add the next level of children FieldDefinitions
		INSERT INTO tblImportCenterFieldDefinition (ImportCenterDefinitionID, ObjectFieldID, ParentFieldID, CSharpExpression, IsKey, KeyComparisonTypeID, DoUpdate, CreatedByUser)
			SELECT @NewID, ObjectFieldID, M.NID, CSharpExpression, IsKey, KeyComparisonTypeID, DoUpdate, CreatedByUser
			FROM tblImportCenterFieldDefinition ICFD
			JOIN @NID NID ON NID.ID = ICFD.ID
			JOIN @Map M ON M.OID = ICFD.ParentFieldID

		-- record the old-new mappings for these new FieldDefinitions
		INSERT INTO @Map 
			SELECT OICFD.ID, NICFD.ID, @Level
			FROM tblImportCenterFieldDefinition OICFD
			JOIN tblImportCenterFieldDefinition NICFD ON NICFD.ImportCenterDefinitionID = @NewID AND NICFD.ObjectFieldID = OICFD.ObjectFieldID
			WHERE OICFD.ID IN (SELECT ID FROM @NID)

		--SELECT @debugMap = (SELECT * FROM @Map FOR XML AUTO)

		-- see if any next level new FieldMappings need to be processed
		DELETE FROM @NID
		INSERT INTO @NID 
			SELECT ID FROM tblImportCenterFieldDefinition X JOIN @Map M ON M.OID = X.ParentFieldID WHERE ImportCenterDefinitionID = @importCenterDefinitionID EXCEPT SELECT OID FROM @Map

		--SELECT @debugNID = (SELECT * FROM @NID FOR XML AUTO)
		
		-- increment @Level
		SET @Level = @Level + 1
	END

	-- add all the FieldDefinitionFields based on the accumulated @Map records
	INSERT INTO tblImportCenterFieldDefinitionField (ImportCenterFieldID, ImportFieldName, Position)
		SELECT M.NID, DF.ImportFieldName, DF.Position
		FROM tblImportCenterFieldDefinitionField DF
		JOIN @Map M ON M.OID = DF.ImportCenterFieldID

	COMMIT TRAN CICD
END
GO
GRANT EXECUTE ON spCloneImportCenterDefinition TO role_iis_acct
GO

/*******************************************************
 Creation Info:	3.10.1 - 2016/01/04
 Author:		Kevin Alons
 Purpose:		recursive function to determine the nestLevel of a single ImportCenterFieldDefinition record
 Changes:
*******************************************************/
CREATE FUNCTION fnICFNestLevel(@id int) RETURNS int AS
BEGIN
	DECLARE @ParentFieldID int
	SELECT @ParentFieldID = ParentFieldID FROM tblImportCenterFieldDefinition WHERE ID = @id
	RETURN (CASE WHEN @ParentFieldID IS NULL THEN 0 ELSE 1 + dbo.fnICFNestLevel(@ParentFieldID) END)
END
GO
GRANT EXECUTE ON dbo.fnICFNestLevel TO role_iis_acct
GO

/*******************************************************
 Creation Info:	3.10.1 - 2016/01/07
 Author:		Kevin Alons
 Purpose:		function to retrieve the the ImportFieldNames (in Position Order) as CSV value for the specified ImportCenterFieldID
 Changes:
*******************************************************/
CREATE FUNCTION fnICF_FieldCSV(@id int) RETURNS varchar(max) AS
BEGIN
	DECLARE @ret varchar(max); SET @ret = ''

	DECLARE @ifn varchar(100)
	DECLARE @pos int; SET @pos = 0
	SELECT @ifn = ImportFieldName FROM tblImportCenterFieldDefinitionField WHERE ImportCenterFieldID = @id AND Position = @pos
	WHILE (@ifn IS NOT NULL)
	BEGIN
		SELECT @ret = @ret + ',' + @ifn, @pos = @pos + 1
		SET @ifn = (SELECT ImportFieldName FROM tblImportCenterFieldDefinitionField WHERE ImportCenterFieldID = @id AND Position = @pos)
	END
	RETURN substring(@ret, 2, 8000)
END
GO
GRANT EXECUTE ON dbo.fnICF_FieldCSV TO role_iis_acct
GO

/*******************************************************
 Creation Info:	3.10.1 - 2016/01/10
 Author:		Kevin Alons
 Purpose:		return all fields + some support fields for tblObject table
 Changes:
*******************************************************/
CREATE VIEW viewObject AS
	SELECT O.*
		, IDFieldName = (SELECT TOP 1 FieldName FROM tblObjectField OBF WHERE OBF.ObjectID = O.ID AND IsKey = 1)
	FROM tblObject O
GO
GRANT SELECT ON viewObject TO role_iis_acct
GO

/*******************************************************
 Creation Info:	3.10.1 - 2016/01/04
 Author:		Kevin Alons
 Purpose:		return all fields + some support fields for tblImportCenterFieldDefinition
 Changes:
*******************************************************/
CREATE VIEW viewImportCenterFieldDefinition AS
	SELECT ICDF.* 
		, HasChildren = CASE WHEN EXISTS (SELECT ID FROM tblImportCenterFieldDefinition WHERE ParentFieldID = ICDF.ID) THEN 1 ELSE 0 END
		, Name = OBF.Name , ObjectName = OBF.Object, OBF.FieldName, OBF.ObjectFieldTypeID, OBF.ObjectID, OBF.ObjectSqlSourceName, OBF.ObjectSqlTargetName
			, IDFieldName = (SELECT TOP 1 FieldName FROM tblObjectField WHERE ObjectID = OBF.ObjectID AND IsKey = 1)
			, OBF.AllowNull
			, OBF.ParentObjectID
			, OBF.DefaultValue
		, NestLevel = dbo.fnICFNestLevel(ICDF.ID) 
	FROM tblImportCenterFieldDefinition ICDF
	JOIN viewObjectField OBF ON OBF.ID = ICDF.ObjectFieldID
	LEFT JOIN tblImportCenterFieldDefinition PICDF ON PICDF.ID = ICDF.ParentFieldID
	LEFT JOIN viewObjectField POBF ON POBF.ID = PICDF.ObjectFieldID
GO
GRANT SELECT ON viewImportCenterFieldDefinition  TO role_iis_acct
GO

COMMIT
SET NOEXEC OFF
/* 
select * from tblimportcenterfielddefinition
select * from viewobjectfield where id = 355
SELECT * FROM dbo.viewImportCenterFieldDefinition where importcenterdefinitionid = 1 ORDER BY Nestlevel DESC, ObjectID

select * from tblobjectfield where id = 180
select * from dbo.fnObjectFieldTree(2) where name like 'origin tank%'
select * from tblobjectfield where fieldname like 'orderdate'

select id, count(*) from dbo.fnObjectFieldTree(2) group by id having count(*) > 1
select * from tblobjectfield where id = 222

select * from tblobject
select * from tblobjectfield where objectid = 1 and parentobjectid is not null
-- TODO --
- recursively iterate over ObjectFields, creating Name.Name.Field list (stop recursion at object repeats)
- recursively iterate from root object to each parent object records
	- query each parent object, if 1 record is found, assign that record's ID value to the "parent" record
- at the root record, ensure all required fields are specified, and validate the data (put the success or failure + reasons in temp table column)
- return the evaluated data for review processes

select * from tblimportcenterdefinition 
select * from tblobjectfield where objectid = 2
select * from tblobjectfield where objectid = 1
select * from tblobjectfield where objectid = 19
select * from tblobjectfield where objectid = 30
insert into tblimportcenterdefinition (RootObjectID, Name) values (2, 'test')
insert into tblImportCenterFieldDefinition (ImportCenterDefinitionID, ObjectFieldID) values (1, 316)
insert into tblImportCenterFieldDefinition (ImportCenterDefinitionID, ObjectFieldID, ParentFieldID) values (1, 307, 1)
insert into tblImportCenterFieldDefinition (ImportCenterDefinitionID, ObjectFieldID, ParentFieldID) values (1, 180, 2)
insert into tblImportCenterFieldDefinition (ImportCenterDefinitionID, ObjectFieldID, ParentFieldID) values (1, 308, 1)
insert into tblImportCenterFieldDefinition (ImportCenterDefinitionID, ObjectFieldID, ParentFieldID) values (1, 194, 4)

select * from tblImportCenterFieldDefinition where importcenterdefinitionid = 1
exec spCloneImportCenterDefinition 1, 'test2', 'kalons'
*/
