SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.4.2'
SELECT  @NewVersion = '4.4.3'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, ''
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/**********************************************************
-- Creation Info: 3.13.1 - 2016/07/06
-- Author: Kevin Alons
-- Purpose: return the Audit table records (optionally filtered to just the specified driver and/or ordernum) - without GPS info
-- Changes:
--		4.4.3		2016/11/17		JAE			**NO CHANGE** (ticket was closed)
***********************************************************/
ALTER FUNCTION fnOrders_AllTickets_Audit_NoGPS
(
  @carrierID int
, @driverID int
, @orderNum int
, @id int
) RETURNS TABLE AS RETURN
	SELECT *
		, HasError = cast(CASE WHEN Errors IS NULL THEN 0 ELSE 1 END as bit)
	FROM (
		SELECT O.* 
			, OriginLatLon = ltrim(OO.LAT) + ',' + ltrim(OO.LON)
			, OriginGeoFenceRadiusMeters = OO.GeoFenceRadiusMeters
			, DestLatLon = ltrim(D.LAT) + ',' + ltrim(D.LON)
			, DestGeoFenceRadiusMeters = D.GeoFenceRadiusMeters
			, OriginGrossBBLS = dbo.fnConvertUOM(O.OriginGrossUnits, O.OriginUomID, 1)
			, Errors = 
				nullif(
					SUBSTRING(
						CASE WHEN O.OrderDate IS NULL THEN ',Missing Order Date' ELSE '' END
					  + CASE WHEN O.OriginArriveTimeUTC IS NULL THEN ',Missing Pickup Arrival' ELSE '' END
					  + CASE WHEN O.OriginDepartTimeUTC IS NULL THEN ',Missing Pickup Departure' ELSE '' END
					  + CASE WHEN O.Rejected = 0 AND O.Tickets IS NULL THEN ',No Active Tickets' ELSE '' END
					  + CASE WHEN O.Rejected = 0 AND O.DestArriveTimeUTC IS NULL THEN ',Missing Delivery Arrival' ELSE '' END
					  + CASE WHEN O.Rejected = 0 AND O.DestDepartTimeUTC IS NULL THEN ',Missing Delivery Departure' ELSE '' END
					  + CASE WHEN O.Rejected = 0 AND O.OriginGrossUnits = 0 AND O.OriginNetUnits = 0 AND O.OriginWeightNetUnits = 0 THEN ',No Origin Units are entered. '+CHAR(13)+CHAR(10)+' This may be an unmarked rejected load.' ELSE '' END
					  + CASE WHEN O.Rejected = 0 AND dbo.fnConvertUOM(O.OriginGrossUnits, O.OriginUomID, 2) > isnull(OORMG.MaxGallons, 320) THEN ',Origin GOV Units out of limits' ELSE '' END
					  + CASE WHEN O.Rejected = 0 AND dbo.fnConvertUOM(O.OriginGrossStdUnits, O.OriginUomID, 2) > isnull(OORMG.MaxGallons, 320) THEN ',Origin GSV Units out of limits' ELSE '' END
					  + CASE WHEN O.Rejected = 0 AND dbo.fnConvertUOM(O.OriginNetUnits, O.OriginUomID, 2) > isnull(OORMG.MaxGallons, 320) THEN ',Origin NSV Units out of limits' ELSE '' END				  
					  + CASE WHEN dbo.fnToBool(OOR.Value) = 1 AND (SELECT count(*) FROM tblOrderTicket OT WHERE OrderID = O.ID AND OT.DeleteDateUTC IS NULL AND OT.DispatchConfirmNum IS NULL) > 0 THEN ',Missing Ticket Shipper PO' ELSE '' END
					  + CASE WHEN EXISTS(SELECT CustomerID FROM viewOrder_OrderTicket_Full WHERE ID = O.ID AND DeleteDateUTC IS NULL AND nullif(rtrim(T_DispatchConfirmNum), '') IS NOT NULL GROUP BY CustomerID, T_DispatchConfirmNum HAVING COUNT(*) > 1) THEN ',Duplicate Shipper PO' ELSE '' END
					  + CASE WHEN O.TruckID IS NULL THEN ',Truck Missing' ELSE '' END
					  + CASE WHEN O.TrailerID IS NULL THEN ',Trailer 1 Missing' ELSE '' END
					, 2, 8000) 
				, '')
			, IsEditable = cast(CASE WHEN O.StatusID = 3 THEN 1 ELSE 0 END as bit)
		FROM viewOrder_AllTickets O
		JOIN tblOrigin OO ON OO.ID = O.OriginID
		JOIN tblCustomer C ON C.ID = O.CustomerID
		JOIN tblDestination D ON D.ID = O.DestinationID
		-- max Gallons for this order
		OUTER APPLY (SELECT TOP 1 MaxGallons = cast(Value as decimal(18, 2)) FROM dbo.fnOrderOrderRules(O.ID) WHERE TypeID = 6) OORMG 
		-- ShipperPO_Required OrderRule
		OUTER APPLY (SELECT TOP 1 Value FROM dbo.fnOrderOrderRules(O.ID) WHERE TypeID = 2) OOR
		LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
		LEFT JOIN tblOrderSettlementShipper OIC ON OIC.OrderID = O.ID
		LEFT JOIN tblOrderSettlementCarrier OSC ON OSC.OrderID = O.ID
		LEFT JOIN tblOrderSettlementProducer OSP ON OSP.OrderID = O.ID
		WHERE OIC.BatchID IS NULL AND OSC.BatchID IS NULL AND OSP.BatchID IS NULL /* don't even show SETTLED orders on the AUDIT page ever */
		  AND nullif(OA.Approved, 0) IS NULL /* don't show Approved orders on the AUDIT page */
		  AND (isnull(@carrierID, 0) = -1 OR O.CarrierID = @carrierID)
		  AND (isnull(@driverID, 0) = -1 OR O.OriginDriverID = @driverID)
		  AND ((nullif(@orderNum, 0) IS NULL AND O.DeleteDateUTC IS NULL AND (O.StatusID = 3 AND DeliverPrintStatusID IN (SELECT ID FROM tblPrintStatus WHERE IsCompleted = 1))) OR O.OrderNum = @orderNum)
		  AND (nullif(@id, 0) IS NULL OR O.ID = @id)
	) X

GO

COMMIT
SET NOEXEC OFF
