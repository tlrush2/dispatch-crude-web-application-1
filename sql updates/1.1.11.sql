BEGIN TRAN
GO

/***********************************/
-- Date Created: 28 Feb 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc for 3rd parties (Customer/Carrier)
/***********************************/
ALTER PROCEDURE [dbo].[spOrderTicketFullExport]
(
  @StartDate datetime
, @EndDate datetime
, @CarrierID int = 0 -- -1 = all carriers
, @CustomerID int = 0 -- -1 = all customers
) AS BEGIN
	SELECT OT.ID
		, O.OrderNum
		, O.OrderStatus
		, OT.TicketType 
		, OO.WellAPI
		, O.OriginDepartTime
		, OT.CarrierTicketNum AS TicketNum
		, OO.Name AS Origin
		, OO.LeaseName
		, O.Operator
		, O.Destination
		, OT.NetBarrels
		, TT.HeightFeet AS TankHeight
		, TT.CapacityBarrels AS TankBarrels
		, OT.TankNum
		, OT.ProductObsGravity
		, OT.ProductObsTemp
		, OT.ProductBSW
		, isnull(OT.ProductHighTemp, OT.ProductObsTemp) AS ProductHighTemp
		, OT.OpeningGaugeFeet
		, OT.OpeningGaugeInch
		, OT.OpeningGaugeQ
		, OT.ClosingGaugeFeet
		, OT.ClosingGaugeInch
		, OT.ClosingGaugeQ
		, isnull(OT.ProductLowTemp, OT.ProductObsTemp) AS ProductLowTemp
		, dbo.fnGaugeQtrInches(OT.OpeningGaugeFeet, OT.OpeningGaugeInch, OT.OpeningGaugeQ) AS OpenTotalQ
		, dbo.fnGaugeQtrInches(OT.ClosingGaugeFeet, OT.ClosingGaugeInch, ot.ClosingGaugeQ) AS CloseTotalQ
	FROM dbo.viewOrderTicket OT
	JOIN dbo.viewOrder O ON O.ID = OT.OrderID
	JOIN dbo.tblOrigin OO ON OO.ID = O.OriginID
	LEFT JOIN dbo.tblTankType TT ON TT.ID = OT.TankTypeID
	WHERE O.StatusID IN (3, 4) -- Delivered & Audited
	  AND (@CarrierID=-1 OR @CustomerID=-1 OR O.CarrierID=@CarrierID OR O.CustomerID=@CustomerID) 
	  AND O.OriginDepartTime >= @StartDate AND O.OriginDepartTime < dateadd(day, 1, @EndDate) 
	  AND O.DeleteDate IS NULL AND OT.DeleteDate IS NULL
	ORDER BY O.OriginDepartTime
	
END

GO

UPDATE tblSetting SET Value = '1.1.11' WHERE ID=0

COMMIT