SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.6.9'
SELECT  @NewVersion = '4.6.9.1'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Fix to viewOrderTransferAll logic'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/***************************************
 Created: 4.6.9 - 2017.05.07 - Kevin Alons
 Purpose: return all Orders with OrderTransfer info with "translated friendly" values
 Changes:
--	 4.6.9.1	2017/05/23	JAE			Add IsTransfer flag to indicate which orders are transfers
***************************************/
ALTER VIEW viewOrderTransferAll AS
SELECT OTAOrderID = O.ID /* using OTAOrderID so we can later include all columns with * without an OrderID clash */
	, IsTransfer = CAST((CASE WHEN OTR.OrderID IS NOT NULL THEN 1 ELSE 0 END) AS BIT)	
	, OriginDriverID = ISNULL(OTR.OriginDriverID, O.DriverID)
	, OriginDriverGroupID = ISNULL(ODG.ID, DDG.ID) 
	, OriginDriverGroup = ISNULL(ODG.Name, DDG.Name)
	, OriginDriver = ISNULL(vODR.FullName, vDDR.FullName)
	, OriginDriverFirst = ISNULL(vODR.FirstName, vDDR.FirstName)
	, OriginDriverLast = ISNULL(vODR.LastName, vDDR.LastName)
	, OriginTruckID = ISNULL(OTR.OriginTruckID, O.TruckID)
	, OriginTruck = ISNULL(vOTRU.FullName, vDTRU.FullName)
	, OriginTruckTerminalID = ISNULL(vOTRU.TerminalID, vDTRU.TerminalID)
	, OriginTruckTerminal = ISNULL(vOTRU.Terminal, vDTRU.Terminal)
	, OriginDriverTerminalID = ISNULL(vODR.TerminalID, D.TerminalID)
	, OriginDriverTerminal = ISNULL(vODR.Terminal, D.Terminal)
	, DestDriverID = CASE WHEN OTR.OrderID IS NULL THEN NULL ELSE O.DriverID END
	, DestDriver = CASE WHEN OTR.OrderID IS NULL THEN NULL ELSE D.FullName END
	, DestDriverFirst = CASE WHEN OTR.OrderID IS NULL THEN NULL ELSE D.FirstName END
	, DestDriverLast = CASE WHEN OTR.OrderID IS NULL THEN NULL ELSE D.LastName END
	, DestTruckID = CASE WHEN OTR.OrderID IS NULL THEN NULL ELSE O.TruckID END
	, DestTruck = CASE WHEN OTR.OrderID IS NULL THEN NULL ELSE TRU.FullName END
	, DestTruckTerminalID = CASE WHEN OTR.OrderID IS NULL THEN NULL ELSE TRU.TerminalID END
	, DestTruckTerminal = CASE WHEN OTR.OrderID IS NULL THEN NULL ELSE TRU.Terminal END
	, DestDriverTerminalID = CASE WHEN OTR.OrderID IS NULL THEN NULL ELSE D.TerminalID END
	, DestDriverTerminal = CASE WHEN OTR.OrderID IS NULL THEN NULL ELSE D.Terminal END
	, OriginTruckEndMileage = OTR.OriginTruckEndMileage
	, DestTruckStartMileage = OTR.DestTruckStartMileage
	, TransferPercent = OTR.PercentComplete
	, TransferNotes = OTR.Notes
	, TransferDateUTC = OTR.CreateDateUTC
	, TransferComplete = CAST(OTR.TransferComplete AS BIT)
FROM tblOrder O
LEFT JOIN tblOrderTransfer OTR ON OTR.OrderID = O.ID
LEFT JOIN viewOrigin vO ON vO.ID = O.OriginID
LEFT JOIN viewDestination vD ON vD.ID = O.DestinationID
LEFT JOIN viewDriverBase D ON D.ID = O.DriverID
LEFT JOIN viewDriverBase vODR ON vODR.ID = OTR.OriginDriverID
LEFT JOIN viewDriver vDDR ON vDDR.ID = O.DriverID
LEFT JOIN tblDriverGroup ODG ON ODG.ID = vODR.DriverGroupID
LEFT JOIN tblDriverGroup DDG ON DDG.ID = D.DriverGroupID
LEFT JOIN viewTruck TRU ON TRU.ID = O.TruckID
LEFT JOIN viewTruck vOTRU ON vOTRU.ID = OTR.OriginTruckID
LEFT JOIN viewTruck vDTRU ON vDTRU.ID = O.TruckID

GO

/***************************************/
-- Date Created: 2012.11.25 - Kevin Alons
-- Purpose: return Order records with "translated friendly" values for FK relationships
-- Changes:
-- 3.9.20  - 2015/10/22 - KDA	- add Origin|Dest DriverID fields (using new tblOrderTransfer table) 
--			2015/10/28  - JAE	- added all Order Transfer fields for ease of use in reporting
--			2015/11/03  - BB	- added cast to make TransferComplet BIT type to avoid "Operand type clash" error when running this update script
-- 3.9.21  - 2015/11/03 - KDA	- add OriginDriverGroupID field (from OrderTransfer.OriginDriverID JOIN)
-- 3.9.25  - 2015/11/10 - JAE	- added origin driver and truck to view
-- 3.10.10 - 2016/02/15 - BB	- Add driver region ID to facilitate new user/region filtering feature
-- 3.10.11 - 2016/02/24 - JAE	- Add destination region (name) to view
-- 3.10.13 - 2016/02/29 - JAE	- Add TruckType
-- 3.13.8  - 2016/07/26	- BB	- Add Destination Station
-- 4.1.0.2 - 2016.08.28 - KDA	- rewrite RerouteCount, TicketCount to use a normal JOIN (instead of uncorrelated sub-query)
--								- use viewDriverBase instead of viewDriver
--								- eliminate viewGaugerBase (was unused), use tblOrderStatus instead of viewOrderPrintStatus
-- 4.1.3.3	- 2016/09/14 - BB	- Add origin and destination driving directions (initially for dispatch and truck order create pages)
-- 4.1.8.6	- 2016.09.24 - KDA	- move H2S | TicketCount | RerouteCount fields from viewOrder down to viewOrderBase
-- 4.5.0	- 2017/01/26 - BSB/JAE	- Add driver terminal
-- 4.5.11	- 2017/02/08 - JAE	- Add Contract info
-- 4.5.13	- 2017/02/23 - JAE	- Add Consignee
-- 4.6.9	- 2017.05.07 - KDA	- use viewOrderTransferAll instead of embedded queries (very slightly slower here, but leverages logic in one place)
-- 4.6.9.1	- 2017/05/23 - JAE	- push IsTransfer to viewordertransferall since view was returning all tickets needed a way to flag actual transfers
/***************************************/
ALTER VIEW viewOrder AS
SELECT O.*
	-- GENERATED orders that were GAUGED to COMPLETION or SKIPPED will show "Gauger [gauger status]"
	, PrintStatus = CASE WHEN O.StatusID = -10 AND GOS.IsComplete = 1 THEN 'Gauger ' + GOS.Name ELSE OPS.OrderStatus END
FROM (
	SELECT O.*
	, Origin = vO.Name 
	, OriginFull = vO.FullName 
	, OriginState = vO.State 
	, OriginStateAbbrev = vO.StateAbbrev 
	, OriginStation = vO.Station
	, OriginCounty = vO.County 
	, OriginDrivingDirections = vO.DrivingDirections	-- 4.1.3.3
	, vO.LeaseName
	, vO.LeaseNum
	, OriginTerminal = vO.Terminal
	, OriginRegion = vO.Region
	, OriginCountryID = vO.CountryID
	, OriginCountry = vO.Country
	, OriginCountryShort = vO.CountryShort
	, OriginLegalDescription = vO.LegalDescription 
	, OriginNDIC = vO.NDICFileNum 
	, OriginNDM = vO.NDM 
	, OriginCA = vO.CA 
	, OriginTimeZoneID = vO.TimeZoneID 
	, OriginUseDST = vO.UseDST 
	, Destination = vD.Name 
	, DestinationFull = vD.FullName 
	, DestinationState = vD.State 
	, DestinationStateAbbrev = vD.StateAbbrev 
	, DestinationDrivingDirections = vD.DrivingDirections	-- 4.1.3.3
	, DestinationTypeID = vD.ID
	, DestinationStation = vD.Station -- 3.13.8
	, vD.DestinationType
	, DestStation = vD.Station 
	, DestTimeZoneID = vD.TimeZoneID 
	, DestUseDST = vD.UseDST 
	, DestTerminal = vD.Terminal
	, DestRegion = vD.Region -- 3.10.13
	, DestCountryID = vD.CountryID
	, DestCountry = vD.Country
	, DestCountryShort = vD.CountryShort
	, Customer = C.Name 
	, Consignee = CO.Name
	, Carrier = CA.Name 
	, CarrierType = CT.Name 
	, OS.OrderStatus
	, OS.StatusNum
	, Driver = D.FullName 
	, DriverFirst = D.FirstName 
	, DriverLast = D.LastName 
	, DriverRegionID = D.RegionID  -- 3.10.10
	, DriverTerminalID = D.TerminalID
	, DriverTerminal = D.Terminal
	, Truck = TRU.FullName 
	, Trailer = TR1.FullName 
	, TrailerTerminalID = TR1.TerminalID
	, TrailerTerminal = TR1.Terminal
	, Trailer2 = TR2.FullName 
	, Trailer2TerminalID = TR2.TerminalID
	, Trailer2Terminal = TR2.Terminal
	, P.PriorityNum
	, TicketType = TT.Name 
	, DestTicketTypeID = vD.TicketTypeID 
	, DestTicketType = vD.TicketType 
	, Operator = OP.Name 
	, Producer = PR.Name 
	, Pumper = PU.FullName 
	, DriverNumber = D.IDNumber
	, CarrierNumber = CA.IDNumber
	, CarrierTypeID = CA.CarrierTypeID
	, TruckNumber = TRU.IDNumber
	, TruckType = TRU.TruckType 
	, TrailerNumber = TR1.IDNumber 
	, Trailer2Number = TR2.IDNumber 
	, Product = PRO.Name
	, ProductShort = PRO.ShortName 
	, PRO.ProductGroup
	, OriginUOM = OUom.Name 
	, OriginUomShort = OUom.Abbrev 
	, OriginTankID_Text = coalesce(ORT.TankNum, O.OriginTankNum, '?')
	, DestUOM = DUom.Name 
	, DestUomShort = DUom.Abbrev 
	, Active = CAST((CASE WHEN O.DeleteDateUTC IS NULL THEN 1 ELSE 0 END) AS BIT) 
	, IsDeleted = CAST((CASE WHEN O.DeleteDateUTC IS NOT NULL THEN 1 ELSE 0 END) AS BIT) 
	, PickupPrintStatus = PPS.Name 
	, PickupCompleted = PPS.IsCompleted 
	, DeliverPrintStatus = DPS.Name 
	, DeliverCompleted = DPS.IsCompleted 
	, PrintStatusID = CASE WHEN O.StatusID = -9 THEN ISNULL(-GAO.StatusID, O.StatusID)
						   WHEN O.StatusID = 8 AND PPS.IsCompleted = 0 THEN 7
						   WHEN O.StatusID = 3 AND DPS.IsCompleted = 0 THEN 8
						ELSE O.StatusID END 
	, OriginTankText = CASE WHEN O.OriginTankID IS NULL OR ORT.TankNum = '*' THEN O.OriginTankNum ELSE ORT.TankNum END
	, OriginWaitNum = OWR.Num
	, OriginWaitDesc = OWR.Description
	, OriginWaitNumDesc = OWR.NumDesc
	, DestWaitNum = DWR.Num
	, DestWaitDesc = DWR.Description
	, DestWaitNumDesc = DWR.NumDesc
	, RejectNum = ORR.Num
	, RejectDesc = ORR.Description
	, RejectNumDesc = ORR.NumDesc
	, TotalMinutes = ISNULL(OriginMinutes, 0) + ISNULL(DestMinutes, 0)
	-- TRANSFER FIELDS
	, OTR.*
	-- 
	, GaugerStatusID = GAO.StatusID
	, GaugerID = GAO.GaugerID	
	--
	, ContractNumber = CN.ContractNumber
	, ContractPrintDescription = CN.PrintDescription
	, ContractNotes = CN.Notes
	, ContractUomID = CN.UomID
	, ContractVolume = CN.Units
	FROM viewOrderBase O
	JOIN tblOrderStatus OS ON OS.ID = O.StatusID
	LEFT JOIN viewOrderTransferAll OTR ON OTR.OTAOrderID = O.ID
	LEFT JOIN viewOrigin vO ON vO.ID = O.OriginID
	LEFT JOIN viewDestination vD ON vD.ID = O.DestinationID
	LEFT JOIN tblCustomer C ON C.ID = O.CustomerID
	LEFT JOIN tblCarrier CA ON CA.ID = O.CarrierID
	LEFT JOIN tblCarrierType CT ON CT.ID = CA.CarrierTypeID
	LEFT JOIN tblConsignee CO ON CO.ID = O.ConsigneeID
	LEFT JOIN viewDriverBase D ON D.ID = O.DriverID
	LEFT JOIN viewDriver vDDR ON vDDR.ID = O.DriverID
	LEFT JOIN viewTruck TRU ON TRU.ID = O.TruckID
	LEFT JOIN viewTrailer TR1 ON TR1.ID = O.TrailerID
	LEFT JOIN viewTrailer TR2 ON TR2.ID = O.Trailer2ID
	LEFT JOIN tblPriority P ON P.ID = O.PriorityID
	LEFT JOIN tblTicketType TT ON TT.ID = O.TicketTypeID
	LEFT JOIN tblOriginTank ORT ON ORT.ID = O.OriginTankID
	LEFT JOIN tblOperator OP ON OP.ID = O.OperatorID
	LEFT JOIN viewPumper PU ON PU.ID = O.PumperID
	LEFT JOIN tblProducer PR ON PR.ID = O.ProducerID
	LEFT JOIN viewProduct PRO ON PRO.ID = O.ProductID
	LEFT JOIN tblUom OUom ON OUom.ID = O.OriginUomID
	LEFT JOIN tblUom DUom ON DUom.ID = O.DestUomID
	LEFT JOIN tblPrintStatus PPS ON PPS.ID = O.PickupPrintStatusID
	LEFT JOIN tblPrintStatus DPS ON DPS.ID = O.DeliverPrintStatusID
	LEFT JOIN viewOriginWaitReason OWR ON OWR.ID = O.OriginWaitReasonID
	LEFT JOIN viewDestinationWaitReason DWR ON DWR.ID = O.DestWaitReasonID
	LEFT JOIN viewOrderRejectReason ORR ON ORR.ID = O.RejectReasonID	
	LEFT JOIN tblGaugerOrder GAO ON GAO.OrderID = O.ID
	LEFT JOIN tblContract CN ON CN.ID = O.ContractID
) O
LEFT JOIN tblOrderStatus OPS ON OPS.ID = O.PrintStatusID
LEFT JOIN tblGaugerOrderStatus GOS ON GOS.ID = O.GaugerStatusID

GO


EXEC _spRefreshAllViews
EXEC _spRefreshAllViews
EXEC _spRefreshAllViews
EXEC _spRefreshAllViews
EXEC _spRefreshAllViews
EXEC _spRefreshAllViews
EXEC _spRefreshAllViews
EXEC _spRefreshAllViews
EXEC _spRebuildAllObjects
EXEC _spRebuildAllObjects
EXEC _spRecompileAllStoredProcedures
GO

COMMIT
SET NOEXEC OFF