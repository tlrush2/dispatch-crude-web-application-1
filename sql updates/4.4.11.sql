SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.4.10.1'
SELECT  @NewVersion = '4.4.11'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'JT-338 - Add personal conveyance report'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/********************************************
-- Date Created: 2017 Jan 03
-- Author: Joe Engler
-- Purpose: Get drivers day-by-day Personal Use summary for HOS
		-- CTE IS LIMITED TO 100 loops so max three months should be fine
********************************************/
CREATE FUNCTION fnHosDriverPersonalConveyanceSummary(@DriverID INT, @StartDate DATETIME, @EndDate DATETIME)
RETURNS TABLE AS
	RETURN 
	WITH dayloop AS
	(
		SELECT @StartDate AS [Date]
		UNION ALL
		SELECT DATEADD(DAY, 1, [Date]) 
		FROM dayloop
		WHERE DATEADD(DAY, 1, [Date]) < case when @EndDate < getutcdate() then @endDate else getutcdate() end
	)
		SELECT [Date] = CAST(d.Date AS DATE),
			DriverID,
			TotalHours = SUM(CASE WHEN h.PersonalUse =1 THEN TotalHours ELSE 0 END)
		FROM dayloop d
		OUTER APPLY fnHosSummary(@DriverID, [Date], DATEADD(DAY, 1, [Date])) hs
		LEFT JOIN tblHos h ON h.ID = hs.ID
		GROUP BY driverid, CAST(d.Date AS DATE)

GO


/********************************************
-- Date Created: 2017 Jan 03
-- Author: Joe Engler
-- Purpose: Get daily Personal Use overages for HOS
********************************************/
CREATE FUNCTION fnHosPersonalConveyance(@StartDate DATETIME, @EndDate DATETIME, @ShowAll BIT = 0)
RETURNS TABLE AS
	RETURN 
	SELECT * 
	FROM (
		SELECT 
			Date, DriverID = dp.ID, Driver = dp.FullName, CarrierID, Carrier,
			HOSPolicyID, TotalHours, PersonalDailyLimitHR,
			Overage = CAST(CASE WHEN PersonalDailyLimitHR IS NULL OR PersonalDailyLimitHR = 0 THEN 0 -- No limit
								WHEN TotalHours > PersonalDailyLimitHR THEN 1 
								ELSE 0 END AS BIT)
		FROM (
				-- Active drivers and their HOS policy
				SELECT 
					d.ID,
					d.FullName,
					d.CarrierID,
					d.Carrier,
					HOSPolicyID = hp.ID,
					PersonalDailyLimitHR = hp.PersonalDailyLimit / 60.0
				FROM viewDriverBase d
				OUTER APPLY dbo.fnCarrierRules(getutcdate(), null, 1, d.CarrierID, d.ID, d.OperatingStateID, d.RegionID, 1) cr --best match
				LEFT JOIN tblHosPolicy hp ON RTRIM(cr.Value) = RTRIM(hp.Name)
				WHERE d.DeleteDateUTC IS NULL
		) dp
		OUTER APPLY dbo.fnHosDriverPersonalConveyanceSummary(dp.ID, @startdate, @enddate) pc
	) q
	WHERE @ShowAll = 1 OR Overage = 1
GO


COMMIT
SET NOEXEC OFF
