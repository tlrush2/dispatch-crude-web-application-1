SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.4.6.4'
SELECT  @NewVersion = '4.4.6.5'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'DCWEB-1994 Add missing driver settlement fields to Report Center'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


-- Add Driver Settlement Volume to report center
SET IDENTITY_INSERT tblReportColumnDefinition ON

INSERT INTO tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport, IsTicketField)
  SELECT 352, 1, 
	'DriverSettlementUnits',
	'SETTLEMENT | DRIVER | Driver Settlement Volume', '#0.00', NULL, 4, NULL, 1, '*', 1, 0

SET IDENTITY_INSERT tblReportColumnDefinition OFF

GO


--Update assessorial total fields to check system setting and not id (new driver chainup would cause discrapancy)
UPDATE tblReportColumnDefinition
	SET DataField = '(SELECT sum(Amount) FROM tblOrderSettlementCarrierAssessorialCharge X WHERE X.OrderID=RS.ID AND X.AssessorialRateTypeID IN (SELECT ID FROM tblAssessorialRateType WHERE IsSystem = 0))'
	WHERE ID = 90007

UPDATE tblReportColumnDefinition
	SET DataField = '(SELECT sum(Amount) FROM tblOrderSettlementShipperAssessorialCharge X WHERE X.OrderID=RS.ID AND X.AssessorialRateTypeID IN (SELECT ID FROM tblAssessorialRateType WHERE IsSystem = 0))'
	WHERE ID = 90008

UPDATE tblReportColumnDefinition
	SET DataField = '(SELECT sum(Amount) FROM tblOrderSettlementDriverAssessorialCharge X WHERE X.OrderID=RS.ID AND X.AssessorialRateTypeID IN (SELECT ID FROM tblAssessorialRateType WHERE IsSystem = 0))'
	WHERE ID = 90055

GO


COMMIT
SET NOEXEC OFF
