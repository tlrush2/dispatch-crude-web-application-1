-- rollback
-- select * from tblsetting where id = 0 
/* 
	-- add new "Purge Deleted Orders Obsolete Days" setting value
	-- add new fnSettingValue scalar value function
	-- add support for deleting deleted orphaned Orders past the new Purge Deleted Orders Obsolete Days setting value
*/ 
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.1.14'
SELECT  @NewVersion = '3.1.15'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

INSERT INTO tblSetting (ID, Name, SettingTypeID, Value, Category, CreatedByUser, ReadOnly)
	SELECT 34, 'Purge Deleted Orders Obsolete Days', 3, 90, 'System Maintenance', 'System', 0
GO

CREATE FUNCTION fnSettingValue(@settingID int) RETURNS varchar(max) AS BEGIN
	DECLARE @ret varchar(max)
	SELECT @ret = Value FROM tblSetting WHERE ID = @settingID
	RETURN (@ret)
END

GO
GRANT EXECUTE ON fnSettingValue TO dispatchcrude_iis_acct
GO

/**********************************************************
-- Author: Kevin Alons
-- Date Created: 7 Oct 2014
-- Purpose: delete all unaudited, unpriced orders that have been "orphaned" and were deleted more than
		"Purge Deleted Orders Obsolete Days setting value" days
***********************************************************/		
CREATE PROCEDURE spMaint_PurgeObsoleteDeletedOrders AS
BEGIN
	-- get the list of orders to PURGE
	SELECT O.ID 
	INTO #id
	FROM tblOrder O
	LEFT JOIN tblOrderInvoiceCustomer IOC ON IOC.OrderID = O.ID
	WHERE DeleteDateUTC IS NOT NULL AND DeleteDateUTC < DATEADD(day, -cast(dbo.fnSettingValue(34) as int), GETUTCDATE())
	  -- never purge AUDITed records
	  AND StatusID NOT IN (4)
	  -- never purge records that have been PRICED (for a SHIPPER)
	  AND IOC.ID IS NULL
	  	
	-- set the orders to a STATUS of DECLINED
	UPDATE tblOrder
		SET StatusID = 9
	WHERE ID IN (SELECT ID FROM #id)

	-- remvoe any child records for the soon to be deleted records
	DELETE FROM tblOrderSignature WHERE OrderID IN (SELECT ID FROM #id)
	DELETE FROM tblDriverLocation WHERE OrderID IN (SELECT ID FROM #id)
	DELETE FROM tblOrderTicket WHERE OrderID IN (SELECT ID FROM #id)
	
	-- PURGE (delete) the actual orders now
	DELETE FROM tblOrder WHERE ID IN (SELECT ID FROM #id)
END

GO

COMMIT
SET NOEXEC OFF