-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.7.32.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.7.32.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0
SET NOEXEC OFF
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.7.32'
SELECT  @NewVersion = '3.7.33'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Settlement Center - fix deletion prevention logic related to recent changes with added Driver|Producer criteria'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/***********************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			ensure Locked records are not modified (except the EndDate)
			un-associate changed rates from rated orders
***********************************************/
ALTER TRIGGER trigCarrierAssessorialRate_IU ON tblCarrierAssessorialRate AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblCarrierAssessorialRate X 
			ON i.ID <> X.ID
			  AND i.TypeID = X.TypeID
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(i.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(i.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(i.ProducerID, X.ProducerID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Assessorial Rates are not allowed';
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewCarrierAssessorialRate X ON d.ID = X.ID AND X.Locked = 1
		WHERE d.TypeID <> X.TypeID
			OR dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
			OR dbo.fnCompareNullableInts(d.CarrierID, X.CarrierID) = 0
			OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
			OR dbo.fnCompareNullableInts(d.DriverGroupID, X.DriverGroupID) = 0
			OR dbo.fnCompareNullableInts(d.OriginID, X.OriginID) = 0
			OR dbo.fnCompareNullableInts(d.DestinationID, X.DestinationID) = 0
			OR dbo.fnCompareNullableInts(d.OriginStateID, X.OriginStateID) = 0
			OR dbo.fnCompareNullableInts(d.DestStateID, X.DestStateID) = 0
			OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
			OR dbo.fnCompareNullableInts(d.ProducerID, X.ProducerID) = 0
			OR d.EffectiveDate <> X.EffectiveDate
			OR d.Rate <> X.Rate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed';
	END
	
	IF (@error IS NOT NULL)
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		-- un-rate any Order Assessorial Rates associated with this assessorial rate record
		DELETE AC
		FROM tblOrderSettlementCarrierAssessorialCharge AC
		JOIN tblOrderSettlementCarrier SC ON SC. OrderID = AC.OrderID
		JOIN inserted i ON i.ID = AC.AssessorialRateID
		WHERE SC.BatchID IS NULL
END
GO

/***********************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			prevent changes to Locked (in use) records
			un-associate changed rates from rated orders
***********************************************/
ALTER TRIGGER trigCarrierDestinationWaitRate_IU ON tblCarrierDestinationWaitRate AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblCarrierDestinationWaitRate X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ReasonID, X.ReasonID) = 1
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(i.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(i.ProducerID, X.ProducerID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Wait Rates are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewCarrierDestinationWaitRate X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ReasonID, X.ReasonID) = 0
		  OR dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
		  OR dbo.fnCompareNullableInts(d.CarrierID, X.CarrierID) = 0
		  OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
		  OR dbo.fnCompareNullableInts(d.DriverGroupID, X.DriverGroupID) = 0
		  OR dbo.fnCompareNullableInts(d.DestinationID, X.DestinationID) = 0
		  OR dbo.fnCompareNullableInts(d.StateID, X.StateID) = 0
		  OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
		  OR dbo.fnCompareNullableInts(d.ProducerID, X.ProducerID) = 0
		  OR d.EffectiveDate <> X.EffectiveDate
		  OR d.Rate <> X.Rate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	IF @error IS NOT NULL 
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		UPDATE tblOrderSettlementCarrier SET DestinationWaitRateID = NULL, DestinationWaitAmount = NULL 
		WHERE BatchID IS NULL AND DestinationWaitRateID IN (SELECT ID FROM inserted)
END
GO

EXEC _spDropIndex 'udxCarrierFuelSurchargeRate_Main'
EXEC _spDropIndex 'idxCarrierFuelSurchargeRate_Driver'
GO
ALTER TABLE tblCarrierFuelSurchargeRate DROP COLUMN DriverID
GO
CREATE UNIQUE CLUSTERED INDEX udxCarrierFuelSurchargeRate_Main ON tblCarrierFuelSurchargeRate
(
	ShipperID ASC,
	CarrierID ASC,
	ProductGroupID ASC,
	DriverGroupID ASC,
	EffectiveDate ASC
)
GO
EXEC _spRefreshAllViews
GO

/***********************************************
-- Date Created: 20 Feb 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			prevent changes to Locked (in use) records
			un-associate changed rates from rated orders
***********************************************/
ALTER TRIGGER trigCarrierFuelSurchargeRate_IU ON tblCarrierFuelSurchargeRate AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblCarrierFuelSurchargeRate X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.DriverGroupID, X.DriverGroupID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Fuel Surcharge Rates are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewCarrierFuelSurchargeRate X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
		  OR dbo.fnCompareNullableInts(d.CarrierID, X.CarrierID) = 0
		  OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
		  OR dbo.fnCompareNullableInts(d.DriverGroupID, X.DriverGroupID) = 0
		  OR d.EffectiveDate <> X.EffectiveDate
		  OR d.FuelPriceFloor <> X.FuelPriceFloor
		  OR d.IncrementAmount <> X.IncrementAmount
		  OR d.IntervalAmount <> X.IntervalAmount
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	IF @error IS NOT NULL 
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		UPDATE tblOrderSettlementCarrier SET FuelSurchargeRateID = NULL, FuelSurchargeAmount = NULL 
		WHERE BatchID IS NULL AND FuelSurchargeRateID IN (SELECT ID FROM inserted)
END
GO

/***********************************************
-- Date Created: 20 Feb 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			prevent changes to Locked (in use) records
			un-associate changed rates from rated orders
-- Changes:
	-- 3.7.33 - 2015/06/19 - KDA - ADDED
***********************************************/
CREATE TRIGGER trigCarrierMinSettlementUnits_IU ON tblCarrierMinSettlementUnits AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblCarrierMinSettlementUnits X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(i.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(i.DestinationStateID, X.DestinationStateID) = 1
			  AND dbo.fnCompareNullableInts(i.ProducerID, X.ProducerID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Min Settlement Unit records are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewCarrierMinSettlementUnits  X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
		  OR dbo.fnCompareNullableInts(d.CarrierID, X.CarrierID) = 0
		  OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
		  OR dbo.fnCompareNullableInts(d.OriginID, X.OriginID) = 0
		  OR dbo.fnCompareNullableInts(d.OriginStateID, X.OriginStateID) = 0
		  OR dbo.fnCompareNullableInts(d.DestinationID, X.DestinationID) = 0
		  OR dbo.fnCompareNullableInts(d.DestinationStateID, X.DestinationStateID) = 0
		  OR dbo.fnCompareNullableInts(d.ProducerID, X.ProducerID) = 0
		  OR d.EffectiveDate <> X.EffectiveDate
		  OR d.MinSettlementUnits <> X.MinSettlementUnits
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	IF @error IS NOT NULL 
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		UPDATE tblOrderSettlementCarrier SET MinSettlementUnitsID = NULL, MinSettlementUnits = NULL 
		WHERE BatchID IS NULL AND MinSettlementUnitsID IN (SELECT ID FROM inserted)
END
GO

/***********************************************
-- Date Created: 2015/06/19
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			prevent changes to Locked (in use) records
			un-associate changed rates from rated orders
-- Changes:
	-- 3.7.33 - 2015/06/19 - KDA - ADDED
***********************************************/
CREATE TRIGGER trigCarrierSettlementFactor_IU ON tblCarrierSettlementFactor AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblCarrierSettlementFactor X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(i.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(i.DestinationStateID, X.DestinationStateID) = 1
			  AND dbo.fnCompareNullableInts(i.ProducerID, X.ProducerID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Settlements Unit Type records are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewCarrierSettlementFactor  X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
		  OR dbo.fnCompareNullableInts(d.CarrierID, X.CarrierID) = 0
		  OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
		  OR dbo.fnCompareNullableInts(d.OriginID, X.OriginID) = 0
		  OR dbo.fnCompareNullableInts(d.OriginStateID, X.OriginStateID) = 0
		  OR dbo.fnCompareNullableInts(d.DestinationID, X.DestinationID) = 0
		  OR dbo.fnCompareNullableInts(d.DestinationStateID, X.DestinationStateID) = 0
		  OR dbo.fnCompareNullableInts(d.ProducerID, X.ProducerID) = 0
		  OR d.EffectiveDate <> X.EffectiveDate
		  OR d.SettlementFactorID <> X.SettlementFactorID
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	IF @error IS NOT NULL 
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		UPDATE tblOrderSettlementCarrier SET CarrierSettlementFactorID = NULL, SettlementFactorID = NULL 
		WHERE BatchID IS NULL AND CarrierSettlementFactorID IN (SELECT ID FROM inserted)
END
GO

/***********************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			prevent changes to Locked (in use) records
-- Changes:
	- 3.7.33 - 2015/06/19 - KDA - add missing criteria references (DriverID | ProducerID)
***********************************************/
ALTER TRIGGER [trigCarrierOrderRejectRate_IU] ON [tblCarrierOrderRejectRate] AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblCarrierOrderRejectRate X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ReasonID, X.ReasonID) = 1
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(i.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(i.ProducerID, X.ProducerID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Reject Rates are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewCarrierOrderRejectRate X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ReasonID, X.ReasonID) = 0
		  OR dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
		  OR dbo.fnCompareNullableInts(d.CarrierID, X.CarrierID) = 0
		  OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
		  OR dbo.fnCompareNullableInts(d.DriverGroupID, X.DriverGroupID) = 0
		  OR dbo.fnCompareNullableInts(d.OriginID, X.OriginID) = 0
		  OR dbo.fnCompareNullableInts(d.StateID, X.StateID) = 0
		  OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
		  OR dbo.fnCompareNullableInts(d.ProducerID, X.ProducerID) = 0
		  OR d.EffectiveDate <> X.EffectiveDate
		  OR d.Rate <> X.Rate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	IF @error IS NOT NULL 
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		UPDATE tblOrderSettlementCarrier SET OrderRejectRateID = NULL, OrderRejectAmount = NULL 
		WHERE BatchID IS NULL AND OrderRejectRateID IN (SELECT ID FROM inserted)
END
GO

/***********************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			prevent changes to Locked (in use) records
***********************************************/
ALTER TRIGGER [trigCarrierOriginWaitRate_IU] ON [tblCarrierOriginWaitRate] AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblCarrierOriginWaitRate X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ReasonID, X.ReasonID) = 1
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(i.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Wait Rates are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewCarrierOriginWaitRate X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ReasonID, X.ReasonID) = 0
		  OR dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
		  OR dbo.fnCompareNullableInts(d.CarrierID, X.CarrierID) = 0
		  OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
		  OR dbo.fnCompareNullableInts(d.OriginID, X.OriginID) = 0
		  OR dbo.fnCompareNullableInts(d.StateID, X.StateID) = 0
		  OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
		  OR d.EffectiveDate <> X.EffectiveDate
		  OR d.Rate <> X.Rate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	IF @error IS NOT NULL 
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		UPDATE tblOrderSettlementCarrier SET OriginWaitRateID = NULL, OriginWaitAmount = NULL 
		WHERE BatchID IS NULL AND OriginWaitRateID IN (SELECT ID FROM inserted)
END
GO

/***********************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
/***********************************************/
ALTER TRIGGER [trigCarrierRateSheet_IU] ON [tblCarrierRateSheet] AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblCarrierRateSheet X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.DriverGroupID, X.DriverGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(i.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(i.ProducerID, X.ProducerID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Rate Sheets are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewCarrierRateSheet X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
			OR dbo.fnCompareNullableInts(d.CarrierID, X.CarrierID) = 0
			OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
			OR dbo.fnCompareNullableInts(d.DriverGroupID, X.DriverGroupID) = 0
			OR dbo.fnCompareNullableInts(d.OriginStateID, X.OriginStateID) = 0
			OR dbo.fnCompareNullableInts(d.DestStateID, X.DestStateID) = 0
			OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
			OR dbo.fnCompareNullableInts(d.ProducerID, X.ProducerID) = 0
			OR d.EffectiveDate <> X.EffectiveDate 
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	
	IF (@error IS NOT NULL)
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
END
GO

/***********************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
/***********************************************/
ALTER TRIGGER [trigCarrierRouteRate_IU] ON [tblCarrierRouteRate] AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblCarrierRouteRate X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.DriverGroupID, X.DriverGroupID) = 1
			  AND i.RouteID = X.RouteID
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Route Rates are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewCarrierRouteRate X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
			OR dbo.fnCompareNullableInts(d.CarrierID, X.CarrierID) = 0
			OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
			OR dbo.fnCompareNullableInts(d.DriverGroupID, X.DriverGroupID) = 0
			OR d.RouteID <> X.RouteID
			OR d.EffectiveDate <> X.EffectiveDate
			OR d.Rate <> X.Rate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	IF (@error IS NOT NULL)
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		UPDATE tblOrderSettlementCarrier SET RouteRateID = NULL, RangeRateID = NULL, LoadAmount = NULL 
		WHERE BatchID IS NULL AND RouteRateID IN (SELECT ID FROM inserted)
END
GO

/***********************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			prevent changes to Locked records (except for EndDate)
***********************************************/
ALTER TRIGGER [trigCarrierWaitFeeParameter_IU] ON [tblCarrierWaitFeeParameter] AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblCarrierWaitFeeParameter X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.CarrierID, X.CarrierID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(i.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(i.ProducerID, X.ProducerID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Parameters are not allowed'
	END
	ElSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewCarrierWaitFeeParameter X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
			OR dbo.fnCompareNullableInts(d.CarrierID, X.CarrierID) = 0
			OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
			OR dbo.fnCompareNullableInts(d.OriginStateID, X.OriginStateID) = 0
			OR dbo.fnCompareNullableInts(d.DestStateID, X.DestStateID) = 0
			OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
			OR dbo.fnCompareNullableInts(d.ProducerID, X.ProducerID) = 0
			OR d.EffectiveDate <> X.EffectiveDate
			OR d.OriginThresholdMinutes <> X.OriginThresholdMinutes
			OR d.DestThresholdMinutes <> X.DestThresholdMinutes
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	
	IF (@error IS NOT NULL)
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		UPDATE tblOrderSettlementCarrier SET WaitFeeParameterID = NULL
		WHERE BatchID IS NULL AND WaitFeeParameterID IN (SELECT ID FROM inserted)
END
GO

-------------------------------------

/***********************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			ensure Locked records are not modified (except the EndDate)
***********************************************/
ALTER TRIGGER trigShipperAssessorialRate_IU ON tblShipperAssessorialRate AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblShipperAssessorialRate X 
			ON i.ID <> X.ID
			  AND i.TypeID = X.TypeID
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(i.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(i.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(i.ProducerID, X.ProducerID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Assessorial Rates are not allowed';
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewShipperAssessorialRate X ON d.ID = X.ID AND X.Locked = 1
		WHERE d.TypeID <> X.TypeID
			OR dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
			OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
			OR dbo.fnCompareNullableInts(d.OriginID, X.OriginID) = 0
			OR dbo.fnCompareNullableInts(d.DestinationID, X.DestinationID) = 0
			OR dbo.fnCompareNullableInts(d.OriginStateID, X.OriginStateID) = 0
			OR dbo.fnCompareNullableInts(d.DestStateID, X.DestStateID) = 0
			OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
			OR dbo.fnCompareNullableInts(d.ProducerID, X.ProducerID) = 0
			OR d.EffectiveDate <> X.EffectiveDate
			OR d.Rate <> X.Rate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed';
	END
	
	IF (@error IS NOT NULL)
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		-- un-rate any Order Assessorial Rates associated with this assessorial rate record
		DELETE AC
		FROM tblOrderSettlementShipperAssessorialCharge AC
		JOIN tblOrderSettlementShipper SS ON SS. OrderID = AC.OrderID
		JOIN inserted i ON i.ID = AC.AssessorialRateID
		WHERE SS.BatchID IS NULL
END
GO

/***********************************************
-- Date Created: 20 Feb 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			prevent changes to Locked (in use) records
			un-associate changed rates from rated orders
-- Changes:
	-- 3.7.33 - 2015/06/19 - KDA - ADDED
***********************************************/
CREATE TRIGGER trigShipperMinSettlementUnits_IU ON tblShipperMinSettlementUnits AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblShipperMinSettlementUnits X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(i.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(i.DestinationStateID, X.DestinationStateID) = 1
			  AND dbo.fnCompareNullableInts(i.ProducerID, X.ProducerID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Min Settlement Unit records are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewShipperMinSettlementUnits  X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
		  OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
		  OR dbo.fnCompareNullableInts(d.OriginID, X.OriginID) = 0
		  OR dbo.fnCompareNullableInts(d.OriginStateID, X.OriginStateID) = 0
		  OR dbo.fnCompareNullableInts(d.DestinationID, X.DestinationID) = 0
		  OR dbo.fnCompareNullableInts(d.DestinationStateID, X.DestinationStateID) = 0
		  OR dbo.fnCompareNullableInts(d.ProducerID, X.ProducerID) = 0
		  OR d.EffectiveDate <> X.EffectiveDate
		  OR d.MinSettlementUnits <> X.MinSettlementUnits
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	IF @error IS NOT NULL 
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		UPDATE tblOrderSettlementShipper SET MinSettlementUnitsID = NULL, MinSettlementUnits = NULL 
		WHERE BatchID IS NULL AND MinSettlementUnitsID IN (SELECT ID FROM inserted)
END
GO

/**********************************************
-- Date Created: 2015/06/19
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			prevent changes to Locked (in use) records
			un-associate changed rates from rated orders
-- Changes:
	-- 3.7.33 - 2015/06/19 - KDA - ADDED
***********************************************/
CREATE TRIGGER trigShipperSettlementFactor_IU ON tblShipperSettlementFactor AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblShipperSettlementFactor X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(i.DestinationID, X.DestinationID) = 1
			  AND dbo.fnCompareNullableInts(i.DestinationStateID, X.DestinationStateID) = 1
			  AND dbo.fnCompareNullableInts(i.ProducerID, X.ProducerID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Settlements Unit Type records are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewShipperSettlementFactor  X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
		  OR dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
		  OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
		  OR dbo.fnCompareNullableInts(d.OriginID, X.OriginID) = 0
		  OR dbo.fnCompareNullableInts(d.OriginStateID, X.OriginStateID) = 0
		  OR dbo.fnCompareNullableInts(d.DestinationID, X.DestinationID) = 0
		  OR dbo.fnCompareNullableInts(d.DestinationStateID, X.DestinationStateID) = 0
		  OR dbo.fnCompareNullableInts(d.ProducerID, X.ProducerID) = 0
		  OR d.EffectiveDate <> X.EffectiveDate
		  OR d.SettlementFactorID <> X.SettlementFactorID
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	IF @error IS NOT NULL 
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		UPDATE tblOrderSettlementShipper SET ShipperSettlementFactorID = NULL, SettlementFactorID = NULL 
		WHERE BatchID IS NULL AND ShipperSettlementFactorID IN (SELECT ID FROM inserted)
END
GO

/***********************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			prevent changes to Locked (in use) records
-- Changes:
	- 3.7.33 - 2015/06/19 - KDA - add missing criteria references (DriverID | ProducerID)
***********************************************/
ALTER TRIGGER [trigShipperOrderRejectRate_IU] ON [tblShipperOrderRejectRate] AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblShipperOrderRejectRate X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ReasonID, X.ReasonID) = 1
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginID, X.OriginID) = 1
			  AND dbo.fnCompareNullableInts(i.StateID, X.StateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(i.ProducerID, X.ProducerID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Reject Rates are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewShipperOrderRejectRate X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ReasonID, X.ReasonID) = 0
		  OR dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
		  OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
		  OR dbo.fnCompareNullableInts(d.OriginID, X.OriginID) = 0
		  OR dbo.fnCompareNullableInts(d.StateID, X.StateID) = 0
		  OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
		  OR dbo.fnCompareNullableInts(d.ProducerID, X.ProducerID) = 0
		  OR d.EffectiveDate <> X.EffectiveDate
		  OR d.Rate <> X.Rate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	IF @error IS NOT NULL 
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		UPDATE tblOrderSettlementShipper SET OrderRejectRateID = NULL, OrderRejectAmount = NULL 
		WHERE BatchID IS NULL AND OrderRejectRateID IN (SELECT ID FROM inserted)
END
GO

/***********************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
/***********************************************/
ALTER TRIGGER [trigShipperRateSheet_IU] ON [tblShipperRateSheet] AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblShipperRateSheet X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(i.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(i.ProducerID, X.ProducerID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Rate Sheets are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewShipperRateSheet X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
			OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
			OR dbo.fnCompareNullableInts(d.OriginStateID, X.OriginStateID) = 0
			OR dbo.fnCompareNullableInts(d.DestStateID, X.DestStateID) = 0
			OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
			OR dbo.fnCompareNullableInts(d.ProducerID, X.ProducerID) = 0
			OR d.EffectiveDate <> X.EffectiveDate 
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	
	IF (@error IS NOT NULL)
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
END
GO

/***********************************************/
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
/***********************************************/
ALTER TRIGGER [trigShipperRouteRate_IU] ON [tblShipperRouteRate] AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblShipperRouteRate X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND i.RouteID = X.RouteID
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Route Rates are not allowed'
	END
	ELSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewShipperRouteRate X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
			OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
			OR d.RouteID <> X.RouteID
			OR d.EffectiveDate <> X.EffectiveDate
			OR d.Rate <> X.Rate
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	IF (@error IS NOT NULL)
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		UPDATE tblOrderSettlementShipper SET RouteRateID = NULL, RangeRateID = NULL, LoadAmount = NULL 
		WHERE BatchID IS NULL AND RouteRateID IN (SELECT ID FROM inserted)
END
GO

/***********************************************
-- Date Created: 3 Jan 2015
-- Author: Kevin Alons
-- Purpose: prevent overlapping records (based on EffectiveDate | EndDate)
			prevent changes to Locked records (except for EndDate)
***********************************************/
ALTER TRIGGER [trigShipperWaitFeeParameter_IU] ON [tblShipperWaitFeeParameter] AFTER INSERT, UPDATE AS
BEGIN
	DECLARE @error varchar(255)
	
	IF EXISTS (
		SELECT i.* 
		FROM inserted i 
		JOIN tblShipperWaitFeeParameter X 
			ON i.ID <> X.ID
			  AND dbo.fnCompareNullableInts(i.ShipperID, X.ShipperID) = 1
			  AND dbo.fnCompareNullableInts(i.ProductGroupID, X.ProductGroupID) = 1
			  AND dbo.fnCompareNullableInts(i.OriginStateID, X.OriginStateID) = 1
			  AND dbo.fnCompareNullableInts(i.DestStateID, X.DestStateID) = 1
			  AND dbo.fnCompareNullableInts(i.RegionID, X.RegionID) = 1
			  AND dbo.fnCompareNullableInts(i.ProducerID, X.ProducerID) = 1
		WHERE i.EffectiveDate BETWEEN X.EffectiveDate AND X.EndDate 
			OR i.EndDate BETWEEN X.EffectiveDate AND X.EndDate
			OR X.EffectiveDate BETWEEN i.EffectiveDate AND i.EndDate
	)
	BEGIN
		SET @error = 'Overlapping Parameters are not allowed'
	END
	ElSE IF EXISTS (
		SELECT d.* 
		FROM deleted d
		JOIN viewShipperWaitFeeParameter X ON d.ID = X.ID AND X.Locked = 1
		WHERE dbo.fnCompareNullableInts(d.ShipperID, X.ShipperID) = 0
			OR dbo.fnCompareNullableInts(d.ProductGroupID, X.ProductGroupID) = 0
			OR dbo.fnCompareNullableInts(d.OriginStateID, X.OriginStateID) = 0
			OR dbo.fnCompareNullableInts(d.DestStateID, X.DestStateID) = 0
			OR dbo.fnCompareNullableInts(d.RegionID, X.RegionID) = 0
			OR dbo.fnCompareNullableInts(d.ProducerID, X.ProducerID) = 0
			OR d.EffectiveDate <> X.EffectiveDate
			OR d.OriginThresholdMinutes <> X.OriginThresholdMinutes
			OR d.DestThresholdMinutes <> X.DestThresholdMinutes
	)
	BEGIN
		SET @error = 'Changes to Locked records are not allowed'
	END
	
	IF (@error IS NOT NULL)
	BEGIN
		RAISERROR(@error, 16, 1)
		ROLLBACK
	END
	ELSE
		UPDATE tblOrderSettlementShipper SET WaitFeeParameterID = NULL
		WHERE BatchID IS NULL AND WaitFeeParameterID IN (SELECT ID FROM inserted)
END
GO

/***********************************
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier FuelSurcharge info for the specified order
-- Changes:
	-- 3.7.28 - 2015/06/18 - KDA - add usage of DriverID/DriverGroupID parameters
***********************************/
ALTER FUNCTION [fnOrderCarrierFuelSurchargeRate](@ID int) RETURNS TABLE AS RETURN
	SELECT RateID
		, Rate = IncrementAmount * round(Pricediff / nullif(IntervalAmount, 0.0) + .49, 0)
		, RouteMiles
	FROM (
		SELECT TOP 1 RateID = ID, FuelPriceFloor, IntervalAmount, IncrementAmount
			, PriceDiff = dbo.fnMaxDecimal(0, dbo.fnFuelPriceForOrigin(OriginID, OrderDate) - FuelPriceFloor)
			, RouteMiles
		FROM (
			SELECT R.ID, R.FuelPriceFloor, R.IntervalAmount, R.IncrementAmount, O.OrderDate, O.OriginID, RouteMiles = CASE WHEN O.Rejected = 1 THEN 0 ELSE O.ActualMiles END
			FROM dbo.viewOrderBase O
			CROSS APPLY dbo.fnCarrierFuelSurchargeRate(O.OrderDate, null, O.CustomerID, O.CarrierID, O.ProductGroupID, O.DriverGroupID, 1) R
			WHERE O.ID = @ID 
		) X
	) X2
GO

EXEC _spRebuildAllObjects
GO

COMMIT
SET NOEXEC OFF