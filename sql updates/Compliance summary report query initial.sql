SELECT CR.ID, CR.RegionID, CR.CarrierID, CR.ChainupFee, CR.WaitFee, CR.RejectionFee, CR.RerouteFee 
FROM tblCarrierRates CR 
LEFT JOIN tblCarrier C ON C.ID = CR.CarrierID 
LEFT JOIN tblRegion R ON R.ID = CR.RegionID 
ORDER BY isnull(C.Name,'*'), isnull(R.Name,'*')

SELECT *, CASE WHEN Status = 'Missing' THEN Rate ELSE NULL END AS NewRate
, CASE WHEN Status = 'Missing' THEN EffectiveDate ELSE NULL END AS NewEffectiveDate
, NULL AS ImportOutcome 
FROM dbo.viewCarrierRouteRates 
WHERE (@CarrierID = 0 OR CarrierID = @CarrierID) 
 AND (@OriginID = 0 OR OriginID = @OriginID) 
 AND (@DestinationID = 0 OR DestinationID = @DestinationID) 
 AND (EndDate IS NULL OR EndDate >= @StartDate) 
 AND (Status NOT LIKE 'Active' OR @IncludeActive=1) 
 AND (Status NOT LIKE 'Missing' OR @IncludeMissing=1) 
 AND (Status NOT LIKE 'Route Inactive' OR @IncludeRouteInactive=1) 
 AND (Status NOT LIKE 'Carrier Deleted' OR @IncludeCarrierDeleted=1) 
ORDER BY Carrier, Origin, Destination, EffectiveDate DESC

select * from tblorderstatus
select * from tblorder where ordernum = '107378'
update tblOrder set StatusID = 8 where OrderNum = '107378'