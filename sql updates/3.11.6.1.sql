-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.11.6'
SELECT  @NewVersion = '3.11.6.1'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'make tblObjectField.FieldName be not-required (for IsCustom, but still unique per object when provided)'
	UNION
	SELECT @NewVersion, 0, 'add trigObjectField_IU_UniqueNonNullFieldName (to conditinonally enforce FieldName be unique if provided)'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

ALTER TABLE dbo.tblObjectField
	DROP CONSTRAINT FK_ObjectField_AllowNull
GO
ALTER TABLE dbo.tblObjectFieldAllowNull SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblObjectField
	DROP CONSTRAINT FK_ObjectField_ObjectFieldType
GO
ALTER TABLE dbo.tblObjectFieldType SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblObjectField
	DROP CONSTRAINT FK_ObjectField_Object
GO
ALTER TABLE dbo.tblObjectField
	DROP CONSTRAINT FK_ObjectField_ParentObject
GO
ALTER TABLE dbo.tblObject SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblObjectField
	DROP CONSTRAINT DF_ObjectField_ReadOnly
GO
ALTER TABLE dbo.tblObjectField
	DROP CONSTRAINT DF_ObjectField_AllowNull
GO
ALTER TABLE dbo.tblObjectField
	DROP CONSTRAINT DF_ObjectField_IsKey
GO
ALTER TABLE dbo.tblObjectField
	DROP CONSTRAINT DF_ObjectField_IsCustom
GO
ALTER TABLE dbo.tblObjectField
	DROP CONSTRAINT DF_ObjectField_ParentObjectIDFieldName
GO
ALTER TABLE dbo.tblObjectField
	DROP CONSTRAINT DF_ObjectField_CreateDateUTC
GO
ALTER TABLE dbo.tblObjectField
	DROP CONSTRAINT DF_ObjectField_CreatedByUser
GO
CREATE TABLE dbo.Tmp_tblObjectField
	(
	ID int NOT NULL IDENTITY (1, 1),
	ObjectID int NOT NULL,
	FieldName varchar(50) NULL,
	Name varchar(50) NOT NULL,
	ObjectFieldTypeID smallint NOT NULL,
	DefaultValue varchar(MAX) NULL,
	ReadOnly bit NOT NULL,
	AllowNullID int NOT NULL,
	IsKey bit NOT NULL,
	IsCustom bit NOT NULL,
	ParentObjectID int NULL,
	ParentObjectIDFieldName varchar(50) NULL,
	CreateDateUTC datetime NOT NULL,
	CreatedByUser varchar(100) NOT NULL,
	LastChangeDateUTC datetime NULL,
	LastChangedByUser varchar(100) NULL,
	DeleteDateUTC datetime NULL,
	DeletedByUser varchar(100) NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_tblObjectField SET (LOCK_ESCALATION = TABLE)
GO
GRANT DELETE ON dbo.Tmp_tblObjectField TO role_iis_acct  AS dbo
GO
GRANT INSERT ON dbo.Tmp_tblObjectField TO role_iis_acct  AS dbo
GO
GRANT SELECT ON dbo.Tmp_tblObjectField TO role_iis_acct  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_tblObjectField TO role_iis_acct  AS dbo
GO
ALTER TABLE dbo.Tmp_tblObjectField ADD CONSTRAINT
	DF_ObjectField_ReadOnly DEFAULT ((0)) FOR ReadOnly
GO
ALTER TABLE dbo.Tmp_tblObjectField ADD CONSTRAINT
	DF_ObjectField_AllowNull DEFAULT ((1)) FOR AllowNullID
GO
ALTER TABLE dbo.Tmp_tblObjectField ADD CONSTRAINT
	DF_ObjectField_IsKey DEFAULT ((0)) FOR IsKey
GO
ALTER TABLE dbo.Tmp_tblObjectField ADD CONSTRAINT
	DF_ObjectField_IsCustom DEFAULT ((1)) FOR IsCustom
GO
ALTER TABLE dbo.Tmp_tblObjectField ADD CONSTRAINT
	DF_ObjectField_ParentObjectIDFieldName DEFAULT ('ID') FOR ParentObjectIDFieldName
GO
ALTER TABLE dbo.Tmp_tblObjectField ADD CONSTRAINT
	DF_ObjectField_CreateDateUTC DEFAULT (getutcdate()) FOR CreateDateUTC
GO
ALTER TABLE dbo.Tmp_tblObjectField ADD CONSTRAINT
	DF_ObjectField_CreatedByUser DEFAULT ('System') FOR CreatedByUser
GO
SET IDENTITY_INSERT dbo.Tmp_tblObjectField ON
GO
IF EXISTS(SELECT * FROM dbo.tblObjectField)
	 EXEC('INSERT INTO dbo.Tmp_tblObjectField (ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, ReadOnly, AllowNullID, IsKey, IsCustom, ParentObjectID, ParentObjectIDFieldName, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser)
		SELECT ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, ReadOnly, AllowNullID, IsKey, IsCustom, ParentObjectID, ParentObjectIDFieldName, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser FROM dbo.tblObjectField WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_tblObjectField OFF
GO   
IF EXISTS (select * from sys.foreign_keys where name like 'FK_ImportCenterField_ObjectField')
	ALTER TABLE tblImportCenterFieldDefinition DROP CONSTRAINT FK_ImportCenterField_ObjectField
ELSE
	ALTER TABLE tblImportCenterFieldDefinition DROP CONSTRAINT FK_ImportCenterField_Object
GO
ALTER TABLE dbo.tblObjectCustomData
	DROP CONSTRAINT FK_ObjectCustomData_ObjectField
GO
DROP TABLE dbo.tblObjectField
GO
EXECUTE sp_rename N'dbo.Tmp_tblObjectField', N'tblObjectField', 'OBJECT' 
GO
ALTER TABLE dbo.tblObjectField ADD CONSTRAINT
	PK_ObjectField PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE INDEX idxObjectField_Main ON dbo.tblObjectField
	(
	ObjectID,
	FieldName
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.tblObjectField WITH NOCHECK ADD CONSTRAINT
	CK_ObjectField_FieldNameRequired CHECK (([IsCustom]=(1) OR [FieldName] IS NOT NULL))
GO
/******************************************************
Creation Info: 2016/03/18 - x.x.x
Author: Kevin Alons
Purpose: ensure that any non-null FieldName values are unique per object
******************************************************/
CREATE TRIGGER trigObjectField_IU_UniqueNonNullFieldName ON tblObjectField FOR INSERT, UPDATE AS
BEGIN
	IF EXISTS (SELECT ObjectID FROM tblObjectField WHERE FieldName IS NOT NULL GROUP BY ObjectID, FieldName HAVING count(*) > 1)
	BEGIN
		RAISERROR('non-null FieldName values must be unique for each Object', 16, 1)
		ROLLBACK
	END
END
GO

CREATE UNIQUE NONCLUSTERED INDEX udxObjectField_Name ON dbo.tblObjectField
	(
	ObjectID,
	Name
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.tblObjectField WITH NOCHECK ADD CONSTRAINT
	CK_ObjectField_Custom_NoParent CHECK (([IsCustom]=(0) OR [ParentObjectID] IS NULL))
GO
ALTER TABLE dbo.tblObjectField ADD CONSTRAINT
	FK_ObjectField_Object FOREIGN KEY
	(
	ObjectID
	) REFERENCES dbo.tblObject
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblObjectField ADD CONSTRAINT
	FK_ObjectField_ObjectFieldType FOREIGN KEY
	(
	ObjectFieldTypeID
	) REFERENCES dbo.tblObjectFieldType
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblObjectField ADD CONSTRAINT
	FK_ObjectField_AllowNull FOREIGN KEY
	(
	AllowNullID
	) REFERENCES dbo.tblObjectFieldAllowNull
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblObjectField ADD CONSTRAINT
	FK_ObjectField_ParentObject FOREIGN KEY
	(
	ParentObjectID
	) REFERENCES dbo.tblObject
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblImportCenterFieldDefinition ADD CONSTRAINT
	FK_ImportCenterField_ObjectField FOREIGN KEY
	(
	ObjectFieldID
	) REFERENCES dbo.tblObjectField
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblImportCenterFieldDefinition SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.tblObjectCustomData ADD CONSTRAINT
	FK_ObjectCustomData_ObjectField FOREIGN KEY
	(
		ObjectFieldID
	) REFERENCES dbo.tblObjectField
	(
		ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblObjectCustomData SET (LOCK_ESCALATION = TABLE)
GO

COMMIT
SET NOEXEC OFF