SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.4.8.1'
SELECT  @NewVersion = '4.4.9'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'DCWEB-1551 - Split Import Center into Designer & Import modes + save sample document with each defined Import Mapping'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

create table tblImportCenterInputFile
(
  ImportCenterDefinitionID int not null constraint PK_ImportCenterInputFile primary key 
	constraint FK_ImportCenterInputFile_ImportCenterDefinition foreign key references tblImportCenterDefinition(ID) on delete cascade 
, OriginalFileName varchar(255) not null
, InputDataTableXml xml not null
, FieldNameCSV varchar(max) not null
, CreateDateUTC datetime not null constraint DF_ImportCenterInputFile_CreateDateUTC default (GETUTCDATE())
, CreatedByUser varchar(100) not null constraint DF_ImportCenterInputFile_CreatedByUser default ('System')
)
go

/*****************************************************
Created: 3.10.1 - 2015.12.15 - Kevin Alons
Purpose: clone an existing ImportCenterDefinition with a new name
Changes:
- 3.10.12	- 2016.02.06 - KDA	- remove IsKey & DoUpdate tblImportCenterFieldDefinition fields from processing
- 4.4.7		- 2016.12.01 - KDA	- add/honor @UserNames parameter
- 4.4.9		- 2016.12.15 - KDA	- also clone InputFile
*****************************************************/
ALTER PROCEDURE spCloneImportCenterDefinition
(
  @importCenterDefinitionID int
, @Name varchar(50)
, @UserNames varchar(max)
, @UserName varchar(100)
, @NewID int = NULL OUTPUT
) AS
BEGIN
	BEGIN TRAN CICD

	-- copy the basic ImportCenterDefinition first
	INSERT INTO tblImportCenterDefinition (RootObjectID, Name, UserNames, CreatedByUser)
		SELECT RootObjectID, @Name, @UserNames, @UserName FROM tblImportCenterDefinition WHERE ID = @importCenterDefinitionID
	SET @NewID = SCOPE_IDENTITY()

	-- add the root ImportFieldDefinitions (with no ParentFieldID)
	INSERT INTO tblImportCenterFieldDefinition (ImportCenterDefinitionID, ObjectFieldID, CreatedByUser)
		SELECT @NewID, ObjectFieldID, CreatedByUser
		FROM tblImportCenterFieldDefinition
		WHERE ImportCenterDefinitionID = @importCenterDefinitionID
		  AND ParentFieldID IS NULL
	-- also clone the InputFile record if defined
	INSERT INTO tblImportCenterInputFile (ImportCenterDefinitionID, OriginalFileName, FieldNameCSV, InputDataTableXml, CreatedByUser)
		SELECT @NewID, OriginalFileName, FieldNameCSV, InputDataTableXml, @UserName 
		FROM tblImportCenterInputFile 
		WHERE ImportCenterDefinitionID = @importCenterDefinitionID

	DECLARE @Level int; SET @Level = 0

	-- create a table to store the old-new ImportField mappings (will be used later to add the ImportFieldDefinition_Fields)
	DECLARE @Map TABLE (OID int, NID int, Level int)
	-- populate with the just added root ImportFieldDefinitions
	INSERT INTO @Map 
		SELECT OICFD.ID, NICFD.ID, @Level
		FROM tblImportCenterFieldDefinition OICFD
		JOIN tblImportCenterFieldDefinition NICFD ON NICFD.ImportCenterDefinitionID = @NewID AND NICFD.ObjectFieldID = OICFD.ObjectFieldID
		WHERE OICFD.ImportCenterDefinitionID = @importCenterDefinitionID AND OICFD.ParentFieldID IS NULL

	--DECLARE @debugMap XML = (SELECT * FROM @Map FOR XML AUTO)

	-- create table to remember newly added child FieldDefinition records
	DECLARE @NID IDTABLE
	-- populate with the first child FieldDefinitions to be added
	INSERT INTO @NID 
		SELECT ID FROM tblImportCenterFieldDefinition X JOIN @Map M ON M.OID = X.ParentFieldID WHERE ImportCenterDefinitionID = @importCenterDefinitionID EXCEPT SELECT OID FROM @Map

	--DECLARE @debugNID XML = (SELECT * FROM @NID FOR XML AUTO)

	WHILE EXISTS (SELECT ID FROM @NID)
	BEGIN
		-- add the next level of children FieldDefinitions
		INSERT INTO tblImportCenterFieldDefinition (ImportCenterDefinitionID, ObjectFieldID, ParentFieldID, CSharpExpression, KeyComparisonTypeID, CreatedByUser)
			SELECT @NewID, ObjectFieldID, M.NID, CSharpExpression, KeyComparisonTypeID, CreatedByUser
			FROM tblImportCenterFieldDefinition ICFD
			JOIN @NID NID ON NID.ID = ICFD.ID
			JOIN @Map M ON M.OID = ICFD.ParentFieldID

		-- record the old-new mappings for these new FieldDefinitions
		INSERT INTO @Map 
			SELECT OICFD.ID, NICFD.ID, @Level
			FROM tblImportCenterFieldDefinition OICFD
			JOIN tblImportCenterFieldDefinition NICFD ON NICFD.ImportCenterDefinitionID = @NewID AND NICFD.ObjectFieldID = OICFD.ObjectFieldID
			WHERE OICFD.ID IN (SELECT ID FROM @NID)

		--SELECT @debugMap = (SELECT * FROM @Map FOR XML AUTO)

		-- see if any next level new FieldMappings need to be processed
		DELETE FROM @NID
		INSERT INTO @NID 
			SELECT ID FROM tblImportCenterFieldDefinition X JOIN @Map M ON M.OID = X.ParentFieldID WHERE ImportCenterDefinitionID = @importCenterDefinitionID EXCEPT SELECT OID FROM @Map

		--SELECT @debugNID = (SELECT * FROM @NID FOR XML AUTO)
		
		-- increment @Level
		SET @Level = @Level + 1
	END

	-- add all the FieldDefinitionFields based on the accumulated @Map records
	INSERT INTO tblImportCenterFieldDefinitionField (ImportCenterFieldID, ImportFieldName, Position)
		SELECT M.NID, DF.ImportFieldName, DF.Position
		FROM tblImportCenterFieldDefinitionField DF
		JOIN @Map M ON M.OID = DF.ImportCenterFieldID

	COMMIT TRAN CICD
END

GO

/********************************************************/
-- Created: 4.0.0 - 2016.06.09 - Ben Bloodworth
-- Purpose: This procedure was created to make adding permissions easier in the SQL update files now and going forward.
-- Changes: 
-- 4.0.0	- 2016.06.09 - BB & JAE - Added code to also add the new permission to the admin group and admin users
--									NOTE: That code was going to be a trigger before it was moved here instead to consolidate things
-- ?.?.?	- 2016.08.03 - BB	- Added _DCSystemManager and _DCSupport groups to the recipients list per Maverick
--									NOTE: All "internal" groups should receive new permissions initially then admins can remove them where needed
-- 4.4.9	- 2016.12.17 - KDA	- simplify & improve syntax (use Transaction, use set-based logic instead of variables/multiple statements, etc)
/********************************************************/
ALTER PROCEDURE spAddNewPermission (@NAME VARCHAR(256), @DESCRIPTION VARCHAR(256), @CATEGORY VARCHAR(100), @FRIENDLYNAME VARCHAR(100)) AS
BEGIN
	DECLARE @applicationId uniqueidentifier = '446467D6-39CD-45E3-B3E0-CAC7945AF3E8'
	-- generate a new RoleID (if not already present)
	DECLARE @roleId uniqueidentifier = (SELECT NEWID() WHERE NOT EXISTS (SELECT 1 FROM aspnet_roles WHERE Rolename = @NAME))

	-- validation
	IF @roleId IS NULL BEGIN
		RAISERROR('Role "%s" already used, cannot be created', 16, 1, @NAME)
		RETURN
	END

	BEGIN TRAN AddNewPermission

		-- Insert the basic permission (role)
		INSERT INTO aspnet_Roles (ApplicationId, RoleId, RoleName, LoweredRoleName, Description, Category, FriendlyName)
			SELECT @applicationId, @roleId, @NAME, LOWER(@NAME), @DESCRIPTION, @CATEGORY, @FRIENDLYNAME

		-- add this permission to all "Admin" groups
		INSERT INTO aspnet_RolesInGroups (RoleId, GroupId)
			SELECT @roleId, GroupId FROM aspnet_Groups WHERE GroupName IN ('_DCAdministrator', '_DCSystemManager', '_DCSupport')

		-- Add the new permission to every user in the _DCAdministrator, _DCSystemManager, and _DCSupport groups
		INSERT INTO aspnet_UsersInRoles (UserId, RoleId)
		SELECT UserId, @roleId
		FROM aspnet_UsersInGroups UIG
		JOIN aspnet_Groups G ON G.GroupId = UIG.GroupId
		WHERE GroupName IN ('_DCAdministrator', '_DCSystemManager', '_DCSupport')
		EXCEPT
		SELECT UserId, roleid FROM aspnet_UsersInRoles

	COMMIT TRAN
END

GO

-- add a new Permission (role) for accessing/editing ImportCenter mappings
delete from aspnet_roles where rolename like 'editImportCenter'
go
exec spAddNewPermission 'editImportCenter', 'Allows access/use of ImportCenter Designer', 'Import Center', 'Edit'
go


/************** CLEANUP CODE ************************/
exec _spDropProcedure '_spDropFK'
go
/*********************************************/
-- Created: 4.4.9. - 2016.12.17 - Kevin Alons
-- Purpose: remove a FK by name (only)
-- Changes:
/*********************************************/
CREATE PROCEDURE _spDropFK(@FK varchar(255)) AS
BEGIN
	DECLARE @table nvarchar(255) = (SELECT TOP 1 OBJECT_NAME(parent_object_id) FROM sys.objects WHERE type = 'F' AND name = @FK)
	DECLARE @sql nvarchar(max) = N'ALTER TABLE ' + @table + ' DROP CONSTRAINT ' + @FK
	EXEC (@sql)
END
GO

-- remove these 2 "randomly" named FKs by name (they will be re-created below with CASCADE DELETE)
while exists (select * from sys.objects where type = 'F' and parent_object_id = object_id('aspnet_UsersInRoles'))
begin
	declare @FK varchar(255) = (select top 1 name from sys.objects where type = 'F' and parent_object_id = object_id('aspnet_UsersInRoles'))
	exec _spDropFK @FK
end
-- recreate these with a name & CASCADE DELETE
ALTER TABLE dbo.aspnet_UsersInRoles 
	ADD CONSTRAINT FK_aspnet_UsersInRoles_Users FOREIGN KEY (UserId) REFERENCES dbo.aspnet_Users (UserId) 
		ON UPDATE NO ACTION 
		ON DELETE CASCADE 
GO
ALTER TABLE dbo.aspnet_UsersInRoles 
	ADD CONSTRAINT FK_aspnet_UsersInRoles_Roles FOREIGN KEY (RoleId) REFERENCES dbo.aspnet_Roles (RoleId) 
		ON UPDATE NO ACTION 
		ON DELETE CASCADE 
GO

-- replace existing to add ON DELETE CASCADE
exec _spDropFK 'FK_UsersInGroups_UserId'
go
ALTER TABLE dbo.aspnet_UsersInGroups 
	ADD CONSTRAINT FK_aspnet_UsersInGroups_Users FOREIGN KEY (UserId) REFERENCES dbo.aspnet_Users (UserId) 
		ON UPDATE NO ACTION 
		ON DELETE CASCADE 
GO
exec _spDropFK 'FK_UsersInGroups_GroupId'
go
ALTER TABLE dbo.aspnet_UsersInGroups 
	ADD CONSTRAINT FK_aspnet_UsersInGroups_Groups FOREIGN KEY (GroupId) REFERENCES dbo.aspnet_Groups (GroupId) 
		ON UPDATE NO ACTION 
		ON DELETE CASCADE 
GO

-- recreate these with CASCADE DELETE
exec _spDropFK 'FK_RolesInGroups_GroupId'
go
ALTER TABLE dbo.aspnet_RolesInGroups 
	ADD CONSTRAINT FK_aspnet_RolesInGroups_Group FOREIGN KEY (GroupId) REFERENCES dbo.aspnet_Groups (GroupId) 
		ON UPDATE NO ACTION 
		ON DELETE CASCADE 
GO
exec _spDropFK 'FK_RolesInGroups_RoleId'
go
ALTER TABLE dbo.aspnet_RolesInGroups 
	ADD CONSTRAINT FK_aspnet_RolesInGroups_Role FOREIGN KEY (RoleId) REFERENCES dbo.aspnet_Roles (RoleId) 
		ON UPDATE NO ACTION 
		ON DELETE CASCADE 
GO

-- rename all "random" or inconsistent PKs to PK_ + tablename - NOTE: this is a slow process
select pk = Name, object = object_name(parent_object_id) 
into #pk
from sys.objects 
where type = 'PK' and object_name(parent_object_id) not like 'TT%'
  and (name like 'PK[_][_]%' or name like '%primarykey' or name like 'PK_tbl%' or name = 'PK_USStates')

declare @pk nvarchar(255), @newpk nvarchar(255)
while exists (select * FROM #pk)
begin
	select top 1 @pk = pk, @newpk = 'PK_' + replace(object, 'tbl', '') from #pk
	exec sp_rename @pk, @newpk
	delete from #pk where pk = @pk
end
go

COMMIT
SET NOEXEC OFF

/* test code
select * from tblUserReportDefinition where id = 204
SELECT * FROM viewUserReportColumnDefinition WHERE UserReportID = 204 and reportcolumnid = 4
select * from tblsetting where id = 0
select * from tblsettingtype
select * from tblsetting where settingtypeid = 5
select * from tblImportCenterDefinition where rootobjectid <> 1
select * from aspnet_roles where rolename like '%import%'
select * from aspnet_groups where groupname like 'kda'
select * from aspnet_rolesingroups where groupid = '68981B94-C3D4-4271-A0DB-65EFEACB2003'
select * from aspnet_usersingroups where groupid = '68981B94-C3D4-4271-A0DB-65EFEACB2003'
delete from aspnet_roles where rolename like 'editImportCenter'
*/