SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.1.26'
SELECT  @NewVersion = '4.2.0'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'DCWEB-1623 - HOS Violations'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

EXEC _spDropFunction fnToFloat
GO

/***************************************
Date Created: 2016-10-17
Author: Joe Engler
Purpose: convert a string to an Float value
Changes:
***************************************/	
CREATE FUNCTION fnToFloat(@value VARCHAR(255)) RETURNS FLOAT AS
BEGIN
	DECLARE @ret FLOAT
	IF dbo.fnIsNumeric(@value) = 1
		SET @ret = CAST(@value as FLOAT)
	RETURN (@ret)
END

GO

UPDATE tblCarrierRuleType 
SET Name = 'HOS - Hours Required For Weekly Reset',
	Description = 'Minimum number of hours a driver must rest before weekly status is reset.  (Default is 34)'
WHERE ID = 12

INSERT INTO tblCarrierRuleType
	(ID, Name, RuleTypeID, ForDriverApp, Description)
SELECT 15, 'HOS - Hours Required For Daily Reset', 3, 1, 'Minimum number of hours a driver must rest before daily status is reset. (Default is 10)'
UNION
SELECT 16, 'HOS - Shift Interval (Days)', 3, 1, 'The number of days constituting a shift or "week"'
UNION
SELECT 17, 'HOS - Hours Required For Sleeper Break', 3, 1, 'Minimum number of hours a driver must rest to constitute a sleeper break.  (Default is 8)'
UNION
SELECT 18, 'HOS - Max Driving Hours Before Break', 3, 1, 'Maximum number of hours a driver can drive before taking a required break.  (Default is 8)'
UNION
SELECT 19, 'HOS - Minutes Required For Break', 3, 1, 'Minimum number of minutes a driver must rest to constitute a break.  (Default is 30)'
EXCEPT SELECT * FROM tblCarrierRuleType
GO


/********************************************
-- Date Created: 2016 Jun 23
-- Author: Joe Engler
-- Purpose: Get summary counts from HOS records
--		4.2.0		JAE		2016/09/21		Don't sum up columns (in another fn)
********************************************/
ALTER FUNCTION fnHosSummary(@DriverID INT, @StartDate DATETIME, @EndDate DATETIME) 
RETURNS TABLE AS RETURN
	WITH rows AS (
		-- Dummy start record, gets the previous status when the start date occurred
		SELECT 0 AS rownum,
			ISNULL((SELECT TOP 1 HosDriverStatusID FROM tblHos WHERE DriverID = @DriverID AND LogDateUTC < @StartDate ORDER BY LogDateUTC DESC), 1) AS HosDriverStatusID,
			@StartDate AS LogDateUTC

		UNION

		SELECT ROW_NUMBER() OVER (ORDER BY LogDateUTC) AS rownum,
			HosDriverStatusID, 
			LogDateUTC
		FROM tblHos
		WHERE DriverID = @DriverID
		AND LogDateUTC BETWEEN @StartDate AND @EndDate

		UNION

		-- Dummy end record, gets the final status when the end date occurred
		SELECT (select count(*)+1 FROM tblHos WHERE DriverID = @DriverID AND LogDateUTC BETWEEN @StartDate AND @EndDate) AS rownum,
			ISNULL((SELECT TOP 1 HosDriverStatusID FROM tblHos WHERE DriverID = @DriverID AND LogDateUTC <= @EndDate ORDER BY LogDateUTC DESC), 1), 
			CASE WHEN @EndDate > GETUTCDATE() THEN GETUTCDATE() ELSE @EndDate END
	) 
	SELECT mc.LogDateUTC,
		EndDateUTC = mp.LogDateUTC,
		DATEDIFF(MINUTE, mc.LogDateUTC, mp.LogDateUTC)/60.0 AS TotalHours, 
		mc.HosDriverStatusID,
		SleeperStatusID = CASE WHEN mc.HosDriverStatusID = 2 THEN 1 
							--WHEN mc.HosDriverStatusID = 4 THEN 3
							ELSE mc.hosdriverstatusid END -- group sleeping and off duty together for aggregating rest time
	FROM rows mc
	JOIN rows mp
	ON mc.rownum = mp.rownum-1
	
GO


EXEC _spDropFunction fnHosViolationDetail
GO

/********************************************
-- Date Created: 2016 Sep 21
-- Author: Joe Engler
-- Purpose: Get summary counts from HOS records and determine if violations occur.  Use passed date minus a shift for a starting point
********************************************/
CREATE FUNCTION fnHosViolationDetail(@DriverID INT, @StartDate DATETIME, @EndDate DATETIME)
RETURNS @ret TABLE
(
	DriverID INT,
	LogDateUTC DATETIME, 
	EndDateUTC DATETIME, 
	TotalHours FLOAT, 
	TotalHoursSleeping FLOAT,
	TotalHoursDriving FLOAT,
	SleeperStatusID INT,
	WeeklyReset BIT,
	DailyReset BIT,
	SleepBreak BIT, 
	DriverBreak BIT,
	Status VARCHAR(20),
	SleeperReset BIT,
	LastWeeklyReset DATETIME,
	LastDailyReset DATETIME,
	LastBreak DATETIME,
	LastSleeperBerthReset DATETIME,
	HoursSinceWeeklyReset FLOAT,
	DrivingHoursSinceWeeklyReset FLOAT,
	OnDutyHoursSinceDailyReset FLOAT,
	DrivingHoursSinceDailyReset FLOAT,
	OnDutyDailyLimit FLOAT,
	HoursSinceBreak FLOAT,
	OnDutyHoursLeft FLOAT,
	OnDutyViolation BIT,
	DrivingDailyLimit FLOAT,
	DrivingHoursLeft FLOAT,
	DrivingViolation BIT, 
	BreakLimit FLOAT,
	HoursTilBreak FLOAT,
	BreakViolation BIT,
	WeeklyDrivingLimit FLOAT,
	WeeklyDrivingHoursLeft FLOAT,
	WeeklyDrivingViolation BIT,
	OnDutyViolationDateUTC DATETIME,
	DrivingViolationDateUTC DATETIME,
	BreakViolationDateUTC DATETIME,
	WeeklyDrivingViolationDateUTC DATETIME
)
AS BEGIN

	DECLARE @CarrierRules TABLE
	(
		ID INT,
		Value VARCHAR(255)
	)

	INSERT INTO @CarrierRules
	SELECT cr.TypeID, cr.Value
	FROM viewDriverBase d
	CROSS APPLY dbo.fnCarrierRules(@EndDate, null, null, d.CarrierID, d.ID, d.OperatingStateID, d.RegionID, 1) cr
	WHERE d.id = @DriverID

	DECLARE @WEEKLYRESET FLOAT = ISNULL((SELECT NULLIF(dbo.fnToFloat(Value),0) from @CarrierRules WHERE ID = 12), 34)
	DECLARE @DAILYRESET FLOAT = ISNULL((SELECT NULLIF(dbo.fnToFloat(Value),0) from @CarrierRules WHERE ID = 15), 10)
	DECLARE @SLEEPERBREAK FLOAT = ISNULL((SELECT NULLIF(dbo.fnToFloat(Value),0) from @CarrierRules WHERE ID = 17), 8)
	DECLARE @BREAK FLOAT = ISNULL((SELECT NULLIF(dbo.fnToFloat(Value),0) from @CarrierRules WHERE ID = 19), 30)/60.0

	DECLARE @WEEK FLOAT = ISNULL((SELECT NULLIF(dbo.fnToFloat(Value),0) from @CarrierRules WHERE ID = 16), 7)
	DECLARE @DRIVING_WEEKLY_LIMIT FLOAT = ISNULL((SELECT NULLIF(dbo.fnToFloat(Value),0) from @CarrierRules WHERE ID = 6), 60)
	DECLARE @ONDUTY_DAILY_LIMIT FLOAT = ISNULL((SELECT NULLIF(dbo.fnToFloat(Value),0) from @CarrierRules WHERE ID = 7), 14)
	DECLARE @DRIVING_DAILY_LIMIT FLOAT = ISNULL((SELECT NULLIF(dbo.fnToFloat(Value),0) from @CarrierRules WHERE ID = 5), 11)
	DECLARE @DRIVING_BREAK_LIMIT FLOAT = ISNULL((SELECT NULLIF(dbo.fnToFloat(Value),0) from @CarrierRules WHERE ID = 18), 8)

	IF @StartDate IS NULL
		SELECT @StartDate = DATEADD(DAY, -1, @EndDate) -- one day

	-- Get records up to previous shift for calculating violations, these older records will get filtered out later
	DECLARE @StartDateX DATETIME = DATEADD(DAY, -@WEEK, @StartDate) -- one shift


	-- Core HOS data with the basic info filled in (i.e. is this a weekly reset, daily reset, etc)
	-- Sleeper and Off Duty are grouped together for summing off duty blocks
	DECLARE @tmpHOS TABLE 
	(
		LogDateUTC DATETIME, 
		EndDateUTC DATETIME, 
		TotalHours FLOAT, 
		TotalHoursSleeping FLOAT,
		TotalHoursDriving FLOAT,
		SleeperStatusID INT,
		WeeklyReset INT,
		DailyReset INT,
		SleepBreak INT, -- As INT for multiplying as factor 
		DriverBreak INT,
		Status VARCHAR(20)
	)

	INSERT INTO @tmpHOS
	SELECT qq.*,
		WeeklyReset = CASE WHEN SleeperStatusID = 1 AND TotalHours >= @WEEKLYRESET THEN 1 ELSE 0 END,
		DailyReset = CASE WHEN SleeperStatusID = 1 AND TotalHours >= @DAILYRESET THEN 1 ELSE 0 END,
		SleepBreak = CASE WHEN SleeperStatusID = 2 AND TotalHours >= @SLEEPERBREAK THEN 1 
						 WHEN SleeperStatusID = 1 AND TotalHours >= @DAILYRESET THEN 1 ELSE 0 END,
		DriverBreak = CASE WHEN SleeperStatusID IN (1,2) AND TotalHours >= @BREAK THEN 1 ELSE 0 END,
		Status = CASE WHEN SleeperStatusID = 1 AND TotalHours >= @WEEKLYRESET THEN 'WEEKLY RESET'
							WHEN SleeperStatusID = 1 AND TotalHours >= @DAILYRESET THEN 'DAILY RESET' 
							WHEN SleeperStatusID = 2 AND TotalHours < @DAILYRESET AND TotalHours >= @SLEEPERBREAK THEN 'SLEEP BREAK'
							WHEN SleeperStatusID = 2 AND TotalHours < @DAILYRESET AND TotalHours >= @BREAK THEN 'BREAK' ELSE '' END
	FROM (
		SELECT LogDateUTC = MIN(LogDateUTC), EndDateUTC = MAX(EndDateUTC), TotalHours = SUM(TotalHours), 
			TotalHoursSleeping = SUM(CASE WHEN HosDriverStatusID = 2 THEN TotalHours ELSE NULL END),
			TotalHoursDriving = SUM(CASE WHEN HosDriverStatusID = 3 THEN TotalHours ELSE NULL END),
			SleeperStatusID = CASE WHEN SleeperStatusID = 1 AND SUM(TotalHours) < @DAILYRESET THEN 2 --break
								ELSE SleeperStatusID END
		FROM
		(
			SELECT LogDateUTC, EndDateUTC, 
				TotalHours, 
				SleeperStatusID, HosDriverStatusID,
				Streak = (SELECT COUNT(*) FROM dbo.fnHosSummary(@driverid, @StartDateX, @EndDate) x
								WHERE x.SleeperStatusID <> y.SleeperStatusID AND x.LogDateUTC <= y.LogDateUTC) -- used just to group streaks
			FROM dbo.fnHosSummary(@DriverID, @StartDateX, @EndDate) y
		) Q
		GROUP BY Streak, SleeperStatusID
	) QQ
	WHERE TotalHours > 0 -- Ignore any "quick" changes
	ORDER BY LogDateUTC


	------------------------------------------

	-- Get core HOS data with "last" dates and sleeper reset information
	DECLARE @tmpHOSwithSleeperReset TABLE 
	(
		LogDateUTC DATETIME, 
		EndDateUTC DATETIME, 
		TotalHours FLOAT, 
		TotalHoursSleeping FLOAT,
		TotalHoursDriving FLOAT,
		SleeperStatusID INT,
		WeeklyReset INT,
		DailyReset INT,
		SleepBreak INT, -- As INT for multiplying as factor 
		DriverBreak INT,
		Status VARCHAR(20),
		SleeperReset INT,
		LastWeeklyReset DATETIME,
		LastDailyReset DATETIME,
		LastBreak DATETIME
	)

	INSERT INTO @tmpHOSwithSleeperReset 
	(
		LogDateUTC,
		EndDateUTC, 
		TotalHours, 
		TotalHoursSleeping,
		TotalHoursDriving,
		SleeperStatusID,
		WeeklyReset,
		DailyReset,
		SleepBreak, 
		DriverBreak,
		Status,
		SleeperReset,
		LastWeeklyReset,
		LastDailyReset,
		LastBreak
	)
	SELECT hos.*, 
		SleeperReset = CASE WHEN lsb.Hrs >= 10 OR hos.DailyReset = 1 THEN 1 ELSE 0 END,

		LastWeeklyReset = ISNULL(lr.EndDateUTC, @StartDateX),
		LastDailyReset = COALESCE(ldr.EndDateUTC, lr.EndDateUTC, @StartDateX),
		LastBreak = COALESCE(lb.EndDateUTC, ldr.EndDateUTC, lr.EndDateUTC, @StartDateX)

	FROM @tmphos hos
	-- Last Weekly Reset
	OUTER APPLY (SELECT TOP 1 EndDateUTC 
					FROM @tmphos
					WHERE WeeklyReset = 1 AND LogDateUTC < hos.LogDateUTC 
					ORDER BY LogDateUTC DESC) lr
	-- Last Daily Reset
	OUTER APPLY (SELECT TOP 1 EndDateUTC 
					FROM @tmphos
					WHERE DailyReset = 1 AND LogDateUTC < hos.LogDateUTC 
					ORDER BY LogDateUTC DESC) ldr
	-- Last Break
	OUTER APPLY (SELECT TOP 1 EndDateUTC 
					FROM @tmphos
					WHERE DriverBreak = 1 AND LogDateUTC < hos.LogDateUTC 
					ORDER BY LogDateUTC DESC) lb
	-- Last Sleeper Berth Reset (used to see if a reset, date is grabbed later since you use the previous break)
	OUTER APPLY (SELECT EndDateUTC = MIN(EndDateUTC), 
						Hrs = SUM(ISNULL(TotalHoursSleeping,0))*MAX(ISNULL(SleepBreak,0)) -- Multiply by sleep break to ensure one of the entries is an 8 hr block
					FROM @tmphos
					WHERE DriverBreak = 1  AND logDateUTC < hos.EndDateUTC 
					AND TotalHoursSleeping >= 2 -- Qualified breaks must be 2 hours or more
					AND EndDateUTC >= COALESCE(lb.EndDateUTC, ldr.EndDateUTC)) lsb -- Since Last Daily Reset


	------------------------------------------


	-- Get detail HOS information with total hours and violations
	INSERT INTO @ret (
		DriverID,
		LogDateUTC, 
		EndDateUTC, 
		TotalHours, 
		TotalHoursSleeping,
		TotalHoursDriving,
		SleeperStatusID,
		WeeklyReset,
		DailyReset,
		SleepBreak, 
		DriverBreak,
		Status,
		SleeperReset,
		LastWeeklyReset,
		LastDailyReset,
		LastBreak,
		LastSleeperBerthReset,
		HoursSinceWeeklyReset,
		DrivingHoursSinceWeeklyReset,
		OnDutyHoursSinceDailyReset,
		DrivingHoursSinceDailyReset,
		HoursSinceBreak,
		OnDutyDailyLimit,
		OnDutyHoursLeft,
		OnDutyViolation,
		DrivingDailyLimit,
		DrivingHoursLeft,
		DrivingViolation,
		BreakLimit,
		HoursTilBreak,
		BreakViolation,
		WeeklyDrivingLimit,
		WeeklyDrivingHoursLeft,
		WeeklyDrivingViolation,
		OnDutyViolationDateUTC,
		DrivingViolationDateUTC,
		BreakViolationDateUTC,
		WeeklyDrivingViolationDateUTC
	)
	SELECT @DriverID,
		*,
		OnDutyViolationDateUTC = CASE WHEN OnDutyViolation = 0 THEN NULL
				ELSE DATEADD(MINUTE, 60*(TotalHours - (OnDutyHoursSinceDailyReset - OnDutyDailyLimit)), LogDateUTC) END,
		DrivingViolationDateUTC = CASE WHEN DrivingViolation = 0 THEN NULL
				ELSE DATEADD(MINUTE, 60*(TotalHours - (DrivingHoursSinceDailyReset - DrivingDailyLimit)), LogDateUTC) END,
		BreakViolationDateUTC = CASE WHEN BreakViolation = 0 THEN NULL
				ELSE DATEADD(MINUTE, 60*(TotalHours + HoursTilBreak), LogDateUTC) END,
		WeeklyDrivingViolationDateUTC = CASE WHEN WeeklyDrivingViolation = 0 THEN NULL
				ELSE DATEADD(MINUTE, 60*(TotalHours - (DrivingHoursSinceWeeklyReset - WeeklyDrivingLimit)), LogDateUTC) END
	FROM 
	(
		SELECT hos.*,
			OnDutyDailyLimit = @ONDUTY_DAILY_LIMIT,
			OnDutyHoursLeft = CASE WHEN @ONDUTY_DAILY_LIMIT < OnDutyHoursSinceDailyReset THEN 0 ELSE @ONDUTY_DAILY_LIMIT - OnDutyHoursSinceDailyReset END,
			OnDutyViolation = CASE WHEN @ONDUTY_DAILY_LIMIT < OnDutyHoursSinceDailyReset AND SleeperStatusID IN (3,4) THEN 1 ELSE 0 END,

			DrivingDailyLimit = @DRIVING_DAILY_LIMIT,
			DrivingHoursLeft = CASE WHEN @DRIVING_DAILY_LIMIT < DrivingHoursSinceDailyReset THEN 0 ELSE @DRIVING_DAILY_LIMIT - DrivingHoursSinceDailyReset END,
			DrivingViolation = CASE WHEN @DRIVING_DAILY_LIMIT < DrivingHoursSinceDailyReset AND SleeperStatusID = 3 THEN 1 ELSE 0 END,

			BreakLimit = @DRIVING_BREAK_LIMIT,
			HoursTilBreak = @DRIVING_BREAK_LIMIT - HoursSinceBreak,
			BreakViolation = CASE WHEN @DRIVING_BREAK_LIMIT < HoursSinceBreak AND SleeperStatusID = 3 THEN 1 ELSE 0 END,

			WeeklyDrivingLimit = @DRIVING_WEEKLY_LIMIT,
			WeeklyDrivingHoursLeft = CASE WHEN @DRIVING_WEEKLY_LIMIT < DrivingHoursSinceWeeklyReset THEN 0 ELSE @DRIVING_WEEKLY_LIMIT - DrivingHoursSinceWeeklyReset END,
			WeeklyDrivingViolation = CASE WHEN @DRIVING_WEEKLY_LIMIT < DrivingHoursSinceWeeklyReset AND SleeperStatusID = 3 THEN 1 ELSE 0 END

		FROM
		(
			SELECT hos.*,
				HoursSinceWeeklyReset = 
							CASE WHEN WeeklyReset = 1 THEN 0
							ELSE ISNULL((SELECT SUM(TotalHours) FROM @tmphos WHERE LogDateUTC >= LastWeeklyReset AND LogDateUTC < hos.EndDateUTC), 0) END,
				DrivingHoursSinceWeeklyReset = 
							CASE WHEN WeeklyReset = 1 THEN 0
							ELSE ISNULL((SELECT SUM(TotalHours) FROM @tmphos WHERE LogDateUTC >= LastWeeklyReset AND LogDateUTC < hos.EndDateUTC 
																			AND SleeperStatusID = 3), 0) END,
				OnDutyHoursSinceDailyReset = 
							CASE WHEN DailyReset = 1 THEN 0
							ELSE ISNULL((SELECT SUM(TotalHours) FROM @tmphos WHERE LogDateUTC >= LastSleeperBerthReset AND LogDateUTC < hos.EndDateUTC 
																			AND SleepBreak = 0), 0) END, -- Skip sleeper block
				DrivingHoursSinceDailyReset = 
							CASE WHEN DailyReset = 1 THEN 0
							ELSE ISNULL((SELECT SUM(TotalHours) FROM @tmphos WHERE LogDateUTC >= LastSleeperBerthReset AND LogDateUTC < hos.EndDateUTC 
																			AND SleeperStatusID = 3), 0) END,
				HoursSinceBreak = 
							CASE WHEN DriverBreak = 1 THEN 0
							ELSE ISNULL((SELECT SUM(TotalHours) FROM @tmphos WHERE SleepBreak = 0 AND LogDateUTC >= LastBreak AND LogDateUTC < hos.EndDateUTC),0) END
			FROM 
			(
				SELECT hos.*,
					LastSleeperBerthReset = dbo.fnMaxDateTime(lb.LastBreak, LastDailyReset) -- get more recent between sleeper reset or daily reset

				FROM @tmpHOSwithSleeperReset hos
				-- Last Sleeper Berth Reset
				OUTER APPLY (SELECT TOP 1 LastBreak
									FROM @tmpHOSwithSleeperReset
									WHERE SleeperReset = 1 AND LogDateUTC < hos.LogDateUTC
									ORDER BY LogDateUTC DESC) lb
			) hos
		) hos
	) Q
	WHERE EndDateUTC > @StartDate -- Filter older records used for calculating violations

	RETURN
END
GO


EXEC _spDropFunction fnHosDriverViolationSummary
GO

/********************************************
-- Date Created: 2016 Sep 21
-- Author: Joe Engler
-- Purpose: Get daily summary counts HOS violations
		-- CTE IS LIMITED TO 100 loops so max three months should be fine
********************************************/
CREATE FUNCTION fnHosDriverViolationSummary(@DriverID INT, @StartDate DATETIME, @EndDate DATETIME, @interval VARCHAR(20) = 'day')
RETURNS TABLE AS
	RETURN 
	WITH dayloop AS
	(
		SELECT @StartDate AS [Date]
		UNION ALL
		SELECT CASE @interval WHEN 'month' THEN DATEADD(MONTH, 1, [Date])
								ELSE DATEADD(DAY, 1, [Date]) END
		FROM dayloop
		WHERE CASE @interval WHEN 'month' THEN DATEADD(MONTH, 1, [Date]) 
								ELSE DATEADD(DAY, 1, [Date]) END < @EndDate 
	)
	SELECT d.Date, DriverID = @DriverID,
		UniqueViolations = SUM(CASE WHEN hos.WeeklyDrivingViolation = 1 OR hos.OnDutyViolation = 1 
											OR hos.DrivingViolation = 1 OR hos.BreakViolation = 1 THEN 1 ELSE 0 END),
		WeeklyDrivingViolations = SUM(CASE WHEN hos.WeeklyDrivingViolation = 1 THEN 1 ELSE 0 END),
		OnDutyViolations = SUM(CASE WHEN hos.OnDutyViolation = 1 THEN 1 ELSE 0 END),
		DrivingViolations = SUM(CASE WHEN hos.DrivingViolation = 1 THEN 1 ELSE 0 END),
		BreakViolations = SUM(CASE WHEN hos.BreakViolation = 1 THEN 1 ELSE 0 END)

		FROM dayloop d
		LEFT JOIN fnHosViolationDetail(@DriverID, @StartDate, @EndDate) hos ON hos.LogDateUTC >= d.Date 
					AND hos.LogDateUTC < CASE @interval WHEN 'month' THEN DATEADD(MONTH, 1, [Date]) ELSE DATEADD(DAY, 1, d.Date) END
		GROUP BY d.Date, hos.DriverID
GO



/****************************************************/
-- Created: 2016.09.14 - 4.1.7 - Joe Engler
-- Purpose: Retrieve the drivers (without last known GPS coordinates) for the specified filter criteria
-- Changes:
--		4.1.8.3		JAE		2016-09-21		Fixed refrence to current time to start time (passed in)
--		4.1.10.1	JAE		2016-09-27		Added qualifier to DeleteDateUTC column (since same column was added to route table)
--		4.2.0		JAE		2016-09-30		Add hos fields to view
/****************************************************/
ALTER FUNCTION [dbo].[fnRetrieveEligibleDrivers_NoGPS](@CarrierID INT, @RegionID INT, @StateID INT, @DriverGroupID INT, @StartDate DATETIME) 
RETURNS @ret TABLE 
(
	ID INT, 
	LastName VARCHAR(20), FirstName VARCHAR(20), FullName VARCHAR(41), FullNameLF VARCHAR(42),
	CarrierID INT, Carrier VARCHAR(40),
	RegionID INT,
	TruckID INT, Truck VARCHAR(10),
	TrailerID INT, Trailer VARCHAR(10),
	AvailabilityFactor DECIMAL(4,2),
	ComplianceFactor DECIMAL(4,2),
	TruckAvailabilityFactor DECIMAL(4,2),
	CurrentWorkload INT,
	CurrentECOT INT,
	HOSFactor DECIMAL(4,2),
	DrivingHoursSinceWeeklyReset DECIMAL(6,2),
	WeeklyDrivingHoursLeft DECIMAL(6,2),
	OnDutyHoursSinceDailyReset DECIMAL(6,2),
	OnDutyHoursLeft DECIMAL(6,2),
	DrivingHoursSinceDailyReset DECIMAL(6,2),
	DrivingHoursLeft DECIMAL(6,2),
	HOSHrsOnShift DECIMAL(6,2),
	DriverScore INT 
)
AS BEGIN
	DECLARE @__ENFORCE_DRIVER_AVAILABILITY__ INT = 13
	DECLARE @__ENFORCE_DRIVER_COMPLIANCE__ INT = 14
	DECLARE @__ENFORCE_HOS__ INT = 1

	IF @StartDate IS NULL 
	   SELECT @StartDate = dbo.fnDateOnly(GETDATE())

	INSERT INTO @ret
	SELECT X.*,
		DriverScore = 100 * AvailabilityFactor * ComplianceFactor * TruckAvailabilityFactor * HOSFactor
	FROM (
		SELECT d.ID, 
			d.FirstName, d.LastName, FullName, d.FullNameLF,
			d.CarrierID, d.Carrier,
			d.RegionID,
			d.TruckID, d.Truck,
			d.TrailerID, d.Trailer,

			--Availability
			AvailabilityFactor = CASE WHEN cr_a.Value IS NULL OR dbo.fnToBool(cr_a.Value) = 0 THEN 1 -- availability not enfoced
										WHEN IsAvailable IS NULL THEN 0 -- Not explicitly available
										ELSE IsAvailable END,

			-- Compliance
			ComplianceFactor = CASE WHEN cr_c.Value IS NULL OR dbo.fnToBool(cr_c.Value) = 0 THEN 1 -- compliance not enforced
									WHEN d.CDLExpiration < @StartDate -- CDL expired
											OR d.H2SExpiration < @StartDate -- H2s expired
											OR d.DLExpiration < @StartDate -- DL expired
											--OR d.MedicalCardDate
											THEN 0
									ELSE 1 END, -- all ok

			--Truck/Trailer Availability
			TruckAvailabilityFactor = 1,
			
			--current workload, time on shift
			o.CurrentWorkload,
			CurrentECOT = ISNULL(o.CurrentECOT, 0),

			-- HOS
			HOSFactor = CASE WHEN cr_h.Value IS NULL OR dbo.fnToBool(cr_h.Value) = 0 THEN 1 -- HOS not enabled
							WHEN hos.WeeklyDrivingHoursLeft <= 0 OR hos.OnDutyHoursLeft <= 0 OR hos.DrivingHoursLeft <= 0 THEN 0 
							-- compare ECOT to hours and see
							ELSE 1 END,
			hos.DrivingHoursSinceWeeklyReset,
			hos.WeeklyDrivingHoursLeft,
			hos.OnDutyHoursSinceDailyReset,
			hos.OnDutyHoursLeft,
			hos.DrivingHoursSinceDailyReset,
			hos.DrivingHoursLeft,
			HrsOnShift = CAST(DATEDIFF(MINUTE, AvailDateTime, GETDATE())/60.0*IsAvailable AS DECIMAL(5,1)) -- from availabilty


		FROM viewDriverBase d -- KDA NOTE: slightly more efficient
		LEFT JOIN tblDriverAvailability da ON da.DriverID = d.ID AND CAST(AvailDateTime AS DATE) = @StartDate
		OUTER APPLY dbo.fnCarrierRules(@StartDate, null, @__ENFORCE_DRIVER_AVAILABILITY__, d.CarrierID, d.ID, d.StateID, d.RegionID, 1) cr_a -- Enforce Driver Availability Carrier Rule
		OUTER APPLY dbo.fnCarrierRules(@StartDate, null, @__ENFORCE_DRIVER_COMPLIANCE__, d.CarrierID, d.ID, d.StateID, d.RegionID, 1) cr_c
		OUTER APPLY dbo.fnCarrierRules(@StartDate, null, @__ENFORCE_HOS__, d.CarrierID, d.ID, d.StateID, d.RegionID, 1) cr_h
		OUTER APPLY (SELECT TOP 1 * FROM dbo.fnHosViolationDetail(d.ID, null, @StartDate) ORDER BY LogDateUTC DESC) hos
		OUTER APPLY (
			SELECT CurrentWorkload = COUNT(*), 
					CurrentECOT = SUM(CASE WHEN o.StatusID = 8 THEN ISNULL(r.ECOT, 60+ISNULL(r.ActualMiles,60)+60)/2
										   ELSE COALESCE(r.ECOT, 60+ISNULL(r.ActualMiles,60)+60) END)
			FROM tblOrder o LEFT JOIN tblRoute r ON o.RouteID = r.ID
			WHERE o.DriverID = d.ID
			  AND o.StatusID IN (2,7,8) -- Dispatched, Accepted, Picked Up
			  AND o.DeleteDateUTC IS NULL) o

		WHERE d.DeleteDateUTC IS NULL
			AND d.TruckID IS NOT NULL AND d.TrailerID IS NOT NULL
			AND (ISNULL(@CarrierID, 0) <= 0 OR @CarrierID = d.CarrierID)
			AND (ISNULL(@DriverGroupID, 0) <= 0 OR @DriverGroupID = d.ID)
			AND (ISNULL(@RegionID, -1) <= 0 OR @RegionID = d.RegionID)
			AND (ISNULL(@StateID, 0) <= 0 OR @StateID = d.StateID)
	) AS X
	ORDER BY DriverScore DESC, LastName, FirstName

	RETURN
END

GO


EXEC _spRebuildAllObjects
GO

COMMIT
SET NOEXEC OFF