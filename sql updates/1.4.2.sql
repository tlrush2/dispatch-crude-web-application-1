DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '1.4.1', @NewVersion = '1.4.2'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

ALTER TABLE tblDestination ADD Station varchar(10) NULL
GO
ALTER TABLE tblOrigin ADD TaxRate smallmoney NULL
GO
ALTER TABLE tblCarrierRates ADD FuelSurcharge smallmoney NULL
GO
ALTER TABLE tblCustomerRates ADD FuelSurcharge smallmoney NULL
GO
ALTER TABLE tblOrderInvoiceCarrier ADD FuelSurcharge smallmoney NULL
GO
ALTER TABLE tblOrderInvoiceCustomer ADD FuelSurcharge smallmoney NULL
GO

DROP PROCEDURE spProcessCarrierInvoice_RateMiles
GO
DROP PROCEDURE spProcessCustomerInvoice_RateMiles
GO
DROP TABLE tblCarrierRangeRates
GO
DROP TABLE tblCustomerRangeRates
GO
DROP VIEW viewCarrierRangeRates
GO
DROP VIEW viewCustomerRangeRates
GO

EXEC _spRefreshAllViews
GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Carrier "Settlement" $$ values to an Audited order
/***********************************/
ALTER PROCEDURE [dbo].[spProcessCarrierInvoice]
(
  @ID int
, @UserName varchar(100)
, @ChainupFee money = null
, @RerouteFee money = null
, @WaitFee money = null
, @RejectionFee money = null
, @LoadFee money = null
) AS BEGIN
	EXEC dbo.spSyncProducer @ID
	EXEC dbo.spSyncActualMiles @ID
	
	DELETE FROM tblOrderInvoiceCarrier WHERE OrderID = @ID
	
	INSERT INTO tblOrderInvoiceCarrier (OrderID, ChainupFee, RerouteFee, WaitFeeSubUnitID, WaitFeeRoundingTypeID, BillableWaitMinutes, WaitRate, WaitFee
		, RejectionFee, RouteRate, LoadFee, FuelSurcharge, TotalFee, CreateDate, CreatedByUser)
	SELECT D.ID, ChainupFee, RerouteFee, WaitFeeSubUnitID, WaitFeeRoundingTypeID, BillableWaitMinutes, WaitRate, WaitFee
		, RejectionFee, RouteRate, LoadFee, FuelSurcharge, ChainupFee + RerouteFee + WaitFee + LoadFee AS TotalFee, GETDATE(), @UserName
	FROM (
		SELECT ID
			, isnull(Chainup, 0) * coalesce(@ChainupFee, ChainupFee, 0) AS ChainupFee
			, coalesce(@RerouteFee, RerouteFee * RerouteCount, 0) AS RerouteFee
			, WaitFeeSubUnitID
			, WaitFeeRoundingTypeID
			, cast(BillableWaitHours * 60 as int) AS BillableWaitMinutes
			, WaitFee AS WaitRate
			, coalesce(@WaitFee, BillableWaitHours * WaitFee, 0) AS WaitFee
			, coalesce(@RejectionFee, Rejected * RejectionFee, 0) AS RejectionFee
			, coalesce(@LoadFee, Rate) AS RouteRate
			, coalesce(@LoadFee, dbo.fnMaxDecimal(MinSettlementBarrels, ActualBarrels) * Rate, 0) AS LoadFee
			, FuelSurcharge
		FROM (
			SELECT S.ID
				, S.ChainUp
				, CR.ChainupFee
				, CR.RerouteFee
				, S.RerouteCount
				, S.TotalWaitMinutes
				, CR.WaitFeeSubUnitID
				, CR.WaitFeeRoundingTypeID
				, dbo.fnComputeBillableWaitHours(S.TotalWaitMinutes, CR.WaitFeeSubUnitID, CR.WaitFeeRoundingTypeID) AS BillableWaitHours
				, CR.WaitFee
				, S.Rejected
				, CR.RejectionFee
				, CRR.Rate
				, S.MinSettlementBarrels
				, S.ActualBarrels
				, CR.FuelSurcharge
			FROM (
				SELECT O.ID
					, O.CarrierID
					, O.RouteID
					, O.ChainUp
					, CASE WHEN C.SettlementFactorID = 1 THEN O.OriginGrossBarrels ELSE O.OriginNetBarrels END AS ActualBarrels
					, O.RerouteCount
					, O.TotalWaitMinutes
					, O.Rejected
					, coalesce(CRXX.ID, CRXA.ID, CRAX.ID, CRAA.ID) AS CRID
					, isnull(C.MinSettlementBarrels, 0) AS MinSettlementBarrels
				FROM dbo.viewOrderExportFull O
				JOIN tblOrigin OO ON OO.ID = O.OriginID
				JOIN tblCarrier C ON C.ID = O.CarrierID
				LEFT JOIN tblCarrierRates CRXX ON CRXX.CarrierID = O.CarrierID AND CRXX.RegionID = OO.RegionID
				LEFT JOIN tblCarrierRates CRXA ON CRXA.CarrierID = O.CarrierID AND CRXA.RegionID = -1
				LEFT JOIN tblCarrierRates CRAX ON CRAX.CarrierID = -1 AND CRAX.RegionID = OO.RegionID
				LEFT JOIN tblCarrierRates CRAA ON CRAA.CarrierID = -1 AND CRAA.RegionID = -1
				WHERE O.ID = @ID
			) S
			LEFT JOIN tblCarrierRates CR ON CR.ID = S.CRID
			LEFT JOIN tblCarrierRouteRates CRR ON CRR.CarrierID = S.CarrierID AND CRR.RouteID = S.RouteID 
		) SS
	) D
END

GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Customer "Settlement" $$ values to an Audited order
/***********************************/
ALTER PROCEDURE [dbo].[spProcessCustomerInvoice]
(
  @ID int
, @UserName varchar(100)
, @ChainupFee money = null
, @RerouteFee money = null
, @WaitFee money = null
, @RejectionFee money = null
, @LoadFee money = null
) AS BEGIN
	EXEC dbo.spSyncProducer @ID
	EXEC dbo.spSyncActualMiles @ID
	
	DELETE FROM tblOrderInvoiceCustomer WHERE OrderID = @ID
	
	INSERT INTO tblOrderInvoiceCustomer (OrderID, ChainupFee, RerouteFee, WaitFeeSubUnitID, WaitFeeRoundingTypeID, BillableWaitMinutes, WaitRate, WaitFee
		, RejectionFee, RouteRate, LoadFee, FuelSurcharge, TotalFee, CreateDate, CreatedByUser)
	SELECT D.ID, ChainupFee, RerouteFee, WaitFeeSubUnitID, WaitFeeRoundingTypeID, BillableWaitMinutes, WaitRate, WaitFee
		, RejectionFee, RouteRate, LoadFee, FuelSurcharge, ChainupFee + RerouteFee + WaitFee + LoadFee AS TotalFee, GETDATE(), @UserName
	FROM (
		SELECT ID
			, isnull(Chainup, 0) * coalesce(@ChainupFee, ChainupFee, 0) AS ChainupFee
			, coalesce(@RerouteFee, RerouteFee * RerouteCount, 0) AS RerouteFee
			, WaitFeeSubUnitID
			, WaitFeeRoundingTypeID
			, cast(BillableWaitHours * 60 as int) AS BillableWaitMinutes
			, WaitFee AS WaitRate
			, coalesce(@WaitFee, BillableWaitHours * WaitFee, 0) AS WaitFee
			, coalesce(@RejectionFee, Rejected * RejectionFee, 0) AS RejectionFee
			, coalesce(@LoadFee, Rate) AS RouteRate
			, coalesce(@LoadFee, dbo.fnMaxDecimal(MinSettlementBarrels, ActualBarrels) * Rate, 0) AS LoadFee
			, FuelSurcharge
		FROM (
			SELECT S.ID
				, S.ChainUp
				, CR.ChainupFee
				, CR.RerouteFee
				, S.RerouteCount
				, S.TotalWaitMinutes
				, CR.WaitFeeSubUnitID
				, CR.WaitFeeRoundingTypeID
				, dbo.fnComputeBillableWaitHours(S.TotalWaitMinutes, CR.WaitFeeSubUnitID, CR.WaitFeeRoundingTypeID) AS BillableWaitHours
				, CR.WaitFee
				, S.Rejected
				, CR.RejectionFee
				, CRR.Rate
				, S.MinSettlementBarrels
				, S.ActualBarrels
				, CR.FuelSurcharge
			FROM (
				SELECT O.ID
					, O.CustomerID
					, O.RouteID
					, O.ChainUp
					, CASE WHEN C.SettlementFactorID = 1 THEN O.OriginGrossBarrels ELSE O.OriginNetBarrels END AS ActualBarrels
					, O.RerouteCount
					, O.TotalWaitMinutes
					, O.Rejected
					, coalesce(CRXX.ID, CRXA.ID, CRAX.ID, CRAA.ID) AS CRID
					, isnull(C.MinSettlementBarrels, 0) AS MinSettlementBarrels
				FROM dbo.viewOrderExportFull O
				JOIN tblOrigin OO ON OO.ID = O.OriginID
				JOIN tblCustomer C ON C.ID = O.CustomerID
				LEFT JOIN tblCustomerRates CRXX ON CRXX.CustomerID = O.CustomerID AND CRXX.RegionID = OO.RegionID
				LEFT JOIN tblCustomerRates CRXA ON CRXA.CustomerID = O.CustomerID AND CRXA.RegionID = -1
				LEFT JOIN tblCustomerRates CRAX ON CRAX.CustomerID = -1 AND CRAX.RegionID = OO.RegionID
				LEFT JOIN tblCustomerRates CRAA ON CRAA.CustomerID = -1 AND CRAA.RegionID = -1
				WHERE O.ID = @ID
			) S
			LEFT JOIN tblCustomerRates CR ON CR.ID = S.CRID
			LEFT JOIN tblCustomerRouteRates CRR ON CRR.CustomerID = S.CustomerID AND CRR.RouteID = S.RouteID 
		) SS
	) D
END

GO

COMMIT
SET NOEXEC OFF