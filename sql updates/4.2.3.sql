SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.2.2'
SELECT  @NewVersion = '4.2.3'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1875 - Cleanup for grids'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/* Update existing page view permission to fit with new permissions being added below */
UPDATE aspnet_Roles
SET FriendlyName = 'Questionnaires - View'
, RoleName = 'viewQuestionnaires'
, Category = 'Compliance'
WHERE RoleName = 'manageQuestionnaires'
GO



/* Add new permissions for the Producer maintenance page */
EXEC spAddNewPermission 'createQuestionnaires', 'Allow user to create Questionnaires', 'Compliance', 'Questionnaires - Create'
GO
EXEC spAddNewPermission 'editQuestionnaires', 'Allow user to edit Questionnaires', 'Compliance', 'Questionnaires - Edit'
GO
EXEC spAddNewPermission 'deactivateQuestionnaires', 'Allow user to deactivate Questionnaires', 'Compliance', 'Questionnaires - Deactivate'
GO


/* Fix typo in Carrier documetns permission description */
UPDATE aspnet_Roles
SET Description = 'Allow user to view the Carrier Compliance Documents page'
WHERE RoleName = 'viewComplianceCarrierDocuments'
GO


COMMIT
SET NOEXEC OFF