/* 
	add new System settings for UOM (Report & Default UOMs)
	add new Validation System settings - Max Trailer Capacity Gallons
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.4.0'
SELECT  @NewVersion = '2.4.1'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

INSERT INTO tblSetting (ID, Name, SettingTypeID, Value, Category, CreateDateUTC, CreatedByUser)
	SELECT 16, 'Report Total Unit Of Measure', 3, 1, 'System Wide', GETUTCDATE(), 'System'
INSERT INTO tblSetting (ID, Name, SettingTypeID, Value, Category, CreateDateUTC, CreatedByUser)
	SELECT 17, 'System Default Unit of Measure', 3, 1, 'System Wide', GETUTCDATE(), 'System'
INSERT INTO tblSetting (ID, Name, SettingTypeID, Value, Category, CreateDateUTC, CreatedByUser)
	SELECT 18, 'Max Trailer Capacity (Gallons)', 3, 13440, 'Order Validation', GETUTCDATE(), 'System'
GO

EXEC _spRefreshAllViews
GO
EXEC _spRecompileAllStoredProcedures
GO

COMMIT
SET NOEXEC OFF