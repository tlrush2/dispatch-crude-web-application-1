SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.5.6'
SELECT  @NewVersion = '4.5.7'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Add system setting for requiring origin tank during creation'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


INSERT INTO tblSetting (ID, Name, SettingTypeID, Value, Category, CreateDateUTC, CreatedByUser, ReadOnly, Description, Hidden)
SELECT 70, 'Require Tank on Order Create', 2, 'True', 'Order Create', GETUTCDATE(), 'System', 0, 
		'When set to TRUE, a tank must be selected when creating/dispatching orders.  If FALSE, drivers will have the option to pick a tank during pickup.', 0

GO

COMMIT
SET NOEXEC OFF