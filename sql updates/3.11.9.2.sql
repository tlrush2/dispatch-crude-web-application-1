-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.11.9.1'
SELECT  @NewVersion = '3.11.9.2'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1231: Add roles for un-audit and un-delete pages/actions so they can be applied specifically only to users who need them.'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/* Adding roles (permissions) to the un-audit and un-delete pages in an effort to further the progress of the move to the new roles/permissions changes */
INSERT INTO aspnet_Roles (ApplicationId, RoleId, RoleName, LoweredRoleName, Description)
SELECT '446467D6-39CD-45E3-B3E0-CAC7945AF3E8', NEWID(), 'unauditOrders', 'unauditorders', 'Allow user to un-audit orders'
UNION
SELECT '446467D6-39CD-45E3-B3E0-CAC7945AF3E8', NEWID(), 'undeleteOrders', 'undeleteorders', 'Allow user to un-delete orders'
EXCEPT SELECT ApplicationId, RoleId, RoleName, LoweredRoleName, Description
FROM aspnet_Roles


/* Update role name to match plural standard for permissions */
UPDATE aspnet_Roles
SET RoleName = 'unsettleCarrierBatches', LoweredRoleName = 'unsettlecarrierbatches'
WHERE RoleName = 'unsettleCarrierBatch'


/* Update role name to match plural standard for permissions */
UPDATE aspnet_Roles
SET RoleName = 'unsettleShipperBatches', LoweredRoleName = 'unsettleshipperbatches'
WHERE RoleName = 'unsettleShipperBatch'


COMMIT 
SET NOEXEC OFF