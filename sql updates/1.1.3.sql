/**************************************************************************
-- -Generated by xSQL Schema Compare
-- -Date/Time: Apr 24 2013, 10:12:27 PM

-- -Summary:
    T-SQL script that makes the schema of the database [.\sqlexpr2008r2].[BadlandsDB_SC] 
    the same as the schema of the database [.\sqlexpr2008r2].[BadlandsDB].

-- -Action:
    Execute this script on [.\sqlexpr2008r2].[BadlandsDB_SC].

    **** BACKUP the database [.\sqlexpr2008r2].[BadlandsDB_SC] before running this script ****

-- -Source SQL Server version: 10.50.2500.0
-- -Target SQL Server version: 10.50.2500.0
**************************************************************************/

SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET XACT_ABORT ON
GO


BEGIN TRANSACTION
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************/
-- Date Created: 24 Apr 2013
-- Author: Kevin Alons
-- Purpose: return the total qtr inches for a tank gauge reading
/***********************************/
CREATE FUNCTION [dbo].[fnGaugeQtrInches](@feet smallint, @inch tinyint, @q tinyint) RETURNS int AS
BEGIN
	RETURN @feet * 48 + @inch * 4 + @q
END


GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************/
-- Date Created: 28 Feb 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc for 3rd parties (Customer/Carrier)
/***********************************/
CREATE PROCEDURE [dbo].[spOrderTicketFullExport]
(
  @StartDate datetime
, @EndDate datetime
, @CarrierID int = 0 -- -1 = all carriers
, @CustomerID int = 0 -- -1 = all customers
) AS BEGIN
	SELECT OT.ID
		, O.OrderNum
		, O.OrderStatus
		, OT.TicketType 
		, OO.WellAPI
		, O.OriginDepartTime
		, OT.CarrierTicketNum AS TicketNum
		, OO.LeaseName
		, O.Operator
		, O.Destination
		, OT.NetBarrels
		, TT.HeightFeet AS TankHeight
		, TT.CapacityBarrels AS TankBarrels
		, OT.TankNum
		, OT.ProductObsGravity
		, OT.ProductObsTemp
		, OT.ProductBSW
		, isnull(OT.ProductHighTemp, OT.ProductObsTemp) AS ProductHighTemp
		, OT.OpeningGaugeFeet
		, OT.OpeningGaugeInch
		, OT.OpeningGaugeQ
		, OT.ClosingGaugeFeet
		, OT.ClosingGaugeInch
		, OT.ClosingGaugeQ
		, isnull(OT.ProductLowTemp, OT.ProductObsTemp) AS ProductLowTemp
		, dbo.fnGaugeQtrInches(OT.OpeningGaugeFeet, OT.OpeningGaugeInch, OT.OpeningGaugeQ) AS OpenTotalQ
		, dbo.fnGaugeQtrInches(OT.ClosingGaugeFeet, OT.ClosingGaugeInch, ot.ClosingGaugeQ) AS CloseTotalQ
	FROM dbo.viewOrderTicket OT
	JOIN dbo.viewOrder O ON O.ID = OT.OrderID
	JOIN dbo.tblOrigin OO ON OO.ID = O.OriginID
	JOIN dbo.tblTankType TT ON TT.ID = OT.TankTypeID
	WHERE O.StatusID IN (3, 4) -- Delivered & Audited
	  AND (@CarrierID=-1 OR @CustomerID=-1 OR O.CarrierID=@CarrierID OR O.CustomerID=@CustomerID) 
	  AND O.OriginDepartTime >= @StartDate AND O.OriginDepartTime < dateadd(day, 1, @EndDate) 
	  AND O.DeleteDate IS NULL AND OT.DeleteDate IS NULL
	ORDER BY O.OriginDepartTime
	
END


GO

IF EXISTS(SELECT 1 FROM sys.database_principals WHERE [name] = N'badlandsdb')
	GRANT EXECUTE ON OBJECT::[dbo].[spOrderTicketFullExport] TO [badlandsdb]
GO

IF EXISTS(SELECT 1 FROM sys.database_principals WHERE [name] = N'badlandsdb')
	GRANT EXECUTE ON OBJECT::[dbo].[fnGaugeQtrInches] TO [badlandsdb]
GO

UPDATE tblSetting SET Value = '1.1.3' WHERE ID = 0

COMMIT TRANSACTION
GO

