--select * from tblsetting where id = 0
/*
	-- add Truck | Trailer . TireSize varchar(30) NULL fields & supporting logic changes
	-- updates to viewSunocoSundex + added support function: fnTrimSealValue
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.1.7'
SELECT  @NewVersion = '3.1.8'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

ALTER TABLE tblTruck ADD TireSize varchar(30)
GO
ALTER TABLE tblTrailer ADD TireSize varchar(30)
GO

EXEC _spRebuildAllObjects
GO

CREATE FUNCTION fnTrimSealValue(@sealValue varchar(10), @defValue varchar(10)) RETURNS varchar(10) AS
BEGIN
	DECLARE @ret varchar(10)
	SET @ret = CASE UPPER(RTRIM(@sealValue)) WHEN 'NA' THEN '' WHEN 'N/A' THEN '' ELSE @sealValue END
	SET @ret = isnull(nullif(@ret, ''), @defValue)
	RETURN (@ret)
END
GO
GRANT EXECUTE ON dbo.fnTrimSealValue TO dispatchcrude_iis_acct
GO

/*****************************************************************************************/
-- Author: Kevin Alons
-- Date Created: 27 Feb 2014
-- Purpose: return export final orders in the SunocoSundex
-- TODO: this has some hardcoded values for the SUNOCO customer (shipper), need to make dynamic
/*****************************************************************************************/
ALTER VIEW [dbo].[viewSunocoSundex] AS
SELECT
	_ID = O.ID
	, _CustomerID = O.CustomerID
	, _OrderNum = O.OrderNum
	, Request_Code = CASE WHEN EP.IsNew = 1 THEN 'A' ELSE 'C' END
	, Company_Code = '0007020233'
	, Ticket_Type = UPPER(left(O.TicketType, 1))
	, Ticket_Source_Code = 'BATA'
	, Ticket_Number = isnull(T.CarrierTicketNum, ltrim(O.OrderNum) + 'X')
	, Ticket_Date = dbo.fnDateMMddYYYY(O.OrderDate)
	, SXL_Property_Code = isnull(O.LeaseNum, '')
	, TP_Property_Code = ''
	, Lease_Company_Name = O.Origin
	, Destination = isnull(CDC.Code, '')
	, Tank_Meter_Number = coalesce(T.OriginTankText, (SELECT min(TankNum) FROM tblOriginTank WHERE DeleteDateUTC IS NULL AND OriginID = O.OriginID), '1')
	, Open_Date = dbo.fnDateMMddYYYY(O.OriginArriveTime)
	, Open_Time = dbo.fnTimeOnly(O.OriginArriveTime)
	, Close_Date = dbo.fnDateMMddYYYY(O.OriginDepartTime)
	, Close_Time = dbo.fnTimeOnly(O.OriginDepartTime)
	, Estimated_Volume = cast(ROUND(T.GrossUnits, 2) as decimal(18, 2))
	, Gross_Volume = cast(ROUND(T.GrossUnits, 2) as decimal(18, 2))
	, Net_Volume = cast(ROUND(T.NetUnits, 2) as decimal(18, 2))
	, Observed_Gravity = isnull(ltrim(T.ProductObsGravity), '')
	, Observed_Temperature = isnull(ltrim(T.ProductObsTemp), '')
	, Observed_BSW = isnull(ltrim(T.ProductBSW), '')
	, Corrected_Gravity_API = 0
	, Purchaser = 'Sonoco Logistics'
	, First_Reading_Gauge_Ft = isnull(ltrim(T.OpeningGaugeFeet), '')
	, First_Reading_Gauge_In = isnull(ltrim(T.OpeningGaugeInch), '')
	, First_Reading_Gauge_Nu = isnull(ltrim(T.OpeningGaugeQ), '')
	, First_Reading_Gauge_De = 4
	, First_Temperature = isnull(ltrim(T.ProductHighTemp), '')
	, First_Bottom_Ft = isnull(ltrim(T.BottomFeet), '')
	, First_Bottom_In = isnull(ltrim(T.BottomInches), '')
	, First_Bottom_Nu = isnull(ltrim(T.BottomQ), '')
	, First_Bottom_De = 4
	, Second_Reading_Gauge_Ft = isnull(ltrim(T.ClosingGaugeFeet), '')
	, Second_Reading_Gauge_In = isnull(ltrim(T.ClosingGaugeInch), '')
	, Second_Reading_Gauge_Nu = isnull(ltrim(T.ClosingGaugeQ), '')
	, Second_Reading_Gauge_De = 4
	, Second_Temperature = isnull(ltrim(T.ProductLowTemp), '')
	, Second_Bottom_Ft = 0
	, Second_Bottom_In = 0
	, Second_Bottom_Nu = 0
	, Second_Bottom_De = 4
	, Shrinkage_Incrustation_Factor = 1
	, First_Reading_Meter = 0
	, Second_Reading_Meter = 0
	, Meter_Factor = 0
	, Temp_Comp_Meter = ''
	, Avg_Line_Temp = ''
	, Truck_ID = ''
	, Trailer_ID = ''
	, Driver_ID =''
	, Miles = ''
	, County = ''
	, State = ''
	, Invoice_Number = ''
	, Invoice_Date = ''
	, Remarks= ''
	, API_Compliant_Chapter = ''
	, Use_SXL_Calculation = 'Y'
	, Seal_On = dbo.fnTrimSealValue(T.SealOn, '0')
	, Seal_Off = dbo.fnTrimSealValue(T.SealOff, '0')
	, Ticket_Exclusion_Cd = CASE WHEN isnull(T.Rejected, O.Rejected) = 1 THEN 'RF' ELSE '' END
	, Confirmation_Number = O.DispatchConfirmNum
	, Split_Flag = CASE WHEN (SELECT COUNT(*) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND DeleteDateUTC IS NULL) > 1 THEN 'Y' ELSE 'N' END
	, Paired_Ticket_Number = isnull((SELECT min(CarrierTicketNum) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND DeleteDateUTC IS NULL AND OT.CarrierTicketNum <> T.CarrierTicketNum), '')
	, Bobtail_Flag = 'N'
	, Ticket_Extra_Info_Flag = CASE WHEN T.Rejected = 1 THEN T.RejectNum ELSE '' END
FROM viewOrderLocalDates O
JOIN dbo.viewOrderCustomerFinalExportPending EP ON EP.ID = O.ID
LEFT JOIN viewOrderTicket T ON T.OrderID = O.ID AND T.DeleteDateUTC IS NULL
LEFT JOIN dbo.tblCustomerDestinationCode CDC ON CDC.DestinationID = O.DestinationID

GO

COMMIT
SET NOEXEC OFF