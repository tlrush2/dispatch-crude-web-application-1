-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.11.15.1'
SELECT  @NewVersion = '3.11.15.2'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1273: Fix driver group filter on driver group assignment page.'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/***********************************/
-- Date Created: 26 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Drivers table records with "friendly" translated values included
-- Changes:  3.10.7 - 2016/01/27 - BB - Added TabletID and PrinterSerial from tblDriver_Sync
--			 3.11.15.2 - 2016/04/20 - BB - Added DriverGroupName to fix filtering on web page
/***********************************/
ALTER VIEW [dbo].[viewDriverBase] AS
SELECT X.*
	, CASE WHEN Active = 1 THEN '' ELSE 'Deleted: ' END + FullName AS FullNameD
FROM (
	SELECT D.*
	, Active = cast(CASE WHEN isnull(C.DeleteDateUTC, D.DeleteDateUTC) IS NULL THEN 1 ELSE 0 END as bit) 
	, FullName = D.FirstName + ' ' + D.LastName 
	, FullNameLF = D.LastName + ', ' + D.FirstName 
	, CarrierType= isnull(CT.Name, 'Unknown') 
	, Carrier = C.Name 
	, StateAbbrev = S.Abbreviation 
	, Truck = T.FullName
	, Trailer = T1.FullName 
	, Trailer2 = T2.FullName 
	, Region = R.Name 
	, DS.MobileAppVersion
	, DS.TabletID
	, DS.PrinterSerial
	, DriverGroupName = DG.Name
	FROM dbo.tblDriver D 
	JOIN dbo.tblCarrier C ON C.ID = D.CarrierID 
	LEFT JOIN dbo.tblDriver_Sync DS ON DS.DriverID = D.ID
	LEFT JOIN dbo.tblCarrierType CT ON CT.ID = C.CarrierTypeID
	LEFT JOIN tblState S ON S.ID = D.StateID
	LEFT JOIN viewTruck T ON T.ID = D.TruckID
	LEFT JOIN viewTrailer T1 ON T1.ID = D.TrailerID
	LEFT JOIN viewTrailer T2 ON T2.ID = D.Trailer2ID
	LEFT JOIN tblRegion R ON R.ID = D.RegionID
	LEFT JOIN tblDriverGroup DG ON DG.ID = D.DriverGroupID
) X
GO

EXEC sp_refreshview viewDriverBase
GO
EXEC sp_refreshview viewDriver
GO



COMMIT 
SET NOEXEC OFF