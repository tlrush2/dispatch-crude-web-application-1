/*
	-- more Report Center functionality - fix some fields in the tblReportColumnDefinition
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.0.4'
SELECT  @NewVersion = '3.0.5'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

UPDATE tblReportColumnDefinition SET DataField = 'T_OpeningGaugeInch' WHERE ID = 93
GO
UPDATE tblReportColumnDefinition SET DataField = 'T_ClosingGaugeInch' WHERE ID = 96
GO

COMMIT
SET NOEXEC OFF