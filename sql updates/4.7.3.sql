SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.7.2.2'
SELECT  @NewVersion = '4.7.3'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Override Route Rate page performance enhancement'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

CREATE NONCLUSTERED INDEX idxOrderSettlementCarrier_Route_Batch_Date ON tblOrderSettlementCarrier (RouteRateID, BatchID) INCLUDE (OrderDate)
GO
CREATE NONCLUSTERED INDEX idxOrderSettlementDriver_Route_Batch_Date ON tblOrderSettlementDriver (RouteRateID, BatchID) INCLUDE (OrderDate)
GO
CREATE NONCLUSTERED INDEX idxOrderSettlementShipper_Route_Batch_Date ON tblOrderSettlementShipper (RouteRateID, BatchID) INCLUDE (OrderDate)
GO

COMMIT
SET NOEXEC OFF