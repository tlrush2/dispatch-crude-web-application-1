-- backup database [dispatchcrude.dev] to disk = 'C:\files\Consulting\cts\dc_backups\dispatchcrude.dev.3.7.45.bak'
-- restore database [DispatchCrude.Dev] from disk = 'C:\files\Consulting\cts\dc_backups\dispatchcrude.dev.3.7.45.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.7.45'
SELECT  @NewVersion = '3.8.1'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'BOL Printing: move BOL header image from Shipper Table Mx to separate "Best Match" table'
	UNION SELECT @NewVersion, 1, 'Driver App: split Print Templates into 3 parts + use Shipper | TicketType | Producer Best Match logic'
	UNION SELECT @NewVersion, 1, 'Gauger App: split Print Templates into 2 parts + use Shipper | TicketType | Producer Best Match logic'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

CREATE TABLE tblDriverAppPrintHeaderImage
(
  ID int identity(1, 1) not null constraint PK_DriverAppPrintHeaderImage PRIMARY KEY NONCLUSTERED
, TicketTypeID int null constraint FK_DriverAppPrintHeaderImage_TicketType foreign key references tblTicketType(ID)
, ShipperID int null constraint FK_DriverAppPrintHeaderImage_Shipper foreign key references tblCustomer(ID)
, ProductGroupID int null constraint FK_DriverAppPrintHeaderImage_ProductGroup foreign key references tblProductGroup(ID)
, ProducerID int null constraint FK_DriverAppPrintHeaderImage_Producer foreign key references tblProducer(ID)
, ImageBlob varbinary(max) not null
, ImageFileName varchar(255) null
, ImageLeft int NOT NULL constraint DF_DriverAppPrintHeaderImage_PrintHeaderImageLeft default (0)
, ImageTop int NOT NULL constraint DF_DriverAppPrintHeaderImage_PrintHeaderImageTop default (0)
, ImageWidth int NOT NULL constraint DF_DriverAppPrintHeaderImage_PrintHeaderImageWidth default (576) 
, ImageHeight int NOT NULL constraint DF_DriverAppPrintHeaderImage_PrintHeaderImageHeight default (300)
, CreateDateUTC smalldatetime NULL constraint DF_DriverAppPrintHeaderImage_CreateDateUTC default (getutcdate())
, CreatedByUser varchar(100) NOT NULL constraint DF_DriverAppPrintHeaderImage_CreatedByUser default ('N/A')
, LastChangeDateUTC smalldatetime NULL
, LastChangedByUser varchar(100) NULL
)
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblDriverAppPrintHeaderImage TO role_iis_acct
GO 
CREATE UNIQUE CLUSTERED INDEX udxDriverAppPrintHeaderImage_Main ON tblDriverAppPrintHeaderImage(TicketTypeID, ShipperID, ProductGroupID, ProducerID) 
GO

CREATE TABLE tblDriverAppPrintPickupTemplate
(
  ID int identity(1, 1) not null constraint PK_DriverAppPrintPickupTemplate PRIMARY KEY NONCLUSTERED
, TicketTypeID int null constraint FK_DriverAppPrintPickupTemplate_TicketType foreign key references tblTicketType(ID)
, ShipperID int null constraint FK_DriverAppPrintPickupTemplate_Shipper foreign key references tblCustomer(ID)
, ProductGroupID int null constraint FK_DriverAppPrintPickupTemplate_ProductGroup foreign key references tblProductGroup(ID)
, ProducerID int null constraint FK_DriverAppPrintPickupTemplate_Producer foreign key references tblProducer(ID)
, TemplateText text null
, CreateDateUTC smalldatetime NULL constraint DF_DriverAppPrintPickupTemplate_CreateDateUTC default (getutcdate())
, CreatedByUser varchar(100) NOT NULL constraint DF_DriverAppPrintPickupTemplate_CreatedByUser default ('N/A')
, LastChangeDateUTC smalldatetime NULL
, LastChangedByUser varchar(100) NULL
)
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblDriverAppPrintPickupTemplate TO role_iis_acct
GO
CREATE UNIQUE CLUSTERED INDEX udxDriverAppPrintPickupTemplate_Main ON tblDriverAppPrintPickupTemplate(TicketTypeID, ShipperID, ProductGroupID, ProducerID)
GO

CREATE TABLE tblDriverAppPrintTicketTemplate
(
  ID int identity(1, 1) not null constraint PK_DriverAppPrintTicketTemplate PRIMARY KEY NONCLUSTERED
, TicketTypeID int null constraint FK_DriverAppPrintTicketTemplate_TicketType foreign key references tblTicketType(ID)
, ShipperID int null constraint FK_DriverAppPrintTicketTemplate_Shipper foreign key references tblCustomer(ID)
, ProductGroupID int null constraint FK_DriverAppPrintTicketTemplate_ProductGroup foreign key references tblProductGroup(ID)
, ProducerID int null constraint FK_DriverAppPrintTicketTemplate_Producer foreign key references tblProducer(ID)
, TemplateText text null
, CreateDateUTC smalldatetime NULL constraint DF_DriverAppPrintTicketTemplate_CreateDateUTC default (getutcdate())
, CreatedByUser varchar(100) NOT NULL constraint DF_DriverAppPrintTicketTemplate_CreatedByUser default ('N/A')
, LastChangeDateUTC smalldatetime NULL
, LastChangedByUser varchar(100) NULL
)
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblDriverAppPrintTicketTemplate TO role_iis_acct
GO
CREATE UNIQUE CLUSTERED INDEX udxDriverAppPrintTicketTemplate_Main ON tblDriverAppPrintTicketTemplate(TicketTypeID, ShipperID, ProductGroupID, ProducerID)
GO

CREATE TABLE tblDriverAppPrintDeliverTemplate
(
  ID int identity(1, 1) not null constraint PK_DriverAppPrintDeliverTemplate PRIMARY KEY NONCLUSTERED
, TicketTypeID int null constraint FK_DriverAppPrintDeliverTemplate_TicketType foreign key references tblTicketType(ID)
, ShipperID int null constraint FK_DriverAppPrintDeliverTemplate_Shipper foreign key references tblCustomer(ID)
, ProductGroupID int null constraint FK_DriverAppPrintDeliverTemplate_ProductGroup foreign key references tblProductGroup(ID)
, ProducerID int null constraint FK_DriverAppPrintDeliverTemplate_Producer foreign key references tblProducer(ID)
, TemplateText text null
, CreateDateUTC smalldatetime NOT NULL constraint DF_DriverAppPrintDeliverTemplate_CreateDateUTC default (getutcdate())
, CreatedByUser varchar(100) NOT NULL constraint DF_DriverAppPrintDeliverTemplate_CreatedByUser default ('N/A')
, LastChangeDateUTC smalldatetime NULL
, LastChangedByUser varchar(100) NULL
)
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblDriverAppPrintDeliverTemplate TO role_iis_acct
GO
CREATE UNIQUE NONCLUSTERED INDEX udxDriverAppPrintDeliverTemplate_Main ON tblDriverAppPrintDeliverTemplate(TicketTypeID, ShipperID, ProductGroupID, ProducerID)
GO

/***********************************
-- Date Created: 2015/07/02
-- Author: Kevin Alons
-- Purpose: retrieve and return the matching DriverAppPrintHeaderImage record
-- Changes:
***********************************/
CREATE FUNCTION fnBestMatchDriverAppPrintHeaderImage
(
  @TicketTypeID int
, @ShipperID int
, @ProductGroupID int
, @ProducerID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID
			, Ranking =	dbo.fnRateRanking(@TicketTypeID, R.TicketTypeID, 8, 1)
				  + dbo.fnRateRanking(@ShipperID, R.ShipperID, 4, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 2, 0)
				  + dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 1)
		FROM  dbo.tblDriverAppPrintHeaderImage R
		WHERE coalesce(nullif(@TicketTypeID, 0), R.TicketTypeID, 0) = coalesce(TicketTypeID, nullif(@TicketTypeID, 0), 0)
		  AND coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(ProducerID, nullif(@ProducerID, 0), 0)
	)
	SELECT R.ID, TicketTypeID, ShipperID, ProductGroupID, ProducerID
		, ImageBlob, ImageFileName, ImageLeft, ImageTop, ImageWidth, ImageHeight
		, BestMatch, Ranking
		, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM tblDriverAppPrintHeaderImage R
	JOIN (
		SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE Ranking % 1 = 0.01 * 4  -- ensure some type of match occurred on all criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)
GO
GRANT SELECT ON fnBestMatchDriverAppPrintHeaderImage TO role_iis_acct
GO

/***********************************
-- Date Created: 2015/07/02
-- Author: Kevin Alons
-- Purpose: retrieve and return the matching DriverAppPrintHeaderImage record + friendly translated data
-- Changes:
***********************************/
CREATE FUNCTION fnBestMatchDriverAppPrintHeaderImageDisplay
(
  @TicketTypeID int
, @ShipperID int
, @ProductGroupID int
, @ProducerID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.TicketTypeID, R.ShipperID, R.ProductGroupID, R.ProducerID
		, R.ImageBlob, R.ImageFileName, R.ImageLeft, R.ImageTop, R.ImageWidth, R.ImageHeight
		, TicketType = TT.Name
		, Shipper = SH.Name
		, ProductGroup = PG.Name
		, Producer = P.Name
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
		, Ranking
	FROM dbo.fnBestMatchDriverAppPrintHeaderImage(@TicketTypeID, @ShipperID, @ProductGroupID, @ProducerID, 0) R
	LEFT JOIN tblTicketType TT ON TT.ID = R.TicketTypeID
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblProducer P ON P.ID = R.ProducerID
GO
GRANT SELECT ON fnBestMatchDriverAppPrintHeaderImageDisplay TO role_iis_acct
GO

/***********************************
-- Date Created: 2015/07/02
-- Author: Kevin Alons
-- Purpose: retrieve and return the Best Match DriverAppPrintHeaderImage info for the specified order
-- Changes:
***********************************/
CREATE FUNCTION fnOrderBestMatchDriverAppPrintHeaderImage(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, R.ImageBlob, R.ImageFileName, R.ImageLeft, R.ImageTop, R.ImageWidth, R.ImageHeight, R.LastChangeDateUTC
	FROM dbo.tblOrder O
	JOIN tblProduct PRO ON PRO.ID = O.ProductID
	CROSS APPLY dbo.fnBestMatchDriverAppPrintHeaderImage(O.TicketTypeID, O.CustomerID, PRO.ProductGroupID, O.ProducerID, 1) R
	WHERE O.ID = @ID 
GO
GRANT SELECT ON fnOrderBestMatchDriverAppPrintHeaderImage TO role_iis_acct
GO

/***********************************
-- Date Created: 2015/07/02
-- Author: Kevin Alons
-- Purpose: retrieve and return the matching DriverAppPrintPickupTemplate record
-- Changes:
***********************************/
CREATE FUNCTION fnBestMatchDriverAppPrintPickupTemplate
(
  @TicketTypeID int
, @ShipperID int
, @ProductGroupID int
, @ProducerID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID
			, Ranking =	dbo.fnRateRanking(@TicketTypeID, R.TicketTypeID, 8, 1)
				  + dbo.fnRateRanking(@ShipperID, R.ShipperID, 4, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 2, 0)
				  + dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 1)
		FROM  dbo.tblDriverAppPrintPickupTemplate R
		WHERE coalesce(nullif(@TicketTypeID, 0), R.TicketTypeID, 0) = coalesce(TicketTypeID, nullif(@TicketTypeID, 0), 0)
		  AND coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(ProducerID, nullif(@ProducerID, 0), 0)
	)
	SELECT R.ID, TicketTypeID, ShipperID, ProductGroupID, ProducerID
		, TemplateText
		, BestMatch, Ranking
		, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM tblDriverAppPrintPickupTemplate R
	JOIN (
		SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE Ranking % 1 = 0.01 * 4  -- ensure some type of match occurred on all criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)
GO
GRANT SELECT ON fnBestMatchDriverAppPrintPickupTemplate TO role_iis_acct
GO

/***********************************
-- Date Created: 2015/07/02
-- Author: Kevin Alons
-- Purpose: retrieve and return the matching DriverAppPrintPickupTemplate record + friendly translated data
-- Changes:
***********************************/
CREATE FUNCTION fnBestMatchDriverAppPrintPickupTemplateDisplay
(
  @TicketTypeID int
, @ShipperID int
, @ProductGroupID int
, @ProducerID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.TicketTypeID, R.ShipperID, R.ProductGroupID, R.ProducerID
		, R.TemplateText
		, TicketType = TT.Name
		, Shipper = SH.Name
		, ProductGroup = PG.Name
		, Producer = P.Name
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
		, Ranking
	FROM dbo.fnBestMatchDriverAppPrintPickupTemplate(@TicketTypeID, @ShipperID, @ProductGroupID, @ProducerID, 0) R
	LEFT JOIN tblTicketType TT ON TT.ID = R.TicketTypeID
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblProducer P ON P.ID = R.ProducerID
GO
GRANT SELECT ON fnBestMatchDriverAppPrintPickupTemplateDisplay TO role_iis_acct
GO

/***********************************
-- Date Created: 2015/07/02
-- Author: Kevin Alons
-- Purpose: retrieve and return the Best Match DriverAppPrintPickupTemplate info for the specified order
-- Changes:
***********************************/
CREATE FUNCTION fnOrderBestMatchDriverAppPrintPickupTemplate(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, R.TemplateText, R.LastChangeDateUTC
	FROM dbo.tblOrder O
	JOIN tblProduct PRO ON PRO.ID = O.ProductID
	CROSS APPLY dbo.fnBestMatchDriverAppPrintPickupTemplate(O.TicketTypeID, O.CustomerID, PRO.ProductGroupID, O.ProducerID, 1) R
	WHERE O.ID = @ID 
GO
GRANT SELECT ON fnOrderBestMatchDriverAppPrintPickupTemplate TO role_iis_acct
GO

/***********************************
-- Date Created: 2015/07/02
-- Author: Kevin Alons
-- Purpose: retrieve and return the matching DriverAppPrintTicketTemplate record
-- Changes:
***********************************/
CREATE FUNCTION fnBestMatchDriverAppPrintTicketTemplate
(
  @TicketTypeID int
, @ShipperID int
, @ProductGroupID int
, @ProducerID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID
			, Ranking =	dbo.fnRateRanking(@TicketTypeID, R.TicketTypeID, 8, 1)
				  + dbo.fnRateRanking(@ShipperID, R.ShipperID, 4, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 2, 0)
				  + dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 1)
		FROM  dbo.tblDriverAppPrintTicketTemplate R
		WHERE coalesce(nullif(@TicketTypeID, 0), R.TicketTypeID, 0) = coalesce(TicketTypeID, nullif(@TicketTypeID, 0), 0)
		  AND coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(ProducerID, nullif(@ProducerID, 0), 0)
	)
	SELECT R.ID, TicketTypeID, ShipperID, ProductGroupID, ProducerID
		, TemplateText
		, BestMatch, Ranking
		, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM tblDriverAppPrintTicketTemplate R
	JOIN (
		SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE Ranking % 1 = 0.01 * 4  -- ensure some type of match occurred on all criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)
GO
GRANT SELECT ON fnBestMatchDriverAppPrintTicketTemplate TO role_iis_acct
GO

/***********************************
-- Date Created: 2015/07/02
-- Author: Kevin Alons
-- Purpose: retrieve and return the matching DriverAppPrintTicketTemplate record + friendly translated data
-- Changes:
***********************************/
CREATE FUNCTION fnBestMatchDriverAppPrintTicketTemplateDisplay
(
  @TicketTypeID int
, @ShipperID int
, @ProductGroupID int
, @ProducerID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.TicketTypeID, R.ShipperID, R.ProductGroupID, R.ProducerID
		, R.TemplateText
		, TicketType = TT.Name
		, Shipper = SH.Name
		, ProductGroup = PG.Name
		, Producer = P.Name
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
		, Ranking
	FROM dbo.fnBestMatchDriverAppPrintTicketTemplate(@TicketTypeID, @ShipperID, @ProductGroupID, @ProducerID, 0) R
	LEFT JOIN tblTicketType TT ON TT.ID = R.TicketTypeID
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblProducer P ON P.ID = R.ProducerID
GO
GRANT SELECT ON fnBestMatchDriverAppPrintTicketTemplateDisplay TO role_iis_acct
GO

/***********************************
-- Date Created: 2015/07/02
-- Author: Kevin Alons
-- Purpose: retrieve and return the Best Match DriverAppPrintTicketTemplate info for the specified order
-- Changes:
***********************************/
CREATE FUNCTION fnOrderBestMatchDriverAppPrintTicketTemplate(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, R.TemplateText, R.LastChangeDateUTC
	FROM dbo.tblOrder O
	JOIN tblProduct PRO ON PRO.ID = O.ProductID
	CROSS APPLY dbo.fnBestMatchDriverAppPrintTicketTemplate(O.TicketTypeID, O.CustomerID, PRO.ProductGroupID, O.ProducerID, 1) R
	WHERE O.ID = @ID 
GO
GRANT SELECT ON fnOrderBestMatchDriverAppPrintTicketTemplate TO role_iis_acct
GO

/***********************************
-- Date Created: 2015/07/02
-- Author: Kevin Alons
-- Purpose: retrieve and return the matching DriverAppPrintDeliverTemplate record
-- Changes:
***********************************/
CREATE FUNCTION fnBestMatchDriverAppPrintDeliverTemplate
(
  @TicketTypeID int
, @ShipperID int
, @ProductGroupID int
, @ProducerID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID
			, Ranking =	dbo.fnRateRanking(@TicketTypeID, R.TicketTypeID, 8, 1)
				  + dbo.fnRateRanking(@ShipperID, R.ShipperID, 4, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 2, 0)
				  + dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 1)
		FROM  dbo.tblDriverAppPrintDeliverTemplate R
		WHERE coalesce(nullif(@TicketTypeID, 0), R.TicketTypeID, 0) = coalesce(TicketTypeID, nullif(@TicketTypeID, 0), 0)
		  AND coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(ProducerID, nullif(@ProducerID, 0), 0)
	)
	SELECT R.ID, TicketTypeID, ShipperID, ProductGroupID, ProducerID
		, TemplateText
		, BestMatch, Ranking
		, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM tblDriverAppPrintDeliverTemplate R
	JOIN (
		SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE Ranking % 1 = 0.01 * 4  -- ensure some type of match occurred on all criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)
GO
GRANT SELECT ON fnBestMatchDriverAppPrintDeliverTemplate TO role_iis_acct
GO

/***********************************
-- Date Created: 2015/07/02
-- Author: Kevin Alons
-- Purpose: retrieve and return the matching DriverAppPrintDeliverTemplate record + friendly translated data
-- Changes:
***********************************/
CREATE FUNCTION fnBestMatchDriverAppPrintDeliverTemplateDisplay
(
  @TicketTypeID int
, @ShipperID int
, @ProductGroupID int
, @ProducerID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.TicketTypeID, R.ShipperID, R.ProductGroupID, R.ProducerID
		, R.TemplateText
		, TicketType = TT.Name
		, Shipper = SH.Name
		, ProductGroup = PG.Name
		, Producer = P.Name
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
		, Ranking
	FROM dbo.fnBestMatchDriverAppPrintDeliverTemplate(@TicketTypeID, @ShipperID, @ProductGroupID, @ProducerID, 0) R
	LEFT JOIN tblTicketType TT ON TT.ID = R.TicketTypeID
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblProducer P ON P.ID = R.ProducerID
GO
GRANT SELECT ON fnBestMatchDriverAppPrintDeliverTemplateDisplay TO role_iis_acct
GO

/***********************************
-- Date Created: 2015/07/02
-- Author: Kevin Alons
-- Purpose: retrieve and return the Best Match DriverAppPrintDeliverTemplate info for the specified order
-- Changes:
***********************************/
CREATE FUNCTION fnOrderBestMatchDriverAppPrintDeliverTemplate(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, R.TemplateText, R.LastChangeDateUTC
	FROM dbo.tblOrder O
	JOIN tblProduct PRO ON PRO.ID = O.ProductID
	CROSS APPLY dbo.fnBestMatchDriverAppPrintDeliverTemplate(O.TicketTypeID, O.CustomerID, PRO.ProductGroupID, O.ProducerID, 1) R
	WHERE O.ID = @ID 
GO
GRANT SELECT ON fnBestMatchDriverAppPrintDeliverTemplate TO role_iis_acct
GO

CREATE TABLE tblDriverAppPrintHeaderImageSync
(
  OrderID int not null constraint FK_DriverAppPrintHeaderImageSync_Order foreign key references tblOrder(ID) ON DELETE CASCADE
, DriverID int not null constraint FK_DriverAppPrintHeaderImageSync_Driver foreign key references tblDriver(ID) ON DELETE CASCADE
, RecordID int not null constraint FK_DriverAppPrintHeaderImageSync_Record foreign key references tblDriverAppPrintHeaderImage(ID) ON DELETE CASCADE
, constraint PK_DriverAppPrintHeaderImageSync primary key (OrderID, DriverID, RecordID)
)
GRANT SELECT, INSERT, UPDATE, DELETE ON tblDriverAppPrintHeaderImageSync TO role_iis_acct
GO
CREATE TABLE tblDriverAppPrintPickupTemplateSync
(
  OrderID int not null constraint FK_DriverAppPrintPickupTemplateSync_Order foreign key references tblOrder(ID) ON DELETE CASCADE
, DriverID int not null constraint FK_DriverAppPrintPickupTemplateSync_Driver foreign key references tblDriver(ID) ON DELETE CASCADE
, RecordID int not null constraint FK_DriverAppPrintPickupTemplateSync_Record foreign key references tblDriverAppPrintPickupTemplate(ID) ON DELETE CASCADE
, constraint PK_DriverAppPrintPickupTemplateSync primary key (OrderID, DriverID, RecordID)
)
GRANT SELECT, INSERT, UPDATE, DELETE ON tblDriverAppPrintPickupTemplateSync TO role_iis_acct
GO
CREATE TABLE tblDriverAppPrintTicketTemplateSync
(
  OrderID int not null constraint FK_DriverAppPrintTicketTemplateSync_Order foreign key references tblOrder(ID) ON DELETE CASCADE
, DriverID int not null constraint FK_DriverAppPrintTicketTemplateSync_Driver foreign key references tblDriver(ID) ON DELETE CASCADE
, RecordID int not null constraint FK_DriverAppPrintTicketTemplateSync_Record foreign key references tblDriverAppPrintTicketTemplate(ID) ON DELETE CASCADE
, constraint PK_DriverAppPrintTicketTemplateSync primary key (OrderID, DriverID, RecordID)
)
GRANT SELECT, INSERT, UPDATE, DELETE ON tblDriverAppPrintTicketTemplateSync TO role_iis_acct
GO
CREATE TABLE tblDriverAppPrintDeliverTemplateSync
(
  OrderID int not null constraint FK_DriverAppPrintDeliverTemplateSync_Order foreign key references tblOrder(ID) ON DELETE CASCADE
, DriverID int not null constraint FK_DriverAppPrintDeliverTemplateSync_Driver foreign key references tblDriver(ID) ON DELETE CASCADE
, RecordID int not null constraint FK_DriverAppPrintDeliverTemplateSync_Record foreign key references tblDriverAppPrintDeliverTemplate(ID) ON DELETE CASCADE
, constraint PK_DriverAppPrintDeliverTemplateSync primary key (OrderID, DriverID, RecordID)
)
GRANT SELECT, INSERT, UPDATE, DELETE ON tblDriverAppPrintDeliverTemplateSync TO role_iis_acct
GO
 
/*******************************************
-- Date Created: 2013/04/25
-- Author: Kevin Alons
-- Purpose: return readonly Order data for Driver App sync
-- Changes:
	- 3.8.1 - 2015/07/02 - KDA - add new split DriverApp printing capability (HeaderImageLeft|Top|Width|Height & Pickup|Ticket|Deliver TemplateText)
*******************************************/
ALTER FUNCTION fnOrderReadOnly_DriverApp(@DriverID int, @LastChangeDateUTC datetime) RETURNS TABLE AS
RETURN 
	WITH cteBase AS
	(
		SELECT O.ID
			, O.OrderNum
			, O.StatusID
			, O.TicketTypeID
			, PriorityNum = cast(P.PriorityNum as int) 
			, Product = PRO.Name
			, O.DueDate
			, Origin = OO.Name
			, OriginFull = OO.FullName
			, OO.OriginType
			, O.OriginUomID
			, OriginStation = OO.Station 
			, OriginLeaseNum = OO.LeaseNum 
			, OriginCounty = OO.County 
			, OriginLegalDescription = OO.LegalDescription 
			, OriginNDIC = OO.NDICFileNum
			, OriginNDM = OO.NDM
			, OriginCA = OO.CA
			, OriginState = OO.State
			, OriginAPI = OO.WellAPI 
			, OriginLat = OO.LAT 
			, OriginLon = OO.LON 
			, OriginGeoFenceRadiusMeters = OO.GeoFenceRadiusMeters 
			, Destination = D.Name
			, DestinationFull = D.FullName
			, DestType = D.DestinationType 
			, O.DestUomID
			, DestLat = D.LAT 
			, DestLon = D.LON 
			, DestGeoFenceRadiusMeters = D.GeoFenceRadiusMeters 
			, DestinationStation = D.Station 
			, O.CreateDateUTC
			, O.CreatedByUser
			, O.LastChangeDateUTC
			, O.LastChangedByUser
			, DeleteDateUTC = isnull(ODAVD.VirtualDeleteDateUTC, O.DeleteDateUTC) 
			, DeletedByUser = isnull(ODAVD.VirtualDeletedByUser, O.DeletedByUser) 
			, O.OriginID
			, O.DestinationID
			, PriorityID = cast(O.PriorityID AS int) 
			, Operator = OO.Operator
			, O.OperatorID
			, Pumper = OO.Pumper
			, O.PumperID
			, Producer = OO.Producer
			, O.ProducerID
			, Customer = C.Name
			, O.CustomerID
			, Carrier = CA.Name
			, O.CarrierID
			, O.ProductID
			, TicketType = OO.TicketType
			, EmergencyInfo = isnull(C.EmergencyInfo, 'For an emergency (spill, leak, fire, or accident) contact CHEMTREC @ 800.424.9300 (toll free)') 
			, DestTicketTypeID = D.TicketTypeID
			, DestTicketType = D.TicketType
			, O.OriginTankNum
			, O.OriginTankID
			, O.DispatchNotes
			, O.DispatchConfirmNum
			, RouteActualMiles = isnull(R.ActualMiles, 0)
			, CarrierAuthority = CA.Authority 
			, OriginTimeZone = OO.TimeZone
			, DestTimeZone = D.TimeZone
			, OCTM.OriginThresholdMinutes
			, OCTM.DestThresholdMinutes
			, ShipperHelpDeskPhone = C.HelpDeskPhone
			, OriginDrivingDirections = OO.DrivingDirections
			, LCD.LCD
			, CustomerLastChangeDateUTC = C.LastChangeDateUTC 
			, CarrierLastChangeDateUTC = CA.LastChangeDateUTC 
			, OriginLastChangeDateUTC = OO.LastChangeDateUTC 
			, DestLastChangeDateUTC = D.LastChangeDateUTC
			, RouteLastChangeDateUTC = R.LastChangeDateUTC
			, DriverID = @DriverID
		FROM dbo.tblOrder O
		JOIN dbo.tblPriority P ON P.ID = O.PriorityID
		JOIN dbo.viewOrigin OO ON OO.ID = O.OriginID
		JOIN dbo.viewDestination D ON D.ID = O.DestinationID
		JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
		JOIN dbo.tblRoute R ON R.ID = O.RouteID
		JOIN dbo.tblCarrier CA ON CA.ID = O.CarrierID
		JOIN dbo.tblProduct PRO ON PRO.ID = O.ProductID
		OUTER APPLY dbo.fnOrderBestMatchDriverAppPrintHeaderImage(O.ID) DAHI
		LEFT JOIN tblOrderDriverAppVirtualDelete ODAVD ON ODAVD.OrderID = O.ID AND ODAVD.DriverID = @DriverID
		CROSS JOIN (SELECT DATEADD(second, -5, @LastChangeDateUTC) AS LCD) LCD
		OUTER APPLY dbo.fnOrderCombinedThresholdMinutes(O.ID) OCTM
		WHERE O.ID IN (
			SELECT id FROM tblOrder WHERE DriverID = @driverID AND StatusID IN (2,7,8,3) 
			UNION 
			SELECT OrderID FROM tblOrderDriverAppVirtualDelete WHERE DriverID = @driverID
		)
	)

	SELECT O.*
		, HeaderImageID = DAHI.ID
		, PrintHeaderBlob = DAHI.ImageBlob
		, HeaderImageLeft = DAHI.ImageLeft
		, HeaderImageTop = DAHI.ImageTop
		, HeaderImageWidth = DAHI.ImageWidth
		, HeaderImageHeight = DAHI.ImageHeight
		, PickupTemplateID = DAPT.ID
		, PickupTemplateText = DAPT.TemplateText
		, TicketTemplateID = DATT.ID
		, TicketTemplateText = DATT.TemplateText
		, DeliverTemplateID = DADT.ID
		, DeliverTemplateText = DADT.TemplateText
	FROM cteBase O
	OUTER APPLY dbo.fnOrderBestMatchDriverAppPrintHeaderImage(O.ID) DAHI
	LEFT JOIN tblDriverAppPrintHeaderImageSync DAHIS ON DAHIS.OrderID = O.ID AND DAHIS.DriverID = O.DriverID AND DAHIS.RecordID <> DAHI.ID
	OUTER APPLY dbo.fnOrderBestMatchDriverAppPrintPickupTemplate(O.ID) DAPT
	LEFT JOIN tblDriverAppPrintPickupTemplateSync DAPTS ON DAPTS.OrderID = O.ID AND DAPTS.DriverID = O.DriverID AND DAPTS.RecordID <> DAPT.ID
	OUTER APPLY dbo.fnOrderBestMatchDriverAppPrintTicketTemplate(O.ID) DATT
	LEFT JOIN tblDriverAppPrintTicketTemplateSync DATTS ON DATTS.OrderID = O.ID AND DATTS.DriverID = O.DriverID AND DATTS.RecordID <> DATT.ID
	OUTER APPLY dbo.fnOrderBestMatchDriverAppPrintDeliverTemplate(O.ID) DADT
	LEFT JOIN tblDriverAppPrintTicketTemplateSync DADTS ON DADTS.OrderID = O.ID AND DADTS.DriverID = O.DriverID AND DADTS.RecordID <> DADT.ID
		WHERE O.ID IN (
		SELECT id FROM tblOrder WHERE DriverID = O.driverID AND StatusID IN (2,7,8,3) 
		UNION 
		SELECT OrderID FROM tblOrderDriverAppVirtualDelete WHERE DriverID = O.driverID
	)
	  AND (
		@LastChangeDateUTC IS NULL 
		OR CreateDateUTC >= LCD
		OR O.LastChangeDateUTC >= LCD
		OR O.DeleteDateUTC >= LCD
		OR CustomerLastChangeDateUTC >= LCD
		OR CarrierLastChangeDateUTC >= LCD
		OR OriginLastChangeDateUTC >= LCD
		OR DestLastChangeDateUTC >= LCD
		OR RouteLastChangeDateUTC >= LCD
		-- if any print related record was changed or a different template/image is now valid
		OR DAHI.LastChangeDateUTC >= LCD
		OR DAHIS.RecordID IS NOT NULL
		OR DAPT.LastChangeDateUTC >= LCD
		OR DAPTS.RecordID IS NOT NULL
		OR DATT.LastChangeDateUTC >= LCD
		OR DATTS.RecordID IS NOT NULL
		OR DADT.LastChangeDateUTC >= LCD
		OR DADTS.RecordID IS NOT NULL
	)
GO

/*******************************************
-- Date Created: 2013/04/25
-- Author: Kevin Alons
-- Purpose: return readonly Order data for Driver App sync (and log the Print XXX records so we can faithfully re-sync them when a change occurrs)
-- Changes:
	- 3.8.1 - 2015/07/02 - KDA - ADDED
*******************************************/
CREATE PROCEDURE spOrderReadOnly_DriverApp(@DriverID int, @LastChangeDateUTC datetime = NULL) AS
BEGIN
	BEGIN TRY	
		BEGIN TRAN

		SELECT * 
		INTO #d 
		FROM dbo.fnOrderReadOnly_DriverApp(@DriverID, @LastChangeDateUTC)
		
		-- forget the last sync records for this driver
		DELETE FROM tblDriverAppPrintHeaderImageSync
		WHERE DriverID = @DriverID 
		  AND OrderID IN (SELECT DISTINCT ID FROM #d)
		DELETE FROM tblDriverAppPrintPickupTemplateSync
		WHERE DriverID = @DriverID 
		  AND OrderID IN (SELECT DISTINCT ID FROM #d)
		DELETE FROM tblDriverAppPrintTicketTemplateSync
		WHERE DriverID = @DriverID 
		  AND OrderID IN (SELECT DISTINCT ID FROM #d)
		DELETE FROM tblDriverAppPrintDeliverTemplateSync
		WHERE DriverID = @DriverID 
		  AND OrderID IN (SELECT DISTINCT ID FROM #d)
		
		-- re-cache the last sync records for the sync orders
		INSERT INTO tblDriverAppPrintHeaderImageSync (OrderID, DriverID, RecordID)
			SELECT DISTINCT ID, @DriverID, HeaderImageID FROM #d WHERE HeaderImageID IS NOT NULL
		INSERT INTO tblDriverAppPrintPickupTemplateSync (OrderID, DriverID, RecordID)
			SELECT DISTINCT ID, @DriverID, PickupTemplateID FROM #d WHERE PickupTemplateID IS NOT NULL
		INSERT INTO tblDriverAppPrintTicketTemplateSync (OrderID, DriverID, RecordID)
			SELECT DISTINCT ID, @DriverID, TicketTemplateID FROM #d WHERE TicketTemplateID IS NOT NULL
		INSERT INTO tblDriverAppPrintDeliverTemplateSync (OrderID, DriverID, RecordID)
			SELECT DISTINCT ID, @DriverID, DeliverTemplateID FROM #d WHERE DeliverTemplateID IS NOT NULL
		
		COMMIT TRAN
		SELECT * FROM #d
	END TRY
	BEGIN CATCH
		DECLARE @msg varchar(255), @severity int
		SELECT @msg = left(ERROR_MESSAGE(), 255), @severity = ERROR_SEVERITY()
		IF @@TRANCOUNT > 0
			ROLLBACK TRAN 
		RAISERROR(@msg, @severity, 1)
	END CATCH
END
GO
GRANT EXECUTE ON spOrderReadOnly_DriverApp TO role_iis_acct
GO

-------------------------------------------

CREATE TABLE tblGaugerAppPrintHeaderImage
(
  ID int identity(1, 1) not null constraint PK_GaugerAppPrintHeaderImage PRIMARY KEY NONCLUSTERED
, TicketTypeID int null constraint FK_GaugerAppPrintHeaderImage_TicketType foreign key references tblTicketType(ID)
, ShipperID int null constraint FK_GaugerAppPrintHeaderImage_Shipper foreign key references tblCustomer(ID)
, ProductGroupID int null constraint FK_GaugerAppPrintHeaderImage_ProductGroup foreign key references tblProductGroup(ID)
, ProducerID int null constraint FK_GaugerAppPrintHeaderImage_Producer foreign key references tblProducer(ID)
, ImageBlob varbinary(max) not null
, ImageFileName varchar(255) null
, ImageLeft int NOT NULL constraint DF_GaugerAppPrintHeaderImage_PrintHeaderImageLeft default (0)
, ImageTop int NOT NULL constraint DF_GaugerAppPrintHeaderImage_PrintHeaderImageTop default (0)
, ImageWidth int NOT NULL constraint DF_GaugerAppPrintHeaderImage_PrintHeaderImageWidth default (576) 
, ImageHeight int NOT NULL constraint DF_GaugerAppPrintHeaderImage_PrintHeaderImageHeight default (300)
, CreateDateUTC smalldatetime NULL constraint DF_GaugerAppPrintHeaderImage_CreateDateUTC default (getutcdate())
, CreatedByUser varchar(100) NOT NULL constraint DF_GaugerAppPrintHeaderImage_CreatedByUser default ('N/A')
, LastChangeDateUTC smalldatetime NULL
, LastChangedByUser varchar(100) NULL
)
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblGaugerAppPrintHeaderImage TO role_iis_acct
GO
CREATE UNIQUE CLUSTERED INDEX udxGaugerAppPrintHeaderImage_Main ON tblGaugerAppPrintHeaderImage(TicketTypeID, ShipperID, ProductGroupID, ProducerID)
GO

CREATE TABLE tblGaugerAppPrintPickupTemplate
(
  ID int identity(1, 1) not null constraint PK_GaugerAppPrintPickupTemplate PRIMARY KEY NONCLUSTERED
, TicketTypeID int null constraint FK_GaugerAppPrintPickupTemplate_TicketType foreign key references tblTicketType(ID)
, ShipperID int null constraint FK_GaugerAppPrintPickupTemplate_Shipper foreign key references tblCustomer(ID)
, ProductGroupID int null constraint FK_GaugerAppPrintPickupTemplate_ProductGroup foreign key references tblProductGroup(ID)
, ProducerID int null constraint FK_GaugerAppPrintPickupTemplate_Producer foreign key references tblProducer(ID)
, TemplateText text null
, CreateDateUTC smalldatetime NULL constraint DF_GaugerAppPrintPickupTemplate_CreateDateUTC default (getutcdate())
, CreatedByUser varchar(100) NOT NULL constraint DF_GaugerAppPrintPickupTemplate_CreatedByUser default ('N/A')
, LastChangeDateUTC smalldatetime NULL
, LastChangedByUser varchar(100) NULL
)
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblGaugerAppPrintPickupTemplate TO role_iis_acct
GO
CREATE UNIQUE CLUSTERED INDEX udxGaugerAppPrintPickupTemplate_Main ON tblGaugerAppPrintPickupTemplate(TicketTypeID, ShipperID, ProductGroupID, ProducerID)
GO

CREATE TABLE tblGaugerAppPrintTicketTemplate
(
  ID int identity(1, 1) not null constraint PK_GaugerAppPrintTicketTemplate PRIMARY KEY NONCLUSTERED
, TicketTypeID int null constraint FK_GaugerAppPrintTicketTemplate_TicketType foreign key references tblTicketType(ID)
, ShipperID int null constraint FK_GaugerAppPrintTicketTemplate_Shipper foreign key references tblCustomer(ID)
, ProductGroupID int null constraint FK_GaugerAppPrintTicketTemplate_ProductGroup foreign key references tblProductGroup(ID)
, ProducerID int null constraint FK_GaugerAppPrintTicketTemplate_Producer foreign key references tblProducer(ID)
, TemplateText text null
, CreateDateUTC smalldatetime NULL constraint DF_GaugerAppPrintTicketTemplate_CreateDateUTC default (getutcdate())
, CreatedByUser varchar(100) NOT NULL constraint DF_GaugerAppPrintTicketTemplate_CreatedByUser default ('N/A')
, LastChangeDateUTC smalldatetime NULL
, LastChangedByUser varchar(100) NULL
)
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblGaugerAppPrintTicketTemplate TO role_iis_acct
GO
CREATE UNIQUE CLUSTERED INDEX udxGaugerAppPrintTicketTemplate_Main ON tblGaugerAppPrintTicketTemplate(TicketTypeID, ShipperID, ProductGroupID, ProducerID)
GO

/***********************************
-- Date Created: 2015/07/02
-- Author: Kevin Alons
-- Purpose: retrieve and return the matching GaugerAppPrintHeaderImage record
-- Changes:
***********************************/
CREATE FUNCTION fnBestMatchGaugerAppPrintHeaderImage
(
  @TicketTypeID int
, @ShipperID int
, @ProductGroupID int
, @ProducerID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID
			, Ranking =	dbo.fnRateRanking(@ShipperID, R.TicketTypeID, 8, 0)
				  + dbo.fnRateRanking(@ShipperID, R.ShipperID, 4, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 2, 0)
				  + dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 1)
		FROM  dbo.tblGaugerAppPrintHeaderImage R
		WHERE coalesce(nullif(@TicketTypeID, 0), R.TicketTypeID, 0) = coalesce(TicketTypeID, nullif(@TicketTypeID, 0), 0)
		  AND coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(ProducerID, nullif(@ProducerID, 0), 0)
	)
	SELECT R.ID, TicketTypeID, ShipperID, ProductGroupID, ProducerID
		, ImageBlob, ImageFileName, ImageLeft, ImageTop, ImageWidth, ImageHeight
		, BestMatch, Ranking
		, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM tblGaugerAppPrintHeaderImage R
	JOIN (
		SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE Ranking % 1 = 0.01 * 4  -- ensure some type of match occurred on all criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)
GO
GRANT SELECT ON fnBestMatchGaugerAppPrintHeaderImage TO role_iis_acct
GO

/***********************************
-- Date Created: 2015/07/02
-- Author: Kevin Alons
-- Purpose: retrieve and return the matching GaugerAppPrintHeaderImage record + friendly translated data
-- Changes:
***********************************/
CREATE FUNCTION fnBestMatchGaugerAppPrintHeaderImageDisplay
(
  @TicketTypeID int
, @ShipperID int
, @ProductGroupID int
, @ProducerID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.TicketTypeID, R.ShipperID, R.ProductGroupID, R.ProducerID
		, R.ImageBlob, R.ImageFileName, R.ImageLeft, R.ImageTop, R.ImageWidth, R.ImageHeight
		, TicketType = TT.Name
		, Shipper = SH.Name
		, ProductGroup = PG.Name
		, Producer = P.Name
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
		, Ranking
	FROM dbo.fnBestMatchGaugerAppPrintHeaderImage(@TicketTypeID, @ShipperID, @ProductGroupID, @ProducerID, 0) R
	LEFT JOIN tblTicketType TT ON TT.ID = R.TicketTypeID
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblProducer P ON P.ID = R.ProducerID
GO
GRANT SELECT ON fnBestMatchGaugerAppPrintHeaderImageDisplay TO role_iis_acct
GO

/***********************************
-- Date Created: 2015/07/02
-- Author: Kevin Alons
-- Purpose: retrieve and return the Best Match GaugerAppPrintHeaderImage info for the specified order
-- Changes:
***********************************/
CREATE FUNCTION fnOrderBestMatchGaugerAppPrintHeaderImage(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, R.ImageBlob, R.ImageFileName, R.ImageLeft, R.ImageTop, R.ImageWidth, R.ImageHeight, R.LastChangeDateUTC
	FROM dbo.tblOrder O
	JOIN tblProduct PRO ON PRO.ID = O.ProductID
	CROSS APPLY dbo.fnBestMatchGaugerAppPrintHeaderImage(O.TicketTypeID, O.CustomerID, PRO.ProductGroupID, O.ProducerID, 1) R
	WHERE O.ID = @ID 
GO
GRANT SELECT ON fnOrderBestMatchGaugerAppPrintHeaderImage TO role_iis_acct
GO

/***********************************
-- Date Created: 2015/07/02
-- Author: Kevin Alons
-- Purpose: retrieve and return the matching GaugerAppPrintPickupTemplate record
-- Changes:
***********************************/
CREATE FUNCTION fnBestMatchGaugerAppPrintPickupTemplate
(
  @TicketTypeID int
, @ShipperID int
, @ProductGroupID int
, @ProducerID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID
			, Ranking =	dbo.fnRateRanking(@ShipperID, R.TicketTypeID, 8, 0)
				  + dbo.fnRateRanking(@ShipperID, R.ShipperID, 4, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 2, 0)
				  + dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 1)
		FROM  dbo.tblGaugerAppPrintPickupTemplate R
		WHERE coalesce(nullif(@TicketTypeID, 0), R.TicketTypeID, 0) = coalesce(TicketTypeID, nullif(@TicketTypeID, 0), 0)
		  AND coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(ProducerID, nullif(@ProducerID, 0), 0)
	)
	SELECT R.ID, TicketTypeID, ShipperID, ProductGroupID, ProducerID
		, TemplateText
		, BestMatch, Ranking
		, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM tblGaugerAppPrintPickupTemplate R
	JOIN (
		SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE Ranking % 1 = 0.01 * 4  -- ensure some type of match occurred on all criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)
GO
GRANT SELECT ON fnBestMatchGaugerAppPrintPickupTemplate TO role_iis_acct
GO

/***********************************
-- Date Created: 2015/07/02
-- Author: Kevin Alons
-- Purpose: retrieve and return the matching GaugerAppPrintPickupTemplate record + friendly translated data
-- Changes:
***********************************/
CREATE FUNCTION fnBestMatchGaugerAppPrintPickupTemplateDisplay
(
  @TicketTypeID int
, @ShipperID int
, @ProductGroupID int
, @ProducerID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.TicketTypeID, R.ShipperID, R.ProductGroupID, R.ProducerID
		, R.TemplateText
		, TicketType = TT.Name
		, Shipper = SH.Name
		, ProductGroup = PG.Name
		, Producer = P.Name
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
		, Ranking
	FROM dbo.fnBestMatchGaugerAppPrintPickupTemplate(@TicketTypeID, @ShipperID, @ProductGroupID, @ProducerID, 0) R
	LEFT JOIN tblTicketType TT ON TT.ID = R.TicketTypeID
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblProducer P ON P.ID = R.ProducerID
GO
GRANT SELECT ON fnBestMatchGaugerAppPrintPickupTemplateDisplay TO role_iis_acct
GO

/***********************************
-- Date Created: 2015/07/02
-- Author: Kevin Alons
-- Purpose: retrieve and return the Best Match GaugerAppPrintPickupTemplate info for the specified order
-- Changes:
***********************************/
CREATE FUNCTION fnOrderBestMatchGaugerAppPrintPickupTemplate(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, R.TemplateText, R.LastChangeDateUTC
	FROM dbo.tblOrder O
	JOIN tblProduct PRO ON PRO.ID = O.ProductID
	CROSS APPLY dbo.fnBestMatchGaugerAppPrintPickupTemplate(O.TicketTypeID, O.CustomerID, PRO.ProductGroupID, O.ProducerID, 1) R
	WHERE O.ID = @ID 
GO
GRANT SELECT ON fnOrderBestMatchGaugerAppPrintPickupTemplate TO role_iis_acct
GO

/***********************************
-- Date Created: 2015/07/02
-- Author: Kevin Alons
-- Purpose: retrieve and return the matching GaugerAppPrintTicketTemplate record
-- Changes:
***********************************/
CREATE FUNCTION fnBestMatchGaugerAppPrintTicketTemplate
(
  @TicketTypeID int
, @ShipperID int
, @ProductGroupID int
, @ProducerID int
, @BestMatchOnly bit = 0
)
RETURNS TABLE AS RETURN
(
	WITH cte AS
	(
		SELECT ID
			, Ranking =	dbo.fnRateRanking(@ShipperID, R.TicketTypeID, 8, 1)
				  + dbo.fnRateRanking(@ShipperID, R.ShipperID, 4, 0)
				  + dbo.fnRateRanking(@ProductGroupID, R.ProductGroupID, 2, 0)
				  + dbo.fnRateRanking(@ProducerID, R.ProducerID, 1, 1)
		FROM  dbo.tblGaugerAppPrintTicketTemplate R
		WHERE coalesce(nullif(@TicketTypeID, 0), R.TicketTypeID, 0) = coalesce(TicketTypeID, nullif(@TicketTypeID, 0), 0)
		  AND coalesce(nullif(@ShipperID, 0), R.ShipperID, 0) = coalesce(ShipperID, nullif(@ShipperID, 0), 0)
		  AND coalesce(nullif(@ProductGroupID, 0), R.ProductGroupID, 0) = coalesce(ProductGroupID, nullif(@ProductGroupID, 0), 0)
		  AND coalesce(nullif(@ProducerID, 0), R.ProducerID, 0) = coalesce(ProducerID, nullif(@ProducerID, 0), 0)
	)
	SELECT R.ID, TicketTypeID, ShipperID, ProductGroupID, ProducerID
		, TemplateText
		, BestMatch, Ranking
		, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser 
	FROM tblGaugerAppPrintTicketTemplate R
	JOIN (
		SELECT ID, S.Ranking, BestMatch = cast(CASE WHEN X.Ranking IS NULL THEN 0 ELSE 1 END as bit)
		FROM cte S
		LEFT JOIN (
			SELECT Ranking = MAX(Ranking)
			FROM cte
			WHERE Ranking % 1 = 0.01 * 4  -- ensure some type of match occurred on all criteria choices
		) X ON X.Ranking = S.Ranking
	) X ON X.ID = R.ID
	WHERE (@BestMatchOnly = 0 OR X.BestMatch = 1)
)
GO
GRANT SELECT ON fnBestMatchGaugerAppPrintTicketTemplate TO role_iis_acct
GO

/***********************************
-- Date Created: 2015/07/02
-- Author: Kevin Alons
-- Purpose: retrieve and return the matching GaugerAppPrintTicketTemplate record + friendly translated data
-- Changes:
***********************************/
CREATE FUNCTION fnBestMatchGaugerAppPrintTicketTemplateDisplay
(
  @TicketTypeID int
, @ShipperID int
, @ProductGroupID int
, @ProducerID int
)
RETURNS TABLE AS RETURN
	SELECT TOP 100 PERCENT R.ID, R.TicketTypeID, R.ShipperID, R.ProductGroupID, R.ProducerID
		, R.TemplateText
		, TicketType = TT.Name
		, Shipper = SH.Name
		, ProductGroup = PG.Name
		, Producer = P.Name
		, R.CreateDateUTC, R.CreatedByUser
		, R.LastChangeDateUTC, R.LastChangedByUser
		, BestMatch
		, Ranking
	FROM dbo.fnBestMatchGaugerAppPrintTicketTemplate(@TicketTypeID, @ShipperID, @ProductGroupID, @ProducerID, 0) R
	LEFT JOIN tblTicketType TT ON TT.ID = R.TicketTypeID
	LEFT JOIN tblCustomer SH ON SH.ID = R.ShipperID
	LEFT JOIN tblProductGroup PG ON PG.ID = R.ProductGroupID
	LEFT JOIN tblProducer P ON P.ID = R.ProducerID
GO
GRANT SELECT ON fnBestMatchGaugerAppPrintTicketTemplateDisplay TO role_iis_acct
GO

/***********************************
-- Date Created: 2015/07/02
-- Author: Kevin Alons
-- Purpose: retrieve and return the Best Match GaugerAppPrintTicketTemplate info for the specified order
-- Changes:
***********************************/
CREATE FUNCTION fnOrderBestMatchGaugerAppPrintTicketTemplate(@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, R.TemplateText, R.LastChangeDateUTC
	FROM dbo.tblOrder O
	JOIN tblProduct PRO ON PRO.ID = O.ProductID
	CROSS APPLY dbo.fnBestMatchGaugerAppPrintTicketTemplate(O.TicketTypeID, O.CustomerID, PRO.ProductGroupID, O.ProducerID, 1) R
	WHERE O.ID = @ID 
GO
GRANT SELECT ON fnOrderBestMatchGaugerAppPrintTicketTemplate TO role_iis_acct
GO

CREATE TABLE tblGaugerAppPrintHeaderImageSync
(
  OrderID int not null constraint FK_GaugerAppPrintHeaderImageSync_Order foreign key references tblOrder(ID) ON DELETE CASCADE
, GaugerID int not null constraint FK_GaugerAppPrintHeaderImageSync_Gauger foreign key references tblGauger(ID) ON DELETE CASCADE
, RecordID int not null constraint FK_GaugerAppPrintHeaderImageSync_Record foreign key references tblGaugerAppPrintHeaderImage(ID) ON DELETE CASCADE
, constraint PK_GaugerAppPrintHeaderImageSync primary key (OrderID, GaugerID, RecordID)
)
GRANT SELECT, INSERT, UPDATE, DELETE ON tblGaugerAppPrintHeaderImageSync TO role_iis_acct
GO
CREATE TABLE tblGaugerAppPrintPickupTemplateSync
(
  OrderID int not null constraint FK_GaugerAppPrintPickupTemplateSync_Order foreign key references tblOrder(ID) ON DELETE CASCADE
, GaugerID int not null constraint FK_GaugerAppPrintPickupTemplateSync_Gauger foreign key references tblGauger(ID) ON DELETE CASCADE
, RecordID int not null constraint FK_GaugerAppPrintPickupTemplateSync_Record foreign key references tblGaugerAppPrintPickupTemplate(ID) ON DELETE CASCADE
, constraint PK_GaugerAppPrintPickupTemplateSync primary key (OrderID, GaugerID, RecordID)
)
GRANT SELECT, INSERT, UPDATE, DELETE ON tblGaugerAppPrintPickupTemplateSync TO role_iis_acct
GO
CREATE TABLE tblGaugerAppPrintTicketTemplateSync
(
  OrderID int not null constraint FK_GaugerAppPrintTicketTemplateSync_Order foreign key references tblOrder(ID) ON DELETE CASCADE
, GaugerID int not null constraint FK_GaugerAppPrintTicketTemplateSync_Gauger foreign key references tblGauger(ID) ON DELETE CASCADE
, RecordID int not null constraint FK_GaugerAppPrintTicketTemplateSync_Record foreign key references tblGaugerAppPrintTicketTemplate(ID) ON DELETE CASCADE
, constraint PK_GaugerAppPrintTicketTemplateSync primary key (OrderID, GaugerID, RecordID)
)
GRANT SELECT, INSERT, UPDATE, DELETE ON tblGaugerAppPrintTicketTemplateSync TO role_iis_acct
GO

/*******************************************/
-- Date Created: 13 Apr 2015
-- Author: Kevin Alons
-- Purpose: return readonly Order data for Gauger App sync
/*******************************************/
ALTER FUNCTION fnOrderReadOnly_GaugerApp(@GaugerID int, @LastChangeDateUTC datetime) RETURNS TABLE AS
RETURN 
	WITH cteBase AS
	(
		SELECT O.ID
			, O.OrderNum
			, GAO.StatusID
			, GAO.TicketTypeID
			, GAO.GaugerID
			, PriorityNum = cast(P.PriorityNum as int) 
			, Product = PRO.Name
			, GAO.DueDate
			, Origin = OO.Name
			, OriginFull = OO.FullName
			, OO.OriginType
			, O.OriginUomID
			, OriginStation = OO.Station 
			, OriginLeaseNum = OO.LeaseNum 
			, OriginCounty = OO.County 
			, OriginLegalDescription = OO.LegalDescription 
			, OriginNDIC = OO.NDICFileNum
			, OriginNDM = OO.NDM
			, OriginCA = OO.CA
			, OriginState = OO.State
			, OriginAPI = OO.WellAPI
			, OriginLat = OO.LAT 
			, OriginLon = OO.LON 
			, OriginGeoFenceRadiusMeters = OO.GeoFenceRadiusMeters 
			, O.CreateDateUTC
			, O.CreatedByUser
			, O.LastChangeDateUTC
			, O.LastChangedByUser
			, DeleteDateUTC = isnull(GOVD.VirtualDeleteDateUTC, O.DeleteDateUTC) 
			, DeletedByUser = isnull(GOVD.VirtualDeletedByUser, O.DeletedByUser) 
			, O.OriginID
			, PriorityID = cast(O.PriorityID AS int) 
			, OO.Operator
			, O.OperatorID
			, OO.Pumper
			, O.PumperID
			, OO.Producer
			, O.ProducerID
			, OO.Customer
			, O.CustomerID
			, O.ProductID
			, TicketType = GTT.Name
			, EmergencyInfo = isnull(S.EmergencyInfo, 'For an emergency (spill, leak, fire, or accident) contact CHEMTREC @ 800.424.9300 (toll free)') 
			, O.OriginTankID
			, O.OriginTankNum
			, GAO.DispatchNotes
			, O.CarrierTicketNum
			, O.DispatchConfirmNum
			, OriginTimeZone = OO.TimeZone
			, OCTM.OriginThresholdMinutes
			, ShipperHelpDeskPhone = S.HelpDeskPhone
			, OriginDrivingDirections = OO.DrivingDirections
			, LCD.LCD
			, CustomerLastChangeDateUTC = S.LastChangeDateUTC 
			, OriginLastChangeDateUTC = OO.LastChangeDateUTC 
			, DestLastChangeDateUTC = D.LastChangeDateUTC
			, RouteLastChangeDateUTC = R.LastChangeDateUTC
		FROM dbo.tblOrder O
		JOIN dbo.tblGaugerOrder GAO ON GAO.OrderID = O.ID
		JOIN tblGaugerTicketType GTT ON GTT.ID = GAO.TicketTypeID
		JOIN dbo.tblPriority P ON P.ID = GAO.PriorityID
		JOIN dbo.viewOrigin OO ON OO.ID = O.OriginID
		JOIN dbo.viewDestination D ON D.ID = O.DestinationID
		JOIN dbo.tblCustomer S ON S.ID = O.CustomerID
		JOIN dbo.tblRoute R ON R.ID = O.RouteID
		JOIN tblProduct PRO ON PRO.ID = O.ProductID
		LEFT JOIN tblGaugerOrderVirtualDelete GOVD ON GOVD.OrderID = O.ID AND GOVD.GaugerID = GAO.GaugerID
		CROSS JOIN fnSyncLCDOffset(@LastChangeDateUTC) LCD
		OUTER APPLY dbo.fnOrderCombinedThresholdMinutes(O.ID) OCTM
		WHERE GAO.StatusID NOT IN (1) -- don't include REQUESTED Gauger Order records
		  AND O.ID IN (
			SELECT OrderID FROM tblGaugerOrder WHERE GaugerID = @GaugerID
			UNION 
			SELECT OrderID FROM tblGaugerOrderVirtualDelete WHERE GaugerID = @GaugerID
		)
	)
	SELECT O.* 
		, HeaderImageID = GAHI.ID
		, PrintHeaderBlob = GAHI.ImageBlob
		, HeaderImageLeft = GAHI.ImageLeft
		, HeaderImageTop = GAHI.ImageTop
		, HeaderImageWidth = GAHI.ImageWidth
		, HeaderImageHeight = GAHI.ImageHeight
		, PickupTemplateID = GAPT.ID
		, PickupTemplateText = GAPT.TemplateText
		, TicketTemplateID = GATT.ID
		, TicketTemplateText = GATT.TemplateText
	FROM cteBase O
	OUTER APPLY dbo.fnOrderBestMatchGaugerAppPrintHeaderImage(O.ID) GAHI
	LEFT JOIN tblGaugerAppPrintHeaderImageSync GAHIS ON GAHIS.OrderID = O.ID AND GAHIS.GaugerID = @GaugerID AND GAHIS.RecordID <> GAHI.ID
	OUTER APPLY dbo.fnOrderBestMatchGaugerAppPrintPickupTemplate(O.ID) GAPT
	LEFT JOIN tblGaugerAppPrintPickupTemplateSync GAPTS ON GAPTS.OrderID = O.ID AND GAPTS.GaugerID = @GaugerID AND GAPTS.RecordID <> GAPT.ID
	OUTER APPLY dbo.fnOrderBestMatchGaugerAppPrintTicketTemplate(O.ID) GATT
	LEFT JOIN tblGaugerAppPrintTicketTemplateSync GATTS ON GATTS.OrderID = O.ID AND GATTS.GaugerID = @GaugerID AND GATTS.RecordID <> GATT.ID
	WHERE (@LastChangeDateUTC IS NULL
		OR CreateDateUTC >= LCD
		OR O.LastChangeDateUTC >= LCD
		OR O.LastChangeDateUTC >= LCD
		-- if any print related record was changed or a different template/image is now valid
		OR GAHI.LastChangeDateUTC >= LCD
		OR GAHIS.RecordID IS NOT NULL
		OR GAPT.LastChangeDateUTC >= LCD
		OR GAPTS.RecordID IS NOT NULL
		OR GATT.LastChangeDateUTC >= LCD
		OR GATTS.RecordID IS NOT NULL
	)
GO

/*******************************************
-- Date Created: 2013/04/25
-- Author: Kevin Alons
-- Purpose: return readonly Order data for Gauger App sync (and log the Print XXX records so we can faithfully re-sync them when a change occurrs)
-- Changes:
	- 3.8.1 - 2015/07/02 - KDA - ADDED
*******************************************/
CREATE PROCEDURE spOrderReadOnly_GaugerApp(@GaugerID int, @LastChangeDateUTC datetime = NULL) AS
BEGIN
	BEGIN TRY	
		BEGIN TRAN

		SELECT * 
		INTO #d 
		FROM dbo.fnOrderReadOnly_GaugerApp(@GaugerID, @LastChangeDateUTC)
		
		-- forget the last sync records for this driver
		DELETE FROM tblGaugerAppPrintHeaderImageSync
		WHERE GaugerID = @GaugerID 
		  AND OrderID IN (SELECT DISTINCT ID FROM #d)
		DELETE FROM tblGaugerAppPrintPickupTemplateSync
		WHERE GaugerID = @GaugerID 
		  AND OrderID IN (SELECT DISTINCT ID FROM #d)
		DELETE FROM tblGaugerAppPrintTicketTemplateSync
		WHERE GaugerID = @GaugerID 
		  AND OrderID IN (SELECT DISTINCT ID FROM #d)
		
		-- re-cache the last sync records for the sync orders
		INSERT INTO tblGaugerAppPrintHeaderImageSync (OrderID, GaugerID, RecordID)
			SELECT DISTINCT ID, @GaugerID, HeaderImageID FROM #d WHERE HeaderImageID IS NOT NULL
		INSERT INTO tblGaugerAppPrintPickupTemplateSync (OrderID, GaugerID, RecordID)
			SELECT DISTINCT ID, @GaugerID, PickupTemplateID FROM #d WHERE PickupTemplateID IS NOT NULL
		INSERT INTO tblGaugerAppPrintTicketTemplateSync (OrderID, GaugerID, RecordID)
			SELECT DISTINCT ID, @GaugerID, TicketTemplateID FROM #d WHERE TicketTemplateID IS NOT NULL
		
		COMMIT TRAN
		SELECT * FROM #d
	END TRY
	BEGIN CATCH
		DECLARE @msg varchar(255), @severity int
		SELECT @msg = left(ERROR_MESSAGE(), 255), @severity = ERROR_SEVERITY()
		IF @@TRANCOUNT > 0
			ROLLBACK TRAN 
		RAISERROR(@msg, @severity, 1)
	END CATCH
END
GO
GRANT EXECUTE ON spOrderReadOnly_GaugerApp TO role_iis_acct
GO

/* =============================================
-- Author:		Kevin Alons
-- Create date: 19 Dec 2012
-- Description:	trigger to 
				1) validate any changes, and fail the update if invalid changes are submitted
				2) add a unique, incrementing OrderNum to each new Order (manual Identity column)
				3) recompute wait times (origin|destination based on times provided)
				4) generate route table entry for any newly used origin-destination combination
				5) keep Order consistent with Origin.Producer|Operator|UOM & Dest.UOM then the Origin|Dest change
				6) roll the PrintStatus back to NOTFINALIZED (0) when status is rolled back
				7) when DriverID changes, mark those orders AS DELETED and clone the order with the new DriverID and associated records
				8) update any ticket quantities for open orders when the UOM is changed for the Origin
				9) update the Driver.Truck\Trailer\Trailer2 defaults & related DISPATCHED orders when driver updates them on an order
-REMOVED		10) update the Pickup/DeliverLastChangeDateUTC when Origin/Destination is changed (respectively)
				11) (re) apply Settlement Amounts to orders not yet fully settled when status is changed to DELIVERED
				12) if DBAudit is turned on, save an audit record for this Order change
-- Changes: 
	-- 3.7.4 05/08/15 GSM Added Rack/Bay field to DBAudit logic
	-- 3.7.7 - 15 May 2015 - KDA - REMOVE #10 above, instead update the LastChangeDateUTC whenever an Origin or Destination is changed
	-- 3.7.11 - 18 May 2015 - KDA - generally use GETUTCDATE for all LastChangeDateUTC revisions (instead of related Order.LastChangeDateUTC, etc)
	-- 3.7.12 - 5/19/2015 - KDA - update any existing OrderTicket records when the Order is DISPATCHED to a driver (so the Driver App will ALWAYS receive the existing TICKETS from the GAUGER)
	-- 3.7.23 - 06/05/2015 - GSM - DCDRV-154: Ticket Type following Origin Change
	-- 3.7.23 - 06/05/2015 - GSM - DCWEB-530 - ensure ASSIGNED orders with a driver defined are updated to DISPATCHED
	-- 3.8.1  - 2015/07/04 - KDA - purge any Driver|Gauger App ZPL related SYNC change records when order is AUDITED
-- =============================================*/
ALTER TRIGGER trigOrder_IU ON tblOrder AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @changesFound bit
	
		-- Note: the TRIGGER_NESTLEVEL( OBJECT_ID('trigOrder_IU') ) = 1 statement prevents recursion
	IF (TRIGGER_NESTLEVEL( OBJECT_ID('trigOrder_IU')) = 1 AND TRIGGER_NESTLEVEL( OBJECT_ID('trigOrderTicket_IU')) = 0) BEGIN

		/**********  START OF VALIDATION SECTION ************************/

		IF (UPDATE(OrderNum) 
			OR UPDATE(PriorityID) 
			OR UPDATE(DueDate) 
			OR UPDATE(RouteID) 
			OR UPDATE(OriginID) 
			OR UPDATE(OriginArriveTimeUTC) 
			OR UPDATE(OriginDepartTimeUTC) 
			OR UPDATE(OriginMinutes) 
			OR UPDATE(OriginWaitNotes) 
			OR UPDATE(OriginBOLNum) 
			OR UPDATE(OriginGrossUnits) 
			OR UPDATE(OriginNetUnits) 
			OR UPDATE(DestinationID) 
			OR UPDATE(DestArriveTimeUTC) 
			OR UPDATE(DestDepartTimeUTC) 
			OR UPDATE(DestMinutes) 
			OR UPDATE(DestWaitNotes) 
			OR UPDATE(DestBOLNum) 
			OR UPDATE(DestGrossUnits) 
			OR UPDATE(DestNetUnits) 
			OR UPDATE(CustomerID) 
			OR UPDATE(CarrierID) 
			OR UPDATE(DriverID) 
			OR UPDATE(TruckID)
			OR UPDATE(TrailerID) 
			OR UPDATE(Trailer2ID) 
			OR UPDATE(OperatorID) 
			OR UPDATE(PumperID) 
			OR UPDATE(TicketTypeID) 
			OR UPDATE(Rejected) 
			OR UPDATE(RejectNotes) 
			OR UPDATE(ChainUp) 
			-- allow this to be changed even in audit status
			--OR UPDATE(OriginTruckMileage) 
			OR UPDATE(OriginTankNum) 
			-- allow this to be changed even in audit status
			--OR UPDATE(DestTruckMileage) 
			OR UPDATE(CarrierTicketNum) 
			OR UPDATE(AuditNotes) 
			OR UPDATE(CreateDateUTC) 
			-- it is permissible to change the ActualMiles value on Audited orders up to the point the Order has been Shipper-Settled
			OR (UPDATE(ActualMiles) AND EXISTS (SELECT * FROM deleted i JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = i.ID WHERE OSS.BatchID IS NOT NULL))
			OR UPDATE(ProducerID) 
			OR UPDATE(CreatedByUser) 
			OR UPDATE(LastChangeDateUTC) 
			OR UPDATE(LastChangedByUser) 
			OR UPDATE(DeleteDateUTC) 
			OR UPDATE(DeletedByUser) 
			OR UPDATE(DestProductBSW)
			OR UPDATE(DestProductGravity) 
			OR UPDATE(DestProductTemp) 
			OR UPDATE(ProductID) 
			OR UPDATE(AcceptLastChangeDateUTC) 
			OR UPDATE(PickupLastChangeDateUTC) 
			OR UPDATE(DeliverLastChangeDateUTC) 
			OR UPDATE(OriginUomID) 
			OR UPDATE(DestUomID) 
			OR UPDATE(PickupPrintStatusID) 
			OR UPDATE(DeliverPrintStatusID)
			OR UPDATE(PickupPrintDateUTC) 
			OR UPDATE(DeliverPrintDateUTC) 
			OR UPDATE(OriginTankID) 
			OR UPDATE(OriginGrossStdUnits) 
			OR UPDATE(DispatchConfirmNum) 
			OR UPDATE(DispatchNotes)
			OR UPDATE(PickupDriverNotes)
			OR UPDATE(DeliverDriverNotes)
			OR UPDATE(OriginWaitReasonID)
			OR UPDATE(DestWaitReasonID)
			OR UPDATE(RejectReasonID) 
			OR UPDATE(DestOpenMeterUnits) 
			OR UPDATE(DestCloseMeterUnits)
			OR UPDATE(DestRackBay)
		)
			--OR UPDATE(ReassignKey)) we don't do any work if this value changes so just ignore it
		BEGIN
			-- only allow the StatusID value to be changed on an audited order
			IF EXISTS (SELECT * FROM deleted WHERE StatusID = 4)
			BEGIN
				-- if the "Allow Audited Order Update (temp only)" setting is TRUE then only warn when this occurs
				IF (dbo.fnToBool(dbo.fnSettingValue(35)) = 1)
					PRINT 'AUDITED orders is being modified - please investigate why!'
				ELSE BEGIN
					-- otherwise (normal behavior) - prevent AUDITED orders from being modified (except to UN-AUDIT them)
					RAISERROR('AUDITED orders cannot be modified!', 16, 1)
					IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
					RETURN
				END
			END
		END
		ELSE IF NOT UPDATE(StatusID) -- NO CHANGES DETECTED SO JUST QUIT
			RETURN

		PRINT 'trigOrder_IU FIRED'
					
		-- ensure the Origin and Destinations are both specified unless the Status is:
		--   (Generated, Assigned, Dispatched or Declined)
		IF  EXISTS(SELECT * FROM inserted O WHERE (O.OriginID IS NULL OR O.DestinationID IS NULL) AND O.StatusID NOT IN (-10,1,2,9))
		BEGIN
			RAISERROR('Invalid Order Status when the Origin and/or Or Destination is not specified', 16, 1)
			IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
			RETURN
		END
		
		/**********  END OF VALIDATION SECTION ************************/

		WHILE (SELECT count(*) FROM inserted i JOIN tblOrder O ON O.ID = i.ID WHERE O.OrderNum IS NULL) > 0
		BEGIN
			UPDATE tblOrder 
			  SET OrderNum = isnull((SELECT max(OrderNum) FROM tblOrder), 100000) + 1
				, CreateDateUTC = getutcdate()
			WHERE ID = (SELECT min(O.ID) FROM tblOrder O JOIN inserted i ON i.ID = O.ID WHERE O.OrderNum IS NULL)
		END
		
		-- re-compute the OriginMinutes (in case the website failed to compute it properly)
		IF UPDATE(OriginArriveTimeUTC) OR UPDATE(OriginDepartTimeUTC)
		BEGIN
			UPDATE tblOrder SET OriginMinutes = datediff(minute, i.OriginArriveTimeUTC, i.OriginDepartTimeUTC)
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
		END
		-- re-compute the DestMinutes (in case the website failed to compute it properly)
		IF UPDATE(DestArriveTimeUTC) OR UPDATE(DestDepartTimeUTC)
		BEGIN
			UPDATE tblOrder SET DestMinutes = datediff(minute, i.DestArriveTimeUTC, i.DestDepartTimeUTC)
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
		END
		
		-- ensure missing Routes are created and assigned for the specified Origin/Destination combination
		IF UPDATE(OriginID) OR UPDATE(DestinationID)
		BEGIN
			-- create any missing Route records
			INSERT INTO tblRoute (OriginID, DestinationID, CreateDateUTC, CreatedByUser)
				SELECT i.OriginID, i.DestinationID, GETUTCDATE(), ISNULL(i.LastChangedByUser, i.CreatedByUser)
				FROM inserted i
				LEFT JOIN tblRoute R ON R.OriginID = i.OriginID AND R.DestinationID = i.DestinationID
				WHERE i.OriginID IS NOT NULL AND i.DestinationID IS NOT NULL AND R.ID IS NULL
			
			-- ensure the Order records refer to the correct Route (ID)
			UPDATE tblOrder SET RouteID = R.ID
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
			LEFT JOIN tblRoute R ON R.OriginID = O.OriginID AND R.DestinationID = O.DestinationID
			WHERE O.RouteID IS NULL OR O.RouteID <> R.ID
			
			-- update the ActualMiles from the related Route
			UPDATE tblOrder SET ActualMiles = R.ActualMiles
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
			JOIN tblRoute R ON R.ID = O.RouteID
			WHERE O.ActualMiles <> R.ActualMiles
		END
		
		IF (UPDATE(OriginID))
		BEGIN
			-- update Order.ProducerID/OperatorID/PumperID to match what is assigned to the new Origin
			UPDATE tblOrder 
				SET TicketTypeID = OO.TicketTypeID
					, ProducerID = OO.ProducerID
					, OperatorID = OO.OperatorID
					, PumperID = OO.PumperID
					, LastChangeDateUTC = GETUTCDATE() 
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			WHERE d.OriginID <> O.OriginID

			-- update Order.OriginUomID to match what is assigned to the new Origin (quantities are updated below)
			UPDATE tblOrder SET OriginUomID = OO.UomID
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			WHERE d.OriginID <> O.OriginID AND d.OriginUomID <> OO.UomID
		END
		
		-- keep the DestUomID in sync with the Destination (units are updated below)
		IF (UPDATE(DestinationID))
		BEGIN
			-- update Order.DestUomID to match what is assigned to the new Destination
			UPDATE tblOrder 
			  SET DestUomID = DD.UomID
				, LastChangeDateUTC = GETUTCDATE()
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID
			JOIN tblDestination DD ON DD.ID = O.DestinationID
			WHERE d.DestinationID <> O.DestinationID AND d.DestUomID <> DD.UomID
		END
		
		-- ensure that any set PrintStatusID values are reset to 0 when backing up the general StatusID value
		IF (UPDATE(StatusID))
		BEGIN
			UPDATE tblOrder 
			  SET DeliverPrintStatusID = 0 
				, LastChangeDateUTC = GETUTCDATE()
			  FROM tblOrder O
			  JOIN deleted d ON d.ID = O.ID
			WHERE O.StatusID <> d.StatusID AND O.StatusID IN (-10, 1, 2, 7, 8) -- Generated, Assigned, Dispatched, Accepted, Picked-Up

			UPDATE tblOrder 
			  SET PickupPrintStatusID = 0 
				, LastChangeDateUTC = GETUTCDATE()
			  FROM tblOrder O
			  JOIN deleted d ON d.ID = O.ID
			WHERE O.StatusID <> d.StatusID AND O.StatusID IN (-10, 1, 2, 7) -- Generated, Assigned, Dispatched, Accepted
			
			/* ensure that any orders that are DISPATCHED - any existing orders are TOUCHED so they are syncable to the Driver App */
			UPDATE tblOrderTicket
				SET LastChangeDateUTC = GETUTCDATE()
			WHERE OrderID IN (
				SELECT i.ID 
				FROM inserted i
				JOIN deleted d ON d.ID = i.ID
				WHERE i.StatusID <> d.StatusID AND i.StatusID IN (2) -- DISPATCHED
			)
	END

		-- just updating the tickets here, trigOrderTicket_IU will in turn update this table
		-- ensure any existing OrderTickets are recomputed with the new OriginUOM
		UPDATE tblOrderTicket
		  SET GrossUnits = dbo.fnConvertUOM(GrossUnits, d.OriginUomID, O.OriginUomID)
			, NetUnits = dbo.fnConvertUOM(NetUnits, d.OriginUomID, O.OriginUomID)
			, LastChangeDateUTC = GETUTCDATE()
		FROM tblOrderTicket OT
		JOIN tblOrder O ON O.ID = OT.OrderID
		JOIN deleted d ON d.ID = O.ID
		WHERE O.OriginUomID <> d.OriginUomID
		
		-- recalculate the Destination Unit values if only if the DestUomID changed but the values didn't
		UPDATE tblOrder
		  SET DestGrossUnits = dbo.fnConvertUOM(O.DestGrossUnits, d.DestUomID, O.DestUomID)
			, DestNetUnits = dbo.fnConvertUOM(O.DestNetUnits, d.DestUomID, O.DestUomID)
			, LastChangeDateUTC = GETUTCDATE()
		FROM tblOrder O
		JOIN deleted d ON d.ID = O.ID
		WHERE d.DestUomID <> O.DestUomID 
		  AND d.DestGrossUnits = O.DestGrossUnits
		  AND d.DestNetUnits = O.DestNetUnits

		/* DCWEB-530 - ensure any ASSIGNED orders with a DRIVER assigned is set to DISPATCHED */
		UPDATE tblOrder SET StatusID = 2 /*DISPATCHED*/
		FROM tblOrder O
		JOIN inserted i ON I.ID = O.ID
		WHERE i.StatusID = 1 /*ASSIGNED*/ AND i.CarrierID IS NOT NULL AND i.DriverID IS NOT NULL AND i.DeleteDateUTC IS NULL

		/*************************************************************************************************************/
		/* handle driver changes - "orphan" the order/ticket (with a new ORDERNUM) - and clone it for the new driver */
		IF (UPDATE(DriverID))
		BEGIN
			-- create cloned order/ticket records (these records will be cloned then the original reset to orphaned status)
			SELECT O.*, NewOrderNum = CAST(NULL as int)
			INTO #clone
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID AND d.DriverID IS NOT NULL AND d.DriverID <> isnull(O.DriverID, 0)

			/* generate the next OrderNum values for these new records */
			DECLARE @newOrderNum int; SET @newOrderNum = isnull((SELECT max(OrderNum) FROM tblOrder), 100000) + 1;
			DECLARE @newReassignKey int; SET @newReassignKey = isnull((SELECT max(ReassignKey) FROM tblOrder), 0) + 1;
			WHILE (SELECT count(*) FROM #clone WHERE NewOrderNum IS NULL) > 0
			BEGIN
				UPDATE #clone
				  SET NewOrderNum = @newOrderNum, ReassignKey = isnull(ReassignKey, @newReassignKey)
					, CreateDateUTC = getutcdate()
				WHERE ID = (SELECT min(ID) FROM #clone WHERE NewOrderNum IS NULL)
				-- if the @newReassignKey wasn't used above, then that # will be skipped, but this should not be an issue
				SELECT @newOrderNum = @newOrderNum + 1, @newReassignKey = @newReassignKey + 1
			END
			
			-- reset the existing records as orphaned
			UPDATE tblOrder
				SET DeleteDateUTC = isnull(O.DeleteDateUTC, GETUTCDATE()), DeletedByUser = isnull(O.DeletedByUser, O.LastChangedByUser)
					, CarrierID = d.CarrierID, DriverID = d.DriverID, TruckID = d.TruckID, TrailerID = d.TrailerID, Trailer2ID = d.Trailer2ID
					, DispatchConfirmNum = NULL
					, ReassignKey = C.ReassignKey
			FROM tblOrder O
			JOIN #clone C ON C.ID = O.ID
			JOIN deleted d on D.ID = O.id

			-- create the cloned records (which is essentially the original with a new ID and the Carrrier & default Truck|Trailers of the new driver)
			INSERT INTO tblOrder (OrderNum, StatusID, PriorityID, DueDate, RouteID, OriginID, OriginArriveTimeUTC, OriginDepartTimeUTC, OriginMinutes, OriginWaitNotes, OriginBOLNum, OriginGrossUnits, OriginNetUnits, DestinationID, DestArriveTimeUTC, DestDepartTimeUTC, DestMinutes, DestWaitNotes, DestBOLNum, DestGrossUnits, DestNetUnits, CustomerID, CarrierID, DriverID, TruckID, TrailerID, Trailer2ID, OperatorID, PumperID, TicketTypeID, Rejected, RejectNotes, ChainUp, OriginTruckMileage, OriginTankNum, DestTruckMileage, CarrierTicketNum, AuditNotes, CreateDateUTC, ActualMiles, ProducerID, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser, DestProductBSW, DestProductGravity, DestProductTemp, ProductID, AcceptLastChangeDateUTC, PickupLastChangeDateUTC, DeliverLastChangeDateUTC, OriginUomID, DestUomID, PickupPrintStatusID, DeliverPrintStatusID, PickupPrintDateUTC, DeliverPrintDateUTC, OriginTankID, OriginGrossStdUnits, DispatchNotes, PickupDriverNotes, DeliverDriverNotes, DispatchConfirmNum, OriginWaitReasonID, DestWaitReasonID, RejectReasonID, DestOpenMeterUnits, DestCloseMeterUnits, ReassignKey)
				SELECT NewOrderNum, StatusID, PriorityID, DueDate, RouteID, OriginID, OriginArriveTimeUTC, OriginDepartTimeUTC, OriginMinutes, OriginWaitNotes, OriginBOLNum, OriginGrossUnits, OriginNetUnits, DestinationID, DestArriveTimeUTC, DestDepartTimeUTC, DestMinutes, DestWaitNotes, DestBOLNum, DestGrossUnits, DestNetUnits, CustomerID, D.CarrierID, DriverID, D.TruckID, D.TrailerID, D.Trailer2ID, OperatorID, PumperID, TicketTypeID, Rejected, RejectNotes, ChainUp, OriginTruckMileage, OriginTankNum, DestTruckMileage, CarrierTicketNum, AuditNotes, O.CreateDateUTC, ActualMiles, ProducerID, O.CreatedByUser, GETUTCDATE(), O.LastChangedByUser, O.DeleteDateUTC, O.DeletedByUser, DestProductBSW, DestProductGravity, DestProductTemp, ProductID, AcceptLastChangeDateUTC, PickupLastChangeDateUTC, DeliverLastChangeDateUTC, OriginUomID, DestUomID, PickupPrintStatusID, DeliverPrintStatusID, PickupPrintDateUTC, DeliverPrintDateUTC, OriginTankID, OriginGrossStdUnits, DispatchNotes, PickupDriverNotes, DeliverDriverNotes, DispatchConfirmNum, OriginWaitReasonID, DestWaitReasonID, RejectReasonID, DestOpenMeterUnits, DestCloseMeterUnits, ReassignKey
				FROM #clone O
				LEFT JOIN tblDriver D ON D.ID = O.DriverID
		
			-- cache the affected tickets
			SELECT OT.*, C.NewOrderNum, OrderDeletedByUser = C.LastChangedByUser
			INTO #cloneTicket
			FROM tblOrderTicket OT
			JOIN #clone C ON C.ID = OT.OrderID
			WHERE OT.DeleteDateUTC IS NULL
			
			-- create the cloned Order Ticket records (copy of existing ticket(s) pointed at new "original" Order)
			INSERT INTO tblOrderTicket (OrderID, CarrierTicketNum, TicketTypeID, TankNum, ProductObsGravity, ProductObsTemp, ProductBSW, OpeningGaugeFeet, OpeningGaugeInch, OpeningGaugeQ, ClosingGaugeFeet, ClosingGaugeInch, ClosingGaugeQ
					, GrossUnits, NetUnits, Rejected, RejectNotes, SealOff, SealOn, BOLNum, ProductHighTemp, ProductLowTemp, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser
					, UID, FromMobileApp, OriginTankID, BottomFeet, BottomInches, BottomQ, GrossStdUnits, RejectReasonID, MeterFactor, OpenMeterUnits, CloseMeterUnits)
				SELECT O.ID, CT.CarrierTicketNum, CT.TicketTypeID, CT.TankNum, CT.ProductObsGravity, CT.ProductObsTemp, CT.ProductBSW, CT.OpeningGaugeFeet, CT.OpeningGaugeInch, CT.OpeningGaugeQ, CT.ClosingGaugeFeet, CT.ClosingGaugeInch, CT.ClosingGaugeQ
					, CT.GrossUnits, CT.NetUnits, CT.Rejected, CT.RejectNotes, CT.SealOff, CT.SealOn, CT.BOLNum, CT.ProductHighTemp, CT.ProductLowTemp, CT.CreateDateUTC, CT.CreatedByUser, GETUTCDATE(), CT.LastChangedByUser, CT.DeleteDateUTC, CT.DeletedByUser
					, newid(), CT.FromMobileApp, CT.OriginTankID, CT.BottomFeet, CT.BottomInches, CT.BottomQ, CT.GrossStdUnits, CT.RejectReasonID, CT.MeterFactor, CT.OpenMeterUnits, CT.CloseMeterUnits
				FROM #cloneTicket CT
				JOIN tblOrder O ON O.OrderNum = CT.NewOrderNum
		END
		/*************************************************************************************************************/
		
		/* HANDLE TRUCK | TRAILER | TRAILER2 default changes when the driver updates his Truck | Trailer | Trailer2 on ACCEPTANCE */
		-- TRUCK
		IF (UPDATE(TruckID) AND EXISTS (
			SELECT i.* 
			FROM inserted i 
			JOIN deleted d ON d.ID = i.ID
			  AND i.DriverID = d.DriverID 
			  AND i.TruckID <> d.TruckID
			WHERE i.StatusID IN (7, 8, 3) -- ACCEPTED, PICKEDUP, DELIVERED
			  AND d.StatusID IN (2))) -- DISPATCHED
		BEGIN
			UPDATE tblDriver
			  SET TruckID = i.TruckID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblDriver DR
			JOIN inserted i ON i.DriverID = DR.ID
			JOIN deleted d ON d.DriverID = DR.ID
			WHERE i.TruckID <> d.TruckID
			
			UPDATE tblOrder
			  SET TruckID = i.TruckID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblOrder O
			JOIN inserted i ON i.DriverID = O.DriverID 
			  AND O.ID <> i.ID
			  AND O.StatusID IN (2) -- DISPATCHED
			  AND i.TruckID <> O.TruckID
			  AND O.DeleteDateUTC IS NULL
		END
		-- TRAILER
		IF (UPDATE(TrailerID) AND EXISTS (
			SELECT i.* 
			FROM inserted i 
			JOIN deleted d ON d.ID = i.ID
			  AND i.DriverID = d.DriverID 
			  AND i.TrailerID <> d.TrailerID
			WHERE i.StatusID IN (7, 8, 3) -- ACCEPTED, PICKEDUP, DELIVERED
			  AND d.StatusID IN (2))) -- DISPATCHED
		BEGIN
			UPDATE tblDriver
			  SET TrailerID = i.TrailerID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblDriver DR
			JOIN inserted i ON i.DriverID = DR.ID
			JOIN deleted d ON d.DriverID = DR.ID
			WHERE i.TrailerID <> d.TrailerID
			
			UPDATE tblOrder
			  SET TrailerID = i.TrailerID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblOrder O
			JOIN inserted i ON i.DriverID = O.DriverID 
			  AND O.ID <> i.ID
			  AND O.StatusID IN (2) -- DISPATCHED
			  AND i.TrailerID <> O.TrailerID
			  AND O.DeleteDateUTC IS NULL
		END
		-- TRAILER 2
		IF (UPDATE(Trailer2ID) AND EXISTS (
			SELECT i.* 
			FROM inserted i 
			JOIN deleted d ON d.ID = i.ID
			  AND i.DriverID = d.DriverID 
			  AND isnull(i.Trailer2ID, 0) <> isnull(d.Trailer2ID, 0)
			WHERE i.StatusID IN (7, 8, 3) -- ACCEPTED, PICKEDUP, DELIVERED
			  AND d.StatusID IN (2))) -- DISPATCHED
		BEGIN
			UPDATE tblDriver
			  SET TrailerID = i.TrailerID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblDriver DR
			JOIN inserted i ON i.DriverID = DR.ID
			JOIN deleted d ON d.DriverID = DR.ID
			WHERE isnull(i.Trailer2ID, 0) <> isnull(d.Trailer2ID, 0)
			
			UPDATE tblOrder
			  SET Trailer2ID = i.Trailer2ID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblOrder O
			JOIN inserted i ON i.DriverID = O.DriverID 
			  AND O.ID <> i.ID
			  AND O.StatusID IN (2) -- DISPATCHED
			  AND isnull(i.Trailer2ID, 0) <> isnull(O.Trailer2ID, 0)
			  AND O.DeleteDateUTC IS NULL
		END
		/*************************************************************************************************************/
	
		-- Apply Settlement Rates/Amounts to Order when STATUS is changed to DELIVERED (until it is FINAL settled)
		IF UPDATE(StatusID) OR UPDATE(DeliverPrintStatusID)
		BEGIN
			DECLARE @deliveredIDs TABLE (ID int)
			INSERT INTO @deliveredIDs (ID)
				SELECT i.ID 
				FROM inserted i
				JOIN tblPrintStatus iPS ON iPS.ID = i.DeliverPrintStatusID
				JOIN deleted d ON d.ID = i.ID
				JOIN tblPrintStatus dPS ON dPS.ID = d.DeliverPrintStatusID
				WHERE i.StatusID = 3 AND iPS.IsCompleted = 1 AND i.StatusID + iPS.IsCompleted <> d.StatusID + dPS.IsCompleted
			
			DECLARE @id int
			WHILE EXISTS (SELECT 1 FROM @deliveredIDs)
			BEGIN
				SELECT TOP 1 @id = ID FROM @deliveredIDs
				EXEC spApplyRatesBoth @id, 'System' 
				DELETE FROM @deliveredIDs WHERE ID = @id
			END
		END

		-- purge any DriverApp/Gauger App sync records for any orders that are not in AUDITED status
		IF UPDATE(StatusID) AND EXISTS (SELECT ID FROM inserted WHERE StatusID IN (4))
		BEGIN
			-- DRIVER APP records
			DELETE FROM tblDriverAppPrintHeaderImageSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID IN (4))
			DELETE FROM tblDriverAppPrintPickupTemplateSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID IN (4))
			DELETE FROM tblDriverAppPrintTicketTemplateSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID IN (4))
			DELETE FROM tblDriverAppPrintDeliverTemplateSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID IN (4))
			-- GAUGER APP records
			DELETE FROM tblGaugerAppPrintHeaderImageSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID IN (4))
			DELETE FROM tblGaugerAppPrintPickupTemplateSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID IN (4))
			DELETE FROM tblGaugerAppPrintTicketTemplateSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID IN (4))
		END
				
		--NOTE: we do not update the DestOpenMeterUnits/DestCloseMeterUnits since they don't auto update
		--  (and they simply represent a reading that is assumed to in the DestUomID UOM at entry)
		--  (if they are updated in the future [manually] the will calculate again using the DestUomID at that time)

		IF EXISTS (SELECT * FROM inserted EXCEPT SELECT * FROM deleted)
		BEGIN
			-- optionally add tblOrderDBAudit records
			BEGIN TRY
				IF EXISTS(SELECT * FROM tblSetting WHERE ID = 32 AND (Value LIKE 'true' OR Value LIKE 'yes'))
					INSERT INTO tblOrderDbAudit (DBAuditDate, ID, OrderNum, StatusID, PriorityID, DueDate, RouteID, OriginID, OriginArriveTimeUTC, OriginDepartTimeUTC, OriginMinutes, OriginWaitNotes, OriginBOLNum, OriginGrossUnits, OriginNetUnits, DestinationID, DestArriveTimeUTC, DestDepartTimeUTC, DestMinutes, DestWaitNotes, DestBOLNum, DestGrossUnits, DestNetUnits, CustomerID, CarrierID, DriverID, TruckID, TrailerID, Trailer2ID, OperatorID, PumperID, TicketTypeID, Rejected, RejectNotes, ChainUp, OriginTruckMileage, OriginTankNum, DestTruckMileage, CarrierTicketNum, AuditNotes, CreateDateUTC, ActualMiles, ProducerID, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser, DestProductBSW, DestProductGravity, DestProductTemp, ProductID, AcceptLastChangeDateUTC, PickupLastChangeDateUTC, DeliverLastChangeDateUTC, OriginUomID, DestUomID, PickupPrintStatusID, DeliverPrintStatusID, PickupPrintDateUTC, DeliverPrintDateUTC, OriginTankID, OriginGrossStdUnits, DispatchNotes, PickupDriverNotes, DeliverDriverNotes, DispatchConfirmNum, OriginWaitReasonID, DestWaitReasonID, RejectReasonID, DestOpenMeterUnits, DestCloseMeterUnits, ReassignKey, DestRackBay)
						SELECT GETUTCDATE(), ID, OrderNum, StatusID, PriorityID, DueDate, RouteID, OriginID, OriginArriveTimeUTC, OriginDepartTimeUTC, OriginMinutes, OriginWaitNotes, OriginBOLNum, OriginGrossUnits, OriginNetUnits, DestinationID, DestArriveTimeUTC, DestDepartTimeUTC, DestMinutes, DestWaitNotes, DestBOLNum, DestGrossUnits, DestNetUnits, CustomerID, CarrierID, DriverID, TruckID, TrailerID, Trailer2ID, OperatorID, PumperID, TicketTypeID, Rejected, RejectNotes, ChainUp, OriginTruckMileage, OriginTankNum, DestTruckMileage, CarrierTicketNum, AuditNotes, CreateDateUTC, ActualMiles, ProducerID, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser, DestProductBSW, DestProductGravity, DestProductTemp, ProductID, AcceptLastChangeDateUTC, PickupLastChangeDateUTC, DeliverLastChangeDateUTC, OriginUomID, DestUomID, PickupPrintStatusID, DeliverPrintStatusID, PickupPrintDateUTC, DeliverPrintDateUTC, OriginTankID, OriginGrossStdUnits, DispatchNotes, PickupDriverNotes, DeliverDriverNotes, DispatchConfirmNum, OriginWaitReasonID, DestWaitReasonID, RejectReasonID, DestOpenMeterUnits, DestCloseMeterUnits, ReassignKey, DestRackBay
						FROM deleted d
			END TRY
			BEGIN CATCH
				PRINT 'trigOrder_IU.DBAUDIT FAILURE: ' + ERROR_MESSAGE()
			END CATCH
		END
		
	PRINT 'trigOrder_IU COMPLETE'

	END
	
END


GO
EXEC sp_settriggerorder @triggername=N'[trigOrder_IU]', @order=N'First', @stmttype=N'INSERT'
GO
EXEC sp_settriggerorder @triggername=N'[trigOrder_IU]', @order=N'First', @stmttype=N'UPDATE'
GO

EXEC _spRebuildAllObjects
GO

INSERT INTO tblDriverAppPrintHeaderImage (ShipperID, ImageBlob, ImageFileName, ImageLeft, ImageTop, ImageWidth, ImageHeight)
	SELECT ID, ZPLHeaderBlob, ZPLHeaderFileName, 0, 0, 576, 400 FROM tblCustomer WHERE ZPLHeaderBlob IS NOT NULL
INSERT INTO tblGaugerAppPrintHeaderImage (ShipperID, ImageBlob, ImageFileName, ImageLeft, ImageTop, ImageWidth, ImageHeight)
	SELECT ID, ZPLHeaderBlob, ZPLHeaderFileName, 0, 0, 576, 400 FROM tblCustomer WHERE ZPLHeaderBlob IS NOT NULL

INSERT INTO tblDriverAppPrintPickupTemplate (TemplateText)
	SELECT '^XA
~TA000~JSN^LT0^MNN^MTD^PON^PMN^LH0,0^JMA^PR4,4^MD0^LRN^CI0
^MMT
^PW576
^LL1675
^LS0
^FT277,727^A0N,34,33^FH\^FN134^FD@Orders.DispatchConfirmNum^FS
^FT159,1290^A0N,23,24^FH\^FN109^FD@Orders.OriginAPI^FS
^FT427,1339^A0N,23,24^FH\^FN96^FD@Orders.OriginCA^FS
^FT156,868^A0N,23,24^FH\^FN117^FD@Orders.CarrierAuthority^FS
^FT159,1417^A0N,23,24^FH\^FN128^FD@Orders.OriginWaitNumDesc^FS
^FT427,1313^A0N,23,24^FH\^FN88^FD@Orders.OriginNDM^FS
^FT159,1316^A0N,23,24^FH\^FN11^FD@Orders.OriginState^FS
^FT427,1286^A0N,23,24^FH\^FN87^FD@Orders.OriginNDIC^FS
^FT137,805^A0N,28,28^FH\^FN86^FD@Orders.OriginGrossStdUnits$%,.3f^FS
^FT159,1527^A0N,23,24^FH\^FN129^FD@Orders.RejectNumDesc^FS
^FT156,999^A0N,23,24^FH\^FN26^FD@Orders.ChainUp$%!^FS
^FT156,1026^A0N,23,24^FH\^FN27^FD@Orders.Rejected$%!^FS
^FT159,1645^A0N,23,24^FB408,4,0^FH\^FN141^FD@Orders.PickupDriverNotes^FS
^FT459,804^A0N,23,24^FH\^FN53^FD@Orders.GrossUnitsVariance^FS
^FT459,833^A0N,23,24^FH\^FN54^FD@Orders.NetUnitsVariance^FS
^FT464,975^A0N,23,24^FH\^FN61^FD@Orders.DriverMileage^FS
^FT464,1000^A0N,23,24^FH\^FN62^FD@Orders.OriginTruckMileage^FS
^FT463,1027^A0N,23,24^FH\^FN63^FD@Orders.DestTruckMileage^FS
^FT118,613^A0N,23,24^FH\^FN71^FD@Orders.OriginDepartTimeUTC$%tD^FS
^FT240,773^A0N,28,28^FB74,1,0,R^FH\^FN78^FD@Orders.OriginUomShort^FS
^FT240,836^A0N,28,28^FB74,1,0,R^FH\^FN78^FD@Orders.OriginUomShort^FS
^FT240,805^A0N,28,28^FB74,1,0,R^FH\^FN78^FD@Orders.OriginUomShort^FS
^FT159,1211^A0N,23,24^FH\^FN79^FD@Orders.OriginStation^FS
^FT159,1183^A0N,23,24^FH\^FN125^FD@Orders.Customer^FS
^FT341,1367^A0N,23,24^FH\^FN81^FD@Orders.OriginTimeZone^FS
^FT341,1392^A0N,23,24^FH\^FN81^FD@Orders.OriginTimeZone^FS
^FT432,609^A0N,17,16^FH\^FN83^FD@Print.nowutc()$%tD %tl:%tM %tp^FS
^FT17,565^A0N,23,24^FB540,3,0^FH\^FN139^FD@Orders.EmergencyInfo^FS
^FT137,773^A0N,28,28^FH\^FN84^FD@Orders.OriginGrossUnits$%,.3f^FS
^FT137,836^A0N,28,28^FH\^FN85^FD@Orders.OriginNetUnits$%,.3f^FS
^FT159,1488^A0N,23,24^FB408,3,0^FH\^FN138^FD@Orders.OriginWaitNotes^FS
^FT156,944^A0N,23,24^FH\^FN92^FD@Orders.Trailer^FS
^FT156,972^A0N,23,24^FH\^FN93^FD@Orders.Trailer2^FS
^FT156,919^A0N,23,24^FH\^FN94^FD@Orders.Truck^FS
^FT159,1127^A0N,23,24^FH\^FN121^FD@Orders.Operator^FS
^FT159,1155^A0N,23,24^FH\^FN123^FD@Orders.Producer^FS
^FT159,1553^A0N,23,24^FH\^FN126^FD@Orders.RejectNotes^FS
^FT159,1099^A0N,23,24^FH\^FN122^FD@Orders.Origin^FS
^FT159,1238^A0N,23,24^FH\^FN107^FD@Orders.OriginLeaseNum^FS
^FT6,474^A0N,23,24^FB564,2,0^FH\^FN137^FD@Orders.Product^FS
^FT277,687^A0N,34,33^FH\^FN115^FD@Orders.OrderNum^FS
^FT159,1340^A0N,23,24^FH\^FN108^FD@Orders.OriginCounty^FS
^FT159,1392^A0N,23,24^FH\^FN110^FD@Orders.OriginDepartTimeUTC$%tD %tl:%tM %tp^FS
^FT159,1367^A0N,23,24^FH\^FN111^FD@Orders.OriginArriveTimeUTC$%tD %tl:%tM %tp^FS
^FT156,893^A0N,23,24^FH\^FN114^FD@Orders.DriverName^FS
^FT159,1265^A0N,23,24^FH\^FN118^FD@Orders.OriginLegalDescription^FS
^FT151,649^A0N,39,38^FB274,1,0,C^FH\^FN135^FD@Orders.TicketType^FS
^FT203,1064^A0N,28,28^FB170,1,0,C^FH\^FDPickup Details^FS
^FT351,1027^A0N,23,24^FB108,1,0,R^FH\^FDDEST ODO:^FS
^FT11,1026^A0N,23,24^FB140,1,0,R^FH\^FDREJ. ORIGIN?:^FS
^FT331,1000^A0N,23,24^FB128,1,0,R^FH\^FDORIGIN ODO:^FS
^FT12,836^A0N,28,28^FB120,1,0,R^FH\^FDEST. NSV:^FS
^FT336,975^A0N,23,24^FB123,1,0,R^FH\^FDTRIP MILES:^FS
^FT3,1578^A0N,23,24^FB154,1,0,R^FH\^FDDRIVER NOTES:^FS
^FT67,728^A0N,34,33^FB208,1,0,R^FH\^FDSHIPPER PO #:^FS
^FO358,750^GB0,92,2^FS
^FT41,944^A0N,23,24^FB110,1,0,R^FH\^FDTRAILER 1:^FS
^FT69,1391^A0N,23,24^FB85,1,0,R^FH\^FDDEPART:^FS
^FT25,1265^A0N,23,24^FB129,1,0,R^FH\^FDLEGAL DESC:^FS
^FT42,999^A0N,23,24^FB109,1,0,R^FH\^FDCHAINUP?:^FS
^FT22,1417^A0N,23,24^FB132,1,0,R^FH\^FDWAIT NOTES:^FS
^FT30,1528^A0N,23,24^FB124,1,0,R^FH\^FDREJ. NOTES:^FS
^FT74,1367^A0N,23,24^FB80,1,0,R^FH\^FDARRIVE:^FS
^FO357,747^GB218,0,3^FS
^FT77,919^A0N,23,24^FB74,1,0,R^FH\^FDTRUCK:^FS
^FT406,804^A0N,23,24^FB48,1,0,R^FH\^FDGOV:^FS
^FT85,1316^A0N,23,24^FB69,1,0,R^FH\^FDSTATE:^FS
^FT406,834^A0N,23,24^FB48,1,0,R^FH\^FDNSV:^FS
^FT66,1238^A0N,23,24^FB88,1,0,R^FH\^FDLEASE #:^FS
^FT366,609^A0N,17,16^FB65,1,0,C^FH\^FDPRINTED:^FS
^FT390,1340^A0N,23,24^FB33,1,0,R^FH\^FDCA:^FS
^FT114,1291^A0N,23,24^FB40,1,0,C^FH\^FDAPI:^FS
^FO3,842^GB573,0,3^FS
^FT389,774^A0N,23,24^FB166,1,0,C^FH\^FDDEST VARIANCE:^FS
^FT65,1340^A0N,23,24^FB89,1,0,R^FH\^FDCOUNTY:^FS
^FT369,1313^A0N,23,24^FB54,1,0,R^FH\^FDNDM:^FS
^FT367,1286^A0N,23,24^FB56,1,0,R^FH\^FDNDIC:^FS
^FT76,1100^A0N,23,24^FB78,1,0,R^FH\^FDORIGIN:^FS
^FO4,486^GB568,107,4^FS
^FT29,868^A0N,23,24^FB122,1,0,R^FH\^FDAUTHORITY:^FS
^FT70,893^A0N,23,24^FB81,1,0,R^FH\^FDDRIVER:^FS
^FT9,687^A0N,34,33^FB266,1,0,R^FH\^FDCARRIER ORDER #:^FS
^FO3,1069^GB570,0,8^FS
^FT61,1211^A0N,23,24^FB93,1,0,R^FH\^FDSTATION:^FS
^FT60,1183^A0N,23,24^FB94,1,0,R^FH\^FDSHIPPER:^FS
^FT12,773^A0N,28,28^FB121,1,0,R^FH\^FDEST. GOV:^FS
^FT12,805^A0N,28,28^FB119,1,0,R^FH\^FDEST. GSV:^FS
^FT3,614^A0N,23,24^FB110,1,0,C^FH\^FDRUN DATE:^FS
^FT41,972^A0N,23,24^FB110,1,0,R^FH\^FDTRAILER 2:^FS
^FT41,1128^A0N,23,24^FB113,1,0,R^FH\^FDOPERATOR:^FS
^FT38,1155^A0N,23,24^FB116,1,0,R^FH\^FDPRODUCER:^FS
^FO3,1031^GB570,0,8^FS
^XZ'

INSERT INTO tblDriverAppPrintTicketTemplate (TemplateText)
	SELECT '^XA
~TA000~JSN^LT0^MNN^MTD^PON^PMN^LH0,0^JMA^PR4,4^MD0^LRN^CI0
^MMT
^PW576
^LL0285
^LS0
^FT246,170^A0N,23,24^FH\^FN142^FD@OrderTickets.OpeningGaugeQ^FS
^FT246,193^A0N,23,24^FH\^FN2^FD@OrderTickets.ClosingGaugeQ^FS
^FT327,29^A0N,23,24^FH\^FN3^FD@OrderTickets.Rejected$%!^FS
^FT171,100^A0N,23,24^FB24,1,0,R^FH\^FN10^FD@OrderTickets.BottomInches^FS
^FT170,169^A0N,23,24^FB24,1,0,R^FH\^FN12^FD@OrderTickets.OpeningGaugeFeet^FS
^FT170,193^A0N,23,24^FB24,1,0,R^FH\^FN13^FD@OrderTickets.ClosingGaugeFeet^FS
^FT203,193^A0N,23,24^FB25,1,0,R^FH\^FN14^FD@OrderTickets.ClosingGaugeInch^FS
^FT203,169^A0N,23,24^FB28,1,0,R^FH\^FN23^FD@OrderTickets.OpeningGaugeInch^FS
^FT432,100^A0N,23,24^FH\^FN28^FD@OrderTickets.ProductBSW$%,.3f^FS
^FT451,55^A0N,23,24^FH\^FN32^FD@OrderTickets.TankDescription^FS
^FT170,122^A0N,23,24^FH\^FN36^FD@OrderTickets.ProductObsGravity$%,.2f^FS
^FT432,122^A0N,23,24^FH\^FN38^FD@OrderTickets.Gravity60F$%,.2f^FS
^FT104,29^A0N,23,24^FH\^FN43^FD@OrderTickets.CarrierTicketNum^FS
^FT159,251^A0N,23,24^FH\^FN130^FD@OrderTickets.RejectNumDesc^FS
^FT107,223^A0N,23,24^FH\^FN49^FD@OrderTickets.GrossUnits$%,.3f^FS
^FT492,223^A0N,23,24^FH\^FN50^FD@OrderTickets.NetUnits$%,.3f^FS
^FT297,223^A0N,23,24^FH\^FN52^FD@OrderTickets.GrossStdUnits$%,.3f^FS
^FT433,171^A0N,23,24^FH\^FN64^FD@OrderTickets.MeterFactor^FS
^FT478,145^A0N,23,24^FH\^FN67^FD@OrderTickets.ProductLowTemp$%,.1f^FS
^FT319,145^A0N,23,24^FH\^FN68^FD@OrderTickets.ProductHighTemp$%,.1f^FS
^FT169,145^A0N,23,24^FH\^FN69^FD@OrderTickets.ProductObsTemp$%,.1f^FS
^FT448,29^A0N,23,24^FH\^FN97^FD@OrderTickets.DispatchConfirmNum^FS
^FT168,55^A0N,23,24^FH\^FN89^FD@OrderTickets.OriginTankText^FS
^FT299,55^A0N,23,24^FH\^FN98^FD@Orders.OriginBOLNum^FS
^FT170,78^A0N,23,24^FB120,1,0,R^FH\^FN90^FD@OrderTickets.SealOff^FS
^FT303,78^A0N,23,24^FH\^FN91^FD@OrderTickets.SealOn^FS
^FT159,277^A0N,23,24^FH\^FN127^FD@OrderTickets.RejectNotes^FS
^FT220,100^A0N,28,28^FH\^FDDEMO^FS
^FT255,91^A0N,28,28^FH\^FDDEMO^FS
^FO17,3^GB24,0,3^FS
^FO49,3^GB25,0,3^FS
^FO82,3^GB24,0,3^FS
^FO114,3^GB25,0,3^FS
^FO146,3^GB25,0,3^FS
^FO179,3^GB24,0,3^FS
^FO211,3^GB25,0,3^FS
^FO244,3^GB24,0,3^FS
^FO276,3^GB25,0,3^FS
^FO309,3^GB24,0,3^FS
^FO341,3^GB25,0,3^FS
^FO374,3^GB24,0,3^FS
^FO406,3^GB25,0,3^FS
^FO438,3^GB25,0,3^FS
^FO471,3^GB24,0,3^FS
^FO503,3^GB25,0,3^FS
^FO536,3^GB24,0,3^FS
^FT393,223^A0N,23,24^FB99,1,0,C^FH\^FDEST. NSV:^FS
^FO3,31^GB572,200,2^FS
^FO192,197^GB0,32,2^FS
^FO388,197^GB0,32,2^FS
^FT195,169^A0N,23,24^FH\^FD''^FS
^FT195,194^A0N,23,24^FH\^FD''^FS
^FT292,78^A0N,23,24^FB7,1,0,C^FH\^FD/^FS
^FT7,145^A0N,23,24^FB108,1,0,R^FH\^FDTEMPS (F):^FS
^FT404,145^A0N,23,24^FB71,1,0,R^FH\^FDCLOSE:^FS
^FT291,55^A0N,23,24^FB7,1,0,C^FH\^FD/^FS
^FT194,101^A0N,23,24^FH\^FD"^FS
^FT228,169^A0N,23,24^FH\^FD"^FS
^FT257,145^A0N,23,24^FB61,1,0,R^FH\^FDOPEN:^FS
^FT57,122^A0N,23,24^FB108,1,0,R^FH\^FDOBS GRAV:^FS
^FT228,194^A0N,23,24^FH\^FD"^FS
^FT30,249^A0N,23,24^FB124,1,0,R^FH\^FDREJ. NOTES:^FS
^FT323,122^A0N,23,24^FB104,1,0,R^FH\^FDGRAV 60F:^FS
^FT336,100^A0N,23,24^FB91,1,0,R^FH\^FDBS&W%:^FS
^FT259,170^A0N,23,24^FH\^FDQ^FS
^FT259,194^A0N,23,24^FH\^FDQ^FS
^FT207,29^A0N,23,24^FB118,1,0,C^FH\^FDREJECTED?:^FS
^FT120,145^A0N,23,24^FB47,1,0,R^FH\^FDOBS:^FS
^FO4,196^GB570,0,3^FS
^FT105,169^A0N,23,24^FB62,1,0,R^FH\^FDOPEN:^FS
^FT399,29^A0N,23,24^FB46,1,0,R^FH\^FDPO#:^FS
^FT95,193^A0N,23,24^FB71,1,0,R^FH\^FDCLOSE:^FS
^FT8,78^A0N,23,24^FB157,1,0,R^FH\^FDSEAL OFF/ON #:^FS
^FT342,171^A0N,23,24^FB85,1,0,R^FH\^FDFACTOR:^FS
^FT75,100^A0N,23,24^FB90,1,0,R^FH\^FDBOTTOM:^FS
^FT6,29^A0N,23,24^FB96,1,0,C^FH\^FDTICKET #:^FS
^FT198,223^A0N,23,24^FB98,1,0,C^FH\^FDEST. GSV:^FS
^FT7,223^A0N,23,24^FB100,1,0,C^FH\^FDEST. GOV:^FS
^FT40,55^A0N,23,24^FB125,1,0,R^FH\^FDTANK/BOL #:^FS
^XZ'

INSERT INTO tblDriverAppPrintDeliverTemplate (TemplateText)
	SELECT '^XA
~TA000~JSN^LT0^MNN^MTD^PON^PMN^LH0,0^JMA^PR4,4^MD0^LRN^CI0
^MMT
^PW576
^LL1523
^LS0
^FT181,964^A0N,23,24^FH\^FN114^FD@Orders.DriverName^FS
^FT180,745^A0N,23,24^FH\^FN114^FD@Orders.DriverName^FS
^FT159,505^A0N,23,24^FB408,4,0^FH\^FN143^FD@Orders.DeliverDriverNotes^FS
^FT427,174^A0N,23,24^FH\^FN29^FD@Orders.DestProductBSW$%,.3f^FS
^FT254,148^A0N,23,24^FH\^FN35^FD@Orders.DestUomShort^FS
^FT254,174^A0N,23,24^FH\^FN35^FD@Orders.DestUomShort^FS
^FT159,251^A0N,23,24^FH\^FN37^FD@Orders.DestProductGravity$%,.2f^FS
^FT159,197^A0N,23,24^FH\^FN44^FD@Orders.DestOpenMeterUnits$%,.1f^FS
^FT159,225^A0N,23,24^FH\^FN45^FD@Orders.DestCloseMeterUnits$%,.1f^FS
^FT159,174^A0N,23,24^FH\^FN48^FD@Orders.DestNetUnits$%,.3f^FS
^FT159,148^A0N,23,24^FH\^FN51^FD@Orders.DestGrossUnits$%,.3f^FS
^FT427,148^A0N,23,24^FH\^FN70^FD@Orders.DestProductTemp$%,.1f^FS
^FT160,125^A0N,23,24^FH\^FN80^FD@Orders.DestinationStation^FS
^FT159,392^A0N,23,24^FB408,4,0^FH\^FN140^FD@Orders.DestWaitNotes^FS
^FT346,274^A0N,23,24^FH\^FN82^FD@Orders.DestTimeZone^FS
^FT159,80^A0N,23,24^FH\^FN124^FD@Orders.Destination^FS
^FT346,299^A0N,23,24^FH\^FN82^FD@Orders.DestTimeZone^FS
^FX@Order.DestinationDriverSignature,8,799,576,150^FS
^FX@Order.OriginDriverSignature,8,579,576,150^FS
^FT427,125^A0N,23,24^FH\^FN95^FD@Orders.DestBOLNum^FS
^FT159,274^A0N,23,24^FH\^FN112^FD@Orders.DestArriveTimeUTC$%tD %tl:%tM %tp^FS
^FT159,300^A0N,23,24^FH\^FN113^FD@Orders.DestDepartTimeUTC$%tD %tl:%tM %tp^FS
^FT7,1469^A0N,23,24^FB565,2,0,C^FH\^FN137^FD@Orders.Product^FS
^FT159,101^A0N,23,24^FH\^FN119^FD@Orders.DestTicketType^FS
^FT195,40^A0N,28,28^FB187,1,0,C^FH\^FDDelivery Details^FS
^FT8,225^A0N,23,24^FB146,1,0,R^FH\^FDCLOSE METER:^FS
^FO15,531^GB24,0,2^FS
^FO47,531^GB25,0,2^FS
^FO80,531^GB24,0,2^FS
^FO112,531^GB25,0,2^FS
^FO145,531^GB24,0,2^FS
^FO177,531^GB25,0,2^FS
^FO209,531^GB25,0,2^FS
^FO242,531^GB24,0,2^FS
^FO274,531^GB25,0,2^FS
^FO6,775^GB565,168,2^FS
^FO307,531^GB24,0,2^FS
^FO339,531^GB25,0,2^FS
^FO6,556^GB565,168,2^FS
^FO372,531^GB24,0,2^FS
^FT69,300^A0N,23,24^FB85,1,0,R^FH\^FDDEPART:^FS
^FT3,438^A0N,23,24^FB154,1,0,R^FH\^FDDRIVER NOTES:^FS
^FO404,531^GB25,0,2^FS
^FO437,531^GB24,0,2^FS
^FO469,531^GB25,0,2^FS
^FO502,531^GB24,0,2^FS
^FO534,531^GB24,0,2^FS
^FT5,992^A0N,23,24^FH\^FDThis is to certify that the listed materials are properly ^FS
^FT5,1020^A0N,23,24^FH\^FDclassified, described, packaged, marked and labeled ^FS
^FT5,1048^A0N,23,24^FH\^FDand are in proper condition for transportation, according ^FS
^FT5,1076^A0N,23,24^FH\^FDto the applicable regulations of the U.S. Department ^FS
^FT5,1104^A0N,23,24^FH\^FDof Transportation, the U.S. Department of Treasury ^FS
^FT5,1132^A0N,23,24^FH\^FDand all applicable state and local laws or regulations, ^FS
^FT5,1160^A0N,23,24^FH\^FDand Contractor certifies the proper cargo tank supplied ^FS
^FT5,1188^A0N,23,24^FH\^FDfor shipment is a proper container for the transportation ^FS
^FT5,1216^A0N,23,24^FH\^FDof the commodity and complies with the applicable ^FS
^FT5,1244^A0N,23,24^FH\^FDregulations of the U.S. Department of Transportation, ^FS
^FT5,1272^A0N,23,24^FH\^FDthe U.S. Department of Treasury and all applicable state ^FS
^FT5,1299^A0N,23,24^FH\^FDand local laws or regulations. Crude delivered hereunder ^FS
^FT5,1327^A0N,23,24^FH\^FDmeets the applicable E.P.A. and A.S.T.M. standards for ^FS
^FT5,1355^A0N,23,24^FH\^FDthe time and place of loading.^FS
^FT332,174^A0N,23,24^FB90,1,0,R^FH\^FDBS&W%:^FS
^FT8,773^A0N,23,24^FB263,1,0,C^FH\^FDDESTINATION SIGNATURE:^FS
^FT8,552^A0N,23,24^FB200,1,0,C^FH\^FDORIGIN SIGNATURE:^FS
^FT8,964^A0N,23,24^FB171,1,0,R^FH\^FDGAUGER (PRINT):^FS
^FT8,745^A0N,23,24^FB171,1,0,R^FH\^FDGAUGER (PRINT):^FS
^FT359,148^A0N,23,24^FB63,1,0,R^FH\^FDTEMP:^FS
^FT357,125^A0N,23,24^FB65,1,0,R^FH\^FDBOL #:^FS
^FT22,327^A0N,23,24^FB132,1,0,R^FH\^FDWAIT NOTES:^FS
^FT74,274^A0N,23,24^FB80,1,0,R^FH\^FDARRIVE:^FS
^FT21,102^A0N,23,24^FB133,1,0,R^FH\^FDTICKET TYPE:^FS
^FT55,174^A0N,23,24^FB99,1,0,R^FH\^FDEST. NSV:^FS
^FT61,125^A0N,23,24^FB93,1,0,R^FH\^FDSTATION:^FS
^FT21,252^A0N,23,24^FB133,1,0,R^FH\^FDAPI DENSITY:^FS
^FT18,198^A0N,23,24^FB136,1,0,R^FH\^FDOPEN METER:^FS
^FT13,80^A0N,23,24^FB141,1,0,R^FH\^FDDESTINATION:^FS
^FT97,1421^A0N,34,33^FB385,1,0,C^FH\^FDRESIDUE LAST CONTAINED:^FS
^FO2,1362^GB572,0,8^FS
^FO1,48^GB574,0,8^FS
^FO1,6^GB574,0,8^FS
^FT55,148^A0N,23,24^FB99,1,0,R^FH\^FDEST. GOV:^FS
^XZ'

---
INSERT INTO tblGaugerAppPrintPickupTemplate (TemplateText)
	SELECT '^XA
~TA000~JSN^LT0^MNN^MTD^PON^PMN^LH0,0^JMA^PR4,4^MD0^LRN^CI0
^MMT
^PW576
^LL1675
^LS0
^FT277,727^A0N,34,33^FH\^FN134^FD@Orders.DispatchConfirmNum^FS
^FT159,1290^A0N,23,24^FH\^FN109^FD@Orders.OriginAPI^FS
^FT427,1339^A0N,23,24^FH\^FN96^FD@Orders.OriginCA^FS
^FT156,868^A0N,23,24^FH\^FN117^FD@Orders.CarrierAuthority^FS
^FT159,1417^A0N,23,24^FH\^FN128^FD@Orders.OriginWaitNumDesc^FS
^FT427,1313^A0N,23,24^FH\^FN88^FD@Orders.OriginNDM^FS
^FT159,1316^A0N,23,24^FH\^FN11^FD@Orders.OriginState^FS
^FT427,1286^A0N,23,24^FH\^FN87^FD@Orders.OriginNDIC^FS
^FT137,805^A0N,28,28^FH\^FN86^FD@Orders.OriginGrossStdUnits$%,.3f^FS
^FT159,1527^A0N,23,24^FH\^FN129^FD@Orders.RejectNumDesc^FS
^FT156,999^A0N,23,24^FH\^FN26^FD@Orders.ChainUp$%!^FS
^FT156,1026^A0N,23,24^FH\^FN27^FD@Orders.Rejected$%!^FS
^FT159,1645^A0N,23,24^FB408,4,0^FH\^FN141^FD@Orders.PickupDriverNotes^FS
^FT459,804^A0N,23,24^FH\^FN53^FD@Orders.GrossUnitsVariance^FS
^FT459,833^A0N,23,24^FH\^FN54^FD@Orders.NetUnitsVariance^FS
^FT464,975^A0N,23,24^FH\^FN61^FD@Orders.DriverMileage^FS
^FT464,1000^A0N,23,24^FH\^FN62^FD@Orders.OriginTruckMileage^FS
^FT463,1027^A0N,23,24^FH\^FN63^FD@Orders.DestTruckMileage^FS
^FT118,613^A0N,23,24^FH\^FN71^FD@Orders.OriginDepartTimeUTC$%tD^FS
^FT240,773^A0N,28,28^FB74,1,0,R^FH\^FN78^FD@Orders.OriginUomShort^FS
^FT240,836^A0N,28,28^FB74,1,0,R^FH\^FN78^FD@Orders.OriginUomShort^FS
^FT240,805^A0N,28,28^FB74,1,0,R^FH\^FN78^FD@Orders.OriginUomShort^FS
^FT159,1211^A0N,23,24^FH\^FN79^FD@Orders.OriginStation^FS
^FT159,1183^A0N,23,24^FH\^FN125^FD@Orders.Customer^FS
^FT341,1367^A0N,23,24^FH\^FN81^FD@Orders.OriginTimeZone^FS
^FT341,1392^A0N,23,24^FH\^FN81^FD@Orders.OriginTimeZone^FS
^FT432,609^A0N,17,16^FH\^FN83^FD@Print.nowutc()$%tD %tl:%tM %tp^FS
^FT17,565^A0N,23,24^FB540,3,0^FH\^FN139^FD@Orders.EmergencyInfo^FS
^FT137,773^A0N,28,28^FH\^FN84^FD@Orders.OriginGrossUnits$%,.3f^FS
^FT137,836^A0N,28,28^FH\^FN85^FD@Orders.OriginNetUnits$%,.3f^FS
^FT159,1488^A0N,23,24^FB408,3,0^FH\^FN138^FD@Orders.OriginWaitNotes^FS
^FT156,944^A0N,23,24^FH\^FN92^FD@Orders.Trailer^FS
^FT156,972^A0N,23,24^FH\^FN93^FD@Orders.Trailer2^FS
^FT156,919^A0N,23,24^FH\^FN94^FD@Orders.Truck^FS
^FT159,1127^A0N,23,24^FH\^FN121^FD@Orders.Operator^FS
^FT159,1155^A0N,23,24^FH\^FN123^FD@Orders.Producer^FS
^FT159,1553^A0N,23,24^FH\^FN126^FD@Orders.RejectNotes^FS
^FT159,1099^A0N,23,24^FH\^FN122^FD@Orders.Origin^FS
^FT159,1238^A0N,23,24^FH\^FN107^FD@Orders.OriginLeaseNum^FS
^FT6,474^A0N,23,24^FB564,2,0^FH\^FN137^FD@Orders.Product^FS
^FT277,687^A0N,34,33^FH\^FN115^FD@Orders.OrderNum^FS
^FT159,1340^A0N,23,24^FH\^FN108^FD@Orders.OriginCounty^FS
^FT159,1392^A0N,23,24^FH\^FN110^FD@Orders.OriginDepartTimeUTC$%tD %tl:%tM %tp^FS
^FT159,1367^A0N,23,24^FH\^FN111^FD@Orders.OriginArriveTimeUTC$%tD %tl:%tM %tp^FS
^FT156,893^A0N,23,24^FH\^FN114^FD@Orders.DriverName^FS
^FT159,1265^A0N,23,24^FH\^FN118^FD@Orders.OriginLegalDescription^FS
^FT151,649^A0N,39,38^FB274,1,0,C^FH\^FN135^FD@Orders.TicketType^FS
^FT203,1064^A0N,28,28^FB170,1,0,C^FH\^FDPickup Details^FS
^FT351,1027^A0N,23,24^FB108,1,0,R^FH\^FDDEST ODO:^FS
^FT11,1026^A0N,23,24^FB140,1,0,R^FH\^FDREJ. ORIGIN?:^FS
^FT331,1000^A0N,23,24^FB128,1,0,R^FH\^FDORIGIN ODO:^FS
^FT12,836^A0N,28,28^FB120,1,0,R^FH\^FDEST. NSV:^FS
^FT336,975^A0N,23,24^FB123,1,0,R^FH\^FDTRIP MILES:^FS
^FT3,1578^A0N,23,24^FB154,1,0,R^FH\^FDDRIVER NOTES:^FS
^FT67,728^A0N,34,33^FB208,1,0,R^FH\^FDSHIPPER PO #:^FS
^FO358,750^GB0,92,2^FS
^FT41,944^A0N,23,24^FB110,1,0,R^FH\^FDTRAILER 1:^FS
^FT69,1391^A0N,23,24^FB85,1,0,R^FH\^FDDEPART:^FS
^FT25,1265^A0N,23,24^FB129,1,0,R^FH\^FDLEGAL DESC:^FS
^FT42,999^A0N,23,24^FB109,1,0,R^FH\^FDCHAINUP?:^FS
^FT22,1417^A0N,23,24^FB132,1,0,R^FH\^FDWAIT NOTES:^FS
^FT30,1528^A0N,23,24^FB124,1,0,R^FH\^FDREJ. NOTES:^FS
^FT74,1367^A0N,23,24^FB80,1,0,R^FH\^FDARRIVE:^FS
^FO357,747^GB218,0,3^FS
^FT77,919^A0N,23,24^FB74,1,0,R^FH\^FDTRUCK:^FS
^FT406,804^A0N,23,24^FB48,1,0,R^FH\^FDGOV:^FS
^FT85,1316^A0N,23,24^FB69,1,0,R^FH\^FDSTATE:^FS
^FT406,834^A0N,23,24^FB48,1,0,R^FH\^FDNSV:^FS
^FT66,1238^A0N,23,24^FB88,1,0,R^FH\^FDLEASE #:^FS
^FT366,609^A0N,17,16^FB65,1,0,C^FH\^FDPRINTED:^FS
^FT390,1340^A0N,23,24^FB33,1,0,R^FH\^FDCA:^FS
^FT114,1291^A0N,23,24^FB40,1,0,C^FH\^FDAPI:^FS
^FO3,842^GB573,0,3^FS
^FT389,774^A0N,23,24^FB166,1,0,C^FH\^FDDEST VARIANCE:^FS
^FT65,1340^A0N,23,24^FB89,1,0,R^FH\^FDCOUNTY:^FS
^FT369,1313^A0N,23,24^FB54,1,0,R^FH\^FDNDM:^FS
^FT367,1286^A0N,23,24^FB56,1,0,R^FH\^FDNDIC:^FS
^FT76,1100^A0N,23,24^FB78,1,0,R^FH\^FDORIGIN:^FS
^FO4,486^GB568,107,4^FS
^FT29,868^A0N,23,24^FB122,1,0,R^FH\^FDAUTHORITY:^FS
^FT70,893^A0N,23,24^FB81,1,0,R^FH\^FDDRIVER:^FS
^FT9,687^A0N,34,33^FB266,1,0,R^FH\^FDCARRIER ORDER #:^FS
^FO3,1069^GB570,0,8^FS
^FT61,1211^A0N,23,24^FB93,1,0,R^FH\^FDSTATION:^FS
^FT60,1183^A0N,23,24^FB94,1,0,R^FH\^FDSHIPPER:^FS
^FT12,773^A0N,28,28^FB121,1,0,R^FH\^FDEST. GOV:^FS
^FT12,805^A0N,28,28^FB119,1,0,R^FH\^FDEST. GSV:^FS
^FT3,614^A0N,23,24^FB110,1,0,C^FH\^FDRUN DATE:^FS
^FT41,972^A0N,23,24^FB110,1,0,R^FH\^FDTRAILER 2:^FS
^FT41,1128^A0N,23,24^FB113,1,0,R^FH\^FDOPERATOR:^FS
^FT38,1155^A0N,23,24^FB116,1,0,R^FH\^FDPRODUCER:^FS
^FO3,1031^GB570,0,8^FS
^XZ'

INSERT INTO tblGaugerAppPrintTicketTemplate (TemplateText)
	SELECT '^XA
~TA000~JSN^LT0^MNN^MTD^PON^PMN^LH0,0^JMA^PR4,4^MD0^LRN^CI0
^MMT
^PW576
^LL0285
^LS0
^FT246,170^A0N,23,24^FH\^FN142^FD@OrderTickets.OpeningGaugeQ^FS
^FT246,193^A0N,23,24^FH\^FN2^FD@OrderTickets.ClosingGaugeQ^FS
^FT327,29^A0N,23,24^FH\^FN3^FD@OrderTickets.Rejected$%!^FS
^FT171,100^A0N,23,24^FB24,1,0,R^FH\^FN10^FD@OrderTickets.BottomInches^FS
^FT170,169^A0N,23,24^FB24,1,0,R^FH\^FN12^FD@OrderTickets.OpeningGaugeFeet^FS
^FT170,193^A0N,23,24^FB24,1,0,R^FH\^FN13^FD@OrderTickets.ClosingGaugeFeet^FS
^FT203,193^A0N,23,24^FB25,1,0,R^FH\^FN14^FD@OrderTickets.ClosingGaugeInch^FS
^FT203,169^A0N,23,24^FB28,1,0,R^FH\^FN23^FD@OrderTickets.OpeningGaugeInch^FS
^FT432,100^A0N,23,24^FH\^FN28^FD@OrderTickets.ProductBSW$%,.3f^FS
^FT451,55^A0N,23,24^FH\^FN32^FD@OrderTickets.TankDescription^FS
^FT170,122^A0N,23,24^FH\^FN36^FD@OrderTickets.ProductObsGravity$%,.2f^FS
^FT432,122^A0N,23,24^FH\^FN38^FD@OrderTickets.Gravity60F$%,.2f^FS
^FT104,29^A0N,23,24^FH\^FN43^FD@OrderTickets.CarrierTicketNum^FS
^FT159,251^A0N,23,24^FH\^FN130^FD@OrderTickets.RejectNumDesc^FS
^FT107,223^A0N,23,24^FH\^FN49^FD@OrderTickets.GrossUnits$%,.3f^FS
^FT492,223^A0N,23,24^FH\^FN50^FD@OrderTickets.NetUnits$%,.3f^FS
^FT297,223^A0N,23,24^FH\^FN52^FD@OrderTickets.GrossStdUnits$%,.3f^FS
^FT433,171^A0N,23,24^FH\^FN64^FD@OrderTickets.MeterFactor^FS
^FT478,145^A0N,23,24^FH\^FN67^FD@OrderTickets.ProductLowTemp$%,.1f^FS
^FT319,145^A0N,23,24^FH\^FN68^FD@OrderTickets.ProductHighTemp$%,.1f^FS
^FT169,145^A0N,23,24^FH\^FN69^FD@OrderTickets.ProductObsTemp$%,.1f^FS
^FT448,29^A0N,23,24^FH\^FN97^FD@OrderTickets.DispatchConfirmNum^FS
^FT168,55^A0N,23,24^FH\^FN89^FD@OrderTickets.OriginTankText^FS
^FT299,55^A0N,23,24^FH\^FN98^FD@Orders.OriginBOLNum^FS
^FT170,78^A0N,23,24^FB120,1,0,R^FH\^FN90^FD@OrderTickets.SealOff^FS
^FT303,78^A0N,23,24^FH\^FN91^FD@OrderTickets.SealOn^FS
^FT159,277^A0N,23,24^FH\^FN127^FD@OrderTickets.RejectNotes^FS
^FT220,100^A0N,28,28^FH\^FDDEMO^FS
^FT255,91^A0N,28,28^FH\^FDDEMO^FS
^FO17,3^GB24,0,3^FS
^FO49,3^GB25,0,3^FS
^FO82,3^GB24,0,3^FS
^FO114,3^GB25,0,3^FS
^FO146,3^GB25,0,3^FS
^FO179,3^GB24,0,3^FS
^FO211,3^GB25,0,3^FS
^FO244,3^GB24,0,3^FS
^FO276,3^GB25,0,3^FS
^FO309,3^GB24,0,3^FS
^FO341,3^GB25,0,3^FS
^FO374,3^GB24,0,3^FS
^FO406,3^GB25,0,3^FS
^FO438,3^GB25,0,3^FS
^FO471,3^GB24,0,3^FS
^FO503,3^GB25,0,3^FS
^FO536,3^GB24,0,3^FS
^FT393,223^A0N,23,24^FB99,1,0,C^FH\^FDEST. NSV:^FS
^FO3,31^GB572,200,2^FS
^FO192,197^GB0,32,2^FS
^FO388,197^GB0,32,2^FS
^FT195,169^A0N,23,24^FH\^FD''^FS
^FT195,194^A0N,23,24^FH\^FD''^FS
^FT292,78^A0N,23,24^FB7,1,0,C^FH\^FD/^FS
^FT7,145^A0N,23,24^FB108,1,0,R^FH\^FDTEMPS (F):^FS
^FT404,145^A0N,23,24^FB71,1,0,R^FH\^FDCLOSE:^FS
^FT291,55^A0N,23,24^FB7,1,0,C^FH\^FD/^FS
^FT194,101^A0N,23,24^FH\^FD"^FS
^FT228,169^A0N,23,24^FH\^FD"^FS
^FT257,145^A0N,23,24^FB61,1,0,R^FH\^FDOPEN:^FS
^FT57,122^A0N,23,24^FB108,1,0,R^FH\^FDOBS GRAV:^FS
^FT228,194^A0N,23,24^FH\^FD"^FS
^FT30,249^A0N,23,24^FB124,1,0,R^FH\^FDREJ. NOTES:^FS
^FT323,122^A0N,23,24^FB104,1,0,R^FH\^FDGRAV 60F:^FS
^FT336,100^A0N,23,24^FB91,1,0,R^FH\^FDBS&W%:^FS
^FT259,170^A0N,23,24^FH\^FDQ^FS
^FT259,194^A0N,23,24^FH\^FDQ^FS
^FT207,29^A0N,23,24^FB118,1,0,C^FH\^FDREJECTED?:^FS
^FT120,145^A0N,23,24^FB47,1,0,R^FH\^FDOBS:^FS
^FO4,196^GB570,0,3^FS
^FT105,169^A0N,23,24^FB62,1,0,R^FH\^FDOPEN:^FS
^FT399,29^A0N,23,24^FB46,1,0,R^FH\^FDPO#:^FS
^FT95,193^A0N,23,24^FB71,1,0,R^FH\^FDCLOSE:^FS
^FT8,78^A0N,23,24^FB157,1,0,R^FH\^FDSEAL OFF/ON #:^FS
^FT342,171^A0N,23,24^FB85,1,0,R^FH\^FDFACTOR:^FS
^FT75,100^A0N,23,24^FB90,1,0,R^FH\^FDBOTTOM:^FS
^FT6,29^A0N,23,24^FB96,1,0,C^FH\^FDTICKET #:^FS
^FT198,223^A0N,23,24^FB98,1,0,C^FH\^FDEST. GSV:^FS
^FT7,223^A0N,23,24^FB100,1,0,C^FH\^FDEST. GOV:^FS
^FT40,55^A0N,23,24^FB125,1,0,R^FH\^FDTANK/BOL #:^FS
^XZ'

COMMIT
SET NOEXEC OFF