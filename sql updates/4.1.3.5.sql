SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.1.3.4'
SELECT  @NewVersion = '4.1.3.5'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1770: Add reject filter to BOL and settlement pages'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO



/***********************************
 Date Created: 28 Feb 2013
 Author: Kevin Alons
 Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc for 3rd parties (Customer/Carrier)
 Changes:
	??		- 8/18/15  - BB		- Made delivered orders possible to print eBOLs page
	3.9.31	- 12/04/15 - JAE&BB - Join tblOrderPhoto for access to most recent photos on eBOL print page
								- convert @StartDate & @EndDate to date data type (eliminates need to use fnDateOnly() )
			- 12/09/15 - BB     - Made reject photo accessible separately
	4.0.5	- 08/24/16 - BB		- DCWEB-1655: Added filters for Job#, Contract#, origin, destination, driver, and rejected status
	4.0.11	- 09/01/16 - BB		- DCWEB-1689: Add origin/destination arrive/depart photos
	4.1.3.5	- 09/16/16 - BB		- DCWEB-1770: Add reject filter for BOL page filter
***********************************/
ALTER PROCEDURE [dbo].[spOrdersBasicExport]
(
  @StartDate date
, @EndDate date
, @CarrierID int = 0 -- -1 = all carriers
, @CustomerID int = 0 -- -1 = all customers
, @ProducerID int = 0
, @OrderNum varchar(20) = NULL
, @JobNumber varchar(20) = NULL
, @ContractNumber varchar(20) = NULL
, @DriverID int = 0
, @OriginID int = 0
, @DestinationID int = 0
, @Rejected int = -1 -- -1 = all orders (rejects included) -- 4.1.3.5
, @StatusID_CSV varchar(max) = '3,4'
) AS BEGIN	
	SELECT OE.*
		, OriginPhotoID = (SELECT TOP 1 OP.ID FROM tblOrderPhoto OP WHERE OrderID = OE.ID AND OP.OriginID IS NOT NULL AND OP.PhotoTypeID = 1 ORDER BY OP.CreateDateUTC DESC)
		, OriginArrivePhotoID = (SELECT TOP 1 OP.ID FROM tblOrderPhoto OP WHERE OrderID = OE.ID AND OP.OriginID IS NOT NULL AND OP.PhotoTypeID = 3 ORDER BY OP.CreateDateUTC DESC)
		, OriginDepartPhotoID = (SELECT TOP 1 OP.ID FROM tblOrderPhoto OP WHERE OrderID = OE.ID AND OP.OriginID IS NOT NULL AND OP.PhotoTypeID = 4 ORDER BY OP.CreateDateUTC DESC)
		, RejectPhotoID = (SELECT TOP 1 OP.ID FROM tblOrderPhoto OP WHERE OrderID = OE.ID AND OE.Rejected = 1 AND OP.PhotoTypeID = 2 ORDER BY OP.CreateDateUTC DESC)
		, DestPhotoID = (SELECT TOP 1 OP.ID FROM tblOrderPhoto OP WHERE OrderID = OE.ID AND OP.DestinationID IS NOT NULL AND OP.PhotoTypeID = 1 ORDER BY OP.CreateDateUTC DESC)		
		, DestArrivePhotoID = (SELECT TOP 1 OP.ID FROM tblOrderPhoto OP WHERE OrderID = OE.ID AND OP.DestinationID IS NOT NULL AND OP.PhotoTypeID = 3 ORDER BY OP.CreateDateUTC DESC)		
		, DestDepartPhotoID = (SELECT TOP 1 OP.ID FROM tblOrderPhoto OP WHERE OrderID = OE.ID AND OP.DestinationID IS NOT NULL AND OP.PhotoTypeID = 4 ORDER BY OP.CreateDateUTC DESC)		
	FROM dbo.viewOrderExportFull_Reroute OE
	WHERE (@ProducerID=0 OR OE.ProducerID=@ProducerID)
		AND (@CarrierID=-1 OR @CarrierID=0 OR CarrierID=@CarrierID) 
		AND (@CustomerID=-1 OR @CustomerID=0 OR CustomerID=@CustomerID)
		AND OE.OrderDate BETWEEN @StartDate AND @EndDate
		AND OE.DeleteDateUTC IS NULL
		AND (OE.StatusID IN (SELECT ID FROM dbo.fnSplitCSVIDs(@StatusID_CSV)))
		AND (@OrderNum IS NULL OR OE.OrderNum LIKE @OrderNum)
		AND (@JobNumber IS NULL OR OE.JobNumber LIKE @JobNumber)
		AND (@ContractNumber IS NULL OR OE.ContractNumber LIKE @ContractNumber)
		AND (@DriverID=-1 OR @DriverID=0 OR OE.DriverID=@DriverID)
		AND (@OriginID=-1 OR @OriginID=0 OR OE.OriginID=@OriginID)
		AND (@DestinationID=-1 OR @DestinationID=0 OR OE.DestinationID=@DestinationID)	  
		AND (@Rejected=-1 OR OE.Rejected=@Rejected)
	ORDER BY OE.OrderDate, OE.OrderNum		
END
GO




/**********************************/
-- Created: 2013/03/09 - ?.?.? - Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
-- Changes:
-- 3.7.31	- 2015/06/19 - KDA	- reorder input parameters (move ProviderID down) and add @DriverGroupID parameter
-- 3.9.0	- 2/15/08/14 - KDA	- return Approved column
-- 3.9.19.6	- 2015/09/30 - KDA	- add OverrideXXX fields from OrderApproval table
-- 3.9.21	- 2015/10/03 - KDA	- use [viewOrder] O.OriginDriverGroupID instead of O.DriverGroupID (which could be DestDriver.DriverGroupID)
-- 3.9.34	- 2015/12/16 - BB	- Remove deleted orders from the results (DCWEB-851)
-- 3.10.13.6 - 2015/03/01 - JAE	- add TruckTypeID
-- 3.11.3.1	- 2016/03/22 - KDA	- fix to no-op when @BatchID = 0
-- 3.11.19	- 2016/05/02 - BB	- Add Job number and Contract number
-- 3.11.20	- 2016/05/04 - JAE	- 3 month limit was always being applied, skip if batch is provided
-- 3.11.20.1 - 2016/05/04 - JAE	- Undoing change as timeouts reoccurring, need to investigate
-- 3.11.20.3 - 2016/05/11 - KDA	- optimize @BatchID filtering to query directly from tblOrderSettlementCarrier table
--								- use tblOrder and minimal JOINs intead of expensive viewOrder
-- 4.1.0	- 2016/08/08 - KDA	- use new @StartSession & @SessionID parameters to persist the retrieved records as a "Session"
--								- stop defaulting @StartDate to 3 months prior if not supplied (null parameter value provided)
--								- use simplified viewOrder_Financial_Carrier for return results - which is now again optimized for "reasonable" performance
-- 4.1.3.5	- 2016/09/17 - BB	- DCWEB-1770: Add reject filter
/***********************************/
ALTER PROCEDURE [dbo].[spRetrieveOrdersFinancialCarrier]
(
  @StartDate date = NULL -- will default to first day of 3rd prior month for efficiency sake
, @EndDate date = NULL
, @ShipperID int = -1 -- all
, @CarrierID int = -1 -- all
, @ProductGroupID int = -1 -- all 
, @TruckTypeID int = -1 -- all
, @DriverGroupID int = -1 -- all
, @OriginStateID int = -1 -- all 
, @DestStateID int = -1 -- all
, @ProducerID int = -1 -- all
, @BatchID int = NULL -- either show unbatched (NULL), or the specified batch orders
, @OnlyShipperSettled bit = 0 
, @JobNumber varchar(20) = NULL -- 3.11.19
, @ContractNumber varchar(20) = NULL -- 3.11.19
, @Rejected int = -1 -- all (rejected + not rejected) -- 4.1.3.5
, @StartSession bit = 0 -- when set to 1, assign the retrieved records to a new Session (ID), when 0 and a sessionID was assigned, then retrieve it
, @SessionID varchar(100) = NULL OUTPUT -- if @startSession = 1, then this return a new value, otherwise will retrieve the existing records in that session
) AS 
BEGIN
	IF (@StartSession IS NULL) SET @StartSession = CASE WHEN @SessionID IS NULL THEN 0 ELSE 1 END

	-- validate the incoming parameters
	IF (@BatchID IS NOT NULL
		AND (@ShipperID <> -1 
			OR @CarrierID <> -1 
			OR @ProductGroupID <> -1
			OR @TruckTypeID <> -1
			OR @DriverGroupID <> -1
			OR @OriginStateID <> -1
			OR @DestStateID <> -1
			OR @ProducerID <> -1))
	BEGIN
		RAISERROR('@BatchID parameter cannot be specified with other parameters', 16, 1)
		RETURN
	END

	DECLARE @sql varchar(max) = '
		SELECT O.*
			, OS.RateApplySel, OS.BatchSel, OS.SessionID
		FROM viewOrder_Financial_Carrier O
		LEFT JOIN tblOrderSettlementSelectionCarrier OS ON OS.OrderID = O.ID'

	-- if a @BatchID value was specified then just use this criteria alone
    IF @BatchID IS NOT NULL
	BEGIN
		SET @sql = @sql + '
			WHERE O.BatchID = @BatchID'
		SET @sql = replace(@sql, '@BatchID', @BatchID)
	END
	ELSE IF (@StartSession = 0 AND @SessionID IS NOT NULL)
	BEGIN
		SET @sql = @sql + '
			WHERE O.ID IN (SELECT OrderID FROM tblOrderSettlementSelectionCarrier WHERE SessionID = @SessionID)'
		SET @sql = replace(@sql, '@SessionID', @SessionID)
	END
	ELSE
	BEGIN
		SET @sql = @sql + '
			LEFT JOIN dbo.tblOrderTransfer OTR ON OTR.OrderID = O.ID
			LEFT JOIN dbo.viewDriverBase vODR ON vODR.ID = OTR.OriginDriverID
			LEFT JOIN dbo.viewDriverBase vDDR ON vDDR.ID = O.DriverID
			LEFT JOIN tblOrderSettlementShipper OSP ON OSP.OrderID = O.ID AND OSP.BatchID IS NOT NULL
			WHERE O.StatusID IN (4)  
				AND O.DeleteDateUTC IS NULL
				AND O.BatchID IS NULL 
				AND (@ShipperID=-1 OR O.CustomerID=@ShipperID) 
				AND (@CarrierID=-1 OR O.CarrierID=@CarrierID) 
				AND (@ProductGroupID=-1 OR ProductGroupID=@ProductGroupID) 
				AND (@TruckTypeID=-1 OR TruckTypeID=@TruckTypeID)
				AND (@DriverGroupID=-1 OR ISNULL(vODR.DriverGroupID, vDDR.DriverGroupID) = @DriverGroupID) 
				AND (@OriginStateID=-1 OR O.OriginStateID=@OriginStateID) 
				AND (@DestStateID=-1 OR O.DestStateID=@DestStateID) 
				AND (@ProducerID=-1 OR O.ProducerID=@ProducerID) 
				AND (@StartDate IS NULL OR O.OrderDate >= @StartDate) 
				AND (@EndDate IS NULL OR O.OrderDate <= @EndDate)
				AND (@OnlyShipperSettled = 0 OR OSP.BatchID IS NOT NULL)
				AND (@JobNumber IS NULL OR O.JobNumber = isnull(@JobNumber, ''''))
				AND (@ContractNumber IS NULL OR O.ContractNumber = isnull(@ContractNumber, ''''))
				AND (@Rejected=-1 OR O.Rejected=@Rejected)'
		SET @sql = replace(@sql, '@ShipperID', @ShipperID)
		SET @sql = replace(@sql, '@CarrierID', @CarrierID)
		SET @sql = replace(@sql, '@ProductGroupID', @ProductGroupID)
		SET @sql = replace(@sql, '@TruckTypeID', @TruckTypeID)
		SET @sql = replace(@sql, '@DriverGroupID', @DriverGroupID)
		SET @sql = replace(@sql, '@OriginStateID', @OriginStateID)
		SET @sql = replace(@sql, '@DestStateID', @DestStateID)
		SET @sql = replace(@sql, '@ProducerID', @ProducerID)
		SET @sql = replace(@sql, '@StartDate', dbo.fnQS(@StartDate))
		SET @sql = replace(@sql, '@EndDate', dbo.fnQS(@EndDate))
		SET @sql = replace(@sql, '@OnlyShipperSettled', @OnlyShipperSettled)
		SET @sql = replace(@sql, '@JobNumber', dbo.fnQS(nullif(rtrim(@JobNumber), '')))
		SET @sql = replace(@sql, '@ContractNumber', dbo.fnQS(nullif(rtrim(@ContractNumber), '')))
		SET @sql = replace(@sql, '@Rejected', @Rejected)
	END

	-- PRINT 'sql = ' + @sql
	
	BEGIN TRAN RetrieveOrdersFinancialCarrier

	-- create the temp table to store the data
	SELECT *, RateApplySel = cast(1 as bit), BatchSel = cast(1 as bit), SessionID = cast(NULL AS varchar(100))
	INTO #ret 
	FROM viewOrder_Financial_Carrier 
	WHERE 1 = 0
	
	-- get the data into the temp table
	INSERT INTO #ret EXEC (@sql)

	-- do the SessionID logic
	IF (@StartSession = 1)
	BEGIN
		DELETE FROM tblOrderSettlementSelectionCarrier WHERE OrderID IN (SELECT ID FROM #ret) OR SessionID IN (SELECT SessionID FROM #ret)
		SELECT @sessionID = isnull(max(SessionID), 0) + 1 FROM tblOrderSettlementSelectionCarrier
		UPDATE #ret SET SessionID = @sessionID, RateApplySel = 1, BatchSel = CASE WHEN HasError = 1 THEN 0 ELSE 1 END
		INSERT INTO tblOrderSettlementSelectionCarrier (OrderID, SessionID, RateApplySel, BatchSel)
			SELECT ID, @sessionID, RateApplySel, BatchSel FROM #ret
	END
	
	COMMIT TRAN RetrieveOrdersFinancialCarrier

	-- return the data to the caller
	SELECT * FROM #ret
END
GO



/***********************************/
-- Created: ?.?.? - 2013/03/09 - Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
-- Changes:
-- 3.8.11	- 2015/07/28 - KDA	- remove OriginShipperRegion field
-- 3.9.0	- 2/15/08/14 - KDA	- return Approved column
-- 3.9.19.6	- 2015/09/30 - KDA	- add OverrideXXX fields from OrderApproval table
-- 3.9.34	- 2015/12/16 - BB	- Remove deleted orders from the results (DCWEB-851)
-- 3.10.13.6 - 2015/03/01 - JAE	- add TruckTypeID
-- 3.11.3.1	- 2016/03/22 - KDA	- fix to no-op when @BatchID = 0
-- 3.11.19	- 2016/05/02 - BB	- Add Job number and Contract number
-- 3.11.20.3 - 2016/05/11 - KDA	- optimize @BatchID filtering to query directly from tblOrderSettlementShipper table
--								- use tblOrder and minimal JOINs intead of expensive viewOrder
-- 4.1.0	- 2016/08/08 - KDA	- use new @StartSession & @SessionID parameters to persist the retrieved records as a "Session"
--								- stop defaulting @StartDate to 3 months prior if not supplied (null parameter value provided)
--								- use simplified viewOrder_Financial_Shipper for return results - which is now again optimized for "reasonable" performance
-- 4.1.3.5	- 2016/09/17 - BB	- DCWEB-1770: Add reject filter
/***********************************/
ALTER PROCEDURE [dbo].[spRetrieveOrdersFinancialShipper]
(
  @StartDate date = NULL -- will default to first day of 3rd prior month for efficiency sake
, @EndDate date = NULL
, @ShipperID int = -1 -- all
, @ProductGroupID int = -1 -- all 
, @TruckTypeID int = -1 -- all
, @DriverGroupID int = -1 -- all
, @OriginStateID int = -1 -- all 
, @DestStateID int = -1 -- all
, @ProducerID int = -1 -- all
, @BatchID int = NULL -- either show unbatched (NULL), or the specified batch orders
, @JobNumber varchar(20) = NULL -- 3.11.19
, @ContractNumber varchar(20) = NULL -- 3.11.19
, @Rejected int = -1 -- all (rejected + not rejected) -- 4.1.3.5
, @StartSession bit = 0 -- when set to 1, assign the retrieved records to a new Session (ID), when 0 and a sessionID was assigned, then retrieve it
, @SessionID varchar(100) = NULL OUTPUT -- if @startSession = 1, then this return a new value, otherwise will retrieve the existing records in that session
) AS 
BEGIN
	IF (@StartSession IS NULL) SET @StartSession = CASE WHEN @SessionID IS NULL THEN 0 ELSE 1 END

	-- validate the incoming parameters
	IF (@BatchID IS NOT NULL
		AND (@ShipperID <> -1 
			OR @ProductGroupID <> -1
			OR @TruckTypeID <> -1
			OR @DriverGroupID <> -1
			OR @OriginStateID <> -1
			OR @DestStateID <> -1
			OR @ProducerID <> -1))
	BEGIN
		RAISERROR('@BatchID parameter cannot be specified with other parameters', 16, 1)
		RETURN
	END

	DECLARE @sql varchar(max) = '
		SELECT O.*
			, OS.RateApplySel, OS.BatchSel, OS.SessionID
		FROM viewOrder_Financial_Shipper O
		LEFT JOIN tblOrderSettlementSelectionShipper OS ON OS.OrderID = O.ID'

	-- if a @BatchID value was specified then just use this criteria alone
    IF @BatchID IS NOT NULL
	BEGIN
		SET @sql = @sql + '
			WHERE O.BatchID = @BatchID'
		SET @sql = replace(@sql, '@BatchID', @BatchID)
	END
	ELSE IF (@StartSession = 0 AND @SessionID IS NOT NULL)
	BEGIN
		SET @sql = @sql + '
			WHERE O.ID IN (SELECT OrderID FROM tblOrderSettlementSelectionShipper WHERE SessionID = @SessionID)'
		SET @sql = replace(@sql, '@SessionID', @SessionID)
	END
	ELSE
	BEGIN
		SET @sql = @sql + '
			LEFT JOIN dbo.tblOrderTransfer OTR ON OTR.OrderID = O.ID
			LEFT JOIN dbo.viewDriverBase vODR ON vODR.ID = OTR.OriginDriverID
			LEFT JOIN dbo.viewDriverBase vDDR ON vDDR.ID = O.DriverID
			WHERE O.StatusID IN (4)  
				AND O.DeleteDateUTC IS NULL
				AND O.BatchID IS NULL 
				AND (@ShipperID=-1 OR O.CustomerID=@ShipperID) 
				AND (@ProductGroupID=-1 OR ProductGroupID=@ProductGroupID) 
				AND (@TruckTypeID=-1 OR TruckTypeID=@TruckTypeID)
				AND (@DriverGroupID=-1 OR ISNULL(vODR.DriverGroupID, vDDR.DriverGroupID) = @DriverGroupID) 
				AND (@OriginStateID=-1 OR O.OriginStateID=@OriginStateID) 
				AND (@DestStateID=-1 OR O.DestStateID=@DestStateID) 
				AND (@ProducerID=-1 OR O.ProducerID=@ProducerID) 
				AND (@StartDate IS NULL OR O.OrderDate >= @StartDate) 
				AND (@EndDate IS NULL OR O.OrderDate <= @EndDate)
				AND (@JobNumber IS NULL OR O.JobNumber = isnull(@JobNumber, ''''))
				AND (@ContractNumber IS NULL OR O.ContractNumber = isnull(@ContractNumber, ''''))
				AND (@Rejected=-1 OR O.Rejected=@Rejected)'
		SET @sql = replace(@sql, '@ShipperID', @ShipperID)
		SET @sql = replace(@sql, '@ProductGroupID', @ProductGroupID)
		SET @sql = replace(@sql, '@TruckTypeID', @TruckTypeID)
		SET @sql = replace(@sql, '@DriverGroupID', @DriverGroupID)
		SET @sql = replace(@sql, '@OriginStateID', @OriginStateID)
		SET @sql = replace(@sql, '@DestStateID', @DestStateID)
		SET @sql = replace(@sql, '@ProducerID', @ProducerID)
		SET @sql = replace(@sql, '@StartDate', dbo.fnQS(@StartDate))
		SET @sql = replace(@sql, '@EndDate', dbo.fnQS(@EndDate))
		SET @sql = replace(@sql, '@JobNumber', dbo.fnQS(nullif(rtrim(@JobNumber), '')))
		SET @sql = replace(@sql, '@ContractNumber', dbo.fnQS(nullif(rtrim(@ContractNumber), '')))
		SET @sql = replace(@sql, '@Rejected', @Rejected)
	END

	-- PRINT 'sql = ' + @sql
	
	BEGIN TRAN RetrieveOrdersFinancialShipper

	-- create the temp table to store the data
	SELECT *, RateApplySel = cast(1 as bit), BatchSel = cast(1 as bit), SessionID = cast(NULL AS varchar(100))
	INTO #ret 
	FROM viewOrder_Financial_Shipper 
	WHERE 1 = 0
	
	-- get the data into the temp table
	INSERT INTO #ret EXEC (@sql)

	-- do the SessionID logic
	IF (@StartSession = 1)
	BEGIN
		DELETE FROM tblOrderSettlementSelectionShipper WHERE OrderID IN (SELECT ID FROM #ret) OR SessionID IN (SELECT SessionID FROM #ret)
		SELECT @sessionID = isnull(max(SessionID), 0) + 1 FROM tblOrderSettlementSelectionShipper
		UPDATE #ret SET SessionID = @sessionID, RateApplySel = 1, BatchSel = CASE WHEN HasError = 1 THEN 0 ELSE 1 END
		INSERT INTO tblOrderSettlementSelectionShipper (OrderID, SessionID, RateApplySel, BatchSel)
			SELECT ID, @sessionID, RateApplySel, BatchSel FROM #ret
	END
	
	COMMIT TRAN RetrieveOrdersFinancialShipper

	-- return the data to the caller
	SELECT * FROM #ret
END
GO


/**********************************/
-- Created: 2013/03/09 - ?.?.? - Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
-- Changes:
-- 4.1.0.2	- 2016.08.28 - KDA	- add some PRINT instrumentation statements for performance debugging purposes
-- 4.1.3.5	- 2016/09/17 - BB	- DCWEB-1770: Add reject filter
/***********************************/
ALTER PROCEDURE [dbo].[spRetrieveOrdersFinancialDriver]
(
  @StartDate date = NULL -- will default to first day of 3rd prior month for efficiency sake
, @EndDate date = NULL
, @ShipperID int = -1 -- all
, @CarrierID int = -1 -- all
, @ProductGroupID int = -1 -- all 
, @TruckTypeID int = -1 -- all
, @DriverGroupID int = -1 -- all
, @DriverID int = -1 -- all
, @OriginStateID int = -1 -- all 
, @DestStateID int = -1 -- all
, @ProducerID int = -1 -- all
, @BatchID int = NULL -- either show unbatched (NULL), or the specified batch orders
, @OnlyCarrierSettled bit = 0 
, @JobNumber varchar(20) = NULL -- 3.11.19
, @ContractNumber varchar(20) = NULL -- 3.11.19
, @Rejected int = -1 -- all (rejected + not rejected) -- 4.1.3.5
, @StartSession bit = 0 -- when set to 1, assign the retrieved records to a new Session (ID), when 0 and a sessionID was assigned, then retrieve it
, @SessionID varchar(100) = NULL OUTPUT -- if @startSession = 1, then this return a new value, otherwise will retrieve the existing records in that session
) AS 
BEGIN
	PRINT 'Start=' + CONVERT(VARCHAR(24), GETDATE(), 121)

	IF (@StartSession IS NULL) SET @StartSession = CASE WHEN @SessionID IS NULL THEN 0 ELSE 1 END

	-- validate the incoming parameters
	IF (@BatchID IS NOT NULL
		AND (@ShipperID <> -1 
			OR @CarrierID <> -1 
			OR @ProductGroupID <> -1
			OR @TruckTypeID <> -1
			OR @DriverGroupID <> -1
			OR @DriverID <> -1
			OR @OriginStateID <> -1
			OR @DestStateID <> -1
			OR @ProducerID <> -1))
	BEGIN
		RAISERROR('@BatchID parameter cannot be specified with other parameters', 16, 1)
		RETURN
	END

	DECLARE @sql varchar(max) = '
		SELECT O.*
			, OS.RateApplySel, OS.BatchSel, OS.SessionID
		FROM viewOrder_Financial_Driver O
		LEFT JOIN tblOrderSettlementSelectionDriver OS ON OS.OrderID = O.ID'

	-- if a @BatchID value was specified then just use this criteria alone
    IF @BatchID IS NOT NULL
	BEGIN
		SET @sql = @sql + '
			WHERE O.BatchID = @BatchID'
		SET @sql = replace(@sql, '@BatchID', @BatchID)
	END
	ELSE IF (@StartSession = 0 AND @SessionID IS NOT NULL)
	BEGIN
		SET @sql = @sql + '
			WHERE O.ID IN (SELECT OrderID FROM tblOrderSettlementSelectionDriver WHERE SessionID = @SessionID)'
		SET @sql = replace(@sql, '@SessionID', @SessionID)
	END
	ELSE
	BEGIN
		SET @sql = @sql + '
			LEFT JOIN dbo.tblOrderTransfer OTR ON OTR.OrderID = O.ID
			LEFT JOIN dbo.viewDriverBase vODR ON vODR.ID = OTR.OriginDriverID
			LEFT JOIN dbo.viewDriverBase vDDR ON vDDR.ID = O.DriverID
			LEFT JOIN tblOrderSettlementCarrier OSP ON OSP.OrderID = O.ID AND OSP.BatchID IS NOT NULL
			WHERE O.StatusID IN (4)  
				AND O.DeleteDateUTC IS NULL
				AND O.BatchID IS NULL 
				AND (@ShipperID=-1 OR O.CustomerID=@ShipperID) 
				AND (@CarrierID=-1 OR O.CarrierID=@CarrierID) 
				AND (@ProductGroupID=-1 OR ProductGroupID=@ProductGroupID) 
				AND (@TruckTypeID=-1 OR TruckTypeID=@TruckTypeID)
				AND (@DriverGroupID=-1 OR ISNULL(vODR.DriverGroupID, vDDR.DriverGroupID) = @DriverGroupID) 
				AND (@DriverID=-1 OR ISNULL(vODR.ID, vDDR.ID) = @DriverID) 
				AND (@OriginStateID=-1 OR O.OriginStateID=@OriginStateID) 
				AND (@DestStateID=-1 OR O.DestStateID=@DestStateID) 
				AND (@ProducerID=-1 OR O.ProducerID=@ProducerID) 
				AND (@StartDate IS NULL OR O.OrderDate >= @StartDate) 
				AND (@EndDate IS NULL OR O.OrderDate <= @EndDate)
				AND (@OnlyCarrierSettled = 0 OR OSP.BatchID IS NOT NULL)
				AND (@JobNumber IS NULL OR O.JobNumber = isnull(@JobNumber, ''''))
				AND (@ContractNumber IS NULL OR O.ContractNumber = isnull(@ContractNumber, ''''))
				AND (@Rejected=-1 OR O.Rejected=@Rejected)'
		SET @sql = replace(@sql, '@ShipperID', @ShipperID)
		SET @sql = replace(@sql, '@CarrierID', @CarrierID)
		SET @sql = replace(@sql, '@ProductGroupID', @ProductGroupID)
		SET @sql = replace(@sql, '@TruckTypeID', @TruckTypeID)
		SET @sql = replace(@sql, '@DriverGroupID', @DriverGroupID)
		SET @sql = replace(@sql, '@DriverID', @DriverID)
		SET @sql = replace(@sql, '@OriginStateID', @OriginStateID)
		SET @sql = replace(@sql, '@DestStateID', @DestStateID)
		SET @sql = replace(@sql, '@ProducerID', @ProducerID)
		SET @sql = replace(@sql, '@StartDate', dbo.fnQS(@StartDate))
		SET @sql = replace(@sql, '@EndDate', dbo.fnQS(@EndDate))
		SET @sql = replace(@sql, '@OnlyCarrierSettled', @OnlyCarrierSettled)
		SET @sql = replace(@sql, '@JobNumber', dbo.fnQS(nullif(rtrim(@JobNumber), '')))
		SET @sql = replace(@sql, '@ContractNumber', dbo.fnQS(nullif(rtrim(@ContractNumber), '')))
		SET @sql = replace(@sql, '@Rejected', @Rejected)
	END

	PRINT 'sql = ' + @sql
	
	BEGIN TRAN RetrieveOrdersFinancialDriver

	-- create the temp table to store the data
	SELECT *, RateApplySel = cast(1 as bit), BatchSel = cast(1 as bit), SessionID = cast(NULL AS varchar(100))
	INTO #ret 
	FROM viewOrder_Financial_Driver 
	WHERE 1 = 0
	
	-- get the data into the temp table
	PRINT 'Retrieve:Start=' + CONVERT(VARCHAR(24), GETDATE(), 121)
	INSERT INTO #ret EXEC (@sql)
	PRINT 'Retrieve:Finish=' + CONVERT(VARCHAR(24), GETDATE(), 121)

	-- do the SessionID logic
	IF (@StartSession = 1)
	BEGIN
		PRINT 'SessionCreate:Start=' + CONVERT(VARCHAR(24), GETDATE(), 121)
		DELETE FROM tblOrderSettlementSelectionDriver WHERE OrderID IN (SELECT ID FROM #ret) OR SessionID IN (SELECT SessionID FROM #ret)
		SELECT @sessionID = isnull(max(SessionID), 0) + 1 FROM tblOrderSettlementSelectionDriver
		UPDATE #ret SET SessionID = @sessionID, RateApplySel = 1, BatchSel = CASE WHEN HasError = 1 THEN 0 ELSE 1 END
		INSERT INTO tblOrderSettlementSelectionDriver (OrderID, SessionID, RateApplySel, BatchSel)
			SELECT ID, @sessionID, RateApplySel, BatchSel FROM #ret
		PRINT 'SessionCreate:Finish=' + CONVERT(VARCHAR(24), GETDATE(), 121)
	END
	
	PRINT 'Commit:Start=' + CONVERT(VARCHAR(24), GETDATE(), 121)
	COMMIT TRAN RetrieveOrdersFinancialDriver
	PRINT 'Commit:Finish=' + CONVERT(VARCHAR(24), GETDATE(), 121)

	-- return the data to the caller
	SELECT * FROM #ret

	PRINT 'Finish=' + CONVERT(VARCHAR(24), GETDATE(), 121)
END
GO



EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRecompileAllStoredProcedures
GO
EXEC _spRecompileAllStoredProcedures
GO
EXEC _spRebuildAllObjects
GO
EXEC _spRebuildAllObjects
GO



COMMIT
SET NOEXEC OFF