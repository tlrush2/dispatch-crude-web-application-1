-- select * from tblsetting where id = 0
/* 
	-- allow editing of Order.ActualMiles past being Audited all the way until Shipper-settled
*/ 
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.1.12'
SELECT  @NewVersion = '3.1.13'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 19 Dec 2012
-- Description:	trigger to 
--					1) validate any changes, and fail the update if invalid changes are submitted
--					2) add a unique, incrementing OrderNum to each new Order (manual Identity column)
--					3) recompute wait times (origin|destination based on times provided)
--					4) generate route table entry for any newly used origin-destination combination
--					5) keep Order consistent with Origin.Producer|Operator|UOM & Dest.UOM then the Origin|Dest change
--					6) roll the PrintStatus back to NOTFINALIZED (0) when status is rolled back
--					7) when DriverID is changed for an existing order, update the truck|trailer|trailer2 on other existing open orders to match that driver
--					8) update any ticket quantities for open orders when the UOM is changed for the Origin
--					9) ensure the LastChangeDateUTC is updated for any change to the order
--					10) if DBAudit is turned on, save an audit record for this Order change
-- =============================================
ALTER TRIGGER [dbo].[trigOrder_IU] ON [dbo].[tblOrder] AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

		-- Note: the TRIGGER_NESTLEVEL( OBJECT_ID('trigOrder_IU') ) = 1 statement prevents recursion
	IF (TRIGGER_NESTLEVEL( OBJECT_ID('trigOrder_IU')) = 1) BEGIN

		PRINT 'trigOrder_IU FIRED'

		/**********  START OF VALIDATION SECTION ************************/

		-- only allow the StatusID value to be changed on an audited order
		IF EXISTS (SELECT * FROM deleted WHERE StatusID = 4)
			AND (UPDATE(OrderNum) 
				OR UPDATE(PriorityID) 
				OR UPDATE(DueDate) 
				OR UPDATE(RouteID) 
				OR UPDATE(OriginID) 
				OR UPDATE(OriginArriveTimeUTC) 
				OR UPDATE(OriginDepartTimeUTC) 
				OR UPDATE(OriginMinutes) 
				OR UPDATE(OriginWaitNotes) 
				OR UPDATE(OriginBOLNum) 
				OR UPDATE(OriginGrossUnits) 
				OR UPDATE(OriginNetUnits) 
				OR UPDATE(DestinationID) 
				OR UPDATE(DestArriveTimeUTC) 
				OR UPDATE(DestDepartTimeUTC) 
				OR UPDATE(DestMinutes) 
				OR UPDATE(DestWaitNotes) 
				OR UPDATE(DestBOLNum) 
				OR UPDATE(DestGrossUnits) 
				OR UPDATE(DestNetUnits) 
				OR UPDATE(CustomerID) 
				OR UPDATE(CarrierID) 
				OR UPDATE(DriverID) 
				OR UPDATE(TruckID)
				OR UPDATE(TrailerID) 
				OR UPDATE(Trailer2ID) 
				OR UPDATE(OperatorID) 
				OR UPDATE(PumperID) 
				OR UPDATE(TicketTypeID) 
				OR UPDATE(Rejected) 
				OR UPDATE(RejectNotes) 
				OR UPDATE(ChainUp) 
				OR UPDATE(OriginTruckMileage) 
				OR UPDATE(OriginTankNum) 
				OR UPDATE(DestTruckMileage) 
				OR UPDATE(CarrierTicketNum) 
				OR UPDATE(AuditNotes) 
				OR UPDATE(CreateDateUTC) 
				-- it is permissible to change the ActualMiles value on Audited orders up to the point the Order has been Shipper-Settled
				OR (UPDATE(ActualMiles) AND EXISTS (SELECT * FROM deleted i JOIN tblOrderInvoiceCustomer OIC ON OIC.OrderID = i.ID WHERE OIC.BatchID IS NOT NULL))
				OR UPDATE(ProducerID) 
				OR UPDATE(CreatedByUser) 
				OR UPDATE(LastChangeDateUTC) 
				OR UPDATE(LastChangedByUser) 
				OR UPDATE(DeleteDateUTC) 
				OR UPDATE(DeletedByUser) 
				OR UPDATE(DestProductBSW)
				OR UPDATE(DestProductGravity) 
				OR UPDATE(DestProductTemp) 
				OR UPDATE(ProductID) 
				OR UPDATE(AcceptLastChangeDateUTC) 
				OR UPDATE(PickupLastChangeDateUTC) 
				OR UPDATE(DeliverLastChangeDateUTC) 
				OR UPDATE(OriginUomID) 
				OR UPDATE(DestUomID) 
				OR UPDATE(PickupPrintStatusID) 
				OR UPDATE(DeliverPrintStatusID)
				OR UPDATE(PickupPrintDateUTC) 
				OR UPDATE(DeliverPrintDateUTC) 
				OR UPDATE(OriginTankID) 
				OR UPDATE(OriginGrossStdUnits) 
				OR UPDATE(DispatchConfirmNum) 
				OR UPDATE(DispatchNotes) 
				OR UPDATE(DriverNotes) 
				OR UPDATE(OriginWaitReasonID) 
				OR UPDATE(DestWaitReasonID) 
				OR UPDATE(RejectReasonID) 
				OR UPDATE(DestOpenMeterUnits) 
				OR UPDATE(DestCloseMeterUnits))
		BEGIN
			RAISERROR('AUDITED orders cannot be modified!', 16, 1)
			IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
			RETURN
		END
		
		-- ensure the Origin and Destinations are both specified unless the Status is:
		--   (Generated, Assigned, Dispatched or Declined)
		ELSE IF  EXISTS(SELECT * FROM inserted O WHERE (O.OriginID IS NULL OR O.DestinationID IS NULL) AND O.StatusID NOT IN (-10,1,2,9))
		BEGIN
			RAISERROR('Invalid Order Status when the Origin and/or Or Destination is not specified', 16, 1)
			IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
			RETURN
		END
		/**********  END OF VALIDATION SECTION ************************/

		WHILE (SELECT count(*) FROM inserted i JOIN tblOrder O ON O.ID = i.ID WHERE O.OrderNum IS NULL) > 0
		BEGIN
			UPDATE tblOrder 
			  SET OrderNum = isnull((SELECT max(OrderNum) FROM tblOrder), 100000) + 1
				, CreateDateUTC = getutcdate()
			WHERE ID = (SELECT min(O.ID) FROM tblOrder O JOIN inserted i ON i.ID = O.ID WHERE O.OrderNum IS NULL)
		END
		
		-- re-compute the OriginMinutes (in case the website failed to compute it properly)
		IF UPDATE(OriginArriveTimeUTC) OR UPDATE(OriginDepartTimeUTC)
		BEGIN
			UPDATE tblOrder SET OriginMinutes = datediff(minute, i.OriginArriveTimeUTC, i.OriginDepartTimeUTC)
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
		END
		-- re-compute the DestWaitMinutes (in case the website failed to compute it properly)
		IF UPDATE(DestArriveTimeUTC) OR UPDATE(DestDepartTimeUTC)
		BEGIN
			UPDATE tblOrder SET DestMinutes = datediff(minute, i.DestArriveTimeUTC, i.DestDepartTimeUTC)
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
		END
		
		-- ensure missing Routes are created and assigned for the specified Origin/Destination combination
		IF UPDATE(OriginID) OR UPDATE(DestinationID)
		BEGIN
			-- create any missing Route records
			INSERT INTO tblRoute (OriginID, DestinationID, CreateDateUTC, CreatedByUser)
				SELECT i.OriginID, i.DestinationID, GETUTCDATE(), ISNULL(i.LastChangedByUser, i.CreatedByUser)
				FROM inserted i
				LEFT JOIN tblRoute R ON R.OriginID = i.OriginID AND R.DestinationID = i.DestinationID
				WHERE i.OriginID IS NOT NULL AND i.DestinationID IS NOT NULL AND R.ID IS NULL
			-- ensure the Order records refer to the correct Route (ID)
			UPDATE tblOrder SET RouteID = R.ID
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
			LEFT JOIN tblRoute R ON R.OriginID = O.OriginID AND R.DestinationID = O.DestinationID

			-- update the ActualMiles from the related Route
			UPDATE tblOrder SET ActualMiles = R.ActualMiles
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
			JOIN tblRoute R ON R.ID = O.RouteID
		END
		
		IF (UPDATE(OriginID))
		BEGIN
			-- update Order.ProducerID to match what is assigned to the new Origin
			UPDATE tblOrder SET ProducerID = OO.ProducerID
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			WHERE d.OriginID <> O.OriginID

			-- update Order.OriginUomID to match what is assigned to the new Origin (quantities are updated below)
			UPDATE tblOrder SET OriginUomID = OO.UomID
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			WHERE d.OriginID <> O.OriginID AND d.OriginUomID <> OO.UomID
		END
		
		-- keep the DestUomID in sync with the Destination (units are updated below)
		IF (UPDATE(DestinationID))
		BEGIN
			-- update Order.DestUomID to match what is assigned to the new Destination
			UPDATE tblOrder 
			  SET DestUomID = DD.UomID
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID
			JOIN tblDestination DD ON DD.ID = O.DestinationID
			WHERE d.DestinationID <> O.DestinationID AND d.DestUomID <> DD.UomID
		END
		
		-- ensure that any set PrintStatusID values are reset to 0 when backing up the general StatusID value
		IF (UPDATE(StatusID))
		BEGIN
			UPDATE tblOrder 
			  SET DeliverPrintStatusID = 0 
			  FROM tblOrder O
			  JOIN deleted d ON d.ID = O.ID
			WHERE O.StatusID <> d.StatusID AND O.StatusID IN (-10, 1, 2, 7, 8) -- Generated, Assigned, Dispatched, Accepted, Picked-Up

			UPDATE tblOrder 
			  SET PickupPrintStatusID = 0 
			  FROM tblOrder O
			  JOIN deleted d ON d.ID = O.ID
			WHERE O.StatusID <> d.StatusID AND O.StatusID IN (-10, 1, 2, 7) -- Generated, Assigned, Dispatched, Accepted
		END

		-- ensure that any change to the DriverID will update the Truck/Trailer/Trailer2 values to this driver's defaults
		IF (UPDATE(DriverID))
		BEGIN
			UPDATE tblOrder 
			  SET TruckID = DR.TruckID
				, TrailerID = DR.TrailerID
				, Trailer2ID = DR.Trailer2ID
				, AcceptLastChangeDateUTC = O.LastChangeDateUTC
			  FROM tblOrder O
			  JOIN deleted d ON d.ID = O.ID
			  LEFT JOIN tblDriver DR ON DR.ID = O.DriverID
			WHERE isnull(O.DriverID, 0) <> isnull(d.DriverID, 0)
			  AND (O.TruckID <> d.TruckID OR O.TrailerID <> d.TrailerID OR O.Trailer2ID <> d.Trailer2ID)
			  AND O.StatusID IN (-10,1,2,7,9) -- Generated, Assigned, Dispatched, Accepted, Declined
		END
		
		-- just updating the tickets here, trigOrderTicket_IU will in turn update this table
		-- ensure any existing OrderTickets are recomputed with the new OriginUOM
		UPDATE tblOrderTicket
		  SET GrossUnits = dbo.fnConvertUOM(GrossUnits, d.OriginUomID, i.OriginUomID)
			, NetUnits = dbo.fnConvertUOM(NetUnits, d.OriginUomID, i.OriginUomID)
		FROM tblOrderTicket OT
		JOIN tblOrder i ON i.ID = OT.OrderID
		JOIN deleted d ON d.ID = i.ID
		WHERE i.OriginUomID <> d.OriginUomID
		
		-- recalculate the Destination Unit values if only if the DestUomID changed but the values didn't
		BEGIN
			UPDATE tblOrder
			  SET DestGrossUnits = dbo.fnConvertUOM(O.DestGrossUnits, d.DestUomID, O.DestUomID)
				, DestNetUnits = dbo.fnConvertUOM(O.DestNetUnits, d.DestUomID, O.DestUomID)
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID
			WHERE d.DestUomID <> O.DestUomID 
			  AND d.DestGrossUnits = O.DestGrossUnits
			  AND d.DestNetUnits = O.DestNetUnits
		END
		--NOTE: we do not update the DestOpenMeterUnits/DestCloseMeterUnits since they don't auto update
		--  (and they simply represent a reading that is assumed to in the DestUomID UOM at entry)
		--  (if they are updated in the future [manually] the will calculate again using the DestUomID at that time)

		-- ensure that all changed orders have their LastChangeDateUTC set to UTCNOW
		UPDATE tblOrder
		  SET LastChangeDateUTC = GETUTCDATE()
			, LastChangedByUser = ISNULL(O.LastChangedByUser, 'System')
		FROM tblOrder O
		JOIN inserted i ON i.ID = O.ID
		JOIN deleted d ON d.ID = O.id
		-- ensure a lastchangedateutc value is set (and updated if it isn't close to NOW and was explicitly changed by the callee)
		WHERE O.LastChangeDateUTC IS NULL 
			--
			OR (i.LastChangeDateUTC = d.LastChangeDateUTC AND abs(datediff(second, O.LastChangeDateUTC, GETUTCDATE())) > 2)

		-- optionally add tblOrderDBAudit records
		BEGIN TRY
			IF EXISTS(SELECT * FROM tblSetting WHERE ID = 32 AND (Value LIKE 'true' OR Value LIKE 'yes'))
				INSERT INTO tblOrderDbAudit (DBAuditDate, [ID], [OrderNum], [StatusID], [PriorityID], [DueDate], [RouteID], [OriginID], [OriginArriveTimeUTC], [OriginDepartTimeUTC], [OriginMinutes], [OriginWaitNotes], [OriginBOLNum], [OriginGrossUnits], [OriginNetUnits], [DestinationID], [DestArriveTimeUTC], [DestDepartTimeUTC], [DestMinutes], [DestWaitNotes], [DestBOLNum], [DestGrossUnits], [DestNetUnits], [CustomerID], [CarrierID], [DriverID], [TruckID], [TrailerID], [Trailer2ID], [OperatorID], [PumperID], [TicketTypeID], [Rejected], [RejectNotes], [ChainUp], [OriginTruckMileage], [OriginTankNum], [DestTruckMileage], [CarrierTicketNum], [AuditNotes], [CreateDateUTC], [ActualMiles], [ProducerID], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser], [DestProductBSW], [DestProductGravity], [DestProductTemp], [ProductID], [AcceptLastChangeDateUTC], [PickupLastChangeDateUTC], [DeliverLastChangeDateUTC], [OriginUomID], [DestUomID], [PickupPrintStatusID], [DeliverPrintStatusID], [PickupPrintDateUTC], [DeliverPrintDateUTC], [OriginTankID], [OriginGrossStdUnits], [DispatchNotes], [DriverNotes], [DispatchConfirmNum], [OriginWaitReasonID], [DestWaitReasonID], [RejectReasonID], [DestOpenMeterUnits], [DestCloseMeterUnits])
					SELECT GETUTCDATE(), [ID], [OrderNum], [StatusID], [PriorityID], [DueDate], [RouteID], [OriginID], [OriginArriveTimeUTC], [OriginDepartTimeUTC], [OriginMinutes], [OriginWaitNotes], [OriginBOLNum], [OriginGrossUnits], [OriginNetUnits], [DestinationID], [DestArriveTimeUTC], [DestDepartTimeUTC], [DestMinutes], [DestWaitNotes], [DestBOLNum], [DestGrossUnits], [DestNetUnits], [CustomerID], [CarrierID], [DriverID], [TruckID], [TrailerID], [Trailer2ID], [OperatorID], [PumperID], [TicketTypeID], [Rejected], [RejectNotes], [ChainUp], [OriginTruckMileage], [OriginTankNum], [DestTruckMileage], [CarrierTicketNum], [AuditNotes], [CreateDateUTC], [ActualMiles], [ProducerID], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser], [DestProductBSW], [DestProductGravity], [DestProductTemp], [ProductID], [AcceptLastChangeDateUTC], [PickupLastChangeDateUTC], [DeliverLastChangeDateUTC], [OriginUomID], [DestUomID], [PickupPrintStatusID], [DeliverPrintStatusID], [PickupPrintDateUTC], [DeliverPrintDateUTC], [OriginTankID], [OriginGrossStdUnits], [DispatchNotes], [DriverNotes], [DispatchConfirmNum], [OriginWaitReasonID], [DestWaitReasonID], [RejectReasonID], [DestOpenMeterUnits], [DestCloseMeterUnits]
					FROM deleted d
		END TRY
		BEGIN CATCH
			PRINT 'trigOrder_IU.DBAUDIT FAILURE: ' + ERROR_MESSAGE()
		END CATCH

	PRINT 'trigOrder_IU COMPLETE'

	END
	
END

GO
EXEC sp_settriggerorder @triggername=N'[dbo].[trigOrder_IU]', @order=N'First', @stmttype=N'INSERT'
GO
EXEC sp_settriggerorder @triggername=N'[dbo].[trigOrder_IU]', @order=N'First', @stmttype=N'UPDATE'

COMMIT
SET NOEXEC OFF