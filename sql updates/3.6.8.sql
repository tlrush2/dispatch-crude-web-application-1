-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.6.7'
SELECT  @NewVersion = '3.6.8'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'Report Center: add Shipper Load Rate & Carrier Load Rate column definitions'
	UNION
	SELECT @NewVersion, 1, 'Report Center: relabel several other column definition "titles"'
	UNION
	SELECT @NewVersion, 1, 'Report Center: add Pickup & Deliver Print UTC column definitions'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/* Update three field names */
UPDATE tblReportColumnDefinition
SET Caption = 'ORIGIN | GENERAL | Origin CTB #'
WHERE ID = 240

UPDATE tblReportColumnDefinition
SET Caption = 'SETTLEMENT | SHIPPER | Shipper Origin Tax Rate %'
WHERE ID = 149

UPDATE tblReportColumnDefinition
SET Caption = 'SETTLEMENT | CARRIER | Carrier Origin Tax Rate %'
WHERE ID = 167

/* Add new fields to report center */
SET IDENTITY_INSERT tblReportColumnDefinition ON

IF NOT EXISTS (SELECT * FROM tblReportColumnDefinition WHERE ID = 241)
	INSERT INTO tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport)
	VALUES (241,1,'PickupPrintDateUTC','TRIP | TIMESTAMPS | Pickup Print Date (UTC)','[$-409]m/d/yy hh:mm AM/PM;@',NULL,1,NULL,0,'*',0)
	
IF NOT EXISTS (SELECT * FROM tblReportColumnDefinition WHERE ID = 242)
	INSERT INTO tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport)
	VALUES (242,1,'DeliverPrintDateUTC','TRIP | TIMESTAMPS | Deliver Print Date (UTC)','[$-409]m/d/yy hh:mm AM/PM;@',NULL,1,NULL,0,'*',0);
	
INSERT INTO tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport)
	SELECT 243, 1, 'CarrierLoadRate', 'SETTLEMENT | CARRIER | Carrier Load Rate', NULL, NULL, 1, NULL, 1, 'Administrator,Management,Carrier', 1
	UNION
	SELECT 244, 1, 'ShipperLoadRate', 'SETTLEMENT | SHIPPER | Shipper Load Rate', NULL, NULL, 1, NULL, 1, 'Administrator,Management,Carrier', 1
SET IDENTITY_INSERT tblReportColumnDefinition OFF
GO

COMMIT
SET NOEXEC OFF