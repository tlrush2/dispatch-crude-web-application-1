/*
	-- add DriverApp GPS parameter Settings
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.0.7'
SELECT  @NewVersion = '3.0.8'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

INSERT INTO tblSetting (ID, Name, SettingTypeID, Value, Category, ReadOnly, CreateDateUTC, CreatedByUser)
	SELECT 26, 'GPS Coarse Interval (meters)', 4, 1609, 'Driver App Settings', 0, GETUTCDATE(), 'System'
	UNION
	SELECT 27, 'GPS Coarse Interval (seconds)', 3, 300, 'Driver App Settings', 0, GETUTCDATE(), 'System'
	UNION
	SELECT 28, 'GPS Fine Interval (meters)', 4, 1609, 'Driver App Settings', 0, GETUTCDATE(), 'System'
	UNION
	SELECT 29, 'GPS Find Interval (seconds)', 3, 30, 'Driver App Settings', 0, GETUTCDATE(), 'System'
	UNION
	SELECT 30, 'GPS Fine Threshold (meters)', 4, 2000, 'Driver App Settings', 0, GETUTCDATE(), 'System'
GO

COMMIT
SET NOEXEC OFF
GO