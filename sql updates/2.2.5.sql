/* do not show deleted drivers on DriverAvailability
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.2.4'
SELECT  @NewVersion = '2.2.5'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

/***********************************************************/
-- Date Created: 3 Nov 2013
-- Author: Kevin Alons
-- Purpose: return all "potential" Driver Availability records for a date range
/***********************************************************/
ALTER PROCEDURE [dbo].[spDriverAvailability](@CarrierID int, @StartDate smalldatetime, @EndDate smalldatetime) AS BEGIN
	-- get a table with the entire specified date range (date only values)
	DECLARE @AvailDate smalldatetime
	SELECT @AvailDate = @StartDate, @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	CREATE TABLE #DateRange (AvailDate smalldatetime)
	WHILE (@AvailDate <= @EndDate) BEGIN
		INSERT INTO #DateRange VALUES (@AvailDate)
		SET @AvailDate = DATEADD(day, 1, @AvailDate)
	END
	
	SELECT dbo.fnDateMdYY(AvailDateOnly) AS AvailDate, AvailDateTime, CarrierID, Carrier, x.DriverID, Driver
		, MobilePhone, isnull(TOO.OpenOrders, 0) AS OpenOrders, isnull(DOO.OpenOrders, 0) AS DailyOpenOrders
	FROM (
		SELECT DA.ID
			, dbo.fnDateOnly(DR.AvailDate) AS AvailDateOnly, DA.AvailDateTime
			, D.CarrierID, D.Carrier, D.ID AS DriverID, D.FullName AS Driver
			, D.MobilePhone
		FROM (#DateRange DR
		CROSS JOIN viewDriver D)
		LEFT JOIN tblDriverAvailability DA ON DA.DriverID = D.ID AND dbo.fnDateOnly(DA.AvailDateTime) = DR.AvailDate
		WHERE (@CarrierID = -1 OR D.CarrierID = @CarrierID)
		  AND D.Active = 1
	) x
	LEFT JOIN (
		SELECT DriverID, count(*) AS OpenOrders 
		FROM tblOrder 
		WHERE StatusID IN (2,7,8) 
		GROUP BY DriverID
	) TOO ON TOO.DriverID = x.DriverID
	LEFT JOIN (
		SELECT DriverID, dbo.fnDateOnly(DueDate) AS DueDate, COUNT(*) AS OpenOrders
		FROM tblOrder 
		WHERE StatusID IN (2,7,8) 
		GROUP BY DriverID, dbo.fnDateOnly(DueDate)
	) DOO ON DOO.DriverID = x.DriverID AND DOO.DueDate = x.AvailDateOnly
	ORDER BY Carrier, Driver, AvailDateOnly
	
END

GO

COMMIT
SET NOEXEC OFF