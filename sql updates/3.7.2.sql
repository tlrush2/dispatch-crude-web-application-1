--backup database [dispatchcrude.dev] to disk = 'C:\files\Consulting\cts\dc_backups\dispatchcrude.dev.3.7.1.bak'
--restore database [DispatchCrude.Dev] from disk = 'C:\files\Consulting\cts\dc_backups\dispatchcrude.dev.3.7.1.bak'
--go
-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.7.1'
SELECT  @NewVersion = '3.7.2'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'GaugerApp: fix logic to include GENERATED orders (until they are to or past ASSIGNED order status)'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/**********************************************************/
-- Author: Kevin Alons
-- Date Created: 26 Sep 2013
-- Purpose: track changes to Orders that cause it be "virtually" deleted for a Driver (for DriveApp.Sync purposes)
--			track changes to Orders that cause it be "virtually" deleted for a Gauger (for GaugerApp.Sync purposes)
/**********************************************************/
ALTER TRIGGER trigOrder_U_VirtualDelete ON tblOrder AFTER UPDATE AS
BEGIN
	DECLARE @NewRecords TABLE (
		  OrderID int NOT NULL
		, DriverID int NOT NULL
		, LastChangeDateUTC smalldatetime NOT NULL
		, LastChangedByUser varchar(100) NULL
	)

	SET NOCOUNT ON;
	
	IF (TRIGGER_NESTLEVEL( OBJECT_ID('trigOrder_IU')) = 0 AND TRIGGER_NESTLEVEL( OBJECT_ID('trigOrderTicket_IU')) = 0
		AND EXISTS (SELECT * FROM inserted EXCEPT SELECT * FROM deleted)) 
	BEGIN
		PRINT 'trigOrder_U_VirtualDelete FIRED'
		
		-- delete any records that no longer apply because now the record is valid for the new driver/status
		DELETE FROM tblOrderDriverAppVirtualDelete
		FROM tblOrderDriverAppVirtualDelete ODAVD
		JOIN inserted i ON i.ID = ODAVD.OrderID AND i.DriverID = ODAVD.DriverID
		WHERE i.DeleteDateUTC IS NULL 
			AND dbo.fnPrintStatusID(i.StatusID, i.PickupPrintStatusID, i.DeliverPrintStatusID) IN (2,7,8,3)

		INSERT INTO @NewRecords
			-- insert any applicable VIRTUALDELETE records where the status changed from 
			--   a valid status to an invalid [DriverApp] status
			SELECT d.ID, d.DriverID, GETUTCDATE(), ISNULL(i.LastChangedByUser, 'System')
			FROM deleted d
			JOIN inserted i ON i.ID = D.ID
			WHERE dbo.fnPrintStatusID(i.StatusID, i.PickupPrintStatusID, i.DeliverPrintStatusID) NOT IN (2,7,8,3)
			  AND dbo.fnPrintStatusID(d.StatusID, d.PickupPrintStatusID, d.DeliverPrintStatusID) IN (2,7,8,3)
			  AND d.DriverID IS NOT NULL
			  
			UNION

			-- insert any records where the DriverID changed to another DriverID (for a valid status)
			SELECT d.ID, d.DriverID, GETUTCDATE(), ISNULL(i.LastChangedByUser, 'System')
			FROM deleted d
			JOIN inserted i ON i.ID = D.ID
			WHERE d.DriverID <> i.DriverID 
			  AND dbo.fnPrintStatusID(d.StatusID, d.PickupPrintStatusID, d.DeliverPrintStatusID) IN (2,7,8,3)
			  AND d.DriverID IS NOT NULL
			  
		-- update the VirtualDeleteDateUTC value for any existing records
		UPDATE tblOrderDriverAppVirtualDelete
		  SET VirtualDeleteDateUTC = NR.LastChangeDateUTC, VirtualDeletedByUser = NR.LastChangedByUser
		FROM tblOrderDriverAppVirtualDelete ODAVD
		JOIN @NewRecords NR ON NR.OrderID = ODAVD.OrderID AND NR.DriverID = ODAVD.DriverID

		-- insert the truly new VirtualDelete records	
		INSERT INTO tblOrderDriverAppVirtualDelete (OrderID, DriverID, VirtualDeleteDateUTC, VirtualDeletedByUser)
			SELECT NR.OrderID, NR.DriverID, NR.LastChangeDateUTC, NR.LastChangedByUser
			FROM @NewRecords NR
			LEFT JOIN tblOrderDriverAppVirtualDelete ODAVD ON ODAVD.OrderID = NR.OrderID AND ODAVD.DriverID = NR.DriverID
			WHERE ODAVD.ID IS NULL

		-- delete any records that no longer apply because now the record is valid for the new gauger/status
		DELETE FROM tblGaugerOrderVirtualDelete
		FROM tblGaugerOrderVirtualDelete GOVD
		JOIN tblGaugerOrder GAO ON GAO.OrderID = GOVD.OrderID
		JOIN inserted i ON i.ID = GOVD.OrderID AND GAO.GaugerID = GOVD.GaugerID
		WHERE i.StatusID IN (-9, -10)
			
		-- record that the gauger order is no longer in a status that can be viewed/edited by a Gauger
		INSERT INTO tblGaugerOrderVirtualDelete (OrderID, GaugerID, VirtualDeleteDateUTC, VirtualDeletedByUser)
			SELECT i.ID, GAO.GaugerID, i.LastChangeDateUTC, i.LastChangedByUser
			FROM inserted i 
			JOIN deleted d ON d.ID = i.ID
			JOIN tblGaugerOrder GAO ON GAO.OrderID = i.ID
			LEFT JOIN tblGaugerOrderVirtualDelete GOVD ON GOVD.OrderID = i.ID AND GOVD.GaugerID = GAO.GaugerID
			WHERE i.StatusID <> d.StatusID
			  AND GOVD.ID IS NULL
			  AND i.StatusID NOT IN (-9, -10)

	END
END

GO
EXEC sp_settriggerorder @triggername=N'[dbo].[trigOrder_U_VirtualDelete]', @order=N'Last', @stmttype=N'UPDATE'
GO

/* =============================================
-- Author:		Kevin Alons
-- Create date: 19 Dec 2012
-- Description:	trigger to 
				1) validate any changes, and fail the update if invalid changes are submitted
				2) update the tblOrder table with revelant and appropriate changes
				3) if DBAudit is turned on, save an audit record for this Order change
-- =============================================*/
ALTER TRIGGER trigGaugerOrder_U ON tblGaugerOrder AFTER UPDATE AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @changesFound bit
		-- Note: the TRIGGER_NESTLEVEL( OBJECT_ID('trigGaugerOrder_U') ) = 1 statement prevents recursion
	IF (TRIGGER_NESTLEVEL( OBJECT_ID('trigGaugerOrder_U')) = 1) BEGIN

		/**********  START OF VALIDATION SECTION ************************/
		IF (
			   UPDATE(TicketTypeID)
			OR UPDATE(StatusID)
			OR UPDATE(OriginTankID)
			OR UPDATE(OriginTankNum)
			OR UPDATE(ArriveTimeUTC)
			OR UPDATE(DepartTimeUTC)
			OR UPDATE(GaugerID)
			OR UPDATE(Rejected)
			OR UPDATE(RejectReasonID)
			OR UPDATE(RejectNotes)
			OR UPDATE(PrintDateUTC)
			OR UPDATE(GaugerNotes)
			OR UPDATE(DueDate)
			OR UPDATE(PriorityID)
			OR UPDATE(CreateDateUTC)
			OR UPDATE(CreatedByUser)
			OR UPDATE(LastChangeDateUTC)
			OR UPDATE(LastChangedByUser)
			OR UPDATE(DispatchNotes) 
		) 
		BEGIN
			IF EXISTS ( -- if any affected orders are in AUDITED status
				SELECT * 
				FROM tblOrder O
				JOIN inserted i ON i.OrderID = O.ID
				WHERE O.StatusID IN (4) -- AUDITED
			)
			BEGIN				
				-- if the "Allow Audited Order Update (temp only)" setting is TRUE then only warn when this occurs
				IF (dbo.fnToBool(dbo.fnSettingValue(35)) = 1)
					PRINT 'Gauger data for AUDITED order(s) are being modified - please investigate why!'
				ELSE BEGIN
					-- otherwise (normal behavior) - prevent AUDITED orders from being modified (except to UN-AUDIT them)
					RAISERROR('Gauger data for AUDITED orders cannot be modified!', 16, 1)
					IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
					RETURN
				END
			END
		END
		ELSE -- NO CHANGES DETECTED SO JUST QUIT
			RETURN

		PRINT 'trigGaugerOrder_U FIRED'
					
		/**********  END OF VALIDATION SECTION ************************/

		-- update the associated, relevant fields on the underlying order when base order is still in GENERATED status
		UPDATE tblOrder
			SET OriginTankID = i.OriginTankID
				, OriginTankNum = i.OriginTankNum
				, Rejected = i.Rejected
				, RejectReasonID = i.RejectReasonID
				, RejectNotes = i.RejectNotes
				, LastChangeDateUTC = i.LastChangeDateUTC
				, LastChangedByUser = i.LastChangedByUser
				-- advance the order to GENERATED if the Gauger Status is COMPLETED
				, StatusID = CASE WHEN GOS.IsComplete = 1 THEN -10 ELSE O.StatusID END
		FROM tblOrder O
		JOIN inserted i ON i.OrderID = O.ID
		JOIN deleted d ON d.OrderID = i.OrderID
		JOIN tblGaugerOrderStatus GOS ON GOS.ID = i.StatusID
		WHERE O.StatusID IN (-9, -10) -- GAUGER, GENERATED
		  AND (i.StatusID <> d.StatusID
			OR isnull(i.OriginTankID, 0) <> isnull(d.OriginTankID, 0)
			OR isnull(i.OriginTankNum, '') <> isnull(d.OriginTankNum, '')
			OR i.Rejected <> d.Rejected
			OR ISNULL(i.RejectReasonID, 0) <> ISNULL(d.RejectReasonID, 0)
			OR ISNULL(i.RejectNotes, '') <> ISNULL(d.RejectNotes, ''))
		
		-- delete any records that no longer apply because now the record is valid for the new gauger/status
		DELETE FROM tblGaugerOrderVirtualDelete
		FROM tblGaugerOrderVirtualDelete GOVD
		JOIN tblOrder O ON O.ID = GOVD.OrderID
		JOIN inserted i ON i.OrderID = GOVD.OrderID AND i.GaugerID = GOVD.GaugerID
		WHERE O.StatusID IN (-9, -10) -- GAUGER, GENERATED

		-- record that the gauger order used to belong to the former Gauger (show it as deleted for this user)
		INSERT INTO tblGaugerOrderVirtualDelete (OrderID, GaugerID, VirtualDeleteDateUTC, VirtualDeletedByUser)
			SELECT d.OrderID, d.GaugerID, i.LastChangeDateUTC, i.LastChangedByUser
			FROM deleted d
			JOIN inserted i ON i.OrderID = d.OrderID
			WHERE d.GaugerID <> i.GaugerID
			
		IF EXISTS (SELECT * FROM inserted EXCEPT SELECT * FROM deleted)
		BEGIN
			-- optionally add tblGaugerOrderDBAudit records
			BEGIN TRY
				IF EXISTS(SELECT * FROM tblSetting WHERE ID = 32 AND (Value LIKE 'true' OR Value LIKE 'yes'))
					INSERT INTO tblGaugerOrderDbAudit (DBAuditDate, OrderID, StatusID, OriginTankID, OriginTankNum, ArriveTimeUTC, DepartTimeUTC, GaugerID, TicketTypeID, Rejected, RejectReasonID, RejectNotes, Handwritten, PrintDateUTC, GaugerNotes, DueDate, PriorityID, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DispatchNotes)
						SELECT GETUTCDATE(), OrderID, StatusID, OriginTankID, OriginTankNum, ArriveTimeUTC, DepartTimeUTC, GaugerID, TicketTypeID, Rejected, RejectReasonID, RejectNotes, Handwritten, PrintDateUTC, GaugerNotes, DueDate, PriorityID, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DispatchNotes
						FROM deleted d
			END TRY
			BEGIN CATCH
				PRINT 'trigGaugerOrder_U.DBAUDIT FAILURE: ' + ERROR_MESSAGE()
			END CATCH
		END
		
	PRINT 'trigGaugerOrder_U COMPLETE'

	END
END

GO
EXEC sp_settriggerorder @triggername=N'[dbo].[trigGaugerOrder_U]', @order=N'First', @stmttype=N'UPDATE'
GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 18 Apr 2015
-- Description:	trigger for the following purposes:
--				1) validate data
--				2) push relevant changes to the tblOrderTicket table
--				3) DBAudit
-- =============================================
ALTER TRIGGER trigGaugerOrderTicket_IU ON tblGaugerOrderTicket AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	-- Note: the TRIGGER_NESTLEVEL( OBJECT_ID('trigGaugerOrderTicket_IU') ) = 1 statement prevents recursion
	IF (TRIGGER_NESTLEVEL( OBJECT_ID('trigGaugerOrderTicket_IU')) = 1) BEGIN
		
		-- only do anything if something actually changed
		IF EXISTS (SELECT * FROM inserted EXCEPT SELECT * FROM deleted)
		BEGIN
			PRINT 'trigGaugerOrderTicket_IU FIRED'
			
			/**********  START OF VALIDATION SECTION ************************/
			DECLARE @errorString varchar(255)

			IF EXISTS (
				SELECT * 
				FROM tblOrder 
				WHERE ID IN (SELECT OrderID FROM inserted) 
				  -- prevent changes to any tickets belonging to a Delivered or Audited Order
				  AND StatusID IN (4))
			BEGIN
				IF (dbo.fnToBool(dbo.fnSettingValue(35)) = 1)
					PRINT 'Gauger Ticket for an AUDITED orders is being modified - please investigate why!'
				ELSE 				
					SET @errorString = 'Gauger Tickets of Audited Orders cannot be modified'
			END

			-- prevent reassigning a ticket from one order to another
			IF EXISTS (
				SELECT i.* 
				FROM inserted i
				JOIN deleted d ON i.UID = d.UID
				WHERE i.OrderID <> d.OrderID)
			BEGIN
				SET @errorString = 'Tickets cannot be moved to a different Order'
			END

			IF EXISTS (
				SELECT OT.OrderID
				FROM tblGaugerOrderTicket OT
				JOIN inserted i ON i.OrderID = OT.OrderID AND i.CarrierTicketNum = OT.CarrierTicketNum AND i.UID <> OT.UID
				WHERE OT.DeleteDateUTC IS NULL and I.DeleteDateUTC IS NULL
				GROUP BY OT.OrderID, OT.CarrierTicketNum
				HAVING COUNT(*) > 1
			) 
			BEGIN
				SET @errorString = 'Duplicate active Ticket Numbers are not allowed'
			END
			
			ELSE IF EXISTS (SELECT * FROM inserted WHERE TicketTypeID IN (1) AND OriginTankID IS NULL AND TankNum IS NULL AND Rejected = 0 AND DeleteDateUTC IS NULL)
			BEGIN
				SET @errorString = 'Tank is required for "Gauge Run Basic" tickets'
			END

			ELSE IF EXISTS (SELECT * FROM inserted WHERE TicketTypeID IN (1) AND CarrierTicketNum IS NULL AND DeleteDateUTC IS NULL)
			BEGIN
				SET @errorString = 'Ticket # value is required for "Gauge Run Basic" tickets'
			END

			ELSE IF EXISTS (SELECT * FROM inserted WHERE DeleteDateUTC IS NULL
					AND (TicketTypeID IN (1) AND (ProductObsTemp IS NULL OR ProductObsGravity IS NULL OR ProductBSW IS NULL))
				)
			BEGIN
				SET @errorString = 'All Product Measurement values are required for "Gauge Run Basic" tickets'
			END
			
			ELSE IF EXISTS (SELECT * FROM inserted WHERE TicketTypeID IN (1) AND Rejected = 0 AND DeleteDateUTC IS NULL
				AND (OpeningGaugeFeet IS NULL OR OpeningGaugeInch IS NULL OR OpeningGaugeQ IS NULL))
			BEGIN
				SET @errorString = 'All Opening Gauge values are required for "Gauge Run Basic" tickets'
			END

			IF (@errorString IS NOT NULL)
			BEGIN
				RAISERROR(@errorString, 16, 1)
				IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
			END
			
			/**********  END OF VALIDATION SECTION ************************/
			
			-- update any existing OrderTicket records that are still only populated from Gauger entry
			UPDATE tblOrderTicket
				SET CarrierTicketNum = i.CarrierTicketNum
				, DispatchConfirmNum = i.DispatchConfirmNum
				, OriginTankID = i.OriginTankID
				, TankNum = i.TankNum
				, ProductObsGravity = i.ProductObsGravity
				, ProductObsTemp = i.ProductObsTemp
				, ProductBSW = i.ProductBSW
				, OpeningGaugeFeet = i.OpeningGaugeFeet
				, OpeningGaugeInch = i.OpeningGaugeInch
				, OpeningGaugeQ = i.OpeningGaugeQ
				, BottomFeet = i.BottomFeet
				, BottomInches = i.BottomInches
				, BottomQ = i.BottomQ
				, Rejected = i.Rejected
				, RejectReasonID = i.RejectReasonID
				, RejectNotes = i.RejectNotes
				, SealOff = i.SealOff
				, SealOn = i.SealOn
				, FromMobileApp = i.FromMobileApp
				, LastChangeDateUTC = i.LastChangeDateUTC
				, LastChangedByUser = i.LastChangedByUser
				, DeleteDateUTC = i.DeleteDateUTC
				, DeletedByUser = i.DeletedByUser
			FROM tblOrderTicket OT
			JOIN tblOrder O ON O.ID = OT.OrderID AND O.StatusID IN (-9, -10) -- GAUGER, GENERATED StatusID			
			JOIN inserted i ON i.UID = OT.UID AND i.CreateDateUTC = OT.CreateDateUTC
			LEFT JOIN deleted d ON d.UID = OT.UID AND isnull(d.LastChangeDateUTC, getutcdate()) = isnull(OT.LastChangeDateUTC, getutcdate())

			-- create new OrderTicket records not not present
			INSERT INTO tblOrderTicket (UID, OrderID, TicketTypeID, CarrierTicketNum, DispatchConfirmNum, OriginTankID, TankNum
				, ProductObsGravity, ProductObsTemp, ProductBSW, OpeningGaugeFeet, OpeningGaugeInch, OpeningGaugeQ
				, BottomFeet, BottomInches, BottomQ, Rejected, RejectReasonID, RejectNotes, SealOff, SealOn, FromMobileApp
				, CreateDateUTC, CreatedByUser)
				SELECT i.UID, i.OrderID, GTT.TicketTypeID, i.CarrierTicketNum, i.DispatchConfirmNum, i.OriginTankID, i.TankNum
					, i.ProductObsGravity, i.ProductObsTemp, i.ProductBSW, i.OpeningGaugeFeet, i.OpeningGaugeInch, i.OpeningGaugeQ
					, i.BottomFeet, i.BottomInches, i.BottomQ, i.Rejected, i.RejectReasonID, i.RejectNotes, i.SealOff, i.SealOn, i.FromMobileApp
					, i.CreateDateUTC, i.CreatedByUser
				FROM inserted i
				JOIN tblGaugerTicketType GTT ON GTT.ID = i.TicketTypeID
				LEFT JOIN tblOrderTicket OT ON OT.UID = i.UID
				WHERE OT.UID IS NULL

			/* START DB AUDIT *********************************************************/
			BEGIN TRY
				IF EXISTS(SELECT * FROM tblSetting WHERE ID = 32 AND (Value LIKE 'true' OR Value LIKE 'yes'))
					INSERT INTO tblGaugerOrderTicketDbAudit (DBAuditDate, UID, OrderID, CarrierTicketNum, TicketTypeID, TankNum, ProductObsGravity, ProductObsTemp, OpeningGaugeFeet, OpeningGaugeInch, OpeningGaugeQ, Rejected, RejectNotes, SealOff, SealOn, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser, FromMobileApp, OriginTankID, BottomFeet, BottomInches, BottomQ, RejectReasonID)
						SELECT GETUTCDATE(), UID, OrderID, CarrierTicketNum, TicketTypeID, TankNum, ProductObsGravity, ProductObsTemp, OpeningGaugeFeet, OpeningGaugeInch, OpeningGaugeQ, Rejected, RejectNotes, SealOff, SealOn, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser, FromMobileApp, OriginTankID, BottomFeet, BottomInches, BottomQ, RejectReasonID
						FROM deleted d
			END TRY
			BEGIN CATCH
				PRINT 'trigGaugerOrderTicket_IU.DBAUDIT FAILURE: ' + ERROR_MESSAGE()
			END CATCH
			/* END DB AUDIT *********************************************************/

			PRINT 'trigGaugerOrderTicket_IU COMPLETE'
		END
	END
END

GO
EXEC sp_settriggerorder @triggername=N'[dbo].[trigGaugerOrderTicket_IU]', @order=N'First', @stmttype=N'INSERT'
GO
EXEC sp_settriggerorder @triggername=N'[dbo].[trigGaugerOrderTicket_IU]', @order=N'First', @stmttype=N'UPDATE'
GO

/*******************************************/
-- Date Created: 13 Apr 2015
-- Author: Kevin Alons
-- Purpose: return readonly Order data for Gauger App sync
/*******************************************/
ALTER FUNCTION fnOrderReadOnly_GaugerApp(@GaugerID int, @LastChangeDateUTC datetime) RETURNS TABLE AS
RETURN 
	SELECT O.ID
		, O.OrderNum
		, GAO.StatusID
		, GAO.TicketTypeID
		, GAO.GaugerID
		, PriorityNum = cast(P.PriorityNum as int) 
		, O.Product
		, GAO.DueDate
		, O.Origin
		, O.OriginFull
		, OO.OriginType
		, O.OriginUomID
		, OriginStation = OO.Station 
		, OriginLeaseNum = OO.LeaseNum 
		, OriginCounty = OO.County 
		, OriginLegalDescription = OO.LegalDescription 
		, O.OriginNDIC
		, O.OriginNDM
		, O.OriginCA
		, O.OriginState
		, OriginAPI = OO.WellAPI
		, OriginLat = OO.LAT 
		, OriginLon = OO.LON 
		, OriginGeoFenceRadiusMeters = OO.GeoFenceRadiusMeters 
		, O.CreateDateUTC
		, O.CreatedByUser
		, O.LastChangeDateUTC
		, O.LastChangedByUser
		, DeleteDateUTC = isnull(GOVD.VirtualDeleteDateUTC, O.DeleteDateUTC) 
		, DeletedByUser = isnull(GOVD.VirtualDeletedByUser, O.DeletedByUser) 
		, O.OriginID
		, PriorityID = cast(O.PriorityID AS int) 
		, O.Operator
		, O.OperatorID
		, O.Pumper
		, O.PumperID
		, O.Producer
		, O.ProducerID
		, O.Customer
		, O.CustomerID
		, O.ProductID
		, TicketType = GTT.Name
		, PrintHeaderBlob = S.ZPLHeaderBlob 
		, EmergencyInfo = isnull(S.EmergencyInfo, 'For an emergency (spill, leak, fire, or accident) contact CHEMTREC @ 800.424.9300 (toll free)') 
		, O.OriginTankID
		, O.OriginTankNum
		, GAO.DispatchNotes
		, O.CarrierTicketNum
		, O.DispatchConfirmNum
		, OriginTimeZone = OO.TimeZone
		, OCTM.OriginThresholdMinutes
		, ShipperHelpDeskPhone = S.HelpDeskPhone
		, OriginDrivingDirections = OO.DrivingDirections
	FROM dbo.viewOrder O
	JOIN dbo.tblGaugerOrder GAO ON GAO.OrderID = O.ID
	JOIN tblGaugerTicketType GTT ON GTT.ID = GAO.TicketTypeID
	JOIN dbo.tblPriority P ON P.ID = GAO.PriorityID
	JOIN dbo.viewOrigin OO ON OO.ID = O.OriginID
	JOIN dbo.viewDestination D ON D.ID = O.DestinationID
	JOIN dbo.tblCustomer S ON S.ID = O.CustomerID
	JOIN dbo.tblRoute R ON R.ID = O.RouteID
	LEFT JOIN tblGaugerOrderVirtualDelete GOVD ON GOVD.OrderID = O.ID AND GOVD.GaugerID = GAO.GaugerID
	CROSS JOIN (SELECT DATEADD(second, -5, @LastChangeDateUTC) AS LCD) LCD
	OUTER APPLY dbo.fnOrderCombinedThresholdMinutes(O.ID) OCTM
	WHERE GAO.StatusID NOT IN (1) -- don't include REQUESTED Gauger Order records
	  AND O.ID IN (
		SELECT OrderID FROM tblGaugerOrder WHERE GaugerID = @GaugerID
		UNION 
		SELECT OrderID FROM tblGaugerOrderVirtualDelete WHERE GaugerID = @GaugerID
	)
	  AND (@LastChangeDateUTC IS NULL
		OR O.CreateDateUTC >= LCD.LCD
		OR O.LastChangeDateUTC >= LCD.LCD
		OR OO.DeleteDateUTC >= LCD.LCD
		OR GOVD.VirtualDeleteDateUTC >= LCD.LCD)
GO

/*******************************************/
-- Date Created: 31 Aug 2013
-- Author: Kevin Alons
-- Purpose: return driver editable Order data for Driver App sync
/*******************************************/
ALTER FUNCTION fnGaugerOrder_GaugerApp(@GaugerID int, @LastChangeDateUTC datetime) RETURNS TABLE AS
RETURN 
	SELECT GAO.OrderID
		, GAO.StatusID
		, GAO.GaugerID
		, GAO.ArriveTimeUTC
		, GAO.DepartTimeUTC
		, GAO.Rejected
		, GAO.RejectReasonID
		, GAO.RejectNotes
		, GAO.OriginTankID
		, GAO.OriginTankNum
		, GAO.GaugerNotes
		, GAO.DueDate
		, GAO.PriorityID
		, GAO.CreateDateUTC, GAO.CreatedByUser
		, GAO.LastChangeDateUTC, GAO.LastChangedByUser
		, DeleteDateUTC = isnull(GOVD.VirtualDeleteDateUTC, O.DeleteDateUTC), DeletedByUser = isnull(GOVD.VirtualDeletedByUser, O.DeletedByUser)
	FROM dbo.tblGaugerOrder GAO
	JOIN dbo.tblOrder O ON O.ID = GAO.OrderID
	LEFT JOIN tblGaugerOrderVirtualDelete GOVD ON GOVD.OrderID = GAO.OrderID AND GOVD.GaugerID = GAO.GaugerID
	CROSS JOIN (SELECT DATEADD(second, -5, isnull(@LastChangeDateUTC, dateadd(day, -30, getutcdate()))) AS LCD) LCD
	WHERE GAO.StatusID NOT IN (1) -- don't include REQUESTED Gauger Order records
	  AND GAO.OrderID IN (
		SELECT OrderID FROM tblGaugerOrder WHERE GaugerID = @GaugerID
		UNION 
		SELECT OrderID FROM tblGaugerOrderVirtualDelete WHERE GaugerID = @GaugerID
	)
	  AND (@LastChangeDateUTC IS NULL
		OR GAO.CreateDateUTC >= LCD.LCD
		OR GAO.LastChangeDateUTC >= LCD.LCD
		OR O.DeleteDateUTC >= LCD.LCD
		OR GOVD.VirtualDeleteDateUTC >= LCD.LCD)
GO

/*******************************************/
-- Date Created: 13 Apr 2015
-- Author: Kevin Alons
-- Purpose: return GaugerOrderTicket data for Gauger App sync
/*******************************************/
ALTER FUNCTION fnGaugerOrderTicket_GaugerApp(@GaugerID int, @LastChangeDateUTC datetime) RETURNS TABLE AS
RETURN SELECT OT.UID
		, OT.OrderID
		, OT.CarrierTicketNum
		, OT.OriginTankID
		, OT.DispatchConfirmNum
		, OT.TankNum
		, OT.TicketTypeID
		, OT.ProductObsGravity
		, OT.ProductObsTemp
		, OT.ProductBSW
		, cast(OT.OpeningGaugeFeet as tinyint) AS OpeningGaugeFeet
		, cast(OT.OpeningGaugeInch as tinyint) AS OpeningGaugeInch
		, cast(OT.OpeningGaugeQ as tinyint) AS OpeningGaugeQ
		, OT.BottomFeet
		, OT.BottomInches
		, OT.BottomQ
		, OT.Rejected
		, OT.RejectReasonID
		, OT.RejectNotes
		, OT.SealOff
		, OT.SealOn
		, OT.CreateDateUTC
		, OT.CreatedByUser
		, OT.LastChangeDateUTC
		, OT.LastChangedByUser
		, OT.DeleteDateUTC
		, OT.DeletedByUser
	FROM dbo.tblGaugerOrderTicket OT
	JOIN dbo.tblGaugerOrder GAO ON GAO.OrderID = OT.OrderID
	JOIN dbo.tblOrder O ON O.ID = GAO.OrderID
	LEFT JOIN tblGaugerOrderVirtualDelete GOVD ON GOVD.OrderID = GAO.OrderID AND GOVD.GaugerID = GAO.GaugerID
	CROSS JOIN (SELECT DATEADD(second, -5, @LastChangeDateUTC) AS LCD) LCD
	WHERE GAO.StatusID NOT IN (1) -- don't include REQUESTED Gauger Order records
	  AND GAO.OrderID IN (
		SELECT OrderID FROM tblGaugerOrder WHERE GaugerID = @GaugerID
		UNION 
		SELECT OrderID FROM tblGaugerOrderVirtualDelete WHERE GaugerID = @GaugerID
	)
	  AND (@LastChangeDateUTC IS NULL
		OR GAO.CreateDateUTC >= LCD.LCD
		OR GAO.LastChangeDateUTC >= LCD.LCD
		OR O.DeleteDateUTC >= LCD.LCD
		OR OT.CreateDateUTC >= LCD.LCD
		OR OT.LastChangeDateUTC >= LCD.LCD
		OR GOVD.VirtualDeleteDateUTC >= LCD.LCD)
GO

EXEC _spRebuildAllObjects
GO

/*******************************************/
-- Date Created: 5 Apr 2014
-- Author: Kevin Alons
-- Purpose: return OriginTank data for Gauger App sync
/*******************************************/
ALTER FUNCTION fnOriginTank_GaugerApp(@GaugerID int, @LastChangeDateUTC datetime) RETURNS TABLE AS
RETURN 
	SELECT DISTINCT OT.ID
		, OT.OriginID
		, OT.TankNum
		, OT.TankDescription
		, OT.CreateDateUTC
		, OT.CreatedByUser
		, OT.LastChangeDateUTC
		, OT.LastChangedByUser
		, OT.DeleteDateUTC
		, OT.DeletedByUser
	FROM dbo.tblOriginTank OT
	JOIN dbo.tblOrder O ON O.OriginID = OT.OriginID
	LEFT JOIN tblGaugerOrderVirtualDelete GOVD ON GOVD.OrderID = O.ID AND GOVD.GaugerID = @GaugerID
	CROSS JOIN (SELECT DATEADD(second, -5, @LastChangeDateUTC) AS LCD) LCD
	WHERE O.ID IN (
		SELECT OrderID FROM tblGaugerOrder WHERE GaugerID = @GaugerID
		UNION 
		SELECT OrderID FROM tblGaugerOrderVirtualDelete WHERE GaugerID = @GaugerID
	)
	  AND (@LastChangeDateUTC IS NULL
		OR O.CreateDateUTC >= LCD.LCD
		OR GOVD.VirtualDeleteDateUTC >= LCD.LCD
		OR OT.CreateDateUTC >= LCD.LCD
		OR OT.LastChangeDateUTC >= LCD.LCD
		)
GO

/*******************************************/
-- Date Created: 20 Apr 2015
-- Author: Kevin Alons
-- Purpose: return OriginTankStrapping data for Gauger App sync
/*******************************************/
ALTER FUNCTION fnOriginTankStrapping_GaugerApp( @GaugerID int, @LastChangeDateUTC datetime ) RETURNS TABLE AS
RETURN 
	SELECT DISTINCT OTS.*
		, cast(CASE WHEN OTSD.ID IS NULL THEN 0 ELSE 1 END as bit) AS IsDeleted
	FROM dbo.tblOriginTankStrapping OTS
	JOIN dbo.tblOriginTank OT ON OT.ID = OTS.OriginTankID
	JOIN dbo.tblOrder O ON O.OriginID = OT.OriginID
	LEFT JOIN tblGaugerOrderVirtualDelete GOVD ON GOVD.OrderID = O.ID AND GOVD.GaugerID = @GaugerID
	LEFT JOIN dbo.tblOriginTankStrappingDeleted OTSD ON OTSD.ID = OTS.ID
	CROSS JOIN (SELECT DATEADD(second, -5, @LastChangeDateUTC) AS LCD) LCD
	WHERE O.ID IN (
		SELECT OrderID FROM tblGaugerOrder WHERE GaugerID = @GaugerID
		UNION 
		SELECT OrderID FROM tblGaugerOrderVirtualDelete WHERE GaugerID = @GaugerID
	)
	  AND (@LastChangeDateUTC IS NULL
		OR O.CreateDateUTC >= LCD.LCD
		OR GOVD.VirtualDeleteDateUTC >= LCD.LCD
		OR OT.CreateDateUTC >= LCD.LCD
		OR OT.LastChangeDateUTC >= LCD.LCD
		OR OTS.CreateDateUTC >= LCD.LCD
		OR OTS.LastChangeDateUTC >= LCD.LCD
		OR OTSD.DeleteDateUTC >= LCD.LCD)
GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 6 Feb 2013
-- Description:	trigger to ensure the GrossBarrels and NetBarrels are computed for valid, entered data
-- =============================================
ALTER TRIGGER trigOrderTicket_IU ON tblOrderTicket AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	-- Note: the TRIGGER_NESTLEVEL( OBJECT_ID('trigOrderTicket_IU') ) = 1 statement prevents recursion
	IF (TRIGGER_NESTLEVEL( OBJECT_ID('trigOrderTicket_IU')) = 1) BEGIN
		
		-- only do anything if something actually changed
		IF EXISTS (SELECT * FROM inserted EXCEPT SELECT * FROM deleted)
		BEGIN
			PRINT 'trigOrderTicket_IU FIRED'
			
			/**********  START OF VALIDATION SECTION ************************/
			DECLARE @errorString varchar(1000); SET @errorString = ''

			IF EXISTS (
				SELECT * 
				FROM tblOrder 
				WHERE ID IN (SELECT OrderID FROM inserted) 
				  -- prevent changes to any tickets belonging to a Delivered or Audited Order
				  AND StatusID IN (4))
			BEGIN
				IF (dbo.fnToBool(dbo.fnSettingValue(35)) = 1)
					PRINT 'Ticket for an AUDITED orders is being modified - please investigate why!'
				ELSE 				
					SET @errorString = @errorString + '|Tickets of Audited Orders cannot be modified'
			END

			-- prevent reassigning a ticket from one order to another
			IF EXISTS (
				SELECT i.* 
				FROM inserted i
				JOIN deleted d ON i.UID = d.UID
				WHERE i.OrderID <> d.OrderID)
			BEGIN
				SET @errorString = @errorString + '|Tickets cannot be moved to a different Order'
			END
			
			IF EXISTS (
				SELECT OT.OrderID
				FROM tblOrderTicket OT
				JOIN tblOrder O ON O.ID = OT.OrderID
				JOIN inserted i ON i.OrderID = OT.OrderID AND i.CarrierTicketNum = OT.CarrierTicketNum AND i.ID <> OT.ID
				WHERE OT.DeleteDateUTC IS NULL and I.DeleteDateUTC IS NULL
				GROUP BY OT.OrderID, OT.CarrierTicketNum
				HAVING COUNT(*) > 1
			)
			BEGIN
				SET @errorString = @errorString + '|Duplicate Ticket Numbers are not allowed'
			END
			
			-- store all the tickets for orders that are not in Generated status and not deleted (only these need validation)
			SELECT i.*, O.StatusID 
			INTO #i
			FROM inserted i
			JOIN tblOrder O ON O.ID = i.OrderID
			WHERE i.DeleteDateUTC IS NULL
				
			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (3) AND BOLNum IS NULL)
			BEGIN
				SET @errorString = @errorString + '|BOL # value is required for Meter Run tickets'
			END
			
			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (1, 2) AND OriginTankID IS NULL AND TankNum IS NULL AND Rejected = 0)
			BEGIN
				SET @errorString = @errorString + '|Tank is required for Gauge Run & Net Volume tickets'
			END

			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (1) AND CarrierTicketNum IS NULL)
			BEGIN
				SET @errorString = @errorString + '|Ticket # value is required for Gauge Run tickets'
			END

			IF EXISTS (SELECT ID FROM #i WHERE (TicketTypeID IN (1, 2) AND (ProductObsTemp IS NULL OR ProductObsGravity IS NULL OR ProductBSW IS NULL))
						OR (TicketTypeID IN (1) AND StatusID NOT IN (-9, -10) AND (ProductHighTemp IS NULL OR ProductLowTemp IS NULL))
			)
			BEGIN
				SET @errorString = @errorString + '|All Product Measurement values are required for Gauge Run tickets'
			END
			
			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (1) AND Rejected = 0 
				AND (OpeningGaugeFeet IS NULL OR OpeningGaugeInch IS NULL OR OpeningGaugeQ IS NULL))
			BEGIN
				SET @errorString = @errorString + '|All Opening Gauge values are required for Gauge Run tickets'
			END
			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (1) AND Rejected = 0 AND StatusID NOT IN (-9, -10)
				AND (ClosingGaugeFeet IS NULL OR ClosingGaugeInch IS NULL OR ClosingGaugeQ IS NULL))
			BEGIN
				SET @errorString = @errorString + '|All Closing Gauge values are required for Gauge Run tickets'
			END

			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (2) AND Rejected = 0 AND StatusID NOT IN (-9, -10) AND (GrossUnits IS NULL))
			BEGIN
				SET @errorString = @errorString + '|Gross Volume value is required for Net Volume tickets'
			END

			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (7) AND Rejected = 0 AND StatusID NOT IN (-9, -10) AND (GrossUnits IS NULL))
			BEGIN
				SET @errorString = @errorString + '|Gross Volume value is required for Gauge Net tickets'
			END

			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (3) AND Rejected = 0 AND StatusID NOT IN (-9, -10) AND (GrossUnits IS NULL OR NetUnits IS NULL))
			BEGIN
				SET @errorString = @errorString + '|Gross & Net Volume values are required for Meter Run tickets'
			END

			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (1) AND Rejected = 0 AND StatusID NOT IN (-9, -10) AND (SealOff IS NULL OR SealOn IS NULL))
			BEGIN
				SET @errorString = @errorString + '|All Seal Off & Seal On values are required for Gauge Run tickets'
			END

			IF (len(@errorString) > 0)
			BEGIN
				SET @errorString = replace(substring(@errorString, 2, 1000), '|', char(13) + char(10))
				RAISERROR(@errorString, 16, 1)
				IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
				RETURN
			END
			/**********  END OF VALIDATION SECTION ************************/
			
			-- re-compute GaugeRun ticket Gross barrels
			IF UPDATE(OriginTankID) OR UPDATE(OpeningGaugeFeet) OR UPDATE(OpeningGaugeInch) OR UPDATE(OpeningGaugeQ)
				OR UPDATE (ClosingGaugeFeet) OR UPDATE(ClosingGaugeInch) OR UPDATE(ClosingGaugeQ) 
				OR UPDATE (GrossUnits)
			BEGIN
				UPDATE tblOrderTicket
				  SET GrossUnits = dbo.fnConvertUom(dbo.fnOriginTankStrappingLevelDeltaBarrels(
						OT.OriginTankID
					  , OpeningGaugeFeet
					  , OpeningGaugeInch
					  , OpeningGaugeQ
					  , ClosingGaugeFeet
					  , ClosingGaugeInch
					  , ClosingGaugeQ), 1, O.OriginUomID) -- ensure this value is always normalized from BARRELS
				FROM tblOrderTicket OT
				JOIN tblOrder O ON O.ID = OT.OrderID
				WHERE OT.ID IN (SELECT ID FROM inserted) 
				  AND OT.TicketTypeID = 1 -- Gauge Run Ticket
				  AND OT.OpeningGaugeFeet IS NOT NULL
				  AND OT.OpeningGaugeInch IS NOT NULL
				  AND OT.OpeningGaugeQ IS NOT NULL
				  AND OT.ClosingGaugeFeet IS NOT NULL
				  AND OT.ClosingGaugeInch IS NOT NULL
				  AND OT.ClosingGaugeQ IS NOT NULL
			END
			-- recompute the GrossUnits of any changed Meter Run tickets
			IF UPDATE(OpenMeterUnits) OR UPDATE(CloseMeterUnits) BEGIN
				--Print 'updating tblOrderTicket GOV from Open/Close Meter Units'
				UPDATE tblOrderTicket 
					SET GrossUnits = CloseMeterUnits - OpenMeterUnits
				WHERE TicketTypeID = 3  -- meter run tickets ONLY
				  AND OpenMeterUnits IS NOT NULL AND CloseMeterUnits IS NOT NULL
				
			END
			-- re-compute GaugeRun | Net Barrel | Meter Run tickets NetUnits
			IF UPDATE(OpeningGaugeFeet) OR UPDATE(OpeningGaugeInch) OR UPDATE(OpeningGaugeQ)
				OR UPDATE (ClosingGaugeFeet) OR UPDATE(ClosingGaugeInch) OR UPDATE(ClosingGaugeQ) 
				OR UPDATE(OriginTankID) OR UPDATE(GrossUnits) OR UPDATE(GrossStdUnits) OR UPDATE(NetUnits)
				OR UPDATE(ProductObsTemp) OR UPDATE(ProductObsGravity) OR UPDATE(ProductBSW)
				OR UPDATE(OpenMeterUnits) OR UPDATE(CloseMeterUnits)
			BEGIN
				--PRINT 'updating tblOrderTicket NSV/GSV'
				UPDATE tblOrderTicket
				  SET NetUnits = dbo.fnCrudeNetCalculator(GrossUnits, ProductObsTemp, ProductObsGravity, isnull(ProductBSW, 0))
					, GrossStdUnits = dbo.fnCrudeNetCalculator(GrossUnits, ProductObsTemp, ProductObsGravity, 0)
				WHERE ID IN (SELECT ID FROM inserted)
				  AND TicketTypeID IN (1, 2, 7) -- GAUGE RUN, NET VOLUME, GAUGE NET
				  AND GrossUnits IS NOT NULL
				  AND ProductObsTemp IS NOT NULL
				  AND ProductObsGravity IS NOT NULL
			END
			
			-- ensure the Order record is in-sync with the Tickets
			--declare @gov decimal(9,3); SELECT TOP 1 @gov = OT.GrossUnits FROM tblOrderTicket OT JOIN inserted i ON i.ID = OT.ID;
			--PRINT 'updating tblOrder GOV/GSV/NSV GOV = ' + ltrim(@gov)
			UPDATE tblOrder 
			SET OriginGrossUnits = (SELECT sum(GrossUnits) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND OT.Rejected = 0 AND OT.DeleteDateUTC IS NULL)
			  , OriginNetUnits = (SELECT sum(NetUnits) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND OT.Rejected = 0 AND OT.DeleteDateUTC IS NULL)
			  , OriginGrossStdUnits = (SELECT sum(GrossStdUnits) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND OT.Rejected = 0 AND OT.DeleteDateUTC IS NULL)

				-- use the first MeterRun/BasicRun BOLNum as the Order BOL Num (favor the first un-Rejected)
			  , OriginBOLNum = (
					SELECT TOP 1 BOLNum FROM (
						SELECT TOP 1 BOLNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID NOT IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
						UNION SELECT TOP 1 BOLNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID NOT IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X)
				-- use the first GaugeRun|NetVolume CarrierTicketNum as the Order.CarrierTicketNum (favor first un-Rejected)
			  , CarrierTicketNum = (
					SELECT TOP 1 CarrierTicketNum FROM (
						SELECT TOP 1 CarrierTicketNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
						UNION SELECT TOP 1 CarrierTicketNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X)
				-- use the first GaugeRun|NetVolume OriginTankID for the Order (favor first un-Rejected ticket)
			  , OriginTankID = (
					SELECT TOP 1 OriginTankID FROM (
						SELECT TOP 1 OriginTankID FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
						UNION SELECT TOP 1 OriginTankID FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X)
				-- use the first GaugeRun|NetVolume OriginTankNum for the Order (favor first un-Rejected ticket)
			  , OriginTankNum = (
					SELECT TOP 1 OriginTankNum FROM (
						SELECT TOP 1 OriginTankNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
						UNION SELECT TOP 1 OriginTankNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X)
			  , LastChangeDateUTC = (SELECT MAX(isnull(LastChangeDateUTC, CreateDateUTC)) FROM inserted WHERE OrderID = O.ID)
			  , LastChangedByUser = (SELECT MIN(isnull(LastChangedByUser, CreatedByUser)) FROM inserted WHERE OrderID = O.ID)
			FROM tblOrder O
			WHERE ID IN (SELECT DISTINCT OrderID FROM inserted)

			/* START DB AUDIT *********************************************************/
			BEGIN TRY
				IF EXISTS(SELECT * FROM tblSetting WHERE ID = 32 AND (Value LIKE 'true' OR Value LIKE 'yes'))
					INSERT INTO tblOrderTicketDbAudit (DBAuditDate, ID, OrderID, CarrierTicketNum, TicketTypeID, TankNum, ProductObsGravity, ProductObsTemp, OpeningGaugeFeet, OpeningGaugeInch, OpeningGaugeQ, ClosingGaugeFeet, ClosingGaugeInch, ClosingGaugeQ, GrossUnits, NetUnits, Rejected, RejectNotes, SealOff, SealOn, BOLNum, ProductHighTemp, ProductLowTemp, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser, UID, FromMobileApp, OriginTankID, GrossStdUnits, BottomFeet, BottomInches, BottomQ, RejectReasonID, MeterFactor, OpenMeterUnits, CloseMeterUnits)
						SELECT GETUTCDATE(), ID, OrderID, CarrierTicketNum, TicketTypeID, TankNum, ProductObsGravity, ProductObsTemp, OpeningGaugeFeet, OpeningGaugeInch, OpeningGaugeQ, ClosingGaugeFeet, ClosingGaugeInch, ClosingGaugeQ, GrossUnits, NetUnits, Rejected, RejectNotes, SealOff, SealOn, BOLNum, ProductHighTemp, ProductLowTemp, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser, UID, FromMobileApp, OriginTankID, GrossStdUnits, BottomFeet, BottomInches, BottomQ, RejectReasonID, MeterFactor, OpenMeterUnits, CloseMeterUnits
						FROM deleted d
			END TRY
			BEGIN CATCH
				PRINT 'trigOrderTicket_IU.DBAUDIT FAILURE: ' + ERROR_MESSAGE()
			END CATCH
			/* END DB AUDIT *********************************************************/

			PRINT 'trigOrderTicket_IU COMPLETE'
		END
	END	
END

GO
EXEC sp_settriggerorder @triggername=N'[dbo].[trigOrderTicket_IU]', @order=N'First', @stmttype=N'INSERT'
GO
EXEC sp_settriggerorder @triggername=N'[dbo].[trigOrderTicket_IU]', @order=N'First', @stmttype=N'UPDATE'
GO

COMMIT
SET NOEXEC OFF