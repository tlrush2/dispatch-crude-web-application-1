/*
	-- add Wait|Reject Reason code drop down functionality
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.0.12'
SELECT  @NewVersion = '3.1.0'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

CREATE TABLE tblOriginWaitReason
(
  ID int identity(1, 1) NOT NULL CONSTRAINT PK_OriginWaitReason PRIMARY KEY
, Num varchar(5) NOT NULL
, Description varchar(40) NOT NULL
, RequireNotes bit NOT NULL CONSTRAINT DF_OriginWaitReason_RequireNotes DEFAULT (0)
, CreateDateUTC datetime NOT NULL CONSTRAINT DF_OriginWaitReason_CreateDateUTC DEFAULT(getutcdate())
, CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_OriginWaitReason_CreatedByUser DEFAULT (suser_name())
, LastChangeDateUTC datetime NULL
, LastChangedByUser varchar(100) NULL
, DeleteDateUTC datetime NULL
, DeletedByUser varchar(100) NULL
)
GO
GRANT SELECT, INSERT, UPDATE ON tblOriginWaitReason TO dispatchcrude_iis_acct
GO
CREATE UNIQUE INDEX udxOriginWaitReason_Num ON tblOriginWaitReason(Num)
GO
CREATE UNIQUE INDEX udxOriginWaitReason_Description ON tblOriginWaitReason(Description)
GO
INSERT INTO tblOriginWaitReason (Num, Description, RequireNotes)
	SELECT 999, 'Other (notes required)', 1
GO

CREATE TABLE tblCarrierOriginWaitRates
(
  ID int identity(1, 1) NOT NULL CONSTRAINT PK_CarrierOriginWaitRates PRIMARY KEY
, OriginWaitReasonID int NOT NULL CONSTRAINT FK_CarrierOriginWaitRates_OriginWaitReason FOREIGN KEY REFERENCES tblOriginWaitReason(ID)
, CarrierID int NOT NULL CONSTRAINT FK_CarrierOriginWaitRates_Carrier FOREIGN KEY REFERENCES tblCarrier(ID)
, Rate decimal(9, 3) NOT NULL
, EffectiveDate smalldatetime NOT NULL
, CreateDateUTC datetime NOT NULL CONSTRAINT DF_CarrierOriginWaitRates_CreateDateUTC DEFAULT(getutcdate())
, CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_CarrierOriginWaitRates_CreatedByUser DEFAULT (suser_name())
, LastChangeDateUTC datetime NULL
, LastChangedByUser varchar(100) NULL
)
GO
CREATE UNIQUE INDEX udxCarrierOriginWaitRates_Unique ON tblCarrierOriginWaitRates(OriginWaitReasonID, CarrierID, EffectiveDate)
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblCarrierOriginWaitRates TO dispatchcrude_iis_acct
GO
CREATE TABLE tblCustomerOriginWaitRates
(
  ID int identity(1, 1) NOT NULL CONSTRAINT PK_CustomerOriginWaitRates PRIMARY KEY
, OriginWaitReasonID int NOT NULL CONSTRAINT FK_CustomerOriginWaitRates_OriginWaitReason FOREIGN KEY REFERENCES tblOriginWaitReason(ID)
, CustomerID int NOT NULL CONSTRAINT FK_CustomerOriginWaitRates_Customer FOREIGN KEY REFERENCES tblCustomer(ID)
, Rate decimal(9, 3) NOT NULL
, EffectiveDate smalldatetime NOT NULL
, CreateDateUTC datetime NOT NULL CONSTRAINT DF_CustomerOriginWaitRates_CreateDateUTC DEFAULT(getutcdate())
, CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_CustomerOriginWaitRates_CreatedByUser DEFAULT (suser_name())
, LastChangeDateUTC datetime NULL
, LastChangedByUser varchar(100) NULL
)
GO
CREATE UNIQUE INDEX udxCustomerOriginWaitRates_Unique ON tblCustomerOriginWaitRates(OriginWaitReasonID, CustomerID, EffectiveDate)
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblCustomerOriginWaitRates TO dispatchcrude_iis_acct
GO

CREATE TABLE tblDestinationWaitReason
(
  ID int identity(1, 1) NOT NULL CONSTRAINT PK_DestinationWaitReason PRIMARY KEY
, Num varchar(5) NOT NULL
, Description varchar(40) NOT NULL
, RequireNotes bit NOT NULL CONSTRAINT DF_DestinationWaitReason_RequireNotes DEFAULT (0)
, CreateDateUTC datetime NOT NULL CONSTRAINT DF_DestinationWaitReason_CreateDateUTC DEFAULT(getutcdate())
, CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_DestinationWaitReason_CreatedByUser DEFAULT (suser_name())
, LastChangeDateUTC datetime NULL
, LastChangedByUser varchar(100) NULL
, DeleteDateUTC datetime NULL
, DeletedByUser varchar(100) NULL
)
GO
GRANT SELECT, INSERT, UPDATE ON tblDestinationWaitReason TO dispatchcrude_iis_acct
GO
CREATE UNIQUE INDEX udxDestinationWaitReason_Num ON tblDestinationWaitReason(Num)
GO
CREATE UNIQUE INDEX udxDestinationWaitReason_Description ON tblDestinationWaitReason(Description)
GO
INSERT INTO tblDestinationWaitReason (Num, Description, RequireNotes)
	SELECT 999, 'Other (notes required)', 1
GO

CREATE TABLE tblCarrierDestinationWaitRates
(
  ID int identity(1, 1) NOT NULL CONSTRAINT PK_CarrierDestinationWaitRates PRIMARY KEY
, DestinationWaitReasonID int NOT NULL CONSTRAINT FK_CarrierDestinationWaitRates_DestinationWaitReason FOREIGN KEY REFERENCES tblDestinationWaitReason(ID)
, CarrierID int NOT NULL CONSTRAINT FK_CarrierDestinationWaitRates_Carrier FOREIGN KEY REFERENCES tblCarrier(ID)
, Rate decimal(9, 3) NOT NULL
, EffectiveDate smalldatetime NOT NULL
, CreateDateUTC datetime NOT NULL CONSTRAINT DF_CarrierDestinationWaitRates_CreateDateUTC DEFAULT(getutcdate())
, CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_CarrierDestinationWaitRates_CreatedByUser DEFAULT (suser_name())
, LastChangeDateUTC datetime NULL
, LastChangedByUser varchar(100) NULL
)
GO
CREATE UNIQUE INDEX udxCarrierDestinationWaitRates_Unique ON tblCarrierDestinationWaitRates(DestinationWaitReasonID, CarrierID, EffectiveDate)
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblCarrierDestinationWaitRates TO dispatchcrude_iis_acct
GO
CREATE TABLE tblCustomerDestinationWaitRates
(
  ID int identity(1, 1) NOT NULL CONSTRAINT PK_CustomerDestinationWaitRates PRIMARY KEY
, DestinationWaitReasonID int NOT NULL CONSTRAINT FK_CustomerDestinationWaitRates_DestinationWaitReason FOREIGN KEY REFERENCES tblDestinationWaitReason(ID)
, CustomerID int NOT NULL CONSTRAINT FK_CustomerDestinationWaitRates_Customer FOREIGN KEY REFERENCES tblCustomer(ID)
, Rate decimal(9, 3) NOT NULL
, EffectiveDate smalldatetime NOT NULL
, CreateDateUTC datetime NOT NULL CONSTRAINT DF_CustomerDestinationWaitRates_CreateDateUTC DEFAULT(getutcdate())
, CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_CustomerDestinationWaitRates_CreatedByUser DEFAULT (suser_name())
, LastChangeDateUTC datetime NULL
, LastChangedByUser varchar(100) NULL
)
GO
CREATE UNIQUE INDEX udxCustomerDestinationWaitRates_Unique ON tblCustomerDestinationWaitRates(DestinationWaitReasonID, CustomerID, EffectiveDate)
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblCustomerDestinationWaitRates TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 30 Aug 2014
-- Author: Kevin Alons
-- Purpose: return Carrier order stats (first active order and open order count)
/***********************************/
CREATE VIEW viewOrder_CarrierOrderInfo AS
SELECT CarrierID = C.ID
	, OpenOrderCount = (SELECT count(1) from viewOrder WHERE CarrierID = C.ID AND DeleteDateUTC IS NULL AND PrintStatusID NOT IN (-10,9,1,3,4)) 
	, OpenOrderMinDate = (SELECT MIN(OrderDate) from viewOrder WHERE CarrierID = C.ID AND DeleteDateUTC IS NULL AND PrintStatusID NOT IN (-10,9,1,3,4)) 
	, AuditOrderCount = (SELECT count(1) from viewOrder WHERE CarrierID = C.ID AND DeleteDateUTC IS NULL AND PrintStatusID IN (4)) 
	, AuditOrderMinDate = (SELECT MIN(OrderDate) from viewOrder WHERE CarrierID = C.ID AND DeleteDateUTC IS NULL AND PrintStatusID IN (4)) 
FROM viewCarrier C
GO
GRANT SELECT ON viewOrder_CarrierOrderInfo TO dispatchcrude_iis_acct
GO
/***********************************/
-- Date Created: 30 Aug 2014
-- Author: Kevin Alons
-- Purpose: return Customer order stats (first active order and open order count)
/***********************************/
CREATE VIEW viewOrder_CustomerOrderInfo AS
SELECT CustomerID = C.ID
	, OpenOrderCount = (SELECT count(1) from viewOrder WHERE CustomerID = C.ID AND DeleteDateUTC IS NULL AND PrintStatusID NOT IN (-10,9,1,3,4)) 
	, OpenOrderMinDate = (SELECT MIN(OrderDate) from viewOrder WHERE CustomerID = C.ID AND DeleteDateUTC IS NULL AND PrintStatusID NOT IN (-10,9,1,3,4)) 
	, AuditOrderCount = (SELECT count(1) from viewOrder WHERE CustomerID = C.ID AND DeleteDateUTC IS NULL AND PrintStatusID IN (4)) 
	, AuditOrderMinDate = (SELECT MIN(OrderDate) from viewOrder WHERE CustomerID = C.ID AND DeleteDateUTC IS NULL AND PrintStatusID IN (4)) 
FROM viewCustomer C
GO
GRANT SELECT ON viewOrder_CustomerOrderInfo TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 30 Aug 2014
-- Author: Kevin Alons
-- Purpose: return Carrier Origin Wait rates with computed Earliest & End Date values)
/***********************************/
CREATE VIEW [dbo].[viewCarrierOriginWaitRatesBase] AS
SELECT CR.*
	, (SELECT MIN(dateadd(day, -1, EffectiveDate)) FROM tblCarrierOriginWaitRates X WHERE X.CarrierID = CR.CarrierID AND X.OriginWaitReasonID = CR.OriginWaitReasonID AND X.EffectiveDate > CR.EffectiveDate) AS EndDate
	, (SELECT MAX(dateadd(day, 1, EffectiveDate)) FROM tblCarrierOriginWaitRates X WHERE X.CarrierID = CR.CarrierID AND X.OriginWaitReasonID = CR.OriginWaitReasonID AND X.EffectiveDate < CR.EffectiveDate) AS EarliestEffectiveDate
FROM tblCarrierOriginWaitRates CR

GO
GRANT SELECT ON viewCarrierOriginWaitRatesBase TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 30 Aug 2014
-- Author: Kevin Alons
-- Purpose: return Carrier Origin Wait rates with translated "friendly" values, computed Earliest & End Date values)
/***********************************/
CREATE VIEW [dbo].[viewCarrierOriginWaitRates] AS
SELECT RB.ID
	, OriginWaitReasonID = WR.ID
	, Rate = coalesce(RB.Rate, CRX.WaitFee, CRN.WaitFee, 0)
	, EffectiveDate = coalesce(RB.EffectiveDate, dbo.fnDateOnly(ORD.OpenOrderMinDate), cast(getdate() as date)) 
	, OpenOrderCount = isnull(ORD.OpenOrderCount, 0) 
	, RB.CreateDateUTC, RB.CreatedByUser, RB.LastChangeDateUTC, RB.LastChangedByUser
	, RB.EndDate
	, RB.EarliestEffectiveDate
	, CASE WHEN RB.ID IS NULL THEN 'Missing' ELSE 'Active' END AS Status
	, CarrierID = C.ID
	, Carrier = C.Name 
	, OriginWaitReasonNum = WR.Num
	, OriginWaitReasonDescription = WR.Description
FROM (viewCarrier C CROSS JOIN tblOriginWaitReason WR)
LEFT JOIN viewCarrierOriginWaitRatesBase RB ON RB.CarrierID = C.ID AND RB.OriginWaitReasonID = WR.ID
LEFT JOIN viewOrder_CarrierOrderInfo ORD ON ORD.CarrierID = C.ID
-- get the Carrier specific Carrier.WaitFee
LEFT JOIN (SELECT CarrierID, WaitFee FROM tblCarrierRates) CRX ON CRX.CarrierID = C.ID
-- get the generic Carrier.WaitFee
LEFT JOIN (SELECT CarrierID, WaitFee FROM tblCarrierRates WHERE CarrierID = -1 AND RegionID = -1) CRN ON CRN.CarrierID = -1

GO
GRANT SELECT ON viewCarrierOriginWaitRates TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 30 Aug 2014
-- Author: Kevin Alons
-- Purpose: return Customer Origin Wait rates with computed Earliest & End Date values)
/***********************************/
CREATE VIEW [dbo].[viewCustomerOriginWaitRatesBase] AS
SELECT CR.*
	, (SELECT MIN(dateadd(day, -1, EffectiveDate)) FROM tblCustomerOriginWaitRates X WHERE X.CustomerID = CR.CustomerID AND X.OriginWaitReasonID = CR.OriginWaitReasonID AND X.EffectiveDate > CR.EffectiveDate) AS EndDate
	, (SELECT MAX(dateadd(day, 1, EffectiveDate)) FROM tblCustomerOriginWaitRates X WHERE X.CustomerID = CR.CustomerID AND X.OriginWaitReasonID = CR.OriginWaitReasonID AND X.EffectiveDate < CR.EffectiveDate) AS EarliestEffectiveDate
FROM tblCustomerOriginWaitRates CR

GO
GRANT SELECT ON viewCustomerOriginWaitRatesBase TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 30 Aug 2014
-- Author: Kevin Alons
-- Purpose: return Customer Origin Wait rates with translated "friendly" values, computed Earliest & End Date values)
/***********************************/
CREATE VIEW [dbo].[viewCustomerOriginWaitRates] AS
SELECT RB.ID
	, OriginWaitReasonID = WR.ID
	, Rate = coalesce(RB.Rate, CRX.WaitFee, CRN.WaitFee, 0)
	, EffectiveDate = coalesce(RB.EffectiveDate, dbo.fnDateOnly(ORD.OpenOrderMinDate), cast(getdate() as date)) 
	, OpenOrderCount = isnull(ORD.OpenOrderCount, 0) 
	, RB.CreateDateUTC, RB.CreatedByUser, RB.LastChangeDateUTC, RB.LastChangedByUser
	, RB.EndDate
	, RB.EarliestEffectiveDate
	, CASE WHEN RB.ID IS NULL THEN 'Missing' ELSE 'Active' END AS Status
	, CustomerID = C.ID
	, Customer = C.Name 
	, OriginWaitReasonNum = WR.Num
	, OriginWaitReasonDescription = WR.Description
FROM (viewCustomer C CROSS JOIN tblOriginWaitReason WR)
LEFT JOIN viewCustomerOriginWaitRatesBase RB ON RB.CustomerID = C.ID AND RB.OriginWaitReasonID = WR.ID
LEFT JOIN viewOrder_CustomerOrderInfo ORD ON ORD.CustomerID = C.ID
-- get the Customer specific Customer.WaitFee
LEFT JOIN (SELECT CustomerID, WaitFee FROM tblCustomerRates) CRX ON CRX.CustomerID = C.ID
-- get the generic Carrier.WaitFee
LEFT JOIN (SELECT CustomerID, WaitFee FROM tblCustomerRates WHERE CustomerID = -1 AND RegionID = -1) CRN ON CRN.CustomerID = -1

GO
GRANT SELECT ON viewCustomerOriginWaitRates TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 30 Aug 2014
-- Author: Kevin Alons
-- Purpose: return Carrier Destination Wait rates with computed Earliest & End Date values)
/***********************************/
CREATE VIEW [dbo].[viewCarrierDestinationWaitRatesBase] AS
SELECT CR.*
	, (SELECT MIN(dateadd(day, -1, EffectiveDate)) FROM tblCarrierDestinationWaitRates X WHERE X.CarrierID = CR.CarrierID AND X.DestinationWaitReasonID = CR.DestinationWaitReasonID AND X.EffectiveDate > CR.EffectiveDate) AS EndDate
	, (SELECT MAX(dateadd(day, 1, EffectiveDate)) FROM tblCarrierDestinationWaitRates X WHERE X.CarrierID = CR.CarrierID AND X.DestinationWaitReasonID = CR.DestinationWaitReasonID AND X.EffectiveDate < CR.EffectiveDate) AS EarliestEffectiveDate
FROM tblCarrierDestinationWaitRates CR

GO
GRANT SELECT ON viewCarrierDestinationWaitRatesBase TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 30 Aug 2014
-- Author: Kevin Alons
-- Purpose: return Carrier Destination Wait rates with translated "friendly" values, computed Earliest & End Date values)
/***********************************/
CREATE VIEW [dbo].[viewCarrierDestinationWaitRates] AS
SELECT RB.ID
	, DestinationWaitReasonID = WR.ID
	, Rate = coalesce(RB.Rate, CRX.WaitFee, CRN.WaitFee, 0)
	, EffectiveDate = coalesce(RB.EffectiveDate, dbo.fnDateOnly(ORD.OpenOrderMinDate), cast(getdate() as date)) 
	, OpenOrderCount = isnull(ORD.OpenOrderCount, 0) 
	, RB.CreateDateUTC, RB.CreatedByUser, RB.LastChangeDateUTC, RB.LastChangedByUser
	, RB.EndDate
	, RB.EarliestEffectiveDate
	, CASE WHEN RB.ID IS NULL THEN 'Missing' ELSE 'Active' END AS Status
	, CarrierID = C.ID
	, Carrier = C.Name 
	, DestinationWaitReasonNum = WR.Num
	, DestinationWaitReasonDescription = WR.Description
FROM (viewCarrier C CROSS JOIN tblDestinationWaitReason WR)
LEFT JOIN viewCarrierDestinationWaitRatesBase RB ON RB.CarrierID = C.ID AND RB.DestinationWaitReasonID = WR.ID
LEFT JOIN viewOrder_CarrierOrderInfo ORD ON ORD.CarrierID = C.ID
-- get the Carrier specific Carrier.WaitFee
LEFT JOIN (SELECT CarrierID, WaitFee FROM tblCarrierRates) CRX ON CRX.CarrierID = C.ID
-- get the generic Carrier.WaitFee
LEFT JOIN (SELECT CarrierID, WaitFee FROM tblCarrierRates WHERE CarrierID = -1 AND RegionID = -1) CRN ON CRN.CarrierID = -1

GO
GRANT SELECT ON viewCarrierDestinationWaitRates TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 30 Aug 2014
-- Author: Kevin Alons
-- Purpose: return Customer Destination Wait rates with computed Earliest & End Date values)
/***********************************/
CREATE VIEW [dbo].[viewCustomerDestinationWaitRatesBase] AS
SELECT CR.*
	, (SELECT MIN(dateadd(day, -1, EffectiveDate)) FROM tblCustomerDestinationWaitRates X WHERE X.CustomerID = CR.CustomerID AND X.DestinationWaitReasonID = CR.DestinationWaitReasonID AND X.EffectiveDate > CR.EffectiveDate) AS EndDate
	, (SELECT MAX(dateadd(day, 1, EffectiveDate)) FROM tblCustomerDestinationWaitRates X WHERE X.CustomerID = CR.CustomerID AND X.DestinationWaitReasonID = CR.DestinationWaitReasonID AND X.EffectiveDate < CR.EffectiveDate) AS EarliestEffectiveDate
FROM tblCustomerDestinationWaitRates CR

GO
GRANT SELECT ON viewCustomerDestinationWaitRatesBase TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 30 Aug 2014
-- Author: Kevin Alons
-- Purpose: return Customer Destination Wait rates with translated "friendly" values, computed Earliest & End Date values)
/***********************************/
CREATE VIEW [dbo].[viewCustomerDestinationWaitRates] AS
SELECT RB.ID
	, DestinationWaitReasonID = WR.ID
	, Rate = coalesce(RB.Rate, CRX.WaitFee, CRN.WaitFee, 0)
	, EffectiveDate = coalesce(RB.EffectiveDate, dbo.fnDateOnly(ORD.OpenOrderMinDate), cast(getdate() as date)) 
	, OpenOrderCount = isnull(ORD.OpenOrderCount, 0) 
	, RB.CreateDateUTC, RB.CreatedByUser, RB.LastChangeDateUTC, RB.LastChangedByUser
	, RB.EndDate
	, RB.EarliestEffectiveDate
	, CASE WHEN RB.ID IS NULL THEN 'Missing' ELSE 'Active' END AS Status
	, CustomerID = C.ID
	, Customer = C.Name 
	, DestinationWaitReasonNum = WR.Num
	, DestinationWaitReasonDescription = WR.Description
FROM (viewCustomer C CROSS JOIN tblDestinationWaitReason WR)
LEFT JOIN viewCustomerDestinationWaitRatesBase RB ON RB.CustomerID = C.ID AND RB.DestinationWaitReasonID = WR.ID
LEFT JOIN viewOrder_CustomerOrderInfo ORD ON ORD.CustomerID = C.ID
-- get the Customer specific Customer.WaitFee
LEFT JOIN (SELECT CustomerID, WaitFee FROM tblCustomerRates) CRX ON CRX.CustomerID = C.ID
-- get the generic Carrier.WaitFee
LEFT JOIN (SELECT CustomerID, WaitFee FROM tblCustomerRates WHERE CustomerID = -1 AND RegionID = -1) CRN ON CRN.CustomerID = -1

GO
GRANT SELECT ON viewCustomerDestinationWaitRates TO dispatchcrude_iis_acct
GO

CREATE TABLE tblOrderRejectReason
(
  ID int identity(1, 1) NOT NULL CONSTRAINT PK_OrderRejectReason PRIMARY KEY
, Num varchar(5) NOT NULL
, Description varchar(40) NOT NULL
, RequireNotes bit NOT NULL CONSTRAINT DF_OrderRejectReason_RequireNotes DEFAULT (0)
, CreateDateUTC datetime NOT NULL CONSTRAINT DF_OrderRejectReason_CreateDateUTC DEFAULT(getutcdate())
, CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_OrderRejectReason_CreatedByUser DEFAULT (suser_name())
, LastChangeDateUTC datetime NULL
, LastChangedByUser varchar(100) NULL
, DeleteDateUTC datetime NULL
, DeletedByUser varchar(100) NULL
)
GO
GRANT SELECT, INSERT, UPDATE ON tblOrderRejectReason TO dispatchcrude_iis_acct
GO
CREATE UNIQUE INDEX udxOrderRejectReason_Num ON tblOrderRejectReason(Num)
GO
CREATE UNIQUE INDEX udxOrderRejectReason_Description ON tblOrderRejectReason(Description)
GO
INSERT INTO tblOrderRejectReason (Num, Description, RequireNotes)
	SELECT 999, 'Other (notes required)', 1
GO

CREATE TABLE tblCarrierOrderRejectRates
(
  ID int identity(1, 1) NOT NULL CONSTRAINT PK_CarrierOrderRejectRates PRIMARY KEY
, OrderRejectReasonID int NOT NULL CONSTRAINT FK_CarrierOrderRejectRates_OrderWaitReason FOREIGN KEY REFERENCES tblOrderRejectReason(ID)
, CarrierID int NOT NULL CONSTRAINT FK_CarrierOrderRejectRates_Carrier FOREIGN KEY REFERENCES tblCarrier(ID)
, Rate decimal(9, 3) NOT NULL
, EffectiveDate smalldatetime NOT NULL
, CreateDateUTC datetime NOT NULL CONSTRAINT DF_CarrierOrderRejectRates_CreateDateUTC DEFAULT(getutcdate())
, CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_CarrierOrderRejectRates_CreatedByUser DEFAULT (suser_name())
, LastChangeDateUTC datetime NULL
, LastChangedByUser varchar(100) NULL
)
GO
CREATE UNIQUE INDEX udxCarrierOrderWaitRates_Unique ON tblCarrierOrderRejectRates(OrderRejectReasonID, CarrierID, EffectiveDate)
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblCarrierOrderRejectRates TO dispatchcrude_iis_acct
GO
CREATE TABLE tblCustomerOrderRejectRates
(
  ID int identity(1, 1) NOT NULL CONSTRAINT PK_CustomerOrderRejectRates PRIMARY KEY
, OrderRejectReasonID int NOT NULL CONSTRAINT FK_CustomerOrderRejectRates_OrderWaitReason FOREIGN KEY REFERENCES tblOrderRejectReason(ID)
, CustomerID int NOT NULL CONSTRAINT FK_CustomerOrderRejectRates_Customer FOREIGN KEY REFERENCES tblCustomer(ID)
, Rate decimal(9, 3) NOT NULL
, EffectiveDate smalldatetime NOT NULL
, CreateDateUTC datetime NOT NULL CONSTRAINT DF_OrderWaitReason_CreateDateUTC DEFAULT(getutcdate())
, CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_OrderWaitReason_CreatedByUser DEFAULT (suser_name())
, LastChangeDateUTC datetime NULL
, LastChangedByUser varchar(100) NULL
)
GO
CREATE UNIQUE INDEX udxCustomerOrderWaitRates_Unique ON tblCustomerOrderRejectRates(OrderRejectReasonID, CustomerID, EffectiveDate)
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblCustomerOrderRejectRates TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 30 Aug 2014
-- Author: Kevin Alons
-- Purpose: return Carrier Order Reject rates with computed Earliest & End Date values)
/***********************************/
CREATE VIEW [dbo].[viewCarrierOrderRejectRatesBase] AS
SELECT CR.*
	, (SELECT MIN(dateadd(day, -1, EffectiveDate)) FROM tblCarrierOrderRejectRates X WHERE X.CarrierID = CR.CarrierID AND X.OrderRejectReasonID = CR.OrderRejectReasonID AND X.EffectiveDate > CR.EffectiveDate) AS EndDate
	, (SELECT MAX(dateadd(day, 1, EffectiveDate)) FROM tblCarrierOrderRejectRates X WHERE X.CarrierID = CR.CarrierID AND X.OrderRejectReasonID = CR.OrderRejectReasonID AND X.EffectiveDate < CR.EffectiveDate) AS EarliestEffectiveDate
FROM tblCarrierOrderRejectRates CR

GO
GRANT SELECT ON viewCarrierOrderRejectRatesBase TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 30 Aug 2014
-- Author: Kevin Alons
-- Purpose: return Carrier Order Reject rates with translated "friendly" values, computed Earliest & End Date values)
/***********************************/
CREATE VIEW [dbo].[viewCarrierOrderRejectRates] AS
SELECT RB.ID
	, OrderRejectReasonID = WR.ID
	, Rate = coalesce(RB.Rate, CRX.WaitFee, CRN.WaitFee, 0)
	, EffectiveDate = coalesce(RB.EffectiveDate, dbo.fnDateOnly(ORD.OpenOrderMinDate), cast(getdate() as date)) 
	, OpenOrderCount = isnull(ORD.OpenOrderCount, 0) 
	, RB.CreateDateUTC, RB.CreatedByUser, RB.LastChangeDateUTC, RB.LastChangedByUser
	, RB.EndDate
	, RB.EarliestEffectiveDate
	, CASE WHEN RB.ID IS NULL THEN 'Missing' ELSE 'Active' END AS Status
	, CarrierID = C.ID
	, Carrier = C.Name 
	, OrderRejectReasonNum = WR.Num
	, OrderRejectReasonDescription = WR.Description
FROM (viewCarrier C CROSS JOIN tblOrderRejectReason WR)
LEFT JOIN viewCarrierOrderRejectRatesBase RB ON RB.CarrierID = C.ID AND RB.OrderRejectReasonID = WR.ID
LEFT JOIN viewOrder_CarrierOrderInfo ORD ON ORD.CarrierID = C.ID
-- get the Carrier specific Carrier.WaitFee
LEFT JOIN (SELECT CarrierID, WaitFee FROM tblCarrierRates) CRX ON CRX.CarrierID = C.ID
-- get the generic Carrier.WaitFee
LEFT JOIN (SELECT CarrierID, WaitFee FROM tblCarrierRates WHERE CarrierID = -1 AND RegionID = -1) CRN ON CRN.CarrierID = -1

GO
GRANT SELECT ON viewCarrierOrderRejectRates TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 30 Aug 2014
-- Author: Kevin Alons
-- Purpose: return Customer Order Reject rates with computed Earliest & End Date values)
/***********************************/
CREATE VIEW [dbo].[viewCustomerOrderRejectRatesBase] AS
SELECT CR.*
	, (SELECT MIN(dateadd(day, -1, EffectiveDate)) FROM tblCustomerOrderRejectRates X WHERE X.CustomerID = CR.CustomerID AND X.OrderRejectReasonID = CR.OrderRejectReasonID AND X.EffectiveDate > CR.EffectiveDate) AS EndDate
	, (SELECT MAX(dateadd(day, 1, EffectiveDate)) FROM tblCustomerOrderRejectRates X WHERE X.CustomerID = CR.CustomerID AND X.OrderRejectReasonID = CR.OrderRejectReasonID AND X.EffectiveDate < CR.EffectiveDate) AS EarliestEffectiveDate
FROM tblCustomerOrderRejectRates CR

GO
GRANT SELECT ON viewCustomerOrderRejectRatesBase TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 30 Aug 2014
-- Author: Kevin Alons
-- Purpose: return Customer Order Reject rates with translated "friendly" values, computed Earliest & End Date values)
/***********************************/
CREATE VIEW [dbo].[viewCustomerOrderRejectRates] AS
SELECT RB.ID
	, OrderRejectReasonID = WR.ID
	, Rate = coalesce(RB.Rate, CRX.WaitFee, CRN.WaitFee, 0)
	, EffectiveDate = coalesce(RB.EffectiveDate, dbo.fnDateOnly(ORD.OpenOrderMinDate), cast(getdate() as date)) 
	, OpenOrderCount = isnull(ORD.OpenOrderCount, 0) 
	, RB.CreateDateUTC, RB.CreatedByUser, RB.LastChangeDateUTC, RB.LastChangedByUser
	, RB.EndDate
	, RB.EarliestEffectiveDate
	, CASE WHEN RB.ID IS NULL THEN 'Missing' ELSE 'Active' END AS Status
	, CustomerID = C.ID
	, Customer = C.Name 
	, OrderRejectReasonNum = WR.Num
	, OrderRejectReasonDescription = WR.Description
FROM (viewCustomer C CROSS JOIN tblOrderRejectReason WR)
LEFT JOIN viewCustomerOrderRejectRatesBase RB ON RB.CustomerID = C.ID AND RB.OrderRejectReasonID = WR.ID
LEFT JOIN viewOrder_CustomerOrderInfo ORD ON ORD.CustomerID = C.ID
-- get the Customer specific Customer.WaitFee
LEFT JOIN (SELECT CustomerID, WaitFee FROM tblCustomerRates) CRX ON CRX.CustomerID = C.ID
-- get the generic Carrier.WaitFee
LEFT JOIN (SELECT CustomerID, WaitFee FROM tblCustomerRates WHERE CustomerID = -1 AND RegionID = -1) CRN ON CRN.CustomerID = -1

GO
GRANT SELECT ON viewCustomerOrderRejectRates TO dispatchcrude_iis_acct
GO

CREATE TABLE tblOrderTicketRejectReason
(
  ID int identity(1, 1) NOT NULL CONSTRAINT PK_OrderTicketRejectReason PRIMARY KEY
, Num varchar(5) NOT NULL
, Description varchar(40) NOT NULL
, RequireNotes bit NOT NULL CONSTRAINT DF_OrderTicketRejectReason_RequireNotes DEFAULT (0)
, CreateDateUTC datetime NOT NULL CONSTRAINT DF_OrderTicketRejectReason_CreateDateUTC DEFAULT(getutcdate())
, CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_OrderTicketRejectReason_CreatedByUser DEFAULT (suser_name())
, LastChangeDateUTC datetime NULL
, LastChangedByUser varchar(100) NULL
, DeleteDateUTC datetime NULL
, DeletedByUser varchar(100) NULL
)
GO
GRANT SELECT, INSERT, UPDATE ON tblOrderTicketRejectReason TO dispatchcrude_iis_acct
GO
CREATE UNIQUE INDEX udxOrderTicketRejectReason_Num ON tblOrderTicketRejectReason(Num)
GO
CREATE UNIQUE INDEX udxOrderTicketRejectReason_Description ON tblOrderTicketRejectReason(Description)
GO
INSERT INTO tblOrderTicketRejectReason (Num, Description, RequireNotes)
	SELECT 999, 'Other (notes required)', 1
GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 12 Aug 2014
-- Description:	trigger to ensure the LastChangeDateUTC & LastChangedByUser are updated when update occur
--  NOTE: this is now separate from the basic trigOrder_IU so it can easily be disabled for system updates
-- =============================================
CREATE TRIGGER [dbo].[trigOrder_IU_LastChangeUpdate] ON [dbo].[tblOrder] AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;
	
	-- just updating the tickets here, trigOrderTicket_IU will in turn update this table
	-- Note: the TRIGGER_NESTLEVEL( OBJECT_ID('trigOrder_IU') ) = 1 statement prevents recursion
	IF (TRIGGER_NESTLEVEL( OBJECT_ID('trigOrder_IU_LastChangeUpdate')) = 1)
	BEGIN
		-- ensure that all changed orders have their LastChangeDateUTC set to UTCNOW
		UPDATE tblOrder
		  SET LastChangeDateUTC = GETUTCDATE()
			, LastChangedByUser = ISNULL(O.LastChangedByUser, 'System')
		FROM tblOrder O
		JOIN inserted i ON i.ID = O.ID
		JOIN deleted d ON d.ID = O.id
		-- ensure a lastchangedateutc value is set (and updated if it isn't close to NOW and was explicitly changed by the callee)
		WHERE O.LastChangeDateUTC IS NULL 
			--
			OR (i.LastChangeDateUTC = d.LastChangeDateUTC AND abs(datediff(second, O.LastChangeDateUTC, GETUTCDATE())) > 2)
	END
END

GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 19 Dec 2012
-- Description:	trigger to add a unique, incrementing OrderNum to each new Order (manual Identity column)
--				and other supporting/coordinating logic
-- =============================================
ALTER TRIGGER [dbo].[trigOrder_IU] ON [dbo].[tblOrder] AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	WHILE (SELECT count(*) FROM inserted i JOIN tblOrder O ON O.ID = i.ID WHERE O.OrderNum IS NULL) > 0
	BEGIN
		UPDATE tblOrder 
		  SET OrderNum = isnull((SELECT max(OrderNum) FROM tblOrder), 100000) + 1
			, CreateDateUTC = getutcdate()
		WHERE ID = (SELECT min(O.ID) FROM tblOrder O JOIN inserted i ON i.ID = O.ID WHERE O.OrderNum IS NULL)
	END
	
	-- re-compute the OriginMinutes (in case the website failed to compute it properly)
	IF UPDATE(OriginArriveTimeUTC) OR UPDATE(OriginDepartTimeUTC)
	BEGIN
		UPDATE tblOrder SET OriginMinutes = datediff(minute, i.OriginArriveTimeUTC, i.OriginDepartTimeUTC)
		FROM tblOrder O
		JOIN inserted i ON i.ID = O.ID
	END
	-- re-compute the DestWaitMinutes (in case the website failed to compute it properly)
	IF UPDATE(DestArriveTimeUTC) OR UPDATE(DestDepartTimeUTC)
	BEGIN
		UPDATE tblOrder SET DestMinutes = datediff(minute, i.DestArriveTimeUTC, i.DestDepartTimeUTC)
		FROM tblOrder O
		JOIN inserted i ON i.ID = O.ID
	END
	
	-- ensure missing Routes are created and assigned for the specified Origin/Destination combination
	IF UPDATE(OriginID) OR UPDATE(DestinationID)
	BEGIN
		-- create any missing Route records
		INSERT INTO tblRoute (OriginID, DestinationID, CreateDateUTC, CreatedByUser)
			SELECT i.OriginID, i.DestinationID, GETUTCDATE(), ISNULL(i.LastChangedByUser, i.CreatedByUser)
			FROM inserted i
			LEFT JOIN tblRoute R ON R.OriginID = i.OriginID AND R.DestinationID = i.DestinationID
			WHERE i.OriginID IS NOT NULL AND i.DestinationID IS NOT NULL AND R.ID IS NULL
		-- ensure the Order records refer to the correct Route (ID)
		UPDATE tblOrder SET RouteID = R.ID
		FROM tblOrder O
		JOIN inserted i ON i.ID = O.ID
		LEFT JOIN tblRoute R ON R.OriginID = O.OriginID AND R.DestinationID = O.DestinationID

		-- update the ActualMiles from the related Route
		UPDATE tblOrder SET ActualMiles = R.ActualMiles
		FROM tblOrder O
		JOIN inserted i ON i.ID = O.ID
		JOIN tblRoute R ON R.ID = O.RouteID
	END
	
	IF (UPDATE(OriginID))
	BEGIN
		-- update Order.ProducerID to match what is assigned to the new Origin
		UPDATE tblOrder SET ProducerID = OO.ProducerID
		FROM tblOrder O
		JOIN deleted d ON d.ID = O.ID
		JOIN tblOrigin OO ON OO.ID = O.OriginID
		WHERE d.OriginID <> O.OriginID

		-- update Order.OriginUomID to match what is assigned to the new Origin (quantities are updated below)
		UPDATE tblOrder SET OriginUomID = OO.UomID
		FROM tblOrder O
		JOIN deleted d ON d.ID = O.ID
		JOIN tblOrigin OO ON OO.ID = O.OriginID
		WHERE d.OriginID <> O.OriginID AND d.OriginUomID <> OO.UomID
	END
	
	-- keep the DestUomID in sync with the Destination (units are updated below)
	IF (UPDATE(DestinationID))
	BEGIN
		-- update Order.DestUomID to match what is assigned to the new Destination
		UPDATE tblOrder 
		  SET DestUomID = DD.UomID
		FROM tblOrder O
		JOIN deleted d ON d.ID = O.ID
		JOIN tblDestination DD ON DD.ID = O.DestinationID
		WHERE d.DestinationID <> O.DestinationID AND d.DestUomID <> DD.UomID
	END
	
	-- ensure that any set PrintStatusID values are reset to 0 when backing up the general StatusID value
	IF (UPDATE(StatusID))
	BEGIN
		UPDATE tblOrder 
		  SET DeliverPrintStatusID = 0 
		  FROM tblOrder O
		  JOIN deleted d ON d.ID = O.ID
		WHERE O.StatusID <> d.StatusID AND O.StatusID IN (-10, 1, 2, 7, 8)

		UPDATE tblOrder 
		  SET PickupPrintStatusID = 0 
		  FROM tblOrder O
		  JOIN deleted d ON d.ID = O.ID
		WHERE O.StatusID <> d.StatusID AND O.StatusID IN (-10, 1, 2, 7)
	END

	-- ensure that any change to the DriverID will update the Truck/Trailer/Trailer2 values to this driver's defaults
	IF (UPDATE(DriverID))
	BEGIN
		UPDATE tblOrder 
		  SET TruckID = DR.TruckID
			, TrailerID = DR.TrailerID
			, Trailer2ID = DR.Trailer2ID
		  FROM tblOrder O
		  JOIN deleted d ON d.ID = O.ID
		  JOIN tblDriver DR ON DR.ID = O.DriverID
		WHERE O.DriverID <> d.DriverID 
		  AND O.StatusID IN (-10,1,2,7,9) -- Generated, Assigned, Dispatched, Accepted, Declined
	END
	
	-- just updating the tickets here, trigOrderTicket_IU will in turn update this table
	-- Note: the TRIGGER_NESTLEVEL( OBJECT_ID('trigOrder_IU') ) = 1 statement prevents recursion
	IF (TRIGGER_NESTLEVEL( OBJECT_ID('trigOrder_IU')) = 1)
	BEGIN
		-- ensure any existing OrderTickets are recomputed with the new OriginUOM
		UPDATE tblOrderTicket
		  SET GrossUnits = dbo.fnConvertUOM(GrossUnits, d.OriginUomID, i.OriginUomID)
			, NetUnits = dbo.fnConvertUOM(NetUnits, d.OriginUomID, i.OriginUomID)
		FROM tblOrderTicket OT
		JOIN tblOrder i ON i.ID = OT.OrderID
		JOIN deleted d ON d.ID = i.ID
		WHERE i.OriginUomID <> d.OriginUomID

	END
	
	-- recalculate the Destination Unit values if only if the DestUomID changed but the values didn't
	BEGIN
		UPDATE tblOrder
		  SET DestGrossUnits = dbo.fnConvertUOM(O.DestGrossUnits, d.DestUomID, O.DestUomID)
			, DestNetUnits = dbo.fnConvertUOM(O.DestNetUnits, d.DestUomID, O.DestUomID)
		FROM tblOrder O
		JOIN deleted d ON d.ID = O.ID
		WHERE d.DestUomID <> O.DestUomID 
		  AND d.DestGrossUnits = O.DestGrossUnits
		  AND d.DestNetUnits = O.DestNetUnits
	END
	--NOTE: we do not update the DestOpenMeterUnits/DestClosignMeterUnits since they don't auto update
	--  (and they simply represent a reading that is assumed to in the DestUomID UOM at entry)
	--  (if they are updated in the future [manually] the will calculate again using the DestUomID at that time)
	
END

GO

-- this update should not update the LastChangeDateUTC or LastChangedByUser so disable this trigger
DISABLE TRIGGER trigOrder_IU_LastChangeUpdate ON tblOrder
GO

ALTER TABLE tblOrder ADD OriginWaitReasonID int NULL
	CONSTRAINT FK_Order_OriginWaitReason FOREIGN KEY REFERENCES tblOriginWaitReason(ID)
GO 

ALTER TABLE tblOrder ADD DestWaitReasonID int NULL
	CONSTRAINT FK_Order_DestWaitReason FOREIGN KEY REFERENCES tblDestinationWaitReason(ID)
GO
ALTER TABLE tblOrder ADD RejectReasonID int NULL
	CONSTRAINT FK_Order_RejectReason FOREIGN KEY REFERENCES tblOrderRejectReason(ID)
GO
ALTER TABLE tblOrderTicket ADD RejectReasonID int NULL
	CONSTRAINT FK_OrderTicket_RejectReason FOREIGN KEY REFERENCES tblOrderTicketRejectReason(ID)
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblOrderDbAudit]') AND type in (N'U'))
BEGIN
	ALTER TABLE tblOrderDbAudit ADD OriginWaitReasonID int NULL
	ALTER TABLE tblOrderDbAudit ADD DestWaitReasonID int NULL
	ALTER TABLE tblOrderDbAudit ADD RejectReasonID int NULL
END
GO

IF EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[trigOrder_U_dbaudit]'))
	DISABLE TRIGGER trigOrder_U_dbaudit ON tblOrder
GO
DISABLE TRIGGER trigOrderTicket_IU ON tblOrderTicket
GO
DISABLE TRIGGER trigOrderTicket_IU_Validate ON tblOrderTicket
GO

ENABLE TRIGGER trigOrder_IU_LastChangeUpdate ON tblOrder
GO
ENABLE TRIGGER trigOrderTicket_IU ON tblOrderTicket
GO
ENABLE TRIGGER trigOrderTicket_IU_Validate ON tblOrderTicket
GO

IF EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[trigOrder_U_dbaudit]'))
	ENABLE TRIGGER trigOrder_U_dbaudit ON tblOrder

EXEC _spRebuildAllObjects
GO

/***********************************/
-- Date Created: 30 Aug 2014
-- Author: Kevin Alons
-- Purpose: return the Origin Wait rate for the specified Carrier/Date combination
/***********************************/
CREATE FUNCTION [dbo].[fnCarrierOriginWaitRate](@CarrierID int, @OrderDate smalldatetime) RETURNS decimal(18, 3)
BEGIN
	DECLARE @ret decimal(18, 3)
	
	SELECT @ret = Rate
	FROM tblCarrierOriginWaitRates
	WHERE CarrierID = @CarrierID
	  AND EffectiveDate = (
		SELECT MAX(EffectiveDate)
		FROM tblCarrierOriginWaitRates
		WHERE CarrierID = @CarrierID
		  AND EffectiveDate <= @OrderDate)
	
	RETURN @ret
END

GO
GRANT EXECUTE ON fnCarrierOriginWaitRate TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 30 Aug 2014
-- Author: Kevin Alons
-- Purpose: return the Origin Wait rate for the specified Customer/Date combination
/***********************************/
CREATE FUNCTION [dbo].[fnCustomerOriginWaitRate](@CustomerID int, @OrderDate smalldatetime) RETURNS decimal(18, 3)
BEGIN
	DECLARE @ret decimal(18, 3)
	
	SELECT @ret = Rate
	FROM tblCustomerOriginWaitRates
	WHERE CustomerID = @CustomerID
	  AND EffectiveDate = (
		SELECT MAX(EffectiveDate)
		FROM tblCustomerOriginWaitRates
		WHERE CustomerID = @CustomerID
		  AND EffectiveDate <= @OrderDate)
	
	RETURN @ret
END

GO
GRANT EXECUTE ON fnCustomerOriginWaitRate TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 30 Aug 2014
-- Author: Kevin Alons
-- Purpose: return the Destination Wait rate for the specified Carrier/Date combination
/***********************************/
CREATE FUNCTION [dbo].[fnCarrierDestinationWaitRate](@CarrierID int, @OrderDate smalldatetime) RETURNS decimal(18, 3)
BEGIN
	DECLARE @ret decimal(18, 3)
	
	SELECT @ret = Rate
	FROM tblCarrierDestinationWaitRates
	WHERE CarrierID = @CarrierID
	  AND EffectiveDate = (
		SELECT MAX(EffectiveDate)
		FROM tblCarrierDestinationWaitRates
		WHERE CarrierID = @CarrierID
		  AND EffectiveDate <= @OrderDate)
	
	RETURN @ret
END

GO
GRANT EXECUTE ON fnCarrierDestinationWaitRate TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 30 Aug 2014
-- Author: Kevin Alons
-- Purpose: return the Destination Wait rate for the specified Customer/Date combination
/***********************************/
CREATE FUNCTION [dbo].[fnCustomerDestinationWaitRate](@CustomerID int, @OrderDate smalldatetime) RETURNS decimal(18, 3)
BEGIN
	DECLARE @ret decimal(18, 3)
	
	SELECT @ret = Rate
	FROM tblCustomerDestinationWaitRates
	WHERE CustomerID = @CustomerID
	  AND EffectiveDate = (
		SELECT MAX(EffectiveDate)
		FROM tblCustomerDestinationWaitRates
		WHERE CustomerID = @CustomerID
		  AND EffectiveDate <= @OrderDate)
	
	RETURN @ret
END

GO
GRANT EXECUTE ON fnCustomerDestinationWaitRate TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 30 Aug 2014
-- Author: Kevin Alons
-- Purpose: return the Origin Reject rate for the specified Carrier/Date combination
/***********************************/
CREATE FUNCTION [dbo].[fnCarrierOrderRejectRate](@CarrierID int, @OrderDate smalldatetime) RETURNS decimal(18, 3)
BEGIN
	DECLARE @ret decimal(18, 3)
	
	SELECT @ret = Rate
	FROM tblCarrierOrderRejectRates
	WHERE CarrierID = @CarrierID
	  AND EffectiveDate = (
		SELECT MAX(EffectiveDate)
		FROM tblCarrierOrderRejectRates
		WHERE CarrierID = @CarrierID
		  AND EffectiveDate <= @OrderDate)
	
	RETURN @ret
END

GO
GRANT EXECUTE ON fnCarrierOrderRejectRate TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 30 Aug 2014
-- Author: Kevin Alons
-- Purpose: return the Origin Reject rate for the specified Customer/Date combination
/***********************************/
CREATE FUNCTION [dbo].[fnCustomerOrderRejectRate](@CustomerID int, @OrderDate smalldatetime) RETURNS decimal(18, 3)
BEGIN
	DECLARE @ret decimal(18, 3)
	
	SELECT @ret = Rate
	FROM tblCustomerOrderRejectRates
	WHERE CustomerID = @CustomerID
	  AND EffectiveDate = (
		SELECT MAX(EffectiveDate)
		FROM tblCustomerOrderRejectRates
		WHERE CustomerID = @CustomerID
		  AND EffectiveDate <= @OrderDate)
	
	RETURN @ret
END

GO
GRANT EXECUTE ON fnCustomerOrderRejectRate TO dispatchcrude_iis_acct
GO

ALTER TABLE tblOrderInvoiceCarrier ADD OriginWaitRate decimal(9, 4) NULL
GO
ALTER TABLE tblOrderInvoiceCarrier ADD DestinationWaitRate decimal(9, 4) NULL
GO
UPDATE tblOrderInvoiceCarrier SET OriginWaitRate = WaitRate, DestinationWaitRate = WaitRate
GO
ALTER TABLE tblOrderInvoiceCarrier DROP COLUMN WaitRate
GO
ALTER TABLE tblOrderInvoiceCustomer ADD OriginWaitRate decimal(9, 4) NULL
GO
ALTER TABLE tblOrderInvoiceCustomer ADD DestinationWaitRate decimal(9, 4) NULL
GO
UPDATE tblOrderInvoiceCustomer SET OriginWaitRate = WaitRate, DestinationWaitRate = WaitRate
GO
ALTER TABLE tblOrderInvoiceCustomer DROP COLUMN WaitRate
GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Carrier "Settlement" $$ values to an Audited order
/***********************************/
ALTER PROCEDURE [dbo].[spProcessCarrierInvoice]
(
  @ID int
, @UserName varchar(100)
, @ChainupFee smallmoney = NULL
, @OriginWaitFee smallmoney = NULL
, @DestWaitFee smallmoney = NULL
, @RerouteFee smallmoney = NULL
, @RejectionFee smallmoney = NULL
, @H2SFee smallmoney = NULL
, @LoadFee smallmoney = NULL
) AS BEGIN
	DELETE FROM tblOrderInvoiceCarrier WHERE OrderID = @ID
	
	-- all Units and Rates are first normalized to the Order.OriginUOM then consistent processing
	-- and persisted in the Invoice (Settlement) record in this Order.OriginUOM 
	INSERT INTO tblOrderInvoiceCarrier (OrderID
		, ChainupFee, RerouteFee
		, WaitFeeSubUnitID, WaitFeeRoundingTypeID, BillableOriginWaitMinutes, BillableDestWaitMinutes, OriginWaitRate, DestinationWaitRate, OriginWaitFee, DestWaitFee
		, UomID, MinSettlementUnits, Units
		, RouteRate, H2SRate, TaxRate
		, RejectionFee, LoadFee, H2SFee, FuelSurcharge
		, TotalFee, CreateDateUTC, CreatedByUser)
	SELECT D.ID
		, ChainupFee, RerouteFee
		, WaitFeeSubUnitID, WaitFeeRoundingTypeID, BillableOriginWaitMinutes, BillableDestWaitMinutes, OriginWaitRate, DestinationWaitRate, OriginWaitFee, DestWaitFee
		, OrderOriginUomID, MinSettlementUnits, ActualUnits
		, RouteRate, H2SRate, TaxRate
		, RejectionFee, LoadFee, H2SFee, FuelSurcharge
		, RejectionFee + ChainupFee + RerouteFee + OriginWaitFee + DestWaitFee + H2SFee + LoadFee AS TotalFee
		, GETUTCDATE(), @UserName
	FROM (
		-- compute the actual fees or use the manual overide fee values (if provided)
		SELECT ID
			, coalesce(@ChainupFee, Chainup * ChainupFee, 0) AS ChainupFee
			, coalesce(@RerouteFee, RerouteFee * RerouteCount, 0) AS RerouteFee
			, WaitFeeSubUnitID
			, WaitFeeRoundingTypeID
			, cast(round(BillableOriginWaitHours * 60, 0) as int) AS BillableOriginWaitMinutes
			, cast(round(BillableDestWaitHours * 60, 0) as int) AS BillableDestWaitMinutes
			, OriginWaitRate
			, DestinationWaitRate
			, coalesce(@OriginWaitFee, BillableOriginWaitHours * OriginWaitRate, 0) AS OriginWaitFee
			, coalesce(@DestWaitFee, BillableDestWaitHours * DestinationWaitRate, 0) AS DestWaitFee
			, coalesce(@RejectionFee, Rejected * RejectionRate, 0) AS RejectionFee
			, H2SRate
			, MinSettlementUnits
			, ActualUnits
			, coalesce(@H2SFee, H2S * dbo.fnMaxDecimal(MinSettlementUnits, ActualUnits) * H2SRate, 0) AS H2SFee
			, isnull(TaxRate, 0) AS TaxRate
			, isnull(RouteRate, 0) AS RouteRate
			-- if rejected, use 0 AS the Rate
			, round(coalesce(@LoadFee, (1 - Rejected) * dbo.fnMaxDecimal(MinSettlementUnits, ActualUnits) * RouteRate, 0), 4) AS LoadFee
			, isnull(FuelSurcharge, 0) AS FuelSurcharge
			, OrderOriginUomID
		FROM (
			-- normalize the Accessorial Rates to Order.OriginUOM + add normalized Route Rate and other time normalization
			SELECT S.ID
				, S.ChainUp
				-- chainupFee is not UOM dependent
				, CR.ChainupFee
				-- reroutefee is not UOM dependent
				, CR.RerouteFee
				, S.RerouteCount
				, CR.WaitFeeSubUnitID
				, CR.WaitFeeRoundingTypeID
				, dbo.fnComputeBillableWaitHours(S.OriginWaitMinutes, CR.WaitFeeSubUnitID, CR.WaitFeeRoundingTypeID) AS BillableOriginWaitHours
				, dbo.fnComputeBillableWaitHours(S.DestWaitMinutes, CR.WaitFeeSubUnitID, CR.WaitFeeRoundingTypeID) AS BillableDestWaitHours
				-- waitFee is not UOM dependent
				, ISNULL(dbo.fnCarrierOriginWaitRate(S.CarrierID, S.OrderDate), CR.WaitFee) AS OriginWaitRate
				, ISNULL(dbo.fnCarrierDestinationWaitRate(S.CarrierID, S.OrderDate), CR.WaitFee) AS DestinationWaitRate
				, S.Rejected
				-- rejectionFee is not based on UOM
				, isnull(dbo.fnCarrierOrderRejectRate(S.CarrierID, S.OrderDate), CR.RejectionFee) AS RejectionRate
				, S.H2S
				-- normalize the Carrier H2SRate for Order.OriginUOM
				, dbo.fnConvertRateUOM(isnull(S.H2S * CR.H2SRate, 0), CR.UomID, S.OrderOriginUomID) AS H2SRate
				, S.TaxRate
				-- normalize the Order.UOM Route Rate for Origin.OriginUOM
				, dbo.fnCarrierRouteRate(S.CarrierID, S.RouteID, S.OrderDate, S.OrderOriginUomID) AS RouteRate
				, S.MinSettlementUnits
				, isnull(S.ActualUnits, 0) AS ActualUnits
				, CR.FuelSurcharge
				, S.OrderOriginUomID
			FROM (
				-- get the Order raw data (with Units Normalized to Origin UOM) and matching Carrier Accessorial Rate ID
				SELECT O.ID
					, O.CarrierID
					, O.RouteID
					, O.ChainUp
					-- get the correct SettlementFactor ActualUnits
					, CASE C.SettlementFactorID WHEN 1 THEN O.OriginGrossUnits WHEN 3 THEN isnull(O.OriginGrossStdUnits, O.OriginGrossUnits) ELSE coalesce(O.OriginNetUnits, O.OriginGrossStdUnits, O.OriginGrossUnits) END AS ActualUnits
					, O.OriginUomID AS OrderOriginUomID
					, OO.UomID AS OriginUomID
					, O.RerouteCount
					, O.OriginWaitMinutes
					, O.DestWaitMinutes
					, O.Rejected
					, OO.H2S
					, O.OrderDate
					, coalesce(CRXX.ID, CRXA.ID, CRAX.ID, CRAA.ID) AS CRID
					-- normalize the Carrier MinSettlementUnits for the Order.OriginUOM
					, isnull(dbo.fnConvertUOM(C.MinSettlementUnits, C.MinSettlementUomID, O.OriginUomID), 0) AS MinSettlementUnits
					, OO.TaxRate
				FROM dbo.viewOrderExportFull O
				JOIN tblOrigin OO ON OO.ID = O.OriginID
				JOIN tblCarrier C ON C.ID = O.CarrierID
				LEFT JOIN tblCarrierRates CRXX ON CRXX.CarrierID = O.CarrierID AND CRXX.RegionID = OO.RegionID
				LEFT JOIN tblCarrierRates CRXA ON CRXA.CarrierID = O.CarrierID AND CRXA.RegionID = -1
				LEFT JOIN tblCarrierRates CRAX ON CRAX.CarrierID = -1 AND CRAX.RegionID = OO.RegionID
				LEFT JOIN tblCarrierRates CRAA ON CRAA.CarrierID = -1 AND CRAA.RegionID = -1
				WHERE O.ID = @ID
			) S
			LEFT JOIN tblCarrierRates CR ON CR.ID = S.CRID
		) SS
	) D
END

GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Customer "Settlement" $$ values to an Audited order
/***********************************/
ALTER PROCEDURE [dbo].[spProcessCustomerInvoice]
(
  @ID int
, @UserName varchar(100)
, @ChainupFee smallmoney = NULL
, @OriginWaitFee smallmoney = NULL
, @DestWaitFee smallmoney = NULL
, @RerouteFee smallmoney = NULL
, @RejectionFee smallmoney = NULL
, @H2SFee smallmoney = NULL
, @LoadFee smallmoney = NULL
) AS BEGIN
	DELETE FROM tblOrderInvoiceCustomer WHERE OrderID = @ID
	
	-- all Units and Rates are first normalized to the Order.OriginUOM then consistent processing
	-- and persisted in the Invoice (Settlement) record in this Order.OriginUOM 
	INSERT INTO tblOrderInvoiceCustomer (OrderID
		, ChainupFee, RerouteFee
		, WaitFeeSubUnitID, WaitFeeRoundingTypeID, BillableOriginWaitMinutes, BillableDestWaitMinutes, OriginWaitRate, DestinationWaitRate, OriginWaitFee, DestWaitFee
		, UomID, MinSettlementUnits, Units
		, RouteRate, H2SRate, TaxRate
		, RejectionFee, LoadFee, H2SFee, FuelSurcharge
		, TotalFee, CreateDateUTC, CreatedByUser)
	SELECT D.ID
		, ChainupFee, RerouteFee
		, WaitFeeSubUnitID, WaitFeeRoundingTypeID, BillableOriginWaitMinutes, BillableDestWaitMinutes, OriginWaitRate, DestinationWaitRate, OriginWaitFee, DestWaitFee
		, OrderOriginUomID, MinSettlementUnits, ActualUnits
		, RouteRate, H2SRate, TaxRate
		, RejectionFee, LoadFee, H2SFee, FuelSurcharge
		, RejectionFee + ChainupFee + RerouteFee + OriginWaitFee + DestWaitFee + H2SFee + LoadFee AS TotalFee
		, GETUTCDATE(), @UserName
	FROM (
		-- compute the actual fees or use the manual overide fee values (if provided)
		SELECT ID
			, coalesce(@ChainupFee, Chainup * ChainupFee, 0) AS ChainupFee
			, coalesce(@RerouteFee, RerouteFee * RerouteCount, 0) AS RerouteFee
			, WaitFeeSubUnitID
			, WaitFeeRoundingTypeID
			, cast(round(BillableOriginWaitHours * 60, 0) as int) AS BillableOriginWaitMinutes
			, cast(round(BillableDestWaitHours * 60, 0) as int) AS BillableDestWaitMinutes
			, OriginWaitRate
			, DestinationWaitRate
			, coalesce(@OriginWaitFee, BillableOriginWaitHours * OriginWaitRate, 0) AS OriginWaitFee
			, coalesce(@DestWaitFee, BillableDestWaitHours * DestinationWaitRate, 0) AS DestWaitFee
			, coalesce(@RejectionFee, Rejected * RejectionRate, 0) AS RejectionFee
			, H2SRate
			, MinSettlementUnits
			, ActualUnits
			, coalesce(@H2SFee, H2S * dbo.fnMaxDecimal(MinSettlementUnits, ActualUnits) * H2SRate, 0) AS H2SFee
			, isnull(TaxRate, 0) AS TaxRate
			, isnull(RouteRate, 0) AS RouteRate
			-- if rejected, use 0 AS the Rate
			, round(coalesce(@LoadFee, (1 - Rejected) * dbo.fnMaxDecimal(MinSettlementUnits, ActualUnits) * RouteRate, 0), 4) AS LoadFee
			, isnull(FuelSurcharge, 0) AS FuelSurcharge
			, OrderOriginUomID
		FROM (
			-- normalize the Accessorial Rates to Order.OriginUOM + add normalized Route Rate and other time normalization
			SELECT S.ID
				, S.ChainUp
				-- chainupFee is not UOM dependent
				, CR.ChainupFee
				-- reroutefee is not UOM dependent
				, CR.RerouteFee
				, S.RerouteCount
				, CR.WaitFeeSubUnitID
				, CR.WaitFeeRoundingTypeID
				, dbo.fnComputeBillableWaitHours(S.OriginWaitMinutes, CR.WaitFeeSubUnitID, CR.WaitFeeRoundingTypeID) AS BillableOriginWaitHours
				, dbo.fnComputeBillableWaitHours(S.DestWaitMinutes, CR.WaitFeeSubUnitID, CR.WaitFeeRoundingTypeID) AS BillableDestWaitHours
				-- waitFee is not UOM dependent
				, ISNULL(dbo.fnCustomerOriginWaitRate(S.CustomerID, S.OrderDate), CR.WaitFee) AS OriginWaitRate
				, ISNULL(dbo.fnCustomerDestinationWaitRate(S.CustomerID, S.OrderDate), CR.WaitFee) AS DestinationWaitRate
				, S.Rejected
				-- rejectionFee is not based on UOM
				, isnull(dbo.fnCustomerOrderRejectRate(S.CustomerID, S.OrderDate), CR.RejectionFee) AS RejectionRate
				, S.H2S
				-- normalize the Customer H2SRate for Order.OriginUOM
				, dbo.fnConvertRateUOM(isnull(S.H2S * CR.H2SRate, 0), CR.UomID, S.OrderOriginUomID) AS H2SRate
				, S.TaxRate
				-- normalize the Order.UOM Route Rate for Origin.OriginUOM
				, dbo.fnCustomerRouteRate(S.CustomerID, S.RouteID, S.OrderDate, S.OrderOriginUomID) AS RouteRate
				, S.MinSettlementUnits
				, isnull(S.ActualUnits, 0) AS ActualUnits
				, CR.FuelSurcharge
				, S.OrderOriginUomID
			FROM (
				-- get the Order raw data (with Units Normalized to Origin UOM) and matching Customer Accessorial Rate ID
				SELECT O.ID
					, O.CustomerID
					, O.RouteID
					, O.ChainUp
					-- get the correct SettlementFactor ActualUnits
					, CASE C.SettlementFactorID WHEN 1 THEN O.OriginGrossUnits WHEN 3 THEN isnull(O.OriginGrossStdUnits, O.OriginGrossUnits) ELSE coalesce(O.OriginNetUnits, O.OriginGrossStdUnits, O.OriginGrossUnits) END AS ActualUnits
					, O.OriginUomID AS OrderOriginUomID
					, OO.UomID AS OriginUomID
					, O.RerouteCount
					, O.OriginWaitMinutes
					, O.DestWaitMinutes
					, O.Rejected
					, OO.H2S
					, O.OrderDate
					, coalesce(CRXX.ID, CRXA.ID, CRAX.ID, CRAA.ID) AS CRID
					-- normalize the Customer MinSettlementUnits for the Order.OriginUOM
					, isnull(dbo.fnConvertUOM(C.MinSettlementUnits, C.MinSettlementUomID, O.OriginUomID), 0) AS MinSettlementUnits
					, OO.TaxRate
				FROM dbo.viewOrderExportFull O
				JOIN tblOrigin OO ON OO.ID = O.OriginID
				JOIN tblCustomer C ON C.ID = O.CustomerID
				LEFT JOIN tblCustomerRates CRXX ON CRXX.CustomerID = O.CustomerID AND CRXX.RegionID = OO.RegionID
				LEFT JOIN tblCustomerRates CRXA ON CRXA.CustomerID = O.CustomerID AND CRXA.RegionID = -1
				LEFT JOIN tblCustomerRates CRAX ON CRAX.CustomerID = -1 AND CRAX.RegionID = OO.RegionID
				LEFT JOIN tblCustomerRates CRAA ON CRAA.CustomerID = -1 AND CRAA.RegionID = -1
				WHERE O.ID = @ID
			) S
			LEFT JOIN tblCustomerRates CR ON CR.ID = S.CRID
		) SS
	) D
END

GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details + FINANCIAL INFO into a single view
/***********************************/
ALTER VIEW [dbo].[viewOrder_Financial_Carrier] AS 
	SELECT OE.* 
		, dbo.fnOrderTicketDetails(OE.ID, 'TicketNums', '<br/>') AS TicketNums
		, dbo.fnOrderTicketDetails(OE.ID, 'TankNums', '<br/>') AS TankNums
		, dbo.fnRerouteDetails(OE.ID, 'PreviousDestinations', '<br/>') AS PreviousDestinations
		, dbo.fnRerouteDetails(OE.ID, 'RerouteUsers', '<br/>') AS RerouteUsers
		, dbo.fnRerouteDetails(OE.ID, 'RerouteDates', '<br/>') AS RerouteDates
		, dbo.fnRerouteDetails(OE.ID, 'RerouteNotes', '<br/>') AS RerouteNotes
		, InvoiceRatesAppliedDate = dbo.fnUTC_To_Local(ISNULL(OIC.LastChangeDateUTC, OIC.CreateDateUTC), OE.OriginTimeZoneID, OE.OriginUseDST)
		, OIC.BatchID
		, SB.BatchNum AS InvoiceBatchNum
		, OIC.ChainupFee AS InvoiceChainupFee
		, OIC.RerouteFee AS InvoiceRerouteFee
		, OIC.BillableOriginWaitMinutes AS InvoiceOriginBillableWaitMinutes
		, OIC.BillableDestWaitMinutes AS InvoiceDestBillableWaitMinutes
		, isnull(OIC.BillableOriginWaitMinutes, 0) + ISNULL(OIC.BillableDestWaitMinutes, 0) AS InvoiceTotalBillableWaitMinutes
		, isnull(WFSU.Name, 'None') AS InvoiceWaitFeeSubUnit
		, isnull(WFRT.Name, 'None') AS InvoiceWaitFeeRoundingType
		, OIC.OriginWaitRate AS InvoiceOriginWaitRate
		, OIC.DestinationWaitRate AS InvoiceDestinationWaitRate
		, OIC.OriginWaitFee AS InvoiceOriginWaitFee
		, OIC.DestWaitFee AS InvoiceDestWaitFee
		, ISNULL(OIC.OriginWaitFee, 0) + ISNULL(OIC.DestWaitFee, 0) AS InvoiceTotalWaitFee
		, OIC.RejectionFee AS InvoiceRejectionFee
		, OIC.H2SRate AS InvoiceH2SRate
		, OIC.H2SFee AS InvoiceH2SFee
		, OIC.TaxRate AS InvoiceTaxRate
		, U.Name AS InvoiceUom
		, U.Abbrev AS InvoiceUomShort
		, OIC.MinSettlementUnits AS InvoiceMinSettlementUnits
		, OIC.Units AS InvoiceUnits
		, OIC.RouteRate AS InvoiceRouteRate
		, OIC.LoadFee AS InvoiceLoadFee
		, OIC.TotalFee AS InvoiceTotalFee
		, OIC.FuelSurcharge AS InvoiceFuelSurcharge
	FROM dbo.viewOrderExportFull OE
	LEFT JOIN dbo.tblOrderInvoiceCarrier OIC ON OIC.OrderID = OE.ID
	LEFT JOIN dbo.tblCarrierSettlementBatch SB ON SB.ID = OIC.BatchID
	LEFT JOIN dbo.tblUom U ON U.ID = OIC.UomID	
	LEFT JOIN dbo.tblWaitFeeSubUnit WFSU ON WFSU.ID = OIC.WaitFeeSubUnitID
	LEFT JOIN dbo.tblWaitFeeRoundingType WFRT ON WFRT.ID = OIC.WaitFeeRoundingTypeID

GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details + FINANCIAL INFO into a single view
/***********************************/
ALTER VIEW [dbo].[viewOrder_Financial_Customer] AS 
	SELECT OE.* 
		, dbo.fnOrderTicketDetails(OE.ID, 'TicketNums', '<br/>') AS TicketNums
		, dbo.fnOrderTicketDetails(OE.ID, 'TankNums', '<br/>') AS TankNums
		, dbo.fnRerouteDetails(OE.ID, 'PreviousDestinations', '<br/>') AS PreviousDestinations
		, dbo.fnRerouteDetails(OE.ID, 'RerouteUsers', '<br/>') AS RerouteUsers
		, dbo.fnRerouteDetails(OE.ID, 'RerouteDates', '<br/>') AS RerouteDates
		, dbo.fnRerouteDetails(OE.ID, 'RerouteNotes', '<br/>') AS RerouteNotes
		, InvoiceRatesAppliedDate = dbo.fnUTC_To_Local(ISNULL(OIC.LastChangeDateUTC, OIC.CreateDateUTC), OE.OriginTimeZoneID, OE.OriginUseDST)
		, OIC.BatchID
		, SB.BatchNum AS InvoiceBatchNum
		, OIC.ChainupFee AS InvoiceChainupFee
		, OIC.RerouteFee AS InvoiceRerouteFee
		, OIC.BillableOriginWaitMinutes AS InvoiceOriginBillableWaitMinutes
		, OIC.BillableDestWaitMinutes AS InvoiceDestBillableWaitMinutes
		, isnull(OIC.BillableOriginWaitMinutes, 0) + ISNULL(OIC.BillableDestWaitMinutes, 0) AS InvoiceTotalBillableWaitMinutes
		, isnull(WFSU.Name, 'None') AS InvoiceWaitFeeSubUnit
		, isnull(WFRT.Name, 'None') AS InvoiceWaitFeeRoundingType
		, OIC.OriginWaitRate AS InvoiceOriginWaitRate
		, OIC.DestinationWaitRate AS InvoiceDestinationWaitRate
		, OIC.OriginWaitFee AS InvoiceOriginWaitFee
		, OIC.DestWaitFee AS InvoiceDestWaitFee
		, ISNULL(OIC.OriginWaitFee, 0) + ISNULL(OIC.DestWaitFee, 0) AS InvoiceTotalWaitFee
		, OIC.RejectionFee AS InvoiceRejectionFee
		, OIC.H2SRate AS InvoiceH2SRate
		, OIC.H2SFee AS InvoiceH2SFee
		, OIC.TaxRate AS InvoiceTaxRate
		, U.Name AS InvoiceUom
		, U.Abbrev AS InvoiceUomShort
		, OIC.MinSettlementUnits AS InvoiceMinSettlementUnits
		, OIC.Units AS InvoiceUnits
		, OIC.RouteRate AS InvoiceRouteRate
		, OIC.LoadFee AS InvoiceLoadFee
		, OIC.TotalFee AS InvoiceTotalFee
		, OIC.FuelSurcharge AS InvoiceFuelSurcharge
	FROM dbo.viewOrderExportFull OE
	LEFT JOIN dbo.tblOrderInvoiceCustomer OIC ON OIC.OrderID = OE.ID
	LEFT JOIN dbo.tblCustomerSettlementBatch SB ON SB.ID = OIC.BatchID
	LEFT JOIN dbo.tblUom U ON U.ID = OIC.UomID	
	LEFT JOIN dbo.tblWaitFeeSubUnit WFSU ON WFSU.ID = OIC.WaitFeeSubUnitID
	LEFT JOIN dbo.tblWaitFeeRoundingType WFRT ON WFRT.ID = OIC.WaitFeeRoundingTypeID

GO

/***********************************/
-- Date Created: 30 Aug 2014
-- Author: Kevin Alons
-- Purpose: add a new CarrierOriginWaitRate
/***********************************/
CREATE PROCEDURE [dbo].[spCarrierOriginWaitRate_Add]
(
  @CarrierID int
, @OriginWaitReasonID int
, @Rate decimal(9, 4)
, @EffectiveDate smalldatetime
, @ReplaceMatching bit
, @ReplaceAllOverlapping bit
, @UserName varchar(100)
, @Success bit = NULL out
, @Message varchar(255) = NULL out
) AS 
BEGIN
	DECLARE @removed int, @replaced int

	IF (@ReplaceMatching = 0)
	BEGIN
		IF ((SELECT count(1) FROM tblCarrierOriginWaitRates WHERE CarrierID=@CarrierID AND OriginWaitReasonID=@OriginWaitReasonID AND EffectiveDate=@EffectiveDate) > 0)
		BEGIN
			SELECT @Success = 0, @Message = 'Skipped: Existing Origin Wait Rate already exists'
			RETURN
		END
	END
	ELSE
	BEGIN
		-- remove any matching/overlapping rate records
		IF (@ReplaceAllOverlapping = 1)
		BEGIN
			SELECT @removed = count(1) FROM tblCarrierOriginWaitRates WHERE CarrierID=@CarrierID AND OriginWaitReasonID=@OriginWaitReasonID AND EffectiveDate>@EffectiveDate
			DELETE FROM tblCarrierOriginWaitRates WHERE ID IN (
				SELECT ID FROM viewCarrierRouteRates WHERE CarrierID=@CarrierID AND OriginWaitReasonID=@OriginWaitReasonID AND EffectiveDate>@EffectiveDate)
		END

		SELECT @replaced = count(1) FROM tblCarrierOriginWaitRates WHERE CarrierID=@CarrierID AND OriginWaitReasonID=@OriginWaitReasonID AND EffectiveDate=@EffectiveDate
		DELETE FROM tblCarrierOriginWaitRates WHERE CarrierID=@CarrierID AND OriginWaitReasonID=@OriginWaitReasonID AND EffectiveDate=@EffectiveDate
	END
	
	-- do the actual insert now
	INSERT INTO tblCarrierOriginWaitRates (CarrierID, OriginWaitReasonID, Rate, EffectiveDate, CreateDateUTC, CreatedByUser) 
		VALUES (@CarrierID, @OriginWaitReasonID, @Rate, @EffectiveDate, GETUTCDATE(), @UserName)
	SELECT @success = 1, @Message = 'Origin Wait Rate added successfully [' + ltrim(isnull(@replaced, 0)) + ' replaced, ' + ltrim(isnull(@removed, 0)) + ' deleted]'
END

GO

GRANT EXECUTE ON spCarrierOriginWaitRate_Add TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 30 Aug 2014
-- Author: Kevin Alons
-- Purpose: add a new CustomerOriginWaitRate
/***********************************/
CREATE PROCEDURE [dbo].[spCustomerOriginWaitRate_Add]
(
  @CustomerID int
, @OriginWaitReasonID int
, @Rate decimal(9, 4)
, @EffectiveDate smalldatetime
, @ReplaceMatching bit
, @ReplaceAllOverlapping bit
, @UserName varchar(100)
, @Success bit = NULL out
, @Message varchar(255) = NULL out
) AS 
BEGIN
	DECLARE @removed int, @replaced int

	IF (@ReplaceMatching = 0)
	BEGIN
		IF ((SELECT count(1) FROM tblCustomerOriginWaitRates WHERE CustomerID=@CustomerID AND OriginWaitReasonID=@OriginWaitReasonID AND EffectiveDate=@EffectiveDate) > 0)
		BEGIN
			SELECT @Success = 0, @Message = 'Skipped: Existing Origin Wait Rate already exists'
			RETURN
		END
	END
	ELSE
	BEGIN
		-- remove any matching/overlapping rate records
		IF (@ReplaceAllOverlapping = 1)
		BEGIN
			SELECT @removed = count(1) FROM tblCustomerOriginWaitRates WHERE CustomerID=@CustomerID AND OriginWaitReasonID=@OriginWaitReasonID AND EffectiveDate>@EffectiveDate
			DELETE FROM tblCustomerOriginWaitRates WHERE ID IN (
				SELECT ID FROM viewCustomerRouteRates WHERE CustomerID=@CustomerID AND OriginWaitReasonID=@OriginWaitReasonID AND EffectiveDate>@EffectiveDate)
		END

		SELECT @replaced = count(1) FROM tblCustomerOriginWaitRates WHERE CustomerID=@CustomerID AND OriginWaitReasonID=@OriginWaitReasonID AND EffectiveDate=@EffectiveDate
		DELETE FROM tblCustomerOriginWaitRates WHERE CustomerID=@CustomerID AND OriginWaitReasonID=@OriginWaitReasonID AND EffectiveDate=@EffectiveDate
	END
	
	-- do the actual insert now
	INSERT INTO tblCustomerOriginWaitRates (CustomerID, OriginWaitReasonID, Rate, EffectiveDate, CreateDateUTC, CreatedByUser) 
		VALUES (@CustomerID, @OriginWaitReasonID, @Rate, @EffectiveDate, GETUTCDATE(), @UserName)
	SELECT @success = 1, @Message = 'Origin Wait Rate added successfully [' + ltrim(isnull(@replaced, 0)) + ' replaced, ' + ltrim(isnull(@removed, 0)) + ' deleted]'
END

GO

GRANT EXECUTE ON spCustomerOriginWaitRate_Add TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 30 Aug 2014
-- Author: Kevin Alons
-- Purpose: add a new CarrierDestinationWaitRate
/***********************************/
CREATE PROCEDURE [dbo].[spCarrierDestinationWaitRate_Add]
(
  @CarrierID int
, @DestinationWaitReasonID int
, @Rate decimal(9, 4)
, @EffectiveDate smalldatetime
, @ReplaceMatching bit
, @ReplaceAllOverlapping bit
, @UserName varchar(100)
, @Success bit = NULL out
, @Message varchar(255) = NULL out
) AS 
BEGIN
	DECLARE @removed int, @replaced int

	IF (@ReplaceMatching = 0)
	BEGIN
		IF ((SELECT count(1) FROM tblCarrierDestinationWaitRates WHERE CarrierID=@CarrierID AND DestinationWaitReasonID=@DestinationWaitReasonID AND EffectiveDate=@EffectiveDate) > 0)
		BEGIN
			SELECT @Success = 0, @Message = 'Skipped: Existing Destination Wait Rate already exists'
			RETURN
		END
	END
	ELSE
	BEGIN
		-- remove any matching/overlapping rate records
		IF (@ReplaceAllOverlapping = 1)
		BEGIN
			SELECT @removed = count(1) FROM tblCarrierDestinationWaitRates WHERE CarrierID=@CarrierID AND DestinationWaitReasonID=@DestinationWaitReasonID AND EffectiveDate>@EffectiveDate
			DELETE FROM tblCarrierDestinationWaitRates WHERE ID IN (
				SELECT ID FROM viewCarrierRouteRates WHERE CarrierID=@CarrierID AND DestinationWaitReasonID=@DestinationWaitReasonID AND EffectiveDate>@EffectiveDate)
		END

		SELECT @replaced = count(1) FROM tblCarrierDestinationWaitRates WHERE CarrierID=@CarrierID AND DestinationWaitReasonID=@DestinationWaitReasonID AND EffectiveDate=@EffectiveDate
		DELETE FROM tblCarrierDestinationWaitRates WHERE CarrierID=@CarrierID AND DestinationWaitReasonID=@DestinationWaitReasonID AND EffectiveDate=@EffectiveDate
	END
	
	-- do the actual insert now
	INSERT INTO tblCarrierDestinationWaitRates (CarrierID, DestinationWaitReasonID, Rate, EffectiveDate, CreateDateUTC, CreatedByUser) 
		VALUES (@CarrierID, @DestinationWaitReasonID, @Rate, @EffectiveDate, GETUTCDATE(), @UserName)
	SELECT @success = 1, @Message = 'Destination Wait Rate added successfully [' + ltrim(isnull(@replaced, 0)) + ' replaced, ' + ltrim(isnull(@removed, 0)) + ' deleted]'
END

GO

GRANT EXECUTE ON spCarrierDestinationWaitRate_Add TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 30 Aug 2014
-- Author: Kevin Alons
-- Purpose: add a new CustomerDestinationWaitRate
/***********************************/
CREATE PROCEDURE [dbo].[spCustomerDestinationWaitRate_Add]
(
  @CustomerID int
, @DestinationWaitReasonID int
, @Rate decimal(9, 4)
, @EffectiveDate smalldatetime
, @ReplaceMatching bit
, @ReplaceAllOverlapping bit
, @UserName varchar(100)
, @Success bit = NULL out
, @Message varchar(255) = NULL out
) AS 
BEGIN
	DECLARE @removed int, @replaced int

	IF (@ReplaceMatching = 0)
	BEGIN
		IF ((SELECT count(1) FROM tblCustomerDestinationWaitRates WHERE CustomerID=@CustomerID AND DestinationWaitReasonID=@DestinationWaitReasonID AND EffectiveDate=@EffectiveDate) > 0)
		BEGIN
			SELECT @Success = 0, @Message = 'Skipped: Existing Destination Wait Rate already exists'
			RETURN
		END
	END
	ELSE
	BEGIN
		-- remove any matching/overlapping rate records
		IF (@ReplaceAllOverlapping = 1)
		BEGIN
			SELECT @removed = count(1) FROM tblCustomerDestinationWaitRates WHERE CustomerID=@CustomerID AND DestinationWaitReasonID=@DestinationWaitReasonID AND EffectiveDate>@EffectiveDate
			DELETE FROM tblCustomerDestinationWaitRates WHERE ID IN (
				SELECT ID FROM viewCustomerRouteRates WHERE CustomerID=@CustomerID AND DestinationWaitReasonID=@DestinationWaitReasonID AND EffectiveDate>@EffectiveDate)
		END

		SELECT @replaced = count(1) FROM tblCustomerDestinationWaitRates WHERE CustomerID=@CustomerID AND DestinationWaitReasonID=@DestinationWaitReasonID AND EffectiveDate=@EffectiveDate
		DELETE FROM tblCustomerDestinationWaitRates WHERE CustomerID=@CustomerID AND DestinationWaitReasonID=@DestinationWaitReasonID AND EffectiveDate=@EffectiveDate
	END
	
	-- do the actual insert now
	INSERT INTO tblCustomerDestinationWaitRates (CustomerID, DestinationWaitReasonID, Rate, EffectiveDate, CreateDateUTC, CreatedByUser) 
		VALUES (@CustomerID, @DestinationWaitReasonID, @Rate, @EffectiveDate, GETUTCDATE(), @UserName)
	SELECT @success = 1, @Message = 'Destination Wait Rate added successfully [' + ltrim(isnull(@replaced, 0)) + ' replaced, ' + ltrim(isnull(@removed, 0)) + ' deleted]'
END

GO

GRANT EXECUTE ON spCustomerDestinationWaitRate_Add TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 30 Aug 2014
-- Author: Kevin Alons
-- Purpose: add a new CarrierOrderRejectRate
/***********************************/
CREATE PROCEDURE [dbo].[spCarrierOrderRejectRate_Add]
(
  @CarrierID int
, @OrderRejectReasonID int
, @Rate decimal(9, 4)
, @EffectiveDate smalldatetime
, @ReplaceMatching bit
, @ReplaceAllOverlapping bit
, @UserName varchar(100)
, @Success bit = NULL out
, @Message varchar(255) = NULL out
) AS 
BEGIN
	DECLARE @removed int, @replaced int

	IF (@ReplaceMatching = 0)
	BEGIN
		IF ((SELECT count(1) FROM tblCarrierOrderRejectRates WHERE CarrierID=@CarrierID AND OrderRejectReasonID=@OrderRejectReasonID AND EffectiveDate=@EffectiveDate) > 0)
		BEGIN
			SELECT @Success = 0, @Message = 'Skipped: Existing Origin Wait Rate already exists'
			RETURN
		END
	END
	ELSE
	BEGIN
		-- remove any matching/overlapping rate records
		IF (@ReplaceAllOverlapping = 1)
		BEGIN
			SELECT @removed = count(1) FROM tblCarrierOrderRejectRates WHERE CarrierID=@CarrierID AND OrderRejectReasonID=@OrderRejectReasonID AND EffectiveDate>@EffectiveDate
			DELETE FROM tblCarrierOrderRejectRates WHERE ID IN (
				SELECT ID FROM viewCarrierRouteRates WHERE CarrierID=@CarrierID AND OrderRejectReasonID=@OrderRejectReasonID AND EffectiveDate>@EffectiveDate)
		END

		SELECT @replaced = count(1) FROM tblCarrierOrderRejectRates WHERE CarrierID=@CarrierID AND OrderRejectReasonID=@OrderRejectReasonID AND EffectiveDate=@EffectiveDate
		DELETE FROM tblCarrierOrderRejectRates WHERE CarrierID=@CarrierID AND OrderRejectReasonID=@OrderRejectReasonID AND EffectiveDate=@EffectiveDate
	END
	
	-- do the actual insert now
	INSERT INTO tblCarrierOrderRejectRates (CarrierID, OrderRejectReasonID, Rate, EffectiveDate, CreateDateUTC, CreatedByUser) 
		VALUES (@CarrierID, @OrderRejectReasonID, @Rate, @EffectiveDate, GETUTCDATE(), @UserName)
	SELECT @success = 1, @Message = 'Origin Wait Rate added successfully [' + ltrim(isnull(@replaced, 0)) + ' replaced, ' + ltrim(isnull(@removed, 0)) + ' deleted]'
END

GO

GRANT EXECUTE ON spCarrierOrderRejectRate_Add TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 30 Aug 2014
-- Author: Kevin Alons
-- Purpose: add a new CustomerOrderRejectRate
/***********************************/
CREATE PROCEDURE [dbo].[spCustomerOrderRejectRate_Add]
(
  @CustomerID int
, @OrderRejectReasonID int
, @Rate decimal(9, 4)
, @EffectiveDate smalldatetime
, @ReplaceMatching bit
, @ReplaceAllOverlapping bit
, @UserName varchar(100)
, @Success bit = NULL out
, @Message varchar(255) = NULL out
) AS 
BEGIN
	DECLARE @removed int, @replaced int

	IF (@ReplaceMatching = 0)
	BEGIN
		IF ((SELECT count(1) FROM tblCustomerOrderRejectRates WHERE CustomerID=@CustomerID AND OrderRejectReasonID=@OrderRejectReasonID AND EffectiveDate=@EffectiveDate) > 0)
		BEGIN
			SELECT @Success = 0, @Message = 'Skipped: Existing Origin Wait Rate already exists'
			RETURN
		END
	END
	ELSE
	BEGIN
		-- remove any matching/overlapping rate records
		IF (@ReplaceAllOverlapping = 1)
		BEGIN
			SELECT @removed = count(1) FROM tblCustomerOrderRejectRates WHERE CustomerID=@CustomerID AND OrderRejectReasonID=@OrderRejectReasonID AND EffectiveDate>@EffectiveDate
			DELETE FROM tblCustomerOrderRejectRates WHERE ID IN (
				SELECT ID FROM viewCustomerRouteRates WHERE CustomerID=@CustomerID AND OrderRejectReasonID=@OrderRejectReasonID AND EffectiveDate>@EffectiveDate)
		END

		SELECT @replaced = count(1) FROM tblCustomerOrderRejectRates WHERE CustomerID=@CustomerID AND OrderRejectReasonID=@OrderRejectReasonID AND EffectiveDate=@EffectiveDate
		DELETE FROM tblCustomerOrderRejectRates WHERE CustomerID=@CustomerID AND OrderRejectReasonID=@OrderRejectReasonID AND EffectiveDate=@EffectiveDate
	END
	
	-- do the actual insert now
	INSERT INTO tblCustomerOrderRejectRates (CustomerID, OrderRejectReasonID, Rate, EffectiveDate, CreateDateUTC, CreatedByUser) 
		VALUES (@CustomerID, @OrderRejectReasonID, @Rate, @EffectiveDate, GETUTCDATE(), @UserName)
	SELECT @success = 1, @Message = 'Origin Wait Rate added successfully [' + ltrim(isnull(@replaced, 0)) + ' replaced, ' + ltrim(isnull(@removed, 0)) + ' deleted]'
END

GO

GRANT EXECUTE ON spCustomerOrderRejectRate_Add TO dispatchcrude_iis_acct
GO

/*******************************************/
-- Date Created: 31 Aug 2013
-- Author: Kevin Alons
-- Purpose: return driver editable Order data for Driver App sync
/*******************************************/
ALTER FUNCTION [dbo].[fnOrderEdit_DriverApp]( @DriverID int, @LastChangeDateUTC datetime ) RETURNS TABLE AS
RETURN 
	SELECT O.ID
		, O.StatusID
		, O.TruckID
		, O.TrailerID
		, O.Trailer2ID
		, O.OriginBOLNum
		, O.OriginArriveTimeUTC
		, O.OriginDepartTimeUTC
		, O.OriginMinutes
		, O.OriginWaitReasonID
		, O.OriginWaitNotes
		, O.OriginTruckMileage
		, O.OriginGrossUnits
		, O.OriginGrossStdUnits
		, O.OriginNetUnits
		, O.ChainUp
		, O.Rejected
		, O.RejectReasonID
		, O.RejectNotes
		, O.OriginTankNum
		, O.DestArriveTimeUTC
		, O.DestDepartTimeUTC
		, O.DestMinutes
		, O.DestWaitReasonID
		, O.DestWaitNotes
		, O.DestBOLNum
		, O.DestTruckMileage
		, O.DestGrossUnits
		, O.DestNetUnits
		, O.DestProductTemp
		, O.DestProductBSW
		, O.DestProductGravity
		, O.DestOpenMeterUnits
		, O.DestCloseMeterUnits
		, O.CarrierTicketNum
		, O.AcceptLastChangeDateUTC
		, O.PickupLastChangeDateUTC
		, O.DeliverLastChangeDateUTC
		, O.PickupPrintStatusID
		, O.DeliverPrintStatusID
		, O.PickupPrintDateUTC
		, O.DeliverPrintDateUTC
		, O.DriverNotes
	FROM dbo.tblOrder O
	LEFT JOIN tblOrderDriverAppVirtualDelete ODAVD ON ODAVD.OrderID = O.ID AND ODAVD.DriverID = @DriverID
	CROSS JOIN (SELECT DATEADD(second, -5, @LastChangeDateUTC) AS LCD) LCD
	WHERE (O.StatusID IN (2, 7, 8, 3) -- Dispatched, Accepted, Picked Up, Delivered
		-- or this potentially was a DriverApp record, that no longer is, so should be included (as deleted)
		OR (ODAVD.ID IS NOT NULL AND ODAVD.VirtualDeleteDateUTC >= LCD.LCD))
	  AND (O.DriverID = @DriverID OR ODAVD.DriverID = @DriverID)
	  AND (
		@LastChangeDateUTC IS NULL 
		OR O.CreateDateUTC >= LCD.LCD
		OR O.LastChangeDateUTC >= LCD.LCD
		OR O.DeleteDateUTC >= LCD.LCD
		OR ODAVD.VirtualDeleteDateUTC >= LCD.LCD)

GO

/*******************************************/
-- Date Created: 25 Apr 2013
-- Author: Kevin Alons
-- Purpose: return OrderTicket data for Driver App sync
/*******************************************/
ALTER FUNCTION [dbo].[fnOrderTicket_DriverApp]( @DriverID int, @LastChangeDateUTC datetime ) RETURNS  TABLE AS
RETURN 
	SELECT OT.ID
		, OT.UID
		, OT.OrderID
		, OT.CarrierTicketNum
		, OT.OriginTankID
		, OT.TankNum
		, OT.TicketTypeID
		, OT.ProductObsGravity
		, OT.ProductObsTemp
		, OT.ProductHighTemp
		, OT.ProductLowTemp
		, OT.ProductBSW
		, cast(OT.OpeningGaugeFeet as tinyint) AS OpeningGaugeFeet
		, cast(OT.OpeningGaugeInch as tinyint) AS OpeningGaugeInch
		, cast(OT.OpeningGaugeQ as tinyint) AS OpeningGaugeQ
		, cast(OT.ClosingGaugeFeet as tinyint) AS ClosingGaugeFeet
		, cast(OT.ClosingGaugeInch as tinyint) AS ClosingGaugeInch
		, cast(OT.ClosingGaugeQ as tinyint) AS ClosingGaugeQ
		, OT.BottomFeet
		, OT.BottomInches
		, OT.BottomQ
		, OT.GrossUnits
		, OT.GrossStdUnits
		, OT.NetUnits
		, OT.Rejected
		, OT.RejectReasonID
		, OT.RejectNotes
		, OT.SealOff
		, OT.SealOn
		, OT.BOLNum
		, OT.CreateDateUTC
		, OT.CreatedByUser
		, OT.LastChangeDateUTC
		, OT.LastChangedByUser
		, OT.DeleteDateUTC
		, OT.DeletedByUser
	FROM dbo.tblOrderTicket OT
	JOIN dbo.tblOrder O ON O.ID = OT.OrderID
	LEFT JOIN tblOrderDriverAppVirtualDelete ODAVD ON ODAVD.OrderID = O.ID AND ODAVD.DriverID = @DriverID
	CROSS JOIN (SELECT DATEADD(second, -5, @LastChangeDateUTC) AS LCD) LCD
	WHERE (O.StatusID IN (2, 7, 8, 3) -- Dispatched, Accepted, Picked Up, Delivered
		-- or this potentially was a DriverApp record, that no longer is, so should be included (as deleted)
		OR (ODAVD.ID IS NOT NULL AND ODAVD.VirtualDeleteDateUTC >= LCD.LCD))
	  AND O.DriverID = @DriverID
	  AND (@LastChangeDateUTC IS NULL 
		OR OT.CreateDateUTC >= LCD.LCD
		OR OT.LastChangeDateUTC >= LCD.LCD
		OR ODAVD.VirtualDeleteDateUTC >= LCD.LCD)
		
GO

COMMIT
SET NOEXEC OFF
GO

EXEC _spRefreshAllViews
EXEC _spRefreshAllViews
EXEC _spRefreshAllViews
GO