SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.11.5'
SELECT  @NewVersion = '3.11.6'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1037: Add additional support fields to viewImportCenterFieldDefinition'
	UNION
	SELECT @NewVersion, 0, 'DCWEB-1037: Add OrderTicket.NoDataCalc field (for use by Import Center to import raw data)'
	UNION
	SELECT @NewVersion, 0, 'DCWEB-1037: Make FromMobileApp readonly for ImportCenter'
	UNION
	SELECT @NewVersion, 0, 'DCWEB-1037: make Object.SqlTargetName be NULLABLE so it can indicate that an Object is ReadOnly (when NULL)'
	UNION
	SELECT @NewVersion, 0, 'Internal: add fnQS, fnQI, fnQD (Quote String | Int | Decimal - wrap in quotes or convert to "NULL")'
	UNION
	SELECT @NewVersion, 0, 'DCWEB-1037: remove tblObjectField.DeleteDateUTC & DeletedByUser'
	UNION
	SELECT @NewVersion, 0, 'DCWEB-1037: revise _spAddNewObjectFields to return functional sql statements, not actually add data to DB'
	UNION
	SELECT @NewVersion, 0, 'DCWEB-1037: add fnCustomDataFieldValue() to facilitate custom Report Center columns to output Custom data'
	UNION
	SELECT @NewVersion, 0, 'Internal: fix some object comments (references to version 3.11.8, etc)'

	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/*******************************************************
 Creation Info:	3.10.1 - 2016/01/04
 Author:		Kevin Alons
 Purpose:		return all fields + some support fields for tblImportCenterFieldDefinition
 Changes:
- 3.10.12	- 2016/02/06	- KDA	- add IsCustom field (from tblObjectField)
- 3.11.8	- 2016/02/04	- KDA	- add InputFieldsCSV field
									- add OBF.ReadOnly field
									- add ObjectReadOnly field
*******************************************************/
ALTER VIEW viewImportCenterFieldDefinition AS
	SELECT ICDF.* 
		, HasChildren = CASE WHEN EXISTS (SELECT ID FROM tblImportCenterFieldDefinition WHERE ParentFieldID = ICDF.ID) THEN 1 ELSE 0 END
		, Name = OBF.Name , ObjectName = OBF.Object, OBF.FieldName, OBF.ObjectFieldTypeID, OBF.ObjectID, OBF.ObjectSqlSourceName, OBF.ObjectSqlTargetName
		, ObjectReadOnly = CASE WHEN OBF.ObjectSqlTargetName IS NULL THEN 1 ELSE 0 END
		, IDFieldName = (SELECT TOP 1 FieldName FROM tblObjectField WHERE ObjectID = OBF.ObjectID AND IsKey = 1)
		, OBF.AllowNull, OBF.IsCustom
		, OBF.ReadOnly
		, OBF.ParentObjectID
		, OBF.DefaultValue
		, InputFieldsCSV = dbo.fnICF_FieldCSV(ICDF.ID)
		, NestLevel = dbo.fnICFNestLevel(ICDF.ID) 
	FROM tblImportCenterFieldDefinition ICDF
	JOIN viewObjectField OBF ON OBF.ID = ICDF.ObjectFieldID
	LEFT JOIN tblImportCenterFieldDefinition PICDF ON PICDF.ID = ICDF.ParentFieldID
	LEFT JOIN viewObjectField POBF ON POBF.ID = PICDF.ObjectFieldID

GO

ALTER TABLE tblOrderTicket ADD NoDataCalc bit NOT NULL CONSTRAINT DF_OrderTicket_NoDataCalc DEFAULT (0)
GO

ALTER TABLE tblOrderTicketDbAudit ADD NoDataCalc bit NULL 
GO

/*****************************************
-- Date Created: ??
-- Author: Kevin Alons, Modified by Geoff Mochau
-- Purpose: specialized, db-level logic enforcing business rules, audit changes, etc
-- Changes:
    - 3.8.12	- 2015/08/04 - GSM	- use the revised (3-temperature) API function
	- 3.8.10	- 2015/07/26 - KDA	- always update the associated tblOrder.LastChangeDateUTC to NOW when a Ticket is changed)
	- 3.9.29.5	- 2015/12/03 - JAE	- add ProductBSW to dbaudit trigger
	- 3.9.38	- 2015/12/17 - BB	- Add WeightTareUnits, WeightGrossUnits, and WeightNetUnits columns to db audit (DCWEB-972)
	- 3.9.38	- 2015/12/21 - BB	- Add validation, calculations, etc. for WeightTareUnits, WeightGrossUnits, and WeightNetUnits (DCWEB-972)
	- 3.10.5.2	- 2016/02/11 - JAE	- Update to order table to sync ticket changes also resets the Pickup Last Change Date
	- 3.11.8	- 2016/03/06 - KDA	- add NoDataCalc field logic (do not calculate quantities, ensure raw data is provided - validation)
*****************************************/
ALTER TRIGGER trigOrderTicket_IU ON tblOrderTicket AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	-- Note: the TRIGGER_NESTLEVEL( OBJECT_ID('trigOrderTicket_IU') ) = 1 statement prevents recursion
	IF (TRIGGER_NESTLEVEL( OBJECT_ID('trigOrderTicket_IU')) = 1) BEGIN
		
		-- only do anything if something actually changed
		IF EXISTS (SELECT * FROM inserted EXCEPT SELECT * FROM deleted)
		BEGIN
			PRINT 'trigOrderTicket_IU FIRED'
			
			/**********  START OF VALIDATION SECTION ************************/
			DECLARE @errorString varchar(1000); SET @errorString = ''

			IF EXISTS (
				SELECT * 
				FROM tblOrder 
				WHERE ID IN (SELECT OrderID FROM inserted) 
				  -- prevent changes to any tickets belonging to a Delivered or Audited Order
				  AND StatusID IN (4))
			BEGIN
				IF (dbo.fnToBool(dbo.fnSettingValue(35)) = 1)
					PRINT 'Ticket for an AUDITED orders is being modified - please investigate why!'
				ELSE 				
					SET @errorString = @errorString + '|Tickets of Audited Orders cannot be modified'
			END

			-- prevent reassigning a ticket from one order to another
			IF EXISTS (
				SELECT i.* 
				FROM inserted i
				JOIN deleted d ON i.UID = d.UID
				WHERE i.OrderID <> d.OrderID)
			BEGIN
				SET @errorString = @errorString + '|Tickets cannot be moved to a different Order'
			END
			
			IF EXISTS (
				SELECT OT.OrderID
				FROM tblOrderTicket OT
				JOIN tblOrder O ON O.ID = OT.OrderID
				JOIN inserted i ON i.OrderID = OT.OrderID AND i.CarrierTicketNum = OT.CarrierTicketNum AND i.ID <> OT.ID
				WHERE OT.DeleteDateUTC IS NULL and I.DeleteDateUTC IS NULL
				GROUP BY OT.OrderID, OT.CarrierTicketNum
				HAVING COUNT(*) > 1
			)
			BEGIN
				SET @errorString = @errorString + '|Duplicate Ticket Numbers are not allowed'
			END
			
			-- store all the tickets for orders that are not in Generated status and not deleted (only these need validation)
			SELECT i.*, O.StatusID 
			INTO #i
			FROM inserted i
			JOIN tblOrder O ON O.ID = i.OrderID
			WHERE i.DeleteDateUTC IS NULL
			
			/* -- removed with version 3.7.5 
			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (3) AND BOLNum IS NULL)
			BEGIN
				SET @errorString = @errorString + '|BOL # value is required for Meter Run tickets'
			END
			*/
			
			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (1, 2) AND OriginTankID IS NULL AND TankNum IS NULL AND Rejected = 0)
			BEGIN
				SET @errorString = @errorString + '|Tank is required for Gauge Run & Net Volume tickets'
			END

			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (1) AND CarrierTicketNum IS NULL)
			BEGIN
				SET @errorString = @errorString + '|Ticket # value is required for Gauge Run tickets'
			END

			/* require all parameters for orders NOT IN (GAUGER, GENERATED, ASSIGNED, DISPATCHED) */
			IF  EXISTS (SELECT  ID FROM #i WHERE  Rejected =  0
					AND  ((TicketTypeID IN  (1, 2)  AND  (ProductObsTemp IS  NULL  OR  ProductObsGravity IS  NULL  OR  ProductBSW IS  NULL))
						OR  (TicketTypeID IN  (1)  AND  StatusID NOT  IN  (-9, -10, 1, 2)  AND  (ProductHighTemp IS  NULL  OR  ProductLowTemp IS  NULL))
					)
				)
			BEGIN
				SET @errorString = @errorString + '|All Product Measurement values are required for Gauge Run tickets'
			END
			
			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (1) AND Rejected = 0 
				AND (OpeningGaugeFeet IS NULL OR OpeningGaugeInch IS NULL OR OpeningGaugeQ IS NULL))
			BEGIN
				SET @errorString = @errorString + '|All Opening Gauge values are required for Gauge Run tickets'
			END
			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (1) AND Rejected = 0 AND StatusID NOT IN (-9, -10, 1, 2)
				AND (ClosingGaugeFeet IS NULL OR ClosingGaugeInch IS NULL OR ClosingGaugeQ IS NULL))
			BEGIN
				SET @errorString = @errorString + '|All Closing Gauge values are required for Gauge Run tickets'
			END

			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (2) AND Rejected = 0 AND StatusID NOT IN (-9, -10, 1, 2) AND (GrossUnits IS NULL))
			BEGIN
				SET @errorString = @errorString + '|Gross Volume value is required for Net Volume tickets'
			END

			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (7) AND Rejected = 0 AND StatusID NOT IN (-9, -10, 1, 2) AND (GrossUnits IS NULL))
			BEGIN
				SET @errorString = @errorString + '|Gross Volume value is required for Gauge Net tickets'
			END

			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (3) AND Rejected = 0 AND StatusID NOT IN (-9, -10, 1, 2) AND (GrossUnits IS NULL OR NetUnits IS NULL))
			BEGIN
				SET @errorString = @errorString + '|Gross & Net Volume values are required for Meter Run tickets'
			END

			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (1) AND Rejected = 0 AND StatusID NOT IN (-9, -10, 1, 2) AND (SealOff IS NULL OR SealOn IS NULL))
			BEGIN
				SET @errorString = @errorString + '|All Seal Off & Seal On values are required for Gauge Run tickets'
			END
			
			-- 3.9.38 - 2015/12/21 - BB - Add WeightGrossUnits check
			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (9) AND Rejected = 0 AND StatusID NOT IN (-9, -10, 1, 2) AND (WeightGrossUnits IS NULL OR WeightNetUnits IS NULL))
			BEGIN
				SET @errorString = @errorString + '|Gross & Net Weight values are required for Mineral Run tickets.'
			END
			
			-- 3.9.38 - 2015/12/21 - BB - Add WeightTareUnits check
			IF EXISTS (SELECT ID FROM #i WHERE TicketTypeID IN (9) AND Rejected = 0 AND StatusID NOT IN (-9, -10, 1, 2) AND (WeightTareUnits IS NULL))
			BEGIN
				SET @errorString = @errorString + '|Tare Weight is required for Mineral Run tickets.'
			END

			-- 3.11.8 - 2016/03/06 - KDA - ensure raw data is provided if NoDataCalc = 1 was set
			IF EXISTS (SELECT ID FROM #i WHERE NoDataCalc = 1 AND Rejected = 0 AND StatusID NOT IN (-9, -10, 1, 2) AND TicketTypeID NOT IN (9) AND (GrossUnits IS NULL OR NetUnits IS NULL))
			BEGIN
				SET @errorString = @errorString + '|Gross & Net Units must be provided for all Tickets with No Data Calc turned ON.'
			END

			-- if any errors were detected, cancel the entire operation (transaction) and return a LINEFEED-deliminated list of errors
			IF (len(@errorString) > 0)
			BEGIN
				SET @errorString = replace(substring(@errorString, 2, 1000), '|', char(13) + char(10))
				RAISERROR(@errorString, 16, 1)
				IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
				RETURN
			END
			/**********  END OF VALIDATION SECTION ************************/
			
			-- re-compute GaugeRun ticket Gross barrels
			IF UPDATE(OriginTankID) OR UPDATE(OpeningGaugeFeet) OR UPDATE(OpeningGaugeInch) OR UPDATE(OpeningGaugeQ)
				OR UPDATE (ClosingGaugeFeet) OR UPDATE(ClosingGaugeInch) OR UPDATE(ClosingGaugeQ) 
				OR UPDATE (GrossUnits)
			BEGIN
				UPDATE tblOrderTicket
				  SET GrossUnits = dbo.fnConvertUom(dbo.fnOriginTankStrappingLevelDeltaBarrels(
						OT.OriginTankID
					  , OpeningGaugeFeet
					  , OpeningGaugeInch
					  , OpeningGaugeQ
					  , ClosingGaugeFeet
					  , ClosingGaugeInch
					  , ClosingGaugeQ), 1, O.OriginUomID) -- ensure this value is always normalized from BARRELS
				FROM tblOrderTicket OT
				JOIN tblOrder O ON O.ID = OT.OrderID
				WHERE (OT.NoDataCalc = 0 OR OT.GrossUnits IS NULL) -- 3.11.8 - do not calculate these values when NoDataCalc is turned ON (1) and not provided
				  AND OT.ID IN (SELECT ID FROM inserted) 
				  AND OT.TicketTypeID = 1 -- Gauge Run Ticket
				  AND OT.OpeningGaugeFeet IS NOT NULL
				  AND OT.OpeningGaugeInch IS NOT NULL
				  AND OT.OpeningGaugeQ IS NOT NULL
				  AND OT.ClosingGaugeFeet IS NOT NULL
				  AND OT.ClosingGaugeInch IS NOT NULL
				  AND OT.ClosingGaugeQ IS NOT NULL
			END
			-- recompute the GrossUnits of any changed Meter Run tickets
			IF UPDATE(OpenMeterUnits) OR UPDATE(CloseMeterUnits) BEGIN
				--Print 'updating tblOrderTicket GOV from Open/Close Meter Units'
				UPDATE tblOrderTicket 
					SET GrossUnits = CloseMeterUnits - OpenMeterUnits
				WHERE TicketTypeID = 3  -- meter run tickets ONLY
				  AND (NoDataCalc = 0 OR GrossUnits IS NULL)  -- 3.11.8 - do not calculate these values when NoDataCalc is turned ON (1) and not provided
				  AND OpenMeterUnits IS NOT NULL AND CloseMeterUnits IS NOT NULL
				
			END
			-- re-compute GaugeRun | Net Barrel | Meter Run tickets NetUnits
			IF UPDATE(OpeningGaugeFeet) OR UPDATE(OpeningGaugeInch) OR UPDATE(OpeningGaugeQ)
				OR UPDATE (ClosingGaugeFeet) OR UPDATE(ClosingGaugeInch) OR UPDATE(ClosingGaugeQ) 
				OR UPDATE(OriginTankID) OR UPDATE(GrossUnits) OR UPDATE(GrossStdUnits) OR UPDATE(NetUnits)
				OR UPDATE(ProductObsTemp) OR UPDATE(ProductObsGravity) OR UPDATE(ProductBSW)
				OR UPDATE(OpenMeterUnits) OR UPDATE(CloseMeterUnits)
				OR UPDATE(ProductHighTemp) OR UPDATE(ProductLowTemp)
			BEGIN
				--PRINT 'updating tblOrderTicket NSV/GSV'
				UPDATE tblOrderTicket
				  SET NetUnits = dbo.fnCrudeNetCalculator3T(GrossUnits, ProductObsTemp, ProductHighTemp, ProductLowTemp, ProductObsGravity, isnull(ProductBSW, 0))
					, GrossStdUnits = dbo.fnCrudeNetCalculator3T(GrossUnits, ProductObsTemp, ProductHighTemp, ProductLowTemp, ProductObsGravity, 0)
				WHERE ID IN (SELECT ID FROM inserted)
				  AND NoDataCalc = 0  -- 3.11.8 - do not calculate these values when NoDataCalc is turned ON (1)
				  AND TicketTypeID IN (1, 2, 7) -- GAUGE RUN, NET VOLUME, GAUGE NET
				  AND GrossUnits IS NOT NULL
				  AND ProductHighTemp IS NOT NULL
				  AND ProductLowTemp IS NOT NULL
				  AND ProductObsTemp IS NOT NULL
				  AND ProductObsGravity IS NOT NULL
			END
			
			-- 3.9.38 - 2015/12/21 - BB - recompute the Net Weight of any changed Mineral Run tickets (DCWEB-972)
			IF UPDATE(WeightGrossUnits) OR UPDATE(WeightTareUnits) BEGIN
				--Print 'updating tblOrderTicket WeightNetUnits from WeightGrossUnits and WeightTareUnits
				UPDATE tblOrderTicket
					SET WeightNetUnits = WeightGrossUnits - WeightTareUnits
				WHERE TicketTypeID = 9 -- Mineral Run tickets only
				  AND (NoDataCalc = 0 OR WeightNetUnits IS NULL) -- 3.11.8 - do not calculate these values when NoDataCalc is turned ON (1) and not provided
				  AND WeightGrossUnits IS NOT NULL AND  WeightTareUnits IS NOT NULL
			END 			
			
			-- ensure the Order record is in-sync with the Tickets
			/* test lines below
			declare @gov decimal(9,3); SELECT TOP 1 @gov = OT.GrossUnits FROM tblOrderTicket OT JOIN inserted i ON i.ID = OT.ID;
			PRINT 'updating tblOrder GOV/GSV/NSV GOV = ' + ltrim(@gov)
			--*/
			UPDATE tblOrder 
			SET OriginGrossUnits = (SELECT sum(GrossUnits) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND OT.Rejected = 0 AND OT.DeleteDateUTC IS NULL)
			  , OriginNetUnits = (SELECT sum(NetUnits) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND OT.Rejected = 0 AND OT.DeleteDateUTC IS NULL)
			  , OriginGrossStdUnits = (SELECT sum(GrossStdUnits) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND OT.Rejected = 0 AND OT.DeleteDateUTC IS NULL)
			    --3.9.38 - 2015/12/21 - BB - Update weight fields (DCWEB-972)  -- Per Maverick 12/21/15 we do not need gross on the order level.
			  --, OriginWeightGrossUnits = (SELECT sum(WeightGrossUnits) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND OT.Rejected = 0 AND OT.DeleteDateUTC IS NULL)
			  , OriginWeightNetUnits = 	(SELECT sum(WeightNetUnits) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND OT.Rejected = 0 AND OT.DeleteDateUTC IS NULL)
				-- use the first MeterRun/BasicRun BOLNum as the Order BOL Num (favor the first un-Rejected)
			  , OriginBOLNum = (
					SELECT TOP 1 BOLNum FROM (
						SELECT TOP 1 BOLNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID NOT IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
						UNION SELECT TOP 1 BOLNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID NOT IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X)
				-- use the first GaugeRun|NetVolume CarrierTicketNum as the Order.CarrierTicketNum (favor first un-Rejected)
			  , CarrierTicketNum = (
					SELECT TOP 1 CarrierTicketNum FROM (
						SELECT TOP 1 CarrierTicketNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
						UNION SELECT TOP 1 CarrierTicketNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X)
				-- use the first GaugeRun|NetVolume OriginTankID for the Order (favor first un-Rejected ticket)
				-- BB included ticket additional ticket type (Gauge Net-7)
			  , OriginTankID = (
					SELECT TOP 1 OriginTankID FROM (
						SELECT TOP 1 OriginTankID FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2,7) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
						UNION SELECT TOP 1 OriginTankID FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2,7) AND DeleteDateUTC IS NULL ORDER BY ID) X)
				-- use the first GaugeRun|NetVolume OriginTankNum for the Order (favor first un-Rejected ticket)
			  , OriginTankNum = (
					SELECT TOP 1 OriginTankNum FROM (
						SELECT TOP 1 OriginTankNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
						UNION SELECT TOP 1 OriginTankNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X)
			  , PickupLastChangeDateUTC = GETUTCDATE() -- 3.10.5.2 info tied to the pickup has been changed
			  , LastChangeDateUTC = GETUTCDATE() -- 3.8.10 update
			  , LastChangedByUser = (SELECT MIN(isnull(LastChangedByUser, CreatedByUser)) FROM inserted WHERE OrderID = O.ID)
			FROM tblOrder O
			WHERE ID IN (SELECT DISTINCT OrderID FROM inserted)

			/* START DB AUDIT *********************************************************/
			BEGIN TRY
				IF EXISTS(SELECT * FROM tblSetting WHERE ID = 32 AND (Value LIKE 'true' OR Value LIKE 'yes'))
					INSERT INTO tblOrderTicketDbAudit (DBAuditDate, ID, OrderID, CarrierTicketNum, TicketTypeID, TankNum, ProductObsGravity
													, ProductObsTemp, OpeningGaugeFeet, OpeningGaugeInch, OpeningGaugeQ, ClosingGaugeFeet, ClosingGaugeInch
													, ClosingGaugeQ, GrossUnits, NetUnits, Rejected, RejectNotes, SealOff, SealOn, BOLNum, ProductHighTemp
													, ProductLowTemp, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC
													, DeletedByUser, UID, FromMobileApp, OriginTankID, GrossStdUnits, BottomFeet, BottomInches
													, BottomQ, RejectReasonID, MeterFactor, OpenMeterUnits, CloseMeterUnits, ProductBSW, WeightTareUnits
													, WeightGrossUnits, WeightNetUnits, NoDataCalc)
						SELECT GETUTCDATE(), ID, OrderID, CarrierTicketNum, TicketTypeID, TankNum, ProductObsGravity, ProductObsTemp, OpeningGaugeFeet
							, OpeningGaugeInch, OpeningGaugeQ, ClosingGaugeFeet, ClosingGaugeInch, ClosingGaugeQ, GrossUnits, NetUnits, Rejected, RejectNotes
							, SealOff, SealOn, BOLNum, ProductHighTemp, ProductLowTemp, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser
							, DeleteDateUTC, DeletedByUser, UID, FromMobileApp, OriginTankID, GrossStdUnits, BottomFeet, BottomInches, BottomQ, RejectReasonID
							, MeterFactor, OpenMeterUnits, CloseMeterUnits, ProductBSW, WeightTareUnits, WeightGrossUnits, WeightNetUnits, NoDataCalc
						FROM deleted d
			END TRY
			BEGIN CATCH
				PRINT 'trigOrderTicket_IU.DBAUDIT FAILURE: ' + ERROR_MESSAGE()
			END CATCH
			/* END DB AUDIT *********************************************************/

			PRINT 'trigOrderTicket_IU COMPLETE'
		END
	END	
END

GO
EXEC sp_settriggerorder @triggername=N'[dbo].[trigOrderTicket_IU]', @order=N'First', @stmttype=N'INSERT'
GO
EXEC sp_settriggerorder @triggername=N'[dbo].[trigOrderTicket_IU]', @order=N'First', @stmttype=N'UPDATE'
GO

SET IDENTITY_INSERT tblObjectField ON
INSERT INTO tblObjectField (ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, AllowNullID, IsKey, IsCustom)
	SELECT 385, 2, 'NoDataCalc', 'No Data Calculation', 2, '0', 1, 0, 0
SET IDENTITY_INSERT tblObjectField OFF

UPDATE tblObjectField SET ReadOnly = 1 WHERE FieldName LIKE 'FromMobileApp'
GO

--------------------------------------------------------------------
-- make tblObject.SqlTargetName be NULLABLE
--------------------------------------------------------------------

ALTER TABLE dbo.tblObject
	DROP CONSTRAINT DF_Object_CreateDateUTC
GO
ALTER TABLE dbo.tblObject
	DROP CONSTRAINT DF_Object_CreatedByUser
GO
CREATE TABLE dbo.Tmp_tblObject
	(
	ID int NOT NULL,
	Name varchar(50) NOT NULL,
	SqlSourceName varchar(255) NOT NULL,
	SqlTargetName varchar(255) NULL,
	CreateDateUTC datetime NOT NULL,
	CreatedByUser varchar(100) NOT NULL
	)  ON [PRIMARY]
GO
GRANT SELECT ON dbo.Tmp_tblObject TO role_iis_acct  AS dbo
GO
ALTER TABLE dbo.Tmp_tblObject ADD CONSTRAINT
	DF_Object_CreateDateUTC DEFAULT (getutcdate()) FOR CreateDateUTC
GO
ALTER TABLE dbo.Tmp_tblObject ADD CONSTRAINT
	DF_Object_CreatedByUser DEFAULT ('System') FOR CreatedByUser
GO
IF EXISTS(SELECT * FROM dbo.tblObject)
	 EXEC('INSERT INTO dbo.Tmp_tblObject (ID, Name, SqlSourceName, SqlTargetName, CreateDateUTC, CreatedByUser)
		SELECT ID, Name, SqlSourceName, SqlTargetName, CreateDateUTC, CreatedByUser FROM dbo.tblObject WITH (HOLDLOCK TABLOCKX)')
GO
ALTER TABLE dbo.tblObjectField
	DROP CONSTRAINT FK_ObjectField_Object
GO
ALTER TABLE dbo.tblObjectField
	DROP CONSTRAINT FK_ObjectField_ParentObject
GO
ALTER TABLE dbo.tblImportCenterDefinition
	DROP CONSTRAINT FK_ImportCenterDefinition_Object
GO
DROP TABLE dbo.tblObject
GO
EXECUTE sp_rename N'dbo.Tmp_tblObject', N'tblObject', 'OBJECT' 
GO
ALTER TABLE dbo.tblObject ADD CONSTRAINT
	PK_Object PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE UNIQUE NONCLUSTERED INDEX udxObject_Main ON dbo.tblObject
	(
	Name
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.tblImportCenterDefinition ADD CONSTRAINT
	FK_ImportCenterDefinition_Object FOREIGN KEY
	(
	RootObjectID
	) REFERENCES dbo.tblObject
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	
GO
ALTER TABLE dbo.tblObjectField ADD CONSTRAINT
	FK_ObjectField_Object FOREIGN KEY
	(
	ObjectID
	) REFERENCES dbo.tblObject
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblObjectField ADD CONSTRAINT
	FK_ObjectField_ParentObject FOREIGN KEY
	(
	ParentObjectID
	) REFERENCES dbo.tblObject
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.tblObjectField SET (LOCK_ESCALATION = TABLE)
GO

-- update Country, TicketType, DestTicketType, OrderStatus, State, PrintStatus, Time Zone objects to be READONLY
UPDATE tblObject SET SqlTargetName = NULL WHERE ID IN (18, 21, 22, 28, 35, 37, 40)
GO

EXEC _spRebuildAllObjects
GO

/************************************************
 Creation Info: 2016/03/07 - 3.11.8
 Author: Kevin Alons
 Purpose: quote a the string parameter value with single-quotes (or 'NULL' if incoming value is NULL)
 Notes: used the abbreviated name since this will likely be called multiple time, to simplify usage syntax
************************************************/
CREATE FUNCTION fnQS(@str varchar(max)) RETURNS varchar(max) AS
BEGIN
	IF (@str IS NULL)
		SELECT @str = 'NULL'
	ELSE IF (left(@str, 1) != '''' AND right(@str, 1) != '''')
		SELECT @str = '''' + @str + ''''
	RETURN (@str)
END
GO
GRANT EXECUTE ON fnQS TO role_iis_acct
GO
/************************************************
 Creation Info: 2016/03/07 - 3.11.8
 Author: Kevin Alons
 Purpose: quote a the int value with single-quotes (or 'NULL' if incoming value is NULL)
 Notes: used the abbreviated name since this will likely be called multiple time, to simplify usage syntax
************************************************/
CREATE FUNCTION fnQSI(@int int) RETURNS varchar(max) AS
BEGIN
	RETURN (dbo.fnQS(ltrim(@int)))
END
GO
GRANT EXECUTE ON fnQSI TO role_iis_acct
GO
/************************************************
 Creation Info: 2016/03/07 - 3.11.8
 Author: Kevin Alons
 Purpose: quote a the decimal value with single-quotes (or 'NULL' if incoming value is NULL)
 Notes: used the abbreviated name since this will likely be called multiple time, to simplify usage syntax
************************************************/
CREATE FUNCTION fnQSD(@dec decimal(20, 4)) RETURNS varchar(max) AS
BEGIN
	RETURN (dbo.fnQS(ltrim(@dec)))
END
GO
GRANT EXECUTE ON fnQSD TO role_iis_acct
GO

ALTER TABLE tblObjectField DROP COLUMN DeleteDateUTC, DeletedByUser
GO

EXEC _spRebuildAllObjects
GO

/************************************************
 Creation Info: 2016/01/14 - 3.10.1
 Author: Kevin Alons
 Purpose: support routine to generate new tblObjectField records for new fields after they are added to a table/view (and the tblObject table)
 Notes: only intended to be used in the back office to assist future database updates
 Changes:
- 3.11.8		- 2016/03/07 - KDA	- add generation of appropriate ACTIVE fields
								- convert to return appropriate sql statements instead of potentially adding the records to the db (removed @viewOnly parameter)
************************************************/
ALTER PROCEDURE _spAddNewObjectFields AS
BEGIN
	/* create basic object "data" field definitions*/
	SELECT X.*
	INTO #X
	FROM (
		SELECT ObjectID = O.ID, FieldName = C.COLUMN_NAME, Name = replace(dbo.fnFriendlyName(C.COLUMN_NAME), 'Order Date', 'Date')
			, OFTID = CASE	WHEN C.DATA_TYPE LIKE '%char%' OR C.DATA_TYPE LIKE 'TEXT' THEN 1
							WHEN C.DATA_TYPE IN ('bit') THEN 2
							WHEN C.DATA_TYPE LIKE '%int%' THEN 3
							WHEN C.DATA_TYPE in ('decimal', 'float', 'money', 'smallmoney') THEN 4
							WHEN C.DATA_TYPE LIKE '%datetime%' THEN 5
							WHEN C.DATA_TYPE LIKE '%date%' THEN 6
							WHEN C.DATA_TYPE LIKE '%time%' THEN 7
							WHEN C.DATA_TYPE LIKE 'unique%' THEN 8
							ELSE NULL END
			, DefaultValue = C.COLUMN_DEFAULT
			, AllowNullID = CASE WHEN C.IS_NULLABLE = 'YES' OR C.COLUMN_DEFAULT IS NOT NULL OR COLUMNPROPERTY(object_id(TABLE_NAME), COLUMN_NAME, 'IsIdentity') = 1 THEN 1 ELSE 0 END
			, IsKey = 0
			, IsCustom = 0
			, ParentObjectID = NULL, ParentObjectIDFieldName = NULL
		FROM INFORMATION_SCHEMA.COLUMNS C
		JOIN tblObject O ON O.SqlTargetName like C.TABLE_NAME
		WHERE C.COLUMN_NAME NOT LIKE '%ID' 
			AND C.DATA_TYPE NOT IN ('varbinary')
			AND C.COLUMN_NAME NOT LIKE '%DocName'
			AND C.COLUMN_NAME NOT LIKE '%FileName'
			AND C.COLUMN_NAME NOT LIKE '%CDL%'
			AND C.COLUMN_NAME NOT LIKE 'Create%'
			AND C.COLUMN_NAME NOT LIKE '%LastChange%'
			AND C.COLUMN_NAME NOT LIKE 'Delete%'

		/* add PK fields for each object */
		UNION SELECT ObjectID = O.ID, FieldName = C.COLUMN_NAME, Name = dbo.fnFriendlyName(C.COLUMN_NAME)
			, OFTID = CASE	WHEN C.DATA_TYPE LIKE '%char%' OR C.DATA_TYPE LIKE 'TEXT' THEN 1
							WHEN C.DATA_TYPE IN ('bit') THEN 2
							WHEN C.DATA_TYPE LIKE '%int%' THEN 3
							WHEN C.DATA_TYPE in ('decimal', 'float', 'money', 'smallmoney') THEN 4
							WHEN C.DATA_TYPE LIKE '%datetime%' THEN 5
							WHEN C.DATA_TYPE LIKE '%date%' THEN 6
							WHEN C.DATA_TYPE LIKE '%time%' THEN 7
							WHEN C.DATA_TYPE LIKE 'unique%' THEN 8
							ELSE NULL END
			, DefaultValue = NULL
			, AllowNullID = CASE WHEN C.IS_NULLABLE = 'YES' OR C.COLUMN_DEFAULT IS NOT NULL OR COLUMNPROPERTY(object_id(TABLE_NAME), COLUMN_NAME, 'IsIdentity') = 1 THEN 1 ELSE 0 END
			, IsKey = 1
			, IsCustom = 0
			, ParentObjectID = NULL, ParentObjectIDFieldName = NULL
		FROM INFORMATION_SCHEMA.COLUMNS C
		JOIN tblObject O ON O.SqlTargetName like C.TABLE_NAME
		WHERE C.COLUMN_NAME IN ('ID', 'UID')

		-- add in LINKAGE (foreign key) columns
		UNION SELECT ObjectID = O.ID, FieldName = C.COLUMN_NAME, Name = dbo.fnFriendlyName(C.COLUMN_NAME)
			, OFTID = CASE	WHEN C.DATA_TYPE LIKE '%char%' OR C.DATA_TYPE LIKE 'TEXT' THEN 1
							WHEN C.DATA_TYPE IN ('bit') THEN 2
							WHEN C.DATA_TYPE LIKE '%int%' THEN 3
							WHEN C.DATA_TYPE in ('decimal', 'float', 'money', 'smallmoney') THEN 4
							WHEN C.DATA_TYPE LIKE '%datetime%' THEN 5
							WHEN C.DATA_TYPE LIKE '%date%' THEN 6
							WHEN C.DATA_TYPE LIKE '%time%' THEN 7
							WHEN C.DATA_TYPE LIKE 'unique%' THEN 8
							ELSE NULL END
			, DefaultValue = NULL
			, AllowNullID = CASE WHEN C.IS_NULLABLE = 'YES' OR C.COLUMN_DEFAULT IS NOT NULL OR COLUMNPROPERTY(object_id(TABLE_NAME), COLUMN_NAME, 'IsIdentity') = 1 THEN 1 ELSE 0 END
			, IsKey = 0
			, IsCustom = 0
			, ParentObjectID = OFP.ObjectID
			, ParentObjectIDFieldName = OFP.Name
		FROM tblObject O
		JOIN INFORMATION_SCHEMA.COLUMNS C ON C.TABLE_NAME = O.SqlTargetName AND C.COLUMN_NAME NOT IN ('EIAPADDRegionID')
		JOIN ( 
			SELECT OFP.ID, OFP.ObjectID, OFP.Name, CName = replace(OP.Name, ' ', '') + OFP.FieldName
			FROM tblObject OP 
			JOIN tblObjectField OFP ON OFP.ObjectID = OP.ID
			WHERE OFP.IsKey = 1
		) OFP ON CASE WHEN C.COLUMN_NAME = 'StatusID' THEN 'OrderStatusID' ELSE C.COLUMN_NAME END LIKE '%' + OFP.CName 
	) X
	LEFT JOIN tblObjectField OBF ON OBF.ObjectID = X.ObjectID AND OBF.FieldName = X.FieldName
	WHERE OBF.ID IS NULL AND X.ObjectID NOT IN (28)
	ORDER BY X.ObjectID, X.FieldName


	-- return the sql statements to generate these new fields
	DECLARE @maxID int
	SELECT @maxID = max(ID) FROM tblObjectField WHERE ID < 90000

	SELECT Sql
	FROM (
		SELECT SortNum = 0, Sql = 'SET IDENTITY_INSERT tblObjectField ON'
		UNION
		SELECT SortNum = 1, Sql = 'INSERT INTO tblObjectField (ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, AllowNullID, IsKey, IsCustom, ParentObjectID, ParentObjectIDFieldName)' + char(13) + char(10)
			+ 'SELECT ' + ltrim(@maxID + ROW_NUMBER() OVER (ORDER BY ObjectID, FieldName)) + ',' + dbo.fnQSI(ObjectID) + ',' + dbo.fnQS(FieldName) + ',' + dbo.fnQS(Name) + ','+ dbo.fnQSI(OFTID) + ',' + dbo.fnQS(DefaultValue) + ',' + dbo.fnQSI(AllowNullID) + ',' + dbo.fnQSI(IsKey) + ',' + dbo.fnQSI(IsCustom) + ',' + dbo.fnQSI(ParentObjectID) + ',' + dbo.fnQS(ParentObjectIDFieldName)
		FROM #X
		UNION
		SELECT SortNum = 2, Sql = 'SET IDENTITY_INSERT tblObjectField OFF'
	) SQL
	ORDER BY SortNum

END
GO

/************************************************
 Creation Info: 2016/02/07 - 3.11.8
 Author: Kevin Alons
 Purpose: retrieve an object custom data field value (expected to be used for Report Center custom fields)
 Changes:
************************************************/
CREATE FUNCTION fnCustomDataFieldValue(@ObjectFieldID int, @RecordID int) RETURNS varchar(max) AS
BEGIN
	DECLARE @ret varchar(max)

	SELECT TOP 1 @ret = Value FROM tblObjectCustomData WHERE ObjectFieldID = @ObjectFieldID AND RecordID = @RecordID
	RETURN (@ret)
END
GO
GRANT EXECUTE ON fnCustomDataFieldValue To role_iis_acct
GO

/*------------------------------------------
FIX SOME 3.11.8 version comments - no logic changes
------------------------------------------*/
GO

/*******************************************
Date Created: 28 May 2015
Author: Kevin Alons
Purpose: format a string to a fixed length (truncate if short, pad (right) with spaces if short
Changes: 
- 3.10.9.2	- BB	- 2016/02/19	- New formatting for ticket number on C.O.D.E. export requires a leading zero which should be cut off when we get to a 7-digit
										Order number.  Adding a parameter to allow this function to trim right or left. 0 = Left, 1 = Right.
*******************************************/
ALTER FUNCTION fnFixedLenStr(@str varchar(max), @fixedLen int, @trimDirection bit) RETURNS varchar(max) AS
BEGIN
	DECLARE @ret varchar(255)
	
	IF (@trimDirection = 1)
		SET @ret = RIGHT(REPLICATE(' ', @fixedLen) + @str, @fixedLen)
	ELSE	
		SET @ret = LEFT(@str + REPLICATE(' ', @fixedLen), @fixedLen)	
	
	RETURN (@ret)
END
GO

/*****************************************************
Creation Info: 3.10.1 - 2015/12/15
Author: Kevin Alons
Purpose: clone an existing ImportCenterDefinition with a new name
Changes:
- 3.10.12		- 2016/02/06	- KDA	- remove IsKey & DoUpdate tblImportCenterFieldDefinition fields from processing
*****************************************************/
ALTER PROCEDURE spCloneImportCenterDefinition(@importCenterDefinitionID int, @Name varchar(50), @UserName varchar(100), @NewID int = NULL OUTPUT) AS
BEGIN
	BEGIN TRAN CICD

	-- copy the basic ImportCenterDefinition first
	INSERT INTO tblImportCenterDefinition (RootObjectID, Name, UserNames, CreatedByUser)
		SELECT RootObjectID, @Name, UserNames, @UserName FROM tblImportCenterDefinition WHERE ID = @importCenterDefinitionID
	SET @NewID = SCOPE_IDENTITY()

	-- add the root ImportFieldDefinitions (with no ParentFieldID)
	INSERT INTO tblImportCenterFieldDefinition (ImportCenterDefinitionID, ObjectFieldID, CreatedByUser)
		SELECT @NewID, ObjectFieldID, CreatedByUser
		FROM tblImportCenterFieldDefinition
		WHERE ImportCenterDefinitionID = @importCenterDefinitionID
		  AND ParentFieldID IS NULL

	DECLARE @Level int; SET @Level = 0

	-- create a table to store the old-new ImportField mappings (will be used later to add the ImportFieldDefinition_Fields)
	DECLARE @Map TABLE (OID int, NID int, Level int)
	-- populate with the just added root ImportFieldDefinitions
	INSERT INTO @Map 
		SELECT OICFD.ID, NICFD.ID, @Level
		FROM tblImportCenterFieldDefinition OICFD
		JOIN tblImportCenterFieldDefinition NICFD ON NICFD.ImportCenterDefinitionID = @NewID AND NICFD.ObjectFieldID = OICFD.ObjectFieldID
		WHERE OICFD.ImportCenterDefinitionID = @importCenterDefinitionID AND OICFD.ParentFieldID IS NULL

	--DECLARE @debugMap XML = (SELECT * FROM @Map FOR XML AUTO)

	-- create table to remember newly added child FieldDefinition records
	DECLARE @NID IDTABLE
	-- populate with the first child FieldDefinitions to be added
	INSERT INTO @NID 
		SELECT ID FROM tblImportCenterFieldDefinition X JOIN @Map M ON M.OID = X.ParentFieldID WHERE ImportCenterDefinitionID = @importCenterDefinitionID EXCEPT SELECT OID FROM @Map

	--DECLARE @debugNID XML = (SELECT * FROM @NID FOR XML AUTO)

	WHILE EXISTS (SELECT ID FROM @NID)
	BEGIN
		-- add the next level of children FieldDefinitions
		INSERT INTO tblImportCenterFieldDefinition (ImportCenterDefinitionID, ObjectFieldID, ParentFieldID, CSharpExpression, KeyComparisonTypeID, CreatedByUser)
			SELECT @NewID, ObjectFieldID, M.NID, CSharpExpression, KeyComparisonTypeID, CreatedByUser
			FROM tblImportCenterFieldDefinition ICFD
			JOIN @NID NID ON NID.ID = ICFD.ID
			JOIN @Map M ON M.OID = ICFD.ParentFieldID

		-- record the old-new mappings for these new FieldDefinitions
		INSERT INTO @Map 
			SELECT OICFD.ID, NICFD.ID, @Level
			FROM tblImportCenterFieldDefinition OICFD
			JOIN tblImportCenterFieldDefinition NICFD ON NICFD.ImportCenterDefinitionID = @NewID AND NICFD.ObjectFieldID = OICFD.ObjectFieldID
			WHERE OICFD.ID IN (SELECT ID FROM @NID)

		--SELECT @debugMap = (SELECT * FROM @Map FOR XML AUTO)

		-- see if any next level new FieldMappings need to be processed
		DELETE FROM @NID
		INSERT INTO @NID 
			SELECT ID FROM tblImportCenterFieldDefinition X JOIN @Map M ON M.OID = X.ParentFieldID WHERE ImportCenterDefinitionID = @importCenterDefinitionID EXCEPT SELECT OID FROM @Map

		--SELECT @debugNID = (SELECT * FROM @NID FOR XML AUTO)
		
		-- increment @Level
		SET @Level = @Level + 1
	END

	-- add all the FieldDefinitionFields based on the accumulated @Map records
	INSERT INTO tblImportCenterFieldDefinitionField (ImportCenterFieldID, ImportFieldName, Position)
		SELECT M.NID, DF.ImportFieldName, DF.Position
		FROM tblImportCenterFieldDefinitionField DF
		JOIN @Map M ON M.OID = DF.ImportCenterFieldID

	COMMIT TRAN CICD
END

GO

/*****************************************************************************************
Author: Kevin Alons
Date Created: 27 Feb 2014
Purpose: with the provided parameters, export the data and mark done (if doing finalExport)
Changes:
			- 8/12/15	- BB		- Expanded procedure to include C.O.D.E. export options
			- 8/21/15	- BB		- Added summary row to CODE export
			- 8/25/15	- BB & JAE	- CODE summary record was not working on final export (only preview). Moved where this record was added.
			- 8/26/15	- BB		- Added summary row to CODE summary row row count per customer request
			- 8/26/15	- BB		- Fixed last update (3.8.19)  I pulled the old version of the stored procedure to add the row count and lost the summary row.
3.10.9.2	- 2/19/16	- BB		- Added new required parameter to calls to fnFixedLenStr to indicate these are the original left trim option
*****************************************************************************************/
ALTER PROCEDURE spOrderExportFinalCustomer
( 
  @customerID int 
, @exportedByUser varchar(100) = NULL -- will default to SUSER_NAME() -- default value for now
, @exportFormat varchar(100) -- Options are: 'SUNOCO_SUNDEX' or 'CODE_STANDARD'
, @finalExport bit = 1 -- defualt to TRUE
) 
AS BEGIN
	SET NOCOUNT ON
	-- default to the current user if not supplied
	IF @exportedByUser IS NULL SET @exportedByUser = SUSER_NAME()
	
	-- Create variable for error catching with CODE export
	DECLARE @CODEErrorFlag BIT = 0 -- Default to false
	
	-- retrieve the order records to export
	DECLARE @orderIDs IDTABLE
	INSERT INTO @orderIDs (ID)
	SELECT ID
	FROM dbo.viewOrderCustomerFinalExportPending 
	WHERE CustomerID = @customerID 

	BEGIN TRAN exportCustomer
	
	-- export the orders in the specified format
	IF (@exportFormat = 'SUNOCO_SUNDEX') BEGIN
		SELECT E.* 
		INTO #output
		FROM dbo.viewSunocoSundex E
		JOIN @orderIDs IDs ON IDs.ID = E._ID
		WHERE E._CustomerID = @customerID
		ORDER BY _OrderNum
	END
	ELSE
	IF (@exportFormat = 'CODE_STANDARD') BEGIN
		DECLARE @CodeDXCode varchar(2) = (SELECT CodeDXCode FROM tblCustomer WHERE ID = @customerID)
		IF LEN(@CodeDXCode) = 2 BEGIN
			SELECT E.ExportText
			INTO #output2
			FROM dbo.viewOrderExport_CODE E
				JOIN @orderIDs IDs ON IDs.ID = E.OrderID
			WHERE E.CompanyID = @CodeDXCode
			
			/* Add summary record to output*/
			INSERT INTO #output2
			SELECT
				'4' -- Summary Record Type RecordID (Hard Coded)
				+ X.Company_ID 
				+ X.Record_Count 
				+ X.Pos_Neg_Code 
				+ X.Total_Net
				+ X.Shipper_Net
				+ dbo.fnFixedLenStr('',89,0)
				+ dbo.fnFixedLenStr('',3,0)
			FROM
				(SELECT
					ECB.Company_ID
					,CAST(dbo.fnFixedLenNum((COUNT(*) + 1), 10, 0, 1) AS VARCHAR)	AS Record_Count
					,ECB.Pos_Neg_Code
					,CAST(dbo.fnFixedLenNum(SUM(CAST(ECB.Total_Net_Volume AS DECIMAL)), 13, 0, 1) AS VARCHAR) Total_Net
					,CAST(dbo.fnFixedLenNum(SUM(CAST(ECB.Shippers_Net_Volume AS DECIMAL)), 13, 0, 1) AS VARCHAR) Shipper_Net
				FROM dbo.viewOrderExport_CODE_Base ECB			
					JOIN @orderIDs IDs ON IDs.ID = ECB.OrderID			
				WHERE ECB.Company_ID = @CodeDXCode
				GROUP BY ECB.Company_ID,ECB.Pos_Neg_Code) X			
		END
		ELSE SET @CODEErrorFlag = 1	
	END

	-- mark the orders as exported FINAL (for a Customer)
	IF (@finalExport = 1) BEGIN
		EXEC spMarkOrderExportFinalCustomer @orderIDs, @exportedByUser
	END
	
	COMMIT TRAN exportCustomer

	-- return the data to 
	IF (@exportFormat = 'SUNOCO_SUNDEX') BEGIN
		SELECT * FROM #output
	END
	
	IF (@exportFormat = 'CODE_STANDARD') BEGIN
		IF @CODEErrorFlag = 0 BEGIN
			SELECT * FROM #output2 ORDER BY ExportText --Sorted so that the summary record always sits at the bottom
		END
		ELSE SELECT 'Error in export data. Check Shipper CODE ID.'
	END		
END

GO

/*****************************************
Author: Kevin Alons
Date Created: 2015/10/08 - 3.9.19.11
Purpose: implement the CODE Standard Shipper export
Changes:
- 3.10.9.2	- 16/02/19	- BB	- Added new required parameter to calls to fnFixedLenStr to indicate these are the original left trim option
******************************************/
ALTER PROCEDURE spOrderExportFinalCustomer_CODE_Standard (@OrderIDs IDTABLE READONLY) AS
BEGIN
	-- retrieve the basic data
	SELECT E.ExportText, E.CompanyID
	INTO #output
	FROM dbo.viewOrderExport_CODE E
	JOIN @orderIDs IDs ON IDs.ID = E.OrderID

	IF (SELECT COUNT(DISTINCT CompanyID) FROM #output) <> 1 OR (SELECT COUNT(*) FROM #output WHERE LEN(isnull(CompanyID, '')) <> 2) > 0
	BEGIN /* invalid input data was specified, so just return an error message */
		RAISERROR('Error in export data. Check Shipper CODE ID.', 16, 1)
		RETURN
	END 
		
	/* Add summary record to output*/
	INSERT INTO #output (ExportText)
	SELECT
		'4' -- Summary Record Type RecordID (Hard Coded)
		+ X.Company_ID 
		+ X.Record_Count 
		+ X.Pos_Neg_Code 
		+ X.Total_Net
		+ X.Shipper_Net
		+ dbo.fnFixedLenStr('',89,0)
		+ dbo.fnFixedLenStr('',3,0)
	FROM (
		SELECT
			ECB.Company_ID
			,CAST(dbo.fnFixedLenNum((COUNT(*) + 1), 10, 0, 1) AS VARCHAR)	AS Record_Count
			,ECB.Pos_Neg_Code
			,CAST(dbo.fnFixedLenNum(SUM(CAST(ECB.Total_Net_Volume AS DECIMAL)), 13, 0, 1) AS VARCHAR) Total_Net
			,CAST(dbo.fnFixedLenNum(SUM(CAST(ECB.Shippers_Net_Volume AS DECIMAL)), 13, 0, 1) AS VARCHAR) Shipper_Net
		FROM dbo.viewOrderExport_CODE_Base ECB			
		JOIN @orderIDs IDs ON IDs.ID = ECB.OrderID			
		GROUP BY ECB.Company_ID,ECB.Pos_Neg_Code) X			
	
	/* return the final export data */
	SELECT ExportText FROM #output ORDER BY ExportText --Sorted so that the summary record always sits at the bottom
END

GO

/******************************************
Author:			Ben Bloodworth
Create date:	5 Jun 2015
Description:	Displays the base information for the CODE export. Please note that some values could change per DispatchCrude customer and thus it may
					be better to retrieve these settings from the Data Exchange settings instead of this view.
					
Updates:		
			- 8/21/15	- BB: Added CASE statments to account for Nulls in gravity readings and added additional filters for rejects and ticket types.
			- 8/26/15	- BB: Altered tank number field to use OriginTankText instead of TankNum.  Altered Property_Code per instructions from customer.
			- 9/24/15	- BB: Added Net Volume tickets to allowed ticket types list in where clause.
			- 10/05/15	- BB: Changed export to use Gross Volume instead of Net Volume (Changed per Cash Rainer at CW2).
- 3.9.19.11 - 10/08/15	- KDA - fix several varchar -> numeric conversions that could fail with entered data 
								(CarrierTicketNum too long, LeaseNum not numeric, T.OriginTankText not numeric, use of fnIsNumeric() function)
			- 10/14/15	- BB: removed KDA's changes in 3.9.19.11 because they broke the export and the customer noticed and called us on it!
								the lease numbers no longer printed and were being zero filled.  HUGE NO NO!!
			- 10/28/15	- BB: Customer now says to ALWAYS send BSW value if it is collected.  Added all relevant ticket types to BSW field logic .
			- 12/10/15	- KDA+BB: ensure any '-' characters in TankNum are eliminated prior to export processing (will cause a crash if not removed)
- 3.10.9.2	- 16/02/19	- BB: Per request from Plains for CW2 I changed the ticket number to be a zero + the order number.  When we hit 7 digits, the zero
								will be trimmed off of the left side due to the change made to fnFixedLenStr that allows left OR right trimming now.
******************************************/
ALTER VIEW viewOrderExport_CODE_Base AS
SELECT 
	OrderID = O.ID
	,OrderTicketID = T.ID
	,Record_ID = '2'
	,Company_ID = C.CodeDXCode
	,Ticket_Type = TT.CodeDXCode
	,Transmit_Ind = '2'  -- Hard coded per Angie Toews (PDS Energy) but may need to be changed to dynamic if the customer says so
	,Run_Type = 1
	-- This zero first ticket number
	,Ticket_Number = dbo.fnFixedLenStr('0' + CAST(O.OrderNum AS VARCHAR),7,1)
	/*
	This is the original way to transmit ticket numbers - commented out instead of deleting so it's easier to go back when we need to.
	,Ticket_Number = CASE WHEN ISNUMERIC(RIGHT(T.CarrierTicketNum,1)) = 0 THEN dbo.fnFixedLenStr(T.CarrierTicketNum,7,0) 
						  ELSE dbo.fnFixedLenNum(T.CarrierTicketNum,7,0,0) 
					 END*/
	,Run_Ticket_Date = dbo.fnDateMMDDYY(O.OrderDate)
	
	--Property code is a non-standard CODE field for Plains (per Steve Carver).  It is a mixture of origin leasnum and destination station num
	,Property_Code = (CASE WHEN O.LeaseNum IS NULL THEN '000000' ELSE CAST(dbo.fnFixedLenNum(O.LeaseNum,6,0,0) AS VARCHAR(6)) END)
					+ '000'
					+ (CASE WHEN O.DestStation IS NULL THEN '00000' ELSE dbo.fnFixedLenStr(O.DestStation,5,0) END)
					
	,Gathering_Charge = '0'  -- Hard coded per Angie Toews (PDS Energy) but may need to be changed to dynamic if the customer says so	
	,Tank_Meter_Num = CAST(dbo.fnFixedLenNum(REPLACE(T.OriginTankText, '-', ''), 6, 0, 0) AS VARCHAR(6)) + '0'	
	,Adjustment_Ind = '0' -- Hard coded per Angie Toews (PDS Energy) but may need to be changed to dynamic if the customer says so
	,Opening_Reading = dbo.fnOpenCloseReadingCODEFormat(T.ID,TT.CodeDXCode,0)
	,Opening_Date = dbo.fnDateMMDD(O.OrderDate)
	,Closing_Reading = dbo.fnOpenCloseReadingCODEFormat(T.ID,TT.CodeDXCode,1)
	,Closing_Date = dbo.fnDateMMDD(O.DestDepartTime)
	,Meter_Factor = CASE WHEN T.MeterFactor IS NULL THEN '00000000' 
						 ELSE dbo.fnFixedLenNum(T.MeterFactor, 2, 6, 1) 
					END
	,Shrinkage_Incrustation = '1000000' -- Hard coded per Angie Toews (PDS Energy) but may need to be changed to dynamic if the customer says so
	,Opening_Temperature = CASE WHEN T.ProductHighTemp IS NULL THEN '0000' 
								ELSE dbo.fnFixedLenNum(T.ProductHighTemp, 3, 1, 1) 
						   END
	,Closing_Temperature = CASE WHEN T.ProductLowTemp IS NULL THEN '0000' 
								ELSE dbo.fnFixedLenNum(T.ProductLowTemp, 3, 1, 1) 
						   END
	,BSW = CASE WHEN TT.ID IN (1,2,3,6,7) THEN dbo.fnFixedLenNum(T.ProductBSW, 2, 2, 1) --Basic, CAN Basic, and Gross Volume tickets are exluded because they dont have this value					
				ELSE '0000'
		   END
	,Observed_Gravity =	CASE WHEN T.ProductObsGravity IS NULL THEN '000'
							 ELSE dbo.fnFixedLenNum(T.ProductObsGravity, 2, 1, 1)
						END
	,Observed_Temperature = dbo.fnFixedLenNum(T.ProductObsTemp, 3, 1, 1)
	,Pos_Neg_Code = '0' -- Hard coded per Angie Toews (PDS Energy) but may need to be changed to dynamic if the customer says so
	,Total_Net_Volume = dbo.fnFixedLenNum(T.GrossUnits, 7, 2, 1) -- BB 10/7/15 this sends GROSS on purpose. this change is per Plains Marketing
	,Shippers_Net_Volume = dbo.fnFixedLenNum(T.GrossUnits, 7, 2, 1) -- BB 10/7/15 this sends GROSS on purpose. this change is per Plains Marketing
	,Corrected_Gravity = CASE WHEN T.Gravity60F IS NULL THEN '000'
							  ELSE dbo.fnFixedLenNum(T.Gravity60F, 2, 1, 1)
						 END
	,Product_Code = dbo.fnFixedLenStr('',3,0)	--Leave 3 blank spaces unless product is not crude oil. For others lookup PETROEX code	
	,Transmission_Date = dbo.fnFixedLenStr('',3,0) --Leave 3 blank spaces. Filled in by export reciever.
FROM viewOrderLocalDates O
	JOIN dbo.viewOrderCustomerFinalExportPending EP ON EP.ID = O.ID
	LEFT JOIN viewOrderTicket T ON T.OrderID = O.ID AND T.DeleteDateUTC IS NULL
	LEFT JOIN tblTicketType TT ON TT.ID = O.TicketTypeID
	LEFT JOIN tblCustomer C ON C.ID = O.CustomerID
	LEFT JOIN tblProduct P ON P.ID = O.ProductID
WHERE O.DeleteDateUTC IS NULL
	AND O.Rejected = 0 -- No rejected orders
	AND T.Rejected = 0 -- No rejected tickets
	AND O.StatusID = 4 -- Only Audited orders
	AND TT.ID IN (1,2,7) -- Only Gauge Run, Gauge Net, and Net Volume tickets

GO

COMMIT
SET NOEXEC OFF