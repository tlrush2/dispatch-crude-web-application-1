SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.1.19.1'
SELECT  @NewVersion = '4.1.20'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1826 - Add new report center date filter option for 7 days date range'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO



/* Update date field filters with new 7 days filter type and update current filter name to say (Excluding Today) */
UPDATE tblReportColumnDefinition
SET FilterDropDownSql =
'SELECT TOP 100 PERCENT ID, Name
	 FROM (
		SELECT ID=1, Name=''Week to Date'', SortID = 1 
		UNION SELECT 2, ''Month to Date'', 2 
		UNION SELECT 3, ''Last Month'', 6 
		UNION SELECT 4, ''Quarter to Date'', 7
		UNION SELECT 5, ''Year to Date'', 8 
		UNION SELECT 6, ''Yesterday'', 3
		UNION SELECT 7, ''Last 7 Days (Excluding Today)'', 4
		UNION SELECT 8, ''Last 7 Days (Including Today)'', 5
	) X
	ORDER BY SortID'
WHERE FilterTypeID = 3



/* Refresh/rebuild all views/fn/sp's that could be affected by the above changes */
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRebuildAllObjects
GO
EXEC _spRebuildAllObjects
GO
EXEC _spRecompileAllStoredProcedures
GO


COMMIT
SET NOEXEC OFF