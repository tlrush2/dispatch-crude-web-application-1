SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20) = '3.13.3.3'
	, @NewVersion varchar(20) = '3.13.4'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1550 - Driver Scheduling - add tri-state "is available" mode to record ON/OFF/undefined dates/times for each driver'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

ALTER TABLE dbo.tblDriverAvailability DROP CONSTRAINT FK_DriverAvailability_Driver
GO
CREATE TABLE dbo.Tmp_tblDriverAvailability
(
  ID int NOT NULL IDENTITY (1, 1)
, DriverID int NOT NULL
, AvailDateTime smalldatetime NOT NULL
, IsAvailable bit NULL
, CreateDateUTC datetime NOT NULL
, CreatedByUser varchar(100) NOT NULL
, LastChangeDateUTC datetime NULL
, LastChangedByUser varchar(100) NULL
) 
GO
INSERT INTO dbo.Tmp_tblDriverAvailability (DriverID, AvailDateTime, IsAvailable, CreateDateUTC, CreatedByUser)
	SELECT DriverID, AvailDateTime, 1, getutcdate(), 'System' FROM dbo.tblDriverAvailability
GO
DROP TABLE dbo.tblDriverAvailability
GO
EXECUTE sp_rename N'dbo.Tmp_tblDriverAvailability', N'tblDriverAvailability', 'OBJECT' 
GO

ALTER TABLE dbo.tblDriverAvailability ADD CONSTRAINT DF_DriverAvailability_IsAvailable DEFAULT ((1)) FOR IsAvailable
GO
ALTER TABLE dbo.tblDriverAvailability ADD CONSTRAINT DF_DriverAvailability_CreateDateUTC DEFAULT (getutcdate()) FOR CreateDateUTC
GO
ALTER TABLE dbo.tblDriverAvailability ADD CONSTRAINT DF_DriverAvailability_CreatedByUser DEFAULT ('System') FOR CreatedByUser
GO
ALTER TABLE dbo.tblDriverAvailability ADD CONSTRAINT PK_DriverAvailability PRIMARY KEY CLUSTERED (ID)
GO
ALTER TABLE dbo.tblDriverAvailability ADD CONSTRAINT FK_DriverAvailability_Driver FOREIGN KEY (DriverID) REFERENCES dbo.tblDriver(ID) ON UPDATE  NO ACTION  ON DELETE  NO ACTION 
GO

EXEC _spDropView 'viewNumbers1000'
GO
/********************************************/
-- Created: 2016/07/16 - 3.13.4 - Kevin Alons
-- Purpose: return a list of numbers 1-1000 for internal uses
-- Changes:
/********************************************/
CREATE VIEW viewNumbers1000 AS
SELECT TOP (1000) n = ROW_NUMBER() OVER (ORDER BY number) 
  FROM [master]..spt_values ORDER BY n;
GO

EXEC _spDropFunction 'fnDriverAvailability'
GO
/***********************************************************/
-- Date Created: 2016/07/10 - 3.13.4 - Kevin Alons
-- Purpose: return all "potential" Driver Availability records for a date range
-- Changes:
/***********************************************************/
CREATE FUNCTION fnDriverAvailability
(
  @StartDate date
, @EndDate date
, @RegionID int = -1
, @CarrierID int = -1
) RETURNS TABLE AS RETURN
	-- generates a full list of records for the specified criteria (with NULL or not assigned as a default)
	WITH cteBASE AS (
		SELECT D.RegionID, D.Region, D.CarrierID, D.Carrier, DriverID = D.ID, Driver = D.FullName, D.MobilePhone, DT.AvailDate
		FROM viewDriverBase D
		CROSS JOIN (
			-- get a table with the entire specified date range (date only values)
			SELECT AvailDate = dateadd(day, n-1, @StartDate)
			FROM viewNumbers1000 
			WHERE n <= datediff(day, @StartDate, @EndDate) + 1
		) DT
		WHERE (@CarrierID = -1 OR D.CarrierID = @CarrierID)
		  AND (@RegionID = -1 OR D.RegionID = @RegionID)
		  AND D.Active = 1
	)

	SELECT B.RegionID, B.Region, B.CarrierID, B.Carrier, B.DriverID, B.Driver, B.MobilePhone, AvailDate, AvailDateTime = CASE WHEN isnull(DA.IsAvailable, 0) = 0 THEN NULL ELSE DA.AvailDateTime END
		, DA.IsAvailable, DA.CreateDateUTC, DA.CreatedByUser, DA.LastChangeDateUTC, DA.LastChangedByUser
	FROM cteBASE B
	LEFT JOIN tblDriverAvailability DA ON DA.DriverID = B.DriverID AND cast(DA.AvailDateTime as date) = B.AvailDate

GO
GRANT SELECT ON fnDriverAvailability TO role_iis_acct
GO

/***********************************************************/
-- Created: 2013/11/3 - ?.?.? - Kevin Alons
-- Purpose: return all "potential" Driver Availability records for a date range
-- Changes:
-- 3.13.4 - 2016/07/19 - KDA - use new fnDriverAvailability for core of data retrieval
/***********************************************************/
ALTER PROCEDURE spDriverAvailability
(
  @StartDate smalldatetime
, @EndDate smalldatetime
, @CarrierID int = -1
, @RegionID int = -1
) AS BEGIN
	SELECT X.RegionID, X.Region, X.CarrierID, X.Carrier, X.DriverID, X.Driver, X.MobilePhone, AvailDate = format(X.AvailDate, 'M/d/yy')
		, AvailDateTime = CASE WHEN X.IsAvailable IS NULL THEN '????' WHEN X.IsAvailable = 0 THEN 'Not Avail' ELSE format(X.AvailDateTime, 'hh:mm tt') END
		, OpenOrders = isnull(TOO.OpenOrders, 0), DailyOpenOrders = isnull(DOO.OpenOrders, 0) 
	FROM dbo.fnDriverAvailability(@StartDate, @EndDate, @RegionID, @CarrierID) X
	LEFT JOIN (
		SELECT DriverID, OpenOrders = count(*)
		FROM tblOrder 
		WHERE StatusID IN (2,7,8) 
		GROUP BY DriverID
	) TOO ON TOO.DriverID = X.DriverID
	LEFT JOIN (
		SELECT DriverID, DueDate = dbo.fnDateOnly(DueDate), OpenOrders = COUNT(*)
		FROM tblOrder 
		WHERE StatusID IN (2,7,8) 
		GROUP BY DriverID, dbo.fnDateOnly(DueDate)
	) DOO ON DOO.DriverID = x.DriverID AND DOO.DueDate = X.AvailDate

	ORDER BY Carrier, Driver, AvailDate
END

GO

EXEC _spDropFunction 'fnCombineDateAndTime'
GO
/************************************************/
-- Created: 2016/07/16 - 3.13.4 - Kevin Alons
-- Purpose: combine a date & time into a datetime variable (for some reason not implemented in sql server??)
-- Changes:
/************************************************/
CREATE FUNCTION fnCombineDateAndTime(@date date, @time time) RETURNS datetime AS
BEGIN
	RETURN (cast(concat(format(@date, 'M/d/yyyy'), ' ', format(cast(@time as datetime), 'HH:mm:ss.fff')) as datetime))
END
GO

EXEC _spDropProcedure 'spDriverAvailabilityUpdate'
GO
/***********************************************************/
-- Created: 2016/07/16 - 3.13.4 - Kevin Alons
-- Purpose: update a single DriverAvailability record based on the specified criteria
-- Changes:
/***********************************************************/
CREATE PROCEDURE spDriverAvailabilityUpdate
(
  @DriverID int
, @AvailDate date
, @AvailTime time
, @IsAvailable bit
, @UserName varchar(255) = 'System'
) AS 
BEGIN
	-- if IsAvailable is TRUE, then ensure a Time is also set (for now the default is "08:00 AM")
	IF (@IsAvailable = 1 AND @AvailTime IS NULL) SET @AvailTime = '08:00 AM'

	DECLARE @AvailDateTime datetime = dbo.fnCombineDateAndTime(@AvailDate, @AvailTime)

	UPDATE tblDriverAvailability SET AvailDateTime = @AvailDateTime, IsAvailable = @IsAvailable 
		, LastChangeDateUTC = getutcdate(), LastChangedByUser = @UserName
	WHERE DriverID = @DriverID AND cast(AvailDateTime as date) = @AvailDate
	IF (@@ROWCOUNT = 0)
		INSERT INTO tblDriverAvailability (DriverID, AvailDateTime, IsAvailable, CreateDateUTC, CreatedByUser)
			VALUES (@DriverID, @AvailDateTime, @IsAvailable, getutcdate(), @UserName)
END
GO
GRANT EXECUTE ON spDriverAvailabilityUpdate TO role_iis_acct
GO

COMMIT
SET NOEXEC OFF