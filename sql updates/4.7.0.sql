SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.6.9.1'
SELECT  @NewVersion = '4.7.0'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'JT-3876 - Add pending status to settlement'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


INSERT INTO tblSetting
SELECT 73, 'Batches - Include pending status', 2, 'False', 'Settlement', GETUTCDATE(), 'System', null, null, 0, 'If set to TRUE, settlement batches will have two levels of Settlement (pending and final).', 0
GO


ALTER TABLE tblCarrierSettlementBatch ADD IsFinal BIT NOT NULL CONSTRAINT DF_CarrierSettlementBatch_IsFinal DEFAULT 0
GO
UPDATE tblCarrierSettlementBatch SET IsFinal = 1 -- finalize all existing
GO

ALTER TABLE tblDriverSettlementBatch ADD IsFinal BIT NOT NULL CONSTRAINT DF_DriverSettlementBatch_IsFinal DEFAULT 0
GO
UPDATE tblDriverSettlementBatch SET IsFinal = 1 -- finalize all existing
GO

ALTER TABLE tblShipperSettlementBatch ADD IsFinal BIT NOT NULL CONSTRAINT DF_ShipperSettlementBatch_IsFinal DEFAULT 0
GO
UPDATE tblShipperSettlementBatch SET IsFinal = 1 -- finalize all existing
GO

exec _spRefreshAllViews
GO


/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details + FINANCIAL INFO into a single view
-- Changes:
--  3.7.28	- 2015/06/18 - KDA	- add translated "SettlementFactor" column
--  4.4.1	- 2016/11/04 - JAE 	- Add Destination Chainup
--  4.4.1.2	- 2016.11.17 - KDA	- update "other" assessorial rates JOIN to use new IsSystem flag (instead of 1,2,3,4 criteria)
--  4.6.2	- 2017.04.20 - KDA	- add BatchDate, PeriodEndDate
--  4.7.0	- 2017.05.12 - JAE	- Add IsFinal to view, and InvoiceNum
/***********************************/
ALTER VIEW viewOrderSettlementCarrier AS 
	SELECT OSC.*
		, SB.BatchNum
		, SB.BatchDate
		, SettlementUom = UOM.Name
		, SettlementUomShort = UOM.Abbrev
		, RouteRate = RR.Rate
		, RouteRateType = RRT.Name
		, RateSheetRate = RSRR.Rate
		, RateSheetRateType = RSRT.Name
		, OrderRejectRate = ORR.Rate
		, OrderRejectRateType = ORRT.Name
		, OriginWaitRate = OWR.Rate
		, DestinationWaitRate = DWR.Rate
		, TotalWaitAmount = cast(isnull(OSC.OriginWaitAmount, 0) as money) + cast(isnull(OSC.DestinationWaitAmount, 0) as money)
		, OriginChainupRate = CAR.Rate
		, OriginChainupRateType = CART.Name
		, OriginChainupAmount = CA.Amount
		, DestChainupRate = DCAR.Rate
		, DestChainupRateType = DCART.Name
		, DestChainupAmount = DCA.Amount
		, RerouteRate = RAR.Rate
		, RerouteRateType = RART.Name
		, RerouteAmount = RA.Amount
		, SplitLoadRate = SAR.Rate
		, SplitLoadRateType = SART.Name
		, SplitLoadAmount = SA.Amount
		, H2SRate = HAR.Rate
		, H2SRateType = HART.Name
		, H2SAmount = HA.Amount
		, OtherAmount = OA.Amount
		, WaitFeeSubUnit = isnull(WFSU.Name, 'None') 
		, WaitFeeRoundingType = isnull(WFRT.Name, 'None')
		, SettlementFactor = SF.Name
		, SB.PeriodEndDate
		, SB.InvoiceNum
		, SB.IsFinal
	FROM tblOrderSettlementCarrier OSC 
	LEFT JOIN tblCarrierSettlementBatch SB ON SB.ID = OSC.BatchID
	LEFT JOIN tblUom UOM ON UOM.ID = OSC.SettlementUomID
	LEFT JOIN tblCarrierOriginWaitRate OWR ON OWR.ID = OSC.OriginWaitRateID
	LEFT JOIN tblCarrierDestinationWaitRate DWR ON DWR.ID = OSC.DestinationWaitRateID
	LEFT JOIN tblCarrierOrderRejectRate ORR ON ORR.ID = OSC.OrderRejectRateID
	LEFT JOIN tblRateType ORRT ON ORRT.ID = ORR.RateTypeID
	-- route rate/ratesheet JOINs
	LEFT JOIN tblCarrierRouteRate RR ON RR.ID = OSC.RouteRateID
	LEFT JOIN tblRateType RRT ON RRT.ID = RR.RateTypeID
	LEFT JOIN viewCarrierRateSheetRangeRate RSRR ON RSRR.ID = OSC.RangeRateID
	LEFT JOIN tblRateType RSRT ON RSRT.ID = RSRR.RateTypeID
	-- chainup
	LEFT JOIN tblOrderSettlementCarrierAssessorialCharge CA ON CA.OrderID = OSC.OrderID AND CA.AssessorialRateTypeID = 1
	LEFT JOIN tblCarrierAssessorialRate CAR ON CAR.ID = CA.AssessorialRateID
	LEFT JOIN tblRateType CART ON CART.ID = CAR.RateTypeID
	LEFT JOIN tblOrderSettlementCarrierAssessorialCharge DCA ON DCA.OrderID = OSC.OrderID AND DCA.AssessorialRateTypeID = 5
	LEFT JOIN tblCarrierAssessorialRate DCAR ON DCAR.ID = DCA.AssessorialRateID
	LEFT JOIN tblRateType DCART ON DCART.ID = DCAR.RateTypeID
	-- Reroute
	LEFT JOIN tblOrderSettlementCarrierAssessorialCharge RA ON RA.OrderID = OSC.OrderID AND RA.AssessorialRateTypeID = 2
	LEFT JOIN tblCarrierAssessorialRate RAR ON RAR.ID = RA.AssessorialRateID
	LEFT JOIN tblRateType RART ON RART.ID = RAR.RateTypeID
	-- Split Load
	LEFT JOIN tblOrderSettlementCarrierAssessorialCharge SA ON SA.OrderID = OSC.OrderID AND SA.AssessorialRateTypeID = 3
	LEFT JOIN tblCarrierAssessorialRate SAR ON SAR.ID = SA.AssessorialRateID
	LEFT JOIN tblRateType SART ON SART.ID = SAR.RateTypeID
	-- H2S
	LEFT JOIN tblOrderSettlementCarrierAssessorialCharge HA ON HA.OrderID = OSC.OrderID AND HA.AssessorialRateTypeID = 4
	LEFT JOIN tblCarrierAssessorialRate HAR ON HAR.ID = HA.AssessorialRateID
	LEFT JOIN tblRateType HART ON HART.ID = HAR.RateTypeID
	-- Other Assessorial Charges (combined)
	LEFT JOIN (
		SELECT OrderID, Amount = SUM(Amount) 
		FROM tblOrderSettlementCarrierAssessorialCharge 
		WHERE AssessorialRateTypeID IN (SELECT ID FROM tblAssessorialRateType WHERE IsSystem = 0) -- 4.4.1.2 - use tblAssessorialRateType.IsSystem
		GROUP BY OrderID
	) OA ON OA.OrderID = OSC.OrderID 
	LEFT JOIN tblSettlementFactor SF ON SF.ID = OSC.SettlementFactorID
	LEFT JOIN tblCarrierWaitFeeParameter WFP ON WFP.ID = OSC.WaitFeeParameterID
	LEFT JOIN dbo.tblWaitFeeSubUnit WFSU ON WFSU.ID = WFP.SubUnitID
	LEFT JOIN dbo.tblWaitFeeRoundingType WFRT ON WFRT.ID = WFP.RoundingTypeID

GO

/***********************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details + FINANCIAL INFO into a single view
-- Changes:
--  4.4.1	- 2016/11/04 - JAE	- Add Destination Chainup
--  4.4.1.2	- 2016.11.17 - KDA	- update "other" assessorial rates JOIN to use new IsSystem flag (instead of 1,2,3,4 criteria)
--  4.6.2	- 2017.04.20 - KDA	- add BatchDate, PeriodEndDate fields
--  4.7.0	- 2017.05.12 - JAE	- Add IsFinal to view, and InvoiceNum
/***********************************/
ALTER VIEW viewOrderSettlementDriver AS 
	SELECT OSC.*
		, SB.BatchNum
		, SB.BatchDate
		, SettlementUom = UOM.Name
		, SettlementUomShort = UOM.Abbrev
		, RouteRate = RR.Rate
		, RouteRateType = RRT.Name
		, RateSheetRate = RSRR.Rate
		, RateSheetRateType = RSRT.Name
		, OrderRejectRate = ORR.Rate
		, OrderRejectRateType = ORRT.Name
		, OriginWaitRate = OWR.Rate
		, DestinationWaitRate = DWR.Rate
		, TotalWaitAmount = cast(isnull(OSC.OriginWaitAmount, 0) as money) + cast(isnull(OSC.DestinationWaitAmount, 0) as money)
		, OriginChainupRate = CAR.Rate
		, OriginChainupRateType = CART.Name
		, OriginChainupAmount = CA.Amount
		, DestChainupRate = DCAR.Rate
		, DestChainupRateType = DCART.Name
		, DestChainupAmount = DCA.Amount
		, RerouteRate = RAR.Rate
		, RerouteRateType = RART.Name
		, RerouteAmount = RA.Amount
		, SplitLoadRate = SAR.Rate
		, SplitLoadRateType = SART.Name
		, SplitLoadAmount = SA.Amount
		, H2SRate = HAR.Rate
		, H2SRateType = HART.Name
		, H2SAmount = HA.Amount
		, OtherAmount = OA.Amount
		, WaitFeeSubUnit = isnull(WFSU.Name, 'None') 
		, WaitFeeRoundingType = isnull(WFRT.Name, 'None')
		, SettlementFactor = SF.Name
		, SB.InvoiceNum
		, SB.PeriodEndDate
		, SB.IsFinal
	FROM tblOrderSettlementDriver OSC 
	LEFT JOIN tblDriverSettlementBatch SB ON SB.ID = OSC.BatchID
	LEFT JOIN tblUom UOM ON UOM.ID = OSC.SettlementUomID
	LEFT JOIN tblDriverOriginWaitRate OWR ON OWR.ID = OSC.OriginWaitRateID
	LEFT JOIN tblDriverDestinationWaitRate DWR ON DWR.ID = OSC.DestinationWaitRateID
	LEFT JOIN tblDriverOrderRejectRate ORR ON ORR.ID = OSC.OrderRejectRateID
	LEFT JOIN tblRateType ORRT ON ORRT.ID = ORR.RateTypeID
	-- route rate/ratesheet JOINs
	LEFT JOIN tblDriverRouteRate RR ON RR.ID = OSC.RouteRateID
	LEFT JOIN tblRateType RRT ON RRT.ID = RR.RateTypeID
	LEFT JOIN viewDriverRateSheetRangeRate RSRR ON RSRR.ID = OSC.RangeRateID
	LEFT JOIN tblRateType RSRT ON RSRT.ID = RSRR.RateTypeID
	-- chainup
	LEFT JOIN tblOrderSettlementDriverAssessorialCharge CA ON CA.OrderID = OSC.OrderID AND CA.AssessorialRateTypeID = 1
	LEFT JOIN tblDriverAssessorialRate CAR ON CAR.ID = CA.AssessorialRateID
	LEFT JOIN tblRateType CART ON CART.ID = CAR.RateTypeID
	LEFT JOIN tblOrderSettlementDriverAssessorialCharge DCA ON DCA.OrderID = OSC.OrderID AND DCA.AssessorialRateTypeID = 5
	LEFT JOIN tblDriverAssessorialRate DCAR ON DCAR.ID = DCA.AssessorialRateID
	LEFT JOIN tblRateType DCART ON DCART.ID = DCAR.RateTypeID
	-- Reroute
	LEFT JOIN tblOrderSettlementDriverAssessorialCharge RA ON RA.OrderID = OSC.OrderID AND RA.AssessorialRateTypeID = 2
	LEFT JOIN tblDriverAssessorialRate RAR ON RAR.ID = RA.AssessorialRateID
	LEFT JOIN tblRateType RART ON RART.ID = RAR.RateTypeID
	-- Split Load
	LEFT JOIN tblOrderSettlementDriverAssessorialCharge SA ON SA.OrderID = OSC.OrderID AND SA.AssessorialRateTypeID = 3
	LEFT JOIN tblDriverAssessorialRate SAR ON SAR.ID = SA.AssessorialRateID
	LEFT JOIN tblRateType SART ON SART.ID = SAR.RateTypeID
	-- H2S
	LEFT JOIN tblOrderSettlementDriverAssessorialCharge HA ON HA.OrderID = OSC.OrderID AND HA.AssessorialRateTypeID = 4
	LEFT JOIN tblDriverAssessorialRate HAR ON HAR.ID = HA.AssessorialRateID
	LEFT JOIN tblRateType HART ON HART.ID = HAR.RateTypeID
	-- Other Assessorial Charges (combined)
	LEFT JOIN (
		SELECT OrderID, Amount = SUM(Amount) 
		FROM tblOrderSettlementDriverAssessorialCharge 
		WHERE AssessorialRateTypeID IN (SELECT ID FROM tblAssessorialRateType WHERE IsSystem = 0) -- 4.4.1.2 - use tblAssessorialRateType.IsSystem
		GROUP BY OrderID
	) OA ON OA.OrderID = OSC.OrderID 
	LEFT JOIN tblSettlementFactor SF ON SF.ID = OSC.SettlementFactorID
	LEFT JOIN tblCarrierWaitFeeParameter WFP ON WFP.ID = OSC.WaitFeeParameterID
	LEFT JOIN dbo.tblWaitFeeSubUnit WFSU ON WFSU.ID = WFP.SubUnitID
	LEFT JOIN dbo.tblWaitFeeRoundingType WFRT ON WFRT.ID = WFP.RoundingTypeID

GO


/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details + FINANCIAL INFO into a single view
-- Changes:
--  3.7.29	- 2015/06/18 - KDA	- add translated "SettlementFactor" column
--  4.4.1	- 2016/11/04 - JAE	- Add Destination Chainup
--  4.4.1.2	- 2016.11.17 - KDA	- update "other" assessorial rates JOIN to use new IsSystem flag (instead of 1,2,3,4 criteria)
--  4.6.2	- 2017.04.20 - KDA	- add BatchDate, PeriodEndDate fields
--  4.7.0	- 2017.05.12 - JAE	- Add IsFinal to view, and invoice number
/***********************************/
ALTER VIEW viewOrderSettlementShipper AS 
	SELECT OSC.*
		, SB.BatchNum
		, SB.BatchDate
		, SettlementUom = UOM.Name
		, SettlementUomShort = UOM.Abbrev
		, RouteRate = RR.Rate
		, RouteRateType = RRT.Name
		, RateSheetRate = RSRR.Rate
		, RateSheetRateType = RSRT.Name
		, OrderRejectRate = ORR.Rate
		, OrderRejectRateType = ORRT.Name
		, OriginWaitRate = OWR.Rate
		, DestinationWaitRate = DWR.Rate
		, TotalWaitAmount = cast(isnull(OSC.OriginWaitAmount, 0) as money) + cast(isnull(OSC.DestinationWaitAmount, 0) as money)
		, OriginChainupRate = CAR.Rate
		, OriginChainupRateType = CART.Name
		, OriginChainupAmount = CA.Amount
		, DestChainupRate = DCAR.Rate
		, DestChainupRateType = DCART.Name
		, DestChainupAmount = DCA.Amount
		, RerouteRate = RAR.Rate
		, RerouteRateType = RART.Name
		, RerouteAmount = RA.Amount
		, SplitLoadRate = SAR.Rate
		, SplitLoadRateType = SART.Name
		, SplitLoadAmount = SA.Amount
		, H2SRate = HAR.Rate
		, H2SRateType = HART.Name
		, H2SAmount = HA.Amount
		, OtherAmount = OA.Amount
		, WaitFeeSubUnit = isnull(WFSU.Name, 'None') 
		, WaitFeeRoundingType = isnull(WFRT.Name, 'None') 
		, SettlementFactor = SF.Name
		, SB.InvoiceNum
		, SB.PeriodEndDate
		, SB.IsFinal
	FROM tblOrderSettlementShipper OSC 
	LEFT JOIN tblShipperSettlementBatch SB ON SB.ID = OSC.BatchID
	LEFT JOIN tblUom UOM ON UOM.ID = OSC.SettlementUomID
	LEFT JOIN tblShipperOriginWaitRate OWR ON OWR.ID = OSC.OriginWaitRateID
	LEFT JOIN tblShipperDestinationWaitRate DWR ON DWR.ID = OSC.DestinationWaitRateID
	LEFT JOIN tblShipperOrderRejectRate ORR ON ORR.ID = OSC.OrderRejectRateID
	LEFT JOIN tblRateType ORRT ON ORRT.ID = ORR.RateTypeID
	-- route rate/ratesheet JOINs
	LEFT JOIN tblShipperRouteRate RR ON RR.ID = OSC.RouteRateID
	LEFT JOIN tblRateType RRT ON RRT.ID = RR.RateTypeID
	LEFT JOIN viewShipperRateSheetRangeRate RSRR ON RSRR.ID = OSC.RangeRateID
	LEFT JOIN tblRateType RSRT ON RSRT.ID = RSRR.RateTypeID
	-- chainup
	LEFT JOIN tblOrderSettlementShipperAssessorialCharge CA ON CA.OrderID = OSC.OrderID AND CA.AssessorialRateTypeID = 1
	LEFT JOIN tblShipperAssessorialRate CAR ON CAR.ID = CA.AssessorialRateID
	LEFT JOIN tblRateType CART ON CART.ID = CAR.RateTypeID
	LEFT JOIN tblOrderSettlementShipperAssessorialCharge DCA ON DCA.OrderID = OSC.OrderID AND DCA.AssessorialRateTypeID = 5
	LEFT JOIN tblShipperAssessorialRate DCAR ON DCAR.ID = DCA.AssessorialRateID
	LEFT JOIN tblRateType DCART ON DCART.ID = DCAR.RateTypeID
	-- Reroute
	LEFT JOIN tblOrderSettlementShipperAssessorialCharge RA ON RA.OrderID = OSC.OrderID AND RA.AssessorialRateTypeID = 2
	LEFT JOIN tblShipperAssessorialRate RAR ON RAR.ID = RA.AssessorialRateID
	LEFT JOIN tblRateType RART ON RART.ID = RAR.RateTypeID
	-- Split Load
	LEFT JOIN tblOrderSettlementShipperAssessorialCharge SA ON SA.OrderID = OSC.OrderID AND SA.AssessorialRateTypeID = 3
	LEFT JOIN tblShipperAssessorialRate SAR ON SAR.ID = SA.AssessorialRateID
	LEFT JOIN tblRateType SART ON SART.ID = SAR.RateTypeID
	-- H2S
	LEFT JOIN tblOrderSettlementShipperAssessorialCharge HA ON HA.OrderID = OSC.OrderID AND HA.AssessorialRateTypeID = 4
	LEFT JOIN tblShipperAssessorialRate HAR ON HAR.ID = HA.AssessorialRateID
	LEFT JOIN tblRateType HART ON HART.ID = HAR.RateTypeID
	-- Other Assessorial Charges (combined)
	LEFT JOIN (
		SELECT OrderID, Amount = SUM(Amount) 
		FROM tblOrderSettlementShipperAssessorialCharge 
		WHERE AssessorialRateTypeID IN (SELECT ID FROM tblAssessorialRateType WHERE IsSystem = 0) -- 4.4.1.2 - use tblAssessorialRateType.IsSystem
		GROUP BY OrderID
	) OA ON OA.OrderID = OSC.OrderID 
	LEFT JOIN tblSettlementFactor SF ON SF.ID = OSC.SettlementFactorID
	LEFT JOIN tblShipperWaitFeeParameter WFP ON WFP.ID = OSC.WaitFeeParameterID
	LEFT JOIN dbo.tblWaitFeeSubUnit WFSU ON WFSU.ID = WFP.SubUnitID
	LEFT JOIN dbo.tblWaitFeeRoundingType WFRT ON WFRT.ID = WFP.RoundingTypeID

GO

exec _spRefreshAllViews
GO




/**********************************/
-- Created: 2013/03/09 - ?.?.? - Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
-- Changes:
-- 3.7.31	- 2015/06/19 - KDA	- reorder input parameters (move ProviderID down) and add @DriverGroupID parameter
-- 3.9.0	- 2/15/08/14 - KDA	- return Approved column
-- 3.9.19.6	- 2015/09/30 - KDA	- add OverrideXXX fields from OrderApproval table
-- 3.9.21	- 2015/10/03 - KDA	- use [viewOrder] O.OriginDriverGroupID instead of O.DriverGroupID (which could be DestDriver.DriverGroupID)
-- 3.9.34	- 2015/12/16 - BB	- Remove deleted orders from the results (DCWEB-851)
-- 3.10.13.6 - 2015/03/01 - JAE	- add TruckTypeID
-- 3.11.3.1	- 2016/03/22 - KDA	- fix to no-op when @BatchID = 0
-- 3.11.19	- 2016/05/02 - BB	- Add Job number and Contract number
-- 3.11.20	- 2016/05/04 - JAE	- 3 month limit was always being applied, skip if batch is provided
-- 3.11.20.1 - 2016/05/04 - JAE	- Undoing change as timeouts reoccurring, need to investigate
-- 3.11.20.3 - 2016/05/11 - KDA	- optimize @BatchID filtering to query directly from tblOrderSettlementCarrier table
--								- use tblOrder and minimal JOINs intead of expensive viewOrder
-- 4.1.0	- 2016/08/08 - KDA	- use new @StartSession & @SessionID parameters to persist the retrieved records as a "Session"
--								- stop defaulting @StartDate to 3 months prior if not supplied (null parameter value provided)
--								- use simplified viewOrder_Financial_Carrier for return results - which is now again optimized for "reasonable" performance
-- 4.1.3.5	- 2016/09/17 - BB	- DCWEB-1770: Add reject filter
-- 4.1.8.6	- 2016.09.21 - KDA	- remove dynamic sql and retrieve data in 2 stages (like before) getting the orderID first, then the final data (expensive query)
--								- use new fnTrimToNull() to ensure optional string parameters are trimmed to NULL
-- 4.1.9.3	- 2026.09.28 - KDA	- ensure @StartSession = 0 when @BatchID is not null
-- 4.1.25.3	- 2016.10.28 - KDA	- ensure we don't reuse a sessionID when generating a new session
-- 4.3.0.2	- 2016.11.11 - KDA	- prevent including the same order multiple times (error in LEFT JOIN)
--								- don't remove existing session on overlapping creation, only "purge" after 1 week (obsolete)
--	4.5.11	- 2017.02.08 - JAE	- Replace contract number with contractID foreign key
--	4.7.0	- 2017/05/12 - JAE	- Don't presume batch = no session, join with batch table to get a few key fields
/***********************************/
ALTER PROCEDURE spRetrieveOrdersFinancialCarrier
(
  @StartDate DATE = NULL -- will default to first day of 3rd prior month for efficiency sake
, @EndDate DATE = NULL
, @ShipperID INT = -1 -- all
, @CarrierID INT = -1 -- all
, @ProductGroupID INT = -1 -- all 
, @TruckTypeID INT = -1 -- all
, @DriverGroupID INT = -1 -- all
, @OriginStateID INT = -1 -- all 
, @DestStateID INT = -1 -- all
, @ProducerID INT = -1 -- all
, @BatchID INT = NULL -- either show unbatched (NULL), or the specified batch orders
, @OnlyShipperSettled BIT = 0 
, @JobNumber VARCHAR(20) = NULL -- 3.11.19
, @ContractNumber VARCHAR(20) = NULL -- 3.11.19
, @Rejected INT = -1 -- all (rejected + not rejected) -- 4.1.3.5
, @UserName VARCHAR(100)
, @StartSession BIT = 1 -- when set to 1, assign the retrieved records to a new Session (ID), when 0 and a sessionID was assigned, then retrieve it
, @SessionID VARCHAR(100) = NULL OUTPUT -- if @startSession = 1, then this return a new value, otherwise will retrieve the existing records in that session
) AS 
BEGIN
	IF (@StartSession IS NULL) SET @StartSession = 0  -- default to FALSE if not supplied
	IF (@SessionID IS NULL) SET @SessionID = 0 -- default value

	-- if an empty/blank string parameter is provided, trim it to NULL
	SELECT @JobNumber = dbo.fnTrimToNull(@JobNumber)
		, @ContractNumber = dbo.fnTrimToNull(@ContractNumber)

	-- validate the incoming parameters
	IF (@BatchID IS NOT NULL
		AND (@ShipperID <> -1 
			OR @CarrierID <> -1 
			OR @ProductGroupID <> -1
			OR @TruckTypeID <> -1
			OR @DriverGroupID <> -1
			OR @OriginStateID <> -1
			OR @DestStateID <> -1
			OR @ProducerID <> -1))
	BEGIN
		RAISERROR('@BatchID parameter cannot be specified with other parameters', 16, 1)
		RETURN
	END

	DECLARE @IDs IDTABLE

	-- if a @BatchID value was specified then just use this criteria alone
    IF @BatchID IS NOT NULL
	BEGIN
		INSERT INTO @IDs SELECT OrderID FROM tblOrderSettlementCarrier WHERE BatchID = @BatchID
	END
	ELSE IF (@StartSession = 0 AND @SessionID IS NOT NULL)
	BEGIN
		INSERT INTO @IDs SELECT OrderID FROM tblOrderSettlementSelectionCarrier WHERE SessionID = @SessionID
	END
	ELSE
	BEGIN
		INSERT INTO @IDs 
			SELECT O.ID
			FROM tblOrder O
			JOIN tblProduct P ON P.ID = O.ProductID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			JOIN tblDestination D ON D.ID = O.DestinationID
			JOIN tblTruck T1 ON T1.ID = O.TruckID
			LEFT JOIN dbo.tblOrderTransfer OTR ON OTR.OrderID = O.ID
			LEFT JOIN tblTruck T2 ON T2.ID = OTR.OriginTruckID
			LEFT JOIN dbo.viewDriverBase vODR ON vODR.ID = OTR.OriginDriverID
			LEFT JOIN dbo.viewDriverBase vDDR ON vDDR.ID = O.DriverID
			LEFT JOIN tblOrderSettlementCarrier OS ON OS.OrderID = O.ID
			LEFT JOIN tblOrderSettlementShipper OSP ON OSP.OrderID = O.ID
			LEFT JOIN tblContract CN ON CN.ID = ContractID
			WHERE O.StatusID IN (4)  
			  AND O.DeleteDateUTC IS NULL  -- 3.9.34
			  AND OS.BatchID IS NULL -- only get order if it's not part of a batch
			  AND (@ShipperID=-1 OR O.CustomerID=@ShipperID) 
			  AND (@CarrierID=-1 OR O.CarrierID=@CarrierID) 
			  AND (@ProductGroupID=-1 OR P.ProductGroupID=@ProductGroupID) 
			  AND (@TruckTypeID=-1 OR T1.TruckTypeID=@TruckTypeID OR T2.TruckTypeID=@TruckTypeID)
			  AND (@DriverGroupID=-1 OR vODR.DriverGroupID = @DriverGroupID OR vDDR.DriverGroupID = @DriverGroupID) 
			  AND (@ProducerID=-1 OR O.ProducerID=@ProducerID) 
			  AND (@OriginStateID=-1 OR OO.StateID=@OriginStateID) 
			  AND (@DestStateID=-1 OR D.StateID=@DestStateID) 
			  AND (@StartDate IS NULL OR O.OrderDate >= @StartDate) 
			  AND (@EndDate IS NULL OR O.OrderDate <= @EndDate)
			  AND (@OnlyShipperSettled = 0 OR OSP.BatchID IS NOT NULL)
			  AND (@JobNumber IS NULL OR O.JobNumber = @JobNumber)
			  AND (@ContractNumber IS NULL OR CN.ContractNumber = @ContractNumber)
			  AND (@Rejected=-1 OR O.Rejected=@Rejected) -- 4.1.3.5
	END

	-- create the temp table to store the data
	SELECT *, RateApplySel = CAST(1 AS BIT), BatchSel = CAST(1 AS BIT), SessionID = CAST(NULL AS VARCHAR(100))
		, BatchNum = CAST(0 AS INT), InvoiceNum = CAST(NULL AS VARCHAR(50)), Notes = CAST(NULL AS VARCHAR(255))
	INTO #ret 
	FROM viewOrder_Financial_Carrier
	WHERE 1 = 0
	
	-- get the data into the temp table
	INSERT INTO #ret
		SELECT O.*
			, OS.RateApplySel, OS.BatchSel, OS.SessionID
			, SB.BatchNum, SB.InvoiceNum, SB.Notes
		FROM viewOrder_Financial_Carrier O
		LEFT JOIN tblOrderSettlementSelectionCarrier OS ON OS.OrderID = O.ID AND SessionID = @sessionID -- 4.3.0.2 - added @sessionID criteria
		LEFT JOIN tblCarrierSettlementBatch SB ON SB.ID = O.BatchID
		WHERE O.OrderID IN (SELECT ID FROM @IDs)
	
	-- do the SessionID logic
	IF (@StartSession = 1 AND EXISTS (SELECT * FROM #ret))
	BEGIN
		BEGIN TRAN RetrieveOrdersFinancialCarrier
	
		/* 4.3.0.2 - generate the "next" sessionID record */
		INSERT INTO tblOrderSettlementSessionCarrier(UserName) VALUES (@UserName)
		SELECT @sessionID = SCOPE_IDENTITY()

		UPDATE #ret SET SessionID = @sessionID, RateApplySel = 1, BatchSel = CASE WHEN HasError = 1 THEN 0 ELSE 1 END
		INSERT INTO tblOrderSettlementSelectionCarrier (SessionID, OrderID, RateApplySel, BatchSel)
			SELECT @sessionID, ID, RateApplySel, BatchSel FROM #ret

		COMMIT TRAN RetrieveOrdersFinancialCarrier

		/* "purge" the tblOrderSettlementSessionShipper table of "obsolete" records (older than a week old)
			NOTE: if this ends up being a performance hit, we should instead execute this via a scheduled task */
		DELETE FROM tblOrderSettlementSessionCarrier WHERE CreateDateUTC < DATEADD(WEEK, -1, GETDATE())
	END

	-- return the data to the caller
	SELECT * FROM #ret
END

GO



/**********************************/
-- Created: 2013/03/09 - ?.?.? - Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
-- Changes:
-- 4.1.0.2	- 2016.08.28 - KDA	- add some PRINT instrumentation statements for performance debugging purposes
-- 4.1.3.5	- 2016/09/17 - BB	- DCWEB-1770: Add reject filter
-- 4.1.8.6	- 2016.09.21 - KDA	- remove dynamic sql and retrieve data in 2 stages (like before) getting the orderID first, then the final data (expensive query)
--								- use new fnTrimToNull() to ensure optional string parameters are trimmed to NULL
-- 4.1.9.3	- 2026.09.28 - KDA	- ensure @StartSession = 0 when @BatchID is not null
-- 4.1.9.4	- 2016.09.28 - JAE	- (omitted from prev update) use new fnTrimToNull() to ensure optional string parameters are trimmed to NULL 
-- 4.1.25.3	- 2016.10.28 - KDA	- ensure we don't reuse a sessionID when generating a new session
-- 4.3.0.2	- 2016.11.11 - KDA	- prevent including the same order multiple times (error in LEFT JOIN)
--								- don't remove existing session on overlapping creation, only "purge" after 1 week (obsolete)
--	4.5.11	- 2017.02.08 - JAE	- Replace contract number with contractID foreign key
--	4.7.0	- 2017/05/12 - JAE	- Don't presume batch = no session, join with batch table to get a few key fields
/***********************************/
ALTER PROCEDURE spRetrieveOrdersFinancialDriver
(
  @StartDate DATE = NULL -- will default to first day of 3rd prior month for efficiency sake
, @EndDate DATE = NULL
, @ShipperID INT = -1 -- all
, @CarrierID INT = -1 -- all
, @ProductGroupID INT = -1 -- all 
, @TruckTypeID INT = -1 -- all
, @DriverGroupID INT = -1 -- all
, @DriverID INT = -1 -- all
, @OriginStateID INT = -1 -- all 
, @DestStateID INT = -1 -- all
, @ProducerID INT = -1 -- all
, @BatchID INT = NULL -- either show unbatched (NULL), or the specified batch orders
, @OnlyCarrierSettled BIT = 0 
, @JobNumber VARCHAR(20) = NULL -- 3.11.19
, @ContractNumber VARCHAR(20) = NULL -- 3.11.19
, @Rejected INT = -1 -- all (rejected + not rejected) -- 4.1.3.5
, @UserName VARCHAR(100)
, @StartSession BIT = 1 -- when set to 1, assign the retrieved records to a new Session (ID), when 0 and a sessionID was assigned, then retrieve it
, @SessionID VARCHAR(100) = NULL OUTPUT -- if @startSession = 1, then this return a new value, otherwise will retrieve the existing records in that session
) AS 
BEGIN
	PRINT 'Start=' + CONVERT(VARCHAR(24), GETDATE(), 121)

	IF (@StartSession IS NULL) SET @StartSession = 0  -- default to FALSE if not supplied
	IF (@SessionID IS NULL) SET @SessionID = 0 -- default value

	-- if an empty/blank string parameter is provided, trim it to NULL
	SELECT @JobNumber = dbo.fnTrimToNull(@JobNumber)
		, @ContractNumber = dbo.fnTrimToNull(@ContractNumber)

	-- validate the incoming parameters
	IF (@BatchID IS NOT NULL
		AND (@ShipperID <> -1 
			OR @CarrierID <> -1 
			OR @ProductGroupID <> -1
			OR @TruckTypeID <> -1
			OR @DriverGroupID <> -1
			OR @DriverID <> -1
			OR @OriginStateID <> -1
			OR @DestStateID <> -1
			OR @ProducerID <> -1))
	BEGIN
		RAISERROR('@BatchID parameter cannot be specified with other parameters', 16, 1)
		RETURN
	END

	DECLARE @IDs IDTABLE

	-- if a @BatchID value was specified then just use this criteria alone
    IF @BatchID IS NOT NULL
	BEGIN
		INSERT INTO @IDs SELECT OrderID FROM tblOrderSettlementDriver WHERE BatchID = @BatchID
	END
	ELSE IF (@StartSession = 0 AND @SessionID IS NOT NULL)
	BEGIN
		INSERT INTO @IDs SELECT OrderID FROM tblOrderSettlementSelectionDriver WHERE SessionID = @SessionID 
	END
	ELSE
	BEGIN
		INSERT INTO @IDs 
			SELECT O.ID
			FROM tblOrder O
			JOIN tblProduct P ON P.ID = O.ProductID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			JOIN tblDestination D ON D.ID = O.DestinationID
			JOIN tblTruck T1 ON T1.ID = O.TruckID
			LEFT JOIN dbo.tblOrderTransfer OTR ON OTR.OrderID = O.ID
			LEFT JOIN tblTruck T2 ON T2.ID = OTR.OriginTruckID
			LEFT JOIN dbo.viewDriverBase vODR ON vODR.ID = OTR.OriginDriverID
			LEFT JOIN dbo.viewDriverBase vDDR ON vDDR.ID = O.DriverID
			LEFT JOIN tblOrderSettlementDriver OS ON OS.OrderID = O.ID
			LEFT JOIN tblOrderSettlementCarrier OSP ON OSP.OrderID = O.ID
			LEFT JOIN tblContract CN ON CN.ID = O.ContractID
			WHERE O.StatusID IN (4)  
			  AND O.DeleteDateUTC IS NULL  -- 3.9.34
			  AND OS.BatchID IS NULL -- only get order if it's not part of a batch
			  AND (@ShipperID=-1 OR O.CustomerID=@ShipperID) 
			  AND (@CarrierID=-1 OR O.CarrierID=@CarrierID) 
			  AND (@ProductGroupID=-1 OR P.ProductGroupID=@ProductGroupID) 
			  AND (@TruckTypeID=-1 OR T1.TruckTypeID=@TruckTypeID OR T2.TruckTypeID=@TruckTypeID)
			  AND (@DriverGroupID=-1 OR vODR.DriverGroupID = @DriverGroupID OR vDDR.DriverGroupID = @DriverGroupID) 
			  AND (@DriverID=-1 OR vODR.ID = @DriverID OR vDDR.ID = @DriverID) 
			  AND (@OriginStateID=-1 OR OO.StateID=@OriginStateID) 
			  AND (@DestStateID=-1 OR D.StateID=@DestStateID) 
			  AND (@ProducerID=-1 OR O.ProducerID=@ProducerID) 
			  AND (@StartDate IS NULL OR O.OrderDate >= @StartDate) 
			  AND (@EndDate IS NULL OR O.OrderDate <= @EndDate)
			  AND (@OnlyCarrierSettled = 0 OR OSP.BatchID IS NOT NULL)
			  AND (@JobNumber IS NULL OR O.JobNumber = @JobNumber)
			  AND (@ContractNumber IS NULL OR CN.ContractNumber = @ContractNumber)
			  AND (@Rejected=-1 OR O.Rejected=@Rejected)  -- 4.1.3.5
	END

	-- create the temp table to store the data
	SELECT *, RateApplySel = CAST(1 AS BIT), BatchSel = CAST(1 AS BIT), SessionID = CAST(NULL AS VARCHAR(100))
		, InvoiceNum = CAST(NULL AS VARCHAR(50)), Notes = CAST(NULL AS VARCHAR(255))
	INTO #ret 
	FROM viewOrder_Financial_Driver
	WHERE 1 = 0
	
	-- get the data into the temp table
	INSERT INTO #ret
		SELECT O.*
			, OS.RateApplySel, OS.BatchSel, OS.SessionID
			, SB.InvoiceNum, SB.Notes
		FROM viewOrder_Financial_Driver O
		LEFT JOIN tblOrderSettlementSelectionDriver OS ON OS.OrderID = O.ID AND SessionID = @sessionID -- 4.3.0.2 - added @sessionID criteria
		LEFT JOIN tblDriverSettlementBatch SB ON SB.ID = O.BatchID
		WHERE O.OrderID IN (SELECT ID FROM @IDs)

	-- do the SessionID logic
	IF (@StartSession = 1 AND EXISTS (SELECT * FROM #ret))
	BEGIN
		PRINT 'SessionCreate:Start=' + CONVERT(VARCHAR(24), GETDATE(), 121)
		BEGIN TRAN RetrieveOrdersFinancialDriver

		/* 4.3.0.2 - generate the "next" sessionID record */
		INSERT INTO tblOrderSettlementSessionDriver(UserName) VALUES (@UserName)
		SELECT @sessionID = SCOPE_IDENTITY()

		UPDATE #ret SET SessionID = @sessionID, RateApplySel = 1, BatchSel = CASE WHEN HasError = 1 THEN 0 ELSE 1 END
		INSERT INTO tblOrderSettlementSelectionDriver (SessionID, OrderID, RateApplySel, BatchSel)
			SELECT @sessionID, ID, RateApplySel, BatchSel FROM #ret
	
		COMMIT TRAN RetrieveOrdersFinancialDriver
		PRINT 'SessionCreate:Finish=' + CONVERT(VARCHAR(24), GETDATE(), 121)

		/* "purge" the tblOrderSettlementSessionDriver table of "obsolete" records (older than a week old)
			NOTE: if this ends up being a performance hit, we should instead execute this via a scheduled task */
		DELETE FROM tblOrderSettlementSessionDriver WHERE CreateDateUTC < DATEADD(WEEK, -1, GETDATE())
	END

	-- return the data to the caller
	SELECT * FROM #ret

	PRINT 'Finish=' + CONVERT(VARCHAR(24), GETDATE(), 121)
END

GO


/***********************************/
-- Created: ?.?.? - 2013/03/09 - Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
-- Changes:
-- 3.8.11	- 2015/07/28 - KDA	- remove OriginShipperRegion field
-- 3.9.0	- 2/15/08/14 - KDA	- return Approved column
-- 3.9.19.6	- 2015/09/30 - KDA	- add OverrideXXX fields from OrderApproval table
-- 3.9.34	- 2015/12/16 - BB	- Remove deleted orders from the results (DCWEB-851)
-- 3.10.13.6 - 2015/03/01 - JAE	- add TruckTypeID
-- 3.11.3.1	- 2016/03/22 - KDA	- fix to no-op when @BatchID = 0
-- 3.11.19	- 2016/05/02 - BB	- Add Job number and Contract number
-- 3.11.20.3 - 2016/05/11 - KDA	- optimize @BatchID filtering to query directly from tblOrderSettlementShipper table
--								- use tblOrder and minimal JOINs intead of expensive viewOrder
-- 4.1.0	- 2016/08/08 - KDA	- use new @StartSession & @SessionID parameters to persist the retrieved records as a "Session"
--								- stop defaulting @StartDate to 3 months prior if not supplied (null parameter value provided)
--								- use simplified viewOrder_Financial_Shipper for return results - which is now again optimized for "reasonable" performance
-- 4.1.3.5	- 2016/09/17 - BB	- DCWEB-1770: Add reject filter
-- 4.1.8.6	- 2016.09.21 - KDA	- remove dynamic sql and retrieve data in 2 stages (like before) getting the orderID first, then the final data (expensive query)
--								- use new fnTrimToNull() to ensure optional string parameters are trimmed to NULL
-- 4.1.9.3	- 2026.09.28 - KDA	- ensure @StartSession = 0 when @BatchID is not null
-- 4.1.25.3	- 2016.10.28 - KDA	- ensure we don't reuse a sessionID when generating a new session
-- 4.3.0.2	- 2016.11.11 - KDA	- prevent including the same order multiple times (error in LEFT JOIN)
--								- don't remove existing session on overlapping creation, only "purge" after 1 week (obsolete)
--	4.5.11	- 2017.02.08 - JAE	- Replace contract number with contractID foreign key
--	4.7.0	- 2017/05/12 - JAE	- Don't presume batch = no session, join with batch table to get a few key fields
/***********************************/
ALTER PROCEDURE spRetrieveOrdersFinancialShipper
(
  @StartDate DATE = NULL -- will default to first day of 3rd prior month for efficiency sake
, @EndDate DATE = NULL
, @ShipperID INT = -1 -- all
, @ProductGroupID INT = -1 -- all 
, @TruckTypeID INT = -1 -- all
, @OriginStateID INT = -1 -- all 
, @DestStateID INT = -1 -- all
, @ProducerID INT = -1 -- all
, @BatchID INT = NULL -- either show unbatched (NULL), or the specified batch orders
, @JobNumber VARCHAR(20) = NULL -- 3.11.19
, @ContractNumber VARCHAR(20) = NULL -- 3.11.19
, @Rejected INT = -1 -- all (rejected + not rejected) -- 4.1.3.5
, @UserName VARCHAR(100)
, @StartSession BIT = 1 -- when set to 1, assign the retrieved records to a new Session (ID), when 0 and a sessionID was assigned, then retrieve it
, @SessionID VARCHAR(100) = NULL OUTPUT -- if @startSession = 1, then this return a new value, otherwise will retrieve the existing records in that session
) AS 
BEGIN
	IF (@StartSession IS NULL) SET @StartSession = 0  -- default to FALSE if not supplied
	IF (@SessionID IS NULL) SET @SessionID = 0 -- default value

	-- if an empty/blank string parameter is provided, trim it to NULL
	SELECT @JobNumber = dbo.fnTrimToNull(@JobNumber)
		, @ContractNumber = dbo.fnTrimToNull(@ContractNumber)

	-- validate the incoming parameters
	IF (@BatchID IS NOT NULL
		AND (@ShipperID <> -1 
			OR @ProductGroupID <> -1
			OR @TruckTypeID <> -1
			OR @OriginStateID <> -1
			OR @DestStateID <> -1
			OR @ProducerID <> -1))
	BEGIN
		RAISERROR('@BatchID parameter cannot be specified with other parameters', 16, 1)
		RETURN
	END

	DECLARE @IDs IDTABLE

	-- if a @BatchID value was specified then just use this criteria alone
    IF @BatchID IS NOT NULL
	BEGIN
		INSERT INTO @IDs SELECT OrderID FROM tblOrderSettlementShipper WHERE BatchID = @BatchID
	END
	ELSE IF (@StartSession = 0 AND @SessionID IS NOT NULL)
	BEGIN
		INSERT INTO @IDs SELECT OrderID FROM tblOrderSettlementSelectionShipper WHERE SessionID = @SessionID
	END
	ELSE
	BEGIN
		INSERT INTO @IDs 
			SELECT O.ID
			FROM tblOrder O
			JOIN tblProduct P ON P.ID = O.ProductID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			JOIN tblDestination D ON D.ID = O.DestinationID
			JOIN tblTruck T1 ON T1.ID = O.TruckID
			LEFT JOIN dbo.tblOrderTransfer OTR ON OTR.OrderID = O.ID
			LEFT JOIN tblTruck T2 ON T2.ID = OTR.OriginTruckID
			LEFT JOIN tblOrderSettlementShipper OS ON OS.OrderID = O.ID
			LEFT JOIN tblContract CN ON CN.ID = O.ContractID
			WHERE O.StatusID IN (4)  
			  AND O.DeleteDateUTC IS NULL  -- 3.9.34
			  AND OS.BatchID IS NULL -- only get order if it's not part of a batch
			  AND (@ShipperID=-1 OR O.CustomerID=@ShipperID) 
			  AND (@ProductGroupID=-1 OR P.ProductGroupID=@ProductGroupID) 
			  AND (@TruckTypeID=-1 OR T1.TruckTypeID=@TruckTypeID OR T2.TruckTypeID=@TruckTypeID)
			  AND (@ProducerID=-1 OR O.ProducerID=@ProducerID) 
			  AND (@OriginStateID=-1 OR OO.StateID=@OriginStateID) 
			  AND (@DestStateID=-1 OR D.StateID=@DestStateID) 
			  AND (@StartDate IS NULL OR O.OrderDate >= @StartDate) 
			  AND (@EndDate IS NULL OR O.OrderDate <= @EndDate)
			  AND (@JobNumber IS NULL OR O.JobNumber = @JobNumber)
			  AND (@ContractNumber IS NULL OR CN.ContractNumber = @ContractNumber)
			  AND (@Rejected=-1 OR O.Rejected=@Rejected) -- 4.1.3.5
	END
	
	-- create the temp table to store the data
	SELECT *, RateApplySel = CAST(1 AS BIT), BatchSel = CAST(1 AS BIT), SessionID = CAST(NULL AS VARCHAR(100))
		, BatchNum = CAST(0 AS INT), InvoiceNum = CAST(NULL AS VARCHAR(50)), Notes = CAST(NULL AS VARCHAR(255))
	INTO #ret 
	FROM viewOrder_Financial_Shipper 
	WHERE 1 = 0
	
	-- get the data into the temp table
	INSERT INTO #ret
		SELECT O.*
			, OS.RateApplySel, OS.BatchSel, OS.SessionID
			, SB.BatchNum, SB.InvoiceNum, SB.Notes
		FROM viewOrder_Financial_Shipper O
		LEFT JOIN tblOrderSettlementSelectionShipper OS ON OS.OrderID = O.ID AND SessionID = @sessionID -- 4.3.0.2 - added @sessionID criteria
		LEFT JOIN tblShipperSettlementBatch SB ON SB.ID = O.BatchID
		WHERE O.OrderID IN (SELECT ID FROM @IDs)

	-- do the SessionID logic
	IF (@StartSession = 1 AND EXISTS (SELECT * FROM #ret))
	BEGIN
		BEGIN TRAN RetrieveOrdersFinancialShipper
	
		/* 4.3.0.2 - generate the "next" sessionID record */
		INSERT INTO tblOrderSettlementSessionShipper(UserName) VALUES (@UserName)
		SELECT @sessionID = SCOPE_IDENTITY()

		UPDATE #ret SET SessionID = @sessionID, RateApplySel = 1, BatchSel = CASE WHEN HasError = 1 THEN 0 ELSE 1 END
		INSERT INTO tblOrderSettlementSelectionShipper (SessionID, OrderID, RateApplySel, BatchSel)
			SELECT @sessionID, ID, RateApplySel, BatchSel FROM #ret
	
		COMMIT TRAN RetrieveOrdersFinancialShipper

		/* "purge" the tblOrderSettlementSessionShipper table of "obsolete" records (older than a week old)
			NOTE: if this ends up being a performance hit, we should instead execute this via a scheduled task */
		DELETE FROM tblOrderSettlementSessionShipper WHERE CreateDateUTC < DATEADD(WEEK, -1, GETDATE())
	END
	
	-- return the data to the caller
	SELECT * FROM #ret
END

GO


/*************************************************/
-- Created: 2013.06.02 - Kevin Alons
-- Purpose: compute and add the various Carrier "Settlement" $$ values to an Delivered/Audited order
-- Changes:
-- 3.7.26	- 2015/06/13	- KDA	- add support for: Driver|DriverGroup, Producer to some rates, separate Carrier|Shipper MinSettlementUnits|SettlementFactor best-match values, 
-- 3.9.0	- 2015/08/13	- KDA	- honor Approve overrides in rating process
-- 3.9.9	- 2015/08/31	- KDA	- add new @AllowReApplyPostBatch parameter to allow re-applying after batch is applied
-- 3.9.19.2	- 2015/09/24	- KDA	- fix erroneous logic that computed Asssessorial Settlement results before the Order Level results were posted
-- 3.9.37	- 2015/12/22	- JAE	- Use tblOrderApproval to get override for Carrier Min Settlement Units
-- 3.9.39	- 2016/01/15	- JAE	- Also need to recalculate settlement units since the calculated/max values was already passed
-- 3.13.1	- 2016/07/04	- KDA	- add START/DONE PRINT statements (for debugging purposes)
-- 4.1.8.6	- 2016.09.21	- KDA	- move populate #IA table before transaction starts
--									- @debugStatements parameter + add additional START/DONE PRINT statements (for debugging purposes)
-- 4.1.9.1	- 2016.09.26	- KDA	- restore regressed 3.9.19.2 changes that accidentally got reverted with 4.1.8.6 (added a NOTE to hopefully prevent this again)
-- 4.4.1	- 2016/11/04	- JAE	- Add Destination Chainup
-- 4.6.2	- 2017.04.20	- KDA	- add @Notes parameter (and logic to persist to DB)
-- 4.6.4	- 2017-04-28	- JAE	- Added Miles to the Assessorial Amounts function call
-- 4.7.0	- 2017-05-12	- JAE	- Suppress check if settled is not final
/*************************************************/
ALTER PROCEDURE spApplyRatesCarrier
(
  @ID int
, @UserName varchar(100)
, @OriginWaitAmount money = NULL
, @DestWaitAmount money = NULL
, @RejectionAmount money = NULL
, @FuelSurchargeAmount money = NULL
, @LoadAmount money = NULL
, @Notes varchar(255) = NULL
, @AssessorialRateTypeID_CSV varchar(max) = NULL
, @AssessorialAmount_CSV varchar(max) = null
, @ResetOverrides bit = 0
, @AllowReApplyPostBatch bit = 0
, @debugStatements bit = 0 -- turn on to get debugging statements during execution
) AS BEGIN

	SET NOCOUNT ON

	IF (@debugStatements = 1) PRINT 'spApplyRatesCarrier (' + ltrim(@ID) + ') START ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	
	IF (@AllowReApplyPostBatch = 0) AND EXISTS (SELECT 1 FROM viewOrderSettlementCarrier WHERE OrderID = @ID AND BatchID IS NOT NULL AND IsFinal = 1)
	BEGIN
		RAISERROR('Invoice has already been Carrier Settled', 16, 1)
		RETURN
	END
	
	DECLARE @BatchID INT
	SELECT @BatchID = BatchID FROM viewOrderSettlementCarrier WHERE OrderID = @ID AND IsFinal = 0 -- Persist batch ID if pending batch

	/* ensure that Shipper Rates have been applied prior to applying Carrier rates (since they could be dependent on Shipper rates) */
	IF NOT EXISTS (SELECT * FROM tblOrderSettlementShipper WHERE OrderID = @ID)
		EXEC spApplyRatesShipper @ID, @UserName

	/* transform the provided manual Assessorial Rates (if any) into a queryable table variable */
	DECLARE @ManualAssessorialRates TABLE (ID int primary key, Amount money)
	INSERT INTO @ManualAssessorialRates (ID, Amount) 
		SELECT ID.ID, R.Amount
		FROM (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), ID FROM dbo.fnSplitCSVIDs(@AssessorialRateTypeID_CSV)) ID
		JOIN (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), amount = CAST(value as money) FROM dbo.fnSplitCSV(@AssessorialAmount_CSV)) R ON R.sortid = ID.sortid

	/* persist all existing MANUAL Assessorial Rates from the existing Order Settlement (if any present and not resetting them) */
	INSERT INTO @ManualAssessorialRates (ID, Amount)
		SELECT AssessorialRateTypeID, Amount 
		FROM dbo.tblOrderSettlementCarrierAssessorialCharge 
		WHERE OrderID = @ID AND AssessorialRateID IS NULL 
			AND ID NOT IN (SELECT ID FROM @ManualAssessorialRates)
			AND @ResetOverrides = 0

	/* persist all existing settlement amount overrides from the existing Order Settlement - if not resetting them */
	IF (@ResetOverrides = 0)
	BEGIN 
		SELECT @OriginWaitAmount = OriginWaitAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @OriginWaitAmount IS NULL AND OriginWaitRateID IS NULL
		SELECT @DestWaitAmount = DestinationWaitAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @DestWaitAmount IS NULL AND DestinationWaitRateID IS NULL
		SELECT @RejectionAmount = OrderRejectAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @RejectionAmount IS NULL AND OrderRejectRateID IS NULL
		SELECT @FuelSurchargeAmount = FuelSurchargeAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @FuelSurchargeAmount IS NULL AND FuelSurchargeRateID IS NULL
		SELECT @LoadAmount = LoadAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @LoadAmount IS NULL AND RouteRateID IS NULL AND RangeRateID IS NULL
	END
	/* persist any existing notes by preserving them if no new note is defined */
	SELECT @Notes = Notes FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @Notes IS NULL AND Notes IS NOT NULL

	/* compute the basic settlement units from the Order being invoices */
	DECLARE @SettlementUnits TABLE
	(
		OrderID int
	  , SettlementUomID int
	  , SettlementFactorID int
	  , CarrierSettlementFactorID int
	  , MinSettlementUnits decimal(18, 10)
	  , MinSettlementUnitsID int
	  , ActualUnits decimal(18, 10)
	  , SettlementUnits decimal(18, 10) -- calculate value basically max(actual, minimum)
	)
	INSERT INTO @SettlementUnits (OrderID, SettlementUomID, SettlementFactorID, CarrierSettlementFactorID, MinSettlementUnits, MinSettlementUnitsID, ActualUnits, SettlementUnits)
		SELECT OSU.OrderID, OSU.SettlementUomID, OSU.SettlementFactorID, OSU.CarrierSettlementFactorID, ISNULL(OA.OverrideCarrierMinSettlementUnits, OSU.MinSettlementUnits), OSU.MinSettlementUnitsID, OSU.ActualUnits, 
			-- use passed settlement (already calculated) or pick the max between the actual and overrride min - JAE 1/15/16 - 3.9.39
			CASE WHEN OA.OverrideCarrierMinSettlementUnits IS NULL THEN OSU.SettlementUnits
						ELSE dbo.fnMaxDecimal(OSU.ActualUnits, OA.OverrideCarrierMinSettlementUnits) END
		FROM dbo.viewOrderSettlementUnitsCarrier OSU
		LEFT JOIN tblOrderApproval OA ON OA.OrderID = OSU.OrderID
		WHERE OSU.OrderID = @ID

	/* compute the order level settlement rateIDs & settlement amounts - into temp table #I */
	SELECT OrderID = @ID
		, OrderDate
		, CarrierSettlementFactorID, MinSettlementUnitsID
		, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
		, RouteRateID, RangeRateID, LoadAmount
		, WaitFeeParameterID
		, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount 
		, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount
		, OrderRejectRateID, OrderRejectAmount
		, FuelSurchargeRateID, FuelSurchargeRate, FuelSurchargeAmount
		, OriginTaxRate
		, TotalAmount = ISNULL(LoadAmount, 0) 
			+ ISNULL(OriginWaitAmount, 0) 
			+ ISNULL(DestWaitAmount, 0) 
			+ ISNULL(OrderRejectAmount, 0) 
			+ ISNULL(FuelSurchargeAmount, 0)
		, CreatedByUser = @UserName
		, OriginChainUp, DestChainUp, H2S, Rerouted
	INTO #I
	FROM (
		SELECT OrderDate
			, CarrierSettlementFactorID, MinSettlementUnitsID
			, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
			, RouteRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RouteRateID ELSE NULL END
			, RangeRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RangeRateID ELSE NULL END
			, LoadAmount = round(ISNULL(@LoadAmount, RR.Amount), 2)
			, WaitFeeParameterID
			, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount = ROUND(OriginWaitAmount, 2)
			, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount = ROUND(DestWaitAmount, 2)
			, OrderRejectRateID = CASE WHEN @RejectionAmount IS NULL THEN RD.RateID ELSE NULL END
			, OrderRejectAmount = round(isnull(@RejectionAmount, RD.Amount), 2)
			, FuelSurchargeRateID = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.RateID ELSE NULL END
			, FuelSurchargeRate = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.Rate ELSE NULL END
			, FuelSurchargeAmount = round(isnull(@FuelSurchargeAmount, FSR.Amount), 2)
			, OriginTaxRate
			, OriginChainUp, DestChainup, H2S, Rerouted = cast(CASE WHEN RerouteCount = 0 THEN 0 ELSE 1 END as bit) /* used for the Auto-Approve logic below */
		FROM (
			SELECT O.ID
				, O.OriginID
				, O.CarrierID
				, O.RouteID
				, SU.CarrierSettlementFactorID
				, SU.SettlementFactorID
				, SU.SettlementUomID
				, SU.ActualUnits
				, SU.MinSettlementUnitsID
				, SU.MinSettlementUnits
				, SettlementUnits = CASE WHEN O.Rejected = 1 THEN 0 ELSE SU.SettlementUnits END
				, RouteMiles = isnull(OA.OverrideActualMiles, O.ActualMiles)
				, OrderDate = isnull(O.OrderDate, O.DueDate)
				, OriginChainUp = cast(CASE WHEN OA.OverrideOriginChainup = 1 THEN 0 ELSE O.OriginChainup END as bit)
				, DestChainUp = cast(CASE WHEN OA.OverrideDestChainup = 1 THEN 0 ELSE O.DestChainup END as bit)
				, H2S = cast(CASE WHEN OA.OverrideH2S = 1 THEN 0 ELSE O.H2S END as bit)
				, O.Rejected
				, RerouteCount = CASE WHEN OA.OverrideReroute = 1 THEN 0 ELSE O.RerouteCount END
				, IsSplit = CASE WHEN O.Rejected = 0 AND O.TicketCount > 1 THEN 1 ELSE 0 END
				, WaitFeeParameterID = WFP.ID
				, OriginWaitRateID = CASE WHEN @OriginWaitAmount IS NULL THEN OWD.RateID ELSE NULL END
				, OriginWaitBillableMinutes = OWD.BillableMinutes
				, OriginWaitBillableHours = OWD.BillableHours
				, OriginWaitAmount = isnull(@OriginWaitAmount, OWD.Amount)
				, DestWaitRateID = CASE WHEN @DestWaitAmount IS NULL THEN DWD.RateID ELSE NULL END
				, DestWaitBillableMinutes = DWD.BillableMinutes
				, DestWaitBillableHours = DWD.BillableHours
				, DestWaitAmount = isnull(@DestWaitAmount, DWD.Amount)
				, OriginTaxRate = OO.TaxRate
			FROM dbo.viewOrder O
			JOIN @SettlementUnits SU ON SU.OrderID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
			OUTER APPLY dbo.fnOrderCarrierOriginWaitData(@ID) OWD 
			OUTER APPLY dbo.fnOrderCarrierDestinationWaitData(@ID) DWD 
			OUTER APPLY dbo.fnOrderCarrierWaitFeeParameter(@ID) WFP
			WHERE O.ID = @ID
		) X
		OUTER APPLY dbo.fnOrderCarrierLoadAmount(@ID, SettlementUnits, SettlementUomID) RR 
		OUTER APPLY dbo.fnOrderCarrierOrderRejectData(@ID, SettlementUnits, SettlementUomID) RD
		OUTER APPLY dbo.fnOrderCarrierFuelSurchargeData(@ID) FSR
	) X2

	DECLARE @CreatedTran bit; SET @CreatedTran = CASE WHEN @@TRANCOUNT = 0 THEN 1 ELSE 0 END
	BEGIN TRY
		IF (@CreatedTran = 1)
			BEGIN TRAN

		-- remove the existing settlment record (if any)
		DELETE FROM tblOrderSettlementCarrierAssessorialCharge WHERE OrderID = @ID
		DELETE FROM tblOrderSettlementCarrier WHERE OrderID = @ID
	
		-- all Units and Rates are first normalized to the Order.OriginUOM then consistent processing
		-- and persisted in the Invoice (Settlement) record in this Order.OriginUOM 
		INSERT INTO tblOrderSettlementCarrier 
			(OrderID
			, OrderDate
			, CarrierSettlementFactorID
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnitsID
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestinationWaitRateID 
			, DestinationWaitBillableMinutes 
			, DestinationWaitBillableHours
			, DestinationWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser
			, Notes
			, BatchID)
		SELECT OrderID
			, OrderDate
			, CarrierSettlementFactorID
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnitsID
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestWaitRateID 
			, DestWaitBillableMinutes 
			, DestWaitBillableHours
			, DestWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser
			, left(@Notes, 255)
			, @BatchID
		FROM #I

		/* compute the new Assessorial Rates for this order - into temp table #IA */
		/* NOTE: while it would be preferred for this to be out of the TX, it can't because it relies on the final Settlement changes being present */
		SELECT OrderID = @ID, AssessorialRateTypeID, AssessorialRateID = RateID, Amount = ROUND(Amount, 2), CreatedByUser = @UserName 
		INTO #IA
		FROM @SettlementUnits SU
		CROSS APPLY dbo.fnOrderCarrierAssessorialAmounts(@ID, SU.SettlementUnits, SU.SettlementUomID, (SELECT FinalActualMiles FROM viewOrder_Financial_Base_Shipper where ID = @ID)) CAA
		WHERE CAA.AssessorialRateTypeID NOT IN (SELECT ID FROM @ManualAssessorialRates)

		/* apply the computed settlement assessorial rate/amount records to the DB */
		INSERT INTO tblOrderSettlementCarrierAssessorialCharge (OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser)
			SELECT OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser FROM #IA
			UNION
			SELECT @ID, ID, NULL, Amount, @UserName FROM @ManualAssessorialRates

		IF (@CreatedTran = 1)
			COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @msg varchar(max), @severity int
		SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY()
		ROLLBACK
		RAISERROR(@msg, @severity, 1)
		RETURN
	END CATCH	

	IF (@debugStatements = 1) PRINT 'spApplyRatesCarrier (' + ltrim(@ID) + ') DONE ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	
END

GO


/*************************************************/
-- Created: 4.1.0 - 2016.08.22 - Kevin Alons
-- Purpose: compute and add the various Driver "Settlement" $$ values to an Delivered/Audited order
-- Changes:
-- 4.1.8.6	- 2016.09.21	- KDA	- move populate #IA table before transaction starts
--									- @debugStatements parameter + add additional START/DONE PRINT statements (for debugging purposes)
-- 4.1.9.1	- 2016.09.26	- KDA	- restore regressed 3.9.19.2 changes that accidentally got reverted with 4.1.8.6 (added a NOTE to hopefully prevent this again)
-- 4.4.1	- 2016/11/04	- JAE	- Add Destination Chainup
-- 4.6.2	- 2017.04.20	- KDA	- add @Notes parameter (and logic to persist to DB)
-- 4.6.4	- 2017-04-28	- JAE	- Added Miles to the Assessorial Amounts function call
-- 4.7.0	- 2017-05-12	- JAE	- Suppress check if settled is not final
/*************************************************/
ALTER PROCEDURE spApplyRatesDriver
(
  @ID int
, @UserName varchar(100)
, @OriginWaitAmount money = NULL
, @DestWaitAmount money = NULL
, @RejectionAmount money = NULL
, @FuelSurchargeAmount money = NULL
, @LoadAmount money = NULL
, @Notes varchar(255) = NULL
, @AssessorialRateTypeID_CSV varchar(max) = NULL
, @AssessorialAmount_CSV varchar(max) = null
, @ResetOverrides bit = 0
, @AllowReApplyPostBatch bit = 0
, @debugStatements bit = 0 -- turn on to get debugging statements during execution
) AS BEGIN

	SET NOCOUNT ON

	IF (@debugStatements = 1) PRINT 'spApplyRatesDriver (' + ltrim(@ID) + ') START ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	
	IF (@AllowReApplyPostBatch = 0) AND EXISTS (SELECT 1 FROM viewOrderSettlementDriver WHERE OrderID = @ID AND BatchID IS NOT NULL AND IsFinal = 1)
	BEGIN
		RAISERROR('Invoice has already been Driver Settled', 16, 1)
		RETURN
	END
	
	DECLARE @BatchID INT
	SELECT @BatchID = BatchID FROM viewOrderSettlementDriver WHERE OrderID = @ID AND IsFinal = 0 -- Persist batch ID if pending batch

	/* ensure that Carrier Rates have been applied prior to applying Driver rates (since they could be dependent on Carrier rates) */
	IF NOT EXISTS (SELECT * FROM tblOrderSettlementCarrier WHERE OrderID = @ID)
		EXEC spApplyRatesCarrier @ID, @UserName

	/* transform the provided manual Assessorial Rates (if any) into a queryable table variable */
	DECLARE @ManualAssessorialRates TABLE (ID int primary key, Amount money)
	INSERT INTO @ManualAssessorialRates (ID, Amount) 
		SELECT ID.ID, R.Amount
		FROM (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), ID FROM dbo.fnSplitCSVIDs(@AssessorialRateTypeID_CSV)) ID
		JOIN (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), amount = CAST(value as money) FROM dbo.fnSplitCSV(@AssessorialAmount_CSV)) R ON R.sortid = ID.sortid

	/* persist all existing MANUAL Assessorial Rates from the existing Order Settlement (if any present and not resetting them) */
	INSERT INTO @ManualAssessorialRates (ID, Amount)
		SELECT AssessorialRateTypeID, Amount 
		FROM dbo.tblOrderSettlementDriverAssessorialCharge 
		WHERE OrderID = @ID AND AssessorialRateID IS NULL 
			AND ID NOT IN (SELECT ID FROM @ManualAssessorialRates)
			AND @ResetOverrides = 0

	/* persist all existing settlement amount overrides from the existing Order Settlement - if not resetting them */
	IF (@ResetOverrides = 0)
	BEGIN 
		SELECT @OriginWaitAmount = OriginWaitAmount FROM tblOrderSettlementDriver WHERE OrderID = @ID AND @OriginWaitAmount IS NULL AND OriginWaitRateID IS NULL
		SELECT @DestWaitAmount = DestinationWaitAmount FROM tblOrderSettlementDriver WHERE OrderID = @ID AND @DestWaitAmount IS NULL AND DestinationWaitRateID IS NULL
		SELECT @RejectionAmount = OrderRejectAmount FROM tblOrderSettlementDriver WHERE OrderID = @ID AND @RejectionAmount IS NULL AND OrderRejectRateID IS NULL
		SELECT @FuelSurchargeAmount = FuelSurchargeAmount FROM tblOrderSettlementDriver WHERE OrderID = @ID AND @FuelSurchargeAmount IS NULL AND FuelSurchargeRateID IS NULL
		SELECT @LoadAmount = LoadAmount FROM tblOrderSettlementDriver WHERE OrderID = @ID AND @LoadAmount IS NULL AND RouteRateID IS NULL AND RangeRateID IS NULL
	END
	/* persist any existing notes by preserving them if no new note is defined */
	SELECT @Notes = Notes FROM tblOrderSettlementDriver WHERE OrderID = @ID AND @Notes IS NULL AND Notes IS NOT NULL

	/* compute the basic settlement units from the Order being invoices */
	DECLARE @SettlementUnits TABLE
	(
		OrderID int
	  , SettlementUomID int
	  , SettlementFactorID int
	  , CarrierSettlementFactorID int
	  , MinSettlementUnits decimal(18, 10)
	  , MinSettlementUnitsID int
	  , ActualUnits decimal(18, 10)
	  , SettlementUnits decimal(18, 10) -- calculate value basically max(actual, minimum)
	)
	INSERT INTO @SettlementUnits (OrderID, SettlementUomID, SettlementFactorID, CarrierSettlementFactorID, MinSettlementUnits, MinSettlementUnitsID, ActualUnits, SettlementUnits)
		SELECT OSU.OrderID, OSU.SettlementUomID, OSU.SettlementFactorID, OSU.CarrierSettlementFactorID, ISNULL(OA.OverrideCarrierMinSettlementUnits, OSU.MinSettlementUnits), OSU.MinSettlementUnitsID, OSU.ActualUnits
			-- use passed settlement (already calculated) or pick the max between the actual and overrride min - JAE 1/15/16 - 3.9.39
			, CASE WHEN OA.OverrideCarrierMinSettlementUnits IS NULL THEN OSU.SettlementUnits
					ELSE dbo.fnMaxDecimal(OSU.ActualUnits, OA.OverrideCarrierMinSettlementUnits) END
		FROM dbo.viewOrderSettlementUnitsCarrier OSU
		LEFT JOIN tblOrderApproval OA ON OA.OrderID = OSU.OrderID
		WHERE OSU.OrderID = @ID

	/* compute the order level settlement rateIDs & settlement amounts - into temp table #I */
	SELECT OrderID = @ID
		, OrderDate
		, CarrierSettlementFactorID, MinSettlementUnitsID
		, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
		, RouteRateID, RangeRateID, LoadAmount
		, WaitFeeParameterID
		, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount 
		, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount
		, OrderRejectRateID, OrderRejectAmount
		, FuelSurchargeRateID, FuelSurchargeRate, FuelSurchargeAmount
		, OriginTaxRate
		, TotalAmount = ISNULL(LoadAmount, 0) 
			+ ISNULL(OriginWaitAmount, 0) 
			+ ISNULL(DestWaitAmount, 0) 
			+ ISNULL(OrderRejectAmount, 0) 
			+ ISNULL(FuelSurchargeAmount, 0)
		, CreatedByUser = @UserName
		, OriginChainUp, DestChainUp, H2S, Rerouted
	INTO #I
	FROM (
		SELECT OrderDate
			, CarrierSettlementFactorID, MinSettlementUnitsID
			, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
			, RouteRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RouteRateID ELSE NULL END
			, RangeRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RangeRateID ELSE NULL END
			, LoadAmount = round(ISNULL(@LoadAmount, RR.Amount), 2)
			, WaitFeeParameterID
			, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount = ROUND(OriginWaitAmount, 2)
			, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount = ROUND(DestWaitAmount, 2)
			, OrderRejectRateID = CASE WHEN @RejectionAmount IS NULL THEN RD.RateID ELSE NULL END
			, OrderRejectAmount = round(isnull(@RejectionAmount, RD.Amount), 2)
			, FuelSurchargeRateID = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.RateID ELSE NULL END
			, FuelSurchargeRate = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.Rate ELSE NULL END
			, FuelSurchargeAmount = round(isnull(@FuelSurchargeAmount, FSR.Amount), 2)
			, OriginTaxRate
			, OriginChainUp, DestChainup, H2S, Rerouted = cast(CASE WHEN RerouteCount = 0 THEN 0 ELSE 1 END as bit) /* used for the Auto-Approve logic below */
		FROM (
			SELECT O.ID
				, O.OriginID
				, O.CarrierID
				, O.RouteID
				, SU.CarrierSettlementFactorID
				, SU.SettlementFactorID
				, SU.SettlementUomID
				, SU.ActualUnits
				, SU.MinSettlementUnitsID
				, SU.MinSettlementUnits
				, SettlementUnits = CASE WHEN O.Rejected = 1 THEN 0 ELSE SU.SettlementUnits END
				, RouteMiles = isnull(OA.OverrideActualMiles, O.ActualMiles)
				, OrderDate = isnull(O.OrderDate, O.DueDate)
				, OriginChainUp = cast(CASE WHEN OA.OverrideOriginChainup = 1 THEN 0 ELSE O.OriginChainup END as bit)
				, DestChainUp = cast(CASE WHEN OA.OverrideDestChainup = 1 THEN 0 ELSE O.DestChainup END as bit)
				, H2S = cast(CASE WHEN OA.OverrideH2S = 1 THEN 0 ELSE O.H2S END as bit)
				, O.Rejected
				, RerouteCount = CASE WHEN OA.OverrideReroute = 1 THEN 0 ELSE O.RerouteCount END
				, IsSplit = CASE WHEN O.Rejected = 0 AND O.TicketCount > 1 THEN 1 ELSE 0 END
				, WaitFeeParameterID = WFP.ID
				, OriginWaitRateID = CASE WHEN @OriginWaitAmount IS NULL THEN OWD.RateID ELSE NULL END
				, OriginWaitBillableMinutes = OWD.BillableMinutes
				, OriginWaitBillableHours = OWD.BillableHours
				, OriginWaitAmount = isnull(@OriginWaitAmount, OWD.Amount)
				, DestWaitRateID = CASE WHEN @DestWaitAmount IS NULL THEN DWD.RateID ELSE NULL END
				, DestWaitBillableMinutes = DWD.BillableMinutes
				, DestWaitBillableHours = DWD.BillableHours
				, DestWaitAmount = isnull(@DestWaitAmount, DWD.Amount)
				, OriginTaxRate = OO.TaxRate
			FROM dbo.viewOrder O
			JOIN @SettlementUnits SU ON SU.OrderID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
			OUTER APPLY dbo.fnOrderDriverOriginWaitData(@ID) OWD 
			OUTER APPLY dbo.fnOrderDriverDestinationWaitData(@ID) DWD 
			OUTER APPLY dbo.fnOrderCarrierWaitFeeParameter(@ID) WFP
			WHERE O.ID = @ID
		) X
		OUTER APPLY dbo.fnOrderDriverLoadAmount(@ID, SettlementUnits, SettlementUomID) RR 
		OUTER APPLY dbo.fnOrderDriverOrderRejectData(@ID, SettlementUnits, SettlementUomID) RD
		OUTER APPLY dbo.fnOrderDriverFuelSurchargeData(@ID) FSR
	) X2

	DECLARE @CreatedTran bit; SET @CreatedTran = CASE WHEN @@TRANCOUNT = 0 THEN 1 ELSE 0 END
	BEGIN TRY
		IF (@CreatedTran = 1)
			BEGIN TRAN

		-- remove the existing settlment record (if any)
		DELETE FROM tblOrderSettlementDriverAssessorialCharge WHERE OrderID = @ID
		DELETE FROM tblOrderSettlementDriver WHERE OrderID = @ID
	
		-- all Units and Rates are first normalized to the Order.OriginUOM then consistent processing
		-- and persisted in the Invoice (Settlement) record in this Order.OriginUOM 
		INSERT INTO tblOrderSettlementDriver 
			(OrderID
			, OrderDate
			, CarrierSettlementFactorID
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnitsID
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestinationWaitRateID 
			, DestinationWaitBillableMinutes 
			, DestinationWaitBillableHours
			, DestinationWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser
			, Notes
			, BatchID)
		SELECT OrderID
			, OrderDate
			, CarrierSettlementFactorID
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnitsID
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestWaitRateID 
			, DestWaitBillableMinutes 
			, DestWaitBillableHours
			, DestWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser
			, left(@Notes, 255)
			, @BatchID
		FROM #I

		/* compute the new Assessorial Rates for this order - into temp table #IA */
		/* NOTE: while it would be preferred for this to be out of the TX, it can't because it relies on the final Settlement changes being present */
		SELECT OrderID = @ID, AssessorialRateTypeID, AssessorialRateID = RateID, Amount = ROUND(Amount, 2), CreatedByUser = @UserName 
		INTO #IA
		FROM @SettlementUnits SU
		CROSS APPLY dbo.fnOrderDriverAssessorialAmounts(@ID, SU.SettlementUnits, SU.SettlementUomID,  (SELECT FinalActualMiles FROM viewOrder_Financial_Base_Driver where ID = @ID)) AA
		WHERE AA.AssessorialRateTypeID NOT IN (SELECT ID FROM @ManualAssessorialRates)

		/* apply the computed settlement assessorial rate/amount records to the DB */
		INSERT
		 INTO tblOrderSettlementDriverAssessorialCharge (OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser)
			SELECT OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser FROM #IA
			UNION
			SELECT @ID, ID, NULL, Amount, @UserName FROM @ManualAssessorialRates

		IF (@CreatedTran = 1)
			COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @msg varchar(max), @severity int
		SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY()
		ROLLBACK
		RAISERROR(@msg, @severity, 1)
		RETURN
	END CATCH	

	IF (@debugStatements = 1) PRINT 'spApplyRatesDriver (' + ltrim(@ID) + ') DONE ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	
END

GO




/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Shipper "Settlement" $$ values to an Delivered/Audited order
-- Changes:
-- 3.9.0	- 2015/08/13	- KDA	- add Auto-Approve logic
--										- honor Approve overrides in rating process
-- 3.9.9	- 2015/08/31	- KDA	- add new @AllowReApplyPostBatch parameter to allow re-applying after batch is applied
-- 3.9.19.2	- 2015/09/24	- KDA	- fix erroneous logic that computed Asssessorial Settlement results before the Order Level results were posted
-- 3.9.37	- 2015/12/22	- JAE	- Use tblOrderApproval to get override for Shipper Min Settlement Units
-- 3.9.39	- 2016/01/15	- JAE	- Also need to recalculate settlement units since the calculated/max values was already passed
-- 3.13.1	- 2016/07/04	- KDA	- add START/DONE PRINT statements (for debugging purposes)
-- 4.1.8.6	- 2016.09.21	- KDA	- move populate #IA table before transaction starts
--									- fix erroneous OR ActualMiles criteria (needed to be wrapped in () to avoid unnecessary processing)
--									- @debugStatements parameter + add additional START/DONE PRINT statements (for debugging purposes)
-- 4.1.9.1	- 2016.09.26	- KDA	- restore regressed 3.9.19.2 changes that accidentally got reverted with 4.1.8.6 (added a NOTE to hopefully prevent this again)
-- 4.4.1	- 2016/11/04	- JAE	- Add Destination Chainup
-- 4.6.2	- 2017.04.20	- KDA	- add @Notes parameter (and logic to persist to DB)
-- 4.6.4	- 2017-04-28	- JAE	- Added Miles to the Assessorial Amounts function call
-- 4.7.0	- 2017-05-12	- JAE	- Suppress check if settled is not final
/**********************************/
ALTER PROCEDURE spApplyRatesShipper
(
  @ID int
, @UserName varchar(100)
, @OriginWaitAmount money = NULL
, @DestWaitAmount money = NULL
, @RejectionAmount money = NULL
, @FuelSurchargeAmount money = NULL
, @LoadAmount money = NULL
, @Notes varchar(255) = NULL
, @AssessorialRateTypeID_CSV varchar(max) = NULL
, @AssessorialAmount_CSV varchar(max) = null
, @ResetOverrides bit = 0
, @AllowReApplyPostBatch bit = 0
, @debugStatements bit = 0 -- turn on to get debugging statements during execution
) AS BEGIN

	SET NOCOUNT ON
	
	IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ') START ' + (CONVERT(VARCHAR(24), GETDATE(), 121))

	-- ensure this order hasn't yet been fully settled
	IF (@AllowReApplyPostBatch = 0) AND EXISTS (SELECT 1 FROM viewOrderSettlementShipper WHERE OrderID = @ID AND BatchID IS NOT NULL AND IsFinal = 1)
	BEGIN
		RAISERROR('Invoice has already been Shipper Settled', 16, 1)
		RETURN
	END

	DECLARE @BatchID INT
	SELECT @BatchID = BatchID FROM viewOrderSettlementShipper WHERE OrderID = @ID AND IsFinal = 0 -- Persist batch ID if pending batch

	IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ').UpdateActualMiles START ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	-- ensure the current Route.ActualMiles is assigned to the order
	UPDATE tblOrder SET ActualMiles = R.ActualMiles
	FROM tblOrder O
	JOIN tblRoute R ON R.ID = O.RouteID
	LEFT JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = O.ID
	WHERE O.ID = @ID
	  AND (O.ActualMiles IS NULL OR O.ActualMiles <> R.ActualMiles)
	  AND (@AllowReApplyPostBatch = 1 OR OSS.BatchID IS NULL)
	IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ').UpdateActualMiles DONE ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	
	/* transform the provided manual Assessorial Rates (if any) into a queryable table variable */
	DECLARE @ManualAssessorialRates TABLE (ID int primary key, Amount money)
	INSERT INTO @ManualAssessorialRates (ID, Amount) 
		SELECT ID.ID, R.Amount
		FROM (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), ID FROM dbo.fnSplitCSVIDs(@AssessorialRateTypeID_CSV)) ID
		JOIN (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), amount = CAST(value as money) FROM dbo.fnSplitCSV(@AssessorialAmount_CSV)) R ON R.sortid = ID.sortid

	/* persist all existing MANUAL Assessorial Rates from the existing Order Settlement (if any present and not resetting them) */
	INSERT INTO @ManualAssessorialRates (ID, Amount)
		SELECT AssessorialRateTypeID, Amount 
		FROM dbo.tblOrderSettlementShipperAssessorialCharge 
		WHERE OrderID = @ID AND AssessorialRateID IS NULL 
			AND ID NOT IN (SELECT ID FROM @ManualAssessorialRates)
			AND @ResetOverrides = 0

	/* persist all existing settlement amount overrides from the existing Order Settlement - if not resetting them */
	IF (@ResetOverrides = 0)
	BEGIN 
		SELECT @OriginWaitAmount = OriginWaitAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @OriginWaitAmount IS NULL AND OriginWaitRateID IS NULL
		SELECT @DestWaitAmount = DestinationWaitAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @DestWaitAmount IS NULL AND DestinationWaitRateID IS NULL
		SELECT @RejectionAmount = OrderRejectAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @RejectionAmount IS NULL AND OrderRejectRateID IS NULL
		SELECT @FuelSurchargeAmount = FuelSurchargeAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @FuelSurchargeAmount IS NULL AND FuelSurchargeRateID IS NULL
		SELECT @LoadAmount = LoadAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @LoadAmount IS NULL AND RouteRateID IS NULL AND RangeRateID IS NULL
	END
	/* persist any existing notes by preserving them if no new note is defined */
	SELECT @Notes = Notes FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @Notes IS NULL AND Notes IS NOT NULL

	/* compute the basic settlement units from the Order being invoices */
	DECLARE @SettlementUnits TABLE
	(
		OrderID int
	  , SettlementUomID int
	  , SettlementFactorID int
	  , ShipperSettlementFactorID int
	  , MinSettlementUnits decimal(18, 10)
	  , MinSettlementUnitsID int
	  , ActualUnits decimal(18, 10)
	  , SettlementUnits decimal(18, 10)
	)
	IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ').RetrieveSettlementUnits START ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	INSERT INTO @SettlementUnits (OrderID, SettlementUomID, SettlementFactorID, ShipperSettlementFactorID, MinSettlementUnits, MinSettlementUnitsID, ActualUnits, SettlementUnits)
		SELECT OSU.OrderID, OSU.SettlementUomID, OSU.SettlementFactorID, OSU.ShipperSettlementFactorID, ISNULL(OA.OverrideShipperMinSettlementUnits, OSU.MinSettlementUnits), OSU.MinSettlementUnitsID, OSU.ActualUnits, 
			-- use passed settlement (already calculated) or pick the max between the actual and overrride min - JAE 1/15/16 - 3.9.39
			CASE WHEN OA.OverrideShipperMinSettlementUnits IS NULL THEN OSU.SettlementUnits
						ELSE dbo.fnMaxDecimal(OSU.ActualUnits, OA.OverrideShipperMinSettlementUnits) END
		FROM dbo.viewOrderSettlementUnitsShipper OSU
		LEFT JOIN tblOrderApproval OA ON OA.OrderID = OSU.OrderID
		WHERE OSU.OrderID = @ID
	IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ').RetrieveSettlementUnits DONE ' + (CONVERT(VARCHAR(24), GETDATE(), 121))

	/* compute the order level settlement rateIDs & settlement amounts - into temp table #I */
	IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ').RetrieveData START ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	SELECT OrderID = @ID
		, OrderDate
		, ShipperSettlementFactorID, MinSettlementUnitsID
		, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
		, RouteRateID, RangeRateID, LoadAmount
		, WaitFeeParameterID
		, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount 
		, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount
		, OrderRejectRateID, OrderRejectAmount
		, FuelSurchargeRateID, FuelSurchargeRate, FuelSurchargeAmount
		, OriginTaxRate
		, TotalAmount = ISNULL(LoadAmount, 0) 
			+ ISNULL(OriginWaitAmount, 0) 
			+ ISNULL(DestWaitAmount, 0) 
			+ ISNULL(OrderRejectAmount, 0) 
			+ ISNULL(FuelSurchargeAmount, 0)
		, CreatedByUser = @UserName
		, OriginChainup, DestChainup, H2S, Rerouted /* used for the Auto-Approve logic below */
	INTO #I
	FROM (
		SELECT OrderDate
			, ShipperSettlementFactorID, MinSettlementUnitsID
			, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
			, RouteRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RouteRateID ELSE NULL END
			, RangeRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RangeRateID ELSE NULL END
			, LoadAmount = round(ISNULL(@LoadAmount, RR.Amount), 2)
			, WaitFeeParameterID
			, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount = ROUND(OriginWaitAmount, 2)
			, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount = ROUND(DestWaitAmount, 2)
			, OrderRejectRateID = CASE WHEN @RejectionAmount IS NULL THEN RD.RateID ELSE NULL END
			, OrderRejectAmount = round(isnull(@RejectionAmount, RD.Amount), 2)
			, FuelSurchargeRateID = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.RateID ELSE NULL END
			, FuelSurchargeRate = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.Rate ELSE NULL END
			, FuelSurchargeAmount = round(isnull(@FuelSurchargeAmount, FSR.Amount), 2)
			, OriginTaxRate
			, OriginChainup, DestChainup, H2S, Rerouted = cast(CASE WHEN RerouteCount = 0 THEN 0 ELSE 1 END as bit) /* used for the Auto-Approve logic below */
		FROM (
			SELECT O.ID
				, O.OriginID
				, O.CustomerID
				, O.RouteID
				, SU.ShipperSettlementFactorID
				, SU.SettlementFactorID
				, SU.SettlementUomID
				, SU.ActualUnits
				, SU.MinSettlementUnitsID
				, SU.MinSettlementUnits
				, SettlementUnits = CASE WHEN O.Rejected = 1 THEN 0 ELSE SU.SettlementUnits END
				, RouteMiles = isnull(OA.OverrideActualMiles, O.ActualMiles)
				, OrderDate = isnull(O.OrderDate, O.DueDate)
				, OriginChainUp = cast(CASE WHEN OA.OverrideOriginChainup = 1 THEN 0 ELSE O.OriginChainup END as bit)
				, DestChainUp = cast(CASE WHEN OA.OverrideDestChainup = 1 THEN 0 ELSE O.DestChainup END as bit)
				, H2S = cast(CASE WHEN OA.OverrideH2S = 1 THEN 0 ELSE O.H2S END as bit)
				, O.Rejected
				, RerouteCount = CASE WHEN OA.OverrideReroute = 1 THEN 0 ELSE O.RerouteCount END
				, IsSplit = CASE WHEN O.Rejected = 0 AND O.TicketCount > 1 THEN 1 ELSE 0 END
				, WaitFeeParameterID = WFP.ID
				, OriginWaitRateID = CASE WHEN @OriginWaitAmount IS NULL THEN OWD.RateID ELSE NULL END
				, OriginWaitBillableMinutes = OWD.BillableMinutes
				, OriginWaitBillableHours = OWD.BillableHours
				, OriginWaitAmount = isnull(@OriginWaitAmount, OWD.Amount)
				, DestWaitRateID = CASE WHEN @DestWaitAmount IS NULL THEN DWD.RateID ELSE NULL END
				, DestWaitBillableMinutes = DWD.BillableMinutes
				, DestWaitBillableHours = DWD.BillableHours
				, DestWaitAmount = isnull(@DestWaitAmount, DWD.Amount)
				, OriginTaxRate = OO.TaxRate
			FROM dbo.viewOrder O
			JOIN @SettlementUnits SU ON SU.OrderID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
			OUTER APPLY dbo.fnOrderShipperOriginWaitData(@ID) OWD 
			OUTER APPLY dbo.fnOrderShipperDestinationWaitData(@ID) DWD 
			OUTER APPLY dbo.fnOrderShipperWaitFeeParameter(@ID) WFP
			WHERE O.ID = @ID
		) X
		OUTER APPLY dbo.fnOrderShipperLoadAmount(@ID, SettlementUnits, SettlementUomID) RR 
		OUTER APPLY dbo.fnOrderShipperOrderRejectData(@ID, SettlementUnits, SettlementUomID) RD
		OUTER APPLY dbo.fnOrderShipperFuelSurchargeData(@ID) FSR
	) X2
	IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ').RetrieveData DONE ' + (CONVERT(VARCHAR(24), GETDATE(), 121))

	IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ').StoreData START ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	DECLARE @CreatedTran bit; SET @CreatedTran = CASE WHEN @@TRANCOUNT = 0 THEN 1 ELSE 0 END
	BEGIN TRY
		IF (@CreatedTran = 1)
			BEGIN TRAN

		-- remove the existing settlment record (if any)
		DELETE FROM tblOrderSettlementShipperAssessorialCharge WHERE OrderID = @ID
		DELETE FROM tblOrderSettlementShipper WHERE OrderID = @ID

		-- all Units and Rates are first normalized to the Order.OriginUOM then consistent processing
		-- and persisted in the Invoice (Settlement) record in this Order.OriginUOM 
		INSERT INTO tblOrderSettlementShipper (
/*1*/		OrderID
/*2*/		, OrderDate
/*3*/		, ShipperSettlementFactorID
/*4*/		, SettlementFactorID 
/*5*/		, SettlementUomID 
/*6*/		, MinSettlementUnitsID
/*7*/		, MinSettlementUnits 
/*8*/		, SettlementUnits
/*9*/		, RouteRateID
/*10*/		, RangeRateID 
/*11*/		, LoadAmount
/*12*/		, WaitFeeParameterID 
/*13*/		, OriginWaitRateID 
/*14*/		, OriginWaitBillableMinutes 
/*15*/		, OriginWaitBillableHours
/*16*/		, OriginWaitAmount 
/*17*/		, DestinationWaitRateID 
/*18*/		, DestinationWaitBillableMinutes 
/*19*/		, DestinationWaitBillableHours
/*20*/		, DestinationWaitAmount 
/*21*/		, OrderRejectRateID 
/*22*/		, OrderRejectAmount 
/*23*/		, FuelSurchargeRateID 
/*24*/		, FuelSurchargeRate 
/*25*/		, FuelSurchargeAmount 
/*26*/		, OriginTaxRate 
/*27*/		, TotalAmount 
/*28*/		, CreatedByUser
/*29*/		, Notes
/*30*/		, BatchID)
SELECT 
/*01*/		OrderID
/*02*/		, OrderDate
/*03*/		, ShipperSettlementFactorID
/*04*/		, SettlementFactorID 
/*05*/		, SettlementUomID 
/*06*/		, MinSettlementUnitsID
/*07*/		, MinSettlementUnits 
/*08*/		, SettlementUnits
/*09*/		, RouteRateID
/*10*/		, RangeRateID 
/*11*/		, LoadAmount
/*12*/		, WaitFeeParameterID 
/*13*/		, OriginWaitRateID 
/*14*/		, OriginWaitBillableMinutes 
/*15*/		, OriginWaitBillableHours
/*16*/		, OriginWaitAmount 
/*17*/		, DestWaitRateID 
/*18*/		, DestWaitBillableMinutes 
/*19*/		, DestWaitBillableHours
/*20*/		, DestWaitAmount 
/*21*/		, OrderRejectRateID 
/*22*/		, OrderRejectAmount 
/*23*/		, FuelSurchargeRateID 
/*24*/		, FuelSurchargeRate 
/*25*/		, FuelSurchargeAmount 
/*26*/		, OriginTaxRate 
/*27*/		, TotalAmount 
/*28*/		, CreatedByUser
/*29*/		, left(@Notes, 255)
/*30*/		, @BatchID -- persist batchID if it was a pending settle
		FROM #I

		IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ').RetrieveAssessorialData START ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
		/* compute the new Assessorial Rates for this order - into temp table #IA */
		/* NOTE: while it would be preferred for this to be out of the TX, it can't because it relies on the final Settlement changes being present */
		SELECT OrderID = @ID, AssessorialRateTypeID, AssessorialRateID = RateID, Amount = ROUND(Amount, 2), CreatedByUser = @UserName 
		INTO #IA
		FROM @SettlementUnits SU
		CROSS APPLY dbo.fnOrderShipperAssessorialAmounts(@ID, SU.SettlementUnits, SU.SettlementUomID, (SELECT FinalActualMiles FROM viewOrder_Financial_Base_Shipper where ID = @ID)) CAA
		WHERE CAA.AssessorialRateTypeID NOT IN (SELECT ID FROM @ManualAssessorialRates)
		IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ').RetrieveAssessorialData DONE ' + (CONVERT(VARCHAR(24), GETDATE(), 121))

		/* apply the computed settlement assessorial rate/amount records to the DB */
		INSERT INTO tblOrderSettlementShipperAssessorialCharge (OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser)
			SELECT OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser FROM #IA
			UNION
			SELECT @ID, ID, NULL, Amount, @UserName FROM @ManualAssessorialRates

		IF (@CreatedTran = 1)
			COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @msg varchar(max), @severity int
		SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY()
		ROLLBACK
		RAISERROR(@msg, @severity, 1)
		RETURN
	END CATCH	
	IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ').StoreData DONE ' + (CONVERT(VARCHAR(24), GETDATE(), 121))

	IF (@debugStatements = 1) PRINT 'spApplyRatesShipper (' + ltrim(@ID) + ') DONE ' + (CONVERT(VARCHAR(24), GETDATE(), 121))
	
END

GO


/*************************************/
-- Date Created: ? - 2014.12.27 - Kevin Alons
-- Purpose: ensure the TotalAmount always reflects the sum of the individual amounts (including associated Assessorial charges)
-- Changes:
-- 4.1.0.4 - change to also fire on DELETE (and rename to ..._IUD)
-- 4.7.0	- 2017-05-12	- JAE	- Suppress check if settled is not final
/*************************************/
ALTER TRIGGER trigOrderSettlementCarrierAssessorialCharge_IUD ON tblOrderSettlementCarrierAssessorialCharge FOR INSERT, UPDATE, DELETE  AS
BEGIN
	IF EXISTS (SELECT 1 FROM viewOrderSettlementCarrier WHERE BatchID IS NOT NULL AND IsFinal = 1 AND OrderID IN (SELECT OrderID FROM inserted UNION SELECT OrderID FROM deleted))
		RAISERROR('Cannot modify carrier settlement values for a settled order', 16, 1)
	ELSE
		UPDATE tblOrderSettlementCarrier
			SET TotalAmount = 0
		WHERE OrderID IN (SELECT DISTINCT OrderID FROM inserted UNION SELECT DISTINCT OrderID FROM deleted)
END

GO


/*************************************/
-- Created: 4.1.0.4 - 2016.09.07 - Kevin Alons
-- Purpose: ensure the TotalAmount always reflects the sum of the individual amounts (including associated Assessorial charges)
-- Changes:
-- 4.7.0	- 2017-05-12	- JAE	- Suppress check if settled is not final
/*************************************/
ALTER TRIGGER trigOrderSettlementDriverAssessorialCharge_IUD ON tblOrderSettlementDriverAssessorialCharge FOR INSERT, UPDATE, DELETE AS
BEGIN 
	IF EXISTS (SELECT 1 FROM viewOrderSettlementDriver WHERE BatchID IS NOT NULL AND IsFinal = 1 AND OrderID IN (SELECT OrderID FROM inserted UNION SELECT OrderID FROM deleted))
		RAISERROR('Cannot modify driver settlement values for a settled order', 16, 1)
	ELSE
		-- force an update of the underlying Settlement record (the total will be recomputed)	
		UPDATE tblOrderSettlementDriver
			SET TotalAmount = 0
		WHERE OrderID IN (SELECT DISTINCT OrderID FROM inserted UNION SELECT DISTINCT OrderID FROM deleted)
END

GO

/*************************************/
-- Date Created: ? - 2014.12.27 - Kevin Alons
-- Purpose: ensure the TotalAmount always reflects the sum of the individual amounts (including associated Assessorial charges)
-- Changes:
-- 4.1.0.4 - change to also fire on DELETE (and rename to ..._IUD)
-- 4.7.0	- 2017-05-12	- JAE	- Suppress check if settled is not final
/*************************************/
ALTER TRIGGER trigOrderSettlementShipperAssessorialCharge_IUD ON tblOrderSettlementShipperAssessorialCharge FOR INSERT, UPDATE, DELETE  AS
BEGIN
	IF EXISTS (SELECT 1 FROM viewOrderSettlementShipper WHERE BatchID IS NOT NULL AND IsFinal = 1 AND OrderID IN (SELECT OrderID FROM inserted UNION SELECT OrderID FROM deleted))
		RAISERROR('Cannot modify shipper settlement values for a settled order', 16, 1)
	ELSE
		UPDATE tblOrderSettlementShipper
			SET TotalAmount = 0
		WHERE OrderID IN (SELECT DISTINCT OrderID FROM inserted UNION SELECT DISTINCT OrderID FROM deleted)
END

GO





/***********************************/
-- Created: ?.?.? - 2013/06/02 - Kevin Alons
-- Purpose: compute and add the various Carrier "Settlement" $$ values to an Audited order
-- Changes:
-- 4.1.0	- 2016/08/21 - KDA	- use new @SessionID parameter to create the entire settlement record in a single invocation
-- 4.1.8.6	- 2016.09.24 - KDA	- optimize data retrieval into 2 separate steps (MAJOR performance improvement workaround)
-- 4.3.0.2	- 2016.11.11 - KDA	- prevent execution if any orders in the session are already Settled
--								- remove related tblOrderSettlementSessionCarrier record at end of settlement if all records are settled
-- 4.6.2	- 2017.04.18 - KDA	- add @PeriodEndDate parameter and add to tblCarrierSettlementBatch on SettlementBatch creation
-- 4.7.0	- 2017/05/12 - JAE	- Add isfinal option to parameters
/***********************************/
ALTER PROCEDURE spCreateCarrierSettlementBatch
(
  @SessionID varchar(100)
, @CarrierID int
, @PeriodEndDate date
, @InvoiceNum varchar(50)
, @Notes varchar(255)
, @UserName varchar(255)
, @IsFinal bit = 1
, @BatchID int = NULL output
, @BatchNum int = NULL output
, @Outcome varchar(25) = 'Success' output
, @Msg varchar(255) = NULL output
) AS BEGIN
	IF @PeriodEndDate IS NULL SET @PeriodEndDate = cast(getdate() as date)

	-- retrieve the basic session order data first
	SELECT OS.OrderID, IsRated = cast(0 as bit), HasError = cast(0 as bit), IsSettled = cast(0 as bit)
	INTO #data
	FROM tblOrderSettlementSelectionCarrier OS 
	WHERE OS.SessionID = @SessionID AND OS.BatchSel = 1
	-- add the Financial information next (done in 2 steps for MAJOR performance reasons - a workaround but a more intuitive way was not identified)
	UPDATE #data
	  SET IsRated = cast(CASE WHEN OFB.InvoiceRatesAppliedDateUTC IS NULL THEN 0 ELSE 1 END as bit)
		, HasError = OFB.HasError
		, IsSettled = CASE WHEN BatchID IS NULL THEN 0 ELSE 1 END
	FROM #data O
	JOIN (
		SELECT OrderID, InvoiceRatesAppliedDateUTC, HasError , BatchID
		FROM viewOrder_Financial_Base_Carrier 
		WHERE OrderID IN (SELECT OrderID FROM #data)
	) OFB ON OFB.OrderID = O.OrderID

	-- validate the data next
	IF NOT EXISTS (SELECT * FROM #data)
	BEGIN
		SELECT @Outcome = 'Error', @Msg = 'At least order must be selected to complete settlement!'
		RETURN
	END
	ELSE IF EXISTS (SELECT * FROM #data WHERE IsRated = 0 OR HasError <> 0)
	BEGIN
		SELECT @Outcome = 'Error', @Msg = 'All selected orders must be fully rated with no errors to complete Settlement!'
		RETURN
	END
	ELSE IF EXISTS (SELECT * FROM #data WHERE IsSettled = 1)
	BEGIN
		SELECT @Outcome = 'Error', @Msg = 'At least 1 order is already settled - Refresh to re-query and continue!'
		RETURN
	END

	-- if validation passed, complete the settlement process below
	SET XACT_ABORT ON
	BEGIN TRAN CarrierSettlement

	INSERT INTO dbo.tblCarrierSettlementBatch(CarrierID, PeriodEndDate, InvoiceNum, BatchNum, BatchDate, Notes, IsFinal, CreateDateUTC, CreatedByUser)
		SELECT @CarrierID
			, @PeriodEndDate
			, @InvoiceNum
			, isnull(max(BatchNum), 0) + 1
			, dbo.fnDateOnly(getdate()), @Notes, @IsFinal, getutcdate(), @UserName 
		FROM dbo.tblCarrierSettlementBatch
	SELECT @BatchID = scope_identity()
	SELECT @BatchNum = BatchNum FROM dbo.tblCarrierSettlementBatch WHERE ID = @BatchID

	UPDATE tblOrderSettlementCarrier 
		SET BatchID = @BatchID
	FROM tblOrderSettlementCarrier OSC
	JOIN #data D ON D.OrderID = OSC.OrderID
	WHERE BatchID IS NULL  -- double-check that we aren't re-settling the order

	SELECT @Outcome = 'Success', @Msg = 'Settlement process complete: ' + ltrim(count(1)) + ' record(s) were added to batch: ' + ltrim(@BatchNum)
	FROM #data

	DELETE FROM tblOrderSettlementSelectionCarrier WHERE SessionID = @SessionID AND BatchSel = 1
	IF NOT EXISTS (SELECT SessionID FROM tblOrderSettlementSelectionCarrier WHERE SessionID = @SessionID)
		DELETE FROM tblOrderSettlementSessionCarrier WHERE ID = @SessionID

	COMMIT TRAN
END

GO

/***********************************/
-- Created: ?.?.? - 2013/06/02 - Kevin Alons
-- Purpose: compute and add the various Driver "Settlement" $$ values to an Audited order
-- Changes:
-- 4.1.8.6	- 2016.09.24 - KDA	- optimize data retrieval into 2 separate steps (MAJOR performance improvement workaround)
-- 4.3.0.2	- 2016.11.11 - KDA	- prevent execution if any orders in the session are already Settled
--								- remove related tblOrderSettlementSessionDriver record at end of settlement if all records are settled
-- 4.6.2	- 2017.04.18 - KDA	- add @PeriodEndDate parameter and add to tblDriverSettlementBatch on SettlementBatch creation
-- 4.7.0	- 2017/05/12 - JAE	- Add isfinal option to parameters
/***********************************/
ALTER PROCEDURE spCreateDriverSettlementBatch
(
  @SessionID varchar(100)
, @CarrierID int
, @DriverGroupID int = NULL
, @DriverID int = NULL
, @PeriodEndDate date
, @InvoiceNum varchar(50) = NULL
, @Notes varchar(255) = NULL
, @UserName varchar(255) 
, @IsFinal bit = 1
, @BatchID int = NULL output
, @BatchNum int = NULL output
, @Outcome varchar(25) = 'Success' output
, @Msg varchar(255) = NULL output
) AS BEGIN
	IF (@DriverGroupID = 0) SET @DriverGroupID = NULL
	IF (@DriverID = 0) SET @DriverID = NULL
	IF @PeriodEndDate IS NULL SET @PeriodEndDate = cast(getdate() as date)

	PRINT 'spCreateDriverSettlementBatch(' + ltrim(@SessionID) + ').Retrieve START ' + (CONVERT(VARCHAR(24), GETDATE(), 121))

	-- retrieve the basic session order data first
	SELECT OS.OrderID, IsRated = cast(0 as bit), HasError = cast(0 as bit), IsSettled = cast(0 as bit)
	INTO #data
	FROM tblOrderSettlementSelectionDriver OS 
	WHERE OS.SessionID = @SessionID AND OS.BatchSel = 1
	-- add the Financial information next (done in 2 steps for MAJOR performance reasons - a workaround but a more intuitive way was not identified)
	UPDATE #data
	  SET IsRated = cast(CASE WHEN OFB.InvoiceRatesAppliedDateUTC IS NULL THEN 0 ELSE 1 END as bit)
		, HasError = OFB.HasError
		, IsSettled = CASE WHEN BatchID IS NULL THEN 0 ELSE 1 END
	FROM #data O
	JOIN (
		SELECT OrderID, InvoiceRatesAppliedDateUTC, HasError, BatchID
		FROM viewOrder_Financial_Base_Driver 
		WHERE OrderID IN (SELECT OrderID FROM #data)
	) OFB ON OFB.OrderID = O.OrderID

	PRINT 'spCreateDriverSettlementBatch(' + ltrim(@SessionID) + ').Retrieve DONE ' + (CONVERT(VARCHAR(24), GETDATE(), 121))

	-- validate the data next
	IF NOT EXISTS (SELECT * FROM #data)
	BEGIN
		SELECT @Outcome = 'Error', @Msg = 'At least 1 order must be selected to complete settlement!'
		RETURN
	END
	ELSE IF EXISTS (SELECT * FROM #data WHERE IsRated = 0 OR HasError <> 0)
	BEGIN
		SELECT @Outcome = 'Error', @Msg = 'All selected orders must be fully rated with no errors to complete Settlement!'
		RETURN
	END
	ELSE IF EXISTS (SELECT * FROM #data WHERE IsSettled = 1)
	BEGIN
		SELECT @Outcome = 'Error', @Msg = 'At least 1 order is already settled - Refresh to re-query and continue!'
		RETURN
	END

	-- if validation passed, complete the settlement process below
	SET XACT_ABORT ON
	BEGIN TRAN DriverSettlement

	INSERT INTO dbo.tblDriverSettlementBatch(CarrierID, DriverGroupID, DriverID, PeriodEndDate, InvoiceNum, BatchNum, BatchDate, Notes, IsFinal, CreateDateUTC, CreatedByUser)
		SELECT @CarrierID, @DriverGroupID, @DriverID
			, @PeriodEndDate
			, @InvoiceNum
			, isnull(max(BatchNum), 0) + 1
			, dbo.fnDateOnly(getdate()), @Notes, @IsFinal, getutcdate(), @UserName 
		FROM dbo.tblDriverSettlementBatch
	SELECT @BatchID = scope_identity()
	SELECT @BatchNum = BatchNum FROM dbo.tblDriverSettlementBatch WHERE ID = @BatchID

	UPDATE tblOrderSettlementDriver 
		SET BatchID = @BatchID
	FROM tblOrderSettlementDriver OSC
	JOIN #data D ON D.OrderID = OSC.OrderID
	WHERE BatchID IS NULL  -- double-check that we aren't re-settling the order

	SELECT @Outcome = 'Success', @Msg = 'Settlement process complete: ' + ltrim(count(1)) + ' record(s) were added to batch: ' + ltrim(@BatchNum)
	FROM #data

	DELETE FROM tblOrderSettlementSelectionDriver WHERE SessionID = @SessionID AND BatchSel = 1
	IF NOT EXISTS (SELECT SessionID FROM tblOrderSettlementSelectionDriver WHERE SessionID = @SessionID)
		DELETE FROM tblOrderSettlementSessionDriver WHERE ID = @SessionID

	COMMIT TRAN
END

GO

/***********************************/
-- Created: ?.?.? - 2013/06/02 - Kevin Alons
-- Purpose: compute and add the various Shipper "Settlement" $$ values to an Audited order
-- Changes:
-- 4.1.0 - 2016/08/21 - KDA - use new @SessionID parameter to create the entire settlement record in a single invocation
-- 4.1.8.6	- 2016.09.24 - KDA	- optimize data retrieval into 2 separate steps (MAJOR performance improvement workaround)
-- 4.3.0.2	- 2016.11.11 - KDA	- prevent execution if any orders in the session are already Settled
--								- remove related tblOrderSettlementSessionShipper record at end of settlement if all records are settled
-- 4.6.2	- 2017.04.18 - KDA	- add @PeriodEndDate parameter and add to tblShipperSettlementBatch on SettlementBatch creation
-- 4.7.0	- 2017/05/12 - JAE	- Add isfinal option to parameters
/***********************************/
ALTER PROCEDURE spCreateShipperSettlementBatch
(
  @SessionID varchar(100)
, @ShipperID int
, @PeriodEndDate date
, @InvoiceNum varchar(50)
, @Notes varchar(255)
, @UserName varchar(255)
, @BatchID int = NULL output
, @BatchNum int = NULL output
, @IsFinal bit = 1
, @Outcome varchar(25) = 'Success' output
, @Msg varchar(255) = NULL output
) AS BEGIN
	IF @PeriodEndDate IS NULL SET @PeriodEndDate = cast(getdate() as date)

	-- retrieve the basic session order data first
	SELECT OS.OrderID, IsRated = cast(0 as bit), HasError = cast(0 as bit), IsSettled = cast(0 as bit)
	INTO #data
	FROM tblOrderSettlementSelectionShipper OS 
	WHERE OS.SessionID = @SessionID AND OS.BatchSel = 1
	-- add the Financial information next (done in 2 steps for MAJOR performance reasons - a workaround but a more intuitive way was not identified)
	UPDATE #data
	  SET IsRated = cast(CASE WHEN OFB.InvoiceRatesAppliedDateUTC IS NULL THEN 0 ELSE 1 END as bit)
		, HasError = OFB.HasError
		, IsSettled = CASE WHEN BatchID IS NULL THEN 0 ELSE 1 END
	FROM #data O
	JOIN (
		SELECT OrderID, InvoiceRatesAppliedDateUTC, HasError, BatchID
		FROM viewOrder_Financial_Base_Shipper 
		WHERE OrderID IN (SELECT OrderID FROM #data)
	) OFB ON OFB.OrderID = O.OrderID

	-- validate the data next
	IF NOT EXISTS (SELECT * FROM #data)
	BEGIN
		SELECT @Outcome = 'Error', @Msg = 'At least order must be selected to complete settlement!'
		RETURN
	END
	ELSE IF EXISTS (SELECT * FROM #data WHERE IsRated = 0 OR HasError <> 0)
	BEGIN
		SELECT @Outcome = 'Error', @Msg = 'All selected orders must be fully rated with no errors to complete Settlement!'
		RETURN
	END
	ELSE IF EXISTS (SELECT * FROM #data WHERE IsSettled = 1)
	BEGIN
		SELECT @Outcome = 'Error', @Msg = 'At least 1 order is already settled - Refresh to re-query and continue!'
		RETURN
	END

	-- if validation passed, complete the settlement process below
	SET XACT_ABORT ON
	BEGIN TRAN ShipperSettlement

	INSERT INTO dbo.tblShipperSettlementBatch(ShipperID, PeriodEndDate, InvoiceNum, BatchNum, BatchDate, Notes, IsFinal, CreateDateUTC, CreatedByUser)
		SELECT @ShipperID
			, @PeriodEndDate
			, @InvoiceNum
			, isnull(max(BatchNum), 0) + 1
			, dbo.fnDateOnly(getdate()), @Notes, @IsFinal, getutcdate(), @UserName 
		FROM dbo.tblShipperSettlementBatch
	SELECT @BatchID = scope_identity()
	SELECT @BatchNum = BatchNum FROM dbo.tblShipperSettlementBatch WHERE ID = @BatchID

	UPDATE tblOrderSettlementShipper 
		SET BatchID = @BatchID
	FROM tblOrderSettlementShipper OSC
	JOIN #data D ON D.OrderID = OSC.OrderID
	WHERE BatchID IS NULL  -- double-check that we aren't re-settling the order

	SELECT @Outcome = 'Success', @Msg = 'Settlement process complete: ' + ltrim(count(1)) + ' record(s) were added to batch: ' + ltrim(@BatchNum)
	FROM #data

	DELETE FROM tblOrderSettlementSelectionShipper WHERE SessionID = @SessionID AND BatchSel = 1
	IF NOT EXISTS (SELECT SessionID FROM tblOrderSettlementSelectionShipper WHERE SessionID = @SessionID)
		DELETE FROM tblOrderSettlementSessionShipper WHERE ID = @SessionID

	COMMIT TRAN
END

GO

COMMIT
SET NOEXEC OFF