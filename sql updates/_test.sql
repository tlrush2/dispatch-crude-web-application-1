exec spRetrieveOrdersFinancialCarrier @StartDate='2015-07-01',@EndDate='2015-08-10',@ShipperID=-1,@CarrierID=-1,@ProductGroupID=-1,@TruckTypeID=-1,@DriverGroupID=-1,@OriginStateID=-1,@DestStateID=-1,@ProducerID=-1,@BatchID=NULL,@OnlyShipperSettled=0,@JobNumber=NULL,@ContractNumber=NULL
go
EXEC spRetrieveOrdersFinancialCarrier 
                    @StartDate = '7/1/2015'
                    , @EndDate = '7/5/2015'
                    , @ShipperID = -1
                    , @CarrierID = -1
                    , @ProductGroupID = -1
                    , @TruckTypeID = -1
                    , @DriverGroupID = -1
                    , @OriginStateID = -1
                    , @DestStateID = -1
                    , @ProducerID = -1
                    , @BatchID = NULL
                    , @OnlyShipperSettled = 0
                    , @JobNumber = NULL
                    , @ContractNumber = NULL

declare @StartDate date = '7/1/2015'
    , @EndDate date = '7/5/2015'
    , @ShipperID int = -1
    , @CarrierID int = -1
    , @ProductGroupID int = -1
    , @TruckTypeID int = -1
    , @DriverGroupID int = -1
    , @OriginStateID int = -1
    , @DestStateID int = -1
    , @ProducerID int = -1
    , @BatchID int = NULL
    , @OnlyShipperSettled bit = 0
    , @JobNumber varchar(100) = NULL
    , @ContractNumber varchar(100) = NULL

declare @IDs IDTABLE

		-- retrieve the Order.ID values for the specified parameters
		INSERT INTO @IDs (ID)
			SELECT O.ID
			FROM tblOrder O
			LEFT JOIN dbo.tblOrderTransfer OTR ON OTR.OrderID = O.ID
			LEFT JOIN dbo.viewDriver vODR ON vODR.ID = OTR.OriginDriverID
			LEFT JOIN dbo.viewDriver vDDR ON vDDR.ID = O.DriverID
			LEFT JOIN dbo.tblDriverGroup ODG ON ODG.ID = vODR.DriverGroupID
			JOIN tblProduct P ON P.ID = O.ProductID
			JOIN tblTruck T ON T.ID = ISNULL(OTR.OriginTruckID, O.TruckID)
			JOIN viewOrigin OO ON OO.ID = O.OriginID
			JOIN tblDestination D ON D.ID = O.DestinationID
			LEFT JOIN tblOrderSettlementCarrier OSC ON OSC.OrderID = O.ID
			WHERE O.StatusID IN (4)  
			  AND O.DeleteDateUTC IS NULL  -- 3.9.34
			  AND OSC.BatchID IS NULL -- only get order if it's not part of a batch
			  AND (@ShipperID=-1 OR O.CustomerID=@ShipperID) 
			  AND (@CarrierID=-1 OR O.CarrierID=@CarrierID) 
			  AND (@ProductGroupID=-1 OR P.ProductGroupID=@ProductGroupID) 
			  AND (@TruckTypeID=-1 OR T.TruckTypeID=@TruckTypeID)
			  AND (@DriverGroupID=-1 OR ISNULL(vODR.DriverGroupID, vDDR.DriverGroupID) = @DriverGroupID) 
			  AND (@OriginStateID=-1 OR OO.StateID=@OriginStateID) 
			  AND (@DestStateID=-1 OR D.StateID=@DestStateID) 
			  AND (@ProducerID=-1 OR O.ProducerID=@ProducerID) 
			  AND (@StartDate IS NULL OR O.OrderDate >= @StartDate) 
			  AND (@EndDate IS NULL OR O.OrderDate <= @EndDate)
			  AND (ISNULL(@JobNumber,'') = '' OR O.JobNumber = @JobNumber)  -- 3.11.19
			  AND (ISNULL(@ContractNumber,'') = '' OR O.ContractNumber = @ContractNumber)  -- 3.11.19

	SELECT DISTINCT OE.* 
		--, ShipperSettled = cast(CASE WHEN OSS.BatchID IS NOT NULL THEN 1 ELSE 0 END as bit)
		, InvoiceOtherDetailsTSV = dbo.fnOrderCarrierAssessorialDetailsTSV(OE.ID, 0)
		, InvoiceOtherAmountsTSV = dbo.fnOrderCarrierAssessorialAmountsTSV(OE.ID, 0)
		, Approved = cast(ISNULL(OA.Approved, 0) as bit)
		, FinalActualMiles = ISNULL(OA.OverrideActualMiles, OE.ActualMiles)
		, FinalOriginMinutes = ISNULL(OA.OverrideOriginMinutes, OE.OriginMinutes)
		, FinalDestMinutes = ISNULL(OA.OverrideDestMinutes, OE.DestMinutes)
		, FinalH2S = cast(CASE WHEN isnull(OA.OverrideH2S, 0) = 1 THEN 0 ELSE OE.H2S END as bit)
		, FinalChainup = cast(CASE WHEN isnull(OA.OverrideChainup, 0) = 1 THEN 0 ELSE OE.Chainup END as bit)
		, FinalRerouteCount = CASE WHEN isnull(OA.OverrideReroute, 0) = 1 THEN 0 ELSE OE.RerouteCount END
	FROM dbo.viewOrder_Financial_Carrier OE
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = OE.ID
where oe.id in (select id from @ids)

select * from viewOrder_Financial_Carrier_final where id in (select ID from @ids)