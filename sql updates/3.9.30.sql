-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.9.29.4.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.9.29.4.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0

SET NOEXEC OFF  -- since this is 
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.9.29.5'
SELECT  @NewVersion = '3.9.30'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'Fix to ensure transferred orders are returned to the driver'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/*******************************************
 Date Created: 25 Apr 2013
 Author: Kevin Alons
 Purpose: return OrderTicket data for Driver App sync
 Changes:
	3.9.30 - 2015/12/03 - KDA - fix to ensure transferred orders are returned to the driver
*******************************************/
ALTER FUNCTION fnOrderTicket_DriverApp( @DriverID int, @LastChangeDateUTC datetime ) RETURNS TABLE AS
RETURN 
	SELECT OT.ID
		, OT.UID
		, OT.OrderID
		, OT.CarrierTicketNum
		, OT.OriginTankID
		, OT.TankNum
		, OT.TicketTypeID
		, OT.ProductObsGravity
		, OT.ProductObsTemp
		, OT.ProductHighTemp
		, OT.ProductLowTemp
		, OT.ProductBSW
		, OpeningGaugeFeet = cast(OT.OpeningGaugeFeet as tinyint) 
		, OpeningGaugeInch = cast(OT.OpeningGaugeInch as tinyint)  
		, OpeningGaugeQ = cast(OT.OpeningGaugeQ as tinyint) 
		, ClosingGaugeFeet = cast(OT.ClosingGaugeFeet as tinyint) 
		, ClosingGaugeInch = cast(OT.ClosingGaugeInch as tinyint) 
		, ClosingGaugeQ = cast(OT.ClosingGaugeQ as tinyint) 
		, OT.BottomFeet
		, OT.BottomInches
		, OT.BottomQ
		, OT.GrossUnits
		, OT.GrossStdUnits
		, OT.NetUnits
		, OT.Rejected
		, OT.RejectReasonID
		, OT.RejectNotes
		, OT.SealOff
		, OT.SealOn
		, OT.BOLNum
		, OT.CreateDateUTC
		, OT.CreatedByUser
		, OT.LastChangeDateUTC
		, OT.LastChangedByUser
		, OT.DeleteDateUTC
		, OT.DeletedByUser
		, OT.MeterFactor
		, OT.OpenMeterUnits
		, OT.CloseMeterUnits
		, OT.DispatchConfirmNum
	FROM dbo.tblOrderTicket OT
	JOIN dbo.tblOrder O ON O.ID = OT.OrderID
	LEFT JOIN tblOrderDriverAppVirtualDelete ODAVD ON ODAVD.OrderID = O.ID AND ODAVD.DriverID = @DriverID
	LEFT JOIN tblOrderTransfer OTR ON OTR.OrderID = O.ID
	CROSS JOIN fnSyncLCDOffset(@LastChangeDateUTC) LCD
	WHERE (O.StatusID IN (2, 7, 8, 3) -- Dispatched, Accepted, Picked Up, Delivered
		-- or this potentially was a DriverApp record, that no longer is, so should be included (as deleted)
		OR (ODAVD.ID IS NOT NULL AND ODAVD.VirtualDeleteDateUTC >= LCD.LCD))
	  AND (O.DriverID = @DriverID OR OTR.OriginDriverID = @DriverID)
	  AND (@LastChangeDateUTC IS NULL 
		OR OT.CreateDateUTC >= LCD.LCD
		OR OT.LastChangeDateUTC >= LCD.LCD
		OR OT.DeleteDateUTC >= LCD.LCD
		OR ODAVD.VirtualDeleteDateUTC >= LCD.LCD)
GO

COMMIT
SET NOEXEC OFF