/* add logic to keep Order.ActualMiles consistent with Route table
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.0.9', @NewVersion = '2.0.10'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 19 Dec 2012
-- Description:	trigger to add a unique, incrementing OrderNum to each new Order (manual Identity column)
-- =============================================
ALTER TRIGGER [dbo].[trigOrder_IU] ON [dbo].[tblOrder] AFTER INSERT, UPDATE AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	WHILE (SELECT count(*) FROM inserted i JOIN tblOrder O ON O.ID = i.ID WHERE O.OrderNum IS NULL) > 0
	BEGIN
		UPDATE tblOrder 
		  SET OrderNum = isnull((SELECT max(OrderNum) FROM tblOrder), 100000) + 1
			, CreateDateUTC = getutcdate()
		WHERE ID = (SELECT min(O.ID) FROM tblOrder O JOIN inserted i ON i.ID = O.ID WHERE O.OrderNum IS NULL)
	END
	-- re-compute the OriginMinutes (in case the website failed to compute it properly)
	IF UPDATE(OriginArriveTimeUTC) OR UPDATE(OriginDepartTimeUTC)
	BEGIN
		UPDATE tblOrder SET OriginMinutes = datediff(minute, OriginArriveTimeUTC, OriginDepartTimeUTC)
	END
	-- re-compute the DestWaitMinutes (in case the website failed to compute it properly)
	IF UPDATE(DestArriveTimeUTC) OR UPDATE(DestDepartTimeUTC)
	BEGIN
		UPDATE tblOrder SET DestMinutes = datediff(minute, DestArriveTimeUTC, DestDepartTimeUTC)
	END
	
	-- ensure missing Routes are created and assigned for the specified Origin/Destination combination
	IF UPDATE(OriginID) OR UPDATE(DestinationID)
	BEGIN
		-- create any missing Route records
		INSERT INTO tblRoute (OriginID, DestinationID, CreateDateUTC, CreatedByUser)
			SELECT i.OriginID, i.DestinationID, GETUTCDATE(), ISNULL(i.LastChangedByUser, i.CreatedByUser)
			FROM inserted i
			LEFT JOIN tblRoute R ON R.OriginID = i.OriginID AND R.DestinationID = i.DestinationID
			WHERE i.OriginID IS NOT NULL AND i.DestinationID IS NOT NULL AND R.ID IS NULL
		-- ensure the Order records refer to the correct Route (ID)
		UPDATE tblOrder SET RouteID = R.ID
		FROM tblOrder O
		JOIN inserted i ON i.ID = O.ID
		LEFT JOIN tblRoute R ON R.OriginID = O.OriginID AND R.DestinationID = O.DestinationID
		
		-- update the ActualMiles from the related Route
		UPDATE tblOrder SET ActualMiles = R.ActualMiles
		FROM tblOrder O
		JOIN inserted i ON i.ID = O.ID
		JOIN tblRoute R ON R.ID = O.RouteID
	END
	
END

GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 11 Sep 2013
-- Description:	trigger to ensure changes to Routes also update related (relevant) orders
-- =============================================
CREATE TRIGGER [dbo].[trigRoute_U] ON [dbo].[tblRoute] AFTER UPDATE AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE tblOrder
	  SET ActualMiles = R.ActualMiles
	FROM tblOrder O
	JOIN inserted R ON R.ID = O.RouteID
	LEFT JOIN tblOrderInvoiceCarrier OIC ON OIC.OrderID = O.ID
	LEFT JOIN tblOrderInvoiceCustomer OIS ON OIS.OrderID = O.ID
	WHERE OIC.BatchID IS NULL AND OIS.BatchID IS NULL
END
GO

UPDATE tblOrder
SET ActualMiles = R.ActualMiles
FROM tblOrder O
JOIN tblRoute R ON R.ID = O.RouteID
LEFT JOIN tblOrderInvoiceCarrier OIC ON OIC.OrderID = O.ID
LEFT JOIN tblOrderInvoiceCustomer OIS ON OIS.OrderID = O.ID
WHERE OIC.BatchID IS NULL AND OIS.BatchID IS NULL

COMMIT
SET NOEXEC OFF