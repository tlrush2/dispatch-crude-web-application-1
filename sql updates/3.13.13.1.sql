SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20) = '3.13.13'
	, @NewVersion varchar(20) = '3.13.13.1'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1505 - Dispatch Card Planner permission'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


-- Add new role
INSERT INTO ASPNET_ROLES
select applicationid, NEWID(), 'viewDispatchBoard', LOWER('viewDispatchBoard'), 'View and use card planner/dispatch board' from aspnet_applications 
where not exists (select 1 from aspnet_roles where rolename = 'viewDispatchBoard')
GO

-- Add current admins to role
EXEC spAddUserPermissionsByRole 'Administrator', 'viewDispatchBoard'
GO

COMMIT
SET NOEXEC OFF