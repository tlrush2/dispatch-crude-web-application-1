SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.5.14'
SELECT  @NewVersion = '4.5.14.1'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'JT-2255 - HOTFIX: Restoring db code that was commented out as a temporary fix for the sync error discovered when 4.5.14 was published'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


-- Replace blank strings with NULL for serial number before replacing the unique index (also do this for model number as cleanup for that field)
UPDATE tblDriverEquipment SET SerialNum = NULL WHERE SerialNum = ''
GO
UPDATE tblDriverEquipment SET ModelNum = NULL WHERE ModelNum = ''
GO



-- Restore the index that was removed as a quick fix after publish of 4.5.14 (Only if it does not exist since some databases like dev, test, and new customer did not get this index removed)
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE name='udxDriverEquipment_SerialNum_ManufacturerID' )
BEGIN 
	SET QUOTED_IDENTIFIER OFF 
	CREATE UNIQUE NONCLUSTERED INDEX udxDriverEquipment_SerialNum_ManufacturerID ON tblDriverEquipment
	(
		DriverEquipmentManufacturerID ASC,
		SerialNum ASC
	)
	WHERE (SerialNum IS NOT NULL)
	WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
	SET QUOTED_IDENTIFIER ON
END
GO



-- Restore changes from 4.5.14 (They were commented out to fix the immediate sync issue)
GO
/***************************************
Date Created: 2016/02/01 - 3.10.7
Author: Kevin Alons
Purpose: Add or update the new TabletID and/or Printer Serial number to tblDriverEquipment
Changes:
	- 4.5.14		2017/02/23		BSB		Added Android API level, model number, serial number, and manufacturer to updateable information about tablets
***************************************/
ALTER PROCEDURE spDriverSync_UpdateDevices
(
  @TabletID VARCHAR(20)
, @PrinterSerial VARCHAR(100)
, @UserName VARCHAR(100)
, @AndroidApiLevel VARCHAR(10)
, @ModelNum VARCHAR(50)
, @SerialNum VARCHAR(100)
, @Manufacturer VARCHAR(50)
) AS
BEGIN
	-- 4.5.14 - prep work for the manufacturer name that is now being sent during sync
	DECLARE @MANUFACTURER_ID INT

	IF (@Manufacturer IS NULL OR @Manufacturer = '')  -- IF NULL
		BEGIN
			SET @MANUFACTURER_ID = 1 -- "Unknown"
		END
	ELSE  -- IF NOT NULL
		BEGIN			
			SET @MANUFACTURER_ID = (SELECT TOP 1 ID FROM tblDriverEquipmentManufacturer WHERE UPPER(Name) LIKE UPPER(@Manufacturer))

			IF (@MANUFACTURER_ID IS NULL)
				BEGIN
					-- INSERT RECORD
					INSERT INTO tblDriverEquipmentManufacturer (Name)
					SELECT @Manufacturer
					-- NOW GET ID OF NEWLY INSERTED RECORD
					SET @MANUFACTURER_ID = SCOPE_IDENTITY()
				END
		END	

	-- insert or update an equipment record for the specified tablet (if provided)
	IF (@TabletID IS NOT NULL AND @TabletID <> '')
	BEGIN
		INSERT INTO tblDriverEquipment (DriverEquipmentTypeID
										, DriverEquipmentManufacturerID
										, PhoneNum
										, AndroidApiLevel
										, ModelNum
										, SerialNum
										, CreatedByUser)
			SELECT 1, @MANUFACTURER_ID, @TabletID, @AndroidApiLevel, @ModelNum, @SerialNum, @UserName 
			WHERE NOT EXISTS (SELECT ID FROM tblDriverEquipment DE WHERE DE.PhoneNum = @TabletID AND DriverEquipmentTypeID = 1)
		IF (@@ROWCOUNT = 0)	
			UPDATE tblDriverEquipment 
			SET AndroidApiLevel = @AndroidApiLevel  -- Always update the current API level
				, ModelNum = @ModelNum  -- Always update the model number
				, SerialNum = @SerialNum  -- Always update the serial number
				, DriverEquipmentManufacturerID = @MANUFACTURER_ID  -- Always update the manufacturer
				, LastChangeDateUTC = getutcdate()
				, LastChangedByUser = @UserName
				, DeleteDateUTC = NULL
				, DeletedByUser = NULL 
			WHERE PhoneNum = @TabletID AND DriverEquipmentTypeID = 1
	END

	IF (@PrinterSerial IS NOT NULL AND @PrinterSerial <> '')
	BEGIN
		INSERT INTO tblDriverEquipment (DriverEquipmentTypeID
										, DriverEquipmentManufacturerID
										, SerialNum
										, CreatedByUser)
			SELECT 2, 1, @PrinterSerial, @UserName
			WHERE NOT EXISTS (SELECT ID FROM tblDriverEquipment DE WHERE DE.SerialNum = @PrinterSerial AND DriverEquipmentTypeID = 2)
		IF (@@ROWCOUNT = 0)
			UPDATE tblDriverEquipment 
			SET LastChangeDateUTC = getutcdate(), LastChangedByUser = @UserName
				, DeleteDateUTC = NULL, DeletedByUser = NULL 
			WHERE SerialNum = @PrinterSerial AND DriverEquipmentTypeID = 2
	END
END
GO



EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRecompileAllStoredProcedures
GO
EXEC _spRebuildAllObjects
GO
EXEC _spRebuildAllObjects
GO



COMMIT
SET NOEXEC OFF