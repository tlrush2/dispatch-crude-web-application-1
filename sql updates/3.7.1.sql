--backup database [dispatchcrude.dev] to disk = 'C:\files\Consulting\cts\dc_backups\dispatchcrude.dev.3.7.0.bak'
--restore database [DispatchCrude.Dev] from disk = 'C:\files\Consulting\cts\dc_backups\dispatchcrude.dev.3.7.0.bak'
--go
-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.7.0'
SELECT  @NewVersion = '3.7.1'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'GaugerApp: add Gauger Order Print Templates'
	UNION SELECT @NewVersion, 0, 'GaugerApp: add tblGaugerPrintTemplate TABLE'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

CREATE TABLE tblGaugerPrintTemplate
(
  ID int IDENTITY(1,1) NOT NULL CONSTRAINT PK_GaugerPrintTemplate PRIMARY KEY
, TicketTypeID int NOT NULL CONSTRAINT FK_GaugerPrintTemplate_TicketType FOREIGN KEY REFERENCES tblGaugerTicketType(ID)
, TemplateText text NULL
, HeaderImageLeft int NOT NULL constraint DF_GaugerPrintTemplate_HeaderImageLeft DEFAULT (0)
, HeaderImageTop int NOT NULL constraint DF_GaugerPrintTemplate_HeaderImageTop DEFAULT (0)
, HeaderImageWidth int NOT NULL constraint DF_GaugerPrintTemplate_HeaderImageWidth DEFAULT (580)
, HeaderImageHeight int NOT NULL constraint DF_GaugerPrintTemplate_HeaderImageHeight DEFAULT (300)
, CreateDateUTC smalldatetime NOT NULL constraint DF_GaugerPrintTemplate_CreateDateUTC DEFAULT (getutcdate())
, CreatedByUser varchar(100) NULL
, LastChangeDateUTC smalldatetime NULL
, LastChangedByUser varchar(100) NULL
)
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblGaugerPrintTemplate TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 4 Apr 2013
-- Author: Kevin Alons
-- Purpose: return the PrintTemplate with translated "friendly" names
/***********************************/
CREATE VIEW viewGaugerPrintTemplate AS
	SELECT PT.*
		, TicketType = GTT.Name
	FROM tblGaugerPrintTemplate PT
	JOIN tblGaugerTicketType GTT ON GTT.ID = PT.TicketTypeID
GO
GRANT SELECT ON viewGaugerPrintTemplate TO dispatchcrude_iis_acct
GO

INSERT INTO tblGaugerPrintTemplate (TicketTypeID, TemplateText, HeaderImageLeft, HeaderImageTop, HeaderImageWidth, HeaderImageHeight, CreateDateUTC, CreatedByUser)
	SELECT TicketTypeID, TemplateText, HeaderImageLeft, HeaderImageTop, HeaderImageWidth, HeaderImageHeight, CreateDateUTC, CreatedByUser
	FROM tblPrintTemplate 
	WHERE TicketTypeID = 1

COMMIT
SET NOEXEC OFF