-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.5.7'
SELECT  @NewVersion = '3.5.8'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'add new Mileage Log functionality'
	UNION SELECT @NewVersion, 0, 'add SETTING BOOL RequireTruckMileage'
	UNION SELECT @NewVersion, 0, 'add VIEW viewTruckWithMileage'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

INSERT INTO tblSetting (ID, Name, SettingTypeID, Value, Category, CreatedByUser, ReadOnly)
	SELECT 39, 'Require Truck Mileage', 2, 'True', 'Order Entry Rules', 'System', 0
	UNION
	SELECT 40, 'Mileage Distance Warning', 3, '500', 'Order Entry Rules', 'System', 0
GO 

/***********************************/
-- Date Created: 12 Feb 2015
-- Author: Kevin Alons
-- Purpose: return last truck mileage for each truck (from order table)
/***********************************/
CREATE VIEW viewOrderLastTruckMileage AS
	SELECT O.TruckID, Odometer = DestTruckMileage, OdometerDate = OrderDate
	FROM viewOrderBase O
	JOIN ( 
		SELECT TruckID, ID = MAX(id) FROM tblOrder WHERE DestTruckMileage IS NOT NULL GROUP BY TruckID
	) T ON T.ID = O.ID
GO
GRANT SELECT ON viewOrderLastTruckMileage TO dispatchcrude_iis_acct
GO
	
/***********************************/
-- Date Created: 26 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Truck info with Last Odometer entry 
/***********************************/
CREATE VIEW viewTruckWithLastOdometer AS
	SELECT T.*, LastOdometer = TM.Odometer, LastOdometerDate = TM.OdometerDate
	FROM dbo.viewTruck T
	LEFT JOIN viewOrderLastTruckMileage TM ON TM.TruckID = T.ID
GO
GRANT SELECT ON viewTruckWithLastOdometer TO dispatchcrude_iis_acct
GO

/*********************************************
Date Create: 12 Feb 2015
Author: Kevin Alons
Purpose: return a "Mileage Log" from the Order table
*********************************************/
CREATE VIEW viewMileageLog AS
	SELECT ID, OrderNum, CarrierID, Carrier, TruckID, Truck, DriverID, Driver, StartTime = OriginArriveTime, EndTime = DestDepartTime
		, StartOdometer = OriginTruckMileage, EndOdometer = DestTruckMileage
		, Mileage = DestTruckMileage - OriginTruckMileage
		, RouteMiles = O.ActualMiles
		, XID = null, NID = null
	FROM viewOrderLocalDates O
	WHERE O.OriginArriveTimeUTC IS NOT NULL AND O.DestDepartTimeUTC IS NOT NULL AND O.OriginTruckMileage IS NOT NULL AND O.DestTruckMileage IS NOT NULL
	  AND O.TruckID IS NOT NULL
	UNION 
	SELECT ID = NULL, OrderNum = NULL, CarrierID, Carrier, TruckID, Truck, DriverID, Driver = NULL, StartTime = O.DestDepartTime, EndTime = NO.OriginArriveTime
		, StartOdometer = O.DestTruckMileage, EndOdometer = NO.OriginTruckMileage
		, Mileage = NO.OriginTruckMileage - O.DestTruckMileage
		, RouteMiles = NULL
		, O.ID, no.id
	FROM (
		SELECT O.ID, O.DestDepartTime, O.DestTruckMileage
			, NOID = (SELECT MIN(NO.ID) FROM tblOrder NO WHERE NO.ID > O.ID AND NO.TruckID = O.TruckID AND NO.OriginArriveTimeUTC IS NOT NULL AND NO.DestDepartTimeUTC IS NOT NULL AND NO.DestTruckMileage IS NOT NULL)
		FROM viewOrderLocalDates O
		WHERE O.OriginArriveTimeUTC IS NOT NULL AND O.DestDepartTime IS NOT NULL AND O.OriginTruckMileage IS NOT NULL 
		  AND O.Truck IS NOT NULL
	) O
	JOIN viewOrderLocalDates NO ON NO.ID = O.NOID
	WHERE O.NOID IS NOT NULL
	--ORDER BY Truck, StartTime
GO
GRANT SELECT ON viewMileageLog TO dispatchcrude_iis_acct
GO

/*********************************************
Date Create: 12 Feb 2015
Author: Kevin Alons
Purpose: return a "Mileage Log" from the Order table
*********************************************/
CREATE FUNCTION fnMileageLog(@StartDate date, @EndDate date, @CarrierID int = -1, @DriverID int = -1, @TruckID int = -1)
RETURNS @data TABLE
(
  ID int
  , OrderNum int
  , Carrier varchar(100)
  , Truck varchar(100)
  , Driver text
  , StartTime datetime
  , EndTime datetime
  , StartOdometer int
  , EndOdometer int
  , Mileage int
  , RouteMiles int
  , Type varchar(10)
  , Locked bit
)
AS BEGIN
	INSERT INTO @data
		SELECT ID, OrderNum, Carrier, Truck, Driver
			, StartTime = OriginArriveTime, EndTime = DestDepartTime
			, StartOdometer = OriginTruckMileage, EndOdometer = DestTruckMileage
			, Mileage = DestTruckMileage - OriginTruckMileage
			, RouteMiles = O.ActualMiles
			, Type = 'Loaded'
			, Locked = CASE WHEN O.StatusID = 4 THEN 1 ELSE 0 END
		FROM viewOrderLocalDates O
		WHERE O.OriginArriveTimeUTC IS NOT NULL AND O.DestDepartTimeUTC IS NOT NULL AND O.OriginTruckMileage IS NOT NULL AND O.DestTruckMileage IS NOT NULL
		  AND O.TruckID IS NOT NULL
		  AND (@CarrierID = -1 OR @CarrierID = CarrierID)
		  AND (@DriverID = -1 OR @DriverID = DriverID)
		  AND (@TruckID = -1 OR @TruckID = TruckID)
		  AND O.OrderDate BETWEEN @StartDate and @EndDate

	INSERT INTO @data
		SELECT ID = NULL, OrderNum = NULL, Carrier, Truck, Driver = NULL
			, StartTime = C.EndTime, EndTime = N.StartTime
			, StartOdometer = C.EndOdometer, EndOdometer = N.StartOdometer
			, Mileage = N.StartOdometer - C.EndOdometer
			, RouteMiles = NULL
			, Type = 'Unloaded'
			, Locked = 1
		FROM @data C
		CROSS APPLY (
			SELECT TOP 1 ID, StartTime, StartOdometer FROM @data N WHERE N.StartTime > C.StartTime AND N.Truck = C.Truck ORDER BY StartTime
		) N
	RETURN
END
GO
GRANT SELECT ON fnMileageLog TO dispatchcrude_iis_acct
GO

COMMIT
SET NOEXEC OFF