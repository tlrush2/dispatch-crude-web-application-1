-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.7.17.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.7.17.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0

DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.7.17'
SELECT  @NewVersion = '3.7.18'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'fixes to address performance (timeout) and not always applying Route.ActualMiles when "Rating" selected orders (DCWEB-760)'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Carrier "Settlement" $$ values to an Delivered/Audited order
/***********************************/
ALTER PROCEDURE spApplyRatesCarrier
(
  @ID int
, @UserName varchar(100)
, @OriginWaitAmount money = NULL
, @DestWaitAmount money = NULL
, @RejectionAmount money = NULL
, @FuelSurchargeAmount money = NULL
, @LoadAmount money = NULL
, @AssessorialRateTypeID_CSV varchar(max) = NULL
, @AssessorialAmount_CSV varchar(max) = null
, @ResetOverrides bit = 0
) AS BEGIN

	SET NOCOUNT ON
	
	-- ensure this order hasn't yet been fully settled
	IF EXISTS (SELECT * FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND BatchID IS NOT NULL)
	BEGIN
		RAISERROR('Invoice has already been fully settled', 16, 1)
		RETURN
	END

	-- ensure that Shipper Rates have been applied prior to applying Carrier rates (since they could be dependent on Shipper rates)
	IF NOT EXISTS (SELECT * FROM tblOrderSettlementShipper WHERE OrderID = @ID)
		EXEC spApplyRatesShipper @ID, @UserName
/* test code
drop table #i
drop table #ia

declare @id int, @userName varchar(100)
, @OriginWaitAmount money 
, @DestWaitAmount money 
, @RejectionAmount money 
, @FuelSurchargeAmount money 
, @LoadAmount money 
, @AssessorialRateTypeID_CSV varchar(max) 
, @AssessorialAmount_CSV varchar(max) 
, @ResetOverrides bit 
select @ID=62210,@UserName='kalons'
--select @OriginWaitAmount=1.0000,@DestWaitAmount=2.0000,@RejectionAmount=3.0000,@FuelSurchargeAmount=4.0000,@LoadAmount=5.0000,@AssessorialRateTypeID_CSV='1',@AssessorialAmount_CSV='99'
select @ResetOverrides=0
-- */
	-- get the provided manual Assessorial Rates (if any) into a usable able
	DECLARE @ManualAssessorialRates TABLE (ID int primary key, Amount money)
	INSERT INTO @ManualAssessorialRates (ID, Amount) 
		SELECT ID.ID, R.Amount
		FROM (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), ID FROM dbo.fnSplitCSVIDs(@AssessorialRateTypeID_CSV)) ID
		JOIN (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), amount = CAST(value as money) FROM dbo.fnSplitCSV(@AssessorialAmount_CSV)) R ON R.sortid = ID.sortid

	-- add in an existing manual Assessorial Rates from the existing Order Settlement (if any)
	INSERT INTO @ManualAssessorialRates (ID, Amount)
		SELECT AssessorialRateTypeID, Amount 
		FROM dbo.tblOrderSettlementCarrierAssessorialCharge 
		WHERE OrderID = @ID AND AssessorialRateID IS NULL 
			AND ID NOT IN (SELECT ID FROM @ManualAssessorialRates)
			AND @ResetOverrides = 0
--select * from @ManualAssessorialRates

	IF (@ResetOverrides = 0)
	BEGIN 
		SELECT @OriginWaitAmount = OriginWaitAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @OriginWaitAmount IS NULL AND OriginWaitRateID IS NULL
		SELECT @DestWaitAmount = DestinationWaitAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @DestWaitAmount IS NULL AND DestinationWaitRateID IS NULL
		SELECT @RejectionAmount = OrderRejectAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @RejectionAmount IS NULL AND OrderRejectRateID IS NULL
		SELECT @FuelSurchargeAmount = FuelSurchargeAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @FuelSurchargeAmount IS NULL AND FuelSurchargeRateID IS NULL
		SELECT @LoadAmount = LoadAmount FROM tblOrderSettlementCarrier WHERE OrderID = @ID AND @LoadAmount IS NULL AND RouteRateID IS NULL AND RangeRateID IS NULL
	END
--select OverrideOriginWaitAmount = @OriginWaitAmount, OverrideDestWaitAmount = @DestWaitAmount, OverrideRejectionAmount = @RejectionAmount, OverrideFuelSurchargeAmount = @FuelSurchargeAmount, OverrideLoadAmount = @LoadAmount

	DECLARE @SettlementUnits TABLE
	(
		OrderID int
	  , SettlementUomID int
	  , SettlementFactorID int
	  , MinSettlementUnits decimal(18, 10)
	  , ActualUnits decimal(18, 10)
	  , SettlementUnits decimal(18, 10)
	)
	INSERT INTO @SettlementUnits (OrderID, SettlementUomID, SettlementFactorID, MinSettlementUnits, ActualUnits, SettlementUnits)
		SELECT OrderID, SettlementUomID, SettlementFactorID, MinSettlementUnits, ActualUnits, SettlementUnits
		FROM dbo.viewOrderSettlementUnitsCarrier 
		WHERE OrderID = @ID

	SELECT OrderID = @ID
		, OrderDate
		, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
		, RouteRateID, RangeRateID, LoadAmount
		, WaitFeeParameterID
		, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount 
		, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount
		, OrderRejectRateID, OrderRejectAmount
		, FuelSurchargeRateID, FuelSurchargeRate, FuelSurchargeAmount
		, OriginTaxRate
		, TotalAmount = ISNULL(LoadAmount, 0) 
			+ ISNULL(OriginWaitAmount, 0) 
			+ ISNULL(DestWaitAmount, 0) 
			+ ISNULL(OrderRejectAmount, 0) 
			+ ISNULL(FuelSurchargeAmount, 0)
		, CreatedByUser = @UserName
	INTO #I
	FROM (
		SELECT OrderDate
			, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
			, RouteRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RouteRateID ELSE NULL END
			, RangeRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RangeRateID ELSE NULL END
			, LoadAmount = round(ISNULL(@LoadAmount, RR.Amount), 2)
			, WaitFeeParameterID
			, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount = ROUND(OriginWaitAmount, 2)
			, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount = ROUND(DestWaitAmount, 2)
			, OrderRejectRateID = CASE WHEN @RejectionAmount IS NULL THEN RD.RateID ELSE NULL END
			, OrderRejectAmount = round(isnull(@RejectionAmount, RD.Amount), 2)
			, FuelSurchargeRateID = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.RateID ELSE NULL END
			, FuelSurchargeRate = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.Rate ELSE NULL END
			, FuelSurchargeAmount = round(isnull(@FuelSurchargeAmount, FSR.Amount), 2)
			, OriginTaxRate
		FROM (
			SELECT O.ID
				, O.OriginID
				, O.CarrierID
				, O.RouteID
				, SU.SettlementFactorID
				, SU.SettlementUomID
				, SU.ActualUnits
				, SU.MinSettlementUnits
				, SettlementUnits = CASE WHEN O.Rejected = 1 THEN 0 ELSE SU.SettlementUnits END
				, RouteMiles = O.ActualMiles
				, OrderDate = isnull(O.OrderDate, O.DueDate)
				, O.ChainUp
				, O.H2S
				, O.Rejected
				, O.RerouteCount
				, IsSplit = CASE WHEN O.Rejected = 0 AND O.TicketCount > 1 THEN 1 ELSE 0 END
				, WaitFeeParameterID = WFP.ID
				, OriginWaitRateID = CASE WHEN @OriginWaitAmount IS NULL THEN OWD.RateID ELSE NULL END
				, OriginWaitBillableMinutes = OWD.BillableMinutes
				, OriginWaitBillableHours = OWD.BillableHours
				, OriginWaitAmount = isnull(@OriginWaitAmount, OWD.Amount)
				, DestWaitRateID = CASE WHEN @DestWaitAmount IS NULL THEN DWD.RateID ELSE NULL END
				, DestWaitBillableMinutes = DWD.BillableMinutes
				, DestWaitBillableHours = DWD.BillableHours
				, DestWaitAmount = isnull(@DestWaitAmount, DWD.Amount)
				, OriginTaxRate = OO.TaxRate
			FROM dbo.viewOrder O
			JOIN @SettlementUnits SU ON SU.OrderID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			OUTER APPLY dbo.fnOrderCarrierOriginWaitData(@ID) OWD 
			OUTER APPLY dbo.fnOrderCarrierDestinationWaitData(@ID) DWD 
			OUTER APPLY dbo.fnOrderCarrierWaitFeeParameter(@ID) WFP
			WHERE O.ID = @ID
		) X
		OUTER APPLY dbo.fnOrderCarrierLoadAmount(@ID, SettlementUnits, SettlementUomID) RR 
		OUTER APPLY dbo.fnOrderCarrierOrderRejectData(@ID, SettlementUnits, SettlementUomID) RD
		OUTER APPLY dbo.fnOrderCarrierFuelSurchargeData(@ID) FSR
	) X2
--select * from #I
--select * from #i
	SELECT OrderID = @ID, AssessorialRateTypeID, AssessorialRateID = RateID, Amount = ROUND(Amount, 2), CreatedByUser = @UserName 
	INTO #IA
	FROM @SettlementUnits SU
	CROSS APPLY dbo.fnOrderCarrierAssessorialAmounts(@ID, SU.SettlementUnits, SU.SettlementUomID) CAA
	WHERE CAA.AssessorialRateTypeID NOT IN (SELECT ID FROM @ManualAssessorialRates)
/* more test code
select OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser from #IA
union
select @ID, ID, NULL, Amount, @UserName from @ManualAssessorialRates
--*/
	DECLARE @CreatedTran bit; SET @CreatedTran = CASE WHEN @@TRANCOUNT = 0 THEN 1 ELSE 0 END
	BEGIN TRY
		IF (@CreatedTran = 1)
			BEGIN TRAN

		-- remove the existing settlment record (if any)
		DELETE FROM tblOrderSettlementCarrierAssessorialCharge WHERE OrderID = @ID
		DELETE FROM tblOrderSettlementCarrier WHERE OrderID = @ID
	
		-- all Units and Rates are first normalized to the Order.OriginUOM then consistent processing
		-- and persisted in the Invoice (Settlement) record in this Order.OriginUOM 
		INSERT INTO tblOrderSettlementCarrier (
			OrderID
			, OrderDate
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestinationWaitRateID 
			, DestinationWaitBillableMinutes 
			, DestinationWaitBillableHours
			, DestinationWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser)
		SELECT OrderID
			, OrderDate
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestWaitRateID 
			, DestWaitBillableMinutes 
			, DestWaitBillableHours
			, DestWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser
		FROM #I

		INSERT INTO tblOrderSettlementCarrierAssessorialCharge (OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser)
			SELECT OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser FROM #IA
			UNION
			SELECT @ID, ID, NULL, Amount, @UserName FROM @ManualAssessorialRates
	
		IF (@CreatedTran = 1)
			COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @msg varchar(max), @severity int
		SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY()
		ROLLBACK
		RAISERROR(@msg, @severity, 1)
		RETURN
	END CATCH	
	
END
GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Shipper "Settlement" $$ values to an Delivered/Audited order
/***********************************/
ALTER PROCEDURE spApplyRatesShipper
(
  @ID int
, @UserName varchar(100)
, @OriginWaitAmount money = NULL
, @DestWaitAmount money = NULL
, @RejectionAmount money = NULL
, @FuelSurchargeAmount money = NULL
, @LoadAmount money = NULL
, @AssessorialRateTypeID_CSV varchar(max) = NULL
, @AssessorialAmount_CSV varchar(max) = null
, @ResetOverrides bit = 0
) AS BEGIN

	SET NOCOUNT ON
	
	-- ensure this order hasn't yet been fully settled
	IF EXISTS (SELECT * FROM tblOrderSettlementShipper WHERE OrderID = @ID AND BatchID IS NOT NULL)
	BEGIN
		RAISERROR('Invoice has already been fully settled', 16, 1)
		RETURN
	END

	-- ensure the current Route.ActualMiles is assigned to the order
	UPDATE tblOrder SET ActualMiles = R.ActualMiles
	FROM tblOrder O
	JOIN tblRoute R ON R.ID = O.RouteID
	WHERE O.ID = @ID
	  AND O.ActualMiles IS NULL OR O.ActualMiles <> R.ActualMiles
	
	-- get the provided manual Assessorial Rates (if any) into a usable able
	DECLARE @ManualAssessorialRates TABLE (ID int primary key, Amount money)
	INSERT INTO @ManualAssessorialRates (ID, Amount) 
		SELECT ID.ID, R.Amount
		FROM (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), ID FROM dbo.fnSplitCSVIDs(@AssessorialRateTypeID_CSV)) ID
		JOIN (SELECT sortid = ROW_NUMBER() OVER (ORDER BY getdate()), amount = CAST(value as money) FROM dbo.fnSplitCSV(@AssessorialAmount_CSV)) R ON R.sortid = ID.sortid

	-- add in an existing manual Assessorial Rates from the existing Order Settlement (if any)
	INSERT INTO @ManualAssessorialRates (ID, Amount)
		SELECT AssessorialRateTypeID, Amount 
		FROM dbo.tblOrderSettlementShipperAssessorialCharge 
		WHERE OrderID = @ID AND AssessorialRateID IS NULL 
			AND ID NOT IN (SELECT ID FROM @ManualAssessorialRates)
			AND @ResetOverrides = 0

	IF (@ResetOverrides = 0)
	BEGIN 
		SELECT @OriginWaitAmount = OriginWaitAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @OriginWaitAmount IS NULL AND OriginWaitRateID IS NULL
		SELECT @DestWaitAmount = DestinationWaitAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @DestWaitAmount IS NULL AND DestinationWaitRateID IS NULL
		SELECT @RejectionAmount = OrderRejectAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @RejectionAmount IS NULL AND OrderRejectRateID IS NULL
		SELECT @FuelSurchargeAmount = FuelSurchargeAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @FuelSurchargeAmount IS NULL AND FuelSurchargeRateID IS NULL
		SELECT @LoadAmount = LoadAmount FROM tblOrderSettlementShipper WHERE OrderID = @ID AND @LoadAmount IS NULL AND RouteRateID IS NULL AND RangeRateID IS NULL
	END

	DECLARE @SettlementUnits TABLE
	(
		OrderID int
	  , SettlementUomID int
	  , SettlementFactorID int
	  , MinSettlementUnits decimal(18, 10)
	  , ActualUnits decimal(18, 10)
	  , SettlementUnits decimal(18, 10)
	)
	INSERT INTO @SettlementUnits (OrderID, SettlementUomID, SettlementFactorID, MinSettlementUnits, ActualUnits, SettlementUnits)
		SELECT OrderID, SettlementUomID, SettlementFactorID, MinSettlementUnits, ActualUnits, SettlementUnits
		FROM dbo.viewOrderSettlementUnitsShipper 
		WHERE OrderID = @ID

	SELECT OrderID = @ID
		, OrderDate
		, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
		, RouteRateID, RangeRateID, LoadAmount
		, WaitFeeParameterID
		, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount 
		, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount
		, OrderRejectRateID, OrderRejectAmount
		, FuelSurchargeRateID, FuelSurchargeRate, FuelSurchargeAmount
		, OriginTaxRate
		, TotalAmount = ISNULL(LoadAmount, 0) 
			+ ISNULL(OriginWaitAmount, 0) 
			+ ISNULL(DestWaitAmount, 0) 
			+ ISNULL(OrderRejectAmount, 0) 
			+ ISNULL(FuelSurchargeAmount, 0)
		, CreatedByUser = @UserName
	INTO #I
	FROM (
		SELECT OrderDate
			, SettlementFactorID, SettlementUomID, MinSettlementUnits, SettlementUnits
			, RouteRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RouteRateID ELSE NULL END
			, RangeRateID = CASE WHEN @LoadAmount IS NULL THEN RR.RangeRateID ELSE NULL END
			, LoadAmount = round(ISNULL(@LoadAmount, RR.Amount), 2)
			, WaitFeeParameterID
			, OriginWaitRateID, OriginWaitBillableMinutes, OriginWaitBillableHours, OriginWaitAmount = ROUND(OriginWaitAmount, 2)
			, DestWaitRateID, DestWaitBillableMinutes, DestWaitBillableHours, DestWaitAmount = ROUND(DestWaitAmount, 2)
			, OrderRejectRateID = CASE WHEN @RejectionAmount IS NULL THEN RD.RateID ELSE NULL END
			, OrderRejectAmount = round(isnull(@RejectionAmount, RD.Amount), 2)
			, FuelSurchargeRateID = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.RateID ELSE NULL END
			, FuelSurchargeRate = CASE WHEN @FuelSurchargeAmount IS NULL THEN FSR.Rate ELSE NULL END
			, FuelSurchargeAmount = round(isnull(@FuelSurchargeAmount, FSR.Amount), 2)
			, OriginTaxRate
		FROM (
			SELECT O.ID
				, O.OriginID
				, O.CustomerID
				, O.RouteID
				, SU.SettlementFactorID
				, SU.SettlementUomID
				, SU.ActualUnits
				, SU.MinSettlementUnits
				, SettlementUnits = CASE WHEN O.Rejected = 1 THEN 0 ELSE SU.SettlementUnits END
				, RouteMiles = O.ActualMiles
				, OrderDate = isnull(O.OrderDate, O.DueDate)
				, O.ChainUp
				, O.H2S
				, O.Rejected
				, O.RerouteCount
				, IsSplit = CASE WHEN O.Rejected = 0 AND O.TicketCount > 1 THEN 1 ELSE 0 END
				, WaitFeeParameterID = WFP.ID
				, OriginWaitRateID = CASE WHEN @OriginWaitAmount IS NULL THEN OWD.RateID ELSE NULL END
				, OriginWaitBillableMinutes = OWD.BillableMinutes
				, OriginWaitBillableHours = OWD.BillableHours
				, OriginWaitAmount = isnull(@OriginWaitAmount, OWD.Amount)
				, DestWaitRateID = CASE WHEN @DestWaitAmount IS NULL THEN DWD.RateID ELSE NULL END
				, DestWaitBillableMinutes = DWD.BillableMinutes
				, DestWaitBillableHours = DWD.BillableHours
				, DestWaitAmount = isnull(@DestWaitAmount, DWD.Amount)
				, OriginTaxRate = OO.TaxRate
			FROM dbo.viewOrder O
			JOIN @SettlementUnits SU ON SU.OrderID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			OUTER APPLY dbo.fnOrderShipperOriginWaitData(@ID) OWD 
			OUTER APPLY dbo.fnOrderShipperDestinationWaitData(@ID) DWD 
			OUTER APPLY dbo.fnOrderShipperWaitFeeParameter(@ID) WFP
			WHERE O.ID = @ID
		) X
		OUTER APPLY dbo.fnOrderShipperLoadAmount(@ID, SettlementUnits, SettlementUomID) RR 
		OUTER APPLY dbo.fnOrderShipperOrderRejectData(@ID, SettlementUnits, SettlementUomID) RD
		OUTER APPLY dbo.fnOrderShipperFuelSurchargeData(@ID) FSR
	) X2

	SELECT OrderID = @ID, AssessorialRateTypeID, AssessorialRateID = RateID, Amount = ROUND(Amount, 2), CreatedByUser = @UserName 
	INTO #IA
	FROM @SettlementUnits SU
	CROSS APPLY dbo.fnOrderShipperAssessorialAmounts(@ID, SU.SettlementUnits, SU.SettlementUomID) CAA
	WHERE CAA.AssessorialRateTypeID NOT IN (SELECT ID FROM @ManualAssessorialRates)

	DECLARE @CreatedTran bit; SET @CreatedTran = CASE WHEN @@TRANCOUNT = 0 THEN 1 ELSE 0 END
	BEGIN TRY
		IF (@CreatedTran = 1)
			BEGIN TRAN

		-- remove the existing settlment record (if any)
		DELETE FROM tblOrderSettlementShipperAssessorialCharge WHERE OrderID = @ID
		DELETE FROM tblOrderSettlementShipper WHERE OrderID = @ID
	
		-- all Units and Rates are first normalized to the Order.OriginUOM then consistent processing
		-- and persisted in the Invoice (Settlement) record in this Order.OriginUOM 
		INSERT INTO tblOrderSettlementShipper (
			OrderID
			, OrderDate
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestinationWaitRateID 
			, DestinationWaitBillableMinutes 
			, DestinationWaitBillableHours
			, DestinationWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser)
		SELECT OrderID
			, OrderDate
			, SettlementFactorID 
			, SettlementUomID 
			, MinSettlementUnits 
			, SettlementUnits
			, RouteRateID
			, RangeRateID 
			, LoadAmount
			, WaitFeeParameterID 
			, OriginWaitRateID 
			, OriginWaitBillableMinutes 
			, OriginWaitBillableHours
			, OriginWaitAmount 
			, DestWaitRateID 
			, DestWaitBillableMinutes 
			, DestWaitBillableHours
			, DestWaitAmount 
			, OrderRejectRateID 
			, OrderRejectAmount 
			, FuelSurchargeRateID 
			, FuelSurchargeRate 
			, FuelSurchargeAmount 
			, OriginTaxRate 
			, TotalAmount 
			, CreatedByUser
		FROM #I

		INSERT INTO tblOrderSettlementShipperAssessorialCharge (OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser)
			SELECT OrderID, AssessorialRateTypeID, AssessorialRateID, Amount, CreatedByUser FROM #IA
			UNION
			SELECT @ID, ID, NULL, Amount, @UserName FROM @ManualAssessorialRates
	
		IF (@CreatedTran = 1)
			COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @msg varchar(max), @severity int
		SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY()
		ROLLBACK
		RAISERROR(@msg, @severity, 1)
		RETURN
	END CATCH	
	
END
GO

EXEC _spDropFunction 'fnFixedLenStr'
GO
/*******************************************
-- Date Created: 28 May 2015
-- Author: Kevin Alons
-- Purpose: format a string to a fixed length (truncate if short, pad (right) with spaces if short
*******************************************/
CREATE FUNCTION fnFixedLenStr(@str varchar(max), @fixedLen int) RETURNS varchar(max) AS
BEGIN
	DECLARE @ret varchar(255)
	SET @ret = LEFT(@str + REPLICATE(' ', @fixedLen), @fixedLen)
	RETURN (@ret)
END
GO
GRANT EXECUTE ON fnFixedLenStr TO role_iis_acct
GO

EXEC _spDropFunction 'fnFixedLenNum'
GO
/*******************************************
-- Date Created: 28 May 2015
-- Author: Kevin Alons
-- Purpose: format a number to a fixed length
*******************************************/
CREATE FUNCTION fnFixedLenNum(@num decimal(38, 10), @leftDigits int, @decimalDigits int, @fixedLen int) RETURNS varchar(255) AS
BEGIN
	DECLARE @ret varchar(255)
	SET @ret = ''
	
	DECLARE @strNum varchar(255)
	SET @strNum = CAST(round(@num, @decimalDigits) as varchar(255))

	DECLARE @decimalPos int
	SET @decimalPos = CHARINDEX('.', @strNum, 1)
	IF @decimalPos = 0 SET @decimalPos = LEN(@strNum) + 1
	
	SET @ret = SUBSTRING(@strNum, 1, @decimalPos - 1)
	SET @ret = RIGHT(REPLICATE('0', @leftDigits) + @ret, @leftDigits)
	
	IF @decimalDigits > 0 
	BEGIN
		DECLARE @strDecimal varchar(255)
		
		SET @strDecimal = SUBSTRING(@strNum + REPLICATE('0', @decimalDigits), @decimalPos + 1, @decimalDigits)
		SET @ret = @ret + '.' + @strDecimal
	END

	IF @fixedLen <> 0 
	BEGIN
		IF LEN(@ret) > @fixedLen
			SET @ret = SUBSTRING(@ret, 1, @fixedLen)
		ELSE IF LEN(@ret) < @fixedLen
			SET @ret = REPLICATE('0', @fixedLen - LEN(@ret)) + @ret
	END
	RETURN (@ret)
END
GO
GRANT EXECUTE ON fnFixedLenNum TO role_iis_acct
GO

COMMIT
SET NOEXEC OFF