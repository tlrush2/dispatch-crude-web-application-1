SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20) = '3.13.14'
	, @NewVersion varchar(20) = '3.13.15'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'Clean up maps, add permission for new driver maps'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

	
-- Add new role
DECLARE @roleName VARCHAR(100) = 'viewDriverMap'
DECLARE @roleDesc VARCHAR(100) = 'View Driver Maps'

INSERT INTO ASPNET_ROLES
select applicationid, NEWID(), @roleName, @roleName, @roleDesc from aspnet_applications 
where not exists (select 1 from aspnet_roles where rolename = @roleName)

-- Add current admins to role
EXEC spAddUserPermissionsByRole 'Administrator', @roleName
GO


COMMIT
SET NOEXEC OFF