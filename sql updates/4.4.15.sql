SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.4.14'
SELECT  @NewVersion = '4.4.15'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'JT-162/JT-327/JT-328  - Add Signatures for HOS'	
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

-----------------------------------------------------------------------------------------------

CREATE TABLE tblHosSignature
(
	ID INT IDENTITY(1,1) NOT NULL,
	UID UNIQUEIDENTIFIER NOT NULL CONSTRAINT DF_HOSSignature_UID DEFAULT NEWID(),
	DriverID INT NOT NULL CONSTRAINT FK_HOSSignature_Driver REFERENCES tblDriver(ID),
	HosDate DATE NOT NULL,
	SignatureBlob VARBINARY(MAX) NULL,
	CreateDateUTC DATETIME NOT NULL CONSTRAINT DF_HOSSignature_CreateDateUTC DEFAULT GETUTCDATE(),
	CreatedByUser VARCHAR(100) NOT NULL CONSTRAINT DF_HOSSignature_CreatedByUser DEFAULT 'System'
)

GO


/*******************************************
-- Date Created: 2016/12/07
-- Author: Joe Engler
-- Purpose: return confirmation of Signature records for the specified driver (that are still relevant at this time)
-- Changes: 
*******************************************/
CREATE PROCEDURE spHosSignatures_DriverApp(@DriverID int, @LastChangeDateUTC datetime = NULL) AS
BEGIN
    DECLARE @DaysToKeep INT = 
			(SELECT DriverAppLogRetentionDays 
				FROM tblHosPolicy 
				WHERE ID = COALESCE(
					(SELECT ID FROM tblHosPolicy WHERE Name = (SELECT r.Value
						FROM tblDriver d 
						CROSS APPLY dbo.fnCarrierRules(GETDATE(), null, 1, d.CarrierID, d.ID, d.OperatingStateID, d.RegionID, 1) r 
						WHERE d.ID = @DriverID)), 1) -- Use default policy if none set
			)

	DECLARE @StartDate DATETIME = DATEADD(DAY, -@DaysToKeep, GETUTCDATE());

    SELECT HS.ID, HS.UID, HS.DriverID, HOSDate = DATEADD(HOUR, 12, CAST (HS.HosDate AS DATETIME)), SignatureBlob = NULL, HS.CreateDateUTC, HS.CreatedByUser -- Mobile app wants date as local noon
    FROM dbo.tblHosSignature HS
    CROSS JOIN dbo.fnSyncLCDOffset(@LastChangeDateUTC) LCD
    WHERE HS.DriverID = @DriverID 
      AND ( HS.CreateDateUTC >= LCD.LCD
		     /* always include days-to-keep signatures if a NULL @LastChangeDateUTC is specified */
			 OR @LastChangeDateUTC IS NULL AND HS.CreateDateUTC > @StartDate
        )
END

GO


/******************************************
** Date Created:	2016/07/15
** Author:			Joe Engler
** Purpose:			Return the historical HOS entries for a driver, calculate the TimeInState for ease of computation on mobile side
** Changes:
--		4.2.5		JAE		2016-10-26		Use operating state (instead of state) to carrier rule call
--		4.4.15		JAE		2017-01-06		Use policy (not carrier rule) to get days to keep
******************************************/
ALTER PROCEDURE spDriverHOSLog(@DriverID INT, @LastSyncDate DATETIME) AS
BEGIN
    DECLARE @DaysToKeep INT = 
			(SELECT DriverAppLogRetentionDays 
				FROM tblHosPolicy 
				WHERE ID = COALESCE(
					(SELECT ID FROM tblHosPolicy WHERE Name = (SELECT r.Value
						FROM tblDriver d 
						CROSS APPLY dbo.fnCarrierRules(GETDATE(), null, 1, d.CarrierID, d.ID, d.OperatingStateID, d.RegionID, 1) r 
						WHERE d.ID = @DriverID)), 1) -- Use default policy if none set
			)

	DECLARE @StartDate DATETIME = DATEADD(DAY, -@DaysToKeep, GETUTCDATE());

	WITH rows AS
	(
 			-- Dummy start record, gets the previous record prior to the start date
			SELECT 0 AS rownum, *
            FROM tblHos
			WHERE ID = (SELECT TOP 1 ID FROM tblHos WHERE DriverID = @DriverID AND LogDateUTC < @StartDate ORDER BY LogDateUTC DESC) 

			UNION

			SELECT ROW_NUMBER() OVER (ORDER BY LogDateUTC) AS rownum, *
			FROM tblHos
			WHERE DriverID = @DriverID
			AND LogDateUTC BETWEEN @StartDate AND GETUTCDATE()

			UNION

			-- Dummy end record, gets the final record again so it will show in the summary list (with TimeInState = 0)
			SELECT (select count(*)+1 FROM tblHos WHERE DriverID = @DriverID AND LogDateUTC BETWEEN @StartDate AND GETUTCDATE()) AS rownum, *
			FROM tblHos
			WHERE DriverID = @DriverID
			AND ID = (SELECT TOP 1 ID FROM tblHos WHERE DriverID = @DriverID AND LogDateUTC <= GETUTCDATE() ORDER BY LogDateUTC DESC)
	)
		SELECT mc.*, CASE WHEN mc.ID = mp.ID THEN null -- send null time in state for current HOS (repeated at end) 
						else DATEDIFF(MINUTE, mc.LogDateUTC, mp.LogDateUTC) END AS TimeInState
		FROM rows mc
		JOIN rows mp
		ON mc.rownum = mp.rownum-1
		CROSS JOIN fnSyncLCDOffset(@LastSyncDate) LCD
		WHERE @LastSyncDate IS NULL 
		   OR mc.CreateDateUTC >= LCD.LCD -- use offset to get nearest city updates for records just sent to the server
		   OR mc.LastChangeDateUTC >= LCD.LCD
		   OR mc.DeleteDateUTC >= LCD.LCD
		ORDER BY mc.LogDateUTC
END

GO


COMMIT
SET NOEXEC OFF