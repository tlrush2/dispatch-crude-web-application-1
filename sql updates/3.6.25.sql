-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.6.24'
SELECT  @NewVersion = '3.6.25'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DriverApp: Sync was not properly returning Ticket + MeterFactor, Open|Close Meter Units, DispatchConfirmNum'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/*******************************************/
-- Date Created: 25 Apr 2013
-- Author: Kevin Alons
-- Purpose: return OrderTicket data for Driver App sync
/*******************************************/
ALTER FUNCTION fnOrderTicket_DriverApp( @DriverID int, @LastChangeDateUTC datetime ) RETURNS  TABLE AS
RETURN 
	SELECT OT.ID
		, OT.UID
		, OT.OrderID
		, OT.CarrierTicketNum
		, OT.OriginTankID
		, OT.TankNum
		, OT.TicketTypeID
		, OT.ProductObsGravity
		, OT.ProductObsTemp
		, OT.ProductHighTemp
		, OT.ProductLowTemp
		, OT.ProductBSW
		, cast(OT.OpeningGaugeFeet as tinyint) AS OpeningGaugeFeet
		, cast(OT.OpeningGaugeInch as tinyint) AS OpeningGaugeInch
		, cast(OT.OpeningGaugeQ as tinyint) AS OpeningGaugeQ
		, cast(OT.ClosingGaugeFeet as tinyint) AS ClosingGaugeFeet
		, cast(OT.ClosingGaugeInch as tinyint) AS ClosingGaugeInch
		, cast(OT.ClosingGaugeQ as tinyint) AS ClosingGaugeQ
		, OT.BottomFeet
		, OT.BottomInches
		, OT.BottomQ
		, OT.GrossUnits
		, OT.GrossStdUnits
		, OT.NetUnits
		, OT.Rejected
		, OT.RejectReasonID
		, OT.RejectNotes
		, OT.SealOff
		, OT.SealOn
		, OT.BOLNum
		, OT.CreateDateUTC
		, OT.CreatedByUser
		, OT.LastChangeDateUTC
		, OT.LastChangedByUser
		, OT.DeleteDateUTC
		, OT.DeletedByUser
		, OT.MeterFactor
		, OT.OpenMeterUnits
		, OT.CloseMeterUnits
		, OT.DispatchConfirmNum
	FROM dbo.tblOrderTicket OT
	JOIN dbo.tblOrder O ON O.ID = OT.OrderID
	LEFT JOIN tblOrderDriverAppVirtualDelete ODAVD ON ODAVD.OrderID = O.ID AND ODAVD.DriverID = @DriverID
	CROSS JOIN (SELECT DATEADD(second, -5, @LastChangeDateUTC) AS LCD) LCD
	WHERE (O.StatusID IN (2, 7, 8, 3) -- Dispatched, Accepted, Picked Up, Delivered
		-- or this potentially was a DriverApp record, that no longer is, so should be included (as deleted)
		OR (ODAVD.ID IS NOT NULL AND ODAVD.VirtualDeleteDateUTC >= LCD.LCD))
	  AND O.DriverID = @DriverID
	  AND (@LastChangeDateUTC IS NULL 
		OR OT.CreateDateUTC >= LCD.LCD
		OR OT.LastChangeDateUTC >= LCD.LCD
		OR ODAVD.VirtualDeleteDateUTC >= LCD.LCD)
GO

COMMIT
SET NOEXEC OFF