SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.1.1'
SELECT  @NewVersion = '4.1.2'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1668 Capture driver local time'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

ALTER TABLE tblQuestionnaireSubmission ADD SubmissionTimeZoneID TINYINT NULL CONSTRAINT FK_QuestionnaireSubmission_LogTimeZone REFERENCES tblTimeZone(ID)
ALTER TABLE tblQuestionnaireSubmission DROP COLUMN SubmissionTZOffset

GO


COMMIT
SET NOEXEC OFF