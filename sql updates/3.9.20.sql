-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.9.19.14.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.9.19.14.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0

SET NOEXEC OFF
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.9.19.14'
SELECT  @NewVersion = '3.9.20'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'DCWEB-889, 891, 249, 890, 911: Add Order Transfer functionality (Several big changes)'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


IF OBJECT_ID('tblOrderTransfer', 'U') IS NOT NULL DROP TABLE tblOrderTransfer 
GO
CREATE TABLE tblOrderTransfer
(
  OrderID INT NOT NULL CONSTRAINT PK_OrderTransfer PRIMARY KEY
, OriginDriverID INT NOT NULL CONSTRAINT FK_OrderTransfer_Driver FOREIGN KEY REFERENCES tblDriver(ID)
, OriginTruckID INT NOT NULL CONSTRAINT FK_OrderTransfer_Truck FOREIGN KEY REFERENCES tblTruck(ID)
, OriginTruckEndMileage INT NULL
, DestTruckStartMileage INT NULL
, PercentComplete INT
, Notes VARCHAR(255) NULL
, CreateDateUTC DATETIME NOT NULL CONSTRAINT DF_OrderTransfer_CreateDateUTC DEFAULT (GETUTCDATE())
, CreatedByUser VARCHAR(100) NULL
, LastChangeDateUTC DATETIME NULL
, LastChangedByUser VARCHAR(100) NULL
, TransferComplete BIT NOT NULL CONSTRAINT DF_OrderTransfer_TransferComplete DEFAULT (0)
)
GO
GRANT SELECT, INSERT, DELETE, UPDATE ON tblOrderTransfer TO role_iis_acct
GO

-- JAE 10/26/15 - add approval feature for override transfer % complete for order transfers
ALTER TABLE tblOrderApproval ADD OverrideTransferPercentComplete int NULL
GO

/************************************************
-- Author:		Kevin Alons
-- Create date: 19 Dec 2012
-- Description:	trigger to 
				1) validate any changes, and fail the update if invalid changes are submitted
				2) add a unique, incrementing OrderNum to each new Order (manual Identity column)
				3) recompute wait times (origin|destination based on times provided)
				4) generate route table entry for any newly used origin-destination combination
				5) keep Order consistent with Origin.Producer|Operator|UOM & Dest.UOM then the Origin|Dest change
				6) roll the PrintStatus back to NOTFINALIZED (0) when status is rolled back
				7) when DriverID changes, mark those orders AS DELETED and clone the order with the new DriverID and associated records
				8) update any ticket quantities for open orders when the UOM is changed for the Origin
				9) update the Driver.Truck\Trailer\Trailer2 defaults & related DISPATCHED orders when driver updates them on an order
-REMOVED		10) update the Pickup/DeliverLastChangeDateUTC when Origin/Destination is changed (respectively)
				11) (re) apply Settlement Amounts to orders not yet fully settled when status is changed to DELIVERED
				12) if DBAudit is turned on, save an audit record for this Order change
-- Changes: 
	- 3.7.4 05/08/15 GSM Added Rack/Bay field to DBAudit logic
	- 3.7.7 - 15 May 2015 - KDA - REMOVE #10 above, instead update the LastChangeDateUTC whenever an Origin or Destination is changed
	- 3.7.11 - 18 May 2015 - KDA - generally use GETUTCDATE for all LastChangeDateUTC revisions (instead of related Order.LastChangeDateUTC, etc)
	- 3.7.12 - 5/19/2015 - KDA - update any existing OrderTicket records when the Order is DISPATCHED to a driver (so the Driver App will ALWAYS receive the existing TICKETS from the GAUGER)
	- 3.7.23 - 06/05/2015 - GSM - DCDRV-154: Ticket Type following Origin Change
	- 3.7.23 - 06/05/2015 - GSM - DCWEB-530 - ensure ASSIGNED orders with a driver defined are updated to DISPATCHED
	- 3.8.1  - 2015/07/04 - KDA - purge any Driver|Gauger App ZPL related SYNC change records when order is AUDITED
	- 3.9.0  - 2015/08/20 - KDA - add invocation of spAutoAuditOrder for DELIVERED orders (that qualify) when the Auto-Audit global setting value is TRUE
								- add invocation of spAutoApproveOrder for AUDITED orders (that qualify)
	- 3.9.2  - 2015/08/25 - KDA - appropriately use new tblOrder.OrderDate date column
	- 3.9.4  - 2015/08/30 - KDA - performanc optimization to prevent Orderdate computation if no timestamp fields are yet populated
	- 3.9.5  - 2015/08/31 - KDA - more extensive performance optimizations to minimize performance ramifications or computing OrderDate dynamically from OrderRule
	- 3.9.13 - 2015/09/01 - KDA - add (but commented out) some code to validate Arrive|Depart time being present when required
								- allow orders to be rolled back from AUDITED to DELIVERED when in AUTO-AUDIT mode
								- always AUTO UNAPPROVE orders when the status is reverted from AUDITED to DELIVERED 
	- 3.9.19 - 2015/09/23 - KDA - remove obsolete reference to tblDriverAppPrintTicketTemplate
	- 3.9.20 - 2015/09/23 - KDA - add support for tblOrderTransfer (do not accomplish #7 above - cloning/delete orders that are driver re-assigned)
************************************************/
ALTER TRIGGER trigOrder_IU ON tblOrder AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @changesFound bit
	
		-- Note: the TRIGGER_NESTLEVEL( OBJECT_ID('trigOrder_IU') ) = 1 statement prevents recursion
	IF (TRIGGER_NESTLEVEL( OBJECT_ID('trigOrder_IU')) = 1 AND TRIGGER_NESTLEVEL( OBJECT_ID('trigOrderTicket_IU')) = 0) BEGIN

		/**********  START OF VALIDATION SECTION ************************/

		IF (UPDATE(OrderNum) 
			OR UPDATE(PriorityID) 
			OR UPDATE(DueDate) 
			OR UPDATE(RouteID) 
			OR UPDATE(OriginID) 
			OR UPDATE(OriginArriveTimeUTC) 
			OR UPDATE(OriginDepartTimeUTC) 
			OR UPDATE(OriginMinutes) 
			OR UPDATE(OriginWaitNotes) 
			OR UPDATE(OriginBOLNum) 
			OR UPDATE(OriginGrossUnits) 
			OR UPDATE(OriginNetUnits) 
			OR UPDATE(DestinationID) 
			OR UPDATE(DestArriveTimeUTC) 
			OR UPDATE(DestDepartTimeUTC) 
			OR UPDATE(DestMinutes) 
			OR UPDATE(DestWaitNotes) 
			OR UPDATE(DestBOLNum) 
			OR UPDATE(DestGrossUnits) 
			OR UPDATE(DestNetUnits) 
			OR UPDATE(CustomerID) 
			OR UPDATE(CarrierID) 
			OR UPDATE(DriverID) 
			OR UPDATE(TruckID)
			OR UPDATE(TrailerID) 
			OR UPDATE(Trailer2ID) 
			OR UPDATE(OperatorID) 
			OR UPDATE(PumperID) 
			OR UPDATE(TicketTypeID) 
			OR UPDATE(Rejected) 
			OR UPDATE(RejectNotes) 
			OR UPDATE(ChainUp) 
			-- allow this to be changed even in audit status
			--OR UPDATE(OriginTruckMileage) 
			OR UPDATE(OriginTankNum) 
			-- allow this to be changed even in audit status
			--OR UPDATE(DestTruckMileage) 
			OR UPDATE(CarrierTicketNum) 
			OR UPDATE(AuditNotes) 
			OR UPDATE(CreateDateUTC) 
			-- it is permissible to change the ActualMiles value on Audited orders up to the point the Order has been Shipper-Settled
			OR (UPDATE(ActualMiles) AND EXISTS (SELECT * FROM deleted i JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = i.ID WHERE OSS.BatchID IS NOT NULL))
			OR UPDATE(ProducerID) 
			OR UPDATE(CreatedByUser) 
			OR UPDATE(LastChangeDateUTC) 
			OR UPDATE(LastChangedByUser) 
			OR UPDATE(DeleteDateUTC) 
			OR UPDATE(DeletedByUser) 
			OR UPDATE(DestProductBSW)
			OR UPDATE(DestProductGravity) 
			OR UPDATE(DestProductTemp) 
			OR UPDATE(ProductID) 
			OR UPDATE(AcceptLastChangeDateUTC) 
			OR UPDATE(PickupLastChangeDateUTC) 
			OR UPDATE(DeliverLastChangeDateUTC) 
			OR UPDATE(OriginUomID) 
			OR UPDATE(DestUomID) 
			OR UPDATE(PickupPrintStatusID) 
			OR UPDATE(DeliverPrintStatusID)
			OR UPDATE(PickupPrintDateUTC) 
			OR UPDATE(DeliverPrintDateUTC) 
			OR UPDATE(OriginTankID) 
			OR UPDATE(OriginGrossStdUnits) 
			OR UPDATE(DispatchConfirmNum) 
			OR UPDATE(DispatchNotes)
			OR UPDATE(PickupDriverNotes)
			OR UPDATE(DeliverDriverNotes)
			OR UPDATE(OriginWaitReasonID)
			OR UPDATE(DestWaitReasonID)
			OR UPDATE(RejectReasonID) 
			OR UPDATE(DestOpenMeterUnits) 
			OR UPDATE(DestCloseMeterUnits)
			OR UPDATE(DestRackBay)
			OR UPDATE(OrderDate)
		)
			--OR UPDATE(ReassignKey)) we don't do any work if this value changes so just ignore it
		BEGIN
			-- only allow the StatusID value to be changed on an audited order
			IF EXISTS (SELECT * FROM deleted WHERE StatusID = 4)
			BEGIN
				-- if the "Allow Audited Order Update (temp only)" setting is TRUE then only warn when this occurs
				IF (dbo.fnToBool(dbo.fnSettingValue(35)) = 1)
					PRINT 'AUDITED orders is being modified - please investigate why!'
				ELSE BEGIN
					-- otherwise (normal behavior) - prevent AUDITED orders from being modified (except to UN-AUDIT them)
					RAISERROR('AUDITED orders cannot be modified!', 16, 1)
					IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
					RETURN
				END
			END
		END
		ELSE IF NOT UPDATE(StatusID) -- NO CHANGES DETECTED SO JUST QUIT
			RETURN

		PRINT 'trigOrder_IU FIRED'
					
		-- ensure the Origin and Destinations are both specified unless the Status is:
		--   (Generated, Assigned, Dispatched or Declined)
		IF  EXISTS(SELECT * FROM inserted O WHERE (O.OriginID IS NULL OR O.DestinationID IS NULL) AND O.StatusID NOT IN (-10,1,2,9))
		BEGIN
			RAISERROR('Invalid Order Status when the Origin and/or Or Destination is not specified', 16, 1)
			IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
			RETURN
		END
		/* this is commented out until we get the Android issues closer to resolved 
		IF EXISTS (SELECT * FROM inserted WHERE StatusID IN (8, 3, 4) AND OriginArriveTimeUTC IS NULL OR OriginDepartTimeUTC IS NULL)
		BEGIN
			RAISERROR('OriginArriveTimeUTC and/or OriginDepartTimeUTC missing when required', 16, 1)
			IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
			RETURN
		END
		IF EXISTS (SELECT * FROM inserted WHERE StatusID IN (3, 4) AND DestArriveTimeUTC IS NULL OR DestDepartTimeUTC IS NULL)
		BEGIN
			RAISERROR('DestArriveTimeUTC and/or DestDepartTimeUTC missing when required', 16, 1)
			IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
			RETURN
		END
		******************************************************************************/
		
		/**********  END OF VALIDATION SECTION ************************/

		WHILE (SELECT count(*) FROM inserted i JOIN tblOrder O ON O.ID = i.ID WHERE O.OrderNum IS NULL) > 0
		BEGIN
			UPDATE tblOrder 
			  SET OrderNum = isnull((SELECT max(OrderNum) FROM tblOrder), 100000) + 1
				, CreateDateUTC = getutcdate()
			WHERE ID = (SELECT min(O.ID) FROM tblOrder O JOIN inserted i ON i.ID = O.ID WHERE O.OrderNum IS NULL)
		END
		
		-- re-compute the OriginMinutes (in case the website failed to compute it properly)
		IF UPDATE(OriginArriveTimeUTC) OR UPDATE(OriginDepartTimeUTC)
		BEGIN
			UPDATE tblOrder SET OriginMinutes = datediff(minute, i.OriginArriveTimeUTC, i.OriginDepartTimeUTC)
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
		END
		-- re-compute the DestMinutes (in case the website failed to compute it properly)
		IF UPDATE(DestArriveTimeUTC) OR UPDATE(DestDepartTimeUTC)
		BEGIN
			UPDATE tblOrder SET DestMinutes = datediff(minute, i.DestArriveTimeUTC, i.DestDepartTimeUTC)
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
		END
		
		/* ensure any changes to the order always update the Order.OrderDate field */
		IF (UPDATE(CustomerID) 
			OR UPDATE(CarrierID) 
			OR UPDATE(ProductID) 
			OR UPDATE(OriginID) 
			OR UPDATE(DestinationID) 
			OR UPDATE(ProducerID) 
			OR UPDATE(OriginArriveTimeUTC) OR UPDATE(OriginDepartTimeUTC)
			OR UPDATE(DestArriveTimeUTC) OR UPDATE(DestDepartTimeUTC))
		BEGIN
			UPDATE tblOrder 
			  SET OrderDate = dbo.fnOrderDate(O.ID)
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
			JOIN deleted d ON d.ID = i.ID
			LEFT JOIN tblOrderSettlementCarrier OSC ON OSC.OrderID = i.ID
			LEFT JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = i.ID
			WHERE i.StatusID <> 4 
			  -- the order has at least 1 valid timestamp
			  AND (i.OriginArriveTimeUTC IS NOT NULL OR i.OriginDepartTimeUTC IS NOT NULL OR i.DestArriveTimeUTC IS NOT NULL OR i.DestDepartTimeUTC IS NOT NULL)
			  -- the order is not yet settled
			  AND OSC.BatchID IS NULL
			  AND OSS.BatchID IS NULL
		END
		
		-- ensure missing Routes are created and assigned for the specified Origin/Destination combination
		IF UPDATE(OriginID) OR UPDATE(DestinationID)
		BEGIN
			-- create any missing Route records
			INSERT INTO tblRoute (OriginID, DestinationID, CreateDateUTC, CreatedByUser)
				SELECT i.OriginID, i.DestinationID, GETUTCDATE(), ISNULL(i.LastChangedByUser, i.CreatedByUser)
				FROM inserted i
				LEFT JOIN tblRoute R ON R.OriginID = i.OriginID AND R.DestinationID = i.DestinationID
				WHERE i.OriginID IS NOT NULL AND i.DestinationID IS NOT NULL AND R.ID IS NULL
			
			-- ensure the Order records refer to the correct Route (ID)
			UPDATE tblOrder SET RouteID = R.ID
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
			LEFT JOIN tblRoute R ON R.OriginID = O.OriginID AND R.DestinationID = O.DestinationID
			WHERE O.RouteID IS NULL OR O.RouteID <> R.ID
			
			-- update the ActualMiles from the related Route
			UPDATE tblOrder SET ActualMiles = R.ActualMiles
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
			JOIN tblRoute R ON R.ID = O.RouteID
			WHERE O.ActualMiles <> R.ActualMiles
		END
		
		IF (UPDATE(OriginID))
		BEGIN
			-- update Order.ProducerID/OperatorID/PumperID to match what is assigned to the new Origin
			UPDATE tblOrder 
				SET TicketTypeID = OO.TicketTypeID
					, ProducerID = OO.ProducerID
					, OperatorID = OO.OperatorID
					, PumperID = OO.PumperID
					, LastChangeDateUTC = GETUTCDATE() 
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			WHERE d.OriginID <> O.OriginID

			-- update Order.OriginUomID to match what is assigned to the new Origin (quantities are updated below)
			UPDATE tblOrder SET OriginUomID = OO.UomID
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			WHERE d.OriginID <> O.OriginID AND d.OriginUomID <> OO.UomID
		END
		
		-- keep the DestUomID in sync with the Destination (units are updated below)
		IF (UPDATE(DestinationID))
		BEGIN
			-- update Order.DestUomID to match what is assigned to the new Destination
			UPDATE tblOrder 
			  SET DestUomID = DD.UomID
				, LastChangeDateUTC = GETUTCDATE()
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID
			JOIN tblDestination DD ON DD.ID = O.DestinationID
			WHERE d.DestinationID <> O.DestinationID AND d.DestUomID <> DD.UomID
		END
		
		-- ensure that any set PrintStatusID values are reset to 0 when backing up the general StatusID value
		IF (UPDATE(StatusID))
		BEGIN
			UPDATE tblOrder 
			  SET DeliverPrintStatusID = 0 
				, LastChangeDateUTC = GETUTCDATE()
			  FROM tblOrder O
			  JOIN deleted d ON d.ID = O.ID
			WHERE O.StatusID <> d.StatusID AND O.StatusID IN (-10, 1, 2, 7, 8) -- Generated, Assigned, Dispatched, Accepted, Picked-Up

			UPDATE tblOrder 
			  SET PickupPrintStatusID = 0 
				, LastChangeDateUTC = GETUTCDATE()
			  FROM tblOrder O
			  JOIN deleted d ON d.ID = O.ID
			WHERE O.StatusID <> d.StatusID AND O.StatusID IN (-10, 1, 2, 7) -- Generated, Assigned, Dispatched, Accepted
			
			/* ensure that any orders that are DISPATCHED - any existing orders are TOUCHED so they are syncable to the Driver App */
			UPDATE tblOrderTicket
				SET LastChangeDateUTC = GETUTCDATE()
			WHERE OrderID IN (
				SELECT i.ID 
				FROM inserted i
				JOIN deleted d ON d.ID = i.ID
				WHERE i.StatusID <> d.StatusID AND i.StatusID IN (2) -- DISPATCHED
			)
	END

		-- just updating the tickets here, trigOrderTicket_IU will in turn update this table
		-- ensure any existing OrderTickets are recomputed with the new OriginUOM
		UPDATE tblOrderTicket
		  SET GrossUnits = dbo.fnConvertUOM(GrossUnits, d.OriginUomID, O.OriginUomID)
			, NetUnits = dbo.fnConvertUOM(NetUnits, d.OriginUomID, O.OriginUomID)
			, LastChangeDateUTC = GETUTCDATE()
		FROM tblOrderTicket OT
		JOIN tblOrder O ON O.ID = OT.OrderID
		JOIN deleted d ON d.ID = O.ID
		WHERE O.OriginUomID <> d.OriginUomID
		
		-- recalculate the Destination Unit values if only if the DestUomID changed but the values didn't
		UPDATE tblOrder
		  SET DestGrossUnits = dbo.fnConvertUOM(O.DestGrossUnits, d.DestUomID, O.DestUomID)
			, DestNetUnits = dbo.fnConvertUOM(O.DestNetUnits, d.DestUomID, O.DestUomID)
			, LastChangeDateUTC = GETUTCDATE()
		FROM tblOrder O
		JOIN deleted d ON d.ID = O.ID
		WHERE d.DestUomID <> O.DestUomID 
		  AND d.DestGrossUnits = O.DestGrossUnits
		  AND d.DestNetUnits = O.DestNetUnits

		/* DCWEB-530 - ensure any ASSIGNED orders with a DRIVER assigned is set to DISPATCHED */
		UPDATE tblOrder SET StatusID = 2 /*DISPATCHED*/
		FROM tblOrder O
		JOIN inserted i ON I.ID = O.ID
		WHERE i.StatusID = 1 /*ASSIGNED*/ AND i.CarrierID IS NOT NULL AND i.DriverID IS NOT NULL AND i.DeleteDateUTC IS NULL

		/*************************************************************************************************************/
		/* handle driver changes - "orphan" the order/ticket (with a new ORDERNUM) - and clone it for the new driver */
		IF (UPDATE(DriverID))
		BEGIN
			-- create cloned order/ticket records (these records will be cloned then the original reset to orphaned status)
			SELECT O.*, NewOrderNum = CAST(NULL as int)
			INTO #clone
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID AND d.DriverID IS NOT NULL AND d.DriverID <> isnull(O.DriverID, 0)
			/* JOIN to tblOrderTransfer so we can prevent treating an OrderTransfer "driver change" as a Orphaned Order */
			LEFT JOIN tblOrderTransfer OTR ON OTR.OrderID = O.ID  /* 3.9.20 - added */
			WHERE OTR.OrderID IS NULL

			/* generate the next OrderNum values for these new records */
			DECLARE @newOrderNum int; SET @newOrderNum = isnull((SELECT max(OrderNum) FROM tblOrder), 100000) + 1;
			DECLARE @newReassignKey int; SET @newReassignKey = isnull((SELECT max(ReassignKey) FROM tblOrder), 0) + 1;
			WHILE (SELECT count(*) FROM #clone WHERE NewOrderNum IS NULL) > 0
			BEGIN
				UPDATE #clone
				  SET NewOrderNum = @newOrderNum, ReassignKey = isnull(ReassignKey, @newReassignKey)
					, CreateDateUTC = getutcdate()
				WHERE ID = (SELECT min(ID) FROM #clone WHERE NewOrderNum IS NULL)
				-- if the @newReassignKey wasn't used above, then that # will be skipped, but this should not be an issue
				SELECT @newOrderNum = @newOrderNum + 1, @newReassignKey = @newReassignKey + 1
			END
			
			-- reset the existing records as orphaned
			UPDATE tblOrder
				SET DeleteDateUTC = isnull(O.DeleteDateUTC, GETUTCDATE()), DeletedByUser = isnull(O.DeletedByUser, O.LastChangedByUser)
					, CarrierID = d.CarrierID, DriverID = d.DriverID, TruckID = d.TruckID, TrailerID = d.TrailerID, Trailer2ID = d.Trailer2ID
					, DispatchConfirmNum = NULL
					, ReassignKey = C.ReassignKey
			FROM tblOrder O
			JOIN #clone C ON C.ID = O.ID
			JOIN deleted d on D.ID = O.id

			-- create the cloned records (which is essentially the original with a new ID and the Carrrier & default Truck|Trailers of the new driver)
			INSERT INTO tblOrder (OrderNum, StatusID, PriorityID, DueDate, RouteID, OriginID, OriginArriveTimeUTC, OriginDepartTimeUTC, OriginMinutes, OriginWaitNotes, OriginBOLNum, OriginGrossUnits, OriginNetUnits, DestinationID, DestArriveTimeUTC, DestDepartTimeUTC, DestMinutes, DestWaitNotes, DestBOLNum, DestGrossUnits, DestNetUnits, CustomerID, CarrierID, DriverID, TruckID, TrailerID, Trailer2ID, OperatorID, PumperID, TicketTypeID, Rejected, RejectNotes, ChainUp, OriginTruckMileage, OriginTankNum, DestTruckMileage, CarrierTicketNum, AuditNotes, CreateDateUTC, ActualMiles, ProducerID, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser, DestProductBSW, DestProductGravity, DestProductTemp, ProductID, AcceptLastChangeDateUTC, PickupLastChangeDateUTC, DeliverLastChangeDateUTC, OriginUomID, DestUomID, PickupPrintStatusID, DeliverPrintStatusID, PickupPrintDateUTC, DeliverPrintDateUTC, OriginTankID, OriginGrossStdUnits, DispatchNotes, PickupDriverNotes, DeliverDriverNotes, DispatchConfirmNum, OriginWaitReasonID, DestWaitReasonID, RejectReasonID, DestOpenMeterUnits, DestCloseMeterUnits, ReassignKey)
				SELECT NewOrderNum, StatusID, PriorityID, DueDate, RouteID, OriginID, OriginArriveTimeUTC, OriginDepartTimeUTC, OriginMinutes, OriginWaitNotes, OriginBOLNum, OriginGrossUnits, OriginNetUnits, DestinationID, DestArriveTimeUTC, DestDepartTimeUTC, DestMinutes, DestWaitNotes, DestBOLNum, DestGrossUnits, DestNetUnits, CustomerID, D.CarrierID, DriverID, D.TruckID, D.TrailerID, D.Trailer2ID, OperatorID, PumperID, TicketTypeID, Rejected, RejectNotes, ChainUp, OriginTruckMileage, OriginTankNum, DestTruckMileage, CarrierTicketNum, AuditNotes, O.CreateDateUTC, ActualMiles, ProducerID, O.CreatedByUser, GETUTCDATE(), O.LastChangedByUser, O.DeleteDateUTC, O.DeletedByUser, DestProductBSW, DestProductGravity, DestProductTemp, ProductID, AcceptLastChangeDateUTC, PickupLastChangeDateUTC, DeliverLastChangeDateUTC, OriginUomID, DestUomID, PickupPrintStatusID, DeliverPrintStatusID, PickupPrintDateUTC, DeliverPrintDateUTC, OriginTankID, OriginGrossStdUnits, DispatchNotes, PickupDriverNotes, DeliverDriverNotes, DispatchConfirmNum, OriginWaitReasonID, DestWaitReasonID, RejectReasonID, DestOpenMeterUnits, DestCloseMeterUnits, ReassignKey
				FROM #clone O
				LEFT JOIN tblDriver D ON D.ID = O.DriverID
		
			-- cache the affected tickets
			SELECT OT.*, C.NewOrderNum, OrderDeletedByUser = C.LastChangedByUser
			INTO #cloneTicket
			FROM tblOrderTicket OT
			JOIN #clone C ON C.ID = OT.OrderID
			WHERE OT.DeleteDateUTC IS NULL
			
			-- create the cloned Order Ticket records (copy of existing ticket(s) pointed at new "original" Order)
			INSERT INTO tblOrderTicket (OrderID, CarrierTicketNum, TicketTypeID, TankNum, ProductObsGravity, ProductObsTemp, ProductBSW, OpeningGaugeFeet, OpeningGaugeInch, OpeningGaugeQ, ClosingGaugeFeet, ClosingGaugeInch, ClosingGaugeQ
					, GrossUnits, NetUnits, Rejected, RejectNotes, SealOff, SealOn, BOLNum, ProductHighTemp, ProductLowTemp, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser
					, UID, FromMobileApp, OriginTankID, BottomFeet, BottomInches, BottomQ, GrossStdUnits, RejectReasonID, MeterFactor, OpenMeterUnits, CloseMeterUnits)
				SELECT O.ID, CT.CarrierTicketNum, CT.TicketTypeID, CT.TankNum, CT.ProductObsGravity, CT.ProductObsTemp, CT.ProductBSW, CT.OpeningGaugeFeet, CT.OpeningGaugeInch, CT.OpeningGaugeQ, CT.ClosingGaugeFeet, CT.ClosingGaugeInch, CT.ClosingGaugeQ
					, CT.GrossUnits, CT.NetUnits, CT.Rejected, CT.RejectNotes, CT.SealOff, CT.SealOn, CT.BOLNum, CT.ProductHighTemp, CT.ProductLowTemp, CT.CreateDateUTC, CT.CreatedByUser, GETUTCDATE(), CT.LastChangedByUser, CT.DeleteDateUTC, CT.DeletedByUser
					, newid(), CT.FromMobileApp, CT.OriginTankID, CT.BottomFeet, CT.BottomInches, CT.BottomQ, CT.GrossStdUnits, CT.RejectReasonID, CT.MeterFactor, CT.OpenMeterUnits, CT.CloseMeterUnits
				FROM #cloneTicket CT
				JOIN tblOrder O ON O.OrderNum = CT.NewOrderNum
		END
		/*************************************************************************************************************/
		
		/* HANDLE TRUCK | TRAILER | TRAILER2 default changes when the driver updates his Truck | Trailer | Trailer2 on ACCEPTANCE */
		-- TRUCK
		IF (UPDATE(TruckID) AND EXISTS (
			SELECT i.* 
			FROM inserted i 
			JOIN deleted d ON d.ID = i.ID
			  AND i.DriverID = d.DriverID 
			  AND i.TruckID <> d.TruckID
			WHERE i.StatusID IN (7, 8, 3) -- ACCEPTED, PICKEDUP, DELIVERED
			  AND d.StatusID IN (2))) -- DISPATCHED
		BEGIN
			UPDATE tblDriver
			  SET TruckID = i.TruckID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblDriver DR
			JOIN inserted i ON i.DriverID = DR.ID
			JOIN deleted d ON d.DriverID = DR.ID
			WHERE i.TruckID <> d.TruckID
			
			UPDATE tblOrder
			  SET TruckID = i.TruckID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblOrder O
			JOIN inserted i ON i.DriverID = O.DriverID 
			  AND O.ID <> i.ID
			  AND O.StatusID IN (2) -- DISPATCHED
			  AND i.TruckID <> O.TruckID
			  AND O.DeleteDateUTC IS NULL
		END
		-- TRAILER
		IF (UPDATE(TrailerID) AND EXISTS (
			SELECT i.* 
			FROM inserted i 
			JOIN deleted d ON d.ID = i.ID
			  AND i.DriverID = d.DriverID 
			  AND i.TrailerID <> d.TrailerID
			WHERE i.StatusID IN (7, 8, 3) -- ACCEPTED, PICKEDUP, DELIVERED
			  AND d.StatusID IN (2))) -- DISPATCHED
		BEGIN
			UPDATE tblDriver
			  SET TrailerID = i.TrailerID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblDriver DR
			JOIN inserted i ON i.DriverID = DR.ID
			JOIN deleted d ON d.DriverID = DR.ID
			WHERE i.TrailerID <> d.TrailerID
			
			UPDATE tblOrder
			  SET TrailerID = i.TrailerID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblOrder O
			JOIN inserted i ON i.DriverID = O.DriverID 
			  AND O.ID <> i.ID
			  AND O.StatusID IN (2) -- DISPATCHED
			  AND i.TrailerID <> O.TrailerID
			  AND O.DeleteDateUTC IS NULL
		END
		-- TRAILER 2
		IF (UPDATE(Trailer2ID) AND EXISTS (
			SELECT i.* 
			FROM inserted i 
			JOIN deleted d ON d.ID = i.ID
			  AND i.DriverID = d.DriverID 
			  AND isnull(i.Trailer2ID, 0) <> isnull(d.Trailer2ID, 0)
			WHERE i.StatusID IN (7, 8, 3) -- ACCEPTED, PICKEDUP, DELIVERED
			  AND d.StatusID IN (2))) -- DISPATCHED
		BEGIN
			UPDATE tblDriver
			  SET TrailerID = i.TrailerID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblDriver DR
			JOIN inserted i ON i.DriverID = DR.ID
			JOIN deleted d ON d.DriverID = DR.ID
			WHERE isnull(i.Trailer2ID, 0) <> isnull(d.Trailer2ID, 0)
			
			UPDATE tblOrder
			  SET Trailer2ID = i.Trailer2ID
				, LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = i.LastChangedByUser
			FROM tblOrder O
			JOIN inserted i ON i.DriverID = O.DriverID 
			  AND O.ID <> i.ID
			  AND O.StatusID IN (2) -- DISPATCHED
			  AND isnull(i.Trailer2ID, 0) <> isnull(O.Trailer2ID, 0)
			  AND O.DeleteDateUTC IS NULL
		END
		/*************************************************************************************************************/
	
		DECLARE @deliveredIDs TABLE (ID int)
		DECLARE @auditedIDs TABLE (ID int)
		DECLARE @id int
		DECLARE @autoAudit bit; SET @autoAudit = dbo.fnToBool(dbo.fnSettingValue(54))
		DECLARE @toAudit bit
		-- Auto-Audit/Apply Settlement Rates/Amounts/Auto-Approve to Order when STATUS is changed to DELIVERED (until it is FINAL settled)
		IF (UPDATE(StatusID) OR UPDATE(DeliverPrintStatusID))
		BEGIN
			INSERT INTO @deliveredIDs (ID)
				SELECT i.ID 
				FROM inserted i
				JOIN tblPrintStatus iPS ON iPS.ID = i.DeliverPrintStatusID
				JOIN deleted d ON d.ID = i.ID
				JOIN tblPrintStatus dPS ON dPS.ID = d.DeliverPrintStatusID
				WHERE i.StatusID = 3 AND d.StatusID <> 4 AND iPS.IsCompleted = 1 AND i.StatusID + iPS.IsCompleted <> d.StatusID + dPS.IsCompleted
			
			SELECT @id = MIN(ID) FROM @deliveredIDs
			WHILE @id IS NOT NULL
			BEGIN
				-- attempt to AUTO-AUDIT the order if this feature is enabled (global setting)
				IF (@autoAudit = 1)
				BEGIN
					EXEC spAutoAuditOrder @ID, 'System', @toAudit OUTPUT
					/* if the order was updated to AUDITED status, then we need to attempt also to auto-approve [below] */
					IF (@toAudit = 1)
						INSERT INTO @auditedIDs VALUES (@id)
				END
				-- attempt to apply rates to all newly DELIVERED orders
				EXEC spApplyRatesBoth @id, 'System' 
				SET @id = (SELECT MIN(id) FROM @deliveredIDs WHERE ID > @id)
			END
		END

		-- Auto-Approve any un-approved orders when STATUS changed to AUDIT
		IF (UPDATE(StatusID) OR EXISTS (SELECT * FROM @auditedIDs))
		BEGIN
			INSERT INTO @auditedIDs (ID)
				SELECT i.ID 
				FROM inserted i
				JOIN deleted d ON d.ID = i.ID
				LEFT JOIN tblOrderApproval OA ON OA.OrderID = i.ID
				LEFT JOIN @auditedIDs AID ON AID.ID = i.ID
				WHERE i.StatusID = 4 AND d.StatusID <> 4 AND OA.OrderID IS NULL AND AID.ID IS NULL

			SELECT @id = MIN(ID) FROM @auditedIDs
			WHILE @id IS NOT NULL
			BEGIN
				EXEC spAutoApproveOrder @id, 'System'
				SET @id = (SELECT MIN(id) FROM @auditedIDs WHERE ID > @id)
			END
			
		END
		
		/* auto UNAPPROVE any orders that have their status reverted from AUDITED */
		IF (UPDATE(StatusID))
		BEGIN
			DELETE FROM tblOrderApproval
			WHERE OrderID IN (
				SELECT i.ID
				FROM inserted i
				JOIN deleted d ON d.ID = i.ID
				JOIN tblOrderApproval OA ON OA.OrderID = i.ID
				WHERE d.StatusID = 4 AND i.StatusID <> 4
			)
		END
		
		-- purge any DriverApp/Gauger App sync records for any orders that are not in AUDITED status
		IF UPDATE(StatusID) AND EXISTS (SELECT ID FROM inserted WHERE StatusID IN (4))
		BEGIN
			-- DRIVER APP records
			DELETE FROM tblDriverAppPrintHeaderImageSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID IN (4))
			DELETE FROM tblDriverAppPrintPickupTemplateSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID IN (4))
			DELETE FROM tblDriverAppPrintDeliverTemplateSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID IN (4))
			-- GAUGER APP records
			DELETE FROM tblGaugerAppPrintHeaderImageSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID IN (4))
			DELETE FROM tblGaugerAppPrintPickupTemplateSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID IN (4))
			DELETE FROM tblGaugerAppPrintTicketTemplateSync WHERE OrderID IN (SELECT ID FROM inserted WHERE StatusID IN (4))
		END
				
		--NOTE: we do not update the DestOpenMeterUnits/DestCloseMeterUnits since they don't auto update
		--  (and they simply represent a reading that is assumed to in the DestUomID UOM at entry)
		--  (if they are updated in the future [manually] the will calculate again using the DestUomID at that time)

		IF EXISTS (SELECT * FROM inserted EXCEPT SELECT * FROM deleted)
		BEGIN
			-- optionally add tblOrderDBAudit records
			BEGIN TRY
				IF EXISTS(SELECT * FROM tblSetting WHERE ID = 32 AND (Value LIKE 'true' OR Value LIKE 'yes'))
					INSERT INTO tblOrderDbAudit (DBAuditDate, ID, OrderNum, StatusID, PriorityID, DueDate, RouteID, OriginID, OriginArriveTimeUTC, OriginDepartTimeUTC, OriginMinutes, OriginWaitNotes, OriginBOLNum, OriginGrossUnits, OriginNetUnits, DestinationID, DestArriveTimeUTC, DestDepartTimeUTC, DestMinutes, DestWaitNotes, DestBOLNum, DestGrossUnits, DestNetUnits, CustomerID, CarrierID, DriverID, TruckID, TrailerID, Trailer2ID, OperatorID, PumperID, TicketTypeID, Rejected, RejectNotes, ChainUp, OriginTruckMileage, OriginTankNum, DestTruckMileage, CarrierTicketNum, AuditNotes, CreateDateUTC, ActualMiles, ProducerID, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser, DestProductBSW, DestProductGravity, DestProductTemp, ProductID, AcceptLastChangeDateUTC, PickupLastChangeDateUTC, DeliverLastChangeDateUTC, OriginUomID, DestUomID, PickupPrintStatusID, DeliverPrintStatusID, PickupPrintDateUTC, DeliverPrintDateUTC, OriginTankID, OriginGrossStdUnits, DispatchNotes, PickupDriverNotes, DeliverDriverNotes, DispatchConfirmNum, OriginWaitReasonID, DestWaitReasonID, RejectReasonID, DestOpenMeterUnits, DestCloseMeterUnits, ReassignKey, DestRackBay, OrderDate)
						SELECT GETUTCDATE(), ID, OrderNum, StatusID, PriorityID, DueDate, RouteID, OriginID, OriginArriveTimeUTC, OriginDepartTimeUTC, OriginMinutes, OriginWaitNotes, OriginBOLNum, OriginGrossUnits, OriginNetUnits, DestinationID, DestArriveTimeUTC, DestDepartTimeUTC, DestMinutes, DestWaitNotes, DestBOLNum, DestGrossUnits, DestNetUnits, CustomerID, CarrierID, DriverID, TruckID, TrailerID, Trailer2ID, OperatorID, PumperID, TicketTypeID, Rejected, RejectNotes, ChainUp, OriginTruckMileage, OriginTankNum, DestTruckMileage, CarrierTicketNum, AuditNotes, CreateDateUTC, ActualMiles, ProducerID, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser, DestProductBSW, DestProductGravity, DestProductTemp, ProductID, AcceptLastChangeDateUTC, PickupLastChangeDateUTC, DeliverLastChangeDateUTC, OriginUomID, DestUomID, PickupPrintStatusID, DeliverPrintStatusID, PickupPrintDateUTC, DeliverPrintDateUTC, OriginTankID, OriginGrossStdUnits, DispatchNotes, PickupDriverNotes, DeliverDriverNotes, DispatchConfirmNum, OriginWaitReasonID, DestWaitReasonID, RejectReasonID, DestOpenMeterUnits, DestCloseMeterUnits, ReassignKey, DestRackBay, OrderDate
						FROM deleted d
			END TRY
			BEGIN CATCH
				PRINT 'trigOrder_IU.DBAUDIT FAILURE: ' + ERROR_MESSAGE()
			END CATCH
		END
		
	PRINT 'trigOrder_IU COMPLETE'

	END
	
END

GO
EXEC sp_settriggerorder @triggername=N'[trigOrder_IU]', @order=N'First', @stmttype=N'INSERT'
GO
EXEC sp_settriggerorder @triggername=N'[trigOrder_IU]', @order=N'First', @stmttype=N'UPDATE'
GO


EXEC _spDropProcedure 'spOrderTransfer'
GO
/************************************************
-- Author:		Kevin Alons
-- Create date: 2015/09/23
-- Description:	procedure to accomplish Order Transfer (driver) process
-- Changes:
--		JAE 11/4/2015 - "Touch" truck record to ensure the mobile app gets updated odometer reading. 
--                         Mobile app keys off last modify date for refreshing truck info
-- 		JAE 11/5/2015 - "Touch" order record to ensure mobile app gets the updated transfer order
-- 					  -  Mobile app keys off last modify date for refreshing order info
************************************************/
CREATE PROCEDURE spOrderTransfer
(
  @OrderID int
, @DestDriverID int
, @OriginTruckEndMileage int
, @PercentComplete int
, @Notes Text
, @UserName varchar(100)
, @DestTruckID int = NULL
) AS
BEGIN
	INSERT INTO tblOrderTransfer(OrderID, OriginDriverID, OriginTruckID, OriginTruckEndMileage, PercentComplete, Notes, CreateDateUTC, CreatedByUser, TransferComplete)
		SELECT ID, DriverID, TruckID, @OriginTruckEndMileage, @PercentComplete, @Notes, GETUTCDATE(), @UserName, 0
		FROM tblOrder 
		WHERE ID = @OrderID
	UPDATE tblOrder SET DriverID = @DestDriverID, TruckID = ISNULL(@DestTruckID, TruckID), 
			LastChangedByUser = @UserName, LastChangeDateUTC = GETUTCDATE() 
		WHERE ID = @OrderID
	UPDATE tblTruck SET LastChangedByUser = @UserName, LastChangeDateUTC = GETUTCDATE()
		WHERE ID = (SELECT OriginTruckID FROM tblOrderTransfer WHERE OrderID = @OrderID)
END
GO
GRANT EXECUTE ON spOrderTransfer TO role_iis_acct
GO



/***********************************
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Order records with "translated friendly" values for FK relationships
-- Changes:
	- 3.9.20 - 2015/10/22 - KDA - add Origin|Dest DriverID fields (using new tblOrderTransfer table) 
			 - 2015/10/28 - JAE - Added all Order Transfer fields for ease of use in reporting
			 - 2015/11/03 - BB  - added cast to make TransferComplet BIT type to avoid "Operand type clash" error when running this update script
***********************************/
ALTER VIEW viewOrder AS
SELECT O.*
	-- GENERATED orders that were GAUGED to COMPLETION or SKIPPED will show "Gauger [gauger status]"
	, PrintStatus = CASE WHEN O.StatusID = -10 AND GOS.IsComplete = 1 THEN 'Gauger ' + GOS.Name ELSE OPS.OrderStatus END
FROM (
	SELECT O.*
	, Origin = vO.Name 
	, OriginFull = vO.FullName 
	, OriginState = vO.State 
	, OriginStateAbbrev = vO.StateAbbrev 
	, OriginStation = vO.Station
	, OriginCounty = vO.County 
	, vO.LeaseName
	, vO.LeaseNum
	, OriginRegion = vO.Region
	, OriginCountryID = vO.CountryID
	, OriginCountry = vO.Country
	, OriginCountryShort = vO.CountryShort
	, OriginLegalDescription = vO.LegalDescription 
	, OriginNDIC = vO.NDICFileNum 
	, OriginNDM = vO.NDM 
	, OriginCA = vO.CA 
	, OriginTimeZoneID = vO.TimeZoneID 
	, OriginUseDST = vO.UseDST 
	, vO.H2S
	, Destination = vD.Name 
	, DestinationFull = vD.FullName 
	, DestinationState = vD.State 
	, DestinationStateAbbrev = vD.StateAbbrev 
	, DestinationTypeID = vD.ID
	, vD.DestinationType
	, DestStation = vD.Station 
	, DestTimeZoneID = vD.TimeZoneID 
	, DestUseDST = vD.UseDST 
	, DestCountryID = vD.CountryID
	, DestCountry = vD.Country
	, DestCountryShort = vD.CountryShort
	, Customer = C.Name 
	, Carrier = CA.Name 
	, CarrierType = CT.Name 
	, OS.OrderStatus
	, OS.StatusNum
	, Driver = D.FullName 
	, DriverFirst = D.FirstName 
	, DriverLast = D.LastName 
	, Truck = TRU.FullName 
	, Trailer = TR1.FullName 
	, Trailer2 = TR2.FullName 
	, P.PriorityNum
	, TicketType = TT.Name 
	, DestTicketTypeID = vD.TicketTypeID 
	, DestTicketType = vD.TicketType 
	, Operator = OP.Name 
	, Producer = PR.Name 
	, Pumper = PU.FullName 
	, DriverNumber = D.IDNumber
	, CarrierNumber = CA.IDNumber
	, CarrierTypeID = CA.CarrierTypeID
	, TruckNumber = TRU.IDNumber 
	, TrailerNumber = TR1.IDNumber 
	, Trailer2Number = TR2.IDNumber 
	, Product = PRO.Name
	, ProductShort = PRO.ShortName 
	, PRO.ProductGroup
	, OriginUOM = OUom.Name 
	, OriginUomShort = OUom.Abbrev 
	, OriginTankID_Text = CASE WHEN ORT.TankNum = '*' THEN '*' + isnull(O.OriginTankNum, '?') ELSE ORT.TankNum END 
	, DestUOM = DUom.Name 
	, DestUomShort = DUom.Abbrev 
	, Active = CAST((CASE WHEN O.DeleteDateUTC IS NULL THEN 1 ELSE 0 END) AS BIT) 
	, IsDeleted = CAST((CASE WHEN O.DeleteDateUTC IS NOT NULL THEN 1 ELSE 0 END) AS BIT) 
	, PickupPrintStatus = PPS.Name 
	, PickupCompleted = PPS.IsCompleted 
	, DeliverPrintStatus = DPS.Name 
	, DeliverCompleted = DPS.IsCompleted 
	, PrintStatusID = CASE WHEN O.StatusID = -9 THEN ISNULL(-GAO.StatusID, O.StatusID)
						   WHEN O.StatusID = 8 AND PPS.IsCompleted = 0 THEN 7
						   WHEN O.StatusID = 3 AND DPS.IsCompleted = 0 THEN 8
						ELSE O.StatusID END 
	, OriginTankText = CASE WHEN O.OriginTankID IS NULL OR ORT.TankNum = '*' THEN O.OriginTankNum ELSE ORT.TankNum END
	, OriginWaitNum = OWR.Num
	, OriginWaitDesc = OWR.Description
	, OriginWaitNumDesc = OWR.NumDesc
	, DestWaitNum = DWR.Num
	, DestWaitDesc = DWR.Description
	, DestWaitNumDesc = DWR.NumDesc
	, RejectNum = ORR.Num
	, RejectDesc = ORR.Description
	, RejectNumDesc = ORR.NumDesc
	, RerouteCount = (SELECT COUNT(1) FROM tblOrderReroute ORE WHERE ORE.OrderID = O.ID)
	, TicketCount = (SELECT COUNT(1) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND OT.DeleteDateUTC IS NULL)
	, TotalMinutes = ISNULL(OriginMinutes, 0) + ISNULL(DestMinutes, 0)
	, OriginDriverID = ISNULL(OTR.OriginDriverID, O.DriverID)
	, DestDriverID = O.DriverID
	, OriginTruckID = ISNULL(OTR.OriginTruckID, O.TruckID)
	, DestTruckID = O.TruckID
	, OriginTruckEndMileage = OTR.OriginTruckEndMileage
	, DestTruckStartMileage = OTR.DestTruckStartMileage
	, TransferPercent = OTR.PercentComplete
	, TransferNotes = OTR.Notes
	, TransferDateUTC = OTR.CreateDateUTC
	, TransferComplete = CAST(OTR.TransferComplete AS BIT)
	, IsTransfer = CAST((CASE WHEN OTR.OrderID IS NOT NULL THEN 1 ELSE 0 END) AS BIT)	
	, GaugerStatusID = GAO.StatusID
	, GaugerID = GAO.GaugerID
	FROM dbo.viewOrderBase O
	JOIN dbo.tblOrderStatus AS OS ON OS.ID = O.StatusID
	LEFT JOIN dbo.viewOrigin vO ON vO.ID = O.OriginID
	LEFT JOIN dbo.viewDestination vD ON vD.ID = O.DestinationID
	LEFT JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
	LEFT JOIN dbo.tblCarrier CA ON CA.ID = O.CarrierID
	LEFT JOIN dbo.tblCarrierType CT ON CT.ID = CA.CarrierTypeID
	LEFT JOIN dbo.viewDriverBase D ON D.ID = O.DriverID
	LEFT JOIN dbo.viewTruck TRU ON TRU.ID = O.TruckID
	LEFT JOIN dbo.viewTrailer TR1 ON TR1.ID = O.TrailerID
	LEFT JOIN dbo.viewTrailer TR2 ON TR2.ID = O.Trailer2ID
	LEFT JOIN dbo.tblPriority P ON P.ID = O.PriorityID
	LEFT JOIN dbo.tblTicketType TT ON TT.ID = O.TicketTypeID
	LEFT JOIN dbo.tblOriginTank ORT ON ORT.ID = O.OriginTankID
	LEFT JOIN dbo.tblOperator OP ON OP.ID = O.OperatorID
	LEFT JOIN dbo.viewPumper PU ON PU.ID = O.PumperID
	LEFT JOIN dbo.tblProducer PR ON PR.ID = O.ProducerID
	LEFT JOIN dbo.viewProduct PRO ON PRO.ID = O.ProductID
	LEFT JOIN dbo.tblUom OUom ON OUom.ID = O.OriginUomID
	LEFT JOIN dbo.tblUom DUom ON DUom.ID = O.DestUomID
	LEFT JOIN dbo.tblPrintStatus PPS ON PPS.ID = O.PickupPrintStatusID
	LEFT JOIN dbo.tblPrintStatus DPS ON DPS.ID = O.DeliverPrintStatusID
	LEFT JOIN dbo.viewOriginWaitReason OWR ON OWR.ID = O.OriginWaitReasonID
	LEFT JOIN dbo.viewDestinationWaitReason DWR ON DWR.ID = O.DestWaitReasonID
	LEFT JOIN dbo.viewOrderRejectReason ORR ON ORR.ID = O.RejectReasonID
	LEFT JOIN dbo.tblOrderTransfer OTR ON OTR.OrderID = O.ID
	LEFT JOIN dbo.tblGaugerOrder GAO ON GAO.OrderID = O.ID
	LEFT JOIN dbo.viewGaugerBase G ON G.ID = GAO.GaugerID
) O
LEFT JOIN dbo.viewOrderPrintStatus OPS ON OPS.ID = O.PrintStatusID
LEFT JOIN dbo.tblGaugerOrderStatus GOS ON GOS.ID = O.GaugerStatusID
GO

EXEC sp_refreshview viewOrder
GO
EXEC sp_refreshview viewOrderLocalDates
GO
EXEC sp_refreshview viewOrder_OrderTicket_Full
GO

/***********************************
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Report Center Order data
-- Changes:
   -		 - 05/21/15	 - GSM - Adding Gauger Process data elements to Report Center
   -		 - 06/08/15	 - BB  - Add "OriginCity" and "OriginCityState" columns			
   -		 - 06/17/15  - BB  - Add "DestCity" and "DestCityState" columns
   - 3.7.39 - 2015/06/30 - BB  - Add "Shipper Ticket Type" column
   - 3.8.20 - 2015/08/26 - BB  - Add Order Create Date column
   - 3.9.14 - 2015/09/08 - BB  - Add Order Approved column
   - 3.9.20 - 2015/10/22 - BB  - Add Origin|Dest Driver fields
						 - JAE - Added Origin Truck field 
***********************************/
ALTER VIEW viewReportCenter_Orders AS
	SELECT O.*
		, OriginDriver = OD.FullName
		, DestDriver = O.Driver
		, OriginTruck = OT.FullName
		, DestTruck = O.Truck
		, ShipperBatchNum = SS.BatchNum
		, ShipperBatchInvoiceNum = SSB.InvoiceNum
		, ShipperSettlementUomID = SS.SettlementUomID
		, ShipperSettlementUom = SS.SettlementUom
		, ShipperMinSettlementUnits = SS.MinSettlementUnits
		, ShipperSettlementUnits = SS.SettlementUnits
		, ShipperRateSheetRate = SS.RateSheetRate
		, ShipperRateSheetRateType = SS.RateSheetRateType
		, ShipperRouteRate = SS.RouteRate
		, ShipperRouteRateType = SS.RouteRateType
		, ShipperLoadRate = ISNULL(SS.RouteRate, SS.RateSheetRate)
		, ShipperLoadRateType = ISNULL(SS.RouteRateType, SS.RateSheetRateType)
		, ShipperLoadAmount = SS.LoadAmount
		, ShipperOrderRejectRate = SS.OrderRejectRate 
		, ShipperOrderRejectRateType = SS.OrderRejectRateType 
		, ShipperOrderRejectAmount = SS.OrderRejectAmount 
		, ShipperWaitFeeSubUnit = SS.WaitFeeSubUnit  
		, ShipperWaitFeeRoundingType = SS.WaitFeeRoundingType  
		, ShipperOriginWaitBillableHours = SS.OriginWaitBillableHours  
		, ShipperOriginWaitBillableMinutes = SS.OriginWaitBillableMinutes  
		, ShipperOriginWaitRate = SS.OriginWaitRate
		, ShipperOriginWaitAmount = SS.OriginWaitAmount
		, ShipperDestinationWaitBillableHours = SS.DestinationWaitBillableHours 
		, ShipperDestinationWaitBillableMinutes = SS.DestinationWaitBillableMinutes   
		, ShipperDestinationWaitRate = SS.DestinationWaitRate  
		, ShipperDestinationWaitAmount = SS.DestinationWaitAmount  
		, ShipperTotalWaitAmount = SS.TotalWaitAmount
		, ShipperTotalWaitBillableMinutes = NULLIF(ISNULL(SS.OriginWaitBillableMinutes, 0) + ISNULL(SS.DestinationWaitBillableMinutes, 0), 0)
		, ShipperTotalWaitBillableHours = NULLIF(ISNULL(SS.OriginWaitBillableHours, 0) + ISNULL(SS.DestinationWaitBillableHours, 0), 0)		
		, ShipperFuelSurchargeRate = SS.FuelSurchargeRate
		, ShipperFuelSurchargeAmount = SS.FuelSurchargeAmount
		, ShipperChainupRate = SS.ChainupRate
		, ShipperChainupRateType = SS.ChainupRateType  
		, ShipperChainupAmount = SS.ChainupAmount
		, ShipperRerouteRate = SS.RerouteRate
		, ShipperRerouteRateType = SS.RerouteRateType  
		, ShipperRerouteAmount = SS.RerouteAmount
		, ShipperSplitLoadRate = SS.SplitLoadRate
		, ShipperSplitLoadRateType = SS.SplitLoadRateType  
		, ShipperSplitLoadAmount = SS.SplitLoadAmount
		, ShipperH2SRate = SS.H2SRate
		, ShipperH2SRateType = SS.H2SRateType  
		, ShipperH2SAmount = SS.H2SAmount
		, ShipperTaxRate = SS.OriginTaxRate
		, ShipperTotalAmount = SS.TotalAmount
		, ShipperDestCode = CDC.Code
		, ShipperTicketType = STT.TicketType
		--
		, CarrierBatchNum = SC.BatchNum
		, CarrierSettlementUomID = SC.SettlementUomID
		, CarrierSettlementUom = SC.SettlementUom
		, CarrierMinSettlementUnits = SC.MinSettlementUnits
		, CarrierSettlementUnits = SC.SettlementUnits
		, CarrierRateSheetRate = SC.RateSheetRate
		, CarrierRateSheetRateType = SC.RateSheetRateType
		, CarrierRouteRate = SC.RouteRate
		, CarrierRouteRateType = SC.RouteRateType
		, CarrierLoadRate = ISNULL(SC.RouteRate, SC.RateSheetRate)
		, CarrierLoadRateType = ISNULL(SC.RouteRateType, SC.RateSheetRateType)
		, CarrierLoadAmount = SC.LoadAmount
		, CarrierOrderRejectRate = SC.OrderRejectRate 
		, CarrierOrderRejectRateType = SC.OrderRejectRateType 
		, CarrierOrderRejectAmount = SC.OrderRejectAmount 
		, CarrierWaitFeeSubUnit = SC.WaitFeeSubUnit  
		, CarrierWaitFeeRoundingType = SC.WaitFeeRoundingType  
		, CarrierOriginWaitBillableHours = SS.OriginWaitBillableHours  
		, CarrierOriginWaitBillableMinutes = SC.OriginWaitBillableMinutes  
		, CarrierOriginWaitRate = SC.OriginWaitRate
		, CarrierOriginWaitAmount = SC.OriginWaitAmount
		, CarrierDestinationWaitBillableHours = SS.DestinationWaitBillableHours 
		, CarrierDestinationWaitBillableMinutes = SC.DestinationWaitBillableMinutes  
		, CarrierDestinationWaitRate = SC.DestinationWaitRate 
		, CarrierDestinationWaitAmount = SC.DestinationWaitAmount  
		, CarrierTotalWaitAmount = SC.TotalWaitAmount
		, CarrierTotalWaitBillableMinutes = NULLIF(ISNULL(SS.OriginWaitBillableMinutes, 0) + ISNULL(SS.DestinationWaitBillableMinutes, 0), 0)
		, CarrierTotalWaitBillableHours = NULLIF(ISNULL(SS.OriginWaitBillableHours, 0) + ISNULL(SS.DestinationWaitBillableHours, 0), 0)		
		, CarrierFuelSurchargeRate = SC.FuelSurchargeRate
		, CarrierFuelSurchargeAmount = SC.FuelSurchargeAmount
		, CarrierChainupRate = SC.ChainupRate
		, CarrierChainupRateType = SC.ChainupRateType  
		, CarrierChainupAmount = SC.ChainupAmount
		, CarrierRerouteRate = SC.RerouteRate
		, CarrierRerouteRateType = SC.RerouteRateType  
		, CarrierRerouteAmount = SC.RerouteAmount
		, CarrierSplitLoadRate = SC.SplitLoadRate
		, CarrierSplitLoadRateType = SC.SplitLoadRateType  
		, CarrierSplitLoadAmount = SC.SplitLoadAmount
		, CarrierH2SRate = SC.H2SRate
		, CarrierH2SRateType = SC.H2SRateType  
		, CarrierH2SAmount = SC.H2SAmount
		, CarrierTaxRate = SC.OriginTaxRate
		, CarrierTotalAmount = SC.TotalAmount
		--
		, OriginGpsLatLon = LTRIM(DLO.Lat) + ',' + LTRIM(DLO.Lon)
		, OriginLatLon = LTRIM(OO.LAT) + ',' + LTRIM(OO.LON)
		, OriginAccuracyMeters = DLO.SourceAccuracyMeters
		, OriginDistance = ISNULL(cast(DLO.DistanceToPoint AS INT), 99999)
		, OriginGpsArrived = CASE WHEN DLO.DistanceToPoint IS NOT NULL AND DLO.DistanceToPoint BETWEEN 0 AND OO.GeoFenceRadiusMeters THEN 1 ELSE 0 END
		, OriginCTBNum = OO.CTBNum
		, OriginFieldName = OO.FieldName
		, OriginCity = OO.City																				
		, OriginCityState = OO.City + ', ' + (SELECT Abbreviation FROM tblState WHERE ID = OO.StateID)		
		--
		, DestGpsLatLon = LTRIM(DLD.Lat) + ',' + LTRIM(DLD.Lon)
		, DestLatLon = LTRIM(D.LAT) + ',' + LTRIM(D.LON)
		, DestAccuracyMeters = DLD.SourceAccuracyMeters
		, DestDistance = ISNULL(CAST(DLD.DistanceToPoint AS INT), 99999)
		, DestGpsArrived = CASE WHEN DLD.DistanceToPoint IS NOT NULL AND DLD.DistanceToPoint BETWEEN 0 AND D.GeoFenceRadiusMeters THEN 1 ELSE 0 END		
		, DestCity = D.City
		, DestCityState = D.City + ', ' + (SELECT Abbreviation FROM tblState WHERE ID = OO.StateID)
		--
		, Gauger = GAO.Gauger						
		, GaugerIDNumber = GA.IDNumber
		, GaugerFirstName = GA.FirstName
		, GaugerLastName = GA.LastName
		, GaugerRejected = GAO.Rejected
		, GaugerRejectReasonID = GAO.RejectReasonID
		, GaugerRejectNotes = GAO.RejectNotes
		, GaugerRejectNumDesc = GORR.NumDesc
		, GaugerPrintDate = dbo.fnUTC_to_Local(GAO.PrintDateUTC, O.OriginTimeZoneID, O.OriginUseDST)
		--
		, T_GaugerCarrierTicketNum = CASE WHEN GAO.TicketCount = 0 THEN LTRIM(GAO.OrderNum) + CASE WHEN GAO.Rejected = 1 THEN 'X' ELSE '' END ELSE GOT.CarrierTicketNum END 
		, T_GaugerTankNum = ISNULL(GOT.OriginTankText, GAO.OriginTankText)
		, T_GaugerIsStrappedTank = GOT.IsStrappedTank 
		, T_GaugerProductObsTemp = GOT.ProductObsTemp
		, T_GaugerProductObsGravity = GOT.ProductObsGravity
		, T_GaugerProductBSW = GOT.ProductBSW		
		, T_GaugerOpeningGaugeFeet = GOT.OpeningGaugeFeet
		, T_GaugerOpeningGaugeInch = GOT.OpeningGaugeInch		
		, T_GaugerOpeningGaugeQ = GOT.OpeningGaugeQ			
		, T_GaugerBottomFeet = GOT.BottomFeet
		, T_GaugerBottomInches = GOT.BottomInches		
		, T_GaugerBottomQ = GOT.BottomQ		
		, T_GaugerRejected = GOT.Rejected
		, T_GaugerRejectReasonID = GOT.RejectReasonID
		, T_GaugerRejectNumDesc = GOT.RejectNumDesc
		, T_GaugerRejectNotes = GOT.RejectNotes	
		--
		, OrderCreateDate = dbo.fnUTC_to_Local(O.CreateDateUTC, O.OriginTimeZoneID, O.OriginUseDST)
		, OrderApproved = CAST(ISNULL(OA.Approved,0) AS BIT)
	FROM viewOrder_OrderTicket_Full O
	JOIN tblOrigin OO ON OO.ID = O.OriginID
	JOIN tblDestination D ON D.ID = O.DestinationID
	--
	LEFT JOIN viewDriver OD ON OD.ID = O.OriginDriverID
	LEFT JOIN viewTruck OT ON OT.ID = O.OriginTruckID
	--
    LEFT JOIN viewGaugerOrder GAO ON GAO.OrderID = O.ID			            
    LEFT JOIN viewGaugerOrderTicket GOT ON GOT.UID = O.T_UID	            
    LEFT JOIN viewGauger GA ON GA.ID = GAO.GaugerID				            
    LEFT JOIN viewOrderRejectReason GORR ON GORR.ID = GAO.RejectReasonID 
    --
    LEFT JOIN tblCustomerDestinationCode CDC ON CDC.CustomerID = O.CustomerID AND CDC.DestinationID = O.DestinationID
	LEFT JOIN viewDriverLocation_OriginFirstArrive DLO ON DLO.OrderID = O.ID AND DLO.OriginID = O.OriginID
	LEFT JOIN viewDriverLocation_DestinationFirstArrive DLD ON DLD.OrderID = O.ID AND DLD.DestinationID = O.DestinationID
	LEFT JOIN viewOrderSettlementCarrier SC ON SC.OrderID = O.ID
	LEFT JOIN viewOrderSettlementShipper SS ON SS.OrderID = O.ID
	LEFT JOIN tblShipperSettlementBatch SSB ON SSB.ID = SS.BatchID
	LEFT JOIN tblShipperTicketType AS STT ON STT.CustomerID = O.CustomerID AND STT.TicketTypeID = O.TicketTypeID
	--
	LEFT JOIN tblOrderApproval AS OA ON OA.OrderID = O.ID
GO

EXEC _spRefreshAllViews
GO

/*******************************************
-- Date Created: 2013/04/25
-- Author: Kevin Alons
-- Purpose: return readonly Order data for Driver App sync
-- Changes:
	- 3.8.1 - 2015/07/02 - KDA - add new split DriverApp printing capability (HeaderImageLeft|Top|Width|Height & Pickup|Ticket|Deliver TemplateText)
	- 3.9.19 - 2015/07/23 - KDA - remove TicketTemplate column (it will be provided as a separate TicketTemplates sync table)
	- 3.9.20 - 2015/10/22 - KDA - return OriginDriver | DestDriver fields (from OrderTransfer logic)
*******************************************/
ALTER FUNCTION fnOrderReadOnly_DriverApp(@DriverID INT, @LastChangeDateUTC DATETIME) RETURNS TABLE AS
RETURN 
	WITH cteBase AS
	(
		SELECT O.ID
			, O.OrderNum
			, O.StatusID
			, O.TicketTypeID
			, PriorityNum = CAST(P.PriorityNum AS INT) 
			, Product = PRO.Name
			, O.DueDate
			, Origin = OO.Name
			, OriginFull = OO.FullName
			, OO.OriginType
			, O.OriginUomID
			, OriginStation = OO.Station 
			, OriginLeaseNum = OO.LeaseNum 
			, OriginCounty = OO.County 
			, OriginLegalDescription = OO.LegalDescription 
			, OriginNDIC = OO.NDICFileNum
			, OriginNDM = OO.NDM
			, OriginCA = OO.CA
			, OriginState = OO.State
			, OriginAPI = OO.WellAPI 
			, OriginLat = OO.LAT 
			, OriginLon = OO.LON 
			, OriginGeoFenceRadiusMeters = OO.GeoFenceRadiusMeters 
			, Destination = D.Name
			, DestinationFull = D.FullName
			, DestType = D.DestinationType 
			, O.DestUomID
			, DestLat = D.LAT 
			, DestLon = D.LON 
			, DestGeoFenceRadiusMeters = D.GeoFenceRadiusMeters 
			, DestinationStation = D.Station 
			, O.CreateDateUTC
			, O.CreatedByUser
			, O.LastChangeDateUTC
			, O.LastChangedByUser
			, DeleteDateUTC = ISNULL(ODAVD.VirtualDeleteDateUTC, O.DeleteDateUTC) 
			, DeletedByUser = ISNULL(ODAVD.VirtualDeletedByUser, O.DeletedByUser) 
			, O.OriginID
			, O.DestinationID
			, PriorityID = CAST(O.PriorityID AS INT) 
			, Operator = OO.Operator
			, O.OperatorID
			, Pumper = OO.Pumper
			, O.PumperID
			, Producer = OO.Producer
			, O.ProducerID
			, Customer = C.Name
			, O.CustomerID
			, Carrier = CA.Name
			, O.CarrierID
			, O.ProductID
			, TicketType = OO.TicketType
			, EmergencyInfo = ISNULL(C.EmergencyInfo, 'For an emergency (spill, leak, fire, or accident) contact CHEMTREC @ 800.424.9300 (toll free)') 
			, DestTicketTypeID = D.TicketTypeID
			, DestTicketType = D.TicketType
			, O.OriginTankNum
			, O.OriginTankID
			, O.DispatchNotes
			, O.DispatchConfirmNum
			, RouteActualMiles = ISNULL(R.ActualMiles, 0)
			, CarrierAuthority = CA.Authority 
			, OriginTimeZone = OO.TimeZone
			, DestTimeZone = D.TimeZone
			, OCTM.OriginThresholdMinutes
			, OCTM.DestThresholdMinutes
			, ShipperHelpDeskPhone = C.HelpDeskPhone
			, OriginDrivingDirections = OO.DrivingDirections
			, LCD.LCD
			, CustomerLastChangeDateUTC = C.LastChangeDateUTC 
			, CarrierLastChangeDateUTC = CA.LastChangeDateUTC 
			, OriginLastChangeDateUTC = OO.LastChangeDateUTC 
			, DestLastChangeDateUTC = D.LastChangeDateUTC
			, RouteLastChangeDateUTC = R.LastChangeDateUTC
			, DriverID = @DriverID
			, OriginDriver = OD.FullName
			, OriginTruckID = ISNULL(OTR.OriginTruckID, O.TruckID)
			, DestDriver = DD.FullName
		FROM dbo.tblOrder O
		JOIN dbo.tblPriority P ON P.ID = O.PriorityID
		JOIN dbo.viewOrigin OO ON OO.ID = O.OriginID
		JOIN dbo.viewDestination D ON D.ID = O.DestinationID
		JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
		JOIN dbo.tblRoute R ON R.ID = O.RouteID
		JOIN dbo.tblCarrier CA ON CA.ID = O.CarrierID
		JOIN dbo.tblProduct PRO ON PRO.ID = O.ProductID
		LEFT JOIN dbo.tblOrderTransfer OTR ON OTR.OrderID = O.ID
		LEFT JOIN dbo.viewDriver OD ON OD.ID = ISNULL(OTR.OriginDriverID, O.DriverID)
		LEFT JOIN dbo.viewDriver DD ON DD.ID = O.DriverID
		OUTER APPLY dbo.fnOrderBestMatchDriverAppPrintHeaderImage(O.ID) DAHI
		LEFT JOIN tblOrderDriverAppVirtualDelete ODAVD ON ODAVD.OrderID = O.ID AND ODAVD.DriverID = @DriverID
		CROSS JOIN (SELECT DATEADD(SECOND, -5, @LastChangeDateUTC) AS LCD) LCD
		OUTER APPLY dbo.fnOrderCombinedThresholdMinutes(O.ID) OCTM
		WHERE O.ID IN (
			SELECT id FROM tblOrder WHERE DriverID = @driverID AND StatusID IN (2,7,8,3) 
			UNION 
			SELECT OrderID FROM tblOrderDriverAppVirtualDelete WHERE DriverID = @driverID
		)
	)

	SELECT O.*
		, HeaderImageID = DAHI.ID
		, PrintHeaderBlob = DAHI.ImageBlob
		, HeaderImageLeft = DAHI.ImageLeft
		, HeaderImageTop = DAHI.ImageTop
		, HeaderImageWidth = DAHI.ImageWidth
		, HeaderImageHeight = DAHI.ImageHeight
		, PickupTemplateID = DAPT.ID
		, PickupTemplateText = DAPT.TemplateText
		, DeliverTemplateID = DADT.ID
		, DeliverTemplateText = DADT.TemplateText
	FROM cteBase O
	OUTER APPLY dbo.fnOrderBestMatchDriverAppPrintHeaderImage(O.ID) DAHI
	LEFT JOIN tblDriverAppPrintHeaderImageSync DAHIS ON DAHIS.OrderID = O.ID AND DAHIS.DriverID = O.DriverID AND DAHIS.RecordID <> DAHI.ID
	OUTER APPLY dbo.fnOrderBestMatchDriverAppPrintPickupTemplate(O.ID) DAPT
	LEFT JOIN tblDriverAppPrintPickupTemplateSync DAPTS ON DAPTS.OrderID = O.ID AND DAPTS.DriverID = O.DriverID AND DAPTS.RecordID <> DAPT.ID
	OUTER APPLY dbo.fnOrderBestMatchDriverAppPrintDeliverTemplate(O.ID) DADT
	LEFT JOIN tblDriverAppPrintDeliverTemplateSync DADTS ON DADTS.OrderID = O.ID AND DADTS.DriverID = O.DriverID AND DADTS.RecordID <> DADT.ID
	WHERE O.ID IN (
		SELECT ID FROM tblOrder X WHERE DriverID = @DriverID AND StatusID IN (2,7,8,3) 
		UNION 
		SELECT OrderID FROM tblOrderDriverAppVirtualDelete WHERE DriverID = @driverID
	)
	  AND (
		@LastChangeDateUTC IS NULL 
		OR CreateDateUTC >= LCD
		OR O.LastChangeDateUTC >= LCD
		OR O.DeleteDateUTC >= LCD
		OR CustomerLastChangeDateUTC >= LCD
		OR CarrierLastChangeDateUTC >= LCD
		OR OriginLastChangeDateUTC >= LCD
		OR DestLastChangeDateUTC >= LCD
		OR RouteLastChangeDateUTC >= LCD
		-- if any print related record was changed or a different template/image is now valid
		OR DAHI.LastChangeDateUTC >= LCD
		OR DAHIS.RecordID IS NOT NULL
		OR DAPT.LastChangeDateUTC >= LCD
		OR DAPTS.RecordID IS NOT NULL
		OR DADT.LastChangeDateUTC >= LCD
		OR DADTS.RecordID IS NOT NULL
	)
GO

EXEC _spDropFunction 'fnOrderTransfer_DriverApp'
GO
/*******************************************/
-- Date Created: 2015/09/24
-- Author: Kevin Alons
-- Purpose: return OrderTransfer data for Driver App sync
/*******************************************/
CREATE FUNCTION fnOrderTransfer_DriverApp( @DriverID INT, @LastChangeDateUTC DATETIME ) RETURNS  TABLE AS
RETURN 
	SELECT OTR.OrderID
        , OTR.DestTruckStartMileage
        , OTR.TransferComplete
        , OTR.Notes
		, OTR.CreateDateUTC
		, OTR.CreatedByUser
		, OTR.LastChangeDateUTC
		, OTR.LastChangedByUser
	FROM dbo.tblOrderTransfer OTR
	JOIN dbo.tblOrder O ON O.ID = OTR.OrderID
	LEFT JOIN tblOrderDriverAppVirtualDelete ODAVD ON ODAVD.OrderID = O.ID AND ODAVD.DriverID = @DriverID
	CROSS JOIN fnSyncLCDOffset(@LastChangeDateUTC) LCD
	WHERE (O.StatusID IN (2, 7, 8, 3) -- Dispatched, Accepted, Picked Up, Delivered
		-- or this potentially was a DriverApp record, that no longer is, so should be included (as deleted)
		OR (ODAVD.ID IS NOT NULL AND ODAVD.VirtualDeleteDateUTC >= LCD.LCD))
	  AND O.DriverID = @DriverID
	  AND (@LastChangeDateUTC IS NULL 
		OR OTR.CreateDateUTC >= LCD.LCD
		OR OTR.LastChangeDateUTC >= LCD.LCD
		OR ODAVD.VirtualDeleteDateUTC >= LCD.LCD)
GO
GRANT SELECT ON fnOrderTransfer_DriverApp TO role_iis_acct
GO


--------------------------------------------------------------------------
/*
	JAE 10/26/15 - Order transfer updates for approval page and for reporting
*/
------------------------------------------------------------------------
GO -- if you don't put a GO here, then the above comment will become part of the view definition below

/*************************************************************
-- Date Created: 15 Jun 2015
-- Author: Kevin Alons
-- Purpose: return the base data for the Order Approval page
-- Changes:
	- 3.9.2  - 2015/08/25 - KDA - remove obsolete OrderDateUTC & DeliverDateUTC fields (which were used to optimize date searches when OrderDate was virtual)
	- 3.9.20 - 2015/10/26 - JAE - Add columns for transfer percentage and override percentage
*************************************************************/
ALTER VIEW viewOrderApprovalSource
AS
	SELECT O.ID
		, O.StatusID
		, Status = O.OrderStatus
		, O.OrderNum
		, O.OrderDate
		, DeliverDate = ISNULL(O.DeliverDate, O.OrderDate)
		, ShipperID = O.CustomerID
		, O.CarrierID
		, O.DriverID
		, O.TruckID
		, O.TrailerID
		, O.Trailer2ID
		, O.ProducerID
		, O.OperatorID
		, O.PumperID
		, Shipper = O.Customer
		, O.Carrier
		, O.Driver
		, O.Truck
		, O.Trailer
		, O.Trailer2
		, O.Producer
		, O.Operator
		, O.Pumper
        , O.TicketTypeID
		, O.TicketType
		, O.ProductID
		, O.Product
		, O.ProductGroupID
		, O.ProductGroup
    
		, O.Rejected
		, RejectReason = O.RejectNumDesc
		, O.RejectNotes
		, O.ChainUp
		, OA.OverrideChainup
		, O.H2S
		, OA.OverrideH2S
        , O.DispatchConfirmNum
		, O.DispatchNotes
		, O.PickupDriverNotes
		, O.DeliverDriverNotes
		, Approved = CAST(ISNULL(OA.Approved, 0) AS BIT)
		, OSS.WaitFeeParameterID, O.AuditNotes
		
		, O.OriginID
		, O.Origin
		, O.OriginStateID
		, O.OriginArriveTime
		, O.OriginDepartTime
		, O.OriginMinutes
		, O.OriginUomID
		, O.OriginUOM
		, O.OriginGrossUnits
		, O.OriginGrossStdUnits
		, O.OriginNetUnits
		, OriginWaitReason = OWR.NumDesc
		, O.OriginWaitNotes
		, ShipperOriginWaitBillableMinutes = OSS.OriginWaitBillableMinutes
		, CarrierOriginWaitBillableMinutes = OSC.OriginWaitBillableMinutes
		, OA.OverrideOriginMinutes 
		
		, O.DestinationID
		, O.Destination
		, O.DestStateID
		, O.DestArriveTime
		, O.DestDepartTime
		, O.DestMinutes
		, O.DestUomID
		, O.DestUOM
		, O.DestGrossUnits
		, O.DestNetUnits
		, O.DestOpenMeterUnits
		, O.DestCloseMeterUnits
		, DestWaitReason = OWR.NumDesc
		, O.DestWaitNotes
		, ShipperDestinationWaitBillableMinutes = OSS.DestinationWaitBillableMinutes
		, CarrierDestinationWaitBillableMinutes = OSC.DestinationWaitBillableMinutes
		, OA.OverrideDestMinutes
	
		, Rerouted = CAST(CASE WHEN ORD.OrderID IS NULL THEN 0 ELSE 1 END AS BIT)
		, OA.OverrideReroute
		, O.ActualMiles
		, RerouteMiles = ROUND(ORD.RerouteMiles, 0)
		, OA.OverrideActualMiles
		, IsTransfer = CAST(CASE WHEN OT.OrderID IS NULL THEN 0 ELSE 1 END AS BIT)
		, TransferPercentComplete = ISNULL(OT.PercentComplete,100)
		, OA.OverrideTransferPercentComplete
		--, OA.OverrideMinSettlementUomID, OA.OverrideMinSettlementUnits    -- Commented out for 3.9.20, this is for future change by JAE
		
		, OA.CreateDateUTC
		, OA.CreatedByUser
		
		, OrderID = OSC.OrderID | OSS.OrderID -- NULL if either are NULL - if NULL indicates that the order is not yet rates (used below to rate these)
	FROM viewOrderLocalDates O
	OUTER APPLY dbo.fnOrderRerouteData(O.ID) ORD
	LEFT JOIN tblOrderReroute ORE ON ORE.OrderID = O.ID
	LEFT JOIN tblOrderTransfer OT ON OT.OrderID = O.ID
	LEFT JOIN tblOrderSettlementCarrier OSC ON OSC.OrderID = O.ID
	LEFT JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = O.ID
	LEFT JOIN viewOriginWaitReason OWR ON OWR.ID = O.OriginWaitReasonID
	LEFT JOIN viewDestinationWaitReason DWR ON DWR.ID = O.DestWaitReasonID
	LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
	WHERE O.StatusID IN (4) /* only include orders that are marked AUDITED */
	  AND OSC.BatchID IS NULL AND OSS.BatchID IS NULL /* don't show orders that have been settled */

GO
GRANT SELECT ON viewOrderApprovalSource TO role_iis_acct
GO

/***********************************
-- Date Created: 2015/08/14
-- Author: Kevin Alons
-- Purpose: create/update APPROVAL on an individual order (and re-rate the order)
-- Changes:
	3.9.12 - 2015/09/01 - KDA - add SET NOCOUNT ON to statement
							  - RAISE an ERROR if no record is updated (either due to target record not in valid state or not found
	3.9.20 - 2015/10/03 - JAE - remove OverrideMinSettlementUnits, add OverrideTransferPercentComplete parameters/logic
***********************************/
ALTER PROCEDURE spUpdateOrderApproval
(
  @ID INT
, @UserName varchar(100)
, @OverrideActualMiles INT = NULL
, @OverrideOriginMinutes INT = NULL
, @OverrideDestMinutes INT = NULL
, @OverrideChainup BIT = NULL
, @OverrideH2S BIT = NULL
, @OverrideReroute BIT = NULL
, @Approved BIT = 1
, @OverrideTransferPercentComplete INT = NULL
--, @OverrideMinSettlementUnits decimal = NULL
)
AS BEGIN
	SET NOCOUNT ON;
	
	/* only allowed AUDITED | non-settled orders from being APPROVED */
	IF EXISTS (
		SELECT O.* 
		FROM tblOrder O 
		LEFT JOIN tblOrderSettlementCarrier OSC ON OSC.OrderID = O.ID
		LEFT JOIN tblOrderSettlementShipper OSS ON OSS.OrderID = O.ID
		WHERE O.ID = @ID AND O.StatusID IN (4) /* 4 = AUDITED */
		  AND OSC.BatchID IS NULL AND OSS.BatchID IS NULL /* this order is not yet SETTLED */
	)
	BEGIN
		DECLARE @startedTX BIT
		BEGIN TRY
			IF (@@TRANCOUNT = 0)
			BEGIN
				BEGIN TRAN
				SET @startedTX = 1
			END
			UPDATE tblOrderApproval 
				SET OverrideActualMiles = @OverrideActualMiles
					, OverrideOriginMinutes = @OverrideOriginMinutes
					, OverrideDestMinutes = @OverrideDestMinutes
					, OverrideChainup = @OverrideChainup
					, OverrideH2S = @OverrideH2S
					, OverrideReroute = @OverrideReroute
					, OverrideTransferPercentComplete = @OverrideTransferPercentComplete
	--				, OverrideMinSettlementUnits = @OverrideMinSettlementUnits   -- Commented out for 3.9.20, this is for future change by JAE
					, Approved = @Approved
			WHERE OrderID = @ID
			IF (@@ROWCOUNT = 0)
			BEGIN
				INSERT INTO tblOrderApproval 
					(OrderID, OverrideActualMiles, OverrideOriginMinutes, OverrideDestMinutes, OverrideChainup, OverrideH2S, OverrideReroute, OverrideTransferPercentComplete
						, Approved, CreatedByUser, CreateDateUTC) 
				VALUES 
					(@ID, @OverrideActualMiles, @OverrideOriginMinutes, @OverrideDestMinutes, @OverrideChainup, @OverrideH2S, @OverrideReroute, @OverrideTransferPercentComplete
						, @Approved, @UserName, GETUTCDATE())
			END
			/* ensure any changes to this Approval are applied - which will affect applied rates */
			EXEC spApplyRatesBoth @ID, @UserName
			
			IF (@startedTX = 1)
				COMMIT TRAN
		END TRY
		BEGIN CATCH
			DECLARE @msg VARCHAR(MAX), @severity INT
			SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY()
			IF (@startedTX = 1)
				ROLLBACK TRAN
			RAISERROR(@msg, @severity, 1)
		END CATCH
	END
	ELSE
	BEGIN
		RAISERROR('Record not valid for Approval or not found', 16, 1)
	END
END
GO


/*************************************************************
-- Date Created: 15 Jun 2015
-- Author: Kevin Alons
-- Purpose: return the data for the Order Approval page
-- Changes:
	- 3.9.2  - 2015/08/26 - KDA - remove obsolete performance optimization using OrderDateUTC to force use of index (now OrderDate is indexed)
	- 3.9.3  - 2015/08/29 - KDA - add new FinalXXX fields
	- 3.9.20 - 2015/10/26 - JAE - update stored procedure to save transfer override values
*************************************************************/
ALTER PROCEDURE spOrderApprovalSource
(
  @userName VARCHAR(100)
, @ShipperID INT = NULL
, @CarrierID INT = NULL
, @ProductGroupID INT = NULL
, @OriginStateID INT = NULL
, @DestStateID INT = NULL
, @Start DATE = NULL
, @End DATE = NULL
, @IncludeApproved BIT = 0
) AS BEGIN
	SET NOCOUNT ON;
	
	SELECT *
		, FinalActualMiles = ISNULL(OverrideActualMiles, ActualMiles)
		, FinalOriginMinutes = ISNULL(OverrideOriginMinutes, OriginMinutes)
		, FinalDestMinutes = ISNULL(OverrideDestMinutes, DestMinutes)
		, FinalChainup = CAST(ISNULL(1-NULLIF(OverrideChainup, 0), Chainup) AS BIT)
		, FinalH2S = CAST(ISNULL(1-NULLIF(OverrideH2S, 0), H2S) AS BIT)
		, FinalRerouted = CAST(ISNULL(1-NULLIF(OverrideReroute, 0), Rerouted) AS BIT)
		, FinalTransferPercentComplete = ISNULL(OverrideTransferPercentComplete, TransferPercentComplete)
	INTO #X
	FROM viewOrderApprovalSource 
	WHERE (NULLIF(@ShipperID, -1) IS NULL OR ShipperID = @ShipperID)
	  AND (NULLIF(@CarrierID, -1) IS NULL OR CarrierID = @CarrierID)
	  AND (NULLIF(@ProductGroupID, -1) IS NULL OR ProductGroupID = @ProductGroupID)
	  AND (NULLIF(@OriginStateID, -1) IS NULL OR OriginStateID = @OriginStateID)
	  AND (NULLIF(@DestStateID, -1) IS NULL OR DestStateID = @DestStateID)
	  AND OrderDate BETWEEN ISNULL(@Start, '1/1/1900') AND ISNULL(@End, '1/1/2200')
	  AND (@IncludeApproved = 1 OR ISNULL(Approved, 0) = 0) 
	  
	DECLARE @id int
	WHILE EXISTS (SELECT * FROM #x WHERE OrderID IS NULL)
	BEGIN
		SELECT @id = min(ID) FROM #x WHERE OrderID IS NULL
		EXEC spApplyRatesBoth @id, @userName
		UPDATE #x SET OrderID = @id, Approved = (SELECT Approved FROM tblOrderApproval WHERE OrderID = @id) WHERE ID = @id
	END
	ALTER TABLE #X DROP COLUMN OrderID
	
	SELECT * FROM #x WHERE (@IncludeApproved = 1 OR ISNULL(Approved, 0) = 0) 

END

GO



/***********************************
-- Date Created: 12 Feb 2015
-- Author: Kevin Alons
-- Purpose: return last truck mileage for each truck (from order table)
-- Changes:
    - 3.9.20 - 2015/10/23 - KDA - revise logic to use tblOrderTransfer data (when a transfer took place that changed Trucks)
			 - 2015/10/29 - JAE - Include incomplete orders as well as transfers that keep the same truck
***********************************/
ALTER VIEW viewOrderLastTruckMileage AS
SELECT *
FROM (
    SELECT TruckID = T.ID
        , Odometer = CASE WHEN OTM.OrderID > DTM.OrderID THEN OTM.Odometer ELSE DTM.Odometer END
        , OdometerDate = CASE WHEN OTM.OrderID > DTM.OrderID THEN OTM.OdometerDate ELSE DTM.OdometerDate END
        , OdometerDateUTC = CASE WHEN OTM.OrderID > DTM.OrderID THEN OTM.OdometerDateUTC ELSE DTM.OdometerDateUTC END
    FROM tblTruck T

    LEFT JOIN (
        -- Origin truck (for transfers with origin truck not same as destination truck)
        SELECT OrderID = O.ID, TruckID = OTR.OriginTruckID, Odometer = OTR.OriginTruckEndMileage, OdometerDate = O.OrderDate, OdometerDateUTC = OTR.CreateDateUTC
        FROM tblOrderTransfer OTR
        JOIN (
            SELECT OrderID = MAX(OTR.OrderID), OriginTruckID 
            FROM tblOrderTransfer OTR 
            JOIN tblOrder O ON O.ID = OTR.OrderID 
				AND O.TruckID <> OTR.OriginTruckID -- truck change
            GROUP BY OriginTruckID
        ) X ON X.OrderID = OTR.OrderID
        JOIN tblOrder O ON O.ID = OTR.OrderID
    ) OTM ON OTM.TruckID = T.ID

    LEFT JOIN (
		-- Destination truck (for non-transfers or transfers with same truck)
        SELECT ordernum,OrderID = O.ID, O.TruckID, O.truck,
        Odometer = CASE WHEN (o.TruckID = o.OriginTruckID) 
					THEN COALESCE(DestTruckMileage, DestTruckStartMileage, OriginTruckEndMileage, OriginTruckMileage) --use any valid mileage
					ELSE ISNULL(DestTruckMileage, DestTruckStartMileage) END, -- dont use origin mileages when truck changed (transfers)
        OdometerDate = OrderDate, 
        OdometerDateUTC = DeliverLastChangeDateUTC
        FROM viewOrder O
        JOIN (
            SELECT TruckID, ID = MAX(ID) FROM viewOrder
            WHERE TruckID = OriginTruckID AND OriginTruckMileage IS NOT NULL -- if regular or same-truck transfer, origin start must be set
			   OR TruckID <> OriginTruckID AND DestTruckStartMileage IS NOT NULL -- if transfer truck, dest start must be set
            GROUP BY TruckID
        ) X ON X.ID = O.ID
    ) DTM ON DTM.TruckID = T.ID
) X
WHERE X.OdometerDate IS NOT NULL
GO


/*********************************************
Date Create: 12 Feb 2015
Author: Kevin Alons
Purpose: return a "Mileage Log" from the Order table
-- Changes:
	- 3.9.20 - 2015/10/28 - JAE - Update report to include transfer orders
*********************************************/
ALTER FUNCTION fnMileageLog(@StartDate DATE, @EndDate DATE, @CarrierID INT = -1, @DriverID INT = -1, @TruckID INT = -1)
RETURNS @data TABLE
(
  ID INT
  , OrderNum INT
  , Carrier VARCHAR(100)
  , Truck VARCHAR(100)
  , Driver TEXT
  , StartTime DATETIME
  , EndTime DATETIME
  , StartOdometer INT
  , EndOdometer INT
  , Mileage INT
  , RouteMiles INT
  , Type VARCHAR(20)
  , Locked BIT
)
AS BEGIN
	INSERT INTO @data
		--Non-transfer orders
		SELECT ID, OrderNum, Carrier, Truck, Driver
			, StartTime = OriginArriveTimeUTC, EndTime = DestDepartTimeUTC
			, StartOdometer = OriginTruckMileage, EndOdometer = DestTruckMileage
			, Mileage = DestTruckMileage - OriginTruckMileage
			, RouteMiles = O.ActualMiles
			, Type = 'Loaded'
			, Locked = CASE WHEN O.StatusID = 4 THEN 1 ELSE 0 END
		FROM viewOrderLocalDates O
		WHERE O.IsTransfer = 0
		  AND O.OriginArriveTimeUTC IS NOT NULL AND O.DestDepartTimeUTC IS NOT NULL 
		  AND O.OriginTruckMileage IS NOT NULL AND O.DestTruckMileage IS NOT NULL
		  AND O.TruckID IS NOT NULL
		  AND (@CarrierID = -1 OR @CarrierID = CarrierID)
		  AND (@DriverID = -1 OR @DriverID = DriverID)
		  AND (@TruckID = -1 OR @TruckID = TruckID)
		  AND O.OrderDate BETWEEN @StartDate AND @EndDate

	INSERT INTO @data
		-- First leg of transfer orders
		SELECT o.ID, OrderNum, o.Carrier, T.FullName AS Truck, D.FullName AS Driver
			, StartTime = OriginArriveTimeUTC
			, EndTime = TransferDateUTC
			, StartOdometer = OriginTruckMileage
			, EndOdometer = OriginTruckEndMileage
			, Mileage = OriginTruckEndMileage - OriginTruckMileage
			, RouteMiles = ActualMiles
			, Type = 'Loaded (1st leg)'
			, Locked = CASE WHEN StatusID = 4 THEN 1 ELSE 0 END
		FROM viewOrderLocalDates O
		LEFT JOIN viewTruck T ON T.ID = O.OriginTruckID
		LEFT JOIN viewDriverBase D ON D.ID = O.OriginDriverID
		WHERE IsTransfer = 1
		  AND O.OriginArriveTimeUTC IS NOT NULL
		  AND O.OriginTruckMileage IS NOT NULL AND OriginTruckEndMileage IS NOT NULL
		  AND OriginTruckID IS NOT NULL
		  AND (@CarrierID = -1 OR @CarrierID = O.CarrierID)
		  AND (@DriverID = -1 OR @DriverID = O.DriverID)
		  AND (@TruckID = -1 OR @TruckID = OriginTruckID)
		  AND O.OrderDate BETWEEN @StartDate AND @EndDate

	INSERT INTO @data
		-- Second leg of transfer tickets
		SELECT ID, OrderNum, Carrier, Truck, Driver
			, StartTime = TransferDateUTC
			, EndTime = DestDepartTimeUTC
			, StartOdometer = DestTruckStartMileage
			, EndOdometer = DestTruckMileage
			, Mileage = DestTruckMileage - DestTruckStartMileage
			, RouteMiles = O.ActualMiles
			, Type = 'Loaded (2nd leg)'
			, Locked = CASE WHEN O.StatusID = 4 THEN 1 ELSE 0 END
		FROM viewOrderLocalDates O
		WHERE IsTransfer = 1
		  AND OriginArriveTimeUTC IS NOT NULL AND DestDepartTimeUTC IS NOT NULL 
		  AND DestTruckStartMileage IS NOT NULL AND DestTruckMileage IS NOT NULL
		  AND TruckID IS NOT NULL
		  AND (@CarrierID = -1 OR @CarrierID = CarrierID)
		  AND (@DriverID = -1 OR @DriverID = DriverID)
		  AND (@TruckID = -1 OR @TruckID = TruckID)
		  AND OrderDate BETWEEN @StartDate AND @EndDate

	INSERT INTO @data
		--To next order
		SELECT ID = NULL, OrderNum = NULL, Carrier, Truck, Driver = NULL
			, StartTime = C.EndTime, EndTime = N.StartTime
			, StartOdometer = C.EndOdometer, EndOdometer = N.StartOdometer
			, Mileage = N.StartOdometer - C.EndOdometer
			, RouteMiles = NULL
			, Type = 'Unloaded'
			, Locked = 1
		FROM @data C
		CROSS APPLY (
			SELECT TOP 1 ID, StartTime, StartOdometer FROM @data N WHERE N.StartTime > C.StartTime AND N.Truck = C.Truck 
			AND (C.Type = 'Loaded' OR C.Type = 'Loaded (2nd leg)') -- Include delivered loads only (ignore 1st leg for transfers)
			ORDER BY StartTime
		) N
	RETURN
END
GO

---------------------------------------------------
-- Insert Order Transfer fields into Report Center	
-- Updates to tblReportColumnDefinition
---------------------------------------------------

-- JAE - 2015/10/26 - allow longer strings in the filterdatafield for custom fields to work
ALTER TABLE tblReportColumnDefinition 
ALTER COLUMN FilterDataField VARCHAR(255)
GO


-- JAE - 2015/10/26 - correct actual miles for transfers (continue using end - start for non-transfers, but calculate each leg separately for transfers)
UPDATE tblReportColumnDefinition 
SET DataField = 'CASE WHEN IsTransfer = 0 THEN DestTruckMileage - OriginTruckMileage ELSE (DestTruckMileage - DestTruckStartMileage) + (OriginTruckEndMileage - OriginTruckMileage) END'
WHERE id = 90001


/*
	BB 10/22/15 - DCWEB-891: Edit existing report center driver field for new order transfer functionality.
	BB 11/03/15 - Remove caption change in favor of leaving it as "Driver" (id = 27)
	            - Remove new report column from Joe's insert in favor of utilizing current truck column (id=28)
				- Renamed "mileage" captions to "odometer" to make more sense with new transfer odometer fields (id=36 and id=38)
*/
UPDATE tblReportColumnDefinition SET DataField = 'OriginDriver', FilterDataField = 'OriginDriverID' WHERE ID = 27
UPDATE tblReportColumnDefinition SET DataField = 'OriginTruck', FilterDataField = 'OriginTruckID' WHERE ID = 28
UPDATE tblReportColumnDefinition SET Caption = 'TRIP | MILES | Origin Truck Odometer' WHERE ID = 36
UPDATE tblReportColumnDefinition SET Caption = 'TRIP | MILES | Dest Truck Odometer' WHERE ID = 38
GO


-- JAE - 2015/10/26 - Insert transfer columns 
-- BB  - 10/22/15 - DCWEB-891: Add transfer driver field to report center.
-- BB  - 11/03/15 - Renamed a few captions for new fields to keep consistent with current RC captions
--                - Edited a few transfer fields to use existing fields as "origin" fields
SET IDENTITY_INSERT tblReportColumnDefinition ON
  INSERT tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport)
	SELECT 275,1,'DestDriver','TRIP | TRANSFER | Transfer Driver',NULL,'DestDriverID',2,'SELECT ID, Name = FullName FROM viewDriver ORDER BY FullName',0,'*',0
  UNION
	SELECT 276,1,'DestTruck','TRIP | TRANSFER | Transfer Truck',NULL,'DestTruckID',2,'SELECT ID, Name = FullName FROM viewTruck ORDER BY FullName',0,'*',0
  UNION
	SELECT 277,1,'IsTransfer','TRIP | TRANSFER | Transfer? (Y/N)',NULL,NULL,5,'SELECT ID=1, Name=''Yes'' UNION SELECT 0, ''No''',0,'*',0
  UNION
	SELECT 278,1,'OriginTruckEndMileage','TRIP | MILES | Transfer Pickup Odometer End',NULL,NULL,4,NULL,1,'*',0
  UNION
	SELECT 279,1,'DestTruckStartMileage','TRIP | MILES | Transfer Deliver Odometer Begin',NULL,NULL,4,NULL,1,'*',0
  UNION
	SELECT 280,1,'TransferNotes','TRIP | TRANSFER | Transfer Notes',NULL,NULL,1,NULL,1,'*',0
  UNION
	SELECT 90029,1,'OriginTruckEndMileage - OriginTruckMileage','TRIP | MILES | Transfer Pickup Driver Miles',NULL,NULL,4,NULL,1,'*',0
  UNION
	SELECT 90030,1,'DestTruckMileage - DestTruckStartMileage','TRIP | MILES | Transfer Deliver Driver Miles',NULL,NULL,4,NULL,1,'*',0
  UNION
	SELECT 90031,1,'ISNULL(TransferPercent, 100)','TRIP | TRANSFER | Transfer Pickup Driver %',NULL,'ISNULL(TransferPercent, 100)',4,NULL,1,'*',0
  UNION
	SELECT 90032,1,'100 - ISNULL(TransferPercent,100)','TRIP | TRANSFER | Transfer Deliver Driver %',NULL,'100 - ISNULL(TransferPercent,100)',4,NULL,1,'*',0
  UNION
	SELECT 90033,1,'COALESCE((SELECT OA.OverrideTransferPercentComplete FROM tblOrderApproval OA WHERE OA.OrderID = RS.ID), TransferPercent, 100)'
           ,'TRIP | TRANSFER | Transfer Final Pickup Driver %',NULL
		   ,'COALESCE((SELECT OA.OverrideTransferPercentComplete FROM tblOrderApproval OA WHERE OA.OrderID = ID), TransferPercent, 100)' -- same as datafield without RS.
           ,4,NULL,1,'*',0
  UNION
	SELECT 90034,1,'100 - COALESCE((SELECT OA.OverrideTransferPercentComplete FROM tblOrderApproval OA WHERE OA.OrderID = RS.ID), TransferPercent, 100)'
           ,'TRIP | TRANSFER | Transfer Final Deliver Driver %'
           ,NULL,'100 - COALESCE((SELECT OA.OverrideTransferPercentComplete FROM tblOrderApproval OA WHERE OA.OrderID = ID), TransferPercent, 100)' -- same as datafield without RS.
           ,4,NULL,1,'*',0
  EXCEPT SELECT ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport FROM tblReportColumnDefinition
SET IDENTITY_INSERT tblReportColumnDefinition OFF


GO


EXEC _spRebuildAllObjects
GO

EXEC _spRefreshAllViews
GO

COMMIT
SET NOEXEC OFF
