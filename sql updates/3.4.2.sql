-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.4.1'
SELECT  @NewVersion = '3.4.2'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'revise trigOrderTicket_IU no longer error changes to Tickets for DELIVERED orders (only AUDITED)'
GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 6 Feb 2013
-- Description:	trigger to ensure the GrossBarrels and NetBarrels are computed for valid, entered data
-- =============================================
ALTER TRIGGER [dbo].[trigOrderTicket_IU] ON [dbo].[tblOrderTicket] AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	-- Note: the TRIGGER_NESTLEVEL( OBJECT_ID('trigOrderTicket_IU') ) = 1 statement prevents recursion
	IF (TRIGGER_NESTLEVEL( OBJECT_ID('trigOrderTicket_IU')) = 1) BEGIN
		
		-- only do anything if something actually changed
		IF EXISTS (SELECT * FROM inserted EXCEPT SELECT * FROM deleted)
		BEGIN
			PRINT 'trigOrderTicket_IU FIRED'
			
			/**********  START OF VALIDATION SECTION ************************/
			DECLARE @errorString varchar(255)

			IF EXISTS (
				SELECT * 
				FROM tblOrder 
				WHERE ID IN (SELECT OrderID FROM inserted) 
				  -- prevent changes to any tickets belonging to a Delivered or Audited Order
				  AND StatusID IN (4))
			BEGIN
				IF (dbo.fnToBool(dbo.fnSettingValue(35)) = 1)
					PRINT 'Ticket for an AUDITED orders is being modified - please investigate why!'
				ELSE 				
					SET @errorString = 'Tickets of Audited Orders cannot be modified'
			END

			-- prevent reassigning a ticket from one order to another
			IF EXISTS (
				SELECT i.* 
				FROM inserted i
				JOIN deleted d ON i.ID = d.ID
				WHERE i.OrderID <> d.OrderID)
			BEGIN
				SET @errorString = 'Tickets cannot be moved to a different Order'
			END
			
			IF (SELECT COUNT(*) FROM (
					SELECT OT.OrderID
					FROM tblOrderTicket OT
					JOIN inserted i ON i.OrderID = OT.OrderID AND i.CarrierTicketNum = OT.CarrierTicketNum
					WHERE OT.DeleteDateUTC IS NULL
					GROUP BY OT.OrderID, OT.CarrierTicketNum
					HAVING COUNT(*) > 1
				) v) > 0
			BEGIN
				SET @errorString = 'Duplicate Ticket Numbers are not allowed'
			END
			
			ELSE IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (3) AND BOLNum IS NULL AND DeleteDateUTC IS NULL) > 0
			BEGIN
				SET @errorString = 'BOL # value is required for Meter Run tickets'
			END
			
			ELSE IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1, 2) AND OriginTankID IS NULL AND TankNum IS NULL AND DeleteDateUTC IS NULL) > 0
			BEGIN
				SET @errorString = 'Tank ID value is required for Gauge Run & Net Volume tickets'
			END

			ELSE IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1) AND CarrierTicketNum IS NULL AND DeleteDateUTC IS NULL) > 0
			BEGIN
				SET @errorString = 'Ticket # value is required for Gauge Run tickets'
			END

			ELSE IF (SELECT COUNT(*) FROM inserted WHERE DeleteDateUTC IS NULL
					AND (TicketTypeID IN (1, 2) AND (ProductObsTemp IS NULL OR ProductObsGravity IS NULL OR ProductBSW IS NULL))
						OR (TicketTypeID IN (1) AND (ProductHighTemp IS NULL OR ProductLowTemp IS NULL))
				) > 0
			BEGIN
				SET @errorString = 'All Product Measurement values are required for Gauge Run & Net Volume tickets'
			END
			
			ELSE IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1) AND Rejected = 0 AND DeleteDateUTC IS NULL
				AND (OpeningGaugeFeet IS NULL OR OpeningGaugeInch IS NULL OR OpeningGaugeQ IS NULL)) > 0
			BEGIN
				SET @errorString = 'All Opening Gauge values are required for Gauge Run tickets'
			END
			ELSE IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1) AND Rejected = 0 AND DeleteDateUTC IS NULL
				AND (ClosingGaugeFeet IS NULL OR ClosingGaugeInch IS NULL OR ClosingGaugeQ IS NULL)) > 0
			BEGIN
				SET @errorString = 'All Closing Gauge values are required for Gauge Run tickets'
			END

			ELSE IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (2) AND Rejected = 0 AND DeleteDateUTC IS NULL
				AND (GrossUnits IS NULL)) > 0
			BEGIN
				SET @errorString = 'Gross Volume value is required for Net Volume tickets'
			END

			ELSE IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (3) AND Rejected = 0 AND DeleteDateUTC IS NULL
				AND (GrossUnits IS NULL OR NetUnits IS NULL)) > 0
			BEGIN
				SET @errorString = 'Gross & Net Volume values are required for Meter Run tickets'
				IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
				RETURN
			END

			ELSE IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1) AND Rejected = 0 AND DeleteDateUTC IS NULL
				AND (SealOff IS NULL OR SealOn IS NULL)) > 0
			BEGIN
				SET @errorString = 'All Seal Off & Seal On values are required for Gauge Run tickets'
			END

			IF (@errorString IS NOT NULL)
			BEGIN
				RAISERROR(@errorString, 16, 1)
				IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
			END
			
			/**********  END OF VALIDATION SECTION ************************/
			
			-- re-compute GaugeRun ticket Gross barrels
			IF UPDATE(OriginTankID) OR UPDATE(OpeningGaugeFeet) OR UPDATE(OpeningGaugeInch) OR UPDATE(OpeningGaugeQ)
				OR UPDATE (ClosingGaugeFeet) OR UPDATE(ClosingGaugeInch) OR UPDATE(ClosingGaugeQ) 
				OR UPDATE (GrossUnits)
			BEGIN
				UPDATE tblOrderTicket
				  SET GrossUnits = dbo.fnConvertUom(dbo.fnOriginTankStrappingLevelDeltaBarrels(
						OT.OriginTankID
					  , OpeningGaugeFeet
					  , OpeningGaugeInch
					  , OpeningGaugeQ
					  , ClosingGaugeFeet
					  , ClosingGaugeInch
					  , ClosingGaugeQ), 1, O.OriginUomID) -- ensure this value is always normalized from BARRELS
				FROM tblOrderTicket OT
				JOIN tblOrder O ON O.ID = OT.OrderID
				WHERE OT.ID IN (SELECT ID FROM inserted) 
				  AND OT.TicketTypeID = 1 -- Gauge Run Ticket
				  AND OT.OpeningGaugeFeet IS NOT NULL
				  AND OT.OpeningGaugeInch IS NOT NULL
				  AND OT.OpeningGaugeQ IS NOT NULL
				  AND OT.ClosingGaugeFeet IS NOT NULL
				  AND OT.ClosingGaugeInch IS NOT NULL
				  AND OT.ClosingGaugeQ IS NOT NULL
			END
			-- recompute the GrossUnits of any changed Meter Run tickets
			IF UPDATE(OpenMeterUnits) OR UPDATE(CloseMeterUnits) BEGIN
				--Print 'updating tblOrderTicket GOV from Open/Close Meter Units'
				UPDATE tblOrderTicket 
					SET GrossUnits = CloseMeterUnits - OpenMeterUnits
				WHERE TicketTypeID = 3  -- meter run tickets ONLY
				  AND OpenMeterUnits IS NOT NULL AND CloseMeterUnits IS NOT NULL
				
			END
			-- re-compute GaugeRun | Net Barrel | Meter Run tickets NetUnits
			IF UPDATE(OpeningGaugeFeet) OR UPDATE(OpeningGaugeInch) OR UPDATE(OpeningGaugeQ)
				OR UPDATE (ClosingGaugeFeet) OR UPDATE(ClosingGaugeInch) OR UPDATE(ClosingGaugeQ) 
				OR UPDATE(OriginTankID) OR UPDATE(GrossUnits) OR UPDATE(GrossStdUnits) OR UPDATE(NetUnits)
				OR UPDATE(ProductObsTemp) OR UPDATE(ProductObsGravity) OR UPDATE(ProductBSW)
				OR UPDATE(OpenMeterUnits) OR UPDATE(CloseMeterUnits)
			BEGIN
				--PRINT 'updating tblOrderTicket NSV/GSV'
				UPDATE tblOrderTicket
				  SET NetUnits = dbo.fnCrudeNetCalculator(GrossUnits, ProductObsTemp, ProductObsGravity, isnull(ProductBSW, 0))
					, GrossStdUnits = dbo.fnCrudeNetCalculator(GrossUnits, ProductObsTemp, ProductObsGravity, 0)
				WHERE ID IN (SELECT ID FROM inserted)
				  AND TicketTypeID IN (1,2)
				  AND GrossUnits IS NOT NULL
				  AND ProductObsTemp IS NOT NULL
				  AND ProductObsGravity IS NOT NULL
			END
			
			-- ensure the Order record is in-sync with the Tickets
			--declare @gov decimal(9,3); SELECT TOP 1 @gov = OT.GrossUnits FROM tblOrderTicket OT JOIN inserted i ON i.ID = OT.ID;
			--PRINT 'updating tblOrder GOV/GSV/NSV GOV = ' + ltrim(@gov)
			UPDATE tblOrder 
			SET OriginGrossUnits = (SELECT sum(GrossUnits) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND OT.Rejected = 0 AND OT.DeleteDateUTC IS NULL)
			  , OriginNetUnits = (SELECT sum(NetUnits) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND OT.Rejected = 0 AND OT.DeleteDateUTC IS NULL)
			  , OriginGrossStdUnits = (SELECT sum(GrossStdUnits) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND OT.Rejected = 0 AND OT.DeleteDateUTC IS NULL)

				-- use the first MeterRun/BasicRun BOLNum as the Order BOL Num (favor the first un-Rejected)
			  , OriginBOLNum = (
					SELECT TOP 1 BOLNum FROM (
						SELECT TOP 1 BOLNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID NOT IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
						UNION SELECT TOP 1 BOLNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID NOT IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X)
				-- use the first GaugeRun|NetVolume CarrierTicketNum as the Order.CarrierTicketNum (favor first un-Rejected)
			  , CarrierTicketNum = (
					SELECT TOP 1 CarrierTicketNum FROM (
						SELECT TOP 1 CarrierTicketNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
						UNION SELECT TOP 1 CarrierTicketNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X)
				-- use the first GaugeRun|NetVolume OriginTankID for the Order (favor first un-Rejected ticket)
			  , OriginTankID = (
					SELECT TOP 1 OriginTankID FROM (
						SELECT TOP 1 OriginTankID FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
						UNION SELECT TOP 1 OriginTankID FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X)
				-- use the first GaugeRun|NetVolume OriginTankNum for the Order (favor first un-Rejected ticket)
			  , OriginTankNum = (
					SELECT TOP 1 OriginTankNum FROM (
						SELECT TOP 1 OriginTankNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
						UNION SELECT TOP 1 OriginTankNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X)
			  , LastChangeDateUTC = (SELECT MAX(isnull(LastChangeDateUTC, CreateDateUTC)) FROM inserted WHERE OrderID = O.ID)
			  , LastChangedByUser = (SELECT MIN(isnull(LastChangedByUser, CreatedByUser)) FROM inserted WHERE OrderID = O.ID)
			FROM tblOrder O
			WHERE ID IN (SELECT DISTINCT OrderID FROM inserted)

			/* START DB AUDIT *********************************************************/
			BEGIN TRY
				IF EXISTS(SELECT * FROM tblSetting WHERE ID = 32 AND (Value LIKE 'true' OR Value LIKE 'yes'))
					INSERT INTO tblOrderTicketDbAudit (DBAuditDate, ID, OrderID, CarrierTicketNum, TicketTypeID, TankNum, ProductObsGravity, ProductObsTemp, OpeningGaugeFeet, OpeningGaugeInch, OpeningGaugeQ, ClosingGaugeFeet, ClosingGaugeInch, ClosingGaugeQ, GrossUnits, NetUnits, Rejected, RejectNotes, SealOff, SealOn, BOLNum, ProductHighTemp, ProductLowTemp, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser, UID, FromMobileApp, OriginTankID, GrossStdUnits, BottomFeet, BottomInches, BottomQ, RejectReasonID, MeterFactor, OpenMeterUnits, CloseMeterUnits)
						SELECT GETUTCDATE(), ID, OrderID, CarrierTicketNum, TicketTypeID, TankNum, ProductObsGravity, ProductObsTemp, OpeningGaugeFeet, OpeningGaugeInch, OpeningGaugeQ, ClosingGaugeFeet, ClosingGaugeInch, ClosingGaugeQ, GrossUnits, NetUnits, Rejected, RejectNotes, SealOff, SealOn, BOLNum, ProductHighTemp, ProductLowTemp, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser, UID, FromMobileApp, OriginTankID, GrossStdUnits, BottomFeet, BottomInches, BottomQ, RejectReasonID, MeterFactor, OpenMeterUnits, CloseMeterUnits
						FROM deleted d
			END TRY
			BEGIN CATCH
				PRINT 'trigOrderTicket_IU.DBAUDIT FAILURE: ' + ERROR_MESSAGE()
			END CATCH
			/* END DB AUDIT *********************************************************/

			PRINT 'trigOrderTicket_IU COMPLETE'
		END
	END
	
END

GO
EXEC sp_settriggerorder @triggername=N'[dbo].[trigOrderTicket_IU]', @order=N'First', @stmttype=N'INSERT'
GO
EXEC sp_settriggerorder @triggername=N'[dbo].[trigOrderTicket_IU]', @order=N'First', @stmttype=N'UPDATE'
GO

COMMIT
SET NOEXEC OFF