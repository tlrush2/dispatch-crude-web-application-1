SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.4.20'
SELECT  @NewVersion = '4.4.21'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'JT-697 - Add Terminal to Import Center'	
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


insert into tblobject (id, name, sqlsourcename, sqltargetname) values
(48, 'Terminal', 'tblTerminal', 'tblTerminal')

GO

-- first call to _spAddNewObjectFields to add terminal fields (and others)
SET IDENTITY_INSERT tblObjectField ON
INSERT INTO tblObjectField (ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, AllowNullID, IsKey, IsCustom, ParentObjectID, ParentObjectIDFieldName)
SELECT 444,'13','Notes','Notes','1',NULL,'1','0','0',NULL,NULL
INSERT INTO tblObjectField (ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, AllowNullID, IsKey, IsCustom, ParentObjectID, ParentObjectIDFieldName)
SELECT 445,'13','TerminationDate','Termination Date','5',NULL,'1','0','0',NULL,NULL
INSERT INTO tblObjectField (ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, AllowNullID, IsKey, IsCustom, ParentObjectID, ParentObjectIDFieldName)
SELECT 446,'48','Address','Address','1',NULL,'1','0','0',NULL,NULL
INSERT INTO tblObjectField (ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, AllowNullID, IsKey, IsCustom, ParentObjectID, ParentObjectIDFieldName)
SELECT 447,'48','City','City','1',NULL,'1','0','0',NULL,NULL
INSERT INTO tblObjectField (ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, AllowNullID, IsKey, IsCustom, ParentObjectID, ParentObjectIDFieldName)
SELECT 448,'48','ID','ID','3',NULL,'1','1','0',NULL,NULL
INSERT INTO tblObjectField (ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, AllowNullID, IsKey, IsCustom, ParentObjectID, ParentObjectIDFieldName)
SELECT 449,'48','LAT','LAT','4',NULL,'1','0','0',NULL,NULL
INSERT INTO tblObjectField (ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, AllowNullID, IsKey, IsCustom, ParentObjectID, ParentObjectIDFieldName)
SELECT 450,'48','LON','LON','4',NULL,'1','0','0',NULL,NULL
INSERT INTO tblObjectField (ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, AllowNullID, IsKey, IsCustom, ParentObjectID, ParentObjectIDFieldName)
SELECT 451,'48','Name','Name','1',NULL,'0','0','0',NULL,NULL
INSERT INTO tblObjectField (ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, AllowNullID, IsKey, IsCustom, ParentObjectID, ParentObjectIDFieldName)
SELECT 452,'48','OfficePhone','Office Phone','1',NULL,'1','0','0',NULL,NULL
INSERT INTO tblObjectField (ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, AllowNullID, IsKey, IsCustom, ParentObjectID, ParentObjectIDFieldName)
SELECT 453,'48','StateID','State','3',NULL,'1','0','0','35','ID'
INSERT INTO tblObjectField (ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, AllowNullID, IsKey, IsCustom, ParentObjectID, ParentObjectIDFieldName)
SELECT 454,'48','Zip','Zip','1',NULL,'1','0','0',NULL,NULL
SET IDENTITY_INSERT tblObjectField OFF

-- second call to _spAddNewObjectFields to add terminal foreign keys once ID was added
SET IDENTITY_INSERT tblObjectField ON
INSERT INTO tblObjectField (ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, AllowNullID, IsKey, IsCustom, ParentObjectID, ParentObjectIDFieldName)
SELECT 455,'12','TerminalID','Terminal','3',NULL,'1','0','0','48','ID'
INSERT INTO tblObjectField (ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, AllowNullID, IsKey, IsCustom, ParentObjectID, ParentObjectIDFieldName)
SELECT 456,'13','TerminalID','Terminal','3',NULL,'0','0','0','48','ID'
INSERT INTO tblObjectField (ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, AllowNullID, IsKey, IsCustom, ParentObjectID, ParentObjectIDFieldName)
SELECT 457,'14','TerminalID','Terminal','3',NULL,'0','0','0','48','ID'
INSERT INTO tblObjectField (ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, AllowNullID, IsKey, IsCustom, ParentObjectID, ParentObjectIDFieldName)
SELECT 458,'15','TerminalID','Terminal','3',NULL,'1','0','0','48','ID'
INSERT INTO tblObjectField (ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, AllowNullID, IsKey, IsCustom, ParentObjectID, ParentObjectIDFieldName)
SELECT 459,'19','TerminalID','Terminal','3',NULL,'1','0','0','48','ID'
INSERT INTO tblObjectField (ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, AllowNullID, IsKey, IsCustom, ParentObjectID, ParentObjectIDFieldName)
SELECT 460,'20','TerminalID','Terminal','3',NULL,'1','0','0','48','ID'
SET IDENTITY_INSERT tblObjectField OFF

GO


COMMIT
SET NOEXEC OFF