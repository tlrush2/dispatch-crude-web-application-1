-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.0.0'
SELECT  @NewVersion = '4.0.0.1'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'DCWEB-1659: Hotfix for report center field permissions'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/* Add edit/design permission for report center */
EXEC spAddNewPermission 'editReportCenter', 'Allow user to Create and Edit Report Center reports (Requires Report Center permission)', 'Reports', 'Design RC Reports'
GO

/* Set all report center fields to unrestricted (per Maverick) */
UPDATE tblReportColumnDefinition SET AllowedRoles = '*' WHERE AllowedRoles <> '*'
GO


COMMIT
SET NOEXEC OFF