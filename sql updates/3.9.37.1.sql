SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.9.37'
SELECT  @NewVersion = '3.9.37.1'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-897: Fix to Min Settlement Units in Report Center'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO



-- Needed to update Carrier Min Settle Units because CarrierMinSettlementUnits was using the value after approval at settlement
update tblreportcolumndefinition 
SET datafield = '(SELECT MinSettlementUnits FROM viewOrderSettlementUnitsCarrier OSUC WHERE OSUC.OrderID = RS.ID)'
where id = 177

-- Needed to update Shipper Min Settle Units because ShipperMinSettlementUnits was using the value after approval at settlement
update tblreportcolumndefinition 
SET datafield = '(SELECT MinSettlementUnits FROM viewOrderSettlementUnitsShipper OSUS WHERE OSUS.OrderID = RS.ID)'
where id = 159

-- Carrier Min Settlement Units Final (null if not approved)
update tblreportcolumndefinition 
set datafield = '(SELECT CASE WHEN OA.Approved=1 THEN CarrierMinSettlementUnits ELSE NULL END FROM tblOrderApproval OA WHERE OA.OrderID = RS.ID)'
where id = 90041

-- Shipper Min Settlement Units Final (null if not approved)
update tblreportcolumndefinition 
set datafield = '(SELECT CASE WHEN OA.Approved=1 THEN ShipperMinSettlementUnits ELSE NULL END FROM tblOrderApproval OA WHERE OA.OrderID = RS.ID)'
where id = 90042

GO



COMMIT
SET NOEXEC OFF