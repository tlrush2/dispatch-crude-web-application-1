SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.11.17.5'
SELECT  @NewVersion = '3.11.18'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'HOTFIX - Update pricing formula, don''t allow negative deducts/premiums'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/*************************************************
-- Date Created: 3/31/2016
-- Author: Joe Engler
-- Purpose: Compute and add the various Producer "Settlement" $$ values to a Delivered/Audited order 
-- Changes:
--      3.11.18 - JAE - 5/2/2016 - Update formula since deduct is stored as a positive number
*************************************************/
ALTER PROCEDURE spApplyRatesProducer
(
  @ID int
, @UserName varchar(100)
, @AllowReApplyPostBatch bit = 0
) AS BEGIN

	SET NOCOUNT ON
	
	IF (@AllowReApplyPostBatch = 0) AND EXISTS (SELECT * FROM tblOrderSettlementProducer WHERE OrderID = @ID AND BatchID IS NOT NULL)
	BEGIN
		RAISERROR('Invoice has already been Producer Settled', 16, 1)
		RETURN
	END
	
	/* compute the basic settlement units from the Order being invoices */
	DECLARE @SettlementUnits TABLE
	(
		OrderID int
	  , SettlementUomID int
	  , SettlementFactorID int
	  , SettlementUnits decimal(18, 10)
	)
	INSERT INTO @SettlementUnits (OrderID, SettlementUomID, SettlementFactorID, SettlementUnits)
		SELECT OSU.OrderID, OSU.SettlementUomID, OSU.SettlementFactorID, OSU.SettlementUnits
		FROM dbo.viewOrderSettlementUnitsProducer OSU
		LEFT JOIN tblOrderApproval OA ON OA.OrderID = OSU.OrderID
		WHERE OSU.OrderID = @ID

	/* compute the order level settlement rateIDs & settlement amounts - into temp table #I */
	SELECT OrderID = @ID
		, OrderDate
		, SettlementFactorID, SettlementUomID, SettlementUnits
		, CommodityPurchasePricebookID
		, CommodityPurchasePricebookPrice = ISNULL(CommodityPurchasePricebookPrice, 0)
		, CommodityPurchasePricebookDeduct = ISNULL(CommodityPurchasePricebookDeduct, 0)
		, CommodityPurchasePricebookPremium = ISNULL(CommodityPurchasePricebookPremium, 0)
		, CommodityPurchasePricebookUomID
		, CreatedByUser = @UserName
	INTO #I
	FROM (
		SELECT OrderDate
			, SettlementFactorID, SettlementUomID, SettlementUnits
			, CommodityPurchasePricebookID = OPPP.ID
			, CommodityPurchasePricebookPrice = OPPP.Price
			, CommodityPurchasePricebookDeduct = OPPP.Deduct
			, CommodityPurchasePricebookPremium = OPPP.Premium
            , CommodityPurchasePricebookUomID = OPPP.UomID
		FROM (
			SELECT O.ID
				, O.OriginID
				, O.CarrierID
				, O.RouteID
				, SU.SettlementFactorID
				, SU.SettlementUomID
				, SettlementUnits = CASE WHEN O.Rejected = 1 THEN 0 ELSE SU.SettlementUnits END
				, OrderDate = isnull(O.OrderDate, O.DueDate)
				, O.Rejected
			FROM dbo.viewOrder O
			JOIN @SettlementUnits SU ON SU.OrderID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			LEFT JOIN tblOrderApproval OA ON OA.OrderID = O.ID
			WHERE O.ID = @ID
		) X
		OUTER APPLY dbo.fnOrderProducerPurchasePricebook(@ID) OPPP
	) X2

	DECLARE @CreatedTran bit = CASE WHEN @@TRANCOUNT = 0 THEN 1 ELSE 0 END
	BEGIN TRY
		IF (@CreatedTran = 1)
			BEGIN TRAN

		-- remove the existing settlment record (if any)
		DELETE FROM tblOrderSettlementProducer WHERE OrderID = @ID
	
		-- all Units and Rates are first normalized to the Order.OriginUOM then consistent processing
		-- and persisted in the Invoice (Settlement) record in this Order.OriginUOM 
		INSERT INTO tblOrderSettlementProducer 
			(OrderID
			, OrderDate
			, SettlementFactorID 
			, SettlementUomID 
			, SettlementUnits
			, CommodityPurchasePricebookID
			, CommodityPurchasePricebookPriceAmount
			, CommodityPurchasePricebookDeductAmount
			, CommodityPurchasePricebookPremiumAmount
			, CommodityPurchasePricebookTotalAmount
			, CreatedByUser)
		SELECT OrderID
			, OrderDate
			, SettlementFactorID 
			, SettlementUomID 
			, SettlementUnits
			, CommodityPurchasePricebookID
			, CommodityPurchasePricebookPriceAmount = CommodityPurchasePricebookPrice * dbo.fnConvertUOM(SettlementUnits, SettlementUomID, CommodityPurchasePricebookUomID)
			, CommodityPurchasePricebookDeductAmount = CommodityPurchasePricebookDeduct * dbo.fnConvertUOM(SettlementUnits, SettlementUomID, CommodityPurchasePricebookUomID)
			, CommodityPurchasePricebookPremiumAmount = CommodityPurchasePricebookPremium * dbo.fnConvertUOM(SettlementUnits, SettlementUomID, CommodityPurchasePricebookUomID)
			, CommodityPurchasePricebookTotalAmount = CommodityPurchasePricebookPrice * dbo.fnConvertUOM(SettlementUnits, SettlementUomID, CommodityPurchasePricebookUomID)
													- CommodityPurchasePricebookDeduct * dbo.fnConvertUOM(SettlementUnits, SettlementUomID, CommodityPurchasePricebookUomID)
													+ CommodityPurchasePricebookPremium * dbo.fnConvertUOM(SettlementUnits, SettlementUomID, CommodityPurchasePricebookUomID)
			, CreatedByUser
		FROM #I

		IF (@CreatedTran = 1)
			COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @msg varchar(max), @severity int
		SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY()
		ROLLBACK
		RAISERROR(@msg, @severity, 1)
		RETURN
	END CATCH	
	
END

GO


COMMIT 
SET NOEXEC OFF