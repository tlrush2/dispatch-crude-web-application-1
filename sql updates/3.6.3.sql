-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.6.2'
SELECT  @NewVersion = '3.6.3'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'Report Center: fix Origin|Dest Gps Arrived to properly indicate N/A when coordinates not available'
	UNION SELECT @NewVersion, 0, 'Audit Page: fix Origin|Dest Gps Arrived to properly indicate N/A when coordinates not available'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/**********************************************************
-- Author: Kevin Alons
-- Date Created: 8 Oct 2014
-- Purpose: return the Audit table records (optionally filtered to just the specified driver and/or ordernum)
***********************************************************/
ALTER FUNCTION fnOrders_AllTickets_Audit(@carrierID int, @driverID int, @orderNum int, @id int) RETURNS TABLE AS RETURN
SELECT *
	, OriginDistanceText = CASE WHEN OriginGpsArrived = 1 THEN 'Verified' ELSE isnull(ltrim(OriginDistance), 'N/A') END
	, DestDistanceText = CASE WHEN DestGpsArrived = 1 THEN 'Verified' ELSE isnull(ltrim(DestDistance), 'N/A') END
	, HasError = cast(CASE WHEN Errors IS NULL THEN 0 ELSE 1 END as bit)
FROM (
	SELECT O.* 
		, OriginGpsLatLon = ltrim(DLO.Lat) + ',' + ltrim(DLO.Lon)
		, OriginLatLon = ltrim(OO.LAT) + ',' + ltrim(OO.LON)
		, OriginAccuracyMeters = DLO.SourceAccuracyMeters
		, OriginDistance = CASE WHEN DLO.DistanceToPoint < 0 THEN NULL ELSE DLO.DistanceToPoint END
		, OriginGpsArrived = CASE WHEN DLO.DistanceToPoint IS NOT NULL AND DLO.DistanceToPoint BETWEEN 0 AND OO.GeoFenceRadiusMeters THEN 1 ELSE 0 END
		, DestGpsLatLon = ltrim(DLD.Lat) + ',' + ltrim(DLD.Lon)
		, DestLatLon = ltrim(D.LAT) + ',' + ltrim(D.LON)
		, DestAccuracyMeters = DLD.SourceAccuracyMeters
		, DestDistance = CASE WHEN DLD.DistanceToPoint < 0 THEN NULL ELSE DLD.DistanceToPoint END
		, DestGpsArrived = CASE WHEN DLD.DistanceToPoint IS NOT NULL AND DLD.DistanceToPoint BETWEEN 0 AND D.GeoFenceRadiusMeters THEN 1 ELSE 0 END
		, OriginGrossBBLS = dbo.fnConvertUOM(O.OriginGrossUnits, O.OriginUomID, 1)
		, Errors = 
			nullif(
				SUBSTRING(
					CASE WHEN O.OriginArriveTimeUTC IS NULL THEN ',Missing Pickup Arrival' ELSE '' END
				  + CASE WHEN O.OriginDepartTimeUTC IS NULL THEN ',Missing Pickup Departure' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND O.Tickets IS NULL THEN ',No Active Tickets' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND O.DestArriveTimeUTC IS NULL THEN ',Missing Delivery Arrival' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND O.DestDepartTimeUTC IS NULL THEN ',Missing Delivery Departure' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND O.OriginGrossUnits = 0 AND O.OriginNetUnits = 0 THEN 'No Origin Units are entered' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND dbo.fnConvertUOM(O.OriginGrossUnits, O.OriginUomID, 1) NOT BETWEEN 0 AND 320 THEN ',Origin GOV Units out of limits' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND dbo.fnConvertUOM(O.OriginGrossStdUnits, O.OriginUomID, 1) NOT BETWEEN 0 AND 320 THEN ',Origin GSV Units out of limits' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND dbo.fnConvertUOM(O.OriginNetUnits, O.OriginUomID, 1) NOT BETWEEN 0 AND 320 THEN ',Origin NSV Units out of limits' ELSE '' END
				  + CASE WHEN C.RequireDispatchConfirmation = 1 AND nullif(rtrim(O.DispatchConfirmNum), '') IS NULL THEN ',Missing Dispatch Confirm #' ELSE '' END
				  + CASE WHEN O.TruckID IS NULL THEN ',Truck Missing' ELSE '' END
				  + CASE WHEN O.TrailerID IS NULL THEN ',Trailer 1 Missing' ELSE '' END
				, 2, 8000) 
			, '')
		, IsEditable = cast(CASE WHEN O.StatusID = 3 THEN 1 ELSE 0 END as bit)
	FROM viewOrder_AllTickets O
	JOIN tblOrigin OO ON OO.ID = O.OriginID
	JOIN tblCustomer C ON C.ID = OO.CustomerID
	JOIN tblDestination D ON D.ID = O.DestinationID
	LEFT JOIN tblOrderSettlementShipper OIC ON OIC.OrderID = O.ID
	LEFT JOIN viewDriverLocation_OriginFirstArrive DLO ON DLO.OrderID = O.ID AND DLO.OriginID = O.OriginID
	LEFT JOIN viewDriverLocation_DestinationFirstArrive DLD ON DLD.OrderID = O.ID AND DLD.DestinationID = O.DestinationID
	WHERE OIC.BatchID IS NULL /* don't even show SETTLED orders on the AUDIT page ever */
	  AND (isnull(@carrierID, 0) = -1 OR O.CarrierID = @carrierID)
	  AND (isnull(@driverID, 0) = -1 OR O.DriverID = @driverID)
	  AND ((nullif(@orderNum, 0) IS NULL AND O.DeleteDateUTC IS NULL AND (O.StatusID = 3 AND DeliverPrintStatusID IN (SELECT ID FROM tblPrintStatus WHERE IsCompleted = 1))) OR O.OrderNum = @orderNum)
	  AND (nullif(@id, 0) IS NULL OR O.ID LIKE @id)
) X
GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Report Center Order data
/***********************************/
ALTER VIEW viewReportCenter_Orders AS
	SELECT O.*
		, ShipperBatchNum = SS.BatchNum
		, ShipperBatchInvoiceNum = SSB.InvoiceNum
		, ShipperSettlementUomID = SS.SettlementUomID
		, ShipperSettlementUom = SS.SettlementUom
		, ShipperMinSettlementUnits = SS.MinSettlementUnits
		, ShipperSettlementUnits = SS.SettlementUnits
		, ShipperRateSheetRate = SS.RateSheetRate
		, ShipperRateSheetRateType = SS.RateSheetRateType
		, ShipperRouteRate = SS.RouteRate
		, ShipperRouteRateType = SS.RouteRateType
		, ShipperLoadRate = isnull(SS.RouteRate, SS.RateSheetRate)
		, ShipperLoadRateType = isnull(SS.RouteRateType, SS.RateSheetRateType)
		, ShipperLoadAmount = SS.LoadAmount
		, ShipperOrderRejectRate = SS.OrderRejectRate -- changed
		, ShipperOrderRejectRateType = SS.OrderRejectRateType -- new
		, ShipperOrderRejectAmount = SS.OrderRejectAmount -- changed
		, ShipperWaitFeeSubUnit = SS.WaitFeeSubUnit  -- new
		, ShipperWaitFeeRoundingType = SS.WaitFeeRoundingType  -- new
		, ShipperOriginWaitBillableHours = SS.OriginWaitBillableHours  -- new
		, ShipperOriginWaitBillableMinutes = SS.OriginWaitBillableMinutes  -- new
		, ShipperOriginWaitRate = SS.OriginWaitRate
		, ShipperOriginWaitAmount = SS.OriginWaitAmount
		, ShipperDestinationWaitBillableHours = SS.DestinationWaitBillableHours  -- new
		, ShipperDestinationWaitBillableMinutes = SS.DestinationWaitBillableMinutes   -- new
		, ShipperDestinationWaitRate = SS.DestinationWaitRate  -- changed
		, ShipperDestinationWaitAmount = SS.DestinationWaitAmount  -- changed
		, ShipperTotalWaitAmount = SS.TotalWaitAmount
		, ShipperTotalWaitBillableMinutes = nullif(ISNULL(SS.OriginWaitBillableMinutes, 0) + ISNULL(SS.DestinationWaitBillableMinutes, 0), 0)
		, ShipperTotalWaitBillableHours = nullif(ISNULL(SS.OriginWaitBillableHours, 0) + ISNULL(SS.DestinationWaitBillableHours, 0), 0)
		
		, ShipperFuelSurchargeRate = SS.FuelSurchargeRate
		, ShipperFuelSurchargeAmount = SS.FuelSurchargeAmount

		, ShipperChainupRate = SS.ChainupRate
		, ShipperChainupRateType = SS.ChainupRateType  -- new
		, ShipperChainupAmount = SS.ChainupAmount
		, ShipperRerouteRate = SS.RerouteRate
		, ShipperRerouteRateType = SS.RerouteRateType  -- new
		, ShipperRerouteAmount = SS.RerouteAmount
		, ShipperSplitLoadRate = SS.SplitLoadRate
		, ShipperSplitLoadRateType = SS.SplitLoadRateType  -- new
		, ShipperSplitLoadAmount = SS.SplitLoadAmount
		, ShipperH2SRate = SS.H2SRate
		, ShipperH2SRateType = SS.H2SRateType  -- new
		, ShipperH2SAmount = SS.H2SAmount

		, ShipperTaxRate = SS.OriginTaxRate
		, ShipperTotalAmount = SS.TotalAmount

		, CarrierBatchNum = SC.BatchNum
		, CarrierSettlementUomID = SC.SettlementUomID
		, CarrierSettlementUom = SC.SettlementUom
		, CarrierMinSettlementUnits = SC.MinSettlementUnits
		, CarrierSettlementUnits = SC.SettlementUnits
		, CarrierRateSheetRate = SC.RateSheetRate
		, CarrierRateSheetRateType = SC.RateSheetRateType
		, CarrierRouteRate = SC.RouteRate
		, CarrierRouteRateType = SC.RouteRateType
		, CarrierLoadRate = isnull(SC.RouteRate, SC.RateSheetRate)
		, CarrierLoadRateType = isnull(SC.RouteRateType, SC.RateSheetRateType)
		, CarrierLoadAmount = SC.LoadAmount
		, CarrierOrderRejectRate = SC.OrderRejectRate -- changed
		, CarrierOrderRejectRateType = SC.OrderRejectRateType -- new
		, CarrierOrderRejectAmount = SC.OrderRejectAmount -- changed
		, CarrierWaitFeeSubUnit = SC.WaitFeeSubUnit  -- new
		, CarrierWaitFeeRoundingType = SC.WaitFeeRoundingType  -- new
		, CarrierOriginWaitBillableHours = SS.OriginWaitBillableHours  -- new
		, CarrierOriginWaitBillableMinutes = SC.OriginWaitBillableMinutes  -- new
		, CarrierOriginWaitRate = SC.OriginWaitRate
		, CarrierOriginWaitAmount = SC.OriginWaitAmount
		, CarrierDestinationWaitBillableHours = SS.DestinationWaitBillableHours  -- new
		, CarrierDestinationWaitBillableMinutes = SC.DestinationWaitBillableMinutes  -- new
		, CarrierDestinationWaitRate = SC.DestinationWaitRate -- changed
		, CarrierDestinationWaitAmount = SC.DestinationWaitAmount  -- changed
		, CarrierTotalWaitAmount = SC.TotalWaitAmount
		, CarrierTotalWaitBillableMinutes = nullif(ISNULL(SS.OriginWaitBillableMinutes, 0) + ISNULL(SS.DestinationWaitBillableMinutes, 0), 0)
		, CarrierTotalWaitBillableHours = nullif(ISNULL(SS.OriginWaitBillableHours, 0) + ISNULL(SS.DestinationWaitBillableHours, 0), 0)
		
		, CarrierFuelSurchargeRate = SC.FuelSurchargeRate
		, CarrierFuelSurchargeAmount = SC.FuelSurchargeAmount

		, CarrierChainupRate = SC.ChainupRate
		, CarrierChainupRateType = SC.ChainupRateType  -- new
		, CarrierChainupAmount = SC.ChainupAmount
		, CarrierRerouteRate = SC.RerouteRate
		, CarrierRerouteRateType = SC.RerouteRateType  -- new
		, CarrierRerouteAmount = SC.RerouteAmount
		, CarrierSplitLoadRate = SC.SplitLoadRate
		, CarrierSplitLoadRateType = SC.SplitLoadRateType  -- new
		, CarrierSplitLoadAmount = SC.SplitLoadAmount
		, CarrierH2SRate = SC.H2SRate
		, CarrierH2SRateType = SC.H2SRateType  -- new
		, CarrierH2SAmount = SC.H2SAmount

		, CarrierTaxRate = SC.OriginTaxRate
		, CarrierTotalAmount = SC.TotalAmount

		, OriginGpsLatLon = ltrim(DLO.Lat) + ',' + ltrim(DLO.Lon)
		, OriginLatLon = ltrim(OO.LAT) + ',' + ltrim(OO.LON)
		, OriginAccuracyMeters = DLO.SourceAccuracyMeters
		, OriginDistance = isnull(cast(DLO.DistanceToPoint as int), 99999)
		, OriginGpsArrived = CASE WHEN DLO.DistanceToPoint IS NOT NULL AND DLO.DistanceToPoint BETWEEN 0 AND OO.GeoFenceRadiusMeters THEN 1 ELSE 0 END
		, DestGpsLatLon = ltrim(DLD.Lat) + ',' + ltrim(DLD.Lon)
		, DestLatLon = ltrim(D.LAT) + ',' + ltrim(D.LON)
		, DestAccuracyMeters = DLD.SourceAccuracyMeters
		, DestDistance = isnull(cast(DLD.DistanceToPoint as int), 99999)
		, DestGpsArrived = CASE WHEN DLD.DistanceToPoint IS NOT NULL AND DLD.DistanceToPoint BETWEEN 0 AND D.GeoFenceRadiusMeters THEN 1 ELSE 0 END
		, ShipperDestCode = CDC.Code
		, OriginCTBNum = OO.CTBNum
	FROM viewOrder_OrderTicket_Full O
	JOIN tblOrigin OO ON OO.ID = O.OriginID
	JOIN tblDestination D ON D.ID = O.DestinationID
	LEFT JOIN tblCustomerDestinationCode CDC ON CDC.CustomerID = O.CustomerID AND CDC.DestinationID = O.DestinationID
	LEFT JOIN viewDriverLocation_OriginFirstArrive DLO ON DLO.OrderID = O.ID AND DLO.OriginID = O.OriginID
	LEFT JOIN viewDriverLocation_DestinationFirstArrive DLD ON DLD.OrderID = O.ID AND DLD.DestinationID = O.DestinationID
	LEFT JOIN viewOrderSettlementCarrier SC ON SC.OrderID = O.ID
	LEFT JOIN viewOrderSettlementShipper SS ON SS.OrderID = O.ID
	LEFT JOIN tblShipperSettlementBatch SSB ON SSB.ID = SS.BatchID

GO

COMMIT
SET NOEXEC OFF