-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.7.30.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.7.30.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0

DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.7.30'
SELECT  @NewVersion = '3.7.31'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Report Center: Add total gauge inches columns.  Updated/Added Rejected columns to allow for T/F and Y/N options'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/*
	BB - 6/18/2015 (DCWEB-778) -- Add open and close gauge total inches columns to report center.	
*/
SET IDENTITY_INSERT tblReportColumnDefinition ON
  INSERT tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport)
  SELECT 90018,1,'T_OpenTotalQ/4.0','TICKET | GENERAL | Ticket Open Total Inches',NULL,NULL,0,NULL,1,'*',0
  UNION
  SELECT 90019,1,'T_CloseTotalQ/4.0','TICKET | GENERAL | Ticket Close Total Inches',NULL,NULL,0,NULL,1,'*',0
  EXCEPT SELECT ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport FROM tblReportColumnDefinition
SET IDENTITY_INSERT tblReportColumnDefinition OFF

/*
	BB - 6/18/2015 (DCWEB-779) -- Update current report center reject indicator columns to display they are True/False format not Yes/No.  Also fix filter text to show TRUE & FALSE instead of Yes & No (it was misleading).
*/
UPDATE tblReportColumnDefinition 
SET FilterDropDownSql = 'SELECT ID=1, Name=''TRUE'' UNION SELECT 0, ''FALSE'''
,Caption = 'GENERAL | Order Rejected? (T/F)'
WHERE ID = 11
  
UPDATE tblReportColumnDefinition 
SET FilterDropDownSql = 'SELECT ID=1, Name=''TRUE'' UNION SELECT 0, ''FALSE'''
,Caption = 'TICKET | GENERAL | Ticket Rejected (T/F)'
WHERE ID = 109
  
UPDATE tblReportColumnDefinition 
SET FilterAllowCustomText = 0
,Caption = 'GAUGER | TICKETS | Gauger Ticket Rejected (T/F)'
WHERE ID = 261
  
UPDATE tblReportColumnDefinition 
SET FilterAllowCustomText = 0 
,Caption = 'GAUGER | Gauger Rejected (T/F)'
WHERE ID = 265

/*
	BB - 6/18/2015 (DCWEB-779) -- Add Rejected? Yes/No columns to report center
*/
SET IDENTITY_INSERT tblReportColumnDefinition ON
  INSERT tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport)
  SELECT 90020,1,'CASE WHEN Rejected = ''FALSE'' THEN CONVERT(VARCHAR,''No'',0) WHEN Rejected = ''TRUE'' THEN CONVERT(VARCHAR,''Yes'',0) ELSE '''' END','GENERAL | Order Rejected? (Y/N)',NULL,NULL,5,'SELECT ID=1, Name=''Yes'' UNION SELECT 0, ''No''',0,'*',0
  UNION
  SELECT 90021,1,'CASE WHEN T_Rejected = ''FALSE'' THEN CONVERT(VARCHAR,''No'',0) WHEN T_Rejected = ''TRUE'' THEN CONVERT(VARCHAR,''Yes'',0) ELSE '''' END','TICKET | GENERAL | Ticket Rejected (Y/N)',NULL,NULL,5,'SELECT ID=1, Name=''Yes'' UNION SELECT 0, ''No''',0,'*',0
  UNION
  SELECT 90022,1,'CASE WHEN T_GaugerRejected = ''FALSE'' THEN CONVERT(VARCHAR,''No'',0) WHEN T_GaugerRejected = ''TRUE'' THEN CONVERT(VARCHAR,''Yes'',0) ELSE '''' END','GAUGER | TICKETS | Gauger Ticket Rejected (Y/N)',NULL,NULL,5,'SELECT ID=1, Name=''Yes'' UNION SELECT 0, ''No''',0,'*',0
  UNION
  SELECT 90023,1,'CASE WHEN GaugerRejected = ''FALSE'' THEN CONVERT(VARCHAR,''No'',0) WHEN GaugerRejected = ''TRUE'' THEN CONVERT(VARCHAR,''Yes'',0) ELSE '''' END','GAUGER | Gauger Rejected (Y/N)',NULL,NULL,5,'SELECT ID=1, Name=''Yes'' UNION SELECT 0, ''No''',0,'*',0  
  EXCEPT SELECT ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport FROM tblReportColumnDefinition
SET IDENTITY_INSERT tblReportColumnDefinition OFF


COMMIT
SET NOEXEC OFF