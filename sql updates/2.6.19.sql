/*  
	-- add strapping table support to DB
*/ 
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.6.18b'
SELECT  @NewVersion = '2.6.19'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

CREATE TABLE tblOriginTank
(
  ID int identity(1, 1) NOT NULL CONSTRAINT PK_OriginTank PRIMARY KEY
, OriginID int NOT NULL CONSTRAINT FK_OriginTank_Origin FOREIGN KEY REFERENCES tblOrigin(ID) ON DELETE CASCADE
, TankNum varchar(20) NOT NULL 
, TankDescription varchar(20) NOT NULL
, CreateDateUTC datetime NOT NULL CONSTRAINT DF_OriginTank_CreateDateUTC DEFAULT (getutcdate())
, CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_OriginTank_CreatedByUser DEFAULT (SUSER_NAME())
, LastChangeDateUTC datetime NULL
, LastChangedByUser varchar(100) NULL
, DeleteDateUTC datetime NULL
, DeletedByUser varchar(100) NULL
)
GO
CREATE UNIQUE INDEX udxOriginTank_Main ON tblOriginTank(OriginID, TankNum)
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblOriginTank TO dispatchcrude_iis_acct
GO

CREATE TABLE tblOriginTankStrapping
(
  ID int identity(1, 1) NOT NULL
, OriginTankID int NOT NULL CONSTRAINT FK_OriginTankStrapping_OriginTank FOREIGN KEY REFERENCES tblOriginTank(ID)
, Feet tinyint NOT NULL
, Inches tinyint NOT NULL CONSTRAINT CK_OriginTankStrapping_Inches CHECK (Inches BETWEEN 0 AND 11)
, Q tinyint NOT NULL CONSTRAINT CK_OriginTankStrapping_Q CHECK (Q BETWEEN 0 AND 3)
, BPQ decimal(18, 10) NOT NULL CONSTRAINT CK_OriginTankStrapping_BPQ CHECK (BPQ >= 0)
, IsMinimum bit NOT NULL CONSTRAINT DF_OriginTankStrapping_IsMinimum DEFAULT (0)
, IsMaximum bit NOT NULL CONSTRAINT DF_OriginTankStrapping_IsMaximum DEFAULT (0)
, CreateDateUTC datetime NOT NULL CONSTRAINT DF_OriginTankStrapping_CreateDateUTC DEFAULT (getutcdate())
, CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_OriginTankStrapping_CreatedByUser DEFAULT (SUSER_NAME())
, LastChangeDateUTC datetime NULL
, LastChangedByUser varchar(100) NULL
)
CREATE UNIQUE INDEX udxOriginTankStrapping_Main ON tblOriginTankStrapping(OriginTankID, Feet, Inches, Q)
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblOriginTankStrapping TO dispatchcrude_iis_acct
GO

-- add some basic Catchall Origin tank records for existing active well origins
INSERT INTO tblOriginTank (OriginID, TankNum, TankDescription, CreatedByUser)
	SELECT O.ID, '*', rtrim(TT.HeightFeet) + 'FT|' + RTRIM(TT.CapacityBarrels) + 'BBLS', 'System' 
	FROM tblOrigin O
	JOIN tblTankType TT ON TT.ID = O.TankTypeID
	WHERE Active = 1 AND OriginTypeID IN (2)
	
INSERT INTO tblOriginTankStrapping (OriginTankID, Feet, Inches, Q, BPQ, IsMinimum, CreatedByUser)
	SELECT OT.ID, 0, 0, 0, 0, 1, 'System'
	FROM tblOriginTank OT
	WHERE TankNum = '*'
INSERT INTO tblOriginTankStrapping (OriginTankID, Feet, Inches, Q, BPQ, IsMaximum, CreatedByUser)
	SELECT OT.ID, isnull(TT.HeightFeet, 20), 0, 0, isnull(TT.BarrelsPerInch / 4, 0.4175), 1, 'System'
	FROM tblOriginTank OT
	JOIN tblOrigin O ON O.ID = OT.OriginID
	left JOIN tblTankType TT ON TT.ID = O.TankTypeID
	WHERE OT.TankNum = '*'

GO

ALTER TABLE tblOriginType ADD HasTanks bit NOT NULL CONSTRAINT DF_OriginType_HasTanks DEFAULT (0)
GO
UPDATE tblOriginType SET HasTanks = 1 WHERE ID = 2
GO

ALTER TABLE tblTicketType ADD ForTanksOnly bit NOT NULL CONSTRAINT DF_TicketType_ForTanksOnly DEFAULT (0)
GO
UPDATE tblTicketType SET ForTanksOnly = 1 WHERE ID IN (1,2)
GO

ALTER TABLE tblOrder ADD OriginTankID int NULL
GO
UPDATE tblOrder SET OriginTankID = OT.ID
FROM tblOrder O
JOIN tblOrigin OO ON OO.ID = O.OriginID
JOIN tblOriginTank OT ON OT.OriginID = OO.ID AND OT.TankNum = '*'
GO

ALTER TABLE tblOrderTicket DROP CONSTRAINT DF_OrderTicket_TankTypeID
GO
ALTER TABLE tblOrderTicket DROP CONSTRAINT FK_OrderTicket_TankType
GO
ALTER TABLE tblOrderTicket DROP COLUMN TankTypeID
GO
ALTER TABLE tblOrderTicket DROP COLUMN BarrelsPerInch
GO
ALTER TABLE tblOrigin DROP CONSTRAINT DF_tblOrigin_TankTypeID
GO
ALTER TABLE tblOrigin DROP COLUMN TankTypeID
GO
ALTER TABLE tblOrigin DROP COLUMN BarrelsPerInch
GO
ALTER TABLE tblOrigin ADD NDM varchar(25) NULL
GO
ALTER TABLE tblOrigin ADD CA varchar(25) NULL
GO

ALTER TABLE tblOrderTicket ADD OriginTankID int NULL CONSTRAINT FK_OrderTicket_OriginTank FOREIGN KEY REFERENCES tblOriginTank(ID)
GO

ALTER TABLE tblOrder ADD OriginGrossStdUnits decimal(18, 6)
GO
ALTER TABLE tblOrderTicket ADD GrossStdUnits decimal(18, 6)
GO

ALTER TABLE tblOrderTicket ADD BottomFeet tinyint NULL
GO
ALTER TABLE tblOrderTicket ADD BottomInches tinyint NULL
GO
ALTER TABLE tblOrderTicket ADD BottomQ tinyint NULL
GO

ALTER TABLE tblOrder ADD DispatchConfirmNum varchar(25) NULL
GO
ALTER TABLE tblOrder ADD DispatchNotes varchar(500) NULL
GO
ALTER TABLE tblOrder ADD DriverNotes varchar(255) NULL
GO

ALTER TABLE tblCarrier ADD Authority varchar(30) NULL
GO

ALTER TABLE tblCustomer ADD RequireDispatchConfirmation bit NOT NULL CONSTRAINT DF_Customer_RequireDispatchConfirmation DEFAULT (0)
GO

/***********************************/
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Origin records with translated value and FullName field (which includes Origin Type)
/***********************************/
ALTER VIEW [dbo].[viewOrigin] AS
SELECT O.*
	, CASE WHEN O.H2S = 1 THEN 'H2S-' ELSE '' END + OT.OriginType + ' - ' + O.Name AS FullName
	, OT.OriginType
	, S.FullName AS State
	, S.Abbreviation AS StateAbbrev
	, OP.Name AS Operator
	, P.FullName AS Pumper
	, TT.Name AS TicketType
	, TT.ForTanksOnly
	, R.Name AS Region
	, C.Name AS Customer
	, PR.Name AS Producer
	, TZ.Name AS TimeZone
	, UOM.Name AS UOM
	, UOM.Abbrev AS UomShort
	, OT.HasTanks
	, (SELECT COUNT(*) FROM tblOriginTank WHERE OriginID = O.ID AND DeleteDateUTC IS NULL) AS TankCount
FROM dbo.tblOrigin O
JOIN dbo.tblOriginType OT ON OT.ID = O.OriginTypeID
LEFT JOIN tblState S ON S.ID = O.StateID
LEFT JOIN dbo.tblOperator OP ON OP.ID = O.OperatorID
LEFT JOIN dbo.viewPumper P ON P.ID = O.PumperID
LEFT JOIN dbo.tblTicketType TT ON TT.ID = O.TicketTypeID
LEFT JOIN dbo.tblRegion R ON R.ID = O.RegionID
LEFT JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
LEFT JOIN dbo.tblProducer PR ON PR.ID = O.ProducerID
LEFT JOIN dbo.tblTimeZone TZ ON TZ.ID = O.TimeZoneID
LEFT JOIN dbo.tblUom UOM ON UOM.ID = O.UomID

GO

/***********************************/
-- Date Created: 12 Mar 2014
-- Author: Kevin Alons
-- Purpose: return Origin Tank records with translated values
/***********************************/
CREATE VIEW [dbo].[viewOriginTank] AS
	SELECT OT.*
		, O.Name AS Origin
		, CASE WHEN OT.TankNum = '*' THEN '[Enter Unstrapped]' ELSE OT.TankNum END AS TankNum_Unstrapped
	FROM dbo.tblOriginTank OT
	JOIN dbo.viewOrigin O ON O.ID = OT.OriginID

GO
GRANT SELECT ON viewOriginTank TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 15 Mar 2014
-- Author: Kevin Alons
-- Purpose: return Origin Tank Strapping Table records with translated values
/***********************************/
CREATE VIEW viewOriginTankStrapping AS
	SELECT OTST.*
		, dbo.fnGaugeQtrInches(Feet, Inches, Q) AS TotalQ
		, O.Name AS Origin
		, OT.TankNum
	FROM tblOriginTankStrapping OTST
	JOIN tblOriginTank OT ON OT.ID = OTST.OriginTankID
	JOIN tblOrigin O ON O.ID = OT.OriginID
GO
GRANT SELECT ON viewOriginTankStrapping TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 16 Mar 2014
-- Author: Kevin Alons
-- Purpose: return a virtual fully populated OriginTank Strapping Table (with running Barrels)
/***********************************/
CREATE FUNCTION dbo.fnOriginTankStrapping(@originTankID int) 
RETURNS 
	@OriginTankStrapping TABLE (
		ID int
		, OriginTankID int
		, Feet tinyint
		, Inches tinyint
		, Q tinyint
		, TotalQ int
		, BPQ decimal(18, 10)
		, Barrels decimal(18, 10)
		, IsMinimum bit
		, IsMaximum bit
	)
AS BEGIN
	INSERT INTO @OriginTankStrapping
		SELECT ID
			, OriginTankID
			, Feet
			, Inches
			, Q
			, TotalQ
			, BPQ
			, 0
			, IsMinimum
			, IsMaximum bit
		FROM dbo.viewOriginTankStrapping
		WHERE OriginTankID = @originTankID
		ORDER BY TotalQ
		
	DECLARE @workTotalQ int, @lastTotalQ int, @workBPQ decimal(18, 10), @Barrels decimal(18, 10)
	SELECT @workTotalQ = MIN(TotalQ), @lastTotalQ = MAX(TotalQ) FROM @OriginTankStrapping
	SELECT TOP 1 @workBPQ = BPQ, @Barrels = TotalQ * BPQ FROM @OriginTankStrapping ORDER BY TotalQ
	
	WHILE @workTotalQ <= @lastTotalQ
	BEGIN
		IF ((SELECT COUNT(*) FROM @OriginTankStrapping WHERE TotalQ = @workTotalQ) > 0)
			UPDATE @OriginTankStrapping SET Barrels = @Barrels WHERE TotalQ = @workTotalQ
		ELSE
			INSERT INTO @OriginTankStrapping (OriginTankID, Feet, Inches, Q, TotalQ
					, BPQ, Barrels, IsMinimum, IsMaximum)
				VALUES (@originTankID, @workTotalQ / 48, (@workTotalQ % 48) / 4, @workTotalQ % 4, @workTotalQ
					, @workBPQ, @Barrels, 0, 0)
		-- advance to the next workTotalQ	
		SET @workTotalQ = @workTotalQ + 1				
		-- get the next appropriate BPQ value (which is the next higher)
		SELECT TOP 1 @workBPQ = BPQ FROM @OriginTankStrapping WHERE TotalQ > @workTotalQ ORDER BY TotalQ ASC
		-- compute the next total Barrels value
		SELECT @Barrels = @Barrels + @workBPQ
	END

	RETURN	
END

GO
GRANT SELECT ON dbo.fnOriginTankStrapping TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 24 Apr 2013
-- Author: Kevin Alons
-- Purpose: return the running total Barrels for the specified Strapping Level (Feet/Inches/Q)
/***********************************/
CREATE FUNCTION [dbo].[fnOriginTankStrappingLevelBarrels](@originTankID int, @feet smallint, @inch tinyint, @q tinyint) RETURNS decimal(18, 10) AS
BEGIN
	DECLARE @ret decimal(18, 10)
	SELECT @ret = Barrels FROM dbo.fnOriginTankStrapping(@originTankID) WHERE Feet = @feet AND Inches = @inch AND Q = @q
	RETURN (@ret)
END

GO
GRANT EXECUTE ON dbo.fnOriginTankStrappingLevelBarrels TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 24 Apr 2013
-- Author: Kevin Alons
-- Purpose: return the running total Barrels for the specified Strapping Level (Feet/Inches/Q)
/***********************************/
CREATE FUNCTION [dbo].[fnOriginTankStrappingLevelDeltaBarrels](@originTankID int, @openFeet smallint, @openInch tinyint, @openQ tinyint, @closeFeet smallint, @closeInch tinyint, @closeQ tinyint) RETURNS decimal(18, 10) AS
BEGIN
	DECLARE @openBarrels decimal(18, 10), @closeBarrels decimal(18, 10)
	SELECT @openBarrels = Barrels FROM dbo.fnOriginTankStrapping(@originTankID) WHERE Feet = @openFeet AND Inches = @openInch AND Q = @openQ
	SELECT @closeBarrels = Barrels FROM dbo.fnOriginTankStrapping(@originTankID) WHERE Feet = @closeFeet AND Inches = @closeInch AND Q = @closeQ
	RETURN (@openBarrels - @closeBarrels)
END

GO
GRANT EXECUTE ON dbo.fnOriginTankStrappingLevelDeltaBarrels TO dispatchcrude_iis_acct
GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 19 Dec 2012
-- Description:	trigger to ensure the isMinimum/isMaximum fields are only uniquely specified per origin tank
-- =============================================
CREATE TRIGGER [dbo].[trigOriginTankStrapping_IU] ON [dbo].[tblOriginTankStrapping] AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	-- ensure only the last specified record for each OriginTank is marked as IsMinimum
	UPDATE tblOriginTankStrapping
	  SET IsMinimum = 0 
	WHERE OriginTankID IN (SELECT DISTINCT OriginTankID FROM inserted WHERE IsMinimum <> 0)
	  AND ID NOT IN (SELECT ID FROM inserted WHERE IsMinimum <> 0)
	-- ensure only the last specified record for each OriginTank is marked as IsMaximum
	UPDATE tblOriginTankStrapping
	  SET IsMaximum = 0 
	WHERE OriginTankID IN (SELECT DISTINCT OriginTankID FROM inserted WHERE IsMaximum <> 0)
	  AND ID NOT IN (SELECT ID FROM inserted WHERE IsMaximum <> 0)

END

GO

/***********************************/
-- Date Created: 16 Mar 2014
-- Author: Kevin Alons
-- Purpose: return a virtual fully populated OriginTank Strapping Table (with running Barrels)
/***********************************/
ALTER FUNCTION [dbo].[fnOriginTankStrapping](@originTankID int) 
RETURNS 
	@OriginTankStrapping TABLE (
		ID int
		, OriginTankID int
		, Feet tinyint
		, Inches tinyint
		, Q tinyint
		, TotalQ int
		, BPQ decimal(18, 10)
		, Barrels decimal(18, 10)
		, IsMinimum bit
		, IsMaximum bit
		, CreateDateUTC datetime
		, CreatedByUser varchar(100)
		, LastChangeDateUTC datetime
		, LastChangedByUser varchar(100)
		, Origin varchar(50)
		, TankNum varchar(20)
	)
AS BEGIN
	INSERT INTO @OriginTankStrapping
		SELECT ID
			, OriginTankID
			, Feet
			, Inches
			, Q
			, TotalQ
			, BPQ
			, 0
			, IsMinimum
			, IsMaximum 
			, CreateDateUTC
			, CreatedByUser 
			, LastChangeDateUTC
			, LastChangedByUser
			, Origin
			, TankNum
		FROM dbo.viewOriginTankStrapping
		WHERE OriginTankID = @originTankID
		ORDER BY TotalQ
		
	DECLARE @origin varchar(50), @tankNum varchar(20)
	SELECT @origin = MIN(Origin), @tankNum = MIN(TankNum) FROM @OriginTankStrapping
	DECLARE @workTotalQ int, @lastTotalQ int, @workBPQ decimal(18, 10), @Barrels decimal(18, 10)
	SELECT @workTotalQ = MIN(TotalQ), @lastTotalQ = MAX(TotalQ) FROM @OriginTankStrapping
	SELECT TOP 1 @workBPQ = BPQ, @Barrels = TotalQ * BPQ FROM @OriginTankStrapping ORDER BY TotalQ
	
	WHILE @workTotalQ <= @lastTotalQ
	BEGIN
		IF ((SELECT COUNT(*) FROM @OriginTankStrapping WHERE TotalQ = @workTotalQ) > 0)
			UPDATE @OriginTankStrapping SET Barrels = @Barrels WHERE TotalQ = @workTotalQ
		ELSE
			INSERT INTO @OriginTankStrapping (OriginTankID, Feet, Inches, Q, TotalQ
					, BPQ, Barrels, IsMinimum, IsMaximum, Origin, TankNum)
				VALUES (@originTankID, @workTotalQ / 48, (@workTotalQ % 48) / 4, @workTotalQ % 4, @workTotalQ
					, @workBPQ, @Barrels, 0, 0, @origin, @tankNum)
		-- advance to the next workTotalQ	
		SET @workTotalQ = @workTotalQ + 1				
		-- get the next appropriate BPQ value (which is the next higher)
		SELECT TOP 1 @workBPQ = BPQ FROM @OriginTankStrapping WHERE TotalQ > @workTotalQ ORDER BY TotalQ ASC
		-- compute the next total Barrels value
		SELECT @Barrels = @Barrels + @workBPQ
	END

	RETURN	
END

GO

/***********************************/
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Order records with "translated friendly" values for FK relationships
/***********************************/
ALTER VIEW [dbo].[viewOrder] AS
SELECT O.*
	, OPS.OrderStatus AS PrintStatus
FROM (
	SELECT O.*
	, dbo.fnDateOnly(dbo.fnUTC_to_Local(O.OriginDepartTimeUTC, vO.TimeZoneID, vO.UseDST)) AS OrderDate
	, vO.Name AS Origin
	, vO.FullName AS OriginFull
	, vO.State AS OriginState
	, vO.StateAbbrev AS OriginStateAbbrev
	, vO.Station AS OriginStation
	, vO.LeaseName
	, vO.LeaseNum
	, vO.NDICFileNum AS OriginNDIC
	, vO.NDM AS OriginNDM
	, vO.CA AS OriginCA
	, vO.TimeZoneID AS OriginTimeZoneID
	, vO.UseDST AS OriginUseDST
	, vO.H2S
	, vD.Name AS Destination
	, vD.FullName AS DestinationFull
	, vD.State AS DestinationState
	, vD.StateAbbrev AS DestinationStateAbbrev
	, vD.DestinationType
	, vD.Station AS DestStation
	, vD.TimeZoneID AS DestTimeZoneID
	, vD.UseDST AS DestUseDST
	, C.Name AS Customer
	, CA.Name AS Carrier
	, CT.Name AS CarrierType
	, OS.OrderStatus
	, OS.StatusNum
	, D.FullName AS Driver
	, D.FirstName AS DriverFirst
	, D.LastName AS DriverLast
	, TRU.FullName AS Truck
	, TR1.FullName AS Trailer
	, TR2.FullName AS Trailer2
	, P.PriorityNum
	, TT.Name AS TicketType
	, vD.TicketTypeID AS DestTicketTypeID
	, vD.TicketType AS DestTicketType
	, OP.Name AS Operator
	, PR.Name AS Producer
	, PU.FullName AS Pumper
	, D.IDNumber AS DriverNumber
	, CA.IDNumber AS CarrierNumber
	, TRU.IDNumber AS TruckNumber
	, TR1.IDNumber AS TrailerNumber
	, TR2.IDNumber AS Trailer2Number
	, PRO.Name as Product
	, PRO.ShortName AS ProductShort
	, OUom.Name AS OriginUOM
	, OUom.Abbrev AS OriginUomShort
	, CASE WHEN ORT.TankNum = '*' THEN '*' + isnull(O.OriginTankNum, '?') ELSE ORT.TankNum END AS OriginTankID_Text
	, DUom.Name AS DestUOM
	, DUom.Abbrev AS DestUomShort
	, cast((CASE WHEN O.DeleteDateUTC IS NULL THEN 1 ELSE 0 END) as bit) AS Active
	, cast((CASE WHEN O.DeleteDateUTC IS NOT NULL THEN 1 ELSE 0 END) as bit) AS IsDeleted
	, PPS.Name AS PickupPrintStatus
	, PPS.IsCompleted AS PickupCompleted
	, DPS.Name AS DeliverPrintStatus
	, DPS.IsCompleted AS DeliverCompleted
	, CASE WHEN StatusID = 8 AND PPS.IsCompleted = 0 THEN 7
		   WHEN StatusID = 3 AND DPS.IsCompleted = 0 THEN 8
		   ELSE StatusID END AS PrintStatusID
	FROM dbo.tblOrder O
	LEFT JOIN dbo.viewOrigin vO ON vO.ID = O.OriginID
	LEFT JOIN dbo.viewDestination vD ON vD.ID = O.DestinationID
	LEFT JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
	JOIN dbo.tblOrderStatus AS OS ON OS.ID = O.StatusID
	LEFT JOIN dbo.tblCarrier CA ON CA.ID = O.CarrierID
	LEFT JOIN dbo.tblCarrierType CT ON CT.ID = CA.CarrierTypeID
	LEFT JOIN dbo.viewDriver D ON D.ID = O.DriverID
	LEFT JOIN dbo.viewTruck TRU ON TRU.ID = O.TruckID
	LEFT JOIN dbo.viewTrailer TR1 ON TR1.ID = O.TrailerID
	LEFT JOIN dbo.viewTrailer TR2 ON TR2.ID = O.Trailer2ID
	LEFT JOIN dbo.tblPriority P ON P.ID = O.PriorityID
	LEFT JOIN dbo.tblTicketType TT ON TT.ID = O.TicketTypeID
	LEFT JOIN dbo.tblOriginTank ORT ON ORT.ID = O.OriginTankID
	LEFT JOIN dbo.tblOperator OP ON OP.ID = O.OperatorID
	LEFT JOIN dbo.viewPumper PU ON PU.ID = O.PumperID
	LEFT JOIN dbo.tblProducer PR ON PR.ID = O.ProducerID
	LEFT JOIN dbo.tblProduct PRO ON PRO.ID = O.ProductID
	LEFT JOIN dbo.tblUom OUom ON OUom.ID = O.OriginUomID
	LEFT JOIN dbo.tblUom DUom ON DUom.ID = O.DestUomID
	LEFT JOIN dbo.tblPrintStatus PPS ON PPS.ID = O.PickupPrintStatusID
	LEFT JOIN dbo.tblPrintStatus DPS ON DPS.ID = O.DeliverPrintStatusID
) O
LEFT JOIN dbo.tblOrderStatus OPS ON OPS.ID = O.PrintStatusID

GO

/***********************************/
-- Date Created: 22 Jan 2013
-- Author: Kevin Alons
-- Purpose: query the tblOrderTicket table and include "friendly" translated values
/***********************************/
ALTER VIEW [dbo].[viewOrderTicket] AS
SELECT OT.*
	, TT.Name AS TicketType
	, CASE WHEN OT.OriginTankID IS NULL OR ORT.TankNum = '*' THEN OT.TankNum ELSE ORT.TankNum END AS OriginTankText
	, cast(CASE WHEN OT.OriginTankID IS NULL OR ORT.TankNum = '*' THEN 0 ELSE 1 END as bit) AS IsStrappedTank
	, cast((CASE WHEN OT.DeleteDateUTC IS NULL THEN 1 ELSE 0 END) as bit) AS Active
	, cast((CASE WHEN OT.DeleteDateUTC IS NOT NULL THEN 1 ELSE 0 END) as bit) AS IsDeleted
FROM tblOrderTicket OT
JOIN tblTicketType TT ON TT.ID = OT.TicketTypeID
LEFT JOIN tblOriginTank ORT ON ORT.ID = OT.OriginTankID

GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details into a single view
/***********************************/
ALTER VIEW [dbo].[viewOrder_OrderTicket_Full] AS 
	SELECT OE.* 
		, CASE WHEN OE.TicketCount = 0 THEN OE.TicketType ELSE OT.TicketType END AS T_TicketType
		, CASE WHEN OE.TicketCount = 0 THEN OE.CarrierTicketNum ELSE OT.CarrierTicketNum END AS T_CarrierTicketNum
		, CASE WHEN OE.TicketCount = 0 THEN OE.OriginBOLNum ELSE OT.BOLNum END AS T_BOLNum
		, OT.OriginTankText AS T_TankNum
		, OT.IsStrappedTank AS T_IsStrappedTank
		, OT.OpeningGaugeFeet AS T_OpeningGaugeFeet, OT.OpeningGaugeInch AS T_OpeningGaugeInch, OT.OpeningGaugeQ AS T_OpeningGaugeQ
		, OT.ClosingGaugeFeet AS T_ClosingGaugeFeet, OT.ClosingGaugeInch AS T_ClosingGaugeInch, OT.ClosingGaugeQ AS T_ClosingGaugeQ
		, dbo.fnGaugeQtrInches(OT.OpeningGaugeFeet, OT.OpeningGaugeInch, OT.OpeningGaugeQ) AS T_OpenTotalQ
		, dbo.fnGaugeQtrInches(OT.ClosingGaugeFeet, OT.ClosingGaugeInch, ot.ClosingGaugeQ) AS T_CloseTotalQ
		, ltrim(OT.OpeningGaugeFeet) + 'ft ' + ltrim(OT.OpeningGaugeInch) + 'in ' + ltrim(OT.OpeningGaugeQ) + 'q' AS T_OpenReading
		, ltrim(OT.ClosingGaugeFeet) + 'ft ' + ltrim(OT.ClosingGaugeInch) + 'in ' + ltrim(OT.ClosingGaugeQ) + 'q' AS T_CloseReading
		-- using cast(xx as decimal(9,4)) to properly round to 3 decimal digits (with no trailing 0's)
		, ltrim(round(cast(dbo.fnCorrectedAPIGravity(OT.ProductObsGravity, OT.ProductObsTemp) as decimal(9,4)), 9, 4)) AS T_CorrectedAPIGravity
		, ltrim(OT.SealOff) AS T_SealOff
		, ltrim(OT.SealOn) AS T_SealOn
		, OT.ProductObsTemp AS T_ProductObsTemp
		, OT.ProductObsGravity AS T_ProductObsGravity
		, OT.ProductBSW AS T_ProductBSW
		, OT.Rejected AS T_Rejected
		, OT.RejectNotes AS T_RejectNotes
		, OT.GrossUnits AS T_GrossUnits
		, OT.NetUnits AS T_NetUnits
		, dbo.fnRerouteDetails(OE.ID, 'PreviousDestinations', '<br/>') AS PreviousDestinations
		, dbo.fnRerouteDetails(OE.ID, 'RerouteUsers', '<br/>') AS RerouteUsers
		, dbo.fnRerouteDetails(OE.ID, 'RerouteDates', '<br/>') AS RerouteDates
		, dbo.fnRerouteDetails(OE.ID, 'RerouteNotes', '<br/>') AS RerouteNotes
	FROM dbo.viewOrderExportFull OE
	LEFT JOIN viewOrderTicket OT ON OT.OrderID = OE.ID AND OT.DeleteDateUTC IS NULL
	WHERE OE.DeleteDateUTC IS NULL
	
GO

/***********************************/
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: create new Order (loads) for the specified criteria
/***********************************/
ALTER PROCEDURE [dbo].[spCreateLoads]
(
  @OriginID int
, @DestinationID int
, @TicketTypeID int
, @DueDate datetime
, @CustomerID int
, @CarrierID int = NULL
, @DriverID int = NULL
, @Qty int
, @UserName varchar(100)
, @OriginTankID int = NULL
, @OriginTankNum varchar(20) = NULL
, @OriginBOLNum varchar(15) = NULL
, @PriorityID int = 3 -- LOW priority
, @StatusID smallint = -10 -- GENERATED
, @ProductID int = 1 -- basic Crude Oil
) AS
BEGIN
	DECLARE @PumperID int, @OperatorID int, @ProducerID int, @OriginUomID int, @DestUomID int
	SELECT @PumperID = PumperID, @OperatorID = OperatorID, @ProducerID = ProducerID, @OriginUomID = UomID
	FROM tblOrigin 
	WHERE ID = @OriginID
	SELECT @DestUomID = UomID FROM tblDestination WHERE ID = @DestinationID

	DECLARE @incrementBOL bit
	SELECT @incrementBOL = CASE WHEN @OriginBOLNum LIKE '%+' THEN 1 ELSE 0 END
	IF (@incrementBOL = 1) SELECT @OriginBOLNum = left(@OriginBOLNum, len(@OriginBOLNum) - 1)
	
	DECLARE @i int
	SELECT @i = 0
	
	WHILE @i < @Qty BEGIN
		
		INSERT INTO dbo.tblOrder (OriginID, OriginUomID, DestinationID, DestUomID, TicketTypeID, DueDate, CustomerID
				, CarrierID, DriverID, StatusID, PriorityID, OriginTankID, OriginTankNum, OriginBOLNum
				, OrderNum, ProductID, PumperID, OperatorID, ProducerID, CreateDateUTC, CreatedByUser)
			VALUES (@OriginID, @OriginUomID, @DestinationID, @DestUomID, @TicketTypeID, @DueDate, @CustomerID
				, @CarrierID, @DriverID, @StatusID, @PriorityID, @OriginTankID, @OriginTankNum, @OriginBOLNum
				, (SELECT isnull((SELECT max(OrderNum) FROM tblOrder), 100000) + 1), @ProductID
				, @PumperID, @OperatorID, @ProducerID, GETUTCDATE(), @UserName)
		
		IF (@DriverID IS NOT NULL)
		BEGIN
			UPDATE tblOrder SET TruckID = D.TruckID, TrailerID = D.TrailerID, Trailer2ID = D.Trailer2ID
			FROM tblOrder O
			JOIN tblDriver D ON D.ID = O.DriverID
			WHERE O.ID = SCOPE_IDENTITY()
		END
		
		IF (@incrementBOL = 1 AND isnumeric(@OriginBOLNum) = 1) SELECT @OriginBOLNum = rtrim(cast(@OriginBOLNum as int) + 1)

		SET @i = @i + 1
	END
END

GO

DROP TRIGGER trigOrderTicket_IU
GO
DROP TRIGGER trigOrderTicket_IU_Validate
GO

UPDATE tblOrderTicket SET OriginTankID = O.OriginTankID
FROM tblOrderTicket OT
JOIN tblOrder O ON O.ID = OT.OrderID
GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 6 Feb 2013
-- Description:	trigger to ensure the GrossBarrels and NetBarrels are computed for valid, entered data
-- =============================================
CREATE TRIGGER [dbo].[trigOrderTicket_IU] ON [dbo].[tblOrderTicket] AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	-- re-compute GaugeRun ticket Gross barrels
	IF UPDATE(OriginTankID) OR UPDATE(OpeningGaugeFeet) OR UPDATE(OpeningGaugeInch) OR UPDATE(OpeningGaugeQ)
		OR UPDATE (ClosingGaugeFeet) OR UPDATE(ClosingGaugeInch) OR UPDATE(ClosingGaugeQ) 
		OR UPDATE (GrossUnits)
	BEGIN
		UPDATE tblOrderTicket
		  SET GrossUnits = dbo.fnConvertUom(dbo.fnOriginTankStrappingLevelDeltaBarrels(
				OT.OriginTankID
			  , OpeningGaugeFeet
			  , OpeningGaugeInch
			  , OpeningGaugeQ
			  , ClosingGaugeFeet
			  , ClosingGaugeInch
			  , ClosingGaugeQ), 1, O.OriginUomID) -- ensure this value is always normalized from BARRELS
		FROM tblOrderTicket OT
		JOIN tblOrder O ON O.ID = OT.OrderID
		WHERE OT.ID IN (SELECT ID FROM inserted) 
		  AND OT.TicketTypeID = 1 -- Gauge Run Ticket
		  AND OT.OpeningGaugeFeet IS NOT NULL
		  AND OT.OpeningGaugeInch IS NOT NULL
		  AND OT.OpeningGaugeQ IS NOT NULL
		  AND OT.ClosingGaugeFeet IS NOT NULL
		  AND OT.ClosingGaugeInch IS NOT NULL
		  AND OT.ClosingGaugeQ IS NOT NULL
	END
	-- re-compute GaugeRun | Net Barrel tickets NetUnits
	IF UPDATE(OpeningGaugeFeet) OR UPDATE(OpeningGaugeInch) OR UPDATE(OpeningGaugeQ)
		OR UPDATE (ClosingGaugeFeet) OR UPDATE(ClosingGaugeInch) OR UPDATE(ClosingGaugeQ) 
		OR UPDATE(OriginTankID) OR UPDATE(GrossUnits) OR UPDATE(GrossStdUnits) OR UPDATE(NetUnits)
		OR UPDATE(ProductObsTemp) OR UPDATE(ProductObsGravity) OR UPDATE(ProductBSW)
	BEGIN
		UPDATE tblOrderTicket
		  SET NetUnits = dbo.fnCrudeNetCalculator(GrossUnits, ProductObsTemp, ProductObsGravity, ProductBSW)
			, GrossStdUnits = dbo.fnCrudeNetCalculator(GrossUnits, ProductObsTemp, ProductObsGravity, 0)
		WHERE ID IN (SELECT ID FROM inserted)
		  AND TicketTypeID IN (1,2)
		  AND GrossUnits IS NOT NULL
		  AND ProductObsTemp IS NOT NULL
		  AND ProductObsGravity IS NOT NULL
		  AND ProductBSW IS NOT NULL
	END
	
	-- ensure the Order record is in-sync with the Tickets
	UPDATE tblOrder 
	SET OriginGrossUnits = (SELECT sum(GrossUnits) FROM tblOrderTicket WHERE OrderID = O.ID AND Rejected = 0 AND DeleteDateUTC IS NULL)
	  , OriginNetUnits = (SELECT sum(NetUnits) FROM tblOrderTicket WHERE OrderID = O.ID AND Rejected = 0 AND DeleteDateUTC IS NULL)
	  , OriginGrossStdUnits = (SELECT sum(GrossStdUnits) FROM tblOrderTicket WHERE OrderID = O.ID AND Rejected = 0 AND DeleteDateUTC IS NULL)
		-- use the first MeterRun/BasicRun CarrierTicketNum num as the Order BOL Num
	  , OriginBOLNum = (SELECT min(BOLNum) FROM tblOrderTicket WHERE OrderID = O.ID AND TicketTypeID NOT IN (1,2) AND DeleteDateUTC IS NULL)
	  , CarrierTicketNum = (SELECT min(CarrierTicketNum) FROM tblOrderTicket WHERE ID IN 
		(SELECT min(ID) FROM tblOrderTicket WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL))
	  , LastChangeDateUTC = (SELECT MAX(isnull(LastChangeDateUTC, CreateDateUTC)) FROM inserted WHERE OrderID = O.ID)
	  , LastChangedByUser = (SELECT MIN(isnull(LastChangedByUser, CreatedByUser)) FROM inserted WHERE OrderID = O.ID)
	FROM tblOrder O
	WHERE ID IN (SELECT DISTINCT OrderID FROM inserted)
END

GO

UPDATE tblOrderTicket SET GrossStdUnits = dbo.fnCrudeNetCalculator(GrossUnits, ProductObsTemp, ProductObsGravity, 0)
GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 6 Feb 2013
-- Description:	trigger to ensure the entered values for an OrderTicket are actually valid
-- =============================================
CREATE TRIGGER [dbo].[trigOrderTicket_IU_Validate] ON [dbo].[tblOrderTicket] AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	IF (SELECT COUNT(*) FROM (
			SELECT OT.OrderID
			FROM tblOrderTicket OT
			JOIN inserted i ON i.OrderID = OT.OrderID AND i.CarrierTicketNum = OT.CarrierTicketNum
			WHERE OT.DeleteDateUTC IS NULL
			GROUP BY OT.OrderID, OT.CarrierTicketNum
			HAVING COUNT(*) > 1
		) v) > 0
	BEGIN
		RAISERROR('Duplicate Ticket Numbers are not allowed', 16, 1)
		RETURN
	END
	
	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (3) AND BOLNum IS NULL) > 0
	BEGIN
		RAISERROR('BOL # value is required for Meter Run tickets', 16, 1)
		RETURN
	END
	
	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1, 2) AND OriginTankID IS NULL AND TankNum IS NULL) > 0
	BEGIN
		RAISERROR('Tank ID value is required for Gauge Run & Net Barrel tickets', 16, 1)
		RETURN
	END

	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1) AND CarrierTicketNum IS NULL) > 0
	BEGIN
		RAISERROR('Ticket # value is required for Gauge Run tickets', 16, 1)
		RETURN
	END

	IF (SELECT COUNT(*) FROM inserted WHERE 
		(TicketTypeID IN (1, 2) AND (ProductOBsTemp IS NULL OR ProductObsGravity IS NULL OR ProductBSW IS NULL))
			OR (TicketTypeID IN (1) AND (ProductHighTemp IS NULL OR ProductLowTemp IS NULL))
		) > 0
	BEGIN
		RAISERROR('All Product Measurement values are required for Gauge Run & Net Barrel tickets', 16, 1)
		RETURN
	END
	
	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1) AND Rejected = 0 
		AND (OpeningGaugeFeet IS NULL OR OpeningGaugeInch IS NULL OR OpeningGaugeQ IS NULL)) > 0
	BEGIN
		RAISERROR('All Opening Gauge values are required for Gauge Run tickets', 16, 1)
		RETURN
	END
	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1) AND Rejected = 0 
		AND (ClosingGaugeFeet IS NULL OR ClosingGaugeInch IS NULL OR ClosingGaugeQ IS NULL)) > 0
	BEGIN
		RAISERROR('All Closing Gauge values are required for Gauge Run tickets', 16, 1)
		RETURN
	END

	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (2) AND Rejected = 0 
		AND (GrossUnits IS NULL)) > 0
	BEGIN
		RAISERROR('Gross Volume value is required for Net Unit tickets', 16, 1)
		RETURN
	END

	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (3) AND Rejected = 0 
		AND (GrossUnits IS NULL OR NetUnits IS NULL)) > 0
	BEGIN
		RAISERROR('Gross & Net Volume values are required for Meter Run tickets', 16, 1)
		RETURN
	END

	IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1) AND Rejected = 0 
		AND (SealOff IS NULL OR SealOn IS NULL)) > 0
	BEGIN
		RAISERROR('All Seal Off & Seal On values are required for Gauge Run tickets', 16, 1)
		RETURN
	END

END

GO

/***********************************/
-- Date Created: 28 Feb 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc for 3rd parties (Customer/Carrier)
/***********************************/
ALTER PROCEDURE [dbo].[spOrdersBasicExport]
(
  @StartDate datetime
, @EndDate datetime
, @CarrierID int = 0 -- -1 = all carriers
, @CustomerID int = 0 -- -1 = all customers
, @ProducerID int = 0
, @OrderNum varchar(20) = NULL
, @StatusID_CSV varchar(max) = '4'
) AS BEGIN
	SELECT @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	
	SELECT * 
	FROM dbo.viewOrderExportFull_Reroute
	WHERE (@CarrierID=-1 OR @CustomerID=-1 OR CarrierID=@CarrierID OR CustomerID=@CustomerID OR ProducerID=@ProducerID) 
	  AND OrderDate BETWEEN @StartDate AND @EndDate
	  AND DeleteDateUTC IS NULL
	  AND (StatusID IN (SELECT ID FROM dbo.fnSplitCSVIDs(@StatusID_CSV)))
	  AND (@OrderNum IS NULL OR OrderNum LIKE @OrderNum)
	ORDER BY OrderDate, OrderNum
END

GO

/*******************************************/
-- Date Created: 25 Apr 2013
-- Author: Kevin Alons
-- Purpose: return readonly Order data for Driver App sync
/*******************************************/
ALTER FUNCTION [dbo].[fnOrderReadOnly_DriverApp]( @DriverID int, @LastChangeDateUTC datetime ) RETURNS TABLE AS
RETURN 
	SELECT O.ID
		, O.OrderNum
		, O.StatusID
		, O.TicketTypeID
		, cast(P.PriorityNum as int) AS PriorityNum
		, O.Product
		, O.DueDate
		, O.Origin
		, O.OriginFull
		, OO.OriginType
		, O.OriginUomID
		, OO.Station AS OriginStation
		, OO.LeaseNum AS OriginLeaseNum
		, OO.County AS OriginCounty
		, OO.LegalDescription AS OriginLegalDescription
		, O.OriginNDIC
		, O.OriginNDM
		, O.OriginCA
		, O.OriginState
		, OO.WellAPI AS OriginAPI
		, OO.LAT AS OriginLat
		, OO.LON AS OriginLon
		, O.Destination
		, O.DestinationFull
		, D.DestinationType AS DestType 
		, O.DestUomID
		, D.LAT AS DestLat
		, D.LON as DestLon
		, D.Station AS DestinationStation
		, O.CreateDateUTC
		, O.CreatedByUser
		, O.LastChangeDateUTC
		, O.LastChangedByUser
		, isnull(ODAVD.VirtualDeleteDateUTC, O.DeleteDateUTC) AS DeleteDateUTC
		, isnull(ODAVD.VirtualDeletedByUser, O.DeletedByUser) AS DeletedByUser
		, O.OriginID
		, O.DestinationID
		, cast(O.PriorityID AS int) AS PriorityID
		, O.Operator
		, O.OperatorID
		, O.Pumper
		, O.PumperID
		, O.Producer
		, O.ProducerID
		, O.Customer
		, O.CustomerID
		, O.Carrier
		, O.CarrierID
		, O.ProductID
		, O.TicketType
		, C.ZPLHeaderBlob AS PrintHeaderBlob
		, isnull(C.EmergencyInfo, 'For an emergency (spill, leak, fire, or accident) contact CHEMTREC @ 800.424.9300 (toll free)') AS EmergencyInfo
		, O.DestTicketTypeID
		, O.DestTicketType
		, O.OriginTankNum
		, O.OriginTankID
		, O.DispatchNotes
		, O.DispatchConfirmNum
		, isnull(R.ActualMiles, 0) AS RouteActualMiles
		, CA.Authority AS CarrierAuthority
	FROM dbo.viewOrder O
	JOIN dbo.tblPriority P ON P.ID = O.PriorityID
	JOIN dbo.viewOrigin OO ON OO.ID = O.OriginID
	JOIN dbo.viewDestination D ON D.ID = O.DestinationID
	JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
	JOIN dbo.tblRoute R ON R.ID = O.RouteID
	JOIN dbo.tblCarrier CA ON CA.ID = O.CarrierID
	LEFT JOIN dbo.tblPrintStatus DPS ON DPS.ID = O.DeliverPrintStatusID
	LEFT JOIN tblOrderDriverAppVirtualDelete ODAVD ON ODAVD.OrderID = O.ID AND ODAVD.DriverID = @DriverID
	CROSS JOIN (SELECT DATEADD(second, -5, @LastChangeDateUTC) AS LCD) LCD
	WHERE (O.StatusID IN (2, 7, 8) -- Dispatched, Accepted, Picked Up
		-- or status = Delivered and the driver still has access to the record
		OR (StatusID = 3 AND isnull(DPS.IsCompleted, 0) = 0)
		-- or this potentially was a DriverApp record, that no longer is, so should be included (as deleted)
		OR (ODAVD.ID IS NOT NULL AND ODAVD.VirtualDeleteDateUTC >= LCD.LCD))
	  AND (O.DriverID = @DriverID OR ODAVD.DriverID = @DriverID)
	  AND (
		@LastChangeDateUTC IS NULL 
		OR O.CreateDateUTC >= LCD.LCD
		OR O.LastChangeDateUTC >= LCD.LCD
		OR O.DeleteDateUTC >= LCD.LCD
		OR C.LastChangeDateUTC >= LCD.LCD
		OR CA.LastChangeDateUTC >= LCD.LCD
		OR OO.LastChangeDateUTC >= LCD.LCD
		OR R.LastChangeDateUTC >= LCD.LCD
		OR ODAVD.VirtualDeleteDateUTC >= LCD.LCD)
		
GO

/*******************************************/
-- Date Created: 31 Aug 2013
-- Author: Kevin Alons
-- Purpose: return driver editable Order data for Driver App sync
/*******************************************/
ALTER FUNCTION [dbo].[fnOrderEdit_DriverApp]( @DriverID int, @LastChangeDateUTC datetime ) RETURNS TABLE AS
RETURN 
	SELECT O.ID
		, O.StatusID
		, O.TruckID
		, O.TrailerID
		, O.Trailer2ID
		, O.OriginBOLNum
		, O.OriginArriveTimeUTC
		, O.OriginDepartTimeUTC
		, O.OriginMinutes
		, O.OriginWaitNotes
		, O.OriginTruckMileage
		, O.OriginGrossUnits
		, O.OriginGrossStdUnits
		, O.OriginNetUnits
		, O.ChainUp
		, O.Rejected
		, O.RejectNotes
		, O.OriginTankNum
		, O.DestArriveTimeUTC
		, O.DestDepartTimeUTC
		, O.DestMinutes
		, O.DestWaitNotes
		, O.DestBOLNum
		, O.DestTruckMileage
		, O.DestGrossUnits
		, O.DestNetUnits
		, O.DestProductTemp
		, O.DestProductBSW
		, O.DestProductGravity
		, O.DestOpenMeterUnits
		, O.DestCloseMeterUnits
		, O.CarrierTicketNum
		, O.AcceptLastChangeDateUTC
		, O.PickupLastChangeDateUTC
		, O.DeliverLastChangeDateUTC
		, O.PickupPrintStatusID
		, O.DeliverPrintStatusID
		, O.PickupPrintDateUTC
		, O.DeliverPrintDateUTC
		, O.DriverNotes
	FROM dbo.tblOrder O
	LEFT JOIN tblOrderDriverAppVirtualDelete ODAVD ON ODAVD.OrderID = O.ID AND ODAVD.DriverID = @DriverID
	LEFT JOIN dbo.tblPrintStatus DPS ON DPS.ID = O.DeliverPrintStatusID
	CROSS JOIN (SELECT DATEADD(second, -5, @LastChangeDateUTC) AS LCD) LCD
	WHERE (O.StatusID IN (2, 7, 8) -- Dispatched, Accepted, Picked Up
		-- or status = Delivered and the driver still has access to the record
		OR (StatusID = 3 AND isnull(DPS.IsCompleted, 0) = 0)
		-- or this potentially was a DriverApp record, that no longer is, so should be included (as deleted)
		OR (ODAVD.ID IS NOT NULL AND ODAVD.VirtualDeleteDateUTC >= LCD.LCD))
	  AND (O.DriverID = @DriverID OR ODAVD.DriverID = @DriverID)
	  AND (
		@LastChangeDateUTC IS NULL 
		OR O.CreateDateUTC >= LCD.LCD
		OR O.LastChangeDateUTC >= LCD.LCD
		OR O.DeleteDateUTC >= LCD.LCD
		OR ODAVD.VirtualDeleteDateUTC >= LCD.LCD)
		
GO

/*******************************************/
-- Date Created: 25 Apr 2013
-- Author: Kevin Alons
-- Purpose: return OrderTicket data for Driver App sync
/*******************************************/
ALTER FUNCTION [dbo].[fnOrderTicket_DriverApp]( @DriverID int, @LastChangeDateUTC datetime ) RETURNS  TABLE AS
RETURN 
	SELECT OT.ID
		, OT.UID
		, OT.OrderID
		, OT.CarrierTicketNum
		, OT.OriginTankID
		, OT.TankNum
		, OT.TicketTypeID
		, OT.ProductObsGravity
		, OT.ProductObsTemp
		, OT.ProductHighTemp
		, OT.ProductLowTemp
		, OT.ProductBSW
		, cast(OT.OpeningGaugeFeet as tinyint) AS OpeningGaugeFeet
		, cast(OT.OpeningGaugeInch as tinyint) AS OpeningGaugeInch
		, cast(OT.OpeningGaugeQ as tinyint) AS OpeningGaugeQ
		, cast(OT.ClosingGaugeFeet as tinyint) AS ClosingGaugeFeet
		, cast(OT.ClosingGaugeInch as tinyint) AS ClosingGaugeInch
		, cast(OT.ClosingGaugeQ as tinyint) AS ClosingGaugeQ
		, OT.BottomFeet
		, OT.BottomInches
		, OT.BottomQ
		, OT.GrossUnits
		, OT.GrossStdUnits
		, OT.NetUnits
		, OT.Rejected
		, OT.RejectNotes
		, OT.SealOff
		, OT.SealOn
		, OT.BOLNum
		, OT.CreateDateUTC
		, OT.CreatedByUser
		, OT.LastChangeDateUTC
		, OT.LastChangedByUser
		, OT.DeleteDateUTC
		, OT.DeletedByUser
	FROM dbo.tblOrderTicket OT
	JOIN dbo.tblOrder O ON O.ID = OT.OrderID
	LEFT JOIN dbo.tblPrintStatus DPS ON DPS.ID = O.DeliverPrintStatusID
	CROSS JOIN (SELECT DATEADD(second, -5, @LastChangeDateUTC) AS LCD) LCD
	WHERE (StatusID IN (2, 7, 8) -- Dispatched, Accepted, Picked Up
		-- or status = Delivered and the driver still has access to the record
		OR (StatusID = 3 AND isnull(DPS.IsCompleted, 0) = 0))
	  AND O.DriverID = @DriverID
	  AND (@LastChangeDateUTC IS NULL 
		OR OT.CreateDateUTC >= LCD.LCD
		OR OT.LastChangeDateUTC >= LCD.LCD)

GO

/*******************************************/
-- Date Created: 5 Apr 2014
-- Author: Kevin Alons
-- Purpose: return OriginTank data for Driver App sync
/*******************************************/
CREATE FUNCTION [dbo].[fnOriginTank_DriverApp]( @DriverID int, @LastChangeDateUTC datetime ) RETURNS  TABLE AS
RETURN 
	SELECT DISTINCT OT.ID
		, OT.OriginID
		, OT.TankNum
		, OT.TankDescription
		, OT.CreateDateUTC
		, OT.CreatedByUser
		, OT.LastChangeDateUTC
		, OT.LastChangedByUser
		, OT.DeleteDateUTC
		, OT.DeletedByUser
	FROM dbo.tblOriginTank OT
	JOIN dbo.tblOrder O ON O.OriginID = OT.OriginID
	LEFT JOIN dbo.tblPrintStatus DPS ON DPS.ID = O.DeliverPrintStatusID
	CROSS JOIN (SELECT DATEADD(second, -5, @LastChangeDateUTC) AS LCD) LCD
	WHERE (StatusID IN (2, 7, 8) -- Dispatched, Accepted, Picked Up
		-- or status = Delivered and the driver still has access to the record
		OR (StatusID = 3 AND isnull(DPS.IsCompleted, 0) = 0))
	  AND O.DriverID = @DriverID
	  AND (@LastChangeDateUTC IS NULL 
		OR OT.CreateDateUTC >= LCD.LCD
		OR OT.LastChangeDateUTC >= LCD.LCD
		OR OT.DeleteDateUTC >= LCD.LCD)

GO
GRANT SELECT ON fnOriginTank_DriverApp TO dispatchcrude_iis_acct
GO

CREATE TABLE tblOriginTankStrappingDeleted
(
  ID int NOT NULL CONSTRAINT PK_OriginTankStrappingDeleted PRIMARY KEY 
, DeleteDateUTC datetime CONSTRAINT DF_OriginTankStrappingDeleted DEFAULT (getutcdate())
)
GO
GRANT SELECT, INSERT ON tblOriginTankStrappingDeleted TO dispatchcrude_iis_acct
GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 5 Apr 2014
-- Description:	trigger to ensure record deleted OriginTankStrappings (so we can sync deletes with the mobile app)
-- =============================================
CREATE TRIGGER [dbo].[trigOriginTankStrapping_D] ON [dbo].[tblOriginTankStrapping] AFTER DELETE AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO tblOriginTankStrappingDeleted (ID) SELECT ID FROM deleted
END 

GO

/*******************************************/
-- Date Created: 5 Apr 2014
-- Author: Kevin Alons
-- Purpose: return OriginTankStrapping data for Driver App sync
/*******************************************/
CREATE FUNCTION [dbo].[fnOriginTankStrapping_DriverApp]( @DriverID int, @LastChangeDateUTC datetime ) RETURNS  TABLE AS
RETURN 
	SELECT DISTINCT OTS.ID
		, OTS.OriginTankID
		, OTS.Feet
		, OTS.CreateDateUTC
		, OTS.CreatedByUser
		, OTS.LastChangeDateUTC
		, OTS.LastChangedByUser
		, cast(CASE WHEN OTSD.ID IS NULL THEN 0 ELSE 1 END as bit) AS IsDeleted
	FROM dbo.tblOriginTankStrapping OTS
	JOIN dbo.tblOriginTank OT ON OT.ID = OTS.OriginTankID
	JOIN dbo.tblOrder O ON O.OriginID = OT.OriginID
	LEFT JOIN dbo.tblOriginTankStrappingDeleted OTSD ON OTSD.ID = OTS.ID
	LEFT JOIN dbo.tblPrintStatus DPS ON DPS.ID = O.DeliverPrintStatusID
	CROSS JOIN (SELECT DATEADD(second, -5, @LastChangeDateUTC) AS LCD) LCD
	WHERE (StatusID IN (2, 7, 8) -- Dispatched, Accepted, Picked Up
		-- or status = Delivered and the driver still has access to the record
		OR (StatusID = 3 AND isnull(DPS.IsCompleted, 0) = 0))
	  AND O.DriverID = @DriverID
	  AND (@LastChangeDateUTC IS NULL 
		OR OT.CreateDateUTC >= LCD.LCD
		OR OT.LastChangeDateUTC >= LCD.LCD
		OR OTSD.DeleteDateUTC >= LCD.LCD)

GO
GRANT SELECT ON fnOriginTankStrapping_DriverApp TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 4 Apr 2013
-- Author: Kevin Alons
-- Purpose: return Carrier records with translated "friendly" values
/***********************************/
ALTER VIEW [dbo].[viewCarrier] AS
SELECT C.*
	, cast(CASE WHEN C.DeleteDateUTC IS NULL THEN 1 ELSE 0 END as bit) AS Active
	, CT.Name AS CarrierType
	, S.Abbreviation AS StateAbbrev
	, SF.Name AS SettlementFactor 
	, UOM.Name AS MinSettlementUOM
	, UOM.Abbrev AS MinSettlementUomShort
FROM tblCarrier C 
JOIN tblCarrierType CT ON CT.ID = C.CarrierTypeID
LEFT JOIN tblState S ON S.ID = C.StateID
LEFT JOIN tblSettlementFactor SF ON SF.ID = C.SettlementFactorID
LEFT JOIN dbo.tblUom UOM ON UOM.ID = C.MinSettlementUomID

GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 20 Mar 2013
-- Description:	return all CarrierTicketNums for an Order (separated by ",")
-- =============================================
ALTER VIEW [dbo].[viewOrder_AllTickets] AS 
WITH ActiveTickets AS
(
	SELECT ID, OrderID, CarrierTicketNum FROM tblOrderTicket WHERE DeleteDateUTC IS NULL
)
, Tickets AS 
( 
	--initialization 
	SELECT OT.OrderID, OT.ID, cast(CarrierTicketNum as varchar(255)) AS Tickets
	FROM ActiveTickets OT 
	JOIN (SELECT OrderID, MIN(ID) AS ID FROM tblOrderTicket GROUP BY OrderID) OTI 
		ON OTI.OrderID = OT.OrderID AND OTI.ID = OT.ID
	
	UNION ALL 
	--recursive execution 
	SELECT T.OrderID, OT.ID, cast(T.Tickets + ',' + OT.CarrierTicketNum as varchar(255)) AS Tickets
	FROM ActiveTickets OT JOIN Tickets T ON OT.ID > T.ID AND OT.OrderID = T.OrderID
)

SELECT O.*, T.Tickets
FROM viewOrderLocalDates O
LEFT JOIN (
	SELECT OrderID, max(Tickets) AS Tickets
	FROM Tickets T
	GROUP BY OrderID
) T ON T.OrderID = O.ID

GO

DROP VIEW viewTankType
GO
DROP TABLE tblTankType
GO
DROP TABLE tblOrderPrintLogTicket
GO
DROP TABLE tblOrderPrintLog
GO

COMMIT
SET NOEXEC OFF

EXEC _spRefreshAllViews
GO

EXEC _spRefreshAllViews
GO

EXEC _spRecompileAllStoredProcedures
GO