SET NOEXEC OFF  -- since this is 
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.12.6.1'
SELECT  @NewVersion = '3.12.6.2'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1437 Add Due Date Option to Order Date Source Order Rule'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/* 6/10/16 - DCWEB-1437 -  Add Due Date Option to Order Date Source Order Rule */
INSERT INTO tblOrderRuleTypeDropDownValues (OrderRuleTypeID, Value, SortNum, IsDefault, InternalData)
SELECT 8, 'Due Date', 5, 0, 'DueDate'
GO


/********************************************************
	Author: Kevin Alons
	Create Date: 2015/08/31 - 3.9.5
	Purpose: compute and return the current OrderDate for the specified Order
	Changes:
		3.9.23 - 2015/11/05 - KDA+JAE - revise OOR to only return TypeID = 8 OrderRule records
		         2015/11/12 - BB - Force reject tickets to get pickup depart time if order date is set to a delivery 
							  time. This keeps them from getting stuck on the audit page with no order date.
		3.12.6.2 - 2016/6/10 - BB - Add Due Date option
********************************************************/
ALTER FUNCTION [dbo].[fnOrderDate](@id int) RETURNS date AS
BEGIN
	DECLARE @ret date 
	SELECT @ret = cast(CASE OOR.InternalData WHEN 'OriginArriveTime' THEN OLD.OriginArriveTime
											WHEN 'OriginDepartTime' THEN OLD.OriginDepartTime
											WHEN 'DestArriveTime' THEN
												CASE OLD.Rejected  WHEN 0 THEN OLD.DestArriveTime
																   WHEN 1 THEN OLD.OriginDepartTime
												END
											WHEN 'DestDepartTime' THEN
												CASE OLD.Rejected  WHEN 0 THEN OLD.DestDepartTime
																   WHEN 1 THEN OLD.OriginDepartTime
												END
											WHEN 'DueDate' THEN OLD.DueDate
				END as date)
	FROM viewOrderLocalDates OLD 
	CROSS APPLY (SELECT * FROM dbo.fnOrderOrderRules(OLD.ID) WHERE TypeID = 8) OOR
	WHERE OLD.ID = @id

	RETURN @ret
END
GO


COMMIT
SET NOEXEC OFF