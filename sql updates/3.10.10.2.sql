SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.10.10.1'
SELECT  @NewVersion = '3.10.10.2'

-- only ensure the curr version matches when we are not testing with an "x.x.x" DEV version
IF @NewVersion <> 'x.x.x' AND (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

-- only update the DB VERSION when not testing with an "x.x.x" DEV version
IF @NewVersion <> 'x.x.x' 
BEGIN
	UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

	INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
		SELECT @NewVersion, 1, 'DCWEB-1066 - OriginTank | OriginTankStrapping sync improvements'
		EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
END
GO

/*******************************************
 Date Created: 5 Apr 2014
 Author: Kevin Alons
 Purpose: return OriginTank data for Driver App sync
 Changes:
  - 3.10.5		- 2016/01/29	- KDA	- use tblOrderAppChanges to only include records when Order.OriginID not any Order change
  - 3.10.10.2	- 2016/02/24	- KDA	- remove VirtualDelete logic and ensure when an order.LastChangeDateUTC changes (not just CreateDateUTC)
*******************************************/
ALTER FUNCTION fnOriginTank_DriverApp(@DriverID int, @LastChangeDateUTC datetime) RETURNS TABLE AS
RETURN 
	SELECT DISTINCT OT.ID
		, OT.OriginID
		, OT.TankNum
		, OT.TankDescription
		, OT.CreateDateUTC
		, OT.CreatedByUser
		, OT.LastChangeDateUTC
		, OT.LastChangedByUser
		, OT.DeleteDateUTC
		, OT.DeletedByUser
	FROM dbo.tblOriginTank OT
	JOIN dbo.tblOrder O ON O.OriginID = OT.OriginID
	LEFT JOIN tblOrderAppChanges OAC ON OAC.OrderID = O.ID
	CROSS JOIN fnSyncLCDOffset(@LastChangeDateUTC) LCD
	WHERE O.StatusID IN (2, 7, 8, 3) -- Dispatched, Accepted, Picked Up, Delivered
	  AND O.DriverID = @DriverID
	  AND (@LastChangeDateUTC IS NULL 
		OR O.CreateDateUTC >= LCD
		OR O.LastChangeDateUTC >= LCD
		-- required to ensure that when the Order.OriginID is changed, the OriginTanks/Strappings are always synced
		OR OAC.OriginChangeDateUTC >= LCD
		OR OT.CreateDateUTC >= LCD
		OR OT.LastChangeDateUTC >= LCD
		OR OT.DeleteDateUTC >= LCD)
GO

/*******************************************
 Date Created: 5 Apr 2014
 Author: Kevin Alons
 Purpose: return OriginTankStrapping data for Driver App sync
 Changes:
 - 3.10.5		- 2016/01/29	- KDA	- use new OAC table to only include when the Order.OriginID changes
										- show IsDeleted = 1 when the record or any relevant parent records are deleted 
 - 3.10.10.2	- 2016/02/24	- KDA	- remove VirtualDelete logic and ensure when an order.LastChangeDateUTC changes (not just CreateDateUTC)
*******************************************/
ALTER FUNCTION fnOriginTankStrapping_DriverApp(@DriverID int, @LastChangeDateUTC datetime) RETURNS TABLE AS
RETURN 
	SELECT DISTINCT OTS.*
		, IsDeleted = cast(CASE WHEN isnull(ltrim(OTSD.ID), OT.DeleteDateUTC) IS NULL THEN 0 ELSE 1 END as bit) 
	FROM dbo.tblOriginTankStrapping OTS
	JOIN dbo.tblOriginTank OT ON OT.ID = OTS.OriginTankID
	JOIN dbo.tblOrder O ON O.OriginID = OT.OriginID
	LEFT JOIN tblOrderAppChanges OAC ON OAC.OrderID = O.ID
	LEFT JOIN dbo.tblOriginTankStrappingDeleted OTSD ON OTSD.ID = OTS.ID
	CROSS JOIN fnSyncLCDOffset(@LastChangeDateUTC) LCD
	WHERE O.StatusID IN (2, 7, 8, 3) -- Dispatched, Accepted, Picked Up, Delivered
	  AND O.DriverID = @DriverID
	  AND (@LastChangeDateUTC IS NULL 
		OR O.CreateDateUTC >= LCD
		OR O.LastChangeDateUTC >= LCD
		-- required to ensure that when the Order.OriginID is changed, the OriginTanks/Strappings are always synced
		OR OAC.OriginChangeDateUTC >= LCD
		OR OT.CreateDateUTC >= LCD
		OR OT.LastChangeDateUTC >= LCD
		OR OTS.CreateDateUTC >= LCD
		OR OTS.LastChangeDateUTC >= LCD
		OR OTSD.DeleteDateUTC >= LCD)
GO

COMMIT
SET NOEXEC OFF