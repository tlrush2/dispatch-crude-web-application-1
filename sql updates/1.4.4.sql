DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '1.4.3', @NewVersion = '1.4.4'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

ALTER TABLE [dbo].[tblDestination] DROP CONSTRAINT [DF__tblDestin__Ticke__79E80B25]
GO

ALTER TABLE [dbo].[tblDestination] ADD CONSTRAINT [DF__tblDestination_TicketType] DEFAULT ((1)) FOR [TicketTypeID]
GO

COMMIT TRANSACTION
SET NOEXEC OFF
