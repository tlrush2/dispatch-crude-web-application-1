-- rollback
SET NOEXEC OFF
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.10.9.1'
SELECT  @NewVersion = '3.10.9.2'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1058: Changed C.O.D.E. Export ticket number to send order number with leading zero "0######" instead of "######A"'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/*******************************************
-- Date Created: 28 May 2015
-- Author: Kevin Alons
-- Purpose: format a string to a fixed length (truncate if short, pad (right) with spaces if short
-- Changes: x.x.x - BB - 2/19/16 - New formatting for ticket number on C.O.D.E. export requires a leading zero which should be cut off when we get to a 7-digit
--                                 Order number.  Adding a parameter to allow this function to trim right or left. 0 = Left, 1 = Right.
*******************************************/
ALTER FUNCTION [dbo].[fnFixedLenStr](@str varchar(max), @fixedLen int, @trimDirection bit) RETURNS varchar(max) AS
BEGIN
	DECLARE @ret varchar(255)
	
	IF (@trimDirection = 1)
		SET @ret = RIGHT(REPLICATE(' ', @fixedLen) + @str, @fixedLen)
	ELSE	
		SET @ret = LEFT(@str + REPLICATE(' ', @fixedLen), @fixedLen)	
	
	RETURN (@ret)
END
GO



/*=============================================
	Author:			Ben Bloodworth
	Create date:	5 Jun 2015
	Description:	Displays the base information for the CODE export. Please note that some values could change per DispatchCrude customer and thus it may
					be better to retrieve these settings from the Data Exchange settings instead of this view.
					
	Updates:		8/21/15 - BB: Added CASE statments to account for Nulls in gravity readings and added additional filters for rejects and ticket types.
					8/26/15 - BB: Altered tank number field to use OriginTankText instead of TankNum.  Altered Property_Code per instructions from customer.
					9/24/15 - BB: Added Net Volume tickets to allowed ticket types list in where clause.
				   10/05/15 - BB: Changed export to use Gross Volume instead of Net Volume (Changed per Cash Rainer at CW2).
	 - 3.9.19.11 - 10/08/15 - KDA - fix several varchar -> numeric conversions that could fail with entered data 
							(CarrierTicketNum too long, LeaseNum not numeric, T.OriginTankText not numeric, use of fnIsNumeric() function)
				   10/14/15 - BB: removed KDA's changes in 3.9.19.11 because they broke the export and the customer noticed and called us on it!
									the lease numbers no longer printed and were being zero filled.  HUGE NO NO!!
				   10/28/15 - BB: Customer now says to ALWAYS send BSW value if it is collected.  Added all relevant ticket types to BSW field logic .
				   12/10/15 - KDA+BB: ensure any '-' characters in TankNum are eliminated prior to export processing (will cause a crash if not removed)
	- x.x.x - 2/19/16 - BB: Per request from Plains for CW2 I changed the ticket number to be a zero + the order number.  When we hit 7 digits, the zero
								will be trimmed off of the left side due to the change made to fnFixedLenStr that allows left OR right trimming now.
=============================================*/
ALTER VIEW [dbo].[viewOrderExport_CODE_Base] AS
SELECT 
	OrderID = O.ID
	,OrderTicketID = T.ID
	,Record_ID = '2'
	,Company_ID = C.CodeDXCode
	,Ticket_Type = TT.CodeDXCode
	,Transmit_Ind = '2'  -- Hard coded per Angie Toews (PDS Energy) but may need to be changed to dynamic if the customer says so
	,Run_Type = 1
	-- This zero first ticket number
	,Ticket_Number = dbo.fnFixedLenStr('0' + CAST(O.OrderNum AS VARCHAR),7,1)
	/*
	This is the original way to transmit ticket numbers - commented out instead of deleting so it's easier to go back when we need to.
	,Ticket_Number = CASE WHEN ISNUMERIC(RIGHT(T.CarrierTicketNum,1)) = 0 THEN dbo.fnFixedLenStr(T.CarrierTicketNum,7,0) 
						  ELSE dbo.fnFixedLenNum(T.CarrierTicketNum,7,0,0) 
					 END*/
	,Run_Ticket_Date = dbo.fnDateMMDDYY(O.OrderDate)
	
	--Property code is a non-standard CODE field for Plains (per Steve Carver).  It is a mixture of origin leasnum and destination station num
	,Property_Code = (CASE WHEN O.LeaseNum IS NULL THEN '000000' ELSE CAST(dbo.fnFixedLenNum(O.LeaseNum,6,0,0) AS VARCHAR(6)) END)
					+ '000'
					+ (CASE WHEN O.DestStation IS NULL THEN '00000' ELSE dbo.fnFixedLenStr(O.DestStation,5,0) END)
					
	,Gathering_Charge = '0'  -- Hard coded per Angie Toews (PDS Energy) but may need to be changed to dynamic if the customer says so	
	,Tank_Meter_Num = CAST(dbo.fnFixedLenNum(REPLACE(T.OriginTankText, '-', ''), 6, 0, 0) AS VARCHAR(6)) + '0'	
	,Adjustment_Ind = '0' -- Hard coded per Angie Toews (PDS Energy) but may need to be changed to dynamic if the customer says so
	,Opening_Reading = dbo.fnOpenCloseReadingCODEFormat(T.ID,TT.CodeDXCode,0)
	,Opening_Date = dbo.fnDateMMDD(O.OrderDate)
	,Closing_Reading = dbo.fnOpenCloseReadingCODEFormat(T.ID,TT.CodeDXCode,1)
	,Closing_Date = dbo.fnDateMMDD(O.DestDepartTime)
	,Meter_Factor = CASE WHEN T.MeterFactor IS NULL THEN '00000000' 
						 ELSE dbo.fnFixedLenNum(T.MeterFactor, 2, 6, 1) 
					END
	,Shrinkage_Incrustation = '1000000' -- Hard coded per Angie Toews (PDS Energy) but may need to be changed to dynamic if the customer says so
	,Opening_Temperature = CASE WHEN T.ProductHighTemp IS NULL THEN '0000' 
								ELSE dbo.fnFixedLenNum(T.ProductHighTemp, 3, 1, 1) 
						   END
	,Closing_Temperature = CASE WHEN T.ProductLowTemp IS NULL THEN '0000' 
								ELSE dbo.fnFixedLenNum(T.ProductLowTemp, 3, 1, 1) 
						   END
	,BSW = CASE WHEN TT.ID IN (1,2,3,6,7) THEN dbo.fnFixedLenNum(T.ProductBSW, 2, 2, 1) --Basic, CAN Basic, and Gross Volume tickets are exluded because they dont have this value					
				ELSE '0000'
		   END
	,Observed_Gravity =	CASE WHEN T.ProductObsGravity IS NULL THEN '000'
							 ELSE dbo.fnFixedLenNum(T.ProductObsGravity, 2, 1, 1)
						END
	,Observed_Temperature = dbo.fnFixedLenNum(T.ProductObsTemp, 3, 1, 1)
	,Pos_Neg_Code = '0' -- Hard coded per Angie Toews (PDS Energy) but may need to be changed to dynamic if the customer says so
	,Total_Net_Volume = dbo.fnFixedLenNum(T.GrossUnits, 7, 2, 1) -- BB 10/7/15 this sends GROSS on purpose. this change is per Plains Marketing
	,Shippers_Net_Volume = dbo.fnFixedLenNum(T.GrossUnits, 7, 2, 1) -- BB 10/7/15 this sends GROSS on purpose. this change is per Plains Marketing
	,Corrected_Gravity = CASE WHEN T.Gravity60F IS NULL THEN '000'
							  ELSE dbo.fnFixedLenNum(T.Gravity60F, 2, 1, 1)
						 END
	,Product_Code = dbo.fnFixedLenStr('',3,0)	--Leave 3 blank spaces unless product is not crude oil. For others lookup PETROEX code	
	,Transmission_Date = dbo.fnFixedLenStr('',3,0) --Leave 3 blank spaces. Filled in by export reciever.
FROM viewOrderLocalDates O
	JOIN dbo.viewOrderCustomerFinalExportPending EP ON EP.ID = O.ID
	LEFT JOIN viewOrderTicket T ON T.OrderID = O.ID AND T.DeleteDateUTC IS NULL
	LEFT JOIN tblTicketType TT ON TT.ID = O.TicketTypeID
	LEFT JOIN tblCustomer C ON C.ID = O.CustomerID
	LEFT JOIN tblProduct P ON P.ID = O.ProductID
WHERE O.DeleteDateUTC IS NULL
	AND O.Rejected = 0 -- No rejected orders
	AND T.Rejected = 0 -- No rejected tickets
	AND O.StatusID = 4 -- Only Audited orders
	AND TT.ID IN (1,2,7) -- Only Gauge Run, Gauge Net, and Net Volume tickets
GO



/* Refresh viewOrderExport_CODE since it relies on viewOrderExport_CODE_Base */
EXEC sp_RefreshView viewOrderExport_CODE
GO


/*****************************************************************************************/
-- Author: Kevin Alons
-- Date Created: 27 Feb 2014
-- Purpose: with the provided parameters, export the data and mark done (if doing finalExport)
--		8/12/15 - Ben B. - Expanded procedure to include C.O.D.E. export options
--		8/21/15 - Ben B. - Added summary row to CODE export
--		8/25/15 - Ben B. & Joe E. - CODE summary record was not working on final export (only preview). Moved where this record was added.
--		8/26/15 - Ben B. - Added summary row to CODE summary row row count per customer request
--		8/26/15 - Ben B. - Fixed last update (3.8.19)  I pulled the old version of the stored procedure to add the row count and lost the summary row.
--      x.x.x - 2/19/16 - Ben B. - Added new required parameter to calls to fnFixedLenStr to indicate these are the original left trim option
/*****************************************************************************************/
ALTER PROCEDURE [dbo].[spOrderExportFinalCustomer]
( 
  @customerID int 
, @exportedByUser varchar(100) = NULL -- will default to SUSER_NAME() -- default value for now
, @exportFormat varchar(100) -- Options are: 'SUNOCO_SUNDEX' or 'CODE_STANDARD'
, @finalExport bit = 1 -- defualt to TRUE
) 
AS BEGIN
	SET NOCOUNT ON
	-- default to the current user if not supplied
	IF @exportedByUser IS NULL SET @exportedByUser = SUSER_NAME()
	
	-- Create variable for error catching with CODE export
	DECLARE @CODEErrorFlag BIT = 0 -- Default to false
	
	-- retrieve the order records to export
	DECLARE @orderIDs IDTABLE
	INSERT INTO @orderIDs (ID)
	SELECT ID
	FROM dbo.viewOrderCustomerFinalExportPending 
	WHERE CustomerID = @customerID 

	BEGIN TRAN exportCustomer
	
	-- export the orders in the specified format
	IF (@exportFormat = 'SUNOCO_SUNDEX') BEGIN
		SELECT E.* 
		INTO #output
		FROM dbo.viewSunocoSundex E
		JOIN @orderIDs IDs ON IDs.ID = E._ID
		WHERE E._CustomerID = @customerID
		ORDER BY _OrderNum
	END
	ELSE
	IF (@exportFormat = 'CODE_STANDARD') BEGIN
		DECLARE @CodeDXCode varchar(2) = (SELECT CodeDXCode FROM tblCustomer WHERE ID = @customerID)
		IF LEN(@CodeDXCode) = 2 BEGIN
			SELECT E.ExportText
			INTO #output2
			FROM dbo.viewOrderExport_CODE E
				JOIN @orderIDs IDs ON IDs.ID = E.OrderID
			WHERE E.CompanyID = @CodeDXCode
			
			/* Add summary record to output*/
			INSERT INTO #output2
			SELECT
				'4' -- Summary Record Type RecordID (Hard Coded)
				+ X.Company_ID 
				+ X.Record_Count 
				+ X.Pos_Neg_Code 
				+ X.Total_Net
				+ X.Shipper_Net
				+ dbo.fnFixedLenStr('',89,0)
				+ dbo.fnFixedLenStr('',3,0)
			FROM
				(SELECT
					ECB.Company_ID
					,CAST(dbo.fnFixedLenNum((COUNT(*) + 1), 10, 0, 1) AS VARCHAR)	AS Record_Count
					,ECB.Pos_Neg_Code
					,CAST(dbo.fnFixedLenNum(SUM(CAST(ECB.Total_Net_Volume AS DECIMAL)), 13, 0, 1) AS VARCHAR) Total_Net
					,CAST(dbo.fnFixedLenNum(SUM(CAST(ECB.Shippers_Net_Volume AS DECIMAL)), 13, 0, 1) AS VARCHAR) Shipper_Net
				FROM dbo.viewOrderExport_CODE_Base ECB			
					JOIN @orderIDs IDs ON IDs.ID = ECB.OrderID			
				WHERE ECB.Company_ID = @CodeDXCode
				GROUP BY ECB.Company_ID,ECB.Pos_Neg_Code) X			
		END
		ELSE SET @CODEErrorFlag = 1	
	END

	-- mark the orders as exported FINAL (for a Customer)
	IF (@finalExport = 1) BEGIN
		EXEC spMarkOrderExportFinalCustomer @orderIDs, @exportedByUser
	END
	
	COMMIT TRAN exportCustomer

	-- return the data to 
	IF (@exportFormat = 'SUNOCO_SUNDEX') BEGIN
		SELECT * FROM #output
	END
	
	IF (@exportFormat = 'CODE_STANDARD') BEGIN
		IF @CODEErrorFlag = 0 BEGIN
			SELECT * FROM #output2 ORDER BY ExportText --Sorted so that the summary record always sits at the bottom
		END
		ELSE SELECT 'Error in export data. Check Shipper CODE ID.'
	END		
END
GO



/*****************************************
Author: Kevin Alons
Date Created: 2015/10/08 - 3.9.19.11
Purpose: implement the CODE Standard Shipper export
    x.x.x - 2/19/16 - Ben B. - Added new required parameter to calls to fnFixedLenStr to indicate these are the original left trim option
******************************************/
ALTER PROCEDURE [dbo].[spOrderExportFinalCustomer_CODE_Standard] (@OrderIDs IDTABLE READONLY) AS
BEGIN
	-- retrieve the basic data
	SELECT E.ExportText, E.CompanyID
	INTO #output
	FROM dbo.viewOrderExport_CODE E
	JOIN @orderIDs IDs ON IDs.ID = E.OrderID

	IF (SELECT COUNT(DISTINCT CompanyID) FROM #output) <> 1 OR (SELECT COUNT(*) FROM #output WHERE LEN(isnull(CompanyID, '')) <> 2) > 0
	BEGIN /* invalid input data was specified, so just return an error message */
		RAISERROR('Error in export data. Check Shipper CODE ID.', 16, 1)
		RETURN
	END 
		
	/* Add summary record to output*/
	INSERT INTO #output (ExportText)
	SELECT
		'4' -- Summary Record Type RecordID (Hard Coded)
		+ X.Company_ID 
		+ X.Record_Count 
		+ X.Pos_Neg_Code 
		+ X.Total_Net
		+ X.Shipper_Net
		+ dbo.fnFixedLenStr('',89,0)
		+ dbo.fnFixedLenStr('',3,0)
	FROM (
		SELECT
			ECB.Company_ID
			,CAST(dbo.fnFixedLenNum((COUNT(*) + 1), 10, 0, 1) AS VARCHAR)	AS Record_Count
			,ECB.Pos_Neg_Code
			,CAST(dbo.fnFixedLenNum(SUM(CAST(ECB.Total_Net_Volume AS DECIMAL)), 13, 0, 1) AS VARCHAR) Total_Net
			,CAST(dbo.fnFixedLenNum(SUM(CAST(ECB.Shippers_Net_Volume AS DECIMAL)), 13, 0, 1) AS VARCHAR) Shipper_Net
		FROM dbo.viewOrderExport_CODE_Base ECB			
		JOIN @orderIDs IDs ON IDs.ID = ECB.OrderID			
		GROUP BY ECB.Company_ID,ECB.Pos_Neg_Code) X			
	
	/* return the final export data */
	SELECT ExportText FROM #output ORDER BY ExportText --Sorted so that the summary record always sits at the bottom
END
GO



COMMIT
SET NOEXEC OFF