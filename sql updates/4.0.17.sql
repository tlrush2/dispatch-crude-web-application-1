-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.0.16'
SELECT  @NewVersion = '4.0.17'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'DCWEB-1725 - prevent Import Center from creating orders without a Destination defined'
	UNION
	SELECT @NewVersion, 0, 'Updates to support procedure: _spAddNewObjectFields'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

update tblObjectField set AllowNullID = 0 where ID = 304
go

/************************************************/
-- Creation: 3.10.1 - 2016/01/14 - Kevin Alons
-- Purpose: support routine to generate new tblObjectField records for new fields after they are added to a table/view (and the tblObject table)
-- Notes: only intended to be used in the back office to assist future database updates
-- Changes:
--- 3.11.6.1	- 2016/03/07 - KDA	- add generation of appropriate ACTIVE fields
--									- convert to return appropriate sql statements instead of potentially adding the records to the db (removed @viewOnly parameter)
-- 3.11.6.2		- 2016/03/19 - KDA	- handle when SqlTargetName IS NULL
-- 3.11.6.3		- 2016/03/20 - KDA	- better logic to handle Customer/Shipper names
-- 4.0.17		- 2016.09.08 - KDA	- eliminate wrapping () characters on DefaultValues
--									- don't return the SET IDENTITY_INSERT ON / OFF if no real data will be returned
/************************************************/
ALTER PROCEDURE _spAddNewObjectFields AS
BEGIN
	/* create basic object "data" field definitions*/
	SELECT X.ObjectID, X.FieldName, Name = replace(X.Name, 'Customer', 'Shipper'), OFTID, X.DefaultValue, X.AllowNullID, X.IsKey, X.IsCustom, X.ParentObjectID, X.ParentObjectIDFieldName
	INTO #X
	FROM (
		SELECT ObjectID = O.ID, FieldName = C.COLUMN_NAME, Name = replace(dbo.fnFriendlyName(C.COLUMN_NAME), 'Order Date', 'Date')
			, OFTID = CASE	WHEN C.DATA_TYPE LIKE '%char%' OR C.DATA_TYPE LIKE 'TEXT' THEN 1
							WHEN C.DATA_TYPE IN ('bit') THEN 2
							WHEN C.DATA_TYPE LIKE '%int%' THEN 3
							WHEN C.DATA_TYPE in ('decimal', 'float', 'money', 'smallmoney') THEN 4
							WHEN C.DATA_TYPE LIKE '%datetime%' THEN 5
							WHEN C.DATA_TYPE LIKE '%date%' THEN 6
							WHEN C.DATA_TYPE LIKE '%time%' THEN 7
							WHEN C.DATA_TYPE LIKE 'unique%' THEN 8
							ELSE NULL END
			, DefaultValue = C.COLUMN_DEFAULT
			, AllowNullID = CASE WHEN C.IS_NULLABLE = 'YES' OR C.COLUMN_DEFAULT IS NOT NULL OR COLUMNPROPERTY(object_id(TABLE_NAME), COLUMN_NAME, 'IsIdentity') = 1 THEN 1 ELSE 0 END
			, IsKey = 0
			, IsCustom = 0
			, ParentObjectID = NULL, ParentObjectIDFieldName = NULL
		FROM INFORMATION_SCHEMA.COLUMNS C
		JOIN tblObject O ON isnull(O.SqlTargetName, O.SqlSourceName) LIKE C.TABLE_NAME
		WHERE C.COLUMN_NAME NOT LIKE '%ID' 
			AND C.DATA_TYPE NOT IN ('varbinary')
			AND C.COLUMN_NAME NOT LIKE '%DocName'
			AND C.COLUMN_NAME NOT LIKE '%FileName'
			AND C.COLUMN_NAME NOT LIKE '%CDL%'
			AND C.COLUMN_NAME NOT LIKE 'Create%'
			AND C.COLUMN_NAME NOT LIKE '%LastChange%'
			AND C.COLUMN_NAME NOT LIKE 'Delete%'

		/* add PK fields for each object */
		UNION SELECT ObjectID = O.ID, FieldName = C.COLUMN_NAME, Name = dbo.fnFriendlyName(C.COLUMN_NAME)
			, OFTID = CASE	WHEN C.DATA_TYPE LIKE '%char%' OR C.DATA_TYPE LIKE 'TEXT' THEN 1
							WHEN C.DATA_TYPE IN ('bit') THEN 2
							WHEN C.DATA_TYPE LIKE '%int%' THEN 3
							WHEN C.DATA_TYPE in ('decimal', 'float', 'money', 'smallmoney') THEN 4
							WHEN C.DATA_TYPE LIKE '%datetime%' THEN 5
							WHEN C.DATA_TYPE LIKE '%date%' THEN 6
							WHEN C.DATA_TYPE LIKE '%time%' THEN 7
							WHEN C.DATA_TYPE LIKE 'unique%' THEN 8
							ELSE NULL END
			, DefaultValue = NULL
			, AllowNullID = CASE WHEN C.IS_NULLABLE = 'YES' OR C.COLUMN_DEFAULT IS NOT NULL OR COLUMNPROPERTY(object_id(TABLE_NAME), COLUMN_NAME, 'IsIdentity') = 1 THEN 1 ELSE 0 END
			, IsKey = 1
			, IsCustom = 0
			, ParentObjectID = NULL, ParentObjectIDFieldName = NULL
		FROM INFORMATION_SCHEMA.COLUMNS C
		JOIN tblObject O ON isnull(O.SqlTargetName, O.SqlSourceName) LIKE C.TABLE_NAME
		WHERE C.COLUMN_NAME IN ('ID', 'UID')

		-- add in LINKAGE (foreign key) columns
		UNION SELECT ObjectID = O.ID, FieldName = C.COLUMN_NAME, Name = dbo.fnFriendlyName(C.COLUMN_NAME)
			, OFTID = CASE	WHEN C.DATA_TYPE LIKE '%char%' OR C.DATA_TYPE LIKE 'TEXT' THEN 1
							WHEN C.DATA_TYPE IN ('bit') THEN 2
							WHEN C.DATA_TYPE LIKE '%int%' THEN 3
							WHEN C.DATA_TYPE in ('decimal', 'float', 'money', 'smallmoney') THEN 4
							WHEN C.DATA_TYPE LIKE '%datetime%' THEN 5
							WHEN C.DATA_TYPE LIKE '%date%' THEN 6
							WHEN C.DATA_TYPE LIKE '%time%' THEN 7
							WHEN C.DATA_TYPE LIKE 'unique%' THEN 8
							ELSE NULL END
			, DefaultValue = NULL
			, AllowNullID = CASE WHEN C.IS_NULLABLE = 'YES' OR C.COLUMN_DEFAULT IS NOT NULL OR COLUMNPROPERTY(object_id(TABLE_NAME), COLUMN_NAME, 'IsIdentity') = 1 THEN 1 ELSE 0 END
			, IsKey = 0
			, IsCustom = 0
			, ParentObjectID = OFP.ObjectID
			, ParentObjectIDFieldName = OFP.Name
		FROM tblObject O
		JOIN INFORMATION_SCHEMA.COLUMNS C ON C.TABLE_NAME = isnull(O.SqlTargetName, O.SqlSourceName) AND C.COLUMN_NAME NOT IN ('EIAPADDRegionID')
		JOIN ( 
			SELECT OFP.ID, OFP.ObjectID, OFP.Name, CName = replace(OP.Name, ' ', '') + OFP.FieldName
			FROM tblObject OP 
			JOIN tblObjectField OFP ON OFP.ObjectID = OP.ID
			WHERE OFP.IsKey = 1
		) OFP ON CASE WHEN C.COLUMN_NAME = 'StatusID' THEN 'OrderStatusID' ELSE replace(C.COLUMN_NAME, 'Customer', 'Shipper') END LIKE '%' + OFP.CName 
	) X
	LEFT JOIN tblObjectField OBF ON OBF.ObjectID = X.ObjectID AND OBF.FieldName = X.FieldName
	WHERE OBF.ID IS NULL AND X.ObjectID NOT IN (28)
	ORDER BY X.ObjectID, X.FieldName

	-- return the sql statements to generate these new fields
	DECLARE @maxID int
	SELECT @maxID = max(ID) FROM tblObjectField WHERE ID < 90000

	-- strip any wrapping () on DefaultValues (these are put there by Sql Server for some reason - on the actual underlying table default constraint)
	WHILE EXISTS (SELECT * FROM #X WHERE DefaultValue LIKE '(%)')
		UPDATE #X SET DefaultValue = substring(DefaultValue, 2, len(DefaultValue) - 2) WHERE DefaultValue LIKE '(%)'

	SELECT Sql
	FROM (
		SELECT SortNum = 0, Sql = 'SET IDENTITY_INSERT tblObjectField ON' WHERE EXISTS (SELECT * FROM #X)
		UNION
		SELECT SortNum = 1, Sql = 'INSERT INTO tblObjectField (ID, ObjectID, FieldName, Name, ObjectFieldTypeID, DefaultValue, AllowNullID, IsKey, IsCustom, ParentObjectID, ParentObjectIDFieldName)' + char(13) + char(10)
			+ 'SELECT ' + ltrim(@maxID + ROW_NUMBER() OVER (ORDER BY ObjectID, FieldName)) + ',' + dbo.fnQSI(ObjectID) + ',' + dbo.fnQS(FieldName) + ',' + dbo.fnQS(Name) + ','+ dbo.fnQSI(OFTID) + ',' + dbo.fnQS(DefaultValue) + ',' + dbo.fnQSI(AllowNullID) + ',' + dbo.fnQSI(IsKey) + ',' + dbo.fnQSI(IsCustom) + ',' + dbo.fnQSI(ParentObjectID) + ',' + dbo.fnQS(ParentObjectIDFieldName)
		FROM #X
		UNION
		SELECT SortNum = 2, Sql = 'SET IDENTITY_INSERT tblObjectField OFF' WHERE EXISTS (SELECT * FROM #X)
	) SQL
	ORDER BY SortNum
END

GO

COMMIT
SET NOEXEC OFF