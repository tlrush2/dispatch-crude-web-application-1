-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.1.0'
SELECT  @NewVersion = '4.1.0.1'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-945 - Add Independent Driver/Driver Group Settlement - update'
	UNION SELECT @NewVersion, 0, 'Settlement Enhancements - add new user rights into new users/groups/permissions subsystem'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

EXEC spAddNewPermission 'viewDriverAssessorialRates', 'Allow user to view the Driver Assessorial Rates page', 'Settlement - Driver Rates', 'Assessorial'
EXEC spAddNewPermission 'viewDriverFuelSurchargeRates', 'Allow user to view the Driver Fuel Surcharge Rates page', 'Settlement - Driver Rates', 'Fuel Surcharge'
EXEC spAddNewPermission 'viewDriverOriginWaitRates', 'Allow user to view the Driver Origin Wait Rates page', 'Settlement - Driver Rates', 'Origin Wait'
EXEC spAddNewPermission 'viewDriverDestinationWaitRates', 'Allow user to view the Driver Destination Wait Rates page', 'Settlement - Driver Rates', 'Destination Wait'
EXEC spAddNewPermission 'viewDriverOrderRejectRates', 'Allow user to view the Driver Order Reject Rates page', 'Settlement - Driver Rates', 'Order Reject'
EXEC spAddNewPermission 'viewDriverRateSheets', 'Allow user to view the Driver Rate Sheets page', 'Settlement - Driver Rates', 'Rate Sheets'
EXEC spAddNewPermission 'viewDriverRouteRates', 'Allow user to view the Driver Route Rates page', 'Settlement - Driver Rates', 'Route Rates'
EXEC spAddNewPermission 'viewDriverWaitFeeParameters', 'Allow user to view the Driver Wait Fee Parameters page', 'Settlement - Driver Rates', 'Wait Fee Params'
EXEC spAddNewPermission 'viewDriverSettlement', 'Allow user to view Driver Order Settlement page', 'Settlement', 'Settle Drivers'
EXEC spAddNewPermission 'viewDriverSettlementBatches', 'Allow user to view the Driver Settlement Batches page', 'Settlement', 'Driver Batches'
EXEC spAddNewPermission 'unsettleDriverBatches', 'Allow user to unsettle a driver settlement batch', 'Settlement', 'Unsettle Driver Batches'
UPDATE aspnet_Roles SET Category = 'Settlement - Driver Rates', RoleName = 'viewDriverSettlementGroups', LoweredRoleName = LOWER('viewDriverSettlementGroups') WHERE RoleId = 'B1C52FBE-ABB4-41C7-B03B-44CF851306F0'
UPDATE aspnet_Roles SET Category = 'Settlement - Driver Rates', RoleName = 'viewDriverSettlementGroupAssignments', LoweredRoleName = LOWER('viewDriverSettlementGroupAssignments') WHERE RoleId = '02E55ABD-C867-4975-96E3-A993895458BC'
GO

/********************************************************/
-- Created: 4.0.0 - 2016/6/28 - Ben Bloodworth
-- Purpose: To add permissions to users when the group(s) they are in are given more permissions.
--			The permissions will only be added if they are not already provided by another group.
-- Changes: 
-- 2016/07/21 - ?.?.?	- BB	- Removed Delete portion and turned this into an insert only trigger. spDeleteGroupAndUserPermissions now takes care of the delete task.
-- 2016/08/27 - 4.1.0.1	- KDA	- cleanup code and ensure it won't fail if an insert is invoked that affects multiple rows
/********************************************************/
ALTER TRIGGER trigRolesInGroups_I ON aspnet_RolesInGroups AFTER INSERT AS 
BEGIN
	-- Create the new permission record for each user that is currently in the affected group
	INSERT INTO aspnet_UsersInRoles (UserId, RoleId)
		SELECT DISTINCT UG.UserId, i.RoleId
		FROM inserted i
		JOIN aspnet_UsersInGroups UG ON UG.UserId = i.GroupId
		EXCEPT
		SELECT UserId, RoleId FROM aspnet_UsersInRoles
END 
GO

/********************************************************/
-- Created: 4.0.0 - 2016/6/22 - Ben Bloodworth
-- Purpose: When a group is assigned to a user this trigger will add records to
--	        aspnet_UsersInRoles matching the permissions associated with the users new group.
--			Permissions the user already has assigned to them from another group will not be added a second time.
-- Changes: 
-- 2016/08/27 - 4.1.0.1	- KDA	- cleanup code and ensure it won't fail if an insert is invoked that affects multiple rows
/********************************************************/
ALTER TRIGGER trigUsersInGroups_I ON aspnet_UsersInGroups AFTER INSERT AS 
BEGIN		
	INSERT INTO aspnet_UsersInRoles (UserId, RoleId)
		SELECT DISTINCT i.UserId, RG.RoleId
		FROM inserted i
		JOIN aspnet_RolesInGroups RG ON RG.GroupId = i.GroupId
		EXCEPT
		SELECT Userid, RoleId FROM aspnet_UsersInRoles
END
GO

COMMIT
SET NOEXEC OFF