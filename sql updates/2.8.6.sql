/*
	-- add routine to Deactivate stale origins/destinations
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.8.5'
SELECT  @NewVersion = '2.8.6'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 19 Dec 2012
-- Description:	trigger to ensure the UomID for Carrier/Customer Route Rates matches that assigned to the Origin
-- =============================================
ALTER TRIGGER [dbo].[trigOrigin_IU] ON [dbo].[tblOrigin] AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	IF (UPDATE(UomID))
	BEGIN
		-- update matching CarrierRouteRates.UomID to match what is assigned to the new Origin
		UPDATE tblCarrierRouteRates 
		  SET UomID = i.UomID, LastChangeDateUTC = i.LastChangeDateUTC, LastChangedByUser = i.LastChangedByUser
		FROM tblCarrierRouteRates CRR
		JOIN tblRoute R ON R.ID = CRR.RouteID
		JOIN inserted i ON i.ID = R.OriginID
		JOIN deleted d ON d.ID = i.ID
		WHERE d.UomID <> i.UomID AND CRR.EffectiveDate <= dbo.fnDateOnly(GETDATE())

		-- update matching CustomerRouteRates.UomID to match what is assigned to the new Origin
		UPDATE tblCustomerRouteRates 
		  SET UomID = i.UomID, LastChangeDateUTC = i.LastChangeDateUTC, LastChangedByUser = i.LastChangedByUser
		FROM tblCustomerRouteRates CRR
		JOIN tblRoute R ON R.ID = CRR.RouteID
		JOIN inserted i ON i.ID = R.OriginID
		JOIN deleted d ON d.ID = i.ID
		WHERE d.UomID <> i.UomID AND CRR.EffectiveDate <= dbo.fnDateOnly(GETDATE())
	END
END

GO

INSERT INTO tblSetting (ID, Name, SettingTypeID, Value, Category, CreateDateUTC, CreatedByUser, ReadOnly)
	SELECT 21, 'Stale Origin - Days', 3, 90, 'System Maintenance', GETUTCDATE(), 'System', 0
	UNION
	SELECT 22, 'Stale Destination - Days', 3, 90, 'System Maintenance', GETUTCDATE(), 'System', 0
GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 19 Jun 2014
-- Description:	deactivate any currently active Origins that haven't had any traffic since the Stale days threshold
-- =============================================
CREATE PROCEDURE spDeactivateStaleOrigins
(
  @UserName varchar(100)
) 
AS BEGIN
	DECLARE @staleDays int
	SELECT @staleDays = Value FROM tblSetting WHERE ID = 21
	
	UPDATE tblOrigin 
		SET DeleteDateUTC = GETUTCDATE(), DeletedByUser = @UserName
	WHERE DeleteDateUTC IS NULL
		AND ID NOT IN (SELECT OriginID FROM viewOrder WHERE OrderDate > DATEADD(day, -@staleDays, getdate()))
END

GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 19 Jun 2014
-- Description:	deactivate any currently active Destinations that haven't had any traffic since the Stale days threshold
-- =============================================
CREATE PROCEDURE spDeactivateStaleDestinations
(
  @UserName varchar(100)
) 
AS BEGIN
	DECLARE @staleDays int
	SELECT @staleDays = Value FROM tblSetting WHERE ID = 21
	
	UPDATE tblDestination
		SET DeleteDateUTC = GETUTCDATE(), DeletedByUser = @UserName
	WHERE DeleteDateUTC IS NULL
		AND ID NOT IN (SELECT DestinationID FROM viewOrder WHERE OrderDate > DATEADD(day, -@staleDays, getdate()))
END

GO

COMMIT
SET NOEXEC OFF