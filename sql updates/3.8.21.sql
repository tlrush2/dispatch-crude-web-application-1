-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.8.20.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.8.20.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0

DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.8.20'
SELECT  @NewVersion = '3.8.21'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'C.O.D.E. Export changes per Steve Carver with Plains. Changes are specific to Plains and deviate from standard CODE format.'	
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/*=============================================
	Author:			Ben Bloodworth
	Create date:	5 Jun 2015
	Description:	Displays the base information for the CODE export. Please note that some values could change per DispatchCrude customer and thus it may
					be better to retrieve these settings from the Data Exchange settings instead of this view.
					
	Updates:		8/21/15 - BB: Added CASE statments to account for Nulls in gravity readings and added additional filters for rejects and ticket types
					8/26/15 - BB: Altered tank number field to use OriginTankText instead of TankNum.  Altered Property_Code per instructions from customer
=============================================*/
ALTER VIEW [dbo].[viewOrderExport_CODE_Base] AS
SELECT 
	OrderID = O.ID
	,OrderTicketID = T.ID
	,Record_ID = '2'
	,Company_ID = C.CodeDXCode
	,Ticket_Type = TT.CodeDXCode
	,Transmit_Ind = '2'  -- Hard coded per Angie Toews (PDS Energy) but may need to be changed to dynamic if the customer says so
	,Run_Type = 1
	,Ticket_Number = CASE WHEN ISNUMERIC(RIGHT(T.CarrierTicketNum,1)) = 0 THEN dbo.fnFixedLenStr(T.CarrierTicketNum,7) 
						  ELSE dbo.fnFixedLenNum(T.CarrierTicketNum,7,0,0) 
					 END
	,Run_Ticket_Date = dbo.fnDateMMDDYY(O.OrderDate)
	
	--Property code is a non-standard CODE field for Plains (per Steve Carver).  It is a mixture of origin leasnum and destination station num
	,Property_Code = (CASE WHEN O.LeaseNum IS NULL THEN '000000' ELSE CAST(dbo.fnFixedLenNum(O.LeaseNum,6,0,0) AS VARCHAR(6)) END)
					+ '000'
					+ (CASE WHEN O.DestStation IS NULL THEN '00000' ELSE dbo.fnFixedLenStr(O.DestStation,5) END)
					
	,Gathering_Charge = '0'  -- Hard coded per Angie Toews (PDS Energy) but may need to be changed to dynamic if the customer says so
	,Tank_Meter_Num = CAST(dbo.fnFixedLenNum(T.OriginTankText, 6, 0, 0) AS VARCHAR(6)) + '0'	
	,Adjustment_Ind = '0' -- Hard coded per Angie Toews (PDS Energy) but may need to be changed to dynamic if the customer says so
	,Opening_Reading = dbo.fnOpenCloseReadingCODEFormat(T.ID,TT.CodeDXCode,0)
	,Opening_Date = dbo.fnDateMMDD(O.OrderDate)
	,Closing_Reading = dbo.fnOpenCloseReadingCODEFormat(T.ID,TT.CodeDXCode,1)
	,Closing_Date = dbo.fnDateMMDD(O.DestDepartTime)
	,Meter_Factor = CASE WHEN T.MeterFactor IS NULL THEN '00000000' 
						 ELSE dbo.fnFixedLenNum(T.MeterFactor, 2, 6, 1) 
					END
	,Shrinkage_Incrustation = '1000000' -- Hard coded per Angie Toews (PDS Energy) but may need to be changed to dynamic if the customer says so
	,Opening_Temperature = CASE WHEN T.ProductHighTemp IS NULL THEN '0000' 
								ELSE dbo.fnFixedLenNum(T.ProductHighTemp, 3, 1, 1) 
						   END
	,Closing_Temperature = CASE WHEN T.ProductLowTemp IS NULL THEN '0000' 
								ELSE dbo.fnFixedLenNum(T.ProductLowTemp, 3, 1, 1) 
						   END
	,BSW = CASE WHEN TT.ID IN (1,3) THEN dbo.fnFixedLenNum(T.ProductBSW, 2, 2, 1) --Non-Estmated ticket types (Gauge Run, Meter Run)					
				ELSE '0000'
		   END	
	,Observed_Gravity =	CASE WHEN T.ProductObsGravity IS NULL THEN '000'
							 ELSE dbo.fnFixedLenNum(T.ProductObsGravity, 2, 1, 1)
						END
	,Observed_Temperature = dbo.fnFixedLenNum(T.ProductObsTemp, 3, 1, 1)
	,Pos_Neg_Code = '0' -- Hard coded per Angie Toews (PDS Energy) but may need to be changed to dynamic if the customer says so
	,Total_Net_Volume = dbo.fnFixedLenNum(T.NetUnits, 7, 2, 1)
	,Shippers_Net_Volume = dbo.fnFixedLenNum(T.NetUnits, 7, 2, 1)
	,Corrected_Gravity = CASE WHEN T.Gravity60F IS NULL THEN '000'
							  ELSE dbo.fnFixedLenNum(T.Gravity60F, 2, 1, 1)
						 END
	,Product_Code = dbo.fnFixedLenStr('',3)	--Leave 3 blank spaces unless product is not crude oil. For others lookup PETROEX code	
	,Transmission_Date = dbo.fnFixedLenStr('',3) --Leave 3 blank spaces. Filled in by export reciever.
FROM viewOrderLocalDates O
	JOIN dbo.viewOrderCustomerFinalExportPending EP ON EP.ID = O.ID
	LEFT JOIN viewOrderTicket T ON T.OrderID = O.ID AND T.DeleteDateUTC IS NULL
	LEFT JOIN tblTicketType TT ON TT.ID = O.TicketTypeID
	LEFT JOIN tblCustomer C ON C.ID = O.CustomerID
	LEFT JOIN tblProduct P ON P.ID = O.ProductID
WHERE O.DeleteDateUTC IS NULL
	AND O.Rejected = 0 -- No rejected orders
	AND T.Rejected = 0 -- No rejected tickets
	AND O.StatusID = 4 -- Only Audited orders
	AND TT.ID IN (1,7) -- Only Gauge Run and Gauge Net tickets
GO

EXEC _spRebuildAllObjects
GO

COMMIT
SET NOEXEC OFF