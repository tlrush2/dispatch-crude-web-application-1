-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.7.12.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.7.12.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.7.12'
SELECT  @NewVersion = '3.7.13'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'fnOrderCarrierAssessorialAmounts: bug fix for Carrier Assessorial using "% of Shipper" rate type'
	UNION
	SELECT @NewVersion, 0, 'Carrier|Shipper OrderOrigin|Destination Wait Rate functions: bug fix for preventing a "missing rate warning" in settlement pages when no matching Origin|Destination Wait Rate is found (but should)'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier AssessorialRate Amounts info for the specified order
-- Changes:
	-- 3.7.13 - 5/19/2015 - KDA - fix to "% of Shipper" rates
/***********************************/
ALTER FUNCTION fnOrderCarrierAssessorialAmounts(@ID int, @Units decimal(18, 10), @UomID int) RETURNS TABLE AS RETURN
	SELECT AssessorialRateTypeID = TypeID, RateID, RateTypeID, Amount
	FROM (
		SELECT TypeID, RateID = ID, RateTypeID
			, Amount = dbo.fnRateToAmount(RateTypeID, @Units, @UomID, Rate, UomID, OSC.LoadAmount
			, CASE WHEN RateTypeID = 3 THEN (SELECT TOP 1 Amount FROM tblOrderSettlementShipperAssessorialCharge WHERE OrderID = @ID AND AssessorialRateTypeID = AssessorialRateTypeID) ELSE NULL END
			, NULL)
		FROM dbo.fnOrderCarrierAssessorialRates(@ID)
		JOIN tblOrderSettlementCarrier OSC ON OSC.OrderID = @ID
	) X
	WHERE Amount IS NOT NULL
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier DestinationWaitRate info for the specified order
-- Changes
	-- 3.7.13 - 5/21/15 - KDA - fix bug which prevented missing rate warning when billable minutes existed but no rate matched
/***********************************/
ALTER FUNCTION [dbo].[fnOrderCarrierDestinationWaitRate](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, Minutes = O.DestMinutes, R.Rate
	FROM viewOrder O
	OUTER APPLY dbo.fnCarrierDestinationWaitRate(O.OrderDate, null, O.DestWaitReasonID, O.CustomerID, O.CarrierID, O.ProductGroupID, O.DestinationID, O.DestStateID, O.DestRegionID, 1) R
	WHERE O.ID = @ID
GO
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Carrier OriginWaitRate info for the specified order
-- Changes
	-- 3.7.13 - 5/21/15 - KDA - fix bug which prevented missing rate warning when billable minutes existed but no rate matched
/***********************************/
ALTER FUNCTION [dbo].[fnOrderCarrierOriginWaitRate](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, Minutes = O.OriginMinutes, R.Rate
	FROM viewOrder O
	CROSS APPLY dbo.fnCarrierOriginWaitRate(O.OrderDate, null, O.OriginWaitReasonID, O.CustomerID, O.CarrierID, O.ProductGroupID, O.OriginID, O.OriginStateID, O.OriginRegionID, 1) R
	WHERE O.ID = @ID
GO

/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper DestinationWaitRate info for the specified order
-- Changes
	-- 3.7.13 - 5/21/15 - KDA - fix bug which prevented missing rate warning when billable minutes existed but no rate matched
/***********************************/
ALTER FUNCTION [dbo].[fnOrderShipperDestinationWaitRate](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, Minutes = O.DestMinutes, R.Rate
	FROM viewOrder O
	OUTER APPLY dbo.fnShipperDestinationWaitRate(O.OrderDate, null, O.DestWaitReasonID, O.CustomerID, O.ProductGroupID, O.DestinationID, O.DestStateID, O.DestRegionID, 1) R
	WHERE O.ID = @ID
GO
/***********************************/
-- Date Created: 21 Dec 2014
-- Author: Kevin Alons
-- Purpose: retrieve and return the Shipper OriginWaitRate info for the specified order
-- Changes
	-- 3.7.13 - 5/21/15 - KDA - fix bug which prevented missing rate warning when billable minutes existed but no rate matched
/***********************************/
ALTER FUNCTION [dbo].[fnOrderShipperOriginWaitRate](@ID int) RETURNS TABLE AS RETURN
	SELECT R.ID, Minutes = O.OriginMinutes, R.Rate
	FROM viewOrder O
	CROSS APPLY dbo.fnShipperOriginWaitRate(O.OrderDate, null, O.OriginWaitReasonID, O.CustomerID, O.ProductGroupID, O.OriginID, O.OriginStateID, O.OriginRegionID, 1) R
	WHERE O.ID = @ID
GO

COMMIT
SET NOEXEC OFF