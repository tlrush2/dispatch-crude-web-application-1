/* 
  add new Mobile App settings (Pickup Max BackDate Hours, Max Forward Date Hours)
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.2.9'
SELECT  @NewVersion = '2.3.0'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

INSERT INTO tblSetting (ID, Name, SettingTypeID, Value, Category, CreateDateUTC, CreatedByUser)
	SELECT 14, 'Pickup BackDate Max Hours', 3, 6, 'Driver App Settings', GETUTCDATE(), 'System'
INSERT INTO tblSetting (ID, Name, SettingTypeID, Value, Category, CreateDateUTC, CreatedByUser)
	SELECT 15, 'Deliver Forward Max Hours', 3, 6, 'Driver App Settings', GETUTCDATE(), 'System'
GO

COMMIT
SET NOEXEC OFF