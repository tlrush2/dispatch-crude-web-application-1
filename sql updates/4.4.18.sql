SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.4.17'
SELECT  @NewVersion = '4.4.18'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'JT-448 - Sysetm Settings cleanup'	
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


-- Add "Hidden" column to tblSetting.  This is to be used to remove settings from the UI so we
-- no longer need to permanently delete records from this table when settings become obsolete
ALTER TABLE tblSetting
ADD Hidden BIT NOT NULL 
CONSTRAINT DF_Setting_Hidden DEFAULT 0
GO

-- Change category for several settings
UPDATE tblSetting
SET Category = 'System Preferences'
WHERE ID IN (21,22,34,58,62,64,67)
GO

-- Change category for several settings
UPDATE tblSetting
SET Category = 'System Maintenance'
WHERE Category = 'System Wide'
GO

-- Change category for 'Wait Notes requires threshold minutes' setting
UPDATE tblSetting
SET Category = 'Mobile App - All'
WHERE ID = 6
GO

-- Put DBVersion setting in System Maintenance category
UPDATE tblSetting
SET Category = 'System Maintenance'
WHERE ID = 0
GO

-- Hide obsolete settings
UPDATE tblSetting
SET Hidden = 1
WHERE ID IN (7,15,32,43)
GO

-- Refresh viewSetting
EXEC _spRefreshAllViews
GO


COMMIT
SET NOEXEC OFF