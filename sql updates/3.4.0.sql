-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.3.6'
SELECT  @NewVersion = '3.4.0'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Add TABLE tblCountry'
	UNION
	SELECT @NewVersion, 0, 'Add TABLE tblReportColumnDefinitionCountryException'
	UNION
	SELECT @NewVersion, 0, 'Revise VIEW viewReportColumnDefinition'
	UNION
	SELECT @NewVersion, 0, 'Add COLUMN tblState.CountryID'
	UNION
	SELECT @NewVersion, 0, 'Add SYSTEM SETTING "System Wide.Country"'
	UNION
	SELECT @NewVersion, 1, 'Add basic support for CANADA (System-wide)'
GO

CREATE TABLE tblCountry (ID int not null constraint PK_Country primary key, Name varchar(50) not null, Abbrev varchar(5) not null)
GO
GRANT SELECT ON tblCountry TO dispatchcrude_iis_acct
GO
CREATE UNIQUE INDEX udxCountry_Name ON tblCountry(Name)
GO
CREATE UNIQUE INDEX udxCountry_Abbrev ON tblCountry(Abbrev)
GO
insert into tblCountry 
	select 1, 'United States', 'U.S.'
	union select 2, 'Canada', 'CAN'

alter table tblState ADD CountryID int constraint FK_State_Country FOREIGN KEY REFERENCES tblCountry(ID)
go

update tblState set countryid = 1
go

insert into tblState (Abbreviation, FullName, CreateDateUTC, CreatedByUser, eiapaddregionID, countryid)
	select 'AB', 'Alberta', GETUTCDATE(), 'System', null, 2
	union select 'BC', 'British Columbia', GETUTCDATE(), 'System', null, 2
	union select 'MB', 'Manitoba', GETUTCDATE(), 'System', null, 2
	union select 'NB', 'New Brunswick', GETUTCDATE(), 'System', null, 2
	union select 'NL', 'Newfoundland and Labrador', GETUTCDATE(), 'System', null, 2
	union select 'NS', 'Nova Scotia', GETUTCDATE(), 'System', null, 2
	union select 'NT', 'Northwest Territories', GETUTCDATE(), 'System', null, 2
	union select 'NU', 'Nunavut', GETUTCDATE(), 'System', null, 2
	union select 'ON', 'Ontario', GETUTCDATE(), 'System', null, 2
	union select 'PE', 'Prince Edward Island', GETUTCDATE(), 'System', null, 2
	union select 'QC', 'Quebec', GETUTCDATE(), 'System', null, 2
	union select 'SK', 'Saskatchewan', GETUTCDATE(), 'System', null, 2
	union select 'YT', 'Yukon', GETUTCDATE(), 'System', null, 2

INSERT INTO tblSetting (ID, Category, Name, Value, SettingTypeID, ReadOnly, CreateDateUTC, CreatedByUser)
	select 36, 'System Wide', 'Country [US=1, CAN=2]', 1, 3, 0, GETUTCDATE(), 'System'
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tblReportColumnDefinition]') AND name = N'udxReportColumnDefinition_Report_Caption')
	DROP INDEX [udxReportColumnDefinition_Report_Caption] ON [dbo].[tblReportColumnDefinition] WITH ( ONLINE = OFF )
GO

CREATE INDEX udxReportColumnDefinition_Report_Caption ON tblReportColumnDefinition(ReportID, Caption)
GO

CREATE TABLE tblReportColumnDefinitionCountryException
(
  ID int identity(1, 1) not null constraint PK_ReportColumnDefinitionCountryException primary key
, ReportColumnID int not null constraint FK_ReportColumnDefinitionCountryException_ReportColumnDefinition foreign key references tblReportColumnDefinition(ID)
, CountryID int not null constraint FK_ReportColumnDefinitionCountryException_Country foreign key references tblCountry(ID)
, Caption varchar(50) not null
)
GO
GRANT SELECT ON tblReportColumnDefinitionCountryException TO dispatchcrude_iis_acct
GO

insert into tblReportColumnDefinitionCountryException (ReportColumnID, CountryID, Caption)
	select 58, 2, 'Origin LSD'
	union
	select 108, 2, 'Ticket Water Cut'
	union
	select 43, 2, 'Destination Water Cut'
	union
	select 44, 2, 'Destination Density'
	union
	select 107, 2, 'Ticket Density'
	union
	select 41, 2, 'Actual Kilometers'
GO

set identity_insert tblReportColumnDefinition on
INSERT INTO tblReportColumnDefinition(id, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport)
	SELECT 90002, 1, 'SELECT CASE WHEN dbo.fnSettingValue(36) = 2 THEN ''C'' ELSE ''F'' END', 'Unit of Temperature', null, null, 1, null, 1, '*', 0
set identity_insert tblReportColumnDefinition off
go

ALTER VIEW [dbo].[viewReportColumnDefinition] AS
	SELECT RCD.ID
		, RCD.ReportID
		, RCD.DataField
		, Caption = coalesce(CE.Caption, RCD.Caption, RCD.DataField)
		, RCD.DataFormat
		, RCD.FilterDataField
		, RCD.FilterTypeID
		, RCD.FilterDropDownSql
		, RCD.FilterAllowCustomText
		, RCD.AllowedRoles
		, RCD.OrderSingleExport
		, RFT.FilterOperatorID_CSV
		, BaseFilterCount = (SELECT COUNT(*) FROM tblReportColumnDefinitionBaseFilter WHERE ReportColumnID = RCD.ID)
		, FilterOperatorID_CSV_Delim = ',' + RFT.FilterOperatorID_CSV + ','
	FROM tblReportColumnDefinition RCD
	LEFT JOIN tblReportColumnDefinitionCountryException CE ON CE.CountryID = dbo.fnSettingValue(36) AND CE.ReportColumnID = RCD.ID
	JOIN tblReportFilterType RFT ON RFT.ID = RCD.FilterTypeID;

GO

ALTER TABLE tblTicketType ADD CountryID_CSV varchar(100) NOT NULL constraint DF_TicketType_CountryID_CSV DEFAULT ('1')
GO

CREATE VIEW viewTicketType AS
	SELECT *, CountryID_CSV_Delim = ',' + CountryID_CSV + ','
	FROM tblTicketType
GO

/******************************************
-- Date Created: 11/5/2014
-- Author: Kevin Alons
-- Purpose: return the ticket types that are valid for the current system-wide Country
*******************************************/
CREATE FUNCTION fnTicketTypeForCountry() RETURNS TABLE AS RETURN
SELECT * FROM dbo.viewTicketType WHERE CountryID_CSV_Delim LIKE '%' + dbo.fnSettingValue(36) + '%'
GO
GRANT SELECT ON fnTicketTypeForCountry TO dispatchcrude_iis_acct
GO

/******************************************
-- Date Created: 11/5/2014
-- Author: Kevin Alons
-- Purpose: return the sttes that are valid for the current system-wide Country
*******************************************/
CREATE FUNCTION fnStateForCountry() RETURNS TABLE AS RETURN
SELECT * FROM dbo.tblState S WHERE CountryID = dbo.fnSettingValue(36)
GO
GRANT SELECT ON fnStateForCountry TO dispatchcrude_iis_acct
GO

EXEC _spRebuildAllObjects
GO

COMMIT
SET NOEXEC OFF