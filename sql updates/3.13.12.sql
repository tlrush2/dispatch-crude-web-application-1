SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20) = '3.13.11'
	, @NewVersion varchar(20) = '3.13.12'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1625 - Add Origin lease number to truck and gauger create pages'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/**********************************
-- Date Created: 18 Apr 2015
-- Author: Kevin Alons
-- Purpose: return GaugerOrder records with "translated friendly" values for FK relationships
-- Changes:
	-- 22 May 2015 - KDA - add "TicketCount" field
	- 3.11.19 - 2016/04/29 - BB - Add Job number and contract number fields
	- 3.13.12 - 2016/8/3 - BB - Add lease number DCWEB-1625
***********************************/
ALTER VIEW viewGaugerOrder AS
SELECT GAO.*
	, OrderNum = O.OrderNum
	, OrderDate = O.OrderDate
	, Status = GOS.Name
	, CarrierTicketNum = O.CarrierTicketNum
	, DispatchConfirmNum = O.DispatchConfirmNum
	, JobNumber = O.JobNumber
	, ContractNumber = O.ContractNumber
	, Gauger = G.FullName
	, OriginID = O.OriginID
	, Origin = O.Origin
	, OriginFull = O.OriginFull
	, OriginState = O.OriginState
	, OriginStateAbbrev = O.OriginStateAbbrev
	, LeaseNum = O.LeaseNum  -- 3.13.12
	, CustomerID = O.CustomerID
	, ShipperID = O.CustomerID
	, Shipper = O.Customer
	, DestinationID = O.DestinationID
	, Destination = O.Destination
	, DestinationFull = O.DestinationFull
	, DestinationState = O.DestinationState
	, DestinationStateAbbrev = O.DestinationStateAbbrev
	, ProductID = O.ProductID
	, Product = O.Product
	, ProductShort = O.ProductShort
	, ProductGroupID = O.ProductGroupID
	, ProductGroup = O.ProductGroup
	, OriginTankText = ISNULL(OT.TankNum, GAO.OriginTankNum)
	, P.PriorityNum
	, TicketType = GTT.Name
	, PrintStatusID = O.PrintStatusID
	, OrderStatusID = O.StatusID
	, DeleteDateUTC = O.DeleteDateUTC
	, DeletedByUser = O.DeletedByUser
	, TicketCount = (SELECT COUNT(*) FROM tblGaugerOrderTicket GOT WHERE GOT.OrderID = GAO.OrderID AND GOT.DeleteDateUTC IS NULL)
FROM dbo.tblGaugerOrder GAO 
JOIN viewOrder O ON O.ID = GAO.OrderID 
JOIN dbo.tblGaugerOrderStatus GOS ON GOS.ID = GAO.StatusID
LEFT JOIN dbo.tblOriginTank OT ON OT.ID = GAO.OriginTankID
LEFT JOIN dbo.viewGauger G ON G.ID = GAO.GaugerID
LEFT JOIN dbo.tblGaugerTicketType GTT ON GTT.ID = GAO.TicketTypeID
LEFT JOIN tblPriority P ON P.ID = GAO.PriorityID
GO

/* Refresh/Rebuild/Recompile everything */
EXEC sp_refreshview viewGaugerOrder
GO
EXEC sp_refreshview viewGaugerOrderTicket 
GO
EXEC sp_refreshview viewOrderWithGauger 
GO
EXEC sp_refreshview viewReportCenter_Orders_NoGPS 
GO
EXEC sp_refreshview viewReportCenter_Orders 
GO
EXEC _spRefreshAllViews
GO

EXEC _spRebuildAllObjects
GO
EXEC _spRecompileAllStoredProcedures
GO


COMMIT
SET NOEXEC OFF