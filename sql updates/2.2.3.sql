/* remove tblConsignee table, and Destination.ConsigneeID column & foreign key
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.2.2'
SELECT  @NewVersion = '2.2.3'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

ALTER TABLE tblDestination DROP CONSTRAINT FK_Destination_Consignee
ALTER TABLE tblDestination DROP COLUMN ConsigneeID
GO
DROP TABLE tblConsignee
GO

/***********************************/
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Destination records with translated value and FullName field (which includes Destination Type)
/***********************************/
ALTER VIEW [dbo].[viewDestination] AS
SELECT D.*
	, cast(CASE WHEN D.DeleteDateUTC IS NULL THEN 1 ELSE 0 END as bit) AS Active
	, DT.DestinationType
	, DT.DestinationType + ' - ' + D.Name AS FullName
	, S.FullName AS State, S.Abbreviation AS StateAbbrev
	, R.Name AS Region
	, DTT.Name AS TicketType
	, TZ.Name AS TimeZone
FROM dbo.tblDestination D
JOIN dbo.tblDestinationType DT ON DT.ID = D.DestinationTypeID
JOIN dbo.tblDestTicketType DTT ON DTT.ID = D.TicketTypeID
LEFT JOIN tblState S ON S.ID = D.StateID
LEFT JOIN dbo.tblRegion R ON R.ID = D.RegionID
LEFT JOIN dbo.tblTimeZone TZ ON TZ.ID = D.TimeZoneID

GO

COMMIT
SET NOEXEC OFF