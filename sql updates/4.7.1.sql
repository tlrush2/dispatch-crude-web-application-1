-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.7.0'
SELECT  @NewVersion = '4.7.1'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'JT-3873 - Log changes to settlement history'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


-- ---------------------------------------------------------
-- Clone core view into dbaudit tables (normalize key columns into one view view with audit date column)
-- ---------------------------------------------------------

-- Carrier tables

CREATE TABLE tblCarrierSettlementBatchDbAudit
(
    DbAuditDate DATETIME NOT NULL CONSTRAINT DF_CarrierSettlementBatchDbAudit_DbAuditDate DEFAULT GETUTCDATE(),
	ID INT NOT NULL,
	BatchNum int NOT NULL,
	CarrierID int NOT NULL,
	Notes varchar(255) NULL,
	CreateDateUTC smalldatetime NOT NULL,
	CreatedByUser varchar(100) NULL,
	LastChangeDateUTC smalldatetime NULL,
	LastChangedByUser varchar(100) NULL,
	BatchDate smalldatetime NOT NULL,
	InvoiceNum varchar(50) NULL,
	PeriodEndDate date NOT NULL,
	IsFinal bit NOT NULL
)

GO

CREATE TABLE tblOrderSettlementCarrierDbAudit
(
    DbAuditDate DATETIME NOT NULL CONSTRAINT DF_OrderSettlementCarrierDbAudit_DbAuditDate DEFAULT GETUTCDATE(),
	OrderID int NOT NULL,
	OrderNum INT NULL,
	BatchID int NULL,
	BatchNum INT NULL,
	JobNumber VARCHAR(20) NULL,
	ContractNumber VARCHAR(20) NULL,
	ContractID INT NULL,
	OrderDate date NULL,
	Shipper VARCHAR(40) NULL,
	ShipperID INT NULL,
	Carrier VARCHAR(120) NULL,
	CarrierID INT NULL,
	Origin VARCHAR(50) NULL,
	OriginID INT NULL,
	Destination VARCHAR(50) NULL,
	DestinationID INT NULL,
	PreviousDestinations VARCHAR(MAX),
	ActualMiles INT,
	InvoiceUnits decimal(18, 10) NULL,
	InvoiceSettlementUom VARCHAR(25) NULL,
	InvoiceOriginChainupAmount money NULL,
	InvoiceDestChainupAmount money NULL,
	InvoiceH2SAmount money NULL,
	InvoiceRerouteAmount money NULL,
	InvoiceOrderRejectAmount money NULL,
	InvoiceOriginWaitAmount money NULL,
	InvoiceDestinationWaitAmount money NULL,
	InvoiceTotalWaitAmount MONEY NULL,
	InvoiceSplitLoadAmount MONEY NULL,
	InvoiceOtherDetailsTSV VARCHAR(MAX) NULL,
	InvoiceOtherAmount MONEY NULL,
	InvoiceFuelSurchargeAmount money NULL,
	InvoiceTaxRate MONEY NULL,
	InvoiceRouteRate decimal(18, 10) NULL,
	InvoiceRateSheetRate decimal(18, 10) NULL,
	InvoiceLoadAmount MONEY NULL,
	InvoiceTotalAmount MONEY NULL, 
	BatchDate SMALLDATETIME NULL,
	InvoicePeriodEnd DATE NULL,
	BatchInvoiceNum VARCHAR(50) NULL,
	AuditNotes VARCHAR(255) NULL,
	InvoiceNotes varchar(255) NULL,
	BatchCreateDateUTC DATETIME NULL,
	BatchCreatedByUser VARCHAR(100) NULL,
	BatchLastChangeDateUTC DATETIME NULL,
	BatchLastChangedByUser VARCHAR(100) NULL
)

GO


-- Driver tables

CREATE TABLE tblDriverSettlementBatchDbAudit
(
    DbAuditDate DATETIME NOT NULL CONSTRAINT DF_DriverSettlementBatchDbAudit_DbAuditDate DEFAULT GETUTCDATE(),
	ID int NOT NULL,
	BatchNum int NOT NULL,
	CarrierID int NOT NULL,
	DriverGroupID int NULL,
	DriverID int NULL,
	Notes varchar(255) NULL,
	CreateDateUTC smalldatetime NOT NULL,
	CreatedByUser varchar(100) NULL,
	LastChangeDateUTC smalldatetime NULL,
	LastChangedByUser varchar(100) NULL,
	BatchDate smalldatetime NOT NULL,
	InvoiceNum varchar(50) NULL,
	PeriodEndDate date NOT NULL,
	IsFinal bit NOT NULL
)

GO

CREATE TABLE tblOrderSettlementDriverDbAudit
(
    DbAuditDate DATETIME NOT NULL CONSTRAINT DF_OrderSettlementDriverDbAudit_DbAuditDate DEFAULT GETUTCDATE(),
	OrderID int NOT NULL,
	OrderNum INT NULL,
	BatchID int NULL,
	BatchNum INT NULL,
	JobNumber VARCHAR(20) NULL,
	ContractNumber VARCHAR(20) NULL,
	ContractID INT NULL,
	OrderDate date NULL,
	Shipper VARCHAR(40) NULL,
	ShipperID INT NULL,
	Carrier VARCHAR(120) NULL,
	CarrierID INT NULL,
	Driver VARCHAR(61) NULL,
	DriverFirst VARCHAR(30) NULL,
	DriverLast VARCHAR(30) NULL,
	DriverID INT NULL,
	DriverGroup VARCHAR(50) NULL,
	DriverGroupID INT NULL,
	Origin VARCHAR(50) NULL,
	OriginID INT NULL,
	Destination VARCHAR(50) NULL,
	DestinationID INT NULL,
	PreviousDestinations VARCHAR(MAX),
	ActualMiles INT,
	InvoiceUnits decimal(18, 10) NULL,
	InvoiceSettlementUom VARCHAR(25) NULL,
	InvoiceOriginChainupAmount money NULL,
	InvoiceDestChainupAmount money NULL,
	InvoiceH2SAmount money NULL,
	InvoiceRerouteAmount money NULL,
	InvoiceOrderRejectAmount money NULL,
	InvoiceOriginWaitAmount money NULL,
	InvoiceDestinationWaitAmount money NULL,
	InvoiceTotalWaitAmount MONEY NULL,
	InvoiceSplitLoadAmount MONEY NULL,
	InvoiceOtherDetailsTSV VARCHAR(MAX) NULL,
	InvoiceOtherAmount MONEY NULL,
	InvoiceFuelSurchargeAmount money NULL,
	InvoiceTaxRate MONEY NULL,
	InvoiceRouteRate decimal(18, 10) NULL,
	InvoiceRateSheetRate decimal(18, 10) NULL,
	InvoiceLoadAmount MONEY NULL,
	InvoiceTotalAmount MONEY NULL, 
	BatchDate SMALLDATETIME NULL,
	InvoicePeriodEnd DATE NULL,
	BatchInvoiceNum VARCHAR(50) NULL,
	AuditNotes VARCHAR(255) NULL,
	InvoiceNotes varchar(255) NULL,
	BatchCreateDateUTC DATETIME NULL,
	BatchCreatedByUser VARCHAR(100) NULL,
	BatchLastChangeDateUTC DATETIME NULL,
	BatchLastChangedByUser VARCHAR(100) NULL
)
 
GO


-- Shipper tables

CREATE TABLE tblShipperSettlementBatchDbAudit
(
    DbAuditDate DATETIME NOT NULL CONSTRAINT DF_ShipperSettlementBatchDbAudit_DbAuditDate DEFAULT GETUTCDATE(),
	ID int NOT NULL,
	BatchNum int NOT NULL,
	ShipperID int NOT NULL,
	Notes varchar(255) NULL,
	CreateDateUTC smalldatetime NOT NULL,
	CreatedByUser varchar(100) NULL,
	LastChangeDateUTC smalldatetime NULL,
	LastChangedByUser varchar(100) NULL,
	BatchDate smalldatetime NOT NULL,
	InvoiceNum varchar(50) NULL,
	PeriodEndDate date NOT NULL,
	IsFinal bit NOT NULL
)
	
GO

CREATE TABLE tblOrderSettlementShipperDbAudit
(
    DbAuditDate DATETIME NOT NULL CONSTRAINT DF_OrderSettlementShipperDbAudit_DbAuditDate DEFAULT GETUTCDATE(),
	OrderID int NOT NULL,
	OrderNum INT NULL,
	BatchID int NULL,
	BatchNum INT NULL,
	JobNumber VARCHAR(20) NULL,
	ContractNumber VARCHAR(20) NULL,
	ContractID INT NULL,
	OrderDate date NULL,
	Shipper VARCHAR(40) NULL,
	ShipperID INT NULL,
	Origin VARCHAR(50) NULL,
	OriginID INT NULL,
	Destination VARCHAR(50) NULL,
	DestinationID INT NULL,
	PreviousDestinations VARCHAR(MAX),
	ActualMiles INT,
	InvoiceUnits decimal(18, 10) NULL,
	InvoiceSettlementUom VARCHAR(25) NULL,
	InvoiceOriginChainupAmount money NULL,
	InvoiceDestChainupAmount money NULL,
	InvoiceH2SAmount money NULL,
	InvoiceRerouteAmount money NULL,
	InvoiceOrderRejectAmount money NULL,
	InvoiceOriginWaitAmount money NULL,
	InvoiceDestinationWaitAmount money NULL,
	InvoiceTotalWaitAmount MONEY NULL,
	InvoiceSplitLoadAmount MONEY NULL,
	InvoiceOtherDetailsTSV VARCHAR(MAX) NULL,
	InvoiceOtherAmount MONEY NULL,
	InvoiceFuelSurchargeAmount money NULL,
	InvoiceTaxRate MONEY NULL,
	InvoiceRouteRate decimal(18, 10) NULL,
	InvoiceRateSheetRate decimal(18, 10) NULL,
	InvoiceLoadAmount MONEY NULL,
	InvoiceTotalAmount MONEY NULL, 
	BatchDate SMALLDATETIME NULL,
	InvoicePeriodEnd DATE NULL,
	BatchInvoiceNum VARCHAR(50) NULL,
	AuditNotes VARCHAR(255) NULL,
	InvoiceNotes varchar(255) NULL,
	BatchCreateDateUTC DATETIME NULL,
	BatchCreatedByUser VARCHAR(100) NULL,
	BatchLastChangeDateUTC DATETIME NULL,
	BatchLastChangedByUser VARCHAR(100) NULL
)

GO



-- ---------------------------------------------------------
-- Create triggers to automatically populate the audit tables
-- ---------------------------------------------------------

-- Carrier

CREATE TRIGGER trigCarrierSettlementBatchDbAudit ON tblCarrierSettlementBatch AFTER UPDATE, DELETE AS
BEGIN
	SET NOCOUNT ON;

	PRINT 'trigCarrierSettlementBatchDbAudit BEGIN: ' + (CONVERT(VARCHAR(24), GETDATE(), 121))

	IF EXISTS (SELECT * FROM inserted EXCEPT SELECT * FROM deleted)
	BEGIN TRY
		-- Copy unsettled batch into batch log
		INSERT INTO tblCarrierSettlementBatchDbAudit
			SELECT GETUTCDATE(), d.* FROM deleted d 
			LEFT JOIN inserted i ON d.ID = i.ID
			WHERE i.ID IS NULL AND d.IsFinal = 1 -- deleted batch
			OR i.IsFinal = 0 AND d.IsFinal = 1 -- returned to pending

		-- Copy order changes into order-batch log only if deleted or returned to pending
		INSERT INTO tblOrderSettlementCarrierDbAudit
			SELECT GETUTCDATE(),
				OFC.OrderID, OFC.OrderNum,
				OFC.BatchID, OFC.InvoiceBatchNum,
				OFC.JobNumber,
				OFC.ContractNumber, OFC.ContractID,
				OFC.OrderDate,
				Shipper = OFC.Customer, ShipperID = OFC.CustomerID,
				OFC.Carrier, OFC.CarrierID,
				OFC.Origin, OFC.OriginID,
				OFC.Destination, OFC.DestinationID,
				OFC.PreviousDestinations,
				OFC.ActualMiles,
				OFC.InvoiceUnits,
				OFC.InvoiceSettlementUom,
				OFC.InvoiceOriginChainupAmount,
				OFC.InvoiceDestChainupAmount,
				OFC.InvoiceH2SAmount,
				OFC.InvoiceRerouteAmount,
				OFC.InvoiceOrderRejectAmount,
				OFC.InvoiceOriginWaitAmount,
				OFC.InvoiceDestinationWaitAmount,
				OFC.InvoiceTotalWaitAmount,
				OFC.InvoiceSplitLoadAmount,
				OFC.InvoiceOtherDetailsTSV,
				OFC.InvoiceOtherAmount,
				OFC.InvoiceFuelSurchargeAmount,
				OFC.InvoiceTaxRate,
				OFC.InvoiceRouteRate,
				OFC.InvoiceRateSheetRate,
				OFC.InvoiceLoadAmount,
				OFC.InvoiceTotalAmount, 
				OFC.InvoiceBatchDate,
				OFC.InvoicePeriodEnd,
				d.InvoiceNum,
				OFC.AuditNotes,
				OFC.InvoiceNotes, 
				d.CreateDateUTC,
				d.CreatedByUser,
				d.LastChangeDateUTC,
				d.LastChangedByUser
			FROM viewOrder_Financial_Carrier OFC
			JOIN deleted d ON d.ID = OFC.BatchID
			LEFT JOIN inserted i ON d.ID = i.ID
			WHERE i.ID IS NULL AND d.IsFinal = 1 -- deleted batch
			OR i.IsFinal = 0 AND d.IsFinal = 1 -- returned to pending

	END TRY
	BEGIN CATCH
		PRINT 'trigCarrierSettlementBatchDbAudit.DBAUDIT FAILURE: ' + ERROR_MESSAGE()
	END CATCH
		
	PRINT 'trigCarrierSettlementBatchDbAudit COMPLETE: ' + (CONVERT(VARCHAR(24), GETDATE(), 121))

END

GO


-- Driver

CREATE TRIGGER trigDriverSettlementBatchDbAudit ON tblDriverSettlementBatch AFTER UPDATE, DELETE AS
BEGIN
	SET NOCOUNT ON;

	PRINT 'trigDriverSettlementBatchDbAudit BEGIN: ' + (CONVERT(VARCHAR(24), GETDATE(), 121))

	IF EXISTS (SELECT * FROM inserted EXCEPT SELECT * FROM deleted)
	BEGIN TRY
		-- Copy unsettled batch into batch log
		INSERT INTO tblDriverSettlementBatchDbAudit
			SELECT GETUTCDATE(), d.* FROM deleted d 
			LEFT JOIN inserted i ON d.ID = i.ID
			WHERE i.ID IS NULL AND d.IsFinal = 1 -- deleted batch
			OR i.IsFinal = 0 AND d.IsFinal = 1 -- returned to pending

		-- Copy order changes into order-batch log only if deleted or returned to pending
		INSERT INTO tblOrderSettlementDriverDbAudit
			SELECT GETUTCDATE(),
				OFD.OrderID, OFD.OrderNum,
				OFD.BatchID, OFD.BatchNum,
				OFD.JobNumber,
				OFD.ContractNumber, OFD.ContractID,
				OFD.OrderDate,
				Shipper = OFD.Customer, ShipperID = OFD.CustomerID,
				OFD.Carrier, OFD.CarrierID,
				Driver = OFD.OriginDriver, DriverFirst = OFD.OriginDriverFirst, DriverLast = OFD.OriginDriverLast, DriverID = OFD.OriginDriverID, 
				DriverGroup = OFD.OriginDriverGroup, DriverGroupID = OFD.OriginDriverGroupID,
				OFD.Origin, OFD.OriginID,
				OFD.Destination, OFD.DestinationID,
				OFD.PreviousDestinations,
				OFD.ActualMiles,
				OFD.InvoiceUnits,
				OFD.InvoiceSettlementUom,
				OFD.InvoiceOriginChainupAmount,
				OFD.InvoiceDestChainupAmount,
				OFD.InvoiceH2SAmount,
				OFD.InvoiceRerouteAmount,
				OFD.InvoiceOrderRejectAmount,
				OFD.InvoiceOriginWaitAmount,
				OFD.InvoiceDestinationWaitAmount,
				OFD.InvoiceTotalWaitAmount,
				OFD.InvoiceSplitLoadAmount,
				OFD.InvoiceOtherDetailsTSV,
				OFD.InvoiceOtherAmount,
				OFD.InvoiceFuelSurchargeAmount,
				OFD.InvoiceTaxRate,
				OFD.InvoiceRouteRate,
				OFD.InvoiceRateSheetRate,
				OFD.InvoiceLoadAmount,
				OFD.InvoiceTotalAmount, 
				OFD.BatchDate,
				OFD.InvoicePeriodEnd,
				d.InvoiceNum,
				OFD.AuditNotes,
				OFD.InvoiceNotes,
				d.CreateDateUTC,
				d.CreatedByUser,
				d.LastChangeDateUTC,
				d.LastChangedByUser
			FROM viewOrder_Financial_Driver OFD
			JOIN deleted d ON d.ID = OFD.BatchID
			LEFT JOIN inserted i ON d.ID = i.ID
			WHERE i.ID IS NULL AND d.IsFinal = 1 -- deleted batch
			OR i.IsFinal = 0 AND d.IsFinal = 1 -- returned to pending

	END TRY
	BEGIN CATCH
		PRINT 'trigDriverSettlementBatchDbAudit.DBAUDIT FAILURE: ' + ERROR_MESSAGE()
	END CATCH
		
	PRINT 'trigDriverSettlementBatchDbAudit COMPLETE: ' + (CONVERT(VARCHAR(24), GETDATE(), 121))

END

GO


-- Shipper

CREATE TRIGGER trigShipperSettlementBatchDbAudit ON tblShipperSettlementBatch AFTER UPDATE, DELETE AS
BEGIN
	SET NOCOUNT ON;

	PRINT 'trigShipperSettlementBatchDbAudit BEGIN: ' + (CONVERT(VARCHAR(24), GETDATE(), 121))

	IF EXISTS (SELECT * FROM inserted EXCEPT SELECT * FROM deleted)
	BEGIN TRY
		-- Copy unsettled batch into batch log
		INSERT INTO tblShipperSettlementBatchDbAudit
			SELECT GETUTCDATE(), d.* FROM deleted d 
			LEFT JOIN inserted i ON d.ID = i.ID
			WHERE i.ID IS NULL AND d.IsFinal = 1 -- deleted batch
			OR i.IsFinal = 0 AND d.IsFinal = 1 -- returned to pending

		-- Copy order changes into order-batch log only if deleted or returned to pending
		INSERT INTO tblOrderSettlementShipperDbAudit
			SELECT GETUTCDATE(), 
				OFS.OrderID, OFS.OrderNum,
				OFS.BatchID, OFS.InvoiceBatchNum,
				OFS.JobNumber,
				OFS.ContractNumber, OFS.ContractID,
				OFS.OrderDate,
				Shipper = Customer, ShipperID = OFS.CustomerID,
				OFS.Origin, OFS.OriginID,
				OFS.Destination, OFS.DestinationID,
				OFS.PreviousDestinations,
				OFS.ActualMiles,
				OFS.InvoiceUnits,
				OFS.InvoiceSettlementUom,
				OFS.InvoiceOriginChainupAmount,
				OFS.InvoiceDestChainupAmount,
				OFS.InvoiceH2SAmount,
				OFS.InvoiceRerouteAmount,
				OFS.InvoiceOrderRejectAmount,
				OFS.InvoiceOriginWaitAmount,
				OFS.InvoiceDestinationWaitAmount,
				OFS.InvoiceTotalWaitAmount,
				OFS.InvoiceSplitLoadAmount,
				OFS.InvoiceOtherDetailsTSV,
				OFS.InvoiceOtherAmount,
				OFS.InvoiceFuelSurchargeAmount,
				OFS.InvoiceTaxRate,
				OFS.InvoiceRouteRate,
				OFS.InvoiceRateSheetRate,
				OFS.InvoiceLoadAmount,
				OFS.InvoiceTotalAmount, 
				OFS.InvoiceBatchDate,
				OFS.InvoicePeriodEnd,
				d.InvoiceNum,
				OFS.AuditNotes,
				OFS.InvoiceNotes,
				d.CreateDateUTC,
				d.CreatedByUser,
				d.LastChangeDateUTC,
				d.LastChangedByUser
			FROM viewOrder_Financial_Shipper OFS
			JOIN deleted d ON d.ID = OFS.BatchID
			LEFT JOIN inserted i ON d.ID = i.ID
			WHERE i.ID IS NULL AND d.IsFinal = 1 -- deleted batch
			OR i.IsFinal = 0 AND d.IsFinal = 1 -- returned to pending

	END TRY
	BEGIN CATCH
		PRINT 'trigShipperSettlementBatchDbAudit.DBAUDIT FAILURE: ' + ERROR_MESSAGE()
	END CATCH
		
	PRINT 'trigShipperSettlementBatchDbAudit COMPLETE: ' + (CONVERT(VARCHAR(24), GETDATE(), 121))

END

GO



-- --------------------------------------------------------------------
--  Create views for audit tables that match the regular order settlment table
-- --------------------------------------------------------------------

GO

/*************************************************************/
-- Date Created: 2017/05/19 (4.7.1)
-- Author: Joe Engler
-- Purpose: Show Carrier settlement history
-- Changes:
/*************************************************************/
CREATE FUNCTION fnCarrierSettlementHistory (@BatchID INT, @OrderID INT) RETURNS TABLE AS
RETURN
	SELECT Description = 'Settled'
		, IsCurrent = 1
		, OSC.OrderID
		, OSC.BatchNum
		, O.OrderNum
		, JobNumber = NULLIF(O.JobNumber, '')
		, ContractNumber = NULLIF(O.ContractNumber, '')
		, ActivityDate = ISNULL(CSB.LastChangeDateUTC, CSB.CreateDateUTC)
		, Shipper = O.Customer
		, Carrier = O.Carrier
		, O.Origin
		, O.Destination
		, PreviousDestinations = dbo.fnRerouteDetails(O.ID, 'PreviousDestinations', '<br/>') 
		, O.ActualMiles
		, SettlementVolume = OSC.SettlementUnits
		, OSC.SettlementUom
		, OSC.OriginChainupAmount
		, OSC.DestChainupAmount
		, OSC.H2SAmount
		, OSC.RerouteAmount
		, OSC.OrderRejectAmount
		, OSC.OriginWaitAmount
		, OSC.DestinationWaitAmount
		, OSC.TotalWaitAmount
		, OSC.SplitLoadAmount
		, MiscDetails = dbo.fnOrderShipperAssessorialDetailsTSV(O.ID, 0)
		, MiscAmount = OSC.OtherAmount
		, OSC.FuelSurchargeAmount
		, OSC.OriginTaxRate
		, OSC.RouteRate
		, OSC.RateSheetRate
		, OSC.LoadAmount
		, OSC.TotalAmount
		, CSB.BatchDate
		, CSB.PeriodEndDate
		, CSB.InvoiceNum
		, OrderNotes = OSC.Notes
		, BatchNotes = CSB.Notes
		, SettledByUser = ISNULL(CSB.LastChangedByUser, CSB.CreatedByUser)
		, SettlementTimestamp = ISNULL(CSB.LastChangeDateUTC, CSB.CreateDateUTC)
	FROM viewOrderSettlementCarrier OSC 
	JOIN viewOrder O ON OSC.OrderID = O.ID
	JOIN tblCarrierSettlementBatch CSB ON CSB.ID = OSC.BatchID
	WHERE CSB.IsFinal = 1
  	AND (@BatchID = -1 OR Batchid = @BatchID)
	AND (@OrderID = -1 OR OrderID=@OrderID)

	UNION

	SELECT Description = 'Settled', IsCurrent=0
		, OrderID
		, BatchNum
		, OrderNum
		, JobNumber = NULLIF(JobNumber, '')
		, ContractNumber = NULLIF(ContractNumber, '')
		, OrderDate
		, Shipper
		, Carrier
		, Origin
		, Destination
		, PreviousDestinations
		, ActualMiles
		, InvoiceUnits
		, InvoiceSettlementUom
		, InvoiceOriginChainupAmount
		, InvoiceDestChainupAmount
		, InvoiceH2SAmount
		, InvoiceRerouteAmount
		, InvoiceOrderRejectAmount
		, InvoiceOriginWaitAmount
		, InvoiceDestinationWaitAmount
		, InvoiceTotalWaitAmount
		, InvoiceSplitLoadAmount
		, InvoiceOtherDetailsTSV
		, InvoiceOtherAmount
		, InvoiceFuelSurchargeAmount
		, InvoiceTaxRate
		, InvoiceRouteRate
		, InvoiceRateSheetRate
		, InvoiceLoadAmount
		, InvoiceTotalAmount
		, BatchDate
		, InvoicePeriodEnd
		, BatchInvoiceNum
		, OrderNotes = AuditNotes
		, BatchNotes = InvoiceNotes
		, SettledByUser = ISNULL(BatchLastChangedByUser, BatchCreatedByUser)
		, SettlementTimestamp = ISNULL(BatchLastChangeDateUTC, BatchCreateDateUTC)
	FROM tblOrderSettlementCarrierDbAudit
	WHERE (@BatchID = -1 OR Batchid = @BatchID)
	AND (@OrderID = -1 OR OrderID=@OrderID)

	UNION

	SELECT Description = 'Unsettled'
		, IsCurrent=0
		, OrderID
		, BatchNum
		, OrderNum
		, JobNumber = NULLIF(JobNumber, '')
		, ContractNumber = NULLIF(ContractNumber, '')
		, OrderDate
		, Shipper
		, Carrier
		, Origin
		, Destination
		, PreviousDestinations
		, ActualMiles
		, -InvoiceUnits
		, InvoiceSettlementUom
		, -InvoiceOriginChainupAmount
		, -InvoiceDestChainupAmount
		, -InvoiceH2SAmount
		, -InvoiceRerouteAmount
		, -InvoiceOrderRejectAmount
		, -InvoiceOriginWaitAmount
		, -InvoiceDestinationWaitAmount
		, -InvoiceTotalWaitAmount
		, -InvoiceSplitLoadAmount
		, InvoiceOtherDetailsTSV
		, -InvoiceOtherAmount
		, -InvoiceFuelSurchargeAmount
		, InvoiceTaxRate
		, InvoiceRouteRate
		, InvoiceRateSheetRate
		, -InvoiceLoadAmount
		, -InvoiceTotalAmount
		, BatchDate
		, InvoicePeriodEnd
		, BatchInvoiceNum
		, OrderNotes = AuditNotes
		, BatchNotes = InvoiceNotes
		, SettledByUser = ISNULL(BatchLastChangedByUser, BatchCreatedByUser)
		, SettlementTimestamp = DbAuditDate
	FROM tblOrderSettlementCarrierDbAudit
	WHERE (@BatchID = -1 OR Batchid = @BatchID)
	AND (@OrderID = -1 OR OrderID=@OrderID)

GO


/*************************************************************/
-- Date Created: 2017/05/19 (4.7.1)
-- Author: Joe Engler
-- Purpose: Show driver settlement history
-- Changes:
/*************************************************************/
CREATE FUNCTION fnDriverSettlementHistory (@BatchID INT, @OrderID INT) RETURNS TABLE AS
RETURN
	SELECT Description = 'Settled'
		, IsCurrent = CAST(1 AS BIT)
		, OSD.OrderID
		, OSD.BatchNum
		, O.OrderNum
		, JobNumber = NULLIF(O.JobNumber, '')
		, ContractNumber = NULLIF(O.ContractNumber, '')
		, OrderDate = O.OrderDate
		, Shipper = O.Customer
		, O.Carrier
		, Driver = O.OriginDriver
		, DriverFirst = OriginDriverFirst
		, DriverLast = OriginDriverLast
		, DriverGroup = OriginDriverGroup
		, O.Origin
		, O.Destination
		, PreviousDestinations = dbo.fnRerouteDetails(O.ID, 'PreviousDestinations', '<br/>')
		, O.ActualMiles
		, OSD.SettlementUnits
		, OSD.SettlementUom
		, OSD.OriginChainupAmount
		, OSD.DestChainupAmount
		, OSD.H2SAmount
		, OSD.RerouteAmount
		, OSD.OrderRejectAmount
		, OSD.OriginWaitAmount
		, OSD.DestinationWaitAmount
		, OSD.TotalWaitAmount
		, OSD.SplitLoadAmount
		, MiscDetails = dbo.fnOrderShipperAssessorialDetailsTSV(O.ID, 0)
		, MiscAmount = OSD.OtherAmount
		, OSD.FuelSurchargeAmount
		, OSD.OriginTaxRate
		, OSD.RouteRate
		, OSD.RateSheetRate
		, OSD.LoadAmount
		, OSD.TotalAmount
		, DSB.BatchDate
		, DSB.PeriodEndDate
		, DSB.InvoiceNum
		, OrderNotes = OSD.Notes
		, BatchNotes = DSB.Notes
		, SettledByUser = ISNULL(DSB.LastChangedByUser, DSB.CreatedByUser)
		, SettlementTimestamp = ISNULL(DSB.LastChangeDateUTC, DSB.CreateDateUTC)
	FROM viewOrderSettlementDriver OSD 
	JOIN viewOrder O ON OSD.OrderID = O.ID
	JOIN tblDriverSettlementBatch DSB ON DSB.ID = OSD.BatchID
	WHERE DSB.IsFinal = 1
  	AND (@BatchID = -1 OR Batchid = @BatchID)
	AND (@OrderID = -1 OR OrderID=@OrderID)

	UNION

	SELECT Description = 'Settled', IsCurrent=0
		, OrderID
		, BatchNum
		, OrderNum
		, JobNumber = NULLIF(JobNumber, '')
		, ContractNumber = NULLIF(ContractNumber, '')
		, OrderDate
		, Shipper
		, Carrier
		, Driver
		, DriverFirst
		, DriverLast
		, DriverGroup
		, Origin
		, Destination
		, PreviousDestinations
		, ActualMiles
		, InvoiceUnits
		, InvoiceSettlementUom
		, InvoiceOriginChainupAmount
		, InvoiceDestChainupAmount
		, InvoiceH2SAmount
		, InvoiceRerouteAmount
		, InvoiceOrderRejectAmount
		, InvoiceOriginWaitAmount
		, InvoiceDestinationWaitAmount
		, InvoiceTotalWaitAmount
		, InvoiceSplitLoadAmount
		, InvoiceOtherDetailsTSV
		, InvoiceOtherAmount
		, InvoiceFuelSurchargeAmount
		, InvoiceTaxRate
		, InvoiceRouteRate
		, InvoiceRateSheetRate
		, InvoiceLoadAmount
		, InvoiceTotalAmount
		, BatchDate
		, InvoicePeriodEnd
		, BatchInvoiceNum
		, OrderNotes = AuditNotes
		, BatchNotes = InvoiceNotes
		, SettledByUser = ISNULL(BatchLastChangedByUser, BatchCreatedByUser)
		, SettlementTimestamp = ISNULL(BatchLastChangeDateUTC, BatchCreateDateUTC)
	FROM tblOrderSettlementDriverDbAudit 
	WHERE (@BatchID = -1 OR Batchid = @BatchID)
	AND (@OrderID = -1 OR OrderID=@OrderID)

	UNION

	SELECT Description = 'Unsettled'
		, IsCurrent=0
		, OrderID
		, BatchNum
		, OrderNum
		, JobNumber = NULLIF(JobNumber, '')
		, ContractNumber = NULLIF(ContractNumber, '')
		, OrderDate
		, Shipper
		, Carrier
		, Driver
		, DriverFirst
		, DriverLast
		, DriverGroup
		, Origin
		, Destination
		, PreviousDestinations
		, ActualMiles
		, -InvoiceUnits
		, InvoiceSettlementUom
		, -InvoiceOriginChainupAmount
		, -InvoiceDestChainupAmount
		, -InvoiceH2SAmount
		, -InvoiceRerouteAmount
		, -InvoiceOrderRejectAmount
		, -InvoiceOriginWaitAmount
		, -InvoiceDestinationWaitAmount
		, -InvoiceTotalWaitAmount
		, -InvoiceSplitLoadAmount
		, InvoiceOtherDetailsTSV
		, -InvoiceOtherAmount
		, -InvoiceFuelSurchargeAmount
		, InvoiceTaxRate
		, InvoiceRouteRate
		, InvoiceRateSheetRate
		, -InvoiceLoadAmount
		, -InvoiceTotalAmount
		, BatchDate
		, InvoicePeriodEnd
		, BatchInvoiceNum
		, OrderNotes = AuditNotes
		, BatchNotes = InvoiceNotes
		, SettledByUser = ISNULL(BatchLastChangedByUser, BatchCreatedByUser)
		, SettlementTimestamp = DbAuditDate
	FROM tblOrderSettlementDriverDbAudit OSDa 
	WHERE (@BatchID = -1 OR Batchid = @BatchID)
	AND (@OrderID = -1 OR OrderID=@OrderID)

GO

/*************************************************************/
-- Date Created: 2017/05/19 (4.7.1)
-- Author: Joe Engler
-- Purpose: Show Shipper settlement history
-- Changes:
/*************************************************************/
CREATE FUNCTION fnShipperSettlementHistory (@BatchID INT, @OrderID INT) RETURNS TABLE AS
RETURN
	SELECT Description = 'Settled'
		, IsCurrent = CAST(1 AS BIT)
		, OSS.OrderID
		, OSS.BatchNum
		, O.OrderNum
		, JobNumber = NULLIF(O.JobNumber, '')
		, ContractNumber = NULLIF(O.ContractNumber, '')
		, OrderDate = O.OrderDate
		, Shipper = O.Customer
		, O.Origin
		, O.Destination
		, PreviousDestinations = dbo.fnRerouteDetails(O.ID, 'PreviousDestinations', '<br/>') 
		, O.ActualMiles
		, OSS.SettlementUnits
		, OSS.SettlementUom
		, OSS.OriginChainupAmount
		, OSS.DestChainupAmount
		, OSS.H2SAmount
		, OSS.RerouteAmount
		, OSS.OrderRejectAmount
		, OSS.OriginWaitAmount
		, OSS.DestinationWaitAmount
		, OSS.TotalWaitAmount
		, OSS.SplitLoadAmount
		, MiscDetails = dbo.fnOrderShipperAssessorialDetailsTSV(O.ID, 0)
		, MiscAmount = OSS.OtherAmount
		, OSS.FuelSurchargeAmount
		, OSS.OriginTaxRate
		, OSS.RouteRate
		, OSS.RateSheetRate
		, OSS.LoadAmount
		, OSS.TotalAmount
		, SSB.BatchDate
		, SSB.PeriodEndDate
		, SSB.InvoiceNum
		, OrderNotes = OSS.Notes
		, BatchNotes = SSB.Notes
		, SettledByUser = ISNULL(SSB.LastChangedByUser, SSB.CreatedByUser)
		, SettlementTimestamp = ISNULL(SSB.LastChangeDateUTC, SSB.CreateDateUTC)
	FROM viewOrderSettlementShipper OSS 
	JOIN viewOrder O ON OSS.OrderID = O.ID
	JOIN tblShipperSettlementBatch SSB ON SSB.ID = OSS.BatchID
	WHERE SSB.IsFinal = 1
  	AND (@BatchID = -1 OR Batchid = @BatchID)
	AND (@OrderID = -1 OR OrderID=@OrderID)

	UNION

	SELECT Description = 'Settled', IsCurrent=0
		, OrderID
		, BatchNum
		, OrderNum
		, JobNumber = NULLIF(JobNumber, '')
		, ContractNumber = NULLIF(ContractNumber, '')
		, OrderDate
		, Shipper
		, Origin
		, Destination
		, PreviousDestinations
		, ActualMiles
		, InvoiceUnits
		, InvoiceSettlementUom
		, InvoiceOriginChainupAmount
		, InvoiceDestChainupAmount
		, InvoiceH2SAmount
		, InvoiceRerouteAmount
		, InvoiceOrderRejectAmount
		, InvoiceOriginWaitAmount
		, InvoiceDestinationWaitAmount
		, InvoiceTotalWaitAmount
		, InvoiceSplitLoadAmount
		, InvoiceOtherDetailsTSV
		, InvoiceOtherAmount
		, InvoiceFuelSurchargeAmount
		, InvoiceTaxRate
		, InvoiceRouteRate
		, InvoiceRateSheetRate
		, InvoiceLoadAmount
		, InvoiceTotalAmount
		, BatchDate
		, InvoicePeriodEnd
		, BatchInvoiceNum
		, OrderNotes = AuditNotes
		, BatchNotes = InvoiceNotes
		, SettledByUser = ISNULL(BatchLastChangedByUser, BatchCreatedByUser)
		, SettlementTimestamp = ISNULL(BatchLastChangeDateUTC, BatchCreateDateUTC)
	FROM tblOrderSettlementShipperDbAudit
	WHERE (@BatchID = -1 OR Batchid = @BatchID)
	AND (@OrderID = -1 OR OrderID=@OrderID)

	UNION

	SELECT Description = 'Unsettled'
		, IsCurrent=0
		, OrderID
		, BatchNum
		, OrderNum
		, JobNumber = NULLIF(JobNumber, '')
		, ContractNumber = NULLIF(ContractNumber, '')
		, OrderDate
		, Shipper
		, Origin
		, Destination
		, PreviousDestinations
		, ActualMiles
		, -InvoiceUnits
		, InvoiceSettlementUom
		, -InvoiceOriginChainupAmount
		, -InvoiceDestChainupAmount
		, -InvoiceH2SAmount
		, -InvoiceRerouteAmount
		, -InvoiceOrderRejectAmount
		, -InvoiceOriginWaitAmount
		, -InvoiceDestinationWaitAmount
		, -InvoiceTotalWaitAmount
		, -InvoiceSplitLoadAmount
		, InvoiceOtherDetailsTSV
		, -InvoiceOtherAmount
		, -InvoiceFuelSurchargeAmount
		, InvoiceTaxRate
		, InvoiceRouteRate
		, InvoiceRateSheetRate
		, -InvoiceLoadAmount
		, -InvoiceTotalAmount
		, BatchDate
		, InvoicePeriodEnd
		, BatchInvoiceNum
		, OrderNotes = AuditNotes
		, BatchNotes = InvoiceNotes
		, SettledByUser = ISNULL(BatchLastChangedByUser, BatchCreatedByUser)
		, SettlementTimestamp = DbAuditDate
	FROM tblOrderSettlementShipperDbAudit
	WHERE (@BatchID = -1 OR Batchid = @BatchID)
	AND (@OrderID = -1 OR OrderID=@OrderID)

GO

COMMIT
SET NOEXEC OFF
