SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.0.5.1'
SELECT  @NewVersion = '4.0.5.2'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1702: Fix odometer/mileage issues'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/***********************************
-- Date Created: 12 Feb 2015
-- Author: Kevin Alons
-- Purpose: return last truck mileage for each truck (from order table)
-- Changes:
    - 3.9.20 - 2015/10/23 - KDA - revise logic to use tblOrderTransfer data (when a transfer took place that changed Trucks)
			 - 2015/10/29 - JAE - Include incomplete orders as well as transfers that keep the same truck
	- 4.0.5.2 - 2016/08/30 - JAE - Change max ID to most recent, omit mileage updates until status is "complete" (print status vs regular)
***********************************/
ALTER VIEW [dbo].[viewOrderLastTruckMileage] AS
SELECT *
FROM (
    SELECT TruckID = T.ID
        , Odometer = CASE WHEN OTM.OrderID > DTM.OrderID THEN OTM.Odometer ELSE DTM.Odometer END
        , OdometerDate = CASE WHEN OTM.OrderID > DTM.OrderID THEN OTM.OdometerDate ELSE DTM.OdometerDate END
        , OdometerDateUTC = CASE WHEN OTM.OrderID > DTM.OrderID THEN OTM.OdometerDateUTC ELSE DTM.OdometerDateUTC END
    FROM tblTruck T

    LEFT JOIN (
        -- Origin truck (for transfers with origin truck not same as destination truck)
        SELECT OrderID = O.ID, TruckID = OTR.OriginTruckID, Odometer = ISNULL(OTR.OriginTruckEndMileage, O.OriginTruckMileage), OdometerDate = O.OrderDate, OdometerDateUTC = OTR.CreateDateUTC
        FROM tblOrderTransfer OTR
        JOIN (
			-- most recent entry per truck
            SELECT OrderID = tr.OrderID, TruckID = t.ID
            FROM tblTruck t 
			CROSS APPLY (SELECT TOP 1 OrderID FROM tblOrderTransfer otr 
							JOIN tblOrder o ON otr.OrderID = o.ID
							WHERE t.ID = otr.OriginTruckID 
							  AND o.TruckID <> otr.OriginTruckID --truck change
							ORDER BY otr.LastChangeDateUTC DESC) tr
        ) X ON X.OrderID = OTR.OrderID
        JOIN tblOrder O ON O.ID = OTR.OrderID
    ) OTM ON OTM.TruckID = T.ID

    LEFT JOIN (
		-- Destination truck (for non-transfers or transfers with same truck)
        SELECT ordernum,OrderID = O.ID, O.TruckID, O.truck,
        Odometer = CASE WHEN (o.TruckID = o.OriginTruckID AND (Rejected = 1 OR PrintStatusID = 8)) 
							THEN COALESCE(DestTruckStartMileage, OriginTruckEndMileage, OriginTruckMileage) --use any valid non-delivery mileage
						WHEN (o.TruckID = o.OriginTruckID AND Rejected = 0 AND PrintStatusID in (3,4)) 
							THEN COALESCE(DestTruckMileage, DestTruckStartMileage, OriginTruckEndMileage, OriginTruckMileage) --use any valid mileage
						WHEN (o.TruckID <> o.OriginTruckID AND PrintStatusID = 8) 
							THEN DestTruckStartMileage -- don't use origin mileages when truck changed (transfers)
						ELSE ISNULL(DestTruckMileage, DestTruckStartMileage) END, -- don't use origin mileages when truck changed (transfers)
        OdometerDate = OrderDate, 
        OdometerDateUTC = ISNULL(DeliverLastChangeDateUTC, PickupLastChangeDateUTC)
        FROM viewOrder O
        JOIN (
			-- most recent entry per truck
			SELECT OrderID = o2.ID, TruckID = t.ID
			FROM tblTruck t
			CROSS APPLY (SELECT TOP 1 ID FROM viewOrder o
						WHERE t.ID = o.TruckID 
						  AND (   o.TruckID = o.OriginTruckID AND o.OriginTruckMileage IS NOT NULL -- if regular or same-truck transfer, origin start must be set
							   OR o.TruckID <> o.OriginTruckID AND o.DestTruckStartMileage IS NOT NULL) -- if transfer truck, dest start must be set
						  AND o.PrintStatusID in (3,8,4) -- only consider if pickup complete
                        ORDER BY LastChangeDateUTC DESC) o2
        ) X ON X.OrderID = O.ID
    ) DTM ON DTM.TruckID = T.ID
) X
WHERE X.OdometerDate IS NOT NULL

GO


COMMIT
SET NOEXEC OFF