/* 
	ensure that the error condition of Dispatched orders with a NULL driverID will not crash the virtual delete order table trigger
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.6.8'
SELECT  @NewVersion = '2.6.9'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

UPDATE tblOrder SET StatusID = -10 
WHERE (StatusID = 1 AND CarrierID is null) 
   OR (StatusID = 2 AND DriverID is null)
GO

/**********************************************************/
-- Author: Kevin Alons
-- Date Created: 26 Sep 2013
-- Purpose: track changes to Orders that cause it be "virtually" deleted for a Driver (for DriveApp.Sync purposes)
/**********************************************************/
ALTER TRIGGER [dbo].[trigOrder_U_VirtualDelete] ON [dbo].[tblOrder] AFTER UPDATE AS
BEGIN
	DECLARE @NewRecords TABLE (
		  OrderID int NOT NULL
		, DriverID int NOT NULL
		, LastChangeDateUTC smalldatetime NOT NULL
		, LastChangedByUser varchar(100) NULL
	)

	-- delete any records that no longer apply because now the record is valid for the new driver/status
	DELETE FROM tblOrderDriverAppVirtualDelete
	FROM tblOrderDriverAppVirtualDelete ODAVD
	JOIN inserted i ON i.ID = ODAVD.OrderID AND i.DriverID = ODAVD.DriverID
	WHERE i.DeleteDateUTC IS NULL 
		AND dbo.fnPrintStatusID(i.StatusID, i.PickupPrintStatusID, i.DeliverPrintStatusID) IN (2,7,8)

	INSERT INTO @NewRecords
		-- insert any applicable VIRTUALDELETE records where the status changed from 
		--   a valid status to an invalid [DriverApp] status
		SELECT d.ID, d.DriverID, GETUTCDATE(), ISNULL(i.LastChangedByUser, 'System')
		FROM deleted d
		JOIN inserted i ON i.ID = D.ID
		WHERE dbo.fnPrintStatusID(i.StatusID, i.PickupPrintStatusID, i.DeliverPrintStatusID) NOT IN (2,7,8)
		  AND dbo.fnPrintStatusID(d.StatusID, d.PickupPrintStatusID, d.DeliverPrintStatusID) IN (2,7,8)
		  AND d.DriverID IS NOT NULL
		  
		UNION

		-- insert any records where the DriverID changed to another DriverID (for a valid status)
		SELECT d.ID, d.DriverID, GETUTCDATE(), ISNULL(i.LastChangedByUser, 'System')
		FROM deleted d
		JOIN inserted i ON i.ID = D.ID
		WHERE d.DriverID <> i.DriverID 
		  AND dbo.fnPrintStatusID(d.StatusID, d.PickupPrintStatusID, d.DeliverPrintStatusID) IN (2,7,8)
		  AND d.DriverID IS NOT NULL
		  
	-- update the VirtualDeleteDateUTC value for any existing records
	UPDATE tblOrderDriverAppVirtualDelete
	  SET VirtualDeleteDateUTC = NR.LastChangeDateUTC, VirtualDeletedByUser = NR.LastChangedByUser
	FROM tblOrderDriverAppVirtualDelete ODAVD
	JOIN @NewRecords NR ON NR.OrderID = ODAVD.OrderID AND NR.DriverID = ODAVD.DriverID

	-- insert the truly new VirtualDelete records	
	INSERT INTO tblOrderDriverAppVirtualDelete (OrderID, DriverID, VirtualDeleteDateUTC, VirtualDeletedByUser)
		SELECT NR.OrderID, NR.DriverID, NR.LastChangeDateUTC, NR.LastChangedByUser
		FROM @NewRecords NR
		LEFT JOIN tblOrderDriverAppVirtualDelete ODAVD ON ODAVD.OrderID = NR.OrderID AND ODAVD.DriverID = NR.DriverID
		WHERE ODAVD.ID IS NULL

END

GO

COMMIT
SET NOEXEC OFF