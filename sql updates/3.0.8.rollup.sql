DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.8.10'
SELECT  @NewVersion = '3.0.8'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

INSERT INTO tblSetting (ID, Name, SettingTypeID, Value, Category, ReadOnly, CreateDateUTC, CreatedByUser)
	SELECT 26, 'GPS Coarse Interval (meters)', 4, 1609, 'Driver App Settings', 0, GETUTCDATE(), 'System'
	UNION
	SELECT 27, 'GPS Coarse Interval (seconds)', 3, 300, 'Driver App Settings', 0, GETUTCDATE(), 'System'
	UNION
	SELECT 28, 'GPS Fine Interval (meters)', 4, 1609, 'Driver App Settings', 0, GETUTCDATE(), 'System'
	UNION
	SELECT 29, 'GPS Find Interval (seconds)', 3, 30, 'Driver App Settings', 0, GETUTCDATE(), 'System'
	UNION
	SELECT 30, 'GPS Fine Threshold (meters)', 4, 2000, 'Driver App Settings', 0, GETUTCDATE(), 'System'
GO
INSERT [tblSetting] ([ID], [Name], [SettingTypeID], [Value], [Category], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [ReadOnly]) VALUES (24, N'Show Bottom Detail Entry Fields (", Q)', 2, N'False', N'Product Acceptance Criteria', CAST(0xA3620116 AS SmallDateTime), N'System', CAST(0xA362054A AS SmallDateTime), N'kalons', 0)
INSERT [tblSetting] ([ID], [Name], [SettingTypeID], [Value], [Category], [CreateDateUTC], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [ReadOnly]) VALUES (25, N'Allow Unstrapped Tanks', 2, N'False', N'Order Entry Rules', CAST(0xA3620546 AS SmallDateTime), N'System', CAST(0xA362054A AS SmallDateTime), N'kalons', 0)
GO

SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET XACT_ABORT ON
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblOrderInvoiceCustomer] DROP CONSTRAINT [FK_OrderInvoiceCustomer_SettlementBatch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblOrder] DROP CONSTRAINT [FK_Order_Operator]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblOriginProducts] DROP CONSTRAINT [FK_OriginProducts_Product]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblOrigin] DROP CONSTRAINT [FK_tblOrigin_tblOperator]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblPumper] DROP CONSTRAINT [FK_tblPumper_tblOperator]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblOperator] DROP CONSTRAINT [PK__tblOpera__3214EC2760083D91]
GO

DROP PROCEDURE [dbo].[aspnet_PersonalizationAdministration_DeleteAllState]
GO

DROP PROCEDURE [dbo].[aspnet_PersonalizationAdministration_FindState]
GO

DROP PROCEDURE [dbo].[aspnet_PersonalizationAdministration_GetCountOfState]
GO

DROP PROCEDURE [dbo].[aspnet_PersonalizationAdministration_ResetSharedState]
GO

DROP PROCEDURE [dbo].[aspnet_PersonalizationAdministration_ResetUserState]
GO

DROP PROCEDURE [dbo].[aspnet_PersonalizationAllUsers_GetPageSettings]
GO

DROP PROCEDURE [dbo].[aspnet_PersonalizationAllUsers_ResetPageSettings]
GO

DROP PROCEDURE [dbo].[aspnet_PersonalizationAllUsers_SetPageSettings]
GO

DROP PROCEDURE [dbo].[aspnet_PersonalizationPerUser_GetPageSettings]
GO

DROP PROCEDURE [dbo].[aspnet_PersonalizationPerUser_ResetPageSettings]
GO

DROP PROCEDURE [dbo].[aspnet_PersonalizationPerUser_SetPageSettings]
GO

DROP PROCEDURE [dbo].[aspnet_Personalization_GetApplicationId]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblLocationType]
(
	[ID] [tinyint] NOT NULL,
	[Name] [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[CreateDateUTC] [smalldatetime] NOT NULL,
	[CreatedByUser] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
INSERT [tblLocationType] ([ID], [Name], [CreateDateUTC], [CreatedByUser]) VALUES (1, N'Auto', CAST(0xA35E011D AS SmallDateTime), NULL)
INSERT [tblLocationType] ([ID], [Name], [CreateDateUTC], [CreatedByUser]) VALUES (2, N'Driver Arrive', CAST(0xA35E011D AS SmallDateTime), NULL)
INSERT [tblLocationType] ([ID], [Name], [CreateDateUTC], [CreatedByUser]) VALUES (3, N'Driver Depart', CAST(0xA35E011D AS SmallDateTime), NULL)
INSERT [tblLocationType] ([ID], [Name], [CreateDateUTC], [CreatedByUser]) VALUES (4, N'Geo Fence In', CAST(0xA35E011D AS SmallDateTime), NULL)
INSERT [tblLocationType] ([ID], [Name], [CreateDateUTC], [CreatedByUser]) VALUES (5, N'Geo Fence Out', CAST(0xA35E011D AS SmallDateTime), NULL)
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblReportBaseFilterType]
(
	[ID] [int] NOT NULL,
	[Name] [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
INSERT [tblReportBaseFilterType] ([ID], [Name]) VALUES (1, N'None')
INSERT [tblReportBaseFilterType] ([ID], [Name]) VALUES (2, N'Profile Value')
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblReportDefinition]
(
	[ID] [int] NOT NULL,
	[Name] [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[SourceTable] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
INSERT [tblReportDefinition] ([ID], [Name], [SourceTable]) VALUES (1, N'Orders', N'viewOrder_OrderTicket_Full')
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblReportFilterOperator]
(
	[ID] [int] NOT NULL,
	[Name] [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
INSERT [tblReportFilterOperator] ([ID], [Name]) VALUES (4, N'Between')
INSERT [tblReportFilterOperator] ([ID], [Name]) VALUES (6, N'Contains')
INSERT [tblReportFilterOperator] ([ID], [Name]) VALUES (2, N'Equals')
INSERT [tblReportFilterOperator] ([ID], [Name]) VALUES (11, N'Greater Than')
INSERT [tblReportFilterOperator] ([ID], [Name]) VALUES (12, N'Greater Than Or Equal')
INSERT [tblReportFilterOperator] ([ID], [Name]) VALUES (3, N'In')
INSERT [tblReportFilterOperator] ([ID], [Name]) VALUES (7, N'Is Empty')
INSERT [tblReportFilterOperator] ([ID], [Name]) VALUES (8, N'Is Not Empty')
INSERT [tblReportFilterOperator] ([ID], [Name]) VALUES (9, N'Less Than')
INSERT [tblReportFilterOperator] ([ID], [Name]) VALUES (10, N'Less Than Or Equal')
INSERT [tblReportFilterOperator] ([ID], [Name]) VALUES (1, N'No Filter')
INSERT [tblReportFilterOperator] ([ID], [Name]) VALUES (5, N'Starts With')
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblReportFilterType]
(
	[ID] [int] NOT NULL,
	[Name] [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[FilterOperatorID_CSV] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
INSERT [tblReportFilterType] ([ID], [Name], [FilterOperatorID_CSV]) VALUES (0, N'DateTime [No filtering]', N'1')
INSERT [tblReportFilterType] ([ID], [Name], [FilterOperatorID_CSV]) VALUES (1, N'Text', N'1,5,6,7,8')
INSERT [tblReportFilterType] ([ID], [Name], [FilterOperatorID_CSV]) VALUES (2, N'DropDown', N'1,3')
INSERT [tblReportFilterType] ([ID], [Name], [FilterOperatorID_CSV]) VALUES (3, N'Date Period', N'1,2')
INSERT [tblReportFilterType] ([ID], [Name], [FilterOperatorID_CSV]) VALUES (4, N'Number', N'1,2,7,8,9,10,11,12')
INSERT [tblReportFilterType] ([ID], [Name], [FilterOperatorID_CSV]) VALUES (5, N'Yes/No', N'1,2')
INSERT [tblReportFilterType] ([ID], [Name], [FilterOperatorID_CSV]) VALUES (6, N'Date Range', N'1,2,4')
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblOrigin] ADD [GeoFenceRadiusMeters] [int] NULL
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblDestination] ADD [GeoFenceRadiusMeters] [int] NULL
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO 
CREATE TABLE [dbo].[tblReportColumnDefinition]
(
	[ID] [int] NOT NULL,
	[ReportID] [int] NOT NULL,
	[DataField] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Caption] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DataFormat] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FilterDataField] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FilterTypeID] [int] NOT NULL,
	[FilterDropDownSql] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FilterAllowCustomText] [bit] NOT NULL
) ON [PRIMARY]
GO
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (1, 1, N'Carrier', N'Carrier', NULL, N'CarrierID', 2, N'SELECT ID, Name FROM tblCarrier ORDER BY Name', 0)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (2, 1, N'Customer', N'Shipper', NULL, N'CustomerID', 2, N'SELECT ID, Name FROM tblCustomer ORDER BY Name', 0)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (3, 1, N'Destination', N'Destination', NULL, N'DestinationID', 2, N'SELECT ID, Name FROM tblOrigin ORDER BY Name', 0)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (4, 1, N'OrderDate', N'Date', N'm/d/yyyy', NULL, 3, N'SELECT ID=1, Name=''Week to Date'', SortID = 1 UNION SELECT 2, ''Month to Date'', 2 UNION SELECT 3, ''Last Month'', 3 UNION SELECT 4, ''Quarter to Date'', 4 UNION SELECT 5, ''Year to Date'', 5 ORDER BY SortID', 0)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (5, 1, N'OrderNum', N'Order #', NULL, NULL, 1, NULL, 0)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (6, 1, N'Origin', N'Origin', NULL, N'OriginID', 2, N'SELECT ID, Name FROM tblOrigin ORDER BY Name', 0)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (7, 1, N'OriginGrossStdUnits', N'Origin GSV', N'#,##0.00', NULL, 4, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (8, 1, N'OriginGrossUnits', N'Origin GOV', N'#,##0.00', NULL, 4, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (9, 1, N'OriginNetUnits', N'Origin NSV', N'#,##0.00', NULL, 4, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (10, 1, N'Product', N'Product', NULL, N'ProductID', 2, N'SELECT ID, Name FROM tblProduct ORDER BY Name', 0)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (11, 1, N'Rejected', N'Rejected?', NULL, NULL, 5, N'SELECT ID=1, Name=''Yes'' UNION SELECT 2, ''No''', 0)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (12, 1, N'PrintStatus', N'Status', NULL, N'StatusID', 2, N'SELECT ID, Name=OrderStatus FROM tblOrderStatus ORDER BY StatusNum', 0)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (13, 1, N'Priority', N'Priority', NULL, N'PriorityID', 2, N'SELECT ID, Name FROM tblPriority ORDER BY Name', 0)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (14, 1, N'DueDate', N'Due Date', NULL, NULL, 6, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (15, 1, N'OriginArriveTime', N'Origin Arrival', NULL, NULL, 0, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (16, 1, N'OriginDepartTime', N'Origin Departure', NULL, NULL, 0, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (17, 1, N'OriginMinutes', N'Origin Minutes', NULL, NULL, 4, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (18, 1, N'OriginWaitNotes', N'Origin Wait Notes', NULL, NULL, 1, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (19, 1, N'OriginBOLNum', N'Origin BOL #', NULL, NULL, 1, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (20, 1, N'DestArriveTime', N'Dest Arrival', NULL, NULL, 0, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (21, 1, N'DestDepartTime', N'Dest Departure', NULL, NULL, 0, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (22, 1, N'DestMinutes', N'Dest Minutes', NULL, NULL, 4, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (23, 1, N'DestWaitNotes', N'Dest Wait Notes', NULL, NULL, 1, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (24, 1, N'DestBOLNum', N'Dest BOL #', NULL, NULL, 1, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (25, 1, N'DestGrossUnits', N'Dest GOV', NULL, NULL, 4, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (26, 1, N'DestNetUnits', N'Dest GSV', NULL, NULL, 4, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (27, 1, N'Driver', N'Driver', NULL, N'DriverID', 2, N'SELECT ID, Name = FullName FROM viewDriver ORDER BY FullName', 0)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (28, 1, N'Truck', N'Truck', NULL, N'TruckID', 2, N'SELECT ID, Name = FullName FROM viewTruck ORDER BY FullName', 0)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (29, 1, N'Trailer', N'Trailer 1', NULL, N'TrailerID', 2, N'SELECT ID, Name = FullName FROM viewTrailer ORDER BY FullName', 0)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (30, 1, N'Trailer2', N'Trailer 2', NULL, N'Trailer2ID', 2, N'SELECT ID, Name = FullName FROM viewTrailer ORDER BY FullName', 0)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (31, 1, N'Operator', N'Operator', NULL, N'OperatorID', 2, N'SELECT ID, Name FROM tblOperator ORDER BY Name', 0)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (32, 1, N'Pumper', N'Pumper', NULL, N'PumperID', 2, N'SELECT ID, Name FROM tblPumper ORDER BY Name', 0)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (33, 1, N'TicketType', N'Order Type', NULL, N'TicketTypeID', 2, N'SELECT ID, Name FROM tblTicketType ORDER BY Name', 0)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (34, 1, N'RejectNotes', N'Reject Notes', NULL, NULL, 1, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (35, 1, N'ChainUp', N'Chain-Up', NULL, NULL, 5, N'SELECT ID=1, Name=''Yes'' UNION SELECT 2, ''No''', 0)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (36, 1, N'OriginTruckMileage', N'Origin Truck Mileage', NULL, NULL, 4, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (37, 1, N'OriginTankText', N'Origin Tank #', NULL, NULL, 1, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (38, 1, N'DestTruckMileage', N'Dest Truck Mileage', NULL, NULL, 4, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (40, 1, N'AuditNotes', N'Audit Notes', NULL, NULL, 1, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (41, 1, N'ActualMiles', N'Actual Miles', NULL, NULL, 4, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (42, 1, N'Producer', N'Producer', NULL, N'ProducerID', 2, N'SELECT ID, Name FROM tblProducer ORDER BY Name', 0)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (43, 1, N'DestProductBSW', N'Dest BS&W', NULL, NULL, 4, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (44, 1, N'DestProductGravity', N'Dest Gravity', NULL, NULL, 4, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (45, 1, N'DestProductTemp', N'Dest Temp', NULL, NULL, 4, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (46, 1, N'DestOpenMeterUnits', N'Dest Open Meters', NULL, NULL, 4, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (47, 1, N'DestCloseMeterUnits', N'Dest Close Meters', NULL, NULL, 4, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (48, 1, N'OriginUom', N'Origin UOM', NULL, N'OriginUOMID', 2, N'SELECT ID, Name FROM tblUOM ORDER BY Name', 0)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (49, 1, N'DestUom', N'Dest UOM', NULL, N'DestUOMID', 2, N'SELECT ID, Name FROM tblUOM ORDER BY Name', 0)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (50, 1, N'DispatchNotes', N'Dispatch Notes', NULL, NULL, 1, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (51, 1, N'DriverNotes', N'Driver Notes', NULL, NULL, 1, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (52, 1, N'DispatchConfirmNum', N'Dispatch Confirm #', NULL, NULL, 1, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (53, 1, N'OriginState', N'Origin State', NULL, N'OriginStateID', 2, N'SELECT ID, Name=FullName FROM tblState ORDER BY FullName', 0)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (54, 1, N'OriginStateAbbrev', N'Origin ST', NULL, N'OriginStateID', 2, N'SELECT ID, Name=Abbreviation FROM tblState ORDER BY Abbreviation', 0)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (55, 1, N'OriginStation', N'Origin Station', NULL, NULL, 1, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (56, 1, N'LeaseName', N'Origin Lease Name', NULL, NULL, 1, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (57, 1, N'LeaseNum', N'Origin Lease #', NULL, NULL, 1, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (58, 1, N'OriginLegalDescription', N'Origin Legal Desc', NULL, NULL, 1, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (59, 1, N'OriginNDIC', N'Origin NDIC', NULL, NULL, 1, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (60, 1, N'OriginNDM', N'Origin NDM', NULL, NULL, 1, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (61, 1, N'OriginCA', N'Origin CA', NULL, NULL, 1, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (62, 1, N'OriginTimeZone', N'Origin TZ', NULL, N'OriginTimeZoneID', 2, N'SELECT ID, Name FROM tblTimeZone ORDER BY StandardOffsetHours DESC', 0)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (63, 1, N'OriginUseDST', N'Origin Use DST', NULL, NULL, 5, N'SELECT ID=1, Name=''Yes'' UNION SELECT 2, ''No''', 0)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (64, 1, N'H2S', N'H2S', NULL, NULL, 5, N'SELECT ID=1, Name=''Yes'' UNION SELECT 2, ''No''', 0)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (65, 1, N'DestinationState', N'Dest State', NULL, N'DestStateID', 2, N'SELECT ID, Name=FullName FROM tblState ORDER BY FullName', 0)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (66, 1, N'DestinationStateAbbrev', N'Destination ST', NULL, N'DestStateID', 2, N'SELECT ID, Name=Abbreviation FROM tblState ORDER BY Abbreviation', 0)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (67, 1, N'DestStation', N'Dest Station', NULL, NULL, 1, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (68, 1, N'DestTimeZone', N'Dest TZ', NULL, N'DestTimeZoneID', 2, N'SELECT ID, Name FROM tblTimeZone ORDER BY StandardOffsetHours DESC', 0)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (69, 1, N'DestUseDST', N'Dest Use DST', NULL, NULL, 5, N'SELECT ID=1, Name=''Yes'' UNION SELECT 2, ''No''', 0)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (70, 1, N'CarrierType', N'Carrier Type', NULL, N'CarrierTypeID', 2, N'SELECT ID, Name FROM tblCarrierType ORDER BY Name', 0)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (71, 1, N'DriverFirst', N'Driver First', NULL, NULL, 1, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (72, 1, N'DriverLast', N'Driver Last', NULL, NULL, 1, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (73, 1, N'DestTicketType', N'Dest Ticket Type', NULL, N'DestTicketTypeID', 2, N'SELECT ID, Name FROM tblDestTicketType ORDER BY Name', 0)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (74, 1, N'DriverNumber', N'Driver #', NULL, NULL, 1, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (75, 1, N'CarrierNumber', N'Carrier #', NULL, NULL, 1, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (76, 1, N'ProductGroup', N'Product Group', NULL, NULL, 1, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (77, 1, N'OriginUomShort', N'Origin UOM Short', NULL, N'OriginUOMID', 2, N'SELECT ID, Name=Abbrev FROM tblUOM ORDER BY Abbrev', 0)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (78, 1, N'DestUomShort', N'Dest UOM Short', NULL, N'DestUOMID', 2, N'SELECT ID, Name=Abbrev FROM tblUOM ORDER BY Abbrev', 0)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (79, 1, N'TransitMinutes', N'Transit Min', NULL, NULL, 4, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (80, 1, N'TicketCount', N'Ticket Count', NULL, NULL, 4, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (81, 1, N'RerouteCount', N'Reroute Count', NULL, NULL, 4, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (82, 1, N'TotalMinutes', N'Total Minutes', NULL, NULL, 4, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (83, 1, N'TotalWaitMinutes', N'Total Wait Minutes', NULL, NULL, 4, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (84, 1, N'T_TicketType', N'Ticket Type', NULL, NULL, 1, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (85, 1, N'T_CarrierTicketNum', N'Carrier Ticket #', NULL, NULL, 1, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (86, 1, N'T_BOLNum', N'Ticket BOL #', NULL, NULL, 1, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (87, 1, N'T_TankNum', N'Ticket Tank #', NULL, NULL, 1, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (88, 1, N'T_IsStrappedTank', N'Ticket - Strapped?', NULL, NULL, 5, N'SELECT ID=1, Name=''Yes'' UNION SELECT 2, ''No''', 0)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (89, 1, N'T_BottomFeet', N'Ticket Bottom FT', NULL, NULL, 4, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (90, 1, N'T_BottomInches', N'Ticket Bottom IN', NULL, NULL, 4, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (91, 1, N'T_BottomQ', N'Ticket Bottom Q', NULL, NULL, 4, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (92, 1, N'T_OpeningGaugeFeet', N'Ticket Open FT', NULL, NULL, 4, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (93, 1, N'T_OpeningGaugeInch', N'Ticket Open IN', NULL, NULL, 4, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (94, 1, N'T_OpeningGaugeQ', N'Ticket Open Q', NULL, NULL, 4, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (95, 1, N'T_ClosingGaugeFeet', N'Ticket Close FT', NULL, NULL, 4, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (96, 1, N'T_ClosingGaugeInch', N'Ticket Close IN', NULL, NULL, 4, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (97, 1, N'T_ClosingGaugeQ', N'Ticket Close Q', NULL, NULL, 4, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (98, 1, N'T_OpenReading', N'Ticket Open Reading', NULL, NULL, 1, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (99, 1, N'T_CloseReading', N'Ticket Close Reading', NULL, NULL, 1, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (100, 1, N'T_CorrectedAPIGravity', N'Ticket Corr. API Gravity', NULL, NULL, 4, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (101, 1, N'T_GrossStdUnits', N'Ticket GSV', NULL, NULL, 4, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (102, 1, N'T_GrossUnits', N'Ticket GOV', NULL, NULL, 4, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (103, 1, N'T_NetUnits', N'Ticket NSV', NULL, NULL, 4, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (104, 1, N'T_SealOff', N'Ticket Seal Off', NULL, NULL, 1, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (105, 1, N'T_SealOn', N'Ticket Seal On', NULL, NULL, 1, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (106, 1, N'T_ProductObsTemp', N'Ticket Obs Temp', NULL, NULL, 4, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (107, 1, N'T_ProductObsGravity', N'Ticket Obs Gravity', NULL, NULL, 4, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (108, 1, N'T_ProductBSW', N'Ticket BS&W', NULL, NULL, 4, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (109, 1, N'T_Rejected', N'Ticket Rejected', NULL, NULL, 5, N'SELECT ID=1, Name=''Yes'' UNION SELECT 2, ''No''', 0)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (110, 1, N'T_RejectNotes', N'Ticket Reject Notes', NULL, NULL, 1, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (111, 1, N'PreviousDestinations', N'Prev Destinations', NULL, NULL, 1, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (112, 1, N'RerouteUsers', N'Reroute Users', NULL, NULL, 1, NULL, 1)
INSERT [tblReportColumnDefinition] ([ID], [ReportID], [DataField], [Caption], [DataFormat], [FilterDataField], [FilterTypeID], [FilterDropDownSql], [FilterAllowCustomText]) VALUES (113, 1, N'RerouteNotes', N'Reroute Notes', NULL, NULL, 1, NULL, 1)
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblUserReportDefinition]
(
	[ID] [int] IDENTITY(1,1),
	[Name] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ReportID] [int] NOT NULL,
	[UserName] [nvarchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CreateDateUTC] [datetime] NOT NULL,
	[CreatedByUser] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[LastChangeDateUTC] [datetime] NULL,
	[LastChangedByUser] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblLocationType] ADD CONSTRAINT [DF_LocationType] DEFAULT (getutcdate()) FOR [CreateDateUTC]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblReportColumnDefinitionBaseFilter]
(
	[ID] [int] NOT NULL,
	[ReportColumnID] [int] NOT NULL,
	[BaseFilterTypeID] [int] NOT NULL,
	[ProfileField] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IncludeWhereClause] [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IncludeWhereClause_ProfileTemplate] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [tblReportColumnDefinitionBaseFilter] ([ID], [ReportColumnID], [BaseFilterTypeID], [ProfileField], [IncludeWhereClause], [IncludeWhereClause_ProfileTemplate]) VALUES (1, 1, 2, N'CarrierID', N'(@CarrierID = -1 OR ID = @CarrierID)', N'@CarrierID')
INSERT [tblReportColumnDefinitionBaseFilter] ([ID], [ReportColumnID], [BaseFilterTypeID], [ProfileField], [IncludeWhereClause], [IncludeWhereClause_ProfileTemplate]) VALUES (2, 1, 2, N'CustomerID', N'(@CustomerID = -1 OR ID IN (SELECT DISTINCT CarrierID FROM tblOrder WHERE CustomerID = @CustomerID))', N'@CustomerID')
INSERT [tblReportColumnDefinitionBaseFilter] ([ID], [ReportColumnID], [BaseFilterTypeID], [ProfileField], [IncludeWhereClause], [IncludeWhereClause_ProfileTemplate]) VALUES (3, 2, 2, N'CarrierID', N'(@CarrierID = -1 OR ID IN (SELECT DISTINCT CustomerID FROM tblOrder WHERE CarrierID = @CarrierID))', N'@CarrierID')
INSERT [tblReportColumnDefinitionBaseFilter] ([ID], [ReportColumnID], [BaseFilterTypeID], [ProfileField], [IncludeWhereClause], [IncludeWhereClause_ProfileTemplate]) VALUES (4, 2, 2, N'CustomerID', N'(@CustomerID = -1 OR ID = @CustomerID)', N'@CustomerID')
INSERT [tblReportColumnDefinitionBaseFilter] ([ID], [ReportColumnID], [BaseFilterTypeID], [ProfileField], [IncludeWhereClause], [IncludeWhereClause_ProfileTemplate]) VALUES (5, 3, 2, N'CarrierID', N'(@CarrierID = -1 OR ID IN (SELECT DISTINCT DestinationID FROM tblOrder WHERE CarrierID = @CarrierID))', N'@CarrierID')
INSERT [tblReportColumnDefinitionBaseFilter] ([ID], [ReportColumnID], [BaseFilterTypeID], [ProfileField], [IncludeWhereClause], [IncludeWhereClause_ProfileTemplate]) VALUES (6, 3, 2, N'CustomerID', N'(@CustomerID = -1 OR ID IN (SELECT DestinationID FROM tblDestinationCustomers WHERE CustomerID = @CustomerID UNION SELECT DISTINCT DestinationID FROM tblOrder WHERE CustomerID = @CustomerID))', N'@CustomerID')
INSERT [tblReportColumnDefinitionBaseFilter] ([ID], [ReportColumnID], [BaseFilterTypeID], [ProfileField], [IncludeWhereClause], [IncludeWhereClause_ProfileTemplate]) VALUES (7, 6, 2, N'CarrierID', N'(@CarrierID = -1 OR ID IN (SELECT DISTINCT OriginID FROM tblOrder WHERE CarrierID = @CarrierID))', N'@CarrierID')
INSERT [tblReportColumnDefinitionBaseFilter] ([ID], [ReportColumnID], [BaseFilterTypeID], [ProfileField], [IncludeWhereClause], [IncludeWhereClause_ProfileTemplate]) VALUES (8, 6, 2, N'CustomerID', N'(@CustomerID = -1 OR ID IN (SELECT ID FROM tblOrigin WHERE CustomerID = @CustomerID UNION SELECT DISTINCT OriginID FROM tblOrder WHERE CustomerID = @CustomerID))', N'@CustomerID')
INSERT [tblReportColumnDefinitionBaseFilter] ([ID], [ReportColumnID], [BaseFilterTypeID], [ProfileField], [IncludeWhereClause], [IncludeWhereClause_ProfileTemplate]) VALUES (9, 10, 2, N'CarrierID', N'(@CarrierID = -1 OR ID IN (SELECT DISTINCT ProductID FROM tblOrder WHERE CarrierID = @CarrierID))', N'@CarrierID')
INSERT [tblReportColumnDefinitionBaseFilter] ([ID], [ReportColumnID], [BaseFilterTypeID], [ProfileField], [IncludeWhereClause], [IncludeWhereClause_ProfileTemplate]) VALUES (10, 10, 2, N'CustomerID', N'(@CustomerID = -1 OR ID IN (SELECT ProductID FROM tblOrigin O JOIN tblOriginProducts OP ON OP.OriginID = O.ID WHERE O.CustomerID = @CustomerID UNION SELECT DISTINCT ProductID FROM tblOrder WHERE CustomerID = @CustomerID))', N'@CustomerID')
INSERT [tblReportColumnDefinitionBaseFilter] ([ID], [ReportColumnID], [BaseFilterTypeID], [ProfileField], [IncludeWhereClause], [IncludeWhereClause_ProfileTemplate]) VALUES (11, 31, 2, N'CarrierID', N'(@CarrierID = -1 OR ID IN (SELECT DISTINCT OperatorID FROM tblOrder WHERE CarrierID = @CarrierID))', N'@CarrierID')
INSERT [tblReportColumnDefinitionBaseFilter] ([ID], [ReportColumnID], [BaseFilterTypeID], [ProfileField], [IncludeWhereClause], [IncludeWhereClause_ProfileTemplate]) VALUES (12, 31, 2, N'CustomerID', N'(@CustomerID = -1 OR ID IN (SELECT OperatorID FROM tblOrigin WHERE CustomerID = @CustomerID UNION SELECT DISTINCT OperatorID FROM tblOrder WHERE CustomerID = @CustomerID))', N'@CustomerID')
INSERT [tblReportColumnDefinitionBaseFilter] ([ID], [ReportColumnID], [BaseFilterTypeID], [ProfileField], [IncludeWhereClause], [IncludeWhereClause_ProfileTemplate]) VALUES (13, 32, 2, N'CarrierID', N'(@CarrierID = -1 OR ID IN (SELECT DISTINCT PumperID FROM tblOrder WHERE CarrierID = @CarrierID))', N'@CarrierID')
INSERT [tblReportColumnDefinitionBaseFilter] ([ID], [ReportColumnID], [BaseFilterTypeID], [ProfileField], [IncludeWhereClause], [IncludeWhereClause_ProfileTemplate]) VALUES (14, 32, 2, N'CustomerID', N'(@CustomerID = -1 OR ID IN (SELECT PumperID FROM tblOrigin WHERE CustomerID = @CustomerID UNION SELECT DISTINCT PumperID FROM tblOrder WHERE CustomerID = @CustomerID))', N'@CustomerID')
INSERT [tblReportColumnDefinitionBaseFilter] ([ID], [ReportColumnID], [BaseFilterTypeID], [ProfileField], [IncludeWhereClause], [IncludeWhereClause_ProfileTemplate]) VALUES (15, 42, 2, N'CarrierID', N'(@CarrierID = -1 OR ID IN (SELECT DISTINCT ProducerID FROM tblOrder WHERE CarrierID = @CarrierID))', N'@CarrierID')
INSERT [tblReportColumnDefinitionBaseFilter] ([ID], [ReportColumnID], [BaseFilterTypeID], [ProfileField], [IncludeWhereClause], [IncludeWhereClause_ProfileTemplate]) VALUES (16, 42, 2, N'CustomerID', N'(@CustomerID = -1 OR ID IN (SELECT ProducerID FROM tblOrigin WHERE CustomerID = @CustomerID UNION SELECT DISTINCT ProducerID FROM tblOrder WHERE CustomerID = @CustomerID))', N'@CustomerID')
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblUserReportColumnDefinition]
(
	[ID] [int] IDENTITY(1,1),
	[UserReportID] [int] NOT NULL,
	[ReportColumnID] [int] NOT NULL,
	[Caption] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SortNum] [int] NULL,
	[DataFormat] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FilterOperatorID] [int] NOT NULL,
	[FilterValue1] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FilterValue2] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CreateDateUTC] [datetime] NOT NULL,
	[CreatedByUser] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[LastChangeDateUTC] [datetime] NULL,
	[LastChangedByUser] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*************************************************/
-- Date Created: 26 Jul 2014
-- Author: Kevin Alons
-- Purpose: return the tblUserReportDefinition along with any any relevant tblReportDefinition fields
/*************************************************/
CREATE VIEW [dbo].[viewUserReportDefinition] AS
	SELECT URD.*
		, RD.SourceTable
	FROM tblUserReportDefinition URD
	JOIN tblReportDefinition RD ON RD.ID = URD.ReportID


GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblReportColumnDefinition] ADD CONSTRAINT [DF_ReportColumnDefinition_FilterAllowCustomText] DEFAULT ((0)) FOR [FilterAllowCustomText]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblUserReportDefinition] ADD CONSTRAINT [DF_UserReportDefinition_CreateDateUTC] DEFAULT (getutcdate()) FOR [CreateDateUTC]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblUserReportDefinition] ADD CONSTRAINT [DF_UserReportDefinition_CreatedByUser] DEFAULT (suser_name()) FOR [CreatedByUser]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[viewReportColumnDefinition] AS
	SELECT RCD.ID
		, RCD.ReportID
		, RCD.DataField
		, Caption = ISNULL(RCD.Caption, RCD.DataField)
		, RCD.DataFormat
		, RCD.FilterDataField
		, RCD.FilterTypeID
		, RCD.FilterDropDownSql
		, RCD.FilterAllowCustomText
		, RFT.FilterOperatorID_CSV
		, BaseFilterCount = (SELECT COUNT(*) FROM tblReportColumnDefinitionBaseFilter WHERE ReportColumnID = RCD.ID)
		, FilterOperatorID_CSV_Delim = ',' + RFT.FilterOperatorID_CSV + ','
	FROM tblReportColumnDefinition RCD
	JOIN tblReportFilterType RFT ON RFT.ID = RCD.FilterTypeID


GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************/
-- Date Created: 27 Jul 2014
-- Author: Kevin Alons
-- Purpose: add related JOINed fields to the tblUserReportColumnDefinition table results
/***********************************************/
CREATE VIEW [dbo].[viewUserReportColumnDefinition] AS
	SELECT URCD.ID
		, URCD.UserReportID
		, URCD.ReportColumnID
		, URCD.Caption
		, DataFormat = isnull(URCD.DataFormat, RCD.DataFormat)
		, URCD.SortNum
		, URCD.FilterOperatorID
		, URCD.FilterValue1
		, URCD.FilterValue2
		, URCD.CreateDateUTC, URCD.CreatedByUser
		, URCD.LastChangeDateUTC, URCD.LastChangedByUser
		, RCD.DataField
		, BaseCaption = RCD.Caption
		, OutputCaption = coalesce(URCD.Caption, RCD.Caption, RCD.DataField)
		, RCD.FilterTypeID
		, FilterDataField = isnull(RCD.FilterDataField, RCD.DataField)
		, RCD.FilterDropDownSql
		, RCD.FilterAllowCustomText
		, FilterOperator = RFO.Name
		, FilterValues = CASE 
				WHEN RFO.ID IN (1,4,5) OR RCD.FilterAllowCustomText = 1 THEN FilterValue1 + ISNULL(' - ' + FilterValue2, '')
				ELSE 
					CASE WHEN FilterValue1 IS NOT NULL THEN '&lt;filtered&gt;' 
						 ELSE NULL 
					END 
			END
	FROM tblUserReportColumnDefinition URCD
	JOIN tblReportColumnDefinition RCD ON RCD.ID = URCD.ReportColumnID
	JOIN tblReportFilterOperator RFO ON RFO.ID = URCD.FilterOperatorID


GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************/
-- Date Created: 23 Jul 2014
-- Author: Kevin Alons
-- Purpose: clone an existing UserReport (defaults to a full clone with columns, but can do an AddNew otherwise)
/*******************************************/
CREATE PROCEDURE [dbo].[spCloneUserReport]
(
  @userReportID int
, @reportName varchar(50)
, @userName varchar(100)
, @includeColumns bit = 1
, @newID int = 0 OUTPUT 
) AS
BEGIN
	SET NOCOUNT ON
	INSERT INTO tblUserReportDefinition (Name, ReportID, UserName, CreatedByUser)
		SELECT @reportName, ReportID, @userName, isnull(@userName, 'Administrator')
		FROM tblUserReportDefinition WHERE ID = @userReportID
	SET @newID = SCOPE_IDENTITY()
	IF (@includeColumns = 1)
	BEGIN
		INSERT INTO tblUserReportColumnDefinition (UserReportID, ReportColumnID, Caption
			, SortNum, DataFormat, FilterOperatorID, FilterValue1, FilterValue2, CreatedByUser)
			SELECT @newID, ReportColumnID, Caption
				, SortNum, DataFormat, FilterOperatorID, FilterValue1, FilterValue2, isnull(@userName, 'Administrator')
			FROM tblUserReportColumnDefinition WHERE UserReportID = @userReportID
	END
	
	SELECT NEWID=@newID
	RETURN (isnull(@newID, 0))
END


GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblUserReportColumnDefinition] ADD CONSTRAINT [DF_UserReportColumnDefinition_CreateDateUTC] DEFAULT (getutcdate()) FOR [CreateDateUTC]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblUserReportColumnDefinition] ADD CONSTRAINT [DF_UserReportColumnDefinition_CreatedByUser] DEFAULT (suser_name()) FOR [CreatedByUser]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblUserReportColumnDefinition] ADD CONSTRAINT [DF_UserReportColumnDefinition_FilterOperator] DEFAULT ((1)) FOR [FilterOperatorID]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblDriverLocation]
(
	[ID] [int] IDENTITY(1,1),
	[UID] [uniqueidentifier] NOT NULL,
	[DriverID] [int] NOT NULL,
	[OrderID] [int] NULL,
	[OriginID] [int] NULL,
	[DestinationID] [int] NULL,
	[LocationTypeID] [tinyint] NOT NULL,
	[Lat] [decimal](9,6) NOT NULL,
	[Lon] [decimal](9,6) NOT NULL,
	[DistanceToPoint] [int] NULL,
	[SourceAccuracyMeters] [smallint] NULL,
	[SourceDateUTC] [smalldatetime] NULL,
	[CreateDateUTC] [smalldatetime] NOT NULL,
	[CreatedByUser] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************/
-- Date Created: 12 Mar 2014
-- Author: Kevin Alons
-- Purpose: return Origin Tank records with translated values
/***********************************/
ALTER VIEW [dbo].[viewOriginTank] AS
	SELECT OT.*
		, O.Name AS Origin
		, CASE WHEN OT.TankNum = '*' THEN '[Enter Details]' ELSE OT.TankNum END AS TankNum_Unstrapped
	FROM dbo.tblOriginTank OT
	JOIN dbo.viewOrigin O ON O.ID = OT.OriginID



GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*******************************************/
-- Date Created: 5 Apr 2014
-- Author: Kevin Alons
-- Purpose: return OriginTankStrapping data for Driver App sync
/*******************************************/
ALTER FUNCTION [dbo].[fnOriginTankStrapping_DriverApp]( @DriverID int, @LastChangeDateUTC datetime ) RETURNS  TABLE AS
RETURN 
	SELECT DISTINCT OTS.*
/*		, OTS.OriginTankID
		, OTS.Feet
		, OTS.Inches
		, OTS.Q
		, OTS.BPQ
		, OTS.IsMinimum
		, OTS.IsMaximum
		, OTS.CreateDateUTC
		, OTS.CreatedByUser
		, OTS.LastChangeDateUTC
		, OTS.LastChangedByUser */
		, cast(CASE WHEN OTSD.ID IS NULL THEN 0 ELSE 1 END as bit) AS IsDeleted
	FROM dbo.tblOriginTankStrapping OTS
	JOIN dbo.tblOriginTank OT ON OT.ID = OTS.OriginTankID
	JOIN dbo.tblOrder O ON O.OriginID = OT.OriginID
	LEFT JOIN tblOrderDriverAppVirtualDelete ODAVD ON ODAVD.OrderID = O.ID AND ODAVD.DriverID = @DriverID
	LEFT JOIN dbo.tblOriginTankStrappingDeleted OTSD ON OTSD.ID = OTS.ID
	CROSS JOIN (SELECT DATEADD(second, -5, @LastChangeDateUTC) AS LCD) LCD
	WHERE (StatusID IN (2, 7, 8, 3) -- Dispatched, Accepted, Picked Up, Delivered
		-- or this potentially was a DriverApp record, that no longer is, so should be included (as deleted)
		OR (ODAVD.ID IS NOT NULL AND ODAVD.VirtualDeleteDateUTC >= LCD.LCD))
	  AND O.DriverID = @DriverID
	  AND (@LastChangeDateUTC IS NULL 
		OR O.CreateDateUTC >= LCD.LCD
		OR O.LastChangeDateUTC >= LCD.LCD
		OR OT.CreateDateUTC >= LCD.LCD
		OR OT.LastChangeDateUTC >= LCD.LCD
		OR OTS.CreateDateUTC >= LCD.LCD
		OR OTS.LastChangeDateUTC >= LCD.LCD
		OR OTSD.DeleteDateUTC >= LCD.LCD)


GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*******************************************/
-- Date Created: 5 Apr 2014
-- Author: Kevin Alons
-- Purpose: return OriginTank data for Driver App sync
/*******************************************/
ALTER FUNCTION [dbo].[fnOriginTank_DriverApp]( @DriverID int, @LastChangeDateUTC datetime ) RETURNS  TABLE AS
RETURN 
	SELECT DISTINCT OT.ID
		, OT.OriginID
		, OT.TankNum
		, OT.TankDescription
		, OT.CreateDateUTC
		, OT.CreatedByUser
		, OT.LastChangeDateUTC
		, OT.LastChangedByUser
		, OT.DeleteDateUTC
		, OT.DeletedByUser
	FROM dbo.tblOriginTank OT
	JOIN dbo.tblOrder O ON O.OriginID = OT.OriginID
	LEFT JOIN tblOrderDriverAppVirtualDelete ODAVD ON ODAVD.OrderID = O.ID AND ODAVD.DriverID = @DriverID
	CROSS JOIN (SELECT DATEADD(second, -5, @LastChangeDateUTC) AS LCD) LCD
	WHERE (StatusID IN (2, 7, 8, 3) -- Dispatched, Accepted, Picked Up, Delivered
		-- or this potentially was a DriverApp record, that no longer is, so should be included (as deleted)
		OR (ODAVD.ID IS NOT NULL AND ODAVD.VirtualDeleteDateUTC >= LCD.LCD))
	  AND O.DriverID = @DriverID
	  AND (@LastChangeDateUTC IS NULL 
		OR O.CreateDateUTC >= LCD.LCD
		OR O.LastChangeDateUTC >= LCD.LCD
		OR OT.CreateDateUTC >= LCD.LCD
		OR OT.LastChangeDateUTC >= LCD.LCD
		OR OT.DeleteDateUTC >= LCD.LCD
		OR ODAVD.VirtualDeleteDateUTC >= LCD.LCD)


GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************/
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Order records with "translated friendly" values for FK relationships
/***********************************/
ALTER VIEW [dbo].[viewOrder] AS
SELECT O.*
	, OPS.OrderStatus AS PrintStatus
FROM (
	SELECT O.*
	, OrderDate = dbo.fnDateOnly(dbo.fnUTC_to_Local(O.OriginDepartTimeUTC, vO.TimeZoneID, vO.UseDST)) 
	, Origin = vO.Name 
	, OriginFull = vO.FullName 
	, OriginState = vO.State 
	, OriginStateAbbrev = vO.StateAbbrev 
	, OriginStation = vO.Station 
	, vO.LeaseName
	, vO.LeaseNum
	, OriginLegalDescription = vO.LegalDescription 
	, OriginNDIC = vO.NDICFileNum 
	, OriginNDM = vO.NDM 
	, OriginCA = vO.CA 
	, OriginTimeZoneID = vO.TimeZoneID 
	, OriginUseDST = vO.UseDST 
	, OriginStateID = vO.StateID
	, vO.H2S
	, Destination = vD.Name 
	, DestinationFull = vD.FullName 
	, DestinationState = vD.State 
	, DestinationStateAbbrev = vD.StateAbbrev 
	, DestinationTypeID = vD.ID
	, vD.DestinationType
	, DestStation = vD.Station 
	, DestTimeZoneID = vD.TimeZoneID 
	, DestUseDST = vD.UseDST 
	, DestStateID = vD.StateID
	, Customer = C.Name 
	, Carrier = CA.Name 
	, CarrierType = CT.Name 
	, OS.OrderStatus
	, OS.StatusNum
	, Driver = D.FullName 
	, DriverFirst = D.FirstName 
	, DriverLast = D.LastName 
	, Truck = TRU.FullName 
	, Trailer = TR1.FullName 
	, Trailer2 = TR2.FullName 
	, P.PriorityNum
	, TicketType = TT.Name 
	, DestTicketTypeID = vD.TicketTypeID 
	, DestTicketType = vD.TicketType 
	, Operator = OP.Name 
	, Producer = PR.Name 
	, Pumper = PU.FullName 
	, DriverNumber = D.IDNumber
	, CarrierNumber = CA.IDNumber
	, CarrierTypeID = CA.CarrierTypeID
	, TruckNumber = TRU.IDNumber 
	, TrailerNumber = TR1.IDNumber 
	, Trailer2Number = TR2.IDNumber 
	, Product = PRO.Name
	, ProductShort = PRO.ShortName 
	, ProductGroup = isnull(PRO.ProductGroup, PRO.ShortName)
	, OriginUOM = OUom.Name 
	, OriginUomShort = OUom.Abbrev 
	, OriginTankID_Text = CASE WHEN ORT.TankNum = '*' THEN '*' + isnull(O.OriginTankNum, '?') ELSE ORT.TankNum END 
	, DestUOM = DUom.Name 
	, DestUomShort = DUom.Abbrev 
	, Active = cast((CASE WHEN O.DeleteDateUTC IS NULL THEN 1 ELSE 0 END) as bit) 
	, IsDeleted = cast((CASE WHEN O.DeleteDateUTC IS NOT NULL THEN 1 ELSE 0 END) as bit) 
	, PickupPrintStatus = PPS.Name 
	, PickupCompleted = PPS.IsCompleted 
	, DeliverPrintStatus = DPS.Name 
	, DeliverCompleted = DPS.IsCompleted 
	, PrintStatusID = CASE WHEN StatusID = 8 AND PPS.IsCompleted = 0 THEN 7
						   WHEN StatusID = 3 AND DPS.IsCompleted = 0 THEN 8
						ELSE StatusID END 
	, OriginTankText = CASE WHEN O.OriginTankID IS NULL OR ORT.TankNum = '*' THEN O.OriginTankNum ELSE ORT.TankNum END
	FROM dbo.tblOrder O
	LEFT JOIN dbo.viewOrigin vO ON vO.ID = O.OriginID
	LEFT JOIN dbo.viewDestination vD ON vD.ID = O.DestinationID
	LEFT JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
	JOIN dbo.tblOrderStatus AS OS ON OS.ID = O.StatusID
	LEFT JOIN dbo.tblCarrier CA ON CA.ID = O.CarrierID
	LEFT JOIN dbo.tblCarrierType CT ON CT.ID = CA.CarrierTypeID
	LEFT JOIN dbo.viewDriver D ON D.ID = O.DriverID
	LEFT JOIN dbo.viewTruck TRU ON TRU.ID = O.TruckID
	LEFT JOIN dbo.viewTrailer TR1 ON TR1.ID = O.TrailerID
	LEFT JOIN dbo.viewTrailer TR2 ON TR2.ID = O.Trailer2ID
	LEFT JOIN dbo.tblPriority P ON P.ID = O.PriorityID
	LEFT JOIN dbo.tblTicketType TT ON TT.ID = O.TicketTypeID
	LEFT JOIN dbo.tblOriginTank ORT ON ORT.ID = O.OriginTankID
	LEFT JOIN dbo.tblOperator OP ON OP.ID = O.OperatorID
	LEFT JOIN dbo.viewPumper PU ON PU.ID = O.PumperID
	LEFT JOIN dbo.tblProducer PR ON PR.ID = O.ProducerID
	LEFT JOIN dbo.tblProduct PRO ON PRO.ID = O.ProductID
	LEFT JOIN dbo.tblUom OUom ON OUom.ID = O.OriginUomID
	LEFT JOIN dbo.tblUom DUom ON DUom.ID = O.DestUomID
	LEFT JOIN dbo.tblPrintStatus PPS ON PPS.ID = O.PickupPrintStatusID
	LEFT JOIN dbo.tblPrintStatus DPS ON DPS.ID = O.DeliverPrintStatusID
) O
LEFT JOIN dbo.tblOrderStatus OPS ON OPS.ID = O.PrintStatusID


GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************/
-- Date Created: 22 Jan 2013
-- Author: Kevin Alons
-- Purpose: query the tblOrderTicket table and include "friendly" translated values
/***********************************/
ALTER VIEW [dbo].[viewOrderTicket] AS
SELECT OT.*
	, TT.Name AS TicketType
	, CASE WHEN OT.OriginTankID IS NULL OR ORT.TankNum = '*' THEN OT.TankNum ELSE ORT.TankNum END AS OriginTankText
	, cast(CASE WHEN OT.OriginTankID IS NULL OR ORT.TankNum = '*' THEN 0 ELSE 1 END as bit) AS IsStrappedTank
	, cast((CASE WHEN OT.DeleteDateUTC IS NULL THEN 1 ELSE 0 END) as bit) AS Active
	, cast((CASE WHEN OT.DeleteDateUTC IS NOT NULL THEN 1 ELSE 0 END) as bit) AS IsDeleted
	, Gravity60F = dbo.fnCorrectedAPIGravity(ProductObsGravity, ProductObsTemp)
FROM tblOrderTicket OT
JOIN tblTicketType TT ON TT.ID = OT.TicketTypeID
LEFT JOIN tblOriginTank ORT ON ORT.ID = OT.OriginTankID


GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*******************************************/
-- Date Created: 31 Aug 2013
-- Author: Kevin Alons
-- Purpose: return driver editable Order data for Driver App sync
/*******************************************/
ALTER FUNCTION [dbo].[fnOrderEdit_DriverApp]( @DriverID int, @LastChangeDateUTC datetime ) RETURNS TABLE AS
RETURN 
	SELECT O.ID
		, O.StatusID
		, O.TruckID
		, O.TrailerID
		, O.Trailer2ID
		, O.OriginBOLNum
		, O.OriginArriveTimeUTC
		, O.OriginDepartTimeUTC
		, O.OriginMinutes
		, O.OriginWaitNotes
		, O.OriginTruckMileage
		, O.OriginGrossUnits
		, O.OriginGrossStdUnits
		, O.OriginNetUnits
		, O.ChainUp
		, O.Rejected
		, O.RejectNotes
		, O.OriginTankNum
		, O.DestArriveTimeUTC
		, O.DestDepartTimeUTC
		, O.DestMinutes
		, O.DestWaitNotes
		, O.DestBOLNum
		, O.DestTruckMileage
		, O.DestGrossUnits
		, O.DestNetUnits
		, O.DestProductTemp
		, O.DestProductBSW
		, O.DestProductGravity
		, O.DestOpenMeterUnits
		, O.DestCloseMeterUnits
		, O.CarrierTicketNum
		, O.AcceptLastChangeDateUTC
		, O.PickupLastChangeDateUTC
		, O.DeliverLastChangeDateUTC
		, O.PickupPrintStatusID
		, O.DeliverPrintStatusID
		, O.PickupPrintDateUTC
		, O.DeliverPrintDateUTC
		, O.DriverNotes
	FROM dbo.tblOrder O
	LEFT JOIN tblOrderDriverAppVirtualDelete ODAVD ON ODAVD.OrderID = O.ID AND ODAVD.DriverID = @DriverID
	CROSS JOIN (SELECT DATEADD(second, -5, @LastChangeDateUTC) AS LCD) LCD
	WHERE (O.StatusID IN (2, 7, 8, 3) -- Dispatched, Accepted, Picked Up, Delivered
		-- or this potentially was a DriverApp record, that no longer is, so should be included (as deleted)
		OR (ODAVD.ID IS NOT NULL AND ODAVD.VirtualDeleteDateUTC >= LCD.LCD))
	  AND (O.DriverID = @DriverID OR ODAVD.DriverID = @DriverID)
	  AND (
		@LastChangeDateUTC IS NULL 
		OR O.CreateDateUTC >= LCD.LCD
		OR O.LastChangeDateUTC >= LCD.LCD
		OR O.DeleteDateUTC >= LCD.LCD
		OR ODAVD.VirtualDeleteDateUTC >= LCD.LCD)


GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*******************************************/
-- Date Created: 25 Apr 2013
-- Author: Kevin Alons
-- Purpose: return OrderTicket data for Driver App sync
/*******************************************/
ALTER FUNCTION [dbo].[fnOrderTicket_DriverApp]( @DriverID int, @LastChangeDateUTC datetime ) RETURNS  TABLE AS
RETURN 
	SELECT OT.ID
		, OT.UID
		, OT.OrderID
		, OT.CarrierTicketNum
		, OT.OriginTankID
		, OT.TankNum
		, OT.TicketTypeID
		, OT.ProductObsGravity
		, OT.ProductObsTemp
		, OT.ProductHighTemp
		, OT.ProductLowTemp
		, OT.ProductBSW
		, cast(OT.OpeningGaugeFeet as tinyint) AS OpeningGaugeFeet
		, cast(OT.OpeningGaugeInch as tinyint) AS OpeningGaugeInch
		, cast(OT.OpeningGaugeQ as tinyint) AS OpeningGaugeQ
		, cast(OT.ClosingGaugeFeet as tinyint) AS ClosingGaugeFeet
		, cast(OT.ClosingGaugeInch as tinyint) AS ClosingGaugeInch
		, cast(OT.ClosingGaugeQ as tinyint) AS ClosingGaugeQ
		, OT.BottomFeet
		, OT.BottomInches
		, OT.BottomQ
		, OT.GrossUnits
		, OT.GrossStdUnits
		, OT.NetUnits
		, OT.Rejected
		, OT.RejectNotes
		, OT.SealOff
		, OT.SealOn
		, OT.BOLNum
		, OT.CreateDateUTC
		, OT.CreatedByUser
		, OT.LastChangeDateUTC
		, OT.LastChangedByUser
		, OT.DeleteDateUTC
		, OT.DeletedByUser
	FROM dbo.tblOrderTicket OT
	JOIN dbo.tblOrder O ON O.ID = OT.OrderID
	LEFT JOIN tblOrderDriverAppVirtualDelete ODAVD ON ODAVD.OrderID = O.ID AND ODAVD.DriverID = @DriverID
	CROSS JOIN (SELECT DATEADD(second, -5, @LastChangeDateUTC) AS LCD) LCD
	WHERE (O.StatusID IN (2, 7, 8, 3) -- Dispatched, Accepted, Picked Up, Delivered
		-- or this potentially was a DriverApp record, that no longer is, so should be included (as deleted)
		OR (ODAVD.ID IS NOT NULL AND ODAVD.VirtualDeleteDateUTC >= LCD.LCD))
	  AND O.DriverID = @DriverID
	  AND (@LastChangeDateUTC IS NULL 
		OR OT.CreateDateUTC >= LCD.LCD
		OR OT.LastChangeDateUTC >= LCD.LCD
		OR ODAVD.VirtualDeleteDateUTC >= LCD.LCD)


GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblDriverLocation] ADD CONSTRAINT [DF_DriverLocation_CreateDateUTC] DEFAULT (getutcdate()) FOR [CreateDateUTC]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblDriverLocation] ADD CONSTRAINT [DF_DriverLocation_UID] DEFAULT (newid()) FOR [UID]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 19 Jun 2014
-- Description:	deactivate any currently active Destinations that haven't had any traffic since the Stale days threshold
-- =============================================
ALTER PROCEDURE [dbo].[spDeactivateStaleDestinations]
(
  @UserName varchar(100)
) 
AS BEGIN
	DECLARE @staleDays int
	SELECT @staleDays = Value FROM tblSetting WHERE ID = 21
	
	UPDATE tblDestination
		SET DeleteDateUTC = GETUTCDATE(), DeletedByUser = @UserName
	WHERE DeleteDateUTC IS NULL
		AND CreateDateUTC < DATEADD(day, -@staleDays, getdate())
		AND ID NOT IN (SELECT DestinationID FROM viewOrder WHERE OrderDate > DATEADD(day, -@staleDays, getdate()))
END


GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 19 Jun 2014
-- Description:	deactivate any currently active Origins that haven't had any traffic since the Stale days threshold
-- =============================================
ALTER PROCEDURE [dbo].[spDeactivateStaleOrigins]
(
  @UserName varchar(100)
) 
AS BEGIN
	DECLARE @staleDays int
	SELECT @staleDays = Value FROM tblSetting WHERE ID = 21
	
	UPDATE tblOrigin 
		SET DeleteDateUTC = GETUTCDATE(), DeletedByUser = @UserName
	WHERE DeleteDateUTC IS NULL
		AND CreateDateUTC < DATEADD(day, -@staleDays, getdate())
		AND ID NOT IN (SELECT OriginID FROM viewOrder WHERE OrderDate > DATEADD(day, -@staleDays, getdate()))
END


GO

EXEC _spRefreshAllViews
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************/
-- Date Created: 25 Apr 2013
-- Author: Kevin Alons
-- Purpose: return readonly Order data for Driver App sync
/*******************************************/
ALTER FUNCTION [dbo].[fnOrderReadOnly_DriverApp]( @DriverID int, @LastChangeDateUTC datetime ) RETURNS TABLE AS
RETURN 
	SELECT O.ID
		, O.OrderNum
		, O.StatusID
		, O.TicketTypeID
		, PriorityNum = cast(P.PriorityNum as int) 
		, O.Product
		, O.DueDate
		, O.Origin
		, O.OriginFull
		, OO.OriginType
		, O.OriginUomID
		, OriginStation = OO.Station 
		, OriginLeaseNum = OO.LeaseNum 
		, OriginCounty = OO.County 
		, OriginLegalDescription = OO.LegalDescription 
		, O.OriginNDIC
		, O.OriginNDM
		, O.OriginCA
		, O.OriginState
		, OO.WellAPI AS OriginAPI
		, OriginLat = OO.LAT 
		, OriginLon = OO.LON 
		, OriginGeoFenceRadiusMeters = OO.GeoFenceRadiusMeters 
		, O.Destination
		, O.DestinationFull
		, DestType = D.DestinationType 
		, O.DestUomID
		, DestLat = D.LAT 
		, DestLon = D.LON 
		, DestGeoFenceRadiusMeters = D.GeoFenceRadiusMeters 
		, D.Station AS DestinationStation
		, O.CreateDateUTC
		, O.CreatedByUser
		, O.LastChangeDateUTC
		, O.LastChangedByUser
		, DeleteDateUTC = isnull(ODAVD.VirtualDeleteDateUTC, O.DeleteDateUTC) 
		, DeletedByUser = isnull(ODAVD.VirtualDeletedByUser, O.DeletedByUser) 
		, O.OriginID
		, O.DestinationID
		, PriorityID = cast(O.PriorityID AS int) 
		, O.Operator
		, O.OperatorID
		, O.Pumper
		, O.PumperID
		, O.Producer
		, O.ProducerID
		, O.Customer
		, O.CustomerID
		, O.Carrier
		, O.CarrierID
		, O.ProductID
		, O.TicketType
		, PrintHeaderBlob = C.ZPLHeaderBlob 
		, EmergencyInfo = isnull(C.EmergencyInfo, 'For an emergency (spill, leak, fire, or accident) contact CHEMTREC @ 800.424.9300 (toll free)') 
		, O.DestTicketTypeID
		, O.DestTicketType
		, O.OriginTankNum
		, O.OriginTankID
		, O.DispatchNotes
		, O.DispatchConfirmNum
		, RouteActualMiles = isnull(R.ActualMiles, 0)
		, CarrierAuthority = CA.Authority 
		, OriginTimeZone = OO.TimeZone
		, DestTimeZone = D.TimeZone
	FROM dbo.viewOrder O
	JOIN dbo.tblPriority P ON P.ID = O.PriorityID
	JOIN dbo.viewOrigin OO ON OO.ID = O.OriginID
	JOIN dbo.viewDestination D ON D.ID = O.DestinationID
	JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
	JOIN dbo.tblRoute R ON R.ID = O.RouteID
	JOIN dbo.tblCarrier CA ON CA.ID = O.CarrierID
	LEFT JOIN tblOrderDriverAppVirtualDelete ODAVD ON ODAVD.OrderID = O.ID AND ODAVD.DriverID = @DriverID
	CROSS JOIN (SELECT DATEADD(second, -5, @LastChangeDateUTC) AS LCD) LCD
	WHERE (O.StatusID IN (2, 7, 8, 3) -- Dispatched, Accepted, Picked Up, Delivered
		-- or this potentially was a DriverApp record, that no longer is, so should be included (as deleted)
		OR (ODAVD.ID IS NOT NULL AND ODAVD.VirtualDeleteDateUTC >= LCD.LCD))
	  AND (O.DriverID = @DriverID OR ODAVD.DriverID = @DriverID)
	  AND (
		@LastChangeDateUTC IS NULL 
		OR O.CreateDateUTC >= LCD.LCD
		OR O.LastChangeDateUTC >= LCD.LCD
		OR O.DeleteDateUTC >= LCD.LCD
		OR C.LastChangeDateUTC >= LCD.LCD
		OR CA.LastChangeDateUTC >= LCD.LCD
		OR OO.LastChangeDateUTC >= LCD.LCD
		OR R.LastChangeDateUTC >= LCD.LCD
		OR ODAVD.VirtualDeleteDateUTC >= LCD.LCD)


GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*****************************************************************************************/
-- Author: Kevin Alons
-- Date Created: 27 Feb 2014
-- Purpose: return export final orders in the SunocoSundex
-- TODO: this has some hardcoded values for the SUNOCO customer (shipper), need to make dynamic
/*****************************************************************************************/
ALTER VIEW [dbo].[viewSunocoSundex] AS
SELECT
	_ID = O.ID
	, _CustomerID = O.CustomerID
	, _OrderNum = O.OrderNum
	, Request_Code = CASE WHEN EP.IsNew = 1 THEN 'A' ELSE 'C' END
	, Company_Code = '0007020233'
	, Ticket_Type = UPPER(left(O.TicketType, 1))
	, Ticket_Source_Code = 'BATA'
	, Ticket_Number = isnull(T.CarrierTicketNum, ltrim(O.OrderNum) + 'X')
	, Ticket_Date = dbo.fnDateMMddYYYY(O.OrderDate)
	, SXL_Property_Code = isnull(O.LeaseNum, '')
	, TP_Property_Code = ''
	, Lease_Company_Name = O.Origin
	, Destination = isnull(CDC.Code, '')
	, Tank_Meter_Number = coalesce(T.OriginTankText, (SELECT min(TankNum) FROM tblOriginTank WHERE DeleteDateUTC IS NULL AND OriginID = O.OriginID), '1')
	, Open_Date = dbo.fnDateMMddYYYY(O.OriginArriveTime)
	, Open_Time = dbo.fnTimeOnly(O.OriginArriveTime)
	, Close_Date = dbo.fnDateMMddYYYY(O.OriginDepartTime)
	, Close_Time = dbo.fnTimeOnly(O.OriginDepartTime)
	, Estimated_Volume = cast(ROUND(O.OriginGrossStdUnits, 2) as decimal(18, 2))
	, Gross_Volume = cast(ROUND(O.OriginGrossUnits, 2) as decimal(18, 2))
	, Net_Volume = cast(ROUND(O.OriginNetUnits, 2) as decimal(18, 2))
	, Observed_Gravity = isnull(ltrim(T.ProductObsGravity), '')
	, Observed_Temperature = isnull(ltrim(T.ProductObsTemp), '')
	, Observed_BSW = isnull(ltrim(T.ProductBSW), '')
	, Corrected_Gravity_API = 0
	, Purchaser = 'Sonoco Logistics'
	, First_Reading_Gauge_Ft = isnull(ltrim(T.OpeningGaugeFeet), '')
	, First_Reading_Gauge_In = isnull(ltrim(T.OpeningGaugeInch), '')
	, First_Reading_Gauge_Nu = isnull(ltrim(T.OpeningGaugeQ), '')
	, First_Reading_Gauge_De = 4
	, First_Temperature = isnull(ltrim(T.ProductHighTemp), '')
	, First_Bottom_Ft = isnull(ltrim(T.BottomFeet), '')
	, First_Bottom_In = isnull(ltrim(T.BottomInches), '')
	, First_Bottom_Nu = isnull(ltrim(T.BottomQ), '')
	, First_Bottom_De = 4
	, Second_Reading_Gauge_Ft = isnull(ltrim(T.ClosingGaugeFeet), '')
	, Second_Reading_Gauge_In = isnull(ltrim(T.ClosingGaugeInch), '')
	, Second_Reading_Gauge_Nu = isnull(ltrim(T.ClosingGaugeQ), '')
	, Second_Reading_Gauge_De = 4
	, Second_Temperature = isnull(ltrim(T.ProductLowTemp), '')
	, Second_Bottom_Ft = 0
	, Second_Bottom_In = 0
	, Second_Bottom_Nu = 0
	, Second_Bottom_De = 4
	, Shrinkage_Incrustation_Factor = 1
	, First_Reading_Meter = 0
	, Second_Reading_Meter = 0
	, Meter_Factor = 0
	, Temp_Comp_Meter = ''
	, Avg_Line_Temp = ''
	, Truck_ID = ''
	, Trailer_ID = ''
	, Driver_ID =''
	, Miles = ''
	, CountyState = ''
	, Invoice_Number = ''
	, Invoice_Date = ''
	, Remarks= ''
	, API_Compliant_Chapter = ''
	, Use_SXL_Calculation = 'Y'
	, Seal_On = isnull(replace(T.SealOn, 'n/a', ''), '')
	, Seal_Off = isnull(replace(T.SealOff, 'n/a', ''), '')
	, Ticket_Exclusion_Cd = CASE WHEN isnull(T.Rejected, O.Rejected) = 1 THEN 'RF' ELSE '' END
	, Confirmation_Number = O.DispatchConfirmNum
	, Split_Flag = CASE WHEN (SELECT COUNT(*) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND DeleteDateUTC IS NULL) > 1 THEN 'Y' ELSE 'N' END
	, Paired_Ticket_Number = isnull((SELECT min(CarrierTicketNum) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND DeleteDateUTC IS NULL AND OT.CarrierTicketNum <> T.CarrierTicketNum), '')
	, Bobtail_Flag = 'N'
	, Ticket_Extra_Info_Flag = ''
FROM viewOrderLocalDates O
JOIN dbo.viewOrderCustomerFinalExportPending EP ON EP.ID = O.ID
LEFT JOIN viewOrderTicket T ON T.OrderID = O.ID AND T.DeleteDateUTC IS NULL
LEFT JOIN dbo.tblCustomerDestinationCode CDC ON CDC.DestinationID = O.DestinationID


GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order records with "computed" fields used in exporting to spreadsheet/etc
/***********************************/
ALTER PROCEDURE [dbo].[spOrdersExportCustomer]
(
  @StartDate datetime = NULL
, @EndDate datetime = NULL
, @CustomerID int = -1 -- all customers
, @ProductID int = -1 -- all products
, @BatchID int = NULL  -- either show unbatched (NULL), or the specified batch orders
) AS BEGIN
	SELECT @StartDate = dbo.fnDateOnly(@StartDate), @EndDate = dbo.fnDateOnly(@EndDate)
	
	SELECT DISTINCT OE.* 
	FROM dbo.viewOrder_Financial_Customer OE
	WHERE OE.StatusID IN (4)  
	  AND (@CustomerID=-1 OR OE.CustomerID=@CustomerID) 
	  AND (@ProductID=-1 OR OE.ProductID=@ProductID) 
	  AND (@StartDate IS NULL OR OE.OrderDate >= @StartDate) 
	  AND (@EndDate IS NULL OR OE.OrderDate <= @EndDate)
	  AND ((@BatchID IS NULL AND OE.BatchID IS NULL) OR OE.BatchID = @BatchID)
	ORDER BY OE.OriginDepartTimeUTC
	
END


GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblLocationType] ADD CONSTRAINT [PK_LocationType] PRIMARY KEY CLUSTERED
(
	[ID] ASC
)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblReportBaseFilterType] ADD CONSTRAINT [PK_ReportBaseFilterType] PRIMARY KEY CLUSTERED
(
	[ID] ASC
)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblReportDefinition] ADD CONSTRAINT [PK_ReportDefinition] PRIMARY KEY CLUSTERED
(
	[ID] ASC
)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblReportFilterOperator] ADD CONSTRAINT [PK_ReportFilterOperator] PRIMARY KEY CLUSTERED
(
	[ID] ASC
)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblReportFilterType] ADD CONSTRAINT [PK_ReportFilterType] PRIMARY KEY CLUSTERED
(
	[ID] ASC
)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblOperator] ADD CONSTRAINT [PK_Operator] PRIMARY KEY CLUSTERED
(
	[ID] ASC
)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblReportColumnDefinition] ADD CONSTRAINT [PK_ReportColumnDefinition] PRIMARY KEY CLUSTERED
(
	[ID] ASC
)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblUserReportDefinition] ADD CONSTRAINT [PK_UserReportDefinition] PRIMARY KEY CLUSTERED
(
	[ID] ASC
)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblReportColumnDefinitionBaseFilter] ADD CONSTRAINT [PK_ReportColumnDefinitionBaseFilter] PRIMARY KEY CLUSTERED
(
	[ID] ASC
)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblUserReportColumnDefinition] ADD CONSTRAINT [PK_UserReportColumnDefinition] PRIMARY KEY CLUSTERED
(
	[ID] ASC
)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblDriverLocation] ADD CONSTRAINT [PK_DriverLocation] PRIMARY KEY CLUSTERED
(
	[ID] ASC
)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [udxLocationType_Name] ON [dbo].[tblLocationType]
(
	[Name] ASC
)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [udxReportBaseFilterType_Name] ON [dbo].[tblReportBaseFilterType]
(
	[Name] ASC
)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [udxReportDefinition_Name] ON [dbo].[tblReportDefinition]
(
	[Name] ASC
)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [udxReportFilterOperator_Name] ON [dbo].[tblReportFilterOperator]
(
	[Name] ASC
)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [udxReportFilterType_Name] ON [dbo].[tblReportFilterType]
(
	[Name] ASC
)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [udxReportColumnDefinition_Report_DataField] ON [dbo].[tblReportColumnDefinition]
(
	[ReportID] ASC, [DataField] ASC
)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [udxUserReportDefinition_UserName_Name] ON [dbo].[tblUserReportDefinition]
(
	[UserName] ASC, [Name] ASC
)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblReportColumnDefinition] WITH CHECK ADD CONSTRAINT [FK_ReportColumnDefinition_FilterType] FOREIGN KEY
(
	[FilterTypeID]
)
REFERENCES [dbo].[tblReportFilterType]
(
	[ID]
)
ON DELETE NO ACTION
ON UPDATE NO ACTION
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblReportColumnDefinition] WITH CHECK ADD CONSTRAINT [FK_ReportColumnDefinition_ReportDefinition] FOREIGN KEY
(
	[ReportID]
)
REFERENCES [dbo].[tblReportDefinition]
(
	[ID]
)
ON DELETE NO ACTION
ON UPDATE NO ACTION
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblUserReportDefinition] WITH CHECK ADD CONSTRAINT [FK_UserReportDefinition_ReportID] FOREIGN KEY
(
	[ReportID]
)
REFERENCES [dbo].[tblReportDefinition]
(
	[ID]
)
ON DELETE NO ACTION
ON UPDATE NO ACTION
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblReportColumnDefinitionBaseFilter] WITH CHECK ADD CONSTRAINT [FK_ReportColumnDefinitionBaseFilter_BaseFilterType] FOREIGN KEY
(
	[BaseFilterTypeID]
)
REFERENCES [dbo].[tblReportBaseFilterType]
(
	[ID]
)
ON DELETE NO ACTION
ON UPDATE NO ACTION
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblReportColumnDefinitionBaseFilter] WITH CHECK ADD CONSTRAINT [FK_ReportColumnDefinitionBaseFilter_ReportColumn] FOREIGN KEY
(
	[ReportColumnID]
)
REFERENCES [dbo].[tblReportColumnDefinition]
(
	[ID]
)
ON DELETE NO ACTION
ON UPDATE NO ACTION
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblPumper] WITH CHECK ADD CONSTRAINT [FK_tblPumper_tblOperator] FOREIGN KEY
(
	[OperatorID]
)
REFERENCES [dbo].[tblOperator]
(
	[ID]
)
ON DELETE NO ACTION
ON UPDATE NO ACTION
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblUserReportColumnDefinition] WITH CHECK ADD CONSTRAINT [FK_UserReportColumnDefinition_FilterOperator] FOREIGN KEY
(
	[FilterOperatorID]
)
REFERENCES [dbo].[tblReportFilterOperator]
(
	[ID]
)
ON DELETE NO ACTION
ON UPDATE NO ACTION
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblUserReportColumnDefinition] WITH CHECK ADD CONSTRAINT [FK_UserReportColumnDefinition_ReportColumn] FOREIGN KEY
(
	[ReportColumnID]
)
REFERENCES [dbo].[tblReportColumnDefinition]
(
	[ID]
)
ON DELETE NO ACTION
ON UPDATE NO ACTION
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblUserReportColumnDefinition] WITH CHECK ADD CONSTRAINT [FK_UserReportColumnDefinition_UserReport] FOREIGN KEY
(
	[UserReportID]
)
REFERENCES [dbo].[tblUserReportDefinition]
(
	[ID]
)
ON DELETE NO ACTION
ON UPDATE NO ACTION
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblOrigin] WITH CHECK ADD CONSTRAINT [FK_tblOrigin_tblOperator] FOREIGN KEY
(
	[OperatorID]
)
REFERENCES [dbo].[tblOperator]
(
	[ID]
)
ON DELETE NO ACTION
ON UPDATE NO ACTION
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblOriginProducts] WITH CHECK ADD CONSTRAINT [FK_OriginProducts_Product] FOREIGN KEY
(
	[ProductID]
)
REFERENCES [dbo].[tblProduct]
(
	[ID]
)
ON DELETE CASCADE
ON UPDATE NO ACTION
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblOrder] WITH CHECK ADD CONSTRAINT [FK_Order_Operator] FOREIGN KEY
(
	[OperatorID]
)
REFERENCES [dbo].[tblOperator]
(
	[ID]
)
ON DELETE SET NULL
ON UPDATE NO ACTION
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblDriverLocation] WITH CHECK ADD CONSTRAINT [FK_DriverLocation_Destination] FOREIGN KEY
(
	[DestinationID]
)
REFERENCES [dbo].[tblDestination]
(
	[ID]
)
ON DELETE NO ACTION
ON UPDATE NO ACTION
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblDriverLocation] WITH CHECK ADD CONSTRAINT [FK_DriverLocation_Driver] FOREIGN KEY
(
	[DriverID]
)
REFERENCES [dbo].[tblDriver]
(
	[ID]
)
ON DELETE NO ACTION
ON UPDATE NO ACTION
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblDriverLocation] WITH CHECK ADD CONSTRAINT [FK_DriverLocation_LocationType] FOREIGN KEY
(
	[LocationTypeID]
)
REFERENCES [dbo].[tblLocationType]
(
	[ID]
)
ON DELETE NO ACTION
ON UPDATE NO ACTION
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblDriverLocation] WITH CHECK ADD CONSTRAINT [FK_DriverLocation_Order] FOREIGN KEY
(
	[OrderID]
)
REFERENCES [dbo].[tblOrder]
(
	[ID]
)
ON DELETE NO ACTION
ON UPDATE NO ACTION
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblDriverLocation] WITH CHECK ADD CONSTRAINT [FK_DriverLocation_Origin] FOREIGN KEY
(
	[OriginID]
)
REFERENCES [dbo].[tblOrigin]
(
	[ID]
)
ON DELETE NO ACTION
ON UPDATE NO ACTION
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
ALTER TABLE [dbo].[tblOrderInvoiceCustomer] WITH CHECK ADD CONSTRAINT [FK_OrderInvoiceCustomer_SettlementBatch] FOREIGN KEY
(
	[BatchID]
)
REFERENCES [dbo].[tblCustomerSettlementBatch]
(
	[ID]
)
ON DELETE SET NULL
ON UPDATE CASCADE
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Kevin Alons
-- Create date: 23 Jul 2014
-- Description:	trigger to advance any duplicated SortNums in a UserReport to make reordering easier
-- =============================================
CREATE TRIGGER [dbo].[trigUserReportColumnDefinition_IU_SortNum] ON [dbo].[tblUserReportColumnDefinition] FOR INSERT, UPDATE AS
BEGIN
	IF (trigger_nestlevel() < 2)  -- logic below will recurse unless this is used to prevent it
	BEGIN
		IF (UPDATE(SortNum))
		BEGIN
			SELECT i.UserReportID, i.SortNum, ID = MAX(i2.ID)
			INTO #new
			FROM (
				SELECT UserReportID, SortNum = min(SortNum)
				FROM inserted
				GROUP BY UserReportID
			) i
			JOIN inserted i2 ON i2.UserReportID = i.UserReportID AND i2.SortNum = i.SortNum
			GROUP BY i.UserReportID, i.SortNum
			
			DECLARE @UserReportID int, @SortNum int, @ID int
			SELECT TOP 1 @UserReportID = UserReportID, @SortNum = SortNum, @ID = ID FROM #new 

			WHILE (@UserReportID IS NOT NULL)
			BEGIN
				UPDATE tblUserReportColumnDefinition 
					SET SortNum = N.NewSortNum
				FROM tblUserReportColumnDefinition URCD
				JOIN (
					SELECT ID, UserReportID, NewSortNum = @SortNum + ROW_NUMBER() OVER (ORDER BY SortNum) 
					FROM tblUserReportColumnDefinition
					WHERE UserReportID = @UserReportID AND SortNum >= @SortNum AND ID <> @ID
				) N ON N.ID = URCD.ID
				
				DELETE FROM #new WHERE ID = @ID
				SET @UserReportID = NULL
				SELECT TOP 1 @UserReportID = UserReportID, @SortNum = SortNum, @ID = ID FROM #new
			END	
		END
	END
END


GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/**********************************************************/
-- Author: Kevin Alons
-- Date Created: 26 Sep 2013
-- Purpose: track changes to Orders that cause it be "virtually" deleted for a Driver (for DriveApp.Sync purposes)
/**********************************************************/
ALTER TRIGGER [dbo].[trigOrder_U_VirtualDelete] ON [dbo].[tblOrder] AFTER UPDATE AS
BEGIN
	DECLARE @NewRecords TABLE (
		  OrderID int NOT NULL
		, DriverID int NOT NULL
		, LastChangeDateUTC smalldatetime NOT NULL
		, LastChangedByUser varchar(100) NULL
	)

	-- delete any records that no longer apply because now the record is valid for the new driver/status
	DELETE FROM tblOrderDriverAppVirtualDelete
	FROM tblOrderDriverAppVirtualDelete ODAVD
	JOIN inserted i ON i.ID = ODAVD.OrderID AND i.DriverID = ODAVD.DriverID
	WHERE i.DeleteDateUTC IS NULL 
		AND dbo.fnPrintStatusID(i.StatusID, i.PickupPrintStatusID, i.DeliverPrintStatusID) IN (2,7,8,3)

	INSERT INTO @NewRecords
		-- insert any applicable VIRTUALDELETE records where the status changed from 
		--   a valid status to an invalid [DriverApp] status
		SELECT d.ID, d.DriverID, GETUTCDATE(), ISNULL(i.LastChangedByUser, 'System')
		FROM deleted d
		JOIN inserted i ON i.ID = D.ID
		WHERE dbo.fnPrintStatusID(i.StatusID, i.PickupPrintStatusID, i.DeliverPrintStatusID) NOT IN (2,7,8,3)
		  AND dbo.fnPrintStatusID(d.StatusID, d.PickupPrintStatusID, d.DeliverPrintStatusID) IN (2,7,8,3)
		  AND d.DriverID IS NOT NULL
		  
		UNION

		-- insert any records where the DriverID changed to another DriverID (for a valid status)
		SELECT d.ID, d.DriverID, GETUTCDATE(), ISNULL(i.LastChangedByUser, 'System')
		FROM deleted d
		JOIN inserted i ON i.ID = D.ID
		WHERE d.DriverID <> i.DriverID 
		  AND dbo.fnPrintStatusID(d.StatusID, d.PickupPrintStatusID, d.DeliverPrintStatusID) IN (2,7,8,3)
		  AND d.DriverID IS NOT NULL
		  
	-- update the VirtualDeleteDateUTC value for any existing records
	UPDATE tblOrderDriverAppVirtualDelete
	  SET VirtualDeleteDateUTC = NR.LastChangeDateUTC, VirtualDeletedByUser = NR.LastChangedByUser
	FROM tblOrderDriverAppVirtualDelete ODAVD
	JOIN @NewRecords NR ON NR.OrderID = ODAVD.OrderID AND NR.DriverID = ODAVD.DriverID

	-- insert the truly new VirtualDelete records	
	INSERT INTO tblOrderDriverAppVirtualDelete (OrderID, DriverID, VirtualDeleteDateUTC, VirtualDeletedByUser)
		SELECT NR.OrderID, NR.DriverID, NR.LastChangeDateUTC, NR.LastChangedByUser
		FROM @NewRecords NR
		LEFT JOIN tblOrderDriverAppVirtualDelete ODAVD ON ODAVD.OrderID = NR.OrderID AND ODAVD.DriverID = NR.DriverID
		WHERE ODAVD.ID IS NULL

END


GO

IF EXISTS(SELECT 1 FROM sys.database_principals WHERE [name] = N'dispatchcrude_iis_acct')
	GRANT SELECT ON OBJECT::[dbo].[tblReportColumnDefinitionBaseFilter] TO [dispatchcrude_iis_acct]
GO

IF EXISTS(SELECT 1 FROM sys.database_principals WHERE [name] = N'dispatchcrude_iis_acct')
	GRANT DELETE ON OBJECT::[dbo].[tblUserReportColumnDefinition] TO [dispatchcrude_iis_acct]
GO

IF EXISTS(SELECT 1 FROM sys.database_principals WHERE [name] = N'dispatchcrude_iis_acct')
	GRANT SELECT ON OBJECT::[dbo].[tblReportBaseFilterType] TO [dispatchcrude_iis_acct]
GO

IF EXISTS(SELECT 1 FROM sys.database_principals WHERE [name] = N'dispatchcrude_iis_acct')
	GRANT SELECT ON OBJECT::[dbo].[tblUserReportDefinition] TO [dispatchcrude_iis_acct]
GO

IF EXISTS(SELECT 1 FROM sys.database_principals WHERE [name] = N'dispatchcrude_iis_acct')
	GRANT SELECT ON OBJECT::[dbo].[tblDriverLocation] TO [dispatchcrude_iis_acct]
GO

IF EXISTS(SELECT 1 FROM sys.database_principals WHERE [name] = N'dispatchcrude_iis_acct')
	GRANT DELETE ON OBJECT::[dbo].[tblUserReportDefinition] TO [dispatchcrude_iis_acct]
GO

IF EXISTS(SELECT 1 FROM sys.database_principals WHERE [name] = N'dispatchcrude_iis_acct')
	GRANT SELECT ON OBJECT::[dbo].[tblUserReportColumnDefinition] TO [dispatchcrude_iis_acct]
GO

IF EXISTS(SELECT 1 FROM sys.database_principals WHERE [name] = N'dispatchcrude_iis_acct')
	GRANT SELECT ON OBJECT::[dbo].[viewUserReportDefinition] TO [dispatchcrude_iis_acct]
GO

IF EXISTS(SELECT 1 FROM sys.database_principals WHERE [name] = N'dispatchcrude_iis_acct')
	GRANT SELECT ON OBJECT::[dbo].[viewReportColumnDefinition] TO [dispatchcrude_iis_acct]
GO

IF EXISTS(SELECT 1 FROM sys.database_principals WHERE [name] = N'dispatchcrude_iis_acct')
	GRANT INSERT ON OBJECT::[dbo].[tblUserReportDefinition] TO [dispatchcrude_iis_acct]
GO

IF EXISTS(SELECT 1 FROM sys.database_principals WHERE [name] = N'dispatchcrude_iis_acct')
	GRANT INSERT ON OBJECT::[dbo].[tblDriverLocation] TO [dispatchcrude_iis_acct]
GO

IF EXISTS(SELECT 1 FROM sys.database_principals WHERE [name] = N'dispatchcrude_iis_acct')
	GRANT SELECT ON OBJECT::[dbo].[viewUserReportColumnDefinition] TO [dispatchcrude_iis_acct]
GO

IF EXISTS(SELECT 1 FROM sys.database_principals WHERE [name] = N'dispatchcrude_iis_acct')
	GRANT SELECT ON OBJECT::[dbo].[tblLocationType] TO [dispatchcrude_iis_acct]
GO

IF EXISTS(SELECT 1 FROM sys.database_principals WHERE [name] = N'dispatchcrude_iis_acct')
	GRANT SELECT ON OBJECT::[dbo].[tblReportDefinition] TO [dispatchcrude_iis_acct]
GO

IF EXISTS(SELECT 1 FROM sys.database_principals WHERE [name] = N'dispatchcrude_iis_acct')
	GRANT DELETE ON OBJECT::[dbo].[tblDriverLocation] TO [dispatchcrude_iis_acct]
GO

IF EXISTS(SELECT 1 FROM sys.database_principals WHERE [name] = N'dispatchcrude_iis_acct')
	GRANT EXECUTE ON OBJECT::[dbo].[spOrderCreationSource] TO [dispatchcrude_iis_acct]
GO

IF EXISTS(SELECT 1 FROM sys.database_principals WHERE [name] = N'dispatchcrude_iis_acct')
	GRANT UPDATE ON OBJECT::[dbo].[tblUserReportDefinition] TO [dispatchcrude_iis_acct]
GO

IF EXISTS(SELECT 1 FROM sys.database_principals WHERE [name] = N'dispatchcrude_iis_acct')
	GRANT UPDATE ON OBJECT::[dbo].[tblUserReportColumnDefinition] TO [dispatchcrude_iis_acct]
GO

IF EXISTS(SELECT 1 FROM sys.database_principals WHERE [name] = N'dispatchcrude_iis_acct')
	GRANT EXECUTE ON OBJECT::[dbo].[spCloneUserReport] TO [dispatchcrude_iis_acct]
GO

IF EXISTS(SELECT 1 FROM sys.database_principals WHERE [name] = N'dispatchcrude_iis_acct')
	GRANT SELECT ON OBJECT::[dbo].[tblReportColumnDefinition] TO [dispatchcrude_iis_acct]
GO

IF EXISTS(SELECT 1 FROM sys.database_principals WHERE [name] = N'dispatchcrude_iis_acct')
	GRANT SELECT ON OBJECT::[dbo].[tblReportFilterOperator] TO [dispatchcrude_iis_acct]
GO

IF EXISTS(SELECT 1 FROM sys.database_principals WHERE [name] = N'dispatchcrude_iis_acct')
	GRANT INSERT ON OBJECT::[dbo].[tblUserReportColumnDefinition] TO [dispatchcrude_iis_acct]
GO

COMMIT TRANSACTION
GO

SET NOEXEC OFF
GO

EXEC _spRefreshAllViews
EXEC _spRefreshAllViews
EXEC _spRefreshAllViews
EXEC _spRecompileAllStoredProcedures
GO
