/*
	-- add additional Report Center - Order report columns
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.1.3'
SELECT  @NewVersion = '3.1.4'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

/***********************************/
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Order records with "translated friendly" values for FK relationships
/***********************************/
ALTER VIEW [dbo].[viewOrder] AS
SELECT O.*
	, OPS.OrderStatus AS PrintStatus
FROM (
	SELECT O.*
	, OrderDate = dbo.fnDateOnly(dbo.fnUTC_to_Local(O.OriginDepartTimeUTC, vO.TimeZoneID, vO.UseDST)) 
	, Origin = vO.Name 
	, OriginFull = vO.FullName 
	, OriginState = vO.State 
	, OriginStateAbbrev = vO.StateAbbrev 
	, OriginStation = vO.Station 
	, vO.LeaseName
	, vO.LeaseNum
	, OriginLegalDescription = vO.LegalDescription 
	, OriginNDIC = vO.NDICFileNum 
	, OriginNDM = vO.NDM 
	, OriginCA = vO.CA 
	, OriginTimeZoneID = vO.TimeZoneID 
	, OriginUseDST = vO.UseDST 
	, OriginStateID = vO.StateID
	, vO.H2S
	, Destination = vD.Name 
	, DestinationFull = vD.FullName 
	, DestinationState = vD.State 
	, DestinationStateAbbrev = vD.StateAbbrev 
	, DestinationTypeID = vD.ID
	, vD.DestinationType
	, DestStation = vD.Station 
	, DestTimeZoneID = vD.TimeZoneID 
	, DestUseDST = vD.UseDST 
	, DestStateID = vD.StateID
	, Customer = C.Name 
	, Carrier = CA.Name 
	, CarrierType = CT.Name 
	, OS.OrderStatus
	, OS.StatusNum
	, Driver = D.FullName 
	, DriverFirst = D.FirstName 
	, DriverLast = D.LastName 
	, Truck = TRU.FullName 
	, Trailer = TR1.FullName 
	, Trailer2 = TR2.FullName 
	, P.PriorityNum
	, TicketType = TT.Name 
	, DestTicketTypeID = vD.TicketTypeID 
	, DestTicketType = vD.TicketType 
	, Operator = OP.Name 
	, Producer = PR.Name 
	, Pumper = PU.FullName 
	, DriverNumber = D.IDNumber
	, CarrierNumber = CA.IDNumber
	, CarrierTypeID = CA.CarrierTypeID
	, TruckNumber = TRU.IDNumber 
	, TrailerNumber = TR1.IDNumber 
	, Trailer2Number = TR2.IDNumber 
	, Product = PRO.Name
	, ProductShort = PRO.ShortName 
	, ProductGroup = isnull(PRO.ProductGroup, PRO.ShortName)
	, OriginUOM = OUom.Name 
	, OriginUomShort = OUom.Abbrev 
	, OriginTankID_Text = CASE WHEN ORT.TankNum = '*' THEN '*' + isnull(O.OriginTankNum, '?') ELSE ORT.TankNum END 
	, DestUOM = DUom.Name 
	, DestUomShort = DUom.Abbrev 
	, Active = cast((CASE WHEN O.DeleteDateUTC IS NULL THEN 1 ELSE 0 END) as bit) 
	, IsDeleted = cast((CASE WHEN O.DeleteDateUTC IS NOT NULL THEN 1 ELSE 0 END) as bit) 
	, PickupPrintStatus = PPS.Name 
	, PickupCompleted = PPS.IsCompleted 
	, DeliverPrintStatus = DPS.Name 
	, DeliverCompleted = DPS.IsCompleted 
	, PrintStatusID = CASE WHEN StatusID = 8 AND PPS.IsCompleted = 0 THEN 7
						   WHEN StatusID = 3 AND DPS.IsCompleted = 0 THEN 8
						ELSE StatusID END 
	, OriginTankText = CASE WHEN O.OriginTankID IS NULL OR ORT.TankNum = '*' THEN O.OriginTankNum ELSE ORT.TankNum END
	, OriginWaitNum = OWR.Num
	, OriginWaitDesc = OWR.Description
	, OriginWaitNumDesc = OWR.NumDesc
	, DestWaitNum = DWR.Num
	, DestWaitDesc = DWR.Description
	, DestWaitNumDesc = DWR.NumDesc
	, RejectNum = ORR.Num
	, RejectDesc = ORR.Description
	, RejectNumDesc = ORR.NumDesc
	FROM dbo.tblOrder O
	LEFT JOIN dbo.viewOrigin vO ON vO.ID = O.OriginID
	LEFT JOIN dbo.viewDestination vD ON vD.ID = O.DestinationID
	LEFT JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
	JOIN dbo.tblOrderStatus AS OS ON OS.ID = O.StatusID
	LEFT JOIN dbo.tblCarrier CA ON CA.ID = O.CarrierID
	LEFT JOIN dbo.tblCarrierType CT ON CT.ID = CA.CarrierTypeID
	LEFT JOIN dbo.viewDriver D ON D.ID = O.DriverID
	LEFT JOIN dbo.viewTruck TRU ON TRU.ID = O.TruckID
	LEFT JOIN dbo.viewTrailer TR1 ON TR1.ID = O.TrailerID
	LEFT JOIN dbo.viewTrailer TR2 ON TR2.ID = O.Trailer2ID
	LEFT JOIN dbo.tblPriority P ON P.ID = O.PriorityID
	LEFT JOIN dbo.tblTicketType TT ON TT.ID = O.TicketTypeID
	LEFT JOIN dbo.tblOriginTank ORT ON ORT.ID = O.OriginTankID
	LEFT JOIN dbo.tblOperator OP ON OP.ID = O.OperatorID
	LEFT JOIN dbo.viewPumper PU ON PU.ID = O.PumperID
	LEFT JOIN dbo.tblProducer PR ON PR.ID = O.ProducerID
	LEFT JOIN dbo.tblProduct PRO ON PRO.ID = O.ProductID
	LEFT JOIN dbo.tblUom OUom ON OUom.ID = O.OriginUomID
	LEFT JOIN dbo.tblUom DUom ON DUom.ID = O.DestUomID
	LEFT JOIN dbo.tblPrintStatus PPS ON PPS.ID = O.PickupPrintStatusID
	LEFT JOIN dbo.tblPrintStatus DPS ON DPS.ID = O.DeliverPrintStatusID
	LEFT JOIN dbo.viewOriginWaitReason OWR ON OWR.ID = O.OriginWaitReasonID
	LEFT JOIN dbo.viewDestinationWaitReason DWR ON DWR.ID = O.DestWaitReasonID
	LEFT JOIN dbo.viewOrderRejectReason ORR ON ORR.ID = O.RejectReasonID
) O
LEFT JOIN dbo.tblOrderStatus OPS ON OPS.ID = O.PrintStatusID

GO

/***********************************/
-- Date Created: 22 Jan 2013
-- Author: Kevin Alons
-- Purpose: query the tblOrderTicket table and include "friendly" translated values
/***********************************/
ALTER VIEW [dbo].[viewOrderTicket] AS
SELECT OT.*
	, TicketType = TT.Name 
	, OriginTankText = CASE WHEN OT.OriginTankID IS NULL OR ORT.TankNum = '*' THEN OT.TankNum ELSE ORT.TankNum END 
	, IsStrappedTank = cast(CASE WHEN OT.OriginTankID IS NULL OR ORT.TankNum = '*' THEN 0 ELSE 1 END as bit) 
	, Active = cast((CASE WHEN OT.DeleteDateUTC IS NULL THEN 1 ELSE 0 END) as bit) 
	, IsDeleted = cast((CASE WHEN OT.DeleteDateUTC IS NOT NULL THEN 1 ELSE 0 END) as bit) 
	, Gravity60F = dbo.fnCorrectedAPIGravity(ProductObsGravity, ProductObsTemp)
	, RejectNum = OTRR.Num
	, RejectDesc = OTRR.Description
	, RejectNumDesc = OTRR.NumDesc
FROM tblOrderTicket OT
JOIN tblTicketType TT ON TT.ID = OT.TicketTypeID
LEFT JOIN tblOriginTank ORT ON ORT.ID = OT.OriginTankID
LEFT JOIN dbo.viewOrderTicketRejectReason OTRR ON OTRR.ID = OT.RejectReasonID

GO

EXEC _spRefreshAllViews
GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details into a single view
/***********************************/
ALTER VIEW [dbo].[viewOrder_OrderTicket_Full] AS 
	SELECT OE.* 
		, T_TicketType = CASE WHEN OE.TicketCount = 0 THEN OE.TicketType ELSE OT.TicketType END 
		, T_CarrierTicketNum = CASE WHEN OE.TicketCount = 0 THEN OE.CarrierTicketNum ELSE OT.CarrierTicketNum END 
		, T_BOLNum = CASE WHEN OE.TicketCount = 0 THEN OE.OriginBOLNum ELSE OT.BOLNum END 
		, T_TankNum = OT.OriginTankText 
		, T_IsStrappedTank = OT.IsStrappedTank 
		, T_BottomFeet = OT.BottomFeet
		, T_BottomInches = OT.BottomInches
		, T_BottomQ = OT.BottomQ 
		, T_OpeningGaugeFeet = OT.OpeningGaugeFeet 
		, T_OpeningGaugeInch = OT.OpeningGaugeInch 
		, T_OpeningGaugeQ = OT.OpeningGaugeQ 
		, T_ClosingGaugeFeet = OT.ClosingGaugeFeet 
		, T_ClosingGaugeInch = OT.ClosingGaugeInch 
		, T_ClosingGaugeQ = OT.ClosingGaugeQ 
		, T_OpenTotalQ = dbo.fnGaugeQtrInches(OT.OpeningGaugeFeet, OT.OpeningGaugeInch, OT.OpeningGaugeQ) 
		, T_CloseTotalQ = dbo.fnGaugeQtrInches(OT.ClosingGaugeFeet, OT.ClosingGaugeInch, ot.ClosingGaugeQ) 
		, T_OpenReading = ltrim(OT.OpeningGaugeFeet) + 'ft ' + ltrim(OT.OpeningGaugeInch) + 'in ' + ltrim(OT.OpeningGaugeQ) + 'q' 
		, T_CloseReading = ltrim(OT.ClosingGaugeFeet) + 'ft ' + ltrim(OT.ClosingGaugeInch) + 'in ' + ltrim(OT.ClosingGaugeQ) + 'q' 
		, T_CorrectedAPIGravity = round(cast(OT.Gravity60F as decimal(9,4)), 9, 4) 
		, T_GrossStdUnits = OT.GrossStdUnits
		, T_SealOff = ltrim(OT.SealOff) 
		, T_SealOn = ltrim(OT.SealOn) 
		, T_HighTemp = OT.ProductHighTemp
		, T_LowTemp = OT.ProductLowTemp
		, T_ProductObsTemp = OT.ProductObsTemp 
		, T_ProductObsGravity = OT.ProductObsGravity 
		, T_ProductBSW = OT.ProductBSW 
		, T_Rejected = OT.Rejected 
		, T_RejectReasonID = OT.RejectReasonID
		, T_RejectNum = OT.RejectNum
		, T_RejectDesc = OT.RejectDesc
		, T_RejectNumDesc = OT.RejectNumDesc
		, T_RejectNotes = OT.RejectNotes 
		, T_GrossUnits = OT.GrossUnits 
		, T_NetUnits = OT.NetUnits 
		, PreviousDestinations = dbo.fnRerouteDetails(OE.ID, 'PreviousDestinations', '<br/>') 
		, RerouteUsers = dbo.fnRerouteDetails(OE.ID, 'RerouteUsers', '<br/>') 
		, RerouteDates = dbo.fnRerouteDetails(OE.ID, 'RerouteDates', '<br/>') 
		, RerouteNotes = dbo.fnRerouteDetails(OE.ID, 'RerouteNotes', '<br/>') 
	FROM dbo.viewOrderExportFull OE
	LEFT JOIN viewOrderTicket OT ON OT.OrderID = OE.ID AND OT.DeleteDateUTC IS NULL
	WHERE OE.DeleteDateUTC IS NULL

GO

EXEC _spRebuildAllObjects
GO

UPDATE tblReportColumnDefinition SET DataField = 'OriginWaitNumDesc', Caption = 'Origin Wait Num+Desc' WHERE ID = 114
GO
UPDATE tblReportColumnDefinition SET DataField = 'DestWaitNumDesc', Caption = 'Dest Wait Num+Desc' WHERE ID = 115
GO
UPDATE tblReportColumnDefinition SET DataField = 'RejectNumDesc', Caption = 'Order Reject Num+Desc' WHERE ID = 116
GO

INSERT tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport) 
	VALUES (117, 1, N'OriginWaitNum', N'Origin Wait Num', NULL, 'OriginWaitReasonID', 2, 'SELECT ID, Name=Num FROM viewOriginWaitReason WHERE DeleteDateUTC IS NULL ORDER BY Num', 0, '*', 1)
INSERT tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport) 
	VALUES (118, 1, N'DestWaitNum', N'Dest Wait Num', NULL, 'DestWaitReasonID', 2, 'SELECT ID, Name=Num FROM viewDestinationWaitReason WHERE DeleteDateUTC IS NULL ORDER BY Num', 0, '*', 1)
INSERT tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport) 
	VALUES (119, 1, N'RejectNum', N'Order Reject Num', NULL, 'RejectReasonID', 2, 'SELECT ID, Name=Num FROM viewOrderRejectReason WHERE DeleteDateUTC IS NULL ORDER BY Num', 0, '*', 1)
GO

INSERT tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport) 
	VALUES (120, 1, N'OriginWaitDesc', N'Origin Wait Desc', NULL, 'OriginWaitReasonID', 2, 'SELECT ID, Name=Description FROM viewOriginWaitReason WHERE DeleteDateUTC IS NULL ORDER BY Description', 0, '*', 1)
INSERT tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport) 
	VALUES (121, 1, N'DestWaitDesc', N'Dest Wait Desc', NULL, 'DestWaitReasonID', 2, 'SELECT ID, Name=Description FROM viewDestinationWaitReason WHERE DeleteDateUTC IS NULL ORDER BY Description', 0, '*', 1)
INSERT tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport) 
	VALUES (122, 1, N'RejectDesc', N'Order Reject Desc', NULL, 'RejectReasonID', 2, 'SELECT ID, Name=Description FROM viewOrderRejectReason WHERE DeleteDateUTC IS NULL ORDER BY Description', 0, '*', 1)
GO

INSERT tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport) 
	VALUES (123, 1, N'T_HighTemp', N'Ticket Open Temp', NULL, NULL, 4, NULL, 0, '*', 0)
INSERT tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport) 
	VALUES (124, 1, N'T_LowTemp', N'Ticket Close Temp', NULL, NULL, 4, NULL, 0, '*', 0)
GO

INSERT tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport) 
	VALUES (125, 1, N'T_RejectNum', N'Ticket Reject Num', NULL, 'T_RejectReasonID', 2, 'SELECT ID, Name=Num FROM viewOrderTicketRejectReason WHERE DeleteDateUTC IS NULL ORDER BY Num', 0, '*', 1)
INSERT tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport) 
	VALUES (126, 1, N'T_RejectDesc', N'Ticket Reject Desc', NULL, 'T_RejectReasonID', 2, 'SELECT ID, Name=Description FROM viewOrderTicketRejectReason WHERE DeleteDateUTC IS NULL ORDER BY Description', 0, '*', 1)
INSERT tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport) 
	VALUES (127, 1, N'T_RejectNumDesc', N'Ticket Reject Num+Desc', NULL, 'T_RejectReasonID', 2, 'SELECT ID, Name=NumDesc FROM viewOrderTicketRejectReason WHERE DeleteDateUTC IS NULL ORDER BY NumDesc', 0, '*', 1)
GO

COMMIT
SET NOEXEC OFF