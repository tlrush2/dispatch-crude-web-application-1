-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.2.10'
SELECT  @NewVersion = '3.2.11'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Revisions to Audit Error rules (Origin Units validation)'
	UNION SELECT @NewVersion, 0, 'Add additional OrderTicket trigger validation to prevent changes when owning Order is Delivered|Audited'
	UNION SELECT @NewVersion, 1, 'Add OrderTicket DbAudit functionality'
	UNION SELECT @NewVersion, 0, 'Add TABLE tblOrderTicketDbAudit'
	UNION SELECT @NewVersion, 0, 'Add PROCEDURE spGetOrderTicketChanges'
GO

/**********************************************************
-- Author: Kevin Alons
-- Date Created: 8 Oct 2014
-- Purpose: return the Audit table records (optionally filtered to just the specified driver and/or ordernum)
***********************************************************/
ALTER FUNCTION [dbo].[fnOrders_AllTickets_Audit](@carrierID int, @driverID int, @orderNum int, @id int) RETURNS TABLE AS RETURN
SELECT *
	, OriginDistanceText = CASE WHEN OriginGpsArrived = 1 THEN 'Verified' ELSE isnull(ltrim(OriginDistance), 'N/A') END
	, DestDistanceText = CASE WHEN DestGpsArrived = 1 THEN 'Verified' ELSE isnull(ltrim(DestDistance), 'N/A') END
	, HasError = cast(CASE WHEN Errors IS NULL THEN 0 ELSE 1 END as bit)
FROM (
	SELECT O.* 
		, OriginGpsLatLon = ltrim(DLO.Lat) + ',' + ltrim(DLO.Lon)
		, OriginLatLon = ltrim(OO.LAT) + ',' + ltrim(OO.LON)
		, OriginAccuracyMeters = DLO.SourceAccuracyMeters
		, OriginDistance = DLO.DistanceToPoint
		, OriginGpsArrived = CASE WHEN DLO.DistanceToPoint IS NOT NULL AND DLO.DistanceToPoint <= OO.GeoFenceRadiusMeters THEN 1 ELSE 0 END
		, DestGpsLatLon = ltrim(DLD.Lat) + ',' + ltrim(DLD.Lon)
		, DestLatLon = ltrim(D.LAT) + ',' + ltrim(D.LON)
		, DestAccuracyMeters = DLD.SourceAccuracyMeters
		, DestDistance = DLD.DistanceToPoint
		, DestGpsArrived = CASE WHEN DLD.DistanceToPoint IS NOT NULL AND DLD.DistanceToPoint <= OO.GeoFenceRadiusMeters THEN 1 ELSE 0 END
		, OriginGrossBBLS = dbo.fnConvertUOM(O.OriginGrossUnits, O.OriginUomID, 1)
		, Errors = 
			nullif(
				SUBSTRING(
					CASE WHEN O.OriginArriveTimeUTC IS NULL THEN ',Missing Pickup Arrival' ELSE '' END
				  + CASE WHEN O.OriginDepartTimeUTC IS NULL THEN ',Missing Pickup Departure' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND O.DestArriveTimeUTC IS NULL THEN ',Missing Delivery Arrival' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND O.DestDepartTimeUTC IS NULL THEN ',Missing Delivery Departure' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND O.OriginGrossUnits = 0 AND O.OriginNetUnits = 0 THEN 'No Origin Units are entered' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND dbo.fnConvertUOM(O.OriginGrossUnits, O.OriginUomID, 1) NOT BETWEEN 0 AND 320 THEN ',Origin GOV Units out of limits' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND dbo.fnConvertUOM(O.OriginGrossStdUnits, O.OriginUomID, 1) NOT BETWEEN 0 AND 320 THEN ',Origin GSV Units out of limits' ELSE '' END
				  + CASE WHEN O.Rejected = 0 AND dbo.fnConvertUOM(O.OriginNetUnits, O.OriginUomID, 1) NOT BETWEEN 0 AND 320 THEN ',Origin NSV Units out of limits' ELSE '' END
				  + CASE WHEN C.RequireDispatchConfirmation = 1 AND nullif(rtrim(O.DispatchConfirmNum), '') IS NULL THEN ',Missing Dispatch Confirm #' ELSE '' END
				  + CASE WHEN O.TruckID IS NULL THEN ',Truck Missing' ELSE '' END
				  + CASE WHEN O.TrailerID IS NULL THEN ',Trailer 1 Missing' ELSE '' END
				, 2, 8000) 
			, '')
		, IsEditable = cast(CASE WHEN O.StatusID = 3 THEN 1 ELSE 0 END as bit)
	FROM viewOrder_AllTickets O
	JOIN tblOrigin OO ON OO.ID = O.OriginID
	JOIN tblCustomer C ON C.ID = OO.CustomerID
	JOIN tblDestination D ON D.ID = O.DestinationID
	LEFT JOIN tblOrderInvoiceCustomer IOC ON IOC.OrderID = O.ID
	LEFT JOIN viewDriverLocation_OriginFirstArrive DLO ON DLO.OrderID = O.ID AND DLO.OriginID = O.OriginID
	LEFT JOIN viewDriverLocation_DestinationFirstArrive DLD ON DLD.OrderID = O.ID AND DLD.DestinationID = O.DestinationID
	WHERE IOC.BatchID IS NULL /* don't even show SETTLED orders on the AUDIT page ever */
	  AND (isnull(@carrierID, 0) = -1 OR O.CarrierID = @carrierID)
	  AND (isnull(@driverID, 0) = -1 OR O.DriverID = @driverID)
	  AND ((nullif(@orderNum, 0) IS NULL AND O.DeleteDateUTC IS NULL AND (O.StatusID = 3 AND DeliverPrintStatusID IN (SELECT ID FROM tblPrintStatus WHERE IsCompleted = 1))) OR O.OrderNum = @orderNum)
	  AND (nullif(@id, 0) IS NULL OR O.ID LIKE @id)
) X

GO

CREATE TABLE [dbo].[tblOrderTicketDbAudit](
	dbauditdate datetime not null constraint DF_tblOrderTicketDbAudit_auditdate DEFAULT (getdate()),
	[ID] [int] NOT NULL,
	[OrderID] [int] NOT NULL,
	[CarrierTicketNum] [varchar](15) NOT NULL,
	[TicketTypeID] [int] NOT NULL,
	[TankNum] [varchar](20) NULL,
	[ProductObsGravity] [decimal](9, 3) NULL,
	[ProductObsTemp] [decimal](9, 3) NULL,
	[ProductBSW] [decimal](9, 3) NULL,
	[OpeningGaugeFeet] [tinyint] NULL,
	[OpeningGaugeInch] [tinyint] NULL,
	[OpeningGaugeQ] [tinyint] NULL,
	[ClosingGaugeFeet] [tinyint] NULL,
	[ClosingGaugeInch] [tinyint] NULL,
	[ClosingGaugeQ] [tinyint] NULL,
	[GrossUnits] [decimal](9, 3) NULL,
	[NetUnits] [decimal](9, 3) NULL,
	[Rejected] [bit] NOT NULL,
	[RejectNotes] [varchar](255) NULL,
	[SealOff] [varchar](10) NULL,
	[SealOn] [varchar](10) NULL,
	[BOLNum] [varchar](15) NULL,
	[ProductHighTemp] [decimal](9, 3) NULL,
	[ProductLowTemp] [decimal](9, 3) NULL,
	[CreateDateUTC] [smalldatetime] NULL,
	[CreatedByUser] [varchar](100) NULL,
	[LastChangeDateUTC] [smalldatetime] NULL,
	[LastChangedByUser] [varchar](100) NULL,
	[DeleteDateUTC] [smalldatetime] NULL,
	[DeletedByUser] [varchar](100) NULL,
	[UID] [uniqueidentifier] NULL,
	[FromMobileApp] [bit] NOT NULL,
	[OriginTankID] [int] NULL,
	[GrossStdUnits] [decimal](18, 6) NULL,
	[BottomFeet] [tinyint] NULL,
	[BottomInches] [tinyint] NULL,
	[BottomQ] [tinyint] NULL,
	[RejectReasonID] [int] NULL,
	[MeterFactor] [varchar](15) NULL,
	[OpenMeterUnits] [decimal](18, 3) NULL,
	[CloseMeterUnits] [decimal](18, 3) NULL,
) ON [PRIMARY]

GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 6 Feb 2013
-- Description:	trigger to ensure the GrossBarrels and NetBarrels are computed for valid, entered data
-- =============================================
ALTER TRIGGER [dbo].[trigOrderTicket_IU] ON [dbo].[tblOrderTicket] AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	-- Note: the TRIGGER_NESTLEVEL( OBJECT_ID('trigOrderTicket_IU') ) = 1 statement prevents recursion
	IF (TRIGGER_NESTLEVEL( OBJECT_ID('trigOrderTicket_IU')) = 1) BEGIN
		
		-- only do anything if something actually changed
		IF EXISTS (SELECT * FROM inserted EXCEPT SELECT * FROM deleted)
		BEGIN
			PRINT 'trigOrderTicket_IU FIRED'
			
			/**********  START OF VALIDATION SECTION ************************/
			DECLARE @errorString varchar(255)

			IF EXISTS (
				SELECT * 
				FROM tblOrder 
				WHERE ID IN (SELECT OrderID FROM inserted) 
				  -- prevent changes to any tickets belonging to a Delivered or Audited Order
				  AND StatusID IN (3, 4))
			BEGIN
				SET @errorString = 'Tickets of Delivered or Audited Orders cannot be modified'
			END

			-- prevent reassigning a ticket from one order to another
			IF EXISTS (
				SELECT i.* 
				FROM inserted i
				JOIN deleted d ON i.ID = d.ID
				WHERE i.OrderID <> d.OrderID)
			BEGIN
				SET @errorString = 'Tickets cannot be moved to a different Order'
			END
			
			IF (SELECT COUNT(*) FROM (
					SELECT OT.OrderID
					FROM tblOrderTicket OT
					JOIN inserted i ON i.OrderID = OT.OrderID AND i.CarrierTicketNum = OT.CarrierTicketNum
					WHERE OT.DeleteDateUTC IS NULL
					GROUP BY OT.OrderID, OT.CarrierTicketNum
					HAVING COUNT(*) > 1
				) v) > 0
			BEGIN
				SET @errorString = 'Duplicate Ticket Numbers are not allowed'
			END
			
			ELSE IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (3) AND BOLNum IS NULL AND DeleteDateUTC IS NULL) > 0
			BEGIN
				SET @errorString = 'BOL # value is required for Meter Run tickets'
			END
			
			ELSE IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1, 2) AND OriginTankID IS NULL AND TankNum IS NULL AND DeleteDateUTC IS NULL) > 0
			BEGIN
				SET @errorString = 'Tank ID value is required for Gauge Run & Net Volume tickets'
			END

			ELSE IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1) AND CarrierTicketNum IS NULL AND DeleteDateUTC IS NULL) > 0
			BEGIN
				SET @errorString = 'Ticket # value is required for Gauge Run tickets'
			END

			ELSE IF (SELECT COUNT(*) FROM inserted WHERE DeleteDateUTC IS NULL
					AND (TicketTypeID IN (1, 2) AND (ProductObsTemp IS NULL OR ProductObsGravity IS NULL OR ProductBSW IS NULL))
						OR (TicketTypeID IN (1) AND (ProductHighTemp IS NULL OR ProductLowTemp IS NULL))
				) > 0
			BEGIN
				SET @errorString = 'All Product Measurement values are required for Gauge Run & Net Volume tickets'
			END
			
			ELSE IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1) AND Rejected = 0 AND DeleteDateUTC IS NULL
				AND (OpeningGaugeFeet IS NULL OR OpeningGaugeInch IS NULL OR OpeningGaugeQ IS NULL)) > 0
			BEGIN
				SET @errorString = 'All Opening Gauge values are required for Gauge Run tickets'
			END
			ELSE IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1) AND Rejected = 0 AND DeleteDateUTC IS NULL
				AND (ClosingGaugeFeet IS NULL OR ClosingGaugeInch IS NULL OR ClosingGaugeQ IS NULL)) > 0
			BEGIN
				SET @errorString = 'All Closing Gauge values are required for Gauge Run tickets'
			END

			ELSE IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (2) AND Rejected = 0 AND DeleteDateUTC IS NULL
				AND (GrossUnits IS NULL)) > 0
			BEGIN
				SET @errorString = 'Gross Volume value is required for Net Volume tickets'
			END

			ELSE IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (3) AND Rejected = 0 AND DeleteDateUTC IS NULL
				AND (GrossUnits IS NULL OR NetUnits IS NULL)) > 0
			BEGIN
				SET @errorString = 'Gross & Net Volume values are required for Meter Run tickets'
				IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
				RETURN
			END

			ELSE IF (SELECT COUNT(*) FROM inserted WHERE TicketTypeID IN (1) AND Rejected = 0 AND DeleteDateUTC IS NULL
				AND (SealOff IS NULL OR SealOn IS NULL)) > 0
			BEGIN
				SET @errorString = 'All Seal Off & Seal On values are required for Gauge Run tickets'
			END

			IF (@errorString IS NOT NULL)
			BEGIN
				RAISERROR(@errorString, 16, 1)
				IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
			END
			
			/**********  END OF VALIDATION SECTION ************************/
			
			-- re-compute GaugeRun ticket Gross barrels
			IF UPDATE(OriginTankID) OR UPDATE(OpeningGaugeFeet) OR UPDATE(OpeningGaugeInch) OR UPDATE(OpeningGaugeQ)
				OR UPDATE (ClosingGaugeFeet) OR UPDATE(ClosingGaugeInch) OR UPDATE(ClosingGaugeQ) 
				OR UPDATE (GrossUnits)
			BEGIN
				UPDATE tblOrderTicket
				  SET GrossUnits = dbo.fnConvertUom(dbo.fnOriginTankStrappingLevelDeltaBarrels(
						OT.OriginTankID
					  , OpeningGaugeFeet
					  , OpeningGaugeInch
					  , OpeningGaugeQ
					  , ClosingGaugeFeet
					  , ClosingGaugeInch
					  , ClosingGaugeQ), 1, O.OriginUomID) -- ensure this value is always normalized from BARRELS
				FROM tblOrderTicket OT
				JOIN tblOrder O ON O.ID = OT.OrderID
				WHERE OT.ID IN (SELECT ID FROM inserted) 
				  AND OT.TicketTypeID = 1 -- Gauge Run Ticket
				  AND OT.OpeningGaugeFeet IS NOT NULL
				  AND OT.OpeningGaugeInch IS NOT NULL
				  AND OT.OpeningGaugeQ IS NOT NULL
				  AND OT.ClosingGaugeFeet IS NOT NULL
				  AND OT.ClosingGaugeInch IS NOT NULL
				  AND OT.ClosingGaugeQ IS NOT NULL
			END
			-- recompute the GrossUnits of any changed Meter Run tickets
			IF UPDATE(OpenMeterUnits) OR UPDATE(CloseMeterUnits) BEGIN
				--Print 'updating tblOrderTicket GOV from Open/Close Meter Units'
				UPDATE tblOrderTicket 
					SET GrossUnits = CloseMeterUnits - OpenMeterUnits
				WHERE TicketTypeID = 3  -- meter run tickets ONLY
				  AND OpenMeterUnits IS NOT NULL AND CloseMeterUnits IS NOT NULL
				
			END
			-- re-compute GaugeRun | Net Barrel | Meter Run tickets NetUnits
			IF UPDATE(OpeningGaugeFeet) OR UPDATE(OpeningGaugeInch) OR UPDATE(OpeningGaugeQ)
				OR UPDATE (ClosingGaugeFeet) OR UPDATE(ClosingGaugeInch) OR UPDATE(ClosingGaugeQ) 
				OR UPDATE(OriginTankID) OR UPDATE(GrossUnits) OR UPDATE(GrossStdUnits) OR UPDATE(NetUnits)
				OR UPDATE(ProductObsTemp) OR UPDATE(ProductObsGravity) OR UPDATE(ProductBSW)
				OR UPDATE(OpenMeterUnits) OR UPDATE(CloseMeterUnits)
			BEGIN
				--PRINT 'updating tblOrderTicket NSV/GSV'
				UPDATE tblOrderTicket
				  SET NetUnits = dbo.fnCrudeNetCalculator(GrossUnits, ProductObsTemp, ProductObsGravity, isnull(ProductBSW, 0))
					, GrossStdUnits = dbo.fnCrudeNetCalculator(GrossUnits, ProductObsTemp, ProductObsGravity, 0)
				WHERE ID IN (SELECT ID FROM inserted)
				  AND TicketTypeID IN (1,2)
				  AND GrossUnits IS NOT NULL
				  AND ProductObsTemp IS NOT NULL
				  AND ProductObsGravity IS NOT NULL
			END
			
			-- ensure the Order record is in-sync with the Tickets
			--declare @gov decimal(9,3); SELECT TOP 1 @gov = OT.GrossUnits FROM tblOrderTicket OT JOIN inserted i ON i.ID = OT.ID;
			--PRINT 'updating tblOrder GOV/GSV/NSV GOV = ' + ltrim(@gov)
			UPDATE tblOrder 
			SET OriginGrossUnits = (SELECT sum(GrossUnits) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND OT.Rejected = 0 AND OT.DeleteDateUTC IS NULL)
			  , OriginNetUnits = (SELECT sum(NetUnits) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND OT.Rejected = 0 AND OT.DeleteDateUTC IS NULL)
			  , OriginGrossStdUnits = (SELECT sum(GrossStdUnits) FROM tblOrderTicket OT WHERE OT.OrderID = O.ID AND OT.Rejected = 0 AND OT.DeleteDateUTC IS NULL)

				-- use the first MeterRun/BasicRun BOLNum as the Order BOL Num (favor the first un-Rejected)
			  , OriginBOLNum = (
					SELECT TOP 1 BOLNum FROM (
						SELECT TOP 1 BOLNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID NOT IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
						UNION SELECT TOP 1 BOLNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID NOT IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X)
				-- use the first GaugeRun|NetVolume CarrierTicketNum as the Order.CarrierTicketNum (favor first un-Rejected)
			  , CarrierTicketNum = (
					SELECT TOP 1 CarrierTicketNum FROM (
						SELECT TOP 1 CarrierTicketNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
						UNION SELECT TOP 1 CarrierTicketNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X)
				-- use the first GaugeRun|NetVolume OriginTankID for the Order (favor first un-Rejected ticket)
			  , OriginTankID = (
					SELECT TOP 1 OriginTankID FROM (
						SELECT TOP 1 OriginTankID FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
						UNION SELECT TOP 1 OriginTankID FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X)
				-- use the first GaugeRun|NetVolume OriginTankNum for the Order (favor first un-Rejected ticket)
			  , OriginTankNum = (
					SELECT TOP 1 OriginTankNum FROM (
						SELECT TOP 1 OriginTankNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
						UNION SELECT TOP 1 OriginTankNum FROM tblOrderTicket 
						WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X)
			  , LastChangeDateUTC = (SELECT MAX(isnull(LastChangeDateUTC, CreateDateUTC)) FROM inserted WHERE OrderID = O.ID)
			  , LastChangedByUser = (SELECT MIN(isnull(LastChangedByUser, CreatedByUser)) FROM inserted WHERE OrderID = O.ID)
			FROM tblOrder O
			WHERE ID IN (SELECT DISTINCT OrderID FROM inserted)

			/* START DB AUDIT *********************************************************/
			BEGIN TRY
				IF EXISTS(SELECT * FROM tblSetting WHERE ID = 32 AND (Value LIKE 'true' OR Value LIKE 'yes'))
					INSERT INTO tblOrderTicketDbAudit (DBAuditDate, ID, OrderID, CarrierTicketNum, TicketTypeID, TankNum, ProductObsGravity, ProductObsTemp, OpeningGaugeFeet, OpeningGaugeInch, OpeningGaugeQ, ClosingGaugeFeet, ClosingGaugeInch, ClosingGaugeQ, GrossUnits, NetUnits, Rejected, RejectNotes, SealOff, SealOn, BOLNum, ProductHighTemp, ProductLowTemp, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser, UID, FromMobileApp, OriginTankID, GrossStdUnits, BottomFeet, BottomInches, BottomQ, RejectReasonID, MeterFactor, OpenMeterUnits, CloseMeterUnits)
						SELECT GETUTCDATE(), ID, OrderID, CarrierTicketNum, TicketTypeID, TankNum, ProductObsGravity, ProductObsTemp, OpeningGaugeFeet, OpeningGaugeInch, OpeningGaugeQ, ClosingGaugeFeet, ClosingGaugeInch, ClosingGaugeQ, GrossUnits, NetUnits, Rejected, RejectNotes, SealOff, SealOn, BOLNum, ProductHighTemp, ProductLowTemp, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser, UID, FromMobileApp, OriginTankID, GrossStdUnits, BottomFeet, BottomInches, BottomQ, RejectReasonID, MeterFactor, OpenMeterUnits, CloseMeterUnits
						FROM deleted d
			END TRY
			BEGIN CATCH
				PRINT 'trigOrderTicket_IU.DBAUDIT FAILURE: ' + ERROR_MESSAGE()
			END CATCH
			/* END DB AUDIT *********************************************************/

			PRINT 'trigOrderTicket_IU COMPLETE'
		END
	END
	
END



GO
EXEC sp_settriggerorder @triggername=N'[dbo].[trigOrderTicket_IU]', @order=N'First', @stmttype=N'INSERT'
GO
EXEC sp_settriggerorder @triggername=N'[dbo].[trigOrderTicket_IU]', @order=N'First', @stmttype=N'UPDATE'

-- =============================================
-- Author:		Kevin Alons
-- Create date: 11 Sep 2014
-- Description:	compute and return the order changes for a single order (specified by an Order.ID value)
-- =============================================
ALTER PROCEDURE [dbo].[spGetOrderChanges](@id int) 
AS BEGIN
	DECLARE @ret TABLE 
	(
		FieldName varchar(255)
	  , NewValue varchar(max)
	  , OldValue varchar(max)
	  , ChangeDateUTC datetime
	  , ChangedByUser varchar(100)
	  , IsCurrent bit
	)

	DECLARE @columns TABLE (id int, name varchar(100))
	INSERT INTO @columns 		
		SELECT ROW_NUMBER() OVER (ORDER BY COLUMN_NAME), COLUMN_NAME 
		FROM INFORMATION_SCHEMA.COLUMNS 
		WHERE TABLE_NAME = 'tblOrder' AND COLUMN_NAME NOT IN ('ID','LastChangeDateUTC')
	
	SELECT rowID = ROW_NUMBER() OVER (ORDER BY sortnum, dbauditdate desc, RouteID desc), * 
	INTO #history
	FROM (
		SELECT dbauditdate = NULL, *, sortnum = 0 from tblOrder where ID = @id
		UNION ALL SELECT *, sortnum = 1 from tblOrderDbAudit where ID = @id
	) X

	DECLARE @rowID int, @colID int, @colName varchar(100), @newValue varchar(max), @oldValue varchar(max), @sql nvarchar(max), @newColID int
	DECLARE @changeDateUTC datetime, @changeUser varchar(100), @isCurrent bit

	SELECT @rowID = MIN(rowID) FROM #history
	WHILE EXISTS(SELECT * FROM #history WHERE rowID = @rowID + 1) BEGIN
		SELECT TOP 1 @colName = name, @colID = id FROM @columns WHERE id = 1
		WHILE (@colName IS NOT NULL) BEGIN

			-- get the "now" value
			SET @sql = N'SELECT TOP 1 @value=' + @colName + N', @changeDateUTC=LastChangeDateUTC, @changeUser=LastChangedByUser FROM #history WHERE rowID = @rowID'
			EXEC sp_executesql @sql
				, N'@rowID int, @changeDateUTC datetime OUTPUT, @changeUser varchar(100) OUTPUT, @value varchar(max) OUTPUT'
				, @rowID = @rowID, @changeDateUTC = @changeDateUTC OUTPUT, @changeUser = @changeUser OUTPUT, @value = @newValue OUTPUT

			-- get the "from" value
			SET @sql = N'SELECT TOP 1 @value=' + @colName + N' FROM #history WHERE rowID = @rowID + 1'
			EXEC sp_executesql @sql, N'@rowID int, @value varchar(max) OUTPUT', @rowID = @rowID, @value = @oldValue OUTPUT
			
			IF (isnull(@newValue, '\r') <> isnull(@oldValue, '\r')) BEGIN
				INSERT INTO @ret 
					SELECT @colName, @newValue, @oldValue, @changeDateUTC, @changeUser, CASE WHEN @rowID = 1 THEN 1 ELSE 0 END
			END
			
			SET @colName = NULL
			SELECT TOP 1 @colName = name, @colID = id FROM @columns WHERE id > @colID ORDER BY ID
			
		END

		SET @rowID = @rowID + 1
	END
	
	SELECT * FROM @ret

END

GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 11 Sep 2014
-- Description:	compute and return the order changes for a single order (specified by an Order.ID value)
-- =============================================
CREATE PROCEDURE [dbo].[spGetOrderTicketChanges](@id int) 
AS BEGIN
	DECLARE @ret TABLE 
	(
		FieldName varchar(255)
	  , NewValue varchar(max)
	  , OldValue varchar(max)
	  , ChangeDateUTC datetime
	  , ChangedByUser varchar(100)
	  , IsCurrent bit
	)

	DECLARE @columns TABLE (id int, name varchar(100))
	INSERT INTO @columns 		
		SELECT ROW_NUMBER() OVER (ORDER BY COLUMN_NAME), COLUMN_NAME 
		FROM INFORMATION_SCHEMA.COLUMNS 
		WHERE TABLE_NAME = 'tblOrderTicket' AND COLUMN_NAME NOT IN ('ID','LastChangeDateUTC')
	
	SELECT rowID = ROW_NUMBER() OVER (ORDER BY sortnum, dbauditdate desc, CarrierTicketNum), * 
	INTO #history
	FROM (
		SELECT dbauditdate = NULL, *, sortnum = 0 from tblOrderTicket where ID = @id
		UNION ALL SELECT *, sortnum = 1 from tblOrderTicketDbAudit where ID = @id
	) X

	DECLARE @rowID int, @colID int, @colName varchar(100), @newValue varchar(max), @oldValue varchar(max), @sql nvarchar(max), @newColID int
	DECLARE @changeDateUTC datetime, @changeUser varchar(100), @isCurrent bit

	SELECT @rowID = MIN(rowID) FROM #history
	WHILE EXISTS(SELECT * FROM #history WHERE rowID = @rowID + 1) BEGIN
		SELECT TOP 1 @colName = name, @colID = id FROM @columns WHERE id = 1
		WHILE (@colName IS NOT NULL) BEGIN

			-- get the "now" value
			SET @sql = N'SELECT TOP 1 @value=' + @colName + N', @changeDateUTC=LastChangeDateUTC, @changeUser=LastChangedByUser FROM #history WHERE rowID = @rowID'
			EXEC sp_executesql @sql
				, N'@rowID int, @changeDateUTC datetime OUTPUT, @changeUser varchar(100) OUTPUT, @value varchar(max) OUTPUT'
				, @rowID = @rowID, @changeDateUTC = @changeDateUTC OUTPUT, @changeUser = @changeUser OUTPUT, @value = @newValue OUTPUT

			-- get the "from" value
			SET @sql = N'SELECT TOP 1 @value=' + @colName + N' FROM #history WHERE rowID = @rowID + 1'
			EXEC sp_executesql @sql, N'@rowID int, @value varchar(max) OUTPUT', @rowID = @rowID, @value = @oldValue OUTPUT
			
			IF (isnull(@newValue, '\r') <> isnull(@oldValue, '\r')) BEGIN
				INSERT INTO @ret 
					SELECT @colName, @newValue, @oldValue, @changeDateUTC, @changeUser, CASE WHEN @rowID = 1 THEN 1 ELSE 0 END
			END
			
			SET @colName = NULL
			SELECT TOP 1 @colName = name, @colID = id FROM @columns WHERE id > @colID ORDER BY ID
			
		END

		SET @rowID = @rowID + 1
	END
	
	SELECT * FROM @ret

END

GO

/* =============================================
-- Author:		Kevin Alons
-- Create date: 19 Dec 2012
-- Description:	trigger to 
				1) validate any changes, and fail the update if invalid changes are submitted
				2) add a unique, incrementing OrderNum to each new Order (manual Identity column)
				3) recompute wait times (origin|destination based on times provided)
				4) generate route table entry for any newly used origin-destination combination
				5) keep Order consistent with Origin.Producer|Operator|UOM & Dest.UOM then the Origin|Dest change
				6) roll the PrintStatus back to NOTFINALIZED (0) when status is rolled back
				7) when DriverID changes, mark those orders AS DELETED and clone the order with the new DriverID and associated records
				8) update any ticket quantities for open orders when the UOM is changed for the Origin
				disabled 9) ensure the LastChangeDateUTC is updated for any change to the order
				10) update the Pickup/DeliverLastChangeDateUTC when Origin/Destination is changed (respectively)
				11) if DBAudit is turned on, save an audit record for this Order change
-- =============================================*/
ALTER TRIGGER [dbo].[trigOrder_IU] ON [dbo].[tblOrder] AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @changesFound bit
	
		-- Note: the TRIGGER_NESTLEVEL( OBJECT_ID('trigOrder_IU') ) = 1 statement prevents recursion
	IF (TRIGGER_NESTLEVEL( OBJECT_ID('trigOrder_IU')) = 1 AND TRIGGER_NESTLEVEL( OBJECT_ID('trigOrderTicket_IU')) = 0) BEGIN

		/**********  START OF VALIDATION SECTION ************************/

		IF (UPDATE(OrderNum) 
			OR UPDATE(PriorityID) 
			OR UPDATE(DueDate) 
			OR UPDATE(RouteID) 
			OR UPDATE(OriginID) 
			OR UPDATE(OriginArriveTimeUTC) 
			OR UPDATE(OriginDepartTimeUTC) 
			OR UPDATE(OriginMinutes) 
			OR UPDATE(OriginWaitNotes) 
			OR UPDATE(OriginBOLNum) 
			OR UPDATE(OriginGrossUnits) 
			OR UPDATE(OriginNetUnits) 
			OR UPDATE(DestinationID) 
			OR UPDATE(DestArriveTimeUTC) 
			OR UPDATE(DestDepartTimeUTC) 
			OR UPDATE(DestMinutes) 
			OR UPDATE(DestWaitNotes) 
			OR UPDATE(DestBOLNum) 
			OR UPDATE(DestGrossUnits) 
			OR UPDATE(DestNetUnits) 
			OR UPDATE(CustomerID) 
			OR UPDATE(CarrierID) 
			OR UPDATE(DriverID) 
			OR UPDATE(TruckID)
			OR UPDATE(TrailerID) 
			OR UPDATE(Trailer2ID) 
			OR UPDATE(OperatorID) 
			OR UPDATE(PumperID) 
			OR UPDATE(TicketTypeID) 
			OR UPDATE(Rejected) 
			OR UPDATE(RejectNotes) 
			OR UPDATE(ChainUp) 
			OR UPDATE(OriginTruckMileage) 
			OR UPDATE(OriginTankNum) 
			OR UPDATE(DestTruckMileage) 
			OR UPDATE(CarrierTicketNum) 
			OR UPDATE(AuditNotes) 
			OR UPDATE(CreateDateUTC) 
			-- it is permissible to change the ActualMiles value on Audited orders up to the point the Order has been Shipper-Settled
			OR (UPDATE(ActualMiles) AND EXISTS (SELECT * FROM deleted i JOIN tblOrderInvoiceCustomer OIC ON OIC.OrderID = i.ID WHERE OIC.BatchID IS NOT NULL))
			OR UPDATE(ProducerID) 
			OR UPDATE(CreatedByUser) 
			OR UPDATE(LastChangeDateUTC) 
			OR UPDATE(LastChangedByUser) 
			OR UPDATE(DeleteDateUTC) 
			OR UPDATE(DeletedByUser) 
			OR UPDATE(DestProductBSW)
			OR UPDATE(DestProductGravity) 
			OR UPDATE(DestProductTemp) 
			OR UPDATE(ProductID) 
			OR UPDATE(AcceptLastChangeDateUTC) 
			OR UPDATE(PickupLastChangeDateUTC) 
			OR UPDATE(DeliverLastChangeDateUTC) 
			OR UPDATE(OriginUomID) 
			OR UPDATE(DestUomID) 
			OR UPDATE(PickupPrintStatusID) 
			OR UPDATE(DeliverPrintStatusID)
			OR UPDATE(PickupPrintDateUTC) 
			OR UPDATE(DeliverPrintDateUTC) 
			OR UPDATE(OriginTankID) 
			OR UPDATE(OriginGrossStdUnits) 
			OR UPDATE(DispatchConfirmNum) 
			OR UPDATE(DispatchNotes)
			OR UPDATE(DriverNotes)
			OR UPDATE(OriginWaitReasonID)
			OR UPDATE(DestWaitReasonID)
			OR UPDATE(RejectReasonID) 
			OR UPDATE(DestOpenMeterUnits) 
			OR UPDATE(DestCloseMeterUnits))
			--OR UPDATE(ReassignKey)) we don't do any work if this value changes so just ignore it
		BEGIN
			-- only allow the StatusID value to be changed on an audited order
			IF EXISTS (SELECT * FROM deleted WHERE StatusID = 4)
			BEGIN
				-- if the "Allow Audited Order Update (temp only)" setting is TRUE then only warn when this occurs
				IF (dbo.fnToBool(dbo.fnSettingValue(35)) = 1)
					PRINT 'AUDITED orders is being modified - please investigate why!'
				ELSE BEGIN
					-- otherwise (normal behavior) - prevent AUDITED orders from being modified (except to UN-AUDIT them)
					RAISERROR('AUDITED orders cannot be modified!', 16, 1)
					IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
					RETURN
				END
			END
		END
		ELSE -- NO CHANGES DETECTED SO JUST QUIT
			RETURN

		PRINT 'trigOrder_IU FIRED'
					
		-- ensure the Origin and Destinations are both specified unless the Status is:
		--   (Generated, Assigned, Dispatched or Declined)
		IF  EXISTS(SELECT * FROM inserted O WHERE (O.OriginID IS NULL OR O.DestinationID IS NULL) AND O.StatusID NOT IN (-10,1,2,9))
		BEGIN
			RAISERROR('Invalid Order Status when the Origin and/or Or Destination is not specified', 16, 1)
			IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
			RETURN
		END

		-- prevent advancing past Accepted status without at least 1 undeleted, un-rejected ticket (unless the ORDER is REJECTED)
		IF  EXISTS(
			SELECT * 
			FROM inserted O 
			JOIN deleted D ON D.ID = O.ID
			LEFT JOIN tblOrderTicket OT ON OT.OrderID = O.ID AND OT.DeleteDateUTC IS NULL AND OT.Rejected = 0
			WHERE O.Rejected = 0 -- not rejected
			  AND D.StatusID NOT IN (8,3,4) -- only check when advancing (not retreating since that would prevent fixes by the website/backoffice)
			  AND O.StatusID IN (8,3,4) -- statusID IN ( PICKED UP, DELIVERED, AUDITED )
			  AND OT.ID IS NULL -- no ticket was found
		)
		BEGIN
			RAISERROR('Cannot advance Order to PICKED UP without at least 1 un-deleted, un-rejected ticket', 16, 1)
			IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
			RETURN
		END
		
		/**********  END OF VALIDATION SECTION ************************/

		WHILE (SELECT count(*) FROM inserted i JOIN tblOrder O ON O.ID = i.ID WHERE O.OrderNum IS NULL) > 0
		BEGIN
			UPDATE tblOrder 
			  SET OrderNum = isnull((SELECT max(OrderNum) FROM tblOrder), 100000) + 1
				, CreateDateUTC = getutcdate()
			WHERE ID = (SELECT min(O.ID) FROM tblOrder O JOIN inserted i ON i.ID = O.ID WHERE O.OrderNum IS NULL)
		END
		
		-- re-compute the OriginMinutes (in case the website failed to compute it properly)
		IF UPDATE(OriginArriveTimeUTC) OR UPDATE(OriginDepartTimeUTC)
		BEGIN
			UPDATE tblOrder SET OriginMinutes = datediff(minute, i.OriginArriveTimeUTC, i.OriginDepartTimeUTC)
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
		END
		-- re-compute the DestWaitMinutes (in case the website failed to compute it properly)
		IF UPDATE(DestArriveTimeUTC) OR UPDATE(DestDepartTimeUTC)
		BEGIN
			UPDATE tblOrder SET DestMinutes = datediff(minute, i.DestArriveTimeUTC, i.DestDepartTimeUTC)
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
		END
		
		-- ensure missing Routes are created and assigned for the specified Origin/Destination combination
		IF UPDATE(OriginID) OR UPDATE(DestinationID)
		BEGIN
			-- create any missing Route records
			INSERT INTO tblRoute (OriginID, DestinationID, CreateDateUTC, CreatedByUser)
				SELECT i.OriginID, i.DestinationID, GETUTCDATE(), ISNULL(i.LastChangedByUser, i.CreatedByUser)
				FROM inserted i
				LEFT JOIN tblRoute R ON R.OriginID = i.OriginID AND R.DestinationID = i.DestinationID
				WHERE i.OriginID IS NOT NULL AND i.DestinationID IS NOT NULL AND R.ID IS NULL
			-- ensure the Order records refer to the correct Route (ID)
			UPDATE tblOrder SET RouteID = R.ID
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
			LEFT JOIN tblRoute R ON R.OriginID = O.OriginID AND R.DestinationID = O.DestinationID

			-- update the ActualMiles from the related Route
			UPDATE tblOrder SET ActualMiles = R.ActualMiles
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
			JOIN tblRoute R ON R.ID = O.RouteID
		END
		
		IF (UPDATE(OriginID))
		BEGIN
			-- update Order.ProducerID to match what is assigned to the new Origin
			UPDATE tblOrder 
				SET ProducerID = OO.ProducerID
					-- update this stage specific date field so the mobile app knows the pickup has been modified
					, PickupLastChangeDateUTC = GETUTCDATE() -- mobile app never changes this so just use getutcdate()
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			WHERE d.OriginID <> O.OriginID

			-- update Order.OriginUomID to match what is assigned to the new Origin (quantities are updated below)
			UPDATE tblOrder SET OriginUomID = OO.UomID
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID
			JOIN tblOrigin OO ON OO.ID = O.OriginID
			WHERE d.OriginID <> O.OriginID AND d.OriginUomID <> OO.UomID
		END
		
		-- keep the DestUomID in sync with the Destination (units are updated below)
		IF (UPDATE(DestinationID))
		BEGIN
			-- update Order.DestUomID to match what is assigned to the new Destination
			UPDATE tblOrder 
			  SET DestUomID = DD.UomID
				-- updating this will ensure the mobile app refreshes itself with this revision (it doesn't use LastChangeDateUTC but rather the stage specific date value)
				, DeliverLastChangeDateUTC = GETUTCDATE() -- this will never be called by the mobile app so just use getutcdate()
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID
			JOIN tblDestination DD ON DD.ID = O.DestinationID
			WHERE d.DestinationID <> O.DestinationID AND d.DestUomID <> DD.UomID
		END
		
		-- ensure that any set PrintStatusID values are reset to 0 when backing up the general StatusID value
		IF (UPDATE(StatusID))
		BEGIN
			UPDATE tblOrder 
			  SET DeliverPrintStatusID = 0 
			  FROM tblOrder O
			  JOIN deleted d ON d.ID = O.ID
			WHERE O.StatusID <> d.StatusID AND O.StatusID IN (-10, 1, 2, 7, 8) -- Generated, Assigned, Dispatched, Accepted, Picked-Up

			UPDATE tblOrder 
			  SET PickupPrintStatusID = 0 
			  FROM tblOrder O
			  JOIN deleted d ON d.ID = O.ID
			WHERE O.StatusID <> d.StatusID AND O.StatusID IN (-10, 1, 2, 7) -- Generated, Assigned, Dispatched, Accepted
		END

		-- just updating the tickets here, trigOrderTicket_IU will in turn update this table
		-- ensure any existing OrderTickets are recomputed with the new OriginUOM
		UPDATE tblOrderTicket
		  SET GrossUnits = dbo.fnConvertUOM(GrossUnits, d.OriginUomID, O.OriginUomID)
			, NetUnits = dbo.fnConvertUOM(NetUnits, d.OriginUomID, O.OriginUomID)
		FROM tblOrderTicket OT
		JOIN tblOrder O ON O.ID = OT.OrderID
		JOIN deleted d ON d.ID = O.ID
		WHERE O.OriginUomID <> d.OriginUomID
		
		-- recalculate the Destination Unit values if only if the DestUomID changed but the values didn't
		UPDATE tblOrder
		  SET DestGrossUnits = dbo.fnConvertUOM(O.DestGrossUnits, d.DestUomID, O.DestUomID)
			, DestNetUnits = dbo.fnConvertUOM(O.DestNetUnits, d.DestUomID, O.DestUomID)
		FROM tblOrder O
		JOIN deleted d ON d.ID = O.ID
		WHERE d.DestUomID <> O.DestUomID 
		  AND d.DestGrossUnits = O.DestGrossUnits
		  AND d.DestNetUnits = O.DestNetUnits

		/*************************************************************************************************************/
		/* handle driver changes - "orphan" the order/ticket (with a new ORDERNUM) - and clone it for the new driver */
		IF (UPDATE(DriverID))
		BEGIN
			-- create cloned order/ticket records (these records will be cloned then the original reset to orphaned status)
			SELECT O.*, NewOrderNum = CAST(NULL as int)
			INTO #clone
			FROM tblOrder O
			JOIN deleted d ON d.ID = O.ID AND d.DriverID IS NOT NULL AND d.DriverID <> isnull(O.DriverID, 0)

			/* generate the next OrderNum values for these new records */
			DECLARE @newOrderNum int; SET @newOrderNum = isnull((SELECT max(OrderNum) FROM tblOrder), 100000) + 1;
			DECLARE @newReassignKey int; SET @newReassignKey = isnull((SELECT max(ReassignKey) FROM tblOrder), 0) + 1;
			WHILE (SELECT count(*) FROM #clone WHERE NewOrderNum IS NULL) > 0
			BEGIN
				UPDATE #clone
				  SET NewOrderNum = @newOrderNum, ReassignKey = isnull(ReassignKey, @newReassignKey)
					, CreateDateUTC = getutcdate()
				WHERE ID = (SELECT min(ID) FROM #clone WHERE NewOrderNum IS NULL)
				-- if the @newReassignKey wasn't used above, then that # will be skipped, but this should not be an issue
				SELECT @newOrderNum = @newOrderNum + 1, @newReassignKey = @newReassignKey + 1
			END
			
			-- reset the existing records as orphaned
			UPDATE tblOrder
				SET DeleteDateUTC = isnull(O.DeleteDateUTC, GETUTCDATE()), DeletedByUser = isnull(O.DeletedByUser, O.LastChangedByUser)
					, CarrierID = d.CarrierID, DriverID = d.DriverID, TruckID = d.TruckID, TrailerID = d.TrailerID, Trailer2ID = d.Trailer2ID
					, DispatchConfirmNum = NULL
					, ReassignKey = C.ReassignKey
			FROM tblOrder O
			JOIN #clone C ON C.ID = O.ID
			JOIN deleted d on D.ID = O.id

			-- create the cloned records (which is essentially the original with a new ID and the Carrrier & default Truck|Trailers of the new driver)
			INSERT INTO tblOrder (OrderNum, [StatusID], [PriorityID], [DueDate], [RouteID], [OriginID], [OriginArriveTimeUTC], [OriginDepartTimeUTC], [OriginMinutes], [OriginWaitNotes], [OriginBOLNum], [OriginGrossUnits], [OriginNetUnits], [DestinationID], [DestArriveTimeUTC], [DestDepartTimeUTC], [DestMinutes], [DestWaitNotes], [DestBOLNum], [DestGrossUnits], [DestNetUnits], [CustomerID], [CarrierID], [DriverID], [TruckID], [TrailerID], [Trailer2ID], [OperatorID], [PumperID], [TicketTypeID], [Rejected], [RejectNotes], [ChainUp], [OriginTruckMileage], [OriginTankNum], [DestTruckMileage], [CarrierTicketNum], [AuditNotes], [CreateDateUTC], [ActualMiles], [ProducerID], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser], [DestProductBSW], [DestProductGravity], [DestProductTemp], [ProductID], [AcceptLastChangeDateUTC], [PickupLastChangeDateUTC], [DeliverLastChangeDateUTC], [OriginUomID], [DestUomID], [PickupPrintStatusID], [DeliverPrintStatusID], [PickupPrintDateUTC], [DeliverPrintDateUTC], [OriginTankID], [OriginGrossStdUnits], [DispatchNotes], [DriverNotes], [DispatchConfirmNum], [OriginWaitReasonID], [DestWaitReasonID], [RejectReasonID], [DestOpenMeterUnits], [DestCloseMeterUnits], [ReassignKey])
				SELECT NewOrderNum, [StatusID], [PriorityID], [DueDate], [RouteID], [OriginID], [OriginArriveTimeUTC], [OriginDepartTimeUTC], [OriginMinutes], [OriginWaitNotes], [OriginBOLNum], [OriginGrossUnits], [OriginNetUnits], [DestinationID], [DestArriveTimeUTC], [DestDepartTimeUTC], [DestMinutes], [DestWaitNotes], [DestBOLNum], [DestGrossUnits], [DestNetUnits], [CustomerID], D.[CarrierID], [DriverID], D.TruckID, D.TrailerID, D.Trailer2ID, [OperatorID], [PumperID], [TicketTypeID], [Rejected], [RejectNotes], [ChainUp], [OriginTruckMileage], [OriginTankNum], [DestTruckMileage], [CarrierTicketNum], [AuditNotes], O.CreateDateUTC, [ActualMiles], [ProducerID], O.CreatedByUser, O.LastChangeDateUTC, O.LastChangedByUser, O.DeleteDateUTC, O.DeletedByUser, [DestProductBSW], [DestProductGravity], [DestProductTemp], [ProductID], [AcceptLastChangeDateUTC], [PickupLastChangeDateUTC], [DeliverLastChangeDateUTC], [OriginUomID], [DestUomID], [PickupPrintStatusID], [DeliverPrintStatusID], [PickupPrintDateUTC], [DeliverPrintDateUTC], [OriginTankID], [OriginGrossStdUnits], [DispatchNotes], [DriverNotes], DispatchConfirmNum, [OriginWaitReasonID], [DestWaitReasonID], [RejectReasonID], [DestOpenMeterUnits], [DestCloseMeterUnits], ReassignKey
				FROM #clone O
				LEFT JOIN tblDriver D ON D.ID = O.DriverID
		
			-- cache the affected tickets
			SELECT OT.*, C.NewOrderNum, OrderDeletedByUser = C.LastChangedByUser
			INTO #cloneTicket
			FROM tblOrderTicket OT
			JOIN #clone C ON C.ID = OT.OrderID

			/*			
			-- mark the existing related tickets as deleted
			UPDATE tblOrderTicket
				SET DeleteDateUTC = isnull(OT.DeleteDateUTC, GETUTCDATE()), DeletedByUser = isnull(OT.DeletedByUser, CT.OrderDeletedByUser)
			FROM tblOrderTicket OT
			JOIN #cloneTicket CT ON CT.ID = OT.ID
			*/
			
			-- create the cloned Order Ticket records (copy of existing ticket(s) pointed at new "original" Order)
			INSERT INTO tblOrderTicket (OrderID, CarrierTicketNum, TicketTypeID, TankNum, ProductObsGravity, ProductObsTemp, ProductBSW, OpeningGaugeFeet, OpeningGaugeInch, OpeningGaugeQ, ClosingGaugeFeet, ClosingGaugeInch, ClosingGaugeQ
					, GrossUnits, NetUnits, Rejected, RejectNotes, SealOff, SealOn, BOLNum, ProductHighTemp, ProductLowTemp, CreateDateUTC, CreatedByUser, LastChangeDateUTC, LastChangedByUser, DeleteDateUTC, DeletedByUser
					, UID, FromMobileApp, OriginTankID, BottomFeet, BottomInches, BottomQ, GrossStdUnits, RejectReasonID, MeterFactor, OpenMeterUnits, CloseMeterUnits)
				SELECT O.ID, CT.CarrierTicketNum, CT.TicketTypeID, CT.TankNum, CT.ProductObsGravity, CT.ProductObsTemp, CT.ProductBSW, CT.OpeningGaugeFeet, CT.OpeningGaugeInch, CT.OpeningGaugeQ, CT.ClosingGaugeFeet, CT.ClosingGaugeInch, CT.ClosingGaugeQ
					, CT.GrossUnits, CT.NetUnits, CT.Rejected, CT.RejectNotes, CT.SealOff, CT.SealOn, CT.BOLNum, CT.ProductHighTemp, CT.ProductLowTemp, CT.CreateDateUTC, CT.CreatedByUser, CT.LastChangeDateUTC, CT.LastChangedByUser, CT.DeleteDateUTC, CT.DeletedByUser
					, newid(), CT.FromMobileApp, CT.OriginTankID, CT.BottomFeet, CT.BottomInches, CT.BottomQ, CT.GrossStdUnits, CT.RejectReasonID, CT.MeterFactor, CT.OpenMeterUnits, CT.CloseMeterUnits
				FROM #cloneTicket CT
				JOIN tblOrder O ON O.OrderNum = CT.NewOrderNum
		END
		/*************************************************************************************************************/
		
		--NOTE: we do not update the DestOpenMeterUnits/DestCloseMeterUnits since they don't auto update
		--  (and they simply represent a reading that is assumed to in the DestUomID UOM at entry)
		--  (if they are updated in the future [manually] the will calculate again using the DestUomID at that time)

		IF EXISTS (SELECT * FROM inserted EXCEPT SELECT * FROM deleted)
		BEGIN
			/*
			-- ensure that all changed orders have their LastChangeDateUTC set to UTCNOW
			UPDATE tblOrder
			  SET LastChangeDateUTC = GETUTCDATE()
				, LastChangedByUser = ISNULL(O.LastChangedByUser, 'System')
			FROM tblOrder O
			JOIN inserted i ON i.ID = O.ID
			JOIN deleted d ON d.ID = O.id
			-- ensure a lastchangedateutc value is set (and updated if it isn't close to NOW and was explicitly changed by the callee)
			WHERE O.LastChangeDateUTC IS NULL 
				--
				OR (i.LastChangeDateUTC = d.LastChangeDateUTC AND abs(datediff(second, O.LastChangeDateUTC, GETUTCDATE())) > 2)
			*/
			
			-- optionally add tblOrderDBAudit records
			BEGIN TRY
				IF EXISTS(SELECT * FROM tblSetting WHERE ID = 32 AND (Value LIKE 'true' OR Value LIKE 'yes'))
					INSERT INTO tblOrderDbAudit (DBAuditDate, [ID], [OrderNum], [StatusID], [PriorityID], [DueDate], [RouteID], [OriginID], [OriginArriveTimeUTC], [OriginDepartTimeUTC], [OriginMinutes], [OriginWaitNotes], [OriginBOLNum], [OriginGrossUnits], [OriginNetUnits], [DestinationID], [DestArriveTimeUTC], [DestDepartTimeUTC], [DestMinutes], [DestWaitNotes], [DestBOLNum], [DestGrossUnits], [DestNetUnits], [CustomerID], [CarrierID], [DriverID], [TruckID], [TrailerID], [Trailer2ID], [OperatorID], [PumperID], [TicketTypeID], [Rejected], [RejectNotes], [ChainUp], [OriginTruckMileage], [OriginTankNum], [DestTruckMileage], [CarrierTicketNum], [AuditNotes], [CreateDateUTC], [ActualMiles], [ProducerID], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser], [DestProductBSW], [DestProductGravity], [DestProductTemp], [ProductID], [AcceptLastChangeDateUTC], [PickupLastChangeDateUTC], [DeliverLastChangeDateUTC], [OriginUomID], [DestUomID], [PickupPrintStatusID], [DeliverPrintStatusID], [PickupPrintDateUTC], [DeliverPrintDateUTC], [OriginTankID], [OriginGrossStdUnits], [DispatchNotes], [DriverNotes], [DispatchConfirmNum], [OriginWaitReasonID], [DestWaitReasonID], [RejectReasonID], [DestOpenMeterUnits], [DestCloseMeterUnits], ReassignKey)
						SELECT GETUTCDATE(), [ID], [OrderNum], [StatusID], [PriorityID], [DueDate], [RouteID], [OriginID], [OriginArriveTimeUTC], [OriginDepartTimeUTC], [OriginMinutes], [OriginWaitNotes], [OriginBOLNum], [OriginGrossUnits], [OriginNetUnits], [DestinationID], [DestArriveTimeUTC], [DestDepartTimeUTC], [DestMinutes], [DestWaitNotes], [DestBOLNum], [DestGrossUnits], [DestNetUnits], [CustomerID], [CarrierID], [DriverID], [TruckID], [TrailerID], [Trailer2ID], [OperatorID], [PumperID], [TicketTypeID], [Rejected], [RejectNotes], [ChainUp], [OriginTruckMileage], [OriginTankNum], [DestTruckMileage], [CarrierTicketNum], [AuditNotes], [CreateDateUTC], [ActualMiles], [ProducerID], [CreatedByUser], [LastChangeDateUTC], [LastChangedByUser], [DeleteDateUTC], [DeletedByUser], [DestProductBSW], [DestProductGravity], [DestProductTemp], [ProductID], [AcceptLastChangeDateUTC], [PickupLastChangeDateUTC], [DeliverLastChangeDateUTC], [OriginUomID], [DestUomID], [PickupPrintStatusID], [DeliverPrintStatusID], [PickupPrintDateUTC], [DeliverPrintDateUTC], [OriginTankID], [OriginGrossStdUnits], [DispatchNotes], [DriverNotes], [DispatchConfirmNum], [OriginWaitReasonID], [DestWaitReasonID], [RejectReasonID], [DestOpenMeterUnits], [DestCloseMeterUnits], ReassignKey
						FROM deleted d
			END TRY
			BEGIN CATCH
				PRINT 'trigOrder_IU.DBAUDIT FAILURE: ' + ERROR_MESSAGE()
			END CATCH
		END
		
	PRINT 'trigOrder_IU COMPLETE'

	END
	
END



GO
EXEC sp_settriggerorder @triggername=N'[dbo].[trigOrder_IU]', @order=N'First', @stmttype=N'INSERT'
GO
EXEC sp_settriggerorder @triggername=N'[dbo].[trigOrder_IU]', @order=N'First', @stmttype=N'UPDATE'
GO

COMMIT
SET NOEXEC OFF