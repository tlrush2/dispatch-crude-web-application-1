SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.11.17.4'
SELECT  @NewVersion = '3.11.17.5'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'HOTFIX - Correct commodity price sync error'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/****************************************/
-- Date Created: 31 Dec 2014
-- Author: Kevin Alons
-- Purpose: apply both Shipper & Carrier rates (in the appropriate order) 
-- Changes:
--		- 3.11.17.1 - JAE - 3/31/2016 - Added producer (should probably be renamed)
--      - 3.11.17.5 - JAE - 4/28/2016 - Omit Producer apply rates for now
/****************************************/
ALTER PROCEDURE spApplyRatesBoth(@ID int, @UserName varchar(255), @ResetOverrides bit = 0) AS
BEGIN
	BEGIN TRY
		EXEC spApplyRatesShipper @ID = @ID, @UserName = @UserName, @ResetOverrides = @ResetOverrides
		EXEC spApplyRatesCarrier @ID = @ID, @UserName = @UserName, @ResetOverrides = @ResetOverrides
		--EXEC spApplyRatesProducer @ID = @ID, @UserName = @UserName
	END TRY
	BEGIN CATCH
		DECLARE @msg varchar(max), @severity int
		SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY()
		RAISERROR(@msg, @severity, 1)
	END CATCH
END

GO

COMMIT 
SET NOEXEC OFF