-- backup database [dispatchcrude.dev] to disk = 'd:\data\backup\dispatchcrude.dev.3.8.19.bak'
-- restore database [DispatchCrude.Dev] from disk = 'd:\data\backup\dispatchcrude.dev.3.8.19.bak'
-- go
-- rollback
-- select value from tblsetting where id = 0

DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.8.19'
SELECT  @NewVersion = '3.8.20'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Fix for last DB Update (3.8.19).  It was not fully tested and broke several unexpected things.'	
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/*****************************************************************************************/
-- Author: Kevin Alons
-- Date Created: 27 Feb 2014
-- Purpose: with the provided parameters, export the data and mark done (if doing finalExport)
--		8/12/15 - Ben B. - Expanded procedure to include C.O.D.E. export options
--		8/21/15 - Ben B. - Added summary row to CODE export
--		8/25/15 - Ben B. & Joe E. - CODE summary record was not working on final export (only preview). Moved where this record was added.
--		8/26/15 - Ben B. - Added summary row to CODE summary row row count per customer request
--		8/26/15 - Ben B. - Fixed last update (3.8.19)  I pulled the old version of the stored procedure to add the row count and lost the summary row.
/*****************************************************************************************/
ALTER PROCEDURE [dbo].[spOrderExportFinalCustomer]
( 
  @customerID int 
, @exportedByUser varchar(100) = NULL -- will default to SUSER_NAME() -- default value for now
, @exportFormat varchar(100) -- Options are: 'SUNOCO_SUNDEX' or 'CODE_STANDARD'
, @finalExport bit = 1 -- defualt to TRUE
) 
AS BEGIN
	SET NOCOUNT ON
	-- default to the current user if not supplied
	IF @exportedByUser IS NULL SET @exportedByUser = SUSER_NAME()
	
	-- Create variable for error catching with CODE export
	DECLARE @CODEErrorFlag BIT = 0 -- Default to false
	
	-- retrieve the order records to export
	DECLARE @orderIDs IDTABLE
	INSERT INTO @orderIDs (ID)
	SELECT ID
	FROM dbo.viewOrderCustomerFinalExportPending 
	WHERE CustomerID = @customerID 

	BEGIN TRAN exportCustomer
	
	-- export the orders in the specified format
	IF (@exportFormat = 'SUNOCO_SUNDEX') BEGIN
		SELECT E.* 
		INTO #output
		FROM dbo.viewSunocoSundex E
		JOIN @orderIDs IDs ON IDs.ID = E._ID
		WHERE E._CustomerID = @customerID
		ORDER BY _OrderNum
	END
	ELSE
	IF (@exportFormat = 'CODE_STANDARD') BEGIN
		DECLARE @CodeDXCode varchar(2) = (SELECT CodeDXCode FROM tblCustomer WHERE ID = @customerID)
		IF LEN(@CodeDXCode) = 2 BEGIN
			SELECT E.ExportText
			INTO #output2
			FROM dbo.viewOrderExport_CODE E
				JOIN @orderIDs IDs ON IDs.ID = E.OrderID
			WHERE E.CompanyID = @CodeDXCode
			
			/* Add summary record to output*/
			INSERT INTO #output2
			SELECT
				'4' -- Summary Record Type RecordID (Hard Coded)
				+ X.Company_ID 
				+ X.Record_Count 
				+ X.Pos_Neg_Code 
				+ X.Total_Net
				+ X.Shipper_Net
				+ dbo.fnFixedLenStr('',89)
				+ dbo.fnFixedLenStr('',3)
			FROM
				(SELECT
					ECB.Company_ID
					,CAST(dbo.fnFixedLenNum((COUNT(*) + 1), 10, 0, 1) AS VARCHAR)	AS Record_Count
					,ECB.Pos_Neg_Code
					,CAST(dbo.fnFixedLenNum(SUM(CAST(ECB.Total_Net_Volume AS DECIMAL)), 13, 0, 1) AS VARCHAR) Total_Net
					,CAST(dbo.fnFixedLenNum(SUM(CAST(ECB.Shippers_Net_Volume AS DECIMAL)), 13, 0, 1) AS VARCHAR) Shipper_Net
				FROM dbo.viewOrderExport_CODE_Base ECB			
					JOIN @orderIDs IDs ON IDs.ID = ECB.OrderID			
				WHERE ECB.Company_ID = @CodeDXCode
				GROUP BY ECB.Company_ID,ECB.Pos_Neg_Code) X			
		END
		ELSE SET @CODEErrorFlag = 1	
	END

	-- mark the orders as exported FINAL (for a Customer)
	IF (@finalExport = 1) BEGIN
		EXEC spMarkOrderExportFinalCustomer @orderIDs, @exportedByUser
	END
	
	COMMIT TRAN exportCustomer

	-- return the data to 
	IF (@exportFormat = 'SUNOCO_SUNDEX') BEGIN
		SELECT * FROM #output
	END
	
	IF (@exportFormat = 'CODE_STANDARD') BEGIN
		IF @CODEErrorFlag = 0 BEGIN
			SELECT * FROM #output2 ORDER BY ExportText --Sorted so that the summary record always sits at the bottom
		END
		ELSE SELECT 'Error in export data. Check Shipper CODE ID.'
	END		
END
GO

/* remove Order Create Date from this view...it broke a LOT of other things that didn't make sense to me to be related */
/***********************************/
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return the Local Arrive|Depart Times + their respective TimeZone abbreviations
/***********************************/
ALTER VIEW [dbo].[viewOrderLocalDates] AS
SELECT O.*	
	, dbo.fnUTC_to_Local(O.OriginArriveTimeUTC, O.OriginTimeZoneID, O.OriginUseDST) AS OriginArriveTime
	, dbo.fnUTC_to_Local(O.OriginDepartTimeUTC, O.OriginTimeZoneID, O.OriginUseDST) AS OriginDepartTime
	, dbo.fnUTC_to_Local(O.DestArriveTimeUTC, O.DestTimeZoneID, O.DestUseDST) AS DestArriveTime
	, dbo.fnUTC_to_Local(O.DestDepartTimeUTC, O.DestTimeZoneID, O.DestUseDST) AS DestDepartTime
	, dbo.fnTimeZoneAbbrev(O.OriginArriveTimeUTC, O.OriginTimeZoneID, O.OriginUseDST) AS OriginTimeZone
	, dbo.fnTimeZoneAbbrev(O.DestArriveTimeUTC, O.DestTimeZoneID, O.DestUseDST) AS DestTimeZone
	, DATEDIFF(minute, O.OriginDepartTimeUTC, O.DestArriveTimeUTC) AS TransitMinutes
	FROM viewOrder O
GO

exec sp_refreshview viewOrder_OrderTicket_Full
GO

exec sp_refreshview viewOrderExportFull_Reroute
GO

exec sp_refreshview viewOrder_AllTickets
GO

exec sp_refreshview viewOrder_Financial_Carrier
GO

exec sp_refreshview viewOrder_Financial_Shipper
GO

exec sp_refreshview viewOrder_Financial_Carrier
GO

exec sp_refreshview viewReportCenter_Orders
GO

exec sp_refreshview viewOrderDisplay
GO


/* delete newly added report center column that broke things */
DELETE FROM tblReportColumnDefinition WHERE ID = 275
GO

/***********************************
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Report Center Order data
-- Changes:
   -		 - 05/21/15	 - GSM - Adding Gauger Process data elements to Report Center
   -		 - 06/08/15	 - BB  - Add "OriginCity" and "OriginCityState" columns			
   -		 - 06/17/15  - BB  - Add "DestCity" and "DestCityState" columns
   - 3.7.39 - 2015/06/30 - BB  - Add "Shipper Ticket Type" column
   - 3.8.20 - 2015/08/26 - BB  - Add Order Create Date column
***********************************/
ALTER VIEW [dbo].[viewReportCenter_Orders] AS
	SELECT O.*
		, ShipperBatchNum = SS.BatchNum
		, ShipperBatchInvoiceNum = SSB.InvoiceNum
		, ShipperSettlementUomID = SS.SettlementUomID
		, ShipperSettlementUom = SS.SettlementUom
		, ShipperMinSettlementUnits = SS.MinSettlementUnits
		, ShipperSettlementUnits = SS.SettlementUnits
		, ShipperRateSheetRate = SS.RateSheetRate
		, ShipperRateSheetRateType = SS.RateSheetRateType
		, ShipperRouteRate = SS.RouteRate
		, ShipperRouteRateType = SS.RouteRateType
		, ShipperLoadRate = isnull(SS.RouteRate, SS.RateSheetRate)
		, ShipperLoadRateType = isnull(SS.RouteRateType, SS.RateSheetRateType)
		, ShipperLoadAmount = SS.LoadAmount
		, ShipperOrderRejectRate = SS.OrderRejectRate 
		, ShipperOrderRejectRateType = SS.OrderRejectRateType 
		, ShipperOrderRejectAmount = SS.OrderRejectAmount 
		, ShipperWaitFeeSubUnit = SS.WaitFeeSubUnit  
		, ShipperWaitFeeRoundingType = SS.WaitFeeRoundingType  
		, ShipperOriginWaitBillableHours = SS.OriginWaitBillableHours  
		, ShipperOriginWaitBillableMinutes = SS.OriginWaitBillableMinutes  
		, ShipperOriginWaitRate = SS.OriginWaitRate
		, ShipperOriginWaitAmount = SS.OriginWaitAmount
		, ShipperDestinationWaitBillableHours = SS.DestinationWaitBillableHours 
		, ShipperDestinationWaitBillableMinutes = SS.DestinationWaitBillableMinutes   
		, ShipperDestinationWaitRate = SS.DestinationWaitRate  
		, ShipperDestinationWaitAmount = SS.DestinationWaitAmount  
		, ShipperTotalWaitAmount = SS.TotalWaitAmount
		, ShipperTotalWaitBillableMinutes = nullif(ISNULL(SS.OriginWaitBillableMinutes, 0) + ISNULL(SS.DestinationWaitBillableMinutes, 0), 0)
		, ShipperTotalWaitBillableHours = nullif(ISNULL(SS.OriginWaitBillableHours, 0) + ISNULL(SS.DestinationWaitBillableHours, 0), 0)		
		, ShipperFuelSurchargeRate = SS.FuelSurchargeRate
		, ShipperFuelSurchargeAmount = SS.FuelSurchargeAmount
		, ShipperChainupRate = SS.ChainupRate
		, ShipperChainupRateType = SS.ChainupRateType  
		, ShipperChainupAmount = SS.ChainupAmount
		, ShipperRerouteRate = SS.RerouteRate
		, ShipperRerouteRateType = SS.RerouteRateType  
		, ShipperRerouteAmount = SS.RerouteAmount
		, ShipperSplitLoadRate = SS.SplitLoadRate
		, ShipperSplitLoadRateType = SS.SplitLoadRateType  
		, ShipperSplitLoadAmount = SS.SplitLoadAmount
		, ShipperH2SRate = SS.H2SRate
		, ShipperH2SRateType = SS.H2SRateType  
		, ShipperH2SAmount = SS.H2SAmount
		, ShipperTaxRate = SS.OriginTaxRate
		, ShipperTotalAmount = SS.TotalAmount
		, ShipperDestCode = CDC.Code
		, ShipperTicketType = STT.TicketType
		--
		, CarrierBatchNum = SC.BatchNum
		, CarrierSettlementUomID = SC.SettlementUomID
		, CarrierSettlementUom = SC.SettlementUom
		, CarrierMinSettlementUnits = SC.MinSettlementUnits
		, CarrierSettlementUnits = SC.SettlementUnits
		, CarrierRateSheetRate = SC.RateSheetRate
		, CarrierRateSheetRateType = SC.RateSheetRateType
		, CarrierRouteRate = SC.RouteRate
		, CarrierRouteRateType = SC.RouteRateType
		, CarrierLoadRate = isnull(SC.RouteRate, SC.RateSheetRate)
		, CarrierLoadRateType = isnull(SC.RouteRateType, SC.RateSheetRateType)
		, CarrierLoadAmount = SC.LoadAmount
		, CarrierOrderRejectRate = SC.OrderRejectRate 
		, CarrierOrderRejectRateType = SC.OrderRejectRateType 
		, CarrierOrderRejectAmount = SC.OrderRejectAmount 
		, CarrierWaitFeeSubUnit = SC.WaitFeeSubUnit  
		, CarrierWaitFeeRoundingType = SC.WaitFeeRoundingType  
		, CarrierOriginWaitBillableHours = SS.OriginWaitBillableHours  
		, CarrierOriginWaitBillableMinutes = SC.OriginWaitBillableMinutes  
		, CarrierOriginWaitRate = SC.OriginWaitRate
		, CarrierOriginWaitAmount = SC.OriginWaitAmount
		, CarrierDestinationWaitBillableHours = SS.DestinationWaitBillableHours 
		, CarrierDestinationWaitBillableMinutes = SC.DestinationWaitBillableMinutes  
		, CarrierDestinationWaitRate = SC.DestinationWaitRate 
		, CarrierDestinationWaitAmount = SC.DestinationWaitAmount  
		, CarrierTotalWaitAmount = SC.TotalWaitAmount
		, CarrierTotalWaitBillableMinutes = nullif(ISNULL(SS.OriginWaitBillableMinutes, 0) + ISNULL(SS.DestinationWaitBillableMinutes, 0), 0)
		, CarrierTotalWaitBillableHours = nullif(ISNULL(SS.OriginWaitBillableHours, 0) + ISNULL(SS.DestinationWaitBillableHours, 0), 0)		
		, CarrierFuelSurchargeRate = SC.FuelSurchargeRate
		, CarrierFuelSurchargeAmount = SC.FuelSurchargeAmount
		, CarrierChainupRate = SC.ChainupRate
		, CarrierChainupRateType = SC.ChainupRateType  
		, CarrierChainupAmount = SC.ChainupAmount
		, CarrierRerouteRate = SC.RerouteRate
		, CarrierRerouteRateType = SC.RerouteRateType  
		, CarrierRerouteAmount = SC.RerouteAmount
		, CarrierSplitLoadRate = SC.SplitLoadRate
		, CarrierSplitLoadRateType = SC.SplitLoadRateType  
		, CarrierSplitLoadAmount = SC.SplitLoadAmount
		, CarrierH2SRate = SC.H2SRate
		, CarrierH2SRateType = SC.H2SRateType  
		, CarrierH2SAmount = SC.H2SAmount
		, CarrierTaxRate = SC.OriginTaxRate
		, CarrierTotalAmount = SC.TotalAmount
		--
		, OriginGpsLatLon = ltrim(DLO.Lat) + ',' + ltrim(DLO.Lon)
		, OriginLatLon = ltrim(OO.LAT) + ',' + ltrim(OO.LON)
		, OriginAccuracyMeters = DLO.SourceAccuracyMeters
		, OriginDistance = isnull(cast(DLO.DistanceToPoint as int), 99999)
		, OriginGpsArrived = CASE WHEN DLO.DistanceToPoint IS NOT NULL AND DLO.DistanceToPoint BETWEEN 0 AND OO.GeoFenceRadiusMeters THEN 1 ELSE 0 END
		, OriginCTBNum = OO.CTBNum
		, OriginFieldName = OO.FieldName
		, OriginCity = OO.City																				
		, OriginCityState = OO.City + ', ' + (SELECT Abbreviation FROM tblState WHERE ID = OO.StateID)		
		--
		, DestGpsLatLon = ltrim(DLD.Lat) + ',' + ltrim(DLD.Lon)
		, DestLatLon = ltrim(D.LAT) + ',' + ltrim(D.LON)
		, DestAccuracyMeters = DLD.SourceAccuracyMeters
		, DestDistance = isnull(cast(DLD.DistanceToPoint as int), 99999)
		, DestGpsArrived = CASE WHEN DLD.DistanceToPoint IS NOT NULL AND DLD.DistanceToPoint BETWEEN 0 AND D.GeoFenceRadiusMeters THEN 1 ELSE 0 END		
		, DestCity = D.City
		, DestCityState = D.City + ', ' + (SELECT Abbreviation FROM tblState WHERE ID = OO.StateID)
		--
		, Gauger = GAO.Gauger						
		, GaugerIDNumber = GA.IDNumber
		, GaugerFirstName = GA.FirstName
		, GaugerLastName = GA.LastName
		, GaugerRejected = GAO.Rejected
		, GaugerRejectReasonID = GAO.RejectReasonID
		, GaugerRejectNotes = GAO.RejectNotes
		, GaugerRejectNumDesc = GORR.NumDesc
		, GaugerPrintDate = dbo.fnUTC_to_Local(GAO.PrintDateUTC, O.OriginTimeZoneID, O.OriginUseDST)
		--
		, T_GaugerCarrierTicketNum = CASE WHEN GAO.TicketCount = 0 THEN ltrim(GAO.OrderNum) + CASE WHEN GAO.Rejected = 1 THEN 'X' ELSE '' END ELSE GOT.CarrierTicketNum END 
		, T_GaugerTankNum = isnull(GOT.OriginTankText, GAO.OriginTankText)
		, T_GaugerIsStrappedTank = GOT.IsStrappedTank 
		, T_GaugerProductObsTemp = GOT.ProductObsTemp
		, T_GaugerProductObsGravity = GOT.ProductObsGravity
		, T_GaugerProductBSW = GOT.ProductBSW		
		, T_GaugerOpeningGaugeFeet = GOT.OpeningGaugeFeet
		, T_GaugerOpeningGaugeInch = GOT.OpeningGaugeInch		
		, T_GaugerOpeningGaugeQ = GOT.OpeningGaugeQ			
		, T_GaugerBottomFeet = GOT.BottomFeet
		, T_GaugerBottomInches = GOT.BottomInches		
		, T_GaugerBottomQ = GOT.BottomQ		
		, T_GaugerRejected = GOT.Rejected
		, T_GaugerRejectReasonID = GOT.RejectReasonID
		, T_GaugerRejectNumDesc = GOT.RejectNumDesc
		, T_GaugerRejectNotes = GOT.RejectNotes	
		--
		, OrderCreateDate = dbo.fnUTC_to_Local(O.CreateDateUTC, O.OriginTimeZoneID, O.OriginUseDST)
	FROM viewOrder_OrderTicket_Full O
	JOIN tblOrigin OO ON OO.ID = O.OriginID
	JOIN tblDestination D ON D.ID = O.DestinationID
	--
    LEFT JOIN viewGaugerOrder GAO ON GAO.OrderID = O.ID			            
    LEFT JOIN viewGaugerOrderTicket GOT ON GOT.UID = O.T_UID	            
    LEFT JOIN viewGauger GA ON GA.ID = GAO.GaugerID				            
    LEFT JOIN viewOrderRejectReason GORR ON GORR.ID = GAO.RejectReasonID 
    --
    LEFT JOIN tblCustomerDestinationCode CDC ON CDC.CustomerID = O.CustomerID AND CDC.DestinationID = O.DestinationID
	LEFT JOIN viewDriverLocation_OriginFirstArrive DLO ON DLO.OrderID = O.ID AND DLO.OriginID = O.OriginID
	LEFT JOIN viewDriverLocation_DestinationFirstArrive DLD ON DLD.OrderID = O.ID AND DLD.DestinationID = O.DestinationID
	LEFT JOIN viewOrderSettlementCarrier SC ON SC.OrderID = O.ID
	LEFT JOIN viewOrderSettlementShipper SS ON SS.OrderID = O.ID
	LEFT JOIN tblShipperSettlementBatch SSB ON SSB.ID = SS.BatchID
	LEFT JOIN tblShipperTicketType AS STT ON STT.CustomerID = O.CustomerID AND STT.TicketTypeID = O.TicketTypeID
GO

/*
	BB 8/26/15 - DCWEB-845: Added order create date to report center.
*/
SET IDENTITY_INSERT tblReportColumnDefinition ON
  INSERT tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport)
  SELECT 90027,1,'OrderCreateDate','TRIP | TIMESTAMPS | Order Create Date',NULL,NULL,0,NULL,1,'*',0    
  EXCEPT SELECT ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport FROM tblReportColumnDefinition
SET IDENTITY_INSERT tblReportColumnDefinition OFF
GO


EXEC _spRebuildAllObjects
GO

COMMIT
SET NOEXEC OFF