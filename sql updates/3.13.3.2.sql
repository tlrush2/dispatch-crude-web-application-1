SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20) = '3.13.3.1'
	, @NewVersion varchar(20) = '3.13.3.2'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1605 - Change Origin NDM to Origin BLM Info'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


-- Extend length of NDM column
ALTER TABLE tblOrigin ALTER COLUMN NDM VARCHAR(50) NULL
GO


-- Change Report Center field name
UPDATE tblReportColumnDefinition SET Caption = 'ORIGIN | GENERAL | Origin BLM Info' WHERE ID = 60
GO


COMMIT
SET NOEXEC OFF