-- rollback
-- select value from tblsetting where id = 0
/*
	- add Shipper/Carrier Units (settlement)
	- add ShipperDestinationCode
*/
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.2.6'
SELECT  @NewVersion = '3.2.7'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Report Center Order data
/***********************************/
ALTER VIEW [dbo].[viewReportCenter_Orders] AS
	SELECT O.*
		, ShipperH2SRate = IOCS.H2SRate
		, ShipperH2SFee = IOCS.H2SFee
		, ShipperChainupFee = IOCS.ChainupFee
		, ShipperTaxRate = IOCS.TaxRate
		, ShipperRerouteFee = IOCS.RerouteFee
		, ShipperRejectFee = IOCS.RejectionFee
		, ShipperOriginWaitRate = IOCS.OriginWaitRate
		, ShipperOriginWaitFee = IOCS.OriginWaitFee
		, ShipperDestWaitRate = IOCS.DestinationWaitRate
		, ShipperDestWaitFee = IOCS.DestWaitFee
		, ShipperTotalWaitFee = cast(IOCS.OriginWaitFee as int) + cast(IOCS.DestWaitFee as int)
		, ShipperTotalFee = IOCS.TotalFee
		, ShipperSettlementUomID = IOCS.UomID
		, ShipperSettlementUom = SUOM.Abbrev
		, ShipperMinSettlementUnits = IOCS.MinSettlementUnits
		, ShipperRouteRate = IOCS.RouteRate
		, ShipperRouteFee = IOCS.LoadFee
		, ShipperBatchNum = SSB.BatchNum
		, ShipperFuelCharge = IOCS.FuelSurcharge
		, ShipperUnits = IOCS.Units
		, CarrierH2SRate = IOCC.H2SRate
		, CarrierH2SFee = IOCC.H2SFee
		, CarrierChainupFee = IOCC.ChainupFee
		, CarrierTaxRate = IOCC.TaxRate
		, CarrierRerouteFee = IOCC.RerouteFee
		, CarrierRejectFee = IOCC.RejectionFee
		, CarrierOriginWaitRate = IOCC.OriginWaitRate
		, CarrierOriginWaitFee = IOCC.OriginWaitFee
		, CarrierDestWaitRate = IOCC.DestinationWaitRate
		, CarrierDestWaitFee = IOCC.DestWaitFee
		, CarrierTotalWaitFee = cast(IOCC.OriginWaitFee as int) + cast(IOCC.DestWaitFee as int)
		, CarrierTotalFee = IOCC.TotalFee
		, CarrierSettlementUomID = IOCC.UomID
		, CarrierSettlementUom = CUOM.Abbrev
		, CarrierMinSettlementUnits = IOCC.MinSettlementUnits
		, CarrierRouteRate = IOCC.RouteRate
		, CarrierRouteFee = IOCC.LoadFee
		, CarrierBatchNum = CSB.BatchNum
		, CarrierFuelCharge = IOCC.FuelSurcharge
		, CarrierUnits = IOCC.Units
		, OriginGpsLatLon = ltrim(DLO.Lat) + ',' + ltrim(DLO.Lon)
		, OriginLatLon = ltrim(OO.LAT) + ',' + ltrim(OO.LON)
		, OriginAccuracyMeters = DLO.SourceAccuracyMeters
		, OriginDistance = isnull(cast(DLO.DistanceToPoint as int), 99999)
		, OriginGpsArrived = CASE WHEN DLO.DistanceToPoint IS NOT NULL AND DLO.DistanceToPoint <= OO.GeoFenceRadiusMeters THEN 1 ELSE 0 END
		, DestGpsLatLon = ltrim(DLD.Lat) + ',' + ltrim(DLD.Lon)
		, DestLatLon = ltrim(D.LAT) + ',' + ltrim(D.LON)
		, DestAccuracyMeters = DLD.SourceAccuracyMeters
		, DestDistance = isnull(cast(DLD.DistanceToPoint as int), 99999)
		, DestGpsArrived = CASE WHEN DLD.DistanceToPoint IS NOT NULL AND DLD.DistanceToPoint <= OO.GeoFenceRadiusMeters THEN 1 ELSE 0 END
		, ShipperDestCode = CDC.Code
	FROM viewOrder_OrderTicket_Full O
	JOIN tblOrigin OO ON OO.ID = O.OriginID
	JOIN tblDestination D ON D.ID = O.DestinationID
	LEFT JOIN tblOrderInvoiceCustomer IOCS ON IOCS.OrderID = O.ID
	LEFT JOIN tblCustomerSettlementBatch SSB ON SSB.ID = IOCS.BatchID
	LEFT JOIN tblUom SUOM ON SUOM.ID = IOCS.UomID
	LEFT JOIN tblOrderInvoiceCarrier IOCC ON IOCC.OrderID = O.ID
	LEFT JOIN tblCarrierSettlementBatch CSB ON CSB.ID = IOCC.BatchID
	LEFT JOIN tblUom CUOM ON CUOM.ID = IOCC.UomID
	LEFT JOIN tblCustomerDestinationCode CDC ON CDC.CustomerID = O.CustomerID AND CDC.DestinationID = O.DestinationID
	LEFT JOIN viewDriverLocation_OriginFirstArrive DLO ON DLO.OrderID = O.ID AND DLO.OriginID = O.OriginID
	LEFT JOIN viewDriverLocation_DestinationFirstArrive DLD ON DLD.OrderID = O.ID AND DLD.DestinationID = O.DestinationID

GO

INSERT INTO tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport)
	SELECT 182, 1, 'ShipperUnits', 'Shipper Settlement Volume', '#0.00', null, 4, null, 1, 'Administrator,Management', 1
	UNION
	SELECT 183, 1, 'CarrierUnits', 'Carrier Settlement Volume', '#0.00', null, 4, null, 1, 'Administrator,Management', 1
	UNION
	SELECT 184, 1, 'ShipperDestCode', 'Shipper Destination Code', null, null, 1, null, 1, 'Administrator,Management', 1

COMMIT
SET NOEXEC OFF