-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.12.7.4'
SELECT  @NewVersion = '3.12.7.5'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'DCWEB-1508 - Persist user page layouts on demand (via Save/Reset buttons)'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

CREATE TABLE tblUserPersistedValue
(
  ID int identity(1, 1) NOT NULL constraint PK_UserPersistedValue PRIMARY KEY
, UserName varchar(256) NOT NULL
, [Key] varchar(100) NOT NULL
, Value varchar(max) NOT NULL
)
GO
CREATE UNIQUE INDEX udxUserPersistedValue ON tblUserPersistedValue(UserName, [Key])
GO

COMMIT
SET NOEXEC OFF