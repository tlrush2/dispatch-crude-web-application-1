-- rollback
-- select value from tblsetting where id = 0
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '3.2.9'
SELECT  @NewVersion = '3.2.10'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Add TABLE tblShipperFuelSurchargeRateTable'
	UNION 
	SELECT @NewVersion, 0, 'Add FUNCTION fnMinDecimal()'
	UNION
	SELECT @NewVersion, 0, 'Add FUNCTION fnFuelPriceForEffectiveDate()'
	UNION
	SELECT @NewVersion, 0, 'Add TABLE tblEiaPADDRegions'
	UNION
	SELECT @NewVersion, 0, 'Add TABLE tblFuelPrice'
	UNION
	SELECT @newVersion, 0, 'Add FUNCTION fnShipperFuelSurchargeRate()'
	UNION
	SELECT @newVersion, 0, 'Add COLUMN tblCustomerRates.SplitLoadRate money NULL'
	UNION
	SELECT @newVersion, 0, 'Add COLUMN tblCarrierRates.SplitLoadRate money NULL'
	UNION
	SELECT @newVersion, 0, 'Revise FUNCTION fnOrderTicketDetails'
	UNION
	SELECT @NewVersion, 1, 'Add Fuel Surcharge Invoicing logic'
GO

CREATE TABLE tblEIAPADDRegion
(
  ID int identity(1, 1) NOT NULL CONSTRAINT PK_EIAPADDRegion PRIMARY KEY
, Name varchar(10) NOT NULL
, SourceKey varchar(50) NOT NULL
)
GO
GRANT SELECT ON tblEIAPADDRegion TO dispatchcrude_iis_acct
GO
CREATE UNIQUE INDEX udxEIDPADDRegion_Name ON tblEIAPADDRegion(Name)
GO
CREATE UNIQUE INDEX udxEIDPADDRegion_SourceKey ON tblEIAPADDRegion(SourceKey)
GO

INSERT INTO tblEIAPADDRegion (Name, SourceKey)
	SELECT 'PADD 1', 'EMD_EPD2D_PTE_R10_DPG'
	UNION
	SELECT 'PADD 2', 'EMD_EPD2D_PTE_R20_DPG'
	UNION
	SELECT 'PADD 3', 'EMD_EPD2D_PTE_R30_DPG'
	UNION
	SELECT 'PADD 4', 'EMD_EPD2D_PTE_R40_DPG'
	UNION
	SELECT 'PADD 5', 'EMD_EPD2D_PTE_R50_DPG'
GO

ALTER TABLE tblState ADD EIAPADDRegionID int 
	CONSTRAINT DF_State_EIDPADDRegion DEFAULT (1)
	CONSTRAINT FK_State_EIDPADDRegion FOREIGN KEY REFERENCES tblEIAPADDRegion(ID)
GO

UPDATE tblState Set EIAPADDRegionID = (SELECT TOP 1 EIA.ID FROM tblEIAPADDRegion EIA WHERE Name LIKE 'PADD 5') FROM tblState S WHERE S.ID = 2
UPDATE tblState Set EIAPADDRegionID = (SELECT TOP 1 EIA.ID FROM tblEIAPADDRegion EIA WHERE Name LIKE 'PADD 3') FROM tblState S WHERE S.ID = 1
UPDATE tblState Set EIAPADDRegionID = (SELECT TOP 1 EIA.ID FROM tblEIAPADDRegion EIA WHERE Name LIKE 'PADD 3') FROM tblState S WHERE S.ID = 4
UPDATE tblState Set EIAPADDRegionID = (SELECT TOP 1 EIA.ID FROM tblEIAPADDRegion EIA WHERE Name LIKE 'PADD 5') FROM tblState S WHERE S.ID = 3
UPDATE tblState Set EIAPADDRegionID = (SELECT TOP 1 EIA.ID FROM tblEIAPADDRegion EIA WHERE Name LIKE 'PADD 5') FROM tblState S WHERE S.ID = 5
UPDATE tblState Set EIAPADDRegionID = (SELECT TOP 1 EIA.ID FROM tblEIAPADDRegion EIA WHERE Name LIKE 'PADD 4') FROM tblState S WHERE S.ID = 6
UPDATE tblState Set EIAPADDRegionID = (SELECT TOP 1 EIA.ID FROM tblEIAPADDRegion EIA WHERE Name LIKE 'PADD 1') FROM tblState S WHERE S.ID = 7
UPDATE tblState Set EIAPADDRegionID = (SELECT TOP 1 EIA.ID FROM tblEIAPADDRegion EIA WHERE Name LIKE 'PADD 1') FROM tblState S WHERE S.ID = 9
UPDATE tblState Set EIAPADDRegionID = (SELECT TOP 1 EIA.ID FROM tblEIAPADDRegion EIA WHERE Name LIKE 'PADD 1') FROM tblState S WHERE S.ID = 8
UPDATE tblState Set EIAPADDRegionID = (SELECT TOP 1 EIA.ID FROM tblEIAPADDRegion EIA WHERE Name LIKE 'PADD 3') FROM tblState S WHERE S.ID = 10
UPDATE tblState Set EIAPADDRegionID = (SELECT TOP 1 EIA.ID FROM tblEIAPADDRegion EIA WHERE Name LIKE 'PADD 3') FROM tblState S WHERE S.ID = 11
UPDATE tblState Set EIAPADDRegionID = (SELECT TOP 1 EIA.ID FROM tblEIAPADDRegion EIA WHERE Name LIKE 'PADD 5') FROM tblState S WHERE S.ID = 12
UPDATE tblState Set EIAPADDRegionID = (SELECT TOP 1 EIA.ID FROM tblEIAPADDRegion EIA WHERE Name LIKE 'PADD 2') FROM tblState S WHERE S.ID = 16
UPDATE tblState Set EIAPADDRegionID = (SELECT TOP 1 EIA.ID FROM tblEIAPADDRegion EIA WHERE Name LIKE 'PADD 4') FROM tblState S WHERE S.ID = 13
UPDATE tblState Set EIAPADDRegionID = (SELECT TOP 1 EIA.ID FROM tblEIAPADDRegion EIA WHERE Name LIKE 'PADD 2') FROM tblState S WHERE S.ID = 14
UPDATE tblState Set EIAPADDRegionID = (SELECT TOP 1 EIA.ID FROM tblEIAPADDRegion EIA WHERE Name LIKE 'PADD 2') FROM tblState S WHERE S.ID = 15
UPDATE tblState Set EIAPADDRegionID = (SELECT TOP 1 EIA.ID FROM tblEIAPADDRegion EIA WHERE Name LIKE 'PADD 2') FROM tblState S WHERE S.ID = 17
UPDATE tblState Set EIAPADDRegionID = (SELECT TOP 1 EIA.ID FROM tblEIAPADDRegion EIA WHERE Name LIKE 'PADD 2') FROM tblState S WHERE S.ID = 18
UPDATE tblState Set EIAPADDRegionID = (SELECT TOP 1 EIA.ID FROM tblEIAPADDRegion EIA WHERE Name LIKE 'PADD 3') FROM tblState S WHERE S.ID = 19
UPDATE tblState Set EIAPADDRegionID = (SELECT TOP 1 EIA.ID FROM tblEIAPADDRegion EIA WHERE Name LIKE 'PADD 1') FROM tblState S WHERE S.ID = 22
UPDATE tblState Set EIAPADDRegionID = (SELECT TOP 1 EIA.ID FROM tblEIAPADDRegion EIA WHERE Name LIKE 'PADD 1') FROM tblState S WHERE S.ID = 21
UPDATE tblState Set EIAPADDRegionID = (SELECT TOP 1 EIA.ID FROM tblEIAPADDRegion EIA WHERE Name LIKE 'PADD 1') FROM tblState S WHERE S.ID = 20
UPDATE tblState Set EIAPADDRegionID = (SELECT TOP 1 EIA.ID FROM tblEIAPADDRegion EIA WHERE Name LIKE 'PADD 2') FROM tblState S WHERE S.ID = 23
UPDATE tblState Set EIAPADDRegionID = (SELECT TOP 1 EIA.ID FROM tblEIAPADDRegion EIA WHERE Name LIKE 'PADD 2') FROM tblState S WHERE S.ID = 24
UPDATE tblState Set EIAPADDRegionID = (SELECT TOP 1 EIA.ID FROM tblEIAPADDRegion EIA WHERE Name LIKE 'PADD 2') FROM tblState S WHERE S.ID = 26
UPDATE tblState Set EIAPADDRegionID = (SELECT TOP 1 EIA.ID FROM tblEIAPADDRegion EIA WHERE Name LIKE 'PADD 3') FROM tblState S WHERE S.ID = 25
UPDATE tblState Set EIAPADDRegionID = (SELECT TOP 1 EIA.ID FROM tblEIAPADDRegion EIA WHERE Name LIKE 'PADD 4') FROM tblState S WHERE S.ID = 27
UPDATE tblState Set EIAPADDRegionID = (SELECT TOP 1 EIA.ID FROM tblEIAPADDRegion EIA WHERE Name LIKE 'PADD 3') FROM tblState S WHERE S.ID = 34
UPDATE tblState Set EIAPADDRegionID = (SELECT TOP 1 EIA.ID FROM tblEIAPADDRegion EIA WHERE Name LIKE 'PADD 2') FROM tblState S WHERE S.ID = 35
UPDATE tblState Set EIAPADDRegionID = (SELECT TOP 1 EIA.ID FROM tblEIAPADDRegion EIA WHERE Name LIKE 'PADD 2') FROM tblState S WHERE S.ID = 28
UPDATE tblState Set EIAPADDRegionID = (SELECT TOP 1 EIA.ID FROM tblEIAPADDRegion EIA WHERE Name LIKE 'PADD 1') FROM tblState S WHERE S.ID = 30
UPDATE tblState Set EIAPADDRegionID = (SELECT TOP 1 EIA.ID FROM tblEIAPADDRegion EIA WHERE Name LIKE 'PADD 1') FROM tblState S WHERE S.ID = 31
UPDATE tblState Set EIAPADDRegionID = (SELECT TOP 1 EIA.ID FROM tblEIAPADDRegion EIA WHERE Name LIKE 'PADD 3') FROM tblState S WHERE S.ID = 32
UPDATE tblState Set EIAPADDRegionID = (SELECT TOP 1 EIA.ID FROM tblEIAPADDRegion EIA WHERE Name LIKE 'PADD 5') FROM tblState S WHERE S.ID = 29
UPDATE tblState Set EIAPADDRegionID = (SELECT TOP 1 EIA.ID FROM tblEIAPADDRegion EIA WHERE Name LIKE 'PADD 1') FROM tblState S WHERE S.ID = 33
UPDATE tblState Set EIAPADDRegionID = (SELECT TOP 1 EIA.ID FROM tblEIAPADDRegion EIA WHERE Name LIKE 'PADD 2') FROM tblState S WHERE S.ID = 36
UPDATE tblState Set EIAPADDRegionID = (SELECT TOP 1 EIA.ID FROM tblEIAPADDRegion EIA WHERE Name LIKE 'PADD 2') FROM tblState S WHERE S.ID = 37
UPDATE tblState Set EIAPADDRegionID = (SELECT TOP 1 EIA.ID FROM tblEIAPADDRegion EIA WHERE Name LIKE 'PADD 5') FROM tblState S WHERE S.ID = 38
UPDATE tblState Set EIAPADDRegionID = (SELECT TOP 1 EIA.ID FROM tblEIAPADDRegion EIA WHERE Name LIKE 'PADD 1') FROM tblState S WHERE S.ID = 39
UPDATE tblState Set EIAPADDRegionID = (SELECT TOP 1 EIA.ID FROM tblEIAPADDRegion EIA WHERE Name LIKE 'PADD 1') FROM tblState S WHERE S.ID = 40
UPDATE tblState Set EIAPADDRegionID = (SELECT TOP 1 EIA.ID FROM tblEIAPADDRegion EIA WHERE Name LIKE 'PADD 3') FROM tblState S WHERE S.ID = 41
UPDATE tblState Set EIAPADDRegionID = (SELECT TOP 1 EIA.ID FROM tblEIAPADDRegion EIA WHERE Name LIKE 'PADD 2') FROM tblState S WHERE S.ID = 42
UPDATE tblState Set EIAPADDRegionID = (SELECT TOP 1 EIA.ID FROM tblEIAPADDRegion EIA WHERE Name LIKE 'PADD 2') FROM tblState S WHERE S.ID = 43
UPDATE tblState Set EIAPADDRegionID = (SELECT TOP 1 EIA.ID FROM tblEIAPADDRegion EIA WHERE Name LIKE 'PADD 3') FROM tblState S WHERE S.ID = 44
UPDATE tblState Set EIAPADDRegionID = (SELECT TOP 1 EIA.ID FROM tblEIAPADDRegion EIA WHERE Name LIKE 'PADD 4') FROM tblState S WHERE S.ID = 45
UPDATE tblState Set EIAPADDRegionID = (SELECT TOP 1 EIA.ID FROM tblEIAPADDRegion EIA WHERE Name LIKE 'PADD 3') FROM tblState S WHERE S.ID = 47
UPDATE tblState Set EIAPADDRegionID = (SELECT TOP 1 EIA.ID FROM tblEIAPADDRegion EIA WHERE Name LIKE 'PADD 1') FROM tblState S WHERE S.ID = 46
UPDATE tblState Set EIAPADDRegionID = (SELECT TOP 1 EIA.ID FROM tblEIAPADDRegion EIA WHERE Name LIKE 'PADD 5') FROM tblState S WHERE S.ID = 48
UPDATE tblState Set EIAPADDRegionID = (SELECT TOP 1 EIA.ID FROM tblEIAPADDRegion EIA WHERE Name LIKE 'PADD 2') FROM tblState S WHERE S.ID = 50
UPDATE tblState Set EIAPADDRegionID = (SELECT TOP 1 EIA.ID FROM tblEIAPADDRegion EIA WHERE Name LIKE 'PADD 3') FROM tblState S WHERE S.ID = 49
UPDATE tblState Set EIAPADDRegionID = (SELECT TOP 1 EIA.ID FROM tblEIAPADDRegion EIA WHERE Name LIKE 'PADD 4') FROM tblState S WHERE S.ID = 51
GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute the min decimal value between the two provided values
/***********************************/
CREATE FUNCTION [dbo].[fnMinDecimal](@first decimal(18,6), @second decimal(18, 6)) RETURNS decimal(18,3) AS BEGIN
	SELECT @first = isnull(@first, 0), @second = isnull(@second, 0)
	RETURN (CASE WHEN @first < @second THEN @first ELSE @second END)
END

GO
GRANT EXECUTE ON fnMinDecimal TO dispatchcrude_iis_acct
GO

CREATE TABLE tblFuelPrice
(
  ID int identity(1, 1) NOT NULL CONSTRAINT PK_FuelPrices PRIMARY KEY
, Price money NOT NULL
, EIAPADDRegionID int NOT NULL 
	CONSTRAINT FK_FuelPrice_EIDPADDRegion FOREIGN KEY REFERENCES tblEIAPADDRegion(ID)
, EffectiveDate date
, CreateDateUTC smalldatetime NOT NULL CONSTRAINT DF_FuelPrices_CreateDateUTC DEFAULT (getutcdate())
, CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_FuelPrices_CreatedByUser DEFAULT (SUSER_NAME())
, LastChangeDateUTC smalldatetime NULL
, LastChangedByUser varchar(100) NULL
)
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblFuelPrice TO dispatchcrude_iis_acct
GO
CREATE UNIQUE INDEX udxFuelPrice_EIAPADDRegion_EffectiveDate ON tblFuelPrice(EIAPADDRegionID, EffectiveDate)
GO

CREATE TABLE tblShipperFuelSurchargeRates
(
  ID int identity(1, 1) NOT NULL CONSTRAINT PK_ShipperFuelSurchargeRates PRIMARY KEY
, ShipperID int NOT NULL CONSTRAINT FK_ShipperFuelSurchargeRates_Customer FOREIGN KEY REFERENCES tblCustomer(ID)
, FuelPriceFloor money NOT NULL
, IntervalAmount money NOT NULL
, IncrementAmount money NOT NULL
, EffectiveDate smalldatetime NOT NULL
, CreateDateUTC smalldatetime NOT NULL CONSTRAINT DF_ShipperFuelSurchargeRates_CreateDateUTC DEFAULT (getutcdate())
, CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_ShipperFuelSurchargeRates_CreatedByUser DEFAULT (SUSER_NAME())
, LastChangeDateUTC smalldatetime NULL
, LastChangedByUser varchar(100) NULL
)
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblShipperFuelSurchargeRates TO dispatchcrude_iis_acct
GO
CREATE UNIQUE INDEX udxShipperFuelSurchargeRates ON tblShipperFuelSurchargeRates(ShipperID, EffectiveDate)
GO

CREATE TABLE tblCarrierFuelSurchargeRates
(
  ID int identity(1, 1) NOT NULL CONSTRAINT PK_CarrierFuelSurchargeRates PRIMARY KEY
, CarrierID int NOT NULL CONSTRAINT FK_CarrierFuelSurchargeRates_Customer FOREIGN KEY REFERENCES tblCarrier(ID)
, FuelPriceFloor money NOT NULL
, IntervalAmount money NOT NULL
, IncrementAmount money NOT NULL
, EffectiveDate smalldatetime NOT NULL
, CreateDateUTC smalldatetime NOT NULL CONSTRAINT DF_CarrierFuelSurchargeRates_CreateDateUTC DEFAULT (getutcdate())
, CreatedByUser varchar(100) NOT NULL CONSTRAINT DF_CarrierFuelSurchargeRates_CreatedByUser DEFAULT (SUSER_NAME())
, LastChangeDateUTC smalldatetime NULL
, LastChangedByUser varchar(100) NULL
)
GO
GRANT SELECT, INSERT, UPDATE, DELETE ON tblCarrierFuelSurchargeRates TO dispatchcrude_iis_acct
GO
CREATE UNIQUE INDEX udxCarrierFuelSurchargeRates ON tblCarrierFuelSurchargeRates(CarrierID, EffectiveDate)
GO

/***********************************/
-- Date Created: 3 Nov 2014
-- Author: Kevin Alons
-- Purpose: compute the appropriate FuelPrice for the specified Origin/EffectiveDate
/***********************************/
CREATE FUNCTION fnFuelPriceForOrigin(@OriginID int, @EffectiveDate smalldatetime) RETURNS money AS
BEGIN
	DECLARE @ret money
	SELECT TOP 1 @ret = FP.Price 
	FROM tblOrigin O 
	JOIN tblState S ON S.ID = O.StateID
	JOIN tblFuelPrice FP ON FP.EIAPADDRegionID = S.EIAPADDRegionID
	WHERE O.ID = @OriginID
	  AND FP.EffectiveDate <= @EffectiveDate ORDER BY FP.EffectiveDate DESC
	RETURN @ret
END
GO
GRANT EXECUTE ON fnFuelPriceForOrigin TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 3 Nov 2014
-- Author: Kevin Alons
-- Purpose: compute the appropriate shipperFuelSurcharge for the specified parameters
/***********************************/
CREATE FUNCTION fnShipperFuelSurchargeRate(@OriginID int, @ShipperID int, @EffectiveDate smalldatetime) RETURNS money AS
BEGIN
	DECLARE @ret money, @fuelPriceFloor money, @intervalAmount money, @incrementAmount money, @pricediff money	
	
	SELECT TOP 1 @fuelPriceFloor = FuelPriceFloor, @intervalAmount = IntervalAmount, @incrementAmount = IncrementAmount
		, @pricediff = dbo.fnMaxDecimal(0, dbo.fnFuelPriceForOrigin(@OriginID, @effectiveDate) - fuelPriceFloor)
	FROM tblShipperFuelSurchargeRates 
	WHERE ShipperID = @ShipperID AND EffectiveDate <= @EffectiveDate
	ORDER BY EffectiveDate DESC
	
	SELECT @ret = @incrementAmount * (round(@pricediff / @intervalAmount, 0) + CASE WHEN @pricediff % @intervalAmount > 0 THEN 1 ELSe 0 END)
	
	RETURN @ret
END

GO
GRANT EXECUTE ON fnShipperFuelSurchargeRate TO dispatchcrude_iis_acct
GO

/***********************************/
-- Date Created: 3 Nov 2014
-- Author: Kevin Alons
-- Purpose: compute the appropriate CarrierFuelSurcharge for the specified parameters
/***********************************/
CREATE FUNCTION fnCarrierFuelSurchargeRate(@OriginID int, @CarrierID int, @EffectiveDate smalldatetime) RETURNS money AS
BEGIN
	DECLARE @ret money, @fuelPriceFloor money, @intervalAmount money, @incrementAmount money, @pricediff money	
	
	SELECT TOP 1 @fuelPriceFloor = FuelPriceFloor, @intervalAmount = IntervalAmount, @incrementAmount = IncrementAmount
		, @pricediff = dbo.fnMaxDecimal(0, dbo.fnFuelPriceForOrigin(@OriginID, @effectiveDate) - fuelPriceFloor)
	FROM tblCarrierFuelSurchargeRates 
	WHERE CarrierID = @CarrierID AND EffectiveDate <= @EffectiveDate
	ORDER BY EffectiveDate DESC
	
	SELECT @ret = @incrementAmount * (round(@pricediff / @intervalAmount, 0) + CASE WHEN @pricediff % @intervalAmount > 0 THEN 1 ELSe 0 END)
	
	RETURN @ret
END

GO
GRANT EXECUTE ON fnCarrierFuelSurchargeRate TO dispatchcrude_iis_acct
GO

ALTER TABLE tblCarrierRates ADD SplitLoadRate money NULL
GO
ALTER TABLE tblCustomerRates ADD SplitLoadRate money NULL
GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 1 Jun 2013
-- Description:	provide a way to "clone" an existing set of Assessorial rates
-- =============================================
ALTER PROCEDURE [dbo].[spCloneCarrierAccessorialRates](@ID int, @UserName varchar(100)) AS
BEGIN
	DELETE FROM tblCarrierRates WHERE CarrierID IS NULL AND RegionID IS NULL
	INSERT INTO tblCarrierRates (CarrierID, RegionID, ChainupFee, WaitFee, RejectionFee, RerouteFee, H2SRate, FuelSurcharge, SplitLoadRate
		, WaitFeeSubUnitID, WaitFeeRoundingTypeID, CreateDateUTC, CreatedByUser)
		
		SELECT 0, 0, ChainupFee, WaitFee, RejectionFee, RerouteFee, H2SRate, FuelSurcharge, SplitLoadRate
			, WaitFeeSubUnitID, WaitFeeRoundingTypeID, GETUTCDATE(), @UserName
		FROM tblCarrierRates WHERE ID = @ID
END

GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 1 Jun 2013
-- Description:	provide a way to "clone" an existing set of Assessorial rates
-- =============================================
ALTER PROCEDURE [dbo].[spCloneCustomerAccessorialRates](@ID int, @UserName varchar(100)) AS
BEGIN
	DELETE FROM tblCustomerRates WHERE CustomerID IS NULL AND RegionID IS NULL
	INSERT INTO tblCustomerRates (CustomerID, RegionID, ChainupFee, WaitFee, RejectionFee, RerouteFee, H2SRate, FuelSurcharge, SplitLoadRate
		, WaitFeeSubUnitID, WaitFeeRoundingTypeID, CreateDateUTC, CreatedByUser)
		
		SELECT 0, 0, ChainupFee, WaitFee, RejectionFee, RerouteFee, H2SRate, FuelSurcharge, SplitLoadRate
			, WaitFeeSubUnitID, WaitFeeRoundingTypeID, GETUTCDATE(), @UserName
		FROM tblCustomerRates WHERE ID = @ID
END

GO 
-----------------------------------------------
EXECUTE sp_rename N'dbo.tblOrderInvoiceCarrier.FuelSurcharge', N'tmp_FuelSurchargeFee', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.tblOrderInvoiceCarrier.tmp_FuelSurchargeFee', N'FuelSurchargeFee', 'COLUMN' 
GO
ALTER TABLE dbo.tblOrderInvoiceCarrier ADD FuelSurchargeRate money NULL
GO
ALTER TABLE dbo.tblOrderInvoiceCarrier ADD SplitLoadFee money NULL
GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Carrier "Settlement" $$ values to an Audited order
/***********************************/
ALTER PROCEDURE [dbo].[spProcessCarrierInvoice]
(
  @ID int
, @UserName varchar(100)
, @ChainupFee smallmoney = NULL
, @OriginWaitFee smallmoney = NULL
, @DestWaitFee smallmoney = NULL
, @RerouteFee smallmoney = NULL
, @RejectionFee smallmoney = NULL
, @H2SFee smallmoney = NULL
, @SplitLoadFee smallmoney = NULL
, @FuelSurchargeFee smallmoney = NULL
, @LoadFee smallmoney = NULL
) AS BEGIN
	DELETE FROM tblOrderInvoiceCarrier WHERE OrderID = @ID
	
	-- all Units and Rates are first normalized to the Order.OriginUOM then consistent processing
	-- and persisted in the Invoice (Settlement) record in this Order.OriginUOM 
	INSERT INTO tblOrderInvoiceCarrier (OrderID
		, ChainupFee, RerouteFee
		, WaitFeeSubUnitID, WaitFeeRoundingTypeID, BillableOriginWaitMinutes, BillableDestWaitMinutes, OriginWaitRate, DestinationWaitRate, OriginWaitFee, DestWaitFee
		, UomID, MinSettlementUnits, Units
		, RouteRate, H2SRate, TaxRate
		, RejectionFee, LoadFee, H2SFee, FuelSurchargeRate, FuelSurchargeFee, SplitLoadFee
		, TotalFee, CreateDateUTC, CreatedByUser)
	SELECT D.ID
		, ChainupFee, RerouteFee
		, WaitFeeSubUnitID, WaitFeeRoundingTypeID, BillableOriginWaitMinutes, BillableDestWaitMinutes, OriginWaitRate, DestinationWaitRate, OriginWaitFee, DestWaitFee
		, OrderOriginUomID, MinSettlementUnits, ActualUnits
		, RouteRate, H2SRate, TaxRate
		, RejectionFee, LoadFee, H2SFee, FuelSurchargeRate, FuelSurchargeFee, SplitLoadFee
		, TotalFee = RejectionFee + ChainupFee + RerouteFee + OriginWaitFee + DestWaitFee + H2SFee + LoadFee + FuelSurchargeFee + SplitLoadFee
		, GETUTCDATE(), @UserName
	FROM (
		-- compute the actual fees or use the manual overide fee values (if provided)
		SELECT ID
			, coalesce(@ChainupFee, Chainup * ChainupFee, 0) AS ChainupFee
			, coalesce(@RerouteFee, RerouteFee * RerouteCount, 0) AS RerouteFee
			, WaitFeeSubUnitID
			, WaitFeeRoundingTypeID
			, cast(round(BillableOriginWaitHours * 60, 0) as int) AS BillableOriginWaitMinutes
			, cast(round(BillableDestWaitHours * 60, 0) as int) AS BillableDestWaitMinutes
			, OriginWaitRate
			, DestinationWaitRate
			, coalesce(@OriginWaitFee, BillableOriginWaitHours * OriginWaitRate, 0) AS OriginWaitFee
			, coalesce(@DestWaitFee, BillableDestWaitHours * DestinationWaitRate, 0) AS DestWaitFee
			, coalesce(@RejectionFee, Rejected * RejectionRate, 0) AS RejectionFee
			, H2SRate
			, MinSettlementUnits
			, ActualUnits
			, coalesce(@H2SFee, H2S * dbo.fnMaxDecimal(MinSettlementUnits, ActualUnits) * H2SRate, 0) AS H2SFee
			, isnull(TaxRate, 0) AS TaxRate
			, isnull(RouteRate, 0) AS RouteRate
			-- if rejected, use 0 AS the Rate
			, round(coalesce(@LoadFee, (1 - Rejected) * dbo.fnMaxDecimal(MinSettlementUnits, ActualUnits) * RouteRate, 0), 4) AS LoadFee
			, SplitLoadFee = coalesce(@SplitLoadFee, SplitLoadFee, 0)
			, FuelSurchargeRate = ISNULL(FuelSurchargeRate, 0)
			, FuelSurchargeFee = coalesce(@FuelSurchargeFee, FuelSurchargeFee, 0)
			, OrderOriginUomID
		FROM (
			-- normalize the Assessorial Rates to Order.OriginUOM + add normalized Route Rate and other time normalization
			SELECT S.ID
				, S.ChainUp
				-- chainupFee is not UOM dependent
				, CR.ChainupFee
				-- reroutefee is not UOM dependent
				, CR.RerouteFee
				, S.RerouteCount
				, CR.WaitFeeSubUnitID
				, CR.WaitFeeRoundingTypeID
				, dbo.fnComputeBillableWaitHours(S.OriginWaitMinutes, CR.WaitFeeSubUnitID, CR.WaitFeeRoundingTypeID) AS BillableOriginWaitHours
				, dbo.fnComputeBillableWaitHours(S.DestWaitMinutes, CR.WaitFeeSubUnitID, CR.WaitFeeRoundingTypeID) AS BillableDestWaitHours
				-- waitFee is not UOM dependent
				, ISNULL(dbo.fnCarrierOriginWaitRate(S.CarrierID, S.OrderDate), CR.WaitFee) AS OriginWaitRate
				, ISNULL(dbo.fnCarrierDestinationWaitRate(S.CarrierID, S.OrderDate), CR.WaitFee) AS DestinationWaitRate
				, S.Rejected
				-- rejectionFee is not based on UOM
				, isnull(dbo.fnCarrierOrderRejectRate(S.CarrierID, S.OrderDate), CR.RejectionFee) AS RejectionRate
				, S.H2S
				-- normalize the Carrier H2SRate for Order.OriginUOM
				, dbo.fnConvertRateUOM(isnull(S.H2S * CR.H2SRate, 0), CR.UomID, S.OrderOriginUomID) AS H2SRate
				, S.TaxRate
				-- normalize the Order.UOM Route Rate for Origin.OriginUOM
				, dbo.fnCarrierRouteRate(S.CarrierID, S.RouteID, S.OrderDate, S.OrderOriginUomID) AS RouteRate
				, S.MinSettlementUnits
				, isnull(S.ActualUnits, 0) AS ActualUnits
				, FuelSurchargeRate 
				, FuelSurchargeFee = FuelSurchargeRate * S.RouteMiles
				, SplitLoadFee = CASE WHEN S.IsSplit = 1 THEN CR.SplitLoadRate ELSE 0 END
				, S.IsSplit
				, S.OrderOriginUomID
			FROM (
				-- get the Order raw data (with Units Normalized to Origin UOM) and matching Carrier Assessorial Rate ID
				SELECT O.ID
					, O.CarrierID
					, O.RouteID
					, O.ChainUp
					-- get the correct SettlementFactor ActualUnits
					, ActualUnits = CASE C.SettlementFactorID WHEN 1 THEN O.OriginGrossUnits WHEN 3 THEN isnull(O.OriginGrossStdUnits, O.OriginGrossUnits) ELSE coalesce(O.OriginNetUnits, O.OriginGrossStdUnits, O.OriginGrossUnits) END 
					, OrderOriginUomID = O.OriginUomID 
					, OriginUomID = OO.UomID 
					, O.RerouteCount
					, O.OriginWaitMinutes
					, O.DestWaitMinutes
					, O.Rejected
					, OO.H2S
					, O.OrderDate
					, CRID = coalesce(CRXX.ID, CRXA.ID, CRAX.ID, CRAA.ID) 
					-- normalize the Carrier MinSettlementUnits for the Order.OriginUOM
					, MinSettlementUnits = isnull(dbo.fnConvertUOM(C.MinSettlementUnits, C.MinSettlementUomID, O.OriginUomID), 0) 
					, OO.TaxRate
					, RouteMiles = O.ActualMiles
					, FuelSurchargeRate = dbo.fnCarrierFuelSurchargeRate(O.OriginID, O.CarrierID, O.OrderDate)
					, IsSplit = CASE WHEN O.Rejected = 0 AND O.TicketCount > 1 THEN 1 ELSE 0 END
				FROM dbo.viewOrderExportFull O
				JOIN tblOrigin OO ON OO.ID = O.OriginID
				JOIN tblCarrier C ON C.ID = O.CarrierID
				LEFT JOIN tblCarrierRates CRXX ON CRXX.CarrierID = O.CarrierID AND CRXX.RegionID = OO.RegionID
				LEFT JOIN tblCarrierRates CRXA ON CRXA.CarrierID = O.CarrierID AND CRXA.RegionID = -1
				LEFT JOIN tblCarrierRates CRAX ON CRAX.CarrierID = -1 AND CRAX.RegionID = OO.RegionID
				LEFT JOIN tblCarrierRates CRAA ON CRAA.CarrierID = -1 AND CRAA.RegionID = -1
				WHERE O.ID = @ID
			) S
			LEFT JOIN tblCarrierRates CR ON CR.ID = S.CRID
		) SS
	) D
END

GO
-------------------------------
EXECUTE sp_rename N'dbo.tblOrderInvoiceCustomer.FuelSurcharge', N'tmp_FuelSurchargeFee', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.tblOrderInvoiceCustomer.tmp_FuelSurchargeFee', N'FuelSurchargeFee', 'COLUMN' 
GO
ALTER TABLE dbo.tblOrderInvoiceCustomer ADD FuelSurchargeRate money NULL
GO
ALTER TABLE dbo.tblOrderInvoiceCustomer ADD SplitLoadFee money NULL
GO

/***********************************/
-- Date Created: 2 Jun 2013
-- Author: Kevin Alons
-- Purpose: compute and add the various Customer "Settlement" $$ values to an Audited order
/***********************************/
ALTER PROCEDURE [dbo].[spProcessCustomerInvoice]
(
  @ID int
, @UserName varchar(100)
, @ChainupFee smallmoney = NULL
, @OriginWaitFee smallmoney = NULL
, @DestWaitFee smallmoney = NULL
, @RerouteFee smallmoney = NULL
, @RejectionFee smallmoney = NULL
, @H2SFee smallmoney = NULL
, @SplitLoadFee smallmoney = NULL
, @FuelSurchargeFee smallmoney = NULL
, @LoadFee smallmoney = NULL
) AS BEGIN
	DELETE FROM tblOrderInvoiceCustomer WHERE OrderID = @ID
	
	-- all Units and Rates are first normalized to the Order.OriginUOM then consistent processing
	-- and persisted in the Invoice (Settlement) record in this Order.OriginUOM 
	INSERT INTO tblOrderInvoiceCustomer (OrderID
		, ChainupFee, RerouteFee
		, WaitFeeSubUnitID, WaitFeeRoundingTypeID, BillableOriginWaitMinutes, BillableDestWaitMinutes, OriginWaitRate, DestinationWaitRate, OriginWaitFee, DestWaitFee
		, UomID, MinSettlementUnits, Units
		, RouteRate, H2SRate, TaxRate
		, RejectionFee, LoadFee, H2SFee, FuelSurchargeRate, FuelSurchargeFee, SplitLoadFee
		, TotalFee, CreateDateUTC, CreatedByUser)
	SELECT D.ID
		, ChainupFee, RerouteFee
		, WaitFeeSubUnitID, WaitFeeRoundingTypeID, BillableOriginWaitMinutes, BillableDestWaitMinutes, OriginWaitRate, DestinationWaitRate, OriginWaitFee, DestWaitFee
		, OrderOriginUomID, MinSettlementUnits, ActualUnits
		, RouteRate, H2SRate, TaxRate
		, RejectionFee, LoadFee, H2SFee, FuelSurchargeRate, FuelSurchargeFee, SplitLoadFee
		, TotalFee = RejectionFee + ChainupFee + RerouteFee + OriginWaitFee + DestWaitFee + H2SFee + LoadFee + FuelSurchargeFee + SplitLoadFee
		, GETUTCDATE(), @UserName
	FROM (
		-- compute the actual fees or use the manual overide fee values (if provided)
		SELECT ID
			, coalesce(@ChainupFee, Chainup * ChainupFee, 0) AS ChainupFee
			, coalesce(@RerouteFee, RerouteFee * RerouteCount, 0) AS RerouteFee
			, WaitFeeSubUnitID
			, WaitFeeRoundingTypeID
			, cast(round(BillableOriginWaitHours * 60, 0) as int) AS BillableOriginWaitMinutes
			, cast(round(BillableDestWaitHours * 60, 0) as int) AS BillableDestWaitMinutes
			, OriginWaitRate
			, DestinationWaitRate
			, coalesce(@OriginWaitFee, BillableOriginWaitHours * OriginWaitRate, 0) AS OriginWaitFee
			, coalesce(@DestWaitFee, BillableDestWaitHours * DestinationWaitRate, 0) AS DestWaitFee
			, coalesce(@RejectionFee, Rejected * RejectionRate, 0) AS RejectionFee
			, H2SRate
			, MinSettlementUnits
			, ActualUnits
			, coalesce(@H2SFee, H2S * dbo.fnMaxDecimal(MinSettlementUnits, ActualUnits) * H2SRate, 0) AS H2SFee
			, isnull(TaxRate, 0) AS TaxRate
			, isnull(RouteRate, 0) AS RouteRate
			-- if rejected, use 0 AS the Rate
			, round(coalesce(@LoadFee, (1 - Rejected) * dbo.fnMaxDecimal(MinSettlementUnits, ActualUnits) * RouteRate, 0), 4) AS LoadFee
			, SplitLoadFee = coalesce(@SplitLoadFee, SplitLoadFee, 0)
			, FuelSurchargeRate = ISNULL(FuelSurchargeRate, 0)
			, FuelSurchargeFee = coalesce(@FuelSurchargeFee, FuelSurchargeFee, 0)
			, OrderOriginUomID
		FROM (
			-- normalize the Assessorial Rates to Order.OriginUOM + add normalized Route Rate and other time normalization
			SELECT S.ID
				, S.ChainUp
				-- chainupFee is not UOM dependent
				, CR.ChainupFee
				-- reroutefee is not UOM dependent
				, CR.RerouteFee
				, S.RerouteCount
				, CR.WaitFeeSubUnitID
				, CR.WaitFeeRoundingTypeID
				, dbo.fnComputeBillableWaitHours(S.OriginWaitMinutes, CR.WaitFeeSubUnitID, CR.WaitFeeRoundingTypeID) AS BillableOriginWaitHours
				, dbo.fnComputeBillableWaitHours(S.DestWaitMinutes, CR.WaitFeeSubUnitID, CR.WaitFeeRoundingTypeID) AS BillableDestWaitHours
				-- waitFee is not UOM dependent
				, ISNULL(dbo.fnCustomerOriginWaitRate(S.CustomerID, S.OrderDate), CR.WaitFee) AS OriginWaitRate
				, ISNULL(dbo.fnCustomerDestinationWaitRate(S.CustomerID, S.OrderDate), CR.WaitFee) AS DestinationWaitRate
				, S.Rejected
				-- rejectionFee is not based on UOM
				, isnull(dbo.fnCustomerOrderRejectRate(S.CustomerID, S.OrderDate), CR.RejectionFee) AS RejectionRate
				, S.H2S
				-- normalize the Customer H2SRate for Order.OriginUOM
				, dbo.fnConvertRateUOM(isnull(S.H2S * CR.H2SRate, 0), CR.UomID, S.OrderOriginUomID) AS H2SRate
				, S.TaxRate
				-- normalize the Order.UOM Route Rate for Origin.OriginUOM
				, dbo.fnCustomerRouteRate(S.CustomerID, S.RouteID, S.OrderDate, S.OrderOriginUomID) AS RouteRate
				, S.MinSettlementUnits
				, isnull(S.ActualUnits, 0) AS ActualUnits
				, FuelSurchargeRate 
				, FuelSurchargeFee = FuelSurchargeRate * S.RouteMiles
				, SplitLoadFee = CASE WHEN S.IsSplit = 1 THEN CR.SplitLoadRate ELSE 0 END
				, S.IsSplit
				, S.OrderOriginUomID
			FROM (
				-- get the Order raw data (with Units Normalized to Origin UOM) and matching Customer Assessorial Rate ID
				SELECT O.ID
					, O.CustomerID
					, O.RouteID
					, O.ChainUp
					-- get the correct SettlementFactor ActualUnits
					, ActualUnits = CASE C.SettlementFactorID WHEN 1 THEN O.OriginGrossUnits WHEN 3 THEN isnull(O.OriginGrossStdUnits, O.OriginGrossUnits) ELSE coalesce(O.OriginNetUnits, O.OriginGrossStdUnits, O.OriginGrossUnits) END 
					, OrderOriginUomID = O.OriginUomID 
					, OriginUomID = OO.UomID 
					, O.RerouteCount
					, O.OriginWaitMinutes
					, O.DestWaitMinutes
					, O.Rejected
					, OO.H2S
					, O.OrderDate
					, CRID = coalesce(CRXX.ID, CRXA.ID, CRAX.ID, CRAA.ID) 
					-- normalize the Customer MinSettlementUnits for the Order.OriginUOM
					, MinSettlementUnits = isnull(dbo.fnConvertUOM(C.MinSettlementUnits, C.MinSettlementUomID, O.OriginUomID), 0) 
					, OO.TaxRate
					, RouteMiles = O.ActualMiles
					, FuelSurchargeRate = dbo.fnShipperFuelSurchargeRate(O.OriginID, O.CustomerID, O.OrderDate)
					, IsSplit = CASE WHEN O.Rejected = 0 AND O.TicketCount > 1 THEN 1 ELSE 0 END
				FROM dbo.viewOrderExportFull O
				JOIN tblOrigin OO ON OO.ID = O.OriginID
				JOIN tblCustomer C ON C.ID = O.CustomerID
				LEFT JOIN tblCustomerRates CRXX ON CRXX.CustomerID = O.CustomerID AND CRXX.RegionID = OO.RegionID
				LEFT JOIN tblCustomerRates CRXA ON CRXA.CustomerID = O.CustomerID AND CRXA.RegionID = -1
				LEFT JOIN tblCustomerRates CRAX ON CRAX.CustomerID = -1 AND CRAX.RegionID = OO.RegionID
				LEFT JOIN tblCustomerRates CRAA ON CRAA.CustomerID = -1 AND CRAA.RegionID = -1
				WHERE O.ID = @ID
			) S
			LEFT JOIN tblCustomerRates CR ON CR.ID = S.CRID
		) SS
	) D
END

GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return concatenated OrderTicket details for a single order
/***********************************/
ALTER FUNCTION [dbo].[fnOrderTicketDetails](@OrderID int, @fieldName varchar(50), @delimiter varchar(10)) RETURNS varchar(max) AS BEGIN
	DECLARE @ret varchar(max)
	
	-- Retrieve the OrderTicket Details that are specified by @OrderID | fieldName
	DECLARE @ID int
	SELECT @ID = min(ID) FROM dbo.tblOrderTicket WHERE OrderID = @OrderID AND ID > 0
	WHILE @ID IS NOT NULL
	BEGIN
		SELECT @ret = isnull(@ret + @delimiter, '') 
			+ CASE WHEN @fieldName LIKE 'TicketNums' THEN OT.CarrierTicketNum
					WHEN @fieldName LIKE 'TankNums' THEN OT.TankNum END
		FROM tblOrderTicket OT
		WHERE OT.ID = @ID AND DeleteDateUTC IS NULL
		SELECT @ID = min(ID) FROM tblOrderTicket WHERE OrderID = @OrderID AND ID > @ID
	END

	RETURN (@ret)
END

GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details + FINANCIAL INFO into a single view
/***********************************/
ALTER VIEW [dbo].[viewOrder_Financial_Carrier] AS 
	SELECT OE.* 
		, dbo.fnOrderTicketDetails(OE.ID, 'TicketNums', '<br/>') AS TicketNums
		, dbo.fnOrderTicketDetails(OE.ID, 'TankNums', '<br/>') AS TankNums
		, dbo.fnRerouteDetails(OE.ID, 'PreviousDestinations', '<br/>') AS PreviousDestinations
		, dbo.fnRerouteDetails(OE.ID, 'RerouteUsers', '<br/>') AS RerouteUsers
		, dbo.fnRerouteDetails(OE.ID, 'RerouteDates', '<br/>') AS RerouteDates
		, dbo.fnRerouteDetails(OE.ID, 'RerouteNotes', '<br/>') AS RerouteNotes
		, InvoiceRatesAppliedDate = dbo.fnUTC_To_Local(ISNULL(OIC.LastChangeDateUTC, OIC.CreateDateUTC), OE.OriginTimeZoneID, OE.OriginUseDST)
		, OIC.BatchID
		, SB.BatchNum AS InvoiceBatchNum
		, OIC.ChainupFee AS InvoiceChainupFee
		, OIC.RerouteFee AS InvoiceRerouteFee
		, OIC.BillableOriginWaitMinutes AS InvoiceOriginBillableWaitMinutes
		, OIC.BillableDestWaitMinutes AS InvoiceDestBillableWaitMinutes
		, isnull(OIC.BillableOriginWaitMinutes, 0) + ISNULL(OIC.BillableDestWaitMinutes, 0) AS InvoiceTotalBillableWaitMinutes
		, isnull(WFSU.Name, 'None') AS InvoiceWaitFeeSubUnit
		, isnull(WFRT.Name, 'None') AS InvoiceWaitFeeRoundingType
		, OIC.OriginWaitRate AS InvoiceOriginWaitRate
		, OIC.DestinationWaitRate AS InvoiceDestinationWaitRate
		, OIC.OriginWaitFee AS InvoiceOriginWaitFee
		, OIC.DestWaitFee AS InvoiceDestWaitFee
		, ISNULL(OIC.OriginWaitFee, 0) + ISNULL(OIC.DestWaitFee, 0) AS InvoiceTotalWaitFee
		, OIC.RejectionFee AS InvoiceRejectionFee
		, OIC.H2SRate AS InvoiceH2SRate
		, OIC.H2SFee AS InvoiceH2SFee
		, OIC.TaxRate AS InvoiceTaxRate
		, U.Name AS InvoiceUom
		, U.Abbrev AS InvoiceUomShort
		, OIC.MinSettlementUnits AS InvoiceMinSettlementUnits
		, OIC.Units AS InvoiceUnits
		, OIC.RouteRate AS InvoiceRouteRate
		, OIC.LoadFee AS InvoiceLoadFee
		, OIC.TotalFee AS InvoiceTotalFee
		, InvoiceFuelSurchargeRate = OIC.FuelSurchargeRate
		, InvoiceFuelSurchargeFee = OIC.FuelSurchargeFee
		, InvoiceSplitLoadFee = OIC.SplitLoadFee
	FROM dbo.viewOrderExportFull OE
	LEFT JOIN dbo.tblOrderInvoiceCarrier OIC ON OIC.OrderID = OE.ID
	LEFT JOIN dbo.tblCarrierSettlementBatch SB ON SB.ID = OIC.BatchID
	LEFT JOIN dbo.tblUom U ON U.ID = OIC.UomID	
	LEFT JOIN dbo.tblWaitFeeSubUnit WFSU ON WFSU.ID = OIC.WaitFeeSubUnitID
	LEFT JOIN dbo.tblWaitFeeRoundingType WFRT ON WFRT.ID = OIC.WaitFeeRoundingTypeID
	WHERE OE.StatusID NOT IN (-10) -- don't include "Generated" orders (only Assigned+)

GO

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Order JOIN OrderTicket + computed Reroute details + FINANCIAL INFO into a single view
/***********************************/
ALTER VIEW [dbo].[viewOrder_Financial_Customer] AS 
	SELECT OE.* 
		, dbo.fnOrderTicketDetails(OE.ID, 'TicketNums', '<br/>') AS TicketNums
		, dbo.fnOrderTicketDetails(OE.ID, 'TankNums', '<br/>') AS TankNums
		, dbo.fnRerouteDetails(OE.ID, 'PreviousDestinations', '<br/>') AS PreviousDestinations
		, dbo.fnRerouteDetails(OE.ID, 'RerouteUsers', '<br/>') AS RerouteUsers
		, dbo.fnRerouteDetails(OE.ID, 'RerouteDates', '<br/>') AS RerouteDates
		, dbo.fnRerouteDetails(OE.ID, 'RerouteNotes', '<br/>') AS RerouteNotes
		, InvoiceRatesAppliedDate = dbo.fnUTC_To_Local(ISNULL(OIC.LastChangeDateUTC, OIC.CreateDateUTC), OE.OriginTimeZoneID, OE.OriginUseDST)
		, OIC.BatchID
		, SB.BatchNum AS InvoiceBatchNum
		, OIC.ChainupFee AS InvoiceChainupFee
		, OIC.RerouteFee AS InvoiceRerouteFee
		, OIC.BillableOriginWaitMinutes AS InvoiceOriginBillableWaitMinutes
		, OIC.BillableDestWaitMinutes AS InvoiceDestBillableWaitMinutes
		, isnull(OIC.BillableOriginWaitMinutes, 0) + ISNULL(OIC.BillableDestWaitMinutes, 0) AS InvoiceTotalBillableWaitMinutes
		, isnull(WFSU.Name, 'None') AS InvoiceWaitFeeSubUnit
		, isnull(WFRT.Name, 'None') AS InvoiceWaitFeeRoundingType
		, OIC.OriginWaitRate AS InvoiceOriginWaitRate
		, OIC.DestinationWaitRate AS InvoiceDestinationWaitRate
		, OIC.OriginWaitFee AS InvoiceOriginWaitFee
		, OIC.DestWaitFee AS InvoiceDestWaitFee
		, ISNULL(OIC.OriginWaitFee, 0) + ISNULL(OIC.DestWaitFee, 0) AS InvoiceTotalWaitFee
		, OIC.RejectionFee AS InvoiceRejectionFee
		, OIC.H2SRate AS InvoiceH2SRate
		, OIC.H2SFee AS InvoiceH2SFee
		, OIC.TaxRate AS InvoiceTaxRate
		, U.Name AS InvoiceUom
		, U.Abbrev AS InvoiceUomShort
		, OIC.MinSettlementUnits AS InvoiceMinSettlementUnits
		, OIC.Units AS InvoiceUnits
		, OIC.RouteRate AS InvoiceRouteRate
		, OIC.LoadFee AS InvoiceLoadFee
		, OIC.TotalFee AS InvoiceTotalFee
		, InvoiceFuelSurchargeRate = OIC.FuelSurchargeRate
		, InvoiceFuelSurchargeFee = OIC.FuelSurchargeFee
		, InvoiceSplitLoadFee = OIC.SplitLoadFee
	FROM dbo.viewOrderExportFull OE
	LEFT JOIN dbo.tblOrderInvoiceCustomer OIC ON OIC.OrderID = OE.ID
	LEFT JOIN dbo.tblCustomerSettlementBatch SB ON SB.ID = OIC.BatchID
	LEFT JOIN dbo.tblUom U ON U.ID = OIC.UomID	
	LEFT JOIN dbo.tblWaitFeeSubUnit WFSU ON WFSU.ID = OIC.WaitFeeSubUnitID
	LEFT JOIN dbo.tblWaitFeeRoundingType WFRT ON WFRT.ID = OIC.WaitFeeRoundingTypeID
	WHERE OE.StatusID NOT IN (-10) -- don't include "Generated" orders (only Assigned+)

GO
----------------------------------------

/***********************************/
-- Date Created: 9 Mar 2013
-- Author: Kevin Alons
-- Purpose: return Report Center Order data
/***********************************/
ALTER VIEW [dbo].[viewReportCenter_Orders] AS
	SELECT O.*
		, ShipperH2SRate = IOCS.H2SRate
		, ShipperH2SFee = IOCS.H2SFee
		, ShipperChainupFee = IOCS.ChainupFee
		, ShipperTaxRate = IOCS.TaxRate
		, ShipperRerouteFee = IOCS.RerouteFee
		, ShipperRejectFee = IOCS.RejectionFee
		, ShipperOriginWaitRate = IOCS.OriginWaitRate
		, ShipperOriginWaitFee = IOCS.OriginWaitFee
		, ShipperDestWaitRate = IOCS.DestinationWaitRate
		, ShipperDestWaitFee = IOCS.DestWaitFee
		, ShipperTotalWaitFee = cast(IOCS.OriginWaitFee as int) + cast(IOCS.DestWaitFee as int)
		, ShipperTotalFee = IOCS.TotalFee
		, ShipperSettlementUomID = IOCS.UomID
		, ShipperSettlementUom = SUOM.Abbrev
		, ShipperMinSettlementUnits = IOCS.MinSettlementUnits
		, ShipperRouteRate = IOCS.RouteRate
		, ShipperRouteFee = IOCS.LoadFee
		, ShipperBatchNum = SSB.BatchNum
		, ShipperSplitLoadFee = IOCS.SplitLoadFee
		, ShipperFuelSurchargeRate = IOCS.FuelSurchargeRate
		, ShipperFuelSurchargeFee = IOCS.FuelSurchargeFee
		, ShipperUnits = IOCS.Units
		, CarrierH2SRate = IOCC.H2SRate
		, CarrierH2SFee = IOCC.H2SFee
		, CarrierChainupFee = IOCC.ChainupFee
		, CarrierTaxRate = IOCC.TaxRate
		, CarrierRerouteFee = IOCC.RerouteFee
		, CarrierRejectFee = IOCC.RejectionFee
		, CarrierOriginWaitRate = IOCC.OriginWaitRate
		, CarrierOriginWaitFee = IOCC.OriginWaitFee
		, CarrierDestWaitRate = IOCC.DestinationWaitRate
		, CarrierDestWaitFee = IOCC.DestWaitFee
		, CarrierTotalWaitFee = cast(IOCC.OriginWaitFee as int) + cast(IOCC.DestWaitFee as int)
		, CarrierTotalFee = IOCC.TotalFee
		, CarrierSettlementUomID = IOCC.UomID
		, CarrierSettlementUom = CUOM.Abbrev
		, CarrierMinSettlementUnits = IOCC.MinSettlementUnits
		, CarrierRouteRate = IOCC.RouteRate
		, CarrierRouteFee = IOCC.LoadFee
		, CarrierBatchNum = CSB.BatchNum
		, CarrierSplitLoadFee = IOCC.SplitLoadFee
		, CarrierFuelSurchargeRate = IOCC.FuelSurchargeRate
		, CarrierFuelSurchargeFee = IOCC.FuelSurchargeFee
		, CarrierUnits = IOCC.Units
		, OriginGpsLatLon = ltrim(DLO.Lat) + ',' + ltrim(DLO.Lon)
		, OriginLatLon = ltrim(OO.LAT) + ',' + ltrim(OO.LON)
		, OriginAccuracyMeters = DLO.SourceAccuracyMeters
		, OriginDistance = isnull(cast(DLO.DistanceToPoint as int), 99999)
		, OriginGpsArrived = CASE WHEN DLO.DistanceToPoint IS NOT NULL AND DLO.DistanceToPoint <= OO.GeoFenceRadiusMeters THEN 1 ELSE 0 END
		, DestGpsLatLon = ltrim(DLD.Lat) + ',' + ltrim(DLD.Lon)
		, DestLatLon = ltrim(D.LAT) + ',' + ltrim(D.LON)
		, DestAccuracyMeters = DLD.SourceAccuracyMeters
		, DestDistance = isnull(cast(DLD.DistanceToPoint as int), 99999)
		, DestGpsArrived = CASE WHEN DLD.DistanceToPoint IS NOT NULL AND DLD.DistanceToPoint <= OO.GeoFenceRadiusMeters THEN 1 ELSE 0 END
		, ShipperDestCode = CDC.Code
	FROM viewOrder_OrderTicket_Full O
	JOIN tblOrigin OO ON OO.ID = O.OriginID
	JOIN tblDestination D ON D.ID = O.DestinationID
	LEFT JOIN tblOrderInvoiceCustomer IOCS ON IOCS.OrderID = O.ID
	LEFT JOIN tblCustomerSettlementBatch SSB ON SSB.ID = IOCS.BatchID
	LEFT JOIN tblUom SUOM ON SUOM.ID = IOCS.UomID
	LEFT JOIN tblOrderInvoiceCarrier IOCC ON IOCC.OrderID = O.ID
	LEFT JOIN tblCarrierSettlementBatch CSB ON CSB.ID = IOCC.BatchID
	LEFT JOIN tblUom CUOM ON CUOM.ID = IOCC.UomID
	LEFT JOIN tblCustomerDestinationCode CDC ON CDC.CustomerID = O.CustomerID AND CDC.DestinationID = O.DestinationID
	LEFT JOIN viewDriverLocation_OriginFirstArrive DLO ON DLO.OrderID = O.ID AND DLO.OriginID = O.OriginID
	LEFT JOIN viewDriverLocation_DestinationFirstArrive DLD ON DLD.OrderID = O.ID AND DLD.DestinationID = O.DestinationID

GO

UPDATE tblReportColumnDefinition SET DataField = 'ShipperFuelSurchargeFee', Caption = 'Shipper Fuel Surcharge $$' WHERE ID = 163
UPDATE tblReportColumnDefinition SET DataField = 'CarrierFuelSurchargeFee', Caption = 'Carrier Fuel Surcharge $$' WHERE ID = 181
SET IDENTITY_INSERT tblReportColumnDefinition ON
INSERT INTO tblReportColumnDefinition (ID, ReportID, DataField, Caption, DataFormat, FilterDataField, FilterTypeID, FilterDropDownSql, FilterAllowCustomText, AllowedRoles, OrderSingleExport)
	SELECT 185, 1, 'ShipperFuelSurchargeRate',	'Shipper Fuel Surcharge Rate', '#0.0000', NULL, 4, NULL, 1, 'Administrator,Management',1
	UNION
	SELECT 186, 1, 'CarrierFuelSurchargeRate',	'Carrier Fuel Surcharge Rate', '#0.0000', NULL, 4, NULL, 1, 'Administrator,Management',1
	UNION
	SELECT 187, 1, 'ShipperSplitLoadFee',	'Shipper Split Load $$', '#0.0000', NULL, 4, NULL, 1, 'Administrator,Management',1
	UNION
	SELECT 188, 1, 'CarrierSplitLoadFee',	'Carrier Split Load $$', '#0.0000', NULL, 4, NULL, 1, 'Administrator,Management',1
GO
SET IDENTITY_INSERT tblReportColumnDefinition OFF

EXEC _spRebuildAllObjects
GO

update tblCarrierRates set splitloadrate = 0
update tblCustomerRates set splitloadrate = 0

insert into tblCarrierFuelSurchargeRates (EffectiveDate, CarrierID, FuelPriceFloor, IntervalAmount, IncrementAmount)
	select '1/1/2014', C.ID, 100.0, 0.05, 0.025
	FROM tblCarrier C
	where DeleteDateUTC is null

insert into tblShipperFuelSurchargeRates (EffectiveDate, ShipperID, FuelPriceFloor, IntervalAmount, IncrementAmount)
	select '1/1/2014', C.ID, case when C.name like 'sunoco%' then 2.30 else 100.0 end, 0.05, 0.025
	from tblCustomer C
go

insert into tblFuelPrice (EffectiveDate, EIAPADDRegionID, Price)
	select '10/27/2014', 1, 3.627
	union
	select '10/27/2014', 2, 3.611
	union
	select '10/27/2014', 3, 3.563
	union
	select '10/27/2014', 4, 3.714
	union
	select '10/27/2014', 5, 3.799
go

COMMIT 
SET NOEXEC OFF

delete from tblFuelPrice
/*
insert into tblFuelPrice (EIAPADDRegion, EffectiveDate, Price)
	select '10/27/2014', 1, 3.627
	union
	select '10/27/2014', 2, 3.611
	union
	select '10/27/2014', 3, 3.563
	union
	select '10/27/2014', 2, 3.714
	union
	select '10/27/2014', 2, 3.799
	
go

select * from tbleiaPADDRegion
insert into tblCarrierFuelSurchargeRates (EffectiveDate, ShipperID, FuelPriceFloor, IntervalAmount, IncrementAmount)
	select '1/1/2014', C.ID, 2.30, 0.05, 0.025
	FROM tblCustomer C


insert into tblShipperFuelSurchargeRates (EffectiveDate, ShipperID, FuelPriceFloor, IntervalAmount, IncrementAmount)
	select '1/1/2014', C.ID, 2.30, 0.05, 0.025
	FROM tblCarrier C
	
go

select dbo.fnFuelPriceForEffectiveDate('11/1/2014')
select dbo.fnShipperFuelSurchargeRate(12, '11/1/2014')
select * from tblShipperFuelSurchargeRates

declare @ShipperID int, @EffectiveDate smalldatetime; select @shipperid = 12, @effectiveDate = '11/1/2014'
DECLARE @ret money, @fuelPriceFloor money, @intervalAmount money, @incrementAmount money, @pricediff money	

SELECT TOP 1 FuelPriceFloor, IntervalAmount, IncrementAmount
	, dbo.fnMaxDecimal(0, dbo.fnFuelPriceForEffectiveDate(@effectiveDate) - fuelPriceFloor)
FROM tblShipperFuelSurchargeRates 
WHERE ShipperID = @ShipperID AND EffectiveDate <= @EffectiveDate
ORDER BY EffectiveDate DESC

SELECT TOP 1 @fuelPriceFloor = FuelPriceFloor, @intervalAmount = IntervalAmount, @incrementAmount = IncrementAmount
	, @pricediff = dbo.fnMinDecimal(0, dbo.fnFuelPriceForEffectiveDate(@effectiveDate) - fuelPriceFloor)
FROM tblShipperFuelSurchargeRates 
WHERE ShipperID = @ShipperID AND EffectiveDate <= @EffectiveDate
ORDER BY EffectiveDate DESC

SELECT @ret = @incrementAmount * (round(@pricediff / @intervalAmount, 0) + CASE WHEN @pricediff % @intervalAmount > 0 THEN 1 ELSe 0 END)

select round((CAST(3.751 as money) - cast(3.701 as money)) / 0.025, 0)
	+ CASE WHEN (CAST(3.751 as money) - cast(3.701 as money)) % 0.025 > 0 THEN 1 ELSE 0 END
*/
