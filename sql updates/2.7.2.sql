/*  
	-- add the Gravity60F field to the viewOrderTicket view
	-- update the OrderTicket IU trigger to sync the 1st OrderTicket.OriginTankNum | OrderTicket.OriginTankID w/ the Order
	-- convert Origin | Operator | Producer from Active flag to DeleteDateUTC | DeletedByUser
*/ 
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '2.7.1'
SELECT  @NewVersion = '2.7.2'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0
GO

/***********************************/
-- Date Created: 22 Jan 2013
-- Author: Kevin Alons
-- Purpose: query the tblOrderTicket table and include "friendly" translated values
/***********************************/
ALTER VIEW [dbo].[viewOrderTicket] AS
SELECT OT.*
	, TT.Name AS TicketType
	, CASE WHEN OT.OriginTankID IS NULL OR ORT.TankNum = '*' THEN OT.TankNum ELSE ORT.TankNum END AS OriginTankText
	, cast(CASE WHEN OT.OriginTankID IS NULL OR ORT.TankNum = '*' THEN 0 ELSE 1 END as bit) AS IsStrappedTank
	, cast((CASE WHEN OT.DeleteDateUTC IS NULL THEN 1 ELSE 0 END) as bit) AS Active
	, cast((CASE WHEN OT.DeleteDateUTC IS NOT NULL THEN 1 ELSE 0 END) as bit) AS IsDeleted
	, Gravity60F = dbo.fnCorrectedAPIGravity(ProductObsGravity, ProductObsTemp)
FROM tblOrderTicket OT
JOIN tblTicketType TT ON TT.ID = OT.TicketTypeID
LEFT JOIN tblOriginTank ORT ON ORT.ID = OT.OriginTankID

GO

-- =============================================
-- Author:		Kevin Alons
-- Create date: 6 Feb 2013
-- Description:	trigger to ensure the GrossBarrels and NetBarrels are computed for valid, entered data
-- =============================================
ALTER TRIGGER [dbo].[trigOrderTicket_IU] ON [dbo].[tblOrderTicket] AFTER INSERT, UPDATE AS
BEGIN
	SET NOCOUNT ON;

	-- re-compute GaugeRun ticket Gross barrels
	IF UPDATE(OriginTankID) OR UPDATE(OpeningGaugeFeet) OR UPDATE(OpeningGaugeInch) OR UPDATE(OpeningGaugeQ)
		OR UPDATE (ClosingGaugeFeet) OR UPDATE(ClosingGaugeInch) OR UPDATE(ClosingGaugeQ) 
		OR UPDATE (GrossUnits)
	BEGIN
		UPDATE tblOrderTicket
		  SET GrossUnits = dbo.fnConvertUom(dbo.fnOriginTankStrappingLevelDeltaBarrels(
				OT.OriginTankID
			  , OpeningGaugeFeet
			  , OpeningGaugeInch
			  , OpeningGaugeQ
			  , ClosingGaugeFeet
			  , ClosingGaugeInch
			  , ClosingGaugeQ), 1, O.OriginUomID) -- ensure this value is always normalized from BARRELS
		FROM tblOrderTicket OT
		JOIN tblOrder O ON O.ID = OT.OrderID
		WHERE OT.ID IN (SELECT ID FROM inserted) 
		  AND OT.TicketTypeID = 1 -- Gauge Run Ticket
		  AND OT.OpeningGaugeFeet IS NOT NULL
		  AND OT.OpeningGaugeInch IS NOT NULL
		  AND OT.OpeningGaugeQ IS NOT NULL
		  AND OT.ClosingGaugeFeet IS NOT NULL
		  AND OT.ClosingGaugeInch IS NOT NULL
		  AND OT.ClosingGaugeQ IS NOT NULL
	END
	-- re-compute GaugeRun | Net Barrel tickets NetUnits
	IF UPDATE(OpeningGaugeFeet) OR UPDATE(OpeningGaugeInch) OR UPDATE(OpeningGaugeQ)
		OR UPDATE (ClosingGaugeFeet) OR UPDATE(ClosingGaugeInch) OR UPDATE(ClosingGaugeQ) 
		OR UPDATE(OriginTankID) OR UPDATE(GrossUnits) OR UPDATE(GrossStdUnits) OR UPDATE(NetUnits)
		OR UPDATE(ProductObsTemp) OR UPDATE(ProductObsGravity) OR UPDATE(ProductBSW)
	BEGIN
		UPDATE tblOrderTicket
		  SET NetUnits = dbo.fnCrudeNetCalculator(GrossUnits, ProductObsTemp, ProductObsGravity, ProductBSW)
			, GrossStdUnits = dbo.fnCrudeNetCalculator(GrossUnits, ProductObsTemp, ProductObsGravity, 0)
		WHERE ID IN (SELECT ID FROM inserted)
		  AND TicketTypeID IN (1,2)
		  AND GrossUnits IS NOT NULL
		  AND ProductObsTemp IS NOT NULL
		  AND ProductObsGravity IS NOT NULL
		  AND ProductBSW IS NOT NULL
	END
	
	-- ensure the Order record is in-sync with the Tickets
	UPDATE tblOrder 
	SET OriginGrossUnits = (SELECT sum(GrossUnits) FROM tblOrderTicket WHERE OrderID = O.ID AND Rejected = 0 AND DeleteDateUTC IS NULL)
	  , OriginNetUnits = (SELECT sum(NetUnits) FROM tblOrderTicket WHERE OrderID = O.ID AND Rejected = 0 AND DeleteDateUTC IS NULL)
	  , OriginGrossStdUnits = (SELECT sum(GrossStdUnits) FROM tblOrderTicket WHERE OrderID = O.ID AND Rejected = 0 AND DeleteDateUTC IS NULL)

		-- use the first MeterRun/BasicRun BOLNum as the Order BOL Num (favor the first un-Rejected)
	  , OriginBOLNum = (
			SELECT TOP 1 BOLNum FROM (
				SELECT TOP 1 BOLNum FROM tblOrderTicket 
				WHERE OrderID = O.ID AND TicketTypeID NOT IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
				UNION SELECT TOP 1 BOLNum FROM tblOrderTicket 
				WHERE OrderID = O.ID AND TicketTypeID NOT IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X)
		-- use the first GaugeRun|NetVolume CarrierTicketNum as the Order.CarrierTicketNum (favor first un-Rejected)
	  , CarrierTicketNum = (
			SELECT TOP 1 CarrierTicketNum FROM (
				SELECT TOP 1 CarrierTicketNum FROM tblOrderTicket 
				WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
				UNION SELECT TOP 1 CarrierTicketNum FROM tblOrderTicket 
				WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X)
		-- use the first GaugeRun|NetVolume OriginTankID for the Order (favor first un-Rejected ticket)
	  , OriginTankID = (
			SELECT TOP 1 OriginTankID FROM (
				SELECT TOP 1 OriginTankID FROM tblOrderTicket 
				WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
				UNION SELECT TOP 1 OriginTankID FROM tblOrderTicket 
				WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X)
		-- use the first GaugeRun|NetVolume OriginTankNum for the Order (favor first un-Rejected ticket)
	  , OriginTankNum = (
			SELECT TOP 1 OriginTankNum FROM (
				SELECT TOP 1 OriginTankNum FROM tblOrderTicket 
				WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
				UNION SELECT TOP 1 OriginTankNum FROM tblOrderTicket 
				WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X)
	  , LastChangeDateUTC = (SELECT MAX(isnull(LastChangeDateUTC, CreateDateUTC)) FROM inserted WHERE OrderID = O.ID)
	  , LastChangedByUser = (SELECT MIN(isnull(LastChangedByUser, CreatedByUser)) FROM inserted WHERE OrderID = O.ID)
	FROM tblOrder O
	WHERE ID IN (SELECT DISTINCT OrderID FROM inserted)
END

GO

ALTER TABLE tblOperator ADD DeleteDateUTC smalldatetime NULL
GO
ALTER TABLE tblOperator ADD DeletedByUser varchar(100) NULL
GO
UPDATE tblOperator SET DeleteDateUTC = '1/1/2014', DeletedByUser = 'Was Inactive' WHERE Active = 0
GO
ALTER TABLE tblOperator DROP CONSTRAINT DF_tblOperator_Active
GO
ALTER TABLE tblOperator DROP COLUMN Active
GO

ALTER TABLE tblOrigin ADD DeleteDateUTC smalldatetime NULL
GO
ALTER TABLE tblOrigin ADD DeletedByUser varchar(100) NULL
GO
UPDATE tblOrigin SET DeleteDateUTC = '1/1/2014', DeletedByUser = 'Was Inactive' WHERE Active = 0
GO
ALTER TABLE tblOrigin DROP CONSTRAINT DF_tblOrigin_Active
GO
ALTER TABLE tblOrigin DROP COLUMN Active
GO

ALTER TABLE tblProducer ADD DeleteDateUTC smalldatetime NULL
GO
ALTER TABLE tblProducer ADD DeletedByUser varchar(100) NULL
GO
UPDATE tblProducer SET DeleteDateUTC = '1/1/2014', DeletedByUser = 'Was Inactive' WHERE Active = 0
GO
ALTER TABLE tblProducer DROP CONSTRAINT DF_tblProducer_Active
GO
ALTER TABLE tblProducer DROP COLUMN Active
GO

/***********************************/
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Origin records with translated value and FullName field (which includes Origin Type)
/***********************************/
ALTER VIEW [dbo].[viewOrigin] AS
SELECT O.*
	, FullName = CASE WHEN O.H2S = 1 THEN 'H2S-' ELSE '' END + OT.OriginType + ' - ' + O.Name
	, OT.OriginType
	, State = S.FullName 
	, StateAbbrev = S.Abbreviation 
	, Operator = OP.Name
	, Pumper = P.FullName
	, TicketType = TT.Name
	, TT.ForTanksOnly
	, Region = R.Name
	, Customer = C.Name 
	, Producer = PR.Name 
	, TimeZone = TZ.Name 
	, UOM = UOM.Name 
	, UomShort = UOM.Abbrev 
	, OT.HasTanks
	, Active = cast(CASE WHEN O.DeleteDateUTC IS NULL THEN 1 ELSE 0 END as bit)
	, TankCount = (SELECT COUNT(*) FROM tblOriginTank WHERE OriginID = O.ID AND DeleteDateUTC IS NULL) 
FROM dbo.tblOrigin O
JOIN dbo.tblOriginType OT ON OT.ID = O.OriginTypeID
LEFT JOIN tblState S ON S.ID = O.StateID
LEFT JOIN dbo.tblOperator OP ON OP.ID = O.OperatorID
LEFT JOIN dbo.viewPumper P ON P.ID = O.PumperID
LEFT JOIN dbo.tblTicketType TT ON TT.ID = O.TicketTypeID
LEFT JOIN dbo.tblRegion R ON R.ID = O.RegionID
LEFT JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
LEFT JOIN dbo.tblProducer PR ON PR.ID = O.ProducerID
LEFT JOIN dbo.tblTimeZone TZ ON TZ.ID = O.TimeZoneID
LEFT JOIN dbo.tblUom UOM ON UOM.ID = O.UomID

GO

/***********************************/
-- Date Created: 25 Nov 2012
-- Author: Kevin Alons
-- Purpose: return Order records with "translated friendly" values for FK relationships
/***********************************/
ALTER VIEW [dbo].[viewOrder] AS
SELECT O.*
	, OPS.OrderStatus AS PrintStatus
FROM (
	SELECT O.*
	, dbo.fnDateOnly(dbo.fnUTC_to_Local(O.OriginDepartTimeUTC, vO.TimeZoneID, vO.UseDST)) AS OrderDate
	, vO.Name AS Origin
	, vO.FullName AS OriginFull
	, vO.State AS OriginState
	, vO.StateAbbrev AS OriginStateAbbrev
	, vO.Station AS OriginStation
	, vO.LeaseName
	, vO.LeaseNum
	, vO.LegalDescription AS OriginLegalDescription
	, vO.NDICFileNum AS OriginNDIC
	, vO.NDM AS OriginNDM
	, vO.CA AS OriginCA
	, vO.TimeZoneID AS OriginTimeZoneID
	, vO.UseDST AS OriginUseDST
	, vO.H2S
	, vD.Name AS Destination
	, vD.FullName AS DestinationFull
	, vD.State AS DestinationState
	, vD.StateAbbrev AS DestinationStateAbbrev
	, vD.DestinationType
	, vD.Station AS DestStation
	, vD.TimeZoneID AS DestTimeZoneID
	, vD.UseDST AS DestUseDST
	, C.Name AS Customer
	, CA.Name AS Carrier
	, CT.Name AS CarrierType
	, OS.OrderStatus
	, OS.StatusNum
	, D.FullName AS Driver
	, D.FirstName AS DriverFirst
	, D.LastName AS DriverLast
	, TRU.FullName AS Truck
	, TR1.FullName AS Trailer
	, TR2.FullName AS Trailer2
	, P.PriorityNum
	, TT.Name AS TicketType
	, vD.TicketTypeID AS DestTicketTypeID
	, vD.TicketType AS DestTicketType
	, OP.Name AS Operator
	, PR.Name AS Producer
	, PU.FullName AS Pumper
	, D.IDNumber AS DriverNumber
	, CA.IDNumber AS CarrierNumber
	, TRU.IDNumber AS TruckNumber
	, TR1.IDNumber AS TrailerNumber
	, TR2.IDNumber AS Trailer2Number
	, PRO.Name as Product
	, PRO.ShortName AS ProductShort
	, OUom.Name AS OriginUOM
	, OUom.Abbrev AS OriginUomShort
	, CASE WHEN ORT.TankNum = '*' THEN '*' + isnull(O.OriginTankNum, '?') ELSE ORT.TankNum END AS OriginTankID_Text
	, DUom.Name AS DestUOM
	, DUom.Abbrev AS DestUomShort
	, cast((CASE WHEN O.DeleteDateUTC IS NULL THEN 1 ELSE 0 END) as bit) AS Active
	, cast((CASE WHEN O.DeleteDateUTC IS NOT NULL THEN 1 ELSE 0 END) as bit) AS IsDeleted
	, PPS.Name AS PickupPrintStatus
	, PPS.IsCompleted AS PickupCompleted
	, DPS.Name AS DeliverPrintStatus
	, DPS.IsCompleted AS DeliverCompleted
	, CASE WHEN StatusID = 8 AND PPS.IsCompleted = 0 THEN 7
		   WHEN StatusID = 3 AND DPS.IsCompleted = 0 THEN 8
		   ELSE StatusID END AS PrintStatusID
	, OriginTankText = CASE WHEN O.OriginTankID IS NULL OR ORT.TankNum = '*' THEN O.OriginTankNum ELSE ORT.TankNum END
	FROM dbo.tblOrder O
	LEFT JOIN dbo.viewOrigin vO ON vO.ID = O.OriginID
	LEFT JOIN dbo.viewDestination vD ON vD.ID = O.DestinationID
	LEFT JOIN dbo.tblCustomer C ON C.ID = O.CustomerID
	JOIN dbo.tblOrderStatus AS OS ON OS.ID = O.StatusID
	LEFT JOIN dbo.tblCarrier CA ON CA.ID = O.CarrierID
	LEFT JOIN dbo.tblCarrierType CT ON CT.ID = CA.CarrierTypeID
	LEFT JOIN dbo.viewDriver D ON D.ID = O.DriverID
	LEFT JOIN dbo.viewTruck TRU ON TRU.ID = O.TruckID
	LEFT JOIN dbo.viewTrailer TR1 ON TR1.ID = O.TrailerID
	LEFT JOIN dbo.viewTrailer TR2 ON TR2.ID = O.Trailer2ID
	LEFT JOIN dbo.tblPriority P ON P.ID = O.PriorityID
	LEFT JOIN dbo.tblTicketType TT ON TT.ID = O.TicketTypeID
	LEFT JOIN dbo.tblOriginTank ORT ON ORT.ID = O.OriginTankID
	LEFT JOIN dbo.tblOperator OP ON OP.ID = O.OperatorID
	LEFT JOIN dbo.viewPumper PU ON PU.ID = O.PumperID
	LEFT JOIN dbo.tblProducer PR ON PR.ID = O.ProducerID
	LEFT JOIN dbo.tblProduct PRO ON PRO.ID = O.ProductID
	LEFT JOIN dbo.tblUom OUom ON OUom.ID = O.OriginUomID
	LEFT JOIN dbo.tblUom DUom ON DUom.ID = O.DestUomID
	LEFT JOIN dbo.tblPrintStatus PPS ON PPS.ID = O.PickupPrintStatusID
	LEFT JOIN dbo.tblPrintStatus DPS ON DPS.ID = O.DeliverPrintStatusID
) O
LEFT JOIN dbo.tblOrderStatus OPS ON OPS.ID = O.PrintStatusID

GO

UPDATE tblOrder 
SET 
	-- use the first MeterRun/BasicRun BOLNum as the Order BOL Num (favor the first un-Rejected)
  OriginBOLNum = isnull(OriginBOLNum, 
		(SELECT TOP 1 BOLNum FROM (
			SELECT TOP 1 BOLNum FROM tblOrderTicket 
			WHERE OrderID = O.ID AND TicketTypeID NOT IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
			UNION SELECT TOP 1 BOLNum FROM tblOrderTicket 
			WHERE OrderID = O.ID AND TicketTypeID NOT IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X))
	-- use the first GaugeRun|NetVolume CarrierTicketNum as the Order.CarrierTicketNum (favor first un-Rejected)
  , CarrierTicketNum = isnull(CarrierTicketNum, 
		(SELECT TOP 1 CarrierTicketNum FROM (
			SELECT TOP 1 CarrierTicketNum FROM tblOrderTicket 
			WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
			UNION SELECT TOP 1 CarrierTicketNum FROM tblOrderTicket 
			WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X))
	-- use the first GaugeRun|NetVolume OriginTankID for the Order (favor first un-Rejected ticket)
  , OriginTankID = isnull(OriginTankID, 
		(SELECT TOP 1 OriginTankID FROM (
			SELECT TOP 1 OriginTankID FROM tblOrderTicket 
			WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
			UNION SELECT TOP 1 OriginTankID FROM tblOrderTicket 
			WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X))
	-- use the first GaugeRun|NetVolume OriginTankNum for the Order (favor first un-Rejected ticket)
  , OriginTankNum = isnull(OriginTankNum, 
		(SELECT TOP 1 OriginTankNum FROM (
			SELECT TOP 1 OriginTankNum FROM tblOrderTicket 
			WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND Rejected = 0 AND DeleteDateUTC IS NULL ORDER BY ID
			UNION SELECT TOP 1 OriginTankNum FROM tblOrderTicket 
			WHERE OrderID = O.ID AND TicketTypeID IN (1,2) AND DeleteDateUTC IS NULL ORDER BY ID) X))
FROM tblOrder O
GO

EXEC _spRefreshAllViews
GO

COMMIT
SET NOEXEC OFF