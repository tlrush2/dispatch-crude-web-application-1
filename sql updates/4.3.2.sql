SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.3.1'
SELECT  @NewVersion = '4.3.2'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'Fixes to Driver Scheduling page + new Driver Scheduling Report page & export'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/***********************************************************/
-- Date Created: 4.3.0 - 2016.10.20 - Kevin Alons
-- Purpose: return all "potential" Driver Scheduling records for a date range and input criteria
-- Changes:
-- 4.3.2	- 2016.11.05 - KDA	- add @DriverID parameter (and also honor @DriverGroupID parameter which was present but used)
/***********************************************************/
ALTER FUNCTION fnDriverSchedule
(
  @StartDate date
, @EndDate date
, @RegionID int = -1
, @CarrierID int = -1
, @DriverGroupID int = -1
, @DriverID int = -1
) RETURNS TABLE AS RETURN
	-- generates a full list of records for the specified criteria (with NULL or not assigned as a default)
	WITH cteBASE AS (
		SELECT D.RegionID, D.Region, D.CarrierID, D.Carrier, D.DriverGroupID, DriverGroup = D.DriverGroupName
			, DriverID = D.ID, Driver = D.FullName, D.MobilePhone, DT.ScheduleDate
			, StatusID = 1 /* undefined */, StartHours = cast(null as tinyint), DurationHours = cast(null as tinyint)
			, Pos = ROW_NUMBER() OVER (PARTITION BY D.ID ORDER By DT.ScheduleDate)
		FROM viewDriverBase D
		CROSS JOIN (
			-- get a table with the entire specified date range (date only values)
			SELECT ScheduleDate = dateadd(day, n-1, @StartDate)
			FROM viewNumbers1000 
			WHERE n <= datediff(day, @StartDate, @EndDate) + 1
		) DT
		WHERE (@CarrierID = -1 OR D.CarrierID = @CarrierID)
		 AND (@RegionID = -1 OR D.RegionID = @RegionID)
		 AND (@DriverGroupID = -1 OR D.DriverGroupID = @DriverGroupID)
		 AND (@DriverID = -1 OR D.ID = @DriverID)
		 AND D.Active = 1
	)

	SELECT B.RegionID, B.Region, B.CarrierID, B.Carrier, B.DriverGroupID, B.DriverGroup, B.DriverID, B.Driver, B.MobilePhone
		, DSA.DriverShiftTypeID, DSA.DriverShiftStartDate
		, StatusID = isnull(DS.StatusID, B.StatusID), Status = SS.Abbrev, Pos
		, StartHours = isnull(DS.StartHours, B.StartHours), DurationHours = isnull(DS.DurationHours, B.DurationHours)
		, B.ScheduleDate, DS.ManualAssignment
		, DS.CreateDateUTC, DS.CreatedByUser
	FROM cteBASE B
	LEFT JOIN tblDriverSchedule DS ON DS.DriverID = B.DriverID AND DS.ScheduleDate = B.ScheduleDate
	LEFT JOIN tblDriverScheduleAssignment DSA ON DSA.DriverID = B.DriverID
	LEFT JOIN tblDriverScheduleStatus SS ON SS.ID = isnull(DS.StatusID, B.StatusID)

GO

/***********************************************************/
-- Date Created: 4.3.0 - 2016.10.20 - Kevin Alons
-- Purpose: return all "potential" Driver Scheduling records for a date range and input criteria, in format for code-based PIVOTing
-- Changes:
-- 4.3.2	- 2016.11.05 - KDA	- add @DriverID parameter
/***********************************************************/
ALTER PROCEDURE spDriverSchedulePivotSource
(
  @Start date
, @End date
, @RegionID int = -1
, @CarrierID int = -1
, @DriverGroupID int = -1
, @DriverID int = -1
) AS
BEGIN
	SELECT DriverID, Region, Carrier, DriverGroup, Driver, DriverShiftTypeID, DriverShiftStartDate
		, Heading = FORMAT(ScheduleDate, 'SDM_d_yy')
		, PivotValue = ltrim(Pos) + '|' + ltrim(StatusID) + '|' + Status + '|' + isnull(ltrim(StartHours), '') + '|' + isnull(ltrim(DurationHours), '') + '|' + isnull(ltrim(ManualAssignment), '0')
	FROM dbo.fnDriverSchedule(@Start, @End, @RegionID, @CarrierID, @DriverGroupID, @DriverID)
	ORDER BY Carrier, Driver, ScheduleDate;
END

GO

exec _spDropProcedure 'spDriverScheduleReportPivotSource'
go
/***********************************************************/
-- Date Created: 4.3.2 - 2016.11.05 - Kevin Alons
-- Purpose: return all "potential" Driver Scheduling Report records for a date range and input criteria, in format for code-based PIVOTing
-- Changes:
/***********************************************************/
CREATE PROCEDURE spDriverScheduleReportPivotSource
(
  @Start date
, @End date
, @RegionID int = -1
, @CarrierID int = -1
, @DriverGroupID int = -1
, @DriverID int = -1
) AS
BEGIN
	SELECT DS.DriverID, Region, Carrier, DriverGroup, Driver, DriverShiftTypeID, DriverShiftStartDate, MobilePhone
		, TotalOpenCount = isnull(TC.TC, 0)
		, Heading = FORMAT(ScheduleDate, 'SDM_d_yy')
		, PivotStatus =  isnull(Status, '??') 
		, PivotStartHours = format(cast('2016/01/01 ' + ltrim(StartHours) + ':00' as datetime), 'h tt')
		, PivotDurationHours = DurationHours
		, PivotOrderCount = isnull(DC.DC, 0)
	FROM dbo.fnDriverSchedule(@Start, @End, @RegionID, @CarrierID, @DriverGroupID, @DriverID) DS
	LEFT JOIN (SELECT DriverID, TC = count(*) FROM tblOrder WHERE StatusID IN (2, 7, 8) /* DISPATCHED,ACCEPTED,PICKED UP */ AND DeleteDateUTC IS NULL GROUP BY DriverID) TC ON TC.DriverID = DS.DriverID
	LEFT JOIN (SELECT DriverID, OrderDate, DC = count(*) FROM tblOrder WHERE StatusID IN (2, 7, 8) /* DISPATCHED,ACCEPTED,PICKED UP */ AND DeleteDateUTC IS NULL GROUP BY DriverID, OrderDate) DC ON DC.DriverID = DS.DriverID AND DC.OrderDate = DS.ScheduleDate
	ORDER BY Carrier, Driver, ScheduleDate;
END

GO
GRANT EXECUTE ON spDriverScheduleReportPivotSource TO role_iis_acct
GO

exec _spDropView 'viewDriverSchedule'
go
/****************************************************/
-- Created: 4.3.2 - 2016.11.07 - Kevin Alons
-- Purpose: retrieve DriverSchedule records + Status values + calculated Start/End Times
-- Changes:
/****************************************************/
CREATE VIEW viewDriverSchedule AS
	SELECT *
		, StartDateTimeUTC = dbo.fnCTZ_to_UTC(StartDateTime)
		, EndDateTimeUTC = dbo.fnCTZ_to_UTC(EndDateTime)
	FROM (
		SELECT DS.* 
			, Status = DSS.Name, DSS.OnDuty, DSS.Utilized, StatusAbbrev = DSS.Abbrev
			, StartDateTime = dateadd(hour, DS.StartHours % 24, cast(ScheduleDate as datetime))
			, EndDateTime = dateadd(hour, DS.StartHours % 24 + DurationHours, cast(ScheduleDate as datetime))
		FROM tblDriverSchedule DS
		JOIN tblDriverScheduleStatus DSS ON DSS.ID = DS.StatusID
	) X
GO

/****************************************************/
-- Created: 2016.09.14 - 4.1.7 - Joe Engler
-- Purpose: Retrieve the drivers (without last known GPS coordinates) for the specified filter criteria
-- Changes:
--	4.1.8.3		JAE		2016-09-21		Fixed reference to current time to start time (passed in)
--	4.1.10.1	JAE		2016-09-27		Added qualifier to DeleteDateUTC column (since same column was added to route table)
--	4.2.0		JAE		2016-09-30		Add hos fields to view
--	4.2.5		JAE		2016-10-26		Use operating state (instead of state) to carrier rule call
--											Added Weekly OnDuty fields
--	4.3.2		KDA		2016.11.07		replace tblDriverAvailability references with viewDriverSchedule (tblDriverSchedule)
/****************************************************/
ALTER FUNCTION fnRetrieveEligibleDrivers_NoGPS(@CarrierID INT, @RegionID INT, @StateID INT, @DriverGroupID INT, @StartDate DATETIME) 
RETURNS @ret TABLE 
(
	ID INT, 
	LastName VARCHAR(20), FirstName VARCHAR(20), FullName VARCHAR(41), FullNameLF VARCHAR(42),
	CarrierID INT, Carrier VARCHAR(40),
	RegionID INT,
	TruckID INT, Truck VARCHAR(10),
	TrailerID INT, Trailer VARCHAR(10),
	AvailabilityFactor DECIMAL(4,2),
	ComplianceFactor DECIMAL(4,2),
	TruckAvailabilityFactor DECIMAL(4,2),
	CurrentWorkload INT,
	CurrentECOT INT,
	HOSFactor DECIMAL(4,2),
	OnDutyHoursSinceWeeklyReset DECIMAL(6,2),
	WeeklyOnDutyHoursLeft DECIMAL(6,2),
	DrivingHoursSinceWeeklyReset DECIMAL(6,2),
	WeeklyDrivingHoursLeft DECIMAL(6,2),
	OnDutyHoursSinceDailyReset DECIMAL(6,2),
	OnDutyHoursLeft DECIMAL(6,2),
	DrivingHoursSinceDailyReset DECIMAL(6,2),
	DrivingHoursLeft DECIMAL(6,2),
	HOSHrsOnShift DECIMAL(6,2),
	DriverScore INT 
)
AS BEGIN
	DECLARE @__ENFORCE_DRIVER_SCHEDULING__ INT = 13
	DECLARE @__ENFORCE_DRIVER_COMPLIANCE__ INT = 14
	DECLARE @__ENFORCE_HOS__ INT = 1
	
	IF @StartDate IS NULL 
	   SELECT @StartDate = dbo.fnDateOnly(GETDATE())

	INSERT INTO @ret
	SELECT X.*,
		DriverScore = 100 * AvailabilityFactor * ComplianceFactor * TruckAvailabilityFactor * HOSFactor
	FROM (
		SELECT d.ID, 
			d.FirstName, d.LastName, FullName, d.FullNameLF,
			d.CarrierID, d.Carrier,
			d.RegionID,
			d.TruckID, d.Truck,
			d.TrailerID, d.Trailer,

			--Availability
			AvailabilityFactor = CASE WHEN cr_a.Value IS NULL OR dbo.fnToBool(cr_a.Value) = 0 THEN 1 -- scheduling not enforced
										ELSE isnull(DS.OnDuty, 0) END,

			-- Compliance
			ComplianceFactor = CASE WHEN cr_c.Value IS NULL OR dbo.fnToBool(cr_c.Value) = 0 THEN 1 -- compliance not enforced
									WHEN d.CDLExpiration < @StartDate -- CDL expired
											OR d.H2SExpiration < @StartDate -- H2s expired
											OR d.DLExpiration < @StartDate -- DL expired
											--OR d.MedicalCardDate
											THEN 0
									ELSE 1 END, -- all ok

			--Truck/Trailer Availability
			TruckAvailabilityFactor = 1,
			
			--current workload, time on shift
			o.CurrentWorkload,
			CurrentECOT = ISNULL(o.CurrentECOT, 0),

			-- HOS
			HOSFactor = CASE WHEN cr_h.Value IS NULL OR dbo.fnToBool(cr_h.Value) = 0 THEN 1 -- HOS not enabled
							WHEN hos.WeeklyOnDutyHoursLeft <= 0 OR hos.OnDutyHoursLeft <= 0 OR hos.DrivingHoursLeft <= 0 THEN 0 
							-- compare ECOT to hours and see
							ELSE 1 END,
			hos.OnDutyHoursSinceWeeklyReset,
			hos.WeeklyOnDutyHoursLeft,
			hos.DrivingHoursSinceWeeklyReset,
			hos.WeeklyDrivingHoursLeft,
			hos.OnDutyHoursSinceDailyReset,
			hos.OnDutyHoursLeft,
			hos.DrivingHoursSinceDailyReset,
			hos.DrivingHoursLeft,
			HrsOnShift = CAST(DATEDIFF(MINUTE, ds.StartDateTime, getdate())/60.0*isnull(ds.OnDuty, 0) AS DECIMAL(5,1)) -- from DriverScheduling
			/* return the number of hours the Driver Scheduling module defines the driver is Scheduled on the specified day */
			--,ShiftHours = dbo.fnMinInt(DS.DurationHours, 24 - DS.StartHours % 24)-- from DriverScheduling

		FROM viewDriverBase d -- KDA NOTE: slightly more efficient
		LEFT JOIN (SELECT * FROM viewDriverSchedule WHERE OnDuty = 1) ds ON ds.DriverID = d.ID AND ScheduleDate = @StartDate
		OUTER APPLY dbo.fnCarrierRules(@StartDate, null, @__ENFORCE_DRIVER_SCHEDULING__, d.CarrierID, d.ID, d.OperatingStateID, d.RegionID, 1) cr_a -- Enforce Driver Scheduling Carrier Rule
		OUTER APPLY dbo.fnCarrierRules(@StartDate, null, @__ENFORCE_DRIVER_COMPLIANCE__, d.CarrierID, d.ID, d.OperatingStateID, d.RegionID, 1) cr_c
		OUTER APPLY dbo.fnCarrierRules(@StartDate, null, @__ENFORCE_HOS__, d.CarrierID, d.ID, d.OperatingStateID, d.RegionID, 1) cr_h
		OUTER APPLY (SELECT TOP 1 * FROM dbo.fnHosViolationDetail(d.ID, null, @StartDate) ORDER BY LogDateUTC DESC) hos
		OUTER APPLY (
			SELECT CurrentWorkload = COUNT(*), 
					CurrentECOT = SUM(CASE WHEN o.StatusID = 8 THEN ISNULL(r.ECOT, 60+ISNULL(r.ActualMiles,60)+60)/2
										   ELSE COALESCE(r.ECOT, 60+ISNULL(r.ActualMiles,60)+60) END)
			FROM tblOrder o LEFT JOIN tblRoute r ON o.RouteID = r.ID
			WHERE o.DriverID = d.ID
			  AND o.StatusID IN (2,7,8) -- Dispatched, Accepted, Picked Up
			  AND o.DeleteDateUTC IS NULL) o

		WHERE d.DeleteDateUTC IS NULL
			AND d.TruckID IS NOT NULL AND d.TrailerID IS NOT NULL
			AND (ISNULL(@CarrierID, 0) <= 0 OR @CarrierID = d.CarrierID)
			AND (ISNULL(@DriverGroupID, 0) <= 0 OR @DriverGroupID = d.ID)
			AND (ISNULL(@RegionID, -1) <= 0 OR @RegionID = d.RegionID)
			AND (ISNULL(@StateID, 0) <= 0 OR @StateID = d.StateID)
	) AS X
	ORDER BY DriverScore DESC, LastName, FirstName

	RETURN
END

GO

/* remove any existing Driver Availability objects */
exec _spDropProcedure 'spDriverAvailabilityUpdate'
exec _spDropProcedure 'spDriverAvailability'
exec _spDropFunction 'fnDriverAvailability'
exec _spdropview 'viewDriverAvailability'
drop table tblDriverAvailability
go

COMMIT
SET NOEXEC OFF
GO

/* Removed change to _spRebuildAllObjects, it was causing an error when this was run via visual bild. Running this update manually seemed to work just fine oddly enough. see updates 3.13.13.2 and 3.13.13.3 */