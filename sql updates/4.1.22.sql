SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.1.21'
SELECT  @NewVersion = '4.1.22'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1828: Add misc. details section to the ebol and add BOL Fields configuration page to control which extra fields show up on the ebol'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/* Create new table to manage the extra fields portion of the eBOL*/
CREATE TABLE tblCustomBOLFields
(
	ID int IDENTITY(1,1) NOT NULL
	, DataField VARCHAR(100) NOT NULL
	, InternalLabel VARCHAR(100) NOT NULL
	, DataLabel VARCHAR(100) NOT NULL	
	, DisplayOnBOL BIT NOT NULL DEFAULT 0
	, CreateDateUTC SMALLDATETIME NULL DEFAULT GETUTCDATE()
	, CreatedByUser VARCHAR(100) NULL DEFAULT 'System'
	, LastChangeDateUTC SMALLDATETIME NULL
	, LastChangedByUser VARCHAR(100) NULL
	 CONSTRAINT PK_tblCustomBOLFields PRIMARY KEY CLUSTERED (ID ASC)	
) 
GO



/* Create view and edit permissions for the new eBOL fields maintenance/configuration page */
EXEC spAddNewPermission 'viewBOLFieldMaintenance','View BOL field maintenance page','Print Configuration','BOL Fields - View'
GO
EXEC spAddNewPermission 'editBOLFieldMaintenance','View BOL field maintenance page','Print Configuration','BOL Fields - Edit'
GO



/* Insert the initial list of possible choices for extra eBOL fields */
INSERT INTO tblCustomBOLFields (DataField, DataLabel, InternalLabel)
SELECT 'OriginTruckMileage','Origin Odometer','Origin Truck Mileage'
UNION
SELECT 'DestTruckMileage','Destination Odometer','Dest Truck Mileage'
UNION
SELECT 'DestRackBay','Destination Rack/Bay','Destination Rack/Bay'
UNION
SELECT 'Route.ActualMiles','Route Miles','Actual Miles'
UNION
SELECT 'VarianceGOV','GOV Variance','GOV Variance'
UNION
SELECT 'VarianceNSV','NSV Variance','NSV Variance'
UNION
SELECT 'TripMiles','Trip Miles','Trip Miles'
GO





COMMIT
SET NOEXEC OFF