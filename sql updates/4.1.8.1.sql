SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '4.1.8'
SELECT  @NewVersion = '4.1.8.1'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'DCWEB-1785: Change truck and gauger order create page H2S highlighting to work like the dispatch page currently does.'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO



/**********************************
-- Date Created: 18 Apr 2015
-- Author: Kevin Alons
-- Purpose: return GaugerOrder records with "translated friendly" values for FK relationships
-- Changes:
	-- 22 May 2015 - KDA - add "TicketCount" field
	- 3.11.19 - 2016/04/29 - BB - Add Job number and contract number fields
	- 3.13.12 - 2016/8/3 - BB - Add lease number DCWEB-1625
	- 4.1.8.1 - 2016/09/21 - BB - (DCWEB-1785) Add H2S column to allow alternate way of highlighting that does not depend on the "H2S" text beging in the origin name
***********************************/
ALTER VIEW [dbo].[viewGaugerOrder] AS
SELECT GAO.*
	, OrderNum = O.OrderNum
	, OrderDate = O.OrderDate
	, Status = GOS.Name
	, CarrierTicketNum = O.CarrierTicketNum
	, DispatchConfirmNum = O.DispatchConfirmNum
	, JobNumber = O.JobNumber
	, ContractNumber = O.ContractNumber
	, Gauger = G.FullName
	, OriginID = O.OriginID
	, Origin = O.Origin
	, OriginFull = O.OriginFull
	, OriginState = O.OriginState
	, OriginStateAbbrev = O.OriginStateAbbrev
	, O.H2S	-- 4.1.8.1
	, LeaseNum = O.LeaseNum  -- 3.13.12
	, CustomerID = O.CustomerID
	, ShipperID = O.CustomerID
	, Shipper = O.Customer
	, DestinationID = O.DestinationID
	, Destination = O.Destination
	, DestinationFull = O.DestinationFull
	, DestinationState = O.DestinationState
	, DestinationStateAbbrev = O.DestinationStateAbbrev
	, ProductID = O.ProductID
	, Product = O.Product
	, ProductShort = O.ProductShort
	, ProductGroupID = O.ProductGroupID
	, ProductGroup = O.ProductGroup
	, OriginTankText = ISNULL(OT.TankNum, GAO.OriginTankNum)
	, P.PriorityNum
	, TicketType = GTT.Name
	, PrintStatusID = O.PrintStatusID
	, OrderStatusID = O.StatusID
	, DeleteDateUTC = O.DeleteDateUTC
	, DeletedByUser = O.DeletedByUser
	, TicketCount = (SELECT COUNT(*) FROM tblGaugerOrderTicket GOT WHERE GOT.OrderID = GAO.OrderID AND GOT.DeleteDateUTC IS NULL)
FROM dbo.tblGaugerOrder GAO 
JOIN viewOrder O ON O.ID = GAO.OrderID 
JOIN dbo.tblGaugerOrderStatus GOS ON GOS.ID = GAO.StatusID
LEFT JOIN dbo.tblOriginTank OT ON OT.ID = GAO.OriginTankID
LEFT JOIN dbo.viewGauger G ON G.ID = GAO.GaugerID
LEFT JOIN dbo.tblGaugerTicketType GTT ON GTT.ID = GAO.TicketTypeID
LEFT JOIN tblPriority P ON P.ID = GAO.PriorityID
GO


EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRefreshAllViews
GO
EXEC _spRecompileAllStoredProcedures
GO
EXEC _spRecompileAllStoredProcedures
GO
EXEC _spRebuildAllObjects
GO
EXEC _spRebuildAllObjects
GO


COMMIT
SET NOEXEC OFF