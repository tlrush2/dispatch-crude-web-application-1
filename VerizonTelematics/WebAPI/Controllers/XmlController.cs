﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Xml;
using System.Xml.Serialization;
using VerizonTelematicsWebAPI.Models;
using AlonsIT;

namespace VerizonTelematicsWebAPI.Controllers
{
    public class XmlController : ApiController
    {
        [HttpPost]
        public IHttpActionResult Submit(HttpRequestMessage request)
        {
            try
            {
                string xml = request.Content.ReadAsStringAsync().Result;
                NetworkfleetMessage.Create(xml).Save(xml);
                return Ok(xml);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

     }
}
