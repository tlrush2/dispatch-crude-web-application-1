﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VerizonTelenaticsTestHarness
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            var content = new StringContent(txtXml.Text, Encoding.UTF8, "text/xml");
            var client = new HttpClient();
            var result = client.PostAsync(txtUrl.Text, content).Result;
            txtResponse.Text = string.Format("Status: {0}\r\nData:\r\n{1}", result.StatusCode, result.Content.ReadAsStringAsync().Result);
            tabCtrl.SelectTab(1);
        }
    }
}
