﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Data;
using System.IO;
using System.Drawing;
using System.Text.RegularExpressions;
using Syncfusion.XlsIO;
using Syncfusion.XlsIO.Implementation;
using AlonsIT;
using DispatchCrude.Core;
using DispatchCrude.Extensions;
using DispatchCrude.Models;

namespace DispatchCrude.DataExchange
{
    public class ReportCenterExcelExport
    {
        static public string FLD_EXPORT = "Export", FLD_ALIAS = "Alias";
        static private string NO_VALUE_DATAFIELD = null, FLD_DATAFORMAT = "DataFormat", FLD_GROUP_FUNC = "GroupingFunctionID";
        static private int ID_GROUP_EXCELSUBTOTAL = 3, ID_GROUP_FUNC_LASTDAY = 16;

        static private string EXCEPTION1_REPORTSOURCE_FIND = "viewReportCenter_Orders_NoGPS"
            , EXCEPTION1_REPORTCOLID_CSV = "138,141,142,144,145" // OriginGpsLatLon, OriginGpsArrived, DestGpsLatLon, DestDistance, DestGpsArrived'
            , EXCEPTION1_REPORTSOURCE_REPLACE = "viewReportCenter_Orders";

        private string _userName = null;
        private int _userReportID = 0;
        private string _reportSource = string.Empty;

        public ReportCenterExcelExport(string userName, string reportSource, int userReportID)
        {
            _userName = userName;

            _userReportID = userReportID;


            if (reportSource == null)
                reportSource = SSDB.QuerySingleValueStatic("SELECT SourceTable FROM viewUserReportDefinition WHERE ID = {0}", (object)userReportID).ToString();

            _reportSource = reportSource;

            // ensure this value is populated
            if (NO_VALUE_DATAFIELD == null)
            {
                NO_VALUE_DATAFIELD = SSDB.QuerySingleValueStatic("SELECT DataField FROM tblReportColumnDefinition WHERE ID=88").ToString();
            }
        }

        public Stream Export(DateTime? start = null, DateTime? end = null, bool isPreview = false, HashSet<int> orderNumbers = null)
        {
            DataTable dtUserReportColumnDefinitions = GetUserReportColumnDefinitions(_userReportID, _userName, start, end, orderNums: orderNumbers);

            return Export(dtUserReportColumnDefinitions, isPreview);
        }

        internal class OrderDateRange
        {
            public DateTime Start { get; private set; }
            public DateTime End { get; private set; }
            public OrderDateRange(string orderIDTable, string filterField, string filterValue, string idField = "OrderID")
            {
                Init(string.Format("SELECT {3} FROM {0} WHERE [{1}] = {2}", orderIDTable, filterField, DBHelper.QuoteStr(filterValue), idField));
            }
            public OrderDateRange(string orderIDWhereClause)
            {
                Init(orderIDWhereClause);
            }
            private void Init(string orderIDClause)
            {
                using (SSDB db = new SSDB())
                {
                    string[] dates = db.QuerySingleValue("SELECT ltrim(min(OrderDate)) + '|' + ltrim(max(OrderDate)) FROM tblOrder WHERE ID IN ({0})", orderIDClause).ToString().Split('|');
                    if (dates.Length == 2)
                    {
                        Start = DBHelper.ToDateTime(dates[0]);
                        End = DBHelper.ToDateTime(dates[1]);
                    }
                    else  // no OrderDates were found, so just default to TODAY
                        Start = End = DateTime.Now.Date;
                }
            }
        }
        public Stream ExportShipperBatch(int batchID, bool isPreview = false)
        {
            OrderDateRange dates = new OrderDateRange("tblOrderSettlementShipper", "BatchID", batchID.ToString());
            DataTable dtUserReportColumnDefinitions = GetUserReportColumnDefinitions(
                _userReportID
                , _userName
                , dates.Start
                , dates.End
                , shipperBatchID: batchID);
            return Export(dtUserReportColumnDefinitions, isPreview);
        }
        public Stream ExportCarrierBatch(int batchID, bool isPreview = false)
        {
            OrderDateRange dates = new OrderDateRange("tblOrderSettlementCarrier", "BatchID", batchID.ToString());
            DataTable dtUserReportColumnDefinitions = GetUserReportColumnDefinitions(
                _userReportID
                , _userName
                , dates.Start
                , dates.End
                , carrierBatchID: batchID);
            return Export(dtUserReportColumnDefinitions, isPreview);
        }
        public Stream ExportDriverBatch(int batchID, bool isPreview = false)
        {
            OrderDateRange dates = new OrderDateRange("tblOrderSettlementDriver", "BatchID", batchID.ToString());
            DataTable dtUserReportColumnDefinitions = GetUserReportColumnDefinitions(
                _userReportID
                , _userName
                , dates.Start
                , dates.End
                , driverBatchID: batchID);
            return Export(dtUserReportColumnDefinitions, isPreview);
        }
        public Stream ExportProducerBatch(int batchID, bool isPreview = false)
        {
            OrderDateRange dates = new OrderDateRange("tblOrderSettlementProducer", "BatchID", batchID.ToString());
            DataTable dtUserReportColumnDefinitions = GetUserReportColumnDefinitions(
                _userReportID
                , _userName
                , dates.Start
                , dates.End
                , producerBatchID: batchID);
            return Export(dtUserReportColumnDefinitions, isPreview);
        }
        public Stream ExportShipperSession(int sessionID, bool isPreview = false)
        {
            OrderDateRange dates = new OrderDateRange("tblOrderSettlementSelectionShipper", "SessionID", sessionID.ToString());
            DataTable dtUserReportColumnDefinitions = GetUserReportColumnDefinitions(
                _userReportID
                , _userName
                , dates.Start
                , dates.End
                , shipperSessionID: sessionID);
            return Export(dtUserReportColumnDefinitions, isPreview);
        }
        public Stream ExportCarrierSession(int sessionID, bool isPreview = false)
        {
            OrderDateRange dates = new OrderDateRange("tblOrderSettlementSelectionCarrier", "SessionID", sessionID.ToString());
            DataTable dtUserReportColumnDefinitions = GetUserReportColumnDefinitions(
                _userReportID
                , _userName
                , dates.Start
                , dates.End
                , carrierSessionID: sessionID);
            return Export(dtUserReportColumnDefinitions, isPreview);
        }
        public Stream ExportDriverSession(int sessionID, bool isPreview = false)
        {
            OrderDateRange dates = new OrderDateRange("tblOrderSettlementSelectionDriver", "SessionID", sessionID.ToString());
            DataTable dtUserReportColumnDefinitions = GetUserReportColumnDefinitions(
                _userReportID
                , _userName
                , dates.Start
                , dates.End
                , driverSessionID: sessionID);
            return Export(dtUserReportColumnDefinitions, isPreview);
        }

        public Stream Export(DataTable dtUserReportColumnDefinitions, bool isPreview = false)
        {
            MemoryStream ret = null;

            bool includeNullDates = IncludeNullOrderDates(dtUserReportColumnDefinitions);

            if (_reportSource.Equals(EXCEPTION1_REPORTSOURCE_FIND, StringComparison.CurrentCultureIgnoreCase) 
                && dtUserReportColumnDefinitions.Select(string.Format("ReportColumnID IN ({0})", EXCEPTION1_REPORTCOLID_CSV)).Length > 0)
            {
                _reportSource = EXCEPTION1_REPORTSOURCE_REPLACE;
            }
            System.Diagnostics.Debug.WriteLine(string.Format("Export().RetrieveData().Begin: {0:yyyy/MM/dd hh:mm:ss.fff}", DateTime.UtcNow));
            DataTable dtData = new ReportCenterData(_userName, _reportSource, dtUserReportColumnDefinitions).Retrieve(includeNullDates);
            System.Diagnostics.Debug.WriteLine(string.Format("Export().RetrieveData().End: {0:yyyy/MM/dd hh:mm:ss.fff}", DateTime.UtcNow));

            using (ExcelEngine excelEngine = new ExcelEngine())
            {
                IApplication excelApp = null;
                IWorkbook wb = null;
                IWorksheet ws = null;
                try
                {
                    excelApp = excelEngine.Excel;
                    wb = excelApp.Workbooks.Add(ExcelVersion.Excel2013);
                    wb.CalculationOptions.CalculationMode = ExcelCalculationMode.Automatic;
                    wb.CalculationOptions.RecalcOnSave = true;
                    ws = wb.Worksheets[0];
                    ws.EnableSheetCalculations();

                    if (isPreview)
                        ws.PageSetup.BackgoundImage = new Bitmap(HttpContext.Current.Server.MapPath("/Content/images/excelPreview.png"));

                    ExcelWriter excelWriter = new ExcelWriter(ws, dtUserReportColumnDefinitions.Select("Export=true", "SortNum"));
                    excelWriter.WriteData(dtData);
                    try
                    {
                        if (dtData.Rows.Count > 0)
                            ws.Range[1, 1, Math.Min(dtData.Rows.Count, 200), dtData.Columns.Count].AutofitColumns();
                    }
                    catch (Exception)
                    {
                        // eat errors
                    }

                    try
                    {
                        ws.Range[2, 1].FreezePanes();
                    }
                    catch
                    {
                        // eat errors
                    }


                    ret = new MemoryStream();
                    wb.SaveAs(ret, ExcelSaveType.SaveAsXLS);
                    wb.Close();
                }
                //catch (Exception ex)
                //{
                //    throw ex;
                //}
                finally
                {
                    if (wb != null)
                        wb.Close();
                }
            }
            return ret;
        }

        private DataTable GetUserReportColumnDefinitions(int userReportID, string userName
            , DateTime? start = null, DateTime? end = null
            , int? shipperBatchID = null, int? carrierBatchID = null, int? driverBatchID = null, int? producerBatchID = null
            , int? shipperSessionID = null, int? carrierSessionID = null, int? driverSessionID = null, HashSet<int> orderNums = null)
        {
            using (SSDB db = new SSDB())
            {
                DataTable ret = db.GetPopulatedDataTable("SELECT * FROM viewUserReportColumnDefinition WHERE UserReportID = {0} AND dbo.fnUserInRoles({1}, AllowedRoles) = 1"
                    , new object[] { userReportID, DBHelper.QuoteStr(userName) });

                // ensure a column exists for all Base Filters (to ensure the column is defined for filtering purposes)
                DataTable dtBaseFilterColumns = db.GetPopulatedDataTable(
                    @"SELECT BF.*, RCD.DataField, RCD.Caption, RCD.FilterDataField, OrderSingleExport=cast(0 as bit) 
                    FROM tblReportColumnDefinitionBaseFilter BF 
                    JOIN tblReportColumnDefinition RCD ON RCD.ID = BF.ReportColumnID");
                foreach (DataRow dr in dtBaseFilterColumns.Rows)
                {
                    if (ret.Select(string.Format("ReportColumnID={0}", dr["ReportColumnID"])).Length == 0)
                    {
                        ret.NewColRow(dr, ReportGroupingFunction.TYPE.None);
                    }
                }

                // if a "grouping" reference is filtering the report, then use null dates (which approximates no orderdate filter), otherwise 
                bool noOrderDateFilter = shipperBatchID.HasValue || carrierBatchID.HasValue || driverBatchID.HasValue || producerBatchID.HasValue
                    || shipperSessionID.HasValue || carrierSessionID.HasValue || driverSessionID.HasValue || orderNums != null;
                if (noOrderDateFilter)
                    start = end = null;

                // ensure an "OrderDate" column exists or is has a proper filter setting assigned
                OrderDateParams dates = noOrderDateFilter || start.HasValue || end.HasValue ? new OrderDateParams(start, end) : new OrderDateParams(ret);
                ret.EnsureColRowExists(ReportColumnDefinition.SystemID.OrderDate, dates.Operator, dates.Start, dates.End, onlyNewIfFiltered: true);

                // ensure an "ShipperBatchNum" column exists if a BatchNum filter value was specified
                if (shipperBatchID.HasValue)
                {
                    string batchNum = SSDB.QuerySingleValueStatic("SELECT BatchNum FROM tblShipperSettlementBatch WHERE ID = {0}", shipperBatchID).ToString();
                    ret.EnsureColRowExists(ReportColumnDefinition.SystemID.ShipperBatchNum, ReportFilterOperator.TYPE.Equals, batchNum);
                }
                // ensure an "CarrierBatchNum" column exists if a BatchNum filter value was specified
                else if (carrierBatchID.HasValue)
                {
                    string batchNum = SSDB.QuerySingleValueStatic("SELECT BatchNum FROM tblCarrierSettlementBatch WHERE ID = {0}", carrierBatchID).ToString();
                    ret.EnsureColRowExists(ReportColumnDefinition.SystemID.CarrierBatchNum, ReportFilterOperator.TYPE.Equals, batchNum);
                }
                // ensure an "DriverBatchNum" column exists if a BatchNum filter value was specified
                else if (driverBatchID.HasValue)
                {
                    string batchNum = SSDB.QuerySingleValueStatic("SELECT BatchNum FROM tblDriverSettlementBatch WHERE ID = {0}", driverBatchID).ToString();
                    ret.EnsureColRowExists(ReportColumnDefinition.SystemID.DriverBatchNum, ReportFilterOperator.TYPE.Equals, batchNum);
                }
                // ensure an "ProducerBatchNum" column exists if a BatchNum filter value was specified
                else if (producerBatchID.HasValue)
                {
                    string batchNum = SSDB.QuerySingleValueStatic("SELECT BatchNum FROM tblProducerSettlementBatch WHERE ID = {0}", producerBatchID).ToString();
                    ret.EnsureColRowExists(ReportColumnDefinition.SystemID.ProducerBatchNum, ReportFilterOperator.TYPE.Equals, batchNum);
                }

                // if a HashSet of order numbers has been passed ensure the order number column exists and is filtered by those numbers
                if (orderNums != null)
                {
                    var orderNumCSV = orderNums.Count > 0 ? string.Join(",", orderNums) : "0";
                    ret.EnsureColRowExists(ReportColumnDefinition.SystemID.OrderNum, ReportFilterOperator.TYPE.IN, orderNumCSV);
                }
                // or if a SessionID was specified, then get the OrderNum list and add this criteria
                else if (shipperSessionID.HasValue)
                {
                    var orderNumCSV = string.Format("SELECT OrderNum FROM tblOrder WHERE ID IN (SELECT OrderID FROM tblOrderSettlementSelectionShipper WHERE SessionID={0})", shipperSessionID);
                    ret.EnsureColRowExists(ReportColumnDefinition.SystemID.OrderNum, ReportFilterOperator.TYPE.IN, orderNumCSV);
                }
                else if (carrierSessionID.HasValue)
                {
                    var orderNumCSV = string.Format("SELECT OrderNum FROM tblOrder WHERE ID IN (SELECT OrderID FROM tblOrderSettlementSelectionCarrier WHERE SessionID={0})", carrierSessionID);
                    ret.EnsureColRowExists(ReportColumnDefinition.SystemID.OrderNum, ReportFilterOperator.TYPE.IN, orderNumCSV);
                }
                else if (driverSessionID.HasValue)
                {
                    var orderNumCSV = string.Format("SELECT OrderNum FROM tblOrder WHERE ID IN (SELECT OrderID FROM tblOrderSettlementSelectionDriver WHERE SessionID={0})", driverSessionID);
                    ret.EnsureColRowExists(ReportColumnDefinition.SystemID.OrderNum, ReportFilterOperator.TYPE.IN, orderNumCSV);
                }
                else  // else just ensure that an OrderNum column exists (with no filter)
                    ret.EnsureColRowExists(ReportColumnDefinition.SystemID.OrderNum);

                return ret;
            }
        }

        //  routine to determine whether any NON-PICKED UP statuses are included (for all STATUS filter column definition rows)
        // if no STATUS filters are defined, or all of them either don't filter or include at least one non-PICKED UP statuses, return TRUE, else return FALSE
        private bool IncludeNullOrderDates(DataTable dtUserReportColumns)
        {
            DataRow[] drOrderStatuses = dtUserReportColumns.Select(string.Format("ReportColumnID={0}", (int)ReportColumnDefinition.SystemID.PrintStatus));
            if (drOrderStatuses.Length == 0)
                return true;
            else
            {
                bool excluded = false; // default value
                foreach (DataRow drOrderStatus in drOrderStatuses)
                {
                    excluded = false; // default value
                    switch (DBHelper.ToInt32(drOrderStatus["FilterOperatorID"]))
                    {
                        case (int)ReportFilterOperator.TYPE.IN:
                        case (int)ReportFilterOperator.TYPE.NotIN:
                            excluded = true; // default to TRUE unless at least 1 non-PICKED UP status if found below
                            foreach (int statusID in new IdContainer(drOrderStatus["FilterValue1"].ToString()).IntIDS)
                            {
                                switch (statusID)
                                {   // these statuses include an OrderDate value, so they exclude the the OrderDate IS NULL records
                                    case (int)OrderStatus.STATUS.PickedUp:
                                    case (int)OrderStatus.STATUS.Delivered:
                                    case (int)OrderStatus.STATUS.Audited:
                                        break;
                                    default:    // including any other status will include OrderDate IS NULL records
                                        excluded = false;
                                        break;
                                }
                            }
                            break;
                    }
                    if (excluded)
                        return false;
                }
                return !excluded;
            }
        }

        public class ProfileValues : Dictionary<string, string>
        {
            public ProfileValues(string userName)
            {
                userName = DBHelper.QuoteStr(userName);
                using (SSDB db = new SSDB())
                {
                    // retrieve the specified user's profile values
                    foreach (DataRow drPV in db.GetPopulatedDataTable("SELECT * FROM dbo.fnUserAllProfileValues({0})", (object)userName).Rows)
                        this.Add(drPV["name"].ToString(), drPV["value"].ToString());
                }
            }
            public string GetValue(string key, string defaultValue)
            {
                string ret = (this.ContainsKey(key) ? this[key] ?? defaultValue : defaultValue).ToString().Trim();
                if (ret.Length == 0) ret = defaultValue;
                return ret;
            }
        }

        internal class ExcelWriter
        {
            internal class GF // group by field
            {
                public int Col { get; set; }
                public bool GroupBy { get; set; }
                public bool Valid { get; set; }
                public ConsolidationFunction Function { get; set; }
                public string DataFormat { get; set; }
                public int DivideByDays { get; set; }
                public int ExcelSubTotalParam
                {
                    get
                    {
                        switch (Function)
                        {
                            case ConsolidationFunction.Sum:
                            case ConsolidationFunction.Count:
                            case ConsolidationFunction.Average:
                            case ConsolidationFunction.Max:
                            case ConsolidationFunction.Min:
                                return (int)Function;
                            default: return 0;
                        }
                    }
                }
                public GF(int col, int groupingFunctionID, string dataFormat, int daysInRange)
                {
                    Col = col;
                    DataFormat = dataFormat;
                    Valid = true; // default value
                    switch (groupingFunctionID)
                    {
                        case (int)ReportGroupingFunction.TYPE.SummaryFunc:
                            GroupBy = true;
                            break;
                        case (int)ReportGroupingFunction.TYPE.Sum:
                            GroupBy = false;
                            Function = ConsolidationFunction.Sum;
                            break;
                        case (int)ReportGroupingFunction.TYPE.Avg:
                            GroupBy = false;
                            Function = ConsolidationFunction.Average;
                            break;
                        case (int)ReportGroupingFunction.TYPE.Count:
                            GroupBy = false;
                            Function = ConsolidationFunction.Count;
                            break;
                        case (int)ReportGroupingFunction.TYPE.Min: 
                            GroupBy = false;
                            Function = ConsolidationFunction.Min;
                            break;
                        case (int)ReportGroupingFunction.TYPE.Max:
                            GroupBy = false;
                            Function = ConsolidationFunction.Max;
                            break;
                        case (int)ReportGroupingFunction.TYPE.DailyAvg:
                            GroupBy = false;
                            Function = ConsolidationFunction.Sum;
                            DivideByDays = daysInRange;
                            break;
                        case (int)ReportGroupingFunction.TYPE.Sum_LastDay:
                            GroupBy = false;
                            Function = ConsolidationFunction.Sum;
                            break;
                        default:
                            Valid = false;
                            break;
                    }
                }
            }

            private IWorksheet _ws = null;
            private DataRow[] _headerColumns = null;
            private int _row = 0;

            public ExcelWriter(IWorksheet ws, DataRow[] headerColumns)
            {
                _ws = ws;
                foreach (DataRow drHeader in headerColumns)
                {
                    if (!DBHelper.IsNull(drHeader["DataFormat"]))
                    {
                        drHeader["DataFormat"] = HttpUtility.HtmlDecode(drHeader["DataFormat"].ToString());
                        drHeader.AcceptChanges();
                    }
                }
                _headerColumns = headerColumns;
            }

            public void WriteData(DataTable dtData)
            {
                System.Diagnostics.Debug.WriteLine(string.Format("WriteData.ImportDataTable().Begin: {0:yyyy/MM/dd hh:mm:ss.fff}", DateTime.UtcNow));
                //_row = _ws.ImportDataTable(dtData, false, 2, 1, true) + 1;
                System.Diagnostics.Debug.WriteLine(string.Format("WriteData.ImportDataTable().End: {0:yyyy/MM/dd hh:mm:ss.fff}", DateTime.UtcNow));

                WriteHeaderRow(1);
                _row++;
                //System.Diagnostics.Debug.WriteLine(string.Format("WriteData.ImportDataTable().Begin: {0:yyyy/MM/dd hh:mm:ss.fff}", DateTime.UtcNow));
                foreach (DataRow drData in dtData.Rows)
                {
                    WriteDataRow(drData);
                }
                //System.Diagnostics.Debug.WriteLine(string.Format("WriteData.ImportDataTable().End: {0:yyyy/MM/dd hh:mm:ss.fff}", DateTime.UtcNow));
                //AddFormatting(dtData);
                AddGrouping();
            }

            private void AddGrouping()
            {
                System.Diagnostics.Debug.WriteLine(string.Format("AddGrouping().Begin: {0:yyyy/MM/dd hh:mm:ss.fff}", DateTime.UtcNow));
                List<GF> groupColumns = new List<GF>();
                int col = 0;  // subtotalling seems to use 0-based column references (in comparison to Range references which are 1-based)
                foreach (DataRow drHeader in _headerColumns)
                {
                    OrderDateParams odParams = new OrderDateParams(_headerColumns);
                    GF gf = new GF(col++, DBHelper.ToInt32(drHeader["GroupingFunctionID"]), drHeader[FLD_DATAFORMAT].ToString()
                        , odParams.DaysInRange);
                    if (gf.Valid)
                        groupColumns.Add(gf);
                }

                int[] grouped = new int[groupColumns.Count(gb => gb.GroupBy == true)];
                int[] agg = new int[groupColumns.Count(gb => gb.GroupBy == false)];
                if (grouped.Length > 0 && agg.Length > 0 && _row > 1)
                {
                    int i = 0;
                    groupColumns.FindAll(gb => gb.GroupBy == true).ForEach(gb => { grouped[i++] = gb.Col; });
                    i = 0;
                    groupColumns.FindAll(gb => gb.GroupBy == false).ForEach(gb => { agg[i++] = gb.Col; });

                    // add the Excel SubTotalling specified by the Grouping Function settings for each column
                    (_ws.Range[1, 1, _row, _headerColumns.Length] as RangeImpl).SubTotal(grouped, ConsolidationFunction.Sum, agg, false, false, true);

                    // the range will increase (due to the added summary rows) so get the UsedRange LastRow...
                    _row = _ws.UsedRange.LastRow - 1;

                    // replace the SUM subtotal excel function parameter with the actual user-commanded subtotal parameter (if not SUM)
                    // bold all subtotal cells
                    // set the background cell color to light gray all subtotal rows
                    groupColumns.FindAll(gb => gb.GroupBy == false).ForEach(
                        gb =>
                        {
                            for (int aggRow = 1; aggRow <= _row; aggRow++)
                            {
                                IRange rng = _ws.Range[aggRow, gb.Col + 1];
                                string formula = rng.Formula;
                                if (!Converter.IsNullOrEmpty(formula) && formula.Contains("(9,"))
                                {
                                    if (gb.Function != ConsolidationFunction.Count)
                                        rng.Formula = formula.Replace("(9,", "(" + gb.ExcelSubTotalParam + ",")
                                            // divide by the number of days to get the "daily" average (if a non-zero DivideByDays is specified)
                                            + (gb.DivideByDays > 1 ? string.Format("/{0:N1}", gb.DivideByDays) : "");
                                    rng.CellStyle.Font.Bold = true;
                                    _ws.Range[aggRow, 1, aggRow, gb.Col + 1].CellStyle.Color = Color.LightGray;
                                    if (gb.DataFormat.Length > 0 && !gb.DataFormat.StartsWith("="))
                                        rng.NumberFormat = gb.DataFormat;
                                }
                            }
                        }
                    );
                    // change the "Grand Total" final caption to "Grand Summary"
                    IRange x = _ws.Range[_row, 1];
                    if (x.Text == "Grand Total")
                        x.Text = "Grand Summary";

                    // freeze the header row
                    _ws.Range[2, 1].FreezePanes();
                }
                System.Diagnostics.Debug.WriteLine(string.Format("AddGrouping().End: {0:yyyy/MM/dd hh:mm:ss.fff}", DateTime.UtcNow));
            }

            private void WriteHeaderRow(int row)
            {
                int col = 1;
                foreach (DataRow drHeader in _headerColumns)
                {
                    _ws.Range[row, col++].Value = drHeader["OutputCaption"].ToString();
                }
            }

            private void AddFormatting(DataTable dtData)
            {
                // no-op if no rows to format
                if (dtData.Rows.Count == 0) return;

                System.Diagnostics.Debug.WriteLine(string.Format("AddFormatting().Begin: {0:yyyy/MM/dd hh:mm:ss.fff}", DateTime.UtcNow));
                int col = 0;
                IMigrantRange rng = _ws.MigrantRange;
                foreach (DataRow drHeader in _headerColumns)
                {
                    col++;
                    if (!DBHelper.IsNull(drHeader[FLD_DATAFORMAT]))
                    {
                        string dataField = drHeader["DataField"].ToString()
                            , alias = drHeader[FLD_ALIAS].ToString()
                            , dataFormat = drHeader[FLD_DATAFORMAT].ToString();
                        bool usedFormula = false;
                        // if the dataformat starts with an "=" sign, it is an Excel formula (but can have a suffix dataformat with a |)
                        if (dataFormat.StartsWith("="))
                        {
                            usedFormula = true;
                            string[] elems = dataFormat.Split(new char[] { '|' });
                            string formula = elems[0].Trim();
                            string numberFormat = elems.Length > 1 ? elems[1] : null;
                            string pattern = "(?<=[a-zA-Z]+)2";
                            Regex rgx = new Regex(pattern);
                            formula = rgx.Replace(formula, "@X@");
                            for (int i = 1; i <= dtData.Rows.Count; i++)
                            {
                                rng.ResetRowColumn(i+1, col);
                                // generate 
                                rng.Formula = formula.Replace("@X@", (i+1).ToString()); // rgx.Replace(formula, (i+1).ToString());
                                // if a NumberFormat was also supplied, set update the dataFormat value to this value so it will process below
                            }
                            dataFormat = numberFormat ?? "";
                        }
                        if (dataFormat.StartsWith("~"))
                        {
                            for (int i = 1; i <= dtData.Rows.Count; i++)
                            {
                                rng.ResetRowColumn(i + 1, col);
                                if (rng.HasDateTime)
                                {
                                    try
                                    {
                                        rng.SetValue(rng.DateTime.ToString(dataFormat.Substring(1)));
                                    }
                                    catch (Exception)
                                    {
                                        // do nothing
                                    }
                                }
                                else
                                {
                                    rng.SetValue(string.Format("{0:" + dataFormat.Substring(1) + "}", rng.Value2));
                                }
                            }
                        }
                        else if (dataFormat.StartsWith("@") && dataFormat.Contains("{"))
                        {
                            int i = 2;
                            foreach (DataRow dr in dtData.Rows)
                            {
                                string val = string.Empty;
                                Type dataType = dtData.Columns[alias].DataType;
                                val = string.Format(dataFormat.Substring(1), Converter.ToObjectWithDataType(dr[alias], dataType));
                                // assign the value a plain string
                                rng.ResetRowColumn(i++, col);
                                rng.SetValue(val.ToString());
                            }
                        }
                        else if (!usedFormula && dataField.Equals(NO_VALUE_DATAFIELD, StringComparison.CurrentCultureIgnoreCase))
                        {
                            IRange rngCol = _ws.Range[2, col, dtData.Rows.Count + 1, col];
                            rngCol.Text = dataFormat;
                        }
                        else if (dataFormat.Length > 0 && !dataFormat.StartsWith("="))
                        {
                            IRange rngCol = _ws.Range[2, col, dtData.Rows.Count + 1, col];
                            rngCol.NumberFormat = dataFormat;
                        }
                    }
                }
                System.Diagnostics.Debug.WriteLine(string.Format("AddFormatting().End: {0:yyyy/MM/dd hh:mm:ss.fff}", DateTime.UtcNow));
            }

            private void WriteDataRow(DataRow drData)
            {
                _row++;
                int col = 1;
                IMigrantRange rng = _ws.MigrantRange;
                foreach (DataRow drHeader in _headerColumns)
                {
                    string dataField = drHeader["DataField"].ToString()
                        , alias = drHeader[FLD_ALIAS].ToString();
                    // data retrieval will NULL any repeated OrderSingleExport values, so just skip them if NULL is present
                    if (!DBHelper.ToBoolean(drHeader["OrderSingleExport"]) || !DBHelper.IsNull(drData[alias]))
                    {
                        rng.ResetRowColumn(_row, col);
                        string dataFormat = drHeader[FLD_DATAFORMAT].ToString();
                        if (dataField.Equals(NO_VALUE_DATAFIELD, StringComparison.CurrentCultureIgnoreCase))
                        {
                            // if the dataformat starts with an "=" sign, it is an Excel formula (but can have a suffix dataformat with a |)
                            if (dataFormat.StartsWith("="))
                            {
                                string[] elems = dataFormat.Split(new char[] { '|' });
                                dataFormat = elems[0].Trim();
                                string pattern = "(?<=[a-zA-Z]+)2";
                                string replacement = _row.ToString();
                                Regex rgx = new Regex(pattern);
                                string formula = rgx.Replace(dataFormat, replacement);
                                rng.Formula = formula;
                                // if secondary dataformat value was specified, assign it now (after the "|" delimiter)
                                if (elems.Length > 1 && !elems[1].StartsWith("="))
                                    rng.NumberFormat = elems[1].Trim();
                            }
                            else
                                rng.Text = dataFormat;
                        }
                        else if (dataFormat.StartsWith("~"))
                        {
                            // default value
                            string val = string.Empty;

                            Type dataType = drData.Table.Columns[alias].DataType;
                            DateTime? dval = null;
                            if (dataType == typeof(DateTime) && (dval = Converter.ToNullableDateTime(drData[alias])).HasValue)
                            {
                                try
                                {
                                    val = dval.Value.ToString(dataFormat.Substring(1));
                                }
                                catch (Exception)
                                {
                                    val = drData[alias].ToString();
                                }
                            }
                            else
                                val = string.Format("{0:" + dataFormat.Substring(1) + "}", Converter.ToObjectWithDataType(drData[alias], dataType));

                            // assign the value a plain string
                            rng.SetValue(val.ToString());
                        }
                        else if (dataFormat.StartsWith("@") && dataFormat.Contains("{"))
                        {
                            string val = string.Empty;
                            Type dataType = drData.Table.Columns[alias].DataType;
                            val = string.Format(dataFormat.Substring(1), Converter.ToObjectWithDataType(drData[alias], dataType));
                            // assign the value a plain string
                            rng.SetValue(val.ToString());
                        }
                        else if (!dataFormat.StartsWith("="))
                        {
                            Type dataType = drData.Table.Columns[alias].DataType;
                            XlsIOHelper.AssignRangeValue(rng, drData[alias], dataType);
                            if (dataFormat.Length > 0)
                                rng.NumberFormat = dataFormat;
                        }
                    }
                    col++;
                }
            } 
        }

        internal class ReportCenterData
        {
            private ProfileValues _profileValues = null;
            private string _reportSource = string.Empty;
            private DataTable _dtUserReportColumnDefinitions = null;

            public ReportCenterData(string userName, int userReportID)
            {
                _profileValues = new ProfileValues(userName);
                using (SSDB ssdb = new SSDB())
                {
                    _reportSource = ssdb.QuerySingleValue("SELECT ReportSource FROM viewUserReportDefinition WHERE ID = {0}", userReportID).ToString();
                    _dtUserReportColumnDefinitions = ssdb.GetPopulatedDataTable("SELECT * FROM viewUserReportColumnDefinition WHERE ID = {0}", userReportID);
                }
            }
            public ReportCenterData(string userName, string reportSource, DataTable dtUserReportColumnDefinitions)
            {
                _profileValues = new ProfileValues(userName);

                _reportSource = reportSource;
                _dtUserReportColumnDefinitions = dtUserReportColumnDefinitions;
            }
            public DataTable Retrieve(bool includeNullDates)
            {
                DataTable ret = null;

                string sql = string.Empty;
                try
                {
                    WhereClause whereClause = new WhereClause(true);

                    SelectClause selectClause = new SelectClause(_profileValues, includeNullDates, _dtUserReportColumnDefinitions, whereClause);
                    // need to build this up after the SELECT clause because that is where ALIASes are assigned
                    OrderByClause orderbyClause = new OrderByClause(_dtUserReportColumnDefinitions);

                    sql = string.Format("SELECT DISTINCT TOP 100 PERCENT {0} FROM {2} RS {1}"
                        , selectClause.Get
                        , orderbyClause.Get
                        // OFR = OrderFirstRecord, which is used for the OrderSingleExport ReportCenter column attribute
                        , (_reportSource.ToLower().StartsWith("viewreportcenter_orders")? "(SELECT *, OFR = ROW_NUMBER() OVER (PARTITION BY OrderNum ORDER BY OrderNum, T_CarrierTicketNum) FROM #RS)" : "#RS")
                    ).Trim();
                    GroupByClause groupByClause = new GroupByClause(_dtUserReportColumnDefinitions);
                    if (groupByClause.Has)
                    {
                        sql = groupByClause.BuildSql(sql);
                    }
                    // SELECT ... INTO #RS FROM ... first (raw data retrieval into temp table), then formatting/grouping statement follows
                    sql = string.Format("{0}; {1}", SqlInnerClause.Get(_reportSource, whereClause.Get), sql);

                    using (SSDB ssdb = new SSDB())
                    {
                        using (System.Data.SqlClient.SqlCommand cmd = ssdb.BuildCommand(sql))
                        {
                            cmd.CommandTimeout = 300; // 5 minutes
                            ret = SSDB.GetPopulatedDataTable(cmd);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Erroneous SQL Statement: " + sql, ex);
                }
                return ret;
            }
        }

        public class SqlInnerClause
        {
            static public string Get(string reportSource, string whereClause)
            {
                // build up an inner select statement that includes the WHERE clause only (for performance optimization reasons)
                return string.Format("SELECT * INTO #RS FROM {0} RS {1}",
                    reportSource
                    , whereClause);
            }
        }

        public class OrderDateParams
        {
            const string FLD_ID = "ReportColumnID", FILTERVALUE1 = "FilterValue1", FILTERVALUE2 = "FilterValue2";

            private bool _isSubtotaled = true;

            public OrderDateParams(DataRow[] drFieldDefs)
                : this(drFieldDefs.FirstOrDefault(x => DBHelper.ToInt32(x[FLD_ID]) == (int)ReportColumnDefinition.SystemID.OrderDate))
            {
            }
            public OrderDateParams(DataTable dtFieldDefs)
                : this(dtFieldDefs.Select(string.Format("{0}={1}", FLD_ID, (int)ReportColumnDefinition.SystemID.OrderDate)).FirstOrDefault())
            {
            }
            public OrderDateParams(DateTime? start = null, DateTime? end = null)
            {
                Operator = ReportFilterOperator.TYPE.Between;
                // a start and/or end value is NULL, then use a "boundary" value to approximate no filter (so even if a filter is already defined it will always be set to a date range filter before sql execution)
                Start = start.HasValue ? start.Value : new DateTime(2000, 1, 1);
                End = end.HasValue ? end.Value : DateTime.Now.Date.AddYears(1);
            }
            public OrderDateParams(DataRow drOrderDate)
            {
                // default values
                End = DateTime.Now.Date;
                Start = DateTime.Now.Date.AddDays(-1);
                Operator = ReportFilterOperator.TYPE.Between;

                _isSubtotaled = true;
                if (drOrderDate != null)
                {
                    try
                    {
                        _isSubtotaled = DBHelper.ToInt32(drOrderDate[FLD_GROUP_FUNC]) == ID_GROUP_EXCELSUBTOTAL;

                        switch ((ReportFilterOperator.TYPE)DBHelper.ToInt32(drOrderDate["FilterOperatorID"]))
                        {
                            case ReportFilterOperator.TYPE.Between:
                                AssignByBetweenOperator(drOrderDate);
                                break;
                            case ReportFilterOperator.TYPE.Equals:
                                AssignByEqualsOperator(drOrderDate);
                                break;
                            case ReportFilterOperator.TYPE.NoFilter:
                                _isSubtotaled = true; // if not dates are available, then just treated as subtotaled so no date range is used
                                break;
                        }
                    }
                    catch (Exception)
                    {
                        // eat these, just use the default values
                    }
                }
            }
            public DateTime Start { get; private set; }
            public DateTime End { get; private set; }
            public ReportFilterOperator.TYPE Operator { get; private set; }

            public int DaysInRange
            {
                get
                {
                    // if subtotalling on the Order Date column, then we don't need to divide by the total days (just divide by 1)
                    return _isSubtotaled ? 1 : DBHelper.ToInt32((End - Start).TotalDays) + 1;
                }
            }
            private void AssignByBetweenOperator(DataRow drOrderDate)
            {
                Start = DBHelper.ToDateTime(drOrderDate[FILTERVALUE1]);
                End = DBHelper.ToDateTime(drOrderDate[FILTERVALUE2]);
            }
            private void AssignByEqualsOperator(DataRow drOrderDate)
            {
                switch (DBHelper.ToInt32(drOrderDate[FILTERVALUE1]))
                {
                    case (int)DispatchCrudeHelper.ReportDatePeriodType.WEEKTODATE:
                        Start = Core.DateHelper.StartOfWeek();
                        break;
                    case (int)DispatchCrudeHelper.ReportDatePeriodType.MONTHTODATE:
                        Start = DateTime.Now.Date.AddDays(1 - DateTime.Now.Date.Day);
                        break;
                    case (int)DispatchCrudeHelper.ReportDatePeriodType.LASTMONTH:
                        Start = DateTime.Now.Date.AddMonths(-1).AddDays(1 - DateTime.Now.Date.Day);
                        End = Start.AddMonths(1).AddDays(-1);
                        break;
                    case (int)DispatchCrudeHelper.ReportDatePeriodType.QTRTODATE:
                        Start = Core.DateHelper.StartOfQuarter();
                        break;
                    case (int)DispatchCrudeHelper.ReportDatePeriodType.YEARTODATE:
                        Start = new DateTime(DateTime.Now.Year, 1, 1);
                        break;
                    case (int)DispatchCrudeHelper.ReportDatePeriodType.YESTERDAY:
                        Start = End = End.AddDays(-1);
                        break;
                    case (int)DispatchCrudeHelper.ReportDatePeriodType.LAST7DAYS:
                        End = End.AddDays(-1);
                        Start = End.AddDays(-7);
                        break;
                    case (int)DispatchCrudeHelper.ReportDatePeriodType.LAST7DAYS_INCLUSIVE:
                        Start = End.AddDays(-6);
                        break;
                }
            }
        }
        public class SelectClause
        {
            private string _clause = string.Empty;
            private int _sqlExprCount = 0;

            public SelectClause() { }
            public SelectClause(ProfileValues profileValues, bool includeNullDates, DataTable dtUserReportColumnDefinitions, WhereClause whereClause)
            {
                //Start out assuming no ticket level fields are present in the report definition
                bool containsTicketFields = false;

                if (!dtUserReportColumnDefinitions.Columns.Contains(FLD_ALIAS)) dtUserReportColumnDefinitions.Columns.Add(FLD_ALIAS);

                foreach (DataRow row in dtUserReportColumnDefinitions.Select(null, "SortNum"))
                {
                    //Check each row for the ticket level field flag
                    if (DBHelper.ToBoolean(row["IsTicketField"]) == true)
                    {
                        containsTicketFields = true;
                        //If a ticket level field has been found, stop looking for more and continue with the process
                        break;
                    }
                }

                foreach (DataRow drColDef in dtUserReportColumnDefinitions.Select(null, "SortNum"))
                {
                    string dataField = drColDef["DataField"].ToString();

                    if (DBHelper.ToBoolean(drColDef[FLD_EXPORT]))
                    {
                        // if Sum-LASTDAY grouping function, then we are only including data for the "LastDay" by OrderDate for this field data
                        if (DBHelper.ToInt32(drColDef[FLD_GROUP_FUNC]) == ID_GROUP_FUNC_LASTDAY)
                        {
                            DateTime endDate = new OrderDateParams(dtUserReportColumnDefinitions).End;
                            dataField = string.Format("CASE WHEN RS.OrderDate = ({1}) THEN {0} ELSE NULL END", dataField, DBHelper.QuoteStr(endDate.ToShortDateString()));
                        }

                        bool isOrderSingleExport = DBHelper.ToBoolean(drColDef["OrderSingleExport"]);
                        drColDef[FLD_ALIAS] = Add(dataField, isOrderSingleExport, containsTicketFields);
                    }

                    whereClause.Add(ColumnFilterUserSql.Generate(drColDef, includeNullDates));

                    // handle any FilterBase criteria first
                    string filterDataField = drColDef["FilterDataField"].ToString();
                    if (filterDataField.Length > 0)
                        whereClause.Add(ColumnFilterBaseSql.GenerateBaseWhereClause(profileValues, DBHelper.ToInt32(drColDef["ReportColumnID"]), filterDataField));
                }
            }

            public string Add(string dataField, bool isOrderSingleExport, bool containsTicketFields)
            {
                string ret = null;

                dataField = dataField.Trim().Replace(NO_VALUE_DATAFIELD, "0");
                string clause = string.Empty;

                if (isOrderSingleExport && containsTicketFields)
                {
                    dataField = string.Format("CASE WHEN OFR=1 THEN ({0}) ELSE NULL END", dataField);
                }

                while (_clause.Contains(ret = "SQLEXPR" + (++_sqlExprCount).ToString())) ;
                clause = string.Format("[{0}] = ({1})", ret, DBHelper.IsNull(dataField, "NULL", true));

                if (clause.Length > 0)
                    _clause += string.Format("{0}{1}", _clause.Length == 0 ? "" : ", ", clause);

                return ret;
            }
            public string Get { get { return _clause; } }
        }

        public class WhereClause
        {
            private bool _includeWHEREstatement = false;
            public WhereClause(bool includeWHEREstatement)
            {
                _includeWHEREstatement = includeWHEREstatement;
            }
            private string _clause = string.Empty;
            public void Add(string clause)
            {
                if (clause.Trim().Length > 0)
                    _clause += string.Format("{0} {1}", _clause.Length == 0 ? "" : " AND", clause);
            }
            public string Get { get { return (_includeWHEREstatement && _clause.Trim().Length > 0 ? "WHERE " : "") + _clause; } }
        }

        public class GroupByClause
        {
            DataTable _dtUserReportColumnDefinitions = null;
            string _clause = string.Empty;

            public GroupByClause(DataTable dtUserReportColumnDefinitions)
            {
                _dtUserReportColumnDefinitions = dtUserReportColumnDefinitions;
                // if we are doing any aggregate calculations, then we need to group by all non-aggregated columns
                if (dtUserReportColumnDefinitions.Select("GroupingFunctionID > 9 AND Export=true").Length > 0)
                {
                    // all exported columns except (blank) or aggregate group by functions
                    DataRow[] dtGB = dtUserReportColumnDefinitions.Select("GroupingFunctionID < 9 AND Export=true");
                    foreach (DataRow drGB in dtGB)
                    {
                        string clause = drGB[FLD_ALIAS].ToString();
                        _clause += string.Format("{0} {1}", _clause.Length == 0 ? "" : ", ", clause);
                    }
                }
            }
            public bool Has { get { return _clause.Length > 0; } }
            public string Get { get { return Has ? "GROUP BY " + _clause : ""; } }
            public string BuildSql(string sql)
            {
                string ret = sql;

                SelectClause selectClause = new SelectClause();
                OrderByClause orderByClause = new OrderByClause(_dtUserReportColumnDefinitions);

                bool hasSummaryCategory = _dtUserReportColumnDefinitions.Select(string.Format("{0}={1}", FLD_GROUP_FUNC, ID_GROUP_EXCELSUBTOTAL)).Count() > 0;

                foreach (DataRow drColDef in _dtUserReportColumnDefinitions.Select("Export=true", "SortNum"))
                {
                    string dataField = drColDef[FLD_ALIAS].ToString();

                    // never group (at the sql level) the NO_VALUE_DATAFIELD
                    if (dataField != NO_VALUE_DATAFIELD)
                    {
                        string function = null;
                        int divideByDays = 0;
                        Boolean distinct = false;
                        switch (DBHelper.ToInt32(drColDef["GroupingFunctionID"]))
                        {
                            case 10: // sum
                                function = "sum";
                                break;
                            case 11: // avg
                                function = "avg";
                                break;
                            case 12: // count
                                function = "count";
                                distinct = true;
                                break;
                            case 13: // min
                                function = "min";
                                break;
                            case 14: // max
                                function = "max";
                                break;
                            case 15: // Daily Avg
                                function = "sum";
                                // daily average is daily sum / # of days
                                divideByDays = new OrderDateParams(_dtUserReportColumnDefinitions).DaysInRange;
                                break;
                            case 16: // Sum - Last Day
                                function = "sum";
                                break;
                        }
                        if (function != null)
                        {
                            dataField = string.Format("{0}({2}{1}){3}"
                                , function
                                , dataField
                                , distinct ? "DISTINCT " : ""
                                , divideByDays == 0 ? string.Empty : string.Format("/{0:N1}", divideByDays.ToString()));
                        }
                    }
                    selectClause.Add(dataField, false, false);
                }

                ret = string.Format("SELECT {0} FROM ({1}) RS {2} {3}", selectClause.Get, ret, this.Get, orderByClause.Get).Trim();
                return ret;
            }
        }

        public class OrderByClause
        {
            string _clause = string.Empty;

            public OrderByClause(DataTable dtUserReportColumnDefinitions)
            {
                // never order by "(blank)" columns
                DataRow[] dtOB = dtUserReportColumnDefinitions.Select("ReportColumnID <> 88 AND DataSort IS NOT NULL", "DataSortABS", DataViewRowState.CurrentRows);
                foreach (DataRow drOB in dtOB)
                {
                    // don't try to order by fields with no DataField or one that is not actually exported (it will crash)
                    if (drOB[FLD_ALIAS].ToString().Length > 0 && DBHelper.ToBoolean(drOB[FLD_EXPORT]))
                    {
                        string clause = drOB[FLD_ALIAS] + (DBHelper.ToInt16(drOB["DataSort"]) < 0 ? " DESC" : "");
                        _clause += string.Format("{0} {1}", _clause.Length == 0 ? "" : ", ", clause);
                    }
                }
            }
            public string Get { get { return Has ? "ORDER BY " + _clause : ""; } }
            public bool Has { get { return _clause.Length > 0; } }
        }

        public class ColumnFilterBaseSql
        {
            static protected WhereClause GenerateWhere(SSDB ssdb, ProfileValues profileValues, int reportColumnID, bool includeWHEREstatement = true)
            {
                WhereClause whereClause = new WhereClause(includeWHEREstatement);

                DataTable dtBaseFilters = ssdb.GetPopulatedDataTable(
                    "SELECT * FROM tblReportColumnDefinitionBaseFilter WHERE ReportColumnID={0}"
                    , new object[] { reportColumnID });

                foreach (DataRow drBaseFilter in dtBaseFilters.Rows)
                {
                    if (DBHelper.ToInt32(drBaseFilter["BaseFilterTypeID"]) == 2)
                    {
                        // converting NULL to -1 is not ideal, this entire scenario needs to be revisited
                        string propertyValue = profileValues.GetValue(drBaseFilter["ProfileField"].ToString(), "-1");
                        whereClause.Add(drBaseFilter["IncludeWhereClause"].ToString().Replace(
                                drBaseFilter["IncludeWhereClause_ProfileTemplate"].ToString()
                                , propertyValue));
                    }
                }
                return whereClause;
            }

            static public string GenerateDropDownSql(string userName, int reportColumnID, string sql = null)
            {
                return GenerateDropDownSql(new ProfileValues(userName), reportColumnID, sql);
            }
            static public string GenerateDropDownSql(ProfileValues profileValues, int reportColumnID, string sql = null)
            {
                string ret = string.Empty;

                using (SSDB ssdb = new SSDB())
                {
                    if (sql == null)
                        sql = ssdb.QuerySingleValue("SELECT DropDownSql FROM tblReportColumnDefinition WHERE ReportColumnID={0}", reportColumnID).ToString();

                    WhereClause whereClause = GenerateWhere(ssdb, profileValues, reportColumnID, false);
                    if (whereClause.Get.Length > 0)
                    {
                        if (sql.Contains("ORDER BY"))
                        {
                            sql = sql.Truncate(sql.IndexOf("ORDER BY") - 1);
                        }
                        ret = string.Format("SELECT ID, Name FROM ({0}) X WHERE {1} ORDER BY Name", sql, whereClause.Get);
                    }
                }
                return ret;
            }

            static public string GenerateBaseWhereClause(ProfileValues profileValues, int reportColumnID, string filterDataField)
            {
                string ret = string.Empty;

                using (SSDB ssdb = new SSDB())
                {
                    WhereClause whereClause = GenerateWhere(ssdb, profileValues, reportColumnID, false);
                    if (whereClause.Get.Length > 0)
                    {
                        // replace the generic "ID" field used by the base filter generation system to the actual FilterDataField used by the base sql statement
                        ret = whereClause.Get.Replace("[ID]", filterDataField).Replace(" ID", " " + filterDataField);
                    }
                }

                return ret;
            }

        }

        public class ColumnFilterUserSql
        {
            static public string Generate(DataRow drUserReportColDef, bool includeNullDates)
            {
                string ret = string.Empty;

                int reportColumnID = DBHelper.ToInt32(drUserReportColDef["ReportColumnID"]);
                string dataField = drUserReportColDef["FilterDataField"].ToString()
                    , op = string.Empty
                    , filterValue = drUserReportColDef["FilterValue1"].ToString();
                int filterOpID = DBHelper.ToInt32(drUserReportColDef["FilterOperatorID"]);

                if (reportColumnID == (int)ReportColumnDefinition.SystemID.OrderDate && filterOpID != (int)ReportFilterOperator.TYPE.NoFilter)
                {
                    OrderDateParams dates = new OrderDateParams(drUserReportColDef);
                    ret = string.Format("((OrderDate >= '{0}' AND OrderDate < '{1}'){2})"
                        , dates.Start.ToShortDateString()
                        , dates.End.AddDays(1).ToShortDateString()
                        , includeNullDates ? " OR OrderDate IS NULL" : "");
                }
                // Settlement Batch retrieval performance optimization
                else if (dataField.EndsWith("BatchNum") && filterOpID == (int)ReportFilterOperator.TYPE.Equals)
                {
                    ret = string.Format("ID IN (SELECT OrderID FROM viewOrderSettlement{1} WHERE BatchNum = {0})"
                        , DBHelper.QuoteStr(filterValue)
                        , dataField.Replace("BatchNum", "").Trim());
                }
                else
                {
                    switch (filterOpID)
                    {
                        case (int)ReportFilterOperator.TYPE.NoFilter:
                            dataField = null;
                            break;
                        case (int)ReportFilterOperator.TYPE.Equals:
                            op = "=";
                            filterValue = DBHelper.QuoteStr(filterValue);
                            break;
                        case (int)ReportFilterOperator.TYPE.LessThan:
                            op = "<";
                            filterValue = DBHelper.QuoteStr(filterValue);
                            break;
                        case (int)ReportFilterOperator.TYPE.LessThanOrEqual:
                            op = "<=";
                            filterValue = DBHelper.QuoteStr(filterValue);
                            break;
                        case (int)ReportFilterOperator.TYPE.GreaterThan:
                            op = ">";
                            filterValue = DBHelper.QuoteStr(filterValue);
                            break;
                        case (int)ReportFilterOperator.TYPE.GreaterThanOrEqual:
                            op = ">=";
                            filterValue = DBHelper.QuoteStr(filterValue);
                            break;
                        case (int)ReportFilterOperator.TYPE.IN:
                        case (int)ReportFilterOperator.TYPE.NotIN:
                            op = filterOpID == (int)ReportFilterOperator.TYPE.IN ? "IN" : " NOT IN";
                            filterValue = string.Format("({0})", new IdContainer(filterValue).CSV());
                            if (filterValue == "()") filterValue = "('0')";
                            break;
                        case (int)ReportFilterOperator.TYPE.Between:
                            op = "BETWEEN";
                            filterValue = string.Format("{0} AND {1}", DBHelper.QuoteStr(filterValue), DBHelper.QuoteStr(drUserReportColDef["FilterValue2"]));
                            break;
                        case (int)ReportFilterOperator.TYPE.StartsWith:
                            op = "LIKE";
                            filterValue = DBHelper.QuoteStr(string.Format("{0}%", filterValue));
                            break;
                        case (int)ReportFilterOperator.TYPE.Contains:
                            op = "LIKE";
                            filterValue = DBHelper.QuoteStr(string.Format("%{0}%", filterValue));
                            break;
                        case (int)ReportFilterOperator.TYPE.IsEmpty:
                        case (int)ReportFilterOperator.TYPE.IsNotEmpty:
                            op = "IS" + (filterOpID == (int)ReportFilterOperator.TYPE.IsNotEmpty ? " NOT" : "");
                            dataField = string.Format("nullif(ltrim({0}), '')", dataField);
                            filterValue = "NULL";
                            break;
                    }
                    if (dataField != null)
                        ret = string.Format("{0} {1} {2}", dataField, op, filterValue);
                }
                return ret;
            }
        }
    }

    static class ReportExtensions
    {
        public static DataRow EnsureColRowExists(this DataTable dtColDef, ReportColumnDefinition.SystemID colType
            , ReportFilterOperator.TYPE op = ReportFilterOperator.TYPE.NoFilter, object filterValue1 = null, object filterValue2 = null
            , ReportGroupingFunction.TYPE groupingFunction = ReportGroupingFunction.TYPE.Min
            , bool onlyNewIfFiltered = false)
        {
            DataRow ret = null;
            // if an existing row exists, ensure the filter settings are correct
            if ((ret = dtColDef.Select(string.Format("ReportColumnID={0}", (int)colType)).FirstOrDefault()) != null)
            {
                ret["FilterOperatorID"] = (int)op;
                ret["FilterValue1"] = filterValue1;
                ret["FilterValue2"] = filterValue2;
            }
            // if not and we need to add a new Col Row definition, add it now (with the specified parameters)
            else if (!onlyNewIfFiltered || op != ReportFilterOperator.TYPE.NoFilter)
            {
                ret = dtColDef.NewColRow(colType, op, filterValue1, filterValue2, groupingFunction);
            }

            return ret;
        }
        // this will add a new Col Row definition by Col Type (retrieving the base values for the specified type) and the optional additional parameters
        public static DataRow NewColRow(this DataTable dtColDef, ReportColumnDefinition.SystemID colType
            , ReportFilterOperator.TYPE op = ReportFilterOperator.TYPE.NoFilter, object filterValue1 = null, object filterValue2 = null
            , ReportGroupingFunction.TYPE groupingFunction = ReportGroupingFunction.TYPE.Min)
        {
            using (SSDB db = new SSDB())
            {
                return dtColDef.NewColRow(db.GetPopulatedDataTable("SELECT ReportColumnID = ID, * FROM viewReportColumnDefinition WHERE ID = {0}", (object)(int)colType).Rows[0]
                    , groupingFunction, op, filterValue1, filterValue2);
            }
        }
        // this will add a new Col Row definition from an already retrieved base type DataRow + optional additional parameters
        public static DataRow NewColRow(this DataTable dtColDef, DataRow baseRow
            , ReportGroupingFunction.TYPE groupingFunction = ReportGroupingFunction.TYPE.Min
            , ReportFilterOperator.TYPE op = ReportFilterOperator.TYPE.NoFilter, object filterValue1 = null, object filterValue2 = null)
        {
            DataRow nr = dtColDef.NewRow();
            nr["UserReportID"] = dtColDef.Rows[0]["UserReportID"];
            nr["ReportColumnID"] = baseRow["ReportColumnID"];
            nr["DataField"] = baseRow["DataField"];
            nr["FilterDataField"] = DBHelper.IsNull(baseRow["FilterDataField"], baseRow["DataField"], true);
            nr["OrderSingleExport"] = baseRow["OrderSingleExport"];
            nr["Caption"] = baseRow["Caption"].ToString().Split('|').Last().Trim();
            nr["GroupingFunctionID"] = (int)groupingFunction;
            nr["SortNum"] = dtColDef.Rows.Count;
            nr[ReportCenterExcelExport.FLD_EXPORT] = false;
            nr["FilterOperatorID"] = (int)op;
            nr["FilterValue1"] = filterValue1 ?? DBNull.Value;
            nr["FilterValue2"] = filterValue2 ?? DBNull.Value;
            dtColDef.Rows.Add(nr);
            return nr;
        }
    }
}