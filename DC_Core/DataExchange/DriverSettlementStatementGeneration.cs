﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DispatchCrude.Models;
using Syncfusion.DocIO;
using Syncfusion.DocIO.DLS;
using Syncfusion.DocToPDFConverter;
using Syncfusion.Pdf;
using AlonsIT;

namespace DispatchCrude.DataExchange
{
    public class DCWordDocument
    {
        public WordDocument WordDocument { get; private set; }
        public DataRow DRMain { get; private set; }
        public DCWordDocument(WordDocument wordDoc, DataRow drMain)
        {
            this.WordDocument = wordDoc;
            this.DRMain = drMain;
        }
    }
    public class DriverSettlementStatementWordMailMerge: IEnumerable<DCWordDocument>
    {
        private MemoryStream _msWordTemplate = null;
        private ArrayList _relationships = new ArrayList();
        private DataSet _ds = new DataSet();

        public DriverSettlementStatementWordMailMerge(byte[] wordTemplate, int pdfExportID, int batchID, int driverID = 0)
        {
            _msWordTemplate = new MemoryStream(wordTemplate);
            // get the data for the mail merge
            ArrayList commands = new ArrayList();
            using (SSDB db = new SSDB())
            {
                DataTable dt = db.GetPopulatedDataTable("SELECT * FROM viewDriverSettlementBatch_PerDriver WHERE ID = {0} AND {1} IN (0, OriginDriverID)"
                    , (object)batchID, driverID);
                dt.TableName = "FB";
                _ds.Tables.Add(dt);

                dt = dt.Clone();
                dt.TableName = "B";
                _ds.Tables.Add(dt);
                _relationships.Add(new DictionaryEntry(dt.TableName, string.Empty));

                dt = db.GetPopulatedDataTable("SELECT * FROM viewOrder_Financial_Base_Driver WHERE BatchID = {0}", (object)batchID);
                dt.TableName = "O";
                _ds.Tables.Add(dt);
                _relationships.Add(new DictionaryEntry(dt.TableName, "BatchOriginDriverID = %B.BatchOriginDriverID%"));

                dt = db.GetPopulatedDataTable("SELECT * FROM viewOrderSettlementDriver_Amounts WHERE BatchID = {0} ORDER BY SortPos, Name", (object)batchID);
                dt.TableName = "OA";
                _ds.Tables.Add(dt);
                _relationships.Add(new DictionaryEntry(dt.TableName, "OrderID = %O.ID%"));

                dt = db.GetPopulatedDataTable(
                    @"SELECT BatchOriginDriverID, Name, Amount = sum(Amount)
                        FROM viewOrderSettlementDriver_Amounts 
                        WHERE BatchID = {0}
                        GROUP BY BatchOriginDriverID, Name, SortPos
                        ORDER BY SortPos, Name"
                    , (object)batchID);
                dt.TableName = "OAT";
                _ds.Tables.Add(dt);
                _relationships.Add(new DictionaryEntry(dt.TableName, "BatchOriginDriverID = %B.BatchOriginDriverID%"));
            }

        }

        public IEnumerator<DCWordDocument> GetEnumerator()
        {
            // iterate over all the Batch/Driver rows, and add only the "next" one into the "B" table
            foreach (DataRow drMain in _ds.Tables["FB"].Rows)
            {
                _ds.Tables["B"].Rows.Clear();
                _ds.Tables["B"].Rows.Add(drMain.ItemArray);

                // Step 2: obtain a WordDocument from the template (stream)
                _msWordTemplate.Seek(0, SeekOrigin.Begin);

                WordDocument wordDoc = new WordDocument(_msWordTemplate, FormatType.Automatic);
                // Step 3: do the MailMerge operation to add the dynamic data to the template
                wordDoc.MailMerge.ExecuteNestedGroup(_ds, _relationships);
                yield return new DCWordDocument(wordDoc, drMain);
            }
        }
        // required, generic IEnumerable implementation
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

    public class DCPdfDocument
    {
        private PdfDocument _doc;
        public DCPdfDocument(WordDocument wordDoc)
        {
            _doc = new DocToPDFConverter().ConvertToPDF(wordDoc);
        }
        public string FileName { get; set; }
        public MemoryStream Stream
        {
            get
            {
                MemoryStream ms = new MemoryStream();
                _doc.Save(ms);
                return ms;
            }
        }
        public byte[] Bytes
        {
            get
            {
                return Stream.ToArray();
            }
        }
        public PdfDocument PdfDocument { get { return _doc; } }
    }

    public class DriverSettlementStatementPdfDocs : IEnumerable<DCPdfDocument>
    {
        DriverSettlementStatementWordMailMerge engine = null;
        public DriverSettlementStatementPdfDocs(int pdfExportID, int batchID, int driverID = 0)
        {
            // Step1: retrieve the template into a WordDocument (using SSDB instead of DispatchCrudeDB so we can "switch" the database connection in the "Engine")
            using (SSDB db = new SSDB())
            {
                MemoryStream ms = new MemoryStream();
                if (!db.QuerySingleToStream(ms, "SELECT WordDocument FROM tblPdfExport WHERE ID = {0}", (object)pdfExportID))
                {
                    throw new Exception("Pdf Export definition not found!");
                }
                ms.Seek(0, SeekOrigin.Begin);
                engine = new DriverSettlementStatementWordMailMerge(ms.GetBuffer(), pdfExportID, batchID, driverID);
            }
        }
        public IEnumerator<DCPdfDocument> GetEnumerator()
        {
            // iterate over all the Batch/Driver rows, and add only the "next" one into the "B" table
            foreach (DCWordDocument dcWordDoc in engine)
            {
                DCPdfDocument pdfDoc = new DCPdfDocument(dcWordDoc.WordDocument);
                pdfDoc.FileName = string.Format("{0:yyyy-MM-dd}_{1}_{2}.pdf"
                    , DBHelper.ToDateTime(dcWordDoc.DRMain["PeriodEndDate"])
                    , dcWordDoc.DRMain["BatchOriginDriver"]
                    , dcWordDoc.DRMain["BatchNum"]);
                yield return pdfDoc;
            }
        }
        // required, generic IEnumerable implementation
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

    public class DCHtmlDocument
    {
        private WordDocument _wordDoc = null;

        public DCHtmlDocument(WordDocument wordDoc)
        {
            _wordDoc = wordDoc;
        }
        public string FileName { get; set; }
        public MemoryStream Stream
        {
            get
            {
                MemoryStream ms = new MemoryStream();
                _wordDoc.Save(ms, FormatType.Html);
                return ms;
            }
        }
        public byte[] Bytes
        {
            get
            {
                return Stream.ToArray();
            }
        }
    }

    public class DriverSettlementStatementHtmlDocs : IEnumerable<DCHtmlDocument>
    {
        DriverSettlementStatementWordMailMerge engine = null;
        public DriverSettlementStatementHtmlDocs(int definitionID, int batchID, int driverID = 0)
        {
            // Step1: retrieve the template into a WordDocument (using SSDB instead of DispatchCrudeDB so we can "switch" the database connection in the "Engine")
            using (SSDB db = new SSDB())
            {
                int pdfExportID = DBHelper.ToInt32(db.QuerySingleValue("SELECT PdfExportID FROM tblDriverSettlementStatementEmailDefinition WHERE ID = {0}", (object)definitionID));

                MemoryStream ms = new MemoryStream();
                if (!db.QuerySingleToStream(ms, "SELECT EmailBodyWordDocument FROM tblDriverSettlementStatementEmailDefinition WHERE ID = {0}", (object)definitionID))
                {
                    throw new Exception("Email definition not found!");
                }
                ms.Seek(0, SeekOrigin.Begin);
                engine = new DriverSettlementStatementWordMailMerge(ms.GetBuffer(), pdfExportID, batchID, driverID);
            }
        }
        public IEnumerator<DCHtmlDocument> GetEnumerator()
        {
            // iterate over all the Batch/Driver rows, and add only the "next" one into the "B" table
            foreach (DCWordDocument dcWordDoc in engine)
            {
                DCHtmlDocument HtmlDoc = new DCHtmlDocument(dcWordDoc.WordDocument);
                HtmlDoc.FileName = string.Format("EmailBody_{0:yyyy-MM-dd}_{1}_{2}.html"
                    , DBHelper.ToDateTime(dcWordDoc.DRMain["PeriodEndDate"])
                    , dcWordDoc.DRMain["BatchOriginDriver"]
                    , dcWordDoc.DRMain["BatchNum"]);
                yield return HtmlDoc;
            }
        }
        // required, generic IEnumerable implementation
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

}
