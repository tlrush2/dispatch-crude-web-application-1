﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DispatchCrude.Extensions
{
    // adapted from http://stackoverflow.com/questions/32061770/call-orderby-with-a-field-name-as-a-string 

    public static class LinqExtensions
    {
        public static PropertyInfo GetPropertyInfo(Type objType, string name, bool ignoreCase = false)
        {
            var properties = objType.GetProperties();
            var matchedProperty = properties.FirstOrDefault(p => p.Name.Equals(name, ignoreCase ? StringComparison.CurrentCultureIgnoreCase : StringComparison.CurrentCulture));
            if (matchedProperty == null)
                throw new ArgumentException("name");

            return matchedProperty;
        }
        private static LambdaExpression GetOrderExpression(Type objType, PropertyInfo pi)
        {
            var paramExpr = Expression.Parameter(objType);
            var propAccess = Expression.PropertyOrField(paramExpr, pi.Name);
            var expr = Expression.Lambda(propAccess, paramExpr);
            return expr;
        }

        public static IEnumerable<T> OrderBy<T>(this IEnumerable<T> query, string name, bool descending = false, bool ignoreCase = false)
        {
            var propInfo = GetPropertyInfo(typeof(T), name, ignoreCase);
            var expr = GetOrderExpression(typeof(T), propInfo);
            var methodName = descending ? "OrderBy" : "OrderByDescending";
            var method = typeof(Enumerable).GetMethods().FirstOrDefault(m => m.Name == methodName && m.GetParameters().Length == 2);
            var genericMethod = method.MakeGenericMethod(typeof(T), propInfo.PropertyType);
            return (IEnumerable<T>)genericMethod.Invoke(null, new object[] { query, expr.Compile() });
        }

        public static IQueryable<T> OrderBy<T>(this IQueryable<T> query, string name, bool descending = false, bool ignoreCase = false)
        {
            var propInfo = GetPropertyInfo(typeof(T), name);
            var expr = GetOrderExpression(typeof(T), propInfo);
            var methodName = descending ? "OrderBy" : "OrderByDescending";
            var method = typeof(Queryable).GetMethods().FirstOrDefault(m => m.Name == methodName && m.GetParameters().Length == 2);
            var genericMethod = method.MakeGenericMethod(typeof(T), propInfo.PropertyType);
            return (IQueryable<T>)genericMethod.Invoke(null, new object[] { query, expr });
        }

        /* used to generate a dynamic .Select() clause using a string array of property names */
        public static object DynamicProjection(object input, IEnumerable<string> properties)
        {
            var type = input.GetType();
            dynamic dObject = new ExpandoObject();
            var dDict = dObject as IDictionary<string, object>;

            foreach (var p in properties)
            {
                var field = type.GetField(p);
                if (field != null)
                    dDict[p] = field.GetValue(input);

                var prop = type.GetProperty(p);
                if (prop != null && prop.GetIndexParameters().Length == 0)
                    dDict[p] = prop.GetValue(input, null);
            }

            return dObject;
        }
    }
}
