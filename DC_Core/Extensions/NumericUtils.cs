﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DispatchCrude.Extensions
{
    public class NumericUtils
    {
        public static string RemoveNonNumeric(string value)
        {
            return value == null ? value : Regex.Replace(value, @"\D", "");
        }
    }
}
