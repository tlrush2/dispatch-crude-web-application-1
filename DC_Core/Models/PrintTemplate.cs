using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DispatchCrude.Models
{
    [Table("tblPrintTemplate")]
    public class PrintTemplate  : AuditModelLastChangeBase
    {
        [Key]
        public int ID { get; set; }

        public int HeaderImageLeft { get; set; }
        public int HeaderImageTop { get; set; }
        public int HeaderImageWidth { get; set; }
        public int HeaderImageHeight { get; set; }

        public string TemplateText { get; set; }
    }
}
