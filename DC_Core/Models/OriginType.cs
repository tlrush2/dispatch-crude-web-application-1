using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DispatchCrude.Models
{
    [Table("tblOriginType")]
    public class OriginType  : AuditModelDeleteBase, IValidatableObject
    {
        public OriginType()
        {
            this.Origins = new List<Origin>();
        }

        [Key]
        public int ID { get; set; }

        [Required]
        [Column("OriginType")]
        [StringLength(25)]
        public string Name { get; set; }

        [DisplayName("Has Tanks?")]
        public bool HasTanks { get; set; }

        public IEnumerable<Origin> Origins { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!ValidateDuplicateName(ID, Name))
                yield return new ValidationResult("Name is already in use");
            Validated = true;
        }

        private bool ValidateDuplicateName(int id, string name)
        {
            using (DispatchCrudeDB db = new DispatchCrudeDB())
            {
                return db.OriginTypes.Where(m => m.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase) && (id == 0 || m.ID != id)).Count() == 0;
            }
        }
    }
}
