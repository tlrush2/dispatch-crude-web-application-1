﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using System.Linq;
using System;

namespace DispatchCrude.Models
{
    [Table("tblDriverSettlementStatementEmailDefinition")]
    public class DriverSettlementStatementEmailDefinitionBase : AuditModelDeleteBase, IValidatableObject
    {
        [Key]
        public int ID { get; set; }

        [Required, MaxLength(50)]
        public string Name { get; set; }

        [DisplayName("Pdf Export")]
        [UIHint("_ForeignKeyDDL")]
        public int PdfExportID { get; set; }

        [DisplayName("Email Subject")]
        public string EmailSubject { get; set; }

        [DisplayName("Email Body Word Template")]
        public byte[] EmailBodyWordDocument { get; set; }
        [DisplayName("Email Body Word Template")]
        public string EmailBodyWordDocName { get; set; }

        [DisplayName("CC Email Address"), StringLength(255)]
        public string CCEmailAddress { get; set; }

        [DisplayName("Missing Email Address"), StringLength(255)]
        public string MissingEmailAddress { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!ValidateDuplicateName(ID, Name))
                yield return new ValidationResult("Name is already in use");
            Validated = true;
        }

        static protected bool ValidateDuplicateName(int id, string name)
        {
            using (DispatchCrudeDB db = new DispatchCrudeDB())
            {
                return db.Set<DriverSettlementStatementEmailDefinition>().Where(m => m.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase) && (id == 0 || m.ID != id)).Count() == 0;
            }
        }
    }
    public class DriverSettlementStatementEmailDefinition : DriverSettlementStatementEmailDefinitionBase
    {
        [DisplayName("Pdf Export")]
        [ForeignKey("PdfExportID")]
        public PdfExport PdfExport { get; set; }
    }
}
