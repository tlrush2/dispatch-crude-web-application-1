﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace DispatchCrude.Models
{
    [Table("tblDriverWeekShiftType")]
    public class DriverWeekShiftType : AuditModelLastChangeBase
    {
        [Key]
        public int ID { get; set; }

        [Required, StringLength(100)]
        public string Name { get; set; }

        public TimeSpan? SunStartTime { get; set; }
        public TimeSpan? MonStartTime { get; set; }
        public TimeSpan? TueStartTime { get; set; }
        public TimeSpan? WedStartTime { get; set; }
        public TimeSpan? ThuStartTime { get; set; }
        public TimeSpan? FriStartTime { get; set; }
        public TimeSpan? SatStartTime { get; set; }
    }
}
