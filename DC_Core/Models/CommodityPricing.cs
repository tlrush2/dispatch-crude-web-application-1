﻿using AlonsIT;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web.Mvc;

namespace DispatchCrude.Models
{
    [Table("tblCommodityIndex")]
    public class CommodityIndex: IEDTO, IValidatableObject
    {
        [Key]
        [ScaffoldColumn(false)]
        public int ID { get; set; }

        [Required, StringLength(100)]
        public string Name { get; set; }

        [Required, StringLength(15)]
        [DisplayName("Short Name")]
        public string ShortName { get; set; }

        [StringLength(255)]
        public string Description { get; set; }

        [Required]
        [DisplayName("Product Group"), UIHint("GridForeignKey")]
        public int ProductGroupID { get; set; }
        [ForeignKey("ProductGroupID")]
        public virtual ProductGroup ProductGroup { get; set; }

        [Required]
        [DisplayName("Unit of Measure"), UIHint("GridForeignKey")]
        public int UomID { get; set; }
        [ForeignKey("UomID")]
        public virtual Uom Uom { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!ValidateDuplicateName(ID, Name))
                yield return new ValidationResult("Index name is already in use");
            if (!ValidateDuplicateShortName(ID, ShortName))
                yield return new ValidationResult("Short name is already in use");
            Validated = true;
        }

        private bool ValidateDuplicateName(int id, string name)
        {
            using (DispatchCrudeDB db = new DispatchCrudeDB())
            {
                return db.CommodityIndexes.Where(m => m.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase) && (id == 0 || m.ID != id)).Count() == 0;
            }
        }

        private bool ValidateDuplicateShortName(int id, string shortname)
        {
            using (DispatchCrudeDB db = new DispatchCrudeDB())
            {
                return db.CommodityIndexes.Where(m => m.ShortName.Equals(shortname, StringComparison.CurrentCultureIgnoreCase) && (id == 0 || m.ID != id)).Count() == 0;
            }
        }
    }


    [Table("tblCommodityMethod")]
    public class CommodityMethod: IEDTO
    {
        public enum TYPE { CMA = 1, TMA = 2, PICK = 3 }

        [Key]
        public int ID { get; set; }

        [Required, StringLength(100)]
        public string Name { get; set; }

        [Required, StringLength(15)]
        [DisplayName("Short Name")]
        public string ShortName { get; set; }

        [StringLength(255)]
        public string Description { get; set; }

        [DisplayName("Trade Days Only?")]
        public bool TradeDaysOnly { get; set; }
    }
    

    [Table("tblCommodityPrice")]
    public class CommodityPrice: IEDTO, IValidatableObject
    {
        [Key]
        [ScaffoldColumn(false)]
        public int ID { get; set; }

        [Required]
        [DisplayName("Price Date"), UIHint("Date")]
        public DateTime PriceDate { get; set; }

        [Required]
        [UIHint("_ForeignKeyDDL")]
        [DisplayName("Index")]        
        public int CommodityIndexID { get; set; }

        [ForeignKey("CommodityIndexID")]
        [DisplayName("CommodityIndexID")]
        public virtual CommodityIndex CommodityIndex { get; set; }

        [Required]
        [Range(0, Double.MaxValue, ErrorMessage = "Price cannot be negative")]
        [DisplayName("Price")]
        public decimal IndexPrice { get; set; }

        [NotMapped]
        public bool Locked
        {
            get
            {
                using (SSDB ssdb = new SSDB())
                {
                    return DBHelper.ToBoolean(ssdb.QuerySingleValue(
                            "SELECT Locked FROM viewCommodityPrice WHERE ID = {0}", (object)this.ID));
                }
            }
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!ValidatePriceDate())
                yield return new ValidationResult("A price already exists for this index on this date");            
            Validated = true;
        }

        private bool ValidatePriceDate()
        {
            using (DispatchCrudeDB db = new DispatchCrudeDB())
            {                
                return db.CommodityPrices.Count(m =>  m.PriceDate == this.PriceDate.Date &&
                                                m.CommodityIndexID == this.CommodityIndexID                                                
                                                && (this.ID == 0 || m.ID != this.ID)) == 0;                
            }
        }

        public override bool allowRowEdit
        {
            get
            {
                //Override normal edit behavior: if the record has been used in settlement, do not allow edit
                return !Locked;
            }
        }
    }


    [Table("tblCommodityPurchasePricebook")]
    public class CommodityPurchasePricebook : AuditModelLastChangeBase
    {
        [Key]
        public int ID { get; set; }

        [DisplayName("Order Start Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}"), UIHint("Date")]
        [Required]
        public DateTime? OrderStartDate { get; set; }

        [Required]
        [DisplayName("Order End Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}"), UIHint("Date")]
        public DateTime? OrderEndDate { get; set; }

        [DisplayName("Producer")]
        public int? ProducerID { get; set; }
        [ForeignKey("ProducerID")]
        public Producer Producer { get; set; }

        [DisplayName("Operator")]
        public int? OperatorID { get; set; }
        [ForeignKey("OperatorID")]
        public Operator Operator { get; set; }

        [DisplayName("Product")]
        public int? ProductID { get; set; }
        [ForeignKey("ProductID")]
        public Product Product { get; set; }

        [DisplayName("Product Group")]
        public int? ProductGroupID { get; set; }
        [ForeignKey("ProductGroupID")]
        public ProductGroup ProductGroup { get; set; }

        [DisplayName("Origin")]
        public int? OriginID { get; set; }
        [ForeignKey("OriginID")]
        public Origin Origin { get; set; }

        [DisplayName("Origin State")]
        public int? OriginStateID { get; set; }
        [ForeignKey("OriginStateID")]
        public State OriginState { get; set; }

        [DisplayName("Origin Region")]
        public int? OriginRegionID { get; set; }
        [ForeignKey("OriginRegionID")]
        public Region OriginRegion { get; set; }


        [DisplayName("Index")]
        [Required]
        public int CommodityIndexID { get; set; }
        [ForeignKey("CommodityIndexID")]
        public virtual CommodityIndex CommodityIndex { get; set; }

        [DisplayName("Method")]
        [Required]
        public int CommodityMethodID { get; set; }
        [ForeignKey("CommodityMethodID")]
        public virtual CommodityMethod CommodityMethod { get; set; }

        [DisplayName("Index Start Date"), UIHint("Date"), DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        [Required]
        public DateTime? IndexStartDate { get; set; }

        [DisplayName("Index End Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}"), UIHint("Date")]
        public DateTime? IndexEndDate { get; set; }

        [Required]
        [DisplayName("Settlement Units")]
        public int SettlementFactorID { get; set; }
        [ForeignKey("SettlementFactorID")]
        public virtual SettlementFactor SettlementFactor { get; set; }

        [Range(0, Double.MaxValue, ErrorMessage = "Deduct cannot be negative.  Value will be subtracted")]
        public decimal? Deduct { get; set; }
        [DisplayName("Deduct Type"), UIHint("AmountTypes"), StringLength(1)]
        public string DeductType { get; set; }

        [Range(0, Double.MaxValue, ErrorMessage = "Premium cannot be negative.")]
        public decimal? Premium { get; set; }
        [DisplayName("Premium Type"), UIHint("AmountTypes")]
        public string PremiumType { get; set; }
        [DisplayName("Premium Description")]
        public string PremiumDesc { get; set; }

        [NotMapped]
        public bool Locked 
        {
            get
            {
                using (SSDB ssdb = new SSDB())
                {
                    return DBHelper.ToBoolean(ssdb.QuerySingleValue(
                            "SELECT Locked FROM viewCommodityPurchasePricebook WHERE ID = {0}", (object)this.ID));
                }
            }
        }

        [NotMapped]
        public DateTime? LockDate
        {
            get
            {
                using (SSDB ssdb = new SSDB())
                {
                    return DBHelper.ToDateTime(ssdb.QuerySingleValue(
                            "SELECT MinEndDate FROM viewCommodityPurchasePricebook WHERE ID = {0}", (object)this.ID));
                }
            }
        }

        public SelectList TypeSelectList(string selected = null)
        {
            List<string> types = new List<string>();
            types.Add("$");
            types.Add("%");

            return new SelectList(types, selected);
        }
        [NotMapped]
        public string PremiumText
        {
            get
            {
                if (this.PremiumType == "$" && this.Premium.HasValue)
                    return this.Premium.Value.ToString("C2");
                else if (this.PremiumType == "%" && this.Premium.HasValue)
                    return this.Premium.Value + "%";
                return null;
            }
        }
        [NotMapped]
        public string DeductText
        {
            get
            {
                if (this.DeductType == "$" && this.Deduct.HasValue)
                    return this.Deduct.Value.ToString("C2");
                else if (this.DeductType == "%" && this.Deduct.HasValue)
                    return this.Deduct.Value + "%";
                return null;
            }
        }

    }


    [Table("tblNonTradeDays")]
    public class NonTradeDays : AuditModelDeleteBase
    {
        [Key]
        [Required]
        [DisplayName("Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        [Remote("IsDateAvailable", "CommodityPricing", ErrorMessage = "Date already exists in this list.")]   // This does not work!  WHHHYYYYY! :-(
        public DateTime HolidayDate { get; set; }

        [Required]
        [DisplayName("Holiday Name")]
        public string Name { get; set; }
    }
}