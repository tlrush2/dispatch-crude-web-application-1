using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DispatchCrude.Models
{
    [Table("tblCountry")]
    public class Country: IEDTO
    {
        [Key]
        public int ID { get; set; }

        [Required, StringLength(5)]
        public string Abbrev { get; set; }
        
        [Required, StringLength(50)]
        public string Name { get; set; }

        [StringLength(15)]
        public string UOT { get; set; }

        [StringLength(1)]
        public string ShortUOT { get; set; }
    }
}
