using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Data.Entity;
using System.Web.Mvc;
using AlonsIT;
using DispatchCrude.Core;

namespace DispatchCrude.Models
{
    [Table("tblOrderTicket")]
    public class OrderTicket : AuditModelDeleteBase, IValidatableObject
    {
        public static int MAX_TICKET_GALLONS = 13440; // this should not be used, instead check global DB setting #6

        public OrderTicket()
        {
            this.UID = new Guid();
        }
        public OrderTicket(Guid uid)
        {
            this.UID = uid;
        }
        public OrderTicket(Guid uid, int orderID)
        {
            this.UID = uid;
            this.OrderID = orderID;
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid UID { get; set; }

        [Required, StringLength(15), DisplayName("Ticket #")]
        [Remote(
            "IsCarrierTicketUnique"
            , "OrderTicket"
            , AdditionalFields = "ID,OrderID"
            , ErrorMessage = "Ticket # is not unique, please edit the existing Tank or enter a different Ticket #"
            , HttpMethod = "POST"
        )]
        public string CarrierTicketNum { get; set; }

        [StringLength(15), DisplayName("BOL #")]
        public string BOLNum { get; set; }

        [StringLength(20), DisplayName("Tank #")]
        public string TankNum { get; set; }

        [DisplayName("Tank #")]
        [ClientDropDownMustBeSelectedAttribute(ErrorMessage = "Tank must be specified")]
        public int? OriginTankID { get; set; }

        [NotMapped, DisplayName("Tank #")]
        public string OriginTankText { 
            get 
            { 
                OriginTank ot = GetOriginTank;
                if (ot != null && ot.TankNum != "*")
                    return ot.TankNum;
                else
                    return TankNum;
            } 
        }

        [NotMapped]
        public bool IsStrapped { get { return OriginTankID.HasValue && GetOriginTank.TankNum != "*"; } }

        private OriginTank GetOriginTank
        {
            get
            {
                if (OriginTank != null)
                    return OriginTank;
                else if (OriginTankID.HasValue)
                {
                    using (DispatchCrudeDB db = new DispatchCrudeDB())
                    {
                        return db.OriginTanks.Single(ot => ot.ID == OriginTankID.Value);
                    }
                }
                return null;
            }
        }
        [ForeignKey("OriginTankID")]
        public virtual OriginTank OriginTank { get; set; }

        [NotMapped]
        public decimal MinTemp { get { return Settings.SettingsID.ProductTempMinID.AsDecimal(); } }
        [NotMapped]
        public decimal MaxTemp { get { return Settings.SettingsID.ProductTempMaxID.AsDecimal(); } }

        [NotMapped]
        public decimal MinBSW { get { return Settings.SettingsID.BswMinID.AsDecimal(); } }
        [NotMapped]
        public decimal MaxBSW { get { return Settings.SettingsID.BswMaxID.AsDecimal(); } }

        [NotMapped]
        public decimal MinGravity { get { return Settings.SettingsID.GravityMinID.AsDecimal(); } }
        [NotMapped]
        public decimal MaxGravity { get { return Settings.SettingsID.GravityMaxID.AsDecimal(); } }

        [DisplayName("Obs Gravity"), DisplayFormat(DataFormatString = "{0:0.0}", ApplyFormatInEditMode = true)]
        public decimal? ProductObsGravity { get; set; }
        [DisplayName("Obs Temp (F�)"), DisplayFormat(DataFormatString = "{0:0.0}", ApplyFormatInEditMode = true)]
        public decimal? ProductObsTemp { get; set; }
        [DisplayName("Obs BS&W %"), DisplayFormat(DataFormatString = "{0:0.000}%", ApplyFormatInEditMode = true)]
        public decimal? ProductBSW { get; set; }

        [DisplayName("Open Temp (F�)"), DisplayFormat(DataFormatString = "{0:0.0}", ApplyFormatInEditMode = true)]
        public decimal? ProductHighTemp { get; set; }
        [DisplayName("Close Temp (F�)"), DisplayFormat(DataFormatString = "{0:0.0}", ApplyFormatInEditMode = true)]
        public decimal? ProductLowTemp { get; set; }

        [DisplayName("Opening FT")]
        [Range(0, 30)]
        public byte? OpeningGaugeFeet { get; set; }
        [DisplayName("Opening IN")]
        [Range(0, 11)]
        public byte? OpeningGaugeInch { get; set; }
        [DisplayName("Opening Q")]
        [Range(0, 3)]
        public byte? OpeningGaugeQ { get; set; }
        public decimal? OpeningGauge { get { return this.OpeningGaugeFeet*12 + this.OpeningGaugeInch + this.OpeningGaugeQ/4; } }

        [DisplayName("Closing FT")]
        [Range(0, 30)]
        public byte? ClosingGaugeFeet { get; set; }
        [DisplayName("Closing IN")]
        [Range(0, 11)]
        public byte? ClosingGaugeInch { get; set; }
        [DisplayName("Closing Q")]
        [Range(0, 3)]
        public byte? ClosingGaugeQ { get; set; }
        public decimal? ClosingGauge { get { return this.ClosingGaugeFeet * 12 + this.ClosingGaugeInch + this.ClosingGaugeQ / 4; } }

        [DisplayName("Bottom Reading FT")]
        [Range(0, 30)]
        public byte? BottomFeet { get; set; }
        [DisplayName("Bottom Reading IN")]
        [Range(0, 11)]
        public byte? BottomInches { get; set; }
        [DisplayName("Bottom Reading Q")]
        [Range(0, 3)]
        public byte? BottomQ { get; set; }

        [NotMapped, DisplayName("UOM")]
        public int UomID 
        {
            get
            {
                // default to Barrel if not yet assigned (should never have a ticket entered without an Origin so this SHOULD always have a value when it is needed)
                using (SSDB ssdb = new SSDB())
                {
                    return DBHelper.ToInt32(ssdb.QuerySingleValue("SELECT originUomID FROM tblOrder WHERE ID = {0}", this.OrderID), 1);
                }
            }
        }

        [StringLength(15), DisplayName("Meter Factor")]
        public string MeterFactor { get; set; }
        [DisplayName("Open Meter")]
        public decimal? OpenMeterUnits { get; set; }
        [DisplayName("Close Meter")]
        public decimal? CloseMeterUnits { get; set; }

        [DisplayName("GOV"), DisplayFormat(DataFormatString = "{0:0.00}", ApplyFormatInEditMode = true)]
        [Remote(
            "IsGrossQtyValid"
            , "OrderTicket"
            , AdditionalFields = "UomID"
            , ErrorMessage = "Gross Vol value is not valid"
            , HttpMethod = "POST"
        )]
        public decimal? GrossUnits { get; set; }

        [DisplayName("GSV"), DisplayFormat(DataFormatString = "{0:0.00}", ApplyFormatInEditMode = true)]
        public decimal? GrossStdUnits { get; set;  }

        [DisplayName("NSV"), DisplayFormat(DataFormatString = "{0:0.00}", ApplyFormatInEditMode = true)]
        [Remote(
            "IsNetQtyValid"
            , "OrderTicket"
            , AdditionalFields = "UomID"
            , ErrorMessage = "Net Vol value is not valid"
            , HttpMethod = "POST"
        )]
        public decimal? NetUnits { get; set; }

        [DisplayName("Tare Weight"), DisplayFormat(DataFormatString = "{0:0.000}", ApplyFormatInEditMode = true)]
        public decimal? WeightTareUnits { get; set; }

        [DisplayName("Gross Weight"), DisplayFormat(DataFormatString = "{0:0.000}", ApplyFormatInEditMode = true)]
        public decimal? WeightGrossUnits { get; set; }

        [DisplayName("Net Weight"), DisplayFormat(DataFormatString = "{0:0.000}", ApplyFormatInEditMode = true)]
        public decimal? WeightNetUnits { get; set; }

        [DisplayName("Specific Weight")]
        public decimal? ProductObsSpecificWeight { get; set; }

        [DisplayName("Scale Ticket #")]
        public string ScaleTicketNum { get; set; }

        [DisplayName("Shipper PO"), StringLength(25)]
        public string DispatchConfirmNum { get; set; }

        public bool DispatchConfirmNumRequired()
        {
            return DBHelper.ToBoolean(OrderRuleHelper.GetRuleValue(this.OrderID, OrderRuleHelper.Type.RequireShipperPO));
        }

        public bool BOLRequired()
        {
            // order rule only applies to tickets with optional BOL entry
            return DBHelper.ToBoolean(OrderRuleHelper.GetRuleValue(this.OrderID, OrderRuleHelper.Type.OriginBOLRequired, false));
        }

        public bool MeterRunTankRequired()
        {
            // order rule only applies to meter run tickets, other ticket types should not call/ignore this function
            if (TicketTypeID != (int)TicketType.TYPE.MeterRun)
                return false;
            return DBHelper.ToBoolean(OrderRuleHelper.GetRuleValue(this.OrderID, OrderRuleHelper.Type.MeterRunTankRequired, false));
        }

        public bool MeterRunTempsRequired()
        {
            // order rule only applies to meter run tickets, other ticket types should not call/ignore this function
            if (TicketTypeID != (int)TicketType.TYPE.MeterRun)
                return false;
            return DBHelper.ToBoolean(OrderRuleHelper.GetRuleValue(this.OrderID, OrderRuleHelper.Type.MeterRunTempsRequired, false));
        }

        public bool RejectInfoRequired()
        {
            // order rule only applies to gauge run and gauge net tickets, other ticket types should not call/ignore this function
            if (TicketTypeID != (int)TicketType.TYPE.GaugeRun && TicketTypeID != (int)TicketType.TYPE.GaugeNet)
                return false;
            return DBHelper.ToBoolean(OrderRuleHelper.GetRuleValue(this.OrderID, OrderRuleHelper.Type.RejectInfoRequired, false));
        }

        [DisplayName("Seal Off"), StringLength(10)]
        public string SealOff { get; set; }
        [DisplayName("Seal On"), StringLength(10)]
        public string SealOn { get; set; }

        [Required]
        public bool Rejected { get; set; }
        [DisplayName("Reject Reason")]
        [ClientDropDownMustBeSelectedAttribute(ErrorMessage = "Reject Reason must be specified")]
        public int? RejectReasonID { get; set; }
        [ForeignKey("RejectReasonID")]
        public OrderTicketRejectReason RejectReason { get; set; }
        [StringLength(255), DisplayName("Reject Notes")]
        public string RejectNotes { get; set; }

        [DisplayName("Order")]
        public int OrderID { get; set; }
        [DisplayName("Ticket Type")]
        public int TicketTypeID { get; set; }

        [DisplayName("From Mobile App?"), DefaultValue(false)]
        public bool FromMobileApp { get; set; }

        // TODO: this needs to use Trailer Capacity
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (Uom.ConvertUOM(GrossUnits.HasValue ? GrossUnits.Value : (decimal)0, UomID, (int)Uom.UomTypes.Gallon) > MAX_TICKET_GALLONS)
                yield return new ValidationResult("Gross Volume is out of range");
            if (Uom.ConvertUOM(NetUnits.HasValue ? NetUnits.Value : (decimal)0, UomID, (int)Uom.UomTypes.Gallon) > MAX_TICKET_GALLONS)
                yield return new ValidationResult("Net Volume is out of range");
            Validated = true;
        }

        [NotMapped]
        [DisplayName("Gravity @ 60F"), DisplayFormat(DataFormatString = "{0:0.0}")]
        public decimal? Gravity60F
        {
            get 
            {
                if (!ProductObsGravity.HasValue || !ProductObsTemp.HasValue)
                    return null;
                 
	            decimal WATER_DENSITY_60F = 999.016m;
	            decimal K0 = 341.0957m;
	            decimal K1 = 0.0000m;
                decimal i = 0;

	            // calculate difference in observed and base temps
	            decimal delta = ProductObsTemp.Value - 60.0m;
	            // compute hydrometer correction term
	            decimal hyc = 1m - decimal.Round(0.00001278m * delta, 9) - decimal.Round(0.0000000062m * delta * delta, 9);
	            // convert API gravity to density
	            decimal rho = decimal.Round((141.5m * WATER_DENSITY_60F)/(131.5m + ProductObsGravity.Value), 2);
                // Apply hydrometer correction
                decimal rhoT = rho * hyc;
	            // Initialize density @ 60F
	            decimal rho60 = rhoT;

	            decimal rho60prev = rho60 - 1; // something to trigger the first loop
	            decimal alpha;
	            decimal vcf;

	            while (Math.Abs(rho60 - rho60prev) >= .05m && i < 20)
                {
		            // calculate coefficient of thermal expansion
		            alpha = decimal.Round(K0 / rho60 / rho60 + K1 / rho60, 7);

		            // calculate volume correction factor
		            vcf = decimal.Round((decimal) Math.Exp((double) decimal.Round(-alpha * delta - 0.8m * alpha * alpha * delta * delta, 8)), 6);
                    if (vcf == 0)
                        break;

		            rho60prev = rho60;
                    rho60 = Math.Floor(rhoT / vcf * 100) / 100;
                    i++;
	            }

	            return decimal.Round(141360.1980m / rho60 - 131.5m, 1);
            }
        }

        [ForeignKey("OrderID")]
        public virtual Order Order { get; set; }

        [ForeignKey("TicketTypeID")]
        public virtual TicketType TicketType { get; set; }


        public string GetValidTicketTypes()
        {
            return OrderRuleHelper.GetRuleValue(this.OrderID, OrderRuleHelper.Type.TicketTypeChangeAllowed).ToString();
        }

        [NotMapped]
        public decimal ProductTempDeviation
        {
            get
            {
                return Settings.SettingsID.ProductMin_Max_Temp_Deviation_Degrees.AsDecimal();
            }
        }
    }

}
