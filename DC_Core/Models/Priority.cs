using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DispatchCrude.Models
{
    [Table("tblPriority")]
    public class Priority : AuditModelLastChangeBase
    {
        public Priority()
        {
        }

        [Key]
        public byte ID { get; set; }

        [Required, StringLength(20)]
        public string Name { get; set; }

        [Required]
        public byte PriorityNum { get; set; }

        [Required]
        public string ForeColor { get; set; }

        [Required]
        public string BackColor { get; set; }
    }
}
