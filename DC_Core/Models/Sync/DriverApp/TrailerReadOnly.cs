﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DispatchCrude.Models.Sync.DriverApp
{
    public class TrailerReadOnly: IDelete
    {
        public int? ID { get; set; }
        public string IDNumber { get; set; }
        public string DOTNumber { get; set; }
        public int UomID { get; set; }
        public decimal? TotalCapacityUnits { get; set; }
        public string VIN { get; set; }
        public string MeterType { get; set; }
        public Guid QRCode { get; set; }
    }
}