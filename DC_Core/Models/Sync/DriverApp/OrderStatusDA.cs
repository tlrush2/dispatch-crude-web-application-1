﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DispatchCrude.Models.Sync.DriverApp
{
    public class OrderStatusDA: IDelete
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int StatusNum { get; set; }
        public string ActionText { get; set; }
        public int? NextID { get; set; }
        public int? EntrySortNum { get; set; }
    }
}