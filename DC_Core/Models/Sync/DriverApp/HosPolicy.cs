﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AlonsIT;
using System.Data;
using DispatchCrude.Core;

namespace DispatchCrude.Models.Sync.DriverApp
{
    public class HOSPolicy
    {
	    public int ID { get; set; }
        public string Name { get; set; }

        public int OnDutyWeeklyLimit { get; set; }
        public int DrivingWeeklyLimit { get; set; }
        public int OnDutyDailyLimit { get; set; }
        public int DrivingDailyLimit { get; set; }
        public int DrivingBreakLimit { get; set; }
        public int PersonalDailyLimit { get; set; }

        public int ShiftIntervalDays { get; set; }

        public int WeeklyReset { get; set; }
        public int DailyReset { get; set; }
        public int SleeperBreak { get; set; }
        public int DrivingBreak { get; set; }
        public int SleeperBerthSplitTotal { get; set; }
        public int SleeperBerthSplitMinimum { get; set; }

        public int WarnLogoff { get; set; }
        public int ForceLogoff { get; set; }

        public int DriverAppLogRetentionDays { get; set; }
    }

    public class HosPolicyList : List<HOSPolicy>
    {
        /* Send HOS Policy */
        /* Will only send top match */
        /* Will send during login or if policy gets updated.  If a user switches policies, he/she may need to log off and log back in to see the new one */
        public HosPolicyList() { }
        public HosPolicyList(int DriverID, DateTime? syncDateUTC)
        {
            using (SSDB db = new SSDB())
            { 
                string query = string.Format(@"
                        SELECT TOP 1 * FROM viewHosPolicy WHERE Name IN 
                        (
	                        SELECT r.value FROM tbldriver d
	                        OUTER APPLY fnCarrierRules(GETUTCDATE(), GETUTCDATE(), {1}, d.ID, d.CarrierID, d.TerminalID, d.OperatingStateID, d.RegionID, 1) r
	                        WHERE d.ID = {0}
                        )",
                        DriverID,
                        (int)CarrierRuleHelper.Type.HOS_POLICY);

                if (syncDateUTC.HasValue)
                    query += " AND ISNULL(LastChangeDateUTC, CreateDateUTC) > " + DBHelper.QuoteStr(syncDateUTC.Value.ToString("M/dd/yyyy HH:mm:ss"));

                SyncHelper.PopulateListFromDataTable(this, db.GetPopulatedDataTable(query));
            }
        }
    }
}