﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AlonsIT;

namespace DispatchCrude.Models.Sync.DriverApp
{
    public class OrderSignatureList : List<OrderSignature>
    {
        public OrderSignatureList() { }
        public OrderSignatureList(DateTime? syncDateUTC, int driverID)
        {
            using (SSDB db = new SSDB())
            {
                using (System.Data.SqlClient.SqlCommand cmd = db.BuildCommand("spOrderSignatures_DriverApp"))
                {
                    cmd.Parameters["@DriverID"].Value = driverID;
                    if (syncDateUTC.HasValue)
                        cmd.Parameters["@LastChangeDateUTC"].Value = syncDateUTC.Value;
                    SyncHelper.PopulateListFromDataTable(this, SSDB.GetPopulatedDataTable(cmd));
                }
            }
        }
    }
}