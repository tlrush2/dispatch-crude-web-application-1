﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Newtonsoft.Json;

namespace DispatchCrude.Models.Sync.DriverApp
{
    public class SyncDataInput: SyncDataBase
    {
        public OrderPresentList OrdersPresent { get; set; }

        public SyncDataInput() { }
        static public SyncDataInput GetFromJSON(string json)
        {
            // replace any incoming JSON abbreviations to match the SQL Type "OrdersPresent"
            // (they are abbreviated to save space on each JSON app-to-server upload)
            json = new Regex("(\"?OLC\"?:)").Replace(json, "LastChangeDateUTC:");
            json = new Regex("(\"?OELC\"?:)").Replace(json, "OELastChangeDateUTC:");
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            settings.DateFormatHandling = DateFormatHandling.IsoDateFormat;
            return JsonConvert.DeserializeObject<SyncDataInput>(json, settings);
        }
        public string SerializeAsJson(bool forLogging = false)
        {
            return JsonConvert.SerializeObject(this, JSonSettings(forLogging));
        }

    }
}