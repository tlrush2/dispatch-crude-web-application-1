﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Newtonsoft.Json;

namespace DispatchCrude.Models.Sync.DriverApp
{
    public class SyncDataOutput : SyncDataBase
    {
        public SyncDataOutput() : base() { }
        public SyncDataOutput(Outcome syncOutcome) { SyncOutcome = syncOutcome; }
        public SyncDataOutput(DateTime? lastSyncDateUTC, Outcome syncOutcome, MasterData masterData, OrderPresentList ordersPresent)
            : base()
        {
            if (syncOutcome.Success)
            {
                System.Data.DataTable dtOrdersPresent = OrderPresentList.AsDataTable(ordersPresent);

                Master = MasterData.Sync(masterData, ref syncOutcome);
                Orders = new OrderReadOnlyList(lastSyncDateUTC, masterData.DriverID, dtOrdersPresent);
                OrderEdits = new OrderEditList(lastSyncDateUTC, masterData.DriverID, dtOrdersPresent);
                OrderTickets = new OrderTicketList(lastSyncDateUTC, masterData.DriverID, dtOrdersPresent);
                OrderTransfers = new OrderTransferList(lastSyncDateUTC, masterData.DriverID); 
                OrderSignatures = new OrderSignatureList(lastSyncDateUTC, masterData.DriverID);
                OrderStatuses = new OrderStatusList(lastSyncDateUTC);
                PrintTemplates = new PrintTemplateList(lastSyncDateUTC);
                Settings = new SettingList(lastSyncDateUTC);
                OriginTanks = new OriginTankList(lastSyncDateUTC, masterData.DriverID);
                OriginTankStrappings = new OriginTankStrappingList(lastSyncDateUTC, masterData.DriverID);
                TicketTypes = new TicketTypeList(lastSyncDateUTC);
                Trailers = new TrailerList(masterData.DriverID, lastSyncDateUTC);
                Trucks = new TruckList(masterData.DriverID, lastSyncDateUTC);
                Uoms = new UomList(lastSyncDateUTC);
                PrintStatuses = new PrintStatusList(lastSyncDateUTC);
                OriginWaitReasons = new OriginWaitReasonList(lastSyncDateUTC);
                DestinationWaitReasons = new DestinationWaitReasonList(lastSyncDateUTC);
                OrderRejectReasons = new OrderRejectReasonList(lastSyncDateUTC);
                OrderTicketRejectReasons = new OrderTicketRejectReasonList(lastSyncDateUTC);
                OrderRules = new OrderRuleList(masterData.DriverID, lastSyncDateUTC);
                TicketTemplates = new TicketTemplateList(lastSyncDateUTC);
                QuestionnaireQuestionTypes = new QuestionnaireQuestionTypeList(lastSyncDateUTC);
                QuestionnaireTemplates = new QuestionnaireTemplateList(lastSyncDateUTC, masterData.DriverID);
                QuestionnaireQuestions = new QuestionnaireQuestionList(lastSyncDateUTC, masterData.DriverID);
                CarrierRules = new CarrierRuleList(masterData.DriverID, lastSyncDateUTC);
                HosPolicy = new HosPolicyList(masterData.DriverID, lastSyncDateUTC);
                Hos = new HosList(masterData.DriverID, lastSyncDateUTC);
                HosSignatures = new HosSignatureList(masterData.DriverID, lastSyncDateUTC);
                HosViolations = new HosViolationList(masterData.DriverID);
                Driver = new DriverData(masterData.DriverID);
                DispatchMessages = new DispatchMessageList(masterData.DriverID);
            }
            SyncOutcome = syncOutcome;
        }

        public string SerializeAsJson(bool forLogging = false)
        {
            string ret = JsonConvert.SerializeObject(this, JSonSettings(forLogging));
            // since 1.0.16 and prior can't handle receiving a DriverLocations entry, remove it (if it isn't an array, which is handled above)
            new Regex(",? \"DriverLocations\": null", RegexOptions.IgnorePatternWhitespace).Replace(ret, "");
            new Regex(",? \"TabletID\": null", RegexOptions.IgnorePatternWhitespace).Replace(ret, "");
            new Regex(",? \"PrinterSerial\": null", RegexOptions.IgnorePatternWhitespace).Replace(ret, "");
            return ret;
        }
        public Outcome SyncOutcome { get; set; }
        public OrderStatusList OrderStatuses { get; set; }
        public PrintTemplateList PrintTemplates { get; set; }
        public SettingList Settings { get; set; }
        public OriginTankList OriginTanks { get; set; }
        public OriginTankStrappingList OriginTankStrappings { get; set; }
        public TicketTypeList TicketTypes { get; set; }
        public TrailerList Trailers { get; set; }
        public TruckList Trucks { get; set; }
        public OrderReadOnlyList Orders { get; set; }
        public UomList Uoms { get; set; }
        public PrintStatusList PrintStatuses { get; set; }
        public OriginWaitReasonList OriginWaitReasons { get; set; }
        public DestinationWaitReasonList DestinationWaitReasons { get; set; }
        public OrderRejectReasonList OrderRejectReasons { get; set; }
        public OrderTicketRejectReasonList OrderTicketRejectReasons { get; set; }
        public OrderRuleList OrderRules { get; set; }
        public TicketTemplateList TicketTemplates { get; set; }
        public QuestionnaireQuestionTypeList QuestionnaireQuestionTypes { get; set; }
        public QuestionnaireTemplateList QuestionnaireTemplates { get; set; }
        public QuestionnaireQuestionList QuestionnaireQuestions { get; set; }
        public CarrierRuleList CarrierRules { get; set; }
        public DriverData Driver { get; set; }

        static public int AppVersionAsInt(string appVersion)
        {
            int ret = 0;
            int multiplier = 10000000;
            if (appVersion != null)
            {
                foreach (String element in appVersion.Split(new char[] { '.' }))
                {
                    int iElement = 0;
                    if (int.TryParse(element, out iElement))
                    {
                        ret += iElement * multiplier;
                        multiplier /= 100;
                    }
                }
            }
            return ret;
        }
    }
}