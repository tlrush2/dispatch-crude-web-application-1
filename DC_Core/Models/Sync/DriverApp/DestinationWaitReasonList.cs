﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AlonsIT;

namespace DispatchCrude.Models.Sync.DriverApp
{
    public class DestinationWaitReasonList : List<DestinationWaitReasonDA>
    {
        public DestinationWaitReasonList() { }
        public DestinationWaitReasonList(DateTime? syncDateUTC)
        {
            using (SSDB db = new SSDB())
            {
                SyncHelper.PopulateListFromDataTable(this
                    , db.GetPopulatedDataTable(
                        "SELECT X.*, cast(CASE WHEN DeleteDateUTC IS NULL THEN 0 ELSE 1 END as bit) AS Deleted FROM tblDestinationWaitReason X WHERE {0}"
                        , SyncHelper.SyncDateWhereClause(syncDateUTC, "X", true)));
            }
        }
    }
}