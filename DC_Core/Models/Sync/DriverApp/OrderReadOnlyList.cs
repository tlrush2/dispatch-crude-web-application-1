﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AlonsIT;
using System.Data;

namespace DispatchCrude.Models.Sync.DriverApp
{
    public class OrderReadOnlyList: List<OrderReadOnly>
    {
        public OrderReadOnlyList(DateTime? lastSyncDateUTC, int driverID, System.Data.DataTable dtOrdersPresent)
        {
            using (SSDB db = new SSDB())
            {
                using (System.Data.SqlClient.SqlCommand cmd = db.BuildCommand("spOrderReadOnly_DriverApp"))
                {
                    cmd.Parameters["@DriverID"].Value = driverID;
                    if (lastSyncDateUTC.HasValue)
                        cmd.Parameters["@LastSyncDateUTC"].Value = lastSyncDateUTC.Value;
                    cmd.Parameters["@OrdersPresent"].TypeName = "dbo.OrdersPresent";
                    cmd.Parameters["@OrdersPresent"].Value = dtOrdersPresent;
                    DataTable data = SSDB.GetPopulatedDataTable(cmd);
                    SyncHelper.PopulateListFromDataTable(this, data);
                }
            }
        }
    }
}