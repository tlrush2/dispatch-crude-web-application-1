﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DispatchCrude.Core;
using Newtonsoft.Json;

namespace DispatchCrude.Models.Sync.DriverApp
{
    public class SyncDataBase
    {
        public SyncDataBase() { }

        public MasterData Master { get; set; }
        public OrderEditList OrderEdits { get; set; }
        public OrderTicketList OrderTickets { get; set; }
        public OrderTransferList OrderTransfers { get; set; }
        public OrderSignatureList OrderSignatures { get; set; }
        public DriverLocationList DriverLocations { get; set; }
        public HosPolicyList HosPolicy { get; set; }
        public HosList Hos { get; set; }
        public HosViolationList HosViolations { get; set; }
        public HosSignatureList HosSignatures { get; set; }
        public ExceptionLogList Exceptions { get; set; }
        public OrderPhotoList OrderPhotos { get; set; }
        public QuestionnaireSubmissionList QuestionnaireSubmissions { get; set; }
        public QuestionnaireResponseList QuestionnaireResponses { get; set; }
        public QuestionnaireResponseBlobList QuestionnaireResponseBlobs { get; set; }
        public DateTime? CurrentAppDateTime { get; set; }
        public string CurrentAppTzDstOffsets { get; set; }
        public DispatchMessageList DispatchMessages { get; set; }

        static public JsonSerializerSettings JSonSettings(bool forLogging = false)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings();
            if (forLogging)
                settings.ContractResolver = new JsonPropListExcludeContractResolver(new string[] { "MobileAppVersion"
                                                                                                    , "TabletID"
                                                                                                    , "PrinterSerial"
                                                                                                    , "AndroidApiLevel"
                                                                                                    , "ModelNum"
                                                                                                    , "SerialNum"
                                                                                                    , "Manufacturer"
                                                                                                    , "PrintHeaderBlob"
                                                                                                    , "TemplateText"
                                                                                                    , "SignatureBlob"
                                                                                                    , "PhotoBlob"
                                                                                                    , "PickupTemplateText"
                                                                                                    , "TicketTemplateText"
                                                                                                    , "DeliverTemplateText"
                                                                                                    , "FooterTemplateText"
                                                                                                    , "AnswerBlob" });
            else
                settings.ContractResolver = new JsonPropListExcludeContractResolver(new string[] { "MobileAppVersion"
                                                                                                    , "TabletID"
                                                                                                    , "PrinterSerial"
                                                                                                    , "AndroidApiLevel"
                                                                                                    , "ModelNum"
                                                                                                    , "SerialNum"
                                                                                                    , "Manufacturer" });
            settings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            settings.Formatting = Formatting.Indented;
            settings.DateFormatHandling = DateFormatHandling.IsoDateFormat;
            return settings;
        }

    }

}