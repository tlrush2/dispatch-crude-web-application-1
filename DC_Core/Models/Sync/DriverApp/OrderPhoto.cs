﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DispatchCrude.Models.Sync.DriverApp
{
    public class OrderPhotos
    {
        public int ID { get; set; }
        public Guid UID { get; set; }
        public int? OrderID { get; set; }
        public int? OriginID { get; set; }
        public int? DestinationID { get; set; }
        public int? PhotoTypeID { get; set; }
        public int? DriverID { get; set; }
        public byte[] PhotoBlob { get; set; }
        public string GpsData { get; set; }
        public string CreateDateUTC { get; set; }
        public string CreatedByUser { get; set; }
    }
}