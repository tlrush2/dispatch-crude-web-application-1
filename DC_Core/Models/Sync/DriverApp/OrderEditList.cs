﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AlonsIT;
using System.Data;

namespace DispatchCrude.Models.Sync.DriverApp
{
    public class OrderEditList : List<DispatchCrude.Models.Sync.DriverApp.OrderEdit>
    {
        public OrderEditList() { }
        public OrderEditList(DateTime? lastSyncDateUTC, int driverID, System.Data.DataTable dtOrdersPresent)
        {
            using (SSDB db = new SSDB())
            {
                using (System.Data.SqlClient.SqlCommand cmd = db.BuildCommand("spOrderEdit_DriverApp"))
                {
                    cmd.Parameters["@DriverID"].Value = driverID;
                    cmd.Parameters["@OrdersPresent"].TypeName = "dbo.OrdersPresent";
                    cmd.Parameters["@OrdersPresent"].Value = dtOrdersPresent;
                    DataTable data = SSDB.GetPopulatedDataTable(cmd);
                    SyncHelper.PopulateListFromDataTable(this, data);
                }
            }
        }
    }
}