﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DispatchCrude.Models.Sync
{
    public class IDelete
    {
        public bool Deleted { get; set; }
    }
}