﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DispatchCrude.Models.Sync
{
    public class OriginTankReadOnly: AuditModelDeleteUTCBase
    {
        public int ID { get; set; }
        public int OriginID { get; set; }
        public string TankNum { get; set; }
        public string TankDescription { get; set; }
    }
}