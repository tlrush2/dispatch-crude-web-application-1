﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DispatchCrude.Models.Sync
{
    public class OrderPresentList: List<OrderPresent>
    {
        static public System.Data.DataTable AsDataTable(OrderPresentList ordersPresent)
        {
            System.Data.DataTable ret = new System.Data.DataTable("dbo.OrdersPresent");
            ret.Columns.Add("ID", typeof(System.Int32));
            ret.Columns.Add("LastChangeDateUTC", typeof(System.DateTime));
            ret.Columns.Add("OELastChangeDateUTC", typeof(System.DateTime));
            if (ordersPresent != null)
            {
                foreach (OrderPresent op in ordersPresent)
                {
                    ret.Rows.Add(new object[] { op.ID, op.LastChangeDateUTC, op.OELastChangeDateUTC });
                }
            }
            return ret;
        }
    }
}