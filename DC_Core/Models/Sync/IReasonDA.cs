using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using System.Linq;

namespace DispatchCrude.Models.Sync
{
    public class IReasonDA : IDelete
    {
        public int ID { get; set; }
        public string Num { get; set; }
        public string Description{ get; set; }
        public bool RequireNotes { get; set; }
    }
}
