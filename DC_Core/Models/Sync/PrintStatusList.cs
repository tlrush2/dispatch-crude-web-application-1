﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AlonsIT;

namespace DispatchCrude.Models.Sync
{
    public class PrintStatusList : List<PrintStatus>
    {
        public PrintStatusList() { }
        public PrintStatusList(DateTime? syncDateUTC)
        {
            using (SSDB db = new SSDB())
            {
                string sql = string.Format("SELECT * FROM dbo.tblPrintStatus {0}", syncDateUTC.HasValue ? "WHERE 1=0" : "");
                SyncHelper.PopulateListFromDataTable(this
                    , db.GetPopulatedDataTable(sql, null));
            }
        }
    }
}