﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DispatchCrude.Models.Sync.GaugerApp
{
    public class GaugerTicketGA: GaugerTicketBase
    {
        public bool UpdateValues(GaugerTicket current)
        {
            bool ret = SyncHelper.UpdateValues(this, current, new string[] { "ID" } );
            current.FromMobileApp = true;
            return ret;
        }
    }
}