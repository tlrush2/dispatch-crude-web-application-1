﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AlonsIT;
using System.Data;

namespace DispatchCrude.Models.Sync.GaugerApp
{
    public class GaugerOrderList : List<DispatchCrude.Models.Sync.GaugerApp.GaugerOrderGA>
    {
        public GaugerOrderList() { }
        public GaugerOrderList(DateTime? syncDateUTC, int gaugerID)
        {
            using (SSDB db = new SSDB())
            {
                SyncHelper.PopulateListFromDataTable(this
                    , db.GetPopulatedDataTable(
                        "SELECT * FROM fnGaugerOrder_GaugerApp({0}, {1})"
                            , gaugerID
                            , syncDateUTC.HasValue ? DBHelper.QuoteStr(syncDateUTC.Value.ToString("M/d/yyyy HH:mm:ss")) : "NULL"));
            }
        }
    }
}