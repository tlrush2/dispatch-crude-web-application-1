﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using AlonsIT;
using AlonsIT.ExtensionMethods;

namespace DispatchCrude.Models.Sync.GaugerApp
{
    public class GaugerOrderGA: GaugerOrderBase
    {
        public bool UpdateValues(GaugerOrder current)
        {
            bool ret = SyncHelper.UpdateValues(this, current, new string[] { "ID" });
            return ret;
        }
    }
}