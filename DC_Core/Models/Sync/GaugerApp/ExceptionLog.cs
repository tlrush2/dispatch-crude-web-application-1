﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DispatchCrude.Models.Sync.GaugerApp
{
    public class ExceptionLog: IExceptionLog
    {
        public int GaugerID { get; set; }
        public bool Deleted { get; set; }
    }
}