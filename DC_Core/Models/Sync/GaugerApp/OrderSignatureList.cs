﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AlonsIT;

namespace DispatchCrude.Models.Sync.GaugerApp
{
    public class OrderSignatureList : List<OrderSignature>
    {
        public OrderSignatureList() { }
        public OrderSignatureList(DateTime? syncDateUTC, int gaugerID)
        {
            using (SSDB db = new SSDB())
            {
                using (System.Data.SqlClient.SqlCommand cmd = db.BuildCommand("spOrderSignatures_GaugerApp"))
                {
                    cmd.Parameters["@GaugerID"].Value = gaugerID;
                    if (syncDateUTC.HasValue)
                        cmd.Parameters["@LastChangeDateUTC"].Value = syncDateUTC.Value;
                    SyncHelper.PopulateListFromDataTable(this, SSDB.GetPopulatedDataTable(cmd));
                }
            }
        }
    }
}