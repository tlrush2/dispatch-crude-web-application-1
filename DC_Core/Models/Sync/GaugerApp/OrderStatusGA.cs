﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DispatchCrude.Models.Sync.GaugerApp
{
    public class OrderStatusGA: IDelete
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public bool IsComplete { get; set; }
    }
}