﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DispatchCrude.Models.Sync.GaugerApp
{
    public class GaugerOrderStatusGA
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}