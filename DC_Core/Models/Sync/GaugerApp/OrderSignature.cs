﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DispatchCrude.Models.Sync.GaugerApp
{
    public class OrderSignature
    {
        public int ID { get; set; }
        public Guid UID { get; set; }
        public int? OrderID { get; set; }
        public int? OriginID { get; set; }
        public int? DestinationID { get; set; }
        public int? GaugerID { get; set; }
        public byte[] SignatureBlob { get; set; }
    }
}