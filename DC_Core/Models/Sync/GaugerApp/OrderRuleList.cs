﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AlonsIT;

namespace DispatchCrude.Models.Sync.GaugerApp
{
    public class OrderRuleList : List<OrderRule>
    {
        public OrderRuleList() { }
        public OrderRuleList(int gaugerID, DateTime? syncDateUTC)
        {
            using (SSDB db = new SSDB())
            {
                SyncHelper.PopulateListFromDataTable(this
                    , db.GetPopulatedDataTable(
                        "SELECT * FROM dbo.fnOrderRules_GaugerApp({0})"
                        , gaugerID));
            }
        }
    }
}