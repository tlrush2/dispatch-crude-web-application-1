﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DispatchCrude.Models.Sync
{
    public class PrintTemplateReadOnly
    {
        public int? ID { get; set; }
        public int? TicketTypeID { get; set; }
        public int? HeaderImageLeft { get; set; }
        public int? HeaderImageTop { get; set; }
        public int? HeaderImageWidth { get; set; }
        public int? HeaderImageHeight { get; set; }
        public string TemplateText { get; set; }
    }
}