﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DispatchCrude.Models.Sync
{
    public class UomReadOnly: IDelete
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Abbrev { get; set; }
        public decimal GallonEquivalent { get; set; }
        public int UomTypeID { get; set; }
    }
}