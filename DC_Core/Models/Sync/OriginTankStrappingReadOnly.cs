﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DispatchCrude.Models.Sync
{
    public class OriginTankStrappingReadOnly
    {
        public int ID { get; set; }
        public int OriginTankID { get; set; }
        public byte Feet { get; set; }
        public byte Inches { get; set; }
        public byte Q { get; set; }
        public decimal BPQ { get; set; }
        public bool IsMinimum { get; set; }
        public bool IsMaximum { get; set; }
        public bool IsDeleted { get; set; }
    }
}