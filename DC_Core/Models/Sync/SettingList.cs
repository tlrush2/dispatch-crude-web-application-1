﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AlonsIT;
using System.Data;

namespace DispatchCrude.Models.Sync
{
    public class SettingList : List<SettingReadOnly>
    {
        public SettingList() { }
        public SettingList(DateTime? syncDateUTC)
        {
            using (SSDB db = new SSDB())
            {
                SyncHelper.PopulateListFromDataTable(this
                    , db.GetPopulatedDataTable("SELECT * FROM tblSetting WHERE {0}", SyncHelper.SyncDateWhereClause(syncDateUTC)));
            }
        }
    }
}