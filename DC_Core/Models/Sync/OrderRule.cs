﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DispatchCrude.Models.Sync
{
    public class OrderRule
    {
        public int OrderID { get; set; }
        public int TypeID { get; set; }
        public string Value { get; set; }
    }
}