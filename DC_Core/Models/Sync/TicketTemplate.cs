﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DispatchCrude.Models.Sync
{
    public class TicketTemplate
    {
        public int ID { get; set; }
        public int TicketTypeID { get; set; }
        public string TemplateText { get; set; }
    }
}