using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DispatchCrude.Models
{
    [Table("tblDestTicketType")]
    public class DestTicketType
    {
        public enum TYPE { Basic = 1, BOLAvailable = 2, VisualMeter = 3, Propane = 4, CAN_Basic = 5, MineralRun = 6, VisualMeterBOL = 7,
            BOLGrossOnly = 8, Propane_With_Scale = 9 }

        [Key]
        public int ID { get; set; }

        [Required, StringLength(25)]
        public string Name { get; set; }

    }
}
