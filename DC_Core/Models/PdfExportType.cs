﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using System.Linq;
using System;

namespace DispatchCrude.Models
{
    [Table("tblPdfExportType")]
    public class PdfExportType : AuditModelDeleteBase, IValidatableObject
    {
        [Key]
        public int ID { get; set; }

        [Required, StringLength(30)]
        public string Name { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!ValidateDuplicateName(ID, Name))
                yield return new ValidationResult("Type name is already in use");
            Validated = true;
        }

        static protected bool ValidateDuplicateName(int id, string name)
        {
            using (DispatchCrudeDB db = new DispatchCrudeDB())
            {
                return db.Set<PdfExportType>().Where(m => m.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase) && (id == 0 || m.ID != id)).Count() == 0;
            }
        }

        public override bool allowRowDeactivate
        {
            get
            {
                //Override normal deactivate behavior: If the record is marked as a system record.  Do not allow it to be deactivated.
                return ID >= 1000;
            }
        }
    }
}
