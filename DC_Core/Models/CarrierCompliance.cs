using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using System.Web.Script.Serialization;
using System.Linq;
using DispatchCrude.Extensions;
using System.Collections.Generic;

namespace DispatchCrude.Models
{
    [Table("tblCarrierCompliance")]
    public class CarrierCompliance : AuditModelDeleteBase, IValidatableObject
    {
        [Key]
        public int ID { get; set; }

        [RequiredGreaterThanZero]
        [DisplayName("Carrier")]
        [UIHint("_ForeignKeyDDL")]
        public int CarrierID { get; set; }

        [DisplayName("Carrier")]
        [ForeignKey("CarrierID")]
        public Carrier Carrier { get; set; }

        [RequiredGreaterThanZero]
        [DisplayName("Doc Type")]
        [UIHint("_ForeignKeyDDL")]
        public int CarrierComplianceTypeID { get; set; }

        [DisplayName("Type")]
        [ForeignKey("CarrierComplianceTypeID")]
        public virtual CarrierComplianceType CarrierComplianceType { get; set; }

        [DisplayName("Document")]
        [ScriptIgnore]
        public byte[] Document { get; set; }
        [DisplayName("Document Name")]
        public string DocumentName { get; set; }

        [DisplayName("Effective Date")]
        [UIHint("Date")]
        public DateTime? DocumentDate { get; set; }

        [DisplayName("Expiration Date")]
        [UIHint("Date")]
        public DateTime? ExpirationDate { get; set; }

        public string Notes { get; set; }


        /// <summary>
        /// Returns boolean based upon the expiration date for a record
        /// </summary>
        [NotMapped]
        public bool Expired
        {
            get { return ExpirationDate < DateTime.Today; }
        }

        /// <summary>
        /// Returns boolean based upon the existence of a document
        /// </summary>
        [NotMapped]
        public bool MissingDocumentation
        {
            get { return Document == null || Document.Length == 0; }
        }

        /// <summary>
        /// Returns boolean based upon the compliance record type criteria: Expires?, Expired?, and Requires Document?
        /// </summary>
        [NotMapped]
        public bool IsCompliant
        {
            get
            {
                //NOTE: Cannot check here for whether or not the record exists so that 
                //check must be done at the location IsCompliant is being checked

                DispatchCrudeDB db = new DispatchCrudeDB();
                CarrierComplianceType type = db.CarrierComplianceTypes.Find(CarrierComplianceTypeID);

                if (type != null)
                {
                    //If the document can Expire AND has expired - error
                    if (type.Expires && Expired == true)
                        return false;

                    //If a document is required AND does not exist - error
                    if (type.RequiresDocument && Document == null)
                        return false;
                }

                return true;
            }
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var ret = new List<ValidationResult>();

            using (DispatchCrudeDB db = new DispatchCrudeDB())
            {
                // If the effective date is not selected...error
                if (this.DocumentDate == null)
                    ret.Add(new ValidationResult("The Effective Date field is required.", new string[] { "DocumentDate" }));

                // Get compliance type record
                var CarrierComplianceType = db.CarrierComplianceTypes.Find(this.CarrierComplianceTypeID);

                // ensure a valid CarrierComplianceTypeID value was specified
                if (CarrierComplianceType == null)
                {
                    ret.Add(new ValidationResult("Compliance Type value is required", new string[] { "CarrierComplianceTypeID" }));
                }
                // check that the carrier compliance type is valid for this type of carrier
                else if (CarrierComplianceType.CarrierTypeID != null)
                {
                    Carrier c = db.Carriers.Find(CarrierID);
                    if (CarrierComplianceType.CarrierTypeID != c.CarrierTypeID)
                        ret.Add(new ValidationResult("This doc type is not valid for this carrier!", new string[] { "CarrierComplianceTypeID" }));
                }
                else
                {
                    // If the record is new and requires a document and the document is missing...error
                    if (this.ID == 0 && CarrierComplianceType.RequiresDocument && this.Document == null)
                        ret.Add(new ValidationResult(string.Format("The Document is required for '{0}' record type.", CarrierComplianceType.Name), new string[] { "Document" }));

                    // If the record type can expire and no expiration date has been given...error
                    if (CarrierComplianceType.Expires && this.ExpirationDate == null)
                        ret.Add(new ValidationResult(string.Format("The Expiration Date field is required for '{0}' record type.", CarrierComplianceType.Name), new string[] { "ExpirationDate" }));
                }
            }
            Validated = true;
            return ret;
        }

    }
}
