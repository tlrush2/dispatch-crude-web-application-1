using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DispatchCrude.Models
{
    [Table("tblTruck")]
    public class Truck : AuditModelDeleteBase
    {
        public Truck()
        {
            this.Drivers = new List<Driver>();
        }

        public int ID { get; set; }

        [Required, StringLength(10), DisplayName("Truck #")]
        public string IDNumber { get; set; }
        [StringLength(20), DisplayName("DOT #")]
        public string DotNumber { get; set; }
        [StringLength(20)]
        public string VIN { get; set; }
        [StringLength(20)]
        public string Make { get; set; }
        [StringLength(20)]
        public string Model { get; set; }

        public int? Year { get; set; }

        [StringLength(25)]
        public string MeterType { get; set; }

        [DisplayName("Truck Type")]
        public int TruckTypeID { get; set; }

        [DisplayName("Carrier")]
        public int CarrierID { get; set; }

        [DisplayName("GPS Unit"), StringLength(25)]
        public string GPSUnit { get; set; }

        public /*virtual*/ ICollection<Driver> Drivers { get; set; }
         
        [ForeignKey("CarrierID")]
        public Carrier Carrier { get; set; }

        [DisplayName("License #"), StringLength(25)]
        public string LicenseNumber { get; set; }

        [DisplayName("Owner Info"), StringLength(255)]
        public string OwnerInfo { get; set; }
        
        [DisplayName("Purchase $$")]
        public decimal? PurchasePrice { get; set; }
        [DisplayName("Garage City")]
        public string GarageCity { get; set; }
        [DisplayName("Garage State")]
        public int? GarageStateID { get; set; }

        [DisplayName("QR Code")]
        public Guid QRCode { get; set; }

        [DisplayName("Last Odometer")]
        public int? LastOdometer { get; set; }

        [DisplayName("Last Odometer Update (UTC)")]
        public DateTime? LastOdometerDateUTC { get; set; }

        [DisplayName("Terminal")]
        public int? TerminalID { get; set; }
        [ForeignKey("TerminalID")]
        [DisplayName("Terminal")]
        public Terminal Terminal { get; set; }

        [NotMapped]
        public string IDNumber_FlagInactive
        {
            get { return getIDNumber(flagDeleted: true); }
        }

        //Set the display order to put "deactivated" in front for usability's sake.  
        //When in dropdown boxes "name + [DEACTIVATED]" becomes very difficult to read/hard on 
        //the eyes because it creates an uneven edge.
        public string getIDNumber(bool flagDeleted = false)
        {
            return (flagDeleted && Deleted ? "[DEACTIVATED] " + IDNumber : IDNumber);
        }

    }

}
