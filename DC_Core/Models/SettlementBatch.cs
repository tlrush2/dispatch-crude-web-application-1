﻿using System;
using AlonsIT;
using System.Data.SqlClient;
using DispatchCrude.Core;
using System.Data;


namespace DispatchCrude.Models
{
    public abstract class SettlementBatch : AuditModelLastChangeBase
    {
        public static string UNSETTLE_WARNING_TEXT = "This action will delete the selected batch and reset all its orders. Are you sure?";
        public static string REVERT_PENDING_WARNING_TEXT = "This action will revert the selected batch back to Pending status. Are you sure?";


        public abstract string TABLE { get; }


        public bool IncludePendingStatus
        {
            get { return Settings.SettingsID.Settlement_IncludePendingStatus.AsBool(); }
        }


        // Finalize a batch
        public void FinalizeBatch(string BatchID, string InvoiceNum, string Notes)
        {
            if (!String.IsNullOrEmpty(BatchID))
            {
                using (SSDB db = new SSDB())
                {
                    using (SqlCommand cmd = db.BuildCommand("UPDATE dbo.tbl" + TABLE + "SettlementBatch " +
                        "SET IsFinal = 1, InvoiceNum = " + DBHelper.QuoteStr(InvoiceNum) +", Notes = " + DBHelper.QuoteStr(Notes) + ", " +
                            "LastChangeDateUTC = GETUTCDATE(), LastChangedByUser = '" + UserSupport.UserName + "' " +
                        "WHERE ID=@BatchID"))
                    {
                        // Get batch number
                        cmd.Parameters.AddWithValue("@BatchID", BatchID);

                        // Delete settlement records for this batch number
                        cmd.ExecuteNonQuery();
                    }
                }
            }
        }

        // Unsettle a batch--return to pending or delete based on system setting
        public void UnsettleBatch(string BatchID)
        {
            if (IncludePendingStatus)
            {
                ReturnBatchToPending(BatchID);
            }
            else
            { 
                DeleteBatch(BatchID); // no pending flag to consider, just delete batch
            }
        }

        // Return batch to pending status (clear the final flag)
        public void ReturnBatchToPending(string BatchID)
        {
            if (!String.IsNullOrEmpty(BatchID))
            {
                using (SSDB db = new SSDB())
                {
                    using (SqlCommand cmd = db.BuildCommand("UPDATE dbo.tbl" + TABLE + "SettlementBatch " +
                        "SET IsFinal = 0, LastChangeDateUTC = GETUTCDATE(), LastChangedByUser = '" + UserSupport.UserName + "' " +
                        "WHERE ID=@BatchID"))
                    {
                        // Get batch number
                        cmd.Parameters.AddWithValue("@BatchID", BatchID);

                        // Delete settlement records for this batch number
                        cmd.ExecuteNonQuery();
                    }
                }
            }
        }

        // Delete the batch record
        public void DeleteBatch(string BatchID)
        {
            if (!String.IsNullOrEmpty(BatchID))
            {
                using (SSDB db = new SSDB())
                {
                    using (SqlCommand cmd = db.BuildCommand("UPDATE dbo.tblOrderSettlement" + TABLE + " SET BatchID = NULL WHERE BatchID=@BatchID"))
                    {
                        // Get batch number
                        cmd.Parameters.AddWithValue("@BatchID", BatchID);

                        // Delete settlement records for this batch number
                        cmd.ExecuteNonQuery();
                    }

                    using (SqlCommand cmd = db.BuildCommand("DELETE FROM dbo.tbl" + TABLE + "SettlementBatch WHERE ID=@BatchID"))
                    {
                        // Get batch number
                        cmd.Parameters.AddWithValue("@BatchID", BatchID);

                        // Delete batch number
                        cmd.ExecuteNonQuery();
                    }
                }
            }
        }

    }


    public class CarrierSettlementBatch : SettlementBatch
    {
        public override string TABLE { get { return "Carrier"; } }
    }


    // DriverSettlementBatch - see separate class already MVC'ed

    public class ShipperSettlementBatch : SettlementBatch
    {
        public override string TABLE { get { return "Shipper"; } }
    }
}
