﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Data.Entity;
using System.Web.Mvc;
using AlonsIT;

namespace DispatchCrude.Models
{
    [Table("tblObjectCustomData")]
    public class ObjectCustomData : AuditModelLastChangeBase, IValidatableObject
    {
        public ObjectCustomData()
        {
        }

        [Key]
        public int ID { get; set; }

        [DisplayName("Object Field")]
        [UIHint("_ForeignKeyDDL_S2")]
        public int ObjectFieldID { get; set; }
        [ForeignKey("ObjectFieldID")]
        public virtual ObjectField ObjectField { get; set; }

        [DisplayName("Record")]
        [UIHint("_ForeignKeyDDL_S2")]
        public int RecordID { get; set; }

        public string Value { get; set; }

        public string Record
        {
            get
            {
                if (ID == 0 && RecordID == 0)
                    return "";
                else
                {
                    try
                    {
                        using (SSDB db = new SSDB())
                        {
                            return db.QuerySingleValue("SELECT {0} FROM {1} WHERE ID = {2}",
                                            ObjectField.ObjectDef.KeyField, // field
                                            ObjectField.ObjectDef.SqlTargetName ?? ObjectField.ObjectDef.SqlSourceName, // table
                                            this.RecordID
                                        ).ToString(); //ID
                        }
                    }
                    catch (Exception)
                    {
                        return "{" + RecordID + "}";
                    }
                }
            }
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var ret = new List<ValidationResult>();

            using (DispatchCrudeDB db = new DispatchCrudeDB())
            {
                if (RecordID == 0) // check that this ojbect/table has the existing record
                {
                    ret.Add(new ValidationResult("Custom data must be attached to a valid record!", new string[] { "RecordID" }));
                }
                if (db.ObjectCustomData.Where(x => x.ObjectFieldID == ObjectFieldID && x.RecordID == RecordID && x.ID != ID).Count() > 0)
                {
                    ret.Add(new ValidationResult("This custom field is already set for this record!", new string[] { "RecordID" }));
                }

            }
            Validated = true;
            return ret;
        }


    }
}