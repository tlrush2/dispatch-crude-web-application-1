﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlonsIT;

namespace DispatchCrude.Models
{
    public abstract class OrderFinancialBase: OrderBase
    {
        private const string FLD_ErrorFieldCSV = "ErrorFieldCSV", FLD_HasError = "HasError"
            , FLD_ManualFieldCSV = "ManualFieldCSV", FLD_HasManual = "HasManual";

        public string Shipper { get; set; }
		public string TicketNums { get; set; }
		public string TankNums { get; set; }
		public string PreviousDestinations { get; set; }
		public string RerouteUsers { get; set; }
		public string RerouteDates { get; set; }
		public string RerouteNotes { get; set; }
		public DateTime? InvoiceRatesAppliedDate { get; set; }
		public int? BatchID { get; set; }
		public string InvoiceBatchNum { get; set; }
		public int? InvoiceOriginWaitBillableMinutes { get; set; }
		public int? InvoiceDestinationWaitBillableMinutes { get; set; }
		public int? InvoiceTotalWaitBillableMinutes { get; set; }
		public int? InvoiceWaitFeeParameterID { get; set; }
		public string InvoiceWaitFeeSubUnit { get; set; }
		public string InvoiceWaitFeeRoundingType { get; set; }
		public decimal? InvoiceOriginWaitRate { get; set; }
		public decimal? InvoiceOriginWaitAmount { get; set; }
		public decimal? InvoiceDestinationWaitRate { get; set; }
		public decimal? InvoiceDestinationWaitAmount { get; set; }
		public decimal? InvoiceTotalWaitAmount { get; set; }
		public decimal? InvoiceOrderRejectRate { get; set; }
		public decimal? InvoiceOrderRejectRateType { get; set; }
		public decimal? InvoiceOrderRejectAmount { get; set; }
		public decimal? InvoiceOriginChainupRate { get; set; }
		public string InvoiceOriginChainupRateType { get; set; }
		public decimal? InvoiceOriginChainupAmount { get; set; }
		public decimal? InvoiceDestChainupRate { get; set; }
		public string InvoiceDestChainupRateType { get; set; }
		public decimal? InvoiceDestChainupAmount { get; set; }
		public decimal? InvoiceRerouteRate { get; set; }
		public string InvoiceRerouteRateType { get; set; }
		public decimal? InvoiceRerouteAmount { get; set; }
		public decimal? InvoiceH2SRate { get; set; }
		public string InvoiceH2SRateType { get; set; }
		public decimal? InvoiceH2SAmount { get; set; }
		public decimal? InvoiceSplitLoadRate { get; set; }
		public string InvoiceSplitLoadRateType { get; set; }
		public decimal? InvoiceSplitLoadAmount { get; set; }
		public decimal? InvoiceOtherAmount { get; set; }
		public decimal? InvoiceTaxRate { get; set; }
		public string InvoiceSettlementUom { get; set; }
		public string InvoiceSettlementUomShort { get; set; }
		public int? InvoiceCarrierSettlementFactorID { get; set; }
		public int? InvoiceSettlementFactorID { get; set; }
		public string InvoiceSettlementFactor { get; set; }
		public int? InvoiceMinSettlementUnitsID { get; set; }
		public int? InvoiceMinSettlementUnits { get; set; }
		public decimal? InvoiceUnits { get; set; }
		public decimal? InvoiceRouteRate { get; set; }
		public string InvoiceRouteRateType { get; set; }
		public decimal? InvoiceRateSheetRate { get; set; }
		public string InvoiceRateSheetRateType { get; set; }
		public decimal? InvoiceFuelSurchargeRate { get; set; }
		public decimal? InvoiceFuelSurchargeAmount { get; set; }
		public decimal? InvoiceLoadAmount { get; set; }
		public decimal? InvoiceTotalAmount { get; set; }

        public string InvoiceOtherDetailsTSV { get; set; }
        public string InvoiceOtherAmountsTSV { get; set; }
        public bool Approved { get; set; }
        public int? FinalActualMiles { get; set; }
        public int? FinalOriginMinutes { get; set; }
        public int? FinalDestMinutes { get; set; }
        public bool FinalH2S { get; set; }
        public bool FinalOriginChainup { get; set; }
        public bool FinalDestChainup { get; set; }
        public int FinalRerouteCount { get; set; }

        [NotMapped]
        public bool HasError { get; set; }
        [NotMapped]
        public string ErrorFieldCSV { get; set; }
        [NotMapped]
        public string SessionID { get; set; }

        internal class ValidationIssues
        {
            private HashSet<string> ErrorFields = new HashSet<string>();
            private HashSet<string> ManualFields = new HashSet<string>();
            public void AddErrorFields(params string[] errorFields)
            {
                foreach (string errorField in errorFields)
                    ErrorFields.Add(errorField);
            }
            public bool HasErrorFields { get { return ErrorFields.Count > 0; } }
            public string ErrorFieldCSV { get { return string.Join(", ", ErrorFields); } }
            public void AddManualFields(params string[] manualFields)
            {
                foreach (string manualField in manualFields)
                    ManualFields.Add(manualField);
            }
            public bool HasManualFields { get { return ManualFields.Count > 0; } }
            public string ManualFieldCSV { get { return string.Join(", ", ManualFields); } }
        }

        static private bool ValidateRate(DataRowView rv, string baseColName, bool conditionMet, ValidationIssues issues)
        {
            bool ret = true;

            if (conditionMet)
            {
                string rateCol = "Invoice" + baseColName + "Rate"
                    , amountCol = "Invoice" + baseColName + "Amount";

                if (!DBHelper.IsNull(rv[amountCol], true) && DBHelper.IsNull(rv[rateCol], true))
                    issues.AddManualFields(amountCol, rateCol);
            }

            return ret;
        }

        static private bool ValidateValue(DataRowView rv, bool errorCondition, ValidationIssues issues, params string[] colNames)
        {
            bool ret = true;

            if (errorCondition)
            {
                foreach (string colName in colNames)
                {
                    issues.AddErrorFields(colNames);
                }
            }
            return ret;
        }

        static protected DataTable AddManualFieldCSV(DataTable dtData)
        {
            dtData.Columns.Add(FLD_ManualFieldCSV);
            dtData.Columns.Add(FLD_HasManual, typeof(bool));

            foreach (DataRowView rv in dtData.AsDataView())
            {
                ValidationIssues issues = new ValidationIssues();

                if (!DBHelper.IsNull(rv["InvoiceLoadAmount"], true) && DBHelper.IsNull(rv["InvoiceRouteRate"]) && DBHelper.IsNull(rv["InvoiceRateSheetRate"]))
                    issues.AddManualFields("InvoiceRouteRate", "InvoiceRateSheetRate");

                ValidateRate(rv, "OriginChainup", DBHelper.ToBoolean(rv["FinalOriginChainup"]), issues);
                ValidateRate(rv, "DestChainup", DBHelper.ToBoolean(rv["FinalDestChainup"]), issues);
                ValidateRate(rv, "Reroute", DBHelper.ToInt32(rv["FinalRerouteCount"]) > 0, issues);
                ValidateRate(rv, "H2S", DBHelper.ToBoolean(rv["FinalH2S"]), issues);
                ValidateRate(rv, "SplitLoad", DBHelper.ToInt32(rv["TicketCount"]) > 1, issues);

                ValidateRate(rv, "OriginWait", DBHelper.ToInt32(rv["InvoiceOriginWaitBillableMinutes"]) > 0, issues);
                ValidateRate(rv, "DestinationWait", DBHelper.ToInt32(rv["InvoiceDestinationWaitBillableMinutes"]) > 0, issues);
                ValidateRate(rv, "OrderReject", DBHelper.ToBoolean(rv["Rejected"]), issues);

                if (DBHelper.IsNull(rv["InvoiceFuelSurchargeRate"], true) && !DBHelper.IsNull(rv["InvoiceFuelSurchargeAmount"], true))
                    issues.AddManualFields("InvoiceFuelSurchargeRate", "InvoiceFuelSurchargeAmount");

                /* TODO this needs to be rewritten to detect when these errors have occurred
                if (SettlementRateCellHelper.HandleOtherDetails(gdi, rv, "Carrier")) hasManualRates = true;
                */

                rv[FLD_HasManual] = issues.HasManualFields;
                rv[FLD_ManualFieldCSV] = issues.ManualFieldCSV;
            }

            return dtData;
        }

    }
}