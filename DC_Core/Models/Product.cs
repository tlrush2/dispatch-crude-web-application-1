using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DispatchCrude.Models
{
    [Table("tblProduct")]
    public class Product : AuditModelLastChangeBase
    {
        public Product()
        {
            //Origins = new List<Origin>();
            Destinations = new List<Destination>();
        }

        [Key]
        public int ID { get; set; }

        [Required, StringLength(100)]
        public string Name { get; set; }

        [StringLength(25)]
        public string ShortName { get; set; }

        [ForeignKey("ProductGroupID")]
        public virtual ProductGroup ProductGroup { get; set; }

        [DisplayName("Product Group")]
        public int ProductGroupID { get; set; }


        public /*virtual*/ ICollection<Origin> Origins { get; set; }

        public virtual ICollection<Destination> Destinations { get; set; }
    }
}
