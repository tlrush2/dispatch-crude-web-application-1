using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DispatchCrude.Models
{
    [Table("tblReportFilterOperator")]
    public class ReportFilterOperator
    {
        public enum TYPE { NoFilter = 1, Equals = 2, IN = 3, Between = 4, StartsWith = 5, Contains = 6, IsEmpty = 7, IsNotEmpty = 8, LessThan = 9, LessThanOrEqual = 10, GreaterThan = 11, GreaterThanOrEqual = 12, NotIN = 13 }

        [Key]
        public int ID { get; set; }

        [Required, StringLength(25)]
        public string Name { get; set; }

    }
}
