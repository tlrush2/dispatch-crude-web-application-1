﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

namespace DispatchCrude.Models
{
    [Table("tblImportCenterGroupStep")]
    public class ImportCenterGroupStep: IEDTO
    {
        public ImportCenterGroupStep()
        {
        }
        public ImportCenterGroupStep(int groupID, int importCenterDefinitionID, int position)
        {
            GroupID = groupID;
            ImportCenterDefinitionID = importCenterDefinitionID;
            Position = position;
        }

        [Key]
        public int ID { get; set; }

        public int GroupID { get; set; }
        [ForeignKey("GroupID")]
        [ScriptIgnore][JsonIgnore]
        public ImportCenterGroup ImportCenterGroup { get; set; }

        public int ImportCenterDefinitionID { get; set; }
        [ForeignKey("ImportCenterDefinitionID")]
        [ScriptIgnore]
        [JsonIgnore]
        public virtual ImportCenterDefinition ImportCenterDefinition { get; set; }

        public int Position { get; set; }

        [NotMapped]
        public string Name { get { return this.ImportCenterDefinition.Name; } }
    }
}
