using System;
using System.IO;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using System.Web.Script.Serialization;
using DispatchCrude.Extensions;

namespace DispatchCrude.Models
{
    [Table("tblTrailerMaintenance")]
    public class TrailerMaintenance : AuditModelLastChangeBase
    {
        [Key]
        public int ID { get; set; }

        [RequiredGreaterThanZero] 
        [DisplayName("Trailer")]
        [UIHint("_ForeignKeyDDL_S2")]
        public int TrailerID { get; set; }
        [ForeignKey("TrailerID")]
        public Trailer Trailer { get; set; }

        [Required]
        [UIHint("Date")]
        [DisplayName("Date")]
        public DateTime MaintenanceDate { get; set; }
        
        [RequiredGreaterThanZero]
        [DisplayName("Type")]
        [Column("MaintenanceTypeID")]
        [UIHint("_ForeignKeyDDL_S2")]
        public int TruckTrailerMaintenanceTypeID { get; set; }                
        [ForeignKey("TruckTrailerMaintenanceTypeID")]
        public TruckTrailerMaintenanceType TruckTrailerMaintenanceType { get; set; }
        
        [DisplayName("Cost")]
        [UIHint("Currency")]
        public decimal? Cost { get; set; }

        public string Notes { get; set; }

        [DisplayName("Document")]
        [ScriptIgnore]        
        public byte[] Document { get; set; }
        [DisplayName("Document Name")]
        public string DocName { get; set; }
        
        //TODO: check if this is actually needed
        [NotMapped]
        [ScriptIgnore]
        public string DocumentSrc
        {
            get
            {
                return Document == null || Document.Length == 0
                    ? ""
                    : string.Format("data:image/{1};base64,{0}", Convert.ToBase64String(Document), Path.GetExtension(DocName));
            }
        }
    }
}
