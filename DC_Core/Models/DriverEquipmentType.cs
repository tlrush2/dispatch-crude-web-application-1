using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DispatchCrude.Models
{
    [Table("tblDriverEquipmentType")]
    public class DriverEquipmentType : AuditModelDeleteBase
    {        
        [Key]
        public int ID { get; set; }

        [Required]
        [DisplayName("Name")]
        public string Name { get; set; }
    }
}
