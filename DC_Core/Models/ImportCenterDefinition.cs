﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DispatchCrude.Models
{
    [Table("tblImportCenterDefinition")]
    public class ImportCenterDefinition : AuditModelDeleteBase
    {
        public ImportCenterDefinition()
        {
        }

        [Key]
        public int ID { get; set; }

        [DisplayName("Root Object")]
        public int RootObjectID{ get; set; }
        [ForeignKey("RootObjectID")]
        public ObjectDef RootObject { get; set; }

        [Required]
        public string Name { get; set; }

        public string UserNames { get; set; }

        public virtual ImportCenterInputFile InputFile { get; set; }
    }
}