using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using DispatchCrude.Core;

namespace DispatchCrude.Models
{
    [Table("tblDispatchMessage")]
    public class DispatchMessage
    {
        public DispatchMessage()
        {
        }

        [Key]
        public int ID { get; set; }

        public Guid UID { get; set; }

        public int? ThreadID { get; set; }

        [Column("ToUser")]
        public string To { get; set; }
        [Column("FromUser")]
        public string From { get; set; }
        public string Message { get; set; }
        public bool Seen { get; set; }

        public DateTime MessageDateUTC { get; set; }

        [NotMapped, DisplayName("Message Date")]
        public DateTime MessageDate
        {
            get
            {
                return DateHelper.ToLocal(MessageDateUTC, DispatchCrudeHelper.GetProfileTimeZone(HttpContext.Current));
            }
            set
            {
                MessageDateUTC = DateHelper.ToUTC(value, DispatchCrudeHelper.GetProfileTimeZone(HttpContext.Current));
            }
        }

        public static int UnreadMessageCount()
        { 
            using (DispatchCrudeDB db = new DispatchCrudeDB())
            {
                return db.DispatchMessages.Where(m => m.To == UserSupport.UserName && m.Seen == false).Count();
            }
        }

        public static List<DispatchContact> Contacts()
        {
            string sql = @"
                SELECT UserName, DriverID=d.ID, d.FullName, LastChatUTC,
                        online = CAST (CASE WHEN LastChatUTC > DATEADD(HOUR, -2, GETUTCDATE()) THEN 1 ELSE 0 END AS BIT)
	                FROM viewDriverBase d 
	                JOIN tblCACHE_DriverUserNames du ON du.DriverID = d.ID
	                OUTER APPLY dbo.fnSplitString(du.UserNames, ',') cs
                    JOIN ( 
                            SELECT UserName = CASE WHEN FromUser = '" + UserSupport.UserName + @"' THEN ToUser ELSE FromUser END,
                    			    LastChatUTC = MAX(MessageDateUTC)
	                            FROM tblDispatchMessage
	                            WHERE MessageDateUTC > DATEADD(DAY, -30, GETUTCDATE())
        	                    GROUP BY CASE WHEN FromUser = '" + UserSupport.UserName + @"' THEN ToUser ELSE FromUser END
                    ) msg ON msg.UserName = cs.Value";

            using (DispatchCrudeDB db = new DispatchCrudeDB())
            {
                return db.Database.SqlQuery<DispatchContact>(sql).AsQueryable().OrderByDescending(m => m.FullName).ToList();
            }
        }
    }


    [NotMapped] // required so with driver doesnt try to attach to base class (which is tied to db)
    public class DispatchMessageWithDriver : DispatchMessage
    {
        public int? DriverID { get; set; }
        public string Driver { get; set; }
        public string UserName { get; set; }
    }

    public class DispatchContact
    {
        public int DriverID { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public DateTime? LastChatUTC { get; set; }
        public DateTime? LastChat()
        {
            if (LastChatUTC == null)
                return null;
            return DateHelper.ToLocal(LastChatUTC.Value, DispatchCrudeHelper.GetProfileTimeZone(HttpContext.Current));
        }

        public bool online { get; set; }
    }


    public class DispatchBlast
    {
        public string Message { get; set; }
        [DisplayName("Carrier")]
        public int? CarrierID { get; set; }
        [DisplayName("Driver Group")]
        public int? DriverGroupID { get; set; }
        [DisplayName("Driver")]
        public int? DriverID { get; set; }
        [DisplayName("Terminal")]
        public int? TerminalID { get; set; }
        [DisplayName("Region")]
        public int? RegionID { get; set; }
        [DisplayName("State")]
        public int? StateID { get; set; }
    }
}
