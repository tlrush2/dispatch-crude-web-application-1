using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DispatchCrude.Models
{
    [Table("tblDriverEquipment")]
    public class DriverEquipment : AuditModelDeleteBase
    {        
        [Key]
        public int ID { get; set; }

        [DisplayName("Equipment Type")]
        [UIHint("_ForeignKeyDDL_S2")]
        public int? DriverEquipmentTypeID { get; set; }
        [ForeignKey("DriverEquipmentTypeID")]
        public DriverEquipmentType DriverEquipmentType { get; set; }

        [DisplayName("Manufacturer")]
        [UIHint("_ForeignKeyDDL_S2")]
        public int? DriverEquipmentManufacturerID { get; set; }
        [ForeignKey("DriverEquipmentManufacturerID")]
        public DriverEquipmentManufacturer DriverEquipmentManufacturer { get; set; }

        [DisplayName("Model #")]
        public string ModelNum { get; set; }

        [DisplayName("Serial #")]
        public string SerialNum { get; set; }

        [DisplayName("Phone #")]
        public string PhoneNum { get; set; }

        [DisplayName("OS Version")]
        public string AndroidApiLevel { get; set; }        

        [NotMapped]
        public AndroidOSVersion AndroidOSVersion
        {
            get
            {
                using (DispatchCrudeDB db = new DispatchCrudeDB())
                {
                    return db.AndroidOSVersions.Where(x => x.ApiLevel == this.AndroidApiLevel).FirstOrDefault();
                }
            }
        }
    }
}
