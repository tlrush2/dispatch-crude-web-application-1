using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.SqlClient;
using AlonsIT;
using System.Linq;

namespace DispatchCrude.Models
{
    [Table("tblUom")]
    public class Uom : AuditModelDeleteBase, IValidatableObject
    {
        public enum UomTypes { Barrel = 1, Gallon = 2, CubicMeter = 3, Litre = 4, Weight_Pound = 5, Weight_USTon = 6 }

        [Key]
        public int ID { get; set; }

        [Required, StringLength(25)]
        public string Name { get; set; }

        [Required, StringLength(10)]
        [DisplayName("Abbreviation")]
        public string Abbrev { get; set; }

        [Required]
        [DisplayName("Gallon Equivalence Factor")]
        public decimal GallonEquivalent { get; set; }

        [Required]
        [UIHint("_UomTypeDDL")]
        [DisplayName("Uom Type")]
        public int UomTypeID { get; set; }
        [ForeignKey("UomTypeID")]
        public UomType UomType { get; set; }

        [Required]
        [DisplayName("Significant Digits")]
        public int SignificantDigits { get; set; }

        public static decimal ConvertUOM(decimal units, int fromUomID, int toUomID)
        {
            using (SSDB db = new SSDB())
            {
                using (SqlCommand cmd = db.BuildCommand("SELECT dbo.fnConvertUOM(@Units, @fromUomID, @toUomID)"))
                {
                    cmd.Parameters.AddWithValue("@Units", units);
                    cmd.Parameters.AddWithValue("@fromUomID", fromUomID);
                    cmd.Parameters.AddWithValue("@toUomID", toUomID);
                    return Decimal.Parse(cmd.ExecuteScalar().ToString());
                }
            }
        }

        public string getFormat()
        {
            // return the significant figures for fomatting the units (i.e. 0.00)
            if (this.SignificantDigits <= 0)
                return "0";

            return "0." + new String('0', this.SignificantDigits);
        }

        public string getStep()
        {
            // return the step for html inputs (i.e. 0.01)
            if (this.SignificantDigits <= 1)
                return "0";

            return "0." + new String('0', this.SignificantDigits-1) + "1";
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!ValidateDuplicateName(ID, Name))
                yield return new ValidationResult("Name is already in use");
            Validated = true;
        }

        private bool ValidateDuplicateName(int id, string name)
        {
            using (DispatchCrudeDB db = new DispatchCrudeDB())
            {
                return db.Uoms.Where(u => u.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase) && (id == 0 || u.ID != id)).Count() == 0;
            }
        }

    }
}
