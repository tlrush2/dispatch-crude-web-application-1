﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using DispatchCrude.Extensions;

namespace DispatchCrude.Models
{

    [Table("tblContract")]
    public class Contract : AuditModelDeleteBase, IValidatableObject
    {
        [Key]
        public int ID { get; set; }

        [DisplayName("Contract Number")]
        [Required, StringLength(20)]
        public string ContractNumber { get; set; }

        [DisplayName("Description")]
        [StringLength(100)]
        public string PrintDescription { get; set; }

        public string Notes { get; set; }
        
        [DisplayName("Shipper")]
        [UIHint("_ForeignKeyDDL")]
        [RequiredGreaterThanZero]
        public int ShipperID { get; set; }
        [ForeignKey("ShipperID")]
        public Customer Shipper { get; set; }

        [DisplayName("Product Group")]
        [UIHint("_ForeignKeyDDL")]
        [RequiredGreaterThanZero]
        public int ProductGroupID { get; set; }
        [ForeignKey("ProductGroupID")]
        public ProductGroup ProductGroup { get; set; }

        [DisplayName("Producer")]
        [UIHint("_ForeignKeyDDL")]
        [RequiredGreaterThanZero]
        public int ProducerID { get; set; }
        [ForeignKey("ProducerID")]
        public Producer Producer { get; set; }

        public int Units { get; set; }

        [DisplayName("UOM")]
        [UIHint("_ForeignKeyDDL")]
        [RequiredGreaterThanZero]
        public int UomID { get; set; }
        [ForeignKey("UomID")]
        public Uom Uom { get; set; }

        [UIHint("Date")]
        [DisplayName("Effective Date")]
        public DateTime EffectiveDate {get; set;}

        [UIHint("Date")]
        [DisplayName("End date")]
        public DateTime EndDate { get; set; }



        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!ValidateDates(ID, EffectiveDate, EndDate))
                yield return new ValidationResult("End date must be greater than effective date");
            if (!ValidateOverlappingContract(ID, ShipperID, ProductGroupID, ProducerID, EffectiveDate, EndDate))
                yield return new ValidationResult("Contract overlaps an existing one");
            Validated = true;
        }


        private bool ValidateDates(int id, DateTime EffectiveDate, DateTime EndDate)
        {
            return EndDate >= EffectiveDate;
        }

        private bool ValidateOverlappingContract(int id, int ShipperID, int ProductGroupID, int ProducerID, DateTime EffectiveDate, DateTime EndDate)
        {
            // Confirm that another contract with the same shipper/product group/producer doesn't have an overlapping date
            using (DispatchCrudeDB db = new DispatchCrudeDB())
            {
                return db.Contracts.Where(m => m.ShipperID == ShipperID 
                            && m.ProductGroupID == ProductGroupID 
                            && m.ProducerID == ProducerID 
                            && (  (EffectiveDate >= m.EffectiveDate && EffectiveDate <= m.EndDate)
                                || (EndDate >= m.EffectiveDate && EndDate <= m.EndDate)
                                || (m.EffectiveDate >= EffectiveDate && m.EffectiveDate <= EndDate))
                            && (id == 0 || m.ID != id)).Count() == 0;
            }
        }

    }

}
