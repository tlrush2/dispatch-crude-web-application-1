﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchCrude.Models
{
    [Table("tblImportCenterGroup")]
    public class ImportCenterGroup: AuditModelLastChangeBase
    {
        public ImportCenterGroup()
        {
            Steps = new List<ImportCenterGroupStep>();
        }
        public ImportCenterGroup(ImportCenterGroup src, DispatchCrudeDB db): this()
        {
            CopyValues(src, db);
        }
        public void CopyValues(ImportCenterGroup src, DispatchCrudeDB db)
        {
            this.ID = src.ID;
            this.Name = src.Name;
            this.UserNames = src.UserNames;
            // remove existing (we need to recreate due to position (ordering) potentially changing
            this.Steps.ToList().ForEach(s => db.Entry(s).State = EntityState.Deleted);
            // recreate the steps (with ordering preserved with incremented Position value)
            int position = 0;
            foreach (ImportCenterGroupStep step in src.Steps)
            {
                this.Steps.Add(new ImportCenterGroupStep(step.ID, step.ImportCenterDefinitionID, ++position));
            }
        }

        [Key]
        public int ID { get; set; }

        [Required]
        public string Name { get; set; }

        [StringLength(1000)]
        public string UserNames { get; set; }

        [UIHint("_GroupSteps")]
        public virtual List<ImportCenterGroupStep> Steps { get; set; }
    }
}
