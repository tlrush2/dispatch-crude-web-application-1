using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DispatchCrude.Models
{
    [Table("tblPumper")]
    public class Pumper : AuditModelLastChangeBase
    {
        public Pumper()
        {
            this.Origins = new List<Origin>();
        }

        public ICollection<Origin> Origins { get; set; }

        [Key]
        public int ID { get; set; }

        [Required, StringLength(25)]
        public string FirstName { get; set; }
        [Required, StringLength(25)]
        public string LastName { get; set; }

        [StringLength(70), DisplayName("Contact Email")]
        public string ContactEmail { get; set; }
        [StringLength(20), DisplayName("Contact Phone #")]
        public string ContactPhone { get; set; }

        public int OperatorID { get; set; }
        public virtual Operator Operator { get; set; }

        public string FullName
        {
            get { return this.LastName + ", " + this.FirstName; }
        }
    }
}
