using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DispatchCrude.Models
{
    [Table("tblOriginTankStrapping")]
    public class OriginTankStrapping : AuditModelLastChangeBase
    {
        public OriginTankStrapping() { }

        [Key]
        public int ID { get; set; }

        [Required]
        [DisplayName("Tank #")]
        public int OriginTankID { get; set; }

        [ForeignKey("OriginTankID")]
        public /*virtual*/ OriginTank OriginTank { get; set; }

        [DisplayName("Feet")]
        public byte Feet { get; set; }

        [DisplayName("Inches")]
        public byte Inches { get; set; }

        [DisplayName("Q")]
        public byte Q { get; set; }

        [DisplayName("BPQ")]
        public decimal BPQ { get; set; }

        [DisplayName("Is Min?")]
        public bool IsMinimum { get; set; }

        [DisplayName("Is Max?")]
        public bool IsMaximum { get; set; }

    }

}
