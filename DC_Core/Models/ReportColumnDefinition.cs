﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DispatchCrude.Models
{
    [Table("tblReportColumnDefinition")]
    public class ReportColumnDefinition: IEDTO
    {
        public enum SystemID { Carrier = 1, Customer = 2, Destination = 3, OrderDate = 4, OrderNum = 5
            , Origin = 6, OriginGrossStdUnits = 7, OriginGrossUnits = 8, OriginNetUnits = 9, PrintStatus = 12
            , DestGrossUnits = 25, DestNetUnits = 26, DestOpenMeterUnits = 46, DestCloseMeterUnits = 47
            , TicketCount = 80, RerouteCount = 81, TotalMinute = 82
            , Blank = 88
            , ShipperBatchNum = 162, CarrierBatchNum = 180, ProducerBatchNum = 285, DriverBatchNum = 321 }

        public int ID { get; set; }

        public int ReportID { get; set; }
        [ForeignKey("ReportID")]
        public ReportDefinition Report { get; set; }
        
        [Required, StringLength(800)]
        string DataField { get; set; }

        [StringLength(255)]
        public string Caption { get; set; }

        [StringLength(255)]
        public string DataFormat { get; set; }

        [StringLength(255)]
        public string FilterDataField { get; set; }

        public int? FilterTypeID { get; set; }

        [StringLength(1000)]
        public string FilterDropDownSql { get; set; }

        public bool FilterAllowCustomText { get; set; }

        [StringLength(1000)]
        public string AllowedRoles { get; set; }

        public bool OrderSingleExport { get; set; }
    }
}