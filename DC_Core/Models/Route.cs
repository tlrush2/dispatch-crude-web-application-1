using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DispatchCrude.Core;

namespace DispatchCrude.Models
{
    [Table("tblRoute")]
    public class Route : AuditModelDeleteBase
    {
        public Route()
        {
        }

        [Key]
        public int ID { get; set; }

        [UIHint("_OriginDDL")]
        [DisplayName("Origin")]
        public int OriginID { get; set; }

        [ForeignKey("OriginID")]
        [DisplayName("OriginID")]
        public Origin Origin { get; set; }

        [UIHint("_DestinationDDL")]
        [DisplayName("Destination")]
        public int DestinationID { get; set; }

        [ForeignKey("DestinationID")]
        [DisplayName("DestinationID")]
        public Destination Destination { get; set; }

        [DisplayName("Rate Miles")]
        public int? ActualMiles { get; set; }

        [DisplayName("WRMSV Units")]
        public int? WRMSVUnits { get; set; }

        [UIHint("_UomDDL")]
        [DisplayName("WRMSV UOM")]
        public int? WRMSVUomID { get; set; }

        [ForeignKey("WRMSVUomID")]
        [DisplayName("WRMSVUomID")]
        public Uom WRMSVUom { get; set; }
                
        [DisplayName("ECOT (Minutes)")]
        [UIHint("Duration")]
        public int? ECOT { get; set; }

        [DisplayName("ECOT Locked?")]
        public bool ECOTLocked { get; set; }

        //***************
        // Per Maverick, this functionality should be tied to the activation/deactivation 
        // of the origins and destinations not managed manually on the routes page
        //***************
        //Note: to add this to viewmodels this may not need to be a [NotMapped] field
        //[NotMapped]
        //override public bool allowRowDeactivate  //works with customKendoButtons.js to get rid of the deactivate button for locked rows
        //{
        //    get
        //    {
        //        //This is a required field in the models for the Telerik Ajax bound MVC grids but this model has no false conditions
        //        //so we'll return true always.
        //        return true;
        //    }
        //}

        public int getMiles()
        {
            if (ActualMiles != null)
                return ActualMiles.Value;

            try {
                // use as crow flies distance
                 return (int)LocationHelper.getMiles(decimal.Parse(Origin.LAT), decimal.Parse(Origin.LON), decimal.Parse(Destination.LAT), decimal.Parse(Destination.LON));
            }
            catch { return 60; }
        }

        public int getECOT()
        {
            // use database average
            if (ECOT != null)
                return ECOT.Value;

            // use mileage to approximate average time
            const int AVERAGE_MILES_PER_HOUR = 60;
            const int AVERAGE_LOAD_MINUTES = 60;

            return AVERAGE_LOAD_MINUTES + getMiles()*60/AVERAGE_MILES_PER_HOUR + AVERAGE_LOAD_MINUTES;
        }
    }
}
