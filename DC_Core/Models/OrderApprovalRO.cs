﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using AlonsIT;

namespace DispatchCrude.Models
{
    [Table("viewOrderApprovalSource")]
    public class OrderApprovalRO
    {
        static public IEnumerable<OrderApprovalRO> RetrieveWithParameters1(int? shipperID, DateTime? start, DateTime? end, bool includeApproved = false)
        {
            using (DispatchCrudeDB db = new DispatchCrudeDB())
            {
                return db.OrderApprovals.SqlQuery("EXEC spOrderApprovalSource @shipperID=@SID, @start=@S, @end=@E, @userName=@UN, @includeApproved=@IA"
                    , new SqlParameter("SID", shipperID.HasValue ? (object)shipperID.Value : DBNull.Value)
                    , new SqlParameter("S", start.HasValue ? (object)start.Value : DBNull.Value)
                    , new SqlParameter("E", end.HasValue ? (object)end.Value : DBNull.Value)
                    , new SqlParameter("UN", HttpContext.Current.User.Identity.Name)
                    , new SqlParameter("IA", includeApproved)
                ).ToList<OrderApprovalRO>();
            }
        }

        public OrderApprovalRO() { }

        [Key, ForeignKey("Order")]
        public int ID { get; set; }

        [ForeignKey("ID")]
        public Order Order { get; set; }

        public int OrderNum { get; set; }
        public DateTime? OrderDate { get; set; }
        [NotMapped]
        public string OrderDateStr
        {
            get
            {
                return OrderDate.HasValue ? OrderDate.Value.ToString("M/d/yy") : "";
            }
        }
        public DateTime? DeliverDate { get; set; }
        public int StatusID { get; set; }
        public string Status { get; set; }

        public int TicketTypeID { get; set; }
        public string TicketType { get; set; }
        public int ProductID { get; set; }
        public string Product { get; set; }
        public int ProductGroupID { get; set; }
        public string ProductGroup { get; set; }
        public int ShipperID { get; set; }
        public string Shipper { get; set; }
        public int CarrierID { get; set; }
        public string Carrier { get; set; }
        public int DriverID { get; set; }
        public string Driver { get; set; }
        public int? TruckID { get; set; }
        public string Truck { get; set; }
        public int? TrailerID { get; set; }
        public string Trailer { get; set; }
        public int? Trailer2ID { get; set; }
        public string Trailer2 { get; set; }
        public int? OperatorID { get; set; }
        public string Operator { get; set; }
        public int? ProducerID { get; set; }
        public string Producer { get; set; }
        public int? PumperID { get; set; }
        public string Pumper { get; set; }

        public bool Rejected { get; set; }
        public string RejectNotes { get; set; }
        public string RejectReason { get; set; }

        public bool OriginChainUp { get; set; }
        public bool DestChainUp { get; set; }
        public bool H2S { get; set; }

        public int OriginID { get; set; }
        public int? OriginRegionID { get; set; }
        public string Origin { get; set; }
        public DateTime? OriginArriveTime { get; set; }
        public DateTime? OriginDepartTime { get; set; }
        public int? OriginMinutes { get; set; }
        public string OriginWaitReason { get; set; }
        public string OriginWaitNotes { get; set; }
        //public int? OriginWaitBillableMinutes { get; set; }

        public int OriginUomID { get; set; }
        public string OriginUOM { get; set; }
        public decimal? OriginGrossUnits { get; set; }
        public decimal? OriginGrossStdUnits { get; set; }
        public decimal? OriginNetUnits { get; set; }

        public int DestinationID { get; set; }
        public string Destination { get; set; }
        public int? DestRegionID { get; set; }
        public DateTime? DestArriveTime { get; set; }
        public DateTime? DestDepartTime { get; set; }
        public int? DestMinutes { get; set; }
        public string DestWaitReason { get; set; }
        public string DestWaitNotes { get; set; }
        //public int? DestinationWaitBillableMinutes { get; set; }

        public int DestUomID { get; set; }
        public string DestUOM { get; set; }
        public decimal? DestGrossUnits { get; set; }
        public decimal? DestNetUnits { get; set; }
        public decimal? DestOpenMeterUnits { get; set; }
        public decimal? DestCloseMeterUnits { get; set; }

        public int? ActualMiles { get; set; }
        public bool Rerouted { get; set; }
        public int? RerouteMiles { get; set; }
        public string RerouteNotes { get; set; }
        public string PreviousDestinations { get; set; }

        public string AuditNotes { get; set; }

        [DisplayName("Approval Notes")]
        public string ApprovalNotes { get; set; }

        public string DispatchConfirmNum { get; set; }
        public string DispatchNotes { get; set; }
        public string PickupDriverNotes { get; set; }
        public string DeliverDriverNotes { get; set; }

        public int? WaitFeeParameterID { get; set; }

        public bool? OverrideOriginChainup { get; set; }
        public bool? OverrideDestChainup { get; set; }
        public bool? OverrideH2S { get; set; }
        //public int? OriginOverrideMinutes { get; set; }   
        //public int? DestOverrideMinutes { get; set; }
        public bool? OverrideReroute { get; set; }

        public bool Approved { get; set; }
    }
}