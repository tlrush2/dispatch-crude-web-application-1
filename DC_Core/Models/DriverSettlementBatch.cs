﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using System.Web.Script.Serialization;
using System.Linq;
using DispatchCrude.Extensions;
using System.Collections.Generic;

namespace DispatchCrude.Models
{
    [Table("tblDriverSettlementBatch")]
    public class DriverSettlementBatch: SettlementBatch
    {
        public override string TABLE { get { return "Driver"; } }

        [Key]
        public int ID { get; set; }

        public int BatchNum { get; set; }

        [RequiredGreaterThanZero]
        [DisplayName("Carrier")]
        [UIHint("_ForeignKeyDDL")]
        public int CarrierID { get; set; }

        [DisplayName("Carrier")]
        [ForeignKey("CarrierID")]
        public Carrier Carrier { get; set; }

        [RequiredGreaterThanZero]
        [DisplayName("Driver Group")]
        [UIHint("_ForeignKeyDDL")]
        public int? DriverGroupID { get; set; }

        [DisplayName("Driver Group")]
        [ForeignKey("DriverGroupID")]
        public DriverGroup DriverGroup { get; set; }

        [RequiredGreaterThanZero]
        [DisplayName("Driver")]
        [UIHint("_ForeignKeyDDL")]
        public int? DriverID { get; set; }

        [DisplayName("Driver")]
        [ForeignKey("DriverID")]
        public Driver Driver { get; set; }

        [StringLength(255)]
        public string Notes { get; set; }

        [UIHint("Date")]
        public DateTime BatchDate { get; set; }

        [DisplayName("Invoice #")]
        [StringLength(50)]
        public string InvoiceNum { get; set; }

        [UIHint("Date")]
        public DateTime PeriodEndDate { get; set; }
    }
}
