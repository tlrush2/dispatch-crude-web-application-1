using AlonsIT;
using Kendo.Mvc.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DispatchCrude.Models
{
    [Table("tblTicketType")]
    public class TicketType : AuditModelDeleteBase, IValidatableObject
    {
        public enum TYPE { GaugeRun = 1, NetVolume = 2, MeterRun = 3, BasicRun = 4, GrossVolume = 5, CAN_MeterRun = 6, GaugeNet = 7, 
                            CAN_BasicRun = 8, MineralRun = 9, Propane_With_Scale = 10, Production_Water_Run = 11, Gauge_Auto_Close = 12,
                            Asphalt = 13 };
        
        public TicketType()
        {
            //this.Origins = new List<Origin>();
        }

        [Key]
        public int ID { get; set; }

        [Required, StringLength(25)]
        [DisplayName("Friendly Name")]
        public string Name { get; set; }

        [Required]
        [DisplayName("Requires Tanks?")]
        public bool ForTanksOnly { get; set; }

        [Required, StringLength(100)]
        public string CountryID_CSV { get; set; }

        [DisplayName("C.O.D.E. Export ID")]
        public int? CodeDXCode { get; set; }

        [Required, StringLength(25)]
        [DisplayName("Internal Name")]
        public string InternalName { get; set; }

        [StringLength(256)]
        [DisplayName("Description")]
        public string Description { get; set; }
        
        [NotMapped]
        public string SystemName {
            get
            {
                return Name.Replace(" ", "");
            }
        }

        [NotMapped]
        public string NameWithStatus
        {
            get
            {
                if (Active)
                    return Name;
                else
                    return "[DEACTIVATED] " + Name;
            }
        }

        [NotMapped]
        [DisplayName("Country")]
        public string CountryNames
        {
            get
            {
                using (SSDB ssdb = new SSDB())
                {
                    string[] countries = CountryID_CSV.Split(',');
                    string countryNames = "";
                    
                    foreach (string country in countries)
                    {
                        if (countryNames == "")
                        {
                            countryNames = ssdb.QuerySingleValue("SELECT Name FROM tblCountry WHERE ID = {0}", country).ToString();
                        }
                        else
                        {
                            countryNames += ", " + ssdb.QuerySingleValue("SELECT Name FROM tblCountry WHERE ID = {0}", country).ToString();
                        }   
                    }

                    return countryNames;
                }
            }
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!ValidateDuplicateName(ID, Name))
                yield return new ValidationResult("Name is already in use");
            Validated = true;
        }

        private bool ValidateDuplicateName(int id, string name)
        {
            using (DispatchCrudeDB db = new DispatchCrudeDB())
            {
                return db.TicketTypes.Where(m => m.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase) && (id == 0 || m.ID != id)).Count() == 0;
            }
        }
    }

}
