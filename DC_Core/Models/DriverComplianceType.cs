using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using System.Linq;
using System;

namespace DispatchCrude.Models
{
    [Table("tblDriverComplianceType")]
    public class DriverComplianceType : AuditModelDeleteBase, IValidatableObject
    {
        public enum TYPES { CDL = 2, H2S = 12 } // System types only

        [Key]
        public int ID { get; set; }

        [Required]
        public string Name { get; set; }

        //[Required] Dropdown fields don't work right.  Validation done in controller instead.
        [DisplayName("Category")]
        [UIHint("_ForeignKeyDDL")]        
        public int CategoryID { get; set; }

        [ForeignKey("CategoryID")]
        public virtual DriverComplianceCategory DriverComplianceCategory { get; set; }
        
        [DisplayName("Is System?")]
        public bool IsSystem { get; set; }

        [DisplayName("Is Required?")]
        [UIHint("Switch")]
        public bool IsRequired { get; set; }

        [DisplayName("Requires Document?")]
        [UIHint("Switch")]
        public bool RequiresDocument { get; set; }

        [DisplayName("Expiration Length (Days)")]
        public int? ExpirationLength { get; set; }

        /// <summary>
        /// Returns boolean based upon the existence of an expiration date
        /// </summary>
        [NotMapped]
        public bool Expires
        {
            get { return ExpirationLength != null && ExpirationLength > 0; }
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!ValidateDuplicateName(ID, Name))
                yield return new ValidationResult("Type name is already in use");
            Validated = true;
        }

        private bool ValidateDuplicateName(int id, string name)
        {
            using (DispatchCrudeDB db = new DispatchCrudeDB())
            {
                return db.DriverComplianceTypes.Where(m => m.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase) && (id == 0 || m.ID != id)).Count() == 0;
            }
        }

        public override bool allowRowDeactivate
        {
            get
            {
                //Override normal deactivate behavior: If the record is marked as a system record.  Do no allow it to be deactivated.
                return !IsSystem;
            }
        }
    }    
}
