using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DispatchCrude.Models
{
    [Table("tblOrderReroute")]
    public class OrderReroute: IEDTO
    {
        [Key]
        public int ID { get; set; }

        [Required, StringLength(255), DisplayName("User")]
        public string UserName { get; set; }

        [StringLength(255)]
        public string Notes { get; set; }

        [Required, DisplayName("Reroute Date")]
        public Nullable<System.DateTime> RerouteDate { get; set; }

        public int OrderID { get; set; }
        [ForeignKey("OrderID")]
        public virtual Order Order { get; set; }

        public int PreviousDestinationID { get; set; }
        [ForeignKey("PreviousDestinationID")]
        public virtual Destination PreviousDestination { get; set; }
    }

    public class OrderRerouteViewModel
    {
        public OrderRerouteViewModel() { }
        public OrderRerouteViewModel(int OrderID)
        {
            this.OrderID = OrderID;
        }

        [Required]
        public int OrderID { get; set; }

        [Required]
        [DisplayName("Reroute Destination")]
        public int DestinationID { get; set; }

        [StringLength(255), DataType(DataType.MultilineText)]
        public string Notes { get; set; }
    }
}
