﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DispatchCrude.Models
{
    public class OrderException
    {
        public OrderException(string msg) { Message = msg; }

        public string Message { get; set; }
    }
}