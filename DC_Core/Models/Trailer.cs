using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DispatchCrude.Models
{
    [Table("tblTrailer")]
    public class Trailer : AuditModelDeleteBase
    {
        public Trailer()
        {
            //this.Drivers = new List<Driver>();
        }

        [Key]
        public int ID { get; set; }

        [Required, StringLength(10), DisplayName("Trailer #")]
        public string IDNumber { get; set; }

        [DisplayName("Trailer Cert Date")]
        public DateTime? TrailerCertDate { get; set; }
        [StringLength(20), DisplayName("DOT #")]
        public string DOTNumber { get; set; }
        [StringLength(20)]
        public string VIN { get; set; }
        [StringLength(20), DisplayName("License #")]
        public string LicenseNumber { get; set; }
        [StringLength(20)]
        public string Make { get; set; }
        [StringLength(20)]
        public string Model { get; set; }

        public int? Year { get; set; }
        [StringLength(50)]
        public string Composition { get; set; }

        [DisplayName("Owner Info"), StringLength(255)]
        public string OwnerInfo { get; set; }

        [DisplayName("Axle Count")]
        public int? AxleCount { get; set; }

        [StringLength(25)]
        public string MeterType { get; set; }

        [Required, DisplayName("UOM"), DefaultValue(1)]
        public int UomID { get; set; }

        [ForeignKey("UomID"), DisplayName("UOM")]
        public Uom UOM { get; set; }

        [DisplayName("Compartment 1 Vol")]
        public decimal? Compartment1Units { get; set; }
        [DisplayName("Compartment 2 Vol")]
        public decimal? Compartment2Units { get; set; }
        [DisplayName("Compartment 3 Vol")]
        public decimal? Compartment3Units { get; set; }
        [DisplayName("Compartment 4 Vol")]
        public decimal? Compartment4Units { get; set; }
        [DisplayName("Compartment 5 Vol")]
        public decimal? Compartment5Units { get; set; }

        [NotMapped, DisplayName("Compartment Count")]
        public int CompartmentCount
        {
            get
            {
                return (Compartment1Units.HasValue ? 1 : 0)
                    + (Compartment2Units.HasValue ? 1 : 0)
                    + (Compartment3Units.HasValue ? 1 : 0)
                    + (Compartment4Units.HasValue ? 1 : 0)
                    + (Compartment5Units.HasValue ? 1 : 0);
            }
        }

        [NotMapped, DisplayName("Capacity Vol")]
        public decimal? TotalCapacityUnits
        {
            get
            {
                return (Compartment1Units.HasValue ? Compartment1Units.Value : 0)
                    + (Compartment2Units.HasValue ? Compartment2Units.Value : 0)
                    + (Compartment3Units.HasValue ? Compartment3Units.Value : 0)
                    + (Compartment4Units.HasValue ? Compartment4Units.Value : 0)
                    + (Compartment5Units.HasValue ? Compartment5Units.Value : 0);
            }
        }

        [DisplayName("Carrier")]
        public int CarrierID { get; set; }

        [DisplayName("Trailer Type")]
        public int? TrailerTypeID { get; set; }

        //[InverseProperty("Trailer")]
        //public ICollection<Driver> Drivers { get; set; }

        [ForeignKey("CarrierID")]
        public /*virtual*/ Carrier Carrier { get; set; }
        
        [ForeignKey("TrailerTypeID")][DisplayName("Trailer Type")]
        public TrailerType TrailerType { get; set; }
        
        [DisplayName("Purchase $$")]
        public decimal? PurchasePrice { get; set; }
        [DisplayName("Garage City")]
        public string GarageCity { get; set; }
        [DisplayName("Garage State")]
        public int? GarageStateID { get; set; }

        [DisplayName("QR Code")]
        public Guid QRCode { get; set; }

        [DisplayName("Terminal")]
        public int? TerminalID { get; set; }
        [ForeignKey("TerminalID")]
        [DisplayName("Terminal")]
        public Terminal Terminal { get; set; }

        [NotMapped]
        public string IDNumber_FlagInactive
        {
            get { return getIDNumber(flagDeleted: true); }
        }

        //Set the display order to put "deactivated" in front for usability's sake.  
        //When in dropdown boxes "name + [DEACTIVATED]" becomes very difficult to read/hard on 
        //the eyes because it creates an uneven edge.
        public string getIDNumber(bool flagDeleted = false)
        {
            return (flagDeleted && Deleted ? "[DEACTIVATED] " + IDNumber : IDNumber);
        }
    }

}
