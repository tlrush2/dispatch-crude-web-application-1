using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace DispatchCrude.Models
{
    [Table("tblCarrier")]
    public class Carrier : AuditModelDeleteBase
    {
        public Carrier()
        {
            //this.Drivers = new List<Driver>();
            //this.Trailers = new List<Trailer>();
            //this.Trucks = new List<Truck>();
        }

        //public virtual ICollection<Driver> Drivers { get; set; }
        //public /*virtual*/ ICollection<Trailer> Trailers { get; set; }
        //public /*virtual*/ ICollection<Truck> Trucks { get; set; }

        [Key]
        public int ID { get; set; }

        [Required, StringLength(20), DisplayName("Carrier #")]
        public string IDNumber { get; set; }

        [Required, StringLength(40)]
        public string Name { get; set; }

        [StringLength(40)]
        public string Address { get; set; }

        [StringLength(30)]
        public string City { get; set; }

        [DisplayName("State")]
        public int? StateID { get; set; }
        [ForeignKey("StateID"), DisplayName("State")]
        public /*virtual*/ State State { get; set; }

        [StringLength(10)]
        public string Zip { get; set; }

        [StringLength(40)]
        public string ContactName { get; set; }

        [StringLength(50)]
        public string ContactEmail { get; set; }

        [StringLength(15)]
        public string ContactPhone { get; set; }

        [StringLength(255)]
        public string Notes { get; set; }

        [StringLength(10), DisplayName("DOT #")]
        public string DOTNumber { get; set; }

        [StringLength(20), DisplayName("Motor Carrier #")]
        public string MotorCarrierNumber { get; set; }

        public String Authority { get; set; }

        [StringLength(15), DisplayName("FEIN #")]
        public string FEINNumber { get; set; }

        public int CarrierTypeID { get; set; }
        [ForeignKey("CarrierTypeID")]
        public /*virtual*/ CarrierType CarrierType { get; set; }

        [DisplayName("Terminal")]
        public int? TerminalID { get; set; }
        [ForeignKey("TerminalID")]
        [DisplayName("Terminal")]
        public Terminal Terminal { get; set; }

        [NotMapped]
        public string Name_FlagInactive
        {
            get { return getName(flagDeleted: true); }
        }

        //10/11/16 Changed the display order to put "deactivated" in front for usability's sake.  
        //When in dropdown boxes "name + [DEACTIVATED]" becomes very difficult to read/hard on 
        //the eyes because it creates an uneven edge.
        public string getName(bool flagDeleted = false)
        {
            return (flagDeleted && Deleted ? "[DEACTIVATED] " + Name : Name);
        }

    }
}
