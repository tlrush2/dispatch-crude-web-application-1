﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DispatchCrude.Models
{
    [Table("tblImportCenterFieldDefinitionField")]
    public class ImportCenterFieldDefinitionField: IEDTO
    {
        [Key]
        public int ID { get; set; }
        
        [DisplayName("Import Center Field")]
        public int ImportCenterFieldID { get; set; }
        [ForeignKey("ImportCenterFieldID")]
        public ImportCenterFieldDefinition ImportCenterFieldDefinition { get; set; }
        
        [Required, DisplayName("Import Field Name"), StringLength(100)]
        public string ImportFieldName { get; set; }
        
        public byte Position { get; set; }
    }
}