﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;



namespace DispatchCrude.Models
{
    [Table("aspnet_Groups")]  // aspnet roles are now being referred to as permissions DCWEB-1420
    public class DCGroup : AuditModelDeleteBase, IValidatableObject
    {
        public DCGroup()
        {
            this.ApplicationId = Guid.Parse("446467D6-39CD-45E3-B3E0-CAC7945AF3E8");
            this.GroupId = Guid.NewGuid();
            this.Users = new List<DCUser>();
            this.Permissions = new List<DCPermission>();
        }
                
        public Guid ApplicationId { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid GroupId { get; set; }

        [Required]
        [DisplayName("Name")]
        [StringLength(256, MinimumLength = 3, ErrorMessage = "Name must be at least 3 characters long")]
        public string GroupName { get; set; }

        [Required]        
        public string LoweredGroupName
        {
            get
            {
                return GroupName.ToLower();
            }
            set
            {
                value = GroupName.ToLower();
            }
        }

        [Required]
        [DisplayName("Description")]
        [StringLength(256, MinimumLength = 20, ErrorMessage = "Please be more descriptive")]
        public string Description { get; set; }


        public ICollection<DCUser> Users { get; set; }
        public ICollection<DCPermission> Permissions { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!ValidateDuplicateName(GroupId, GroupName))
                yield return new ValidationResult("Group name is already in use", new string[] { "GroupName" });
            Validated = true;
        }

        private bool ValidateDuplicateName(Guid id, string name)
        {
            using (DispatchCrudeDB db = new DispatchCrudeDB())
            {
                return db.DCGroups.Where(u => u.GroupName.Equals(name, StringComparison.CurrentCultureIgnoreCase) && (id == null || u.GroupId != id)).Count() == 0;
            }
        }
    }

    
}
