using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace DispatchCrude.Models
{
    [Table("tblOriginTank")]
    public class OriginTank : AuditModelDeleteBase , IValidatableObject
    {
        public OriginTank()
        {
            this.OriginTankStrappings = new List<OriginTankStrapping>();

        }

        [Key]
        public int ID { get; set; }

        [UIHint("_OriginDDL")]
        [Required]
        [DisplayName("Origin")]
        public int OriginID { get; set; }

        [ForeignKey("OriginID")]
        public Origin Origin { get; set; }

        [Required]
        [StringLength(20)]
        [RegularExpression(@"[A-Za-z0-9\(\)\[\]-]+", ErrorMessage = "Invalid Tank Number. Valid characters are: A-Z, 0-9, -, ( ), [ ]")]
        [DisplayName("Tank #")]
        public string TankNum { get; set; }

        [StringLength(20)]
        [RegularExpression(@"[A-Za-z0-9\(\)\[\]-]+", ErrorMessage = "Invalid Alt Tank Number (Valid characters: A-Z, 0-9, [], (), -)")]
        [DisplayName("Alt Tank #")]
        public string AltTankNum { get; set; }

        [Required]
        [StringLength(20)]
        [DisplayName("Tank Definition")]
        public string TankDescription { get; set; }

        [UIHint("_OriginTankDefinitionDDL")]
        
        [DisplayName("Tank Definition")]
        public int? TankDefinitionID { get; set; }

        [ForeignKey("TankDefinitionID")]
        public OriginTankDefinition OriginTankDefinition { get; set; }

        public /*virtual*/ ICollection<OriginTankStrapping> OriginTankStrappings { get; set; }  // Do we need this virtual???

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!ValidateDuplicateTankNumber(ID, OriginID, TankNum))
                yield return new ValidationResult("Tank number is already in use for this origin.", new string[] { "TankNum" });
            Validated = true;
        }

        private bool ValidateDuplicateTankNumber(int id, int originId, string tanknum)
        {
            using (DispatchCrudeDB db = new DispatchCrudeDB())
            {
                return db.OriginTanks.Where(m => m.TankNum.Equals(tanknum, StringComparison.CurrentCultureIgnoreCase)
                                            && m.OriginID == originId //Only validate tank numbers against tanks that are associated with "this" origin
                                            && (id == 0 || m.ID != id)).Count() == 0;
            }
        }

        [NotMapped]
        public string TankNumVirtual
        {  // This is currently not used anywhere 11/18/16
            get
            {
                return TankNum == "*" ? "[Enter Details]" : TankNum;
            }
        }

        [NotMapped]
        public string TankNumDesc
        {  // This is currently used in the Order and Tickets controllers 11/18/16
            get
            {
                return TankNum + " (" + TankDescription + ")";
            }
        }        

    }

}
