﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace DispatchCrude.Models
{
    [Table("tblHosSignature")]
    public class HosSignature: AuditModelCreateBase
    {
        public HosSignature()
        {
            this.UID = new Guid();
        }
        public HosSignature(Guid uid)
        {
            this.UID = uid;
        }
        
        [Key]
        public int ID { get; set; }

        public Guid UID { get; set; }

        [DisplayName("Driver")]
        public int? DriverID { get; set; }
        [ForeignKey("DriverID")]
        public virtual Driver Driver { get; set; }

        public DateTime HosDate { get; set; }

        public byte[] SignatureBlob { get; set; }

        [NotMapped]
        public string SignatureBlobSrc
        {
            get
            {
                return SignatureBlob.Length == 0 ? "" : string.Format("data:image/{1};base64,{0}", Convert.ToBase64String(SignatureBlob), "png");
            }
        }
    }
}