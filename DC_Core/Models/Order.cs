using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using AlonsIT;
using DispatchCrude.Core;

namespace DispatchCrude.Models
{
    [Table("tblOrder")]
    public class Order : AuditModelDeleteBase
    {
        public Order()
        {
            this.OrderReroutes = new List<OrderReroute>();
            this.OrderTickets = new List<OrderTicket>();

            this.Rejected = false; // default value (to prevent NULL if not assigned)
        }
        [Key]
        public int ID { get; set; }

        public int? OrderNum { get; set; }

        [DisplayName("Job #")]
        public string JobNumber { get; set; }

        [DisplayName("Contract #")]
        public int? ContractID { get; set; }
        [ForeignKey("ContractID")]
        public virtual Contract Contract { get; set; }

        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}"), UIHint("Date")]
        public System.DateTime DueDate { get; set; }

        [DisplayName("Tank #")]
        public int? OriginTankID { get; set; }

        [StringLength(20), DisplayName("Tank Details")]
        public string OriginTankNum { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy hh:mm tt}")]
        public DateTime? OriginArriveTimeUTC { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy hh:mm tt}")]
        public DateTime? OriginDepartTimeUTC { get; set; }

        [DisplayName("Origin Wait Min")]
        public int? OriginMinutes { get; set; }

        [DisplayName("Origin Wait Reason")]
        [ClientDropDownMustBeSelectedAttribute(ErrorMessage = "Wait Reason must be specified")]
        public int? OriginWaitReasonID { get; set; }

        [StringLength(255), DisplayName("Origin Wait Notes")]
        public string OriginWaitNotes { get; set; }

        [StringLength(15), DisplayName("Origin BOL #")]
        public string OriginBOLNum { get; set; }

        [DisplayName("Order Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}"), UIHint("Date")]
        public DateTime? OrderDate { get; set; }

        [DisplayName("Product")]
        public int ProductID { get; set; }

        [DisplayName("GOV"), DisplayFormat(DataFormatString = "{0:0.00}")]
        public decimal? OriginGrossUnits { get; set; }
        [DisplayName("GSV"), DisplayFormat(DataFormatString = "{0:0.00}")]
        public decimal? OriginGrossStdUnits { get; set; }
        [DisplayName("NSV"), DisplayFormat(DataFormatString = "{0:0.00}")]
        public decimal? OriginNetUnits { get; set; }

        [DisplayName("Origin Net Weight")]
        public decimal? OriginWeightNetUnits { get; set; }

        [DefaultValue(1)]
        public int? OriginUomID { get; set; }

        [DefaultValue(1)]
        public int? DestUomID { get; set; }

        [DisplayName("Sulfur Content"), DisplayFormat(DataFormatString = "{0:0.0000}"), Range(0, 10)]
        public decimal? OriginSulfurContent { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy hh:mm tt}")]
        public DateTime? DestArriveTimeUTC { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy hh:mm tt}")]
        public DateTime? DestDepartTimeUTC { get; set; }

        [DisplayName("Dest Wait Min")]
        public int? DestMinutes { get; set; }

        [DisplayName("Destination Wait Reason")]
        [ClientDropDownMustBeSelectedAttribute(ErrorMessage = "Wait Reason must be specified")]
        public int? DestWaitReasonID { get; set; }

        [StringLength(255), DisplayName("Dest Wait Notes")]
        public string DestWaitNotes { get; set; }

        [StringLength(30), DisplayName("Dest BOL #")]
        public string DestBOLNum { get; set; }

        [DisplayName("Dest GOV"), DisplayFormat(DataFormatString = "{0:0.00}", ApplyFormatInEditMode = true)]
        public decimal? DestGrossUnits { get; set; }
        [DisplayName("Dest NSV"), DisplayFormat(DataFormatString = "{0:0.00}", ApplyFormatInEditMode = true)]
        public decimal? DestNetUnits { get; set; }

        [DisplayName("Dest Opening Meter Vol")]
        public decimal? DestOpenMeterUnits { get; set; }
        [DisplayName("Dest Closing Meter Vol")]
        public decimal? DestCloseMeterUnits { get; set; }

        [DisplayName("Dest Product Temp (F�)"), DisplayFormat(DataFormatString = "{0:0.0}", ApplyFormatInEditMode = true)]
        public decimal? DestProductTemp { get; set; }
        [DisplayName("Dest Product BS&W"), DisplayFormat(DataFormatString = "{0:0.000\\%}")]
        public decimal? DestProductBSW { get; set; }
        [DisplayName("Dest Product API Gravity")]

        public decimal? DestProductGravity { get; set; }

        [DisplayName("Dest Gross"), DisplayFormat(DataFormatString = "{0:0.000}", ApplyFormatInEditMode = true)]
        public decimal? DestWeightGrossUnits { get; set; }
        [DisplayName("Dest Tare"), DisplayFormat(DataFormatString = "{0:0.000}", ApplyFormatInEditMode = true)]
        public decimal? DestWeightTareUnits { get; set; }
        [DisplayName("Dest Net"), DisplayFormat(DataFormatString = "{0:0.000}", ApplyFormatInEditMode = true)]
        public decimal? DestWeightNetUnits { get; set; }

        [DisplayName("Railcar #"), StringLength(25)]
        public string DestRailCarNum { get; set; }

        [DisplayName("BOL Trailer Water Cap.")]
        public int? DestTrailerWaterCapacity { get; set; }

        [Required, DisplayName("Order Rejected?"), UIHint("Switch")]
        public bool Rejected { get; set; }
        [StringLength(255), DisplayName("Reject Notes")]
        public string RejectNotes { get; set; }

        [DisplayName("Reject Reason")]
        [ClientDropDownMustBeSelectedAttribute(ErrorMessage = "Reject Reason must be specified")]
        public int? RejectReasonID { get; set; }

        [Required, DisplayName("Origin Chain-Up?"), UIHint("Switch")]
        [Column("OriginChainup")]
        public bool ChainUp { get; set; } // remain as chainup because the mobile app is already expecting/using that name

        [Required, DisplayName("Destination Chain-Up?"), UIHint("Switch")]
        public bool DestChainUp { get; set; }

        [DisplayName("Origin Truck Mileage")]
        public int? OriginTruckMileage { get; set; }

        [DisplayName("Dest Truck Mileage")]
        public int? DestTruckMileage { get; set; }

        [StringLength(15)]
        [DisplayName("Carrier Ticket #")]
        public string CarrierTicketNum { get; set; }

        [DisplayName("Pickup Print Status")]
        public int PickupPrintStatusID { get; set; }
        [DisplayName("Delivery Print Status")]
        public int DeliverPrintStatusID { get; set; }

        [DisplayName("Pickup Print Date")]
        public DateTime? PickupPrintDateUTC { get; set; }
        [DisplayName("Delivery Print Date")]
        public DateTime? DeliverPrintDateUTC { get; set; }

        [DisplayName("Last Accept [UTC]")]
        public DateTime? AcceptLastChangeDateUTC { get; set; }

        [DisplayName("Last Pickup [UTC]")]
        public DateTime? PickupLastChangeDateUTC { get; set; }

        [DisplayName("Last Deliver [UTC]")]
        public DateTime? DeliverLastChangeDateUTC { get; set; }

        [DisplayName("Correction Notes"), StringLength(255)]
        public string AuditNotes { get; set; }

        [DisplayName("Status")]
        public int StatusID { get; set; }

        [DisplayName("Ticket Type")]
        public int TicketTypeID { get; set; }

        [DisplayName("Truck")]
        [ClientDropDownMustBeSelectedAttribute(ErrorMessage = "Truck must be specified")]
        public int? TruckID { get; set; }

        [DisplayName("Trailer")]
        [ClientDropDownMustBeSelectedAttribute(ErrorMessage = "Trailer 1 must be specified")]
        public int? TrailerID { get; set; }

        [DisplayName("Trailer 2")]
        public int? Trailer2ID { get; set; }

        [DisplayName("Dispatch Notes"), StringLength(500)]
        public string DispatchNotes { get; set; }
        [DisplayName("Driver Pickup Notes"), StringLength(255)]
        public string PickupDriverNotes { get; set; }
        [DisplayName("Driver Deliver Notes"), StringLength(255)]
        public string DeliverDriverNotes { get; set; }
        [DisplayName("Shipper PO"), StringLength(25)]
        public string DispatchConfirmNum { get; set; }

        [DisplayName("Carrier")]
        public int? CarrierID { get; set; }

        [DisplayName("Consignee")]
        public int? ConsigneeID { get; set; }

        [DisplayName("Shipper")]
        public int? CustomerID { get; set; }

        [DisplayName("Destination")]
        public int? DestinationID { get; set; }

        [DisplayName("Driver")]
        public int? DriverID { get; set; }

        [DisplayName("Operator")]
        public int? OperatorID { get; set; }

        [DisplayName("Producer")]
        public int? ProducerID { get; set; }

        [DisplayName("Origin")]
        public int? OriginID { get; set; }

        [DisplayName("Priority")]
        public byte PriorityID { get; set; }

        [DisplayName("Sequence No.")]
        public int? SequenceNum { get; set; }

        [DisplayName("Pumper")]
        public int? PumperID { get; set; }

        [DisplayName("Route")]
        public int? RouteID { get; set; }

        [DisplayName("Dest Rack/Bay")]
        public string DestRackBay { get; set; }

        [DisplayName("Reassign Key")]
        //[DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public int? ReassignKey { get; set; }

        [ForeignKey("OriginTankID")]
        public virtual OriginTank OriginTank { get; set; }

        [NotMapped]
        public bool OriginWaitReasonRequired
        {
            get
            {
                return WaitReasonRequired(true);
            }
        }

        private bool WaitReasonRequired(bool forOrigin)
        {
            using (SSDB ssdb = new SSDB())
            {
                return DBHelper.ToBoolean(ssdb.QuerySingleValue(
                    "SELECT CASE WHEN {2} > {1}ThresholdMinutes THEN 1 ELSE 0 END FROM dbo.fnOrderCombinedThresholdMinutes({0})"
                    , (object)this.ID
                    , (object)(forOrigin ? "Origin" : "Dest")
                    , (object)(forOrigin ? this.OriginMinutes.GetValueOrDefault() : this.DestMinutes.GetValueOrDefault())));
            }
        }

        [ForeignKey("OriginWaitReasonID")]
        public virtual OriginWaitReason OriginWaitReason { get; set; }

        [ForeignKey("ProductID")]
        public virtual Product Product { get; set; }

        [ForeignKey("OriginUomID"), DisplayName("UOM")]
        public virtual Uom OriginUOM { get; set; }

        [ForeignKey("DestUomID"), DisplayName("UOM")]
        public virtual Uom DestUOM { get; set; }

        [NotMapped]
        public bool IsOriginQtyValid
        {   // TODO: this needs to use Trailer capacities to truly validate the order qty valid status
            get
            {
                return CalcOriginQtyValid(OriginGrossUnits) && CalcOriginQtyValid(OriginNetUnits);
            }
        }

        private int MAX_ORDER_GALLONS
        {
            get
            {
                return DBHelper.ToInt32(OrderRuleHelper.GetRuleValue(this.ID, OrderRuleHelper.Type.Max_Order_Gallons, 13440)); // why default? shouldn't use db setting
            }
        }

        private bool CalcOriginQtyValid(decimal? units)
        {
            int uomID = OriginUomID.HasValue ? OriginUomID.Value : (Origin == null ? 0 : Origin.UomID);
            return uomID == 0 ||  Uom.ConvertUOM(units.HasValue ? units.Value : (decimal)0, uomID, (int)Uom.UomTypes.Gallon) <= MAX_ORDER_GALLONS;
        }


        private int MAX_ORDER_POUNDS
        {
            get
            {
                return DBHelper.ToInt32(OrderRuleHelper.GetRuleValue(this.ID, OrderRuleHelper.Type.Max_Order_Pounds)); // why default? shouldn't use db setting
            }
        }
        private bool CalcOriginWeightValid(decimal? units)
        {
            if (MAX_ORDER_POUNDS == 0)
                return true; // no limit, any value valid
            int uomID = OriginUomID.HasValue ? OriginUomID.Value : (Origin == null ? 0 : Origin.UomID);
            return uomID == 0 || Uom.ConvertUOM(units.HasValue ? units.Value : (decimal)0, uomID, (int)Uom.UomTypes.Weight_Pound) <= MAX_ORDER_POUNDS;
        }
        [NotMapped]
        public bool IsOriginWeightValid
        {
            get
            {
                return CalcOriginWeightValid(this.OriginWeightNetUnits);
            }
        }

        [NotMapped]
        public bool DestWaitReasonRequired
        {
            get
            {
                return WaitReasonRequired(false);
            }
        }

        [ForeignKey("DestWaitReasonID")]
        public virtual DestinationWaitReason DestinationWaitReason { get; set; }

        [ForeignKey("RejectReasonID")]
        public virtual OrderRejectReason RejectReason { get; set; }

        [ForeignKey("PickupPrintStatusID"), DisplayName("Pickup Print Status")]
        public virtual PrintStatus PickupPrintStatus { get; set; }
        [ForeignKey("DeliverPrintStatusID"), DisplayName("Delivery Print Status")]
        public virtual PrintStatus DeliverPrintStatus { get; set; }

        [NotMapped]
        virtual public int PrintStatusID 
        {
            get
            {
                using (DispatchCrudeDB db = new DispatchCrudeDB())
                {
                    if (this.StatusID == (int)OrderStatus.STATUS.PickedUp
                        && !(PickupPrintStatus != null ? PickupPrintStatus : db.PrintStatuses.Single(ps => ps.ID == PickupPrintStatusID)).IsCompleted)
                    {
                        return (int)OrderStatus.STATUS.Accepted;
                    }
                    else if (this.StatusID == (int)OrderStatus.STATUS.Delivered
                        && !(DeliverPrintStatus != null ? DeliverPrintStatus : db.PrintStatuses.Single(ps => ps.ID == DeliverPrintStatusID)).IsCompleted)
                    {
                        return (int)OrderStatus.STATUS.PickedUp;
                    }
                    else return StatusID;
                }
            }
        }   

        [NotMapped, DisplayName("Arrive Time"), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy hh:mm tt}")]
        public DateTime? OriginArriveTime
        {
            get
            {
                return OriginArriveTimeUTC.HasValue ? Core.DateHelper.ToLocal(OriginArriveTimeUTC.Value, OriginTimeZone) : OriginArriveTimeUTC;
            }
            set
            {
                OriginArriveTimeUTC = value.HasValue ? Core.DateHelper.ToUTC(value.Value, OriginTimeZone) : value;
            }
        }

        [NotMapped, DisplayName("Depart Time"), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy hh:mm tt}")]
        public DateTime? OriginDepartTime
        {
            get
            {
                return OriginDepartTimeUTC.HasValue ? Core.DateHelper.ToLocal(OriginDepartTimeUTC.Value, OriginTimeZone) : OriginDepartTimeUTC;
            }
            set
            {
                OriginDepartTimeUTC = value.HasValue ? Core.DateHelper.ToUTC(value.Value, OriginTimeZone) : value;
            }
        }

        [InverseProperty("Order")]
        public virtual List<OrderSignature> OrderSignatures { get; set; }

        [NotMapped]
        public OrderSignature OriginSignature
        {
            get { return this.OrderSignatures == null || this.OrderSignatures.Count == 0 ? null : this.OrderSignatures.Where(s => s.DriverID != null && s.OriginID == this.OriginID).OrderByDescending(s => s.CreateDateUTC).FirstOrDefault(); }
        }
        [NotMapped]
        public OrderSignature DestinationSignature
        {
            get { return this.OrderSignatures == null || this.OrderSignatures.Count == 0 ? null : this.OrderSignatures.Where(s => s.DriverID != null && s.DestinationID == this.DestinationID).OrderByDescending(s => s.CreateDateUTC).FirstOrDefault(); }
        }

        [InverseProperty("Order")]
        public virtual List<OrderPhoto> OrderPhotos { get; set; }

        [NotMapped]
        public OrderPhoto OriginPhoto
        {
            get { return this.OrderPhotos == null || this.OrderPhotos.Count == 0 ? null : this.OrderPhotos.Where(s => s.PhotoTypeID == (int)PhotoType.TYPES.Location && s.OriginID == this.OriginID).OrderByDescending(s => s.CreateDateUTC).ThenByDescending(s => s.ID).FirstOrDefault(); }
        }

        [NotMapped]
        public OrderPhoto OriginPhoto2
        {
            get { return this.OrderPhotos == null || this.OrderPhotos.Count == 0 ? null : this.OrderPhotos.Where(s => s.PhotoTypeID == (int)PhotoType.TYPES.Location2 && s.OriginID == this.OriginID).OrderByDescending(s => s.CreateDateUTC).ThenByDescending(s => s.ID).FirstOrDefault(); }
        }

        [NotMapped]
        public OrderPhoto OriginArrivePhoto
        {
            get { return this.OrderPhotos == null || this.OrderPhotos.Count == 0 ? null : this.OrderPhotos.Where(s => s.PhotoTypeID == (int)PhotoType.TYPES.ConditionBefore && s.OriginID == this.OriginID).OrderByDescending(s => s.CreateDateUTC).ThenByDescending(s => s.ID).FirstOrDefault(); }
        }
        [NotMapped]
        public OrderPhoto OriginDepartPhoto
        {
            get { return this.OrderPhotos == null || this.OrderPhotos.Count == 0 ? null : this.OrderPhotos.Where(s => s.PhotoTypeID == (int)PhotoType.TYPES.ConditionAfter && s.OriginID == this.OriginID).OrderByDescending(s => s.CreateDateUTC).ThenByDescending(s => s.ID).FirstOrDefault(); }
        }

        [NotMapped]
        public OrderPhoto DestinationPhoto
        {
            get { return this.OrderPhotos == null || this.OrderPhotos.Count == 0 ? null : this.OrderPhotos.Where(s => s.PhotoTypeID == (int)PhotoType.TYPES.Location && s.DestinationID == this.DestinationID).OrderByDescending(s => s.CreateDateUTC).ThenByDescending(s => s.ID).FirstOrDefault(); }
        }

        [NotMapped]
        public OrderPhoto DestinationPhoto2
        {
            get { return this.OrderPhotos == null || this.OrderPhotos.Count == 0 ? null : this.OrderPhotos.Where(s => s.PhotoTypeID == (int)PhotoType.TYPES.Location2 && s.DestinationID == this.DestinationID).OrderByDescending(s => s.CreateDateUTC).ThenByDescending(s => s.ID).FirstOrDefault(); }
        }

        [NotMapped]
        public OrderPhoto DestArrivePhoto
        {
            get { return this.OrderPhotos == null || this.OrderPhotos.Count == 0 ? null : this.OrderPhotos.Where(s => s.PhotoTypeID == (int)PhotoType.TYPES.ConditionBefore && s.DestinationID == this.DestinationID).OrderByDescending(s => s.CreateDateUTC).ThenByDescending(s => s.ID).FirstOrDefault(); }
        }
        [NotMapped]
        public OrderPhoto DestDepartPhoto
        {
            get { return this.OrderPhotos == null || this.OrderPhotos.Count == 0 ? null : this.OrderPhotos.Where(s => s.PhotoTypeID == (int)PhotoType.TYPES.ConditionAfter && s.DestinationID == this.DestinationID).OrderByDescending(s => s.CreateDateUTC).ThenByDescending(s => s.ID).FirstOrDefault(); }
        }

        [NotMapped]
        public OrderPhoto RejectPhoto
        {
            get { return this.OrderPhotos == null || this.OrderPhotos.Count == 0 ? null : this.OrderPhotos.Where(s => s.PhotoTypeID == (int)PhotoType.TYPES.Reject).OrderByDescending(s => s.CreateDateUTC).ThenByDescending(s => s.ID).FirstOrDefault(); }
        }

        [NotMapped]
        private string OriginTimeZone
        {
            get
            {
                if (this.Origin != null && this.Origin.TimeZone != null)
                    return this.Origin.TimeZone.SystemName;
                else
                {
                    using (SSDB db = new SSDB())
                    {
                        return db.QuerySingleValue("SELECT TZ.SystemName FROM tblOrder O JOIN tblOrigin OO ON OO.ID = O.OriginID JOIN tblTimeZone TZ ON TZ.ID = OO.TimeZoneID WHERE O.ID = {0}", this.ID).ToString();
                    }
                }
            }
        }

        [NotMapped]
        private string DestTimeZone
        {
            get
            {
                if (this.Destination != null && this.Destination.TimeZone != null)
                    return this.Destination.TimeZone.SystemName;
                else
                {
                    using (SSDB db = new SSDB())
                    {
                        return db.QuerySingleValue("SELECT TZ.SystemName FROM tblOrder O JOIN tblDestination D ON D.ID = O.DestinationID JOIN tblTimeZone TZ ON TZ.ID = D.TimeZoneID WHERE O.ID = {0}", this.ID).ToString();
                    }
                }
            }
        }

        [NotMapped, DisplayName("Arrive Time"), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy hh:mm tt}")]
        public DateTime? DestArriveTime
        {
            get
            {
                return DestArriveTimeUTC.HasValue ? Core.DateHelper.ToLocal(DestArriveTimeUTC.Value, DestTimeZone) : DestArriveTimeUTC;
            }
            set
            {
                DestArriveTimeUTC = value.HasValue ? Core.DateHelper.ToUTC(value.Value, DestTimeZone) : value;
            }
        }

        [NotMapped, DisplayName("Depart Time"), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy hh:mm tt}")]
        public DateTime? DestDepartTime
        {
            get
            {
                return DestDepartTimeUTC.HasValue ? Core.DateHelper.ToLocal(DestDepartTimeUTC.Value, DestTimeZone) : DestDepartTimeUTC;
            }
            set
            {
                DestDepartTimeUTC = value.HasValue ? Core.DateHelper.ToUTC(value.Value, DestTimeZone) : value;
            }
        }

        [InverseProperty("Order")]
        public virtual ICollection<OrderReroute> OrderReroutes { get; set; }
        public OrderReroute FirstReroute 
        {
            get
            {
                try
                {
                    return OrderReroutes.OrderBy(or => or.RerouteDate).ElementAtOrDefault(0);
                }
                catch (Exception)
                {
                    return new OrderReroute();
                }
            }
        }

        [InverseProperty("Order")]
        public virtual OrderTransfer OrderTransfer { get; set; }
        [NotMapped]
        public bool IsTransfer
        {
            /* if a record exists in the transfer table, the order will have a non-null OrderTransfer  */
            get
            {
                return (OrderTransfer != null);
            }
        }

        [InverseProperty("Order")]
        public virtual OrderApprovalRO OrderApproval { get; set; }
        [NotMapped]
        public bool IsApproved
        {            
            get
            {
                if (OrderApproval != null)
                    return OrderApproval.Approved;
                else
                    return false;
            }
        }

        [InverseProperty("Order")]
        public ICollection<OrderTicket> OrderTickets { get; set; }

        // return the ActiveTicket specified by index (1-based)
        public OrderTicket ActiveTicket(int index)
        {
            return this.ActiveTickets.ElementAtOrDefault(index - 1);
        }
        public IEnumerable<OrderTicket> ActiveTickets
        {
            get
            {
                return this.OrderTickets.Where(t => t.Deleted == false).OrderBy(t => t.CreateDateUTC);
            }
        }

        public enum GaugerStatusEnum { Requested = 1, Dispatched = 2, InProgress = 3, Finalized = 4, Completed = 5, Skipped = 6 }
        static public string GetTicketTypeName(int ticketTypeID)
        {
            return ((TicketType.TYPE)Enum.ToObject(typeof(TicketType.TYPE), ticketTypeID)).ToString();
        }

        [ForeignKey("CarrierID")]
        public virtual Carrier Carrier { get; set; }

        [ForeignKey("ConsigneeID")]
        public virtual Consignee Consignee { get; set; }

        [ForeignKey("CustomerID")]
        public virtual Customer Customer { get; set; }

        [ForeignKey("DestinationID")]
        public virtual Destination Destination { get; set; }

        [ForeignKey("DriverID")]
        public virtual Driver Driver { get; set; }

        [ForeignKey("OperatorID")]
        public virtual Operator Operator { get; set; }

        [ForeignKey("ProducerID")]
        public virtual Producer Producer { get; set; }

        [ForeignKey("StatusID")]
        public virtual OrderStatus Status { get; set; }

        [ForeignKey("OriginID")]
        public virtual Origin Origin { get; set; }

        [ForeignKey("PriorityID")]
        public virtual Priority Priority { get; set; }

        [ForeignKey("PumperID")]
        public virtual Pumper Pumper { get; set; }

        [ForeignKey("RouteID")]
        public virtual Route Route { get; set; }

        public virtual TicketType TicketType { get; set; }

        [ForeignKey("TrailerID")]
        public virtual Trailer Trailer { get; set; }

        [ForeignKey("Trailer2ID")]
        public virtual Trailer Trailer2 { get; set; }

        [ForeignKey("TruckID")]
        public virtual Truck Truck { get; set; }

        [NotMapped]
        public bool Reassigned
        {
            get { return ReassignKey != null; }
        }

        [NotMapped]
        public bool PastDue
        {
            get
            {
                // order must be picked up from origin by due date
                return !OriginInfoRequired() && DueDate.Date < DateTime.UtcNow.Date;
            }
        }

        [NotMapped]
        public bool Settled
        {
            /* if a record exists in one of the settlement tables and has a batch ID, then it is considered settled/reconciled and is unauditable */
            get
            {
                return CarrierBatchNum != null || ShipperBatchNum != null || ProducerBatchNum != null || DriverBatchNum != null;
            }
        }

        [NotMapped]
        [DisplayName("Shipper Batch #")]
        public int? ShipperBatchNum
        {
            get {
                using (SSDB db = new SSDB())
                {
                    var batch = db.QuerySingleValue("SELECT BatchNum FROM tblShipperSettlementBatch WHERE ID IN (SELECT BatchID FROM tblOrderSettlementShipper WHERE OrderID = {0})", ID);
                    return (DBHelper.IsNull(batch) ? null : (int?) batch);
                }
            }
        }
        [NotMapped]
        [DisplayName("Carrier Batch #")]
        public int? CarrierBatchNum
        {
            get {
                using (SSDB db = new SSDB())
                {
                    var batch = db.QuerySingleValue("SELECT BatchNum FROM tblCarrierSettlementBatch WHERE ID IN (SELECT BatchID FROM tblOrderSettlementCarrier WHERE OrderID = {0})", ID);
                    return (DBHelper.IsNull(batch) ? null : (int?) batch);
                }
            }
        }
        [NotMapped]
        [DisplayName("Driver Batch #")]
        public int? DriverBatchNum
        {
            get {
                using (SSDB db = new SSDB())
                {
                    var batch = db.QuerySingleValue("SELECT BatchNum FROM tblDriverSettlementBatch WHERE ID IN (SELECT BatchID FROM tblOrderSettlementDriver WHERE OrderID = {0})", ID);
                    return (DBHelper.IsNull(batch) ? null : (int?) batch);
                }
            }
        }
        [NotMapped]
        [DisplayName("Producer Batch #")]
        public int? ProducerBatchNum
        {
            get {
                using (SSDB db = new SSDB())
                {
                    var batch = db.QuerySingleValue("SELECT BatchNum FROM tblProducerSettlementBatch WHERE ID IN (SELECT BatchID FROM tblOrderSettlementProducer WHERE OrderID = {0})", ID);
                    return (DBHelper.IsNull(batch) ? null : (int?) batch);
                }
            }
        }


        [NotMapped]
        public bool canEdit {
            // can only edit non-settled orders and then audited orders only if global setting is turned on
            get { return !Settled && (StatusID != (int) OrderStatus.STATUS.Audited || Settings.SettingsID.ALLOW_AUDIT_UPDATE.AsBool()); }
        }


        public bool DispatchConfirmNumRequired()
        {
            return DBHelper.ToBoolean(OrderRuleHelper.GetRuleValue(this.ID, OrderRuleHelper.Type.RequireShipperPO));
        }
        public bool RackBayRequired()
        {
            return DBHelper.ToBoolean(OrderRuleHelper.GetRuleValue(this.ID, OrderRuleHelper.Type.RackBayRequired));
        }
        public bool DestBOLRequired()
        {
            return DBHelper.ToBoolean(OrderRuleHelper.GetRuleValue(this.ID, OrderRuleHelper.Type.DestBOLRequired));
        }

        public bool OriginPhotoRequired()
        {
            return DBHelper.ToBoolean(OrderRuleHelper.GetRuleValue(this.ID, OrderRuleHelper.Type.OriginPhotoRequired));
        }
        public bool OriginPhoto2Required()
        {
            return DBHelper.ToBoolean(OrderRuleHelper.GetRuleValue(this.ID, OrderRuleHelper.Type.OriginPhoto2Required));
        }
        public bool OriginConditionPhotosRequired()
        {
            return DBHelper.ToBoolean(OrderRuleHelper.GetRuleValue(this.ID, OrderRuleHelper.Type.OriginConditionPhotosRequired));
        }
        public bool DestPhotoRequired()
        {
            return DBHelper.ToBoolean(OrderRuleHelper.GetRuleValue(this.ID, OrderRuleHelper.Type.DestPhotoRequired));
        }
        public bool DestPhoto2Required()
        {
            return DBHelper.ToBoolean(OrderRuleHelper.GetRuleValue(this.ID, OrderRuleHelper.Type.DestPhoto2Required));
        }
        public bool DestConditionPhotosRequired()
        {
            return DBHelper.ToBoolean(OrderRuleHelper.GetRuleValue(this.ID, OrderRuleHelper.Type.DestConditionPhotosRequired));
        }
        public bool RejectPhotoRequired()
        {
            return DBHelper.ToBoolean(OrderRuleHelper.GetRuleValue(this.ID, OrderRuleHelper.Type.RejectPhotoRequired));
        }

        public bool DestGOVWithinTolerance()
        {
            decimal tolerance = Decimal.Divide(DBHelper.ToInt32(OrderRuleHelper.GetRuleValue(this.ID, OrderRuleHelper.Type.Dest_GOV_Tolerance)), 100);
            if (tolerance == 0)
                return true;
            else
                return (   ((1 - tolerance) * this.OriginGrossUnits <= this.DestGrossUnits)
                         && ((this.DestGrossUnits <= (1 + tolerance) * this.OriginGrossUnits)));
        }

        public bool DestNSVWithinTolerance()
        {
            decimal tolerance = Decimal.Divide(DBHelper.ToInt32(OrderRuleHelper.GetRuleValue(this.ID, OrderRuleHelper.Type.Dest_NSV_Tolerance)), 100);
            if (tolerance == 0)
                return true;
            else
                return (   ((1 - tolerance) * this.OriginNetUnits <= this.DestNetUnits)
                        && ((this.DestNetUnits <= (1 + tolerance) * this.OriginNetUnits)));
        }

        public bool MileageRequired()
        {
            return Settings.SettingsID.RequireTruckMileage.AsBool();
        }

        public bool CalculateVM()
        {
            return DBHelper.ToBoolean(OrderRuleHelper.GetRuleValue(this.ID, OrderRuleHelper.Type.Calc_VM_Deliv_Vol));
        }

        public string GetOrderDateSource()
        {
            return OrderRuleHelper.GetRuleValue(this.ID, OrderRuleHelper.Type.OrderDateSource, "NOT SET").ToString();
        }

        public bool OriginInfoRequired()
        {
            // if Picked Up, Delivered, or Audited, origin details are required
            return this.StatusID == (int)OrderStatus.STATUS.PickedUp || this.StatusID == (int)OrderStatus.STATUS.Delivered || this.StatusID == (int)OrderStatus.STATUS.Audited;
        }

        public bool DestInfoRequired()
        {
            // if Delivered or Audited and not a Reject, destination details are required
            return (this.StatusID == (int)OrderStatus.STATUS.Delivered || this.StatusID == (int)OrderStatus.STATUS.Audited) && !this.Rejected;
        }

        // TODO: this needs to use Trailer Capacity
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!CalcOriginQtyValid(OriginGrossUnits))
                yield return new ValidationResult("Gross Volume is out of range");
            if (!CalcOriginQtyValid(OriginNetUnits))
                yield return new ValidationResult("Net Volume is out of range");
            if (DriverID.HasValue && !TruckID.HasValue)
                yield return new ValidationResult("Truck must be specified when a Driver is specified");
            if (DriverID.HasValue && !TrailerID.HasValue)
                yield return new ValidationResult("Trailer 1 must be specified when a Driver is specified");
            Validated = true;
        }

        static public void UpdateDriverDefaults(Order order, string userName)
        {
            // when the driver changes the truck and/order trailer, make the new values his respective default Truck/Trailer
            using (SSDB db = new SSDB())
            {
                db.ExecuteSql("UPDATE tblDriver SET TruckID={1} "
                        + "  , LastChangeDateUTC = getutcdate(), LastChangedByUser = {2}"
                        + " WHERE ID={0} AND TruckID <> {1}"
                    , order.DriverID, order.TruckID, DBHelper.QuoteStr(userName));
                db.ExecuteSql("UPDATE tblDriver SET TrailerID={1} "
                        + "  , LastChangeDateUTC = getutcdate(), LastChangedByUser = {2}"
                        + " WHERE ID={0} AND TrailerID <> {1}"
                    , order.DriverID, order.TrailerID, DBHelper.QuoteStr(userName));
                db.ExecuteSql("UPDATE tblDriver SET Trailer2ID={1} "
                        + "  , LastChangeDateUTC = getutcdate(), LastChangedByUser = {2}"
                        + " WHERE ID={0} AND isnull(Trailer2ID, 0) <> isnull({1}, 0)"
                    , order.DriverID
                    , order.Trailer2ID.HasValue ? (object)order.Trailer2ID.Value : "NULL"
                    , DBHelper.QuoteStr(userName));
                // update any other Dispatched orders for this same driver
                db.ExecuteSql("UPDATE tblOrder SET TruckID=D.TruckID, TrailerID=D.TrailerID, Trailer2ID=D.Trailer2ID"
                    + "  , LastChangeDateUTC = getutcdate(), LastChangedByUser = {3}"
                    + " FROM tblOrder O JOIN tblDriver D ON D.ID = O.DriverID"
                    + " WHERE O.ID<>{0} AND O.DriverID={1} AND O.StatusID IN ({2})"
                    + " AND (O.TruckID <> D.TruckID OR O.TrailerID <> D.TrailerID OR O.Trailer2ID <> D.Trailer2ID)"
                    , order.ID
                    , order.DriverID
                    , (int)OrderStatus.STATUS.Dispatched
                    , DBHelper.QuoteStr(userName));
            }
        }

        [NotMapped]
        public decimal? VarianceGOV
        {
            get
            {
                return DestGrossUnits - OriginGrossUnits;
            }
        }

        [NotMapped]
        public decimal? VarianceNSV
        {
            get
            {
                return DestNetUnits - OriginNetUnits;
            }
        }

        [NotMapped]
        public int? TripMiles
        {
            get
            {
                return DestTruckMileage - OriginTruckMileage;
            }
        }
    }
}
