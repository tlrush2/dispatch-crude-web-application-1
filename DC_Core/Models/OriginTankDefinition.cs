using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using System.Linq;

namespace DispatchCrude.Models
{
    [Table("tblOriginTankDefinition")]
    public class OriginTankDefinition : AuditModelDeleteBase, IValidatableObject
    {
        public OriginTankDefinition()
        {            
        }

        [Key]
        public int ID { get; set; }

        [Required]
        [StringLength(20)]
        [DisplayName("Definition")]
        public string Name { get; set; }
                
        [DisplayName("Bottom Feet")]
        public byte BottomFeet { get; set; }
                
        [DisplayName("Bottom Inches")]
        public byte BottomInches { get; set; }
                
        [DisplayName("Bottom Q")]
        public byte BottomQ { get; set; }
        
        [Required]
        [DisplayName("Top Feet")]
        public byte TopFeet { get; set; }

        [Required]
        [DisplayName("Top Inches")]
        public byte TopInches { get; set; }

        [Required]
        [DisplayName("Top Q")]
        public byte TopQ { get; set; }
        
        [Required]
        [DisplayName("BPQ")]
        public decimal TopBPQ { get; set; }

        [NotMapped]
        public int CombinedBottomStrap //quarter inches
        {
            get
            {
                return BottomFeet * 48 + BottomInches * 4 + BottomQ;
            }
        }

        [NotMapped]
        public int CombinedTopStrap  //quarter inches
        {
            get
            {
                return TopFeet * 48 + TopInches * 4 + TopQ;
            }
        }



        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!ValidateDuplicateName(ID, Name))
                yield return new ValidationResult("Definition name is already in use");
            if (!ValidateBPQ(TopBPQ))
                yield return new ValidationResult("Invalid BPQ");
            if (!ValidateTopStrap(TopFeet, TopInches, TopQ))
                yield return new ValidationResult("Top must be higher than bottom strap");
            Validated = true;
        }

        private bool ValidateTopStrap(int topFeet, int topInch, int topQ)
        {
            if (CombinedTopStrap <= CombinedBottomStrap)
                return false;

            return true;
        }

        private bool ValidateBPQ(decimal bpq)
        {
            return (!(bpq > 0.0m)) ? false : true;
        }

        private bool ValidateDuplicateName(int id, string name)
        {
            using (DispatchCrudeDB db = new DispatchCrudeDB())
            {
                return db.OriginTankDefinitions.Where(m => m.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase) && (id == 0 || m.ID != id)).Count() == 0;
            }
        }

    }
}
