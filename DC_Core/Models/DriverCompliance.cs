using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using System.Web.Script.Serialization;
using System.Linq;
using DispatchCrude.Extensions;
using System.Collections.Generic;

namespace DispatchCrude.Models
{
    [Table("tblDriverCompliance")]
    public class DriverCompliance : AuditModelDeleteBase, IValidatableObject
    {
        [Key]
        public int ID { get; set; }

        [RequiredGreaterThanZero] 
        [DisplayName("Driver")]
        [UIHint("_ForeignKeyDDL")]
        public int DriverID { get; set; }

        [DisplayName("Driver")]
        [ForeignKey("DriverID")]
        public Driver Driver { get; set; }

        [RequiredGreaterThanZero] 
        [DisplayName("Type")]
        [UIHint("_ForeignKeyDDL")]        
        public int ComplianceTypeID { get; set; }

        [DisplayName("Type")]
        [ForeignKey("ComplianceTypeID")]
        public virtual DriverComplianceType ComplianceType { get; set; }

        [DisplayName("Document")]
        [ScriptIgnore]        
        public byte[] Document { get; set; }
        [DisplayName("Document Name")]
        public string DocumentName { get; set; }


        [DisplayName("Effective Date")]
        [UIHint("Date")]
        public DateTime? DocumentDate { get; set; }

        [DisplayName("Expiration Date")]
        [UIHint("Date")]
        public DateTime? ExpirationDate { get; set; }

        public string Notes { get; set; }


        /* These four fields are only relevant if the category is License */

        [StringLength(20), DisplayName("License #")]
        public string DocumentNumber { get; set; }

        [DisplayName("License state")]
        [UIHint("_ForeignKeyDDL")]
        public int? DocumentStateID { get; set; }

        [ForeignKey("DocumentStateID")]
        public State DocumentState { get; set; }


        [StringLength(255), DisplayName("Endorsements")]
        public string Endorsements { get; set; }

        [StringLength(255), DisplayName("Restrictions")]
        public string Restrictions { get; set; }



        /// <summary>
        /// Returns boolean based upon the expiration date for a record
        /// </summary>
        [NotMapped]
        public bool Expired
        {
            get { return ExpirationDate < DateTime.Today; }
        }

        /// <summary>
        /// Returns boolean based upon the existence of a document
        /// </summary>
        [NotMapped]
        public bool MissingDocumentation
        {
            get { return Document == null || Document.Length == 0; }
        }

        /// <summary>
        /// Returns boolean based upon the compliance record type criteria: Expires?, Expired?, and Requires Document?
        /// </summary>
        [NotMapped]
        public bool IsCompliant
        {
            get
            {
                //NOTE: Cannot check here for whether or not the record exists so that 
                //check must be done at the location IsCompliant is being checked

                DispatchCrudeDB db = new DispatchCrudeDB();
                DriverComplianceType type = db.DriverComplianceTypes.Find(ComplianceTypeID);               

                if (type != null)
                {
                    //If the document can Expire AND has expired - error
                    if (type.Expires && Expired == true)                    
                        return false;

                    //If a document is required AND does not exist - error
                    if (type.RequiresDocument && Document == null)
                        return false;                    
                }                

                return true;
            }
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var ret = new List<ValidationResult>();

            using (DispatchCrudeDB db = new DispatchCrudeDB())
            {
                // If the effective date is not selected...error
                if (this.DocumentDate == null)
                    ret.Add(new ValidationResult("The Effective Date field is required.", new string[] { "DocumentDate" }));

                // Get compliance type record
                var driverComplianceType = db.DriverComplianceTypes.Find(this.ComplianceTypeID);

                // ensure a valid ComplianceTypeID value was specified
                if (driverComplianceType == null)
                    ret.Add(new ValidationResult("Compliance Type value is required", new string[] { "ComplianceTypeID" }));
                else
                {
                    // If the record is new and requires a document and the document is missing...error
                    if (this.ID == 0 && driverComplianceType.RequiresDocument && this.Document == null)
                        ret.Add(new ValidationResult(string.Format("The Document is required for '{0}' record type.", driverComplianceType.Name), new string[] { "Document" }));

                    // If the record type can expire and no expiration date has been given...error
                    if (driverComplianceType.Expires && this.ExpirationDate == null)
                        ret.Add(new ValidationResult(string.Format("The Expiration Date field is required for '{0}' record type.", driverComplianceType.Name), new string[] { "ExpirationDate" }));
                }

                // Check that the Drivers License is not in use
                if (ComplianceTypeID == (int) DriverComplianceType.TYPES.CDL)
                {
                    if (db.DriverCompliances.Where(x => x.DeleteDateUTC == null && x.ComplianceTypeID == ComplianceTypeID
                                    && x.DocumentNumber == DocumentNumber && x.DocumentStateID == DocumentStateID
                                    && x.ID != ID).Any())
                        ret.Add(new ValidationResult(string.Format("That Driver License is already in use."), new string[] { "DocumentNumber" }));
                }
            }
            Validated = true;
            return ret;
        }

    }
}
