﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using AlonsIT;

namespace DispatchCrude.Models
{
    [Table("tblDriverScheduleAssignment")]
    public class DriverScheduleAssignment : AuditModelLastChangeBase
    {
        public DriverScheduleAssignment()
        {
        }

        [Key]
        public Int16 ID { get; set; }

        public string DriverID { get; set; }

        public DateTime DriverShiftStartDate { get; set; }

        static public void UpdateDriverShiftStartDate(int driverID, DateTime shiftStartDate, string userName)
        {
            using (SSDB db = new SSDB())
            {
                using (SqlCommand uCmd = db.BuildCommand("spUpdateDriverShiftStartDate"))
                {
                    uCmd.Parameters["@DriverID"].Value = driverID;
                    uCmd.Parameters["@ShiftStartDate"].Value = shiftStartDate;
                    uCmd.Parameters["@UserName"].Value = userName;
                    uCmd.ExecuteNonQuery();
                }
            }
        }
        static public void UpdateDriverShiftType(int driverID, int shiftTypeID, string userName)
        {
            using (SSDB db = new SSDB())
            {
                using (SqlCommand uCmd = db.BuildCommand("spUpdateDriverShiftType"))
                {
                    uCmd.Parameters["@DriverID"].Value = driverID;
                    uCmd.Parameters["@ShiftTypeID"].Value = shiftTypeID;
                    uCmd.Parameters["@UserName"].Value = userName;
                    uCmd.ExecuteNonQuery();
                }
            }
        }
    }
}
