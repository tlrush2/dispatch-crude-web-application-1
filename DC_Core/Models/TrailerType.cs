using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DispatchCrude.Models
{
    [Table("tblTrailerType")]
    public class TrailerType : AuditModelDeleteBase, IValidatableObject
    {
        public TrailerType()
        {
            this.Trailers = new List<Trailer>();
        }

        [Key]
        public int ID { get; set; }

        [Required, DisplayName("Trailer Type"), StringLength(15)]
        public string Name { get; set; }

        [Required, DisplayName("Cert Required?")]
        public bool CertRequired { get; set; }

        public /*virtual*/ ICollection<Trailer> Trailers { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!ValidateDuplicateName(ID, Name))
                yield return new ValidationResult("Trailer Type is already in use");
            Validated = true;
        }

        private bool ValidateDuplicateName(int id, string name)
        {
            using (DispatchCrudeDB db = new DispatchCrudeDB())
            {
                return db.TrailerTypes.Where(m => m.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase) && (id == 0 || m.ID != id)).Count() == 0;
            }
        }
    }
}
