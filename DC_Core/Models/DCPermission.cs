﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace DispatchCrude.Models
{
    [Table("aspnet_Roles")]  // aspnet roles are now being referred to as permissions DCWEB-1420
    public class DCPermission: IEDTO
    {
        public DCPermission()
        {
            this.RoleId = Guid.NewGuid();
            this.Groups = new List<DCGroup>();
        }

        [Key]
        public Guid RoleId { get; set; }

        [Required]                
        public string RoleName { get; set; }

        [Required]
        public string LoweredRoleName { get; set; }
                        
        public string Description { get; set; }

        public string Category { get; set; }

        public string FriendlyName { get; set; }

        public ICollection<DCGroup> Groups { get; set; }
    }
}
