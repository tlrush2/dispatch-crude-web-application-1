using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace DispatchCrude.Models
{
    [Table("tblGauger")]
    public class Gauger : AuditModelDeleteBase
    {
        public Gauger() { }

        [Key]
        public int ID { get; set; }

        [Required, StringLength(20)]
        public string FirstName { get; set; }
        [Required, StringLength(20)]
        public string LastName { get; set; }

        [DisplayName("Operating Region")]
        public int? RegionID { get; set; }

        [DisplayName("Home State")]
        public int? HomeStateID { get; set; }
        [ForeignKey("HomeStateID")]
        public virtual State HomeState { get; set; }

        [Required, StringLength(20), DisplayName("ID #")]
        public string IDNumber { get; set; }

        [Required, DefaultValue(true)]
        public bool MobileApp { get; set; }

        [Required, DefaultValue(true)]
        public bool MobilePrint { get; set; }

        [StringLength(20)]
        public string MobilePhone { get; set; }

        [StringLength(30)]
        public string MobileProvider { get; set; }

        [StringLength(75)]
        public string Email { get; set; }

        [NotMapped]
        public string FullName
        {
            get { return FirstName + " " + LastName; }
        }
    }
}
