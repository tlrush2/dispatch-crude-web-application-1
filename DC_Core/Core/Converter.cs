﻿using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using AlonsIT;

namespace DispatchCrude.Core
{
	static public class Converter
	{
		static public bool IsNull(object val)
		{
			return val == DBNull.Value || val == null;
		}
        static public bool IsDate(object val)
        {
            DateTime dt;
            return IsNull(val) ? false : DateTime.TryParse(val.ToString(), out dt);
        }
        static public object ToObjectWithDataType(object value, Type dataType)
        {
            object ret = value;

            if (dataType == null)
                dataType = typeof(string);

            if (dataType == typeof(byte)
                || dataType == typeof(Int16)
                || dataType == typeof(Int32)
                || dataType == typeof(Int64)
                || dataType == typeof(UInt16)
                || dataType == typeof(UInt32)
                || dataType == typeof(UInt64)
                || dataType == typeof(ushort)
                || dataType == typeof(short)
                || dataType == typeof(decimal)
                || dataType == typeof(double)
                || dataType == typeof(float))
            {
                ret = DBHelper.ToDoubleSafe(value ?? 0);
            }
            else if (dataType == typeof(DateTime) || dataType == typeof(DateTime?))
            {
                DateTime? date = Converter.ToNullableDateTime(value);
                if (date.HasValue && date != new DateTime())
                    ret = date.Value;
            }
            else if (dataType == typeof(bool))
            {
                ret = DBHelper.ToBoolean(value);
            }
            else
            {
                ret = (value ?? "").ToString();
            }
            return ret;
        }

        static public bool IsNullOrEmpty(object value)
        {
            return IsNull(value) || string.IsNullOrEmpty(value.ToString()) || value.ToString().Equals("&nbsp;", StringComparison.CurrentCultureIgnoreCase);
        }

        static public string ToNullString(object value)
        {
            return IsNullOrEmpty(value.ToString()) ? null : value.ToString();
        }
        static public object ToDBNullFromEmpty(object value)
        {
            return IsNullOrEmpty(value) ? DBNull.Value : value;
        }

		static public bool ToBoolean(object val, bool defValue = false)
		{
            try
            {
                return IsNull(val) || string.IsNullOrWhiteSpace(val.ToString()) ? defValue : Convert.ToBoolean(val ?? defValue);
            }
            catch (Exception)
            {
                return defValue;
            }
		}
        static public bool? ToNullableBool(object val, bool? defValue = new bool?())
        {
            if (!IsNullOrEmpty(val))
                return ToBoolean(val);
            return defValue;
        }

		static public Int32 ToInt32(object val, int defValue = 0)
		{
            val = StripNonNumeric(val);
			return IsNull(val) ? defValue : (val.ToString() == string.Empty ? defValue : Convert.ToInt32(val ?? defValue));
		}
		static public Int16 ToInt16(object val, Int16 defValue = 0)
		{
            val = StripNonNumeric(val);
            return IsNull(val) ? defValue : (val.ToString() == string.Empty ? defValue : Convert.ToInt16(val ?? defValue));
        }
		static public DateTime ToDateTime(object val, DateTime defValue = new DateTime(), DateTimeKind kind = DateTimeKind.Local)
		{
            DateTime ret = DBHelper.ToDateTime(val, defValue);
            return DateTime.SpecifyKind(ret, kind);
		}
        static public DateTime? ToNullableDateTime(object val, DateTime? defValue = new DateTime?())
        {
            try { return Convert.ToDateTime(val); }
            catch { return defValue; }
        }
        static public object StripNonNumeric(object val)
        {
            if (val is string)
                return (val ?? "").ToString().Replace("$", "").Replace("%", "").Replace(",", "");
            else
                return val;
        }

        static public Decimal ToDecimal(object val, decimal defValue = 0)
        {
            val = StripNonNumeric(val);
            return IsNull(val) ? defValue : (val.ToString() == string.Empty ? defValue : Convert.ToDecimal(val ?? defValue));
        }
        static public double ToDouble(object val, double defValue = 0)
        {
            val = StripNonNumeric(val);
            return IsNull(val) ? defValue : (val.ToString() == string.Empty ? defValue : Convert.ToDouble(val ?? defValue));
        }

        static public string SplitCamelCase(string s)
        {
            return Regex.Replace(s
                , "(?<!^)([A-Z][a-z]|(?<=[a-z])[A-Z])"
                , " $1"
                , RegexOptions.Compiled).Trim();
        }

        public static string CleanFileName(string fileName)
        {
            return Path.GetInvalidFileNameChars().Aggregate(fileName, (current, c) => current.Replace(c.ToString(), string.Empty));
        }

        public static string IfNullOrEmpty(object val, string defValue)
        {
            if (val == null)
                val = "";
            return string.IsNullOrEmpty(val.ToString()) ? defValue : val.ToString();
        }

        //IfNotNullOrEmpty created for Routes page link logic.  Needed the opposite of IfNullOrEmpty
        public static string IfNotNullOrEmpty(object val, string defValue)
        {
            if (val == null)
                val = "";
            return string.IsNullOrEmpty(val.ToString()) ? val.ToString() : defValue;
        }

        //Return GPS coordinates as string if neither Lat or Lon value is missing
        public static string IfGPSNullOrEmpty(object lat, object lon, string separator)
        {            
            if (lat.ToString() == "" || lon.ToString() == "")
            {                 
                return "";
            }
            else
            {
                return lat.ToString() + separator + lon.ToString();
            }            
        }
    }
}