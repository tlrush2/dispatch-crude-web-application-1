﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using DispatchCrude.Extensions;

namespace DispatchCrude.Core
{
    public class JsonPropListExcludeContractResolver : DefaultContractResolver
    {
        private readonly string[] _excludeProps = null;
 
        public JsonPropListExcludeContractResolver(string[] excludeProps)
        {
            _excludeProps = excludeProps;
        }
 
        protected override IList<JsonProperty> CreateProperties(Type type, MemberSerialization memberSerialization)
        {
            IList<JsonProperty> properties = base.CreateProperties(type, memberSerialization);

            // only serializer properties that start with the specified character
            properties = properties.Where(p => !p.PropertyName.IsIn(_excludeProps)).ToList();

            return properties;
        }
    }
}