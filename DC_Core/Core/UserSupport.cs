﻿using System.Web;
using AlonsIT;

namespace DispatchCrude.Core
{
    static public class UserSupport
    {
        static public string UserName { get { return HttpContext.Current.User.Identity.Name; } }

        //static public bool IsAdmin(//) //Code using this needs to be replaced.  Edited this to make due temporarily
        //{
        //    return IsInGroup("_DCAdministrator") || IsInGroup("_DCSystemManager") || IsInGroup("_DCSupport");
        //}
        
        // WARNING:  THIS WILL HAVE TO BE REVISED WITH THE NEW USERS/GROUPS/PERMISSIONS CHANGES
        //           THIS WAY OF DECIDING IF A USER HAS PERMISSION WILL BREAK IF THE PAGE TITLE CHANGES
        //           OR IF ROLE NAMES DO NOT MATCH THE TEMPLATE USED BELOW.
        //           I WILL LOOK INTO A LESS FRAGILE WAY TO DO THIS -BEN 6/17/16
        // KEVIN NOTE: this will not break if the pageName changes, because it is a parameter - but maybe "pageName" is not the best name for the parameter - maybe "BaseRole" would be better??)
        //      it is written this way to take advantage of "naming convention" - to reduce the amount of manual coding
        public enum Allow { Create = 1, Edit = 2, Delete = 3 }
        static public bool OnPage(this Allow roleType, string pageName)
        {
            // the default is first character is capitalized so ensure it is set that way here
            pageName = char.ToUpper(pageName[0]) + pageName.Substring(1).ToLower();
            string[] roleNames = { string.Empty, string.Empty };
            switch (pageName)
            {
                case "Special": // handle exceptional role names here
                    roleNames[0] = "special value";
                    break;
                default:
                    if (roleType == Allow.Delete)
                    {
                        roleNames[0] = "deactivate" + pageName;
                        roleNames[1] = "delete" + pageName;
                    }
                    else // default behavior
                        roleNames[0] = roleType.ToString().ToLower() + pageName;
                    break;
            }
            return IsInRoles(roleNames);
        }

        static public bool IsInRole(string roleName)
        {
            return HttpContext.Current != null && HttpContext.Current.User != null && HttpContext.Current.User.IsInRole(roleName);
        }

        static public bool IsInRoles(string[] roleNames)
        {
            if (HttpContext.Current != null && HttpContext.Current.User != null)
            {
                foreach (string roleName in roleNames)
                {
                    if (HttpContext.Current.User.IsInRole(roleName))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        // This is primarily for use on the Groups page to prevent edit/deactivate of the default groups based on the user's group memeberhsip
        static public bool IsInGroup(string groupName)
        {
            if (HttpContext.Current != null && HttpContext.Current.User != null)
            {
                return IsInGroup(groupName, UserName);
            }

            return false;
        }

        // Check to see if a specific user is in the specified group
        static public bool IsInGroup(string groupName, string userName)
        {
            if (!DBHelper.IsNull(UserName))
            {
                return SSDB.QuerySingleValueStatic("SELECT 1 FROM vw_aspnet_UsersInGroups WHERE UserName={0} AND GroupName={1}", DBHelper.QuoteStr(userName), DBHelper.QuoteStr(groupName)) != null;
            }

            return false;
        }
    }
}