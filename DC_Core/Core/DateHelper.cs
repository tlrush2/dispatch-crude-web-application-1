﻿using System;
using System.Web;
using System.Data;
using System.Collections;
using DispatchCrude.Extensions;
using AlonsIT;

namespace DispatchCrude.Core
{
    public class DateHelper
    {
        public const string DEFAULT_TIME_ZONE = "Central Standard Time";


        static public DateTime? Combine(DateTime? date, DateTime? time)
        {
            if (!date.HasValue || !time.HasValue) // if both date and time elements are available, then return only the date (even if no value available)
                return date;
            else
                return Combine(date.Value, time.Value);
        }
        static public object CombineToNull(DateTime? date, DateTime? time)
        {
            return HandleNull(Combine(date, time));
        }

        static public DateTime StartOfWeek(DayOfWeek dayOfWeek = DayOfWeek.Sunday)
        {
            return StartOfWeek(DateTime.Now.Date, dayOfWeek);
        }
        static public DateTime StartOfWeek(DateTime date, DayOfWeek firstDayOfWeek = DayOfWeek.Sunday)
        {
            DateTime ret = date;
            while (ret.DayOfWeek != firstDayOfWeek)
            {
                ret = ret.AddDays(-1);
            }
            return ret;
        }
        static public DateTime StartOfMonth(DateTime date)
        {
            return new DateTime(date.Year, date.Month, 1);
        }
        static public DateTime EndOfMonth(DateTime date)
        {
            return new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month));
        }
        static public DateTime StartOfQuarter()
        {
            return StartOfQuarter(DateTime.Now.Date);
        }
        static public DateTime StartOfQuarter(DateTime date)
        {
            return new DateTime(date.Year, (date.Month + 2 % 3) - 1 * 3 + 1, 1);
        }

        static public DateTime Combine(DateTime date, DateTime time)
        {
            return DateTime.Parse(string.Format("{0:M/d/yyyy} {1:HH:mm}", date, time));
        }

        static public object HandleNull(DateTime? datetime)
        {
            return datetime.HasValue ? datetime.Value as object : DBNull.Value;
        }

        static public DateTime ToUTC(DateTime local, HttpContext context)
        {
            local = DateTime.SpecifyKind(local, DateTimeKind.Unspecified);
            return TimeZoneInfo.ConvertTime(local, ContextTimeZone(context), TimeZoneInfo.Utc);
        }

        static public DateTime ToUTC(DateTime local, string timeZoneName = DEFAULT_TIME_ZONE)
        {
            local = DateTime.SpecifyKind(local, DateTimeKind.Unspecified);
            TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById(timeZoneName);
            return TimeZoneInfo.ConvertTime(local, tzi, TimeZoneInfo.Utc);
        }

        static public DateTime ToLocal(DateTime utc, HttpContext context)
        {
            utc = DateTime.SpecifyKind(utc, DateTimeKind.Utc);
            return TimeZoneInfo.ConvertTime(utc, TimeZoneInfo.Utc, ContextTimeZone(context));
        }
        static public DateTime ToLocal(DateTime utc, string timeZoneName = DEFAULT_TIME_ZONE)
        {
            utc = DateTime.SpecifyKind(utc, DateTimeKind.Utc);
            TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById(timeZoneName);
            return TimeZoneInfo.ConvertTime(utc, TimeZoneInfo.Utc, tzi);
        }

        static public string ContextTimeZoneName(HttpContext ctx)
        {
            return ctx == null ? DEFAULT_TIME_ZONE : DispatchCrudeHelper.GetProfileTimeZone(ctx);
        }
        static public TimeZoneInfo ContextTimeZone(HttpContext ctx)
        {
            try
            {
                return TimeZoneInfo.FindSystemTimeZoneById(ContextTimeZoneName(ctx));
            }
            catch (Exception)
            {
                return TimeZoneInfo.FindSystemTimeZoneById(DEFAULT_TIME_ZONE);
            }
        }

        static public DataTable AddLocalRowStateDateFields(DataTable data, HttpContext context = null, params string[] limitedFieldList)
        {
            Hashtable utcColumns = new Hashtable();
            // find any UTC date columns and add a equivalent local DateTime field (named with UTC stripped from the UTC equivalent)
            foreach (DataColumn col in data.Columns)
            {
                if (col.DataType == typeof(DateTime)
                    && col.ColumnName.EndsWith("UTC")
                    // user can optionally specify a list of fields (only) to process
                    && (limitedFieldList.Length == 0 || col.ColumnName.IsIn(limitedFieldList)))
                {
                    string localColName = col.ColumnName.Replace("UTC", "");
                    if (!data.Columns.Contains(localColName))
                    {
                        utcColumns.Add(col.ColumnName, localColName);
                    }
                }
            }
            // add these local date columns to the data table
            foreach (object key in utcColumns.Keys)
            {
                // insert it into the same position as the current UTC column
                data.Columns.Add(utcColumns[key].ToString(), typeof(DateTime)).SetOrdinal(data.Columns[key.ToString()].Ordinal);
            }
            // iterate over the data and compute and add the local values
            TimeZoneInfo tzi = ContextTimeZone(context);
            foreach (DataRow row in data.Rows)
            {
                row.BeginEdit();
                foreach (object key in utcColumns.Keys)
                {
                    if (!DBHelper.IsNull(row[key.ToString()]))
                        row[utcColumns[key].ToString()] = TimeZoneInfo.ConvertTimeFromUtc(Converter.ToDateTime(row[key.ToString()], kind: DateTimeKind.Utc), tzi);
                }
                row.AcceptChanges();
            }
            return data;
        }

        public static string FunDate(DateTime date)
        {
            if (date.Date == DateTime.Today) return "Today";
            if (date.Date == DateTime.Today.AddDays(-1)) return "Yesterday";
            if (date.Date == DateTime.Today.AddDays(1)) return "Tomorrow";
            return date.ToString("M/d");
        }

        public static string TimeFromMinutes(int minutes)
        {
            TimeSpan ts = TimeSpan.FromMinutes(minutes);
            return string.Format("{0:0}:{1:00}", (int)ts.TotalHours, ts.Minutes); // return 70 min as 1:10
        }

        public static string FormatDuration(decimal hrs)
        {
            int minutes = (int)Math.Round(hrs * 60.0m);

            if (Settings.SettingsID.DurationFormat.AsString() == "As Decimal")
                return hrs.ToString("0.00");
            // else if ...
            //  h hr, m min
            else
                return TimeFromMinutes(minutes);
        }

        public static string FormatDuration(double hrs)
        {
            return FormatDuration(Convert.ToDecimal(hrs));
        }

        /// <summary>
        /// Takes an object value of minutes and returns the duration in H:MM format or 0.00 Format based upon the system setting "Time Duration Format"
        /// </summary>
        /// <param name="minutes"></param>
        /// <returns></returns>
        public static string FormatDurationMinutes(object minutes)
        {
            //This function was created to accommodate the restrictions of the webforms pages "truck order create" and "Dispatch Grid"
            //Once those pages have been rewritten in MVC this may no longer be needed.  If so, delete this at that point
            decimal hours = 0;

            try
            {
                var intMins = Convert.ToInt32(minutes);

                if (intMins > 0)
                    hours = Convert.ToDecimal(intMins) / 60;

                return FormatDuration(hours);
            }
            catch
            {
                return FormatDuration(hours);
            }
        }

        public static double TimeAsDecimal(DateTime d, int decimals = 2)
        {
            return d.Hour + Math.Round(d.Minute/60.0, decimals);
        }

    };
}