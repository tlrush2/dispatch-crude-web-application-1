﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AlonsIT;

namespace DispatchCrude.Core
{
    public class OrderRuleHelper
    {
        public enum Type { PrintDeliveryAtPickup = 1, RequireShipperPO = 2, Dest_GOV_Tolerance = 3, Dest_NSV_Tolerance = 4, Calc_VM_Deliv_Vol = 5
            , Max_Order_Gallons = 6, TicketTypeChangeAllowed = 7, OrderDateSource = 8, RejectPhotoRequired = 9, OriginPhotoRequired = 10
            , DestPhotoRequired = 11, Max_Order_Pounds = 12, DestBOLRequired = 13, MeterRunTankRequired = 14, RackBayRequired = 15
            , MeterRunTempsRequired = 17, OriginConditionPhotosRequired = 18, DestConditionPhotosRequired = 19, RejectInfoRequired = 20
                , OriginPhoto2Required = 21, DestPhoto2Required = 22, OriginBOLRequired = 24
        }

        static public object GetRuleValue(DateTime effDate, Type type, int? shipperID, int? carrierID, int productID, int originID, int destinationID, object defaultValue = null)
        {
            using (SSDB ssdb = new SSDB())
            {
                return ssdb.QuerySingleValue(
                    "SELECT R.Value "
                    + "FROM tblOrigin O "
                    + "JOIN tblDestination D ON D.ID = {6} "
                    + "JOIN tblProduct P ON P.ID = {4} "
                    + "CROSS APPLY dbo.fnOrderRules('{0:M/d/yyyy}', NULL, {1}, {2}, {3}, {4}, {5}, {6}, O.StateID, D.StateID, O.RegionID, O.ProducerID, 1) R "
                    + "WHERE O.ID = {5}"
/* 0 */             , effDate as object
/* 1 */             , (int)type
/* 2 */             , shipperID.HasValue ? shipperID.Value.ToString() : "NULL"
/* 3 */             , carrierID.HasValue ? carrierID.Value.ToString() : "NULL"
/* 4 */             , productID
/* 5 */             , originID
/* 6 */             , destinationID) ?? defaultValue;
            }
        }
        static public object GetRuleValue(int orderID, Type type, object defaultValue = null)
        {
            using (SSDB ssdb = new SSDB())
            {
                return ssdb.QuerySingleValue("SELECT Value FROM dbo.fnOrderOrderRules({0}) WHERE TypeID={1}", orderID, (int)type) ?? defaultValue;
            }
        }
    }
}