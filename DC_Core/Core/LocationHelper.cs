﻿using Newtonsoft.Json;
using System.Net;
using System;

namespace DispatchCrude.Core
{

    public class LocationHelper
    {
        public static decimal GEOGRAPHIC_CENTER_USA_LAT = 39.828168m;
        public static decimal GEOGRAPHIC_CENTER_USA_LON = -98.5795980m;

        public static double? getMiles(decimal? lat1, decimal? lon1, decimal? lat2, decimal? lon2)
        {
            if (lat1 == null || lon1 == null || lat2 == null || lon2 == null)
                return null;
            return distance((double)lat1, (double)lon1, (double)lat2, (double)lon2, 'M');
        }

        // ****************************************************************************
        // https://www.geodatasource.com/developers/c-sharp
        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        //:::                                                                         :::
        //:::  This routine calculates the distance between two points (given the     :::
        //:::  latitude/longitude of those points). It is being used to calculate     :::
        //:::  the distance between two locations using GeoDataSource(TM) products    :::
        //:::                                                                         :::
        //:::  Definitions:                                                           :::
        //:::    South latitudes are negative, east longitudes are positive           :::
        //:::                                                                         :::
        //:::  Passed to function:                                                    :::
        //:::    lat1, lon1 = Latitude and Longitude of point 1 (in decimal degrees)  :::
        //:::    lat2, lon2 = Latitude and Longitude of point 2 (in decimal degrees)  :::
        //:::    unit = the unit you desire for results                               :::
        //:::           where: 'M' is statute miles (default)                         :::
        //:::                  'K' is kilometers                                      :::
        //:::                  'N' is nautical miles                                  :::
        //:::                                                                         :::
        //:::  Worldwide cities and other features databases with latitude longitude  :::
        //:::  are available at http://www.geodatasource.com                          :::
        //:::                                                                         :::
        //:::  For enquiries, please contact sales@geodatasource.com                  :::
        //:::                                                                         :::
        //:::  Official Web site: http://www.geodatasource.com                        :::
        //:::                                                                         :::
        //:::           GeoDataSource.com (C) All Rights Reserved 2015                :::
        //:::                                                                         :::
        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        private static double distance(double lat1, double lon1, double lat2, double lon2, char unit)
        {
            double theta = lon1 - lon2;
            double dist = Math.Sin(lat1 * Math.PI / 180.0) * Math.Sin(lat2 * Math.PI / 180.0) + Math.Cos(lat1 * Math.PI / 180.0) * Math.Cos(lat2 * Math.PI / 180.0) * Math.Cos(theta * Math.PI / 180.0);
            dist = Math.Acos(dist);
            dist = dist / Math.PI * 180.0;
            dist = dist * 60 * 1.1515;
            if (unit == 'K')
            {
                dist = dist * 1.609344;
            }
            else if (unit == 'N')
            {
                dist = dist * 0.8684;
            }
            return (dist);
        }



        /**** The following sections are dependent on Google Maps APIs ****/
        // FOR FUTURE CONSIDERATION https://developers.google.com/maps/documentation/geocoding/usage-limits

        /**************************************************************************************************/
        /** DESCRIPTION:  Use Google Maps Geocode API to determine location (for getting town nearest    **/
        /**     to GPS point).                                                                           **/
        /**     See https://maps.googleapis.com/maps/api/geocode/json?latlng=37.961486,-87.515489         **/
        /**************************************************************************************************/
        public static GoogleLocation getLocation(decimal lat, decimal lon)
        {
            GoogleLocation location = new GoogleLocation();

            try
            {
                using (WebClient wc = new WebClient())
                {

                    string json = wc.DownloadString("https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + "," + lon);
                    dynamic v = JsonConvert.DeserializeObject(json);

                    if (v["status"] == "OK")
                    {
                        // Google found location, loop through address components and update location model
                        var results = v["results"][0]["address_components"];
                        foreach (var address_components in results)
                        {
                            switch ((string)address_components["types"][0])
                            {
                                case "street_number":
                                    location.StreetNumber = address_components["long_name"].ToString();
                                    break;

                                case "route":
                                    location.Street = address_components["long_name"].ToString();
                                    location.StreetAbbrev = address_components["short_name"].ToString();
                                    break;

                                case "locality":
                                    location.City = address_components["long_name"].ToString();
                                    break;

                                case "administrative_area_level_2":
                                    location.County = address_components["long_name"].ToString();
                                    break;

                                case "administrative_area_level_1":
                                    location.State = address_components["long_name"].ToString();
                                    location.StateAbbrev = address_components["short_name"].ToString();
                                    break;

                                case "country":
                                    location.Country = address_components["long_name"].ToString();
                                    location.CountryAbbrev = address_components["short_name"].ToString();
                                    break;

                                case "postal_code":
                                    location.Zip = address_components["long_name"].ToString();
                                    break;

                                case "postal_code_suffix":
                                    location.ZipPlus = address_components["long_name"].ToString();
                                    break;
                            }
                        }
                    }
                }
            }
            catch { }

            return location;
        }
    }

    // Class to store Google Geocode information
    public class GoogleLocation
    {
        public string StreetNumber { get; set; }
        public string Street { get; set; }
        public string StreetAbbrev { get; set; }
        public string City { get; set; }
        public string County { get; set; }
        public string State { get; set; }
        public string StateAbbrev { get; set; }
        public string Country { get; set; }
        public string CountryAbbrev { get; set; }
        public string Zip { get; set; }
        public string ZipPlus { get; set; }

        public string StreetAddress { get { return (string.IsNullOrEmpty(StreetNumber)) ? StreetAbbrev : (StreetNumber + " " + StreetAbbrev); } }
        public string ZipFull { get { return (string.IsNullOrEmpty(ZipPlus)) ? Zip : (Zip + "-" + ZipPlus); } }
        public string CityState { get { return (string.IsNullOrEmpty(City)) ? State : (City + ", " + State); } }
        public string CityST { get { return (string.IsNullOrEmpty(City)) ? StateAbbrev : (City + ", " + StateAbbrev); } }
    }
    
    public class GPS
    {
        public decimal? lat { get; set; }
        public decimal? lon { get; set; }

        public GPS(decimal? lat, decimal? lon)
        {
            this.lat = lat;
            this.lon = lon;
        }

        public GPS(string lat, string lon)
        {
            // convert string to decmial...
            this.lat = decimal.Parse(lat);
            this.lon = decimal.Parse(lon);
        }

        //Return GPS in text format "###.####,-###.####"
        public string TextFormat
        {
            get
            {
                if (lat == null || lon == null)
                {
                    return "Invalid GPS";
                }
                else
                {
                    //Remove trailing zeros from decimals
                    string latString = string.Format("{0:G29}", decimal.Parse(lat.ToString(), System.Globalization.CultureInfo.GetCultureInfo("en-US")));
                    string lonString = string.Format("{0:G29}", decimal.Parse(lon.ToString(), System.Globalization.CultureInfo.GetCultureInfo("en-US")));

                    return latString + "," + lonString;                 
                }
            }
        }

        //Return GPS in link format "###.####,-###.####"
        public string LinkFormat
        {
            get
            {
                if (lat == null || lon == null)
                {
                    return "Invalid GPS";
                }
                else
                {
                    return "<a href=\"http://maps.google.com/maps?z=12&t=k&q=loc:" + TextFormat + "\" target=\"_blank\">" + TextFormat + "</a>";
                }
            }
        }
    }
}
