﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Syncfusion.XlsIO;
using AlonsIT;

namespace DispatchCrude.Core
{
    class XlsIOHelper
    {
        static public void AssignRangeValue(IRange rng, object value, Type dataType = null)
        {
            if (dataType == null) dataType = typeof(String);

            if (dataType == typeof(byte)
                || dataType == typeof(Int16)
                || dataType == typeof(Int32)
                || dataType == typeof(Int64)
                || dataType == typeof(UInt16)
                || dataType == typeof(UInt32)
                || dataType == typeof(UInt64)
                || dataType == typeof(ushort)
                || dataType == typeof(short)
                || dataType == typeof(decimal)
                || dataType == typeof(double)
                || dataType == typeof(float))
            {
                rng.Number = DBHelper.ToDoubleSafe(value ?? 0);
            }
            else if (dataType == typeof(DateTime) || dataType == typeof(DateTime?))
            {
                DateTime? date = Converter.ToNullableDateTime(value);
                if (date.HasValue && date != new DateTime())
                    rng.DateTime = date.Value;
            }
            else if (dataType == typeof(bool))
            {
                rng.Text = DBHelper.ToBoolean(value) ? "TRUE" : "FALSE";
            }
            else
            {
                rng.Text = (value ?? "").ToString();
            }
        }
    }
}
