﻿using System;
using SchedulerManager.Mechanism;
using System.Net;
using System.Net.Mail;
using System.Data;
using DispatchCrude.Models;

namespace SchedulerApp.Jobs
{
    class RepeatableJob : Job
    {
        public override string GetName()
        {
            return this.GetType().Name;
        }
        public override void DoJob()
        {
            SendEmail();
        }
        public override bool IsRepeatable()
        {
            return false;
        }

        static private DispatchCrudeDB _db = new DispatchCrudeDB();

        static void SendEmail()
        {
            UserReportEmailSubscription subscription = _db.UserReportEmailSubscriptions.Find(0);
            
            MailAddress fromAddress = new MailAddress("support@dispatchcrude.com");
            MailAddress toAddress = new MailAddress("jeremiah.silliman@dispatchcommodity.com");

            MailMessage mail = new MailMessage(fromAddress.Address, toAddress.Address);
            mail.IsBodyHtml = true;
            //mail.Subject = "TEST 123";
            mail.Subject = (subscription == null) ? "" : subscription.UserReportID.ToString();
            mail.Body = "<center><b>Test Body</b></center><p><p>This tests the body part of the message with some basic HTML. Thanks for reading!<br/><p><a href='http://www.dispatchcrude.com'>DispatchCrude.com</a><br/>1112 South Villa Drive<br/>Evansville, IN. 47714<br/>(812) 303 - HELP(4357) / Office<br/>(812) 496 - 4195 / Direct<br/>(812) 205 - 2911 / Fax<br/>";

            SmtpClient smtp = new SmtpClient();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;
            smtp.Host = "smtp04.domainlocalhost.com";
            smtp.Port = 587;
            smtp.EnableSsl = true;
            smtp.Timeout = 10000;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential("support@dispatchcrude.com", "Petr01eum");



            System.Net.Mail.Attachment attachment;
            attachment = new System.Net.Mail.Attachment("c:/inetpub/textfile.txt");
            mail.Attachments.Add(attachment);

            try
            {
                smtp.Send(mail);
            }
            catch (Exception)
            {
                
            }
        }
        void smtp_SendCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            if (e.Cancelled == true || e.Error != null)
            {
                System.Console.WriteLine(String.Format("Email was not sent succesfully.".ToString(), this.GetName()));
            }
        }

        public override int GetRepititionIntervalTime()
        {
            throw new NotImplementedException();
        }
    }
}
