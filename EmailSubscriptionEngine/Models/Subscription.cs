﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlonsIT;

namespace DispatchCrude.EmailSubscriptionEngine.Models
{
    class Subscription
    {
        public Subscription()
        {
            Errors = new List<string>();
        }
        public Subscription(DataRow drSubscription): this()
        {
            DBName = drSubscription["DBName"].ToString();
            UserName = drSubscription["UserName"].ToString();
            FileName = drSubscription["Name"].ToString() + ".xlsx";
            EmailAddressCSV = drSubscription["EmailAddressCSV"].ToString().Trim(new char[] { ',' });
            // only "enable" this report subscription for Production sites 
            //  or dev sites when the subscription email address is a single xxx@dispatchcrude.com email address
            Enabled = AppSettings.GetAppSetting("Type").Equals("PROD", StringComparison.CurrentCultureIgnoreCase) 
                || (!EmailAddressCSV.Contains(",") && EmailAddressCSV.ToLower().EndsWith("@dispatchcrude.com"));

            UserReportID = DBHelper.ToInt32(drSubscription["UserReportID"]);
            UserReportEmailSubscriptionID = DBHelper.ToInt32(drSubscription["ID"]);
        }
        public bool Enabled { get; private set; }
        public string DBName { get; private set; }
        public string UserName { get; private set; }
        public string FileName { get; private set; }
        public string EmailAddressCSV { get; private set; }
        public int UserReportID { get; private set; }
        public int UserReportEmailSubscriptionID { get; private set; }
        public DateTime? StartUTC { get; set; }
        public DateTime? EndUTC { get; set; }
        public double? DurationSeconds { get { return new DateDifferential(StartUTC.Value, EndUTC.Value).Milliseconds / 1000.0; } }
        public Stream AttachmentStream { get; set; }
        public List<string> Errors { get; private set; }
        public void AddResult(string error) { if (error != null) Errors.Add(error); }
        public string ErrorCSV { get { return Errors.Count == 0 ? null : string.Join(", ", Errors); } }
    }
}
