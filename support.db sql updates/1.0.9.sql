-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20) = '1.0.8'
	, @NewVersion varchar(20) = '1.0.9'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 0, 'JT-4048 - Add Equipment/Android version reporting to Support database'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO


/* Drop original test version of this procedure */
DROP PROCEDURE _spGetTabletInformation
GO

/********************************************************
* Created: 2017/05/10
* Author: Ben Bloodworth
* SQL Update: 1.0.9
* Purpose: This procedure deletes one or more records associating groups with a user depending upon how it is called.
*          It also removes the records associationg users with permissions (roles) that were contained in any deleted groups.
* Changes:
*	2017/06/15		JAE & BSB		Added driver name
*
* To get the results of this SP on demand run the following against the support database:

-- Drop temp table if it exists from a previous run
IF OBJECT_ID('tempdb..##EQUIPMENT') IS NOT NULL
DROP TABLE ##EQUIPMENT
--Create Temp table for combined results
CREATE TABLE ##EQUIPMENT ([Database] VARCHAR(50), [Phone Number] VARCHAR(50), [Android Version] VARCHAR(50), [Driver App Version] VARCHAR(50)
                              , [Serial Number] VARCHAR(50), [Model Number] VARCHAR(50), [Manufacturer] VARCHAR(50), [Driver] VARCHAR(60), [Last Sync Date UTC] VARCHAR(50))
--Insert results from all relevant databases into the temp table
INSERT INTO ##EQUIPMENT
EXEC sp_msforeachdb N'IF "?" LIKE "Dispatch%.%" AND "?" NOT LIKE "%.Test" AND "?" NOT LIKE "%.Training" AND "?" NOT LIKE "%.Demo"
AND "?" NOT LIKE "%.Dev" AND "?" NOT LIKE "%.BenDev" AND "?" NOT LIKE "%.JoeDev" AND "?" NOT LIKE "%.KevDev"
EXEC [DispatchCrude_Support].dbo.[_spGetTabletInformation] "?"'
--Display the combined results
SELECT * FROM ##EQUIPMENT

********************************************************/
CREATE PROCEDURE _spGetTabletInformation(@DATABASENAME VARCHAR(50)) AS
BEGIN
       DECLARE @sql NVARCHAR(4000);
       SET @sql = N'USE [' + @DATABASENAME + '];       
       SELECT ''' + @DATABASENAME + ''' [Database]
              , DE.PhoneNum [Phone Number]
              , AOSV.Name + '' ('' + AOSV.VersionNumber + '')'' [Android Version]
              , DS.MobileAppVersion [Driver App Version]
              , DE.SerialNum [Serial Number]
              , DE.ModelNum [Model Number]      
              , DS.Manufacturer [Manufacturer]
			  , (D.FirstName + '' '' + D.LastName) [Driver]
              , DE.LastChangeDateUTC [Last Sync Date UTC]
       FROM tblDriverEquipment DE
              CROSS APPLY
                     (SELECT TOP 1 MobileAppVersion, TabletID, AndroidApiLevel, Manufacturer, DriverID
                     FROM tblDriver_Sync
					 WHERE TabletID = DE.PhoneNum
                     ORDER BY LastSyncUTC DESC) DS
              LEFT JOIN tblDriverEquipmentType DET ON DET.ID = DE.DriverEquipmentTypeID
              LEFT JOIN tblAndroidOSVersion AOSV ON AOSV.ApiLevel = DS.AndroidApiLevel
			  LEFT JOIN tblDriver D ON D.ID = DS.DriverID
       WHERE DE.DriverEquipmentTypeID = 1 --TABLET/PHONE
              AND DE.DeleteDateUTC IS NULL
       ORDER BY DE.AndroidApiLevel DESC'
       EXEC(@sql)
END
GO

COMMIT
SET NOEXEC OFF
