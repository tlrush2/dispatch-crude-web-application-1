-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20), @NewVersion varchar(20)
SELECT @CurrVersion = '1.0.1'
SELECT  @NewVersion = '1.0.2'

IF (SELECT MIN(Value) FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'Skip new customer database on email script'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO



UPDATE tblSetting 
   SET Value = 'SELECT name FROM master..sysdatabases WHERE name LIKE ''DispatchCrude.%'' AND name <> ''DispatchCrude.NewCustomer'' OR name LIKE ''DispatchFuel.%'''
   WHERE ID = 1
GO

COMMIT
SET NOEXEC OFF