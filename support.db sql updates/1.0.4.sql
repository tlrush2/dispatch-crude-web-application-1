-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20) = '1.0.3'
	, @NewVersion varchar(20) = '1.0.4'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'Add PROCEDURE to execute a stored procedure on each db'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/*********************************************
Creation Info: 1.0.4 - 2016/07/21
Author: Joe Engler
Description: Execute a no-return stored procedure for each DB (including DEV if parameter is set)
*********************************************/
CREATE PROCEDURE spExecuteAll(@storedProc VARCHAR(200)) AS
BEGIN
	SET NOCOUNT ON;
	
	IF @storedProc IS NULL OR @storedProc = ''
		RETURN

	DECLARE @sql varchar(max)
	SELECT @sql = value FROM tblSetting WHERE ID = 1
	-- retrieve the list of eligible databases
	DECLARE @DBs TABLE (db varchar(100))
	INSERT INTO @DBs EXEC (@sql)

	DECLARE @sqlBase varchar(max) = 'IF (OBJECT_ID(''[%DBNAME%].dbo.%STOREDPROC%'') IS NOT NULL) EXEC (''[%DBNAME%].dbo.%STOREDPROC%'')'
		, @dbName varchar(100)

	-- iterate over each eligible database and execute the stored procedure
	WHILE EXISTS (SELECT * FROM @DBs)
	BEGIN
		SELECT TOP 1 @dbName = db FROM @DBs
		SET @sql = replace(replace(@sqlBase, '%DBNAME%', @dbName), '%STOREDPROC%', @storedProc)
		PRINT 'EXEC(' + @sql + ')' 
		EXEC (@sql)

		DELETE FROM @DBs WHERE db = @dbName
	END

END
GO

COMMIT
SET NOEXEC OFF