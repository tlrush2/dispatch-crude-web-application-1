-- rollback
SET NOEXEC OFF  
DECLARE @CurrVersion varchar(20) = '1.0.6'
	, @NewVersion varchar(20) = '1.0.7'

IF (SELECT TOP 1 Value FROM tblSetting WHERE ID=0) <> @CurrVersion
BEGIN
	DECLARE @msg varchar(255)
	SELECT @msg = 'DB is not at appropriate version to upgrade to ' + @NewVersion
	RAISERROR(@msg, 16, 1)
	SET NOEXEC ON -- terminate processing
END

BEGIN TRANSACTION DBUPDATE

UPDATE tblSetting SET Value = @NewVersion WHERE ID=0

INSERT INTO tblAppChanges (VersionNum, ForPublic, ChangeDescription)
	SELECT @NewVersion, 1, 'Add spRetrieveDispatchCrudeDBTable & use it throughout all stored procedures'
	EXCEPT SELECT VersionNum, ForPublic, ChangeDescription FROM tblAppChanges
GO

/*********************************************
Creation Info: 1.0.7 - 2017.06.01 - Kevin Alons
Purpose: retrieve a list of eligible DispatchCrudeDB databases (based on the incoming parameters)
Parameters: @typeLIKE - specify a different type LIKE value (PROD is default, DEV is all other databases)
			@suffixLIKE - specify a different suffix LIKE value (% is default, specify JOEDEV, etc to get a specific PROD/DEV db)
Changes: 
*********************************************/
CREATE PROCEDURE spRetrieveDispatchCrudeDBTable
(
  @typeLIKE varchar(25) = 'PROD'
, @suffixLIKE varchar(25) = '%'
) AS
BEGIN
	SET NOCOUNT ON;
	/* generate a sql statement to retrieve the eligible DispatchCrudeDB databases */
	DECLARE @sqlDB nvarchar(max)
	SELECT @sqlDB = replace(
			N'SELECT Name 
			FROM (' + Value + N') X 
			WHERE type LIKE "' + @typeLIKE + N'" AND suffix LIKE "' + @suffixLIKE + N'"'
		, '"', '''')
	FROM tblSetting 
	WHERE ID = 1
	
	/* return the list of eligible databases */
	EXEC sp_executesql @sqlDB
END
GO

/*********************************************
Creation Info: 1.0.6 - 2016.05.07 - Kevin Alons
Purpose: retrieve a sql statement based on the provided baseSql value intersected with the eligible databases
Parameters: @sql - the base sql tblSetting.ID of the corresponding base sql statement record
			@typeLIKE - specify a different type LIKE value (PROD is default, DEV is all other databases)
			@suffixLIKE - specify a different suffix LIKE value (% is default, specify JOEDEV, etc to get a specific PROD/DEV db)
			@debug - specify 1 if you want the procedure to only generate the SQL to execute and return it (without execution)
Changes: 
 - 1.0.7 - 2017.06.01 - KDA	- use spRetrieveDispatchCrudeDBTable to retrieve the eligible DispatchCrudeDB databases
 - 1.0.7 - 2017.06.01 - JAE - Pass type and suffix to retrieve db function
*********************************************/
ALTER PROCEDURE spExecuteDispatchCrudeDBSql
(
  @settingID int
, @typeLIKE varchar(25) = 'PROD'
, @suffixLIKE varchar(25) = '%'
, @debug bit = 0
) AS
BEGIN
	SET NOCOUNT ON;
	/* retrieve the list of eligible databases */
	DECLARE @DBs TABLE (db varchar(100))
	INSERT INTO @DBs EXEC spRetrieveDispatchCrudeDBTable @typeLIKE, @suffixLike
	
	DECLARE @sqlBase nvarchar(max) = (SELECT Value FROM tblSetting WHERE ID = @settingID)

	/* generate the final OUTPUT sql statement */
	DECLARE @sqlRun nvarchar(max)
	SELECT @sqlRun = isnull(@sqlRun + '
		UNION ALL ', '') + replace(@sqlBase, '%DBNAME%', db)
	FROM @DBs

	IF (@debug = 1) -- return the statement to execute
		SELECT SQL = @SqlRun
	ELSE -- return the sql results
		EXEC sp_executesql @sqlRun
END

GO

/*********************************************
Creation Info: 1.0.0 - 2016/04/20
Author: Kevin Alons
Description: retrieve all eligible UserReportEmailSubscription records for processing
Changes: 
	- 1.0.1		JAE		2016/06/07	Add day of week check to where clause
	- 1.0.6	- 2017.05.07 - KDA	- simplify the stored procedure with spGenerateDispatchCrudeAllDBSql
	- 1.0.7 - 2017.06.01 - KDA	- simplify with use of EXEC spExecuteDispatchCrudeDBSql
*********************************************/
ALTER PROCEDURE spEligibleUserReportEmailSubscriptions
(
  @typeLIKE varchar(25) = 'PROD'
, @suffixLIKE varchar(25) = '%'
, @debug bit = 0
) AS
BEGIN
	EXEC spExecuteDispatchCrudeDBSql 3, @typeLIKE, @suffixLIKE, @debug
END

GO

/*********************************************
Creation Info: 1.0.4 - 2016/07/21
Author: Joe Engler
Description: Execute a no-return stored procedure for each DB (including DEV if parameter is set)
Changes:
 - 1.0.7 - 2017.06.01 - KDA	- use spRetrieveDispatchCrudeDBTable to retrieve the eligible DispatchCrudeDB databases
 - 1.0.7 - 2017.06.01 - JAE - Pass type and suffix to retrieve db function
							- Removed UNION since execute is for stored procedures
*********************************************/
ALTER PROCEDURE spExecuteAll
(
  @storedProc VARCHAR(200)
, @typeLIKE varchar(25) = 'PROD'
, @suffixLIKE varchar(25) = '%'
, @debug bit = 0
) AS
BEGIN
	SET NOCOUNT ON;
	
	IF @storedProc IS NULL OR @storedProc = ''
		RETURN

	/* retrieve the list of eligible databases */
	DECLARE @DBs TABLE (db varchar(100))
	INSERT INTO @DBs EXEC spRetrieveDispatchCrudeDBTable @typeLIKE, @suffixLIKE

	DECLARE @sqlBase varchar(max) = 
		replace(
			replace(
				'IF (OBJECT_ID("[%DBNAME%].dbo.%STOREDPROC%") IS NOT NULL) EXEC ("[%DBNAME%].dbo.%STOREDPROC%");', '%STOREDPROC%'
				, @storedProc
			)
		, '"', '''')

	/* generate the final OUTPUT sql statement */
	DECLARE @sqlRun nvarchar(max)
	SELECT @sqlRun = isnull(@sqlRun + '
		', '') + replace(@sqlBase, '%DBNAME%', db)
	FROM @DBs

	IF (@debug = 1) -- return the statement to execute
		SELECT SQL = @SqlRun
	ELSE -- return the sql results
		EXEC sp_executesql @sqlRun

END
GO

/*********************************************
Creation Info: 1.0.3 - 2016/07/06
Author: Kevin Alons
Description: execute spOrderProcessStatusChangesAccomplish for each DB (including DEV if parameter is set)
Changes
 - 1.0.7 - 2017.06.01 - KDA	- use spExecuteAll to execute this stored procedure
*********************************************/
ALTER PROCEDURE spOrderProcessStatusChangesAccomplishAll
(
  @typeLIKE varchar(25) = 'PROD'
, @suffixLIKE varchar(25) = '%'
) AS
BEGIN
	EXEC spExecuteAll 'spOrderProcessStatusChangesAccomplish', @typeLIKE, @suffixLIKE
END

GO

COMMIT
SET NOEXEC OFF
